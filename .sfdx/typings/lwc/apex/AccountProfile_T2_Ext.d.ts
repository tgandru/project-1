declare module "@salesforce/apex/AccountProfile_T2_Ext.getAccountInfo" {
  export default function getAccountInfo(param: {accId: any}): Promise<any>;
}
declare module "@salesforce/apex/AccountProfile_T2_Ext.getExistingProfiles" {
  export default function getExistingProfiles(param: {accId: any, profile: any}): Promise<any>;
}
declare module "@salesforce/apex/AccountProfile_T2_Ext.getProfileFields" {
  export default function getProfileFields(param: {profileId: any}): Promise<any>;
}
declare module "@salesforce/apex/AccountProfile_T2_Ext.getProfileInfo" {
  export default function getProfileInfo(param: {profileId: any, accId: any}): Promise<any>;
}
declare module "@salesforce/apex/AccountProfile_T2_Ext.getCYear" {
  export default function getCYear(param: {theProfile: any}): Promise<any>;
}
declare module "@salesforce/apex/AccountProfile_T2_Ext.getContactInfo" {
  export default function getContactInfo(param: {profileId: any}): Promise<any>;
}
declare module "@salesforce/apex/AccountProfile_T2_Ext.getAccountTeam" {
  export default function getAccountTeam(param: {accId: any}): Promise<any>;
}
declare module "@salesforce/apex/AccountProfile_T2_Ext.getProductMarketShare" {
  export default function getProductMarketShare(param: {profileId: any}): Promise<any>;
}
declare module "@salesforce/apex/AccountProfile_T2_Ext.getTop5CYwins" {
  export default function getTop5CYwins(param: {accId: any, theProfile: any}): Promise<any>;
}
declare module "@salesforce/apex/AccountProfile_T2_Ext.getTop5CYcustomers" {
  export default function getTop5CYcustomers(param: {oppList: any}): Promise<any>;
}
declare module "@salesforce/apex/AccountProfile_T2_Ext.getSamsungRevenue" {
  export default function getSamsungRevenue(param: {accId: any, theProfile: any}): Promise<any>;
}
declare module "@salesforce/apex/AccountProfile_T2_Ext.getPipeline" {
  export default function getPipeline(param: {accId: any, theProfile: any}): Promise<any>;
}
declare module "@salesforce/apex/AccountProfile_T2_Ext.Save" {
  export default function Save(param: {profile: any, accProfContacts: any, productShare: any}): Promise<any>;
}
