declare module "@salesforce/apex/AddContactController.getContact" {
  export default function getContact(): Promise<any>;
}
declare module "@salesforce/apex/AddContactController.addContact" {
  export default function addContact(param: {c: any, aId: any}): Promise<any>;
}
