declare module "@salesforce/apex/CallReportController.getCallReport" {
  export default function getCallReport(param: {crId: any}): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.getCallReportB2B" {
  export default function getCallReportB2B(param: {crId: any}): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.getAccount" {
  export default function getAccount(param: {accId: any}): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.getRecordType" {
  export default function getRecordType(param: {recTypeId: any}): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.getselectOptions" {
  export default function getselectOptions(param: {objObject: any, fld: any}): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.getselectOptionsNoSorting" {
  export default function getselectOptionsNoSorting(param: {objObject: any, fld: any}): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.saveCallReport" {
  export default function saveCallReport(param: {callReport: any}): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.submitCallReportB2B" {
  export default function submitCallReportB2B(param: {callReport: any}): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.getTasks" {
  export default function getTasks(param: {callReport: any}): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.getBuildingBlockTasks" {
  export default function getBuildingBlockTasks(param: {callReport: any}): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.saveTasks" {
  export default function saveTasks(param: {tasks: any, callReport: any}): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.getProductLine" {
  export default function getProductLine(param: {callReport: any}): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.saveProductLine" {
  export default function saveProductLine(param: {plines: any, callReport: any}): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.getMeetingAttendees" {
  export default function getMeetingAttendees(param: {callReport: any}): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.saveMeetingAttendees" {
  export default function saveMeetingAttendees(param: {ma: any, callReport: any}): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.getDefaultSamsungAttendees" {
  export default function getDefaultSamsungAttendees(): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.getSamsungAttendees" {
  export default function getSamsungAttendees(param: {callReport: any}): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.saveSamsungAttendees" {
  export default function saveSamsungAttendees(param: {sa: any, callReport: any}): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.getSubAccount" {
  export default function getSubAccount(param: {callReport: any}): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.saveSubAccount" {
  export default function saveSubAccount(param: {subAcc: any, callReport: any}): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.getDistributionLists" {
  export default function getDistributionLists(param: {callReport: any}): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.saveDistributionLists" {
  export default function saveDistributionLists(param: {distList: any, callReport: any}): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.getCRDistributionLists" {
  export default function getCRDistributionLists(param: {callReport: any}): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.saveCRDistributionLists" {
  export default function saveCRDistributionLists(param: {CRdistList: any, callReport: any}): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.DeleteDistributionLists" {
  export default function DeleteDistributionLists(param: {DistListNames: any, CallRprt: any}): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.deleteRecords" {
  export default function deleteRecords(param: {tasks: any, pCategories: any, crAttendee: any, crSamAttendee: any, crSubAcc: any, crNotifyMems: any, crDistLists: any, crMI: any, crBP: any}): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.getDependentOptionsImpl" {
  export default function getDependentOptionsImpl(param: {objApiName: any, contrfieldApiName: any, depfieldApiName: any}): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.getCBDProdCategoryOptions" {
  export default function getCBDProdCategoryOptions(param: {objApiName: any, contrfieldApiName: any, depfieldApiName: any}): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.getDependentOptionsImplB2B" {
  export default function getDependentOptionsImplB2B(param: {objApiName: any, contrfieldApiName: any, depfieldApiName: any}): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.getOptionsCallReportDivision" {
  export default function getOptionsCallReportDivision(param: {objObject: any, fld: any, objApiName: any, contrfieldApiName: any, depfieldApiName: any, division: any}): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.getCurrentLoginUserName" {
  export default function getCurrentLoginUserName(): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.getInstanceUrl" {
  export default function getInstanceUrl(): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.getFiles" {
  export default function getFiles(param: {parentId: any}): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.getCRnotificationMembers" {
  export default function getCRnotificationMembers(param: {callReport: any}): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.saveCRnotificationMembers" {
  export default function saveCRnotificationMembers(param: {crnmList: any, callReport: any}): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.getRecordAccess" {
  export default function getRecordAccess(param: {callReport: any}): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.getMarketIntelligence" {
  export default function getMarketIntelligence(param: {callReport: any}): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.saveMarketIntelligence" {
  export default function saveMarketIntelligence(param: {CallRepMI: any, callReport: any}): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.getBizPerformance" {
  export default function getBizPerformance(param: {callReport: any}): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.saveBizPerformance" {
  export default function saveBizPerformance(param: {CallRepBP: any, callReport: any}): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.getMIhoverText" {
  export default function getMIhoverText(): Promise<any>;
}
declare module "@salesforce/apex/CallReportController.getMIButtonValues" {
  export default function getMIButtonValues(): Promise<any>;
}
