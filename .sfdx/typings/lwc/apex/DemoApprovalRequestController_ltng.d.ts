declare module "@salesforce/apex/DemoApprovalRequestController_ltng.SubmitdemoforApproval" {
  export default function SubmitdemoforApproval(param: {recordid: any}): Promise<any>;
}
