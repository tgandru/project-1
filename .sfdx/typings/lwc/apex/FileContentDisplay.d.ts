declare module "@salesforce/apex/FileContentDisplay.getFiles" {
  export default function getFiles(param: {parentId: any}): Promise<any>;
}
declare module "@salesforce/apex/FileContentDisplay.deleteFiles" {
  export default function deleteFiles(param: {fileId: any}): Promise<any>;
}
