declare module "@salesforce/apex/HARolloutPageController_Ltng.getRolloutInformation" {
  export default function getRolloutInformation(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/HARolloutPageController_Ltng.getOpportunityInformation" {
  export default function getOpportunityInformation(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/HARolloutPageController_Ltng.getCategoryList" {
  export default function getCategoryList(): Promise<any>;
}
declare module "@salesforce/apex/HARolloutPageController_Ltng.getRolloutProductInfo" {
  export default function getRolloutProductInfo(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/HARolloutPageController_Ltng.getRolloutProductInfoByFamily" {
  export default function getRolloutProductInfoByFamily(param: {recordId: any, family: any}): Promise<any>;
}
declare module "@salesforce/apex/HARolloutPageController_Ltng.getROSInformation" {
  export default function getROSInformation(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/HARolloutPageController_Ltng.getROSInformationHelper" {
  export default function getROSInformationHelper(param: {ROPids: any}): Promise<any>;
}
