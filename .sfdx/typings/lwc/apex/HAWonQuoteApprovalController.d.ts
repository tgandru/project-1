declare module "@salesforce/apex/HAWonQuoteApprovalController.submitForWonApproval" {
  export default function submitForWonApproval(param: {OppId: any}): Promise<any>;
}
