declare module "@salesforce/apex/KNOXApprovalCancelRequest.cancelrequest" {
  export default function cancelrequest(param: {soid: any}): Promise<any>;
}
