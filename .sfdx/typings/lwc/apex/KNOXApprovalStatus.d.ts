declare module "@salesforce/apex/KNOXApprovalStatus.getApprovalStatus" {
  export default function getApprovalStatus(param: {soid: any}): Promise<any>;
}
