declare module "@salesforce/apex/Lookup2.searchDB" {
  export default function searchDB(param: {objectName: any, fld_API_Text: any, fld_API_Text2: any, fld_API_Val: any, lim: any, fld_API_Search: any, fld_API_Search2: any, searchText: any, parentId: any}): Promise<any>;
}
