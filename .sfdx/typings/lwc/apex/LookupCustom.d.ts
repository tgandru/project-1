declare module "@salesforce/apex/LookupCustom.searchDB" {
  export default function searchDB(param: {objectName: any, fld_API_Text: any, fld_API_Val: any, lim: any, fld_API_Search: any, searchText: any, parentField: any, parentRecordId: any}): Promise<any>;
}
