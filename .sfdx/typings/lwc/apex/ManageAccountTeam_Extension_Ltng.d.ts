declare module "@salesforce/apex/ManageAccountTeam_Extension_Ltng.getTeamInformation" {
  export default function getTeamInformation(param: {AccId: any}): Promise<any>;
}
declare module "@salesforce/apex/ManageAccountTeam_Extension_Ltng.getActiveUsers" {
  export default function getActiveUsers(): Promise<any>;
}
declare module "@salesforce/apex/ManageAccountTeam_Extension_Ltng.getselectOptionsNoSorting" {
  export default function getselectOptionsNoSorting(param: {objObject: any, fld: any}): Promise<any>;
}
declare module "@salesforce/apex/ManageAccountTeam_Extension_Ltng.UpdateTeamMenbers" {
  export default function UpdateTeamMenbers(param: {Insertlist: any}): Promise<any>;
}
declare module "@salesforce/apex/ManageAccountTeam_Extension_Ltng.DeleteTeamMenbers" {
  export default function DeleteTeamMenbers(param: {deletelist: any}): Promise<any>;
}
