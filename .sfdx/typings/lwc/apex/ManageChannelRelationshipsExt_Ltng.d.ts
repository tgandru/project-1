declare module "@salesforce/apex/ManageChannelRelationshipsExt_Ltng.getPartnerList" {
  export default function getPartnerList(param: {OppId: any, rolesStr: any}): Promise<any>;
}
