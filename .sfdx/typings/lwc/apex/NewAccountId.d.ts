declare module "@salesforce/apex/NewAccountId.updateAccountId" {
  export default function updateAccountId(param: {accId: any}): Promise<any>;
}
