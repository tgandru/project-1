declare module "@salesforce/apex/NewOpportunity_Ltng.getAccountInfo" {
  export default function getAccountInfo(param: {accId: any}): Promise<any>;
}
