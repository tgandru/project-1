declare module "@salesforce/apex/OpportunityCustomCloneController.getOppInformation" {
  export default function getOppInformation(param: {oppid: any}): Promise<any>;
}
declare module "@salesforce/apex/OpportunityCustomCloneController.getRelatedlistInformation" {
  export default function getRelatedlistInformation(param: {oppid: any}): Promise<any>;
}
declare module "@salesforce/apex/OpportunityCustomCloneController.Saverecords" {
  export default function Saverecords(param: {oppid: any, relatedliststring: any}): Promise<any>;
}
declare module "@salesforce/apex/OpportunityCustomCloneController.CloneChannelRecords" {
  export default function CloneChannelRecords(param: {newOpp: any, OldOpp: any}): Promise<any>;
}
