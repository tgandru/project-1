declare module "@salesforce/apex/OpportunityDemoRequestController_Ltng.getOppInformation" {
  export default function getOppInformation(param: {Oppid: any}): Promise<any>;
}
declare module "@salesforce/apex/OpportunityDemoRequestController_Ltng.getInstanceUrl" {
  export default function getInstanceUrl(): Promise<any>;
}
declare module "@salesforce/apex/OpportunityDemoRequestController_Ltng.getOppContactRoles" {
  export default function getOppContactRoles(param: {Oppid: any}): Promise<any>;
}
declare module "@salesforce/apex/OpportunityDemoRequestController_Ltng.SaveDemorecord" {
  export default function SaveDemorecord(param: {newrecord: any}): Promise<any>;
}
