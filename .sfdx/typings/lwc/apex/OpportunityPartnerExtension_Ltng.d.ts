declare module "@salesforce/apex/OpportunityPartnerExtension_Ltng.getOppDetails" {
  export default function getOppDetails(param: {oppId: any}): Promise<any>;
}
declare module "@salesforce/apex/OpportunityPartnerExtension_Ltng.getSelectedPartners" {
  export default function getSelectedPartners(param: {opptyId: any}): Promise<any>;
}
declare module "@salesforce/apex/OpportunityPartnerExtension_Ltng.getSelectedPartnersWrapper2" {
  export default function getSelectedPartnersWrapper2(param: {opptyId: any}): Promise<any>;
}
declare module "@salesforce/apex/OpportunityPartnerExtension_Ltng.getChannelList" {
  export default function getChannelList(param: {opptyId: any}): Promise<any>;
}
declare module "@salesforce/apex/OpportunityPartnerExtension_Ltng.getChannelMap" {
  export default function getChannelMap(param: {allChannelRelations: any}): Promise<any>;
}
declare module "@salesforce/apex/OpportunityPartnerExtension_Ltng.removePartnerAndItsChannels" {
  export default function removePartnerAndItsChannels(param: {spw: any, channelList: any}): Promise<any>;
}
declare module "@salesforce/apex/OpportunityPartnerExtension_Ltng.updateAvailableList" {
  export default function updateAvailableList(param: {searchString: any, role: any, opp: any}): Promise<any>;
}
declare module "@salesforce/apex/OpportunityPartnerExtension_Ltng.onSave" {
  export default function onSave(param: {opp: any, selectedPartners: any, deletePartners: any, selectedPartnersList: any, selAccntIds: any, channelRelationCheck: any, channelsToDelete: any}): Promise<any>;
}
declare module "@salesforce/apex/OpportunityPartnerExtension_Ltng.getselectOptions" {
  export default function getselectOptions(param: {objObject: any, fld: any}): Promise<any>;
}
declare module "@salesforce/apex/OpportunityPartnerExtension_Ltng.getUIThemeDescription" {
  export default function getUIThemeDescription(): Promise<any>;
}
