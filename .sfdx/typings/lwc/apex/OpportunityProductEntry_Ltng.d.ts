declare module "@salesforce/apex/OpportunityProductEntry_Ltng.getSKUInformation" {
  export default function getSKUInformation(param: {opp: any}): Promise<any>;
}
declare module "@salesforce/apex/OpportunityProductEntry_Ltng.getInstanceUrl" {
  export default function getInstanceUrl(): Promise<any>;
}
declare module "@salesforce/apex/OpportunityProductEntry_Ltng.getPBEList" {
  export default function getPBEList(param: {selectedProdGroup: any, searchString: any, opp: any}): Promise<any>;
}
declare module "@salesforce/apex/OpportunityProductEntry_Ltng.getOppInformation" {
  export default function getOppInformation(param: {Oppid: any}): Promise<any>;
}
declare module "@salesforce/apex/OpportunityProductEntry_Ltng.getHasunitcostPermission" {
  export default function getHasunitcostPermission(): Promise<any>;
}
declare module "@salesforce/apex/OpportunityProductEntry_Ltng.getHasmanageAp1Permission" {
  export default function getHasmanageAp1Permission(): Promise<any>;
}
declare module "@salesforce/apex/OpportunityProductEntry_Ltng.getOliInformation" {
  export default function getOliInformation(param: {opp: any}): Promise<any>;
}
declare module "@salesforce/apex/OpportunityProductEntry_Ltng.getQliInformation" {
  export default function getQliInformation(param: {opp: any}): Promise<any>;
}
declare module "@salesforce/apex/OpportunityProductEntry_Ltng.SaveLineItems" {
  export default function SaveLineItems(param: {lineitemstring: any, opp: any}): Promise<any>;
}
declare module "@salesforce/apex/OpportunityProductEntry_Ltng.DeleteLineItems" {
  export default function DeleteLineItems(param: {Deletelineitems: any}): Promise<any>;
}
declare module "@salesforce/apex/OpportunityProductEntry_Ltng.getUIThemeDescription" {
  export default function getUIThemeDescription(): Promise<any>;
}
