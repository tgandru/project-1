declare module "@salesforce/apex/PriceBookSelectionController_Ltng.getOppInformation" {
  export default function getOppInformation(param: {Oppid: any}): Promise<any>;
}
declare module "@salesforce/apex/PriceBookSelectionController_Ltng.getAvilablePriceBooks" {
  export default function getAvilablePriceBooks(param: {opp: any}): Promise<any>;
}
declare module "@salesforce/apex/PriceBookSelectionController_Ltng.SaveOpportunityRecord" {
  export default function SaveOpportunityRecord(param: {Opp: any, pbid: any}): Promise<any>;
}
declare module "@salesforce/apex/PriceBookSelectionController_Ltng.getInstanceUrl" {
  export default function getInstanceUrl(): Promise<any>;
}
