declare module "@salesforce/apex/ProjectRegistrationApprovalExt_Ltng.getOpportunity" {
  export default function getOpportunity(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/ProjectRegistrationApprovalExt_Ltng.submitProjectRegistration" {
  export default function submitProjectRegistration(param: {OppId: any}): Promise<any>;
}
declare module "@salesforce/apex/ProjectRegistrationApprovalExt_Ltng.submitWinApproval" {
  export default function submitWinApproval(param: {opp: any}): Promise<any>;
}
