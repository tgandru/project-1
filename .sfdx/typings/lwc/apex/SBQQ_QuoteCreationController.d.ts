declare module "@salesforce/apex/SBQQ_QuoteCreationController.getOpportunityInfo" {
  export default function getOpportunityInfo(param: {oppId: any}): Promise<any>;
}
