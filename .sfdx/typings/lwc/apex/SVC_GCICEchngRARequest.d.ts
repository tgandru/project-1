declare module "@salesforce/apex/SVC_GCICEchngRARequest.GCICEchngRARequest" {
  export default function GCICEchngRARequest(param: {csid: any}): Promise<any>;
}
