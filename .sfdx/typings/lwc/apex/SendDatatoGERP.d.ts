declare module "@salesforce/apex/SendDatatoGERP.CreateMQRecords" {
  export default function CreateMQRecords(param: {soId: any}): Promise<any>;
}
