declare module "@salesforce/resourceUrl/Angular_Bootstrap" {
    var Angular_Bootstrap: string;
    export default Angular_Bootstrap;
}
declare module "@salesforce/resourceUrl/CaseManageReportCSS" {
    var CaseManageReportCSS: string;
    export default CaseManageReportCSS;
}
declare module "@salesforce/resourceUrl/Confetti" {
    var Confetti: string;
    export default Confetti;
}
declare module "@salesforce/resourceUrl/Down_Arrow_Icon" {
    var Down_Arrow_Icon: string;
    export default Down_Arrow_Icon;
}
declare module "@salesforce/resourceUrl/FiscalCalendarTestData" {
    var FiscalCalendarTestData: string;
    export default FiscalCalendarTestData;
}
declare module "@salesforce/resourceUrl/LDS_212" {
    var LDS_212: string;
    export default LDS_212;
}
declare module "@salesforce/resourceUrl/Left_Arrow_Icon" {
    var Left_Arrow_Icon: string;
    export default Left_Arrow_Icon;
}
declare module "@salesforce/resourceUrl/LightningMassEditJS" {
    var LightningMassEditJS: string;
    export default LightningMassEditJS;
}
declare module "@salesforce/resourceUrl/LightningMassUpdateJS" {
    var LightningMassUpdateJS: string;
    export default LightningMassUpdateJS;
}
declare module "@salesforce/resourceUrl/Lightning_Utility_Circle_Icon_Grey" {
    var Lightning_Utility_Circle_Icon_Grey: string;
    export default Lightning_Utility_Circle_Icon_Grey;
}
declare module "@salesforce/resourceUrl/Lightning_Utility_Circle_Icon_Orange" {
    var Lightning_Utility_Circle_Icon_Orange: string;
    export default Lightning_Utility_Circle_Icon_Orange;
}
declare module "@salesforce/resourceUrl/Lightning_Utility_Down_Grey_Icon" {
    var Lightning_Utility_Down_Grey_Icon: string;
    export default Lightning_Utility_Down_Grey_Icon;
}
declare module "@salesforce/resourceUrl/Lightning_Utility_Up_Green_Icon" {
    var Lightning_Utility_Up_Green_Icon: string;
    export default Lightning_Utility_Up_Green_Icon;
}
declare module "@salesforce/resourceUrl/Lightning_Utility_Up_Green_Icon_PDF" {
    var Lightning_Utility_Up_Green_Icon_PDF: string;
    export default Lightning_Utility_Up_Green_Icon_PDF;
}
declare module "@salesforce/resourceUrl/Lightning_Utility_Up_Grey_Icon" {
    var Lightning_Utility_Up_Grey_Icon: string;
    export default Lightning_Utility_Up_Grey_Icon;
}
declare module "@salesforce/resourceUrl/Lightning_Utility_Up_Red_Icon_PDF" {
    var Lightning_Utility_Up_Red_Icon_PDF: string;
    export default Lightning_Utility_Up_Red_Icon_PDF;
}
declare module "@salesforce/resourceUrl/Lightning_Utility_down_Green_Icon" {
    var Lightning_Utility_down_Green_Icon: string;
    export default Lightning_Utility_down_Green_Icon;
}
declare module "@salesforce/resourceUrl/Lightning_Utility_down_Red_Icon" {
    var Lightning_Utility_down_Red_Icon: string;
    export default Lightning_Utility_down_Red_Icon;
}
declare module "@salesforce/resourceUrl/MassEditCSS" {
    var MassEditCSS: string;
    export default MassEditCSS;
}
declare module "@salesforce/resourceUrl/MassUpdateCSS" {
    var MassUpdateCSS: string;
    export default MassUpdateCSS;
}
declare module "@salesforce/resourceUrl/ModalWidthCSS" {
    var ModalWidthCSS: string;
    export default ModalWidthCSS;
}
declare module "@salesforce/resourceUrl/ReleaseNote" {
    var ReleaseNote: string;
    export default ReleaseNote;
}
declare module "@salesforce/resourceUrl/ReqFieldsInspectorCSS" {
    var ReqFieldsInspectorCSS: string;
    export default ReqFieldsInspectorCSS;
}
declare module "@salesforce/resourceUrl/ReqFieldsInspectorJS" {
    var ReqFieldsInspectorJS: string;
    export default ReqFieldsInspectorJS;
}
declare module "@salesforce/resourceUrl/Right_Arrow_Icon" {
    var Right_Arrow_Icon: string;
    export default Right_Arrow_Icon;
}
declare module "@salesforce/resourceUrl/S1PageLayoutResources" {
    var S1PageLayoutResources: string;
    export default S1PageLayoutResources;
}
declare module "@salesforce/resourceUrl/SLDS221" {
    var SLDS221: string;
    export default SLDS221;
}
declare module "@salesforce/resourceUrl/SamsungLogo" {
    var SamsungLogo: string;
    export default SamsungLogo;
}
declare module "@salesforce/resourceUrl/SamsungLogo1" {
    var SamsungLogo1: string;
    export default SamsungLogo1;
}
declare module "@salesforce/resourceUrl/SamsungLogoWE" {
    var SamsungLogoWE: string;
    export default SamsungLogoWE;
}
declare module "@salesforce/resourceUrl/SamsungPDFBlueLogo" {
    var SamsungPDFBlueLogo: string;
    export default SamsungPDFBlueLogo;
}
declare module "@salesforce/resourceUrl/Samsung_Logo2" {
    var Samsung_Logo2: string;
    export default Samsung_Logo2;
}
declare module "@salesforce/resourceUrl/SaveQuoteAsCustomPDF" {
    var SaveQuoteAsCustomPDF: string;
    export default SaveQuoteAsCustomPDF;
}
declare module "@salesforce/resourceUrl/SaveQuoteAsCustomPDFandEmail" {
    var SaveQuoteAsCustomPDFandEmail: string;
    export default SaveQuoteAsCustomPDFandEmail;
}
declare module "@salesforce/resourceUrl/SiteSamples" {
    var SiteSamples: string;
    export default SiteSamples;
}
declare module "@salesforce/resourceUrl/Tableau_Logo" {
    var Tableau_Logo: string;
    export default Tableau_Logo;
}
declare module "@salesforce/resourceUrl/Up_Arrow_Icon" {
    var Up_Arrow_Icon: string;
    export default Up_Arrow_Icon;
}
declare module "@salesforce/resourceUrl/UtilJS" {
    var UtilJS: string;
    export default UtilJS;
}
declare module "@salesforce/resourceUrl/errorgif" {
    var errorgif: string;
    export default errorgif;
}
declare module "@salesforce/resourceUrl/jQuery" {
    var jQuery: string;
    export default jQuery;
}
declare module "@salesforce/resourceUrl/jQueryDataTablesZip" {
    var jQueryDataTablesZip: string;
    export default jQueryDataTablesZip;
}
declare module "@salesforce/resourceUrl/jquery224" {
    var jquery224: string;
    export default jquery224;
}
declare module "@salesforce/resourceUrl/jqueryblockUI" {
    var jqueryblockUI: string;
    export default jqueryblockUI;
}
declare module "@salesforce/resourceUrl/loader" {
    var loader: string;
    export default loader;
}
declare module "@salesforce/resourceUrl/loader2" {
    var loader2: string;
    export default loader2;
}
declare module "@salesforce/resourceUrl/loader3" {
    var loader3: string;
    export default loader3;
}
declare module "@salesforce/resourceUrl/loader4" {
    var loader4: string;
    export default loader4;
}
declare module "@salesforce/resourceUrl/multiSelect" {
    var multiSelect: string;
    export default multiSelect;
}
declare module "@salesforce/resourceUrl/select2" {
    var select2: string;
    export default select2;
}
declare module "@salesforce/resourceUrl/sucessgif" {
    var sucessgif: string;
    export default sucessgif;
}
