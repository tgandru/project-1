({
	myAction : function(component, event, helper) {
		var recId=component.get("v.recordId");
         var action= component.get("c.getTeamInformation");
         action.setParams({
            "AccId":recId
        });
        action.setCallback(this, function(response){
            	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('Response:: '+ JSON.stringify(response.getReturnValue()));
                component.set("v.Team",response.getReturnValue());
               	helper.fetchPickListVal(component, {'sobjectType':'AccountTeamMember'},'TeamMemberRole','RoleOptions');
                helper.GetActiveUser(component, event);
            }
        });
        $A.enqueueAction(action); 
	},
     removeRow: function(component, event, helper) {
        
        var accountteamList = component.get("v.Team");
        var selectedItem = event.currentTarget;
        var index = selectedItem.dataset.record;
         if(accountteamList[index]!=undefined && accountteamList[index].Id!=undefined){
            var deleterec=component.get("v.deletelist");
            deleterec.push(accountteamList[index]);  
         }
        accountteamList.splice(index, 1);
         if(accountteamList !=null){
             component.set("v.Team", accountteamList);
         }
        
          
    },
    
    addRow: function(component, event, helper) {
        helper.addNewRecord(component, event);
    },
    
    cancelBtn : function(component, event, helper) {
        // Close the action panel
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    },
    SaveRecords : function(component, event, helper) {
		var insertlist=component.get("v.Team");
        var recId=component.get("v.recordId");
         helper.validateFields(component,event);        
          for (var indexVar = 0; indexVar < insertlist.length; indexVar++) {
              if(insertlist[indexVar].UserId!=undefined){
                  if (insertlist[indexVar].UserId.val!=undefined && insertlist[indexVar].UserId!=undefined){
                        insertlist[indexVar].UserId=insertlist[indexVar].UserId.val;
                        insertlist[indexVar].AccountId=recId;
                    }
              } 
              
        }
        
         var isValid=component.get("v.isValid");
        if(isValid){
         var action= component.get("c.UpdateTeamMenbers");
         action.setParams({
            "Insertlist":insertlist
        });
        action.setCallback(this, function(response){
            	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('Save Response:: '+ JSON.stringify(response.getReturnValue()));

                var returnval=response.getReturnValue();
                if (typeof returnval === 'Ok' || returnval === "Ok" || returnval == "Ok"){
                      component.set("v.showError", false);
                }else{
                     component.set("v.showError", true);
                      component.set("v.Message", response.getReturnValue());
                }
                     helper.DeleteRecords(component, event);

            }else{
                var errors = response.getError(); 
                component.set("v.showError", true);
                component.set("v.Message", errors[0].message);
            }
        });
             $A.enqueueAction(action); 
        }
       
	}


})