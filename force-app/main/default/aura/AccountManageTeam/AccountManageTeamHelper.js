({
	addNewRecord: function(component, event) {
        
        var teamList = component.get("v.Team");
         var accid = component.get("v.recordId");
        var userId = $A.get("$SObjectType.CurrentUser.Id");
        
     
        teamList.push({
            'sobjectType': 'AccountTeamMember',
            
        });
        
        component.set("v.Team", teamList);
    },
    
     fetchPickListVal: function(component,objectName,fieldName,  optionList) {
        var action = component.get("c.getselectOptionsNoSorting");

        action.setParams({
            "objObject": objectName,
            "fld": fieldName
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();

                if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        label: "--- None ---",
                        value: ""
                    });
                }
                for (var i = 0; i < allValues.length; i++) {
                    if(fieldName=='Status__c'){
                        if(allValues[i]=='Submitted' || allValues[i]=='Cancelled') continue;
                    }
                    opts.push({
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                
                //console.log('opts='+JSON.stringify(opts));
                //component.find(elementId).set("v.options", opts);
                component.set("v."+optionList, opts);
            }
        });
        $A.enqueueAction(action);
    },
    GetActiveUser : function(component, event) {
        var action= component.get("c.getActiveUsers");
        action.setCallback(this, function(response){
            var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                 
                var returnval=response.getReturnValue();
                component.set("v.ActiveUsers", returnval);
            }
        });
          $A.enqueueAction(action);       
    },
     DeleteRecords : function(component, event) {
		var deletelist=component.get("v.deletelist");
        var recId=component.get("v.recordId");
          for (var indexVar = 0; indexVar < deletelist.length; indexVar++) {
            if (deletelist[indexVar].UserId!=undefined){
            	deletelist[indexVar].UserId=deletelist[indexVar].UserId.val;
                deletelist[indexVar].AccountId=recId;
            }
        }
        
         var action= component.get("c.DeleteTeamMenbers");
         action.setParams({
            "deletelist":deletelist
        });
        action.setCallback(this, function(response){
            	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                 
                var returnval=response.getReturnValue();
                console.log(returnval);
                if (typeof returnval === 'Ok' || returnval === "Ok" || retrunval == "Ok"){
                    console.log('close window');
                    var showmsg= component.get("v.showError");
                    if(!showmsg){
                         $A.get("e.force:closeQuickAction").fire();
                          $A.get('e.force:refreshView').fire();
                    }
                  
                }else{
                     component.set("v.showError", true);
                     component.set("v.Message", returnval);
                }
                 
            }else{
                var errors = response.getError(); 
                component.set("v.showError", true);
                component.set("v.Message", errors[0].message);
            }
        });
        $A.enqueueAction(action); 
	},
    
     validateFields: function(component, event)
    {
        var insertlist=component.get("v.Team");
        var isValid=true;
        var msg='';
        var activeusers=component.get("v.ActiveUsers");
         for(var indexVar = 0; indexVar < insertlist.length; indexVar++){
            if(insertlist[indexVar].UserId!=undefined){
                if (insertlist[indexVar].UserId.val!=undefined && insertlist[indexVar].UserId!=undefined){
                    var found = false;
                    for(var i = 0; i < activeusers.length; i++) {
                        if (activeusers[i].Id == insertlist[indexVar].UserId.val) {
                            found = true;
                            break;
                        }
                    }
                    
                    if(found==false){
                        isValid=false;
                         msg += 'Please Select Valid ("Active") Team Member';
                        console.log('Inactive User');
                        component.set("v.showError", true);
                    }
                }
                
                
            }else{
                isValid=false;
                 msg += 'Please Select Team Member';
                console.log('No User');
                component.set("v.showError", true);
            }
            console.log('Role::'+insertlist[indexVar].TeamMemberRole);
            if(insertlist[indexVar].TeamMemberRole!=undefined){
                if(insertlist[indexVar].TeamMemberRole=='--- None ---'||insertlist[indexVar].TeamMemberRole==''){
                     isValid=false;
                    msg += 'Please Select Team Member Role ';
                    console.log('No Role');
                    component.set("v.showError", true);
                }
            }else{
                isValid=false;
                 msg += 'Please Select Team Member Role ';
                 console.log('No Role');
                component.set("v.showError", true);
            }
        }
        component.set("v.isValid",isValid);
        component.set("v.Message",msg);
        
    }

})