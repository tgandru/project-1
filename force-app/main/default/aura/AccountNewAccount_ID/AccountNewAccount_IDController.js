({
	doInit:function(component,event,helper){
		var action= component.get("c.updateAccountId");
         action.setParams({
            "accId":  component.get("v.recordId")
        });
        action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                var resp=response.getReturnValue();
                component.set("v.respn",response.getReturnValue());
            }});
            $A.enqueueAction(action);
      
         
    }
})