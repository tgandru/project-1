({
	doInit : function(component, event, helper) {
        console.log('redirecting to call report1s...' + component.get("v.recordId"));
        
		var urlEvent = $A.get("e.force:navigateToURL");
        var url = 'https://samsungsea--qa.lightning.force.com/c/CallReportApp.app?accountId='+component.get("v.recordId")
        //window.location = url;
        urlEvent.setParams({
            "url": "#/sObject/SCallReport__c/new?c__accountId=" + component.get("v.recordId")
        });
        console.log('url---------->'+url);
        urlEvent.fire();
        
        
        /*
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:CallReport",
            componentAttributes: {
                accountId : component.get("v.recordId")
            }
        });
        evt.fire();
        */
	}
})