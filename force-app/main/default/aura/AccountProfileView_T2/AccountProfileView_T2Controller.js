({
	doInit : function(component, event, helper) {
        var profileId=component.get("v.recordId");
        var accId=component.get("v.accountId");
        
        helper.getFields(component,event);
        helper.getContactInfo(component,event,profileId);
        
        var action = component.get("c.getProfileInfo");
        action.setParams({
            "profileId":profileId,
            "accId":accId
        });
        action.setCallback(this, function(response){
            var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('profile==='+ JSON.stringify(response.getReturnValue()));
                var resp = response.getReturnValue();
                component.set("v.profile",resp);
                
                helper.getCYear(component,event,resp);
                helper.getProductMarketShare(component,event,profileId);
                
                if(resp.Account__c!=null && resp.Account__c!=undefined){
                    helper.getAccountTeam(component,event,resp.Account__c);
                    helper.getTop5CYwins(component,event,helper,resp.Account__c,resp);
                    //helper.getSamsungRevenue(component,event,resp.Account__c,resp); //Commented on 09/17/2019
                    helper.getPipeline(component,event,resp.Account__c,resp);
                }
            }
        });
        $A.enqueueAction(action);
	},
    Edit : function (component, event, helper) {
 		var profId=component.get("v.recordId");
        window.location.href = "/apex/AccountProfilePage_T2?id="+profId;
    },
    viewAsPDF : function (component, event, helper) {
 		var profId=component.get("v.recordId");
        window.location.href = "/apex/AccountProfile_T2_PDF?id="+profId;
    },
})