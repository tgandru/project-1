({
	getFields : function(component,event) {
		var action= component.get("c.getProfileFields");
        action.setCallback(this, function(response){
            var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('Fields==='+ JSON.stringify(response.getReturnValue()));
                component.set("v.fields",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
	},
    getCYear : function(component,event,profile) {
		var action= component.get("c.getCYear");
        action.setParams({
            "theProfile":profile
        });
        action.setCallback(this, function(response){
            var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('CYear==='+ JSON.stringify(response.getReturnValue()));
                var resp = 0;
                resp = response.getReturnValue();
                var years = [];
                years.push(resp);
                years.push(resp-1);
                console.log('Years==='+ years);
                component.set("v.years",years);
            }
        });
        $A.enqueueAction(action);
	},
    getContactInfo : function(component,event,profileId) {
		var action= component.get("c.getContactInfo");
        action.setParams({
            "profileId":profileId
        });
        action.setCallback(this, function(response){
            var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('Contacts==='+ JSON.stringify(response.getReturnValue()));
                component.set("v.profileContacts",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
	},
    getProductMarketShare : function(component,event,profileId) {
        console.log('product share profileId---->'+profileId);
		var action= component.get("c.getProductMarketShare");
        action.setParams({
            "profileId":profileId
        });
        action.setCallback(this, function(response){
            var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('productShare==='+ JSON.stringify(response.getReturnValue()));
                component.set("v.productShare",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
	},
    getAccountTeam : function(component,event,accId) {
		var action= component.get("c.getAccountTeam");
        action.setParams({
            "accId":accId
        });
        action.setCallback(this, function(response){
            var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('Account Team==='+ JSON.stringify(response.getReturnValue()));
                component.set("v.accountTeam",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
	},
    getTop5CYwins : function(component,event,helper,accId,profile) {
		var action= component.get("c.getTop5CYwins");
        action.setParams({
            "accId":accId,
            "theProfile":profile
        });
        action.setCallback(this, function(response){
            var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('top5CYwins==='+ JSON.stringify(response.getReturnValue()));
                component.set("v.top5CYwins",response.getReturnValue());
            }
            this.getTop5CYcustomers(component,event,response.getReturnValue());
        });
        $A.enqueueAction(action);
	},
    getTop5CYcustomers : function(component,event,oppList) {
		var action= component.get("c.getTop5CYcustomers");
        action.setParams({
            "oppList":oppList
        });
        action.setCallback(this, function(response){
            var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('Top5CYcustomers==='+ JSON.stringify(response.getReturnValue()));
                component.set("v.top5CYcustomers",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
	},
    /* Commented on 09/17/2019
    getSamsungRevenue : function(component,event,accId,profile) {
		var action= component.get("c.getSamsungRevenue");
        action.setParams({
            "accId":accId,
            "theProfile":profile
        });
        action.setCallback(this, function(response){
            var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('SamsungRevenue==='+ JSON.stringify(response.getReturnValue()));
                component.set("v.samsungRevenue",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
	},
    */
    getPipeline : function(component,event,accId,profile) {
		var action= component.get("c.getPipeline");
        action.setParams({
            "accId":accId,
            "theProfile":profile
        });
        action.setCallback(this, function(response){
            var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('Pipeline==='+ JSON.stringify(response.getReturnValue()));
                component.set("v.pipeLine",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
	}
    
})