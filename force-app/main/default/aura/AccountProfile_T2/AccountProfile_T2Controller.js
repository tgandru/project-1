({
	doInit : function(component, event, helper) {
        var profileId=component.get("v.recordId");
        var accId=component.get("v.accountId");
        
        if(profileId==null || profileId==undefined || profileId==''){
            component.set("v.showFYselection",true);
            helper.getAccountInfo(component,event);
        }
        
        var action = component.get("c.getProfileInfo");
        action.setParams({
            "profileId":profileId,
            "accId":accId
        });
        action.setCallback(this, function(response){
            var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('profile==='+ JSON.stringify(response.getReturnValue()));
                var resp = response.getReturnValue();
                component.set("v.profile",resp);
                helper.getCYear(component,event,resp);
                console.log('resp.Id---->'+resp.Id);
                if(resp.Id!=null && resp.Id!=undefined && resp.Id!=''){
                    helper.getFields(component,event);
                    helper.getContactInfo(component,event,resp.Id);
                    helper.getProductMarketShare(component,event,resp.Id);
                }
            }
        });
        $A.enqueueAction(action);
	},
    afterFYselection : function(component, event, helper) {
        component.set("v.showFYselection",false);
        
        var profileId=component.get("v.recordId");
        var accId=component.get("v.accountId");
        
        helper.getExistingProfiles(component,event);
        helper.getFields(component,event);
        helper.getContactInfo(component,event,profileId);
        helper.getProductMarketShare(component,event,profileId);
	},
    /*
    doInit : function(component, event, helper) {
        var profileId=component.get("v.recordId");
        var accId=component.get("v.accountId");
        
        helper.getFields(component,event);
        helper.getAccountInfo(component,event);
        helper.getContactInfo(component,event,profileId);
        
        var action = component.get("c.getProfileInfo");
        action.setParams({
            "profileId":profileId,
            "accId":accId
        });
        action.setCallback(this, function(response){
            var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('profile==='+ JSON.stringify(response.getReturnValue()));
                component.set("v.profile",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
	},
    */
    SaveProfile : function (component, event, helper){
    	helper.saveProfileInfo(component,event);
	},
    
    Cancel : function (component, event, helper) {
 		var profId=component.get("v.recordId");
        var accountId = component.get("v.accountId");
        
        if(profId!=undefined && profId!=''){
            window.location.href = "/"+profId;
        }else{
            window.location.href = "/"+accountId;
        }
    },
})