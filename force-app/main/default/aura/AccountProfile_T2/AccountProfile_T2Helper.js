({
	getFields : function(component,event) {
		var action= component.get("c.getProfileFields");
        action.setCallback(this, function(response){
            var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('Fields==='+ JSON.stringify(response.getReturnValue()));
                /*
                var opts = [];
                var allValues = response.getReturnValue();
                for (var i = 0; i < allValues.length; i++) {
                    var fieldValue = 'v.profile.'+allValues[i];
                    opts.push({
                        label: allValues[i],
                        value: fieldValue
                    });
                }
                component.set("v.fields",opts);
                */
                component.set("v.fields",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
	},
    getCYear : function(component,event,profile) {
		var action= component.get("c.getCYear");
        action.setParams({
            "theProfile":profile
        });
        action.setCallback(this, function(response){
            var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('CYear==='+ JSON.stringify(response.getReturnValue()));
                var resp = 0;
                resp = response.getReturnValue();
                var cYear = resp;
                var pYear = resp-1;
                var NYear = resp+1;
                var years = [];
                //years.push(resp+1);
                //years.push(resp);
                //years.push(resp-1);
                years.push({
                        label: NYear.toString(),
                        value: NYear.toString()
                    });
                years.push({
                        label: cYear.toString(),
                        value: cYear.toString()
                    });
                years.push({
                        label: pYear.toString(),
                        value: pYear.toString()
                    });
                console.log('Years==='+ years);
                component.set("v.years",years);
            }
        });
        $A.enqueueAction(action);
	},
    getAccountInfo : function(component,event) {
		var action= component.get("c.getAccountInfo");
        var accId=component.get("v.accountId");
        action.setParams({
            "accId":accId
        });
        action.setCallback(this, function(response){
            var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('account==='+ JSON.stringify(response.getReturnValue()));
                component.set("v.accnt",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
	},
    getExistingProfiles : function(component,event) {
		var action= component.get("c.getExistingProfiles");
        var accId=component.get("v.accountId");
        var profile=component.get("v.profile");
        action.setParams({
            "accId":accId,
            "profile":profile
        });
        action.setCallback(this, function(response){
            var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('has ExistingProfiles==='+ JSON.stringify(response.getReturnValue()));
                var resp = response.getReturnValue();
                
                if(resp==true){
                    var msg = 'There is an existing profile for this Account and Year. Please update existing profile';
                    component.set("v.showFYselection",true);
                    component.set("v.pageMessage",msg);
            		component.set("v.showPageMessage",true);
                }else{
                    component.set("v.showFYselection",false);
                    component.set("v.showPageMessage",false);
                }
            }
        });
        $A.enqueueAction(action);
	},
    getContactInfo : function(component,event,profileId) {
		var action= component.get("c.getContactInfo");
        action.setParams({
            "profileId":profileId
        });
        action.setCallback(this, function(response){
            var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('Contacts==='+ JSON.stringify(response.getReturnValue()));
                component.set("v.profileContacts",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
	},
    getProductMarketShare : function(component,event,profileId) {
        console.log('product share profileId---->'+profileId);
		var action= component.get("c.getProductMarketShare");
        action.setParams({
            "profileId":profileId
        });
        action.setCallback(this, function(response){
            var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('productShare==='+ JSON.stringify(response.getReturnValue()));
                component.set("v.productShare",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
	},
    saveProfileInfo : function(component,event) {
        console.log('====In Save====');
        
        component.set("v.showPageMessage",false);
        
        var profile = component.get("v.profile");
        var contacts = component.get("v.profileContacts");
        var productShare = component.get("v.productShare");
        
        if (profile.Key_Opportunity_1__c!=undefined) profile.Key_Opportunity_1__c = profile.Key_Opportunity_1__c.val;
        if (profile.Key_Opportunity_2__c!=undefined) profile.Key_Opportunity_2__c = profile.Key_Opportunity_2__c.val;
        if (profile.Key_Opportunity_3__c!=undefined) profile.Key_Opportunity_3__c = profile.Key_Opportunity_3__c.val;
        if (profile.Key_Opportunity_4__c!=undefined) profile.Key_Opportunity_4__c = profile.Key_Opportunity_4__c.val;
        if (profile.Key_Opportunity_5__c!=undefined) profile.Key_Opportunity_5__c = profile.Key_Opportunity_5__c.val;
        
        for (var indexVar = 0; indexVar < contacts.length; indexVar++) {
            if (contacts[indexVar].Contact__c!=undefined) contacts[indexVar].Contact__c=contacts[indexVar].Contact__c.val;
            if (contacts[indexVar].Samsung_Contact__c!=undefined) contacts[indexVar].Samsung_Contact__c=contacts[indexVar].Samsung_Contact__c.val;
        }
		
        var action= component.get("c.Save");
        action.setParams({
            "profile":profile,
            "accProfContacts":contacts,
            "productShare":productShare
        });
        action.setCallback(this, function(response){
            var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('Save Response==='+ JSON.stringify(response.getReturnValue()));
                var resp = response.getReturnValue();
                if(resp!=null){
                    var respStr = '';
                    respStr = response.getReturnValue();
                    if(respStr.includes("Save Successfully,")){
                        component.set("v.showPageMessage",false);
                        respStr = respStr.replace("Save Successfully,","");
                        window.location.href = '/'+respStr;
                    }else{
                        component.set("v.pageMessage",respStr);
            			component.set("v.showPageMessage",true);
                    }
                }
            }
        });
        $A.enqueueAction(action);
	},
})