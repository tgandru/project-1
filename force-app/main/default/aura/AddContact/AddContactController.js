({
	doInit : function(component, event, helper) {

        var action= component.get("c.getContact");
        console.log('Init'+action);

        action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                component.set("v.contact",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
     saveRecord: function(component,event,helper){
         var allValid = component.find('field').reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
         }, true);
         if (!allValid) {
			//alert('Please update the invalid form entries and try again.');
       		helper.showToast(component, event,'Error','Please complete required fields');

         } else {
            var newContact = component.get("v.contact");
            var action= component.get("c.addContact");
            action.setParams({
                "c":newContact,
                "aId":component.get("v.AccountId")
            });
            
             action.setCallback(this, function(response){
                var state= response.getState();
                 
                if(component.isValid() && state=="SUCCESS"){
                    component.set("v.contact",{'sobjectType':'Contact'});
                    
                    //alert('Contact saved');
                    helper.showToast(component, event,'success','Contact is created');
                    helper.Cancel(component,event);
                } else if (state === "ERROR") {
                    var errors = response.getError();
                    console.log(JSON.stringify(errors));
                    var errMsg ='';
                    if (errors) {
                        if (errors[0] && errors[0].pageErrors[0].message) {
                            errMsg = errors[0].pageErrors[0].message;
                            console.log("Error message1: " + errors[0].pageErrors[0].message);
                        }
                    } else {
                        errMsg='Unknown error';
                        console.log("Unknown error");
                    }
                    
                    helper.showToast(component, event,'Error', errMsg);
                }
            });
            $A.enqueueAction(action);
        }
    },
    
    Cancel: function (component, event, helper) {
        helper.Cancel(component,event);
    },
})