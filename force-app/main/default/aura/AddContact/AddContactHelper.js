({
	 showToast : function(component, event, type,message) {
         var toastEvent = $A.get("e.force:showToast");
        if(toastEvent!=undefined){
            toastEvent.setParams({
                "type":type,
                "message": message
            });
            toastEvent.fire();
        } else {
            alert(message);
        }
    },
    
    Cancel: function (component, event) {
           var cmpEvent = component.getEvent("cmpEvent");
           cmpEvent.fire();
    },
})