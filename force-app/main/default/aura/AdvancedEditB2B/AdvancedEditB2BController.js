({
    navigateToMyComponent : function(component, event, helper) {
    	
        console.log('redirecting to CallReport_B2B');
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:CallReport_B2B",
            componentAttributes: {
                recordId : component.get("v.recordId")
            },
            isredirect : true
        });
        evt.fire();
	}
})