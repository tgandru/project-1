({
	doInit : function(component, event, helper) {
        var reportId=component.get("v.recordId");
        var accountId=component.get("v.accountId");
        var action= component.get("c.getCallReport");
         action.setParams({
            "crId":reportId
        });
        console.log('Init:'+action);
        console.log('accountId='+accountId);
        
        var myPageRef = component.get("v.pageReference");
        var accntId = '';
        accntId = myPageRef.state.c__accountId;
        if(accntId!='' && accntId!=null && accntId!=undefined && (accountId==null || accountId==undefined)){
            accountId = accntId;
        }
        //console.log('accntId='+accntId);
        
        //helper.fetchPickListVal(component, {'sobjectType':'Call_Report__c'},'Meeting_Topics__c', 'meetingTopics','selectOptions');
        //helper.fetchDependPickList(component, 'Product_Category_Note__c','Division__c','Product_Category__c','dependentPickList');
		//--08/26/2019--//helper.fetchPickListValDivision(component, {'sobjectType':'Call_Report__c'},'Meeting_Topics__c', 'meetingTopics','selectOptions');
        //--08/26/2019--//helper.fetchPickListVal(component, {'sobjectType':'Call_Report__c'},'Status__c', 'status','statusSelectOptions');
        helper.fetchPickListVal(component, {'sobjectType':'Call_Report_Sub_Account__c'},'Account_Type__c', 'accountType','accountTypeOptions');
        helper.fetchCBDProdCategoryOptions(component, 'Product_Category_Note__c','Division__c','Product_Category__c','dependentPickList');
        
        //Added below for getting Product Category, Timing, Week Number picklist values - 02/28/2019
        //--08/26/2019--//helper.fetchPickListVal(component, {'sobjectType':'Call_Report__c'},'Timing__c', 'timing','timingSelectOptions');
        //--08/26/2019--//helper.fetchPickListValNoSorting(component, {'sobjectType':'Call_Report__c'},'Week_Number__c', 'weekNumber','weekSelectOptions');
        //--08/26/2019--//helper.fetchPickListVal(component, {'sobjectType':'Call_Report__c'},'Primary_Category__c', 'primaryCategory','categorySelectOptions');
        
        //helper.fetchDependPickList(component, 'Call_Report_Market_Intelligence__c','Intelligence_Type__c','Intelligence__c','MarketIntelligence');
        helper.getBuildingBlocks(component,helper);
        helper.getBusinessPerfBlocks(component,helper);
        
        helper.fetchDependPickList(component, 'Call_Report_Market_Intelligence__c','Category__c','Competitor__c','competitorOptions');
        //helper.fetchDependPickList(component, 'Call_Report_Business_Performance__c','Product_Category__c','Product_Sub_Category__c','subCategoryOptions');//Added on 08/06/2019
        
        helper.getMIHoverTextMap(component,helper);
        helper.getMIButtonValues(component,helper);
                
        action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('call report1='+ JSON.stringify(response.getReturnValue()));
                var callRepResponse = response.getReturnValue();
                
                helper.onStatusChange(component,event,callRepResponse);
                
                /*
                if(callRepResponse.Business_Performance_Notes__c!=null 
                   && callRepResponse.Business_Performance_Notes__c!='' 
                   && callRepResponse.Business_Performance_Notes__c!=undefined){
                    component.set("v.showBizPerfNotes", true);
                }*/
                component.set("v.showBizPerfNotes", true);
                component.set("v.callreport",response.getReturnValue());

                var callReportDetails = component.get("v.callreport");
				
                if (reportId != undefined && reportId!=null) {
                    helper.loadedFiles(component, event, helper, reportId);
                    
                    helper.getMeetingAttendees(component,event,callReportDetails);
                    helper.getTasks(component,event,callReportDetails);
                    //helper.getBuildingBlockTasks(component,event,callReportDetails);//Get Building Blocks Tasks--Added--03/06/2019
                    helper.getProductLine(component,event,callReportDetails);
                    helper.getSubAccount(component,event,callReportDetails);
                    helper.getSamsungAttendees(component,event,callReportDetails);
                    helper.getDistributionLists(component,event,callReportDetails); //get the Distribution Lists
                    helper.getMarketIntelligence(component,event,callReportDetails);
                    helper.getBizPerformance(component,event,callReportDetails); //Added on 08/06/2019
                }
                
                //get record type if new record
                if (reportId == undefined || reportId==null) {
                    helper.getDefaultSamsungAttendees(component,event);
                    //var recTypeId1 = component.get("v.recordTypeId");
                    var recTypeId = helper.getParameterByName(component,event,'recordTypeId');
                    console.log('rec type id1='+recTypeId);
                    if (recTypeId!=undefined && recTypeId!=null && recTypeId!='') {
                        component.set("v.callreport.RecordTypeId", recTypeId);

                       var actionRec= component.get("c.getRecordType");
                        actionRec.setParams({
                            "recTypeId":recTypeId
                        });
                        
                         actionRec.setCallback(this, function(response){
                                console.log('rec type='+ JSON.stringify(response.getReturnValue()));
                                var state= response.getState();
                                if(component.isValid() && state=="SUCCESS"){
                                    if (response.getReturnValue().Id!=undefined && response.getReturnValue().Id!=null) {
                                         component.set("v.callreport.RecordType.Id", response.getReturnValue().Id);
                                    	component.set("v.callreport.RecordType.Name", response.getReturnValue().Name);
                                    	console.log('rec type1='+ component.get("v.callreport.RecordType.Name"));
                                    } else {
                                        console.log('rec type is null');
                                    }
                                   
                                }
                            });
                            $A.enqueueAction(actionRec); 
                    }
                    
                    var accId = accountId;
                    //var accId = component.get("v.accountId");
                    
                    if (accId == undefined || accId ==null || accId =='') {
                    	accId = helper.getParameterByName(component,event,'accountId');
                    }
                    
                    console.log('accId from account1='+ accId);

                    if (accId!=undefined && accId!=null && accId!='') {
                            component.set("v.callreport.Primary_Account__c", accId);
                        	console.log("accid after assignment---->"+component.get("v.callreport.Primary_Account__c"));
                            var actionAcc= component.get("c.getAccount");
                             actionAcc.setParams({
                                "accId":accId
                            });
                            actionAcc.setCallback(this, function(response){
                                var state= response.getState();
                                if(component.isValid() && state=="SUCCESS"){
                                    console.log('account res1='+ JSON.stringify(response.getReturnValue()));
                                    component.set("v.callreport.Primary_Account__r.Id", response.getReturnValue().Id);
                                    component.set("v.callreport.Primary_Account__r.Name", response.getReturnValue().Name);
                                    component.set("v.callreport.Primary_Account__c", accId);
    
                                }
                            });
                            $A.enqueueAction(actionAcc);
                    }
                }
           	}
        });
        $A.enqueueAction(action);
	},
            
    saveRecord: function(component,event,helper){
        var callReportDetails = component.get("v.callreport");
        helper.validateFields(component,event);
        var isValid=component.get("v.isValid");
        console.log('call report name is1:'+callReportDetails.Name);
        
        if(isValid){
            /*
            var accountSel = component.get("v.callreport.Primary_Account__c.val");
            console.log("primary account="+JSON.stringify(accountSel));
             if (accountSel!=undefined && accountSel!=null) {
                component.set("v.callreport.Primary_Account__c", accountSel);
             }
             */
            
            //console.log("call report1="+JSON.stringify(callReportDetails));
                
            var action= component.get("c.saveCallReport");
            action.setParams({
                "callReport":callReportDetails
            });
                
            component.set('v.disableSave',true);
             action.setCallback(this, function(response){
                var state= response.getState();
                 
                if(component.isValid() && state=="SUCCESS"){
                    //console.log('saved call report11='+JSON.stringify(response.getReturnValue()));    
                    component.set("v.callreport",response.getReturnValue());
                    var callReportDetails = component.get("v.callreport");
                    
                    var reportId = response.getReturnValue().Id;
                    
                    helper.saveTasks(component,event,callReportDetails);
                    helper.saveProductCategory(component,event,callReportDetails);
                    helper.saveSubAccount(component,event,callReportDetails);
                    helper.saveMeetingAttendees(component,event,callReportDetails);
                    //Save Distribution Lists
                    helper.saveDistributionLists(component,event,callReportDetails);
                    //Delete Distribution Lists
                    helper.deleteDistributionLists(component,event,callReportDetails);
                    //Save Market Intelligence--Added--03/06/2019
                    helper.saveMarketIntelligence(component,event,callReportDetails);
                    //Save Business Performance
                    helper.saveBizPerformance(component,event,callReportDetails);//Added on 08/06/2019
                    
                    // delete records
                    helper.deleteRecords(component, event);
    				helper.saveSamsungAttendees(component,event,callReportDetails);
                    // redirect after files uploaded
                    helper.uploadFile(component, event, helper, reportId, function(result) {
                        console.log("file upload result: " + result);
                        component.set('v.disableSave',false);
                        helper.showToast(component, event,'success','Call Report Saved');
                        helper.GotoView(component,event,reportId);
                    });
                    //helper.showToast(component, event,'success','Call Report Saved');
                    //helper.GotoView(component,event,reportId);
    
                }
               /* else if (state === "ERROR") {
                        var errors = response.getError();
                        console.log(JSON.stringify(errors));
                        var errMsg ='';
                        if (errors) {
                            if (errors[0] && errors[0].pageErrors[0].message) {
                                errMsg = errors[0].pageErrors[0].message;
                                console.log("Error message1: " + errors[0].pageErrors[0].message);
                            }
                        } else {
                            errMsg='Unknown error';
                            console.log("Unknown error");
                        }
                        
                        helper.showToast(component, event,'Error', errMsg);
                    }*/
                 
            });
            $A.enqueueAction(action);
        }
    },
    
    
	addNewTask:function(component,event,helper){
		helper.addNewTask(component,event);
    },
    
    //Add selected buiding block to Action items -- 03/01/2019
     addNewBuidingBlockTask:function(component,event,helper){
		helper.addNewBuidingBlockTask(component,event);
    },
    //Add selected Market Intelligence to Action items -- 03/01/2019
     addNewMarketIntelligence:function(component,event,helper){
        helper.addNewMarketIntelligence(component,event);
    },
    
    //Add New Business Performance--Added-- 08/06/2019
     addNewBizPerformance:function(component,event,helper){
        helper.addNewBizPerformance(component,event);
    },

     addNewProductLine:function(component,event,helper){
        helper.addNewProductLine(component,event);
    },
    
     addNewMeetingAttendee:function(component,event,helper){
        helper.addNewMeetingAttendee(component,event);
    },
    
     addNewSubAccount:function(component,event,helper){
        helper.addNewSubAccount(component,event);
    },
            
     addNewSamsungAttendee:function(component,event,helper){
     helper.addNewSamsungAttendee(component,event);
    },
    
    //Add new Distribution List
    addDistributionList:function(component,event,helper){
     helper.addDistributionList(component,event);
    },
         
    
    toggle : function(component, event, helper) {
        var toggleText = component.find("text");
        $A.util.toggleClass(toggleText, "toggle");
    },
    
    Cancel : function (component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": "#/sObject/Call_Report__c/list?filterName=Recent"
        });
        urlEvent.fire();
    },
    
         removeTaskRow : function(component, event, helper){
            helper.removeRow(component,event,"tasks");
        },
         /*
         removeBuidingBlockTaskRow : function(component, event, helper){
            var idx = event.getSource().get("v.value"); 
            var tasks = component.get("v.buildingBlockTasks");
            var sub = tasks[idx].Subject;
            var callRep = component.get("v.callreport");
            if(callRep.Buiding_Blocks__c!=null && callRep.Buiding_Blocks__c!='' && callRep.Buiding_Blocks__c!=undefined){
                var bBlockStr = callRep.Buiding_Blocks__c;
                var bBlockArray = [];
                bBlockArray = bBlockStr.split(';');
                var bBlockStr=null;                
                for(var indexVar = 0; indexVar < bBlockArray.length; indexVar++){
                    if(bBlockArray[indexVar]!=sub){
                        if(bBlockStr!=null)    
                            bBlockStr = bBlockStr +';'+ bBlockArray[indexVar];
                        else 
                            bBlockStr = bBlockArray[indexVar];
                    }
                }
                callRep.Buiding_Blocks__c = bBlockStr;
                console.log('bBlockStr after---->'+bBlockStr);
                component.set("v.callreport",callRep);
            }
            helper.removeRow(component,event,"buildingBlockTasks");
        },
        */    
         removeProductCategoriesRow : function(component, event, helper){
            helper.removeRow(component,event,"productCategories");
        },
            
         removeSubAccountRow : function(component, event, helper){
            helper.removeRow(component,event,"subAccount");
        },
            
         removeMeetingAttendeesRow : function(component, event, helper){
            helper.removeRow(component,event,"meetingAttendees");
        },    
		
         removeSamsungAttendeesRow : function(component, event, helper){
            helper.removeRow(component,event,"samsungAttendees");
        },
        
         removeDistributionListRow : function(component, event, helper){
            helper.removeRow(component,event,"DistributionLists");
        },
        //Delete Market Intelligence Lines -- 
         removeMarketIntelligenceRow : function(component, event, helper){
            helper.removeRow(component,event,"MarketIntelligenceLines");
        },
        //Delete Biz performance Line--Added---08/06/2019 
         removeBizPerformancRow : function(component, event, helper){
            var AllRowsList = component.get("v.bizPerformanceLines");
            if(AllRowsList.length>1){
                var target = event.getSource();
        		var rowIndexStr = target.get("v.value");
                var rowIndex = Number(rowIndexStr);
                console.log('rowIndex---->'+rowIndex);
                console.log('BP rec---->'+JSON.stringify(AllRowsList[rowIndex]));
                if (AllRowsList[rowIndex]!=undefined && AllRowsList[rowIndex].Id!=undefined) {
                    var deleteList=component.get("v.delete-BizPerformanceLines");
                    console.log('BP rec 2---->'+JSON.stringify(AllRowsList[rowIndex]));
                    deleteList.push(AllRowsList[rowIndex]);
                    component.set("v.delete-BizPerformanceLines",deleteList);
                }
                AllRowsList.splice(rowIndex, 1);
                component.set("v.bizPerformanceLines", AllRowsList);
            }else{
                helper.showToast(component, event,'Error','Atleast One Business Performance Section will be selected when Primary Product Category was Selected',10000);
            }
        },

            
        applycss:function(cmp,event){
            var cmpTarget = cmp.find('Modalbox');
            var cmpBack = cmp.find('MB-Back');
            $A.util.addClass(cmpTarget, 'slds-fade-in-open');
            $A.util.addClass(cmpBack, 'slds-backdrop--open');
        },
        
        removecss:function(cmp,event){
            var cmpTarget = cmp.find('Modalbox');
            var cmpBack = cmp.find('MB-Back');
            $A.util.removeClass(cmpBack,'slds-backdrop--open');
            $A.util.removeClass(cmpTarget, 'slds-fade-in-open');        
        },
    
    businessPerfIncrease:function(component,event,helper){
		//helper.addNewBuidingBlockTask(component,event);
		var selected = event.currentTarget.id;
        var SelectedValue = 'Increase';
        helper.businessPerfControl(component,event,selected,SelectedValue);
    },
    
    businessPerfDecrease:function(component,event,helper){
		var selected = event.currentTarget.id;
        var SelectedValue = 'Decrease';
        helper.businessPerfControl(component,event,selected,SelectedValue);
    },
    
    businessPerfFlat:function(component,event,helper){
		var selected = event.currentTarget.id;
        var SelectedValue = 'Flat';
        helper.businessPerfControl(component,event,selected,SelectedValue);
    },
    
    businessPerfNone:function(component,event,helper){
		//helper.addNewBuidingBlockTask(component,event);
		var selected = event.currentTarget.id;
        var SelectedValue = null;
        helper.businessPerfControl(component,event,selected,SelectedValue);
    },
    
    displayBizPerfNotes:function(component,event,helper){
		component.set("v.showBizPerfNotes", true);
    },
    
    hideBizPerfNotes:function(component,event,helper){
		component.set("v.showBizPerfNotes", false);
    },
    
	showRequiredFields: function(component, event, helper){
        $A.util.removeClass(component.find("CallReportName"), "none");
        $A.util.removeClass(component.find("Primary_Account__c"), "none");
        $A.util.removeClass(component.find("Meeting_Date__c"), "none");
        $A.util.removeClass(component.find("Meeting_Topics__c"), "none");
        $A.util.removeClass(component.find("Division__c"), "none");
        $A.util.removeClass(component.find("Status__c"), "none");
    },   
    
    calculateAgendaLength:function(component,event,helper){
        var callRep = component.get("v.callreport");
        var agendaLen = 0;
        if(callRep.Agenda__c!=null && callRep.Agenda__c!='' && callRep.Agenda__c!=undefined){
            var agenda = '';
            agenda = callRep.Agenda__c;
            agenda = agenda.replace(/(\r\n|\n|\r)/gm, '');
            agenda = agenda.replace(/<[^>]*>/g, '');
            agendaLen = agenda.length;
            component.set("v.agendaLength", agendaLen);
        }
    },
    calculateMeetingSummLength:function(component,event,helper){
        var callRep = component.get("v.callreport");
        var agendaLen = 0;
        if(callRep.Meeting_Summary__c!=null && callRep.Meeting_Summary__c!='' && callRep.Meeting_Summary__c!=undefined){
            var agenda = '';
            agenda = callRep.Meeting_Summary__c;
            agenda = agenda.replace(/(\r\n|\n|\r)/gm,"");
            agenda = agenda.replace(/<[^>]*>/g, '');
            agendaLen = agenda.length;
            component.set("v.meetSummLength", agendaLen);
        }
    },
    
    onStatusChange:function(component,event,helper){
        var callRepResponse = component.get("v.callreport");
        console.log('in Status change');
        helper.onStatusChange(component,event,callRepResponse);
    },
    onProductCategoryChange:function(component,event,helper){
        console.log('in Product Category change');
        component.set("v.isProdCategoryChange",true);
        var crBPList = component.get("v.bizPerformanceLines");
        if(crBPList.length>0){
            component.set("v.isBizPerfDelete",true);
        }else{
            helper.addBizPerformance(component,event);
        }
    },
    onSubCategoryChange:function(component,event,helper){
        console.log('in Product Sub-Category change');
        var prodCategoryChange = component.get("v.isProdCategoryChange");
        if(!prodCategoryChange)
        helper.onSubCategoryChange(component,event);
    },
    bizPerfDelete:function(component,event,helper){
        console.log('in bizPerfDelete');
        var crBPList = component.get("v.bizPerformanceLines");
        var deleteList=component.get("v.delete-BizPerformanceLines");
        for(var i=0;i<crBPList.length;i++){
            if (crBPList[i]!=undefined && crBPList[i].Id!=undefined) { 
                deleteList.push(crBPList[i]);
            }
        }
        component.set("v.delete-BizPerformanceLines", deleteList);
        helper.addBizPerformance(component,event);
    },
    closeBizPerfPopup:function(component,event,helper){
        component.set("v.isBizPerfDelete",false);
        component.find("resetField").forEach(function(f) {
            f.reset();
        }); 
    },
})