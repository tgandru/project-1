({
	
    GotoView:function (component,event,reportId) {
        console.log("report id:" + reportId);
       var urlEvent = $A.get("e.force:navigateToSObject");
        urlEvent.setParams({
          "recordId": reportId,
            "isredirect":'true'
        });
        //window.parent.location = '/#sObject/'+reportId+'/view';    
        urlEvent.fire();
       //$A.get('e.force:refreshView').fire();
        /*var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "#/sObject/"+reportId+"/view",
            "isredirect":"true"
        });
        urlEvent.fire();*/
        //location.reload();
    },
    
    addNewTask:function(component,event){
        var taskList = component.get("v.tasks");
          taskList.push({
            'sobjectType': 'Task'
        });
       component.set("v.tasks",taskList);
    },
    
    //Add Business Performance--Added-- 08/06/2019
    addNewBizPerformance:function(component,event){
        var callRep = component.get("v.callreport");
        var category = callRep.Primary_Category__c;
        
        if(category!='' && category!=null && category!=undefined){
            var crBPList = component.get("v.bizPerformanceLines");
            crBPList.push({
                'sobjectType': 'Call_Report_Business_Performance__c',
                'Product_Category__c': category
            });
            component.set("v.bizPerformanceLines",crBPList);
        }
    },
    
    //Add selected Market Intelligence--Added-- 03/01/2019
    addNewMarketIntelligence:function(component,event){
        var selected = event.getSource().get("v.label");
        var idx = event.getSource().get("v.value");
        var crMIList = component.get("v.MarketIntelligenceLines");
        crMIList.push({
            'sobjectType': 'Call_Report_Market_Intelligence__c',
            'Intelligence_Type__c':idx,
            'Intelligence__c':selected
        });
        console.log('crMIList===>'+ JSON.stringify(crMIList));
        component.set("v.MarketIntelligenceLines",crMIList);
    },
    
    //Add selected building Block -- 03/01/2019
    addNewBuidingBlockTask:function(component,event){
        var idx = event.getSource().get("v.label");
        console.log('Selected Value---->'+idx);
        var taskList = component.get("v.tasks");
        taskList.push({
            'sobjectType': 'Task',
            'Subject':idx
        });
        component.set("v.tasks",taskList);
        
        var callRep = component.get("v.callreport");
        if(callRep.Building_Blocks__c!=null && callRep.Building_Blocks__c!='' && callRep.Building_Blocks__c!=undefined){
            callRep.Building_Blocks__c = callRep.Building_Blocks__c +';'+idx;
        }else{
            callRep.Building_Blocks__c = idx;
        }
        component.set("v.callreport",callRep);
    },
    
    addNewProductLine:function(component,event){
        var pList=component.get("v.productCategories");
        
        pList.push({
            'sobjectType':'Product_Category_Note__c'
        });
        
        component.set("v.productCategories",pList);
    },
    
       addNewSubAccount:function(component,event){
        var sAcc=component.get("v.subAccount");
        sAcc.push({
            'sobjectType':'Call_Report_Sub_Account__c'
        });
        component.set("v.subAccount",sAcc);
    },
    
      addNewMeetingAttendee:function(component,event){
        var maList=component.get("v.meetingAttendees");
        maList.push({
            'sobjectType':'Call_Report_Attendee__c'
        });
        component.set("v.meetingAttendees",maList);
    },
    
     addNewSamsungAttendee:function(component,event){
        var sList=component.get("v.samsungAttendees");
        sList.push({
            'sobjectType':'Call_Report_Samsung_Attendee__c'
        });
        component.set("v.samsungAttendees",sList);
    },
    
    addDistributionList:function(component,event){
        var sList=component.get("v.DistributionLists");
        sList.push({
            'sobjectType':'Call_Report_Distribution_List__c'
        });
          //console.log('dl1='+JSON.stringify(sList));
        component.set("v.DistributionLists",sList);
    },
    
    getTasks:function(component,event,callReport){
        //var action= component.get("c.getTasks");
        var action= component.get("c.getBuildingBlockTasks");
        action.setParams({
            "callReport":callReport
        });
         action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                component.set("v.tasks",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    saveTasks:function(component,event,callReport){
        var tasks = component.get("v.tasks");
        for (var indexVar = 0; indexVar < tasks.length; indexVar++) {
            if (tasks[indexVar].OwnerId!=undefined){
            	tasks[indexVar].OwnerId=tasks[indexVar].OwnerId.val;
            }
        }
		
        var action=component.get("c.saveTasks");
        action.setParams({
            "tasks":tasks,
            "callReport":callReport
        });

         action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
               
            }
         });

        $A.enqueueAction(action);
  
    },
    
    
    getProductLine:function(component,event,callReport){
        var action= component.get("c.getProductLine");
        action.setParams({
            "callReport":callReport
        });
         action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                component.set("v.productCategories",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    saveProductCategory:function(component,event,callReport){
        var pcategory = component.get("v.productCategories");
          for (var indexVar = 0; indexVar < pcategory.length; indexVar++) {
            if (pcategory[indexVar].Sub_Account__c!=undefined){
            	pcategory[indexVar].Sub_Account__c=pcategory[indexVar].Sub_Account__c.val;
            }
        }
        var action=component.get("c.saveProductLine");
        action.setParams({
            "plines":pcategory,
            "callReport":callReport
        });

        action.setCallback(this, function(response){
            var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                //$A.get('e.force:refreshView').fire();
                //this.GotoView(component,event,callReport.Id);

            }
        });

        $A.enqueueAction(action);
    },
    
      getSubAccount:function(component,event,callReport){
        var action= component.get("c.getSubAccount");
        action.setParams({
            "callReport":callReport
        });
         action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                component.set("v.subAccount",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    
    saveSubAccount:function(component,event,callReport){
        var sa = component.get("v.subAccount");
        for (var indexVar = 0; indexVar < sa.length; indexVar++) {
            if (sa[indexVar].Sub_Account__c!=undefined){
            	sa[indexVar].Sub_Account__c=sa[indexVar].Sub_Account__c.val;
            }
        }
        
        var action=component.get("c.saveSubAccount");
        action.setParams({
            "subAcc":sa,
            "callReport":callReport
        });

        action.setCallback(this, function(response){
            var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('<==Sub Accounts Saved==>');
            }
        });

        $A.enqueueAction(action);
    },
    
    getMeetingAttendees:function(component,event,callReport){
        var action= component.get("c.getMeetingAttendees");
        action.setParams({
            "callReport":callReport
        });
         action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                component.set("v.meetingAttendees",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    saveMeetingAttendees:function(component,event,callReport){
        var mAttendees = component.get("v.meetingAttendees");
         for (var indexVar = 0; indexVar < mAttendees.length; indexVar++) {
             if (mAttendees[indexVar].Customer_Attendee__c!=undefined && mAttendees[indexVar].Customer_Attendee__c.val!='') {
            	mAttendees[indexVar].Customer_Attendee__c=mAttendees[indexVar].Customer_Attendee__c.val;
             }
        }
        var action=component.get("c.saveMeetingAttendees");
        action.setParams({
            "ma":mAttendees,
            "callReport":callReport
        });

        action.setCallback(this, function(response){
            var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('Meeting Attendees Saved');
            }
        });

        $A.enqueueAction(action);
    },
    
    //get default samsung attendees
    getDefaultSamsungAttendees:function(component,event){
        var action= component.get("c.getDefaultSamsungAttendees");
        action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                component.set("v.samsungAttendees",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
     
    getSamsungAttendees:function(component,event,callReport){
        var action= component.get("c.getSamsungAttendees");
        action.setParams({
            "callReport":callReport
        });
         action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                component.set("v.samsungAttendees",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
        
    saveSamsungAttendees:function(component,event,callReport){
        var sAttendees = component.get("v.samsungAttendees");
         console.log('sAttendees-------->'+JSON.stringify(sAttendees));
         for (var indexVar = 0; indexVar < sAttendees.length; indexVar++) {
             if (sAttendees[indexVar].Attendee__c!=undefined && sAttendees[indexVar].Attendee__c.val!='') {
            	//console.log('Save Sam Attendees before assignment----->'+sAttendees[indexVar].Attendee__c);
                 var att = sAttendees[indexVar].Attendee__c;
                 var attstr = att.toString();
                 sAttendees[indexVar].Attendee__c=attstr;
                 //console.log('Save Sam Attendees after assignment----->'+sAttendees[indexVar].Attendee__c);
             }
         }
        var action=component.get("c.saveSamsungAttendees");
        action.setParams({
            "sa":sAttendees,
            "callReport":callReport
        });

        action.setCallback(this, function(response){
            var state= response.getState();
            console.log(response.getReturnValue());
            console.log(component.isValid());
            console.log(state);
            if(component.isValid() && state=="SUCCESS"){
                console.log('Sam Attendees Save Success');
                console.log(response.getReturnValue());
            }else{
                console.log('Sam Attendees Save failed');
            }
        });

        $A.enqueueAction(action);
    },
    
    getDistributionLists:function(component,event,callReport){
        var action= component.get("c.getDistributionLists");
        action.setParams({
            "callReport":callReport
        });
         action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                var result = response.getReturnValue();
                for (var indexVar = 0; indexVar < result.length; indexVar++) {
                    //console.log('Distribution list Name--------->'+result[indexVar].Name);
                }
                component.set("v.DistributionLists",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    saveDistributionLists:function(component,event,callReport){
        var dLists = component.get("v.DistributionLists");
        var DlistIds = [];
        for (var indexVar = 0; indexVar < dLists.length; indexVar++) {
            if (dLists[indexVar].Call_Report__c.val!=undefined && dLists[indexVar].Call_Report__c.val!='') {
            	dLists[indexVar].id=dLists[indexVar].Call_Report__c.val;
            	var dlid = dLists[indexVar].id;
            	DlistIds.push(dlid);
            	dLists[indexVar].Call_Report__c=callReport;
             }
        }
        var action=component.get("c.saveDistributionLists");
        action.setParams({
            "distList":DlistIds,
            "callReport":callReport
        });

        action.setCallback(this, function(response){
            var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('<==Disty Lists Saved==>');
            }
        });

        $A.enqueueAction(action);
    },
    
    //get Market Intelligence ---- Added 03/05/2019
    getMarketIntelligence:function(component,event,callReport){
        var action= component.get("c.getMarketIntelligence");
        action.setParams({
            "callReport":callReport
        });
         action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                component.set("v.MarketIntelligenceLines",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    //Save Market Intelligence ---- Added 03/05/2019
    saveMarketIntelligence:function(component,event,callReport){
        var mi = component.get("v.MarketIntelligenceLines");
        
        var action=component.get("c.saveMarketIntelligence");
        action.setParams({
            "CallRepMI":mi,
            "callReport":callReport
        });

        action.setCallback(this, function(response){
            var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
            	console.log('Save Market Intelligence===>'+ JSON.stringify(response.getReturnValue()));
            }
        });

        $A.enqueueAction(action);
    },
    
    //get Business Performance ---- Added 08/06/2019
    getBizPerformance:function(component,event,callReport){
        var action= component.get("c.getBizPerformance");
        action.setParams({
            "callReport":callReport
        });
         action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                component.set("v.bizPerformanceLines",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    //Save Business Performance ---- Added 08/06/2019
    saveBizPerformance:function(component,event,callReport){
        var bp = component.get("v.bizPerformanceLines");
        var action=component.get("c.saveBizPerformance");
        action.setParams({
            "CallRepBP":bp,
            "callReport":callReport
        });

        action.setCallback(this, function(response){
            var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('Save Business Performance===>'+ JSON.stringify(response.getReturnValue()));
            }
        });

        $A.enqueueAction(action);
    },
    
	removeRow : function(component, event, relatedListName){
        var target = event.getSource();
        var rowIndex = target.get("v.value"); 
        var AllRowsList = component.get("v."+relatedListName);

        var deleteDlistIds = [];//Added for Collecting Distribution Lists
        // if record exists
        if(relatedListName=='tasks' && AllRowsList[rowIndex]!=undefined){
            if(AllRowsList[rowIndex].Id!=undefined){
                var deleteList=component.get("v.delete-"+relatedListName);
            	deleteList.push(AllRowsList[rowIndex]);
                component.set("v.delete-"+relatedListName,deleteList);
            }
            var callRep = component.get("v.callreport");
            var bBlock = AllRowsList[rowIndex].Subject;
            var bBlockStr = callRep.Building_Blocks__c;
            if(bBlockStr.includes('; '+bBlock+';')) bBlock = '; '+bBlock+';';
            //else if(bBlockStr.includes(bBlock+';')) bBlock = bBlock+';';
            bBlockStr = bBlockStr.replace(bBlock,'');
            //console.log('bBlockStr after---->'+bBlockStr);
            callRep.Building_Blocks__c = bBlockStr;
            component.set("v.callreport",callRep);
        }
        else if (AllRowsList[rowIndex]!=undefined && AllRowsList[rowIndex].Id!=undefined) { 
            var deleteList=component.get("v.delete-"+relatedListName);
            deleteList.push(AllRowsList[rowIndex]);
            //component.set("v.delete-"+relatedListName,deleteList);
            //below if logic to delete distribution lists based on its name
            if(relatedListName=='DistributionLists'){
                var dlid = AllRowsList[rowIndex].Name;
                deleteDlistIds.push(dlid);
                component.set("v.delete-DistributionListsIds", deleteDlistIds);
            }
        }
        AllRowsList.splice(rowIndex, 1);
        component.set("v."+relatedListName, AllRowsList);
    },
    
    showToast : function(component, event, type,message,duration) {
         var toastEvent = $A.get("e.force:showToast");
        if(toastEvent!=undefined){
            toastEvent.setParams({
                "type":type,
                "message": message,
                "duration": duration

            });
            toastEvent.fire();
        } else {
            alert(message);
        }
    },
    
    getParameterByName : function(cmp,event,name) {
       var url = window.location.href;
       name = name.replace(/[\[\]]/g, "\\$&");
       var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
       results = regex.exec(url);
       if (!results) return null;
       if (!results[2]) return '';
       return decodeURIComponent(results[2].replace(/\+/g, " "));
    },
    
    fetchPickListVal: function(component,objectName,fieldName, elementId, optionList) {
        var action = component.get("c.getselectOptions");

        action.setParams({
            "objObject": objectName,
            "fld": fieldName
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();

                if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        label: "--- None ---",
                        value: ""
                    });
                }
                for (var i = 0; i < allValues.length; i++) {
                    if(fieldName=='Status__c'){
                        if(allValues[i]=='Submitted' || allValues[i]=='Cancelled') continue;
                    }
                    opts.push({
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                
                component.set("v."+optionList, opts);
            }
        });
        $A.enqueueAction(action);
    },
    
    fetchPickListValNoSorting: function(component,objectName,fieldName, elementId, optionList) {
        var action = component.get("c.getselectOptionsNoSorting");

        action.setParams({
            "objObject": objectName,
            "fld": fieldName
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();

                if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        label: "--- None ---",
                        value: ""
                    });
                }
                for (var i = 0; i < allValues.length; i++) {
                    if(fieldName=='Status__c'){
                        if(allValues[i]=='Submitted' || allValues[i]=='Cancelled') continue;
                    }
                    opts.push({
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                
                component.set("v."+optionList, opts);
            }
        });
        $A.enqueueAction(action);
    },
    
    //Added by Thiru for getting the CBD Meeting Type Values--11/15/2018
    fetchPickListValDivision: function(component,objectName,fieldName, elementId, optionList) {
        var action = component.get("c.getOptionsCallReportDivision");

        action.setParams({
            "objObject": objectName,
            "fld": fieldName,
            "objApiName":'Call_Report__c',
            "contrfieldApiName":'Division__c',
            "depfieldApiName":'Meeting_Topics__c',
            "division":'CBD'
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();

                if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        label: "--- None ---",
                        value: ""
                    });
                }
                for (var i = 0; i < allValues.length; i++) {
                    if(fieldName=='Status__c'){
                        if(allValues[i]=='Submitted' || allValues[i]=='Cancelled') continue;
                    }
                    opts.push({
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                
                component.set("v."+optionList, opts);
            }
        });
        $A.enqueueAction(action);
    },
    
     fetchDependPickList: function(component,objectApiName,contrfieldApiName,depfieldApiName, optionList) {
        var action = component.get("c.getDependentOptionsImpl");

        action.setParams({
            'objApiName': objectApiName,
            'contrfieldApiName': contrfieldApiName,
            'depfieldApiName': depfieldApiName
        });
        action.setStorable();
         
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allDepList = [];
                var depMap = response.getReturnValue();
                for (var key in depMap) {
                    var depList = depMap[key];
                    var depObjList =[];
                    for (var d in depList) {
                        depObjList.push({text:depList[d],value:depList[d]});
                    }
                    allDepList.push({dependentList:depObjList, parent:key});
                }
                component.set("v."+optionList, allDepList);
            }
        });
        $A.enqueueAction(action);
    },
    
    //Added by Thiru to not to display B2B values to CBD Product categories--11/15/2018
    fetchCBDProdCategoryOptions: function(component,objectApiName,contrfieldApiName,depfieldApiName, optionList) {
        var action = component.get("c.getCBDProdCategoryOptions");

        action.setParams({
            'objApiName': objectApiName,
            'contrfieldApiName': contrfieldApiName,
            'depfieldApiName': depfieldApiName
        });
        action.setStorable();
         
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allDepList = [];
                var depMap = response.getReturnValue();
                //console.log('dep map1='+JSON.stringify(depMap));
                for (var key in depMap) {
                    var depList = depMap[key];
                    var depObjList =[];
                    for (var d in depList) {
                        depObjList.push({text:depList[d],value:depList[d]});
                    }
                    allDepList.push({dependentList:depObjList, parent:key});
                }
                //console.log('all dep list='+JSON.stringify(allDepList));
                component.set("v."+optionList, allDepList);
            }
        });
        $A.enqueueAction(action);
    },
    
    loadedFiles: function(component, event, helper,reportId){
    	// fire the CallReportLoadedEvt Lightning Event and pass the id
    	console.log('loaded event='+reportId);
        var childCmp = component.find("fileDisplay");
        // call the aura:method in the child component
        childCmp.loadData(reportId);
    },
    
    uploadFile: function(component, event, helper,reportId, callback){
    	console.log('upload file1='+reportId);
        var childCmp = component.find("fileUpload");
        // call the aura:method in the child component
        //childCmp.uploadFile(reportId);
        childCmp.uploadFile(reportId, callback);
    },
    deleteRecords: function(component, event){
        var deleteTasks=component.get("v.delete-tasks");
        var deletePc=component.get("v.delete-productCategories");
        var deleteMa=component.get("v.delete-meetingAttendees");
        var deleteSa=component.get("v.delete-samsungAttendees");
        var deleteSacc=component.get("v.delete-subAccount");
        var deleteMI=component.get("v.delete-MarketIntelligenceLines");
        var deleteBP=component.get("v.delete-BizPerformanceLines");
        
        //Delete Buiding Block Tasks--Added--03/06/2019
        var deleteBBTasks=component.get("v.delete-buildingBlockTasks");
		for(var indexVar = 0; indexVar < deleteBBTasks.length; indexVar++){
            deleteTasks.push(deleteBBTasks[indexVar]);
        }
        
        console.log('deleted tasks='+JSON.stringify(deleteTasks));
        if (deleteTasks!=null || deletePc!=null || deleteMa!=null || deleteSa!=null || deleteSacc!=null || deleteMI!=null) {
            var action = component.get("c.deleteRecords");
            
            action.setParams({
                "tasks": deleteTasks,
                "pCategories": deletePc,
                "crAttendee":deleteMa,
                "crSamAttendee":deleteSa,
                "crSubAcc":deleteSacc,
                "crMI":deleteMI, 
                "crBP":deleteBP
            });
            
            action.setCallback(this, function(response) {
                if (response.getState() == "SUCCESS") {
                    
                }
            });
            
            $A.enqueueAction(action);
        }
    	
    },
    
    deleteDistributionLists: function(component, event, callReport){
        var deleteDlist=component.get("v.delete-DistributionListsIds"); //Delete Distribution List
        
        if (deleteDlist!=null) {
            var action = component.get("c.DeleteDistributionLists");
            
            action.setParams({
                "DistListNames":deleteDlist,
                "CallRprt":callReport
            });
            
            action.setCallback(this, function(response) {
                if (response.getState() == "SUCCESS") {
                }
            });
            
            $A.enqueueAction(action);
        }
    },
    
    validateFields: function(component, event)
    {
        //console.log('call report n1:');
        
        var callReportDetails = component.get("v.callreport");
        var isValid=true;
        var message='';
        
        console.log('call report name is:'+callReportDetails.Name);
        if(callReportDetails.Name==undefined || callReportDetails.Name=='' ){
            message = 'Name';
            isValid=false;
        }
         
        if(callReportDetails.Primary_Account__c==undefined || callReportDetails.Primary_Account__c=='' ){
            if(message.length>0) message+=', ';
            message += 'Primary Account';
            isValid=false;
        }
        
        if(callReportDetails.Status__c==undefined || callReportDetails.Status__c=='' || callReportDetails.Status__c==null){
            if(message.length>0) message+=', ';
            message += 'Status';
            isValid=false;
        }
        if(callReportDetails.Status__c!='Pre-Planning'){
            if(callReportDetails.Meeting_Date__c==undefined || callReportDetails.Meeting_Date__c=='' ){
                if(message.length>0) message+=', ';
                message += 'Meeting Date';
                isValid=false;
            }
            if(callReportDetails.Agenda__c==undefined || callReportDetails.Agenda__c=='' ){
                if(message.length>0) message+=', ';
                message += 'Agenda';
                isValid=false;
            }
            if(callReportDetails.Meeting_Topics__c==undefined || callReportDetails.Meeting_Topics__c=='' ){
                if(message.length>0) message+=', ';
                message += 'Meeting Type';
                isValid=false;
            }
            if(callReportDetails.Meeting_Tone__c==undefined || callReportDetails.Meeting_Tone__c=='' || callReportDetails.Meeting_Tone__c==null){
                if(message.length>0) message+=', ';
                message += 'Meeting Tone';
                isValid=false;
            }
            if(callReportDetails.Primary_Category__c==undefined || callReportDetails.Primary_Category__c=='' || callReportDetails.Primary_Category__c==null){
                if(message.length>0) message+=', ';
                message += 'Primary Product Category';
                isValid=false;
            }
            if(callReportDetails.Product_Sub_Category__c==undefined || callReportDetails.Product_Sub_Category__c=='' || callReportDetails.Product_Sub_Category__c==null){
                if(message.length>0) message+=', ';
                message += 'Product Sub-Category';
                isValid=false;
            }
            if(callReportDetails.Week_Number__c==undefined || callReportDetails.Week_Number__c=='' || callReportDetails.Week_Number__c==null){
                if(message.length>0) message+=', ';
                message += 'SEA Week #';
                isValid=false;
            }
            if(callReportDetails.Meeting_Summary__c==undefined || callReportDetails.Meeting_Summary__c=='' || callReportDetails.Meeting_Summary__c==null){
                if(message.length>0) message+=', ';
                message += 'Meeting Summary';
                isValid=false;
            }
        }
        
        //Added on 08/07/2019--Starts
        var bizPerformance= component.get("v.bizPerformanceLines");
        var subCategories = [];
        var hasDuplicates = false;
        var noSubCategory = false;
        for(var idx=bizPerformance.length-1;idx>=0;idx--)
        {
            if(bizPerformance[idx].Product_Sub_Category__c!=null 
               && bizPerformance[idx].Product_Sub_Category__c!=undefined 
               && bizPerformance[idx].Product_Sub_Category__c!='None' 
               && bizPerformance[idx].Product_Sub_Category__c!=''){
                if(subCategories.includes(bizPerformance[idx].Product_Sub_Category__c)){
                    if(!hasDuplicates) hasDuplicates=true;
                }else{
                    subCategories.push(bizPerformance[idx].Product_Sub_Category__c);
                }
            }
            else{
                if(!noSubCategory) noSubCategory=true;
            }
        }
        if(noSubCategory){
            if(message.length>0) message+=', ';
            message += 'Sub-Category missing in Business Performance';
            isValid=false;
        }
        if(hasDuplicates){
            if(message.length>0) message+=', ';
            message += 'Remove Duplicate Sub-Category Selection in Business Performance';
            isValid=false;
        }
        //Added on 08/07/2019--Ends
        
        var tasks= component.get("v.tasks");
        // remove tasks empty rows
        for(var t=tasks.length-1;t>=0;t--)
        {
            var task=tasks[t];
             if((task.Subject==undefined || task.Subject=='' || task.Subject==null) &&
                (task.OwnerId==undefined || task.OwnerId=='' || task.OwnerId==null) &&
                (task.ActivityDate==undefined || task.ActivityDate=='' || task.ActivityDate==null) &&
                (task.Description==undefined || task.Description=='' || task.Description==null)) 
                {
                	tasks.splice(t, 1);
                }          
            console.log('t is:'+t);
        }
        
        // validate tasks fields, all are mandatory
        var rows='';
        for(var t=0;t<tasks.length;t++)
        {
            var task=tasks[t];
            //Thiru: Added action item in below if condition----03/18/2019
            //|| (task.Action_Item__c==undefined || task.Action_Item__c=='' || task.Action_Item__c==null)
            if((task.OwnerId==undefined || task.OwnerId=='' || task.OwnerId==null)||
               (task.Subject==undefined || task.Subject=='' || task.Subject==null) ||
               (task.ActivityDate==undefined || task.ActivityDate=='' || task.ActivityDate==null) ||
               (task.Description==undefined || task.Description=='' || task.Description==null) )
            {
                if(rows.length>0) rows+=' & ';
                rows += (Number(t)+1);
                isValid=false;
                
            }
        }
        if(rows.length>0) {
            if(message.length>0) message+=', ';
            message += 'all fields for Action Items at row ' + rows;
        }
        
        //Thiru: Added action item due date validation in below ----03/18/2019
        //Starts here
        var dateValidationRows='';
        for(var t=0;t<tasks.length;t++)
        {
            var task=tasks[t];
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth()+1; 
            var yyyy = today.getFullYear();
            if(dd<10) 
            {
                dd='0'+dd;
            } 
            
            if(mm<10) 
            {
                mm='0'+mm;
            } 
            var currDate = yyyy+'-'+mm+'-'+dd;
            console.log('Task ID------->'+task.Id);
            if((task.Id==null||task.Id==''||task.Id==undefined) && task.ActivityDate<=currDate){
                if(dateValidationRows.length>0) dateValidationRows+=' & ';
                dateValidationRows += (Number(t)+1);
                isValid=false;
            }
        }
        if(dateValidationRows.length>0) {
            if(message.length>0) message+=', ';
            message += 'Due Date should be future date for Action Items at row ' + dateValidationRows;
        }
        //Ends Here
        component.set("v.tasks",tasks);

        
        var subAccounts= component.get("v.subAccount");
        // remove empty rows for subaccounts
        for(var idx=subAccounts.length-1;idx>=0;idx--)
        {
            var subAccount=subAccounts[idx];
            if((subAccount.Account_Type__c==undefined || subAccount.Account_Type__c=='' || subAccount.Account_Type__c==null) &&
               (subAccount.Sub_Account__c==undefined || subAccount.Sub_Account__c=='' || subAccount.Sub_Account__c==null))
            {
                subAccounts.splice(idx, 1);
            }
        }
        
        rows='';
        for(var idx=0;idx<subAccounts.length;idx++)
        {
            var subAccount=subAccounts[idx];
            if(subAccount.Account_Type__c!='')   {       
                if(subAccount.Sub_Account__c==undefined || subAccount.Sub_Account__c=='' || subAccount.Sub_Account__c==null){
                    if(rows.length>0) rows+=' & ';
                    rows += (Number(idx)+1);
                    isValid=false;
                }
            }
        }
        if(rows.length>0){
            if(message.length>0) message+=', ';
            message += 'Account Name for Additional Accounts at row ' +rows;
        }
        component.set("v.subAccount",subAccounts);
        
        
        var productCategories= component.get("v.productCategories");
        // remove empty product category rows
        for(var idx=productCategories.length-1;idx>=0;idx--)
        {
            var pc=productCategories[idx];
           if((pc.Division__c==undefined || pc.Division__c=='' || pc.Division__c==null) &&
               (pc.Product_Category__c==undefined || pc.Product_Category__c=='' || pc.Product_Category__c==null) &&
               (pc.Notes__c==undefined || pc.Notes__c=='' || pc.Notes__c==null))
            {
                productCategories.splice(idx, 1);
            }
        }
        
        // validate product category fileds, all are mandatory
        rows='';
        for(var idx=0;idx<productCategories.length;idx++)
        {
            var pc=productCategories[idx];
            if(pc.Division__c=='Smart Home') {
                if(pc.Notes__c==undefined || pc.Notes__c=='' || pc.Notes__c==null){
                    if(rows.length>0) rows+=' & ';
                    rows += (Number(idx)+1);
                    isValid=false;
                }
                    
            } else if ((pc.Division__c==undefined || pc.Division__c=='' || pc.Division__c==null) ||
               (pc.Product_Category__c==undefined || pc.Product_Category__c=='' || pc.Product_Category__c==null) ||
               (pc.Notes__c==undefined || pc.Notes__c=='' || pc.Notes__c==null))
            {
              	if(rows.length>0) rows+=' & ';
                rows += (Number(idx)+1);
                isValid=false;
            }
        }
        
        if(rows.length>0) {
            if(message.length>0) message+=', ';
            message += 'all fields for Product Category Notes at row ' +rows;
        }
        
        component.set("v.productCategories",productCategories);
        
        var meetingAttendees= component.get("v.meetingAttendees");
        // remove empty rows for meetimf attendees
        for(var idx=meetingAttendees.length-1;idx>=0;idx--)
        {
            var ma=meetingAttendees[idx];
            if((ma.Customer_Attendee__c==undefined || ma.Customer_Attendee__c=='' || ma.Customer_Attendee__c==null))
            {
                meetingAttendees.splice(idx, 1);
            }
        }
        component.set("v.meetingAttendees",meetingAttendees);
       
        var samsungAttendees= component.get("v.samsungAttendees");
        for(var idx=samsungAttendees.length-1;idx>=0;idx--)
        {
            var sa=samsungAttendees[idx];
            if((sa.Attendee__c==undefined || sa.Attendee__c=='' || sa.Attendee__c==null))
            {
                samsungAttendees.splice(idx, 1);
            }
        }
        component.set("v.samsungAttendees",samsungAttendees);
        
        
        if(!isValid) {
            this.showToast(component, event,'Error','Please complete: \n'+message,10000);
        }
        
        component.set("v.isValid",isValid);
    },
    
    /*
     sendToVF : function(component, event) {
        var message = "saved";
        var vfOrigin = "https://samsungsea--callrep--c.cs95.visual.force.com/apex/CallReportPage";
        var vfWindow = component.find("cFrame").getElement().contentWindow;
         console.log('sending message to VF');
        vfWindow.postMessage(message, vfOrigin);
    }
    */
    getBuildingBlocks:function(component,event){
        var bblocks = $A.get("{!$Label.c.Call_Report_Building_Blocks}");
        var bblocksArray = [];
        bblocksArray = bblocks.split(',');
        component.set("v.buildingBlocks",bblocksArray);
        //console.log('v.buildingBlocks--->'+component.get("v.buildingBlocks"));
    },
    getBusinessPerfBlocks:function(component,event){
        var bussPerfblockstring = $A.get("{!$Label.c.Call_Report_Market_Intelligence}");
        var bussPerfblocks = [];
        bussPerfblocks = bussPerfblockstring.split(',');
        component.set("v.businessPerfBlocks",bussPerfblocks);
        //console.log('v.businessPerfBlocks--->'+component.get("v.businessPerfBlocks"));
    },
    getBusPerfHoverText:function(component,event){
        var busPerfblockstring = $A.get("{!$Label.c.Call_Report_Market_Intelligence}");
        var busPerfblocks = component.get("v.businessPerfBlocks");
        var hoverMap = {};
        for(var idx=0;idx<busPerfblocks.length;idx++){
            if(busPerfblocks[idx]==''){
                
            }
            hoverMap.set(busPerfblocks[idx],'');
        }
        component.set("v.busPerfHoverTextMap",busPerfblocks);
    },
    
    getMIHoverTextMap:function(component,event){
        var action= component.get("c.getMIhoverText");
        console.log('component valid='+JSON.stringify(component.isValid()));
        action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                var allDepList = [];
                var depMap = response.getReturnValue();
                //console.log('all hover List='+JSON.stringify(response.getReturnValue()));
                //console.log('dep map1='+JSON.stringify(depMap));
                for (var key in depMap) {
                    var depVal = depMap[key];
                    allDepList.push({val:depVal, parent:key});
                }
                //console.log('all hover List='+JSON.stringify(allDepList));                
                component.set("v.miHoverTextMap",allDepList);
            }
        });
        $A.enqueueAction(action);
    },
    
    //Added on 09-12-2019
    getMIButtonValues:function(component,event){
        var action= component.get("c.getMIButtonValues");
        action.setCallback(this, function(response){
          	var state= response.getState();
            //console.log('MI Hover Records===>'+JSON.stringify(response.getReturnValue()));
            
            if(component.isValid() && state=="SUCCESS"){
                var allDepList = [];
                var depMap = response.getReturnValue();
                for (var key in depMap) {
                    var depList = depMap[key];
                    var depObjList =[];
                    for (var d in depList) {
                        depObjList.push({text:depList[d],value:depList[d]});
                    }
                    allDepList.push({parent:key,dependentList:depObjList});
                }
                console.log('allDepList===>'+JSON.stringify(allDepList));
                component.set("v.MarketIntelligence",allDepList);
            }
        });
        $A.enqueueAction(action);
    },
    
    businessPerfControl:function(component,event,selected,SelectedValue){
        var rowIndex = event.currentTarget.value;
        var crBPList = component.get("v.bizPerformanceLines");
        //console.log('Selected---->'+selected);
        if(selected=='Sell-Through') crBPList[rowIndex].Sell_Through__c=SelectedValue;
        if(selected=='Weeks of Supply') crBPList[rowIndex].WOS__c=SelectedValue;
        if(selected=='Balance of Share') crBPList[rowIndex].BOS__c=SelectedValue;
        if(selected=='Flooring/Share of Shelf') crBPList[rowIndex].Flooring__c=SelectedValue;
        if(selected=='Online Sales') crBPList[rowIndex].Online__c=SelectedValue;
        if(selected=='Sell-In') crBPList[rowIndex].Sell_In__c=SelectedValue;
        component.set("v.bizPerformanceLines",crBPList);
    },
    
    onStatusChange:function(component,event,callReport){
        var status = callReport.Status__c;
        var inPrePlanning = false;
        if(status=='Pre-Planning'){
            inPrePlanning = true;
        }
        component.set("v.inPrePlanning",inPrePlanning);
    },
    
    //Add Biz performance--Added---08/06/2019
    addBizPerformance:function(component,event){
        component.set("v.isBizPerfDelete",false);
        component.set("v.isProdCategoryChange",false);
        var callRep = component.get("v.callreport");
        var category = callRep.Primary_Category__c;
        
        if(category!='' && category!=null && category!=undefined){
            var crBPList = [];
            crBPList.push({
                'sobjectType': 'Call_Report_Business_Performance__c',
                'Product_Category__c': category
            });
            
            component.set("v.bizPerformanceLines",crBPList);
        }else{
            var crBPList = [];
            component.set("v.bizPerformanceLines",crBPList);
        }
    },
    
    onSubCategoryChange:function(component,event){
        var callRep = component.get("v.callreport");  
        var category = callRep.Primary_Category__c;
        var subCategory = callRep.Product_Sub_Category__c;
        var result = subCategory.split(";");
        
        var crBPList = component.get("v.bizPerformanceLines");
        var BPList = [];
        var existingSubCat = [];
        var deleteList=component.get("v.delete-BizPerformanceLines");
        for(var i=0;i<crBPList.length;i++){
            if(crBPList[i].Product_Sub_Category__c!='' 
               && crBPList[i].Product_Sub_Category__c!=null 
               && crBPList[i].Product_Sub_Category__c!=undefined)
            {
                existingSubCat.push(crBPList[i].Product_Sub_Category__c);
                if(result.includes(crBPList[i].Product_Sub_Category__c)) BPList.push(crBPList[i]);
                else if(crBPList[i]!=undefined && crBPList[i].Id!=undefined && crBPList[i].Id!=null) deleteList.push(crBPList[i]);
            }
        }
        console.log('existingSubCat===>'+existingSubCat);
        console.log('result===>'+result);
        if(result.length>0){
            for(var i=0;i<result.length;i++){
                if(!existingSubCat.includes(result[i])){
                    BPList.push({
                        'sobjectType': 'Call_Report_Business_Performance__c',
                        'Product_Category__c': category,
                        'Product_Sub_Category__c': result[i]
                    });
                }
            }
        }else{
            BPList.push({
                'sobjectType': 'Call_Report_Business_Performance__c',
                'Product_Category__c': category
            });
        }
        component.set("v.delete-BizPerformanceLines", deleteList);
        component.set("v.bizPerformanceLines",BPList);
    },
    
})