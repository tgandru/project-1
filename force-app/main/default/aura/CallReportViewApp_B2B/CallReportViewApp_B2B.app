<aura:application extends="ltng:outApp" access="GLOBAL">
    <aura:attribute name="recordId" type="String"/>
    <c:CallReportView_B2B recordId="{!v.recordId}"/>
</aura:application>