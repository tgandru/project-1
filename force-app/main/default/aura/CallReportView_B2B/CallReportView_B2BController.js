({
	doInit : function(component, event, helper) {
        var url = $A.get('$Resource.Lightning_Background_Image');
		component.set('v.backgroundImageURL', url);
        
        var reportId=component.get("v.recordId");
        var accountId=component.get("v.accountId");
        var action= component.get("c.getCallReport");
         action.setParams({
            "crId":reportId
        });
        console.log('Init:'+action);
        console.log('accountId='+accountId);
        
        helper.fetchPickListVal(component, {'sobjectType':'Call_Report__c'},'Status__c', 'status','statusSelectOptions');
        
        action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('call report1='+ JSON.stringify(response.getReturnValue()));
                
                component.set("v.callreport",response.getReturnValue());

                var callReportDetails = component.get("v.callreport");
                console.log('Status-------->'+callReportDetails.Status__c);
                
				if(callReportDetails.Status__c=='Submitted'){
                    component.set("v.disableSave","true");
                    component.set("v.showCancel","true");
                }
                
                console.log('DisableSave------>'+component.get("v.disableSave"));
                console.log('Show cancel------>'+component.get("v.showCancel"));
                console.log('DisableEdit------>'+component.get("v.disableEdit"));
                if (reportId != undefined && reportId!=null) {
                    helper.getRecordAccess(component,event,callReportDetails);
                    
                    helper.getMeetingAttendees(component,event,callReportDetails);
                    helper.getTasks(component,event,callReportDetails);
                    helper.getProductLine(component,event,callReportDetails);
                    helper.getSubAccount(component,event,callReportDetails);
                    helper.getSamsungAttendees(component,event,callReportDetails);
                    helper.getDistributionLists(component,event,callReportDetails); //get the Distribution Lists
                    helper.getCRDistributionLists(component,event,callReportDetails); //get CR Distribution Lists
                    helper.getNotificationMembers(component,event,callReportDetails);
                    helper.loadedFiles(component, event, helper, reportId);
                    helper.getInstanceURL(component,event);
                }
                
                //get record type if new record
                if (reportId == undefined || reportId==null) {
                    var recTypeId = helper.getParameterByName(component,event,'recordTypeId');
                    console.log('rec type id1='+recTypeId);
                    if (recTypeId!=undefined && recTypeId!=null && recTypeId!='') {
                        component.set("v.callreport.RecordTypeId", recTypeId);

                       var actionRec= component.get("c.getRecordType");
                        actionRec.setParams({
                            "recTypeId":recTypeId
                        });
                        
                         actionRec.setCallback(this, function(response){
                                console.log('rec type='+ JSON.stringify(response.getReturnValue()));
                                var state= response.getState();
                                if(component.isValid() && state=="SUCCESS"){
                                    if (response.getReturnValue().Id!=undefined && response.getReturnValue().Id!=null) {
                                         component.set("v.callreport.RecordType.Id", response.getReturnValue().Id);
                                    	component.set("v.callreport.RecordType.Name", response.getReturnValue().Name);
                                    	console.log('rec type1='+ component.get("v.callreport.RecordType.Name"));
                                    } else {
                                        console.log('rec type is null');
                                    }
                                   
                                }
                            });
                            $A.enqueueAction(actionRec); 
                    }
                    
                    var accId = accountId;
                    
                    if (accId == undefined || accId ==null || accId =='') {
                    	accId = helper.getParameterByName(component,event,'accountId');
                    }
                    
                    console.log('accId from account1='+ accId);

                    if (accId!=undefined && accId!=null && accId!='') {
                            component.set("v.callreport.Primary_Account__c", accId);
                            var actionAcc= component.get("c.getAccount");
                             actionAcc.setParams({
                                "accId":accId
                            });
                            actionAcc.setCallback(this, function(response){
                                var state= response.getState();
                                if(component.isValid() && state=="SUCCESS"){
                                    console.log('account res1='+ JSON.stringify(response.getReturnValue()));
                                    component.set("v.callreport.Primary_Account__r.Id", response.getReturnValue().Id);
                                    component.set("v.callreport.Primary_Account__r.Name", response.getReturnValue().Name);
                                    component.set("v.callreport.Primary_Account__c", accId);
    
                                }
                            });
                            $A.enqueueAction(actionAcc);
                    }
                }
           	}
        });
        $A.enqueueAction(action);
	},
    
    toggle : function(component, event, helper) {
        var toggleText = component.find("text");
        $A.util.toggleClass(toggleText, "toggle");
    },
    
    EditComponent : function (component, event, helper) {
        var reportId=component.get("v.recordId");
        var accountSel = component.get("v.callreport.Primary_Account__c.val");
        var baseURL = component.get("v.InstanceUrl");
        var urlEvent = $A.get("e.force:navigateToURL");
        if( (typeof sforce != 'undefined') && sforce && (!!sforce.one) ) {
        	if(reportId!=undefined && reportId!=''){
                urlEvent.setParams({
                  "url": "#/sObject/Call_Report__c/list?filterName=Recent"
                });
                urlEvent.fire();
            }else{
                urlEvent.setParams({
                  "url": "#/sObject/Call_Report__c/list?filterName=Recent"
                });
                urlEvent.fire();
            }
        }
        else {
            //self.location = "CallReportPage_B2B?id=a2n18000000w9r0";
			if(reportId!=undefined && reportId!=''){
				window.location = baseURL+"/apex/CallReportPage_B2B?id="+reportId;
            }else{
                window.location = baseURL+"/"+accountSel;
            }
        }
    },
    
    GeneratePDF : function (component, event, helper) {
        var reportId=component.get("v.recordId");
        var baseURL = component.get("v.InstanceUrl");
        var urlEvent = $A.get("e.force:navigateToURL");
        if( (typeof sforce != 'undefined') && sforce && (!!sforce.one) ) {
        	if(reportId!=undefined && reportId!=''){
                urlEvent.setParams({
                  "url": "#/sObject/Call_Report__c/list?filterName=Recent"
                });
                urlEvent.fire();
            }else{
                urlEvent.setParams({
                  "url": "#/sObject/Call_Report__c/list?filterName=Recent"
                });
                urlEvent.fire();
            }
            /*console.log('reportId---->'+reportId);
            if(reportId!=undefined && reportId!=''){
				window.location = baseURL+"/apex/CallReportPDF_B2B?id="+reportId;
            }
            else{
                alert('Cannot generate PDF');
            }*/
        }
        else {
            console.log('reportId---->'+reportId);
            //self.location = "CallReportPage_B2B?id=a2n18000000w9r0";
			if(reportId!=undefined && reportId!=''){
				window.location = baseURL+"/apex/CallReportPDF_B2B?id="+reportId;
            }
            else{
                alert('Cannot generate PDF');
            }
        }
    },
    
    applycss:function(cmp,event){
        var cmpTarget = cmp.find('Modalbox');
        var cmpBack = cmp.find('MB-Back');
        $A.util.addClass(cmpTarget, 'slds-fade-in-open');
        $A.util.addClass(cmpBack, 'slds-backdrop--open');
    },
    
    removecss:function(cmp,event){
        var cmpTarget = cmp.find('Modalbox');
        var cmpBack = cmp.find('MB-Back');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open');        
    },
    
    navigateToRecord: function(cmp, evt){
        window.location.href = '/' + evt.getParam('recordId');
    },
    
    downloadFile : function (component, event, helper){
    	var fileid = event.target.getAttribute("data-id"); 
        var baseURL = component.get("v.InstanceUrl");
        window.location = baseURL+"/"+fileid;
    },
    openTaskRec : function (component, event, helper){
    	var recId = event.target.getAttribute("data-id"); 
        var baseURL = component.get("v.InstanceUrl");
        window.location = baseURL+"/"+recId;
    },
    clickSubmitButton: function(component, event, helper) {
        component.set("v.openModal", true);
    },
    closeModal: function(component, event, helper) {
        component.set("v.openModal", false);
        component.set("v.openValidationModal", false);
    },
    openRevertSubmissionModal: function(component, event, helper) {
        component.set("v.openRevertSubmissionModal", true);
    },
    closeRevertSubmissionModal: function(component, event, helper) {
        component.set("v.openRevertSubmissionModal", false);
    },
    submitCallReport: function(component, event, helper) {
        var isSubmit = true;
        var callReportDetails = component.get("v.callreport");
        callReportDetails.Status__c='Submitted';
        var reportId = callReportDetails.Id;
        var validcheckMsg='';
        if((callReportDetails.Agenda__c==''||callReportDetails.Agenda__c==null||callReportDetails.Agenda__c==undefined)
           ||(callReportDetails.Meeting_Summary__c ==''||callReportDetails.Meeting_Summary__c ==null||callReportDetails.Meeting_Summary__c ==undefined)){
            validcheckMsg='You must populate the Agenda and Meeting Summary fields before submitting the call report.\n';
            isSubmit = false;
        }
        if(callReportDetails.Number_of_Attendees__c==0){
            if(validcheckMsg!=''){
                validcheckMsg = validcheckMsg+'\n'+'You must add at least one attendee before submitting the call report';
            }else{
                validcheckMsg='You must add at least one Customer Attendee before submitting the call report';
            }
            isSubmit = false;
        }
        if(validcheckMsg!=''&&validcheckMsg!=null&&validcheckMsg!=undefined){
            component.set("v.Message",validcheckMsg);
            component.set("v.openModal", false);
        	component.set("v.openValidationModal", true);
        }
        console.log('<----Before Save---->');
        console.log('<----issubmit---->'+isSubmit);
        
        if(isSubmit==true){
            var action= component.get("c.submitCallReportB2B");
            action.setParams({
                "callReport":callReportDetails
            });
            action.setCallback(this, function(response){
                var baseURL = component.get("v.InstanceUrl");
                helper.GotoView(component,event,reportId,baseURL);
            });
            $A.enqueueAction(action);
        }
        
    },
    cancelSubmission: function(component, event, helper) {
        var isSave = true;
        var callReportDetails = component.get("v.callreport");
        var reportId = callReportDetails.Id;
        callReportDetails.Status__c='Cancelled';
        /*
        var validcheckMsg='';
        if(callReportDetails.Status__c=='Submitted'){
            validcheckMsg='You must select Draft or Pre-Planning Status';
            isSave = false;
            console.log('<----Submitted Status---->');
        }
        
        if(validcheckMsg!=''&&validcheckMsg!=null&&validcheckMsg!=undefined){
            component.set("v.Message",validcheckMsg);
        	component.set("v.openRevertSubmissionModal", true);
            console.log('<----Modal---->');
        }
        */
        console.log('<----Before Save---->');        
        if(isSave==true){
            var action= component.get("c.submitCallReportB2B");
            action.setParams({
                "callReport":callReportDetails
            });
            action.setCallback(this, function(response){
                var baseURL = component.get("v.InstanceUrl");
                helper.GotoView(component,event,reportId,baseURL);
            });
            $A.enqueueAction(action);
        }
        
    }
})