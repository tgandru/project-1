({
    GotoView:function (component,event,reportId,baseURL) {
       console.log("report id:" + reportId);
       window.location = baseURL+"/"+reportId;
    },
    getTasks:function(component,event,callReport){
        var action= component.get("c.getTasks");
        action.setParams({
            "callReport":callReport
        });
         action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                var records = response.getReturnValue();
                console.log('result------>'+JSON.stringify(records));
                records.forEach(function(record){
                    var recId = record.Id;
                console.log('record id---->'+recId);
                });
                //component.set("v.tasks",response.getReturnValue());
                component.set("v.tasks",records);
            }
        });
        $A.enqueueAction(action);
    },
    
    getProductLine:function(component,event,callReport){
        var action= component.get("c.getProductLine");
        action.setParams({
            "callReport":callReport
        });
         action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                var records = response.getReturnValue();
                // records.forEach(function(record){
                //    record.linkName = '/'+record.Id;
                //});
                component.set("v.productCategories",records);
            }
        });
        $A.enqueueAction(action);
    },
    
      getSubAccount:function(component,event,callReport){
        var action= component.get("c.getSubAccount");
        action.setParams({
            "callReport":callReport
        });
         action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                var records = response.getReturnValue();
               //  records.forEach(function(record){
               //     record.SubAcclink = '/'+record.Sub_Account__r.Name;
               // });
                component.set("v.subAccount",records);
            }
        });
        $A.enqueueAction(action);
    },
    
    getMeetingAttendees:function(component,event,callReport){
        var action= component.get("c.getMeetingAttendees");
        action.setParams({
            "callReport":callReport
        });
         action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                component.set("v.meetingAttendees",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },

    getSamsungAttendees:function(component,event,callReport){
        var action= component.get("c.getSamsungAttendees");
        action.setParams({
            "callReport":callReport
        });
         action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('sattended='+ JSON.stringify(response.getReturnValue()));
                component.set("v.samsungAttendees",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    getDistributionLists:function(component,event,callReport){
        var action= component.get("c.getDistributionLists");
        action.setParams({
            "callReport":callReport
        });
         action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                var result = response.getReturnValue();
                for (var indexVar = 0; indexVar < result.length; indexVar++) {
                    console.log('Distribution list Name--------->'+result[indexVar].Name);
                }
                component.set("v.DistributionLists",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    //get CR Distribution Lists--12/03/2018
    getCRDistributionLists:function(component,event,callReport){
        var action= component.get("c.getCRDistributionLists");
        action.setParams({
            "callReport":callReport
        });
         action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                var result = response.getReturnValue();
                for (var indexVar = 0; indexVar < result.length; indexVar++) {
                    console.log('Distribution list Name--------->'+result[indexVar].Name);
                }
                component.set("v.crDistributionLists",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    getNotificationMembers:function(component,event,callReport){
        var action= component.get("c.getCRnotificationMembers");
        action.setParams({
            "callReport":callReport
        });
         action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('nMembers='+ JSON.stringify(response.getReturnValue()));
                component.set("v.notificationMembers",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    showToast : function(component, event, type,message,duration) {
         var toastEvent = $A.get("e.force:showToast");
        if(toastEvent!=undefined){
            toastEvent.setParams({
                "type":type,
                "message": message,
                "duration": duration

            });
            toastEvent.fire();
        } else {
            alert(message);
        }
    },
    
    getParameterByName : function(cmp,event,name) {
       var url = window.location.href;
       name = name.replace(/[\[\]]/g, "\\$&");
       var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
       results = regex.exec(url);
       if (!results) return null;
       if (!results[2]) return '';
       return decodeURIComponent(results[2].replace(/\+/g, " "));
    },
    
    getInstanceURL:function(component,event){
        var action= component.get("c.getInstanceUrl");

        action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                component.set("v.InstanceUrl",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    loadedFiles: function(component, event, helper,reportId){
    	console.log('loaded event='+reportId);
        //var childCmp = component.find("fileDisplay");
        //childCmp.loadData(reportId);
        
        var action= component.get("c.getFiles");
        action.setParams({
            "parentId":reportId
        });
         action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('get files='+ JSON.stringify(response.getReturnValue()));
                component.set("v.files",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    fetchPickListVal: function(component,objectName,fieldName, elementId, optionList) {
        var action = component.get("c.getselectOptions");

        action.setParams({
            "objObject": objectName,
            "fld": fieldName
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();

                if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        label: "--- None ---",
                        value: ""
                    });
                }
                for (var i = 0; i < allValues.length; i++) {
                    if(fieldName=='Status__c'){
                        if(allValues[i]=='Submitted' || allValues[i]=='Cancelled') continue;
                    }
                    opts.push({
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                
                //console.log('opts='+JSON.stringify(opts));
                //component.find(elementId).set("v.options", opts);
                component.set("v."+optionList, opts);
            }
        });
        $A.enqueueAction(action);
    },
    
    getRecordAccess:function(component,event,callReport){
        var action= component.get("c.getRecordAccess");
		action.setParams({
            "callReport":callReport
        });
        action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                var callReportAccess = response.getReturnValue();
                console.log('Access in Helper-------->'+callReportAccess);
                component.set("v.recordAccess",response.getReturnValue());
                var callReportAccess2 = component.get("v.recordAccess");
                console.log('Access in Helper after assign-------->'+callReportAccess2);
                if(callReportAccess2=='ReadOnly'){
                    component.set("v.disableEdit","true");
                    component.set("v.disableSave","true");
                    component.set("v.showCancel","true");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
})