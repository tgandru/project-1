({
    doInit : function(component, event, helper) {
        
        var reportId=component.get("v.recordId");
        var action= component.get("c.getCallReport");
         action.setParams({
            "crId":reportId
        });
        console.log('Init:'+action);
        
        action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('call report1='+ JSON.stringify(response.getReturnValue()));
                component.set("v.callreport",response.getReturnValue());
                
                var callReportDetails = component.get("v.callreport");
				
                if(reportId != undefined && reportId!=null){
                    helper.getTasks(component,event,callReportDetails);
                }
            }
        });
        $A.enqueueAction(action);
    },
})