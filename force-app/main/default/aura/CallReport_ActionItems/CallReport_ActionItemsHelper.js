({
	getTasks:function(component,event,callReport){
        var action= component.get("c.getBuildingBlockTasks");
        action.setParams({
            "callReport":callReport
        });
         action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                component.set("v.tasks",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
})