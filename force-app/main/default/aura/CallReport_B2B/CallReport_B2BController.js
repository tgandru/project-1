({
	doInit : function(component, event, helper) {
        
        var reportId=component.get("v.recordId");
        var accountId=component.get("v.accountId");
        var action= component.get("c.getCallReportB2B");
         action.setParams({
            "crId":reportId
        });
        console.log('Init:'+action);
        console.log('accountId='+accountId);

        //helper.fetchPickListVal(component, {'sobjectType':'Call_Report__c'},'Meeting_Topics__c', 'meetingTopics','selectOptions');
        helper.fetchPickListValDivision(component, {'sobjectType':'Call_Report__c'},'Meeting_Topics__c', 'meetingTopics','selectOptions');
		helper.fetchPickListVal(component, {'sobjectType':'Call_Report__c'},'Status__c', 'status','statusSelectOptions');
        helper.fetchPickListVal(component, {'sobjectType':'Call_Report_Sub_Account__c'},'Account_Type__c', 'accountType','accountTypeOptions');
        //helper.fetchDependPickList(component, 'Product_Category_Note__c','Division__c','Product_Category__c','dependentPickList');
        //helper.fetchDependPickList(component, 'Call_Report__c','Meeting_Topics__c','Meeting_Sub_Type__c','MeetingSubTypeOptions');
        helper.fetchDependPickList(component, 'Task','Activity_Type__c','Activity_Sub_Type__c','TaskTypeOptions');
        helper.fetchDependPickListforB2B(component, 'Product_Category_Note__c','Division__c','Product_Category__c','dependentPickList');
        
        action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('call report1='+ JSON.stringify(response.getReturnValue()));
                
                component.set("v.callreport",response.getReturnValue());

                var callReportDetails = component.get("v.callreport");
                console.log('callReportDetails='+ JSON.stringify(callReportDetails));
                console.log('Meeting Type----->'+callReportDetails.Meeting_Topics__c);
                console.log('Meeting Sub Type----->'+callReportDetails.Meeting_Sub_Type__c);
				console.log('reportId1='+reportId);
                if (reportId != undefined && reportId!=null && reportId!='') {
                    helper.getMeetingAttendees(component,event,callReportDetails);
                    helper.getTasks(component,event,callReportDetails);
                    helper.getProductLine(component,event,callReportDetails);
                    helper.getSubAccount(component,event,callReportDetails);
                    helper.getSamsungAttendees(component,event,callReportDetails);
                    helper.getDistributionLists(component,event,callReportDetails); //get the Distribution Lists
                    helper.getCRDistributionLists(component,event,callReportDetails); //get the CR Distribution Lists--12/03/2018
                    helper.getNotificationMembers(component,event,callReportDetails);
                    //console.log('<-----getNotificationMembers----->');
                    helper.loadedFiles(component, event, helper, reportId);
                    helper.setMeetingSubType(component,event,callReportDetails);
                }
                
                //get record type if new record
                console.log('reportId='+reportId);
                if (reportId == undefined || reportId==null || reportId=='') {
                    helper.addNewDefaultSamsungAttendee(component,event);
                    //var recTypeId1 = component.get("v.recordTypeId");
                    var recTypeId = helper.getParameterByName(component,event,'recordTypeId');
                    console.log('rec type id1='+recTypeId);
                    if (recTypeId!=undefined && recTypeId!=null && recTypeId!='') {
                        component.set("v.callreport.RecordTypeId", recTypeId);

                       var actionRec= component.get("c.getRecordType");
                        actionRec.setParams({
                            "recTypeId":recTypeId
                        });
                        
                         actionRec.setCallback(this, function(response){
                                console.log('rec type='+ JSON.stringify(response.getReturnValue()));
                                var state= response.getState();
                                if(component.isValid() && state=="SUCCESS"){
                                    if (response.getReturnValue().Id!=undefined && response.getReturnValue().Id!=null) {
                                         component.set("v.callreport.RecordType.Id", response.getReturnValue().Id);
                                    	component.set("v.callreport.RecordType.Name", response.getReturnValue().Name);
                                    	console.log('rec type1='+ component.get("v.callreport.RecordType.Name"));
                                    } else {
                                        console.log('rec type is null');
                                    }
                                   
                                }
                            });
                            $A.enqueueAction(actionRec); 
                    }
                    
                    var accId = accountId;
                    //var accId = component.get("v.accountId");
                    
                    if (accId == undefined || accId ==null || accId =='') {
                    	accId = helper.getParameterByName(component,event,'accountId');
                    }
                    
                    console.log('accId from account1='+ accId);

                    if (accId!=undefined && accId!=null && accId!='') {
                            component.set("v.callreport.Primary_Account__c", accId);
                        	console.log('accId from account3='+ component.get("v.callreport.Primary_Account__c"));
                            var actionAcc= component.get("c.getAccount");
                             actionAcc.setParams({
                                "accId":accId
                            });
                            actionAcc.setCallback(this, function(response){
                                var state= response.getState();
                                console.log('accId from accountid State='+ state);
                                if(component.isValid() && state=="SUCCESS"){
                                    console.log('account res1='+ JSON.stringify(response.getReturnValue()));
                                    component.set("v.callreport.Primary_Account__r.Id", response.getReturnValue().Id);
                                    component.set("v.callreport.Primary_Account__r.Name", response.getReturnValue().Name);
                                    console.log('acid name------------->'+component.get("v.callreport.Primary_Account__r.Name"));
                                    component.set("v.callreport.Primary_Account__c", accId);
    
                                }
                            });
                            $A.enqueueAction(actionAcc);
                    }
                    
                }
           	}
        });
        $A.enqueueAction(action);
		
	},
            
    saveRecord: function(component,event,helper){
        var callReportDetails = component.get("v.callreport");
        helper.validateFields(component,event);
        var isValid=component.get("v.isValid");
        console.log('call report name is1:'+callReportDetails.Name);
        //console.log('call report account name is1:'+callReportDetails.Primary_Account__r.name);
        
        if(isValid){
            var accountSel = component.get("v.callreport.Primary_Account__c.val");
            console.log("primary account="+JSON.stringify(accountSel));
             if (accountSel!=undefined && accountSel!=null) {
                component.set("v.callreport.Primary_Account__c", accountSel);
             }
            console.log("call report1="+JSON.stringify(callReportDetails));
                
            var action= component.get("c.saveCallReport");
            action.setParams({
                "callReport":callReportDetails
            });
                
            component.set('v.disableSave',true);
             action.setCallback(this, function(response){
                var state= response.getState();
                 
                if(component.isValid() && state=="SUCCESS"){
                    console.log('saved call report11='+JSON.stringify(response.getReturnValue()));
    
                    //$A.get('e.force:refreshView').fire();
    
                    component.set("v.callreport",response.getReturnValue());
                    var callReportDetails = component.get("v.callreport");
                    
                    var reportId = response.getReturnValue().Id;
                    helper.saveTasks(component,event,callReportDetails);
                    helper.saveProductCategory(component,event,callReportDetails);
                    helper.saveSubAccount(component,event,callReportDetails);
                    helper.saveMeetingAttendees(component,event,callReportDetails);
                    helper.saveDistributionLists(component,event,callReportDetails); //Save selected Distribution Lists
                    helper.saveCRDistributionLists(component,event,callReportDetails); //Save selected CR Distribution Lists
                    helper.saveSamsungAttendees(component,event,callReportDetails);
                    //Delete Distribution Lists
                    helper.deleteDistributionLists(component,event,callReportDetails);
                    helper.saveNotificationMembers(component,event,callReportDetails);
                    // delete records
                    helper.deleteRecords(component, event);
    				
                    // redirect after files uploaded
                    helper.uploadFile(component, event, helper, reportId, function(result) {
                        //console.log("callback for aura:method was executed");
                        console.log("file upload result: " + result);
                        component.set('v.disableSave',false);
                        helper.showToast(component, event,'success','Call Report Saved');
                        var baseURL = component.get("v.InstanceUrl");
                        helper.GotoView(component,event,reportId,baseURL);
                    });    
                }
               
                 
            });
            $A.enqueueAction(action);
        }else{
             var msg = component.get("v.Message");
             //helper.showToast(component, event,'Error','Please complete: \n'+msg,10000);  
            console.log('msg----->'+msg);
            component.set("v.openModal", true);
        }
    },
    
    handleCloseModal: function(component, event, helper) {
        //For Close Modal, Set the "openModal" attribute to "fasle"  
        component.set("v.openModal", false);
    },
    
	addNewTask:function(component,event,helper){
		helper.addNewTask(component,event);
    },

    addNewProductLine:function(component,event,helper){
        helper.addNewProductLine(component,event);
    },
    
     addNewMeetingAttendee:function(component,event,helper){
        helper.addNewMeetingAttendee(component,event);
    },
    
     addNewSubAccount:function(component,event,helper){
        helper.addNewSubAccount(component,event);
    },
            
     addNewSamsungAttendee:function(component,event,helper){
     helper.addNewSamsungAttendee(component,event);
    },
    
    //Add new Distribution List
    addDistributionList:function(component,event,helper){
     helper.addDistributionList(component,event);
    },
    //Add new CR Distribution List--12/03/2018
    addCRDistributionList:function(component,event,helper){
     helper.addCRDistributionList(component,event);
    },
      
     addNewNotificationMember:function(component,event,helper){
     helper.addNewNotificationMember(component,event);
    },
    
    onMeetingTypeChange: function(component, event, helper) {
        var callReportDetails = component.get("v.callreport");
        helper.setMeetingSubType(component, event, callReportDetails);
    },
    
    toggle : function(component, event, helper) {
        var toggleText = component.find("text");
        $A.util.toggleClass(toggleText, "toggle");
    },
    
    Cancel : function (component, event, helper) {
 		var reportId=component.get("v.recordId");
        var accountSel = component.get("v.accountId");
        var baseURL = component.get("v.InstanceUrl");
        
        if(reportId!=undefined && reportId!=''){
            window.location = baseURL+"/"+reportId;
        }else{
            window.location = baseURL+"/"+accountSel;
        }
    },
    
    removeTaskRow : function(component, event, helper){
        helper.removeRow(component,event,"tasks");
    },
    
    removeProductCategoriesRow : function(component, event, helper){
        helper.removeRow(component,event,"productCategories");
    },
    
    removeSubAccountRow : function(component, event, helper){
        helper.removeRow(component,event,"subAccount");
    },
    
    removeMeetingAttendeesRow : function(component, event, helper){
        helper.removeRow(component,event,"meetingAttendees");
    },    
    
    removeSamsungAttendeesRow : function(component, event, helper){
        helper.removeRow(component,event,"samsungAttendees");
    },
    
    removeDistributionListRow : function(component, event, helper){
        helper.removeRow(component,event,"DistributionLists");
    },
    
    removeCRDistributionListRow : function(component, event, helper){
        helper.removeRow(component,event,"crDistributionLists");
    },
    removeNotificationMemRow : function(component, event, helper){
        helper.removeRow(component,event,"notificationMembers");
    },
    
    
    applycss:function(cmp,event){
        var cmpTarget = cmp.find('Modalbox');
        var cmpBack = cmp.find('MB-Back');
        $A.util.addClass(cmpTarget, 'slds-fade-in-open');
        $A.util.addClass(cmpBack, 'slds-backdrop--open');
    },
    
    removecss:function(cmp,event){
        var cmpTarget = cmp.find('Modalbox');
        var cmpBack = cmp.find('MB-Back');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open');        
    },
    
    onChangeDivision:function(component, event){
        //var target = event.getSource();
        //var rowIndex = target.get("v.label");
        //console.log('rowIndex---->'+rowIndex);
        
        var pcategory = component.get("v.productCategories");
        console.log('pcategory---->'+JSON.stringify(pcategory));
        component.set("v.productCategories",pcategory);
    }
})