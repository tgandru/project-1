({
	
    GotoView:function (component,event,reportId,baseURL) {
       console.log("report id:" + reportId);
       window.location = baseURL+"/"+reportId;
    },
    
    addNewTask:function(component,event){
        var taskList = component.get("v.tasks");
          taskList.push({
            'sobjectType': 'Task'
        });
       component.set("v.tasks",taskList);
    },
    
    addNewProductLine:function(component,event){
     
        var pList=component.get("v.productCategories");
        
        /*
        var action= component.get("c.getNewPC");
         action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                var pc = response.getReturnValue();
                console.log('PC1 ='+JSON.stringify(pc));
                pList.push(pc);
                component.set("v.productCategories",pList);
            }
        });
        $A.enqueueAction(action);
        */
        
        pList.push({
            'sobjectType':'Product_Category_Note__c'
        });
        console.log('plist='+JSON.stringify(pList));
        component.set("v.productCategories",pList);
    },
    
       addNewSubAccount:function(component,event){
        var sAcc=component.get("v.subAccount");
        sAcc.push({
            'sobjectType':'Call_Report_Sub_Account__c'
        });
           console.log('sub acc='+sAcc);
        component.set("v.subAccount",sAcc);
    },
    
      addNewMeetingAttendee:function(component,event){
        var maList=component.get("v.meetingAttendees");
        maList.push({
            'sobjectType':'Call_Report_Attendee__c'
        });
          console.log('ml='+maList);
        component.set("v.meetingAttendees",maList);
    },
    
     addNewSamsungAttendee:function(component,event){
        var sList=component.get("v.samsungAttendees");
        sList.push({
            'sobjectType':'Call_Report_Samsung_Attendee__c'
        });
          console.log('ml1='+JSON.stringify(sList));
        component.set("v.samsungAttendees",sList);
    },
    
    addNewDefaultSamsungAttendee:function(component,event){
        var sList=component.get("v.samsungAttendees");
        //var currentUser = '00536000002AuZ3AAK';
        var currentUser = $A.get("$SObjectType.CurrentUser.Id");;
        currentUser.toString();
        console.log('----'+currentUser+'----');
        sList.push({
            'sobjectType':'Call_Report_Samsung_Attendee__c',
            'Attendee__c':currentUser
        });
          console.log('ml1='+JSON.stringify(sList));
        component.set("v.samsungAttendees",sList);
    },
    
    addDistributionList:function(component,event){
        var sList=component.get("v.DistributionLists");
        sList.push({
            'sobjectType':'Call_Report_Distribution_List__c'
        });
          console.log('dl1='+JSON.stringify(sList));
        component.set("v.DistributionLists",sList);
    },
    //add new crDistributionList--12/03/2018
     addCRDistributionList:function(component,event){ 
        var sList=component.get("v.crDistributionLists");
        sList.push({
            'sobjectType':'CallReport_Distribution_List__c'
        });
          console.log('CRDL1='+JSON.stringify(sList));
        component.set("v.crDistributionLists",sList);
    },
    
    addNewNotificationMember:function(component,event){
        var nmList=component.get("v.notificationMembers");
        nmList.push({
            'sobjectType':'Call_Report_Notification_Member__c'
        });
          console.log('ml='+nmList);
        component.set("v.notificationMembers",nmList);
    },
    
    getTasks:function(component,event,callReport){
        var action= component.get("c.getTasks");
        action.setParams({
            "callReport":callReport
        });
         action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                component.set("v.tasks",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    saveTasks:function(component,event,callReport){
        var tasks = component.get("v.tasks");
        for (var indexVar = 0; indexVar < tasks.length; indexVar++) {
            if (tasks[indexVar].OwnerId!=undefined){
            	tasks[indexVar].OwnerId=tasks[indexVar].OwnerId.val;
            }
        }
        var action=component.get("c.saveTasks");
        action.setParams({
            "tasks":tasks,
            "callReport":callReport
        });

         action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
               //$A.get('e.force:refreshView').fire();
            	//this.GotoView(component,event,callReport.Id);
            	
            }
         });

        $A.enqueueAction(action);
  
    },
    
    
    getProductLine:function(component,event,callReport){
        //var action= component.get("c.getProductLine");
        var action= component.get("c.getProductLine");
        action.setParams({
            "callReport":callReport
        });
         action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                component.set("v.productCategories",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    saveProductCategory:function(component,event,callReport){
        var pcategory = component.get("v.productCategories");
          for (var indexVar = 0; indexVar < pcategory.length; indexVar++) {
            if (pcategory[indexVar].Sub_Account__c!=undefined){
            	pcategory[indexVar].Sub_Account__c=pcategory[indexVar].Sub_Account__c.val;
            }
        }
        var action=component.get("c.saveProductLine");
        action.setParams({
            "plines":pcategory,
            "callReport":callReport
        });

        action.setCallback(this, function(response){
            var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                //$A.get('e.force:refreshView').fire();
                //this.GotoView(component,event,callReport.Id);

            }
        });

        $A.enqueueAction(action);
    },
    
      getSubAccount:function(component,event,callReport){
        var action= component.get("c.getSubAccount");
        action.setParams({
            "callReport":callReport
        });
         action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                component.set("v.subAccount",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    
    saveSubAccount:function(component,event,callReport){
        var sa = component.get("v.subAccount");
        for (var indexVar = 0; indexVar < sa.length; indexVar++) {
            if (sa[indexVar].Sub_Account__c!=undefined){
            	sa[indexVar].Sub_Account__c=sa[indexVar].Sub_Account__c.val;
            }
        }
        
        var action=component.get("c.saveSubAccount");
        action.setParams({
            "subAcc":sa,
            "callReport":callReport
        });

        action.setCallback(this, function(response){
            var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                //$A.get('e.force:refreshView').fire();
                //this.GotoView(component,event,callReport.Id);

            }
        });

        $A.enqueueAction(action);
    },
    
    getMeetingAttendees:function(component,event,callReport){
        var action= component.get("c.getMeetingAttendees");
        action.setParams({
            "callReport":callReport
        });
         action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                component.set("v.meetingAttendees",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    saveMeetingAttendees:function(component,event,callReport){
        var mAttendees = component.get("v.meetingAttendees");
         for (var indexVar = 0; indexVar < mAttendees.length; indexVar++) {
             /*
             if (mAttendees[indexVar].Customer_Attendee__c!=undefined && mAttendees[indexVar].Customer_Attendee__c.val!='') {
            	mAttendees[indexVar].Customer_Attendee__c=mAttendees[indexVar].Customer_Attendee__c.val;
             }
             */
             if (mAttendees[indexVar].Customer_Attendee__c!=undefined && mAttendees[indexVar].Customer_Attendee__c.val!='') {
            	console.log('Save Meeting Attendees before assignment----->'+mAttendees[indexVar].Customer_Attendee__c);
                 var att = mAttendees[indexVar].Customer_Attendee__c;
                 var attstr = att.toString();
                 mAttendees[indexVar].Customer_Attendee__c=attstr;
                 console.log('Save Meeting Attendees after assignment----->'+mAttendees[indexVar].Customer_Attendee__c);
             }
        }
        var action=component.get("c.saveMeetingAttendees");
        action.setParams({
            "ma":mAttendees,
            "callReport":callReport
        });

        action.setCallback(this, function(response){
            var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                //$A.get('e.force:refreshView').fire();
                //this.GotoView(component,event,callReport.Id);
            }
        });

        $A.enqueueAction(action);
    },
    
    getDefaultSamsungAttendees:function(component,event){
        var action= component.get("c.getDefaultSamsungAttendees");
        action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('DefaultSamAttended='+ JSON.stringify(response.getReturnValue()));
                component.set("v.samsungAttendees",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
     
    getSamsungAttendees:function(component,event,callReport){
        var action= component.get("c.getSamsungAttendees");
        action.setParams({
            "callReport":callReport
        });
         action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('sattended='+ JSON.stringify(response.getReturnValue()));
                component.set("v.samsungAttendees",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
        
     saveSamsungAttendees:function(component,event,callReport){
        var sAttendees = component.get("v.samsungAttendees");
         console.log('sAttendees-------->'+JSON.stringify(sAttendees));
         for (var indexVar = 0; indexVar < sAttendees.length; indexVar++) {
             if (sAttendees[indexVar].Attendee__c!=undefined && sAttendees[indexVar].Attendee__c.val!='') {
            	console.log('Save Sam Attendees before assignment----->'+sAttendees[indexVar].Attendee__c);
                 var att = sAttendees[indexVar].Attendee__c;
                 var attstr = att.toString();
                 sAttendees[indexVar].Attendee__c=attstr;
                 console.log('Save Sam Attendees after assignment----->'+sAttendees[indexVar].Attendee__c);
             }
             
            }
        var action=component.get("c.saveSamsungAttendees");
        action.setParams({
            "sa":sAttendees,
            "callReport":callReport
        });

        action.setCallback(this, function(response){
            var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('Sam Attendees Save Success');
               //$A.get('e.force:refreshView').fire();
                //this.GotoView(component,event,callReport.Id);
            }else{
                console.log('Sam Attendees Save failed');
            }
        });

        $A.enqueueAction(action);
    },
    
    getDistributionLists:function(component,event,callReport){
        var action= component.get("c.getDistributionLists");
        var CallReportId = callReport.id;
        action.setParams({
            "callReport":callReport
        });
         action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                var result = response.getReturnValue();
                for (var indexVar = 0; indexVar < result.length; indexVar++) {
                    console.log('Distribution list Name--------->'+result[indexVar].Name);
                }
                component.set("v.DistributionLists",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    saveDistributionLists:function(component,event,callReport){
        var dLists = component.get("v.DistributionLists");
        var DlistIds = [];
        for (var indexVar = 0; indexVar < dLists.length; indexVar++) {
            if (dLists[indexVar].Call_Report__c.val!=undefined && dLists[indexVar].Call_Report__c.val!='') {
            	console.log('Distribution list call report val----------->'+dLists[indexVar].Call_Report__c.val);
            	dLists[indexVar].id=dLists[indexVar].Call_Report__c.val;
            	var dlid = dLists[indexVar].id;
            	DlistIds.push(dlid);
            	dLists[indexVar].Call_Report__c=callReport;
             }
        }
        console.log('DlistIds--------------------------------->'+DlistIds);
        var action=component.get("c.saveDistributionLists");
        action.setParams({
            "distList":DlistIds,
            "callReport":callReport
        });

        action.setCallback(this, function(response){
            var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                //$A.get('e.force:refreshView').fire();
                //this.GotoView(component,event,callReport.Id);
            }
        });

        $A.enqueueAction(action);
    },
    //get CR Distribution Lists--12/03/2018
    getCRDistributionLists:function(component,event,callReport){
        var action= component.get("c.getCRDistributionLists");
        action.setParams({
            "callReport":callReport
        });
         action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                component.set("v.crDistributionLists",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    //save CR Distribution Lists--12/03/2018
    saveCRDistributionLists:function(component,event,callReport){
        console.log('<---------------Save CR Dist Lists--------------->');
        var dLists = component.get("v.crDistributionLists");
        console.log('CR dLists-------->'+JSON.stringify(dLists));
         for (var indexVar = 0; indexVar < dLists.length; indexVar++) {
             if (dLists[indexVar].Call_Report_Distribution_List__c!=undefined && dLists[indexVar].Call_Report_Distribution_List__c.val!='') {
            	console.log('Save CR Dist Lists before assignment----->'+dLists[indexVar].Call_Report_Distribution_List__c);
                 var att = dLists[indexVar].Call_Report_Distribution_List__c;
                 var attstr = att.toString();
                 dLists[indexVar].Call_Report_Distribution_List__c=attstr;
                 console.log('Save CR Dist Lists after assignment----->'+dLists[indexVar].Call_Report_Distribution_List__c);
             }
        }
        var action=component.get("c.saveCRDistributionLists");
        action.setParams({
            "CRdistList":dLists,
            "callReport":callReport
        });
        action.setCallback(this, function(response){
            var state= response.getState();
        });
        $A.enqueueAction(action);
    },
    
    getNotificationMembers:function(component,event,callReport){
        var action= component.get("c.getCRnotificationMembers");
        action.setParams({
            "callReport":callReport
        });
         action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                component.set("v.notificationMembers",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    saveNotificationMembers:function(component,event,callReport){
        var nMembers = component.get("v.notificationMembers");
         for (var indexVar = 0; indexVar < nMembers.length; indexVar++) {
             /*
             if (nMembers[indexVar].Member__c!=undefined && nMembers[indexVar].Member__c.val!='') {
            	nMembers[indexVar].Member__c=nMembers[indexVar].Member__c.val;
             }
             */
             if (nMembers[indexVar].Member__c!=undefined && nMembers[indexVar].Member__c.val!='') {
            	console.log('Save Notify Mems before assignment----->'+nMembers[indexVar].Member__c);
                 var att = nMembers[indexVar].Member__c;
                 var attstr = att.toString();
                 nMembers[indexVar].Member__c=attstr;
                 console.log('Save Notify Mems after assignment----->'+nMembers[indexVar].Member__c);
             }
        }
        var action=component.get("c.saveCRnotificationMembers");
        action.setParams({
            "crnmList":nMembers,
            "callReport":callReport
        });
        action.setCallback(this, function(response){
            var state= response.getState();
        });
        $A.enqueueAction(action);
    },
    
	   removeRow : function(component, event, relatedListName){
        var target = event.getSource();
        var rowIndex = target.get("v.value"); 
        var AllRowsList = component.get("v."+relatedListName);

        var deleteDlistIds = [];//Added for Collecting Distribution Lists
        // if record exists
        if (AllRowsList[rowIndex]!=undefined && AllRowsList[rowIndex].Id!=undefined) { 
            var deleteList=component.get("v.delete-"+relatedListName);
            deleteList.push(AllRowsList[rowIndex]);
            
            //below if logic to delete distribution lists based on its name
            if(relatedListName=='DistributionLists'){
                var dlid = AllRowsList[rowIndex].Name;
                deleteDlistIds.push(dlid);
                component.set("v.delete-DistributionListsIds", deleteDlistIds);
                console.log('Delete DlistIds---------------->'+deleteDlistIds);
            }
        }
        AllRowsList.splice(rowIndex, 1);
        component.set("v."+relatedListName, AllRowsList);
    },
    
    showToast : function(component, event, type,message,duration) {
         var toastEvent = $A.get("e.force:showToast");
        if(toastEvent!=undefined){
            toastEvent.setParams({
                "type":type,
                "message": message,
                "duration": duration

            });
            toastEvent.fire();
        } else {
            alert(message);
        }
    },
    
    getInstanceURL:function(component,event){
        var action= component.get("c.getInstanceUrl");

        action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                component.set("v.InstanceUrl",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    getParameterByName : function(cmp,event,name) {
       var url = window.location.href;
       name = name.replace(/[\[\]]/g, "\\$&");
       var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
       results = regex.exec(url);
       if (!results) return null;
       if (!results[2]) return '';
       return decodeURIComponent(results[2].replace(/\+/g, " "));
    },
    
    fetchPickListVal: function(component,objectName,fieldName, elementId, optionList) {
        var action = component.get("c.getselectOptions");

        action.setParams({
            "objObject": objectName,
            "fld": fieldName
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();

                if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        label: "--- None ---",
                        value: ""
                    });
                }
                for (var i = 0; i < allValues.length; i++) {
                    if(fieldName=='Status__c'){
                        if(allValues[i]=='Submitted' || allValues[i]=='Cancelled') continue;
                    }
                    opts.push({
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                
                //console.log('opts='+JSON.stringify(opts));
                //component.find(elementId).set("v.options", opts);
                component.set("v."+optionList, opts);
            }
        });
        $A.enqueueAction(action);
    },
    
    fetchPickListValDivision: function(component,objectName,fieldName, elementId, optionList) {
        var action = component.get("c.getOptionsCallReportDivision");

        action.setParams({
            "objObject": objectName,
            "fld": fieldName,
            "objApiName":'Call_Report__c',
            "contrfieldApiName":'Division__c',
            "depfieldApiName":'Meeting_Topics__c',
            "division":'B2B'
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();

                if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        label: "--- None ---",
                        value: ""
                    });
                }
                for (var i = 0; i < allValues.length; i++) {
                    if(fieldName=='Status__c'){
                        if(allValues[i]=='Submitted' || allValues[i]=='Cancelled') continue;
                    }
                    opts.push({
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                
                //console.log('opts='+JSON.stringify(opts));
                //component.find(elementId).set("v.options", opts);
                component.set("v."+optionList, opts);
            }
        });
        $A.enqueueAction(action);
    },
    
     fetchDependPickList: function(component,objectApiName,contrfieldApiName,depfieldApiName, optionList) {
        var action = component.get("c.getDependentOptionsImpl");

        action.setParams({
            'objApiName': objectApiName,
            'contrfieldApiName': contrfieldApiName,
            'depfieldApiName': depfieldApiName
        });
        action.setStorable();
         
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allDepList = [];
                var depMap = response.getReturnValue();
                console.log('dep map1='+JSON.stringify(depMap));
                for (var key in depMap) {
                    var depList = depMap[key];
                    var depObjList =[];
                    for (var d in depList) {
                        depObjList.push({text:depList[d],value:depList[d]});
                    }
                    allDepList.push({dependentList:depObjList, parent:key});
                }
                console.log('all dep list='+JSON.stringify(allDepList));
                component.set("v."+optionList, allDepList);
            }
        });
        $A.enqueueAction(action);
    },
    fetchDependPickListforB2B: function(component,objectApiName,contrfieldApiName,depfieldApiName, optionList) {
        var action = component.get("c.getDependentOptionsImplB2B");

        action.setParams({
            'objApiName': objectApiName,
            'contrfieldApiName': contrfieldApiName,
            'depfieldApiName': depfieldApiName
        });
        action.setStorable();
         
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allDepList = [];
                var depMap = response.getReturnValue();
                console.log('dep map1='+JSON.stringify(depMap));
                for (var key in depMap) {
                    var depList = depMap[key];
                    var depObjList =[];
                    for (var d in depList) {
                        depObjList.push({text:depList[d],value:depList[d]});
                    }
                    allDepList.push({dependentList:depObjList, parent:key});
                }
                console.log('all dep list='+JSON.stringify(allDepList));
                component.set("v."+optionList, allDepList);
            }
        });
        $A.enqueueAction(action);
    },
    
    loadedFiles: function(component, event, helper,reportId){
    	// fire the CallReportLoadedEvt Lightning Event and pass the id
    	console.log('loaded event='+reportId);
        var childCmp = component.find("fileDisplay");
        // call the aura:method in the child component
        childCmp.loadData(reportId);
    },
    
    uploadFile: function(component, event, helper,reportId, callback){
    	console.log('upload file1='+reportId);
        var childCmp = component.find("fileUpload");
        // call the aura:method in the child component
        //childCmp.uploadFile(reportId);
        childCmp.uploadFile(reportId, callback);
    },
    deleteRecords: function(component, event){
        var deleteTasks=component.get("v.delete-tasks");
        var deletePc=component.get("v.delete-productCategories");
        var deleteMa=component.get("v.delete-meetingAttendees");
        var deleteSa=component.get("v.delete-samsungAttendees");
        var deleteSacc=component.get("v.delete-subAccount");
        var deleteNmems=component.get("v.delete-notificationMembers");
        var deleteCRdlists=component.get("v.delete-crDistributionLists");

        console.log('deleted tasks='+JSON.stringify(deleteTasks));
        if (deleteTasks!=null || deletePc!=null || deleteMa!=null || deleteSa!=null || deleteSacc!=null || deleteNmems!=null || deleteCRdlists!=null) {
            var action = component.get("c.deleteRecords");
            
		
            action.setParams({
                "tasks": deleteTasks,
                "pCategories": deletePc,
                "crAttendee":deleteMa,
                "crSamAttendee":deleteSa,
                "crSubAcc":deleteSacc,
                "crNotifyMems":deleteNmems,
                "crDistLists":deleteCRdlists
            });
            
            action.setCallback(this, function(response) {
                if (response.getState() == "SUCCESS") {
                    
                }
            });
            
            $A.enqueueAction(action);
        }
    	
    },
    
    deleteDistributionLists: function(component, event, callReport){
        var deleteDlist=component.get("v.delete-DistributionListsIds"); //Delete Distribution List
        
        console.log('deleted Dlists Names--------------------=>'+JSON.stringify(deleteDlist));
        if (deleteDlist!=null) {
            var action = component.get("c.DeleteDistributionLists");
            
            action.setParams({
                "DistListNames":deleteDlist,
                "CallRprt":callReport
            });
            
            action.setCallback(this, function(response) {
                if (response.getState() == "SUCCESS") {
                }
            });
            
            $A.enqueueAction(action);
        }
    },
    
    validateFields: function(component, event)
    {
        console.log('call report n1:');
        
        var callReportDetails = component.get("v.callreport");
        console.log('call report n1:----->'+JSON.stringify(callReportDetails));
        var isValid=true;
        var message='';
        
        console.log('call report name is:'+callReportDetails.Name);
        if(callReportDetails.Name==undefined || callReportDetails.Name=='' ){
            message = 'Name';
            //alert('Call Report Name is Missing');
           isValid=false;
        }
        
        if(callReportDetails.Status__c==undefined || callReportDetails.Status__c=='' ){
            if(message.length>0) message+=', ';
            message = 'Status';
            //alert('Call Report Name is Missing');
           isValid=false;
        }
        
         if(callReportDetails.Meeting_Date__c==undefined || callReportDetails.Meeting_Date__c=='' ){
             if(message.length>0) message+=', ';
            message += 'Meeting Date';
           isValid=false;
           //alert('Meeting Date is missing. Enter Meeting Date');
        }
        
        if(callReportDetails.Agenda__c==undefined || callReportDetails.Agenda__c=='' ){
            if(message.length>0) message+=', ';
            message += 'Agenda';
           isValid=false;
            //alert('Agenda is missing. Please enter Agenda');
        }
        
         
        if(callReportDetails.Meeting_Topics__c==undefined || callReportDetails.Meeting_Topics__c=='' ){
            if(message.length>0) message+=', ';
            message += 'Meeting Type';
           isValid=false;
            //alert('Select Meeting Type');
        }
        
         
        if(callReportDetails.Primary_Account__c==undefined || callReportDetails.Primary_Account__c=='' ){
            if(message.length>0) message+=', ';
            message += 'Primary Account';
           isValid=false;
            //alert('Select Primary Account');
        }
        
        
        var tasks= component.get("v.tasks");
        // remove tasks empty rows
        for(var t=tasks.length-1;t>=0;t--)
        {
            var task=tasks[t];
             if((task.Subject==undefined || task.Subject=='' || task.Subject==null) &&
                (task.OwnerId==undefined || task.OwnerId=='' || task.OwnerId==null) &&
                (task.ActivityDate==undefined || task.ActivityDate=='' || task.ActivityDate==null) &&
                (task.Description==undefined || task.Description=='' || task.Description==null))
                {
                	tasks.splice(t, 1);
                }          
            console.log('t is:'+t);
        }
        
        // validate tasks fields, all are mandatory
        var rows='';
        for(var t=0;t<tasks.length;t++)
        {
            var task=tasks[t];
            
            if((task.OwnerId==undefined || task.OwnerId=='' || task.OwnerId==null)||
               (task.Subject==undefined || task.Subject=='' || task.Subject==null) ||
               (task.ActivityDate==undefined || task.ActivityDate=='' || task.ActivityDate==null) ||
               (task.Description==undefined || task.Description=='' || task.Description==null))
            {
                if(rows.length>0) rows+=' & ';
                rows += (Number(t)+1);
                isValid=false;
            }
        }
        
        if(rows.length>0) {
            if(message.length>0) message+=', ';
            message += 'All fields for Action Items at row ' + rows;
        }
        component.set("v.tasks",tasks);

        
        
        
        var subAccounts= component.get("v.subAccount");
        // remove empty rows for subaccounts
        for(var idx=subAccounts.length-1;idx>=0;idx--)
        {
            var subAccount=subAccounts[idx];
            if((subAccount.Account_Type__c==undefined || subAccount.Account_Type__c=='' || subAccount.Account_Type__c==null) &&
               (subAccount.Sub_Account__c==undefined || subAccount.Sub_Account__c=='' || subAccount.Sub_Account__c==null))
            {
                subAccounts.splice(idx, 1);
            }
        }
        
        rows='';
        for(var idx=0;idx<subAccounts.length;idx++)
        {
            var subAccount=subAccounts[idx];
            if(subAccount.Account_Type__c!='')   {       
                if(subAccount.Sub_Account__c==undefined || subAccount.Sub_Account__c=='' || subAccount.Sub_Account__c==null){
                    if(rows.length>0) rows+=' & ';
                    rows += (Number(idx)+1);
                    isValid=false;
                }
            }
        }
        if(rows.length>0){
            if(message.length>0) message+=', ';
            message += 'Account Name for Additional Accounts at row ' +rows;
        }
        component.set("v.subAccount",subAccounts);
        
        
        var productCategories= component.get("v.productCategories");
        // remove empty product category rows
        for(var idx=productCategories.length-1;idx>=0;idx--)
        {
            var pc=productCategories[idx];
           if((pc.Division__c==undefined || pc.Division__c=='' || pc.Division__c==null) &&
               (pc.Product_Category__c==undefined || pc.Product_Category__c=='' || pc.Product_Category__c==null) &&
               (pc.Notes__c==undefined || pc.Notes__c=='' || pc.Notes__c==null))
            {
                productCategories.splice(idx, 1);
            }
        }
        
        // validate product category fileds, all are mandatory
        rows='';
        for(var idx=0;idx<productCategories.length;idx++)
        {
            var pc=productCategories[idx];
            if(pc.Division__c=='Smart Home') {
                if(pc.Notes__c==undefined || pc.Notes__c=='' || pc.Notes__c==null){
                    if(rows.length>0) rows+=' & ';
                    rows += (Number(idx)+1);
                    isValid=false;
                }
                    
            } else if ((pc.Division__c==undefined || pc.Division__c=='' || pc.Division__c==null) ||
               (pc.Product_Category__c==undefined || pc.Product_Category__c=='' || pc.Product_Category__c==null) ||
               (pc.Notes__c==undefined || pc.Notes__c=='' || pc.Notes__c==null))
            {
              	if(rows.length>0) rows+=' & ';
                rows += (Number(idx)+1);
                isValid=false;
            }
        }
        
        if(rows.length>0) {
            if(message.length>0) message+=', ';
            message += 'All fields for Competitive Intelligence at row ' +rows;
        }
        
        component.set("v.productCategories",productCategories);
        
        var meetingAttendees= component.get("v.meetingAttendees");
        // remove empty rows for meetimf attendees
        for(var idx=meetingAttendees.length-1;idx>=0;idx--)
        {
            var ma=meetingAttendees[idx];
            if((ma.Customer_Attendee__c==undefined || ma.Customer_Attendee__c=='' || ma.Customer_Attendee__c==null))
            {
                meetingAttendees.splice(idx, 1);
            }
        }
        component.set("v.meetingAttendees",meetingAttendees);
       
        var samsungAttendees= component.get("v.samsungAttendees");
        for(var idx=samsungAttendees.length-1;idx>=0;idx--)
        {
            var sa=samsungAttendees[idx];
            if((sa.Attendee__c==undefined || sa.Attendee__c=='' || sa.Attendee__c==null))
            {
                samsungAttendees.splice(idx, 1);
            }
        }
        component.set("v.samsungAttendees",samsungAttendees);
        
        
        if(!isValid) {
            this.showToast(component, event,'Error','Please complete: \n'+message,10000);
        }
        
        component.set("v.isValid",isValid);
        component.set("v.Message",message);
    },
    
    setMeetingSubType: function(component, event, callReport) {     
        var meetingTypeValue = callReport.Meeting_Topics__c; // get selected controller field value
        var action = component.get("c.getDependentOptionsImpl");

        action.setParams({
            'objApiName': 'Call_Report__c',
            'contrfieldApiName': 'Meeting_Topics__c',
            'depfieldApiName': 'Meeting_Sub_Type__c'
        });
        action.setStorable();
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allDepList = [];
                var depnedentMap = response.getReturnValue();
                console.log('depnedentMap='+JSON.stringify(depnedentMap));
                //var depnedentMap = component.get("v.MeetingSubTypeOptions");
                console.log('callReport.Meeting_Topics__c------>'+callReport.Meeting_Topics__c);
                if (meetingTypeValue != 'None') {
                    var ListOfDependentFields = depnedentMap[meetingTypeValue];
                    console.log('ListOfDependentFields------>'+ListOfDependentFields);
                    if(ListOfDependentFields.length > 0){                
                        var dependentFields = [];
                        dependentFields.push('None');
                        for (var i = 0; i < ListOfDependentFields.length; i++) {
                            dependentFields.push(ListOfDependentFields[i]);
                        }
                        console.log('dependentFields------>'+dependentFields);
                        component.set("v.MeetingSubType", dependentFields);
                    }else{
                        component.set("v.MeetingSubType", ['None']);
                        console.log('<------else MeetingSubType------>');
                    }  
                    
                } else {
                    component.set("v.MeetingSubType", ['None']);
        		}
            }
        });
        $A.enqueueAction(action);
    },

})