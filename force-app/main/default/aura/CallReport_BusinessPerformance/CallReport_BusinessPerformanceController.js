({
    doInit : function(component, event, helper) {
        /*
        component.set('v.columns', [
            {label: 'Subject', fieldName: 'Subject', type: 'text'},
            {label: 'Description', fieldName: 'Description', type: 'text'},
            {label: 'Action Item', fieldName: 'Action_Item__c', type: 'text'},
            {label: 'Assigned To', fieldName: 'OwnerId', type: 'reference'},
            {label: 'Due Date', fieldName: 'ActivityDate', type: 'date'}
        ]);*/
        
        var reportId=component.get("v.recordId");
        var action= component.get("c.getCallReport");
         action.setParams({
            "crId":reportId
        });
        console.log('Init:'+action);
        
        helper.getBusinessPerfBlocks(component,helper);
        
        action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('call report1='+ JSON.stringify(response.getReturnValue()));
                component.set("v.callreport",response.getReturnValue());
                
                var callReportDetails = component.get("v.callreport");
				
                if(reportId != undefined && reportId!=null){
                    if(callReportDetails.Product_Sub_Category__c!=null && callReportDetails.Product_Sub_Category__c!='' && callReportDetails.Product_Sub_Category__c!=undefined){
                    	helper.getSubCategories(component,event,callReportDetails);
                    }
                    helper.getMarketIntelligence(component,event,callReportDetails);
                    helper.getBizPerformance(component,event,callReportDetails); //Added on 08/06/2019
                }
            }
        });
        $A.enqueueAction(action);
    },
})