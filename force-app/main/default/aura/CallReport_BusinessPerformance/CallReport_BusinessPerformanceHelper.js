({
	getBusinessPerfBlocks:function(component,event){
        var bussPerfblockstring = $A.get("{!$Label.c.Call_Report_Market_Intelligence}");
        var bussPerfblocks = [];
        bussPerfblocks = bussPerfblockstring.split(',');
        component.set("v.businessPerfBlocks",bussPerfblocks);
        console.log('v.businessPerfBlocks--->'+component.get("v.businessPerfBlocks"));
    },
    
    getMarketIntelligence:function(component,event,callReport){
        var action= component.get("c.getMarketIntelligence");
        action.setParams({
            "callReport":callReport
        });
         action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('sattended='+ JSON.stringify(response.getReturnValue()));
                component.set("v.MarketIntelligenceLines",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    getSubCategories:function(component,event,callReport){
        var subCategory = callReport.Product_Sub_Category__c;
        var result = subCategory.split(";");
        var subCategoryString = '';
        for(var i=0;i<result.length;i++){
            if(subCategoryString=='') subCategoryString = result[i];
            else subCategoryString = subCategoryString+'; '+result[i];
        }
        component.set("v.subCategoryString",subCategoryString);
    },
    
    //get Business Performance ---- Added 08/06/2019
    getBizPerformance:function(component,event,callReport){
        var action= component.get("c.getBizPerformance");
        action.setParams({
            "callReport":callReport
        });
         action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('get Business Performance===>'+ JSON.stringify(response.getReturnValue()));
                component.set("v.bizPerformanceLines",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
})