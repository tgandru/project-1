({
	doInit : function(component, event, helper) {
		var recId=component.get("v.recordId");
         var action= component.get("c.SubmitdemoforApproval");
         action.setParams({
            "recordid":recId
        });
        action.setCallback(this, function(response){
            	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                
                component.set("v.pageMessage",response.getReturnValue());
                var response= response.getReturnValue();
                console.log('Response:: '+ response);
                if(response !="The Demo has been successfully submitted for approval."){
                     component.set("v.showerror",true);
                }
            }
        });
        $A.enqueueAction(action); 
	},
    
    closepopUp :function(component, event, helper){
          $A.get("e.force:closeQuickAction").fire();
          $A.get('e.force:refreshView').fire();
    }
})