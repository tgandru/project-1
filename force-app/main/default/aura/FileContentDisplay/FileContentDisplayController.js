({
	doInit : function(component, event, helper) {
        
        console.log('file display init1');
        var parentId = component.get("v.parentId");
        helper.loadFiles(component, event, helper, parentId);
	},
    
    loadData : function(component, event, helper) {
        //var parentId = event.getParam("parentId");
        var params = event.getParam('arguments');
        if (params) {
            var parentId = params.parentId;
            console.log("parentId: " + parentId);
             console.log('event received parent id1='+parentId);

            component.set("v.parentId",parentId);
            helper.loadFiles(component, event, helper, parentId);
        }
    },
    
    downloadFile : function (component, event, helper){
    	var id = event.target.getAttribute("data-id");       
         $A.get('e.lightning:openFiles').fire({
            recordIds: [id]
        });
       /* var target = event.getSource();
        var id = target.get("v.data-id"); */
    	//alert('Document ID:' +id);
		
    },
    //Function to delete File row--Added on 04/01/2019 by Thiru.
    deleteFile : function (component, event, helper){
    	helper.deleteFile(component,event);
    }
    
});