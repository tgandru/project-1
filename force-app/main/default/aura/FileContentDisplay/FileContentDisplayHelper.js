({
	loadFiles : function(component, event, helper, parentId) {
        console.log('file display init loadfiles ');
        var parentId = component.get("v.parentId");
        if (parentId!=undefined) {
             var actionFile = component.get("c.getFiles");
            actionFile.setParams({
                "parentId" : parentId
            });
            
            console.log('parent id='+parentId);
            actionFile.setCallback(this, function(rep){
                var attachments = rep.getReturnValue();
                component.set("v.files", attachments);
                console.log('files='+JSON.stringify(attachments));
            });
        
            $A.enqueueAction(actionFile);
        }
       
	},
    
    //Function to delete File row--Added on 04/04/2019 by Thiru.
    deleteFile : function (component, event, helper){
    	var target = event.getSource();
        var fileId = target.get("v.value"); 
        console.log('Delete File Id---->'+fileId);
        
        var action = component.get("c.deleteFiles");
        action.setParams({
            fileId: fileId
        });
        action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('Delete Success');
                var delFileName = response.getReturnValue();
                console.log('Deleted filename---->'+delFileName);
                
                var AllRowsList = component.get("v.files");
                
                var rowIndex=null;
                for (var indexVar = 0; indexVar < AllRowsList.length; indexVar++) {
                    if(AllRowsList[indexVar].Id!=undefined && AllRowsList[indexVar].Id!=''&& AllRowsList[indexVar].Id!=null){
                        var fId = AllRowsList[indexVar].Id;
                        if(fId==fileId){
                            rowIndex = indexVar;
                            console.log('Delete File row---->'+rowIndex);
                        }
                    }
                }
                if(rowIndex!=null){
                    AllRowsList.splice(rowIndex, 1);
                    component.set("v.files", AllRowsList);
                }
            }
        });

        $A.enqueueAction(action);
    }
})