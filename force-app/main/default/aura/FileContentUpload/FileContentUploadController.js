({
    
    doSave: function(component, event, helper) {
        if (component.find("fileId").get("v.files").length > 0) {
            helper.uploadHelper(component, event);
        } else {
            alert('Please Select a Valid File');
        }
    },
 
    handleFilesChange: function(component, event, helper) {
        var fileName = 'No File Selected..';        
        if (event.getSource().get("v.files").length > 0) {
            fileName ="";
            var fileInput = event.getSource().get("v.files");
            for (var idx=0;idx<fileInput.length;idx++) {
                if (idx>0) fileName+=", ";
            	fileName += fileInput[idx]['name'];
            }
        }
        component.set("v.fileName", fileName);
    },
    uploadFile : function(component, event, helper) {
        console.log("Starting Method-1 upload file -- 120-5");
        var si=component.find("fileId").get("v.files");
        var self = this;
        if(si!=null && component.find("fileId").get("v.files").length > 0) {
           console.log("Inside If Loop");
            var params = event.getParam('arguments');
            if (params) {
                var parentId = params.parentId;
                console.log("parentId: " + parentId);
    
                component.set("v.parentId",parentId);
                if (params.callback) {
                    var callback = params.callback;
                	component.set("v.callback",callback);
                }
                helper.uploadHelper(component, event);
            }
        } else {
            var params = event.getParam('arguments');
            if (params!=null && params.callback) {
                params.callback("no files");
            }
        }
    }
})