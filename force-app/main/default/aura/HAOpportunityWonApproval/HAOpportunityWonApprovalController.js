({
	doInit : function(component, event, helper) {
	     var recId = component.get("v.recordId");
        console.log('12::'+recId);
         	var action = component.get("c.submitForWonApproval");
            action.setParams({"OppId":recId});
    
            action.setCallback(this, function(response){
                console.log('response.getState:'+response.getState());
                var state = response.getState();
                if(state=="SUCCESS"){  
                    console.log('response---->'+JSON.stringify(response.getReturnValue()));
                    var msg = response.getReturnValue();
                    if(msg=='Success'){
                        //this.showToast(component, event,'success','The Opportunity has been successfully submitted for Win Approval.',6000);
                         var msg = 'The Opportunity has been successfully submitted for approval.';
                                component.set("v.pageMessage",msg);
                                component.set("v.showSuccessMessage",true);
                                component.set("v.showPageMessage",false);
                    }else{
                        component.set("v.pageMessage",msg);
                        component.set("v.showPageMessage",true);
                        component.set("v.showSuccessMessage",false);
                    }
                }            
            });
            $A.enqueueAction(action);
	},
    closepopUp :function(component, event, helper){
          $A.get("e.force:closeQuickAction").fire();
          $A.get('e.force:refreshView').fire();
    }
})