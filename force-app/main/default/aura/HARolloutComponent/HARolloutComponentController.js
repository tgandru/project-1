({
	doInit : function(component, event, helper) {
		 var recId=component.get("v.recordId");
         var action= component.get("c.getOpportunityInformation");

         action.setParams({
            "recordId":recId
        });
        action.setCallback(this, function(response){
            	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('Response:: '+ JSON.stringify(response.getReturnValue()));
                component.set("v.OppInfo",response.getReturnValue());
                var opp=response.getReturnValue();
                 component.set("v.rolloutplanId",opp.Roll_Out_Plan__c);
                 helper.getCategorylist(component, event);
                 helper.getROPData(component, event);
            }
            
        });
          $A.enqueueAction(action); 
	},
    
     scriptsLoaded : function(component, event, helper) {
		console.log('load successfully');
       
       // active/call select2 plugin function after load jQuery and select2 plugin successfully    
       $(".select2Class").select2({
           placeholder: "Select Product Category",
           value: "MICROWAVE"
       });
	},
    
    enableeditmode :function(component, event, helper) {
         component.set("v.disableeditmode",false);
          component.set("v.displayquantity",false);
         component.set("v.displayrevenue",false);
    },
    
    disableeditmode :function(component, event, helper) {
         component.set("v.disableeditmode",true);
         component.set("v.displayquantity",false);
         component.set("v.displayrevenue",false);
      
    },
    
    displayrevenue :function(component, event, helper) {
         component.set("v.disableeditmode",true);
         component.set("v.displayrevenue",true);
         component.set("v.displayquantity",false);
    },
     displayquantity :function(component, event, helper) {
         component.set("v.disableeditmode",true);
         component.set("v.displayrevenue",false);
         component.set("v.displayquantity",true);
    },
    
    ShowEditmodeWithPagiNation :function(component, event, helper) {
    var pageSize = component.get("v.pageSize");
    component.set("v.start",0);
    component.set("v.end",pageSize-1);
    var data= component.get("v.rolloutInfo");
    var paginationList = [];
    for(var i=0; i< pageSize; i++){
        paginationList.push(data[i]);
     }
     component.set("v.paginationList", paginationList);
    },
    
    first : function(component, event, helper){
        
          var data= component.get("v.rolloutInfo");
          var pageSize = component.get("v.pageSize");
          var paginationList = [];
           for(var i=0; i< pageSize; i++) {
             paginationList.push(data[i]);
           }
          component.set("v.paginationList", paginationList);
        
        },
    last : function(component, event, helper){
          var data= component.get("v.rolloutInfo");
          var pageSize = component.get("v.pageSize");
          var paginationList = [];
          var totalSize = data.length;
          for(var i=totalSize-pageSize+1; i< totalSize; i++){
             paginationList.push(data[i]);
          }
           component.set("v.paginationList", paginationList);
    },
    next : function(component, event, helper){
        var data= component.get("v.rolloutInfo");
        var end = component.get("v.end");
        var start = component.get("v.start");
        var pageSize = component.get("v.pageSize");
        var paginationList = [];
        var counter = 0;
        for(var i=end+1; i<end+pageSize+1; i++){
            if(data.length > end){
                paginationList.push(data[i]);
                counter ++ ;
            }
            start = start + counter;
            end = end + counter;
            component.set("v.start",start);
            component.set("v.end",end);
            component.set("v.paginationList", paginationList);
            
        }
        
        
    },
    previous : function(component, event, helper){
        var data= component.get("v.rolloutInfo");
        var end = component.get("v.end");
        var start = component.get("v.start");
        var pageSize = component.get("v.pageSize");
        var paginationList = [];
        var counter = 0;
        for(var i= start-pageSize; i < start ; i++){
            if(i > -1){
               paginationList.push(data[i]);
               counter ++; 
            }else{
                start++;
            }
        }
        
        start = start- counter;
		end = end - counter;
        component.set("v.start",start);
        component.set("v.end",end);
        component.set("v.paginationList", paginationList);
    }
    
    
    
})