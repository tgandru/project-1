({
	getCategorylist : function(component, event) {
		 var action = component.get("c.getCategoryList");
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                 var allValues = response.getReturnValue();
                component.set("v.category",response.getReturnValue());
            }      
        });
         $A.enqueueAction(action);
	},
    
    getROPData :function(component, event) {
         var recId=component.get("v.rolloutplanId");
         var action= component.get("c.getRolloutProductInfo");
         action.setParams({
            "recordId":recId
         });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                 console.log('ROP Response:: '+ JSON.stringify(response.getReturnValue()));
                 var allValues = response.getReturnValue();
                //component.set("v.rop",response.getReturnValue());
                var ropsize=allValues.length;
               
                for(var i=0; i < 4; i++){
                     var ropids=[];
                    for(var x=0; x<200;x++){
                        var val=x+(i*200);
                        if(val < ropsize){
                            ropids.push(allValues[val].rop.Id)
                        }
                    }
                    
                    if(ropids != undefined && ropids.lenght !=0){
                        this.getROSData2(component, event,ropids);
                    }
                }
                //this.getROSData(component, event);
            }      
        });
         $A.enqueueAction(action);
	},
    
     getROSData2 :function(component, event,ropids) {
       var recId=component.get("v.recordId");
         var action= component.get("c.getROSInformationHelper");
         action.setParams({
            "ROPids":ropids
         });
         action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                console.log('ROS Response:: '+ JSON.stringify(response.getReturnValue()));
                 var allValues = response.getReturnValue();
                var mnthlist = [];
                if(allValues !=undefined && allValues.length >=1){
                 
                        var mnths=allValues[0].monthdata;
                    for(var i=0; i < mnths.length; i++){
                        mnthlist.push(mnths[i].yearMonth);
                    }
                      var existed=component.get("v.rolloutInfo");
                     //console.log('existed::'+existed.length);
                     //existed.push.apply(existed,response.getReturnValue());
                    component.set("v.rolloutInfo",response.getReturnValue());
                 component.set("v.months",mnthlist);
                }
                
                  
               
            }      
        });
         $A.enqueueAction(action);
        
    }
})