({
	doInit : function(component, event, helper) {
        var oppId = component.get("v.recordId");
        
        var qualifiedRoles = {
            "Distributor" : ["Distributor"],
            "Reseller": ["Corporate Reseller","Dealer","Property Developer","Property Management","System Integrator","VAR","Reseller"]
        };
        
        var roles = [];
        for( var qualification in qualifiedRoles ) {
            qualifiedRoles[qualification].forEach(
                function( role ) {
                    roles.push( role );
                }
            ); 
        }
        console.log('Roles------->'+roles);
        var rolesToCheck = "('" + roles.join( "','" ) + "')";
        
        var action = component.get("c.getPartnerList");
        action.setParams({"OppId" : oppId, "rolesStr":rolesToCheck});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state=="SUCCESS"){
                console.log('response.getReturnValue()====>'+response.getReturnValue());
                if(response.getReturnValue()!=null && response.getReturnValue()!=undefined){
                    var distCounter = 0;
                    var resellCounter = 0;
                    var partners = response.getReturnValue();
                    partners.forEach(
                        function( partner ) {
                            if( qualifiedRoles.Distributor.indexOf( partner.Role__c ) !== -1 ) {
                                distCounter++;
                            }
                            if( qualifiedRoles.Reseller.indexOf( partner.Role__c ) !== -1 ) {
                                resellCounter++;
                            }
                        }
                    );
					if(distCounter >= 1 && resellCounter >= 1 ){
                        var url = '/apex/ChannelRelationships_Ltng?id='+oppId+'&retUrl='+oppId;
                        console.log('url-------->'+url);
                        window.location.href = url;
                    }
                    else{
                        var msg = 'Please add at least one Distributor and at least one Reseller to be able to add channel relationships';
                        component.set("v.showPageMessage",true);
                        component.set("v.pageMessage",msg);
                    }
                }
                else{
                    console.log('--------in else block---------');
                    var msg = 'Please add at least one Distributor and at least one Reseller to be able to add channel relationships';
                    component.set("v.showPageMessage",true);
                    component.set("v.pageMessage",msg);
                }
            }
        });
        $A.enqueueAction(action);
	},
    
    close : function (component, event) {
        component.set("v.showPageMessage",false);
        window.history.back();
    },
})