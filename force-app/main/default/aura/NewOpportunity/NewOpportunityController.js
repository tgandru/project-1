({
   doInit: function(component, event, helper) {
       var myPageRef = component.get("v.pageReference");
      
       var accId = myPageRef.state.additionalParams;
       if(accId !=undefined){
           accId = accId.substring(6, 21);
           if(accId.startsWith('001')){
               console.log('accId==:'+accId);
           }else{
                var base64Context = myPageRef.state.inContextOfRef;
                 if(base64Context != undefined){
                base64Context = base64Context.substring(2);
                   var addressableContext = JSON.parse(window.atob(base64Context));
                    console.log('addressableContext :: '+window.atob(base64Context));
                   accId = addressableContext.attributes.recordId;
              if(accId ==undefined){
                   var address=addressableContext.attributes.attributes.address;
                   console.log('address:: '+JSON.stringify(address));
                   var parts = address.split('&');
                   for (var i = 0; i < parts.length; i++) {
                         var nv = parts[i].split('=');
                       if(parts[i].includes('accid')){
                           var accurl=parts[i].replace("%2Fe%3F", "&");
                           accurl=accurl.replace("%3D", "=");
                           var idparts = accurl.split('&');
                           for(var j = 0; j < idparts.length; j++){
                               if(idparts[i].includes('accid')){
                                   accId=idparts[i].replace("accid=", "");
                               }
                           }
                       }
                   } 
               } 
           }
               
           }
           
           
       }else{
           var base64Context = myPageRef.state.inContextOfRef; 
            console.log('URL  :: '+base64Context);
           if(base64Context != undefined){
                base64Context = base64Context.substring(2);
                   var addressableContext = JSON.parse(window.atob(base64Context));
                    console.log('addressableContext :: '+window.atob(base64Context));
                   accId = addressableContext.attributes.recordId;
              if(accId ==undefined){
                   var address=addressableContext.attributes.attributes.address;
                   console.log('address:: '+JSON.stringify(address));
                   var parts = address.split('&');
                   for (var i = 0; i < parts.length; i++) {
                         var nv = parts[i].split('=');
                       if(parts[i].includes('accid')){
                           var accurl=parts[i].replace("%2Fe%3F", "&");
                           accurl=accurl.replace("%3D", "=");
                           var idparts = accurl.split('&');
                           for(var j = 0; j < idparts.length; j++){
                               if(idparts[i].includes('accid')){
                                   accId=idparts[i].replace("accid=", "");
                               }
                           }
                       }
                   } 
               } 
           }
           
       }
       
       
       
       
       
       var recordTypeId = myPageRef.state.recordTypeId;
       console.log('accid---->'+accId);
       console.log('recordTypeId---->'+recordTypeId);
       
       var action = component.get("c.getAccountInfo");
       action.setParams({"accId":accId});
       
       action.setCallback(this, function(response){
           console.log('response.getState:'+response.getState());
           var state = response.getState();
           if(state=="SUCCESS"){
               console.log('Opportunity Rec='+ JSON.stringify(response.getReturnValue()));
               var resp = response.getReturnValue();
			    
               var today = new Date();
               var todayDate = (today.getMonth()+1)+'/'+today.getDate()+'/'+today.getFullYear();
               
               var accName = '';
               accName = resp.Name+' - '+todayDate;
               
               if(resp.BillingState!=null && resp.BillingState!='' && resp.BillingState!=undefined){
                   accName = accName+' - '+resp.BillingState;
               }
               var oppStage = $A.get("{!$Label.c.OppStageIdentification}");
               var createRecordEvent = $A.get("e.force:createRecord");
               if(accId != undefined ){
                   if(accId.startsWith('001')){
                    createRecordEvent.setParams({
                       "entityApiName": "Opportunity",
                       "recordTypeId": recordTypeId,
                       "defaultFieldValues": {
                           'AccountId' : accId,
                           'Name':accName,
                           'StageName':oppStage
                       }
                   });   
                   }else{
                        createRecordEvent.setParams({
                       "entityApiName": "Opportunity",
                       "recordTypeId": recordTypeId,
                        "defaultFieldValues": {
                           'Name':'DO NOT CHANGE',
                           'StageName':oppStage
                       }
                   });                       
                   }
                   
               }else{
                   createRecordEvent.setParams({
                       "entityApiName": "Opportunity",
                       "recordTypeId": recordTypeId,
                        "defaultFieldValues": {
                           'Name':'DO NOT CHANGE',
                           'StageName':oppStage
                       }
                   });
               }
       		   createRecordEvent.fire();
           }
       });
       $A.enqueueAction(action);
       /*
       var createRecordEvent = $A.get("e.force:createRecord");
       if(accId.startsWith('001')){
           createRecordEvent.setParams({
               "entityApiName": "Opportunity",
               "recordTypeId": recordTypeId,
               "defaultFieldValues": {
                   'AccountId' : accId
               }
           });
       }else{
           createRecordEvent.setParams({
               "entityApiName": "Opportunity",
               "recordTypeId": recordTypeId
           });
       }
       createRecordEvent.fire();
       */
   }
})