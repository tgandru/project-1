({
	doInit : function(component, event, helper) {
		var recId=component.get("v.recordId");
         var action= component.get("c.getOppInformation");
         action.setParams({
            "oppid":recId
        });
        action.setCallback(this, function(response){
            	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('Response:: '+ JSON.stringify(response.getReturnValue()));
                component.set("v.Opp",response.getReturnValue());
                helper.getRelatedlists(component,event);
            }
        });
        
        $A.enqueueAction(action); 
	},
     // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
   },
    
 // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
      
    },
    
    startclone : function(component,event,helper){
        console.log('Save Controller');
        var recId=component.get("v.recordId");
         var related=component.get("v.RelatedList");
         var action= component.get("c.Saverecords");
         action.setParams({
            "oppid":recId,
            "relatedliststring":JSON.stringify(related)
        });
        action.setCallback(this, function(response){
            	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('Response:: '+ JSON.stringify(response.getReturnValue()));
                var newoppid=JSON.stringify(response.getReturnValue()).replace("\"","");
                if(newoppid.startsWith('006')){
                    console.log
                    var newlink='/apex/CustomRefreshPage?id='+newoppid;
                    newlink=newlink.replace("\"","");
                 var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                      "url": newlink
                    });
                    urlEvent.fire();
                  
                }else{
                    component.set("v.Showerror", true);
                    component.set("v.errormsg", newoppid);                    
                }
            }
        });
        
        $A.enqueueAction(action); 
    },
     cancelBtn : function(component, event, helper) {
        // Close the action panel
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
            $A.get('e.force:refreshView').fire();
    },
})