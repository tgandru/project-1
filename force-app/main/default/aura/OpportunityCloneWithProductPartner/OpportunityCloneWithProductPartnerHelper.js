({
	getRelatedlists :function(component,event){
         var recId=component.get("v.recordId");
         var action= component.get("c.getRelatedlistInformation");
         action.setParams({
            "oppid":recId
        });
        action.setCallback(this, function(response){
            	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('Response:: '+ JSON.stringify(response.getReturnValue()));
                component.set("v.RelatedList",response.getReturnValue());
            }
        });
        
        $A.enqueueAction(action); 
    },
})