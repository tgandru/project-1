({
    checkStage : function (component){
        
        var newStage = component.get('v.simpleRecord').StageName;
        var amount = component.get('v.simpleRecord').Amount;
        var oldStage = component.get('v.currentVal');
        var winStage = component.get('v.winStage');
        var threshold = component.get('v.threshold');
        
        if(oldStage != winStage && winStage.includes(newStage)){
            
                this.startConfetti(component);
           
        }
    },
    startConfetti : function(component){
        console.log('Start method');
        this.congrats(component);
        component.set('v.displayCanvas',true);
        
        var confettiSettings = { 
            target: 'confetti',
            max: 600,
            props: ['square','line','circle','triangle']
        };
        
        var confetti = new window.ConfettiGenerator(confettiSettings);
        confetti.render();
        
        this.stopConfetti(confetti,component);
    },
    stopConfetti : function(confetti,component){
        window.setTimeout(
            $A.getCallback(function() {
                confetti.clear();
                component.set('v.displayCanvas',false);
            }), 10000
        );
    },
    congrats : function(component) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type" : "success",  
            "message": "Congrats on your Opportunity Win!"
        });
      //  toastEvent.fire();
	}
})