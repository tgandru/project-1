({
	doInit : function(component, event, helper) {
        
		var opId = component.get("v.recordId");
        helper.getUItheme(component,event,helper);
        helper.fetchPickListVal(component, {'sobjectType':'Partner__c'},'Role__c', 'role','roleSelectOptions');
        helper.fetchPickListVal(component, {'sobjectType':'Partner__c'},'Role__c', 'searchRole','roleSearchOptions');
        
        var action = component.get("c.getOppDetails");
        action.setParams({"oppId":opId});

        action.setCallback(this, function(response){
            console.log('response.getState:'+response.getState());
            var state = response.getState();
            //console.log('Opportunity Rec='+ JSON.stringify(response.getReturnValue()));
            if(state=="SUCCESS"){
                component.set("v.oppty",response.getReturnValue());
                   
                console.log('opId---->'+opId);
                   
                if(opId!=null && opId!='' && opId!=undefined){
                    //helper.getSelectedPartnersList(component, event, helper, opId);
                    helper.getSelectedPartnersWrapper(component, event, opId);
                    helper.getChannelList(component,event,opId);
                    var Opportunityinfo = component.get("v.oppty");
                    var isclosedopp=Opportunityinfo.IsClosed;
                   component.set("v.IsOppClosed",isclosedopp);
                }
            }
            var opptyDetails = component.get("v.oppty");
            
        });
        $A.enqueueAction(action);
	},
    
    savePartners : function(component, event, helper){
        helper.savePartners(component, event);
    },
    
    searchPartners : function(component, event, helper){
        helper.searchPartners(component, event, helper);
    },
    
    addPartner : function(component, event, helper){
        helper.addPartner(component, event, helper);
    },
    
    removePartner : function(component, event, helper){
        helper.removePartner(component, event, helper);
    },
    
    Cancel : function (component, event) {
        window.history.back();
        //var oppty = component.get("v.oppty");
        //window.location.href = "/"+oppty.Id;
    },
    
    saveAndAddProducts : function (component, event, helper) {
        helper.savePartners(component, event);
        var isSave = component.get("v.saveSuccess");
        if(isSave){
            var oppty = component.get("v.oppty");
            window.location.href = "/apex/OpportunityProductEntry?addTo="+oppty.Id;
    	}
    },
    
    saveAndChannelRelationships : function (component, event, helper) {
        var oppty = component.get("v.oppty");
        window.location.href = "/apex/ChannelRelationships?id="+oppty.Id;
    },
    
    close : function (component, event) {
        component.set("v.showPageMessage",false);
    },
    
    closeModal : function (component, event) {
        component.set("v.openModal",false);
    },
    
    closeModalYes : function (component, event, helper) {
        component.set("v.openModal",false);
        helper.removePartnerAndItsChannels(component, event, helper);
    },
    
    showResellerRoles : function (component, event) {
        component.set("v.showResellerRoles",true);
    },
    
    hideResellerRoles : function (component, event) {
        component.set("v.showResellerRoles",false);
    },
    
    
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    }
    
})