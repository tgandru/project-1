({
    getSelectedPartnersList : function(component, event, helper, oppId) {
		var action = component.get("c.getSelectedPartners");
        action.setParams({"opptyId" : oppId});
        
        action.setCallback(this, function(response){
            var state = response.getState();
            //console.log('Selected Partners list---->'+JSON.stringify(response.getReturnValue()));
            if(state=="SUCCESS"){
                console.log('Selected Partners list SUCCESS---->');
                console.log('Selected Partners list---->'+JSON.stringify(response.getReturnValue()));
            	component.set("v.selectedPartnersList",response.getReturnValue());
                var resp = response.getReturnValue();
            	this.getSelectedPartnersWrapper(component,event,resp);
            }
        });
        $A.enqueueAction(action);
	},
    
    getSelectedPartnersWrapper : function(component, event, oppId) {
		var action = component.get("c.getSelectedPartnersWrapper2");
        action.setParams({"opptyId" : oppId});
        
        action.setCallback(this, function(response){
            
            var state = response.getState();
            if(state=="SUCCESS"){
                console.log('in SUCCESS get partners');
                var spwrapDetails = response.getReturnValue();
                var accntIds = component.get("v.selAccntIds");
                for (var indexVar = 0; indexVar < spwrapDetails.length; indexVar++) {
                    console.log('PartnerId---->'+spwrapDetails[indexVar].PartnerId);
                    if(spwrapDetails[indexVar].PartnerId!=null && spwrapDetails[indexVar].PartnerId!=undefined && spwrapDetails[indexVar].PartnerId!=''){
                        accntIds.push(spwrapDetails[indexVar].PartnerId);
                    }
                }
                console.log('accntIds=======>'+accntIds);
            	component.set("v.selectedPartners",response.getReturnValue());
                component.set("v.selAccntIds",accntIds);
            }
        });
        $A.enqueueAction(action);
	},
    
    getChannelList : function(component, event, oppId) {
		var action = component.get("c.getChannelList");
        action.setParams({
            "opptyId":oppId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('getChannelList---->'+JSON.stringify(response.getReturnValue()));
            var resp =response.getReturnValue();
            component.set("v.channelList",resp);
            var partnerIdset = [];
            if(resp !=undefined){
               for(var indexVar = 0; indexVar < resp.length; indexVar++){
                    //console.log('Channel Distributor====>'+resp[indexVar].Distributor__c);
                    //console.log('Channel Reseller=======>'+resp[indexVar].Reseller__c);
                    if(resp[indexVar].Distributor__c!=null && resp[indexVar].Distributor__c!=undefined) partnerIdset.push(resp[indexVar].Distributor__c);
                    if(resp[indexVar].Reseller__c!=null && resp[indexVar].Reseller__c!=undefined) partnerIdset.push(resp[indexVar].Reseller__c);
                }
                
            }
            
            component.set("v.channelPartnerIdset",partnerIdset);
            // this.getChannelMap(component,event,resp);
           
        });
        $A.enqueueAction(action);
	},
    
    getChannelMap : function(component, event, channelList) {
		var action = component.get("c.getChannelMap");
        action.setParams({
            "allChannelRelations":channelList
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('getChannelMap---->'+JSON.stringify(response.getReturnValue()));
            component.set("v.channelMap",response.getReturnValue());
        });
        $A.enqueueAction(action);
	},
    
    searchPartners : function(component, event, helper){
         component.set("v.StartShowSpinner",true);
        var searchStr = component.get("v.searchString");
        var role = component.get("v.searchRole");
        if(role==""||role==undefined){
            role=null;
        }
        
        if(role==null && searchStr==""){
            component.set("v.pageMessage",'Please enter Search Keyword and/or a Role for Partner search');
            component.set("v.showPageMessage",true);
        }
        else{
            component.set("v.showPageMessage",false);
            var oppty = component.get("v.oppty");
            
            console.log('role---->'+role);
            console.log('searchStr---->'+searchStr);
            //console.log('oppty---->'+oppty);
            
            var action = component.get("c.updateAvailableList");
            action.setParams({
                "searchString":searchStr,
                "role":role,
                "opp":oppty
            });
            action.setCallback(this, function(response){
                var state = response.getState();
                console.log('Available Partners---->'+JSON.stringify(response.getReturnValue()));
                if(state=="SUCCESS"){
                    component.set("v.availablePartners",response.getReturnValue());
                }
            });
            $A.enqueueAction(action);
        }
    },
    
    addPartner : function(component, event, helper){
        var target = event.getSource();
        var rowIndex = target.get("v.value");  
        var availPartners = component.get("v.availablePartners");
        if (availPartners[rowIndex]!=undefined) { 
            var oppty = component.get("v.oppty");
            
            var rec = availPartners[rowIndex];
        	console.log('rec----->'+JSON.stringify(rec));
        	console.log('Opp Id----->'+JSON.stringify(oppty.Id));
        	var pList=component.get("v.selectedPartners");	
            pList.push({
                'sobjectType':'selectedPartnerWrapper',
                'Name':rec.partnerAcct.Name,
                'PartnerId':rec.partnerAcct.Id,
                'Tier':rec.partnerAcct.RecordType.Name,
                'Role':rec.partnerAcct.Type,
                'Primary':false,
                'contactId':null
            });
            component.set("v.selectedPartners",pList);
            availPartners[rowIndex].isSelected = true;
            component.set("v.availablePartners",availPartners);
            
            var accntIds = component.get("v.selAccntIds");
            if(rec.partnerAcct.Id!=null && rec.partnerAcct.Id!=undefined && rec.partnerAcct.Id!=''){
                accntIds.push(rec.partnerAcct.Id);
            }
            console.log('selAccntIds----->'+accntIds);
            component.set("v.selAccntIds",accntIds);
        }
    },
    
    removePartner : function(component, event, helper){
        var target = event.getSource();
        var rowIndex = target.get("v.value");  
        var selPartners = component.get("v.selectedPartners");
        console.log('rowIndex----->'+rowIndex);
        console.log('selPartners[rowIndex]----->'+JSON.stringify(selPartners[rowIndex]));
        var oppty = component.get("v.oppty");

        if(selPartners.length==1 && oppty.StageName!='Identified' && oppty.StageName !='Drop'){
            component.set("v.pageMessage",'At least one Partner should be associated with an Opportunity and cannot be deleted');
            component.set("v.showPageMessage",true);
        }else if (selPartners[rowIndex]!=undefined && selPartners[rowIndex].PartnerRecId!=undefined && selPartners[rowIndex].PartnerRecId!=null) { 
            var selectedPartnerRow = rowIndex;
            component.set("v.selectedPartnerToDelete",selectedPartnerRow);
            var channelPartnerIds=component.get("v.channelPartnerIdset");
            var showModal = false;
            for (var indexVar = 0; indexVar < channelPartnerIds.length; indexVar++) {
                if(channelPartnerIds[indexVar]==selPartners[rowIndex].PartnerId){
                    showModal = true;
                }
            }
            if(showModal){
            	component.set("v.openModal",true);
            }else{
                var deleteList=component.get("v.deletePartnerList");
                deleteList.push(selPartners[rowIndex]);
                console.log('deleteList---->'+JSON.stringify(deleteList));
                component.set("v.deletePartnerList",deleteList);
                selPartners.splice(rowIndex, 1);
        		component.set("v.selectedPartners",selPartners);
                
                var searchStr = component.get("v.searchString");
                var role = component.get("v.searchRole");
                if((role!=null && role!="" && role!=undefined) || (searchStr!=null && searchStr!="" && searchStr!=undefined)){
                    this.searchPartners(component, event, helper)
                }
            }
        }else{
            var deleteList=component.get("v.deletePartnerList");
            deleteList.push(selPartners[rowIndex]);
            console.log('deleteList---->'+JSON.stringify(deleteList));
            component.set("v.deletePartnerList",deleteList);
            selPartners.splice(rowIndex, 1);
            component.set("v.selectedPartners",selPartners);
            
            var searchStr = component.get("v.searchString");
            var role = component.get("v.searchRole");
            if((role!=null && role!="" && role!=undefined) || (searchStr!=null && searchStr!="" && searchStr!=undefined)){
                this.searchPartners(component, event, helper)
            }
        } 
    },
    
    removePartnerAndItsChannels : function(component, event, helper){
        var rowIndex = component.get("v.selectedPartnerToDelete");  
        var selPartners = component.get("v.selectedPartners");
        console.log('rowIndex----->'+rowIndex);
        console.log('selPartners[rowIndex]----->'+JSON.stringify(selPartners[rowIndex]));
        var oppty = component.get("v.oppty");
        
        if (selPartners[rowIndex]!=undefined && selPartners[rowIndex].PartnerRecId!=undefined && selPartners[rowIndex].PartnerRecId!=null) { 
            var deleteList=component.get("v.deletePartnerList");
            deleteList.push(selPartners[rowIndex]);
            console.log('deleteList---->'+JSON.stringify(deleteList));
            component.set("v.deletePartnerList",deleteList);
            selPartners.splice(rowIndex, 1);
            component.set("v.selectedPartners",selPartners);
            
            var channelList = component.get("v.channelList");
            var action = component.get("c.removePartnerAndItsChannels");
            action.setParams({
                "spw":selPartners[rowIndex],
                "channelList":channelList
            });
            action.setCallback(this, function(response){
                var state = response.getState();
                console.log('Channels to delete---->'+JSON.stringify(response.getReturnValue()));
                var resp = response.getReturnValue();
                if(state=="SUCCESS"){
                    var chToDelete = component.get("v.channelsToDelete");
                    for(var indexVar = 0; indexVar < resp.length; indexVar++){
                        chToDelete.push(resp[indexVar]);
                    }
                    component.set("v.channelsToDelete",chToDelete);
                }
            });
            $A.enqueueAction(action);
        } 
    },
    
    savePartners : function(component, event){
        var target = event.getSource();
        var buttonValue = target.get("v.value");
        console.log('Button---->'+buttonValue);
        var channelRelationCheck = false;
        if(buttonValue=='saveAndChannelRelationships'){
            channelRelationCheck = true;
        }
        
        var selectedPartners = component.get("v.selectedPartners");
        var selectedPartnersList = component.get("v.selectedPartnersList");
        var selAccntIds = component.get("v.selAccntIds");
        var oppty = component.get("v.oppty");
        var delPartners = component.get("v.deletePartnerList");
        var channelsToDelete = component.get("v.channelsToDelete");;
        
		for (var indexVar = 0; indexVar < selectedPartners.length; indexVar++) {
            //console.log('Partner---->'+selectedPartners[indexVar].Name);
            //console.log('Role---->'+selectedPartners[indexVar].Role);
            //console.log('Contact Obj---->'+JSON.stringify(selectedPartners[indexVar].contactId));
            if (selectedPartners[indexVar].contactId!=undefined 
                && selectedPartners[indexVar].contactId!=null  
                && selectedPartners[indexVar].contactId.val!=null 
                && selectedPartners[indexVar].contactId.val!=undefined){
            	selectedPartners[indexVar].contactId=selectedPartners[indexVar].contactId.val;
            }
            console.log('Contact Id---->'+selectedPartners[indexVar].contactId);
        }
        
        var action = component.get("c.onSave");
        action.setParams({
            "opp": oppty,
            "selectedPartners": selectedPartners,
            "deletePartners": delPartners,
            "selectedPartnersList":selectedPartnersList,
            "selAccntIds":selAccntIds,
            "channelRelationCheck":channelRelationCheck,
            "channelsToDelete":channelsToDelete
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state=="SUCCESS"){
                console.log('Save Msg---->'+JSON.stringify(response.getReturnValue()));
                var saveMsg = response.getReturnValue();
                if(saveMsg=='Save Successfully'){
                    component.set("v.saveSuccess",true);
                    var url = '';
                    var theme = component.get("v.uiTheme");
                    console.log('User UI Theme==>'+theme);
                    if(buttonValue=='saveAndAddProducts'){
                        url = '/c/OpportunityProductEntryApp.app?recordId='+oppty.Id;
                        //Added by Joe Jung 2019.07.29 -- For Salesforce1 -- Start
                        if(theme=='Theme4t'){
                            // Salesforce1 navigation                 
                           sforce.one.navigateToURL(url,true);
                           console.log('<--------In sforce.one.navigateToSObject-------->');
                        } 
                        //Added by Joe Jung 2019.07.29 -- For Salesforce1 -- End
                        else {
                            console.log('url-------->'+url);
                            window.location.href = url;
                        }
                    }else if(buttonValue=='saveAndChannelRelationships'){
                        url = '/apex/ChannelRelationships_Ltng?id='+oppty.Id+'&retUrl='+oppty.Id;
                        //Added by Joe Jung 2019.07.29 -- For Salesforce1 -- Start
                        if(theme=='Theme4t'){
                            // Salesforce1 navigation                 
                           sforce.one.navigateToURL(url,true);
                           console.log('<--------In sforce.one.navigateToSObject-------->');
                        } 
                        //Added by Joe Jung 2019.07.29 -- For Salesforce1 -- End
                        else {
                            console.log('url-------->'+url);
                            window.location.href = url;
                        }
                    }else{
                        //Added by Joe Jung 2019.07.29 -- For Salesforce1 -- Start
                        if(theme=='Theme4t'){
                        //if ((typeof sforce != 'undefined') && sforce && (!!sforce.one)){
                            // Salesforce1 navigation  
                            console.log('<---In sforce.one.navigateToSObject--->');               
                            sforce.one.navigateToSObject(oppty.Id, 'detail');
                        } 
                        //Added by Joe Jung 2019.07.29 -- For Salesforce1 -- End
                        else {
                            url = '/'+oppty.Id;
                            console.log('Desktop url-------->'+url);
                            window.location.href = url;
                        } 
                    }
                }else{
                    component.set("v.pageMessage",saveMsg);
            		component.set("v.showPageMessage",true);
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    validate: function(component,event,helper) {
        
    },
    
    fetchPickListVal: function(component,objectName,fieldName, elementId, optionList) {
        var action = component.get("c.getselectOptions");

        action.setParams({
            "objObject": objectName,
            "fld": fieldName
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log('allValues:' + allValues);
                
                if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        label: "--- None ---",
                        value: ""
                    });
                }
                for (var i = 0; i < allValues.length; i++) {
                    if(fieldName=='Status__c'){
                        if(allValues[i]=='Submitted' || allValues[i]=='Cancelled') continue;
                    }
                    opts.push({
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                
                //console.log('opts='+JSON.stringify(opts));
                //component.find(elementId).set("v.options", opts);
                component.set("v."+optionList, opts);
            }
        });
        $A.enqueueAction(action);
    },
    
    getUItheme: function(component,event,helper){
        var action = component.get("c.getUIThemeDescription");
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var theme = response.getReturnValue();
                console.log('theme::::' + theme);
                component.set("v.uiTheme", theme);
            }
        });
        $A.enqueueAction(action);
    },
})