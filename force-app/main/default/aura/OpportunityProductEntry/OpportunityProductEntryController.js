({
	doinit : function(component, event, helper) {
        helper.getUItheme(component,event,helper);
        var recId=component.get("v.recordId");
        console.log('Opp Id---->'+recId);
         var action= component.get("c.getOppInformation");
         action.setParams({
            "Oppid":recId
        });
        action.setCallback(this, function(response){
            	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('Response:: '+ JSON.stringify(response.getReturnValue()));
                component.set("v.Opp",response.getReturnValue());
                var Opportunityinfo = component.get("v.Opp");
                var isclosedopp=Opportunityinfo.IsClosed;
                var division=Opportunityinfo.Division__c;
                if(division=='SBS'){
                    component.set("v.isServiceOpp",true);
                   }
                 component.set("v.IsOppClosed",isclosedopp);
                if(Opportunityinfo.ProductGroupTest__c !=undefined){
                    var group=Opportunityinfo.ProductGroupTest__c.split(';');
                     component.set("v.Pdgrouplist",group);
                    helper.getOLEs(component,event,Opportunityinfo);
                    helper.getInstanceURL(component,event);
                    helper.CheckUnitCostPermission(component,event);
                    helper.CheckAP1Permission(component,event);
                  
                }
             
            }
        });
        
        $A.enqueueAction(action); 
		
	},
     // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
   },
    
 // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
      
    },
    closeModel: function(component, event, helper) {
      // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
      component.set("v.confirmationneeded", false);
   },
    navigate:function(component,event){

window.history.back();
}  ,
    checkboxSelect: function(component, event, helper) {
        // on each checkbox selection update the selected record count 

          console.log('******');
        
        
        var selectedRec = event.getSource().get("v.value");
        var getSelectedNumber = component.get("v.selectedCount");
        if (selectedRec == true) {
            getSelectedNumber++;
        } else {
            getSelectedNumber--;
          
        }
        component.set("v.selectedCount", getSelectedNumber);

    },
    
    addSelectedRecords: function(component, event, helper){
         var allRecords = component.get("v.PBEList");
        var selectedRecords = [];
        var selectedItem = event.getSource();
        var index = selectedItem.get("v.value");
        var isServiceOpp=component.get("v.isServiceOpp");
        if(allRecords[index]!=undefined){
             selectedRecords.push(allRecords[index]);
        }
        
        helper.addOLIRecords(component, selectedRecords);
        if(isServiceOpp==false){
            allRecords.splice(index, 1);
         }
         component.set("v.PBEList", allRecords);
          var selected=0;
         component.set("v.selectedCount", selected);
       
    },
    
     removeRow: function(component, event, helper) {
        
        var OliList = component.get("v.OliList");
        var selectedItem = event.currentTarget;
        var index = selectedItem.dataset.record;
         if(OliList[index]!=undefined && OliList[index].oppLiId!=undefined){
             var deleterec=component.get("v.DeleteOliList");
            deleterec.push(OliList[index].oppLiId); 
              console.log(deleterec);
         }
        
        OliList.splice(index, 1);
         if(OliList !=null){
             component.set("v.OliList", OliList);
         }
        
          
    },
       removeAllRows: function(component, event, helper) {
        console.log('Remove all');
        var OliList = component.get("v.OliList");
          
           for(var j=0; j <= OliList.length; j++){
               
                if(OliList[j]!=undefined && OliList[j].oppLiId!=undefined){
                     var deleterec=component.get("v.DeleteOliList");
                    deleterec.push(OliList[j].oppLiId); 
                     
                 }
              
           }
           var emptylist=[];
             component.set("v.OliList", emptylist);
            var DeleteOliList = component.get("v.DeleteOliList");
           console.log('DeleteOliList::'+DeleteOliList.length);
        
          
    },
    getPBEInformation : function(component, event, helper) {
        helper.getPBEListSearch(component,event);
     },
     getPBEInformationFromEnter : function(component, event, helper) {
        console.log('Enter Button '+event.keyCode );
        if ( event.keyCode == 13 ){
            helper.getPBEListSearch(component,event);
        }
        
     },
    
    SaveandCalculate :function(component, event, helper){
         console.log('Validate And Save Controller');
           var OliList = component.get("v.OliList");
          component.set("v.redirectPage", false);
          var Opportunityinfo = component.get("v.Opp");
          helper.validateFields(component,event);  
        var isValid=component.get("v.isValid");
        if(isValid){
            var action= component.get("c.SaveLineItems");
         action.setParams({
            "lineitemstring":JSON.stringify(OliList),
             "opp":Opportunityinfo
        });
        action.setCallback(this, function(response){
            	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('Save Response:: '+ JSON.stringify(response.getReturnValue()));

                var returnval=response.getReturnValue();
                if (typeof returnval === 'Ok' || returnval === "Ok" || returnval == "Ok"){
                      component.set("v.showError", false);
                        helper.DeleteRecords(component, event);
                        helper.getOLEs(component,event,Opportunityinfo);
                        helper.refreshOppInformation(component, event);
                      
                }else{
                     component.set("v.showError", true);
                      component.set("v.ErrorMsg", response.getReturnValue());
                }
                     

            }else{
                var errors = response.getError(); 
                component.set("v.showError", true);
                component.set("v.ErrorMsg", errors[0].message);
            }
        });
             $A.enqueueAction(action); 
        }
    },
    
    validateDataAndSave :function(component, event, helper){
         console.log('Validate And Save Controller');
           var OliList = component.get("v.OliList");
          var Opportunityinfo = component.get("v.Opp");
        component.set("v.redirectPage", true);
          helper.validateFields(component,event);  
        var isValid=component.get("v.isValid");
        if(isValid){
            var action= component.get("c.SaveLineItems");
         action.setParams({
            "lineitemstring":JSON.stringify(OliList),
             "opp":Opportunityinfo
        });
        action.setCallback(this, function(response){
            var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('Save Response:: '+ JSON.stringify(response.getReturnValue()));

                var returnval=response.getReturnValue();
                if (typeof returnval === 'Ok' || returnval === "Ok" || returnval == "Ok"){
                      component.set("v.showError", false);
                       helper.DeleteRecords(component, event);
                       var isvalid=component.get("v.showError");
                       var recId=component.get("v.recordId");
                       var quoteID=component.get("v.QuoteId");

                       var baseURL = component.get("v.InstanceUrl");
                       //window.history.back();
                    	console.log('recordId::' + recId);
                    	console.log('quoteID::'+quoteID);
                    /*
                    if(quoteID != undefined && quoteID !=null && quoteID !='' && quoteID.startsWith('0Q0')){
                        var url = '/one/one.app#/alohaRedirect/apex/QuoteProductEdit?id='+recId+'&quoteId='+quoteID;
                            console.log('url-------->'+url);
                            window.location.href = url;
                    }else{
                        var url = '/'+recId;
                            console.log('url-------->'+url);
                            window.location.href = url;
                    }*/
                    /*
                    //Added by Joe Jung 2019.07.29 -- For Salesforce1 -- Start
                    var theme = component.get("v.uiTheme");
                    console.log('User UI Theme==>'+theme);
                    if (theme =='Theme4t'){
                        // Salesforce1 navigation                 
                        sforce.one.navigateToSObject(Opportunityinfo.Id, 'detail');
                    } 
                    //Added by Joe Jung 2019.07.29 -- For Salesforce1 -- End
                    else
                    */
                    var theme = component.get("v.uiTheme");
                    console.log('User UI Theme==>'+theme);
                    if(quoteID != undefined && quoteID !=null && quoteID !='' && quoteID.startsWith('0Q0')){
                        console.log('User UI Theme 2==>'+theme);
                        var url = '/one/one.app#/alohaRedirect/apex/QuoteProductEdit?id='+recId+'&quoteId='+quoteID;
                        if (theme =='Theme4t'){
                            // Salesforce1 navigation                 
                            sforce.one.navigateToURL(url,true);
                        } 
                        //Added by Joe Jung 2019.07.29 -- For Salesforce1 -- End
                        else {
                            console.log('url-------->'+url);
                            window.location.href = url;
                        }
                    }else{
                        var url = '/'+recId;
                        console.log('url==>'+url);
                        if (theme =='Theme4t'){
                            // Salesforce1 navigation                 
                            sforce.one.navigateToURL(url,true);
                            //helper.goToRecord(component, event, helper);
                            //window.location = url;
                            console.log('<----In sforce.one.navigateToSObject---->');
                        } 
                        //Added by Joe Jung 2019.07.29 -- For Salesforce1 -- End
                        else {
                            console.log('url-------->'+url);
                            window.location.href = url;
                        }
                    } 
                       // window.location = baseURL+"/"+recId;
                       //  var newlink="/"+recId;
                       //  var urlEvent = $A.get("e.force:navigateToURL");
                       //     urlEvent.setParams({
                       //       "url": newlink
                       //     });
                       //     urlEvent.fire();
                       
                }else{
                     component.set("v.showError", true);
                      component.set("v.ErrorMsg", response.getReturnValue());
                }
            }else{
                var errors = response.getError(); 
                component.set("v.showError", true);
                component.set("v.ErrorMsg", errors[0].message);
            }
        });
             $A.enqueueAction(action); 
        }
    },
    
       SaveAndAddPartners :function(component, event, helper){
         console.log('Validate And Save Controller');
           var OliList = component.get("v.OliList");
          var Opportunityinfo = component.get("v.Opp");
        component.set("v.redirectPage", true);
          helper.validateFields(component,event);  
        var isValid=component.get("v.isValid");
        if(isValid){
            var action= component.get("c.SaveLineItems");
         action.setParams({
            "lineitemstring":JSON.stringify(OliList),
             "opp":Opportunityinfo
        });
        action.setCallback(this, function(response){
            	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('Save Response:: '+ JSON.stringify(response.getReturnValue()));

                var returnval=response.getReturnValue();
                if (typeof returnval === 'Ok' || returnval === "Ok" || returnval == "Ok"){
                    component.set("v.showError", false);
                    helper.DeleteRecords(component, event);
                    var isvalid=component.get("v.showError");
                    var recId=component.get("v.recordId");
                    var baseURL = component.get("v.InstanceUrl");
                    var url = '/apex/OpportunityPartnerCreation_Ltng?id='+recId;
                    if ((typeof sforce != 'undefined') && sforce && (!!sforce.one)){
                        // Salesforce1 navigation                 
                        sforce.one.navigateToURL(url,true);
                    } 
                    //Added by Joe Jung 2019.07.29 -- For Salesforce1 -- End
                    else {
                        console.log('url-------->'+url);
                        window.location.href = url;
                    }
                }else{
                    component.set("v.showError", true);
                    component.set("v.ErrorMsg", response.getReturnValue());
                }
            }else{
                var errors = response.getError(); 
                component.set("v.showError", true);
                component.set("v.ErrorMsg", errors[0].message);
            }
        });
             $A.enqueueAction(action); 
        }
    },
    
    validateandSaveOnConfirmation :function(component, event, helper){
         console.log('Validate And Save Controller on Confirmation');
        component.set("v.confirmationneeded", false);
        component.set("v.isConfirmed", true);
           var OliList = component.get("v.OliList");
          var Opportunityinfo = component.get("v.Opp");
          helper.validateFields(component,event);  
        var isValid=component.get("v.isValid");
        if(isValid){
            var action= component.get("c.SaveLineItems");
         action.setParams({
            "lineitemstring":JSON.stringify(OliList),
             "opp":Opportunityinfo
        });
        action.setCallback(this, function(response){
            	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('Save Response:: '+ JSON.stringify(response.getReturnValue()));

                var returnval=response.getReturnValue();
                if (typeof returnval === 'Ok' || returnval === "Ok" || returnval == "Ok"){
                      component.set("v.showError", false);
                       helper.DeleteRecords(component, event);
                       var isvalid=component.get("v.showError");
                       var redirect2opp=component.get("v.redirectPage");
                    if(isvalid && redirect2opp){
                        var quoteID=component.get("v.QuoteId");
                            if(quoteID != undefined && quoteID !=null && quoteID !='' && quoteID.startsWith('0Q0')){
                            var newlink= '/one/one.app#/alohaRedirect/apex/QuoteProductEdit?id='+recId+'&quoteId='+quoteID;

                         var urlEvent = $A.get("e.force:navigateToURL");
                            urlEvent.setParams({
                              "url": newlink
                            });
                            urlEvent.fire();
                        }else{
                            var newlink='/'+recId;
                         var urlEvent = $A.get("e.force:navigateToURL");
                            urlEvent.setParams({
                              "url": newlink
                            });
                            urlEvent.fire();
                        }                    
                         
                    }else{
                        helper.getOLEs(component,event,Opportunityinfo);
                    }
                      
                       
                }else{
                     component.set("v.showError", true);
                      component.set("v.ErrorMsg", response.getReturnValue());
                }
                     

            }else{
                var errors = response.getError(); 
                component.set("v.showError", true);
                component.set("v.ErrorMsg", errors[0].message);
            }
        });
             $A.enqueueAction(action); 
        }
    },
    
    CalculateValues1 : function(component, event, helper) {
            var target = event.getSource();  
            var index = target.get("v.label") ;
            var OliList = component.get("v.OliList");
        console.log('index=='+index);
        if(OliList[index]!=undefined){
            var qty=OliList[index].quantity;
            var reqprice=OliList[index].requestedPrice;
            var listPrice=OliList[index].listPrice;
            var totalreqprice=qty*reqprice;
            var totallistprice=qty*listPrice;
            var totaldiscuount=totallistprice-totalreqprice;
            var discount= (1-(reqprice/listPrice))*100;
            
            OliList[index].totalPrice=totalreqprice.toFixed(2);
            OliList[index].totalDiscount=totaldiscuount.toFixed(0);
            OliList[index].discount=discount.toFixed(0);
                         
        }
         component.set("v.OliList", OliList);     
    },
      CalculateValues2 : function(component, event, helper) {
            var target = event.getSource();  
            var index = target.get("v.label") ;
            var OliList = component.get("v.OliList");
        console.log('index=='+index);
        if(OliList[index]!=undefined){
             var qty=OliList[index].quantity;
             var listPrice=OliList[index].listPrice;
             var discount= OliList[index].discount;
             var totallistprice=qty*listPrice;
            
             var reqprice=listPrice - (listPrice * (discount / 100));
           
            var totalreqprice=qty*reqprice;
            
            var totaldiscuount=totallistprice-totalreqprice;
            
            
            OliList[index].totalPrice=totalreqprice.toFixed(2);
            OliList[index].totalDiscount=totaldiscuount.toFixed(0);
            OliList[index].requestedPrice=reqprice.toFixed(2);
                         
        }
         component.set("v.OliList", OliList);     
    },
 
 
})