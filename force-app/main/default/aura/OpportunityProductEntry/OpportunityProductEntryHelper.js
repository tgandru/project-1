({
	/*goToRecord:function(component, event, helper)
    {
        var sobjectEvent=$A.get("e.force:navigateToSObject");
        console.log('sobjectEvent-------->'+sobjectEvent);
        sobjectEvent.setParams({
            "recordId": component.get("v.recordId")
        });
        sobjectEvent.fire();
    },  
    */
    getOLEs :function(component,event,Opportunityinfo){
        var action= component.get("c.getOliInformation");
        action.setParams({
            "opp":Opportunityinfo
        });
         action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
               
                component.set("v.OliList",response.getReturnValue());
                console.log('OLI Response:: '+ JSON.stringify(response.getReturnValue()));

            }
        });
        $A.enqueueAction(action);
    },
    
     CheckUnitCostPermission :function(component,event){
        var action= component.get("c.getHasunitcostPermission");
        
         action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                
                component.set("v.canseeunitcost",response.getReturnValue());
                 
            }
        });
        $A.enqueueAction(action);
    },
    
    CheckAP1Permission :function(component,event){
        var action= component.get("c.getHasmanageAp1Permission");
        
         action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                
                component.set("v.manageAp1",response.getReturnValue());
                 
            }
        });
        $A.enqueueAction(action);
    },
    
    
     getQLEs :function(component,event,Opportunityinfo){
        var action= component.get("c.getQliInformation");
        action.setParams({
            "opp":Opportunityinfo
        });
         action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
               
                component.set("v.QliList",response.getReturnValue());
                  console.log('QLI Response:: '+ JSON.stringify(response.getReturnValue()));

            }
        });
        $A.enqueueAction(action);
    },
    addOLIRecords : function(component, objectRecords){
        console.log(objectRecords);
        var OliList = component.get("v.OliList");
        var OppId=component.get("v.recordId");
        var isServiceOpp=component.get("v.isServiceOpp");
        var allRecords = component.get("v.PBEList");
        var existed =[];
        for(var j=0; j < OliList.length; j++){
            existed.push(OliList[j].sapMaterialId);
        }
            for(var i=0; i < objectRecords.length; i++){
                if(existed.includes(objectRecords[i].pdmodelcode) && isServiceOpp==false){
                    console.log('Dup Value');
                }else{
                     OliList.push({
                            'sobjectType': 'Object',
                             'pbEntryId':objectRecords[i].pbe.Id,
                               'quantity':1,
                                'alternative':false,
                                  'ap1':false,
                               'requestedPrice':objectRecords[i].invoice,
                               'listPrice':objectRecords[i].invoice,
                               'productGroup':objectRecords[i].pdgroup,
                               'productName':objectRecords[i].pdname,
                               'petName':objectRecords[i].pdpetname,
                               'sapMaterialId':objectRecords[i].pdmodelcode,
                               'productDescription':objectRecords[i].pddescription,
                               'productType':objectRecords[i].pdtype,
                               'unitCost':objectRecords[i].unitcost,
                               'unitCostReadOnly':objectRecords[i].unitCostReadOnly,
                          });  
                }
             
        }
         component.set("v.OliList", OliList);
    },
    
    getPBEListSearch : function(component,event){
        var Opportunityinfo = component.get("v.Opp");
        var group = component.get("v.SelectedPdgroup");
        var searchtext = component.get("v.searchstring");
        var isServiceOpp=component.get("v.isServiceOpp");
         var action= component.get("c.getPBEList");
         action.setParams({
            "selectedProdGroup":group,
             "searchString":searchtext,
             "opp":Opportunityinfo,
             
        });
         action.setCallback(this, function(response){
            	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('Response:: '+ JSON.stringify(response.getReturnValue()));
                 
                //component.set("v.PBEList",response.getReturnValue()); 
                 var pblist=response.getReturnValue();
                var OliList = component.get("v.OliList");
                var existed =[];
                var finalpbelist=[];
                for(var j=0; j < OliList.length; j++){
                    existed.push(OliList[j].sapMaterialId);
                }
                for(var i=0; i < pblist.length; i++){
                    if(existed.includes(pblist[i].pdmodelcode) && isServiceOpp==false){
                      console.log('Existed Value');
                    }else{
                        finalpbelist.push(pblist[i]);
                    }
                }
                component.set("v.PBEList",finalpbelist);
            }
        });
        
        $A.enqueueAction(action); 
    },
    
      validateFields: function(component, event)
    {
         var isValid=true;
        var msg=''; 
          var OliList = component.get("v.OliList");
           var isconfirmed = component.get("v.isConfirmed");
          component.set("v.showError", false);
        if(OliList.length <1){
            component.set("v.ErrorMsg", 'When an opportunity is qualified, it must have at least one opportunity line item. Please add at least one product to this opportunity and try again.');
            component.set("v.showError", true);
            isValid=false;
        }
           for(var j=0; j <= OliList.length; j++){
               
                if(OliList[j]!=undefined && OliList[j].qliid!=undefined){
                       var qliQty= OliList[j].qliquantity;
                       var qliprice= OliList[j].qliprice;
                       var oliQty= OliList[j].quantity;
                       var oliprice= OliList[j].requestedPrice;
                   
                   
                    if(oliQty < 1){
                       
                        component.set("v.ErrorMsg", 'A product\'s quantity can not be less than 1. If you\'d like 0 quantity of a product, please remove it.');
                         component.set("v.showError", true);
                         isValid=false;
                    }
                    
                     if ((qliQty > oliQty || qliprice > oliprice)&& !isconfirmed){
                         component.set("v.confirmationneeded", true);
                           isValid=false;
                    }
                    
                 }
               
               
               if(OliList[j]!=undefined ){
                     var oliQty= OliList[j].quantity;
                       var oliprice= OliList[j].requestedPrice;
                   
                   
                    if(oliQty < 1){
                       
                        component.set("v.ErrorMsg", 'A product\'s quantity can not be less than 1. If you\'d like 0 quantity of a product, please remove it.');
                         component.set("v.showError", true);
                         isValid=false;
                    }
                   
               }
              
           }
         component.set("v.isValid",isValid);
    },
    
     DeleteRecords : function(component, event) {
		var deletelist=component.get("v.DeleteOliList");
         console.log('deletelist::'+deletelist);
         var action= component.get("c.DeleteLineItems");
         action.setParams({
            "Deletelineitems":deletelist
        });
        action.setCallback(this, function(response){
            	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                 
                var returnval=response.getReturnValue();
                console.log(returnval);
                if (typeof returnval === 'Ok' || returnval === "Ok" || retrunval == "Ok"){
                  
                    var showmsg= component.get("v.showError");
                     component.set("v.showError", false);
                  
                }else{
                     component.set("v.showError", true);
                     component.set("v.ErrorMsg", returnval);
                }
                 
            }else{
                var errors = response.getError(); 
                component.set("v.showError", true);
                component.set("v.ErrorMsg", errors[0].message);
            }
        });
        $A.enqueueAction(action); 
	},
        getInstanceURL:function(component,event){
        var action= component.get("c.getInstanceUrl");

        action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                component.set("v.InstanceUrl",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    refreshOppInformation : function(component, event) {
         var recId=component.get("v.recordId");
         var action= component.get("c.getOppInformation");
         action.setParams({
            "Oppid":recId
        });
        
        action.setCallback(this, function(response){
            var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('Response:: '+ JSON.stringify(response.getReturnValue()));
                component.set("v.Opp",response.getReturnValue());
                var Opportunityinfo = component.get("v.Opp");
                var isclosedopp=Opportunityinfo.IsClosed;
                var division=Opportunityinfo.Division__c;
                if(division=='SBS'){
                    component.set("v.isServiceOpp",true);
                   }
                 component.set("v.IsOppClosed",isclosedopp);
                if(Opportunityinfo.ProductGroupTest__c !=undefined){
                    var group=Opportunityinfo.ProductGroupTest__c.split(';');
                    component.set("v.Pdgrouplist",group);
                }
            }
            
            
            });
        
        $A.enqueueAction(action); 
    },
    getUItheme: function(component,event,helper){
        var action = component.get("c.getUIThemeDescription");
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var theme = response.getReturnValue();
                console.log('theme::::' + theme);
                component.set("v.uiTheme", theme);
            }
        });
        $A.enqueueAction(action);
    },
})