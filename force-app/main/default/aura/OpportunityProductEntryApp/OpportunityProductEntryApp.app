<aura:application extends="force:slds" access="global">
    <aura:attribute name="recordId" type="String"/>
	<c:OpportunityProductEntry recordId="{!v.recordId}"/>
</aura:application>