({
	doInit : function(component, event, helper) {
		var recId=component.get("v.recordId");
         var action= component.get("c.getOppInformation");
         action.setParams({
            "Oppid":recId
        });
        var opts = [];
        action.setCallback(this, function(response){
            	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('Response:: '+ JSON.stringify(response.getReturnValue()));
                component.set("v.Opp",response.getReturnValue());
                var Opportunityinfo = component.get("v.Opp");
                var isclosedopp=Opportunityinfo.IsClosed;
                component.set("v.IsOppClosed",isclosedopp);
                var hasproducts=Opportunityinfo.HasOpportunityLineItem;
                component.set("v.hasproducts",hasproducts);
                
                var pdgroup=Opportunityinfo.ProductGroupTest__c.split(';');
                for (var i = 0; i < pdgroup.length; i++) {
                    opts.push({
                        label: pdgroup[i],
                        value: pdgroup[i]
                    });
                }
                 component.set("v.Pdgrouplist", opts);
                if(isclosedopp==false && hasproducts ==true){
                     component.set("v.showedit","true");
                }else{
                   component.set("v.showError","true");
                   component.set("v.ErrorMsg","Please make sure Opportuinty in Open Stage and has Line Items.");
                }
                helper.getContactroles(component,event);
                helper.getInstanceURL(component,event);
            }
        });
        
        $A.enqueueAction(action); 
	},
     showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
   },
    
 // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
      
    },
    
    saverecord : function(component,event,helper){
       var Opportunityinfo = component.get("v.Opp");
       var demoinfo = component.get("v.demo");
       
        demoinfo.AccountId=Opportunityinfo.AccountId;
        demoinfo.Name=Opportunityinfo.AccountId;
        demoinfo.OpportunityId=Opportunityinfo.Id;
        demoinfo.Status='Open';
        demoinfo.Division__c=Opportunityinfo.Division__c;
        demoinfo.Pricebook2Id=Opportunityinfo.Pricebook2Id;
         var toastEvent = $A.get('e.force:showToast');
        helper.validateData(component,event,demoinfo);
        var validationsucess=component.get("v.validationsucess");
        if(validationsucess){
           var createAction = component.get('c.SaveDemorecord');
        createAction.setParams({
            newrecord: demoinfo
        });
        
         createAction.setCallback(this, function(response) {           
            // Getting the state from response
            var state = response.getState();
             if(state === 'SUCCESS') {
                 var dataMap = response.getReturnValue();
                 console.log('Response:: '+ JSON.stringify(response.getReturnValue()));
                 if(dataMap.status=='success') {
                   var id=dataMap.Id;
                     console.log('id::'+id);
                     var rtnurl="/apex/DemoProductEntry?Id="+id;
                     console.log('rtnurl::'+rtnurl);
                     var urlEvent = $A.get("e.force:navigateToURL");
                        urlEvent.setParams({
                          "url": rtnurl,
                          "isredirect": "true"
                        });
                        urlEvent.fire();
                 }else{
                     component.set("v.showError","true");
                     component.set("v.ErrorMsg","Error Occuer while creating Demo Record. Error   :"+dataMap.message);  
                }
                 
             }
                                                              
         });
         $A.enqueueAction(createAction); 
        }else{
            
                    // component.set("v.showError","true");
                    // component.set("v.ErrorMsg","Please Select Demo Contact.");
        }        
         
    },
     cancelBtn : function(component, event, helper) {
        // Close the action panel
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    },
})