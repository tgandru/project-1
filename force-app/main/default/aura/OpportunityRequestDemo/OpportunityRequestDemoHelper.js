({
  getContactroles :function(component,event){
      var recId=component.get("v.recordId");
         var action= component.get("c.getOppContactRoles");
         action.setParams({
            "Oppid":recId
        });
         var opts = [];
         action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                  console.log('Response:: '+ JSON.stringify(response.getReturnValue()));
                  var allValues = response.getReturnValue();

                 if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        label: "--- None ---",
                        value: "--- None ---"
                    });
                 }else{
                   component.set("v.showedit","flase");
                     component.set("v.showError","true");
                   component.set("v.ErrorMsg","Please make sure Opportuinty in Open Stage and has Line Items and Contact Role.");
                 }
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        label: allValues[i].conactname,
                        value: allValues[i].contactid
                    });
                }
                component.set("v.contactroles", opts);
                
               }
        });
        $A.enqueueAction(action);
    },
     getInstanceURL:function(component,event){
        var action= component.get("c.getInstanceUrl");

        action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                component.set("v.InstanceUrl",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    CheckDemoContact :function (component,event,demoinfo){
      var democontactpresent=false;
         console.log('DemoCOntact ::'+demoinfo.CustomerAuthorizedById);
        if(demoinfo.CustomerAuthorizedById !=undefined && demoinfo.CustomerAuthorizedById !='--- None ---' && demoinfo.CustomerAuthorizedById !=null){
            democontactpresent=true;
        }
       component.set("v.democontactpresent",democontactpresent); 
    },
    
    
    validateData : function (component,event,demoinfo){
        var validatesucess=true;
        component.set("v.showError","false");
        component.set("v.ErrorMsg","");
        console.log('demoinfo::'+JSON.stringify(demoinfo));
        if(demoinfo.CustomerAuthorizedById !=undefined && demoinfo.CustomerAuthorizedById !='--- None ---' && demoinfo.CustomerAuthorizedById !=null){
        }else{
            validatesucess=false;
            component.set("v.showError","true");
            component.set("v.ErrorMsg","Please Select Demo Contact.");  
        }
         if(demoinfo.BusinessReason__c !=undefined  && demoinfo.BusinessReason__c !=null && demoinfo.BusinessReason__c !=''){
        }else{
            validatesucess=false;
            component.set("v.showError","true");
            component.set("v.ErrorMsg","Please Provide the Business Reason.");  
        } 
        
         if(demoinfo.RequestedShipmentDate__c !=undefined  && demoinfo.RequestedShipmentDate__c !=null && demoinfo.RequestedShipmentDate__c !='' ){
        }else{
            validatesucess=false;
            component.set("v.showError","true");
            component.set("v.ErrorMsg","Please Select Expected Delivery Date.");  
        } 
        
        if(demoinfo.EffectiveDate !=undefined  && demoinfo.EffectiveDate !=null && demoinfo.EffectiveDate !=''){
        }else{
            validatesucess=false;
            component.set("v.showError","true");
            component.set("v.ErrorMsg","Please Select Requested Start Date.");  
        }
         if(demoinfo.Product_Group__c !=undefined && demoinfo.Product_Group__c !='--- None ---' && demoinfo.Product_Group__c !=null && demoinfo.Product_Group__c !=''){
        }else{
            validatesucess=false;
            component.set("v.showError","true");
            component.set("v.ErrorMsg","Please Select Product Group.");  
        }
        if(demoinfo.Type !='' && demoinfo.Type != undefined && demoinfo.Type != null){
         }else{
            validatesucess=false;
            component.set("v.showError","true");
            component.set("v.ErrorMsg","Please Select Demo Type.");  
        }
        
    
        component.set("v.validationsucess",validatesucess);
    }
})