({
	doInit : function(component, event, helper) {
		var recId=component.get("v.recordId");
        var object =component.get("v.sObject");
        console.log('object = '+object);
        console.log('recId = '+recId);
          var action= component.get("c.getquestionnaireInfo");
         action.setParams({
             "ObjName":object,
            "recordId":recId
        });
        
         action.setCallback(this, function(response){
            var state= response.getState();
              console.log('state = '+state);
             if(component.isValid() && state=="SUCCESS"){
                  console.log('Response:: '+ JSON.stringify(response.getReturnValue()));
                component.set("v.questionnaire",response.getReturnValue());
             }      
         });
        $A.enqueueAction(action); 
	},
})