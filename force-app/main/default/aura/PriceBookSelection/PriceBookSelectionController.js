({
	doinit : function(component, event, helper) {
        var recId=component.get("v.recordId");
         var action= component.get("c.getOppInformation");
         action.setParams({
            "Oppid":recId
        });
        action.setCallback(this, function(response){
            	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('Response:: '+ JSON.stringify(response.getReturnValue()));
                component.set("v.Opp",response.getReturnValue());
                var Opportunityinfo = component.get("v.Opp");
               
                if(Opportunityinfo.Division__c !=undefined){
                   helper.getPriceBooks(component,event,Opportunityinfo);
                     helper.getInstanceURL(component,event);
                 }
             
            }
        });
        
        $A.enqueueAction(action); 
		
	},
    navigate:function(component,event){
        window.history.back();
    }  ,
    saveandContinue : function(component, event, helper) {
        var selectedPB = component.get("v.selectedValue");
        
        if(selectedPB!=undefined){
            var OpportunityInfo =component.get("v.Opp");
            console.log('selectedPB::'+selectedPB);
             var action= component.get("c.SaveOpportunityRecord");
         action.setParams({
            "Opp":OpportunityInfo,
             "pbid":selectedPB
        });
        action.setCallback(this, function(response){
            	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
            
                  var returnval=response.getReturnValue();
                    if (typeof returnval === 'Ok' || returnval === "Ok" || returnval == "Ok"){
                          console.log('Response:: '+ JSON.stringify(response.getReturnValue()));
                       var newlink=component.get("v.returnURL");
                         var recId=component.get("v.recordId");
                       
                        var baseURL = component.get("v.InstanceUrl");
                        window.location = baseURL+"/"+newlink;
                       
                }
            }
        });
        
        $A.enqueueAction(action); 
     
        }
    }
})