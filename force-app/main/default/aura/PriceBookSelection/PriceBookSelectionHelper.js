({
	  getPriceBooks :function(component,event,Opportunityinfo){
        var action= component.get("c.getAvilablePriceBooks");
        action.setParams({
            "opp":Opportunityinfo
        });
         action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
               
                component.set("v.PBlist",response.getReturnValue());
                console.log(JSON.stringify(response.getReturnValue()));

            }
        });
        $A.enqueueAction(action);
    },
    getInstanceURL:function(component,event){
        var action= component.get("c.getInstanceUrl");

        action.setCallback(this, function(response){
          	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                component.set("v.InstanceUrl",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
   
})