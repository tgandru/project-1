({
	doInit : function(component, event, helper) {
		var recId = component.get("v.recordId");
        var action = component.get("c.getOpportunity");
        action.setParams({"recordId":recId});

        action.setCallback(this, function(response){
            console.log('response.getState:'+response.getState());
            var state = response.getState();
            if(state=="SUCCESS"){
                component.set("v.oppty",response.getReturnValue());
                var oppDetails = response.getReturnValue();
                if(oppDetails.Deal_Registration_Approval__c=='Approved'){
                    var msg = 'Project Registration is already Approved for this Opportunity.';
                    component.set("v.pageMessage",msg);
            		component.set("v.showPageMessage",true);
                }else if(oppDetails.StageName=='Win'){
                    var msg = 'This Opportunity is closed. Cannot submit for Project Registration.';
                    component.set("v.pageMessage",msg);
            		component.set("v.showPageMessage",true);
                }else if(oppDetails.StageName=='Identified'){
                    var msg = 'Opportunity should be in any of Qualified, Proposal, Commit stages to submit Project Registration';
                    component.set("v.pageMessage",msg);
            		component.set("v.showPageMessage",true);
                }/*else if(oppDetails.Reason__c==null || oppDetails.Reason__c==''){
                    var msg = 'Please fill out the Reason field before submitting for Deal Registration Approval';
                    component.set("v.pageMessage",msg);
            		component.set("v.showPageMessage",true);
                } */else{
            		component.set("v.showPageMessage",false);
                    helper.submitForApproval(component,event,recId);
                }
            }            
        });
        $A.enqueueAction(action);
	},
})