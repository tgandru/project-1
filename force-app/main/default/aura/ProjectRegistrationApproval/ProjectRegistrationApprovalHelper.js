({
	submitForApproval : function(component,event,oppId) {
		var action = component.get("c.submitProjectRegistration");
        action.setParams({"OppId":oppId});

        action.setCallback(this, function(response){
            console.log('response.getState:'+response.getState());
            var state = response.getState();
            if(state=="SUCCESS"){  
                var msg = response.getReturnValue();
                if(msg=='Success'){
                    this.showToast(component, event,'success','The Opportunity has been successfully submitted for approval.');
                }else{
                    component.set("v.pageMessage",msg);
                    component.set("v.showPageMessage",true);
                }
            }            
        });
        $A.enqueueAction(action);
	},
    
    showToast : function(component, event, type,message,duration) {
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
         $A.get('e.force:refreshView').fire();
        var toastEvent = $A.get("e.force:showToast");
        if(toastEvent!=undefined){
            toastEvent.setParams({
                "type":type,
                "message": message,
                "duration": duration

            });
            toastEvent.fire();
        } else {
            alert(message);
        }
    }
})