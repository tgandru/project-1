({
	 doInit: function(component, event, helper) {
		 var myPageRef = component.get("v.pageReference");
         console.log('myPageRef::'+JSON.stringify(myPageRef));
          var recordTypeId = myPageRef.state.recordTypeId;
         var oppId;
         var base64Context = myPageRef.state.inContextOfRef; 
         
         if(base64Context != undefined){
             base64Context = base64Context.substring(2);
                   var addressableContext = JSON.parse(window.atob(base64Context));
                    console.log('addressableContext 1 :: '+window.atob(base64Context));
           			
                    oppId = addressableContext.attributes.recordId;
         }
                 
                 var action = component.get("c.getOpportunityInfo");
                 action.setParams({"oppId":oppId});
       
                 action.setCallback(this, function(response){
                     var state = response.getState();
                     if(state=="SUCCESS"){
                         var resp = response.getReturnValue();
                         console.log('resp 2::'+resp);
                         
                         
                         var createRecordEvent = $A.get("e.force:createRecord");
                         
                          if(oppId !=undefined){
                              if(oppId.startsWith('006')){
                                  createRecordEvent.setParams({
                                           "entityApiName": "SBQQ__Quote__c",
                                           "recordTypeId": recordTypeId,
                                           "defaultFieldValues": {
                                               'SBQQ__Opportunity2__c' : oppId,
                                               'SBQQ__Account__c':resp.AccountId
                                                   }
                                       }); 
                              }else{
                                   createRecordEvent.setParams({
                                               "entityApiName": "SBQQ__Quote__c",
                                               "recordTypeId": recordTypeId,
                                               "defaultFieldValues": {
                                                   
                                                       }
                                           });  
                                  
                              }
                                      
                           }else{
                                 createRecordEvent.setParams({
                                               "entityApiName": "SBQQ__Quote__c",
                                               "recordTypeId": recordTypeId,
                                               "defaultFieldValues": {
                                                   
                                                       }
                                           });  
                             }
                           createRecordEvent.fire();
                     }
                 });
                   $A.enqueueAction(action);                 
                  
            
                 
        

	}
})