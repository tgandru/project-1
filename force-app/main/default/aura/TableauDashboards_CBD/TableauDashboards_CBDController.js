({    
    navigateToDashboard : function(component,event,helper){
        console.log('<----Navigate To Web Tab---->');
        var selectedTab = event.currentTarget.id;
    	var pageRef = {
            type: "standard__navItemPage",
            attributes: {
                apiName: selectedTab
            }
        };
        var navService = component.find("navService");
        event.preventDefault();
        navService.navigate(pageRef);
	}
})