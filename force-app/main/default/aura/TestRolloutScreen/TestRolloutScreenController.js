({
	doInit : function(component, event, helper) {
		 var recId=component.get("v.recordId");
         var action= component.get("c.getOpportunityInformation");

         action.setParams({
            "recordId":recId
        });
        action.setCallback(this, function(response){
            	var state= response.getState();
            if(component.isValid() && state=="SUCCESS"){
                console.log('Response:: '+ JSON.stringify(response.getReturnValue()));
                component.set("v.OppInfo",response.getReturnValue());
                var opp=response.getReturnValue();
                 component.set("v.rolloutplanId",opp.Roll_Out_Plan__c);
                 var productfamily='MICROWAVE';
                 helper.getROPData(component, event);
                
            }
            
        });
          $A.enqueueAction(action); 
	},
    microwaveTab: function(component, event, helper) {
        helper.clearAll(component, event);
        //make fruits tab active and show tab data
        component.find("microwaveId").getElement().className = 'slds-tabs--scoped__item slds-active customClassForTab';
        component.find("microwaveDataId").getElement().className = 'slds-tabs--scoped__content slds-show customClassForTabData';
        var productfamily='MICROWAVE';
         helper.getROPData(component, event,productfamily);
    },
    laundryTab: function(component, event, helper) {
        helper.clearAll(component, event);
        //make vegetables tab active and show tab data
        component.find("laundryId").getElement().className = 'slds-tabs--scoped__item slds-active customClassForTab';
        component.find("laundryDataId").getElement().className = 'slds-tabs--scoped__content slds-show customClassForTabData';
        var productfamily='LAUNDRY';
         helper.getROPData(component, event,productfamily);
    },
    rangeTab: function(component, event, helper) {
        helper.clearAll(component, event);
        //make color tab active and show tab data
        component.find("rangeId").getElement().className = 'slds-tabs--scoped__item slds-active customClassForTab';
        component.find("rangeDataId").getElement().className = 'slds-tabs--scoped__content slds-show customClassForTabData';
        var productfamily='RANGE';
         helper.getROPData(component, event,productfamily);
    },
    
     enableeditmode :function(component, event, helper) {
         component.set("v.disableeditmode",false);
          component.set("v.displayquantity",false);
         component.set("v.displayrevenue",false);
         var data=component.get("v.rolloutInfo");
          component.set("v.rolloutInfo",data);
         component.set("v.displayquantity",true);
    },
    
    disableeditmode :function(component, event, helper) {
         component.set("v.disableeditmode",true);
         component.set("v.displayquantity",false);
         component.set("v.displayrevenue",false);
      
    },
    
    displayrevenue :function(component, event, helper) {
         component.set("v.disableeditmode",true);
         component.set("v.displayrevenue",true);
         component.set("v.displayquantity",false);
    },
     displayquantity :function(component, event, helper) {
         component.set("v.disableeditmode",true);
         component.set("v.displayrevenue",false);
         component.set("v.displayquantity",true);
    },
    
     first : function(component, event, helper){
        
          var data= component.get("v.rop");
          var pageSize = component.get("v.pageSize");
          var ropids=[];
           for(var i=0; i< pageSize; i++) {
               ropids.push(data[i].rop.Id);
              }
          helper.getROSData2(component, event,ropids);
          var start = 0;
          component.set("v.start",start);
          component.set("v.end",pageSize-1);
        
        },
    last : function(component, event, helper){
          var data= component.get("v.rop");
          var pageSize = component.get("v.pageSize");
          var paginationList = [];
          var totalSize = data.length;
          var ropids=[];
          for(var i=totalSize-pageSize+1; i< totalSize; i++){
             ropids.push(data[i].rop.Id);
          }
            helper.getROSData2(component, event,ropids);
    },
    next : function(component, event, helper){
        var data= component.get("v.rop");
        var end = component.get("v.end");
        var start = component.get("v.start");
        var pageSize = component.get("v.pageSize");
        var paginationList = [];
        var counter = 0;
         var ropids=[];
        for(var i=end+1; i<end+pageSize+1; i++){
            if(data.length > end){
                 ropids.push(data[i].rop.Id);
                counter ++ ;
            }
        }
            start = start + counter;
            end = end + counter;
        console.log('Click Next: Start Value='+start+'- End Value ='+end);
            component.set("v.start",start);
            component.set("v.end",end);
           helper.getROSData2(component, event,ropids);
            
        
        
        
    },
    previous : function(component, event, helper){
        var data= component.get("v.rop");
        var end = component.get("v.end");
        var start = component.get("v.start");
        var pageSize = component.get("v.pageSize");
        var paginationList = [];
        var counter = 0;
          var ropids=[];
        for(var i= start-pageSize; i < start ; i++){
            if(i > -1){
              ropids.push(data[i].rop.Id);
               counter ++; 
            }else{
                start++;
            }
        }
        
        start = start- counter;
		end = end - counter;
        component.set("v.start",start);
        component.set("v.end",end);
        helper.getROSData2(component, event,ropids);
    },showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
   },
    
 // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
      
    },
    scriptsLoaded : function(component, event, helper) {
		console.log('load successfully');
       
       // active/call select2 plugin function after load jQuery and select2 plugin successfully    
       $(".select2Class").select2({
           placeholder: "Select Product Category",
           value: "MICROWAVE"
       });
	},
    
})