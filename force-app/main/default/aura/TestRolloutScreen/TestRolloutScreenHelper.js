({
	clearAll: function(component, event) {
        // this method set all tabs to hide and inactive
        var getAllLI = document.getElementsByClassName("customClassForTab");
        var getAllDiv = document.getElementsByClassName("customClassForTabData");
        for (var i = 0; i < getAllLI.length; i++) {
            getAllLI[i].className = "slds-tabs--scoped__item slds-text-title--caps customClassForTab";
            getAllDiv[i].className = "slds-tabs--scoped__content slds-hide customClassForTabData";
        }
    },
    
     getROPData :function(component, event) {
         var recId=component.get("v.rolloutplanId");
         var action= component.get("c.getRolloutProductInfo");
         action.setParams({
            "recordId":recId,
           
         });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                 console.log('ROP Response:: '+ JSON.stringify(response.getReturnValue()));
                 var allValues = response.getReturnValue();
                 component.set("v.rop",response.getReturnValue());
                var ropsize=allValues.length;
                  var ropids=[];
                var pageSize = component.get("v.pageSize");
                 component.set("v.start",0);
                 component.set("v.end",pageSize-1);
                 component.set("v.totalSize", ropsize);
                if(ropsize>25){
                    for(var x=0;x<25;x++){
                        ropids.push(allValues[x].rop.Id);
                    }   
                }else{
                     for(var x=0;x<ropsize;x++){
                        ropids.push(allValues[x].rop.Id);
                    }  
                }
                if(ropids != undefined && ropids.lenght !=0){
                    this.getROSData2(component, event,ropids);
                 }
               
            }      
        });
         $A.enqueueAction(action);
	},
    
     getROSData2 :function(component, event,ropids) {
       var recId=component.get("v.recordId");
         var action= component.get("c.getROSInformationHelper");
         action.setParams({
            "ROPids":ropids
         });
         action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                console.log('ROS Response:: '+ JSON.stringify(response.getReturnValue()));
                 var allValues = response.getReturnValue();
                var mnthlist = [];
                if(allValues !=undefined && allValues.length >=1){
                 
                        var mnths=allValues[0].monthdata;
                    for(var i=0; i < mnths.length; i++){
                        mnthlist.push(mnths[i].yearMonth);
                    }
                      var existed=component.get("v.rolloutInfo");
                     //console.log('existed::'+existed.length);
                     //existed.push.apply(existed,response.getReturnValue());
                    component.set("v.rolloutInfo",response.getReturnValue());
                 component.set("v.months",mnthlist);
                }
                
                  
               
            }      
        });
         $A.enqueueAction(action);
        
    }

})