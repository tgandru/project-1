({
	doInit : function(component, event, helper) {
		var recId = component.get("v.recordId");
        var action = component.get("c.getOpportunity");
        action.setParams({"recordId":recId});

        action.setCallback(this, function(response){
            console.log('response.getState:'+response.getState());
            var state = response.getState();
            if(state=="SUCCESS"){
                component.set("v.oppty",response.getReturnValue());
                var oppDetails = response.getReturnValue();
                var str_init = oppDetails.Strategic_Initiatives__c;
                var competitor = oppDetails.Competitor__c;
                var quest1=oppDetails.How_is_the_technology_used_and_who_uses__c;
                var quest2=oppDetails.What_Samsung_or_third_party_applications__c;
                var quest3=oppDetails.Why_did_the_customer_choose_Samsung_ove__c;
                var quest4=oppDetails.What_was_the_customer_s_problem_and_how__c;
                var quest5=oppDetails.How_did_we_effectively_engage_the_custom__c;
				console.log('Strategic_Initiatives---->'+str_init);
                console.log('Opp Amount---->'+oppDetails.Amount);
                if(oppDetails.WinApprovalStatus__c=='Pending Approval'){
                    var msg = 'This Oppoortunity is currently in pending Win Approval.';
                    component.set("v.pageMessage",msg);
            		component.set("v.showPageMessage",true);
                }else if(oppDetails.StageName=='Win'){
                    var msg = 'Cannot submit Win Oppoortunity for Win Approval. Opportunity should be in "Commit" Stage to submit for Win Approval.';
                    component.set("v.pageMessage",msg);
            		component.set("v.showPageMessage",true);
                }else if(oppDetails.StageName!='Commit'){
                    var msg = 'Opportunity should be in "Commit" Stage to submit for Win Approval';
                    component.set("v.pageMessage",msg);
            		component.set("v.showPageMessage",true);
                }else if(str_init!=undefined){
                    if(str_init.includes("Competitive Replacement") && (competitor == '' || competitor == null || competitor == undefined)){
                        var msg = 'Competitor is missing. Select Competitor to process Win Approval';
                        component.set("v.pageMessage",msg);
                        component.set("v.showPageMessage",true);
                    }else if(oppDetails.Amount>=100000){
                        if(quest1==undefined || quest2==undefined || quest3==undefined || quest4==undefined || quest5==undefined){
                            var msg = 'Please Complete Win Note before going to Win Approval.';
                            component.set("v.pageMessage",msg);
                            component.set("v.showPageMessage",true);
                        }else{
                          component.set("v.showPageMessage",false);
                          helper.submitForApproval(component,event,oppDetails);  
                        }
                    }else{
                        component.set("v.showPageMessage",false);
                        helper.submitForApproval(component,event,oppDetails);
                    }
                }else if(oppDetails.Amount>=100000){
                   if((quest1==undefined || quest2==undefined || quest3==undefined || quest4==undefined || quest5==undefined) && (oppDetails.Division__c=='IT' || oppDetails.Division__c=='Mobile')){
                            var msg = 'Please Complete Win Note before going to Win Approval.';
                            component.set("v.pageMessage",msg);
                            component.set("v.showPageMessage",true);
                        }else{
                          component.set("v.showPageMessage",false);
                          helper.submitForApproval(component,event,oppDetails);  
                        }
                }else{
            		component.set("v.showPageMessage",false);
                    helper.submitForApproval(component,event,oppDetails);
                }
            }            
        });
        $A.enqueueAction(action);
	},
    
    submitForApproval : function (component, event, helper) {
        var oppty = component.get("v.oppty");
        if(oppty.How_is_the_technology_used_and_who_uses__c==null 
           || oppty.How_is_the_technology_used_and_who_uses__c=='' 
           || oppty.What_Samsung_or_third_party_applications__c==null 
           || oppty.What_Samsung_or_third_party_applications__c==''
           || oppty.Why_did_the_customer_choose_Samsung_ove__c==null 
           || oppty.Why_did_the_customer_choose_Samsung_ove__c==''
           || oppty.What_was_the_customer_s_problem_and_how__c==null 
           || oppty.What_was_the_customer_s_problem_and_how__c==''
           || oppty.How_did_we_effectively_engage_the_custom__c==null 
           || oppty.How_did_we_effectively_engage_the_custom__c==''){
            component.set("v.requiredFieldsMissing",true);
        }else{
            helper.submitForApproval(component,event,oppty);
        }
    },
    
    Cancel : function (component, event) {
        var closeActionPanel = $A.get("e.force:closeQuickAction");
        closeActionPanel.fire();
    },
     closepopUp :function(component, event, helper){
          $A.get("e.force:closeQuickAction").fire();
          $A.get('e.force:refreshView').fire();
    }
})