({
	submitForApproval : function(component,event,oppty) {
		var action = component.get("c.submitWinApproval");
        action.setParams({"opp":oppty});

        action.setCallback(this, function(response){
            console.log('response.getState:'+response.getState());
            var state = response.getState();
            if(state=="SUCCESS"){  
                console.log('response---->'+JSON.stringify(response.getReturnValue()));
                var msg = response.getReturnValue();
                if(msg=='Success'){
                    //this.showToast(component, event,'success','The Opportunity has been successfully submitted for Win Approval.',6000);
                     var msg = 'The Opportunity has been successfully submitted for Win Approval.';
                            component.set("v.pageMessage",msg);
                            component.set("v.showSuccessMessage",true);
                }else{
                    component.set("v.pageMessage",msg);
                    component.set("v.showPageMessage",true);
                    component.set("v.winNoteDisplay",false);
                }
            }            
        });
        $A.enqueueAction(action);
	},
    
    showToast : function(component, event, type,message,duration) {
        var closeActionPanel = $A.get("e.force:closeQuickAction");
        closeActionPanel.fire();
         $A.get('e.force:refreshView').fire();
        var toastEvent = $A.get("e.force:showToast");
        if(toastEvent!=undefined){
            toastEvent.setParams({
                "type":type,
                "message": message,
                "duration": duration

            });
            toastEvent.fire();
        } else {
            alert(message);
        }
    }
})