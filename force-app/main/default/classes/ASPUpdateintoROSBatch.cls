/*
Created By - Vijay 4/10/2018
Purpose  - Update the ASP data to respective rollout Schedules 
Tiggering Point - This Class will fire from SKU ASP object After trigger 
Parameters - To Run this batch we need to pass Set of SAP Material IDs 
         

*/
global class ASPUpdateintoROSBatch implements Database.Batchable<sObject>,Schedulable,database.stateful{
    
     set<string> skuids = new set<string>();
     global ASPUpdateintoROSBatch(set<string> skuids){
         system.debug('**********'+skuids);
         this.skuids=skuids;
     }


    global void execute(SchedulableContext SC) {
         database.executeBatch(new ASPUpdateintoROSBatch(skuids)) ;
    }
 
   global Database.QueryLocator start(Database.BatchableContext BC){
       return Database.getQueryLocator([select id,name,SAP_Material_Code__c,SalesPrice__c,Roll_Out_Plan__r.Opportunity__r.Stagename,(select id,ASP__c,year__c,fiscal_month__c from Roll_Out_Schedules__r	) 
                                   from Roll_Out_Product__c where SAP_Material_Code__c in :skuids and Opportunity_RecordType_Name__c='Mobile']);
   }

   global void execute(Database.BatchableContext BC, List<Roll_Out_Product__c> scope){
      List<Roll_Out_Schedule__c> updatelist =new list<Roll_Out_Schedule__c>();
      set<string> sku=new set<string>();
      Map<string,List<SKU_ASP__c>> skumap=new  Map<string,List<SKU_ASP__c>>();// MAP to hold all ASP values for one SKU 
      set<id> rpid=new set<id>();
      for(Roll_Out_Product__c rp:scope){
          rpid.add(rp.id);
      }
      //Get ASP values for SKUs
      for(SKU_ASP__c s:[select id,name,month__c,year__C,ASP_Value__c,SAP_Material_ID__c from SKU_ASP__c where SAP_Material_ID__c in :skuids order by Year__c,Month__c asc]){
           sku.add(s.SAP_Material_ID__c);
            if(skumap.containsKey(s.SAP_Material_ID__c)){
                    List<SKU_ASP__c> ol = skumap.get(s.SAP_Material_ID__c);
                            ol.add(s);
                           skumap.put(s.SAP_Material_ID__c, ol);
                }else{
                   skumap.put(s.SAP_Material_ID__c, new List<SKU_ASP__c> { s }); 
                }
                    
       }
    
      for(Roll_Out_Product__c rp:[select id,name,SAP_Material_Code__c,SalesPrice__c,Roll_Out_Plan__r.Opportunity__r.Stagename,(select id,ASP__c,year__c,fiscal_month__c from Roll_Out_Schedules__r	) 
                                   from Roll_Out_Product__c where id in :rpid]){
          
          List<SKU_ASP__c> asplst=skumap.get(rp.SAP_Material_Code__c);
        
          
          for(Roll_Out_Schedule__c ros: rp.Roll_Out_Schedules__r){
              system.debug('**** ASP List Size '+asplst.size());
              for(SKU_ASP__c a :asplst){//Update ROS ASP value based on custom object data
               /*  logic need to fallow is Update Future Rollout Schedules with current Month ASP value
                     later if same month present for that , then update with exact value
                  
               */    
                     Integer currentmonth=system.today().month();
                     integer currentyear=system.today().year();
                     Integer rosyear = integer.valueOf(ros.year__c);
                     Integer rosmnth = integer.valueOf(ros.fiscal_month__c);
                     Integer aspyear = integer.valueOf(a.year__c);
                     if((rp.Roll_Out_Plan__r.Opportunity__r.Stagename=='Win' || rp.Roll_Out_Plan__r.Opportunity__r.Stagename=='Drop') &&((currentyear>rosyear)|| (currentyear==rosyear&&currentmonth>rosmnth) )){
                         //For Closed Opp's we should not update ASP for Past Data.
                         system.debug('Past month rollout');
                     }else{
                        if(rosyear==aspyear&&rosmnth ==a.month__C){
                             System.debug('Same Month and Year ');
                             ros.ASP__c=a.ASP_Value__c;
                         }else if(rosyear==aspyear&&rosmnth > a.month__C){
                              System.debug('Same Year and ROS month is more than ASP. ');
                              ros.ASP__c=a.ASP_Value__c;
                         }else if(rosyear>aspyear){
                             ros.ASP__c=a.ASP_Value__c;
                         }
                     }
                     
                
              }
               updatelist.add(ros); 
          }
       
          
      } 
    
      if(updatelist.size()>0){
          update updatelist;
      }
    }

   global void finish(Database.BatchableContext BC){
   }
   
}