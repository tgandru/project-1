global class ASPUpdateintoROSBatchForMigration implements Database.Batchable<sObject>,Schedulable,database.stateful{
    global void execute(SchedulableContext SC) {
         database.executeBatch(new ASPUpdateintoROSBatchForMigration()) ;
    }
 
   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator([select id,Opportunity_RecordType_Name__c,Roll_Out_Plan__r.Opportunity__r.recordtype.NAme  from  Roll_Out_Product__c where Roll_Out_Plan__r.Opportunity__r.recordtype.NAme='Mobile'  ]);//Roll_Out_Plan__r.Opportunity__c='0063600000GGP0X'
   }

   global void execute(Database.BatchableContext BC, List<Roll_Out_Product__c> scope){
       List<Roll_Out_Schedule__c> updatelist =new list<Roll_Out_Schedule__c>();
        
      set<string> sku=new set<string>();
      Map<string,List<SKU_ASP__c>> skumap=new  Map<string,List<SKU_ASP__c>>();
      set<id> rpid=new set<id>();
       for(SKU_ASP__c s:[select id,name,month__c,year__C,ASP_Value__c,SAP_Material_ID__c from SKU_ASP__c order by Year__c,Month__c asc]){
           sku.add(s.SAP_Material_ID__c);
           
              
            if(skumap.containsKey(s.SAP_Material_ID__c)){
                    List<SKU_ASP__c> ol = skumap.get(s.SAP_Material_ID__c);
                            ol.add(s);
                           skumap.put(s.SAP_Material_ID__c, ol);
                }else{
                   skumap.put(s.SAP_Material_ID__c, new List<SKU_ASP__c> { s }); 
                }
                    
           
           
       }
       for(Roll_Out_Product__c r:scope){
           rpid.add(r.id);
           
       }
    
      for(Roll_Out_Product__c rp:[select id,name,SAP_Material_Code__c,SalesPrice__c,(select id,ASP__c,year__c,fiscal_month__c from Roll_Out_Schedules__r	) 
                                   from Roll_Out_Product__c where id in :rpid]){
          
         
          
          for(Roll_Out_Schedule__c ros: rp.Roll_Out_Schedules__r){
              List<SKU_ASP__c> asp=new List<SKU_ASP__c>();
                  asp=skumap.get(rp.SAP_Material_Code__c);
                                       if(asp!=null && !asp.isEmpty()){
                                           for(SKU_ASP__c a :asp){  
                                                 Integer rosyear = integer.valueOf(ros.year__c);
                                                 Integer rosmnth = integer.valueOf(ros.fiscal_month__c);
                                                 Integer aspyear = integer.valueOf(a.year__c);

                                                     if(rosyear==aspyear&&rosmnth ==a.month__C){
                                                             System.debug('Same Month and Year ');
                                                             ros.ASP__c=a.ASP_Value__c;
                                                         }else if(rosyear==aspyear&&rosmnth > a.month__C){
                                                              System.debug('Same Year and ROS month is more than ASP. ');
                                                              ros.ASP__c=a.ASP_Value__c;
                                                         }else if(rosyear>aspyear){
                                                             ros.ASP__c=a.ASP_Value__c;
                                                         }
                                                         
                                               
                                         }
                                       }else{
                                            ros.ASP__c=rp.SalesPrice__c;
                                       }
                                          
                                          
                                          if(ros.ASP__c==null){
                                              
                                               ros.ASP__c=rp.SalesPrice__c;
                                          }
                                          
                                         
                                           updatelist.add(ros);  
          }
          
      } 
    
      if(updatelist.size()>0){
          update updatelist;
      }
    }

   global void finish(Database.BatchableContext BC){
   }
   
}