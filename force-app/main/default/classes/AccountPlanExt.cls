public class AccountPlanExt{
    public Account_Planning__c theplan {get;set;}
    public Account_Profile__c theprofileinfo {get;set;}
    //public Account_Planning__c theplanid {get;set;}
    Public Account Acc{get;Set;}
    Public Account openactivites{get;Set;}
    public Opportunity[] top10opps{get;set;}
    public Opportunitylineitem[] opplineitems{get;set;}
    public List<Customer_Goals_Pain_Points__c> cgpp {get;set;}
    public List<Customer_Goals_Pain_Points__c> cgpp_update {get;set;}
    public Value_Statements__c[] val {get;set;}
    public Value_Statements__c[] vs_update {get;set;}
    //Public Contact[] Con {get;Set;}
    public AccountContactRole[] Primcon {get;set;}
    Public MAP<String,String> MAP_ContactWithBusinessArea;
    Public MAP<String,String> MAP_ContactWithSamsungcon;
    Public MAP<String,String> MAP_ContactWithDegree;
    public transient ActivityHistory[] Activities {get;set;}
    public string Acountid;
    public string planid{get;set;}
    public Sales_Goals__c[] Sales_goals{get;set;}
    public Product_Share__c[] MobileShare{get;set;}
    public Account_Profile__c[] aproflist{get;set;}

    public Accountplanext(ApexPages.StandardController controller) {
        //getting the Account plan id && Account ID
        /*theplanid = (Account_Planning__c)stdcon.getRecord();
        acountid = theplanid.Account__c;
        planid = theplanid.id;
        */
        MAP<string,Sales_Goals__c> MAP_Exting_Sales_goals = new Map<String,Sales_Goals__c>();
        SET<string> SG_product = new SET<string>{'SMART_PHONE','TABLET','KNOX','ACCESSORY','PC','SMART_SIGNAGE','PRINTER','MEMORY_BRAND','LCD_MONITOR','HOSPITALITY_DISPLAY'};
        planid = Apexpages.currentpage().getParameters().get('id');       
        Acountid = Apexpages.currentpage().getParameters().get('AccountId');
        Sales_goals = new List<Sales_Goals__c>();
        system.debug('account id is :' + Acountid);
        if(planid != Null && planid!= '')
        {
            thePlan = [select Id, name,createddate,Account__c,Strengths__c,Threats__c,Opportunities__c,Weaknesses__c,CY_Total_CL__c,CY_Total_IL__c,CY_LO__c,CY_LO_IL__c,CY_SAM__c,CY_SAM_IL__c,Total_Knox__c,PY_LO__c,PY_LO_IL__c,PY_SAM__c,PY_SAM_IL__c,PY_Total_CL__c,PY_Total_IL__c,CY_Other_IL__c,CY_Other_CL__c,PY_Other_IL__c,PY_Other_CL__c,PY_Total_Knox__c,(select id,Channel_Influencer__c,Commit__c,Distributor__c,open__c,Product__c,Renewal__c,Renewal_Date__c,TAM__c,Won__c from Sales_Goals__r) from Account_planning__c where Id = :planid];
            //thePlan = [select Id, name,createddate,Account__c,CY_Total_CL__c,CY_Total_IL__c,CY_LO__c,CY_LO_IL__c,CY_SAM__c,CY_SAM_IL__c,Total_Knox__c,PY_LO__c,PY_LO_IL__c,PY_SAM__c,PY_SAM_IL__c,PY_Total_CL__c,PY_Total_IL__c,CY_Other_IL__c,CY_Other_CL__c,PY_Other_IL__c,PY_Other_CL__c,PY_Total_Knox__c,(select id,Channel_Influencer__c,Commit__c,Distributor__c,open__c,Product__c,Renewal__c,Renewal_Date__c,TAM__c,Won__c from Sales_Goals__r) from Account_planning__c where Id = :planid];
            Acountid = thePlan.Account__c;
            system.debug('the is is -->' + Acountid);
            for(Sales_Goals__c sg:thePlan.Sales_Goals__r)
            {
                MAP_Exting_Sales_goals.put(sg.Product__c,sg);
            }
            
        }
        Else
        {
            thePlan = new Account_Planning__c();
            thePlan.Account__c = Acountid;  
        }
        
        //getting MDM partner and Carriers from Account Profile
        theprofileinfo = new Account_Profile__c();
        for(Account_Profile__c Apf: [select Id, name,Account__c, FiscalYear__c, MDMPartner__c,Carriers__c from Account_Profile__c where Account__c =:Acountid ORDER BY CreatedDate DESC limit 1])
        {
            theprofileinfo = Apf;
        }
        
        
        //Inserting the Customer goals section in wizard2 
        cgpp = new List<Customer_Goals_Pain_Points__c>();
        for(Customer_Goals_Pain_Points__c cust : [select id,Customer_Goal__c,Samsung_Solution__c,Timeline__c,Expected_Outcome__c from Customer_Goals_Pain_Points__c where Account_planning__c =:planid])
        {
            cgpp.add(cust);
        }
        if(cgpp.size()<=0)
        {
            cgpp.add(new Customer_Goals_Pain_Points__c(Account_planning__c = planid));
        }
        For(String sgprod:SG_product)
        {
            if(MAP_Exting_Sales_goals.Containskey(sgprod))
            {
                //Sales_goals.add(MAP_Exting_Sales_goals.Get(sgprod));
            }Else                
            {
                Sales_Goals__c sgp = new Sales_Goals__c();
                If(theplan.id != Null)
                {
                    sgp.Account_Planning__c = theplan.id;
                    sgp.Product__c =  SgProd;
                    sgp.Won__c=0;
                    sgp.commit__c=0;
                    sgp.Open__c= 0;
                    //Sales_goals.add(sgp);
                    MAP_Exting_Sales_goals.put(sgprod,sgp);
                }
                Else
                {
                    sgp.Product__c =  SgProd;
                    sgp.Won__c=0;
                    sgp.commit__c=0;
                    sgp.Open__c= 0;
                    //Sales_goals.add(sgp);
                    MAP_Exting_Sales_goals.put(sgprod,sgp);
                }                     
            }
        }
        
        //For inserting the Value Statements Section in wizard3
        Val = new List<Value_Statements__c>();
        For(Value_Statements__c vs:[Select id,Value_Statement__c,Relevancy__c,value__c,Differentiation__c from Value_Statements__c where Account_planning__c =: planid])
        {
            Val.add(VS);
        }
        IF(val.size()<=0)
        {
            Val.add(new Value_Statements__c(Account_planning__c = planid));
        }
        
        //value_statements__c vs = new value_statements__c(Account_planning__c = planid);
        //val.add(vs);
        
        //Getting the associated account information for this Account Plan
        //Acc = [select id, Name, lastmodifieddate, lastmodifiedby.name, Owner.name,(SELECT subject,owner.name, ActivityDate,ActivityType,whoid,PrimaryWhoId,primarywho.name,whatId,Expected_Outcome__c,SamsungContact__c,Business_Area__c FROM openactivities order by activitydate DESC limit 10),(SELECT Id, WhoId, whatId,Subject,Expected_Outcome__c,SamsungContact__c,Business_Area__c FROM Tasks where status = 'Open'),(SELECT  WhoId,Id, Subject,whatId,Expected_Outcome__c,SamsungContact__c,Business_Area__c FROM Events where StartDateTime >: system.Now()) from Account where id=:Acountid];
        Acc = [select id, Name, lastmodifieddate, lastmodifiedby.name, Owner.name,(select Id, name,Account__c, FiscalYear__c, MDMPartner__c,Carriers__c from Account_Profiles__r ORDER BY CreatedDate DESC limit 1),(SELECT subject,owner.name, ActivityDate,ActivityType,whoid,PrimaryWhoId,primarywho.name,whatId FROM openactivities order by activitydate DESC limit 10),(SELECT Id, WhoId, whatId,Subject FROM Tasks where status = 'Open'),(SELECT  WhoId,Id, Subject,whatId FROM Events where StartDateTime >: system.Now()) from Account where id=:Acountid];       
        //Acc = [select id,(SELECT subject,owner.name, ActivityDate,ActivityType,whoid,PrimaryWhoId,primarywho.name,whatId,Expected_Outcome__c,SamsungContact__c,Business_Area__c FROM openactivities order by activitydate DESC limit 10), Name, lastmodifieddate, lastmodifiedby.name, Owner.name from Account where id=:Acountid];

        //Getting the primary contacts 
        Primcon = [select id, AccountId, isprimary,contactid, contact.firstname,Contact.id,contact.name, contact.title, contact.email,contact.owner.name,role,contact.degre_of_influence__c,contact.Bussiness_Area__c,contact.User__c from AccountContactRole where accountid =:Acountid and isprimary=true];    
        MAP_ContactWithBusinessArea = new MAP<string,String>();
        MAP_ContactWithSamsungcon = new MAP<string,String>();
        MAP_ContactWithDegree = new MAP<string,String>();
        For(AccountContactRole pc: Primcon)
        {
            if(pc.contact.Bussiness_Area__c!=Null && pc.contact.Bussiness_Area__c!='')
            MAP_ContactWithBusinessArea.put(pc.Contact.id,pc.contact.Bussiness_Area__c);
            Else
            MAP_ContactWithBusinessArea.put(pc.Contact.id,Null);
            If(pc.contact.User__c!=NUll)
            MAP_ContactWithSamsungcon.put(pc.Contact.id,pc.contact.User__c);
            Else
            MAP_ContactWithSamsungcon.put(pc.Contact.id,Null);
            If(pc.contact.degre_of_influence__c!= null)
            MAP_ContactWithDegree.put(pc.Contact.id,pc.contact.degre_of_influence__c);
            Else
            MAP_ContactWithDegree.put(pc.Contact.id,NULL);
        }
        
        //Getting Mobile Share from Account Profile of this Account
        MobileShare = [Select id,name, Divison__c,Product__c,lo__c,samsung__c,other__c,total__c,Account_Profile__c,Account_Profile__r.MDMPartner__c,Account_Profile__r.Carriers__c from Product_Share__c where Divison__c='Mobile' and Account_Profile__r.createddate=THIS_YEAR and Account_Profile__r.Account__c =:ThePlan.Account__c ORDER BY Account_Profile__r.CreatedDate DESC limit 2];
        
        //Getting top 10 opportunities for this account
        top10opps = [select id,name,Amount,stagename,ProductGroupTest__c,closedate,initiative__c,Action_plan__c,Rollout_Duration__c from opportunity where (StageName='Qualification'or StageName='Proposal' or StageName='Identification') and accountid=:acountid ORDER BY Amount DESC  LIMIT 10];
        for(opportunity o : top10opps)
        {
            if(o.Rollout_Duration__c==0){
                o.Rollout_Duration__c=1;
            }
            o.Bypass_Validation__c=True;
        }
        
        //Getting all opportunity products for this account based on the opportunity product group 
        For(opportunitylineitem opline : [select id,Requested_Price__c,ProductGroup__c,totalprice,opportunity.stagename from opportunitylineitem where opportunity.accountid=:acountid and ProductGroup__c IN:SG_product and opportunity.stagename IN('Identification','Qualification','Proposal','Commit','Won') and calendar_year(opportunity.createddate)=2016 order by Opportunity.Division__c DESC])
        {
            Sales_Goals__c sgp1 = new Sales_Goals__c();
            If(MAP_Exting_Sales_goals.Containskey(opline.ProductGroup__c))
            {
                sgp1 = MAP_Exting_Sales_goals.Get(opline.ProductGroup__c);
                sgp1.won__c = 0;
                sgp1.commit__c = 0;
                sgp1.open__c = 0;
            }
            if(sgp1!= NUll)
            {
                if(opline.opportunity.stagename == 'won')
                {
                    sgp1.Won__c = sgp1.Won__c+opline.totalprice;
                }Else if(opline.opportunity.stagename == 'Commit')
                {
                    sgp1.commit__c = sgp1.Commit__c+opline.totalprice;
                }Else
                {
                    sgp1.Open__c = sgp1.Open__c+opline.totalprice;
                }
                MAP_Exting_Sales_goals.put(opline.ProductGroup__c,sgp1);
            }
        }
        For(Sales_Goals__c exitingSG : MAP_Exting_Sales_goals.values())
        {
            Sales_goals.add(exitingSG);
        }       
        List<Sales_Goals__c> List_Sales_Goals = new List<Sales_Goals__c>();
        for(Sales_Goals__c sg: [Select id,Account_Planning__c,Channel_Influencer__c,Distributor__c,Product__c,Renewal__c,Renewal_Date__c from Sales_Goals__c where Account_Planning__c =:planid ])
        {
           List_Sales_Goals.add(sg);
        }
        List<String> SG_products = getPickListValuesIntoList();
        //Getting the all Open activities for this Account
        openactivites = [select id,(select id,whoid from openactivities) from account where id=:acountid];
        list<openactivity> open = new list<openactivity>();
        Set<String> Lst_whoid = new Set<string>();
        for(openactivity op : openactivites.openactivities)
        {
            open.add(op);
            Lst_whoid.add(Op.whoid);
        }
        Map<String,Contact> Map_Contact =new Map<String,COntact>();        
        For(Contact Con: [Select id, firstName, Lastname, title,Owner.Name from contact where id IN: Lst_whoid])
        {
            Map_Contact.put(con.id,con);            
        }
        
        
    }
    //Wizard2--Dynamically ading extra rows
    public void addrow()
    {
        Customer_Goals_Pain_Points__c gpp = new Customer_Goals_Pain_Points__c(Account_Planning__c = planid);
        cgpp.Add(gpp);
    }
    //Wizard3--Dynamically ading extra rows
    public void addrow3()
    {
        value_statements__c vs = new value_statements__c(Account_planning__c = planid);
        val.add(vs);
    }
    
    Public pagereference EditCancel()
    {
         PageReference pageRef = new PageReference('/apex/Account_Planning_view_page?id='+theplan.id+'&accountid='+acountid);
         pageRef.setRedirect(true);
         return pageRef;
    }
    Public pagereference cancel1()
    {
         PageReference pageRef = new PageReference('/apex/Account_Planning_view_page?id='+theplan.id);
         pageRef.setRedirect(true);
         return pageRef;
    }
    Public pagereference cancel()
    {
         PageReference pageRef = new PageReference('/'+Acountid);
         pageRef.setRedirect(true);
         return pageRef;
    }
    Public pagereference Previous2()
    {
         PageReference pageRef = new PageReference('/apex/Account_Planning_Wizard1?id='+theplan.Id+'&accountid='+Acountid);
         pageRef.setRedirect(true);
         return pageRef;
    }
    Public pagereference Previous3()
    {
         PageReference pageRef = new PageReference('/apex/Account_Planning_Wizard2?id='+theplan.Id+'&accountid='+acountid);
         pageRef.setRedirect(true);
         return pageRef;
    }
    Public pagereference Previous4()
    {
         PageReference pageRef = new PageReference('/apex/Account_Planning_Wizard3?id='+theplan.Id+'&accountid='+acountid);
         pageRef.setRedirect(true);
         return pageRef;
    }
    Public pagereference Previous5()
    {
         PageReference pageRef = new PageReference('/apex/Account_Planning_Wizard4?id='+theplan.Id+'&accountid='+acountid);
         pageRef.setRedirect(true);
         return pageRef;
    }
    Public pagereference next1()
    {
         upsert theplan;
         PageReference pageRef = new PageReference('/apex/Account_Planning_Wizard2?id='+theplan.Id+'&accountid='+acountid);
         pageRef.setRedirect(true);
         return pageRef;
    }
    public pagereference saveandnext2()
    {
        cgpp_Update =new List<Customer_Goals_Pain_Points__c>();
        for(Customer_Goals_Pain_Points__c var: cgpp)
        {
            if((var.Customer_Goal__c != Null && var.Customer_Goal__c != '')||(var.Samsung_Solution__c!= Null && var.Samsung_Solution__c!= '')||(var.Timeline__c!= Null && var.Timeline__c!= '')||(var.Expected_Outcome__c!= Null && var.Expected_Outcome__c!= ''))
            {
                cgpp_Update.add(var);
            }
        }
        upsert cgpp_Update;
        update theplan;
        PageReference pageRef = new PageReference('/apex/Account_Planning_Wizard3?id='+theplan.Id+'&accountid='+acountid);
        pageRef.setRedirect(true);
        return pageRef;
    } 
    Public pagereference saveandnext3()
    {
         vs_update = new list<value_statements__c>();
         For(value_statements__c v : val)
         {
             if((v.Value_Statement__c!=null && v.Value_Statement__c!='')||(v.Relevancy__c!=null && v.Relevancy__c!='')||(v.value__c!=null && v.value__c!='')||(v.Differentiation__c!=null && v.Differentiation__c!=''))
             {
                 vs_update.add(v);
             }
         }
         upsert vs_update;
         PageReference pageRef = new PageReference('/apex/Account_Planning_Wizard4?id='+theplan.Id+'&accountid='+acountid);
         pageRef.setRedirect(true);
         return pageRef;
    }
    public pagereference saveandnext4()
    {
         //Update Acc.Tasks;
         //Update Acc.Events;
         List<Contact> Upd_contact = new List<Contact>();
         For(AccountContactrole acr: primcon)
         {
            System.debug('^^^^^1'+acr.contact.Degre_of_influence__c);
            //System.debug('^^^^^2'+MAP_ContactWithPrimcon.get(acr.Contact.id).contact.Degre_of_influence__c);
            
            if(acr.contact.Bussiness_Area__c != MAP_ContactWithBusinessArea.get(acr.Contact.id) || acr.contact.User__c!= MAP_ContactWithSamsungcon.get(acr.Contact.id)|| acr.contact.Degre_of_influence__c != MAP_ContactWithDegree.get(acr.Contact.id))
            {
                System.debug('^^^^^3');
                Upd_contact.add(new Contact(id = acr.Contact.id, User__c =acr.contact.User__c,Degre_of_influence__c = acr.contact.Degre_of_influence__c,Bussiness_Area__c=acr.contact.Bussiness_Area__c));
            }
         }
         System.debug('^^^^^'+Upd_contact);
         Update Upd_contact;
         PageReference pageRef = new PageReference('/apex/Account_Planning_Wizard5?id='+theplan.Id+'&accountid='+acountid);
         pageRef.setRedirect(true);
         return pageRef; 
    }
    /*public pagereference saveandnext5()
    {
        List<Sales_Goals__c> Sales_Goals_Update = new List<Sales_Goals__c>();
        update theplan;
        update top10opps;
        for(Sales_Goals__c sg:Sales_goals)
        {
            //sg.Account_Planning__c = theplan.id;
            Sales_Goals_Update.add(sg);
        }    
        Upsert Sales_Goals_Update;    
        PageReference pageRef = new PageReference('/apex/Account_Planning_view_page?id='+theplan.Id+'&accountid='+acountid);
        pageRef.setRedirect(true);
        return pageRef;
    }*/
    public pagereference finalsave()
    {
        List<Sales_Goals__c> Sales_Goals_Update = new List<Sales_Goals__c>();
        List<Contact> Upd_contact = new List<Contact>();
        update theplan;
        update top10opps;
        upsert cgpp;
        upsert val;
        For(AccountContactrole acr: primcon)
        {
            System.debug('^^^^^1'+acr.contact.Degre_of_influence__c);
            //System.debug('^^^^^2'+MAP_ContactWithPrimcon.get(acr.Contact.id).contact.Degre_of_influence__c);
            
            if(acr.contact.Bussiness_Area__c != MAP_ContactWithBusinessArea.get(acr.Contact.id) || acr.contact.User__c!= MAP_ContactWithSamsungcon.get(acr.Contact.id)|| acr.contact.Degre_of_influence__c != MAP_ContactWithDegree.get(acr.Contact.id))
            {
                System.debug('^^^^^3');
                Upd_contact.add(new Contact(id = acr.Contact.id, User__c =acr.contact.User__c,Degre_of_influence__c = acr.contact.Degre_of_influence__c,Bussiness_Area__c=acr.contact.Bussiness_Area__c));
            }
        }
        System.debug('^^^^^'+Upd_contact);
        Update Upd_contact;
        //update Acc.Tasks;
        //Update Acc.Events;
        
        for(Sales_Goals__c sg:Sales_goals)
        {
            //sg.Account_Planning__c = theplan.id;
            Sales_Goals_Update.add(sg);
        }    
        Upsert Sales_Goals_Update;    
        PageReference pageRef = new PageReference('/apex/Account_Planning_view_page?id='+theplan.Id+'&accountid='+acountid);
        pageRef.setRedirect(true);
        return pageRef;
    } 
    public pagereference editplan()
    {
        PageReference pageRef = new PageReference('/apex/Account_Planning_Edit_page?id='+theplan.Id+'&accountid='+acountid);
        pageRef.setRedirect(true);
        return pageRef;
    }
    public pagereference pdfview()
    {
        PageReference pageRef = new PageReference('/apex/Account_Plan_View_PDF?id='+theplan.Id+'&accountid='+acountid);
        pageRef.setRedirect(true);
        return pageRef;
    }
    public List<String> getPickListValuesIntoList(){
       List<String> pickListValuesList= new List<String>();
        Schema.DescribeFieldResult fieldResult = Sales_Goals__c.Product__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getLabel());
        }     
        return pickListValuesList;
    }
    Public Class Sales_Goals
    {
        Decimal Won;
        Decimal CommitOpp;
        Decimal Open_pipeline;
        Sales_Goals__c SG;
        Public Sales_goals(Decimal Won,Decimal CommitOpp,Decimal Open_pipeline,Sales_Goals__c SG)
        {
            this.won = won;
            this.CommitOpp= CommitOpp;
            this.Open_pipeline = Open_Pipeline;
            this.sg = sg;
        }
        
    }          
}