@istest(seealldata = true)
public class AccountPlanExtTest{
    /*public static testmethod void AccountPlanExtTest(){
        Account testAccount = new Account(Name='Test Company Name1234');
        insert testAccount;
        
        Account_Planning__c accountplan = new Account_planning__c(name = '11/28/2016accplanaccplan',Account__c = testAccount.id);
        insert accountplan;
        
        Pagereference pg = page.Account_planning_wizard1; 
        test.setcurrentpage(pg);
        ApexPages.currentPage().getparameters().put('id',Null);    
        apexpages.currentpage().getparameters().put('AccountId',String.valueof(testAccount.id));
        //ApexPages.currentPage().getparameters().put('AccountId',testaccount.id);
        Account_Planning__c ap = new Account_planning__c(name = '11/28/2016');        
        ApexPages.StandardController sc = new ApexPages.StandardController(ap);
        AccountPlanExt accplan = new AccountPlanExt(sc);
        accplan.next1(); 
        accplan.cancel(); 
        
        
        Pagereference pg2 = page.Account_Planning_Wizard2; 
        test.setcurrentpage(pg2);
        ApexPages.currentPage().getparameters().put('id',String.valueof(ap.id));    
        //ApexPages.currentPage().getparameters().put('Accountid',testaccount.id);
        apexpages.currentpage().getparameters().put('AccountId',String.valueof(testAccount.id));
        ap.strengths__c = 'These are strengths'; 
        Customer_Goals_Pain_Points__c cg = new Customer_Goals_Pain_Points__c();
        cg.Customer_Goal__c = 'New Customer Goal'; 
        cg.Account_Planning__c = 'a0w11000001myE4AAI';
        Customer_Goals_Pain_Points__c cg1 = new Customer_Goals_Pain_Points__c();
        cg1.Customer_Goal__c = 'New Customer Goal';
        cg1.Account_Planning__c = 'a0w11000001myE4AAI';
        accplan.addrow();   
        accplan.saveandnext2();
        accplan.cancel1();
        accplan.previous2();  
        
        
        Pagereference pg3 = page.Account_Planning_Wizard3; 
        test.setcurrentpage(pg3);
        ApexPages.currentPage().getparameters().put('id',String.valueof(ap.id));    
        //ApexPages.currentPage().getparameters().put('Accountid',testaccount.id);
        apexpages.currentpage().getparameters().put('AccountId',String.valueof(testAccount.id));
        Value_Statements__c vs = new Value_Statements__c();
        vs.Value_Statement__c = 'New Value Staement';     
        /*ApexPages.StandardController sc3 = new ApexPages.StandardController(ap);
        AccountPlanExt accplan3 = new AccountPlanExt(sc3);
        accplan3.saveandnext3();
        
        accplan.saveandnext3();
        accplan.cancel1();
        accplan.previous3(); 
        accplan.addrow3();
        
        Pagereference pg4 = page.Account_Planning_Wizard4; 
        test.setcurrentpage(pg4);
        ApexPages.currentPage().getparameters().put('id',String.valueof(ap.id));    
        //ApexPages.currentPage().getparameters().put('Accountid',testaccount.id);
        apexpages.currentpage().getparameters().put('AccountId',String.valueof(testAccount.id));    
        /*ApexPages.StandardController sc4 = new ApexPages.StandardController(ap);
        AccountPlanExt accplan4 = new AccountPlanExt(sc3);
        
        contact c = new contact();
        c.Bussiness_Area__c ='IT';
        c.degre_of_influence__c = 'HIGH';
        accplan.saveandnext4();
        accplan.cancel1();
        accplan.previous4(); 
        
        
        Pagereference pg5 = page.Account_Planning_Edit_Page; 
        test.setcurrentpage(pg5);
        ApexPages.currentPage().getparameters().put('id',accountplan.id);    
        ApexPages.currentPage().getparameters().put('Accountid',testAccount.id); 
        //ApexPages.currentPage().getparameters().put('Accountid','0013600000SaeSJ'); 
        Account_Planning__c apedit = new Account_planning__c();
        ApexPages.StandardController scedit = new ApexPages.StandardController(accountplan);
        AccountPlanext accprof1 = new AccountPlanext(scedit); 
        apedit.Weaknesses__c= 'Weakness test';   
        Sales_Goals__c sg = new Sales_Goals__c();
        sg.Product__c = 'TABLET';
        sg.TAM__c = '100,20K';
        sg.Renewal__c = 'YES';
        //Customer_Goals_Pain_Points__c cg3 = new Customer_Goals_Pain_Points__c();
        //cg3.Customer_Goal__c = 'New Customer Goal';
        //cg3.Account_Planning__c = 'a0w11000001myE4AAI';
        accprof1.addrow();  
        accprof1.finalsave();
        accprof1.editplan();
        accprof1.pdfview();
        accprof1.Previous5();
        accprof1.EditCancel();
        accprof1.cancel1();
        
        
        Pagereference pg1_V = page.Account_planning_wizard1; 
        test.setcurrentpage(pg1_V);
        ApexPages.currentPage().getparameters().put('id',ap.id);    
        apexpages.currentpage().getparameters().put('AccountId',String.valueof(testAccount.id));
        //ApexPages.currentPage().getparameters().put('AccountId',testaccount.id);
        //Account_Planning__c ap_V = new Account_planning__c(name = '11/28/2016');        
        ApexPages.StandardController sc_V = new ApexPages.StandardController(ap);
        AccountPlanExt accplan_V = new AccountPlanExt(sc_V);
        accplan_V.next1(); 
        accplan_V.cancel(); 
    }*/
    public static testmethod void AccountPlanExtTest1(){
        Account testAccount = new Account(Name='Test Company Name1234',SAP_Company_Code__c='123456789');
        insert testAccount;
        
        Account_Planning__c accountplan = new Account_planning__c(name = '11/28/2016accplanaccplan',Account__c = '0013600000SaeSJ');
        insert accountplan;
        Account_Profile__c accprof = new Account_Profile__c(name='01/09/2017',FiscalYear__c='2017',Account__c='0013600000SaeSJ');
        insert accprof;
        
        Pagereference pg = page.Account_planning_wizard1; 
        test.setcurrentpage(pg);
        ApexPages.currentPage().getparameters().put('id',Null);    
        apexpages.currentpage().getparameters().put('AccountId','0013600000SaeSJ');
        //ApexPages.currentPage().getparameters().put('AccountId',testaccount.id);
        Account_Planning__c ap = new Account_planning__c(name = '11/28/2016');
        ApexPages.StandardController sc = new ApexPages.StandardController(ap);
        AccountPlanExt accplan = new AccountPlanExt(sc);
        accplan.next1(); 
        accplan.cancel(); 
        
        
        Pagereference pg2 = page.Account_Planning_Wizard2; 
        test.setcurrentpage(pg2);
        ApexPages.currentPage().getparameters().put('id',String.valueof(ap.id));    
        //ApexPages.currentPage().getparameters().put('Accountid',testaccount.id);
        apexpages.currentpage().getparameters().put('AccountId',String.valueof(testAccount.id));
        ap.strengths__c = 'These are strengths'; 
        Customer_Goals_Pain_Points__c cg = new Customer_Goals_Pain_Points__c();
        cg.Customer_Goal__c = 'New Customer Goal'; 
        cg.Account_Planning__c = 'a0w11000001myE4AAI';
        Customer_Goals_Pain_Points__c cg1 = new Customer_Goals_Pain_Points__c();
        cg1.Customer_Goal__c = 'New Customer Goal';
        cg1.Account_Planning__c = 'a0w11000001myE4AAI';
        accplan.addrow();   
        accplan.saveandnext2();
        accplan.cancel1();
        accplan.previous2();  
        
        
        Pagereference pg3 = page.Account_Planning_Wizard3; 
        test.setcurrentpage(pg3);
        ApexPages.currentPage().getparameters().put('id',String.valueof(ap.id));
        apexpages.currentpage().getparameters().put('AccountId',String.valueof(testAccount.id));
        Value_Statements__c vs = new Value_Statements__c();
        vs.Value_Statement__c = 'New Value Staement';     
        accplan.saveandnext3();
        accplan.cancel1();
        accplan.previous3(); 
        accplan.addrow3();
        
        Pagereference pg4 = page.Account_Planning_Wizard4; 
        test.setcurrentpage(pg4);
        ApexPages.currentPage().getparameters().put('id',String.valueof(ap.id));    
        apexpages.currentpage().getparameters().put('AccountId',String.valueof(testAccount.id));    
        contact c = new contact();
        c.Bussiness_Area__c ='IT';
        c.degre_of_influence__c = 'HIGH';
        accplan.saveandnext4();
        accplan.cancel1();
        accplan.previous4();
        
        Pagereference pg5 = page.Account_Planning_Edit_Page; 
        test.setcurrentpage(pg5);
        ApexPages.currentPage().getparameters().put('id',accountplan.id);    
        //ApexPages.currentPage().getparameters().put('Accountid',testAccount.id); 
        ApexPages.currentPage().getparameters().put('Accountid','0013600000SaeSJ'); 
        Account_Planning__c apedit = new Account_planning__c();
        ApexPages.StandardController scedit = new ApexPages.StandardController(accountplan);
        AccountPlanext accprof1 = new AccountPlanext(scedit); 
        apedit.Weaknesses__c= 'Weakness test';   
        Sales_Goals__c sg = new Sales_Goals__c();
        sg.Product__c = 'TABLET';
        sg.TAM__c = '100,20K';
        sg.Renewal__c = 'YES';
        accprof1.addrow();  
        accprof1.finalsave();
        accprof1.editplan();
        accprof1.pdfview();
        accprof1.Previous5();
        accprof1.EditCancel();
        accprof1.cancel1();
    }
}