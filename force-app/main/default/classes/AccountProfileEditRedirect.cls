public class AccountProfileEditRedirect {
    Public Account_Profile__c Ap;
    Public string apid;
    public AccountProfileEditRedirect(ApexPages.StandardController controller) 
    {
        
    }
    Public pagereference redirect()
    {
        //pagereference pg;
        Apid= Apexpages.currentpage().getparameters().get('id');
        if(Apid != null && Apid != '')
        {
            ap = [select id,Account__c,Account__r.recordtype.name from Account_Profile__c where id =: Apid];
        }
        if(ap != Null)
        {
            if(ap.Account__r.recordtype.name == 'End Customer'){
                //pagereference pg = new pagereference('/apex/Account_Profile?id='+apid);
                pagereference pg = new pagereference('/apex/AccountProfile?id='+apid); //Added on 12/11/2018
                pg.setredirect(true);
                return pg;
            }
            else{
                //pagereference pg = new pagereference('/'+Apid+'/e?retURL=%2F'+Apid+'&nooverride=1');                
                pagereference pg = new pagereference('/apex/AccountProfilePage_T2?id='+Apid);
                pg.setredirect(true);
                return pg;
            }
            
        }Else
        {
            pagereference pg = new pagereference('/a0L/e?nooverride=1');
            pg.setredirect(true);
            return pg;     
        }
    }

}