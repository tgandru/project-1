/*
Description: Extension class for AccountProfileViewPDF page
Author: Paul Mraz
*/

public class AccountProfileExt {
    
    public Account_Profile__c theProfile {get;set;}
    public Account thePartner {get;set;}
    public Account theCustomer {get;set;}
    public Account[] branches {get;set;}
    public transient AccountTeamMember[] theTeam {get;set;}
    public transient AccountContactRole [] keycon {get;set;}
    public transient Partner__c[] partnerrel {get;set;}
    public transient Partner__c[] partneropps {get;set;}
    public Marketing_Investment__c[] minv {get;set;}
    public CompetitiveProductShare__c[] comprod {get;set;}
    public Event[] cevents {get;set;}
    public Opportunity[] opps {get;set;}
    public Opportunity[] wins {get;set;}
    public Opportunity[] loss {get;set;}
    //public Summary[] Summaries {get;set;}
    Date onlyDate = Date.today();
    
    public AccountProfileExt(ApexPages.StandardController controller){

        //Get Information about Account Profile
        theProfile = [select Id, Account__c, FiscalYear__c from Account_Profile__c where Id = :controller.getRecord().Id limit 1];
        
        //Get Details from the Partner Account
        theCustomer = thePartner = [select id, Name, NumberOfEmployees, AnnualRevenue, BillingCity, BillingState, Website, Phone, Type from Account where Id = :theProfile.Account__c];    
        
        //Jagan:- Commented an extra query for theCustomer as both Partner and Customer queries are same
        //Get Details from the Customer Account
        //theCustomer = [select id, Name, NumberOfEmployees, AnnualRevenue, BillingCity, BillingState, Website, Phone, Type from Account where Id = :theProfile.Account__c];    
            
        //Get Account Team
        theTeam = [select id, AccountId, TeamMemberRole, user.name, user.email, user.phone from AccountTeamMember where AccountId = :theProfile.Account__c];
        
        //Get Key Contacts
        keycon = [select id, AccountId, Role, contact.firstname, contact.title, contact.email from AccountContactRole where AccountId = :theProfile.Account__c];
            
        // Get Branch Information / Child accounts
        branches = [select Id, name, billingcity, billingstate, NumberOfEmployees, type, account.owner.name from Account where Parentid = :theProfile.Account__c ORDER BY shippingstate ASC];
        
        // Get Exec Events
        cevents = [select Id, subject, type, Owner.Name, WhoId, Who.Name, ActivityDate, SamsungExec__c from Event where type = 'Executive Sponsor' and WhatId =:theProfile.Account__c ORDER BY ActivityDate DESC];
       
        //Get Opportunity records related within last 365 days
        wins = [select Id, Opportunity_Number__c, Account.Name, Reference__c, Amount, ForecastAmount__C, Plan_Variance__c, Owner.Name, Type, StageName, CloseDate, Division__c from Opportunity where (StageName='Commit'or StageName='Won') and AccountId=:theProfile.Account__c and CloseDate > :onlyDate.addDays(-365) ORDER BY Amount DESC  LIMIT 10];            
        loss = [select Id, Opportunity_Number__c, Account.Name, Reference__c, Amount, Owner.Name, Type, StageName, Drop_Reason__c, CloseDate, Division__c from Opportunity where IsWon=False and IsClosed=True and AccountId=:theProfile.Account__c and CloseDate > :onlyDate.addDays(-365) ORDER BY Amount DESC LIMIT 10 ];            
            
        //Get Partner activity on Opportunities
        partneropps = [select Id, Registered__c, Amount__c, Stage__c, Close_Date__c, Customer__r.Name, Opportunity__r.Name,  role__c, CreatedDate from Partner__c where partner__c = :theProfile.Account__c and Close_Date__c > :onlyDate.addDays(-365) ORDER BY Amount__c DESC Limit 20]; 
        
        //Get Partner relationships for customer opportunities
        partnerrel = [select Id,Partner__r.Name, Opportunity__c, Opportunity__r.Amount, Opportunity__r.IsWon, role__c from Partner__c where customer__c = :theProfile.Account__c]; 
        
        //Get Marketing Investments for Account Profile
        minv= [select Id, Amount__c, FiscalYear__c, Focus__c, Type__c from Marketing_Investment__c where Account_Profile__c =:theProfile.id and FiscalYear__c=:theProfile.FiscalYear__c];
       
        //Get Competitive Information for Account Profile
        comprod= [select Id, CompetitorName__c, CompetitorProduct__c, Product__c, Share_Percent__c  from CompetitiveProductShare__c where Account_Profile__c =:theProfile.id];
            
    }
}