@isTest
private class AccountProfileExtTest {
    static Id endUserRT = TestDataUtility.retrieveRecordTypeId('End_User', 'Account');
    static Id inDirectRT = TestDataUtility.retrieveRecordTypeId('Indirect', 'Account');
    static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
        
    @isTest static void test_method_one() {
        // Implement test code
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();

        Account account = TestDataUtility.createAccount('Main Account');
        account.RecordTypeId = directRT;
        account.Type= 'Distributor';
        account.NumberOfEmployees=100;
        account.AnnualRevenue=50000.15;
        insert account;

        //Creating the Parent Account for Reseller as Partners
        Account customerAcc =TestDataUtility.createAccount('Customer Account');
        customerAcc.RecordTypeId = endUserRT;
        customerAcc.NumberOfEmployees=500;
        customerAcc.AnnualRevenue=1000000.45;
        insert customerAcc;

        Account_Profile__c profileAcc = TestDataUtility.createAccountProfile(account);
        insert profileAcc;

        Account_Profile__c profileCusAcc = TestDataUtility.createAccountProfile(customerAcc);
        insert profileCusAcc;

        AccountProfileExt accProfileExt = new AccountProfileExt(new ApexPages.StandardController(profileAcc));
        accProfileExt.theProfile=profileAcc;
        accProfileExt.thePartner=account;
        accProfileExt.theCustomer=customerAcc;

        AccountProfileExt accProfileExt2 = new AccountProfileExt(new ApexPages.StandardController(profileCusAcc));
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];

        User u2 = new User(Alias = 'newUser', Email='newuser@testorg.com',EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
        LocaleSidKey='en_US', phone = '1234567789',ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='TestnwUaer@testorg.com');
        insert u2;

        List<AccountTeamMember> accTeams= new List<AccountTeamMember>();
        AccountTeamMember accTeamMember = TestDataUtility.createAccountTeamMember(u2,account);
        accTeams.add(accTeamMember);
        insert accTeams;

        accProfileExt.theTeam= accTeams;
        List<Account> childAccount = new List<Account>();
        Account childAcc = TestDataUtility.createAccount('Child Account');
        childAcc.NumberOfEmployees=20;
        childAcc.AnnualRevenue=10000.00;
        childAcc.ParentId=account.Id;
        childAccount.add(childAcc);
        insert childAccount;
        accProfileExt.branches=childAccount;

        Contact contac = TestDataUtility.createContact(account);
        insert contac;

        List<AccountContactRole> contRole = new List<AccountContactRole>();
        AccountContactRole contRol = TestDataUtility.createAcccontactRole(account, contac);
        contRole.add(contRol);
        insert contRole;
        accProfileExt.keycon=contRole;

        List<Event> event = new List<Event>();
        Event events = TestDataUtility.createEvent(contac, account);
        events.ActivityDateTime=DateTime.newInstance(2016, 3, 16, 12, 6, 13);
        events.DurationInMinutes=30;
        event.add(events);
        insert event;
        accProfileExt.cevents=event;

        List<Opportunity> oppOpen = new List<Opportunity>();
        Opportunity opp=TestDataUtility.createOppty('New Opp from Test setup',account, null, 'RFP', 'IT', 'FAX/MFP', 'Identified');
        //opp.IsWon=false;
        //opp.IsClosed=true;
        opp.CloseDate= System.today().addDays(-380);
        oppOpen.add(opp);
        insert oppOpen;
        accProfileExt.loss=oppOpen;

        List<Partner__c> partner1 = new List<Partner__c>();
        Partner__c partner = TestDataUtility.createPartner(opp,account,customerAcc,'Consultant');
        partner1.add(partner);
        insert partner1;
        accProfileExt.partneropps=partner1;

        List<Partner__c> partner2 = new List<Partner__c>();
        Partner__c partners = TestDataUtility.createPartner(opp,customerAcc,customerAcc,'Consultant');
        partner2.add(partners);
        insert partner2;
        accProfileExt2.partnerrel=partner2;
        
        List<Marketing_Investment__c> markInvesment = new List<Marketing_Investment__c>();
        Marketing_Investment__c markInves = TestDataUtility.createMarketingInvesment(profileAcc);
        markInvesment.add(markInves);
        insert markInvesment;
        accProfileExt.minv=markInvesment;

        List<CompetitiveProductShare__c> compShare = new List<CompetitiveProductShare__c>();
        CompetitiveProductShare__c compeShare = TestDataUtility.createCompetitiveProductShare(profileAcc);
        compShare.add(compeShare);
        insert compShare;
        accProfileExt.comprod=compShare;
        
        accProfileExt2.opps = new List<Opportunity>();
    }

    
}