public class AccountProfileExt_V1 {
    public Account_Profile__c theProfile {get;set;}
    Public Account Acc{get;Set;}
    Public Contact[] Con {get;Set;}
    Public Contact[] Con_BeforeUpdate;
    public AccountContactRole [] keycon {get;set;}
    public transient AccountContactRole [] Buyer_keycon {get;set;}
    public Opportunity[] openopps {get;set;}
    public Opportunity[] wins {get;set;}
    public Opportunity[] loss {get;set;}
    public Opportunity[] PYopenopps {get;set;}
    public Opportunity[] PYwins {get;set;}
    public Opportunity[] PYloss {get;set;}
    public Product_Share__c[] MobileProductShare {get;set;}
    public Product_Share__c[] ITProductShare{get;set;}
    public Event[] cevents {get;set;}
    public transient AccountTeamMember[] theTeam {get;set;}
    public ActivityHistory[] Activities {get;set;}
    public OpenActivity[] ActivitiesOpen {get;set;}
    public purchase_History__c[] PurchaseHistory {get;set;}
    public purchase_History__c[] PurchaseHistory_Mobile {get;set;}
    public purchase_History__c[] PurchaseHistory_IT {get;set;}
    Public double Mobile_Purchase_Total{get;set;}
    public string Mobile_Total_Purchase{get;set;}
    Public double IT_Purchase_Total{get;set;}
    public string IT_Total_Purchase{get;set;}
    Public double Purchase_Total{get;set;}
    public string Total_Purchase_Amount{get;set;}
    Public double Mobile_Purchase_Units{get;set;}
    public string Mobile_Total_Units{get;set;}
    Public double IT_Purchase_Units{get;set;}
    public string IT_Total_Units{get;set;}
    Public double Purchase_Units{get;set;}
    public string Total_Purchase_Units{get;set;}
    public string Acountid;
    Public Map<String,string> Map_ContachWDI;
    public string profileid{get;set;}
    public AccountProfileExt_V1(ApexPages.StandardController controller) {
        Con_BeforeUpdate = new List<Contact>(); 
        List<string> ItDivision = new List<String>{'HTV','MEM','MON','NPC','PRT','TAB','VDI','Smart Signage'};
        List<string> MobileDivision = new List<String>{'IL','CL'};
        //List<string> PurchHistory = new List<string>{'HTV','MEM','MON','NPC','PRT','TAB','HHP','VDI','Smart Signage'};
        Map<String,String> Map_ProductDivandProduct = new Map<String,string>();
        Map_ProductDivandProduct.put('HHP','Mobile');
        Map_ProductDivandProduct.put('TAB','Mobile');
        Map_ProductDivandProduct.put('KNOX','Mobile');
        Map_ProductDivandProduct.put('SBS','Mobile');
        Map_ProductDivandProduct.put('WAR','Mobile');
        Map_ProductDivandProduct.put('LFD','IT');
        Map_ProductDivandProduct.put('MON','IT');
        Map_ProductDivandProduct.put('NPC','IT');
        Map_ProductDivandProduct.put('HTV','IT');
        Map_ProductDivandProduct.put('PRT','IT');      
        profileid = Apexpages.currentpage().getParameters().get('id');       
        Acountid = Apexpages.currentpage().getParameters().get('AccountId');
        if(profileid != Null && profileid!= '')
        {
            theProfile = [select Id, name,Account__c, FiscalYear__c, MDMPartner__c,Carriers__c from Account_Profile__c where Id = :profileid];
            //theProfile = (Account_Profile__c)controller.getRecord();
            Acountid = theProfile.Account__c;
            system.debug('the is is -->' + Acountid);
        }
        Else
        {
            theProfile = (Account_Profile__c)controller.getRecord();
            //theProfile = new Account_Profile__c();
            theProfile.Account__c = Acountid;  
        }
        system.debug('the is is 69-->' + Acountid);
        Acc = [select id,(SELECT subject,owner.name, ActivityDate,ActivityType,whoid,PrimaryWhoId,primarywho.name FROM openactivities order by activitydate DESC limit 10), Name, Parent.Name, NumberOfEmployees, AnnualRevenue, Address_For_Profie__c, billingstate,Website, Phone, Prior_Year_Revenue__c,Prior_Year_Number_of_Employees__c, industry from Account where id=:Acountid];
        
        //Get Opportunity records related to current year
        wins = [select Id, name,Bypass_Validation__c,Opportunity_Number__c, Account.Name, Reference__c, Amount, ForecastAmount__c, Plan_Variance__c,ProductGroupTest__c, Owner.Name, Type, StageName, CloseDate, Division__c,Initiative__c from Opportunity where (StageName='Commit'or StageName='Win') and AccountId=:Acountid and CloseDate = THIS_YEAR ORDER BY Amount DESC  LIMIT 5];            
        for(opportunity opwins : wins) // for bypassing the validation on opportunity
        {
            opwins.Bypass_Validation__c=true;
        }
        system.debug('wins################:'+wins);
        //loss = [select Id, name,Opportunity_Number__c, Account.Name, Reference__c, Amount, Owner.Name, Type, StageName, Drop_Reason__c, CloseDate, ProductGroupTest__c,Division__c from Opportunity where IsWon=False and IsClosed=True and AccountId=:Acountid and CloseDate = THIS_YEAR ORDER BY Amount DESC LIMIT 5 ];            
        //openopps = [select Id, name,Bypass_Validation__c,Opportunity_Number__c, Account.Name, Reference__c, Amount, Owner.Name, Type, StageName, Drop_Reason__c, CloseDate, Division__c, ProductGroupTest__c,Initiative__c from Opportunity where (StageName='Qualification'or StageName='Proposal' or StageName='Identification') and AccountId=:Acountid  and CloseDate = THIS_YEAR ORDER BY Amount DESC LIMIT 5 ];
        openopps = [select Id, name,Bypass_Validation__c,Opportunity_Number__c, Account.Name, Reference__c, Amount, Owner.Name, Type, StageName, Drop_Reason__c, CloseDate, Division__c, ProductGroupTest__c,Initiative__c from Opportunity where (StageName='Qualified'or StageName='Proposal' or StageName='Identified') and AccountId=:Acountid  and CloseDate = THIS_YEAR ORDER BY Amount DESC LIMIT 5 ];
        for(opportunity opopen : openopps) // for bypassing the validation on opportunity
        {
            opopen.Bypass_Validation__c=true;
        }
        system.debug('openopps################:'+openopps);
        //Get Opportunity records related to Previous Year.
        PYwins = [select Id, name,Bypass_Validation__c,Opportunity_Number__c, Account.Name, Reference__c, Amount, ForecastAmount__c, Plan_Variance__c,ProductGroupTest__c, Owner.Name, Type, StageName, CloseDate, Division__c,Initiative__c from Opportunity where (StageName='Commit'or StageName='Win') and AccountId=:Acountid and CloseDate = LAST_YEAR ORDER BY Amount DESC  LIMIT 5];            
        for(opportunity opPYwins : PYwins) // for bypassing the validation on opportunity
        {
            opPYwins.Bypass_Validation__c=true;
        }
        system.debug('PYwins################:'+PYwins);
        //Get Key Contacts
        keycon = [select id, AccountId, Role, contact.name, contact.title, Contact.id,contact.email,contact.Degre_of_influence__c,contact.phone,contact.Key_Contact__c from AccountContactRole where (Role = 'Influencer' or Role = 'Economic Buyer' or Role = 'Technical Buyer' or Role = 'Decision Maker') and AccountId = :acountid];
        Map_ContachWDI = new Map<String,string>();
        for(AccountContactRole Acr: Keycon)
        {
            If(Acr.contact.Degre_of_influence__c!= Null)
            Map_ContachWDI.put(acr.Contact.id,Acr.contact.Degre_of_influence__c);
            Else
            Map_ContachWDI.put(acr.Contact.id,NULL);
        }
        //Get Account Team
        theTeam = [select id, AccountId, TeamMemberRole, user.name, user.email, user.phone from AccountTeamMember where AccountId = :acountid];
        
        // Get Exec Events
        cevents = [select Id, subject, type, Owner.Name, WhoId, Who.Name, ActivityDate, SamsungExec__c from Event where type = 'Executive Sponsor' and WhatId =:acountid ORDER BY ActivityDate DESC];
        
        //Get purchase history for Account Profile
        PurchaseHistory = [select id,Account_profile__c,name,Purchase_amount__c,PurchaseAmount__c,Expected_Refresh_Date__c,Quantity__c,Services_included__c,Product_Division__c from Purchase_history__c where Account_profile__c=:profileid order by Product_Division__c];
        
        Account Ac = [SELECT Id,Name,(SELECT subject, ActivityDate,ActivityType,whoid,who.name,PrimaryWhoId,primarywho.name,owner.name FROM ActivityHistories order by activitydate DESC limit 10),(SELECT subject, ActivityDate,ActivityType,whoid,PrimaryWhoId FROM openactivities) FROM Account where id=:acountid];
        Activities = new list<ActivityHistory>();
        
        For(ActivityHistory his: Ac.ActivityHistories)
        {
            Activities.add(his);
        }  
        
      
        ITProductShare = new List<Product_Share__c>();
        MobileProductShare = new List<Product_Share__c>();
        For(Product_Share__c ps:[Select id,name, Divison__c,Product__c,CompetitorName__c,CY_Share__c,lo__c,samsung__c,other__c,total__c,Account_Profile__c from Product_Share__c where Account_Profile__c =: profileid])
        {             
            System.debug('ps-->'+ps);                                                                                                                       
            if(ps.Divison__c == 'IT')
            {
               ITProductShare.add(ps);
            }Else
            {
               MobileProductShare.Add(ps);
            }
        }
        If(ITProductShare.Size()<=0)
        {
            Product_Share__c ITPS; 
            for(String its:ItDivision)
            {
                ITPS = new Product_Share__c();
                itps.Divison__c = 'IT';
                itps.Product__c= its; 
                ITProductShare.add(itps);
            }
        }
        If(MobileProductShare.Size()<=0)
        {
            Product_Share__c ITPS; 
            for(String its:MobileDivision)
            {
                ITPS = new Product_Share__c();
                itps.Divison__c = 'Mobile';
                itps.Product__c= its; 
                MobileProductShare.add(itps);
            }
        }
        
        PurchaseHistory = new list<purchase_history__c>();
        PurchaseHistory_Mobile = new list<purchase_history__c>();
        PurchaseHistory_IT = new list<purchase_history__c>();
        Mobile_Purchase_Total=0;
        IT_Purchase_Total = 0;
        Mobile_Purchase_Units=0;
        IT_Purchase_Units=0;
        Purchase_Total = 0;
        Purchase_Units=0;
        for(Purchase_History__c ph : [select id,name,Account_Profile__c,Expected_Refresh_Date__c,Quantity__c,Purchase_amount__c,PurchaseAmount__c,Services_Included__c,Product_Division__c from purchase_history__c where Account_Profile__c =:profileid])
        {
            If(ph.Product_Division__c=='MOBILE')
            {
                purchaseHistory.add(ph);
                PurchaseHistory_Mobile.add(ph);
                If(ph.PurchaseAmount__c!= null)
                Mobile_Purchase_Total=Mobile_Purchase_Total+ph.PurchaseAmount__c;
                Mobile_Total_Purchase = '$' + Mobile_Purchase_Total.intValue().format();
                If(ph.Quantity__c!=Null)
                Mobile_Purchase_Units = Mobile_Purchase_Units+ph.Quantity__c;
                Mobile_Total_Units = Mobile_Purchase_Units.intValue().format();
            }Else
            {
                purchaseHistory.add(ph);
                PurchaseHistory_IT.add(ph);
                If(ph.PurchaseAmount__c!= null)
                IT_Purchase_Total=IT_Purchase_Total+ph.PurchaseAmount__c;
                IT_Total_Purchase = '$' + IT_Purchase_Total.intValue().format();
                If(ph.Quantity__c!=Null)
                IT_Purchase_Units = IT_Purchase_Units+ph.Quantity__c;
                IT_Total_Units = IT_Purchase_Units.intValue().format();
            }
        }    
        Purchase_Total = Mobile_Purchase_Total+IT_Purchase_Total;
        Total_Purchase_Amount = '$' + Purchase_Total.intValue().format();
        Purchase_Units =Mobile_Purchase_Units +IT_Purchase_Units ;
        Total_Purchase_Units = Purchase_Units.intValue().format();
        if(purchasehistory.size()<=0)
        {
            purchase_history__c phistory;
            for(string p : Map_ProductDivandProduct.keyset())
            {
               // If(Map_ProductDivandProduct.get(p)=='MOBILE')
                //{
                    phistory = new Purchase_history__c();
                    phistory.name= p;
                    phistory.Product_Division__c= Map_ProductDivandProduct.get(p);
                    phistory.Account_Profile__c = profileid;
                    purchasehistory.add(phistory);
                //}
            }
        }
        
    }
    Public pagereference finalsave()
    {
        update wins;
        update openopps;
        update PYwins;
        List<Contact> Con_Update = new List<COntact>();
        //If(Profileid!= Null && profileid!= '')
        //{
            System.debug('theProfile----204-->'+theProfile);
            Upsert theProfile;
            System.debug('****'+keycon);
            for(AccountContactRole acr: keycon)
            {
                If(acr.contact.Degre_of_influence__c!=Map_ContachWDI.get(acr.Contact.id))
                {
                    Con_Update.add(new Contact(id=acr.Contact.id,Degre_of_influence__c = acr.contact.Degre_of_influence__c));
                }
            }
            Update Con_Update;
            /*For(COntact c_Out:[select id, AccountId, firstname, title, email, phone, MailingPostalCode, Key_Contact__c, Degre_of_influence__c from Contact where accountid=:Acountid])
            {
                System.debug('C_Out-->'+C_out);
                For(Contact C_in: Con)
                {
                    System.debug('C_in-->'+C_in);
                    If(c_Out.id==c_in.id)
                    {
                        System.debug('In side id match');
                        If(c_Out.Key_Contact__c != c_in.Key_Contact__c || c_out.Degre_of_influence__c != c_in.Degre_of_influence__c)
                        {
                            System.debug('In side update find');
                            Con_Update.add(c_in);
                        }
                    }
                }
            }
            If(Con_Update.size()>0)
            {
                Update Con_Update;
            }*/
            list<purchase_history__c> phistory = new list<purchase_history__c>(); 
            for(purchase_history__c pu : purchasehistory){
                if(pu.Account_profile__c==null){
                    pu.Account_profile__c=theprofile.id;
                }
                phistory.add(pu);
            }      
            Upsert Phistory;
            List<Product_Share__c> MobileProductShare_Upd = New List<Product_Share__c>();
            If(MobileProductShare.size()>0)
            {
                For(Product_Share__c ps: MobileProductShare)
                {
                    if(ps.Account_Profile__c == Null)
                    {
                        ps.Account_Profile__c = theProfile.id;
                        MobileProductShare_Upd.add(ps);
                    }Else
                    {
                        MobileProductShare_Upd.add(ps);
                    }
                }
                Upsert MobileProductShare_Upd;
            }
            List<Product_Share__c> ItProductShare_Upd = New List<Product_Share__c>();
            If(ItProductShare.size()>0)
            {
                For(Product_Share__c ps: ItProductShare)
                {
                    if(ps.Account_Profile__c == Null)
                    {
                        ps.Account_Profile__c = theProfile.id;
                        ItProductShare_Upd.add(ps);
                    }Else
                    {
                        ItProductShare_Upd.add(ps);
                    }
                }
                Upsert ItProductShare_Upd;
            }
        //}
        //upsert theprofile;
        PageReference pageRef = new PageReference('/apex/Account_Profile_View_Page?id='+theProfile.id);
        pageRef.setRedirect(true);
        return pageRef;
    }
    Public pagereference finalcancel()
    {
        If(profileid!= Null && profileid!='')
        {
            PageReference pageRef = new PageReference('/apex/Account_Profile_View_Page?id='+profileid);
            pageRef.setRedirect(true);
            return pageRef;
        }Else
        {
            PageReference pageRef = new PageReference('/'+Acountid);
            pageRef.setRedirect(true);
            return pageRef;
        }
    }
     Public pagereference ViewEdit()
     {
            PageReference pageRef = new PageReference('/apex/Account_Profile?id='+profileid+'&Accountid='+acountid);
            pageRef.setRedirect(true);
            return pageRef;         
     }
     Public pagereference ViewasPDF()
     {
            PageReference pageRef = new PageReference('/apex/Account_Profile_View_PDF?id='+profileid+'&Accountid='+acountid);
            pageRef.setRedirect(true);
            return pageRef;
     }  
}     
    
    // This code is for Graphs in view page
    /*
    public List<PieWedgeData> getPieData()
    {
        List<PieWedgeData> data = new List<PieWedgeData>();
        for(purchase_history__c ph: PurchaseHistory)
        {
            data.add(new PieWedgeData(Ph.Name,ph.PurchaseAmount__c));
        }
        return data;
    }
    List<Account_profile__c> profilelist = [select id,Carrier_Mix__c,IL_CL_Mix__c from Account_profile__c where id=:profileid];
    public List<piechart> getChart2Data()
    {
        List<piechart> data = new List<piechart>();
            data.add(new piechart('IL Mix - '+theProfile.IL_Mix__c+'%',theProfile.IL_Mix__c));
            data.add(new piechart('CL Mix - '+theProfile.CL_Mix__c+'%',theProfile.CL_Mix__c));
        return data;
    }
    public List<piechart> getcarrierData()
    {
        List<piechart> data = new List<piechart>();
            data.add(new piechart('ATT - '+theProfile.ATT_Carrier_Mix__c+'%',theProfile.ATT_Carrier_Mix__c));
            data.add(new piechart('  T-Mobile - '+theProfile.TMobile_Carrier_Mix__c+'%',theProfile.TMobile_Carrier_Mix__c));
            data.add(new piechart('Verizon - '+theProfile.Verizon_Carrier_Mix__c+'%',theProfile.Verizon_Carrier_Mix__c));
            data.add(new piechart('Sprint - '+theProfile.Sprint_Carrier_Mix__c+'%',theProfile.Sprint_Carrier_Mix__c));
            data.add(new piechart('Other Carrier - '+theProfile.Others_Carrier_Mix__c+'%',theProfile.Others_Carrier_Mix__c));
        return data;
    }
    public List<AccountRevenueData> getAccountPieData()
    {
        List<AccountRevenueData> data = new List<AccountRevenueData>();
        data.add(new AccountRevenueData('Most Recent Year',Acc.AnnualRevenue));
        data.add(new AccountRevenueData('Prior Year',acc.Prior_Year_Revenue__c));
        System.debug('Data-->'+data);
        return data;
    }
    public List<AccountEmployees> getAccountEmpData()
    {
        List<AccountEmployees> data = new List<AccountEmployees>();
        data.add(new AccountEmployees('Most Recent Year',Acc.NumberOfEmployees));
        data.add(new AccountEmployees('Prior Year',acc.Prior_Year_Number_of_Employees__c));
        System.debug('Data-->'+data);
        return data;
    }
     public class PieWedgeData
    {
        public String name { get; set; }
        public Decimal data { get; set; }
        public PieWedgeData(String name, Decimal data)
        {
            this.name = name;
            this.data = data;
        }
    } 
    public class GrowthWrap
    {
        public String Flag{ get; set; }
        public Decimal data { get; set; }
        public GrowthWrap(String Flag, Decimal data)
        {
            this.Flag= flag;
            this.data = data;
        }
    }
    public class AccountRevenueData
    {
        public string name { get; set; }
        public Decimal data { get; set; }
        public AccountRevenueData(String name, Decimal data)
        {
            this.name = name;
            this.data = data;
        }
    }
    public class AccountEmployees
    {
        public string name { get; set; }
        public Decimal data { get; set; }
        public AccountEmployees(String name, Decimal data)
        {
            this.name = name;
            this.data = data;
        }
    }
    public class piechart
    {
        public String name { get; set; }
        public Decimal data { get; set; }
        public piechart(String name, Decimal data)
        {
            this.name = name;
            this.data = data;
        }
    }  
    public class Carrierchart
    {
        public String name { get; set; }
        public Decimal data { get; set; }
        public Carrierchart(String name, Decimal data)
        {
            this.name = name;
            this.data = data;
        }
    } 
    */