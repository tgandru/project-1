@istest(seealldata = true)
Public class AccountProfileExt_V1_test
{
    Public static testmethod void AccountProfileExt_V1_test1()
    {
        Account testAccount = new Account(Name='Test Company Name123',SAP_Company_Code__c='012345678');
        insert testAccount;
        Pagereference pg = page.Account_profile; 
        test.setcurrentpage(pg);
        ApexPages.currentPage().getparameters().put('id',Null);    
        //apexpages.currentpage().getparameters().put('AccountId',String.valueof(testAccount.id));
        ApexPages.currentPage().getparameters().put('AccountId','0013600000SaeSJAAZ');
        Account_Profile__c ap = new Account_Profile__c(FiscalYear__c = '2016');        
        ApexPages.StandardController sc = new ApexPages.StandardController(ap);
        AccountProfileExt_V1 accprof = new AccountProfileExt_V1(sc); 
        accprof.FinalSave();
        accprof.finalcancel();
        accprof.ViewEdit();
        accprof.ViewasPDF();  
        
        
        Pagereference pg1 = page.Account_profile; 
        test.setcurrentpage(pg1);
        ApexPages.currentPage().getparameters().put('id',ap.id);    
        //apexpages.currentpage().getparameters().put('AccountId',String.valueof(testAccount.id));
        ApexPages.currentPage().getparameters().put('AccountId','0013600000SaeSJAAZ');
        Account_Profile__c ap1 = new Account_Profile__c();        
        ApexPages.StandardController sc1 = new ApexPages.StandardController(ap1);
        AccountProfileExt_V1 accprof1 = new AccountProfileExt_V1(sc1); 
        accprof1.FinalSave();        
        
        
    }
}