//Controller Class for AccountProfile, AccountProfileViewPage, AccountProfilePDFView pages
//Created: 12/13/2018
//Author: Thiru
//Functionality: get all the information using this apex class to Tier3 Account Profile Entry/View/PDF Screen.

public class AccountProfileExt_V2 {
    public Account_Profile__c theProfile {get;set;}
    Public Account Acc{get;Set;}
    public string Acountid{get;set;}
    public string profileid{get;set;}
    public List<AccountTeamMember> AccntTeam{get;set;}
    //public List<CL_Special_Purpose__c> CLSPList{get;set;} //Commented on 02/26/2019 to remove seperate object for CL Business Purpose 
    public integer SpecialPuposeLines=3;
    //public List<Account_Profile_EcoSystem_Partners__c> ESpartners{get;set;}//Commented on 03/26/2019 to remove seperate object for Ecosystem Partners
    public Map<String,List<String>> DependentValues{get;set;}
    public Map<String,List<SelectOption>> DependentSelectOptions{get;set;}
    public List<Key_Initiatives_Actions__c> KeyInitiatives{get;set;}
    public integer KeyInitiativesLines=3;
    public List<Account_Profile_Contact__c> AccntProfContacts{get;set;}
    public Map<integer,Account_Profile_Contact__c> AccntProfContactsMap{get;set;}
    public integer AccntProfContactsLines=5;
    public List<RevenueWrapper> WrapSamsungRevenue{get;set;}
    public integer PYear{get;set;}
    public integer Previous2Years{get;set;}
    public List<Account_Profile_IL_CL_Install_Base__c> ILCLInfo{get;set;}
    public List<ILCLWrapper> ILCLwrapperList{get;set;}
    public integer SmartphoneILCL{get;set;}
    public integer TabletILCL{get;set;}
    public List<Account_Profile_Sales_Goals__c> salesGoals{get;set;}
	public List<SalesGoalsWrapper> SalesGoalsWrapperList{get;set;}
    public List<ARWrapper> ARwrapList{get;set;}
    
    public integer conIdtoRemove{get;set;}
    public List<RevenueWrapper> WrapPYSamsungRevenue{get;set;}//for PDF
    public List<RevenueWrapper> WrapPPYSamsungRevenue{get;set;}//for PDF
    
    public String CY {get;set;}
    public String NY {get;set;}
    public boolean showFYselection{get;set;}
    public boolean showEditButton{get;set;}
    
    //PDF text fields
    public String addInfo{get;set;}
    public String aboutCustomer{get;set;}
    
    public List<EcosystemWrapper> EcosystemNames{get;set;}
    
    public AccountProfileExt_V2(ApexPages.StandardController controller) {
        profileid = Apexpages.currentpage().getParameters().get('id');       
        Acountid = Apexpages.currentpage().getParameters().get('AccountId');
        
        //Display Edit button based on User Permission.
        showEditButton = false;
        if(profileid!=null){
            UserRecordAccess ura = [SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE UserId =:Userinfo.getUserId() AND RecordId =:profileid];
            if(ura.HasEditAccess==true){
                showEditButton=true;
            }
        }
        
        Id EndCustProfileRecTypeId = Schema.SObjectType.Account_Profile__c.getRecordTypeInfosByName().get('End Customer').getRecordTypeId();
        
        //Fiscal Year Section Info
        showFYselection=False;
        CY = String.valueOf(system.today().year());
        NY = String.valueOf(system.today().year()+1);
        
        Acc = new Account();
        AccntTeam = new List<AccountTeamMember>();
        AccntProfContacts = new List<Account_Profile_Contact__c>();
        AccntProfContactsMap = new Map<integer,Account_Profile_Contact__c>();
        //CLSPList = new List<CL_Special_Purpose__c>(); //Commented on 02/26/2019 to remove seperate object for CL Business Purpose
        KeyInitiatives = new List<Key_Initiatives_Actions__c>();
        //ESpartners = new List<Account_Profile_EcoSystem_Partners__c>();//Commented on 03/26/2019 to remove seperate object for CL Business Purpose
        DependentSelectOptions = new Map<String,List<SelectOption>>();
        SmartphoneILCL=0;
        TabletILCL=0;
        salesGoals = new List<Account_Profile_Sales_Goals__c>();
		SalesGoalsWrapperList = new List<SalesGoalsWrapper>();
        
        //Get dependent Picklist values to display in Ecosystem/Partners section 
        //Commented below 2 lines on 03/26/2019 to remove seperate object for Ecosystem Partners
        /*Schema.DescribeFieldResult fieldResult = Account_Profile_EcoSystem_Partners__c.Ecosystem__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();*/
        
        Schema.DescribeFieldResult salesGoalsTypeRes = Account_Profile_Sales_Goals__c.Type__c.getDescribe();
        List<Schema.PicklistEntry> salesGoalsTypeValues = salesGoalsTypeRes.getPicklistValues();
        
        DateTime dtGmt = system.now();
        String dtEST = dtGmt.format('yyyy-mm-dd HH:mm:ss','America/New_York');
        integer CYear = integer.valueOf(dtEST.left(4));
        
        if(profileid != Null && profileid!= '')
        {
            theProfile = [Select Id, name,Account__c, Account__r.name, FiscalYear__c, MDMPartner__c,Carriers__c,CEO__c,CIO__c,CFO__c,
                          Additional_Info__c,ADL_Approved_Device__c,ADL_Approved_Device_Other__c,About_the_Customer__c,
                          Account__r.Industry,Account__r.BillingCity,Account__r.BillingState,Account__r.AnnualRevenue,Account__r.NumberOfEmployees,
                          Business_Purpose1__c,BusinessPurpose1_Units__c,BusinessPurpose1_Vendor__c,
                          Business_Purpose2__c,BusinessPurpose2_Units__c,BusinessPurpose2_Vendor__c,
                          Business_Purpose3__c,BusinessPurpose3_Units__c,BusinessPurpose3_Vendor__c,
                          MDM_Name__c, MDM_Other__c,
                          CRM_Name__c, CRM_Other__c,
                          Cloud_Name__c, Cloud_Other__c, 
                          System_Integrator_Name__c, System_Integrator_Other__c,
                          Carriers_Name__c, Carriers_Other__c,
                          VDI_Name__c, VDI_Other__c,
                          Tech_Support_Name__c, Tech_Support_Other__c,
                          Primary_Reseller_Name__c, Primary_Reseller_Other__c,
                          EPP_Name__c, EPP_Other__c,
                          Consulting_Name__c, Consulting_Other__c,
                          B2B_Apps_Name__c, B2B_Apps_Other__c 
                          from Account_Profile__c where Id = :profileid];
            Acountid = theProfile.Account__c;
            system.debug('Acountid---->' + Acountid);
			CYear = integer.valueOf(theProfile.FiscalYear__c);

            //Set PDF file Name
            String fileName = theProfile.name + '.pdf';
            Apexpages.currentPage().getHeaders().put('content-disposition', 'inline; filename='+fileName);
            
            //Set PDF text fields length
            /*addInfo = '';
            aboutCustomer = '';
            if(theProfile.Additional_Info__c!=null) addInfo = theProfile.Additional_Info__c;
            System.debug('theProfile.Additional_Info__c---->'+addInfo);
            if(addInfo.length()>256) addInfo = addInfo.left(256)+' ...';
            System.debug('addInfo---->'+addInfo);
            if(theProfile.About_the_Customer__c!=null) aboutCustomer = theProfile.About_the_Customer__c;
            System.debug('theProfile.About_the_Customer__c---->'+aboutCustomer);
            if(aboutCustomer.length()>256) aboutCustomer = aboutCustomer.left(256)+' ...';
            System.debug('aboutCustomer---->'+aboutCustomer);*/
            
            //Get Account Info
            Acc = [Select id,name,Industry,billingcity,billingstate,AnnualRevenue,NumberOfEmployees from Account where id=:Acountid];Acc = [Select id,name,Industry,billingcity,billingstate,AnnualRevenue,NumberOfEmployees from Account where id=:Acountid];
            
            //Get CL Special Purpose information. //Commented on 02/26/2019 to remove seperate object for CL Business Purpose
            /*CLSPList = [Select id,Account_Profile__c,Purpose__c,Units__c,Vendor__c from CL_Special_Purpose__c where Account_Profile__c=:profileid order by name asc];
            SpecialPuposeLines = SpecialPuposeLines-CLSPList.size();
            for(integer i=0;i<SpecialPuposeLines;i++){
                CL_Special_Purpose__c CLsp = new CL_Special_Purpose__c();
                CLSPList.add(CLsp);
            }*/
            
            //Get Ecosystem/Partners
            //Commented on 03/26/2019 to remove seperate object for Ecosystem Partners
            /*ESpartners = [Select id,Account_Profile__c,Ecosystem__c,Name__c,Other__c from Account_Profile_EcoSystem_Partners__c where Account_Profile__c=:profileid];
            if(ESpartners.size()==0 || ESpartners==null){
                for(Schema.PicklistEntry pickListVal : ple){
                    string pickVal = pickListVal.getLabel();
                    system.debug('PickVal---->'+pickVal);
                    Account_Profile_EcoSystem_Partners__c esp = new Account_Profile_EcoSystem_Partners__c();
                    esp.Ecosystem__c=pickVal;
                    ESpartners.add(esp);
                }
            }*/
            
            //Get Key Initiatives
            //KeyInitiatives = [Select id,Opportunity__c,Customer_Key_Projects__c,Samsung_Action_Plan__c,Timeline__c,Outcome__c,Account_Profile__c from Key_Initiatives_Actions__c where Account_Profile__c=:profileid order by name asc];
            KeyInitiatives = [Select id,Opportunity__c,Opportunity__r.Opportunity_number__c,Opportunity__r.closedate,Opportunity__r.nextstep,Account_Profile__c from Key_Initiatives_Actions__c where Account_Profile__c=:profileid order by name asc];
            KeyInitiativesLines = KeyInitiativesLines-KeyInitiatives.size();
            for(integer i=0;i<KeyInitiativesLines;i++){
                KeyInitiatives.add(new Key_Initiatives_Actions__c());
            }
            
            //Get Account Profile Contacts
            AccntProfContacts = [Select id,Account_Profile__c,Contact__c,Next_Best_Action__c,Position__c,Samsung_Contact__c,Contact__r.Title,Contact__r.Job_Role__c,Contact__r.Job_Position__c,Contact__r.Account.Name from Account_Profile_Contact__c where Account_Profile__c=:profileid order by createddate asc];
            if(AccntProfContacts.size()<=AccntProfContactsLines) AccntProfContactsLines = AccntProfContactsLines-AccntProfContacts.size();
            for(integer i=0;i<AccntProfContactsLines;i++){
                AccntProfContacts.add(new Account_Profile_Contact__c());
            }
            
            salesGoals = [Select id,Account_Profile__c,Type__c,Q1__c,Q2__c,Q3__c,Q4__c,Total__c from Account_Profile_Sales_Goals__c where Account_Profile__c=:profileid];
            
        }
        Else
        {
            theProfile = (Account_Profile__c)controller.getRecord();
            theProfile.Account__c = Acountid;
            theProfile.RecordTypeId = EndCustProfileRecTypeId;
            
            showFYselection=true;//show fiscal year selection
            
            //Get Account Info
            Acc = [Select id,name,Industry,billingcity,billingstate,AnnualRevenue,NumberOfEmployees from Account where id=:Acountid];Acc = [Select id,name,Industry,billingcity,billingstate,AnnualRevenue,NumberOfEmployees from Account where id=:Acountid];
            
            //Commented on 02/26/2019 to remove seperate object for CL Business Purpose
            /*for(integer i=0;i<SpecialPuposeLines;i++){
                CLSPList.add(new CL_Special_Purpose__c());
            }*/
            //Commented below for loop on 03/26/2019 to remove seperate object for Ecosystem Partners
            /*for(Schema.PicklistEntry pickListVal : ple){
                string pickVal = pickListVal.getLabel();
                Account_Profile_EcoSystem_Partners__c esp = new Account_Profile_EcoSystem_Partners__c();
                esp.Ecosystem__c=pickVal;
                ESpartners.add(esp);
            }*/
            
            for(integer i=0;i<KeyInitiativesLines;i++){
                KeyInitiatives.add(new Key_Initiatives_Actions__c());
            }
            
            for(integer i=0;i<AccntProfContactsLines;i++){
                AccntProfContacts.add(new Account_Profile_Contact__c());
                AccntProfContactsMap.put(i,new Account_Profile_Contact__c());
            }
        }
        
        //Ecosystem Partner List
        wrapEcosystemSection(theProfile);
		
		//Calculate previous 2 years
        PYear = CYear-1;
        Previous2Years = PYear-1;
        
        for(integer i=0;i<AccntProfContacts.Size();i++){
            AccntProfContactsMap.put(i,AccntProfContacts[i]);
        }
        
        //set the Sales goals rows with null checking.
        if(salesGoals.Size()>0){
            for(Account_Profile_Sales_Goals__c sg : salesGoals){
                if(sg.Type__c.contains('Actuals')){
                    sg.Q1__c=0;
                    sg.Q2__c=0;
                    sg.Q3__c=0;
                    sg.Q4__c=0;
                }else{
                    if(sg.Q1__c==null) sg.Q1__c=0;
                    if(sg.Q2__c==null) sg.Q2__c=0;
                    if(sg.Q3__c==null) sg.Q3__c=0;
                    if(sg.Q4__c==null) sg.Q4__c=0;
                }
            }
        }else{
            for(Schema.PicklistEntry val : salesGoalsTypeValues){
                salesGoals.add(new Account_Profile_Sales_Goals__c(type__c=string.valueof(val.getLabel()),Q1__c=0,Q2__c=0,Q3__c=0,Q4__c=0));
            }
        }
        
        //Get IL/CL info
        Set<String> InstallBaseTypes = new Set<String>{'Smartphone','Tablet'};
        Schema.DescribeFieldResult ILCLvalResult = Account_Profile_IL_CL_Install_Base__c.IL_CL__c.getDescribe();
        List<Schema.PicklistEntry> ILCLvalues = ILCLvalResult.getPicklistValues();
        
        ILCLInfo = new List<Account_Profile_IL_CL_Install_Base__c>();
        ILCLwrapperList = new List<ILCLWrapper>();
        ILCLInfo = [Select id,Account_Profile__c,Product__c,IL_CL__c,Samsung__c,LO__c,Other__c,Total__c from Account_Profile_IL_CL_Install_Base__c where Account_Profile__c=:profileid order by name asc];
        if(ILCLInfo.size()>0){
            for(Account_Profile_IL_CL_Install_Base__c ilcl : ILCLInfo){
                ILCLWrapper w = new ILCLWrapper();
                w.product = ilcl.Product__c;
                w.ILCL = ilcl.IL_CL__c;
                w.Samsung = ilcl.Samsung__c;
                w.LO = ilcl.LO__c;
                w.Other = ilcl.Other__c;
                w.Total = ilcl.Total__c;
                w.SamsungShare = getARpercent(w.total,w.samsung);
                w.LOShare = getARpercent(w.total,w.LO);
                w.OtherShare = getARpercent(w.total,w.Other);
                w.TotalShare = getARpercent(w.total,w.total);
                ILCLwrapperList.add(w);
            }
            for(ILCLWrapper w : ILCLwrapperList){
                if(w.product=='Smartphone'){
                    SmartphoneILCL=SmartphoneILCL+Integer.valueOf(w.Total);
                }
                if(w.product=='Tablet'){
                    TabletILCL=TabletILCL+Integer.valueOf(w.Total);
                }
            }
        }
        else{
            for(String s : InstallBaseTypes){
                for(Schema.PicklistEntry pickListVal : ILCLvalues){
                    string pickVal = pickListVal.getLabel();
                    ILCLInfo.add(new Account_Profile_IL_CL_Install_Base__c(product__c=s,IL_CL__c=pickVal,Samsung__c=0,LO__c=0,Other__c=0));
                }
            }
        }
        
        //Get ADL Approved Device
        List<Order> ADLdemo = new List<Order>();
        Set<String> DeviceTypes = new Set<String>();
        ADLdemo = [Select id,Device_Type__c from Order where Accountid=:Acountid and (type='EAP' or type='ADL') and ADL_Status__c='Approved' and Calendar_Year(Completion_Date__c)=:CYear];
        for(Order ordr : ADLdemo){
            DeviceTypes.add(ordr.Device_Type__c);
        }
        String DevTypeStr=null;
        for(String s : DeviceTypes){
            if(DevTypeStr==null)
                DevTypeStr = s;
            else
                DevTypeStr = DevTypeStr +', '+s;
        }
        theProfile.ADL_Approved_Device__c=DevTypeStr;
        
        AccntTeam = [Select id,UserId,TeamMemberRole from AccountTeamMember where Accountid=:Acountid];
        
        //get Revenue based on Won Opportunities
        Set<String> ProdTypeSet = new Set<String>{'Smartphones','KNOX','Services','Tablets'};
        Set<Integer> yearSet = new Set<Integer>{PYear,Previous2Years};
        WrapSamsungRevenue = new List<RevenueWrapper>();
        for(String s : ProdTypeSet){
            for(Integer i : yearSet){
                RevenueWrapper rw = new RevenueWrapper();
                rw.year=i;
                rw.ProductType=s;
                WrapSamsungRevenue.add(rw);
            }
        }
        
        for(OpportunityLineItem oli : [Select id,quantity,requested_price__c,totalprice,Opportunity.Closedate,ProductGroup__c,
                                             Opportunity.Channel_Carrier__c,Opportunity.IL_CL__c, Opportunity.recordtype.name
                                             from OpportunityLineItem 
                                             where Opportunity.Accountid=:Acountid 
                                                   and Opportunity.stagename=:Label.oppStageRollout 
                                                   and Calendar_Year(Opportunity.closedate)>=:Previous2Years
                                                   and ProductGroup__c in ('SMART_PHONE','TABLET','KNOX','SOLUTION [MOBILE]','ACCESSORY')])
        {
            integer year = (oli.Opportunity.Closedate).year();
            if(year==PYear || year==Previous2Years){
                if(!((oli.Opportunity.recordtype.name=='Mobile' || oli.Opportunity.recordtype.name=='Services') && (oli.Opportunity.IL_CL__c=='IL' || oli.Opportunity.IL_CL__c==null))){ //Calculate Samsung Revenue only for CL for Mobile, Services RecType Opps. Added on 01/08/2019
                    for(RevenueWrapper rw : WrapSamsungRevenue){
                        if((rw.year==PYear && year==PYear) || (rw.year==Previous2Years && year==Previous2Years)){
                            if(rw.ProductType=='Smartphones' && oli.ProductGroup__c=='SMART_PHONE'
                               || (rw.ProductType=='KNOX' && oli.ProductGroup__c=='KNOX')
                               || (rw.ProductType=='Services' && (oli.ProductGroup__c=='ACCESSORY' || oli.ProductGroup__c=='SOLUTION [MOBILE]'))
                               || (rw.ProductType=='Tablets' && oli.ProductGroup__c=='TABLET'))
                            {
                                rw.Revenue = rw.Revenue + oli.totalprice;
                                rw.Units = rw.Units + integer.valueOf(oli.quantity);
                            }
                        }
                    }
                }
            }else{
                integer mnth = (oli.Opportunity.Closedate).month();
                for(Account_Profile_Sales_Goals__c sg : salesGoals){
                    if(sg.Type__c.contains('Actuals') && 
                       ((sg.Type__c.contains('Carrier') && !sg.Type__c.contains('Non-Carrier') && oli.Opportunity.Channel_Carrier__c!=null && oli.Opportunity.Channel_Carrier__c=='Carrier')  
                       || (sg.Type__c.contains('Non-Carrier') && oli.Opportunity.Channel_Carrier__c!=null && oli.Opportunity.Channel_Carrier__c=='Channel') 
                       || (sg.Type__c.contains('KNOX') && oli.ProductGroup__c=='KNOX') 
                       || (sg.Type__c.contains('Services') && (oli.ProductGroup__c=='SOLUTION [MOBILE]' || oli.ProductGroup__c=='ACCESSORY'))
                       )
                      ){
                        if(mnth>=1&&mnth<=3){
                            if(sg.Q1__c==null) sg.Q1__c=0;
                            sg.Q1__c = sg.Q1__c + oli.totalprice;
                        }else if(mnth>=4&&mnth<=6){
                            if(sg.Q2__c==null) sg.Q2__c=0;
                            sg.Q2__c = sg.Q2__c + oli.totalprice;
                        }else if(mnth>=7&&mnth<=9){
                            if(sg.Q3__c==null) sg.Q3__c=0;
                            sg.Q3__c = sg.Q3__c + oli.totalprice;
                        }else{
                            if(sg.Q4__c==null) sg.Q4__c=0;
                            sg.Q4__c = sg.Q4__c + oli.totalprice;
                        }
                    }
                }
            }
        }
        
        //For PDF seperate Previous Year and 2 Years Prior Samsung Revenue
        WrapPYSamsungRevenue = new List<RevenueWrapper>();
        WrapPPYSamsungRevenue = new List<RevenueWrapper>();
        for(RevenueWrapper rw : WrapSamsungRevenue){
            rw.Revenue = rw.Revenue.round(System.RoundingMode.CEILING);
            if(rw.year==PYear){
                WrapPYSamsungRevenue.add(rw);
            }else{
                WrapPPYSamsungRevenue.add(rw);
            }
        }
        
        //Calculate sales goals A/R % only for Non-channel, Channel
        ARwrapList = new List<ARWrapper>();
        for(Account_Profile_Sales_Goals__c sg : salesGoals){
			SalesGoalsWrapper sgw = new SalesGoalsWrapper();
            sgw.type=sg.type__c;
            sgw.Q1=sg.Q1__c;
            sgw.Q1 = sgw.Q1.round(System.RoundingMode.CEILING);
            sgw.Q2=sg.Q2__c;
            sgw.Q2 = sgw.Q2.round(System.RoundingMode.CEILING);
            sgw.Q3=sg.Q3__c;
            sgw.Q3 = sgw.Q3.round(System.RoundingMode.CEILING);
            sgw.Q4=sg.Q4__c;
            sgw.Q4 = sgw.Q4.round(System.RoundingMode.CEILING);
            sgw.total=sgw.Q1+sgw.Q2+sgw.Q3+sgw.Q4;
            sgw.total = sgw.total.round(System.RoundingMode.CEILING);
            SalesGoalsWrapperList.add(sgw);
			
            //if(sg.Type__c.contains('Non-channel')&&sg.Type__c.contains('Forecast')){
            if(sg.Type__c.contains('Non-Carrier')&&sg.Type__c.contains('Forecast')){
                for(Account_Profile_Sales_Goals__c innersg : salesGoals){
                    //if(innersg.Type__c.contains('Non-channel')&&innersg.Type__c.contains('Actuals')){
                    if(innersg.Type__c.contains('Non-Carrier')&&innersg.Type__c.contains('Actuals')){
                        ARWrapper ar = new ARWrapper();
                        ar.type = 'Non-Carrier CL A/R%';
                        ar.q1AR = getARpercent(sg.Q1__c,innersg.Q1__c);
                        ar.q2AR = getARpercent(sg.Q2__c,innersg.Q2__c);
                        ar.q3AR = getARpercent(sg.Q3__c,innersg.Q3__c);
                        ar.q4AR = getARpercent(sg.Q4__c,innersg.Q4__c);
                        System.debug('Non-Carrier A/R--->'+ar);
                        ARwrapList.add(ar);
                    }
                }
            }//else if(!sg.Type__c.contains('Non-channel') && sg.Type__c.contains('Channel') && sg.Type__c.contains('Forecast')){
            else if(!sg.Type__c.contains('Non-Carrier') && sg.Type__c.contains('Carrier') && sg.Type__c.contains('Forecast')){
                for(Account_Profile_Sales_Goals__c innersg : salesGoals){
                    //if(!innersg.Type__c.contains('Non-channel') && innersg.Type__c.contains('Channel') && innersg.Type__c.contains('Actuals')){
                    if(!innersg.Type__c.contains('Non-Carrier') && innersg.Type__c.contains('Carrier') && innersg.Type__c.contains('Actuals')){
                        ARWrapper ar = new ARWrapper();
                        ar.type = 'Carrier A/R%';
                        ar.q1AR = getARpercent(sg.Q1__c,innersg.Q1__c);
                        ar.q2AR = getARpercent(sg.Q2__c,innersg.Q2__c);
                        ar.q3AR = getARpercent(sg.Q3__c,innersg.Q3__c);
                        ar.q4AR = getARpercent(sg.Q4__c,innersg.Q4__c);
                        System.debug('Carrier A/R--->'+ar);
                        ARwrapList.add(ar);
                    }
                }
            }
        }
        
    }
    
    public decimal getARpercent(decimal F, decimal A){
        decimal AR = 0;
        if(F>0){
            AR = (A/F)*100;
            AR=AR.setScale(1);
        }
        return AR;
    }
    
    public void removeCon(){
        List<Account_Profile_Contact__c> apCon = new List<Account_Profile_Contact__c>();
        
        for(integer i : AccntProfContactsMap.keyset()){
            if(i==conIdtoRemove){
                if(AccntProfContactsMap.get(i).id!=null){
                    apCon.add(AccntProfContactsMap.get(i));
                    AccntProfContactsMap.put(i,new Account_Profile_Contact__c());
                }else{
                    AccntProfContactsMap.put(i,new Account_Profile_Contact__c());
                }
            }
        }
        
        if(apCon.size()>0){
            delete apcon;
        }
    }
    public void addConRow(){
        integer i = AccntProfContactsMap.Size();
        system.debug('Con Map--->'+AccntProfContactsMap);
        AccntProfContactsMap.put(i,new Account_Profile_Contact__c());
    }
    
    Public pagereference finalsave()
    {
        //Upsert Account Profile
        System.debug('theProfile---->'+theProfile);
        //logic to save Ecosystem Partner section fields--03/26/2019
        for(EcosystemWrapper ew : EcosystemNames){
            String selected = null;
            for(SelectOption so : ew.rightOptions){
                if(selected!=null) selected=selected+';'+so.getValue();
                else selected=so.getValue();
            }
            theProfile.put(ew.NameFieldName,selected);
            theProfile.put(ew.OtherFieldName,ew.Other);
        }
        
        Upsert theProfile;
        
        System.debug('theProfile.name 1---->'+theProfile.name);
        if(acc!=null && theProfile.name==null){
            theProfile.name = acc.name +' Profile - '+theProfile.FiscalYear__c;
        }
        System.debug('theProfile.name 2---->'+theProfile.name);
        System.debug('theProfile.Account__c---->'+theProfile.Account__c);
        
        String attname = theProfile.name+'.pdf';
        //Store PDF as attachment -- Added on 01/09/2019
        List<Attachment> attList = [Select id,name from Attachment where ParentId=:theProfile.Account__c and name=:attname];
        if(attList.size()>0){
            delete attList;
        }
        if(!Test.isRunningTest()){
            PageReference ref = page.AccountProfilePDFView;
            ref.getParameters().put('id',theProfile.id);
            Attachment attach = new Attachment();
            // the contents of the attachment from the pdf
            attach.Body = ref.getContentAsPDF();
            // add the user entered name
            attach.Name = theProfile.name+'.pdf';
            attach.IsPrivate = false;
            // attach the pdf to the account
            attach.ParentId = theProfile.Account__c;
            insert attach;
        }
        //Save CL Special Purpose
        //Commented on 02/26/2019 to remove seperate object for CL Business Purpose
        /*List<CL_Special_Purpose__c> UpsertCLSPList = new List<CL_Special_Purpose__c>();
        for(CL_Special_Purpose__c CLsp : CLSPList){
            if(CLsp.Purpose__c!=null && CLsp.Purpose__c!=''){
                if(CLsp.Account_Profile__c==null){
                    CLsp.Account_Profile__c=theProfile.id;
                }
                UpsertCLSPList.add(CLsp);
            }
        }
        if(UpsertCLSPList.size()>0){
            upsert UpsertCLSPList;
        }*/
        
        //Commented below save logic on 03/26/2019 to remove seperate object for Ecosystem Partners
        //Ecosystem Partner Section Save logic
        /*for(Account_Profile_EcoSystem_Partners__c esp : ESpartners){
            if(esp.Account_Profile__c==null){
                esp.Account_Profile__c=theProfile.id;
            }
            if(esp.Name__c=='None'){
                esp.Name__c=null;
            }
        }
        upsert ESpartners;*/
        
        //Key Initiatives/Actions Save logic
        List<Key_Initiatives_Actions__c> upsertKI = new List<Key_Initiatives_Actions__c>();
        List<Key_Initiatives_Actions__c> deleteKI = new List<Key_Initiatives_Actions__c>();
        for(Key_Initiatives_Actions__c kia : KeyInitiatives){
            if(kia.Opportunity__c!=null){
                if(kia.Account_Profile__c==null){
                    kia.Account_Profile__c=theProfile.id;
                }
                upsertKI.add(kia);
            }else if(kia.id!=null){
                deleteKI.add(kia);
            }
        }
        if(upsertKI.size()>0){
            upsert upsertKI;
        }
        if(deleteKI.size()>0){
            delete deleteKI;
        }
        //upsert KeyInitiatives;
        
        //Account Profile Contacts Save
        Set<string> conIdSet = new Set<string>();
        List<Account_Profile_Contact__c> upsertList = new List<Account_Profile_Contact__c>();
        for(Account_Profile_Contact__c con : AccntProfContactsMap.values()){
            if(con.Contact__c!=null){
                if(con.Account_Profile__c==null){
                    con.Account_Profile__c=theProfile.id;
                }
                upsertList.add(con);
                conIdSet.add(con.Contact__c);
            }
        }
        
        Map<id,Contact> conMap = new Map<id,Contact>();
        if(conIdSet.size()>0){
            conMap = new Map<id,Contact>([select id,AccountId from Contact where id=:conIdSet]);
        }
        
        if(upsertList.size()>0){
          /*  for(Account_Profile_Contact__c apc : upsertList){
                if(conMap.containsKey(apc.Contact__c) && conMap.get(apc.Contact__c).AccountId!=Acountid){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'You cannot add contacts that are not on this account :<b>'+Acc.Name+'</b>'));
                    return null;
                }
            } */
            upsert upsertList;
        }
        
        //IL/CL section Save
        for(Account_Profile_IL_CL_Install_Base__c ilcl : ILCLInfo){
            if(ilcl.Account_Profile__c==null){
                ilcl.Account_Profile__c=theProfile.id;
            }
            if(ilcl.Samsung__c==null){
                ilcl.Samsung__c=0;
            }
            if(ilcl.LO__c==null){
                ilcl.LO__c=0;
            }
            if(ilcl.Other__c==null){
                ilcl.Other__c=0;
            }
        }
        upsert ILCLInfo;
        
        //Account Profile Sales Goals save logic
        for(SalesGoalsWrapper sgw : SalesGoalsWrapperList){
            for(Account_Profile_Sales_Goals__c sg : salesGoals){
                if(sg.type__c==sgw.type){
                    sg.q1__c=sgw.Q1;
                    sg.q2__c=sgw.Q2;
                    sg.q3__c=sgw.Q3;
                    sg.q4__c=sgw.Q4;
                }
                if(sg.Account_Profile__c==null){
                    sg.Account_Profile__c=theProfile.id;
                }
            }
        }
        /*for(Account_Profile_Sales_Goals__c sg : salesGoals){
            if(sg.Account_Profile__c==null){
                sg.Account_Profile__c=theProfile.id;
            }
        }*/
        upsert salesGoals;
        
        //Redirect to view page upon Save
        PageReference pageRef = new PageReference('/apex/AccountProfileViewPage?id='+theProfile.id);
        pageRef.setRedirect(true);
        return pageRef;
    }
    Public pagereference finalcancel()
    {
        If(profileid!= Null && profileid!='')
        {
            PageReference pageRef = new PageReference('/apex/AccountProfileViewPage?id='+profileid);
            pageRef.setRedirect(true);
            return pageRef;
        }Else
        {
            PageReference pageRef = new PageReference('/'+Acountid);
            pageRef.setRedirect(true);
            return pageRef;
        }
    }
    
    //Next Button
    public void next(){
        List<Account_Profile__c> accProfs = [Select id from Account_Profile__c where Account__c=:Acountid and FiscalYear__c=:theProfile.FiscalYear__c];
        if(accProfs.size()>0){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'There is an existing profile for this Account and Year. Please update existing profile'));
        }else{
            showFYselection=false;
        }
    }
    
    Public pagereference RedirectToEdit()
    {
        PageReference pageRef = new PageReference('/apex/AccountProfile?id='+profileid);
        pageRef.setRedirect(true);
        return pageRef;
    }
    Public pagereference RedirectToPDF()
    {
        PageReference pageRef = new PageReference('/apex/AccountProfilePDFView?id='+profileid);
        pageRef.setRedirect(true);
        
        //logic for Saving PDF to Account's Attachments
        /*PageReference ref = Page.AccountProfilePDFView;
        ref.getParameters().put('id',profileid);
        ContentVersion cont = new ContentVersion();
        cont.Title = theProfile.name;
        cont.PathOnClient = Acc.Name;
        cont.VersionData = ref.getContent();
        cont.Origin = 'H';
        insert cont;
        
        ContentDocument doc = [Select id from ContentDocument where id=:cont.ContentDocumentId];
        
        ContentDocumentLink conDocLink = new ContentDocumentLink();
        conDocLink.ContentDocumentId = doc.id;
        conDocLink.LinkedEntityId = Acountid;
        conDocLink.ShareType = 'I';
        insert conDocLink;*/
        
        
        return pageRef;
    }
    
    Public class RevenueWrapper{
        public string ProductType{get;set;}
        public Integer year{get;set;}
        public Decimal Revenue{get;set;}
        public integer Units{get;set;}
        public RevenueWrapper(){
            Revenue=0;
            Units=0;
        }
    }
    
    Public class ILCLWrapper{
        public string product{get;set;}
        public string ILCL{get;set;}
        public Decimal Samsung{get;set;}
        public Decimal LO{get;set;}
        public Decimal Other{get;set;}
        public Decimal Total{get;set;}
        public Decimal SamsungShare{get;set;}
        public Decimal LOShare{get;set;}
        public Decimal OtherShare{get;set;}
        public Decimal TotalShare{get;set;}
        public ILCLWrapper(){
            Samsung=0;
            LO=0;
            Other=0;
            Total=0;
            SamsungShare=0;
            LOShare=0;
            OtherShare=0;
            TotalShare=0;
        }
    }
    
    public class ARWrapper{
        public string type{get;set;}
        public string header{get;set;}
        public decimal q1AR{get;set;}
        public decimal q2AR{get;set;}
        public decimal q3AR{get;set;}
        public decimal q4AR{get;set;}
        public ARWrapper(){
            header='Actual';
            q1AR=0;
            q2AR=0;
            q3AR=0;
            q4AR=0;
        }
    }
	
	public class SalesGoalsWrapper{
        public string type{get;set;}
        public decimal total{get;set;}
        public decimal q1{get;set;}
        public decimal q2{get;set;}
        public decimal q3{get;set;}
        public decimal q4{get;set;}
        public SalesGoalsWrapper(){
            total=0;
            q1=0;
            q2=0;
            q3=0;
            q4=0;
        }
    }
    
    //Method to set the Ecosystem Section Values--Added--03/26/2019
    public void wrapEcosystemSection(Account_Profile__c ap){
        EcosystemNames = new List<EcosystemWrapper>();
        
        for(Schema.FieldSetMember f : SObjectType.Account_Profile__c.FieldSets.Ecosystem_Partners.getFields()){
            Set<String> selOptionSet = new Set<String>();
            List<SelectOption> selOptions = new List<SelectOption>();
            String fname = f.getfieldPath();
            String fvalue;
            if(ap.get(fname)!=null){
                String selOptStr = String.valueOf(ap.get(fname));
                for(String s : selOptStr.split(';')){
                    selOptions.add(new SelectOption(s,s));
                    selOptionSet.add(s);
                    if(fvalue!=null) fvalue = fvalue+'; '+s;
                    else fvalue = s;
                }
            }
            
            Schema.sObjectType objType = Account_Profile__c.getSObjectType();
            Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
            map <String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
            list<Schema.PicklistEntry> pleValues = fieldMap.get(fname).getDescribe().getPickListValues();
            
            List<SelectOption> values = new List<SelectOption>();
            for(Schema.PicklistEntry pl : pleValues){
                if(!selOptionSet.contains(pl.getValue())){
                    string s = pl.getValue();
                    values.add(new SelectOption(s,s));
                }
            }
            
            EcosystemWrapper ew = new EcosystemWrapper();
            ew.NameFieldName=fname;
            ew.EcosystemName=fvalue;
            
            String Otherfname = fname.replace('_Name__c','_Other__c');
            
            ew.OtherFieldName=Otherfname;
            
            fname = fname.replace('_Name__c','');
            fname = fname.replace('_','');
            
            ew.Name=fname;
            ew.Other=String.valueOf(ap.get(Otherfname));
            ew.leftOptions=values;
            ew.rightOptions=selOptions;
            EcosystemNames.add(ew);
            
        }
    }
    //Wrapper for Ecosystem Partners Section--Added--03/26/2019
    public class EcosystemWrapper {
        public String Name {get;set;}
        public String EcosystemName {get;set;}
        public String Other {get;set;}
        public String NameFieldName {get;set;}
        public String OtherFieldName {get;set;}
        public List<SelectOption> leftOptions { get; set; }
        public List<SelectOption> rightOptions { get; set; }
    
        public EcosystemWrapper() {
            this.leftOptions = new List<SelectOption>();
            this.rightOptions = new List<SelectOption>();
            this.EcosystemName=null;
        }
    }
    
}