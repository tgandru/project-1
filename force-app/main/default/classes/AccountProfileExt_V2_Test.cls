@istest(seealldata = true)
Public class AccountProfileExt_V2_Test
{
    Public static testmethod void AccountProfileExt_V1_test1()
    {
        Account testAccount = new Account(Name='Test Company Name123',SAP_Company_Code__c='012345678');
        insert testAccount;
        Pagereference pg = page.Accountprofile; 
        test.setcurrentpage(pg);
        ApexPages.currentPage().getparameters().put('id',Null);    
        //apexpages.currentpage().getparameters().put('AccountId',String.valueof(testAccount.id));
        ApexPages.currentPage().getparameters().put('AccountId','0013600000SaeSJAAZ');
        Account_Profile__c ap = new Account_Profile__c(FiscalYear__c = '2018');        
        ApexPages.StandardController sc = new ApexPages.StandardController(ap);
        
        AccountProfileViewredirect redirect = new AccountProfileViewredirect(sc);
        redirect.redirect();
        
        AccountProfileEditRedirect edit = new AccountProfileEditRedirect(sc);
        edit.redirect();
        
        AccountProfileExt_V2 accprof = new AccountProfileExt_V2(sc); 
        accprof.FinalSave();
        accprof.finalcancel();
        //accprof.RedirectEdit();
        //accprof.RedirectPDF();  
        
        Pagereference pg1 = page.Accountprofile; 
        test.setcurrentpage(pg1);
        ApexPages.currentPage().getparameters().put('id',ap.id);    
        //apexpages.currentpage().getparameters().put('AccountId',String.valueof(testAccount.id));
        ApexPages.currentPage().getparameters().put('AccountId','0013600000SaeSJAAZ');
        Account_Profile__c ap1 = new Account_Profile__c();        
        ApexPages.StandardController sc1 = new ApexPages.StandardController(ap1);
        
        AccountProfileExt_V2 accprof1 = new AccountProfileExt_V2(sc1); 
        accprof1.FinalSave(); 
        
        AccountProfileViewredirect redirect2 = new AccountProfileViewredirect(sc1);
        redirect2.redirect();
        
        Account_profile__c ap2 = [select id,Account__c from Account_profile__c where account__r.recordtype.name='End Customer' limit 1];
        Pagereference pg2 = page.AccountprofileEditRedirect; 
        test.setcurrentpage(pg2);
        ApexPages.currentPage().getparameters().put('id',ap2.id); 
        ApexPages.StandardController sc2 = new ApexPages.StandardController(ap2);
        AccountProfileEditRedirect edit2 = new AccountProfileEditRedirect(sc2);
        edit2.redirect();
        
        Account_profile__c partnerProfile = [select id,Account__c from Account_profile__c where account__r.recordtype.name='Indirect' order by createddate desc limit 1];
        Pagereference pg3 = page.AccountProfileViewredirect; 
        test.setcurrentpage(pg3);
        ApexPages.currentPage().getparameters().put('id',partnerProfile.id); 
        ApexPages.StandardController sc3 = new ApexPages.StandardController(partnerProfile);
        AccountProfileViewredirect view3 = new AccountProfileViewredirect(sc3);
        view3.redirect();
    }
}