public class AccountProfileViewredirect {
    Public Account_Profile__c Ap;
    Public string apid;
    public AccountProfileViewredirect(ApexPages.StandardController controller) 
    {
        
    }
    Public pagereference redirect()
    {
        //pagereference pg;
        Apid= Apexpages.currentpage().getparameters().get('id');
        
        if(Apid != null && Apid != '')
        {
            ap = [select id,Account__c,Account__r.recordtype.name from Account_Profile__c where id =: Apid];
            
            UserRecordAccess ura = [SELECT RecordId, HasEditAccess, HasReadAccess FROM UserRecordAccess WHERE UserId =:Userinfo.getUserId() AND RecordId =:Apid];
            if(!ura.HasReadAccess){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'You do not have sufficient privileges to view this record. Please contact system admin'));
                return null;
            }
        }
        if(ap != Null)
        {
            if(ap.Account__r.recordtype.name != 'End Customer')
            {

                //pagereference pg = new pagereference('/' +Apid+'?nooverride=1');
                pagereference pg = new pagereference('/apex/AccountProfileViewPage_T2?id='+apid);
                //system.debug('1--->'+pg);                
                pg.setredirect(true);
                return pg;
                
            }Else if(ap.Account__r.recordtype.name == 'End Customer')
            {
                //pagereference pg = new pagereference('/apex/Account_Profile_View_Page?id='+apid);
                pagereference pg = new pagereference('/apex/AccountProfileViewPage?id='+apid); //Added on 12/11/2018
                pg.setredirect(true);
                return pg;
            }
            
        }Else
        {
            pagereference pg = new pagereference('/a0L/e?nooverride=1');
            //system.debug('1--->'+pg);                
            pg.setredirect(true);
            return pg;     
        }
        return null;
        //return null;
    }
    

}