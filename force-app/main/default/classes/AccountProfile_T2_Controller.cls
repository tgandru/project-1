public class AccountProfile_T2_Controller {
    public string AccId {get;set;}
    public string profileId {get;set;}
    public boolean goToComponent{get;set;}

    public AccountProfile_T2_Controller(ApexPages.StandardController Controller){
        AccId = ApexPages.CurrentPage().getParameters().get('AccountId');
        system.debug('AccountId-------->'+AccId);
        profileId = ApexPages.CurrentPage().getParameters().get('id');
        system.debug('profileId-------->'+profileId);
        
        Set<String> groupMembers = new Set<String>();
        for(groupmember gm : [Select GroupId,UserOrGroupId from groupmember where group.name='T2 Account Profile Access']){
            groupMembers.add(gm.UserOrGroupId);
        }
        
        goToComponent = false;
        if(profileId!=null && profileId!=''){
            Account acc = [Select id,OwnerId,owner2__c,(Select id,UserId from AccountTeamMembers where UserId=:Userinfo.getUserId()) from Account where id in (Select Account__c from Account_Profile__c where id=:profileId)];
            if(acc.OwnerId==Userinfo.getUserId() || acc.owner2__c==Userinfo.getUserId() || acc.AccountTeamMembers.size()>0 || Userinfo.getProfileId()==Label.System_Administrator_ProfileId || groupMembers.contains(Userinfo.getUserId())){
                goToComponent = true;
            }else{
                goToComponent = false;
                system.debug('goToComponent---->'+goToComponent);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'You do not have sufficient access rights to create/edit an Account Profile for this record. You must be the Account Owner or an Account Team Member.'));
            }
        }else if(AccId!=null && AccId!=''){
            Account acc = [Select id,OwnerId,owner2__c,(Select id,UserId from AccountTeamMembers where UserId=:Userinfo.getUserId()) from Account where id=:AccId];
            if(acc.owner2__c==Userinfo.getUserId()  || acc.AccountTeamMembers.size()>0 || Userinfo.getProfileId()==Label.System_Administrator_ProfileId || groupMembers.contains(Userinfo.getUserId())){
                goToComponent = true;
            }else{
                goToComponent = false;
                system.debug('goToComponent---->'+goToComponent);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'You do not have sufficient access rights to create/edit an Account Profile for this record. You must be the Account Owner or an Account Team Member.'));
            }
        }
    }
    
}