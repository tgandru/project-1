public class AccountProfile_T2_Ext {
    @AuraEnabled
    public static Account getAccountInfo(string accId){
        Account acc;
        if(accId!=null && accId!=''){
            List<Account> accList = [select id,Name,RecordTypeId,RecordType.Name,Type from Account where id=:accId];
            acc = accList[0];
        }else{
            acc = new Account();
        }
        return acc;
    }
    
    @AuraEnabled
    public static Boolean getExistingProfiles(string accId, Account_Profile__c profile){
        Boolean hasProfiles = false;
        List<Account_Profile__c> profileList = new List<Account_Profile__c>();
        profileList = [select id,FiscalYear__c from Account_Profile__c where Account__c=:accId and FiscalYear__c=:profile.FiscalYear__c];
        if(profileList.size()>0){
            hasProfiles=true;
        }
        return hasProfiles;
    }
    
    @AuraEnabled
    public static List<String> getProfileFields(String profileId){
        List<String> fields = new List<String>();
        for(Schema.FieldSetMember f : SObjectType.Account_Profile__c.FieldSets.Partner_Account_Profile_Fields.getFields()){
            fields.add(f.getfieldPath());
        }
        return fields;
    }
    
    @AuraEnabled
    public static Account_Profile__c getProfileInfo(String profileId, String accId){
        Id profRecTypeId = Schema.SObjectType.Account_Profile__c.getRecordTypeInfosByName().get('Partner').getRecordTypeId();
        Account_Profile__c accProf;
        if(profileId!=null && profileId!=''){
            string fields = 'FiscalYear__c,Description__c,URL__c,Account__c';
            for(Schema.FieldSetMember f : SObjectType.Account_Profile__c.FieldSets.Partner_Account_Profile_Fields.getFields()){
                fields = fields+','+f.getfieldPath();
            }
            fields = fields+',Partner_Development_1__c,Partner_Development_2__c,Partner_Development_3__c';
            fields = fields+',Product_Market_Expansion_1__c,Product_Market_Expansion_2__c,Product_Market_Expansion_3__c';
            fields = fields+',Share_Growth_1__c,Share_Growth_2__c,Share_Growth_3__c';
            fields = fields+',Key_Opportunity_1__c,Key_Opportunity_1__r.Name,Key_Opportunity_1__r.CloseDate,Key_Opportunity_1__r.NextStep,Key_Opportunity_1__r.Opportunity_Number__c,Key_Opportunity_2__c,Key_Opportunity_2__r.Name,Key_Opportunity_2__r.CloseDate,Key_Opportunity_2__r.NextStep,Key_Opportunity_2__r.Opportunity_Number__c,Key_Opportunity_3__c,Key_Opportunity_3__r.Name,Key_Opportunity_3__r.CloseDate,Key_Opportunity_3__r.NextStep,Key_Opportunity_3__r.Opportunity_Number__c,Key_Opportunity_4__c,Key_Opportunity_4__r.Name,Key_Opportunity_4__r.CloseDate,Key_Opportunity_4__r.NextStep,Key_Opportunity_4__r.Opportunity_Number__c,Key_Opportunity_5__c,Key_Opportunity_5__r.Name,Key_Opportunity_5__r.CloseDate,Key_Opportunity_5__r.NextStep,Key_Opportunity_5__r.Opportunity_Number__c';
            fields = fields+',Demand_Generation__c,Sales_Facing_Training__c,Funded_Head__c,Demo_Units__c,Contests__c,Tradeshows__c,Marketing_Investment_Other__c';
            fields = fields+',Strengths__c,Weaknesses__c,Opportunities__c,Threats__c'; //Added by Joe Jung 2019.08.22
            fields = fields+',MDF_Q1__c,MDF_Q2__c,MDF_Q3__c,MDF_Q4__c,MDF_Comments__c,VIR_Q1__c,VIR_Q2__c,VIR_Q3__c,VIR_Q4__c,VIR_Comments__c,ROI_Q1__c,ROI_Q2__c,ROI_Q3__c,ROI_Q4__c,ROI_Comments__c,Pipeline_Q1__c,Pipeline_Q2__c,Pipeline_Q3__c,Pipeline_Q4__c,Pipeline_Comments__c,TP_A_R_Q1__c,TP_A_R_Q2__c,TP_A_R_Q3__c, TP_A_R_Q4__c,TP_A_R_Comments__c,G_R_Q1__c,G_R_Q2__c,G_R_Q3__c,G_R_Q4__c,G_R_Comments__c'; // Added by Joe Jung 2019.08.28
            fields = fields+',HTV_Samsung_Revenue_CY__c,HTV_Samsung_Revenue_PY__c,HTV_Units_CY__c,HTV_Units_PY__c'; //Added by Thiru 09/16/2019
            fields = fields+',SSD_Samsung_Revenue_CY__c,SSD_Samsung_Revenue_PY__c,SSD_Units_CY__c,SSD_Units_PY__c'; //Added by Thiru 09/16/2019
            fields = fields+',LCD_Samsung_Revenue_CY__c,LCD_Samsung_Revenue_PY__c,LCD_Units_CY__c,LCD_Units_PY__c'; //Added by Thiru 09/16/2019
            fields = fields+',LED_Samsung_Revenue_CY__c,LED_Samsung_Revenue_PY__c,LED_Units_CY__c,LED_Units_PY__c'; //Added by Thiru 09/16/2019
            fields = fields+',LFD_Samsung_Revenue_CY__c,LFD_Samsung_Revenue_PY__c,LFD_Units_CY__c,LFD_Units_PY__c'; //Added by Thiru 09/16/2019
            String queryString = 'Select id,'+fields+' from Account_Profile__c where id=:profileId';
            
            List<Account_Profile__c> profileList = database.query(queryString);
            if(profileList.size()>0) accProf = profileList[0];
            else accProf = new Account_Profile__c();
        }else{
            
            DateTime dtGmt = system.now();
            String dtEST = dtGmt.format('yyyy-mm-dd HH:mm:ss','America/New_York');
            integer CYear = integer.valueOf(dtEST.left(4));
            
            accProf = new Account_Profile__c();
            accProf.Account__c = accId;
            accProf.RecordTypeId = profRecTypeId;
            accProf.FiscalYear__c=String.valueOf(CYear);
        }
        
        return accProf;
    }
    
    @AuraEnabled
    public static integer getCYear(Account_Profile__c theProfile){
        DateTime dtGmt = system.now();
        String dtEST = dtGmt.format('yyyy-mm-dd HH:mm:ss','America/New_York');
        integer CYear = integer.valueOf(dtEST.left(4));
        
        if(theProfile.FiscalYear__c != null && theProfile.FiscalYear__c!= ''){
            CYear = integer.valueOf(theProfile.FiscalYear__c);
        }
        
        return CYear;
    }
    
    @AuraEnabled
    public static List<Account_Profile_Contact__c> getContactInfo(String profileId){
        List<Account_Profile_Contact__c> AccntProfContacts = new List<Account_Profile_Contact__c>();
        integer AccntProfContactsLines=5;
        if(profileId!=null && profileId!=''){
            AccntProfContacts = [Select id,
                                        Account_Profile__c,
                                        Contact__c,
                                        Next_Best_Action__c,
                                        Position__c,
                                        Samsung_Contact__c,
                                        Samsung_Contact__r.Name,
                                        Contact__r.Name,
                                        Contact__r.Title,
                                        Contact__r.Job_Role__c,
                                        Contact__r.Job_Position__c,
                                        Contact__r.Account.Name 
                                        from Account_Profile_Contact__c 
                                        where Account_Profile__c=:profileid order by createddate asc];
            if(AccntProfContacts.size()<=AccntProfContactsLines) AccntProfContactsLines = AccntProfContactsLines-AccntProfContacts.size();
            for(integer i=0;i<AccntProfContactsLines;i++){
                AccntProfContacts.add(new Account_Profile_Contact__c());
            }
        }else{
            for(integer i=0;i<AccntProfContactsLines;i++){
                AccntProfContacts.add(new Account_Profile_Contact__c());
            }
        }
        
        return AccntProfContacts;
    }
    
    @AuraEnabled
    public static List<AccountTeamMember> getAccountTeam(String accId){
        List<AccountTeamMember> AccountTeam = new List<AccountTeamMember>();
        if(accId!=null && accId!=''){
            AccountTeam = [Select id,UserId,User.Name,User.FirstName,TeamMemberRole from AccountTeamMember where Accountid=:accId];
        }
        return AccountTeam;
    }
    
    @AuraEnabled
    public static List<CompetitiveProductShare__c> getProductMarketShare(String profileId){
        List<CompetitiveProductShare__c> productShareList = new List<CompetitiveProductShare__c>();
        
        if(profileId!=null && profileId!=''){
            productShareList = [Select id,
                                        Account_Profile__c,
                                        Product__c,
                                        Company_A__c,
                                        Company_B__c,
                                        Company_C__c,
                                        Company_D__c,
                                        CompanyA_M_S__c,
                                        CompanyB_M_S__c,
                                        CompanyC_M_S__c,
                                        CompanyD_M_S__c,
                                        Other__c,
                                        Other_M_S__c 
                                        from CompetitiveProductShare__c 
                                        where Account_Profile__c=:profileid];
        }/*else{
            Schema.DescribeFieldResult fieldResult = CompetitiveProductShare__c.Product__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            
            for (Schema.PicklistEntry a: ple){
                CompetitiveProductShare__c ps = new CompetitiveProductShare__c(Product__c=a.getValue(),Company_A__c='Samsung',Other__c='Other');
                productShareList.add(ps);
            }
            
        }*/
        
        if(productShareList.size()<=0){
            Schema.DescribeFieldResult fieldResult = CompetitiveProductShare__c.Product__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            
            for (Schema.PicklistEntry a: ple){
                CompetitiveProductShare__c ps = new CompetitiveProductShare__c(Product__c=a.getValue(),Company_A__c='Samsung',Other__c='Other');
                productShareList.add(ps);
            }
        }
        
        return productShareList;
    }
    
    @AuraEnabled
    public static List<Opportunity> getTop5CYwins(String accId, Account_Profile__c theProfile){
        List<Opportunity> oppList = new List<Opportunity>();

        DateTime dtGmt = system.now();
        String dtEST = dtGmt.format('yyyy-mm-dd HH:mm:ss','America/New_York');
        integer CYear = integer.valueOf(dtEST.left(4));
        
        if(theProfile.FiscalYear__c != null && theProfile.FiscalYear__c!= ''){
            CYear = integer.valueOf(theProfile.FiscalYear__c);
        }
        
        oppList = [Select id,Amount,Stagename,AccountId,Account.Name 
                   from Opportunity 
                   where Calendar_Year(closedate)=:CYear 
                         and stagename!=:Label.oppStageIdentification 
                         and Recordtype.Name!='HA Builder' 
                         and Amount!=null 
                         and id in (SELECT Opportunity__c FROM Partner__c where Partner__c=:accId and Role_is_Reseller__c=true and Opportunity__c!=null) 
                         order by Amount desc limit 5];
        system.debug('oppList.size()---->'+oppList.size());
        if(oppList.size()<5){
            for(integer i=oppList.size();i<5;i++){
                Opportunity opp = new Opportunity();
                oppList.add(opp);
            }
        }
        return oppList;
    }
    
    @AuraEnabled
    public static List<AccountWrapper> getTop5CYcustomers(List<Opportunity> oppList){
        List<AccountWrapper> wrapCustomer = new List<AccountWrapper>();
        if(oppList.size()>0){
            
            Set<String> oppIds = new Set<String>();
            for(Opportunity opp : oppList){
                if(opp.id!=null) oppIds.add(opp.id);
            }
            
            List<AggregateResult> result = [select sum(Amount) AmountSum,AccountId,Account.Name AccName from Opportunity where id=:oppIds group by AccountId, Account.Name order by sum(amount) desc];
            for(AggregateResult res : result){
                system.debug('result----->'+res);
                AccountWrapper aw = new AccountWrapper();
                aw.AccountId = (String)res.get('AccountId');
                aw.AccountName = (String)res.get('AccName');
                aw.Revenue = (Decimal)res.get('AmountSum');
                wrapCustomer.add(aw);
            }
            
            /*Map<String,Decimal> customerMap = new Map<String,Decimal>();
            Map<String,String> accNameMap = new Map<String,String>();
            for(Opportunity opp : oppList){
                if(opp.Id!=null){
                system.debug('opp.Id---->'+opp.Id);
                system.debug('opp.Amount---->'+opp.Amount);
                decimal Revenue = opp.Amount;
                if(customerMap.containsKey(opp.AccountId)){
                    system.debug('customerMap.get(opp.AccountId)---->'+customerMap.get(opp.AccountId));
                    Revenue = Revenue + customerMap.get(opp.AccountId);
                }
                customerMap.put(opp.AccountId,Revenue);
                accNameMap.put(opp.AccountId,opp.Account.Name);
                }
            }
            
            for(String str : customerMap.keySet()){
                AccountWrapper aw = new AccountWrapper();
                aw.AccountId = str;
                aw.AccountName = accNameMap.get(str);
                aw.Revenue = customerMap.get(str);
                wrapCustomer.add(aw);
            }*/
            
            if(wrapCustomer.size()<5){
                for(integer i=wrapCustomer.size();i<5;i++){
                    AccountWrapper aw = new AccountWrapper();
                    aw.Revenue=null;
                    wrapCustomer.add(aw);
                }
            }
        }else{
            for(integer i=0;i<5;i++){
                AccountWrapper aw = new AccountWrapper();
                aw.Revenue=null;
                wrapCustomer.add(aw);
            }
        }
        return wrapCustomer;
    }
    
    Public class AccountWrapper{
        @AuraEnabled public string AccountName{get;set;}
        @AuraEnabled public string AccountId{get;set;}
        @AuraEnabled public Decimal Revenue{get;set;}
        public AccountWrapper(){
            Revenue=0;
        }
    }
    
    @AuraEnabled
    public static List<RevenueWrapper> getSamsungRevenue(String accId, Account_Profile__c theProfile){
        List<RevenueWrapper> samsungRevenue = new List<RevenueWrapper>();

        Map<String,String> prodTypeMap = new Map<String,String>();
        prodTypeMap.put('HTV','HOSPITALITY_DISPLAY');
        prodTypeMap.put('SSD','MEMORY_BRAND');
        prodTypeMap.put('LCD','LCD_MONITOR');
        prodTypeMap.put('SMART SIGNAGE','SMART_SIGNAGE');
        //Set<String> ProdTypeSet = new Set<String>{'HOSPITALITY_DISPLAY','MEMORY_BRAND','LCD_MONITOR','SMART_SIGNAGE'};
        
        DateTime dtGmt = system.now();
        String dtEST = dtGmt.format('yyyy-mm-dd HH:mm:ss','America/New_York');
        integer CYear = integer.valueOf(dtEST.left(4));
        
        if(theProfile.FiscalYear__c != null && theProfile.FiscalYear__c!= ''){
            CYear = integer.valueOf(theProfile.FiscalYear__c);
        }
        
        integer PYear = CYear-1;

        Set<Integer> yearSet = new Set<Integer>{CYear,PYear};
        
        //for(String s : ProdTypeSet){
        for(String s : prodTypeMap.keySet()){
            for(Integer i : yearSet){
                RevenueWrapper rw = new RevenueWrapper();
                rw.year=i;
                rw.ProductType=s;
                samsungRevenue.add(rw);
            }
        }
        
        for(Opportunity opp : [Select id,Closedate,
                                      (Select id,quantity,requested_price__c,totalprice,ProductGroup__c
                                       from OpportunityLineItems 
                                       where ProductGroup__c in ('HOSPITALITY_DISPLAY','MEMORY_BRAND','LCD_MONITOR','SMART_SIGNAGE','MO_OPTION'))
                               from Opportunity 
                               where Calendar_Year(closedate)>=:PYear 
                                     and stagename=:Label.oppStageRollout 
                                     and Recordtype.Name!='HA Builder' 
                                     and id in (SELECT Opportunity__c FROM Partner__c where Partner__c=:accId and Role_is_Reseller__c=true and Opportunity__c!=null)]){
            integer year = (Opp.Closedate).year();
            for(OpportunityLineItem oli : opp.OpportunityLineItems){
                for(RevenueWrapper rw : samsungRevenue){
                    if(rw.year==year){
                        if((prodTypeMap.get(rw.ProductType)==oli.ProductGroup__c)||(prodTypeMap.get(rw.ProductType)=='SMART_SIGNAGE' && oli.ProductGroup__c=='MO_OPTION'))
                        {
                            rw.Revenue = rw.Revenue + oli.totalprice;
                            rw.Units = rw.Units + integer.valueOf(oli.quantity);
                        }
                    }
                }
            }
        }
        
        return samsungRevenue;
    }
    
    @AuraEnabled
    public static List<PipeLineWrapper> getPipeline(String accId, Account_Profile__c theProfile){
        List<PipeLineWrapper> Pipeline = new List<PipeLineWrapper>();

        DateTime dtGmt = system.now();
        String dtEST = dtGmt.format('yyyy-mm-dd HH:mm:ss','America/New_York');
        integer CYear = integer.valueOf(dtEST.left(4));
        
        if(theProfile.FiscalYear__c != null && theProfile.FiscalYear__c!= ''){
            CYear = integer.valueOf(theProfile.FiscalYear__c);
        }
        
        Set<String> stages = new Set<String>{Label.oppStageRollout,Label.OppStageCommit,Label.OppStageProposal,Label.OppStageQualification};
        for(String str : stages){
            PipeLineWrapper pw = new PipeLineWrapper();
            pw.Stage = str;
            Pipeline.add(pw);
        }
        
        for(Opportunity opp : [Select id,Amount,Stagename,Closedate 
                               from Opportunity 
                               where Calendar_Year(closedate)=:CYear 
                                     and stagename!=:Label.oppStageIdentification 
                                     and Recordtype.Name!='HA Builder' 
                                     and id in (SELECT Opportunity__c FROM Partner__c where Partner__c=:accId and Role_is_Reseller__c=true and Opportunity__c!=null)]){
            integer month = (Opp.Closedate).month();
            for(PipeLineWrapper pw : Pipeline){
                if(pw.Stage==opp.Stagename){
                    if(month==1 || month==2 || month==3){
                        pw.Q1 = pw.Q1 + opp.Amount;
                    }else if(month==4 || month==5 || month==6){
                        pw.Q2 = pw.Q2 + opp.Amount;
                    }else if(month==7 || month==8 || month==9){
                        pw.Q3 = pw.Q3 + opp.Amount;
                    }else{
                        pw.Q4 = pw.Q4 + opp.Amount;
                    }
                }
            }
        }
        
        for(PipeLineWrapper pw : Pipeline){
            pw.Total = pw.Q1+pw.Q2+pw.Q3+pw.Q4; 
        }
        
        return Pipeline;
    }
    
    @AuraEnabled
    public static string Save(Account_Profile__c profile, List<Account_Profile_Contact__c> accProfContacts,List<CompetitiveProductShare__c> productShare){
        Savepoint sp = Database.setSavepoint();
        try{
            if(profile.Revenue_Split_Hardware__c!=null || profile.Revenue_Split_Service__c!=null){
                if(profile.Revenue_Split_Hardware__c==null) profile.Revenue_Split_Hardware__c=0;
                if(profile.Revenue_Split_Service__c==null) profile.Revenue_Split_Service__c=0;
                integer revSplitSum = integer.valueOf(profile.Revenue_Split_Hardware__c) + Integer.valueOf(profile.Revenue_Split_Service__c);
                if(revSplitSum!=100){
                    return 'Sum of "Revenue Split - Hardware" & "Revenue Split - Service" must be equal to 100';
                }
            }
            upsert profile;
            
            if(profile.id!=null){
                List<Account_Profile_Contact__c> contacts = new List<Account_Profile_Contact__c>();
                for(Account_Profile_Contact__c con : accProfContacts){
                    if(con.Contact__c!=null){
                        if(con.Account_Profile__c==null){
                            con.Account_Profile__c = profile.id;
                        }
                        contacts.add(con);
                    }
                }
                if(contacts.size()>0){
                    upsert contacts;
                }
                
                for(CompetitiveProductShare__c ps : productShare){
                    system.debug('ps---->'+ps);
                    
                    if((ps.CompanyB_M_S__c!=null && ps.Company_B__c==null)||
                       (ps.CompanyC_M_S__c!=null && ps.Company_C__c==null)||
                       (ps.CompanyD_M_S__c!=null && ps.Company_D__c==null)||
                       (ps.CompanyB_M_S__c==null && ps.Company_B__c!=null && ps.Company_B__c!='')||
                       (ps.CompanyC_M_S__c==null && ps.Company_C__c!=null && ps.Company_C__c!='')||
                       (ps.CompanyD_M_S__c==null && ps.Company_D__c!=null && ps.Company_D__c!='')){
                        Database.rollback(sp);
                        String Msg = ' In "'+ps.Product__c+'" Product Market Share Section, Competitor Name or M/S is missing. Both are required while entering market share';
                        return Msg;
                    }
                    
                    decimal totalMS = 0;
                    
                    if(ps.CompanyA_M_S__c!=null) totalMS  = totalMS + ps.CompanyA_M_S__c;
                    if(ps.CompanyB_M_S__c!=null) totalMS  = totalMS + ps.CompanyB_M_S__c;
                    if(ps.CompanyC_M_S__c!=null) totalMS  = totalMS + ps.CompanyC_M_S__c;
                    if(ps.CompanyD_M_S__c!=null) totalMS  = totalMS + ps.CompanyD_M_S__c;
                    if(ps.Other_M_S__c!=null) totalMS  = totalMS + ps.Other_M_S__c;
                    system.debug('totalMS---->'+totalMS);
                    if(totalMS>0 && totalMS!=100){
                        Database.rollback(sp);
                        return 'Total percentage is equals too 100 for each product type in Product Market Share section.';
                    }
                     
                    if(ps.Account_Profile__c==null){
                        ps.Account_Profile__c=profile.Id;
                    }
                }
                if(productShare.size()>0){
                    upsert productShare;
                }
                
                string returnMsg = 'Save Successfully';
                returnMsg = returnMsg+','+profile.id;
                return returnMsg;
            }else{
                return null;
            }
        }catch (Exception ex) {
            Database.rollback(sp);
            String exStr = String.valueof(ex);
            return exStr;
        }
    }
    
    Public class RevenueWrapper{
        @AuraEnabled public string ProductType{get;set;}
        @AuraEnabled public Integer year{get;set;}
        @AuraEnabled public Decimal Revenue{get;set;}
        @AuraEnabled public integer Units{get;set;}
        public RevenueWrapper(){
            Revenue=0;
            Units=0;
        }
    }
    
    Public class PipeLineWrapper{
        @AuraEnabled public string Stage{get;set;}
        @AuraEnabled public Decimal Q1{get;set;}
        @AuraEnabled public Decimal Q2{get;set;}
        @AuraEnabled public Decimal Q3{get;set;}
        @AuraEnabled public Decimal Q4{get;set;}
        @AuraEnabled public Decimal Total{get;set;}
        public PipeLineWrapper(){
            Q1=0;
            Q2=0;
            Q3=0;
            Q4=0;
            Total=0;
        }
    }
    
}