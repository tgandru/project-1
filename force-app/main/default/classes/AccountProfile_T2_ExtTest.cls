@istest(seealldata = true)
public class AccountProfile_T2_ExtTest {
    Public static testmethod void test1(){
        Account_profile__c profile = [select id,FiscalYear__c,Account__c,Revenue_Split_Hardware__c,Revenue_Split_Service__c from Account_profile__c where recordtype.name='Partner' and Account__r.recordtype.name='Indirect' order by createddate desc limit 1];
        
        string accId = profile.Account__c;
        List<Account_Profile_Contact__c> conList = new List<Account_Profile_Contact__c>();
        List<CompetitiveProductShare__c> pshareList = new List<CompetitiveProductShare__c>();
        
        AccountProfile_T2_Ext.getProfileFields(null);
        AccountProfile_T2_Ext.getAccountInfo(accId);
        AccountProfile_T2_Ext.getAccountTeam(accId);
        AccountProfile_T2_Ext.getExistingProfiles(accId,profile);
        AccountProfile_T2_Ext.getProfileInfo(profile.id,accId);
        AccountProfile_T2_Ext.getProfileInfo(null,accId);
        AccountProfile_T2_Ext.getCYear(profile);
        AccountProfile_T2_Ext.getContactInfo(profile.id);
        AccountProfile_T2_Ext.getContactInfo(null);
        AccountProfile_T2_Ext.getProductMarketShare(profile.id);
        AccountProfile_T2_Ext.getProductMarketShare(null);
        AccountProfile_T2_Ext.getSamsungRevenue(accId,profile);
        AccountProfile_T2_Ext.getPipeline(accId,profile);
        List<Opportunity> oppList = new List<Opportunity>();
        AccountProfile_T2_Ext.getTop5CYcustomers(oppList);
        oppList = AccountProfile_T2_Ext.getTop5CYwins(accId,profile);
        AccountProfile_T2_Ext.getTop5CYcustomers(oppList);
        AccountProfile_T2_Ext.Save(profile,conList,pshareList);
        
        Pagereference pg1 = page.AccountprofilePage_T2; 
        test.setcurrentpage(pg1);
        ApexPages.currentPage().getparameters().put('id',profile.id);    
        ApexPages.StandardController sc1 = new ApexPages.StandardController(profile);
        AccountProfile_T2_Controller cont = new AccountProfile_T2_Controller(sc1);
        
        Pagereference pg2 = page.AccountprofilePage_T2; 
        test.setcurrentpage(pg2);
        ApexPages.currentPage().getparameters().put('AccountId',accId); 
        Account_Profile__c newProfile = new Account_Profile__c(FiscalYear__c = '2019');
        ApexPages.StandardController sc2 = new ApexPages.StandardController(newProfile);
        AccountProfile_T2_Controller cont2 = new AccountProfile_T2_Controller(sc2);
        
        Pagereference pdf = page.AccountProfile_T2_PDF; 
        test.setcurrentpage(pdf);
        ApexPages.currentPage().getparameters().put('id',profile.id);    
        ApexPages.StandardController scPDF = new ApexPages.StandardController(profile);
        AccountProfile_T2_PDF_Controller contPDF = new AccountProfile_T2_PDF_Controller(scPDF);
    }
}