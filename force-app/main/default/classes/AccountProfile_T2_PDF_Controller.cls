public class AccountProfile_T2_PDF_Controller {
    public Account_Profile__c accountProfile{get;set;}
    public integer CYear{get;set;}
    public List<AccountProfile_T2_Ext.RevenueWrapper> cySamRevenue{get;set;}
    public List<AccountProfile_T2_Ext.RevenueWrapper> pySamRevenue{get;set;}
    public List<AccountProfile_T2_Ext.PipeLineWrapper> cyPipeline{get;set;}
    public List<Account_Profile_Contact__c> contacts{get;set;}
    public List<AccountTeamMember> accountTeam{get;set;}
    public List<Opportunity> top5CYwins{get;set;}
    public List<AccountProfile_T2_Ext.AccountWrapper> top5CYcustomers{get;set;}
    public List<CompetitiveProductShare__c> productShare{get;set;}

    public AccountProfile_T2_PDF_Controller(ApexPages.StandardController Controller){
        accountProfile= (Account_Profile__c)controller.getRecord();
        
        if(accountProfile.id!=null){
            accountProfile = [Select id,Account__c,FiscalYear__c, Strengths__c, Weaknesses__c, Opportunities__c, Threats__c,
                              MDF_Q1__c,MDF_Q2__c,MDF_Q3__c,MDF_Q4__c,MDF_Comments__c,
                              VIR_Q1__c,VIR_Q2__c,VIR_Q3__c,VIR_Q4__c,VIR_Comments__c,
                              ROI_Q1__c,ROI_Q2__c,ROI_Q3__c,ROI_Q4__c,ROI_Comments__c,
                              Pipeline_Q1__c,Pipeline_Q2__c,Pipeline_Q3__c,Pipeline_Q4__c,Pipeline_Comments__c,
                              TP_A_R_Q1__c,TP_A_R_Q2__c,TP_A_R_Q3__c, TP_A_R_Q4__c,TP_A_R_Comments__c,
                              G_R_Q1__c,G_R_Q2__c,G_R_Q3__c,G_R_Q4__c,G_R_Comments__c 
  							  From Account_Profile__c WHERE id=:accountProfile.Id];
            DateTime dtGmt = system.now();
            String dtEST = dtGmt.format('yyyy-mm-dd HH:mm:ss','America/New_York');
            CYear = integer.valueOf(dtEST.left(4));
            
            if(accountProfile!=null){
                CYear = integer.valueOf(accountProfile.FiscalYear__c);
                Team();
                SamsungRevenue();
                Pipeline();
                profileContacts();
                topOpportunities();
                topCustomers();
                productMarketShare();
            }
        }
        
    }
    
    public List<AccountProfile_T2_Ext.RevenueWrapper> SamsungRevenue(){
        List<AccountProfile_T2_Ext.RevenueWrapper> samRevenue = new List<AccountProfile_T2_Ext.RevenueWrapper>();
        samRevenue = AccountProfile_T2_Ext.getSamsungRevenue(accountProfile.Account__c, accountProfile);
        
        cySamRevenue = new List<AccountProfile_T2_Ext.RevenueWrapper>();
        pySamRevenue = new List<AccountProfile_T2_Ext.RevenueWrapper>();
        
        for(AccountProfile_T2_Ext.RevenueWrapper rw : samRevenue){
            if(rw.year == CYear){
                cySamRevenue.add(rw);
            }else{
                pySamRevenue.add(rw);
            }
        }
        
        return samRevenue;
    }
    
    public List<AccountProfile_T2_Ext.PipeLineWrapper> Pipeline(){
        cyPipeline = new List<AccountProfile_T2_Ext.PipeLineWrapper>();
        cyPipeline = AccountProfile_T2_Ext.getPipeline(accountProfile.Account__c, accountProfile);
        
        return cyPipeline;
    }
    public List<Account_Profile_Contact__c> profileContacts(){
        contacts = new List<Account_Profile_Contact__c>();
        contacts = AccountProfile_T2_Ext.getContactInfo(accountProfile.Id);
        
        return contacts;
    }
    public List<AccountTeamMember> Team(){
        accountTeam = new List<AccountTeamMember>();
        accountTeam = AccountProfile_T2_Ext.getAccountTeam(accountProfile.Account__c);
        
        return accountTeam;
    }
    public List<Opportunity> topOpportunities(){
        top5CYwins = new List<Opportunity>();
        top5CYwins = AccountProfile_T2_Ext.getTop5CYwins(accountProfile.Account__c, accountProfile);
        
        return top5CYwins;
    }
    public List<AccountProfile_T2_Ext.AccountWrapper> topCustomers(){
        top5CYcustomers = new List<AccountProfile_T2_Ext.AccountWrapper>();
        top5CYcustomers = AccountProfile_T2_Ext.getTop5CYcustomers(top5CYwins);
        
        return top5CYcustomers;
    }
    public List<CompetitiveProductShare__c> productMarketShare(){
        productShare = new List<CompetitiveProductShare__c>();
        productShare = AccountProfile_T2_Ext.getProductMarketShare(accountProfile.Id);
        system.debug('product share---->'+productShare);
        return productShare;
    }
    
}