/*
 * Service level class for Account Object, implemented from AccountTriggerHandler.cls.
 * Service class follows the Command Design Pattern.
 * Author: Eric Vennaro
**/

public class AccountService {
    private Account account;
    private Map<Id, Account> oldAccounts;
    private Map<String, Id> recordTypeIds;

    public AccountService(Account account, Map<Id, Account> oldAccounts, Map<String, Id> recordTypeIds) {
        this.account = account;
        this.oldAccounts = oldAccounts;
        this.recordTypeIds = recordTypeIds;
    }

    public void processNewAccount() {
        //updateRecordType();
        //IF RECORD TYPE IS LOADED IN ON A NEW ACCOUNT AND YOU WANT TO UPDATE THE CORRESPONDING CHECKBOX
        //ADD:
        
        //NOTE: This assumes if recordType is blank then the checkbox is checked
        if(account.RecordTypeId == null){
            //assumes checkbox is checked
            updateRecordType();
        } else {
            updateFields();
        }
    }

    public void updateExistingAccount() {
        if(wasRecordUpdated()){
            updateFields();
        } else if(wasCheckboxUpdated()) {
            //updateRecordType();
            updateFields();
        }
    }

    @TestVisible private void updateRecordType() {
        account.RecordTypeId = recordTypeForAccount();
    }

    private Id recordTypeForAccount(){
        Boolean directAccount = account.Direct_Account__c != null ? account.Direct_Account__c : false;
        Boolean endCustomer = account.End_Customer_Account__c != null ? account.End_Customer_Account__c : false;
        Boolean oldDirectAccount = true;
        Boolean oldEndCustomer = true;
        
        if(oldAccounts.get(account.Id) == null) {
            oldDirectAccount = false;
            oldEndCustomer = false;
        } else {
            Account acc = oldAccounts.get(account.Id);
            oldDirectAccount = acc.Direct_Account__c != null ? acc.Direct_Account__c : false;
            oldEndCustomer = acc.End_Customer_Account__c != null ? acc.Direct_Account__c : false;
        }
        
       /* if(directAccount && !oldDirectAccount) {
            return recordTypeIds.get('Temporary');
        } else*/ 
        if(endCustomer && !oldEndCustomer) {
            return recordTypeIds.get('End Customer');
        } else {
            return recordTypeIds.get('Indirect');
        }
    }

    private Boolean wasRecordUpdated(){
        Account oldAccount = oldAccounts.get(account.Id);
        if(account.RecordTypeId != null){
            if(oldAccount.RecordTypeId != null){
                if(account.RecordTypeId != oldAccount.RecordTypeId) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    private void updateFields() {
        if(account.RecordTypeId == recordTypeIds.get('Direct') ||
            account.RecordTypeId == recordTypeIds.get('Temporary')){
            account.Direct_Account__c = true;
            account.Indirect_Account__c = false;
            account.End_Customer_Account__c = false;
        //    account.RecordTypeId = recordTypeIds.get('Temporary');
        } else if(account.RecordTypeId == recordTypeIds.get('End Customer')){
            account.Direct_Account__c = false;
            account.Indirect_Account__c = false;
            account.End_Customer_Account__c = true;
        } else if(account.RecordTypeId == recordTypeIds.get('Indirect')){
            account.Direct_Account__c = false;
            account.Indirect_Account__c = true;
            account.End_Customer_Account__c = false;
        } else if(account.RecordTypeId == recordTypeIds.get('CBD Indirect')){
            account.Direct_Account__c = false;
            account.Indirect_Account__c = true;
            account.End_Customer_Account__c = false;
        }
        System.debug('EV Records: ' + account.Direct_Account__c + ', ' + account.Indirect_Account__c + ', ' + account.End_Customer_Account__c);
    }

    private Boolean wasCheckboxUpdated(){
        Account oldAccount = oldAccounts.get(account.Id);
        if(account.Direct_Account__c != oldAccount.Direct_Account__c || account.End_Customer_Account__c != oldAccount.End_Customer_Account__c || 
            account.Indirect_Account__c != oldAccount.Indirect_Account__c) {
            return true;
        } else {
            return false;
        }
    }
}