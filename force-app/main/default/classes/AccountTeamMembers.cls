public class AccountTeamMembers {
   public Account acct {get;set;}
   public list<id> dealId{get; set;}
   public list<AccountTeamMember> atm {get; set;}
    public list<AccountTeamMember> atmdel {get; set;}
    public AccountTeamMembers(ApexPages.StandardController controller) {
        acct=[select id,name from Account where id=:controller.getRecord().Id limit 1];
       dealId=new list<id>();
         atmdel =new list<AccountTeamMember>();
        atm=[SELECT AccountAccessLevel,AccountId,CaseAccessLevel,CreatedById,CreatedDate,Id,IsDeleted,
                    LastModifiedById,LastModifiedDate,OpportunityAccessLevel,PhotoUrl,SystemModstamp,TeamMemberRole,Title,
                    UserId,User.Name FROM AccountTeamMember where AccountId=:acct.id ]; 
    }
    
    
   public PageReference saveon(){
      upsert atm;
        if(dealId.size()>0){
       try{
       atmdel =[select id from AccountTeamMember where id in :dealId];
       database.delete(atmdel,false);
       }catch(Exception e){
       
       }
     }
       return new PageReference ('/'+acct.id);
   }
    public PageReference cancelon(){
            return new PageReference ('/'+acct.id);
   }
   
   public void RemoveItems(){
       Integer rowNum = Integer.valueof(system.currentpagereference().getparameters().get('index'));
        dealId.add(atm[rowNum-1].Id);
       atm.remove(rowNum - 1); 
   }
   public void adding(){
    AccountTeamMember at=new AccountTeamMember();
    at.AccountId=acct.id;
    atm.add(at);
   
   }
}