@isTest
private class AccountTeamMembersTest {

	private static testMethod void testmethod1() {
	    Test.startTest();
         Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
          Account account = TestDataUtility.createAccount('Marshal Mathers');
          insert account;
          AccountTeamMember at=new AccountTeamMember();
             at.AccountId=account.id;
             at.AccountAccessLevel='Edit';
             at.userid=UserInfo.getUserId();
          insert at;
          PageReference myVfPage = Page.AccountTeamMembersPage;
            Test.setCurrentPage(myVfPage);
               ApexPages.StandardController sc = new ApexPages.StandardController(account);
               AccountTeamMembers con=new AccountTeamMembers(sc);
                con.dealId.add(at.id);
                   con.saveon();
                   con.cancelon();
                   con.adding();
                  
                  
        Test.stopTest();    
            
	}

}