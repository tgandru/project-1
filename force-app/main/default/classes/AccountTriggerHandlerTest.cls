/*
 * Test class for Account Trigger 
 * Author: Eric Vennaro
**/
//commenting this out as AccountTriggerTest is covering 100% on Account Trigger handler and this class has complulation issues
@isTest
private class AccountTriggerHandlerTest{
   /* @testSetup static void init() {
        Account startingAccount = new Account(Name = 'Marshal Mathers', Type = 'Customer', BillingStreet = '8 Mile',
                BillingCity = 'Detroit', BillingState = 'Michigan', BillingPostalCode = '48201', Direct_Account__c = false, 
                End_Customer_Account__c = true);
        insert startingAccount;

        Account secondAccount = new Account(Name = 'Frank White', Type = 'Customer', BillingStreet = '8 Mile',
                BillingCity = 'Detroit', BillingState = 'New York', BillingPostalCode = '48201', Direct_Account__c = true, 
                End_Customer_Account__c = false);
        insert secondAccount;

        Account thirdAccount = new Account(Name = 'Jadakiss', Type = 'Customer', BillingStreet = '8 Mile',
                BillingCity = 'Detroit', BillingState = 'New York', BillingPostalCode = '48201', Direct_Account__c = false, 
                End_Customer_Account__c = false, Indirect_Account__c = true);
        insert thirdAccount;

        Account fourthAccount = new Account(Name = 'Ja Rule', Type = 'Customer', BillingStreet = '8 Mile',
                BillingCity = 'Detroit', BillingState = 'New York', BillingPostalCode = '48201', Direct_Account__c = false, 
                End_Customer_Account__c = false, Indirect_Account__c = true);
        insert fourthAccount;
    }

    @isTest static void testProcessNewAccountEndCustomer() {
        Account updatedAccount = [SELECT Direct_Account__c, 
                                         End_Customer_Account__c, 
                                         RecordType.DeveloperName,
                                         Name
                                  FROM Account 
                                  WHERE Name = 'Marshal Mathers'
                                  LIMIT 1];
        System.assertEquals(true, updatedAccount.End_Customer_Account__c);
        System.assertEquals('Marshal Mathers', updatedAccount.Name);
        System.assertEquals('End_User', updatedAccount.RecordType.DeveloperName);
    }

    @isTest static void testProcessNewAccountDirect() {
        Account updatedAccount = [SELECT Direct_Account__c, 
                                         End_Customer_Account__c, 
                                         RecordType.DeveloperName, 
                                         Name
                                  FROM Account 
                                  WHERE Name = 'Frank White'
                                  LIMIT 1];
        System.assertEquals(true, updatedAccount.Direct_Account__c);
        System.assertEquals('Frank White', updatedAccount.Name);
        System.assertEquals('Temporary', updatedAccount.RecordType.DeveloperName);
        
    }

    @isTest static void testProcessNewAccountIndirect() {
        Account updatedAccount = [SELECT Direct_Account__c, 
                                         End_Customer_Account__c,
                                         Indirect_Account__c, 
                                         RecordType.DeveloperName,
                                         Name 
                                  FROM Account 
                                  WHERE Name = 'Jadakiss'
                                  LIMIT 1];
        System.assertEquals(true, updatedAccount.Indirect_Account__c);
        System.assertEquals('Jadakiss', updatedAccount.Name);
        System.assertEquals('Indirect', updatedAccount.RecordType.DeveloperName);
    }

    @isTest static void testUpdateExistingAccountToEndCustomer() {
    	Account startingAccount = [SELECT Direct_Account__c, 
                                         End_Customer_Account__c,
                                         Indirect_Account__c, 
                                         RecordType.DeveloperName,
                                         Name 
                                  FROM Account 
                                  WHERE Name = 'Ja Rule'
                                  LIMIT 1];

		Map<String, Id> recordTypeIds = RecordTypeUtil.recordTypeMap('Account');
        startingAccount.RecordTypeId = recordTypeIds.get('End Customer');
        update startingAccount;
		Test.startTest();      
        Account updatedAccount2 = [SELECT Direct_Account__c, 
                                          End_Customer_Account__c,
                                          Indirect_Account__c, 
                                          RecordType.DeveloperName,
                                          Name 
                                   FROM Account 
                                   WHERE Name = 'Ja Rule'
                                   LIMIT 1];
        System.assert(updatedAccount2.End_Customer_Account__c);
        System.assert(false == updatedAccount2.Indirect_Account__c);
        Test.stopTest();
    }

    @isTest static void testUpdateExistingAccountToIndirect() {
    	Account startingAccount = [SELECT Direct_Account__c, 
                                         End_Customer_Account__c,
                                         Indirect_Account__c, 
                                         RecordType.DeveloperName,
                                         Name 
                                  FROM Account 
                                  WHERE Name = 'Frank White'
                                  LIMIT 1];
        Map<String, Id> recordTypeIds = RecordTypeUtil.recordTypeMap('Account');
        startingAccount.RecordTypeId = recordTypeIds.get('Indirect');
        update startingAccount;
		Test.startTest();      
        Account updatedAccount2 = [SELECT Direct_Account__c, 
                                          End_Customer_Account__c,
                                          Indirect_Account__c, 
                                          RecordType.DeveloperName,
                                          Name 
                                   FROM Account 
                                   WHERE Name = 'Ja Rule'
                                   LIMIT 1];
        //System.assertEquals(updatedAccount2.RecordType.DeveloperName, 'Indirect');
        System.assert(updatedAccount2.Indirect_Account__c);
        Test.stopTest();
    }
    
    @isTest static void testUpdateExistingAccountToDirect() {
    	Account startingAccount = [SELECT Direct_Account__c, 
                                         End_Customer_Account__c,
                                         Indirect_Account__c, 
                                         RecordType.DeveloperName,
                                         Name 
                                  FROM Account 
                                  WHERE Name = 'Ja Rule'
                                  LIMIT 1];
        Map<String, Id> recordTypeIds = RecordTypeUtil.recordTypeMap('Account');
        startingAccount.RecordTypeId = recordTypeIds.get('Direct');
        update startingAccount;
		Test.startTest();      
        Account updatedAccount2 = [SELECT Direct_Account__c, 
                                          End_Customer_Account__c,
                                          Indirect_Account__c, 
                                          RecordType.DeveloperName,
                                          Name 
                                   FROM Account 
                                   WHERE Name = 'Ja Rule'
                                   LIMIT 1];

        System.assert(updatedAccount2.Direct_Account__c);
		System.assertEquals('Temporary', updatedAccount2.RecordType.DeveloperName);
        Test.stopTest();
    }

    @isTest static void testUpdateCheckboxOnAccountToDirect() {
    	Account startingAccount = [SELECT Direct_Account__c, 
                                         End_Customer_Account__c,
                                         Indirect_Account__c, 
                                         RecordType.DeveloperName,
                                         Name 
                                  FROM Account 
                                  WHERE Name = 'Ja Rule'
                                  LIMIT 1];
        Map<String, Id> recordTypeIds = RecordTypeUtil.recordTypeMap('Account');
        startingAccount.Direct_Account__c = true;
        update startingAccount;
		Test.startTest();      
        Account updatedAccount2 = [SELECT Direct_Account__c, 
                                          End_Customer_Account__c,
                                          Indirect_Account__c, 
                                          RecordType.DeveloperName,
                                          Name 
                                   FROM Account 
                                   WHERE Name = 'Ja Rule'
                                   LIMIT 1];
        System.assert(updatedAccount2.Direct_Account__c);
        System.assert(!updatedAccount2.Indirect_Account__c);
		System.assertEquals('Temporary', updatedAccount2.RecordType.DeveloperName);
        Test.stopTest();
    }

    @isTest static void testUpdateCheckboxOnAccountToIndirect() {
    	Account startingAccount = [SELECT Direct_Account__c, 
                                         End_Customer_Account__c,
                                         Indirect_Account__c, 
                                         RecordType.DeveloperName,
                                         Name 
                                  FROM Account 
                                  WHERE Name = 'Frank White'
                                  LIMIT 1];
        Map<String, Id> recordTypeIds = RecordTypeUtil.recordTypeMap('Account');
        startingAccount.Indirect_Account__c = true;
        update startingAccount;
		Test.startTest();      
        Account updatedAccount2 = [SELECT Direct_Account__c, 
                                          End_Customer_Account__c,
                                          Indirect_Account__c, 
                                          RecordType.DeveloperName,
                                          Name 
                                   FROM Account 
                                   WHERE Name = 'Ja Rule'
                                   LIMIT 1];

        System.assert(updatedAccount2.Indirect_Account__c);
		System.assertEquals('Indirect', updatedAccount2.RecordType.DeveloperName);
        Test.stopTest();
    }

    @isTest static void testUpdateCheckboxOnAccountToEndUser() {
    	Account startingAccount = [SELECT Direct_Account__c, 
                                         End_Customer_Account__c,
                                         Indirect_Account__c, 
                                         RecordType.DeveloperName,
                                         Name 
                                  FROM Account 
                                  WHERE Name = 'Ja Rule'
                                  LIMIT 1];
        Map<String, Id> recordTypeIds = RecordTypeUtil.recordTypeMap('Account');
        startingAccount.End_Customer_Account__c = true;
        update startingAccount;
		Test.startTest();      
        Account updatedAccount2 = [SELECT Direct_Account__c, 
                                          End_Customer_Account__c,
                                          Indirect_Account__c, 
                                          RecordType.DeveloperName,
                                          Name 
                                   FROM Account 
                                   WHERE Name = 'Ja Rule'
                                   LIMIT 1];

        System.assert(updatedAccount2.End_Customer_Account__c);
		System.assertEquals('End_User', updatedAccount2.RecordType.DeveloperName);
        Test.stopTest();
    }*/
}