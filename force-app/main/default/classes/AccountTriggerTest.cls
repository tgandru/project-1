/*
This test class is going to cover AccountTriggerHandler,AccountService,
UpdatePicklistValuesHandler,IntegrationUpdateAccountTriggerHelper classes

Created By : Aarthi RC
Created Date : May 11 2016
*/
@isTest
private class AccountTriggerTest {
  
//Scenario : Calling trigger for before and after insert with no recordType
  @isTest static void test_method_one() 
  {
              Channelinsight.CI_TEST_TestUtils.createBaseInstallData();

    // Implement test code
    List<Account> accounts = new List<Account>();
    
    for(Integer i=0; i<=5; i++ )
    {
      Account a = TestDataUtility.createAccount('Test Account' + i);
      a.SAP_Company_Code__c='99999888'+ i;
      accounts.add(a);
    }
    insert accounts;
    
    System.assertEquals(accounts.size(), 6);
    
    Map<id,Account> AccMap = new Map<id,Account>();
    AccMap.put(accounts[0].id, accounts[0]);
    Map<String, Id> recordTypeIds = new Map<String, Id>();
    Id IndirectId = TestDataUtility.retrieveRecordTypeId('Indirect', 'Account');
    Id EndCustId = TestDataUtility.retrieveRecordTypeId('End customer', 'Account');
    recordTypeIds.put('Indirect', IndirectId);
    recordTypeIds.put('End Customer', IndirectId);
    AccountService ac = new AccountService(accounts[0],AccMap,recordTypeIds);
    ac.updateRecordType();
  }

//Scenario when assigning the direct and end customer account as false for accountTriggerhandler
  @isTest static void test_method_two() 
  {
    // Implement test code
            Channelinsight.CI_TEST_TestUtils.createBaseInstallData();

    List<Account> accounts = new List<Account>();
    Id recordTypeId = TestDataUtility.retrieveRecordTypeId('Indirect', 'Account');
    
    for(Integer i=4; i<=7; i++ )
    {
      Account a = TestDataUtility.createAccount('Test Account' + i);
      a.SAP_Company_Code__c='9999988'+ i;
      accounts.add(a);
    }
    insert accounts;
    
    if(accounts.size() > 0) {
      Account randomAccount = accounts.get(0);
      if(recordTypeId <> null) {
        randomAccount.RecordTypeId = recordTypeId;
        randomAccount.Type = 'VAR';
        update randomAccount;
      }
    }

    List<Account> oldAcc = accounts;
    for(Account a : oldAcc)
    {
      a.Direct_Account__c = false;
      a.End_Customer_Account__c = false;
    }
    try {
      update oldAcc;
    }catch(System.DmlException e) {
            System.debug('update Account caught expected exception: ' + e.getDmlMessage(0));

        //Assert Error Message if Record Type is Indirect Partner
         System.assert(e.getMessage().contains('Please set type to a valid selection for Indirect Partners'), 
             e.getMessage()); 

      //Assert Field
         System.assertEquals(Account.Type, e.getDmlFields(0)[0]);

      //Assert Status Code
         System.assertEquals('FIELD_CUSTOM_VALIDATION_EXCEPTION'  , 
              e.getDmlStatusCode(0));
      }
    System.assertEquals(accounts.size(), 4);
  }
  
  //Scenario when assigning the direct and end customer account as true for accountTriggerhandler
  @isTest static void test_method_three() {
              Channelinsight.CI_TEST_TestUtils.createBaseInstallData();

    // Implement test code
    List<Account> accounts = new List<Account>();
    
    for(Integer i=4; i<=7; i++ )
    {
      Account a = TestDataUtility.createAccount('Test Account' + i);
      a.SAP_Company_Code__c='99997788'+ i;
      accounts.add(a);
    }
    insert accounts;

    List<Account> oldAcc = accounts;
    for(Account a : oldAcc)
    {
      a.Direct_Account__c = true;
      a.End_Customer_Account__c = true;
    }
    update oldAcc;
    System.assertEquals(accounts.size(), 4);
  }

  //Scenario calling the accounts for the updatePicklistValuesHandler
  @isTest static void  test_method_four()
  {
    Test.startTest();
            Channelinsight.CI_TEST_TestUtils.createBaseInstallData();

    //Create the PickListValues
    List<PicklistTranslation__c> pickListValues = TestDataUtility.createPickListTranslation(false);
    insert pickListValues;
    

    Map<String, String> picklistMapT2S = new Map<String, String>();
    Map<String, String> picklistMapS2T = new Map<String, String>();
    for(PicklistTranslation__c pt:pickListValues)
    {
      picklistMapT2S.put(pt.TargetAPIFieldName__c+','+pt.TargetValue__c,pt.SourceAPIName__c+','+pt.SourceValue__c);
        picklistMapS2T.put(pt.SourceAPIName__c+','+pt.SourceValue__c,pt.TargetAPIFieldName__c+','+pt.TargetValue__c);
    }
    
    Id AccEndUserRT = TestDataUtility.retrieveRecordTypeId('End_User', 'Account');
    
    List<Account> accs = new List<Account>();
    Account a1 = TestDataUtility.createAccount('Test data');
    a1.External_Industry__c ='CO';
    a1.External_Top_Tier__c='PT';
    a1.SAP_Company_Code__c='99999668';
    a1.Industry_type__c='Dot com';
    a1.external_business_partner_type__c='CO2';
    a1.External_Type__c='Ex';
    accs.add(a1);

    Account a = TestDataUtility.createAccount('Test Account');
    a.External_industry_type__c='COM';
    a.Partner_top_tier__c='Platinum';
    a.Industry='Communications';
     a.SAP_Company_Code__c='99996668';
    a.business_Partner_Type__c='Danger';
    a.Type='Customer';
    a.RecordTypeId=AccEndUserRT;
    accs.add(a);

    insert accs;

    UpdatePicklistValuesHandler.picklistVals=pickListValues;
    UpdatePicklistValuesHandler.picklistMapT2S=picklistMapT2S;
    UpdatePicklistValuesHandler.picklistMapS2T=picklistMapS2T;

    upsert accs;
    
    //while Updating the account
    List<PicklistTranslation__c> pickListValues1 = TestDataUtility.createPickListTranslation(true);
    insert pickListValues1;
    

    Map<String, String> picklistMapT2S1 = new Map<String, String>();
    Map<String, String> picklistMapS2T1 = new Map<String, String>();
    for(PicklistTranslation__c pt1:pickListValues1)
    {
      picklistMapT2S1.put(pt1.TargetAPIFieldName__c+','+pt1.TargetValue__c,pt1.SourceAPIName__c+','+pt1.SourceValue__c);
        picklistMapS2T1.put(pt1.SourceAPIName__c+','+pt1.SourceValue__c,pt1.TargetAPIFieldName__c+','+pt1.TargetValue__c);
    }

    List<Account> updatedAccs = new List<Account>();
    Account ac = [select External_Industry__c, External_Top_Tier__c, Industry_type__c, external_business_partner_type__c, External_Type__c from Account where name = 'Test data' Limit 1 ];
    ac.External_Industry__c ='CO1';
    ac.External_Top_Tier__c='PT1';
    ac.Industry_type__c='Dot com1';
    ac.External_Type__c='Ex1';
    ac.external_business_partner_type__c='CO21';
    updatedAccs.add(ac);

    Account ac1 = [select External_industry_type__c, Partner_top_tier__c, Industry, business_Partner_Type__c, Type from Account where name = 'Test Account' Limit 1];
    ac1.External_industry_type__c='COM1';
    ac1.Partner_top_tier__c='Platinum1';
    ac1.Industry='Communications1';
    ac1.business_Partner_Type__c='Danger1';
    ac1.Type='Customer';
    updatedAccs.add(ac1);

    UpdatePicklistValuesHandler.picklistVals=pickListValues1;
    UpdatePicklistValuesHandler.picklistMapT2S=picklistMapT2S1;
    UpdatePicklistValuesHandler.picklistMapS2T=picklistMapS2T1;

    update updatedAccs;

    System.assertEquals(ac1.External_industry_type__c, 'COM1');
    
    Test.stopTest();
  }

//scenario to cover the IntegrationUpdateAccountTriggerHelper
@isTest static void test_method_five()
{
  //Create list of Accounts
          
Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
  List<Account> accounts = new List<Account>();
  Account a = TestDataUtility.createAccount('Account 1');
   a.SAP_Company_Code__c='999996686';
  Account a1 = TestDataUtility.createAccount('Account Test');
   a1.SAP_Company_Code__c='999996611';
  accounts.add(a);
  accounts.add(a1);

  insert accounts;

  //Create user for profile as integration user
    Profile p = [SELECT Id FROM Profile WHERE Name='Integration User']; 
      User u = new User(Alias = 'standt', Email='Integrationuser@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName='Integrationuser@testorg.com.qa');

      System.runAs(u) 
      {
          Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        upsert accounts;
        System.assertEquals(p.Id, u.ProfileId);
      }
}

 //Create System administrator user profile
//scenario to cover the IntegrationUpdateAccountTriggerHelper
@isTest static void test_method_six()
{
  //Create list of Accounts
          Channelinsight.CI_TEST_TestUtils.createBaseInstallData();

  List<Account> accounts = new List<Account>();
  Account a = TestDataUtility.createAccount('Account 1');
  Account a1 = TestDataUtility.createAccount('Account Test');
  accounts.add(a);
  accounts.add(a1);

  Profile p1 = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
    User u1 = new User(Alias = 'standt', Email='sysAdminuser@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p1.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName=TestDataUtility.generateRandomString(7)+'@testorg.com');

    System.runAs(u1) {
        insert accounts;
        System.assertEquals(p1.Id, u1.ProfileId);
      }
} 

//Scenario to cover update section of the IntegrationUpdateAccountTriggerHelper

@isTest static void test_method_seven() {
          Channelinsight.CI_TEST_TestUtils.createBaseInstallData();

  Zipcode_Lookup__c zipCode = TestDataUtility.createZipCode();
  Test.startTest();
  // create account
  Account a = TestDataUtility.createAccount('Account 1');
   a.SAP_Company_Code__c='99900668';
  a.Company_Code__c='c460';
  a.Phone='(555) 555-5555';
  a.Fax='4444444444';
  a.Website='www.salesforce.com';
  a.Email__c='bjyy024@163.com';
  a.Direct_Account__c=false;
  a.Indirect_Account__c=false;
  a.End_Customer_Account__c=false;
  a.External_Top_Tier__c='ExTop';
  a.Business_Partner_Tier__c='BTier';
  a.Service_Partner_Tier__c='Servic';
  a.Solution_Partner_Tier__c='Soluti';
  a.external_industry_type__c='ind';
  a.external_business_partner_type__c='com';
  a.External_Industry__c='IT';
  a.Key_Account__c=false;
  a.Account_Level__c='Level3';
  //a.parent_sap_company_code__c='1234';
  //a.Parent_Account_Level__c='Level4';

  insert a;

  System.debug('Account a' + a);
  //update account
  Account a1 = a;
  System.debug('Account a1' + a1);
  a1.Name='Accs';
   a1.SAP_Company_Code__c='999799668';
  a1.Type='Incming';
  a1.BillingStreet='123 Mars pl';
  a1.BillingCity='San Francisco';
  a1.BillingState='CA';
  a1.BillingPostalCode=zipCode.Name;
  a1.Company_Code__c='c430';
  a1.Phone='(666) 666-6666';
  a1.Fax='2222222222';
  a1.Website='www.saleforces.com';
  a1.Email__c='John024@gmail.com';
  a1.Direct_Account__c=true;
  a1.Indirect_Account__c=true;
  a1.End_Customer_Account__c=true;
  a1.External_Top_Tier__c='Topex';
  a1.Business_Partner_Tier__c='TieerB';
  a1.Service_Partner_Tier__c='Service';
  a1.Solution_Partner_Tier__c='Solution';
  a1.external_industry_type__c='indust';
  a1.external_business_partner_type__c='communication';
  a1.External_Industry__c='Tele';
  a1.Key_Account__c=true;
  a1.Account_Level__c='Level5';
  a1.Integration_Status__c='Not In Sync';
  
  Profile p1 = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
    User u1 = new User(Alias = 'standt', Email='sysAdminuser@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p1.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName= TestDataUtility.generateRandomString(6)+'@testorg.com');

    System.runAs(u1) {
      System.debug('Account a1 during uodate' + a1);
      try {
        update a1;
      }catch(System.DmlException e) {
            System.debug('update Account caught expected exception: ' + e.getDmlMessage(0));

        //Assert Error Message if Record Type is End Customer
         System.assert(e.getMessage().contains('Please set type to Customer or Prospect for a End Customer Record Type'), 
             e.getMessage()); 

      //Assert Field
         System.assertEquals(Account.Type, e.getDmlFields(0)[0]);

      //Assert Status Code
         System.assertEquals('FIELD_CUSTOM_VALIDATION_EXCEPTION'  , 
              e.getDmlStatusCode(0));
      }
    }
  System.assertEquals(a1.Integration_Status__c,'Not In Sync');
  Test.stopTest();
}

//Scenario to run a false test on address

@isTest static void zipCodeValidationTest() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
  
  Zipcode_Lookup__c zipCode = TestDataUtility.createZipCode();
  Test.startTest();
    // create account
    Account account = TestDataUtility.createAccount('Account 1');
     account.SAP_Company_Code__c='99999668';
    insert account;
  
    System.debug('Account a' + account);
    //update account
    Account accountToUpdate = account;
    System.debug('Account accountToUpdate' + accountToUpdate);
    accountToUpdate.Name='Accs';
    accountToUpdate.BillingStreet='123 Mars pl';
    accountToUpdate.BillingCity='San Francisco';
    accountToUpdate.BillingState='CA';
    accountToUpdate.BillingPostalCode='12345';
    accountToUpdate.Company_Code__c='c430';
    accountToUpdate.Phone='(666) 666-6666';
    accountToUpdate.Fax='2222222222';
    accountToUpdate.Website='www.saleforces.com';
    accountToUpdate.Email__c='John024@gmail.com';
    
      System.debug('Account accountToUpdate during uodate' + accountToUpdate);
        try {
          update accountToUpdate;
        }catch(System.DmlException e) {
              System.debug('update Account caught expected exception: ' + e.getDmlMessage(0));
  
          //Assert Error Message if Record Type is End Customer
           System.assert(e.getMessage().contains('Invalid Zip'), 
               e.getMessage()); 
  
        //Assert Field
           //System.assertEquals(Account.BillingPostalCode, e.getDmlFields(0)[0]);
  
        //Assert Status Code
           System.assertEquals('FIELD_CUSTOM_VALIDATION_EXCEPTION'  , 
                e.getDmlStatusCode(0));
        }
  
  Test.stopTest();
  }
  
  //Scenario to run a false test on address

@isTest static void countryValidationTest() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
  
  Zipcode_Lookup__c zipCode = TestDataUtility.createZipCode();
  Test.startTest();
    // create account
    Account account = TestDataUtility.createAccount('Account 1');
    account.SAP_Company_Code__c='999996658';
    insert account;
  
    System.debug('Account a' + account);
    //update account
    Account accountToUpdate = account;
    System.debug('Account accountToUpdate' + accountToUpdate);
    accountToUpdate.Name='Accs';
    accountToUpdate.BillingStreet='123 Mars pl';
    accountToUpdate.BillingCity='San Francisco';
    accountToUpdate.BillingState='CA';
    accountToUpdate.BillingPostalCode=zipCode.Name;
    accountToUpdate.BillingCountry='US';
    accountToUpdate.Company_Code__c='c430';
    accountToUpdate.Phone='(666) 666-6666';
    accountToUpdate.Fax='2222222222';
    accountToUpdate.Website='www.saleforces.com';
    accountToUpdate.Email__c='John024@gmail.com';
    
      System.debug('Account accountToUpdate during uodate' + accountToUpdate);
        try {
          update accountToUpdate;
        }catch(System.DmlException e) {
              System.debug('update Account caught expected exception: ' + e.getDmlMessage(0));
  
          //Assert Error Message if Record Type is End Customer
           System.assert(e.getMessage().contains(' Country must be'), 
               e.getMessage()); 
  
        //Assert Field
           //System.assertEquals(Account.BillingPostalCode, e.getDmlFields(0)[0]);
  
        //Assert Status Code
           System.assertEquals('FIELD_CUSTOM_VALIDATION_EXCEPTION'  , 
                e.getDmlStatusCode(0));
        }
  
  Test.stopTest();
  }
  
  //Scenario to run a false test on address

@isTest static void stateAndZipCodeValidationTest() {
  
  Zipcode_Lookup__c zipCode = TestDataUtility.createZipCode();
  Test.startTest();
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();

    // create account
    Account account = TestDataUtility.createAccount('Account 1');
    account.SAP_Company_Code__c='99499668';
    insert account;
  
    System.debug('Account a' + account);
    //update account
    Account accountToUpdate = account;
    System.debug('Account accountToUpdate' + accountToUpdate);
    accountToUpdate.Name='Accs';
    accountToUpdate.BillingStreet='123 Mars pl';
    accountToUpdate.BillingCity='San Francisco';
    accountToUpdate.BillingState='VA';
    accountToUpdate.BillingPostalCode=zipCode.Name;
    accountToUpdate.Company_Code__c='c430';
    accountToUpdate.Phone='(666) 666-6666';
    accountToUpdate.Fax='2222222222';
    accountToUpdate.Website='www.saleforces.com';
    accountToUpdate.Email__c='John024@gmail.com';
    
      System.debug('Account accountToUpdate during uodate' + accountToUpdate);
        try {
          update accountToUpdate;
        }catch(System.DmlException e) {
              System.debug('update Account caught expected exception: ' + e.getDmlMessage(0));
  
          //Assert Error Message if Record Type is End Customer
           System.assert(e.getMessage().contains('must be'), 
               e.getMessage()); 
  
        //Assert Field
           //System.assertEquals(Account.BillingPostalCode, e.getDmlFields(0)[0]);
  
        //Assert Status Code
           System.assertEquals('FIELD_CUSTOM_VALIDATION_EXCEPTION'  , 
                e.getDmlStatusCode(0));
        }
  
  Test.stopTest();
  }
  // Added By Vijay 
  @isTest static void coownertest() {
     
     Test.StartTest();
        

    profile p = [select id, Name from profile where name= 'System Administrator' limit 1];
    UserRole r=[SELECT DeveloperName,Id,Name FROM UserRole WHERE Name = 'IT KAM' limit 1]; 
        User u1 = new User(Alias = 'standt', Email='coowner@ss.com', 
                  EmailEncodingKey='UTF-8', LastName='Account', LanguageLocaleKey='en_US', 
                  LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId=r.id,
                  TimeZoneSidKey='America/Los_Angeles', UserName='abc11234@abc.com');
        insert u1;
        system.runAs(u1){
            Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
    Account acc1 = new account(Name ='Co Account Owner', Type='Customer',SAP_Company_Code__c = 'AAA',BillingStreet='100 challengers road',BillingCity='Ridgefield Park',
                                 BillingState='NJ',BillingCountry='US',BillingPostalCode='07660');
        insert acc1;
        }
    
  Test.stopTest();
  }
  
  @isTest static void test_method_insertkey()
{
         Channelinsight.CI_TEST_TestUtils.createBaseInstallData();

  //Create list of Accounts
  List<Account> accounts = new List<Account>();
  Account a = TestDataUtility.createAccount('Account 1');
    a.billingStreet = '100 challengers road';
    a.BillingCity='Ridgefield Park';
    a.BillingState='NJ';
    a.BillingPostalCode='07660';
    a.BillingCountry='US';
  accounts.add(a);
 

  Profile p1 = [SELECT Id FROM Profile WHERE Name='Integration User']; 
    User u1 = new User(Alias = 'standt', Email='sysAdminuser@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p1.Id, Sales_Team__c='Finance',
      TimeZoneSidKey='America/Los_Angeles', UserName=TestDataUtility.generateRandomString(7)+'@testorg.com');

    System.runAs(u1) {
        insert accounts;
        System.assertEquals(p1.Id, u1.ProfileId);
      }
}
  
  
  @isTest static void test_method_keyaccount()
{
  //Create list of Accounts
          Channelinsight.CI_TEST_TestUtils.createBaseInstallData();

  Account a = TestDataUtility.createAccount('Account 1');


  Profile p1 = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
  Profile p2 = [SELECT Id FROM Profile WHERE Name='Integration User']; 
  list<user> ur=new list<User>();
    User u1 = new User(Alias = 'standt', Email='sysAdminuser@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p1.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName=TestDataUtility.generateRandomString(7)+'@testorg.com');
       User u2 = new User(Alias = 'standt', Email='sysAdminuser21@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p2.Id, Sales_Team__c='Finance',
      TimeZoneSidKey='America/Los_Angeles', UserName=TestDataUtility.generateRandomString(7)+'@testorg.com');
        ur.add(u1);
         ur.add(u2);
         insert ur;
    System.runAs(u1) {
        a.Ownerid=u1.id;
        insert a;
        System.assertEquals(p1.Id, u1.ProfileId);
      }
       System.runAs(u1) {
         Account a1 = a;
         a1.Ownerid=u2.id;
         a1.Key_Account__c=true;
             a1.Approval_Status__c='Approved-Admin';
         update a1;
        System.assertEquals(p1.Id, u1.ProfileId);
      }
       System.runAs(u2) {
         Account a1 = a;
        a1.Ownerid=u2.id;
         a1.Approval_Status__c='Non Key';
           a1.Previous_OwnerId__c=u2.id;
         update a1;
          System.assertEquals(p2.Id, u2.ProfileId);
      }
     System.runAs(u2) {
         Account a1 = a;
         a1.Old_Owner_Sales_Team__c='Federal';
         a1.Approval_Status__c='Pending';
         a1.Previous_OwnerId__c=u2.id;
         update a1;
        
        System.assertEquals(p2.Id, u2.ProfileId);
      }

     System.runAs(u2) {
         Account a1 = a;
         a1.Old_Owner_Sales_Team__c='Federal';
         a1.Approval_Status__c='Rejected';
         update a1;
        
        System.assertEquals(p2.Id, u2.ProfileId);
      }
     
    
} 
    @isTest static void test_method_keyaccount2()
{
  //Create list of Accounts
          Channelinsight.CI_TEST_TestUtils.createBaseInstallData();

  Account a = TestDataUtility.createAccount('Account 1');


  Profile p1 = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
  Profile p2 = [SELECT Id FROM Profile WHERE Name='Integration User']; 
  list<user> ur=new list<User>();
    User u1 = new User(Alias = 'standt', Email='sysAdminuser@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p1.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName=TestDataUtility.generateRandomString(7)+'@testorg.com');
       User u2 = new User(Alias = 'standt', Email='sysAdminuser21@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p2.Id, Sales_Team__c='Finance',
      TimeZoneSidKey='America/Los_Angeles', UserName=TestDataUtility.generateRandomString(7)+'@testorg.com');
     User u3 = new User(Alias = 'standt', Email='sysAdminuser4521@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p2.Id, Sales_Team__c='Finance',
      TimeZoneSidKey='America/Los_Angeles', UserName=TestDataUtility.generateRandomString(7)+'@testorg.com');
    User u4 = new User(Alias = 'standt', Email='sysAdminuser4521@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p2.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName=TestDataUtility.generateRandomString(7)+'@testorg.com');
        ur.add(u4);
        ur.add(u1);
         ur.add(u2);
    ur.add(u3);
         insert ur;
    System.runAs(u1) {
        a.Ownerid=u1.id;
        a.Key_Account__c=true;
        insert a;
        System.assertEquals(p1.Id, u1.ProfileId);
      }
    System.runAs(u1) {
         Account a1 = a;
        a.Ownerid=u2.id;
        a.Key_Account__c=true;
        update a;
        System.assertEquals(p1.Id, u1.ProfileId);
      }
      System.runAs(u2) {
         Account a1 = a;
     
        update a;
        System.assertEquals(p1.Id, u1.ProfileId);
      }
      System.runAs(u1) {
         Account a1 = a;
                a.Key_Account__c=false;
        update a;
        System.assertEquals(p1.Id, u1.ProfileId);
      }
     System.runAs(u2) {
         Account a1 = a;
                a.Ownerid=u2.id;
        update a;
        System.assertEquals(p1.Id, u1.ProfileId);
      }
} 
    @isTest static void test_method_keyaccount3()
{
  //Create list of Accounts
          Channelinsight.CI_TEST_TestUtils.createBaseInstallData();

  Account a = TestDataUtility.createAccount('Account 1');


  Profile p1 = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
  Profile p2 = [SELECT Id FROM Profile WHERE Name='Integration User']; 
  list<user> ur=new list<User>();
    User u1 = new User(Alias = 'standt', Email='sysAdminuser@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p1.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName=TestDataUtility.generateRandomString(7)+'@testorg.com');
       User u2 = new User(Alias = 'standt', Email='sysAdminuser21@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p2.Id, Sales_Team__c='Finance',
      TimeZoneSidKey='America/Los_Angeles', UserName=TestDataUtility.generateRandomString(7)+'@testorg.com');
     User u3 = new User(Alias = 'standt', Email='sysAdminuser4521@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p2.Id, Sales_Team__c='Finance',
      TimeZoneSidKey='America/Los_Angeles', UserName=TestDataUtility.generateRandomString(7)+'@testorg.com');
    User u4 = new User(Alias = 'standt', Email='sysAdminuser4521@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p2.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName=TestDataUtility.generateRandomString(7)+'@testorg.com');
        ur.add(u4);
        ur.add(u1);
         ur.add(u2);
    ur.add(u3);
         insert ur;
    System.runAs(u1) {
        a.Ownerid=u1.id;
        a.Key_Account__c=true;
        a.Owner2__c=u2.id;
        a.Channelinsight__CI_Synch__c=true;
        insert a;
        System.assertEquals(p1.Id, u1.ProfileId);
      }
    System.runAs(u1) {
         Account a1 = a;
        a.Ownerid=u2.id;
        a.Key_Account__c=false;
        a.Owner2__c=u3.id;
        update a;
        System.assertEquals(p1.Id, u1.ProfileId);
      }
      
     System.runAs(u2) {
         Account a1 = a;
                a1.Ownerid=u3.id;
        update a1;
        System.assertEquals(p1.Id, u1.ProfileId);
      }
    System.runAs(u2) {
         Account a1 = a;
        a1.Approval_Status__c='Rejected';
        update a1;
        System.assertEquals(p1.Id, u1.ProfileId);
      } 
      
      
    
} 
  @isTest static void test_method_keyaccount4()
{
  //Create list of Accounts
          Channelinsight.CI_TEST_TestUtils.createBaseInstallData();

  Account a = TestDataUtility.createAccount('Account 1');


  Profile p1 = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
  Profile p2 = [SELECT Id FROM Profile WHERE Name='Integration User']; 
  list<user> ur=new list<User>();
    User u1 = new User(Alias = 'standt', Email='sysAdminuser@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p1.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName=TestDataUtility.generateRandomString(7)+'@testorg.com');
       User u2 = new User(Alias = 'standt', Email='sysAdminuser21@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p2.Id, Sales_Team__c='Finance',
      TimeZoneSidKey='America/Los_Angeles', UserName=TestDataUtility.generateRandomString(7)+'@testorg.com');
     User u3 = new User(Alias = 'standt', Email='sysAdminuser4521@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p2.Id, Sales_Team__c='Finance',
      TimeZoneSidKey='America/Los_Angeles', UserName=TestDataUtility.generateRandomString(7)+'@testorg.com');
    User u4 = new User(Alias = 'standt', Email='sysAdminuser4521@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p2.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName=TestDataUtility.generateRandomString(7)+'@testorg.com');
        ur.add(u4);
        ur.add(u1);
         ur.add(u2);
    ur.add(u3);
         insert ur;
         
        
         Account a2=TestDataUtility.createAccount('Account 432');
         //a2.Channelinsight__CI_Synch__c=true;
       System.runAs(u3) {
         insert a2;
        System.assertEquals(p2.Id, u3.ProfileId);
      } 
        System.runAs(u2) {
         Account a1 = a2;
        a1.Key_Account__c=false;
        a1.Approval_Status__c='Reject Insert';
        update a1;
        System.assertEquals(p2.Id, u2.ProfileId);
      } 
      Account a3=TestDataUtility.createAccount('Account 432');
      a3.Channelinsight__CI_Synch__c=true;
       System.runAs(u3) {
        
        
        insert a3;
        System.assertEquals(p2.Id, u3.ProfileId);
      } 
        System.runAs(u2) {
         Account a1 = a3;
        a1.Key_Account__c=true;
        a1.Approval_Status__c='Approved';
        a1.SAP_Company_Code__c='';
        update a1;
        System.assertEquals(p2.Id, u2.ProfileId);
      } 
      }
}