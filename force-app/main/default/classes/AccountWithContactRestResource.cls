/**
 *  https://samsungsea--qa.cs18.my.salesforce.com/services/apexrest/AccountWithContact/0013600000SacvW
 *  by hong.jeon
 */
@RestResource(urlMapping='/AccountWithContact/*')
global with sharing class AccountWithContactRestResource {
     
    @HttpGet
   global static SFDCStub.AccountWithContactResponse getAccountWithContact() {
     RestRequest req = RestContext.request;
     RestResponse res = RestContext.response;
     
     String accId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
     Account acc = null; 
     string status = 'null';
     try {
       acc = [SELECT Id, Name, Owner.Sales_Team__c, Owner.Name, Owner.UserRole.Name, SAP_Company_Code__c, Type, Key_Account__c, LE_Record_Type__c, 
             Parent.Id, Parent.Name, Industry_Type__c, Industry, External_Industry__c, BillingCountry, BillingState, BillingPostalCode, 
             YearStarted, NumberOfEmployees, Description FROM Account WHERE Id=:accId];
     } catch(Exception e) {
       String errmsg = 'DB Query Error - '+e.getmessage() + ' - '+e.getStackTraceString();
            
            SFDCStub.AccountWithContactResponse response = new SFDCStub.AccountWithContactResponse();
       response.inputHeaders.MSGGUID = cihStub.giveGUID();
       response.inputHeaders.IFID   = '';
       response.inputHeaders.IFDate   = datetime.now().format('YYMMddHHmmss', 'America/New_York');
       response.inputHeaders.MSGSTATUS = 'F';
       response.inputHeaders.ERRORTEXT = 'Fail to retrieve an Account('+accId+') - '+req.requestURI + ', errmsg=' + errmsg;
       response.inputHeaders.ERRORCODE = '';
       
       return response;
     }         
     
     SFDCStub.AccountWithContactResponse response = new SFDCStub.AccountWithContactResponse();
     response.inputHeaders.MSGGUID = cihStub.giveGUID();
     response.inputHeaders.IFID = '';
     response.inputHeaders.IFDate = datetime.now().format('YYMMddHHmmss', 'America/New_York');
     response.inputHeaders.MSGSTATUS = 'S';
     response.inputHeaders.ERRORTEXT = '';
     response.inputHeaders.ERRORCODE = '';
     
     response.body.PAGENO = 1;
     response.body.ROWCNT = 500;
     response.body.COUNT = 1;
     
     SFDCStub.AccountWithContact accountWithContact = new SFDCStub.AccountWithContact(acc,status);
     response.body.Account.add(accountWithContact);
     
     string contactstatus='null';
     List<Contact> contacts = [SELECT Id, Email, Fax, MailingStreet, MailingCity, MailingState, MailingPostalCode, 
                   MailingCountry, FirstName, LastName, Phone, Title FROM Contact WHERE AccountId =:accId];
     for(Contact contact : contacts) {
       SFDCStub.ContactItem item = new SFDCStub.ContactItem(contact,contactstatus);
       accountWithContact.Contact.add(item);
     }
     return response;
   }
   
   @HttpPost
   global static SFDCStub.AccountWithContactResponse getAccountWithContactList() {
     Integration_EndPoints__c endpoint = Integration_EndPoints__c.getInstance(SFDCStub.FLOW_081);
     String layoutId = !Test.isRunningTest() ? endpoint.layout_id__c : SFDCStub.LAYOUTID_081;
     
     RestRequest req = RestContext.request;
     String postBody = req.requestBody.toString();
     
     SFDCStub.AccountWithContactResponse response = new SFDCStub.AccountWithContactResponse();
     
     SFDCStub.AccountWithContactRequest request = null;
     try {
       request = (SFDCStub.AccountWithContactRequest)json.deserialize(postBody, SFDCStub.AccountWithContactRequest.class);
     } catch(Exception e) {
       response.inputHeaders.MSGGUID = '';
       response.inputHeaders.IFID   = '';
       response.inputHeaders.IFDate   = '';
       response.inputHeaders.MSGSTATUS = 'F';
       response.inputHeaders.ERRORTEXT = 'Invalid Request Message. - '+postBody;
       response.inputHeaders.ERRORCODE = '';
       
       return response;
     }
     
     response.inputHeaders.MSGGUID = request.inputHeaders.MSGGUID;
     response.inputHeaders.IFID     = request.inputHeaders.IFID;
     response.inputHeaders.IFDate  = request.inputHeaders.IFDate;
     
     message_queue__c mq = new message_queue__c(MSGUID__c = response.inputHeaders.MSGGUID, 
                           MSGSTATUS__c = 'S',  
                           Integration_Flow_Type__c = SFDCStub.FLOW_081,
                           IFID__c = response.inputHeaders.IFID,
                           IFDate__c = response.inputHeaders.IFDate,
                           External_Id__c = request.body.CONSUMER,
                           Status__c = 'success',
                           Object_Name__c = 'AccountWithContact', 
                           retry_counter__c = 0);
                           
     DateTime lastSuccessfulRun = endpoint.last_successful_run__c;
     DateTime currentDateTime = datetime.now();
     if(lastSuccessfulRun==null) {
       lastSuccessfulRun = currentDateTime - 2;
     }
     
     Integer limitCnt = SFDCStub.LIMIT_COUNT;
     Integer offSetNo = limitCnt*(request.body.PAGENO-1);
     system.debug('Offset----------->'+offSetNo);
     if(offSetNo>=2000) {
       String errmsg = 'Invalid Page Number - ('+request.body.PAGENO+'), The offset should be less than 2000.';
            response.inputHeaders.MSGSTATUS = 'F';
       response.inputHeaders.ERRORTEXT = errmsg;
       response.inputHeaders.ERRORCODE = '';
       
       mq.Status__c = 'failed';
       mq.MSGSTATUS__c = 'F';
       mq.ERRORTEXT__c = errmsg;
       insert mq;
       
       return response;
     }
     
     List<String> excludedUsers = Label.Users_Excluded_from_Integration.split(',');//Added to exclude data last modified by these users----03/20/2019
     
     List<Account> data = null;
     try {
       data = [SELECT Id, Name, Owner.Sales_Team__c, Owner.Name, Owner.UserRole.Name, SAP_Company_Code__c, Type, Key_Account__c, LE_Record_Type__c, 
             Parent.Id, Parent.Name, Industry_Type__c, Industry, External_Industry__c, BillingCountry, BillingState, BillingPostalCode, 
             YearStarted, NumberOfEmployees, Description, LastActivityDate, LastModifiedDate, createddate, isdeleted
             FROM Account WHERE LastModifiedById!=:excludedUsers and LastModifiedDate >= :lastSuccessfulRun and LastModifiedDate <=:currentDateTime ORDER BY LastModifiedDate ASC LIMIT :limitCnt OFFSET :offsetNo ALL ROWS];
     } catch(Exception e) {
       // todo : Exception Handling - message queue
       String errmsg = 'DB Query Error - '+e.getmessage() + ' - '+e.getStackTraceString();
            response.inputHeaders.MSGSTATUS = 'F';
       response.inputHeaders.ERRORTEXT = errmsg;
       response.inputHeaders.ERRORCODE = '';
       
       mq.Status__c = 'failed';
       mq.MSGSTATUS__c = 'F';
       mq.ERRORTEXT__c = errmsg;
       insert mq;
       
       return response;
     }  
              
     response.inputHeaders.MSGSTATUS = 'S';
     response.inputHeaders.ERRORTEXT = '';
     response.inputHeaders.ERRORCODE = '';
                           
     response.body.PAGENO = request.body.PAGENO;
     response.body.ROWCNT = limitCnt;
     response.body.COUNT  = data.size();
     
     Map<String, SFDCStub.AccountWithContact> mapData = new Map<String, SFDCStub.AccountWithContact>();
     DateTime LastModifiedDate = null;
     string status;
     List<message_queue__c> mqList = new List<message_queue__c>();
     for(Account acc : data) {
       try {
         if(acc.createddate>=lastSuccessfulRun){
             status = 'New';
         }
         if(acc.createddate<lastSuccessfulRun && acc.LastModifiedDate>=lastSuccessfulRun){
             status = 'Modified';
         }
         if(acc.isdeleted==true){
             status = 'Deleted';
         }
         SFDCStub.AccountWithContact obj = new SFDCStub.AccountWithContact(acc,status);
         response.body.Account.add(obj);
         mapData.put(obj.Id, obj);
         LastModifiedDate = obj.instance.LastModifiedDate;
         system.debug('Record--------->'+response.body.Account);
       } catch(Exception e) {
         String errmsg = 'Fail to create an Account - '+e.getmessage() + ' - '+e.getStackTraceString();
              message_queue__c pemsg = new message_queue__c(MSGUID__c = response.inputHeaders.MSGGUID, MSGSTATUS__c = 'F',  Integration_Flow_Type__c = SFDCStub.FLOW_081,
                       IFID__c = response.inputHeaders.IFID, IFDate__c = response.inputHeaders.IFDate, Status__c = 'failed', Object_Name__c = 'AccountWithContact', retry_counter__c = 0);
         pemsg.ERRORTEXT__c = errmsg;
         pemsg.Identification_Text__c = acc.Id;
         //insert pemsg;
         mqList.add(pemsg);
         System.debug(errmsg);
       }
     }
     if(mqList.size()>0){
         insert mqList;
     }
     
     string contactstatus;
     List<Contact> contacts = [SELECT AccountId, Id, Email, Fax, MailingStreet, MailingCity, MailingState, MailingPostalCode, 
                   MailingCountry, FirstName, LastName, Phone, Title, isdeleted, createddate, LastModifiedDate 
                   FROM Contact 
                   WHERE AccountId in :mapData.keySet() and LastModifiedById!=:excludedUsers ALL ROWS];
     for(Contact contact : contacts) {
       try {
         if(contact.createddate>=lastSuccessfulRun){
             contactstatus = 'New';
         }
         if(contact.createddate<lastSuccessfulRun && contact.LastModifiedDate>=lastSuccessfulRun){
             contactstatus = 'Modified';
         }
         if(contact.isdeleted==true){
             contactstatus = 'Deleted';
         }
         SFDCStub.ContactItem item = new SFDCStub.ContactItem(contact,contactstatus);
         SFDCStub.AccountWithContact awc = mapData.get(contact.AccountId);
         awc.Contact.add(item);
       } catch(Exception e) {
         String errmsg = 'Fail to create a Contact - '+e.getmessage() + ' - '+e.getStackTraceString();
              message_queue__c pemsg = new message_queue__c(MSGUID__c = response.inputHeaders.MSGGUID, MSGSTATUS__c = 'F',  Integration_Flow_Type__c = SFDCStub.FLOW_081,
                       IFID__c = response.inputHeaders.IFID, IFDate__c = response.inputHeaders.IFDate,  Status__c = 'failed', Object_Name__c = 'AccountWithContact', retry_counter__c = 0);
         pemsg.ERRORTEXT__c = errmsg;
         pemsg.Identification_Text__c = contact.Id;
         insert pemsg;
         System.debug(errmsg);
       }
     }
     
     mq.last_successful_run__c = currentDateTime;
     mq.Object_Name__c = mq.Object_Name__c + ', ' + request.body.PAGENO + ', ' + response.body.COUNT;
     insert mq;
     
     if(!Test.isRunningTest() && response.body.COUNT!=0 && ((offSetNo+limitCnt)>=2000 || response.body.ROWCNT!=response.body.COUNT)) {
       endpoint.last_successful_run__c = currentDateTime;
       update endpoint;
     }
     return response;
   }
   
}