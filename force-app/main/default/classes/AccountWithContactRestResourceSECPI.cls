/**
 *  https://samsungsea--qa.cs65.my.salesforce.com/services/apexrest/AccountWithContactSECPI/0013600000SacvW
 */
@RestResource(urlMapping='/AccountWithContactSECPI/*')
global with sharing class AccountWithContactRestResourceSECPI {
     
    @HttpGet
    global static SFDCStubSECPI.AccountWithContactResponse getAccountWithContactSECPI() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        
        String accId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        Account acc = null; 
        string status = 'null';
        try {
            acc = [SELECT Id, Name, Owner.Sales_Team__c, Owner.Name, Owner.UserRole.Name, SAP_Company_Code__c, Type, Key_Account__c, LE_Record_Type__c, 
                        Parent.Id, Parent.Name, Industry_Type__c, Industry, External_Industry__c, BillingCountry, BillingState, BillingPostalCode, 
                        YearStarted, NumberOfEmployees, createddate,lastmodifieddate,LastActivityDate,SystemModStamp FROM Account WHERE Id=:accId];
        } catch(Exception e) {
            String errmsg = 'DB Query Error - '+e.getmessage() + ' - '+e.getStackTraceString();
            
            SFDCStubSECPI.AccountWithContactResponse response = new SFDCStubSECPI.AccountWithContactResponse();
            response.inputHeaders.MSGGUID = cihStub.giveGUID();
            response.inputHeaders.IFID  = '';
            response.inputHeaders.IFDate    = datetime.now().format('YYMMddHHmmss', 'America/New_York');
            response.inputHeaders.MSGSTATUS = 'F';
            response.inputHeaders.ERRORTEXT = 'Fail to retrieve an Account('+accId+') - '+req.requestURI + ', errmsg=' + errmsg;
            response.inputHeaders.ERRORCODE = '';
            
            return response;
        }              
        
        SFDCStubSECPI.AccountWithContactResponse response = new SFDCStubSECPI.AccountWithContactResponse();
        response.inputHeaders.MSGGUID = cihStub.giveGUID();
        response.inputHeaders.IFID = '';
        response.inputHeaders.IFDate = datetime.now().format('YYMMddHHmmss', 'America/New_York');
        response.inputHeaders.MSGSTATUS = 'S';
        response.inputHeaders.ERRORTEXT = '';
        response.inputHeaders.ERRORCODE = '';
        
        response.body.PAGENO = 1;
        response.body.ROWCNT = 500;
        response.body.COUNT = 1;
        
        SFDCStubSECPI.AccountWithContact accountWithContact = new SFDCStubSECPI.AccountWithContact(acc,status);
        response.body.Account.add(accountWithContact);
        
        string contactstatus='null';
        List<Contact> contacts = [SELECT Id, Email, Fax, MailingStreet, MailingCity, MailingState, MailingPostalCode, 
                                    MailingCountry, FirstName, LastName, Phone, Title, createddate, lastmodifieddate FROM Contact WHERE AccountId =:accId];
        for(Contact contact : contacts) {
            SFDCStubSECPI.ContactItem item = new SFDCStubSECPI.ContactItem(contact,contactstatus);
            accountWithContact.Contact.add(item);
        }
        return response;
    }
    
    @HttpPost
    global static SFDCStubSECPI.AccountWithContactResponse getAccountWithContactListSECPI() {
        integer pageNum;
        //integer pageLimit = 10;
        integer pageLimit = integer.valueOf(label.SECPI_Account_I_F_Page_Limit);
        Integration_EndPoints__c endpoint = Integration_EndPoints__c.getInstance(SFDCStubSECPI.FLOW_081);
        String layoutId = !Test.isRunningTest() ? endpoint.layout_id__c : SFDCStubSECPI.LAYOUTID_081;
        
        RestRequest req = RestContext.request;
        String postBody = req.requestBody.toString();
        
        SFDCStubSECPI.AccountWithContactResponse response = new SFDCStubSECPI.AccountWithContactResponse();
        
        SFDCStubSECPI.AccountWithContactRequest request = null;
        try {
            request = (SFDCStubSECPI.AccountWithContactRequest)json.deserialize(postBody, SFDCStubSECPI.AccountWithContactRequest.class);
        } catch(Exception e) {
            response.inputHeaders.MSGGUID = '';
            response.inputHeaders.IFID  = '';
            response.inputHeaders.IFDate    = '';
            response.inputHeaders.MSGSTATUS = 'F';
            response.inputHeaders.ERRORTEXT = 'Invalid Request Message. - '+postBody;
            response.inputHeaders.ERRORCODE = '';
            
            return response;
        }
        
        response.inputHeaders.MSGGUID = request.inputHeaders.MSGGUID;
        response.inputHeaders.IFID    = request.inputHeaders.IFID;
        response.inputHeaders.IFDate  = request.inputHeaders.IFDate;
        
        string dates = 'From:'+request.body.SEARCH_DTTM_FROM+', To:'+request.body.SEARCH_DTTM_TO;
        
        message_queue__c mq = new message_queue__c(MSGUID__c = response.inputHeaders.MSGGUID, 
                                                    MSGSTATUS__c = 'S',  
                                                    Integration_Flow_Type__c = SFDCStubSECPI.FLOW_081,
                                                    IFID__c = response.inputHeaders.IFID,
                                                    IFDate__c = response.inputHeaders.IFDate,
                                                    External_Id__c = request.body.CONSUMER,
                                                    Status__c = 'success',
                                                    Object_Name__c = 'AccountWithContact', 
                                                    retry_counter__c = 0,
                                                    Identification_Text__c = dates);
        //Added logic for PAGENO Limit --- starts here
        if(request.body.PAGENO!=null){
            pageNum = request.body.PAGENO;
            System.debug('PageNum----->'+pageNum);
        }else{
            String errmsg = 'Page Number is Missing in the Request';
            response.inputHeaders.MSGSTATUS = 'F';response.inputHeaders.ERRORTEXT = errmsg; response.inputHeaders.ERRORCODE = ''; mq.Status__c = 'failed'; mq.MSGSTATUS__c = 'F'; mq.ERRORTEXT__c = errmsg;
            insert mq;
            
            return response;
        }
        
        if(pageNum>pageLimit){
            Datetime new_SEARCH_DTTM = endpoint.Last_Successful_Run_Last_Modified_Date__c;
            System.debug('new SERACH_DTTM----->'+new_SEARCH_DTTM);
            String errmsg = 'SFDC PAGE LIMIT EXCEEDED. Please use new SEARCH_DTTM_FROM:'+new_SEARCH_DTTM+' and start from PAGENO:1 ' ;
            
            response.inputHeaders.MSGSTATUS = 'F';
            response.inputHeaders.ERRORTEXT = errmsg;
            response.inputHeaders.ERRORCODE = 'PAGE_LIMIT_EXCEEDED';
            response.inputHeaders.SEARCH_DTTM_FROM = SFDCStubSECPI.DtTimetoString(new_SEARCH_DTTM);
            
            mq.Status__c = 'failed'; mq.MSGSTATUS__c = 'F'; mq.ERRORTEXT__c = errmsg;
            insert mq;
            
            return response;
        }
        //Ends Here
        
        //Block the callout if the Sent data delete batch is in process.
        List<AsyncApexJob> DeleteBatchProcessing = [select id,TotalJobItems,status,createddate from AsyncApexJob where apexclass.name='Batch_AccountDataToSECPI_Delete' and (status='Holding' or status='Queued' or status='Preparing' or status='Processing') order by createddate desc limit 1];
        if(DeleteBatchProcessing.size()>0){    
            String errmsg = 'The Sent Data Delete Batch is in Process. Try callout after 10 mins';
            response.inputHeaders.MSGSTATUS = 'F';response.inputHeaders.ERRORTEXT = errmsg; response.inputHeaders.ERRORCODE = ''; mq.Status__c = 'failed'; mq.MSGSTATUS__c = 'F'; mq.ERRORTEXT__c = errmsg;
            insert mq;
            
            return response;
        }
        
        string DateFormat = 'yyyymmddhhmmss';
        string DateFormat2 = 'yyyymmdd';
        
        //Added the logic for New request format. Request includes FROM and TO SEARCH_DTTM--05/16/2018
        //Checking the date format.
        if((request.body.SEARCH_DTTM_FROM!=null && request.body.SEARCH_DTTM_FROM!='' && request.body.SEARCH_DTTM_FROM.length()!=DateFormat.length() && request.body.SEARCH_DTTM_FROM.length()!=DateFormat2.length())
            || (request.body.SEARCH_DTTM_TO!=null && request.body.SEARCH_DTTM_TO!='' && request.body.SEARCH_DTTM_TO.length()!=DateFormat.length() && request.body.SEARCH_DTTM_TO.length()!=DateFormat2.length())){
            String errmsg = 'Incorrect SEARCH_DTTM format. The Date Format must be yyyymmddhhmmss or yyyymmdd';
            response.inputHeaders.MSGSTATUS = 'F';
            response.inputHeaders.ERRORTEXT = errmsg;
            response.inputHeaders.ERRORCODE = 'Incorrect SEARCH_DTTM format';
            
            mq.Status__c = 'failed';
            mq.MSGSTATUS__c = 'F';
            mq.ERRORTEXT__c = errmsg;
            insert mq;
            
            return response;
        }
        //Check the Missng SEARCH_DTTM_TO and SEARCH_DTTM_FROM
        if((((request.body.SEARCH_DTTM_TO==null || request.body.SEARCH_DTTM_TO=='') && request.body.SEARCH_DTTM_FROM!=null && request.body.SEARCH_DTTM_FROM!=''))
            || (((request.body.SEARCH_DTTM_FROM==null || request.body.SEARCH_DTTM_FROM=='') && request.body.SEARCH_DTTM_TO!=null && request.body.SEARCH_DTTM_TO!=''))){
            String errmsg = 'SEARCH_DTTM_FROM or SEARCH_DTTM_TO is missing. The request should include both FROM and TO dates while sending TimeStamps';
            response.inputHeaders.MSGSTATUS = 'F';
            response.inputHeaders.ERRORTEXT = errmsg;
            response.inputHeaders.ERRORCODE = '';
            
            mq.Status__c = 'failed';
            mq.MSGSTATUS__c = 'F';
            mq.ERRORTEXT__c = errmsg;
            insert mq;
            
            return response;
        }
        
        if(request.body.SEARCH_DTTM_FROM!=null && request.body.SEARCH_DTTM_FROM!='' && request.body.SEARCH_DTTM_FROM.length()==DateFormat2.length()){
            request.body.SEARCH_DTTM_FROM=request.body.SEARCH_DTTM + '000000';
        }
        if(request.body.SEARCH_DTTM_TO!=null && request.body.SEARCH_DTTM_TO!='' && request.body.SEARCH_DTTM_TO.length()==DateFormat2.length()){
            request.body.SEARCH_DTTM_TO=request.body.SEARCH_DTTM + '000000';
        }
        
        DateTime lastSuccessfulRun = endpoint.last_successful_run__c;
        DateTime currentDateTime = datetime.now();
        DateTime SearchDateTo;
        DateTime SearchDateFrom;
        //Set From and To timestamps for Query
        if((request.body.SEARCH_DTTM_FROM!=null && request.body.SEARCH_DTTM_FROM!='')
            &&(request.body.SEARCH_DTTM_TO!=null && request.body.SEARCH_DTTM_TO!='')){
            SearchDateTo = setDateFromString(request.body.SEARCH_DTTM_TO);
            system.debug('SearchDateTo----->'+SearchDateTo);
            SearchDateFrom = setDateFromString(request.body.SEARCH_DTTM_FROM);
            system.debug('SearchDateFrom----->'+SearchDateFrom);
        }else{
            SearchDateTo = currentDateTime;
            system.debug('SearchDateTo----->'+SearchDateTo);
            SearchDateFrom = lastSuccessfulRun;
        }
        
        //Checing the SEARCH_DTTM_FROM and SEARCH_DTTM_TO time difference
        integer SecsDiff = Integer.valueOf((SearchDateTo.getTime() - SearchDateFrom.getTime())/(1000));
        if(SecsDiff>86400){
            String errmsg = 'Difference between SEARCH_DTTM_FROM and SEARCH_DTTM_TO should be less or Equal to 1 day';
            response.inputHeaders.MSGSTATUS = 'F';
            response.inputHeaders.ERRORTEXT = errmsg;
            response.inputHeaders.ERRORCODE = '';
            
            mq.Status__c = 'failed';
            mq.MSGSTATUS__c = 'F';
            mq.ERRORTEXT__c = errmsg;
            insert mq;
            
            return response;
        }
        
        Integer limitCnt = SFDCStubSECPI.Account_LIMIT_COUNT;
        
        //Get all sent data
        Set<string> sentAccids = new Set<string>();
        List<Data_Send_to_SECPI__c> AllData = Data_Send_to_SECPI__c.getall().values();
        system.debug('size------->'+AllData.size());
        for(Data_Send_to_SECPI__c dataSend : AllData){
            sentAccids.add(dataSend.name);
        }
        
        List<Data_Send_to_SECPI__c> storeIds = new List<Data_Send_to_SECPI__c>();
        //List<SECPI_Data_Send__c> DataSendInsert = new List<SECPI_Data_Send__c>();
        
        List<String> excludedUsers = Label.Users_Excluded_from_Integration.split(',');//Added to exclude data last modified by these users----03/20/2019
        
        List<Account> data = null;
        try {
            List<Account> abc=new list<Account>();
            data = [SELECT Id, Name, Owner.Sales_Team__c, Owner.Name, Owner.UserRole.Name, SAP_Company_Code__c, Type, Key_Account__c, LE_Record_Type__c, 
                        Parent.Id, Parent.Name, Industry_Type__c, Industry, External_Industry__c, BillingCountry, BillingState, BillingPostalCode, 
                        YearStarted, NumberOfEmployees, LastActivityDate, LastModifiedDate, createddate, isdeleted, SystemModStamp
                    FROM Account 
                    WHERE LastModifiedById!=:excludedUsers 
                          and SystemModStamp >= :SearchDateFrom 
                          and SystemModStamp <=:SearchDateTo 
                          and id!=:sentAccids 
                          ORDER BY SystemModStamp ASC LIMIT :limitCnt ALL ROWS];
        } catch(Exception e) {
            // todo : Exception Handling - message queue
            String errmsg = 'DB Query Error - '+e.getmessage() + ' - '+e.getStackTraceString();
            response.inputHeaders.MSGSTATUS = 'F';
            response.inputHeaders.ERRORTEXT = errmsg;
            response.inputHeaders.ERRORCODE = '';
            
            mq.Status__c = 'failed';
            mq.MSGSTATUS__c = 'F';
            mq.ERRORTEXT__c = errmsg;
            insert mq;
            
            return response;
        }   
                       
        response.inputHeaders.MSGSTATUS = 'S';
        response.inputHeaders.ERRORTEXT = '';
        response.inputHeaders.ERRORCODE = '';
                                                    
        response.body.PAGENO = request.body.PAGENO;
        response.body.ROWCNT = limitCnt;
        response.body.COUNT  = data.size();
        
        Map<String, SFDCStubSECPI.AccountWithContact> mapData = new Map<String, SFDCStubSECPI.AccountWithContact>();
        DateTime LastModifiedDate = null;
        string status;
        List<message_queue__c> mqList = new List<message_queue__c>();
        for(Account acc : data) {
            Data_Send_to_SECPI__c storeId = new Data_Send_to_SECPI__c(name=acc.id);
            if(!sentAccids.contains(acc.id)){
                storeIds.add(storeId);
            }
            /*SECPI_Data_Send__c dataSend = new SECPI_Data_Send__c(name=acc.id,LSLM__c=acc.SystemModStamp);
            DataSendInsert.add(dataSend);*/
            try {
                if(acc.createddate>=SearchDateFrom){
                    status = 'New';
                }
                if(acc.createddate<SearchDateFrom && acc.SystemModStamp>=SearchDateFrom){
                    status = 'Modified';
                }
                if(acc.isdeleted==true){
                    status = 'Deleted';
                }
                SFDCStubSECPI.AccountWithContact obj = new SFDCStubSECPI.AccountWithContact(acc,status);
                response.body.Account.add(obj);
                mapData.put(obj.AccountId, obj);
                LastModifiedDate = obj.instance.SystemModStamp;
                system.debug('Record--------->'+response.body.Account);
            } catch(Exception e) {
                String errmsg = 'Fail to create an Account - '+e.getmessage() + ' - '+e.getStackTraceString();
                message_queue__c pemsg = new message_queue__c(MSGUID__c = response.inputHeaders.MSGGUID, MSGSTATUS__c = 'F',  Integration_Flow_Type__c = SFDCStubSECPI.FLOW_081,
                                            IFID__c = response.inputHeaders.IFID, IFDate__c = response.inputHeaders.IFDate, Status__c = 'failed', Object_Name__c = 'AccountWithContact', retry_counter__c = 0);
                pemsg.ERRORTEXT__c = errmsg;
                pemsg.Identification_Text__c = acc.Id;
                //insert pemsg;
                mqList.add(pemsg);
                System.debug(errmsg);
            }
        }
        
        string contactstatus;
        List<Contact> contacts = [SELECT AccountId, Id, Email, Fax, MailingStreet, MailingCity, MailingState, MailingPostalCode, SystemModStamp,
                                    MailingCountry, FirstName, LastName, Phone, Title, isdeleted, createddate, LastModifiedDate 
                                    FROM Contact 
                                    WHERE AccountId in :mapData.keySet() and LastModifiedById!=:excludedUsers ALL ROWS];
        for(Contact contact : contacts) {
            try {
                if(contact.createddate>=SearchDateFrom){
                    contactstatus = 'New';
                }
                if(contact.isdeleted==true){
                    contactstatus = 'Deleted';
                }
                if(contact.createddate<SearchDateFrom && contact.SystemModStamp>=SearchDateFrom){
                    contactstatus = 'Modified';
                }
                SFDCStubSECPI.ContactItem item = new SFDCStubSECPI.ContactItem(contact,contactstatus);
                SFDCStubSECPI.AccountWithContact awc = mapData.get(contact.AccountId);
                awc.Contact.add(item);
            } catch(Exception e) {
                String errmsg = 'Fail to create a Contact - '+e.getmessage() + ' - '+e.getStackTraceString();
                message_queue__c pemsg = new message_queue__c(MSGUID__c = response.inputHeaders.MSGGUID, MSGSTATUS__c = 'F',  Integration_Flow_Type__c = SFDCStubSECPI.FLOW_081,
                                            IFID__c = response.inputHeaders.IFID, IFDate__c = response.inputHeaders.IFDate, Status__c = 'failed', Object_Name__c = 'AccountWithContact', retry_counter__c = 0);
                pemsg.ERRORTEXT__c = errmsg;
                pemsg.Identification_Text__c = contact.Id;
                //insert pemsg;
                mqList.add(pemsg);
                System.debug(errmsg);
            }
        }
        
        mq.last_successful_run__c = currentDateTime;
        mq.Object_Name__c = mq.Object_Name__c + ', ' + request.body.PAGENO + ', ' + response.body.COUNT;
        //insert mq;
        mqList.add(mq);
        
        if(mqList.size()>0){
            insert mqList;
        }
        
        Account LastAccount;
        if(data.size()>0){
            LastAccount  = data[data.size() - 1];
            endpoint.Last_Successful_Run_Last_Modified_Date__c = LastAccount.SystemModStamp;
        }
        if(!Test.isRunningTest() && response.body.COUNT!=0 && response.body.ROWCNT==response.body.COUNT && pageNum<pageLimit) {
            update endpoint;
            insert storeIds;
        //}else if(!Test.isRunningTest() && response.body.COUNT!=0 && ((offSetNo+limitCnt)>=2000 || response.body.ROWCNT!=response.body.COUNT)) {
        }else if(!Test.isRunningTest() && (response.body.ROWCNT!=response.body.COUNT || pageNum==pageLimit)) {
            endpoint.last_successful_run__c = currentDateTime;
            //endpoint.Last_Successful_Run_Last_Modified_Date__c = LastAccount.SystemModStamp;
            update endpoint;
            
            //Delete sent data for final page
            integer count= database.countQuery('select count() from Data_Send_to_SECPI__c limit 10002');
            if(count>10000){
                Batch_AccountDataToSECPI_Delete ba = new Batch_AccountDataToSECPI_Delete();
                Database.executeBatch(ba);
            }else{
                List<Data_Send_to_SECPI__c> deleteData = [select id from Data_Send_to_SECPI__c];
                delete deleteData;
            }
            
        }
        return response;
    }
    
    //Method to convert string to datetime
    public static datetime setDateFromString(string dt){
        string yyyymm = dt.left(6);
        string mm = yyyymm.right(2);
        string yyyy = dt.left(4);
        string yyyymmdd = dt.left(8);
        string dd = yyyymmdd.right(2);
        string finalstr = mm+'/'+dd+'/'+yyyy;
        date d = Date.parse(finalstr);
        system.debug('date----->'+d);
        
        string hhmmss = dt.right(6);
        string hhmm   = hhmmss.left(4);
        integer hh = integer.valueOf(hhmm.left(2));
        integer min = integer.valueOf(hhmm.right(2));
        integer ss = integer.valueOf(hhmmss.right(2));
        
        time t = time.newInstance(hh,min,ss, 0);
        datetime dtime= DateTime.newinstance(d,t);
        
        Timezone tz = Timezone.getTimeZone('America/New_York');
        //datetime now = datetime.now();
        //Integer timeDiff = tz.getOffset(now);
        Integer timeDiff = tz.getOffset(dtime);
        if(timeDiff==-18000000){
            dtime = dtime.addhours(-5);
        }else{
            dtime = dtime.addhours(-4);
        }
        System.debug('dtime----->'+dtime);
        return dtime;
    }
    
}