@isTest
private class AccountWithContactRestResourceTest {

    static testMethod void getAccountWithContactNoData() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        SFDCStub.AccountWithContactRequest reqData = new SFDCStub.AccountWithContactRequest();
        
        reqData.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData.inputHeaders.IFID    = SFDCStub.LAYOUTID_081;
        reqData.body.CONSUMER = 'GMBT';
        reqData.body.PAGENO = 1;
        
        String jsonMsg = JSON.serialize(reqData);
        
        Test.startTest();
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/services/apexrest/AccountWithContact/xxxxx';  //Request URL
        req.httpMethod = 'GET';//HTTP Request Type
        req.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req;
        RestContext.response= res;
        
        SFDCStub.AccountWithContactResponse resData = AccountWithContactRestResource.getAccountWithContact();
        System.debug(resData.inputHeaders.MSGSTATUS);
        System.debug(resData.inputHeaders.ERRORTEXT);
        System.assertEquals('F', resData.inputHeaders.MSGSTATUS);
        
        //AccountWithContactSECPI
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/AccountWithContactSECPI/xxxxx';  //Request URL
        req1.httpMethod = 'GET';//HTTP Request Type
        req1.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req1;
        RestContext.response= res1;
        
        SFDCStubSECPI.AccountWithContactResponse resData1 = AccountWithContactRestResourceSECPI.getAccountWithContactSECPI();
        System.debug(resData1.inputHeaders.MSGSTATUS);
        System.debug(resData1.inputHeaders.ERRORTEXT);
        System.assertEquals('F', resData1.inputHeaders.MSGSTATUS);
        
        Test.stopTest();
    }
    
    static testMethod void getAccountWithContact() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        List<Integration_EndPoints__c> ieps = new List<Integration_EndPoints__c>();
        Integration_EndPoints__c iep1 = new Integration_EndPoints__c();
        iep1.name ='Flow-081';
        iep1.layout_id__c = 'LAY024658';
        iep1.last_successful_run__c = System.now().addHours(-4);
        ieps.add(iep1);
        insert ieps;
        
        Account account = TestDataUtility.createAccount('Marshal Mathers');
        account.SAP_Company_Code__c='99999888';
        insert account;
        
        Contact contact = new Contact(Email = 'TestEmail@test.com', LastName = 'TestLastName');
        contact.Accountid = account.id;
        insert contact;
        
        SFDCStub.AccountWithContactRequest reqData = new SFDCStub.AccountWithContactRequest();
        
        reqData.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData.inputHeaders.IFID    = SFDCStub.LAYOUTID_081;
        reqData.body.CONSUMER = 'GMBT';
        reqData.body.PAGENO = 1;
        
        String jsonMsg = JSON.serialize(reqData);
        
        Test.startTest();
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/services/apexrest/AccountWithContact/'+account.Id;  //Request URL
        req.httpMethod = 'GET';//HTTP Request Type
        req.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req;
        RestContext.response= res;
        
        SFDCStub.AccountWithContactResponse resData = AccountWithContactRestResource.getAccountWithContact();
        System.debug(resData.inputHeaders.MSGSTATUS);
        System.debug(resData.inputHeaders.ERRORTEXT);
        System.assertEquals('S', resData.inputHeaders.MSGSTATUS);
        
        //AccountWithContactSECPI
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/AccountWithContactSECPI/'+account.Id;  //Request URL
        req1.httpMethod = 'GET';//HTTP Request Type
        req1.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req1;
        RestContext.response= res1;
        
        SFDCStubSECPI.AccountWithContactResponse resData1 = AccountWithContactRestResourceSECPI.getAccountWithContactSECPI();
        System.debug(resData1.inputHeaders.MSGSTATUS);
        System.debug(resData1.inputHeaders.ERRORTEXT);
        System.assertEquals('S', resData1.inputHeaders.MSGSTATUS);
        
        Test.stopTest();
    }
    
    static testMethod void getAccountWithContactList() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        List<Integration_EndPoints__c> ieps = new List<Integration_EndPoints__c>();
        Integration_EndPoints__c iep1 = new Integration_EndPoints__c();
        iep1.name ='Flow-081';
        iep1.layout_id__c = 'LAY024658';
        iep1.last_successful_run__c = System.now().addHours(-4);
        ieps.add(iep1);
        
        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='SECPI Flow 081';
        iep2.layout_id__c = 'LAY025591';
        iep2.last_successful_run__c = System.now().addHours(-4);
        ieps.add(iep2);
        
        insert ieps;
        
        Account account = TestDataUtility.createAccount('Marshal Mathers');
        account.SAP_Company_Code__c='99999888';
        insert account;
        Account acc = [select id,systemmodstamp,lastmodifieddate from Account where id=:account.id];
        system.debug(acc.systemmodstamp);
        
        Contact contact = new Contact(Email = 'TestEmail@test.com', LastName = 'TestLastName');
        contact.Accountid = account.id;
        insert contact;
        
        SFDCStub.AccountWithContactRequest reqData = new SFDCStub.AccountWithContactRequest();
        
        reqData.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData.inputHeaders.IFID    = SFDCStub.LAYOUTID_081;
        reqData.body.CONSUMER = 'GMBT';
        reqData.body.PAGENO = 1;
        
        string SearchDtmTo = string.valueOf(system.now().addHours(4));
        SearchDtmTo = SearchDtmTo.replaceAll( '\\s+', '');
        SearchDtmTo = SearchDtmTo.remove(':');
        SearchDtmTo = SearchDtmTo.remove('-');
        
        string SearchDtmFrom = string.valueOf(system.now().addHours(-10));
        SearchDtmFrom = SearchDtmFrom.replaceAll( '\\s+', '');
        SearchDtmFrom = SearchDtmFrom.remove(':');
        SearchDtmFrom = SearchDtmFrom.remove('-');
        
        SFDCStubSECPI.AccountWithContactRequest reqData2 = new SFDCStubSECPI.AccountWithContactRequest();
        reqData2.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData2.inputHeaders.IFID    = SFDCStubSECPI.LAYOUTID_081;
        reqData2.body.CONSUMER = 'SECPIMSTR';
        reqData2.body.SEARCH_DTTM_TO = SearchDtmTo;
        reqData2.body.SEARCH_DTTM_FROM = SearchDtmFrom;
        reqData2.body.PAGENO = 1;
        
        String jsonMsg  = JSON.serialize(reqData);
        String jsonMsg2 = JSON.serialize(reqData2);
        
        Test.startTest();
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/services/apexrest/AccountWithContact';  //Request URL
        req.httpMethod = 'POST';//HTTP Request Type
        req.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req;
        RestContext.response= res;
        
        SFDCStub.AccountWithContactResponse resData = AccountWithContactRestResource.getAccountWithContactList();
        System.debug(resData.inputHeaders.MSGSTATUS);
        System.debug(resData.inputHeaders.ERRORTEXT);
        System.assertEquals('S', resData.inputHeaders.MSGSTATUS);
        
        //AccountWithContactSECPI
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/AccountWithContactSECPI';  //Request URL
        req1.httpMethod = 'POST';//HTTP Request Type
        req1.requestBody = Blob.valueof(jsonMsg2);
        RestContext.request = req1;
        RestContext.response= res1;
        
        SFDCStubSECPI.AccountWithContactResponse resData1 = AccountWithContactRestResourceSECPI.getAccountWithContactListSECPI();
        System.debug(resData1.inputHeaders.MSGSTATUS);
        System.debug(resData1.inputHeaders.ERRORTEXT);
        System.assertEquals('S', resData1.inputHeaders.MSGSTATUS);
        
        Test.stopTest();
    }
    
    static testMethod void getAccountWithContactListWrongRequest() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Account account = TestDataUtility.createAccount('Marshal Mathers');
        account.SAP_Company_Code__c='99999888';
        insert account;
        
        Contact contact = new Contact(Email = 'TestEmail@test.com', LastName = 'TestLastName');
        contact.Account = account;
        insert contact;
        
        Test.startTest();
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/services/apexrest/AccountWithContact';  //Request URL
        req.httpMethod = 'POST';//HTTP Request Type
        req.requestBody = Blob.valueof('Hello World');
        RestContext.request = req;
        RestContext.response= res;
        
        SFDCStub.AccountWithContactResponse resData = AccountWithContactRestResource.getAccountWithContactList();
        System.debug(resData.inputHeaders.MSGSTATUS);
        System.debug(resData.inputHeaders.ERRORTEXT);
        System.assertEquals('F', resData.inputHeaders.MSGSTATUS);
        
        //AccountWithContactSECPI
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/AccountWithContactSECPI';  //Request URL
        req1.httpMethod = 'POST';//HTTP Request Type
        req1.requestBody = Blob.valueof('Hello World');
        RestContext.request = req1;
        RestContext.response= res1;
        
        SFDCStubSECPI.AccountWithContactResponse resData1 = AccountWithContactRestResourceSECPI.getAccountWithContactListSECPI();
        System.debug(resData1.inputHeaders.MSGSTATUS);
        System.debug(resData1.inputHeaders.ERRORTEXT);
        System.assertEquals('F', resData1.inputHeaders.MSGSTATUS);
        
        Test.stopTest();
    }
    
    static testMethod void getAccountWithContactListNoData() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        List<Integration_EndPoints__c> ieps = new List<Integration_EndPoints__c>();
        Integration_EndPoints__c iep1 = new Integration_EndPoints__c();
        iep1.name ='Flow-081';
        iep1.layout_id__c = 'LAY024658';
        iep1.last_successful_run__c = System.now().addHours(-4);
        ieps.add(iep1);
        
        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='SECPI Flow 081';
        iep2.layout_id__c = 'LAY025591';
        iep2.last_successful_run__c = System.now().addHours(-4);
        ieps.add(iep2);
        
        insert ieps;
        
        Account account = TestDataUtility.createAccount('Marshal Mathers');
        account.SAP_Company_Code__c='99999888';
        insert account;
        
        Contact contact = new Contact(Email = 'TestEmail@test.com', LastName = 'TestLastName');
        contact.Account = account;
        insert contact;
        
        SFDCStub.AccountWithContactRequest reqData = new SFDCStub.AccountWithContactRequest();
        
        reqData.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData.inputHeaders.IFID    = SFDCStub.LAYOUTID_081;
        reqData.body.CONSUMER = 'GMBT';
        reqData.body.PAGENO = -1;
        
        SFDCStubSECPI.AccountWithContactRequest reqData2 = new SFDCStubSECPI.AccountWithContactRequest();
        reqData2.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData2.inputHeaders.IFID    = SFDCStubSECPI.LAYOUTID_081;
        reqData2.body.CONSUMER = 'SECPIMSTR';
        reqData2.body.SEARCH_DTTM = '20180216000000';
        reqData2.body.PAGENO = -1;
        
        String jsonMsg  = JSON.serialize(reqData);
        String jsonMsg2 = JSON.serialize(reqData2);

        Test.startTest();
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/services/apexrest/AccountWithContact';  //Request URL
        req.httpMethod = 'POST';//HTTP Request Type
        req.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req;
        RestContext.response= res;
        
        SFDCStub.AccountWithContactResponse resData = AccountWithContactRestResource.getAccountWithContactList();
        System.debug(resData.inputHeaders.MSGSTATUS);
        System.debug(resData.inputHeaders.ERRORTEXT);
        System.assertEquals('F', resData.inputHeaders.MSGSTATUS);
        
        //AccountWithContactSECPI
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/AccountWithContactSECPI';  //Request URL
        req1.httpMethod = 'POST';//HTTP Request Type
        req1.requestBody = Blob.valueof(jsonMsg2);
        RestContext.request = req1;
        RestContext.response= res1;
        
        SFDCStubSECPI.AccountWithContactResponse resData1 = AccountWithContactRestResourceSECPI.getAccountWithContactListSECPI();
        System.assertEquals('S', resData1.inputHeaders.MSGSTATUS);
        
        Test.stopTest();
    }
    
    static testMethod void getAccountWithContactListWrongDateRequest() {
        List<Integration_EndPoints__c> ieps = new List<Integration_EndPoints__c>();
        
        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='SECPI Flow 081';
        iep2.layout_id__c = 'LAY025591';
        iep2.last_successful_run__c = System.now().addHours(-4);
        iep2.Last_Successful_Run_Last_Modified_Date__c = System.now().addHours(-2);
        ieps.add(iep2);
        
        insert ieps;
        
        SFDCStubSECPI.OpportunityActivityRequest reqData = new SFDCStubSECPI.OpportunityActivityRequest();
        
        reqData.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData.inputHeaders.IFID    = SFDCStubSECPI.LAYOUTID_081;
        reqData.body.CONSUMER = 'SECPIMSTR';
        reqData.body.SEARCH_DTTM = '2018410';
        reqData.body.PAGENO = 1;
        
        String jsonMsg = JSON.serialize(reqData);
        
        Test.startTest();
        //AccountWithContactSECPI
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/AccountWithContactSECPI';  //Request URL
        req1.httpMethod = 'POST';//HTTP Request Type
        req1.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req1;
        RestContext.response= res1;
        
        SFDCStubSECPI.AccountWithContactResponse resData1 = AccountWithContactRestResourceSECPI.getAccountWithContactListSECPI();
        System.debug(resData1.inputHeaders.MSGSTATUS);
        System.debug(resData1.inputHeaders.ERRORTEXT);
        System.assertEquals('S', resData1.inputHeaders.MSGSTATUS);
        
        Test.stopTest();
    }
    
    static testMethod void AccountWithContactListPageLimitTest() {
        List<Integration_EndPoints__c> ieps = new List<Integration_EndPoints__c>();
        
        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='SECPI Flow 081';
        iep2.layout_id__c = 'LAY025591';
        iep2.last_successful_run__c = System.now().addHours(-4);
        iep2.Last_Successful_Run_Last_Modified_Date__c = System.now().addHours(-2);
        ieps.add(iep2);
        
        insert ieps;
        
        SFDCStubSECPI.OpportunityActivityRequest reqData = new SFDCStubSECPI.OpportunityActivityRequest();
        
        reqData.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData.inputHeaders.IFID    = SFDCStubSECPI.LAYOUTID_081;
        reqData.body.CONSUMER = 'SECPIMSTR';
        reqData.body.SEARCH_DTTM = '2018410';
        reqData.body.PAGENO = 501;
        
        String jsonMsg = JSON.serialize(reqData);
        
        Test.startTest();
        //AccountWithContactSECPI
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/AccountWithContactSECPI';  //Request URL
        req1.httpMethod = 'POST';//HTTP Request Type
        req1.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req1;
        RestContext.response= res1;
        
        SFDCStubSECPI.AccountWithContactResponse resData1 = AccountWithContactRestResourceSECPI.getAccountWithContactListSECPI();
        System.assertEquals('F', resData1.inputHeaders.MSGSTATUS);
        
        Test.stopTest();
    }
    
    static testMethod void AccountWithContactListToDateMissing() {
        SFDCStubSECPI.AccountWithContactRequest reqData2 = new SFDCStubSECPI.AccountWithContactRequest();
        reqData2.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData2.inputHeaders.IFID    = SFDCStubSECPI.LAYOUTID_081;
        reqData2.body.CONSUMER = 'SECPIMSTR';
        reqData2.body.SEARCH_DTTM_TO = '';
        reqData2.body.SEARCH_DTTM_FROM = '20180522000000';
        reqData2.body.PAGENO = 1;
        
        String jsonMsg2 = JSON.serialize(reqData2);
        
        Test.startTest();
        //AccountWithContactSECPI
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/AccountWithContactSECPI';  //Request URL
        req1.httpMethod = 'POST';//HTTP Request Type
        req1.requestBody = Blob.valueof(jsonMsg2);
        RestContext.request = req1;
        RestContext.response= res1;
        
        SFDCStubSECPI.AccountWithContactResponse resData1 = AccountWithContactRestResourceSECPI.getAccountWithContactListSECPI();
        System.debug(resData1.inputHeaders.MSGSTATUS);
        System.debug(resData1.inputHeaders.ERRORTEXT);
        System.assertEquals('F', resData1.inputHeaders.MSGSTATUS);
        
        Test.stopTest();
    }
    
    static testMethod void AccountWithContactListDateFormatCheck() {
        SFDCStubSECPI.AccountWithContactRequest reqData2 = new SFDCStubSECPI.AccountWithContactRequest();
        reqData2.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData2.inputHeaders.IFID    = SFDCStubSECPI.LAYOUTID_081;
        reqData2.body.CONSUMER = 'SECPIMSTR';
        reqData2.body.SEARCH_DTTM_TO = '2018052300000';
        reqData2.body.SEARCH_DTTM_FROM = '20180522000000';
        reqData2.body.PAGENO = 1;
        
        String jsonMsg2 = JSON.serialize(reqData2);
        
        Test.startTest();
        //AccountWithContactSECPI
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/AccountWithContactSECPI';  //Request URL
        req1.httpMethod = 'POST';//HTTP Request Type
        req1.requestBody = Blob.valueof(jsonMsg2);
        RestContext.request = req1;
        RestContext.response= res1;
        
        SFDCStubSECPI.AccountWithContactResponse resData1 = AccountWithContactRestResourceSECPI.getAccountWithContactListSECPI();
        System.debug(resData1.inputHeaders.MSGSTATUS);
        System.debug(resData1.inputHeaders.ERRORTEXT);
        System.assertEquals('F', resData1.inputHeaders.MSGSTATUS);
        
        Test.stopTest();
    }
    static testMethod void AccountWithContactListPageNumMissing() {
        SFDCStubSECPI.AccountWithContactRequest reqData2 = new SFDCStubSECPI.AccountWithContactRequest();
        reqData2.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData2.inputHeaders.IFID    = SFDCStubSECPI.LAYOUTID_081;
        reqData2.body.CONSUMER = 'SECPIMSTR';
        reqData2.body.SEARCH_DTTM_TO = '20180523000000';
        reqData2.body.SEARCH_DTTM_FROM = '20180522000000';
        //reqData2.body.PAGENO = 1;
        
        String jsonMsg2 = JSON.serialize(reqData2);
        
        Test.startTest();
        //AccountWithContactSECPI
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/AccountWithContactSECPI';  //Request URL
        req1.httpMethod = 'POST';//HTTP Request Type
        req1.requestBody = Blob.valueof(jsonMsg2);
        RestContext.request = req1;
        RestContext.response= res1;
        
        SFDCStubSECPI.AccountWithContactResponse resData1 = AccountWithContactRestResourceSECPI.getAccountWithContactListSECPI();
        System.debug(resData1.inputHeaders.MSGSTATUS);
        System.debug(resData1.inputHeaders.ERRORTEXT);
        System.assertEquals('F', resData1.inputHeaders.MSGSTATUS);
        
        Test.stopTest();
    }

}