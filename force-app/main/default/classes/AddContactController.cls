public class AddContactController {

    @auraEnabled
    public static contact getContact()
    {
        Contact c= new Contact();
        return c;
    }
    
    @auraEnabled
    public static contact addContact(contact c,String aId)
    {
       c.AccountId=aId;
        upsert c;
        return c;
        
    }
}