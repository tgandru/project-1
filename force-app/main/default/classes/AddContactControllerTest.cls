@isTest
public class AddContactControllerTest {

    static testMethod void myTest() {
     
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        
        Account testAccount = new Account();
        testAccount.Name='Test Account' ;
        insert testAccount;
        
		/*Contact c= new Contact();
        c.FirstName='sruthi';
        c.LastName='reddy';
        c.Email='abc@gmail.com';
        c.Phone='123-456-7890';
        c.AccountId=testAccount.id;
        insert c;*/
                
        Test.startTest();
        contact c =AddContactController.getContact();
        c.FirstName='sruthi';
        c.LastName='reddy';
        c.Email='abc@gmail.com';
        c.Phone='123-456-7890';
        AddContactController.addContact(c,testAccount.Id);
        Test.stopTest();
        
        Integer n =[select count() from contact where lastName='reddy'];
        system.assertEquals(1,n);
        
     }
}