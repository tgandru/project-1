public class AddDistributorToQuoteExt {
    //reference the api name of the flow record
    Public Flow.Interview.Add_Dsitributor_and_Reseller_to_Quote myAutoFlow { get; set; }
    public boolean displayFlow {get;set;}
    
    Public AddDistributorToQuoteExt(ApexPages.StandardController controller) {
        Quote q = (Quote) controller.getRecord();
        q = [select id,status from Quote where id=:q.id];
        if(Label.StatusPendingApproval != null && Label.StatusPendingApproval.containsIgnoreCase(q.status) ){
            displayFlow = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,Label.FlowErrorForQuote));
        }
        else
            displayFlow = true;
            
    }
    public String getmyID() {
        if (myAutoFlow==null)
            return '';
        else 
            //Put flow variable that represents the id of newly created record
            return myAutoFlow.vQuoteID;
    }

    public PageReference getNextPage(){
        PageReference p = new PageReference('/' + getmyID() );
        p.setRedirect(true);
        return p;
    }

}