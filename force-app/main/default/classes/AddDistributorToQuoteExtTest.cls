/*
 * Test class for AddDistributorToQuoteExt
 * @Bala Rajasekharan - Updated to add test data
 */
@isTest
private class AddDistributorToQuoteExtTest {

    
    @isTest static void test_method_one() {
        // Implement test code
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        
        Account acc = TestDataUtility.createAccount('Test Acumen');
        insert acc;

        PriceBook2 pb = TestDataUtility.createPriceBook('Test pbe1');       
        insert pb;
            
        Opportunity opp = TestDataUtility.createOppty('New', acc, pb, 'Managed', 'IT', 'product group','Identified');
        insert opp; 
        
         Quote q = TestDataUtility.createQuote('Test Quote', opp,pb);
        insert q;

        //AddDistributorToQuoteExt adExt = new AddDistributorToQuoteExt(new ApexPages.StandardController(new Quote()));
        AddDistributorToQuoteExt adExt = new AddDistributorToQuoteExt(new ApexPages.StandardController(q));
        adExt.getmyID();
        adExt.getNextPage();
    }
    
}