global class AggregateResultIterableHAactualsBatch implements Iterable<AggregateResult> {
    private String query;
    private Date fromDate;
    global AggregateResultIterableHAactualsBatch(String soql, date frmDate){
        fromDate = frmDate;
        query = soql;
        system.debug('query---->'+query);
    }
    
    global Iterator<AggregateResult> Iterator(){
        return new AggregateResultIteratorHAactualsBatch(query,fromDate);
    }
}