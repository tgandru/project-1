global class AssaignedCampaignUpdateOnLead implements Database.Batchable<sObject>{


   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator([SELECT CampaignId,Id,LeadId,Name FROM CampaignMember ]);
   }

   global void execute(Database.BatchableContext BC, List<CampaignMember> scope){
         set<string> leadids = new set<string>();
         set<string> CampaignIds = new set<String>();
          for(CampaignMember cm:Scope){
		     string l = cm.LeadId;
			 string c = cm.CampaignId;
		    if(cm.LeadId !=null){
			   leadids.add(l);
			}
			if(cm.CampaignId!=null){
                 CampaignIds.add(c);			
			}
		  }	
          if(leadids.size()>0){
		     map<id,lead> ld=new map<id,lead>([Select id,name,Assigned_Campaign__c from Lead where id in :leadids ]);
			 map<id,Campaign> camp=new map<id,Campaign>([select id,name from Campaign where id in:CampaignIds]);
			 for(CampaignMember cm:Scope){
			     string l = cm.LeadId;
			     string c = cm.CampaignId;
				 if(ld.get(l)!=null){
				    ld.get(l).Assigned_Campaign__c=camp.get(cm.CampaignId).Name;
				 
				 }
			 
			 }
			 if(ld.size()>0){
			  update ld.values();
			 }
		  }		  
    }

   global void finish(Database.BatchableContext BC){
   }
   
}