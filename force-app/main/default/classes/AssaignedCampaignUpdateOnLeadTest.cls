@isTest
private class AssaignedCampaignUpdateOnLeadTest {

	private static testMethod void test() {
	    string   CampaignTypeId = [Select Id From RecordType Where SobjectType = 'Campaign' and Name = 'MARCOM'  limit 1].Id;
	    string   LeadTypeId = [Select Id From RecordType Where SobjectType = 'Lead' and Name = 'End User'  limit 1].Id;
        Campaign cmp= new Campaign(Name='Test Campaign for test class ',	IsActive=true,type='Email',StartDate=system.Today(),recordtypeid=CampaignTypeId);
        insert cmp;
        lead le1=new lead(lastname='test lead 123',Company='My Lead Company',Email='mylead@sds.com',	MQL_Score__c='T1',Status='New',LeadSource='Others',recordtypeid=LeadTypeId);
	    insert le1;
	    CampaignMember newMember4=new CampaignMember(LeadId = le1.id,  status='Sent',	Product_Interest__c='Desktop Monitor',	MQLScore__c='T1', campaignid = cmp.id);
	    insert newMember4;
	    Test.startTest();
	      database.executeBatch(new AssaignedCampaignUpdateOnLead());
	    Test.stopTest();

	}

}