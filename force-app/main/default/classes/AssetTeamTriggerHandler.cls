public class AssetTeamTriggerHandler {
	public AssetTeamTriggerHandler() {
		
	}

	public Static Void AfterInsert(List<Asset_Team__c> triggerNew) {
		List<String> asset_ids = new List<String>();
		List<String> user_ids = new List<String>();
		Map<String,AssetShare> SharesExistingMap = new Map<String, AssetShare>();
		List<AssetShare> sharesToAdd = new List<AssetShare>();

		for (Asset_Team__c AssetT:triggerNew){
			asset_ids.add(AssetT.Asset__c);
			user_ids.add (AssetT.User__c);
		}

		List<AssetShare> SharesExisting = [select id,AssetId,UserOrGroupId from AssetShare 
			where AssetId IN: asset_ids 
                and UserOrGroupId IN:user_ids];

        for (AssetShare aShare:SharesExisting){
        	SharesExistingMap.put(aShare.AssetId+'--'+aShare.UserOrGroupId,aShare);
        }

        for (Asset_Team__c AssetT:triggerNew){
			String key_str= AssetT.Asset__c+'--'+AssetT.User__c;
			if (!SharesExistingMap.containsKey(key_str)){
				AssetShare newShare = new AssetShare();
				newShare.assetId = AssetT.Asset__c;
				newShare.UserOrGroupId = AssetT.User__c;
				newShare.AssetAccessLevel = 'edit'; 
				newShare.RowCause = 'Manual';
				sharesToAdd.add(newShare);
			}
		}
		if (sharesToAdd.size()>0){
			for (AssetShare a : sharesToAdd){
				system.debug(a.assetid+' -- '+a.UserOrGroupId);
			}
			insert sharesToAdd;
		}

	}

	public Static Void AfterDelete(List<Asset_Team__c> triggerOld) {
		List<String> asset_ids = new List<String>();
		List<String> user_ids = new List<String>();
		Map<String,Integer> ExistingCntMap = new Map<String, Integer>();

		Map<String,Integer> TriggerCntMap = new Map<String, Integer>();
		Integer cnt = 0;

		List<AssetShare> sharesToDelete = new List<AssetShare>();

		List<String> TriggerToBeRemoved = new List<String>();

		for (Asset_Team__c AssetT:triggerOld){
			asset_ids.add(AssetT.Asset__c);
			user_ids.add (AssetT.User__c);
			if (TriggerCntMap.containsKey(AssetT.Asset__c+'--'+AssetT.User__c)){
				cnt = TriggerCntMap.get(AssetT.Asset__c+'--'+AssetT.User__c)+1;
				TriggerCntMap.put(AssetT.Asset__c+'--'+AssetT.User__c,cnt);
			}else{
				TriggerCntMap.put(AssetT.Asset__c+'--'+AssetT.User__c,1);
			}

		}

	
		AggregateResult[] groupedResults = [select asset__c,user__c,count(id) cnt from asset_team__c 
							where asset__c in:asset_ids and user__c in:user_ids group by asset__c,user__c];

		for (AggregateResult ar : groupedResults)  {

			object assetObj = ar.get('asset__c');
			object userObj = ar.get('user__c');
			object cntObj = ar.get('cnt');
			ExistingCntMap.put((String)assetObj+'--'+(String)userObj,(Integer)cntObj);
		}

		system.debug('here');
		for(string keyStr: ExistingCntMap.keyset()){

			system.debug(keyStr+'::'+ExistingCntMap.get(keyStr));
		}

		for (Asset_Team__c AssetT:triggerOld){
			String key_str = AssetT.Asset__c+'--'+AssetT.User__c;
			if (TriggerCntMap.containsKey(key_str)){
				cnt = TriggerCntMap.get(key_str);
				if (ExistingCntMap.containsKey(key_str)){
					if (ExistingCntMap.get(key_str)<cnt){
						TriggerToBeRemoved.add(key_str);
					}
				}else{
					TriggerToBeRemoved.add(key_str);
				}
			}
		}

		Map<String,AssetShare> SharesToBeRemovedMap = new Map<String, AssetShare>();
		
		List<AssetShare> SharesToBeRemoved = [select id,AssetId,UserOrGroupId from AssetShare 
			where AssetId IN: asset_ids 
                and UserOrGroupId IN:user_ids];

        for (AssetShare aShare:SharesToBeRemoved){
        	SharesToBeRemovedMap.put(aShare.AssetId+'--'+aShare.UserOrGroupId,aShare);
        }

        if (TriggerToBeRemoved.size()>0){
        	for (String keyStr:TriggerToBeRemoved){
        		if (SharesToBeRemovedMap.containsKey(keyStr)){
        			sharesToDelete.add(SharesToBeRemovedMap.get(keyStr));
        		}
        	}

        	if (sharesToDelete.size()>0){
        		delete sharesToDelete;
        	}
        }
				
	}
}