public with sharing class AssetTriggerHandler {
    public static String IT_Installation_Recordtype = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('IT Installation').getRecordTypeId();
    public static boolean PartnerAlreadyExistingTestVar = false; //This variable is used to bypass all error causing if conditions for the test class: AssetTriggerHandler_Test
    /*private class contractWrapper{
            Date startDate;
            Date endDate;
            public contractWrapper(Date sDate,Date eDate){
                startDate = sDate;
                endDate = eDate;
            }
    }*/
    /*
    public Static Void setDefaultProjectDatese(List<Asset> triggerNew) {
        
        Map<Id,contractWrapper> contractMap = new Map<Id,contractWrapper>();
        List<ID> contractIds = new List<ID>();
        For (Asset a:triggerNew){
            if (a.contract__c != null && (a.project_start_date__c ==null || a.project_end_date__c ==null)){
                contractIds.add(a.contract__c);     
            }
        }
        if (contractIds.size()>0){
            for (Contract c:[select id, startdate,enddate from contract where id in:contractIds]){
                contractWrapper cr = new contractWrapper(c.startdate,c.enddate);
                contractMap.put(c.id,cr);
            }
        }

        For (Asset a:triggerNew){
            if (a.contract__c != null && a.project_start_date__c ==null){
                a.project_start_date__c = (contractMap.get(a.contract__c)).startdate;
            }
            if (a.contract__c != null && a.project_end_date__c ==null){
                a.project_end_date__c = (contractMap.get(a.contract__c)).enddate;
            }
        }
    }*/
    
    public Static Void BeforeDelete(List<Asset> triggerOld) {

        Map<ID,Integer> entCaseCnts = new Map<ID, Integer>();
        Map<ID,Entitlement> AssetEntwithCaseMap = new Map<ID, Entitlement>();

        Boolean deleteEnt = true;

        Entitlement[] ents = [select id,name,AssetId from Entitlement where AssetId in :triggerOld ];

        AggregateResult[] groupedResults =
            [select entitlementid, count(id) cnt 
                from case where entitlementid in: ents
                group by entitlementid];
        for (AggregateResult ar : groupedResults)  {
            entCaseCnts.put((ID)ar.get('entitlementid'), (Integer)ar.get('cnt'));
        }

        for (Entitlement ent:ents){
            if (entCaseCnts.containsKey(ent.id)){
                AssetEntwithCaseMap.put(ent.Assetid,ent);
            }
        }

        for (Asset ast : triggerOld) {
            if (AssetEntwithCaseMap.containsKey(ast.id)){
                String entName = AssetEntwithCaseMap.get(ast.id).name;
                ast.addError('Cannot delete the Asset.The asset has an Entitlement of \''+entName +
                    '\' that is assigned to case(s). Please remove the entitlement from case(s) before deleting the asset.');
                deleteEnt = false;
            }
         }

        if (deleteEnt){ 
            try {
                delete ents;
            } catch (DmlException e) {

                for (Asset ast : triggerOld) {
                    //ast.addError(e.getMessage());
                    ast.addError(e.getDMLMessage(0));
                }

            }
        }   
    }
    
    /*
     * Added By: Joe Jung
     * Purpose: To block deleting an IT Installation Asset if it has at least one Installation Site record.
	*/
    public static void blockDelete(Map<ID,Asset> triggerOldMap) {
        
        List<Installation_Site__C> installationSiteList = [SELECT Id, InstallationSiteAsset__c, InstallationSiteAsset__r.recordTypeId FROM Installation_Site__C WHERE InstallationSiteAsset__c in: triggerOldMap.keyset()];
        
        Set<Id> assetsToBlockDeleteSet = new Set<Id>();
        for(Installation_Site__C is : installationSiteList) {
            if(is.InstallationSiteAsset__r.recordTypeId== IT_Installation_Recordtype && triggerOldMap.containsKey(is.InstallationSiteAsset__c)) {
                assetsToBlockDeleteSet.add(is.InstallationSiteAsset__c);
            }
        }
        
        for(Asset asset : triggerOldMap.values()) {
            if(asset.RecordTypeId == IT_Installation_Recordtype && assetsToBlockDeleteSet.contains(asset.Id))
            	asset.adderror('The IT Installation Asset cannot be deleted since it has at least one installation site.');
        }
    }
    
    /*
     * Added By: Joe Jung
     * Purpose: To check if there is already a partner on the Opportunity and if there is, add that partner to the asset.
	 */
    public static void CheckInStallationPartner(List<Asset> triggernew){        
        Set<ID> Oppids=new Set<Id>();
        For(Asset a: triggernew){
            OppIds.add(a.Opportunity__c);
        }
        Map<Id,List<Partner__C>> partnermap=new Map<Id,List<Partner__C>>();
        if(OppIds.Size()>0){
            
            For(Partner__c p:[Select id,Name,Partner__C,Opportunity__c,Role__c from Partner__C where Opportunity__c in :OppIds and Role__c !='Customer' and Role__c !='Distributor']){
                	System.debug('Partner Name: ' + p.Name);
                	System.debug('Partner Role: ' + p.Role__c);
                	if(partnermap.containsKey(p.Opportunity__c)){
                        List<Partner__C> existed=partnermap.get(p.Opportunity__c);
                        existed.add(p);
                        partnermap.put(p.Opportunity__c,existed);
                    }else{
                         partnermap.put(p.Opportunity__c,New List<Partner__C>{p});
                    }                
            }
            
            
            for(Asset a:triggernew){
                if(a.Opportunity__c !=null || a.Opportunity__c !=''){
                    If(partnermap.containsKey(a.Opportunity__c)){
                        System.debug('Partner Present');
                    }else{
                        a.addError('Please Add Partner into Opportunity First.');
                    }
                }
            }
            
        }
    }
}