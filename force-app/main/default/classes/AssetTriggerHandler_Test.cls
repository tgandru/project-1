@isTest
public class AssetTriggerHandler_Test {
	
    @isTest(seeAllData=true)
    static void testCheckInStallationPartner() {
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }
        Account testAccount1 = new Account (
          	Name = 'testAccount1',
            RecordTypeId = rtMap.get('Account' + 'End_User'),
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        
        );
        insert testAccount1;
        
        Account installationPartnerAccount = new Account (
          	Name = 'installationPartnerAccount',
            RecordTypeId = rtMap.get('Account' + 'Indirect'),
            Type = 'Installation Partner',
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        );
        insert installationPartnerAccount;
        
        Account installationPartnerAccount2 = new Account (
          	Name = 'installationPartnerAccount2',
            RecordTypeId = rtMap.get('Account' + 'Indirect'),
            Type = 'Installation Partner',
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        );
        insert installationPartnerAccount2;
        
        Account distributorAccount = new Account (
          	Name = 'DistributorAccount',
            RecordTypeId = rtMap.get('Account' + 'Indirect'),
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        );
        insert distributorAccount;
        
        PriceBook2 pb = TestDataUtility.createPriceBook('IT Distributor'); 
        pb.Division__c='IT';
        pb.SAP_Price_Account_Code__c='1234500';
        pb.SAP_Price_Group_Code__c = '9999';
        insert pb;

        Opportunity testOpp1 = new Opportunity (
          Name = 'testOpp1',
          RecordTypeId = rtMap.get('Opportunity' + 'IT'),
          //Project_Type__c = 'SFNC',
          Project_Name__c = 'TestOpp1',
          AccountId = testAccount1.Id,
          StageName = 'Identified',
          CloseDate = Date.today(),
          Roll_Out_Start__c = Date.today(),
          Rollout_Duration__c = 5,
          Number_of_Living_Units__c = 10000,
          Division__c = 'IT',
          ProductGroupTest__c = 'SMART_SIGNAGE',
          Pricebook2Id = pb.Id
        );
        insert testOpp1;
        
        Test.StartTest();
        Opportunity oppForAsset = [SELECT ID, StageName, Primary_Distributor__c,Primary_Reseller__c, Roll_Out_Start__c, Rollout_Duration__c From Opportunity WHERE ID =: testOpp1.Id];
       	
        Partner__c InstallationPartner = new Partner__c();
        InstallationPartner.Opportunity__c = oppForAsset.Id;
        InstallationPartner.Partner__C = installationPartnerAccount2.Id;
        InstallationPartner.Is_Primary__c = false;
        InstallationPartner.Role__c = 'Installation Partner';
        insert InstallationPartner;
        
        Partner__c PrimaryReseller = new Partner__c();
        PrimaryReseller.Opportunity__c = oppForAsset.Id;
        PrimaryReseller.Partner__C = installationPartnerAccount.Id;
        PrimaryReseller.Is_Primary__c = true;
        PrimaryReseller.Role__c = 'Reseller';
        insert PrimaryReseller;
        
        oppForAsset.Primary_Reseller__C = PrimaryReseller.Partner__C;
        
        Partner__c PrimaryDistributor = new Partner__c();
        PrimaryDistributor.Opportunity__c = oppForAsset.Id;
        PrimaryDistributor.Partner__C = installationPartnerAccount.Id;
        PrimaryDistributor.Is_Primary__c = true;
        PrimaryDistributor.Role__c = 'Distributor';
        insert PrimaryDistributor;

        oppForAsset.Primary_Distributor__c = PrimaryDistributor.Partner__C;

        List<String> installationskus=new List<String>();
        installationskus=Label.IT_Installation_SKU_Code.Split(';');
        System.debug('Installationskus ====> '+installationskus);
        
        Product2 standaloneProd = [SELECT Id, Description, Unit_Cost__c, Product_Group__c,Product_Type__c FROM Product2 WHERE SAP_Material_ID__C in: installationskus AND NAme ='DC32E'];
        
        //create the pricebook entries for the custom pricebook
        PriceBookEntry standaloneProdPBE=TestDataUtility.createPriceBookEntry(standaloneProd.Id, pb.Id);
        insert standaloneProdPBE;

        standaloneProdPBE = [SELECT ID,Name,UnitPrice FROM PriceBookEntry WHERE ID =: standaloneProdPBE.Id];
        System.debug('PriceBookEntry ===> ' + standaloneProdPBE.Name);
        
        //Creating opportunity Line Item
        OpportunityLineItem standaloneProdOLI=TestDataUtility.createOpptyLineItem(standaloneProd, standaloneProdPBE, oppForAsset);
        standaloneProdOLI.Quantity = 10;
        standaloneProdOLI.TotalPrice = standaloneProdOLI.Quantity * standaloneProdPBE.UnitPrice;
        insert standaloneProdOLI;
        standaloneProdOLI = [SELECT ID,Name FROM OpportunityLineItem WHERE ID =: standaloneProdOLI.Id];
        System.debug('OpportunityLineItem ===> ' + standaloneProdOLI.Name);
        
        oppForAsset.StageName = 'Qualified';
        
        System.debug('JoeTest Opportunity StageName ===> ' + oppForAsset.StageName);
        System.debug('JoeTest Opportunity Roll_Out_Start__c ===> ' + oppForAsset.Roll_Out_Start__c);
        System.debug('JoeTest Opportunity Rollout_Duration__c ===> ' + oppForAsset.Rollout_Duration__c);
        System.debug('JoeTest Opportunity Primary_Distributor__c ===> ' + oppForAsset.Primary_Distributor__c);
        System.debug('JoeTest Opportunity Primary_Reseller__C ===> ' + oppForAsset.Primary_Reseller__C);
        
        update oppForAsset;
        
        oppForAsset = [SELECT ID, AccountId, StageName, Primary_Distributor__c,Primary_Reseller__c, Roll_Out_Start__c, Rollout_Duration__c From Opportunity WHERE ID =: oppForAsset.Id];
        
        Asset asset = new Asset();
        asset.Name = 'testasset';
		asset.Project_Name__c = 'testasset';
        asset.AccountId = oppForAsset.AccountId;
        asset.Opportunity__c = oppForAsset.Id;
        asset.Installation_Partner__C = oppForAsset.Primary_Reseller__c;
        asset.Installation_Start_Date__c = date.today();
        asset.Installation_End_Date__c = date.today();
        asset.RecordTypeId = AssetTriggerHandler.IT_Installation_Recordtype;
        insert asset;
        
        //Testing when there is no previous Installation Site record.
        Installation_Site__C is = new Installation_Site__C();
        is.InstallationSiteAsset__c = asset.Id;
        is.name= 'testsite';
        is.Pixel_Pitch__c = 'IW008J';
        is.Start_Date__c = date.today();
        is.End_Date__C = date.today();
        is.Status__c = 'Not Scheduled';
        is.Installer_Phone__c = '123-456-7890';
        is.Installer_Email__c = 'test@gmail.com';
        is.Street__c = '100 Challenger Rd';
        is.City__c = 'Ridgefield Park';
        is.State__c = 'NJ';
        is.Zip_Code__c = '07660'; 
        is.LED_Wall_X_Cabinets_Wide__c = 1;
        is.LED_Wall_Y_Cabinets_High__c = 1;
        is.Customer_Spares__c = 1;
        is.Type__c = 'Mounting System';
        insert is;
        
        asset = [SELECT Id,Name,(SELECT Id, Name FROM Installation_sites__r ) FROM Asset WHERE ID =: asset.Id];
        System.debug('Installation Sites ===>' + asset.Installation_sites__r);

        try {
        	delete asset; //Test deleting with a installation site.
        } catch (Exception e) {
            Boolean expectedExceptionThrown =  e.getMessage().contains('The IT Installation Asset cannot be deleted since it has at least one installation site') ? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        }
        
        //asset = [SELECT Id,Name,(SELECT Id, Name FROM Installation_sites__r ) FROM Asset WHERE ID =: asset.Id];
        //System.debug('After deleting the asset ===>' + asset.Installation_sites__r);
        delete is; //Delete the installation site.
        
		/*
        try{
            AssetTriggerHandler.testVar = true;
            if(AssetTriggerHandler.testVar == true){
            	delete asset;
            }
        } catch (Exception e) {
            //System.AssertEquals('Error Message ====> ', e.getMessage());
        	Boolean expectedExceptionThrown =  e.getMessage().contains('Cannot delete the Asset.The asset has an Entitlement of') ? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        }
                
        AssetTriggerHandler.testVar = false;
		*/
        delete asset; //Test deleting without a installation site.
        Test.StopTest();
    }
}