/**
 * This class contains test methods for BatchForCIOpportunitySync
 * 
 * Author: Bala Rajasekharan
 */

@isTest
private class BatchForCIOpportunitySyncTest {

    @testSetup static void setup()   {
    	
         Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
    	//Create some test data
    	Account account = TestDataUtility.createAccount('Test POS SPA ID Sync');
        
        insert account;
        System.debug('Account insert success. Id='+account.id);
    	
    	PriceBook2 pricebook = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(4)); 
    	
        insert pricebook;
        System.debug('Pricebook insert success. ID='+pricebook.id);
    	Opportunity opportunity = TestDataUtility.createOppty('New', account, pricebook, 'Managed', 'IT', 'product group',
                                    'Identified');
        insert opportunity;
        System.debug('Opportunity insert success. ID='+opportunity.id);
        
    	
    	Quote q = TestDataUtility.createQuote('Test POS SPA ID Sync', opportunity,pricebook); 
        
    	//q.External_SPID__c = '000000067'; 	
    	insert q;    	
    	System.debug('Quote insert success. ID='+q.id);
    	System.debug('Quote Opp ID='+q.OpportunityId);
    	
        Quote quoteList = [SELECT  External_SPID__c FROM Quote WHERE id =:q.id].get(0);
    	System.debug('Quote  External_SPID__c: '+quoteList.External_SPID__c);
        Channelinsight__CI_Point_of_Sale__c pos = new Channelinsight__CI_Point_of_Sale__c(Channelinsight__POS_Transaction_ID__c='4363',Channelinsight__POS_Quantity__c=4,Channelinsight__POS_Best_Fit_Price__c=787,Channelinsight__POS_Invoice_Date__c = Date.today().addDays(-21)  );
        pos.Channelinsight__POS_Matched_External_Id__c = quoteList.External_SPID__c;
        insert pos;
        pos.Channelinsight__POS_CI_Opportunity__c=null;
        upsert pos;
        System.debug('Pos test data update worked');
        
      //  q.External_SPID__c = pos.Channelinsight__POS_Matched_External_Id__c;
       // upsert q;
        
        System.debug('Insert Channelinsight__CI_Point_of_Sale__c object success. Pos ID='+pos.Id);
                
        //Add custom setting
        CIOpportunitySync__c cs = new CIOpportunitySync__c(Name = 'Date_Filter' );
        cs.Created_Date__c = DateTime.now() - 30;
        cs.Last_Modified_Date__c = DateTime.now() - 30;
        insert cs;
        System.debug('Successfully created Custom Setting. ID '+cs.Id);
        
        System.debug('Creation Date: '+String.ValueOf(cs.Created_Date__c));
       
     
    }
        
        
    @isTest static  void  testupdatePosOpportunity() {
 		Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        //BatchForCIOpportunitySync batchClass;
        BatchForCIOpportunitySyncV2 batchClass;
        
        Test.startTest();
        //batchClass=new BatchForCIOpportunitySync();
        batchClass = new BatchForCIOpportunitySyncV2();
        Database.executeBatch(batchClass); 
        //System.debug('Batch job scheduled successfully. Id ='+processId);
        Test.stopTest(); 

    }
}