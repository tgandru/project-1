/* Author: Bala Rajasekharan
*  Description: This class Maps Salesforce Opportunity with Channel Insight Opportunity object.
*               This class should be scheduled to run periodically based on business synchronization requirements.
*               Custom setting 'CIOpportunitySync__c' is Optional, but has precedence over default value (This_Week).
*               During regular scheduled runs, please remove custom settings.
*/
global class BatchForCIOpportunitySyncV2 implements Database.Batchable<sObject> {
	
   // Get all CI POS objects that do not have opportunity populated		
   public static String query;

   //Read from custom setting.
   DateTime createdDt =  CIOpportunitySync__c.getInstance('Date_Filter').Created_Date__c;       
   DateTime lastModifiedDt =  CIOpportunitySync__c.getInstance('Date_Filter').Last_Modified_Date__c;

    global BatchForCIOpportunitySyncV2() {
  

    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
         
        System.debug('CustomSetting: createdDt = '+createdDt);
        //If the create date and/or last modified date is not set in custom setting, add default value of last week.
         if( (createdDt == null) && (lastModifiedDt == null) ){
             query = 'Select Id, Channelinsight__POS_Best_Fit_Price__c, Channelinsight__POS_Quantity__c, Channelinsight__POS_CI_Opportunity__c, Channelinsight__POS_Transaction_ID__c,Channelinsight__POS_Matched_External_Id__c from Channelinsight__CI_Point_of_Sale__c where Channelinsight__POS_CI_Opportunity__c=null and Channelinsight__POS_Matched_External_Id__c != null and (LastModifiedDate = This_Week or CreatedDate = This_Week)';
         }
         else {
  
             query ='Select Id,  Channelinsight__POS_Matched_External_Id__c from Channelinsight__CI_Point_of_Sale__c where Channelinsight__POS_CI_Opportunity__c=null and Channelinsight__POS_Matched_External_Id__c != null and ';                 

            //Convert Datetime to String for appending to query
            String cDate = null;
            String mDate = null;
            
            if(createdDt != null && lastModifiedDt == null) {
                cDate = createdDt.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
                query+='(CreatedDate >= '+cDate+')';
            }
            
            else if(createdDt == null && lastModifiedDt != null){
                mDate = lastModifiedDt.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
                query+='(LastModifiedDate >= '+mDate+')';
            } 
            else {
                cDate = createdDt.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
                 mDate = lastModifiedDt.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
                query+='(LastModifiedDate >= '+mDate+' or CreatedDate >= '+cDate+')';
            }             
 
		}
        System.debug('Query: '+query);
        return Database.getQueryLocator(query);

    }
    


    
    global void execute(Database.BatchableContext BC, List<sObject> scope) {

    	System.debug('Starting BatchForCIOpportunitySyncV2 job...with createdDate: '+createdDt+' or lastModifiedDate:'+lastModifiedDt );
    	
    	//Step 1: Query Channelinsight__CI_Point_of_Sale__c and populate a Map [Id ==> Pos Object]
    	//Map1:posId2ObjMap  Create a Map Pos Id ==> Pos Object
    	Map<Id, Channelinsight__CI_Point_of_Sale__c> posId2ObjMap = new Map <Id, Channelinsight__CI_Point_of_Sale__c>();  	
       
        //Step 2; Create another Map [Id ==>External_SPID__c]. This is a link between Channelinsight__CI_Point_of_Sale__c & Quote. This is a many:1 relationship.
        //Map2: posId2extMatchIStrMap
        Map<Id, String> posId2extMatchStrMap = new Map<Id,String>();
        
        //Step 3: Query Quote Object and populate Map for Quote Object mapping[ SpaID (externalMatchedID) | Quote.OpportunityID]
        //Map 3: spaId2ciOppIdMap
        Map<String, String> spaId2ciOppIdMap = new Map<String,String>(); 
        
        //Step 4: Query Channelinsight__CI_Opportunity__c and populate Map with Channelinsight__CI_Opportunity__c ID & Channelinsight__OPP_Opportunity__c 
        //There is a 1:1 relationship between Channelinsight__OPP_Opportunity__c = Quote.OpportunityId   	    	   	
    	Map<String, Channelinsight__CI_Opportunity__c> QuoteOppId2ciOppObjMap= new Map<String, Channelinsight__CI_Opportunity__c>(); 
   	
        //Populate the sets for using in query
        Set<String> matchedExtIdSet = new Set<String>();    	
    	//Set<String> sfOppIdSet = new Set<String>();
    	
    	
    	for(Channelinsight__CI_Point_of_Sale__c pos: (List<Channelinsight__CI_Point_of_Sale__c>)scope) { 
            
    		if(!String.isBlank(pos.Channelinsight__POS_Matched_External_Id__c)) {
				//Poputlate Map1 & Map2
    			posId2ObjMap.put(pos.Id, pos);
    			posId2extMatchStrMap.put(pos.Id, pos.Channelinsight__POS_Matched_External_Id__c);

    			//Add MatchedExt ID to a Set, for querying matching Quote objects
    			matchedExtIdSet.add(String.valueOf(pos.Channelinsight__POS_Matched_External_Id__c)); 
		
    		}     			    		

    	}
        
           	System.debug('Size of posId2ObjMap: '+posId2ObjMap.size());
            System.debug('Contents of matchedExtIdSet: '+matchedExtIdSet);
    		//Loop is added for debug loging, can be removed
    		System.debug('Contents of Map1: posId2objMap...');
    		for(Id posId1: posId2ObjMap.keySet() ) {
    				System.debug(posId1 +' ==> '+posId2ObjMap.get(posId1));    				
    		}
    		System.debug('Contents of Map2:posId2extMatchIStrMap...');   
    		for(Id posId2: posId2extMatchStrMap.keyset() ) {
    				System.debug(posId2 +' ==> '+posId2extMatchStrMap.get(posId2));    				
    		}    	 	
        
        
    	
    	for (Quote q: [select id,OpportunityId, External_SPID__c from Quote where External_SPID__c IN :matchedExtIdSet]) {            
    		if( !spaId2ciOppIdMap.containskey(q.External_SPID__c)  ){
    			spaId2ciOppIdMap.put(q.External_SPID__c, q.OpportunityId);    			
    		}
    	}
    	//Loop is added for debug loging, can be removed
    	System.debug('Contents of Map3: spaId2ciOppIdMap...');
    	for(String spaID: spaId2ciOppIdMap.keySet() ) {
    			System.debug(spaID +' ==> '+spaId2ciOppIdMap.get(spaID));    				
    	}
   	
    	//Populate Map4: QuoteOppId2ciOppObjMap
    	for (Channelinsight__CI_Opportunity__c ciOpp: [select Id, Channelinsight__OPP_Opportunity__c from Channelinsight__CI_Opportunity__c WHERE Channelinsight__OPP_Opportunity__c IN:spaId2ciOppIdMap.values() LIMIT 1]) {
    		QuoteOppId2ciOppObjMap.put(ciOpp.Channelinsight__OPP_Opportunity__c,ciOpp);
    	}
    	//Loop is added for debug loging, can be removed
    	System.debug('Contents of Map4: spaId2ciOppObjMap...');
    	for(String ciOppIdStr: QuoteOppId2ciOppObjMap.keySet() ) {
    			System.debug(ciOppIdStr +' ==> '+QuoteOppId2ciOppObjMap.get(ciOppIdStr));    				
    	}
    	
    	//Map the CI opp to POS
    	String oppId;
        Channelinsight__CI_Opportunity__c ciOpp1;
        Channelinsight__CI_Point_of_Sale__c pos1;
        List<Channelinsight__CI_Point_of_Sale__c> posListToUpdate = new List<Channelinsight__CI_Point_of_Sale__c>();
        for(Id posId :posId2extMatchStrMap.keyset()) {
            if(posId2extMatchStrMap.containsKey(posId)) {
                oppId = spaId2ciOppIdMap.get(posId2extMatchStrMap.get(posId));                
                if(QuoteOppId2ciOppObjMap.containsKey(oppId)) {
                    System.debug('Found Opp ID in spaId2ciOppIdMap(Map3): '+oppId);
                    ciOpp1 = QuoteOppId2ciOppObjMap.get(oppId);                    
                    if(posId2ObjMap.containsKey(posId)){
                        System.debug('Found Channelinsight__CI_Opportunity__c id: '+ciOpp1.id);
                        pos1 = posId2ObjMap.get(posId);
                        System.debug('Found Channelinsight__CI_Point_of_Sale__c object: '+pos1);
                        pos1.Channelinsight__POS_CI_Opportunity__c = ciOpp1.Id;            
                        System.debug('Mapping '+pos1.Channelinsight__POS_CI_Opportunity__c + 'with' +ciOpp1.Id);
                        posListToUpdate.add(pos1);
                    }
                }
            }
                 
        }
        
        try {
             
    		if(!posListToUpdate.isEmpty()) {
                //System.debug('Trying to update pos with CI_OPP: '+posListToUpdate.get(0) );
    			upsert posListToUpdate;
                System.debug('Upsert was success!!! ');
    		}
    	}
    	catch( Exception e) {
    		System.debug('Database error: '+e.getmessage());
    	}
    }
    	

        global void finish(Database.BatchableContext BC) {
        // nothing here
    }
	

    
}