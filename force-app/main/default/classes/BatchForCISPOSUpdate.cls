/*
Author: Kavitha Reddy
Description: 
1. Query CI POS object to get the updates of Quantity,price and update it on respective PricelineItem and Roll Out Schedule records
2. Run this batch job once in a Month
Update: Based on discussion with Bala - Duration is changed to Last_week on 07.15.16
*/
global class BatchForCISPOSUpdate implements Database.Batchable<sObject> {

   //Fetch date range values from custom setting.
   DateTime createdPOSDate =  CIOpportunitySync__c.getInstance('CISPOS_DateFilter').Created_Date__c;       
   DateTime lastModifiedPOSDate =  CIOpportunitySync__c.getInstance('CISPOS_DateFilter').Last_Modified_Date__c;
   public static String query = '';

    
    global BatchForCISPOSUpdate() {

    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        System.debug('## createdPOSDate ::'+createdPOSDate+'LastModifiedDate :: '+lastModifiedPOSDate);
        
        if(createdPOSDate == null && lastModifiedPOSDate == null) {
            query ='Select Id, Channelinsight__POS_CI_Opportunity__c, Channelinsight__POS_CI_Opportunity__r.Channelinsight__OPP_Opportunity__c,Channelinsight__POS_Matched_Product_SKU__c, Channelinsight__POS_Reported_Product_SKU__c,'+
            ' Channelinsight__POS_Quantity__c,Channelinsight__POS_Extended_Best_Fit_Price__c,Channelinsight__POS_Invoice_Date__c from Channelinsight__CI_Point_of_Sale__c'+
            ' where Channelinsight__POS_CI_Opportunity__c!=null and (LastModifiedDate = Last_Week or CreatedDate = Last_Week)';
        }
        else{
            query ='Select Id, Channelinsight__POS_CI_Opportunity__c, Channelinsight__POS_CI_Opportunity__r.Channelinsight__OPP_Opportunity__c,Channelinsight__POS_Matched_Product_SKU__c, Channelinsight__POS_Reported_Product_SKU__c,'+
            ' Channelinsight__POS_Quantity__c,Channelinsight__POS_Extended_Best_Fit_Price__c,Channelinsight__POS_Invoice_Date__c from Channelinsight__CI_Point_of_Sale__c'+
            ' where Channelinsight__POS_CI_Opportunity__c!=null and ';
            //Convert Datetime to String for appending to query
            String cDate = null;
            String mDate = null;
            
            if(createdPOSDate != null && lastModifiedPOSDate == null) {
                cDate = createdPOSDate.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
                query+='(CreatedDate >= '+cDate+')';
            }
            
            else if(createdPOSDate == null && lastModifiedPOSDate != null){
                mDate = lastModifiedPOSDate.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
                query+='(LastModifiedDate >= '+mDate+')';
            } 
            else {
                cDate = createdPOSDate.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
                 mDate = lastModifiedPOSDate.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
                query+='(LastModifiedDate >= '+mDate+' or CreatedDate >= '+cDate+')';
            }
        }
        system.debug('Query....'+query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        
        Set<Id> ciSet = new Set<Id>();
        Map<Id,Id> CIOpptyIdMap = new Map<Id,Id>();//CIOpptyIdSet
        Set<string> CIReportedSKU = new Set<string>();
        Set<Id> oliIdSet = new Set<Id>();
        List<OpportunityLineItem> oliToUpdate = new List<OpportunityLineItem>();
        List<Roll_Out_Schedule__c> rosToUpdate = new List<Roll_Out_Schedule__c>();

        Map<string,Integer> CISOpptyToQtyMap = new Map<string, Integer>();                
        Map<string,OpportunityLineItem> oliMap = new Map<string,OpportunityLineItem>();  
        Map<Id, Id> ropToOLIMap = new Map<Id,Id>();
        Map<string,Map<string,Channelinsight__CI_Point_of_Sale__c>> posFWMap = new Map<string,Map<string,Channelinsight__CI_Point_of_Sale__c>>();
        Map<string,Map<string,Roll_Out_Schedule__c>> rosFWMap = new Map<string,Map<string,Roll_Out_Schedule__c>>();
        Map<Id,String> productMap = new Map<Id,String>();
        //Fetch CI POS information
        String tempPOSFWId = '';
        string oppIdShortForm = '';
        Integer tempFw = 0;
        String posYear =''; 
        String posFWYear = '';
        
        for(Channelinsight__CI_Point_of_Sale__c pos: (List<Channelinsight__CI_Point_of_Sale__c>)scope) {
            ciSet.add(pos.Id); 
            CIOpptyIdMap.put(string.valueOf(pos.Channelinsight__POS_CI_Opportunity__c),pos.Channelinsight__POS_CI_Opportunity__r.Channelinsight__OPP_Opportunity__c); 
            CIReportedSKU.add(pos.Channelinsight__POS_Reported_Product_SKU__c);
            System.debug('##KR CIS OpptyId'+pos.Channelinsight__POS_CI_Opportunity__r.Channelinsight__OPP_Opportunity__c);
            
            tempFw = RosMiscUtilities.giveFiscalWeek(pos.Channelinsight__POS_Invoice_Date__c);
            posYear = string.valueOf(pos.Channelinsight__POS_Invoice_Date__c.year());
            posFWYear = string.valueOf(tempFw)+''+posYear;
            oppIdShortForm = string.valueOf(pos.Channelinsight__POS_CI_Opportunity__r.Channelinsight__OPP_Opportunity__c);
            tempPOSFWId = oppIdShortForm.substring(0,15)+string.valueOf(pos.Channelinsight__POS_Reported_Product_SKU__c);
            
            if(!posFWMap.containsKey(tempPOSFWId)){
                posFWMap.put(tempPOSFWId,new Map<string,Channelinsight__CI_Point_of_Sale__c>());
            }  
            posFWMap.get(tempPOSFWId).put(posFWYear,pos);
        }
        //Below for loop to be removed later - Testing purpose
            for(String x: posFWMap.keySet()){
                System.debug('## posFWMap Outer Map'+x+' :: '+posFWMap.get(x));
            }
        
        //Aggregated values of Quantity group by Opportunity and Product SKU
        AggregateResult[] groupedResults = [SELECT SUM(Channelinsight__POS_Quantity__c)qtyTotal, Channelinsight__POS_CI_Opportunity__c, Channelinsight__POS_Reported_Product_SKU__c
                                            FROM Channelinsight__CI_Point_of_Sale__c where Id IN:ciSet GROUP BY Channelinsight__POS_CI_Opportunity__c, Channelinsight__POS_Reported_Product_SKU__c];
        string tempCISExtId = '';
        string tempOppty = '';
        for (AggregateResult ar : groupedResults)  {
            tempOppty = string.valueOf(ar.get('Channelinsight__POS_CI_Opportunity__c'));
            tempCISExtId = string.valueOf(CIOpptyIdMap.get(tempOppty)) +string.valueOf(ar.get('Channelinsight__POS_Reported_Product_SKU__c'));
            System.debug('## tempCISExtId'+tempCISExtId);
            CISOpptyToQtyMap.put(tempCISExtId,Integer.valueOf(ar.get('qtyTotal')));
            System.debug('##KR Channelinsight__POS_CI_Opportunity__r.Channelinsight__OPP_Opportunity__c' + ar.get('Channelinsight__POS_CI_Opportunity__c'));
            System.debug('##KR qtyTotal' + ar.get('qtyTotal'));
        }
        
        //Query on OpportunityLineItems
        string tempExtId = '';
        for(OpportunityLineItem oli: [Select Id,OpportunityId,SAP_Material_Id__c,Shipped_Quantity__c from OpportunityLineItem where OpportunityId IN: CIOpptyIdMap.values() and SAP_Material_Id__c IN: CIReportedSKU]){
            tempExtId = string.valueOf(oli.OpportunityId) +string.valueOf(oli.SAP_Material_Id__c);
            oliMap.put(tempExtId,oli); 
            oliIdSet.add(oli.Id);
        }
        System.debug('##KR oliMap Values'+oliMap.values());
        
        //Query on RollOut Products
        for(Roll_Out_Product__c rop: [Select Id,Opportunity_Line_Item__c from Roll_Out_Product__c where Opportunity_Line_Item__c IN: oliIdSet]){         
            ropToOLIMap.put(rop.Id,rop.Opportunity_Line_Item__c);
        }
        
        //Query on Products
        for(Product2 product:[Select Id,SAP_Material_ID__c from Product2 where SAP_Material_ID__c IN:CIReportedSKU]){
            productMap.put(product.Id, product.SAP_Material_ID__c);
        }
        
        //Query on Roll Out Schedules
        String tempRosExtId = '';
        String tempROSFWYear = '';
        for(Roll_Out_Schedule__c ros:[Select Id, Roll_Out_Product__c,Roll_Out_Product__r.Product__c,fiscal_week__c,Opportunity_Id__c,Actual_Quantity__c,Actual_Revenue__c,year__c from Roll_Out_Schedule__c where Roll_Out_Product__c IN: ropToOLIMap.keyset()]){
            tempRosExtId = String.valueOf(ros.Opportunity_Id__c)+String.valueOf(productMap.get(ros.Roll_Out_Product__r.Product__c));
            tempROSFWYear =ros.fiscal_week__c+''+ros.year__c;
            if(!rosFWMap.containsKey(tempRosExtId)){
                rosFWMap.put(tempRosExtId, new Map<String,Roll_Out_Schedule__c>());
            }
            rosFWMap.get(tempRosExtId).put(tempROSFWYear,ros);            
        }
        //Below for loop to be removed later - Testing purpose
            for(String y: rosFWMap.keySet()){
                System.debug('## rosFWMap outer Map'+y);
                for(String rosInteger:rosFWMap.get(y).keySet()){
                    System.debug('## rosFWMap Inner Map'+rosInteger+' :: '+rosFWMap.get(y).get(rosInteger));}
            }
        
        //Update OpportunityLineItems
        String tempOLIExtId = '';
        for(OpportunityLineItem oliRec: oliMap.values()){
            tempOLIExtId = string.valueOf(oliRec.OpportunityId)+string.valueOf(oliRec.SAP_Material_Id__c);
            if(CISOpptyToQtyMap.containsKey(tempOLIExtId)){
                oliMap.get(tempOLIExtId).Shipped_Quantity__c = CISOpptyToQtyMap.get(tempOLIExtId);  
                oliToUpdate.add(oliMap.get(tempOLIExtId));
            }
        }
        System.debug('##kr:: oliToUpdate'+oliToUpdate);
        
        //Update RollOut Schedules - where FW+year matches for a respective Opportunity+product combination
        for(string posExtId: posFWMap.keySet()){
            for(string posFW: posFWMap.get(posExtId).keySet()){
                if(rosFWMap.containsKey(posExtId)){
                    System.debug('## @if: posExtId'+posExtId);
                    if(rosFWMap.get(posExtId).containsKey(posFW)){
                        rosFWMap.get(posExtId).get(posFW).Actual_Quantity__c = posFWMap.get(posExtId).get(posFW).Channelinsight__POS_Quantity__c; 
                        rosFWMap.get(posExtId).get(posFW).Actual_Revenue__c = posFWMap.get(posExtId).get(posFW).Channelinsight__POS_Extended_Best_Fit_Price__c;
                        rosToUpdate.add(rosFWMap.get(posExtId).get(posFW));
                    }
                }
            }
        }
        System.debug('##kr:: rosToUpdate'+rosToUpdate);
        try { 
            if(oliToUpdate.size()>0){
                update oliToUpdate;
            }
            if(rosToUpdate.size()>0){
                update rosToUpdate;
            }
        }catch(DMLException e) {
                Integer numErrors = e.getNumDml();
                System.debug('Number DML Errors: ' + numErrors);
                for(Integer i=0; i < numErrors; i++) {
                    System.debug('Dml Field Names: ' + e.getDmlFieldNames(i));
                    System.debug('Dml Message: ' + e.getDmlMessage(i));
                }
            } catch(Exception e) {
                System.debug('Exception occured updating CIS POS :: ' + e.getMessage());
            }
    } 

    global void finish(Database.BatchableContext BC) {
        // nothing here
    }

}