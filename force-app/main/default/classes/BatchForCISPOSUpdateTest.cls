/**
  *   BatchCISPOSUpdateTest
  *   @author: Kavitha Reddy
  **/

@isTest
private class BatchForCISPOSUpdateTest {

    private static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
     @testSetup static void setup() {
        
        Test.loadData(fiscalCalendar__c.sObjectType, 'FiscalCalendarTestData');
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        //List<CIOpportunitySync__c> CIDates = new List<CIOpportunitySync__c>();
        List<CIOpportunitySync__c> CIDatesList = new List<CIOpportunitySync__c>();

        //for(CIOpportunitySync__c updateCIS : CIDates) {
         CIOpportunitySync__c updateCIS = new CIOpportunitySync__c();
        
                updateCIS.Name = 'CISPOS_DateFilter';
                updateCIS.Created_Date__c=System.now();
                updateCIS.Last_Modified_Date__c=System.now();
                CIDatesList.add(updateCIS);
        //}
        insert CIDatesList;
        
        Account account = TestDataUtility.createAccount('Test Accounti am');
        insert account;
        
        Account partnerAcc = TestDataUtility.createAccount('Distributor account');
        partnerAcc.RecordTypeId = directRT;
        insert partnerAcc;
        
         //Creating custom Pricebook
        PriceBook2 pb = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(4)); 
        insert pb;
        Opportunity oppty = TestDataUtility.createOppty('New', account, pb, 'Managed', 'IT', 'product group',
                                    'Identification');
        oppty.CloseDate=System.today();
        oppty.Roll_Out_Start__c = System.today().addDays(1);
        oppty.Rollout_Duration__c = 1.0;
        insert oppty;
        
        Product2 productA = TestDataUtility.createProduct2('SL-SCF5300', 'Standalone Hardware', 'Printer[C2]', 'H/W', 'FAX/MFP');
        productA.ProductCode = '33';
        insert productA;
        Product2 productB = TestDataUtility.createProduct2('SL-SCF5302', 'Standalone Printer', 'Printer[C2]', 'H/W', 'FAX/MFP');
        productB.ProductCode = '33';
        insert productB;
        Product2 productC = TestDataUtility.createProduct2('SL-SCF5303', 'Printer', 'Printer[C2]', 'H/W', 'FAX/MFP');
        productC.ProductCode = '33';
        insert productC;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry pbe_Stnd1 = TestDataUtility.createPriceBookEntry(productA.Id, pricebookId);
        pbe_Stnd1.UnitPrice = 99.1;
       // insert pbe_Stnd1;
        
        PricebookEntry pbe_Stnd2 = TestDataUtility.createPriceBookEntry(productB.Id, pricebookId);
        pbe_Stnd2.UnitPrice = 1.2;
      //  insert pbe_Stnd2;
        
        PricebookEntry pbe_Stnd3 = TestDataUtility.createPriceBookEntry(productC.Id, pricebookId);
        pbe_Stnd3.UnitPrice = 102.3;
      //  insert pbe_Stnd3;
        
        PricebookEntry pbe1 = TestDataUtility.createPriceBookEntry(productA.Id, pb.Id);
        pbe1.UnitPrice = 11.0;
        insert pbe1;

        PricebookEntry pbe2 = TestDataUtility.createPriceBookEntry(productB.Id, pb.Id);
        pbe2.UnitPrice = 11.1;
        insert pbe2;
        
        PricebookEntry pbe3 = TestDataUtility.createPriceBookEntry(productC.Id, pb.Id);
        pbe3.UnitPrice = 11.2;
        insert pbe3;
        
        List<OpportunityLineItem> opportunityLineItems = new List<OpportunityLineItem>();
        OpportunityLineItem op1 = TestDataUtility.createOpptyLineItem(productA, pbe1, oppty);
        op1.Quantity = 100;
        op1.TotalPrice = 1000;
        op1.Requested_Price__c = 100;
        op1.Claimed_Quantity__c = 45;     
        opportunityLineItems.add(op1);
        
        OpportunityLineItem op2 = TestDataUtility.createOpptyLineItem(productB, pbe2, oppty);
        op1.Quantity = 100;
        op1.TotalPrice = 1000;
        op1.Requested_Price__c = 100;
        op1.Claimed_Quantity__c = 45;     
        opportunityLineItems.add(op2);
        
        OpportunityLineItem op3 = TestDataUtility.createOpptyLineItem(productC, pbe3, oppty);
        op1.Quantity = 100;
        op1.TotalPrice = 1000;
        op1.Requested_Price__c = 100;
        op1.Claimed_Quantity__c = 45;     
        opportunityLineItems.add(op3);
        insert opportunityLineItems;
        
        System.debug('Insert OLI success');
        
        //Create record Channelinsight__CI_Opportunity__c
        /*Channelinsight__CI_Opportunity__c ciOPP = new Channelinsight__CI_Opportunity__c(Channelinsight__OPP_Opportunity__c =oppty.Id);
        insert ciOPP;*/
        Channelinsight__CI_Opportunity__c ciOPP = [Select Id,Channelinsight__OPP_Opportunity__c from Channelinsight__CI_Opportunity__c where Channelinsight__OPP_Opportunity__c=:oppty.Id];
        System.debug('## CI Oppty'+ciOPP);
        
        //Create record for Channelinsight__CI_Point_of_Sale__c //add namespace prefix Channelinsight__
        Channelinsight__CI_Point_of_Sale__c pos = new Channelinsight__CI_Point_of_Sale__c(Channelinsight__POS_CI_Opportunity__c = ciOPP.id,
                                                                                Channelinsight__POS_Matched_Product_SKU__c='SL-SCF5302',
                                                                                Channelinsight__POS_Reported_Product_SKU__c='SL-SCF5302',
                                                                                Channelinsight__POS_Quantity__c = 1.0,
                                                                                Channelinsight__POS_Best_Fit_Price__c=787,
                                                                                Channelinsight__POS_Invoice_Date__c = Date.today().addDays(-21),
                                                                                Channelinsight__POS_Transaction_ID__c ='pos12233' );////System.today(),
        insert pos;
        System.debug('## CI pos'+pos);
    }
    
    static testMethod void BatchCISPOSUpdateTest1() {
    //setup();
    Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        BatchForCISPOSUpdate batchClass;
        
        Test.startTest();
        batchClass=new BatchForCISPOSUpdate();
        Database.executeBatch(batchClass);   
        Test.stopTest(); 
    
    }
    static testMethod void BatchCISPOSUpdateWithNullDates() {
   
    Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        CIOpportunitySync__c cisRec = [Select ID,Created_Date__c,Last_Modified_Date__c,Name from CIOpportunitySync__c where Name='CISPOS_DateFilter' limit 1];
            cisRec.Created_Date__c = null;
            cisRec.Last_Modified_Date__c = null;
        update cisRec;
        
        BatchForCISPOSUpdate batchClass;
        
        Test.startTest();
        batchClass=new BatchForCISPOSUpdate();
        Database.executeBatch(batchClass);   
        Test.stopTest(); 
    
    }
    static testMethod void BatchCISPOSUpdateWithNullCreatedDate() {
    
    Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        CIOpportunitySync__c cisRec = [Select ID,Created_Date__c,Last_Modified_Date__c,Name from CIOpportunitySync__c where Name='CISPOS_DateFilter' limit 1];
            cisRec.Created_Date__c = null;
            cisRec.Last_Modified_Date__c = System.now();
        update cisRec;
        BatchForCISPOSUpdate batchClass;
        
        Test.startTest();
        batchClass=new BatchForCISPOSUpdate();
        Database.executeBatch(batchClass);   
        Test.stopTest(); 
    
    }
    static testMethod void BatchCISPOSUpdateWithNullModifiedDate() {
    
    Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        CIOpportunitySync__c cisRec = [Select ID,Created_Date__c,Last_Modified_Date__c,Name from CIOpportunitySync__c where Name='CISPOS_DateFilter' limit 1];
            cisRec.Created_Date__c = System.now();
            cisRec.Last_Modified_Date__c = null;
        update cisRec;
        BatchForCISPOSUpdate batchClass;
        
        Test.startTest();
        batchClass=new BatchForCISPOSUpdate();
        Database.executeBatch(batchClass);   
        Test.stopTest(); 
    
    }    
}