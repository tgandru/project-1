/* Author: Jeongho Lee
*  Description: Weekly Case SnapShot
Execute : BatchForCaseSnapShot b = new BatchForCaseSnapShot();
         
         Manually excuete option 1
         b.setDate = '5/11/2017';
         database.executebatch(b);

         Manually excute option 2
         database.executebatch(b);
*/
public class BatchForCaseSnapShot implements Database.Batchable<sObject>, Database.Stateful{

    public String                       allfields;                  // query String
    public String                       setDate;
    public set<String>                  accSet;
    public Integer                      befYear;
    
    public BatchForCaseSnapShot() {
        Map<String, Schema.SObjectField> fields = Schema.getGlobalDescribe().get('Case').getDescribe().SObjectType.getDescribe().fields.getMap();
        List<String> accessiblefields = new List<String>();
  
        for(Schema.SObjectField field : fields.values()){
            if(field.getDescribe().isAccessible())
                accessiblefields.add(field.getDescribe().getName());
        }
        allfields='';
        for(String fieldname : accessiblefields)
            allfields += fieldname+',';
        allfields = allfields.subString(0,allfields.length()-1);

        system.debug(allfields);

        accSet = new set<String>();
        for(Test_Account__c  ta : [SELECT Id, Id__c FROM TEST_Account__c]){
            accSet.add(ta.Id__c);
        }
        befYear = null;
    }

    public Database.QueryLocator start(Database.BatchableContext BC) {
        // batch start 
        String query = '';
        query += ' SELECT ' + allfields;
        query += ' , RecordType.Name ';
        query += ' , RecordType.DeveloperName ';
        query += ' FROM Case ';
        query += ' WHERE RecordType.DeveloperName != \'SEA_Internal\' ';
        if(setDate != null){
            Date tempDate   = Date.parse(setDate);
            befYear         = tempDate.addYears(-1).year();
            /*Date stDate       = tempDate.adddays(-7).toStartofWeek();
            Date edDate     = stDate.adddays(8);
            query += ' AND DAY_ONLY(CreatedDate) >= ' + String.valueOf(stDate);
            query += ' AND DAY_ONLY(CreatedDate) <  ' + String.valueOf(edDate);*/
            query += ' AND CALENDAR_YEAR(CreatedDate) >=: befYear ' ;  
        }
        else{
            befYear = Date.today().addYears(-1).year();
            query += ' AND CALENDAR_YEAR(CreatedDate) >=: befYear ';
        }
        query += ' AND AccountId NOT IN: accSet';

        system.debug(query);

        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<sObject> scope) {

        List<Case> caseList = (List<Case>)scope;
        List<Case_SnapShot__c> cssList = new List<Case_SnapShot__c>();

        for(Case cs : caseList){

            Case_SnapShot__c css = new Case_SnapShot__c();

            css.Case_Id__c                  = cs.Id                             ;
            css.Origin__c                   = cs.Origin                         ;
            css.Type__c                     = cs.Type                           ;
            css.IsClosed__c                 = cs.IsClosed                       ;
            css.ClosedDate__c               = cs.ClosedDate                     ;
            css.Case_CreatedById__c         = cs.CreatedById                    ;
            css.Case_CreatedDate__c         = cs.CreatedDate                    ;
            css.Date_Assigned__c            = cs.Date_Assigned__c               ;
            css.Division__c                 = cs.Division__c                    ;
            css.EntitlementId__c            = cs.EntitlementId                  ;
            css.Priority__c                 = cs.Priority                       ;
            css.Severity__c                 = cs.Severity__c                    ;
            css.Status__c                   = cs.Status                         ;
            css.Sub_Status__c               = cs.Sub_Status__c                  ;
            css.Case_SystemModstamp__c      = cs.SystemModstamp                 ;
            css.Case_LastModifiedById__c    = cs.LastModifiedById               ;
            css.Case_LastModifiedDate__c    = cs.LastModifiedDate               ;
            css.Account__c                  = cs.AccountId                      ;
            css.Contact__c                  = cs.ContactId                      ;
            css.RecordType__c               = cs.RecordType.Name                ;
            css.Category_1__c               = cs.Category_1__c                  ;
            css.Category_2__c               = cs.Category_2__c                  ;
            css.Level_1__c                  = cs.Level_1__c                     ;
            css.Level2SymtomCode__c         = cs.Level2SymptomCode__c           ;
            css.Subject__c                  = cs.Subject                        ;
            css.Description__c              = cs.Description                    ;
            css.Service_Provider_Account__c = cs.Service_Provider_Account__c    ;
            css.Service_Provider_Contact__c = cs.Service_Provider_Contact__c    ;
            css.C_Report_Week__c            = String.valueOf(isoWeekNumber(Date.valueOf(cs.CreatedDate)))           ;
            css.SC_Report_Week__c           = String.valueOf(isoWeekNumber(Date.valueOf(system.now()).adddays(-7))) ;
            css.Created_Week_Month__c       = String.valueOf(isoYearNumber(Date.valueOf(cs.CreatedDate)))           ;
            css.Snapshot_Week_Month__c      = String.valueOf(isoYearNumber(Date.valueOf(system.now()).adddays(-7))) ;
            cssList.add(css);
            system.debug(cssList);
        }
        if(!cssList.isEmpty()){
            system.debug(cssList);
            insert cssList;
        }
    }
    /**
     * Batch Finish 
     **/
    public void finish(Database.BatchableContext BC) {
    }
    private static Integer isoWeekNumber(Date value) {

        Integer daysSince1900_01_07 = Date.newInstance(1900, 1, 7).daysBetween(value);
        Integer dayNumber = Math.mod(daysSince1900_01_07, 7) + 1;
        Date dateForYear = value.addDays(Math.mod(8 - dayNumber, 7) - 3);
        Integer year = dateForYear.year();
        Date year_01_01 = Date.newInstance(year, 1, 1);
        Integer week = (Integer)Math.floor((year_01_01.daysBetween(value) + Math.mod((Math.mod(Date.newInstance(1900, 1, 7).daysBetween(year_01_01), 7) + 1) + 1, 7) - 3) / 7 + 1);
        return week;

    }
    public static Integer isoYearNumber(Date value){
        Integer daysSince1900_01_07 = Date.newInstance(1900, 1, 7).daysBetween(value);
        Integer dayNumber = Math.mod(daysSince1900_01_07, 7) + 1;
        Date dateForYear = value.addDays(Math.mod(8 - dayNumber, 7) - 3);
        Integer year = dateForYear.year();
        return year;
    }
}