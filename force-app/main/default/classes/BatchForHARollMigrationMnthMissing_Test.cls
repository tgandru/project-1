/**
 * Created by: Thiru
 * Purpose   : Test Class for BatchForHARolloutMigrationMonthMissing class
**/
@isTest
private class BatchForHARollMigrationMnthMissing_Test {

	@isTest(SeeAllData=true)
	static void CreateTest() {
        Test.startTest();
            Map<String,List<String>> ropMap = new Map<String,List<String>>();
            ropMap.put('testROP',new List<String>{'testROP'});
            BatchForHARolloutMigrationMonthMissing ba = new BatchForHARolloutMigrationMonthMissing(null,null,null);
            ba.updatedROPs = ropMap;
            Database.executeBatch(ba);
        Test.stopTest(); 
	}
}