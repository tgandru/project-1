/**
 * Created by: Thiru
 * Purpose   : To update the Rollout plan for missing Rollout Months
**/
global class BatchForHARolloutMigrationMonthMissing implements database.batchable<sObject>,database.stateful {
    global Map<Date,SCM_Calendar__c> fiscalWeekMap;
    global Map<Date,SCM_Calendar__c> OrderedfiscalWeekMap;
    global integer rolloutCreatedYear;
    global List<integer> rolloutCreatedMonths;
    global integer BatchCount;
    global Map<String,List<String>> updatedROPs;
    global List<String> rolloutProductIds;
    
    global BatchForHARolloutMigrationMonthMissing(integer calendarYear, List<integer> calendarMonth, List<String> ropIDs){
        rolloutCreatedYear   = calendarYear;
        rolloutCreatedMonths = new List<integer>();
        rolloutCreatedMonths = calendarMonth;
        rolloutProductIds    = new List<String>();
        rolloutProductIds    = ropIDs;
        BatchCount = 0;
        
        fiscalWeekMap = new Map<Date,SCM_Calendar__c>();
        OrderedfiscalWeekMap = new Map<Date,SCM_Calendar__c>();
        updatedROPs = new Map<String,List<String>>();
    }
    
    global database.queryLocator start(database.batchableContext bc){
        if(Test.isRunningTest()){
            database.Querylocator ql = database.getquerylocator([select id,
                                                                        SAP_Material_Code__c,
                                                                        Roll_Out_Plan__r.Roll_Out_Start__c,
                                                                        Roll_Out_Plan__r.Rollout_Duration__c 
                                                                 from Roll_Out_Product__c 
                                                                 where Roll_Out_Plan__r.Opportunity__r.Recordtype.Name='HA Builder' limit 10]);
            return ql;
        }else if(rolloutProductIds.size()>0){
            database.Querylocator ql = database.getquerylocator([select id,
                                                                        SAP_Material_Code__c,
                                                                        Roll_Out_Plan__r.Roll_Out_Start__c,
                                                                        Roll_Out_Plan__r.Rollout_Duration__c 
                                                                 from Roll_Out_Product__c 
                                                                 where Roll_Out_Plan__r.Opportunity__c=:rolloutProductIds]); //id=:rolloutProductIds]);
            return ql;
        }else{
            database.Querylocator ql = database.getquerylocator([select id,
                                                                        SAP_Material_Code__c,
                                                                        Roll_Out_Plan__r.Roll_Out_Start__c,
                                                                        Roll_Out_Plan__r.Rollout_Duration__c 
                                                                 from Roll_Out_Product__c 
                                                                 where Roll_Out_Plan__r.Opportunity__r.Recordtype.Name='HA Builder' 
                                                                   and Calendar_Year(lastmodifieddate)=:rolloutCreatedYear 
                                                                   and CALENDAR_MONTH(lastmodifieddate)=:rolloutCreatedMonths]);
            return ql;
        }
    }
    global void execute(database.batchableContext bc, List<Roll_Out_Product__c> ropList){
        BatchCount++;
        
        List<Roll_Out_Schedule__c> insertROS = new List<Roll_Out_Schedule__c>();
        Map<id,Roll_Out_Product__c> ropMap = new Map<id,Roll_Out_Product__c>(ropList);
        Map<String,List<date>> ropPlandatesMap = new Map<String,List<date>>();
        
        if(BatchCount==1){
            List<Date> sortLst = new List<Date>();
            for(SCM_Calendar__c fc: HA_RosMiscUtilities.lstFiscCal){
                fiscalWeekMap.put(fc.WeekStartDate__c,fc);
            }
            
            sortLst.addAll(fiscalWeekMap.keyset());
            sortLst.sort();
            
            for(Date d:sortLst){
                OrderedfiscalWeekMap.put(d,fiscalWeekMap.get(d));
            }
        }
        
        for(Roll_Out_Schedule__c ros : [Select id,Plan_Date__c,Roll_Out_Product__c from Roll_Out_Schedule__c where Roll_Out_Product__c=:ropMap.keyset()]){
            List<date> planDates = new List<date>();
            if(ropPlandatesMap.containsKey(ros.Roll_Out_Product__c)){
                planDates = ropPlandatesMap.get(ros.Roll_Out_Product__c);
            }
            planDates.add(ros.Plan_Date__c);
            ropPlandatesMap.put(ros.Roll_Out_Product__c,planDates);
        }
        
        for(Roll_Out_Product__c rop : ropList){
            Set<Date> duplicatePrev = new Set<Date>();
            integer Duration = (Integer)rop.Roll_Out_Plan__r.Rollout_Duration__c;
            date EndDateCalculated = (rop.Roll_Out_Plan__r.Roll_Out_Start__c).addMonths(Duration);
            Date weekBeginforStartDate = HA_RosMiscUtilities.getWeekBeginDate(rop.Roll_Out_Plan__r.Roll_Out_Start__c);
            Date weekBeginforEndDate = HA_RosMiscUtilities.getWeekBeginDate(EndDateCalculated);
            
            if(ropPlandatesMap.containsKey(rop.id)){
                List<date> planDates = ropPlandatesMap.get(rop.id);
                Boolean weekStart = FALSE;
                Boolean weekEnd = FALSE;
                
                for(Date d : OrderedfiscalWeekMap.keyset()){
                    if(!planDates.contains(d) && d >= weekBeginforStartDate && d <= weekBeginforEndDate && !duplicatePrev.contains(d)){
                        insertROS.add(new Roll_Out_Schedule__c(
                                                    Roll_Out_Product__c = rop.Id,  
                                                    Plan_Date__c = d, 
                                                    fiscal_week__c =  OrderedfiscalWeekMap.get(d).FiscalWeek__c,
                                                    fiscal_month__c = OrderedfiscalWeekMap.get(d).FiscalMonth__c,
                                                    YearMonth__c = OrderedfiscalWeekMap.get(d).FiscalYear__c + HA_RosMiscUtilities.strMM(OrderedfiscalWeekMap.get(d).FiscalMonth__c),
                                                    year__c= OrderedfiscalWeekMap.get(d).FiscalYear__c,
                                                    Plan_Quantity__c = 0));
                        duplicatePrev.add(d);
                        
                        List<String> planDatesList = new List<String>();
                        if(updatedROPs.containsKey(rop.Id)){
                            planDatesList = updatedROPs.get(rop.Id);
                        }
                        planDatesList.add(string.valueOf(d));
                        updatedROPs.put(rop.id,planDatesList);
                        
                    }
                    if(d == weekBeginforEndDate){
                        break;
                    }
                }
            }
        }
        try{
            if(insertROS.size()>0) insert insertROS;   
        }
        catch(Exception ex){
            System.debug('##ex inside New HA ROS Insert ::'+ex);
        }
    }
    global void finish(Database.BatchableContext bc){
        if(updatedROPs.size()>0){
            String body = 'Batch job for Updating Rollout Months missing data has finished. \n' + 'PFA - Updated Rollout Products and its newly created ROS Plan Dates List';
 
            // Creating the CSV file
            String finalstr = 'ROP Id, PlanDates \n';
            String subject = 'HA - Oppty Rollout Month Missing data Upload';
            String attName = 'HARolloutProductsUpdateList.csv';
            
            for(String rop : updatedROPs.keyset()){
                if(updatedROPs.get(rop)!=null){
                    finalstr = finalstr + rop;
                    for(String dt : updatedROPs.get(rop)){
                        finalstr = finalstr +','+dt;
                    }
                    finalstr = finalstr +'\n';
                }
            }
 
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage(); 
     
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName(attName);
            efa.setBody(Blob.valueOf(finalstr));
            
            email.setSubject( subject );
            email.setToAddresses( new String[] {'t.gandru@partner.samsung.com'} );
            email.setPlainTextBody( body );
            email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
 
            Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
        }
    }
}