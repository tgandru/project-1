/**
 * Created by: Thiru
 * Purpose   : To update the Rollout plan for all existing HA Rollouts with New Logic
**/
global class BatchForHARolloutPlanMigration implements database.batchable<sObject>,database.stateful {
    global Map<Date,SCM_Calendar__c> fiscalWeekMap;
    global Map<Date,SCM_Calendar__c> OrderedfiscalWeekMap;
    global integer rolloutCreatedYear;
    global List<integer> rolloutCreatedMonths;
    global integer BatchCount;
    
    global BatchForHARolloutPlanMigration(integer calendarYear, List<integer> calendarMonth){
        rolloutCreatedYear = calendarYear;
        rolloutCreatedMonths = new List<integer>();
        rolloutCreatedMonths = calendarMonth;
        BatchCount = 0;
        
        fiscalWeekMap = new Map<Date,SCM_Calendar__c>();
        OrderedfiscalWeekMap = new Map<Date,SCM_Calendar__c>();
    }
    
    global database.queryLocator start(database.batchableContext bc){
        if(Test.isRunningTest()){
            database.Querylocator ql = database.getquerylocator([select id,
                                                                        SAP_Material_Code__c,
                                                                        Roll_Out_Plan__r.Roll_Out_Start__c,
                                                                        Roll_Out_Plan__r.Rollout_Duration__c,
                                                                        (Select id,Plan_Date__c from Roll_Out_Schedules__r order by Plan_Date__c desc limit 1) 
                                                                 from Roll_Out_Product__c 
                                                                 where Roll_Out_Plan__r.Opportunity__r.Recordtype.Name='HA Builder' limit 10]);
            return ql;
        }else{
            database.Querylocator ql = database.getquerylocator([select id,
                                                                        SAP_Material_Code__c,
                                                                        Roll_Out_Plan__r.Roll_Out_Start__c,
                                                                        Roll_Out_Plan__r.Rollout_Duration__c,
                                                                        (Select id,Plan_Date__c from Roll_Out_Schedules__r order by Plan_Date__c desc limit 1) 
                                                                 from Roll_Out_Product__c 
                                                                 where Roll_Out_Plan__r.Opportunity__r.Recordtype.Name='HA Builder' 
                                                                   and Calendar_Year(Createddate)=:rolloutCreatedYear 
                                                                   and CALENDAR_MONTH(Createddate)=:rolloutCreatedMonths]);
            return ql;
        }
    }
    global void execute(database.batchableContext bc, List<Roll_Out_Product__c> ropList){
        BatchCount++;
        
        List<Roll_Out_Schedule__c> insertROS = new List<Roll_Out_Schedule__c>();
        
        if(BatchCount==1){
            List<Date> sortLst = new List<Date>();
            for(SCM_Calendar__c fc: HA_RosMiscUtilities.lstFiscCal){
                fiscalWeekMap.put(fc.WeekStartDate__c,fc);
            }
            
            sortLst.addAll(fiscalWeekMap.keyset());
            sortLst.sort();
            
            for(Date d:sortLst){
                OrderedfiscalWeekMap.put(d,fiscalWeekMap.get(d));
            }
        }
        
        for(Roll_Out_Product__c rop : ropList){
            if(rop.Roll_Out_Schedules__r.size()>0){
                Set<Date> duplicatePrev = new Set<Date>();
                Roll_Out_Schedule__c ros = rop.Roll_Out_Schedules__r[0];
                integer Duration = (Integer)rop.Roll_Out_Plan__r.Rollout_Duration__c;
                date EndDateCalculated = (rop.Roll_Out_Plan__r.Roll_Out_Start__c).addMonths(Duration);
                Date weekBeginforStartDate = ros.Plan_Date__c.addDays(7);
                Date weekBeginforEndDate = HA_RosMiscUtilities.getWeekBeginDate(EndDateCalculated);
                //system.debug('Product---->'+rop.SAP_Material_Code__c);
                //system.debug('ros plandate---->'+ros.Plan_Date__c);
                //system.debug('weekBeginforStartDate---->'+weekBeginforStartDate);
                //system.debug('weekBeginforEndDate---->'+weekBeginforEndDate);
                if(ros.Plan_Date__c!=weekBeginforEndDate && ros.Plan_Date__c<weekBeginforEndDate){
                    Boolean weekStart = FALSE;
                    Boolean weekEnd = FALSE;
                    
                    for(Date d : OrderedfiscalWeekMap.keyset()){
                        if(d==weekBeginforStartDate && !duplicatePrev.contains(d)){
                            weekStart=TRUE;
                            insertROS.add(new Roll_Out_Schedule__c(
                                                        Roll_Out_Product__c = rop.Id,  
                                                        Plan_Date__c = d, 
                                                        fiscal_week__c =  OrderedfiscalWeekMap.get(d).FiscalWeek__c,
                                                        fiscal_month__c = OrderedfiscalWeekMap.get(d).FiscalMonth__c,
                                                        YearMonth__c = OrderedfiscalWeekMap.get(d).FiscalYear__c + HA_RosMiscUtilities.strMM(OrderedfiscalWeekMap.get(d).FiscalMonth__c),
                                                        year__c= OrderedfiscalWeekMap.get(d).FiscalYear__c,
                                                        Plan_Quantity__c = 0));
                            duplicatePrev.add(d);
                        }
                        if(weekStart == TRUE && weekEnd == FALSE && !duplicatePrev.contains(d)){
                            insertROS.add(new Roll_Out_Schedule__c(
                                                        Roll_Out_Product__c = rop.Id,  
                                                        Plan_Date__c = d, 
                                                        fiscal_week__c =  OrderedfiscalWeekMap.get(d).FiscalWeek__c,
                                                        fiscal_month__c = OrderedfiscalWeekMap.get(d).FiscalMonth__c,
                                                        YearMonth__c = OrderedfiscalWeekMap.get(d).FiscalYear__c + HA_RosMiscUtilities.strMM(OrderedfiscalWeekMap.get(d).FiscalMonth__c),
                                                        year__c= OrderedfiscalWeekMap.get(d).FiscalYear__c,
                                                        Plan_Quantity__c = 0));
                            duplicatePrev.add(d);
                        }
                        if(d == weekBeginforEndDate){
                            system.debug('First date of the week of Roll out plan end date.......'+weekBeginForEndDate);
                            weekEnd = TRUE;
                            break;
                        }
                    }
                }
            }
        }
        try{
            if(insertROS.size()>0) insert insertROS;   
        }
        catch(Exception ex){
            System.debug('##ex inside New HA ROS Insert ::'+ex);
        }
    }
    global void finish(Database.BatchableContext bc){
        
    }
}