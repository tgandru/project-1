/**
 * Created by: Thiru
 * Purpose   : Test Class for BatchForHARolloutPlanMigration class
**/
@isTest
private class BatchForHARolloutPlanMigration_Test {

	@isTest(SeeAllData=true)
	static void CreateTest() {
	    
        Test.startTest();
            List<integer> months = new List<Integer>();
            months.add(1);
            BatchForHARolloutPlanMigration ba = new BatchForHARolloutPlanMigration(2018,months);
            Database.executeBatch(ba);
        Test.stopTest(); 

	}
}