global class BatchForMinuteScheduler implements Database.Batchable<sObject>, Database.AllowsCallouts {

public static String query = 'SELECT MSGSTATUS__c,  Status__c, retry_counter__c,Integration_Flow_Type__c,Object_Name__c,Request_Body__c,Response_Body__c, CreatedBy.Name,CreatedBy.Email, Identification_Text__c, ParentMQ__c,ParentMQ__r.status__c, ERRORTEXT__c FROM message_queue__c'+
                             ' WHERE Status__c in (\'not started\',\'failed-retry\')';
                             // MK 7/8/2016 Removed null Parent MQ filter 
                            //' WHERE Status__c in (\'not started\',\'failed-retry\') and ParentMQ__c=null';

        
    global BatchForMinuteScheduler() {

    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
              
        //@Todo:: more flows to be added                    
        for(message_queue__c mq: (List<message_queue__c>)scope) {
          System.debug('lclc in '+mq.Integration_Flow_Type__c + mq.Id);
          try{
              if(mq.Integration_Flow_Type__c == 'Flow-009_1') {                  
                  CastIronIntegrationHelper.receivePBEEntries(mq);                           

              }
        //      if(mq.Integration_Flow_Type__c == 'Flow-003'){
        //        CastIronIntegrationHelper.processPartnerInserts(mq);
        //      }
              // MK 6-29-2016 No longer used. Commenting out.
              /*if(mq.Integration_Flow_Type__c == 'Flow-008'){
                CreditMemoLinesIntegration integration = new CreditMemoLinesIntegration(mq);
                integration.init();
              }
              */
              if(mq.Integration_Flow_Type__c == 'Flow-010'){
                  creditMemoIntegrationHelper.callCreditMemoEndpoint(mq);
              }
              if(mq.Integration_Flow_Type__c=='submit quote for approval'){
                  CastIronIntegrationHelper.receiveQuoteLineItemsForApproval(mq);
              }
              if(mq.Integration_Flow_Type__c=='008 to 012'){
                  CastIronIntegrationHelper.receiveQuoteLineItemsForApproval(mq);
              }
            if(mq.Integration_Flow_Type__c=='012'){
                
                RoiIntegrationHelper.callRoiEndpoint(mq);
            }

            if(mq.Integration_Flow_Type__c=='003'){
                IntegrationUpdateAccountTriggerHelper.recieveInsertedAccts(mq);
            }

            if(mq.Integration_Flow_Type__c=='004'){
              IntegrationUpdateAccountTriggerHelper.recieveUpdatedAccts(mq);
            }
            
            if(mq.Integration_Flow_Type__c == 'Flow-016'){
                PRMLeadStatusHelper.callRoiEndpoint(mq);
            }
            
            if(mq.Integration_Flow_Type__c == 'Flow-016_1'){
              PRMOpputunityStatusHelper.callprmOppEndpoint(mq);
            }
              
            if(mq.Integration_Flow_Type__c=='invoicing 8'){
              CreditMemoController.fromBatchMinSched(mq);
            }
            /* commented by Thiru - to stop contract creation
            if(mq.Integration_Flow_Type__c=='Flow-050'){
              HABuilderIntegrationHelper.createContract(mq);
            }*/
            
            if(mq.Integration_Flow_Type__c=='Flow-051'){
              HABuilderIntegrationHelper.updateShippedQuantity(mq);
            }
            
            if(mq.Integration_Flow_Type__c=='Flow-030'){
              SelloutQuantityIntegration.SendSelloutQTYData(mq);
            }
            if(mq.Integration_Flow_Type__c=='Flow-031'){
              SelloutAccountIntegration.SendSelloutAccountData(mq);
            }
            
            //Get BO Number for HA Win Oppty from Sales Portal
            if(mq.Integration_Flow_Type__c=='Sales Portal'){
                HAContractIntegrationCall.calloutportal(mq);
            }
            //Get SO Numbers for HA Win Oppty from Sales Portal
            if(mq.Integration_Flow_Type__c=='SO Number'){
                HAbalancedqtyCallout.calloutportal(mq);
            }
            //Get SO Lines for HA Win Oppty from GERP
            if(mq.Integration_Flow_Type__c=='SO Line'){
                HAShippedQuantityUpdate.callout(mq);
            }
            
            if(mq.Integration_Flow_Type__c=='KNOX_Status_Update'){
                KNOXApprovalStatus.getApprovalStatu(mq);
            }
            
             if(mq.Integration_Flow_Type__c=='Flow-040'){
                HARolloutFileTransferIntegration.SendfiletoCIH(mq);
            }
          }
           catch(Exception ex){
               String errmsg = 'MSG='+ex.getmessage() + ' - '+ex.getStackTraceString();
               system.debug('## getmessage'+errmsg);
               if(mq.retry_counter__c<=5){
                mq.status__c = 'failed-retry';
                mq.retry_counter__c += 1;
                mq.ERRORTEXT__c = errmsg;
               }
               else {
                mq.status__c = 'failed';
                mq.retry_counter__c += 1;
                mq.ERRORTEXT__c = errmsg;               
               }
           }
           }
        update scope;
    }
    
    global void finish(Database.BatchableContext BC) {
        // nothing here
    }
}