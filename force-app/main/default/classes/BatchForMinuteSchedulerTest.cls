/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 **/
 
 /**
  *   BatchForMinuteSchedulerTest
  *   @author: Mir Khan
  **/
 
@isTest
private class BatchForMinuteSchedulerTest {

    private static String messageQueueId  = '007';
    private static String messageStatus  = 'Test';
    private static String messageQueueStatus = 'Not Started';
    private static Decimal messageQueueRetryCounter = 1;
    private static String messageQueueIntegrationFlowType = '';
    private static String prmLeadId = '0000';
    private static Integer messageTypeIndex = 0;
    private static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
    
    @testSetup static void setup() {
    	
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Account account = TestDataUtility.createAccount('Marshal Mathers');
         account.SAP_Company_Code__c='99999888';
        insert account;
        Account partnerAcc = TestDataUtility.createAccount('Distributor account');
        partnerAcc.RecordTypeId = directRT;
         partnerAcc.SAP_Company_Code__c='99999808';
        insert partnerAcc;
         //Creating custom Pricebook
        PriceBook2 pb = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(4)); 
        insert pb;
        Opportunity opportunity = TestDataUtility.createOppty('New', account, pb, 'Managed', 'IT', 'product group',
                                    'Identification');
        insert opportunity;
        Product2 product = TestDataUtility.createProduct2('SL-SCF5300/SEG', 'Standalone Printer', 'Printer[C2]', 'H/W', 'FAX/MFP');
        insert product;
        Id pricebookId = Test.getStandardPricebookId();
        //PricebookEntry pricebookEntry1 = TestDataUtility.createPriceBookEntry(product.Id, pricebookId);
        //insert pricebookEntry1;
        ///^^ del bc System.DmlException: Insert failed. First exception on row 0; first error: DUPLICATE_VALUE, This price definition already exists in this price book: []
        PricebookEntry pricebookEntry = TestDataUtility.createPriceBookEntry(product.Id, pb.Id);
        insert pricebookEntry;

        List<OpportunityLineItem> opportunityLineItems = new List<OpportunityLineItem>();
        OpportunityLineItem op1 = TestDataUtility.createOpptyLineItem(product, pricebookEntry, opportunity);
        op1.Quantity = 100;
        op1.TotalPrice = 1000;
        op1.Requested_Price__c = 100;
        op1.Claimed_Quantity__c = 45;     
        opportunityLineItems.add(op1);
        insert opportunityLineItems;
        System.debug('Insert OLI success');

        Quote quot = TestDataUtility.createQuote('Test Quote', opportunity, pb);
        quot.ROI_Requestor_Id__c=UserInfo.getUserId();
        insert quot;
        
        List<QuoteLineItem> qlis = new List<QuoteLineItem>();
        QuoteLineItem standardQLI = TestDataUtility.createQuoteLineItem(quot.Id, product, pricebookEntry, op1);
        standardQLI.Quantity = 25;
        standardQLI.UnitPrice = 100;
        standardQLI.Requested_Price__c=100;
        standardQLI.OLIID__c = op1.id;
        qlis.add(standardQLI);
        insert qlis;


        Credit_Memo__c cm = TestDataUtility.createCreditMemo();
        //cm.Credit_Memo_Number__c = TestDataUtility.generateRandomString(5);
        cm.InvoiceNumber__c = TestDataUtility.generateRandomString(6);
        cm.Direct_Partner__c = partnerAcc.id;
        cm.Division__c = 'Mobile';
        insert cm;
      
        List<Credit_Memo_Product__c> creditMemoProducts = new List<Credit_Memo_Product__c>();
        Credit_Memo_Product__c cmProd = TestDataUtility.createCreditMemoProduct(cm, op1);
        cmProd.Request_Quantity__c = 45;
        creditMemoProducts.add(cmProd);
        insert creditMemoProducts;
   
    }
    
    
	/**
	 * Creating Leads and Message Queues with different
	 * Integration Flow Types
	 **/ 
	static void createMessageQueue() {
	 	
	 	Zipcode_Lookup__c zipCode = TestDataUtility.createZipcode();
    	insert zipCode;
        Lead lead = new Lead(PRM_Lead_Id__c = prmLeadId, LastName = 'Doe', Company = 'Samsung', Street='1 Market St',
                        City='San Francisco', State='CA', Country='US', PostalCode=zipCode.Name);
	 	
        insert lead;
        
        
        message_queue__c mq = new message_queue__c(MSGUID__c = messageQueueId, 
	 												MSGSTATUS__c = messageStatus,  
	 												Status__c = messageQueueStatus, 
	 												retry_counter__c = messageQueueRetryCounter);
	 	
	 	mq.Integration_Flow_Type__c = messageTypeIndex == 1  ? messageQueueIntegrationFlowType = 'Flow-009_1' :
	 	                              messageTypeIndex == 2  ? messageQueueIntegrationFlowType = 'Flow-008' :
	 	                              messageTypeIndex == 3  ? messageQueueIntegrationFlowType = 'Flow-010' :
	 	                              messageTypeIndex == 4  ? messageQueueIntegrationFlowType = 'Flow-008_Quote' :
	 	                              messageTypeIndex == 5  ? messageQueueIntegrationFlowType = '008 to 012' :
	 	                              messageTypeIndex == 6  ? messageQueueIntegrationFlowType = '012' :
	 	                              messageTypeIndex == 7  ? messageQueueIntegrationFlowType = '003' :
	 	                              messageTypeIndex == 8  ? messageQueueIntegrationFlowType = '004' :
	 	                              messageTypeIndex == 9  ? messageQueueIntegrationFlowType = 'Flow-016' :
	 	                              messageTypeIndex == 10 ? messageQueueIntegrationFlowType = 'Flow-016_1' :
	 	                              messageTypeIndex == 11 ? messageQueueIntegrationFlowType = 'invoicing 8' : null;
	 	insert mq;
        
	}
    
    /**
     * Flow-009_1
     **/
    static testMethod void testBatchJob1() {
    	messageTypeIndex = 1;
    	createMessageQueue();
        BatchForMinuteScheduler batchClass;
        
        Test.startTest();
        batchClass = new BatchForMinuteScheduler();
        Database.executeBatch(batchClass);   
        Test.stopTest(); 
         
    }
    
    /**
     * Flow-008
     **/
    static testMethod void testBatchJob2() {
    	messageTypeIndex = 2;
    	prmLeadId = '0001';
    	createMessageQueue();
        BatchForMinuteScheduler batchClass;
        
        Test.startTest();
        batchClass = new BatchForMinuteScheduler();
        Database.executeBatch(batchClass);   
        Test.stopTest(); 
         
    }
    
    /**
     * Flow-010
     * CreditMemoIntegrationHelper
     **/
    static testMethod void testBatchJob3() {
    	messageTypeIndex = 3;
    	prmLeadId = '0002';
    	createMessageQueue();
        BatchForMinuteScheduler batchClass;
        
        Test.startTest();
        batchClass = new BatchForMinuteScheduler();
        Database.executeBatch(batchClass);   
        Test.stopTest(); 
         
    }
    
    /**
     * submit quote for approval
     **/
    static testMethod void testBatchJob4() {
    	messageTypeIndex = 4;
    	prmLeadId = '0003';
    	createMessageQueue();
        BatchForMinuteScheduler batchClass;
        
        Test.startTest();
        batchClass = new BatchForMinuteScheduler();
        Database.executeBatch(batchClass);   
        Test.stopTest(); 
         
    }
    
    /**
     * 008 to 012
     **/
    static testMethod void testBatchJob5() {
    	messageTypeIndex = 5;
    	prmLeadId = '0004';
    	createMessageQueue();
        BatchForMinuteScheduler batchClass;
        
        Test.startTest();
        batchClass = new BatchForMinuteScheduler();
        Database.executeBatch(batchClass);   
        Test.stopTest(); 
         
    }
    
    /**
     * 012
     **/
    static testMethod void testBatchJob6() {
    	messageTypeIndex = 6;
    	prmLeadId = '0005';
    	createMessageQueue();
        BatchForMinuteScheduler batchClass;
        
        Test.startTest();
        batchClass = new BatchForMinuteScheduler();
        Database.executeBatch(batchClass);   
        Test.stopTest(); 
         
    }
    
    /**
     * 003
     **/
    static testMethod void testBatchJob7() {
    	messageTypeIndex = 7;
    	prmLeadId = '0006';
    	createMessageQueue();
        BatchForMinuteScheduler batchClass;
        
        Test.startTest();
        batchClass = new BatchForMinuteScheduler();
        Database.executeBatch(batchClass);   
        Test.stopTest(); 
         
    }
    
    /**
     * 004
     **/
    static testMethod void testBatchJob8() {
    	messageTypeIndex = 8;
    	prmLeadId = '0007';
    	createMessageQueue();
        BatchForMinuteScheduler batchClass;
        
        Test.startTest();
        batchClass = new BatchForMinuteScheduler();
        Database.executeBatch(batchClass);   
        Test.stopTest(); 
         
    }
    
    /**
     * Flow-016
     **/
    static testMethod void testBatchJob9() {
    	messageTypeIndex = 9;
    	prmLeadId = '0008';
    	createMessageQueue();
        BatchForMinuteScheduler batchClass;
        
        Test.startTest();
        batchClass = new BatchForMinuteScheduler();
        Database.executeBatch(batchClass);   
        Test.stopTest(); 
         
    }
    
    /**
     * Flow-016_1
     **/
    static testMethod void testBatchJob10() {
    	messageTypeIndex = 10;
    	prmLeadId = '0009';
    	createMessageQueue();
        BatchForMinuteScheduler batchClass;
        
        Test.startTest();
        batchClass = new BatchForMinuteScheduler();
        Database.executeBatch(batchClass);   
        Test.stopTest(); 
         
    }
    
    /**
     * invoicing 8
     **/
    static testMethod void testBatchJob11() {
    	messageTypeIndex = 11;
    	prmLeadId = '00010';
    	createMessageQueue();
        BatchForMinuteScheduler batchClass;
        
        Test.startTest();
        batchClass = new BatchForMinuteScheduler();
        Database.executeBatch(batchClass);   
        Test.stopTest(); 
         
    }
    
}