// This batch is to Auto Drop the Opportunities after 30 days of its closeDate

global class BatchForOpptyAutoDrop implements database.batchable<sObject>,database.stateful{
    
    global Map<Id, String> errorMap;
    global Map<Id, SObject> IdToSObjectMap;
    global integer updateSize=0;
    global string success;
    global date closedt;
    
    global BatchForOpptyAutoDrop(){
        success = 'Opportunityid, Opportunity Name, Closedate, Owner, Createddate \n';
        errorMap = new Map<Id, String>();
        IdToSObjectMap = new Map<Id, SObject>();
        date dt = system.today();
        closedt = dt-30;
    }
    
    global database.querylocator start(database.batchablecontext bc){
       //database.Querylocator ql = database.getquerylocator([select id, Owner.name, Ownerid, Owner.isactive, closedate, stagename, Drop_Reason__c, Drop_Date__c, Division__c from opportunity where Division__c!='Mobile'and recordtype.name!='HA Builder' and closedate=LAST_N_DAYS:365 and (stagename='Identification' or stagename='Qualification' or stagename='Proposal')]);
       database.Querylocator ql = database.getquerylocator([select id, name, Owner.name, Ownerid, Owner.isactive, closedate, stagename, Drop_Reason__c, Drop_Date__c, Division__c, createddate from opportunity where Division__c!='Mobile'and recordtype.name!='HA Builder' and closedate<=:closedt and (stagename='Identified' or stagename='Qualified' or stagename='Proposal')]);
       return ql;
    }
    
    global void execute(database.batchablecontext bc, list<Opportunity> opplist){
        system.debug('-----Opp list-----'+opplist);
        date dt = system.today();
        list<Opportunity> UpdatedOpps = new List<Opportunity>();
        id Seab2bid = [select id,name from user where name='sea b2b'].id;
        for(Opportunity opp : opplist){
           if(opp.Closedate <= dt-30){
                if(opp.owner.isactive == False){
                    opp.stagename='Drop';
                    opp.Drop_Reason__c='Auto Drop';
                    if(opp.Closedate == dt-30){
                        opp.Drop_Date__c= date.today();
                    }else{
                        opp.Drop_Date__c= opp.closedate+30;
                    }
                    opp.ownerid=Seab2bid;
                    UpdatedOpps.add(opp);
                }
                Else{
                    opp.stagename='Drop';
                    opp.Drop_Reason__c='Auto Drop';
                    if(opp.Closedate == dt-30){
                        opp.Drop_Date__c= date.today();
                    }else{
                        opp.Drop_Date__c= opp.closedate+30;
                    }
                    UpdatedOpps.add(opp);
                }
           }
        }
        system.debug('Updated Opps list ---------:'+UpdatedOpps.size());
        updateSize = updateSize + UpdatedOpps.size();
        Database.SaveResult[] srList = database.update(UpdatedOpps,false);
        Integer index = 0;
        set<id> oppids = new set<id>();
        for (Database.SaveResult sr : srList) {
            if (!sr.isSuccess()) {
                // Operation failed, so get all errors               
                String errMsg = sr.getErrors()[0].getMessage();
                errorMap.put(UpdatedOpps[index].Id, errMsg);
                IdToSObjectMap.put(UpdatedOpps[index].Id, UpdatedOpps[index]);
            }else{
                string str = sr.getid();
                oppids.add(sr.getid());
                //success = success + str + '\n';
            }
            index++;
        }
        for(opportunity opp : [select id, name, Owner.name, Ownerid, closedate, stagename, Division__c, createddate from opportunity where id=:oppids]){
            string str = opp.id+','+(opp.name).replace(',','')+','+opp.closedate+','+opp.Owner.name+','+opp.createddate+'\n';
            success = success + str;
        }

    }
    
    global void finish(database.batchablecontext bc){
        system.debug('updateSize----------->'+updateSize);
        bc.getjobid();
        //if(!errorMap.isEmpty()){
        if(!string.isEmpty(success)){
            AsyncApexJob a = [SELECT id, ApexClassId,
                       JobItemsProcessed, TotalJobItems,
                       NumberOfErrors, CreatedBy.Email
                       FROM AsyncApexJob
                       WHERE id = :BC.getJobId()];
            String body = 'Your batch job '+ '"BatchForOpptyAutoDrop" '+ 'has finished. \n' + 'There were '+ errorMap.size()+ ' errors. Please find the list attached';
 
            // Creating the CSV file
            String finalstr = 'Id, Error \n';
            String subject = 'Oppty Auto Drop - Apex Batch execute Success and Error List';
            String attName = 'ErrorRecords.csv';
            for(Id id  : errorMap.keySet()){
                string err = errorMap.get(id);
                Opportunity opp = (Opportunity) IdToSObjectMap.get(id);
                string recordString = '"'+id+'","'+err+'"\n';
                finalstr = finalstr +recordString;
            } 
 
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage(); 
     
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName(attName);
            efa.setBody(Blob.valueOf(finalstr));
            
            Messaging.EmailFileAttachment efa2 = new Messaging.EmailFileAttachment();
            efa2.setFileName('SuccessRecords.csv');
            efa2.setBody(Blob.valueOf(success));
            
            email.setSubject( subject );
            email.setToAddresses( new String[] {'t.gandru@partner.samsung.com','v.kamani@partner.samsung.com','stevepark@samsung.com','p.kabalai@partner.samsung.com'} );
            email.setPlainTextBody( body );
            email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa,efa2});
 
            Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});   
        }
        
    }
}