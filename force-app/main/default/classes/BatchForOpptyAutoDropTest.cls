@istest
Private class BatchForOpptyAutoDropTest{
    static Id AccEndUserRT = TestDataUtility.retrieveRecordTypeId('End_User', 'Account');
    static Id OppITRT = TestDataUtility.retrieveRecordTypeId('IT','Opportunity');
    public static id oppid;
     @testSetup static void setup() {
         Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
         List<Opportunity> Opplist = new List<Opportunity>();
         
         //Insert Account
         Account acc = TestDataUtility.createAccount('TestingPartner 1');
         acc.Recordtypeid = AccEndUserRT;
         acc.Type = 'Customer';
         insert acc;
         
         Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
         id Uid =[select id,name from user where name='Sarah Williamson'].id;
         id Uid2 =[select id,name from user where profile.name='System Administrator' and isactive=true limit 1].id;
         
         date mydate = date.parse('10/10/2017');
         //Insert Opportunities
         Opportunity opp_IT = new Opportunity(Name = 'Test Opp1',
                                               AccountId =acc.Id,
                                               Ownerid=Uid,
                                               RecordTypeId = OppITRT ,
                                               Type = 'Tender',
                                               Description = 'Test Opp IT',
                                               Division__c = 'IT',
                                               Amount = 99,
                                               CloseDate = mydate,
                                               ProductGroupTest__c = 'PRINTER',
                                               LeadSource = 'External_Tradeshow',
                                               Reference__c = 'Test Name',
                                               stageName = 'Identified'); 
         Opplist.add(opp_IT); 
         Opportunity opp_IT1 = new Opportunity(Name = 'Test Opp2',
                                               AccountId =acc.Id,
                                               Ownerid=Uid2,
                                               RecordTypeId = OppITRT ,
                                               Type = 'Tender',
                                               Description = 'Test Opp IT',
                                               Division__c = 'IT',
                                               Amount = 999,
                                               CloseDate = Date.valueOf(System.Today()),
                                               ProductGroupTest__c = 'LCD_MONITOR',
                                               LeadSource = 'External_Tradeshow',
                                               Reference__c = 'Test Name',
                                               stageName = 'Identified'); 
         Opplist.add(opp_IT1);
         insert Opplist;
         oppid = opp_IT1.id;
     } 
   
 @isTest static void OppsToProcessInBatch(){
 Test.startTest();
     List<Opportunity> Opplist = [select id, Ownerid, name, closedate, stagename, Drop_Reason__c, Drop_Date__c, Division__c from Opportunity where Division__c!='Mobile' and closedate=LAST_N_DAYS:365 and stagename='Identified'];
     BatchForOpptyAutoDrop batch = new BatchForOpptyAutoDrop(); 
     Database.executeBatch(batch);
 Test.stopTest();    
 }        
}