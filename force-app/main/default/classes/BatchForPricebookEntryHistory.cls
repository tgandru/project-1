/*
This batch is to insert the pricebookHistory object records.
For each Pricebookentry new pricebookHistory reord is created.
If there is change in List Price of PricebookEntry, instead of updating, new pricebookHistory record is created with the updated Pricebookentry details to keep the old pricebookentry details in the PricebookHistory.
*/

global class BatchForPricebookEntryHistory implements database.batchable<sObject>, Schedulable {
    global void execute(SchedulableContext SC) {
      BatchForPricebookEntryHistory ba = new BatchForPricebookEntryHistory();
      database.executebatch(ba);
    }
    
    global database.querylocator start(database.batchablecontext bc){
       database.Querylocator ql = database.getquerylocator([select id,name,pricebook2.name,product2.SAP_Material_ID__c,product2.Product_Group__c,product2.recordtype.name,unitprice,lastmodifieddate from pricebookentry]);
       return ql; 
    }
    
    global void execute(database.batchablecontext bc, list<pricebookentry> scope){
        Set<id> pbentryids = new Set<id>();
        for(PricebookEntry pbe : scope){
            pbentryids.add(pbe.id);
        }
        List<PriceBookEntry__c> customPBEs = [select id,PriceBookEntryID__c,Product__c,Pricebook__c,List_Price__c,PBE_Last_Modified_Date__c,Product_Group__c,Product_Record_Type__c from PriceBookEntry__c where PriceBookEntryID__c=:pbentryids];
        Set<string> customPBEids = new Set<string>();
        Set<string> PBEids = new Set<string>();

        List<PriceBookEntry__c> insertCustomPBEs = new List<PriceBookEntry__c>();
        for(pricebookentry pbe : scope){
            PBEids.add(pbe.id);
            for(PriceBookEntry__c cpbe : customPBEs){
                customPBEids.add(cpbe.PriceBookEntryID__c);
                if(pbe.id==cpbe.PriceBookEntryID__c){
                    if(pbe.unitprice!=cpbe.List_Price__c){
                        PriceBookEntry__c newCPB =new PriceBookEntry__c();
                        newCPB.PriceBookEntryID__c = pbe.id;
                        newCPB.Product__c= pbe.product2.SAP_Material_ID__c;
                        newCPB.Pricebook__c= pbe.pricebook2.name;
                        newCPB.List_Price__c= pbe.unitprice;
                        newCPB.PBE_Last_Modified_Date__c = pbe.lastmodifieddate;
                        newCPB.Product_Group__c = pbe.product2.Product_Group__c;
                        newCPB.Product_Record_Type__c = pbe.product2.recordtype.name;
                        insertCustomPBEs.add(newCPB);
                    }
                }
            }
        }
        system.debug('updated pbes to insert in PBEhistory-------->'+insertCustomPBEs.size());
        Set<id> insertPBEids = new Set<id>(); 
        for(string idstr : PBEids){
            if(!(customPBEids.contains(idstr))){
                insertPBEids.add(idstr);
            }
        }
        system.debug('New PBEs------->'+insertPBEids.size());
        system.debug('New PBEs------->'+insertPBEids);
        for(pricebookentry pbe : [select id,name,pricebook2.name,product2.SAP_Material_ID__c,unitprice,lastmodifieddate,product2.Product_Group__c,product2.recordtype.name from pricebookentry where id=:insertPBEids]){
            PriceBookEntry__c newCPB =new PriceBookEntry__c();
            newCPB.PriceBookEntryID__c = pbe.id;
            newCPB.Product__c= pbe.product2.SAP_Material_ID__c;
            newCPB.Pricebook__c= pbe.pricebook2.name;
            newCPB.List_Price__c= pbe.unitprice;
            newCPB.PBE_Last_Modified_Date__c = pbe.lastmodifieddate;
            newCPB.Product_Group__c = pbe.product2.Product_Group__c;
            newCPB.Product_Record_Type__c = pbe.product2.recordtype.name;
            insertCustomPBEs.add(newCPB);
        }
        if(insertCustomPBEs.size()>0){
            insert insertCustomPBEs;
        }
        
    }
    
    global void finish(database.batchablecontext bc){
        bc.getjobid();
    }
}