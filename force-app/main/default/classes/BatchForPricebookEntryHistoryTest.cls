@istest
Private class BatchForPricebookEntryHistoryTest{
    @testSetup static void setup() {

        PriceBook2 pb=TestDataUtility.createPriceBook('Test PB');
        insert pb;

        /* Creating Products: SAP_Material_Id__C should be unique and case sensitive ID
        Used the utility method to generate random string for the SAP_Material_ID as it should be unique 
        when every time you passed on
        */

        List<Product2> prods=new List<Product2>();
        Product2 standaloneProd=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Standalone Prod1', 'Printer[C2]', 'FAX/MFP', 'H/W');
        prods.add(standaloneProd);
        Product2 parentProd1=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Parent Prod1', 'Printer[C2]', 'FAX/MFP', 'H/W');
        prods.add(parentProd1);
        Product2 childProd1=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod1', 'Printer[C2]', 'FAX/MFP', 'Service pack');
        prods.add(childProd1);
        Product2 childProd2=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod2', 'Printer[C2]', 'FAX/MFP', 'Service pack');
        prods.add(childProd2);
        Product2 parentProd2=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Parent Prod2', 'Printer[C2]', 'FAX/MFP', 'H/W');
        prods.add(parentProd2);
        Product2 childProd3=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod3', 'Printer[C2]', 'FAX/MFP', 'Service pack');
        prods.add(childProd3);
        Product2 childProd4=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod4', 'Printer[C2]', 'FAX/MFP', 'Service pack');
        prods.add(childProd4);
        Product2 childProd5Standalone=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12), 'child AND standalone prod5', 'Printer[C2]', 'FAX/MFP', 'Service pack');
        prods.add(childProd5Standalone);
        Product2 LCDProduct=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12), 'LCD Product', 'MONITOR [C1]', 'LCD_MONITOR', 'Service pack');
        prods.add(LCDProduct);
        insert prods;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        //create the pricebook entries for the custom pricebook
        List<PriceBookEntry> custompbes=new List<PriceBookEntry>();
        PriceBookEntry standaloneProdPBE=TestDataUtility.createPriceBookEntry(standaloneProd.Id, pb.Id);
        custompbes.add(standaloneProdPBE);
        PriceBookEntry parentProd1PBE=TestDataUtility.createPriceBookEntry(parentProd1.Id, pb.Id);
        custompbes.add(parentProd1PBE);
        PriceBookEntry childProd1PBE=TestDataUtility.createPriceBookEntry(childProd1.Id, pb.Id);
        custompbes.add(childProd1PBE);
        PriceBookEntry childProd2PBE=TestDataUtility.createPriceBookEntry(childProd2.Id, pb.Id);
        custompbes.add(childProd2PBE);
        PriceBookEntry parentProd2PBE=TestDataUtility.createPriceBookEntry(parentProd2.Id, pb.Id);
        custompbes.add(parentProd2PBE);
        PriceBookEntry childProd3PBE=TestDataUtility.createPriceBookEntry(childProd3.Id, pb.Id);
        custompbes.add(childProd3PBE);
        PriceBookEntry childProd4PBE=TestDataUtility.createPriceBookEntry(childProd4.Id, pb.Id);
        custompbes.add(childProd4PBE);
        PriceBookEntry MonitorPBE=TestDataUtility.createPriceBookEntry(LCDProduct.Id, pb.Id);
        custompbes.add(MonitorPBE);
        insert custompbes;
        
        PriceBookEntry__c newCPB = new PriceBookEntry__c();
        newCPB.PriceBookEntryID__c = standaloneProdPBE.id;
        newCPB.Product__c= standaloneProdPBE.product2.SAP_Material_ID__c;
        newCPB.Pricebook__c= standaloneProdPBE.pricebook2.name;
        newCPB.List_Price__c= 10;
        newCPB.PBE_Last_Modified_Date__c = standaloneProdPBE.lastmodifieddate;
        newCPB.Product_Group__c = standaloneProdPBE.product2.Product_Group__c;
        newCPB.Product_Record_Type__c = standaloneProdPBE.product2.recordtype.name;
        insert newCPB;
    }
    @isTest static void RecordsToProcessInBatch(){
        Test.startTest();
         List<Pricebookentry> pbelist = [select id,name,pricebook2.name,product2.SAP_Material_ID__c,product2.Product_Group__c,product2.recordtype.name,unitprice,lastmodifieddate from pricebookentry];
         
         BatchForPricebookEntryHistory batch = new BatchForPricebookEntryHistory(); 
         Database.executeBatch(batch);
        Test.stopTest();
    }        
}