@istest(seealldata = true)
Private class BatchForPrinterTest{
    @isTest static void OppsToProcessInBatch(){
        Test.startTest();
         BatchforCSV2 batch = new BatchforCSV2(3,'t.gandru@partner.samsung.com',true); 
         Database.executeBatch(batch,50);
         BatchforCSV2 batch_1 = new BatchforCSV2(3,'t.gandru@partner.samsung.com',false); 
         Database.executeBatch(batch_1,50);
         BatchforCSV2_V2 batch3 = new BatchforCSV2_V2(3,'t.gandru@partner.samsung.com',true); 
         Database.executeBatch(batch3,50);
         BatchforCSV2_V2 batch3_1 = new BatchforCSV2_V2(3,'t.gandru@partner.samsung.com',false); 
         Database.executeBatch(batch3_1,50);
        Test.stopTest();    
    }        
    @isTest static void OppsToProcessInBatch2(){
        Test.startTest();
         BatchforCSV3 batchcsv3 = new BatchforCSV3(3,'t.gandru@partner.samsung.com'); 
         Database.executeBatch(batchcsv3,50);
         BatchforPRTattachments ba = new BatchforPRTattachments(3,'t.gandru@partner.samsung.com');
         database.executeBatch(ba,50);
         BatchforPRTQuotePDFs ba2 = new BatchforPRTQuotePDFs(3,'t.gandru@partner.samsung.com');
         database.executeBatch(ba2,50);
        Test.stopTest(); 
 }
}