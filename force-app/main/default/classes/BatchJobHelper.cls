/************************************* MODIFICATION LOG ********************************************************************************************
* BatchJobHelper
*
* DESCRIPTION : Contains helper methods to determine if a batch job can be started 
*               based on the total number of batches running and other 
*               individual batch specific criteria
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* Public Method Name            Inputs                                  Description
*---------------------------------------------------------------------------------------------------------------------------------------------------
* canThisBatchRun               Batch class name                        Determines whether its safe or
*                                                                       appropriate to run a new batch process
*                                                                       and returns true or false
*-----------------------------------------------------------------------------------------------------------------------------
*
*                                                   
*/

public class BatchJobHelper {
    //active statuses indicating that a batch job is running.
    private static final Set<String> ActiveBatchStatuses = new Set<String> { 'Queued', 'Preparing', 'Processing','Holding' };
    
    public static ID priceBookEntryId {
        get {
            if(priceBookEntryId == null) {
                return [Select Id From PriceBook2 Where isStandard=true And isActive= true LIMIT 1].Id;
            }
            
            return priceBookEntryId;
        }
    }
    
    //Batch class names
    //Example below - change later
    public static final String webserviceBatch = 'BatchForMinuteScheduler';
    
    //Variable to hold a list of batch classes that are being processed
    public static List<AsyncApexJob> activeAsyncJobs = new List<AsyncApexJob>();
    
    public static Boolean canThisBatchRun(String BatchClassName)
    {
        if (!Test.isRunningTest()){
            activeAsyncJobs = [Select id, Status, ApexClass.Name from AsyncApexJob where Status in : ActiveBatchStatuses];
        }
        //activeAsyncJobs = [Select id, Status, ApexClass.Name from AsyncApexJob where Status in : ActiveBatchStatuses and JobType != 'BatchApexWorker'];
        //Bala - Changed the active size from 4 to 50, to use apex flex queue
        system.debug('activeAsyncJobs size ' + activeAsyncJobs.size());
        if(activeAsyncJobs.size() > 50)
        {
            //do not run any additional batch jobs to avoid a governer limit error being thrown
            return false;
        }
        else
        {
            //run this check for specific batch class
            return canThisBatchRunForClass(BatchClassName);
        }
    }
    
    private static Boolean canThisBatchRunForClass(String BatchClassName)
    {
        Boolean retVar = false;
        //Add more Or conditions as needed
        if(BatchClassName == webserviceBatch)
        {
            retVar = !isThisBatchCurrentlyRunning(BatchClassName);
        }
        //If there are other conditions like queue status that drive
        //the decision on a batch running, add them here and change retVar variable
        
        
        return retVar;
    }

    private static Boolean isThisBatchCurrentlyRunning(String BatchClassName)
    {
        Boolean retVar = false;
        for(AsyncApexJob AJ : activeAsyncJobs)
        {
            if(AJ.ApexClass.Name == BatchClassName)
            {
                retVar = true;
            }
        }
        return retVar;
    }
    
}