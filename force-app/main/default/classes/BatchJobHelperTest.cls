@isTest
private class BatchJobHelperTest {
	static Pricebook2 pb;
	
    private static void setup() {
    	//Jagan: Test data for fiscalCalendar__c
        Test.loadData(fiscalCalendar__c.sObjectType, 'FiscalCalendarTestData');
        
        //Channelinsight configuration
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();

        Zipcode_Lookup__c zip=TestDataUtility.createZipcode();
        insert zip;
        
        //Creating custom Pricebook
        pb = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(4)); 
        pb.isActive= true;
        insert pb;
        
        Pricebook2 pb2 = new Pricebook2();
        pb2.Id = Test.getStandardPricebookId();
        pb2.isActive= true;
        update pb2;
    }
    
    @isTest static void methodTest1() {
    	setup();
    	Test.StartTest();
    		BatchJobHelper.canThisBatchRun(BatchJobHelper.webserviceBatch);
    	Test.StopTest();
    	System.assertNotEquals(null,BatchJobHelper.priceBookEntryId);
    }
    
    @isTest static void methodTest2() {
    	setup();
    	Test.StartTest();
    		BatchJobHelper.priceBookEntryId = Test.getStandardPricebookId();
    	Test.StopTest();
    	System.assertNotEquals(null,BatchJobHelper.priceBookEntryId);
    }
}