global class Batch_AccountDataToSECPI_Delete implements database.batchable<sObject>{
    
    global database.querylocator start(database.batchablecontext bc){
       database.Querylocator ql = database.getquerylocator([select id from Data_Send_to_SECPI__c]);
       return ql;
    }
    
    global void execute(database.batchablecontext bc, list<Data_Send_to_SECPI__c> pagelist){
        Delete pagelist;
    }
    
    global void finish(database.batchablecontext bc){
    
    }
}