@istest
public class Batch_Claimed_Qty_Update_Test {
	static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
	
	@isTest static void Batch_QLIOLIClaimQtyUpdate_AfterLiveTest() {

    		Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
            Account account = TestDataUtility.createAccount('Marshal Mathers');
            insert account;
            Account partnerAcc = TestDataUtility.createAccount('Distributor account');
            partnerAcc.RecordTypeId = directRT;
            insert partnerAcc;
            PriceBook2 priceBook = TestDataUtility.createPriceBook('Shady Records');
            insert pricebook;
            Opportunity opportunity = TestDataUtility.createOppty('New', account, pricebook, 'Managed', 'IT', 'product group',
                                        'Identified');
            insert opportunity;
            Product2 product = TestDataUtility.createProduct2('SL-SCF5300/SEG', 'Standalone Printer', 'Printer[C2]', 'H/W', 'FAX/MFP');
            insert product;
            
            PricebookEntry pricebookEntry = TestDataUtility.createPriceBookEntry(product.Id, priceBook.Id);
            insert pricebookEntry;
    
            List<OpportunityLineItem> opportunityLineItems = new List<OpportunityLineItem>();
            OpportunityLineItem op1 = TestDataUtility.createOpptyLineItem(product, pricebookEntry, opportunity);
            op1.Quantity = 100;
            op1.TotalPrice = 1000;
            op1.Requested_Price__c = 100;
            op1.Claimed_Quantity__c = 45;     
            opportunityLineItems.add(op1);
            insert opportunityLineItems;
    
            Quote quot = TestDataUtility.createQuote('Test Quote', opportunity, pricebook);
            quot.ROI_Requestor_Id__c=UserInfo.getUserId();
            insert quot;
            
            List<QuoteLineItem> qlis = new List<QuoteLineItem>();
            QuoteLineItem standardQLI = TestDataUtility.createQuoteLineItem(quot.Id, product, pricebookEntry, op1);
            standardQLI.Quantity = 25;
            standardQLI.UnitPrice = 100;
            standardQLI.Requested_Price__c=100;
            standardQLI.OLIID__c = op1.id;
            qlis.add(standardQLI);
            insert qlis;
    
    
            Credit_Memo__c cm = TestDataUtility.createCreditMemo();
            cm.InvoiceNumber__c = TestDataUtility.generateRandomString(6);
            cm.Direct_Partner__c = partnerAcc.id;
            insert cm;
          
            List<Credit_Memo_Product__c> creditMemoProducts = new List<Credit_Memo_Product__c>();
            Credit_Memo_Product__c cmProd = TestDataUtility.createCreditMemoProduct(cm, op1);
            cmProd.Request_Quantity__c = 60;
            cmProd.Qli_id__c = standardQLI.id;
            creditMemoProducts.add(cmProd);
            insert creditMemoProducts;
            
            Credit_Memo__c cmemo = [select id,status__c from Credit_Memo__c where id=:cm.id];
            cmemo.status__c = 'Processed';
            update cmemo;
            
            Batch_QLI_OLI_ClaimQty_Update_AfterLive ba = new Batch_QLI_OLI_ClaimQty_Update_AfterLive();
            database.executeBatch(ba);
		
		}
		
	@isTest static void Batch_QLIOLIClaimQtyUpdate_BeforeLiveTest() {
		// Implement test code
            user MigUser = [select id from user where profile.name='MigrationProfile' and isactive=true limit 1];
            system.runAs(MigUser){
        		Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
                Account account = TestDataUtility.createAccount('Marshal Mathers');
                insert account;
                Account partnerAcc = TestDataUtility.createAccount('Distributor account');
                partnerAcc.RecordTypeId = directRT;
                insert partnerAcc;
                PriceBook2 priceBook = TestDataUtility.createPriceBook('Shady Records');
                insert pricebook;
                Opportunity opportunity = TestDataUtility.createOppty('New', account, pricebook, 'Managed', 'IT', 'product group',
                                            'Identified');
                insert opportunity;
                Product2 product = TestDataUtility.createProduct2('SL-SCF5300/SEG', 'Standalone Printer', 'Printer[C2]', 'H/W', 'FAX/MFP');
                insert product;
                
                PricebookEntry pricebookEntry = TestDataUtility.createPriceBookEntry(product.Id, priceBook.Id);
                insert pricebookEntry;
                
                string Olicreateddttm = '20160716000000';
                List<OpportunityLineItem> opportunityLineItems = new List<OpportunityLineItem>();
                OpportunityLineItem op1 = TestDataUtility.createOpptyLineItem(product, pricebookEntry, opportunity);
                op1.Quantity = 100;
                op1.TotalPrice = 1000;
                op1.Requested_Price__c = 100;
                op1.Claimed_Quantity__c = 45;
                opportunityLineItems.add(op1);
                insert opportunityLineItems;
                
                Test.setCreatedDate(op1.Id, DateTime.newInstance(2016,07,16));
        
                Quote quot = TestDataUtility.createQuote('Test Quote', opportunity, pricebook);
                quot.ROI_Requestor_Id__c=UserInfo.getUserId();
                insert quot;
                
                string Qlicreateddttm = '20160716040000';
                List<QuoteLineItem> qlis = new List<QuoteLineItem>();
                QuoteLineItem standardQLI = TestDataUtility.createQuoteLineItem(quot.Id, product, pricebookEntry, op1);
                standardQLI.Quantity = 25;
                standardQLI.UnitPrice = 100;
                standardQLI.Requested_Price__c=100;
                standardQLI.OLIID__c = op1.id;
                qlis.add(standardQLI);
                insert qlis;
                
                Test.setCreatedDate(standardQLI.Id, DateTime.newInstance(2016,07,16,4,0,0));
        
        
                Credit_Memo__c cm = TestDataUtility.createCreditMemo();
                cm.InvoiceNumber__c = TestDataUtility.generateRandomString(6);
                cm.Direct_Partner__c = partnerAcc.id;
                insert cm;
              
                List<Credit_Memo_Product__c> creditMemoProducts = new List<Credit_Memo_Product__c>();
                Credit_Memo_Product__c cmProd = TestDataUtility.createCreditMemoProduct(cm, op1);
                cmProd.Request_Quantity__c = 60;
                cmProd.Qli_id__c = standardQLI.id;
                creditMemoProducts.add(cmProd);
                insert creditMemoProducts;
                
                Credit_Memo__c cmemo = [select id,status__c from Credit_Memo__c where id=:cm.id];
                cmemo.status__c = 'Processed';
                update cmemo;
                /*for(integer i=0;i<10000000;i++){
                    string idstring = 'string'+i;
                    id tempid = (id)idstring;
                    Batch_QLI_OLI_ClaimQty_Update_BeforeLive.oppliUpdateMap.put(tempid,idstring);
                }*/
                
                Batch_QLI_OLI_ClaimQty_Update_BeforeLive ba = new Batch_QLI_OLI_ClaimQty_Update_BeforeLive(10);
                database.executeBatch(ba);
            }
		
		}
}