global class Batch_DeletedDataToSECPI_Delete implements database.batchable<sObject>{
    
    global database.querylocator start(database.batchablecontext bc){
       database.Querylocator ql = database.getquerylocator([select id from Deleted_Data_To_SECPI__c]);
       return ql;
    }
    
    global void execute(database.batchablecontext bc, list<Deleted_Data_To_SECPI__c> pagelist){
        Delete pagelist;
    }
    
    global void finish(database.batchablecontext bc){
    
    }
}