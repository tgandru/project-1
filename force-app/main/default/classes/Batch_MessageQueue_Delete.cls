global class Batch_MessageQueue_Delete implements database.batchable<sObject>,Schedulable{
    
    global void execute(SchedulableContext SC) {
        Batch_MessageQueue_Delete ba = new Batch_MessageQueue_Delete();
        database.executeBatch(ba);
    }
    
    global database.queryLocator start(database.batchablecontext bc){
        return database.getQueryLocator([select id from message_queue__c where createddate<LAST_N_DAYS:90 order by createddate asc]);
    }
    
    global void execute(database.batchablecontext bc, List<message_queue__c> DeleteList){
        delete DeleteList;
    }
    
    global void finish(database.batchablecontext bc){
        
    }
    
}