global class Batch_MigrateAccountContactRole implements Database.Batchable<sObject>{ 
 
    global Batch_MigrateAccountContactRole() {
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([SELECT AccountId,ContactId,CreatedById,CreatedDate,Id,IsDeleted,IsPrimary,LastModifiedById,LastModifiedDate,Role FROM AccountContactRole]); // Query All Account Contact Roles
    }
    
    global void execute(Database.BatchableContext BC, List<AccountContactRole> scope){
        
        Set<id> Accountids=new set<Id>();
        
        for(AccountContactRole ac:scope){
            Accountids.add(ac.AccountId);
        }
        
        List<AccountContactRelation> upsertlist=new List<AccountContactRelation>();
        Map<String,AccountContactRelation> upsertmap=new   Map<String,AccountContactRelation>();
        Map<String,AccountContactRelation> acccontactrelationMap=new Map<String,AccountContactRelation>();
        if(Accountids.size()>0){
            for(AccountContactRelation accrel:[SELECT AccountId,ContactId,CreatedById,CreatedDate,EndDate,Id,IsActive,IsDeleted,IsDirect,LastModifiedById,LastModifiedDate,Roles,StartDate FROM AccountContactRelation where AccountID in:AccountIds]){
                String key=accrel.AccountId+'-'+accrel.ContactId;
                acccontactrelationMap.put(key,accrel);
            }
            
        }
        
        
        for(AccountContactRole ac:scope){
            string key=ac.AccountId+'-'+ac.ContactId;
            
            if(acccontactrelationMap.containsKey(key)){
                AccountContactRelation rel=acccontactrelationMap.get(key);
                rel.Roles=ac.Role;
                rel.IsActive = true;
                upsertlist.add(rel);
                upsertmap.put(key,rel);
            }else{
               AccountContactRelation acRelationNew = new AccountContactRelation();
                                acRelationNew.AccountId = ac.accountid;
                                acRelationNew.contactid = ac.contactid;
                                acRelationNew.IsActive = true;
                                acRelationNew.Roles = ac.Role; 
                                upsertlist.add(acRelationNew);
                                 upsertmap.put(key,acRelationNew);
            }
            
        }
        
        
        
        if(upsertmap.size()>0){
            database.upsert(upsertmap.values(),false);
        }
        
          
    /*    
        

        Map<Id, Account> accMap = new Map<Id, Account>(scope);
        
        Map<Id, AccountContactRole> acRoleMap = new Map<Id, AccountContactRole> ([SELECT Id, AccountId, ContactId, Role FROM AccountContactRole WHERE accountId in: accMap.keySet()]); 
        
        Map<Id, AccountContactRelation> acRelationMap = new Map<Id,AccountContactRelation>([SELECT Id, AccountId, ContactId, IsActive FROM AccountContactRelation WHERE accountId in: accMap.keySet()]);
        
        for(Account acct: accMap.values()){
           
            acRelationUpdateMap = new Map<Id,AccountContactRelation>();
            acRelationInsertMap = new Map<Id,AccountContactRelation>();
            
            Map<Id,String> contactMap = new Map<Id,String>();
            
            for(AccountContactRole acRole:acRoleMap.values()) {
                if(acRole.accountId == acct.Id) {
                    for(AccountContactRelation acRelation : acRelationMap.values()){
                        if(acRelation.accountId == acct.Id) {
                            if(acRole.contactId == acRelation.contactId) {
                                contactMap.put(acRole.contactId,'1');
                            }
                        }
                    }
                }
            }
            
            System.debug('acRoleMap.size():  ' + acRoleMap.size());
            System.debug('acRelationMap.size():  ' + acRelationMap.size());
            System.debug('contactMap.size():  ' + contactMap.size());
            
            for(AccountContactRole acRole:acRoleMap.values()) {
                if(acRole.accountId == acct.Id) {
                    for(AccountContactRelation acRelation : acRelationMap.values()){
                        if(acRelation.accountId == acct.Id) {
                            if(!contactMap.containsKey(acRole.contactId)) {
                                contactMap.put(acRole.contactId,'1');
                                AccountContactRelation acRelationNew = new AccountContactRelation();
                                acRelationNew.AccountId = acRole.accountid;
                                acRelationNew.contactid = acRole.contactid;
                                acRelationNew.IsActive = true;
                                //    acRelation.IsDeleted = acr.IsDeleted; This field is not writeable...
                                acRelationNew.Roles = acRole.Role;
                                acRelationInsertList.add(acRelationNew);
                            } 
                            else {
                                if(!acRelationUpdateMap.containsKey(acRelation.Id)){
                                    acRelation.IsActive = true;
                                    acRelation.Roles = acRole.Role;
                                    acRelationUpdateMap.put(acRelation.Id, acRelation);
                                }
                            }
                        }
                    }
                }
            }
        }
        
        acRelationUpdateList.addAll(acRelationUpdateMap.values());
        System.debug('acRelationInsertList: ' + acRelationInsertList);
        System.debug('acRelationInsertList Size: ' + acRelationInsertList.size());
        System.debug('acRelationUpdateList: ' +acRelationUpdateList);
        System.debug('acRelationUpdateList Size: ' + acRelationUpdateList.size());
        
        Database.SaveResult[] insertedList = Database.insert(acRelationInsertList, true);
        
        // Iterate through each returned result
        for (Database.SaveResult sr : insertedList) {
            if (sr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully inserted account. AccountContactRelation ID: ' + sr.getId());
            }
            else {
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Account fields that affected this error: ' + err.getFields());
                }
            }
        }
        
        Database.SaveResult[] updatedList = Database.update(acRelationUpdateList, true);
        
        // Iterate through each returned result
        for (Database.SaveResult sr : updatedList) {
            if (sr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully inserted account. AccountContactRelation ID: ' + sr.getId());
            }
            else {
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Account fields that affected this error: ' + err.getFields());
                }
            }
        }
        */
    }
    
    global void finish(Database.BatchableContext BC){
    }
}