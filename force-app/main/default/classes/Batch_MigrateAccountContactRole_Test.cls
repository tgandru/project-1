@isTest
private class Batch_MigrateAccountContactRole_Test {

	private static testMethod void test() {
      Test.startTest();
            Channelinsight.CI_TEST_TestUtils.createBaseInstallData();

        Zipcode_Lookup__c zip=TestDataUtility.createZipcode();
        insert zip;
             Id endUserRT = TestDataUtility.retrieveRecordTypeId('End_User', 'Account'); 
             Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
        //Creating test Account
      Account   testMainAccount = TestDataUtility.createAccount(TestDataUtility.generateRandomString(5));
        testMainAccount.RecordTypeId = endUserRT;
        insert testMainAccount;
        
     Account   testCustomerAccount = TestDataUtility.createAccount(TestDataUtility.generateRandomString(5));
        testCustomerAccount.RecordTypeId = directRT;
        insert testCustomerAccount;
        
         Contact contactRecord = TestDataUtility.createContact(testMainAccount);
        insert contactRecord;
        
         Contact contactRecord2 = TestDataUtility.createContact(testCustomerAccount);
        insert contactRecord2;
        
         List<AccountContactRole> roles=new  List<AccountContactRole>();
        AccountContactRole ac1=new AccountContactRole(AccountId=testCustomerAccount.id,ContactId=contactRecord.id,Role='Business User');
         AccountContactRole ac2=new AccountContactRole(AccountId=testCustomerAccount.id,ContactId=contactRecord2.id,Role='Business User');
         roles.add(ac1);
         roles.add(ac2);
         Insert roles;
         
         Database.executeBatch(new Batch_MigrateAccountContactRole() );
         
      Test.StopTest();
	}

}