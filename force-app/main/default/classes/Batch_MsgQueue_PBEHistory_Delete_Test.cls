@isTest
public class Batch_MsgQueue_PBEHistory_Delete_Test {
    public Static String CRON_EXP = '0 0 12 15 2 ?';
    public Static String CRON_EXP2 = '0 0 12 15 4 ?';
    @isTest static void test_method_one(){
        datetime createddt = System.now();
        createddt = createddt.addDays(-91);
        user u =[Select id,name from user where name='Migration User' limit 1];
        system.runAs(u){
            message_queue__c mq = new message_queue__c(Integration_Flow_Type__c='Test',retry_counter__c=0,Status__c='not started', Object_Name__c='Test Object',createddate=createddt);
            insert mq;
            
            PriceBookEntry__c pbe = new PriceBookEntry__c(Pricebook__c='TEST_PB',Product__c='TEST_Product',List_Price__c=99.99,createddate=createddt);
            insert pbe;
            
            Batch_MessageQueue_Delete ba = new Batch_MessageQueue_Delete();
            database.executeBatch(ba);
            
            Batch_PriceBookEntryHistory_Delete ba2 = new Batch_PriceBookEntryHistory_Delete();
            database.executeBatch(ba2);
            
            id jobId = System.schedule('TestScheduledBatch',CRON_EXP, new Batch_MessageQueue_Delete());
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];  
            System.assertEquals(CRON_EXP, ct.CronExpression);
            System.assertEquals(0, ct.TimesTriggered);
            
            id jobId2 = System.schedule('TestScheduledBatch2',CRON_EXP2, new Batch_PriceBookEntryHistory_Delete());
            CronTrigger ct2 = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId2];  
            System.assertEquals(CRON_EXP2, ct2.CronExpression);
            System.assertEquals(0, ct2.TimesTriggered);
        }
    }
}