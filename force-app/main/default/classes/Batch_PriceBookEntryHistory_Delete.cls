global class Batch_PriceBookEntryHistory_Delete implements database.batchable<sObject>,Schedulable{
    
    global void execute(SchedulableContext SC) {
        Batch_PriceBookEntryHistory_Delete ba = new Batch_PriceBookEntryHistory_Delete();
        database.executeBatch(ba);
    }
    
    global database.queryLocator start(database.batchablecontext bc){
        return database.getQueryLocator([select id from PriceBookEntry__c where createddate<LAST_N_DAYS:60 order by createddate asc]);
    }
    
    global void execute(database.batchablecontext bc, List<PriceBookEntry__c> DeleteList){
        delete DeleteList;
    }
    
    global void finish(database.batchablecontext bc){
        
    }
    
}