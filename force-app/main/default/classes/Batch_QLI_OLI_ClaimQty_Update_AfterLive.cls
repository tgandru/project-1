global class Batch_QLI_OLI_ClaimQty_Update_AfterLive implements database.Batchable<sObject>,Database.stateful {
    global integer OLIcount=0;
    global integer QLIcount=0;
    global Map<id,string> oppliUpdateMap;
    global Map<id,OpportunityLineItem> IdToOLIMap;
    global Map<id,string> QliUpdateMap;
    global Map<id,QuoteLineItem> IdToQliMap;
    
    global Batch_QLI_OLI_ClaimQty_Update_AfterLive(){
        oppliUpdateMap = new Map<id,string>();
        QliUpdateMap = new Map<id,string>();
        IdToOLIMap = new Map<id,OpportunityLineItem>();
        IdToQliMap = new Map<id,QuoteLineItem>();
    }
    
    global database.QueryLocator start(Database.BatchableContext BC){
        database.Querylocator ql = database.getquerylocator([select id,Claimed_quantity__c,OLIID__c from Quotelineitem where quote.status!='Cancelled' and createddate>=2016-07-18T04:00:00.000+0000 order by createddate asc]);
        return ql;
    }
    
    global void execute(database.batchablecontext bc, List<quotelineitem> qlis){
        Map<id,Quotelineitem> qlimap = new Map<id,Quotelineitem>(qlis);
        List<Quotelineitem> updateQli = new List<QuoteLineItem>();
        List<Opportunitylineitem> updateOli = new List<OpportunityLineItem>();
        Map<string,integer> oliId_ReqQty_Map = new Map<string,integer>();
        Set<id> CMProd_Qli_Set = new Set<id>();
        for(AggregateResult cmprod : [select SUM(Request_Quantity__c)total_req,Qli_id__c from Credit_Memo_Product__c where (Credit_Memo__r.Status__c = 'Approved' or Credit_Memo__r.Status__c = 'Processed') AND Qli_id__c=:qlimap.keySet() group by Qli_id__c]){
            system.debug('Qli ID------>'+cmprod.get('QLI_ID__c')+' with Total Req Qty----->'+cmprod.get('total_req'));
            string tempid = string.valueOf(cmprod.get('QLI_ID__c'));
            Id qlid = (id)tempid;
            if(qlimap.containsKey(qlid)){
                QuoteLineItem qli = qlimap.get(qlid);
                if(qli.Claimed_quantity__c != integer.valueOf(cmprod.get('total_req'))){
                    qli.Claimed_quantity__c = integer.valueOf(cmprod.get('total_req'));
                    system.debug('Update Qli ID------>'+qli.Claimed_quantity__c+' with Total Req Qty----->'+cmprod.get('total_req'));
                    updateQli.add(qli);
                }
                oliId_ReqQty_Map.put(qlimap.get(qlid).OLIID__c, integer.valueOf(cmprod.get('total_req')));
            }
            CMProd_Qli_Set.add(qlid);
        }
        System.debug('Qli Update Size---->'+updateQli.size());
        System.debug('Oli Update Size---->'+updateOli.size());
        for(quotelineitem qli : qlimap.values()){
            if(!CMProd_Qli_Set.contains(qli.id)){
                if(qli.Claimed_quantity__c>0){
                    qli.Claimed_quantity__c=0;
                    updateQli.add(qli);
                    oliId_ReqQty_Map.put(qli.OLIID__c,0);
                }
            }
        }
        
        for(OpportunityLineitem oli : [select id,Claimed_quantity__c from OpportunityLineItem where id=:oliId_ReqQty_Map.keySet()]){
            if(oli.Claimed_quantity__c!=oliId_ReqQty_Map.get(oli.id)){
                oli.Claimed_quantity__c = oliId_ReqQty_Map.get(oli.id);
                updateOli.add(oli);
            }
        }
        OLIcount = OLIcount+updateOli.size();
        QLIcount = QLIcount+updateQli.size();
        System.debug('Oli Update Size---->'+updateOli.size());
        System.debug('Qli Update Size---->'+updateQli.size());
        /*if(updateOli.size()>0)
            database.update(updateOli,false);
        if(updateQli.size()>0)
            database.update(updateQli,false);*/
            
        Database.SaveResult[] srList = database.update(updateOli,false);
        Integer index = 0;
        for (Database.SaveResult sr : srList) {
            if (!sr.isSuccess()) {
                // Operation failed, so get all errors               
                String errMsg = sr.getErrors()[0].getMessage();
                errMsg = errMsg.replace(',','.');
                errMsg = errMsg.replace('\r\n','.');
                errMsg = errMsg.replace('\n','.');
                errMsg = errMsg.replace('\r','.');
                oppliUpdateMap.put(updateOli[index].Id, errMsg);
                IdToOLIMap.put(updateOli[index].Id, updateOli[index]);
            }else{
                oppliUpdateMap.put(sr.getid(),'Success');
                IdToOLIMap.put(updateOli[index].Id, updateOli[index]);
            }
            index++;
        }
        
        Database.SaveResult[] srListQli = database.update(updateQli,false);
        Integer indexQli = 0;
        for (Database.SaveResult sr : srListQli) {
            if (!sr.isSuccess()) {
                // Operation failed, so get all errors               
                String errMsg = sr.getErrors()[0].getMessage();
                errMsg = errMsg.replace(',','.');
                errMsg = errMsg.replace('\r\n','.');
                errMsg = errMsg.replace('\n','.');
                errMsg = errMsg.replace('\r','.');
                QliUpdateMap.put(updateQli[indexQli].Id, errMsg);
                IdToQliMap.put(updateQli[indexQli].Id, updateQli[indexQli]);
            }else{
                QliUpdateMap.put(sr.getid(),'Success');
                IdToQliMap.put(updateQli[indexQli].Id, updateQli[indexQli]);
            }
            indexQli++;
        }
    }
    
    global void finish(database.batchablecontext bc){
        system.debug('OLIcount-------->'+OLIcount);
        system.debug('QLIcount-------->'+QLIcount);
        
        String Olistr='OLI_ID, Claimed_Qty,Record_Update_Status\n';
        for(OpportunityLineItem oli : IdToOLIMap.values()){
            if(oppliUpdateMap.containsKey(oli.id)){
                Olistr = Olistr +oli.id+ ',' +oli.Claimed_quantity__c+ ',' +oppliUpdateMap.get(oli.id)+'\n';
            }
        }
        
        String Qlistr='QLI_ID, Claimed_Qty,Record_Update_Status\n';
        for(QuoteLineItem qli : IdToQLIMap.values()){
            if(QliUpdateMap.containsKey(qli.id)){
                Qlistr = Qlistr +qli.id+ ',' +qli.Claimed_quantity__c+ ',' +QliUpdateMap.get(qli.id)+'\n';
            }
        }
        
        String body = 'Your batch job '+ '"QLI (created from Live) Claimed Qty Update" '+ 'has finished. \n' + 'Please find the list attached';
        
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage(); 
        Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
        efa.setFileName('Qli_Update_Records.csv');
        efa.setBody(Blob.valueOf(Olistr));
        
        Messaging.EmailFileAttachment efa2 = new Messaging.EmailFileAttachment();
        efa2.setFileName('Qli_Update_Records.csv');
        efa2.setBody(Blob.valueOf(Qlistr));
        
        email.setSubject('QLI and its OLI claimed Qty update result');
        email.setToAddresses( new String[] {'t.gandru@partner.samsung.com'} );
        email.setPlainTextBody( body );
        email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa,efa2});

        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email}); 
    }
}