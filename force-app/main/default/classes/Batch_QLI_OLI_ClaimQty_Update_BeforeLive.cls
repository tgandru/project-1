global class Batch_QLI_OLI_ClaimQty_Update_BeforeLive implements database.Batchable<sObject>,Database.stateful {
    
    global integer OLIcount=0;
    global integer QLIcount=0;
    
    global Map<id,string> oppliUpdateMap;
    global Map<id,OpportunityLineItem> IdToOLIMap;
    global Map<id,string> QliUpdateMap;
    global Map<id,QuoteLineItem> IdToQliMap;
    
    global Map<id,string> oppliUpdateMap2;
    global Map<id,OpportunityLineItem> IdToOLIMap2;
    global Map<id,string> QliUpdateMap2;
    global Map<id,QuoteLineItem> IdToQliMap2;
    
    global Map<id,integer> CMProd_Qli_ReqQtySum;
    global Map<id,integer> CMProd_Oli_ReqQtySum;
    
    global integer HeapSizeLimit;
    global integer Recordlimit;
    global string queryString;
    
    global Batch_QLI_OLI_ClaimQty_Update_BeforeLive(integer Recordlimit){
        oppliUpdateMap = new Map<id,string>();
        QliUpdateMap = new Map<id,string>();
        IdToOLIMap = new Map<id,OpportunityLineItem>();
        IdToQliMap = new Map<id,QuoteLineItem>();
        
        oppliUpdateMap2 = new Map<id,string>();
        QliUpdateMap2 = new Map<id,string>();
        IdToOLIMap2 = new Map<id,OpportunityLineItem>();
        IdToQliMap2 = new Map<id,QuoteLineItem>();
        
        CMProd_Qli_ReqQtySum = new Map<id,integer>();
        CMProd_Oli_ReqQtySum = new Map<id,integer>();
        
        HeapSizeLimit = 9000000;
        
        this.Recordlimit = Recordlimit;
    }
    
    global database.QueryLocator start(Database.BatchableContext BC){
        /*
        database.Querylocator ql = database.getquerylocator([select id,Claimed_quantity__c,OLIID__c from Quotelineitem where createddate<2016-07-18T04:00:00.000+0000 order by createddate asc]);
        return ql;
        */
        queryString = 'select id,Claimed_quantity__c,OLIID__c from Quotelineitem where createddate<2016-07-18T04:00:00.000+0000 order by createddate asc limit '+Recordlimit;
        return Database.getQueryLocator(queryString);
    }
    
    global void execute(database.batchablecontext bc, List<quotelineitem> qlis){
        Map<id,Quotelineitem> qlimap = new Map<id,Quotelineitem>(qlis);
        List<Quotelineitem> updateQli = new List<QuoteLineItem>();
        List<Opportunitylineitem> updateOli = new List<OpportunityLineItem>();
        Map<string,integer> oliId_ReqQty_Map = new Map<string,integer>();
        for(AggregateResult cmprod : [select SUM(Request_Quantity__c)total_req,Qli_id__c from Credit_Memo_Product__c where (Credit_Memo__r.Status__c = 'Approved' or Credit_Memo__r.Status__c = 'Processed') AND Qli_id__c=:qlimap.keySet() group by Qli_id__c]){
            system.debug('Qli ID------>'+cmprod.get('QLI_ID__c')+' with Total Req Qty----->'+cmprod.get('total_req'));
            string tempid = string.valueOf(cmprod.get('QLI_ID__c'));
            Id qlid = (id)tempid;
            if(qlimap.containsKey(qlid)){
                QuoteLineItem qli = qlimap.get(qlid);
                if(qli.Claimed_quantity__c != integer.valueOf(cmprod.get('total_req'))){
                    qli.Claimed_quantity__c = integer.valueOf(cmprod.get('total_req'));
                    system.debug('Update Qli ID------>'+qli.Claimed_quantity__c+' with Total Req Qty----->'+cmprod.get('total_req'));
                    updateQli.add(qli);
                    CMProd_Qli_ReqQtySum.put(qlid,integer.valueOf(cmprod.get('total_req')));
                }
                oliId_ReqQty_Map.put(qlimap.get(qlid).OLIID__c, integer.valueOf(cmprod.get('total_req')));
            }
        }
        
        Map<id,OpportunityLineItem> OliMap = new Map<id,OpportunityLineItem>([select id,Claimed_quantity__c from OpportunityLineItem where id=:oliId_ReqQty_Map.keySet()]);
        
        for(AggregateResult cmprod : [select SUM(Request_Quantity__c)total_req,OLI_ID__c from Credit_Memo_Product__c where (Credit_Memo__r.Status__c = 'Approved' or Credit_Memo__r.Status__c = 'Processed') AND OLI_ID__c=:oliId_ReqQty_Map.keySet() group by OLI_ID__c]){
            system.debug('Oli ID------>'+cmprod.get('OLI_ID__c')+' with Total Req Qty----->'+cmprod.get('total_req'));
            string tempid = string.valueOf(cmprod.get('OLI_ID__c'));
            Id olid = (id)tempid;
            if(OliMap.containsKey(olid)){
                OpportunityLineItem oli = Olimap.get(olid);
                if(oli.Claimed_quantity__c != integer.valueOf(cmprod.get('total_req'))){
                    oli.Claimed_quantity__c = integer.valueOf(cmprod.get('total_req'));
                    system.debug('Update Oli ID------>'+oli.Claimed_quantity__c+' with Total Req Qty----->'+cmprod.get('total_req'));
                    updateOli.add(Oli);
                    CMProd_Oli_ReqQtySum.put(olid,integer.valueOf(cmprod.get('total_req')));
                }
            }
        }
        
        OLIcount = OLIcount+updateOli.size();
        QLIcount = QLIcount+updateQli.size();
        
        System.debug('Oli Update Size---->'+updateOli.size());
        System.debug('Qli Update Size---->'+updateQli.size());
        
        System.debug('Current Heap Size---->'+Limits.getHeapSize());
        System.debug('Heap Size Limit---->'+limits.getLimitHeapSize());
        
        //if(Limits.getHeapSize()<HeapSizeLimit){    
            Database.SaveResult[] srList = database.update(updateOli,false);
            Integer index = 0;
            for (Database.SaveResult sr : srList) {
                if (!sr.isSuccess()) {
                    // Operation failed, so get all errors               
                    String errMsg = sr.getErrors()[0].getMessage();errMsg = errMsg.replace(',','.');errMsg = errMsg.replace('\r\n','.');errMsg = errMsg.replace('\n','.'); errMsg = errMsg.replace('\r','.');
                    oppliUpdateMap.put(updateOli[index].Id, errMsg);
                    IdToOLIMap.put(updateOli[index].Id, updateOli[index]);
                }else{
                    oppliUpdateMap.put(sr.getid(),'Success');
                    IdToOLIMap.put(updateOli[index].Id, updateOli[index]);
                }
                index++;
            }
            
            Database.SaveResult[] srListQli = database.update(updateQli,false);
            Integer indexQli = 0;
            for (Database.SaveResult sr : srListQli) {
                if (!sr.isSuccess()) {
                    // Operation failed, so get all errors               
                    String errMsg = sr.getErrors()[0].getMessage(); errMsg = errMsg.replace(',','.'); errMsg = errMsg.replace('\r\n','.'); errMsg = errMsg.replace('\n','.'); errMsg = errMsg.replace('\r','.');
                    QliUpdateMap.put(updateQli[indexQli].Id, errMsg);
                    IdToQliMap.put(updateQli[indexQli].Id, updateQli[indexQli]);
                }else{
                    QliUpdateMap.put(sr.getid(),'Success');
                    IdToQliMap.put(updateQli[indexQli].Id, updateQli[indexQli]);
                }
                indexQli++;
            }
        /*}else{
            Database.SaveResult[] srList = database.update(updateOli,false);
            Integer index2 = 0;
            for (Database.SaveResult sr : srList) {
                if (!sr.isSuccess()) {
                    // Operation failed, so get all errors               
                    String errMsg = sr.getErrors()[0].getMessage(); errMsg = errMsg.replace(',','.'); errMsg = errMsg.replace('\r\n','.'); errMsg = errMsg.replace('\n','.'); errMsg = errMsg.replace('\r','.');
                    oppliUpdateMap2.put(updateOli[index2].Id, errMsg);
                    IdToOLIMap2.put(updateOli[index2].Id, updateOli[index2]);
                }else{
                    oppliUpdateMap2.put(sr.getid(),'Success');
                    IdToOLIMap2.put(updateOli[index2].Id, updateOli[index2]);
                }
                index2++;
            }
            
            Database.SaveResult[] srListQli = database.update(updateQli,false);
            Integer indexQli2 = 0;
            for (Database.SaveResult sr : srListQli) {
                if (!sr.isSuccess()) {
                    // Operation failed, so get all errors               
                    String errMsg = sr.getErrors()[0].getMessage(); errMsg = errMsg.replace(',','.'); errMsg = errMsg.replace('\r\n','.'); errMsg = errMsg.replace('\n','.'); errMsg = errMsg.replace('\r','.');
                    QliUpdateMap2.put(updateQli[indexQli2].Id, errMsg);
                    IdToQliMap2.put(updateQli[indexQli2].Id, updateQli[indexQli2]);
                }else{
                    QliUpdateMap2.put(sr.getid(),'Success');
                    IdToQliMap2.put(updateQli[indexQli2].Id, updateQli[indexQli2]);
                }
                indexQli2++;
            }
        }*/
    }
    
    global void finish(database.batchablecontext bc){
        system.debug('OLIcount-------->'+OLIcount);
        system.debug('QLIcount-------->'+QLIcount);
        
        String Olistr='OLI_ID, Claimed_Qty,Record_Update_Status,Req_Qty_Sum\n';
        for(OpportunityLineItem oli : IdToOLIMap.values()){
            if(oppliUpdateMap.containsKey(oli.id)){
                Olistr = Olistr +oli.id+ ',' +oli.Claimed_quantity__c+ ',' +oppliUpdateMap.get(oli.id)+ ',' +CMProd_Oli_ReqQtySum.get(oli.id)+'\n';
            }
        }
        
        /*for(OpportunityLineItem oli : IdToOLIMap2.values()){
            if(oppliUpdateMap2.containsKey(oli.id)){
                Olistr = Olistr +oli.id+ ',' +oli.Claimed_quantity__c+ ',' +oppliUpdateMap.get(oli.id)+ ',' +CMProd_Oli_ReqQtySum.get(oli.id)+'\n';
            }
        }*/
        
        String Qlistr='QLI_ID, Claimed_Qty,Record_Update_Status,Req_Qty_Sum\n';
        for(QuoteLineItem qli : IdToQLIMap.values()){
            if(QliUpdateMap.containsKey(qli.id)){
                Qlistr = Qlistr +qli.id+ ',' +qli.Claimed_quantity__c+ ',' +QliUpdateMap.get(qli.id)+ ',' +CMProd_Qli_ReqQtySum.get(qli.id)+'\n';
            }
        }
        
        /*for(QuoteLineItem qli : IdToQLIMap2.values()){
            if(QliUpdateMap2.containsKey(qli.id)){
                Qlistr = Qlistr +qli.id+ ',' +qli.Claimed_quantity__c+ ',' +QliUpdateMap.get(qli.id)+ ',' +CMProd_Qli_ReqQtySum.get(qli.id)+'\n';
            }
        }*/
        
        String body = 'Your batch job '+ '"QLI (created before Live) Claimed Qty Update" '+ 'has finished. \n' + 'Please find the list attached';
        
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage(); 
        Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
        efa.setFileName('Oli_Update_Records.csv');
        efa.setBody(Blob.valueOf(Olistr));
        
        Messaging.EmailFileAttachment efa2 = new Messaging.EmailFileAttachment();
        efa2.setFileName('Qli_Update_Records.csv');
        efa2.setBody(Blob.valueOf(Qlistr));
        
        email.setSubject('QLI and its OLI claimed Qty update result');
        email.setToAddresses( new String[] {'t.gandru@partner.samsung.com','stevepark@samsung.com','v.kamani@partner.samsung.com'} );
        email.setPlainTextBody( body );
        email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa,efa2});

        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email}); 
    }
}