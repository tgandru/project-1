@isTest
public class Batch_SECPICustomSettingDelete_Test {
    @testSetup static void setup(){
        Data_Send_to_SECPI__c AccData = new Data_Send_to_SECPI__c(name='testAccID');
        insert AccData;
        Oppty_Data_to_SECPI__c OppData = new Oppty_Data_to_SECPI__c(name='testOppID');
        insert OppData;
        Rollout_Data_to_SECPI__c RolloutData = new Rollout_Data_to_SECPI__c(name='testRopID');
        insert RolloutData;
        Oppty_Activity_Data_to_SECPI__c ActivityData = new Oppty_Activity_Data_to_SECPI__c(name='testActivityID');
        insert ActivityData;
        Oppty_Activity_Event_Data_to_SECPI__c EventData= new Oppty_Activity_Event_Data_to_SECPI__c(name='TestEvent');
        insert EventData;
        Deleted_Data_To_SECPI__c DelRecordData= new Deleted_Data_To_SECPI__c(name='TestDeletedRecord');
        insert DelRecordData;
    }
    
    @isTest static void testMethod1(){
        Test.startTest();
            Batch_AccountDataToSECPI_Delete ba1 = new Batch_AccountDataToSECPI_Delete();
            database.executeBatch(ba1);
            Batch_OpptyDataToSECPI_Delete ba2 = new Batch_OpptyDataToSECPI_Delete();
            database.executeBatch(ba2);
            Batch_RolloutDataToSECPI_Delete ba3 = new Batch_RolloutDataToSECPI_Delete();
            database.executeBatch(ba3);
            Batch_OpptyActivityDataToSECPI_Delete ba4 = new Batch_OpptyActivityDataToSECPI_Delete();
            database.executeBatch(ba4);
            Batch_OpptyEventActDataToSECPI_Delete ba5 = new Batch_OpptyEventActDataToSECPI_Delete();
            database.executeBatch(ba5);
            Batch_DeletedDataToSECPI_Delete ba6 = new Batch_DeletedDataToSECPI_Delete();
            database.executeBatch(ba6);
        test.stopTest();
    }
}