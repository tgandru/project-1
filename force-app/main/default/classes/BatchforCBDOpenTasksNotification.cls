//Daily batch for Sending all unAcknowledged open tasks list to CBD users.
global class BatchforCBDOpenTasksNotification implements Database.Batchable<sObject>, Database.AllowsCallouts, Schedulable {
    
    global void execute(SchedulableContext SC) {
        BatchforCBDOpenTasksNotification ba = new BatchforCBDOpenTasksNotification();
        database.executeBatch(ba,50);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        if(Test.isRunningTest()){
            database.Querylocator ql = database.getquerylocator([SELECT id,Email from User where id=:UserInfo.getUserId()]);
            return ql;
        }else{
            database.Querylocator ql = database.getquerylocator([SELECT id,Email from User where CBD_User__c=true and IsActive=true and profile.name like 'CBD%']);
            //database.Querylocator ql = database.getquerylocator([SELECT id,Email from User where id in ('00536000002AuZ3','00536000005bi84')]);
            return ql;
        }
    }
    
    global void execute(Database.BatchableContext BC, List<User> scope) {
        Map<id,User> userMap = new Map<id,User>(scope);
        Map<String,List<Task>> userTasksMap = new Map<String,List<Task>>();
        
        for(Task tsk : [SELECT id,Subject,ActivityDate,Ownerid,Owner.Name,Owner.Email,Status,Acknowledged__c,Task_Status__c,Action_Item__c,Description from Task where OwnerId=:userMap.keySet() and Acknowledged__c=false and Status='Open' and ActivityDate!=null]){
            List<task> taskList = new List<task>();
            if(userTasksMap.containsKey(tsk.Ownerid)){
                taskList = userTasksMap.get(tsk.Ownerid);
            }
            taskList.add(tsk);
            userTasksMap.put(tsk.Ownerid,taskList);
        }
        
        Messaging.Email[] emailList = new Messaging.Email[0];
        
        for(String usrId : userTasksMap.keySet()){
            String htmlBody = '<p>***TASKS NOT ACKNOWLEDGED - NOTIFICATION***</p><p>The following Tasks has not been acknowledged</p><br/><table width="100%" border="1" cellspacing="0" cellpadding="4" align="center">'; // bgcolor="#F7F7F7"
            htmlBody = htmlBody + '<thead><tr><td><b>Status</b></td><td><b>Due Date</b></td><td><b>Subject</b></td><td><b>Description</b></td><td><b>Action Item</b></td></tr></thead><tbody>';
            
            integer taskCount = 0;
            for(task t : userTasksMap.get(usrId)){
                taskCount++;
                date currDate = system.today();
                string taskStatus = '';
                if(currDate>t.ActivityDate){
                    taskStatus = '<td bgcolor="red"></td>';
                }else if(currDate.daysBetween(t.ActivityDate)<3){
                    taskStatus = '<td bgcolor="yellow"></td>';
                }else{
                    taskStatus = '<td bgcolor="green"></td>';
                }
                
                string actionItem = '';
                if(t.Action_Item__c!=null) actionItem = t.Action_Item__c;
                
                string Description = '';
                if(t.Description!=null) Description = t.Description;
                
                
                String orgURL = URL.getSalesforceBaseUrl().toExternalForm();
                String subjectURL = '<a href="'+URL.getSalesforceBaseUrl().toExternalForm()+'/'+t.id+'">'+t.Subject+'</a>';
                htmlBody = htmlBody + '<tr>'+taskStatus+'<td>'+t.ActivityDate.format()+'</td><td>'+subjectURL+'</td><td>'+Description+'</td><td>'+actionItem+'</td></tr>';
            }
            htmlBody = htmlBody + '</tbody></table>';
            if(taskCount>0){
                List<String> toAddresses = new List<String>{usrId};
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setToAddresses(toAddresses);
                mail.setSubject('Tasks not Acknowledged');
                mail.setHtmlBody(htmlBody);
                mail.setSaveAsActivity(false);
                mail.setUseSignature(false);
                emailList.add(mail);
            }
        }
        if(emailList.size()>0){
            Messaging.SendEmailResult[] results = Messaging.sendEmail(emailList,false);
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
}