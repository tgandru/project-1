@isTest
public class BatchforCBDOpenTasksNotificationTest {
    @testSetup static void setup() {
         Channelinsight.CI_TEST_TestUtils.createBaseInstallData();

         Account testAccount = new Account();
         testAccount.Name='Test Account' ;
         insert testAccount;
        
         Call_Report__c cr=new Call_Report__c();
         cr.Name='cr1';
         cr.Primary_Account__c=testAccount.Id;
         cr.Agenda__c='Test Agenda';
         cr.Meeting_Summary__c='Test Meeting Summary';
         cr.Competitive_Intelligence__c='Test Cmpetitive Intelligence';
         insert cr;
         

         Task t1=new Task();
         t1.Subject='test Task';
         t1.ownerid=UserInfo.getUserId();
         t1.whatid=cr.id;
         t1.Acknowledged__c=false;
         t1.Status='Open';
         t1.ActivityDate=System.today();
         insert t1;

     } 
   
    @isTest static void testBatch(){
        Test.startTest();
             BatchforCBDOpenTasksNotification batch = new BatchforCBDOpenTasksNotification(); 
             Database.executeBatch(batch);
        Test.stopTest();    
    }        
}