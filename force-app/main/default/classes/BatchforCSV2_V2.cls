// Batch for Download the Printer Data Based on Opportunities Provided by Jeff
// Includes Object Data : OpportunityLineItems, Opportunity Demos, OpportunityTeamMembers, Opportunity Partners, Opportunity Competitors, Opportunity Activity
// Includes PriceBookEntries of Printer Pricebooks
Global class BatchforCSV2_V2 implements Database.Batchable<sObject>, Database.stateful{
    global Map<String, Schema.SObjectField> objectFields;
    global List<String> fieldNames;
    global String Name;
    global String finalactivityHeader;
    global String finalOpenActHeader;
    global String queryString;
    global integer batchcount;
    global String finalstr1;
    global String finalCompString ;
    global String finalDemoString ;
    global String finalOCRstring;
    global String finalstrPBE;
    global String OppPartnerString;
    global String finalPartnerString;
    global String finalPartnerString2;
    global String finalTeamString;
    global String finalchannelString;
    global String finalQtchannelString;
    global integer num;
    global string Email;
    global Boolean Sample;
  
    // Batch Constructor
    global BatchforCSV2_V2(integer num,string Email,Boolean Sample){
        this.num = num;
        this.Email = Email;
        this.Sample = Sample;
         
        objectFields = Schema.getGlobalDescribe().get('Opportunity').getDescribe().fields.getMap();
        DescribeSObjectResult describeResult = Opportunity.getSObjectType().getDescribe();      
        fieldnames = new List<String>(objectFields.keySet());
        Name = describeResult.getName();
        batchcount=0;
        string header1 = 'id,Opportunityid,Opportunity_Name,productId,SAP_Material_Id,ProductGroup,ProductType,Alternative,UnitPrice,Quantity,ListPrice,Requested_Price,TotalPrice,Discount% \n';
        finalstr1 = header1 ;
        string ActHeader = 'id,accountid,AccountName,Opportunityid,OpportunityName,Whoid,activitydate,ActivityType,Subject,Status,EndDateTime,OwnerId,IsTask,Contactid,ContactName,Isclosed \n';
        finalactivityHeader = ActHeader;
        //String CompHeader = 'id,Opportunityid,Competitor_Name,strengths,weaknesses \n';
        String CompHeader = 'id,Opportunityid,Opportunity_Name,Competitor_Name,strengths \n';
        finalCompString = CompHeader;
        String DemoHeader = 'id,name,Opportunityid,Opportunity_name,Status,Order_Number,Type,Order_Start_Date,Expected_Delivery_Date,Product_Group,Contact,Contact_Name,Shipto_Contact,Shipto_Contact_Name,Bill_To_Contact,Bill_To_Contact_Name,ShippingStreet,ShippingCity,ShippingState,ShippingCountry,ShippingPostalcode,createddate \n';
        finalDemoString = DemoHeader;
        String OCRheader = 'id,Opportunityid,Contactid,Role \n';
        finalOCRstring = OCRheader;
        string PBEheader = 'PricebookEntryid,productid,SAP_Material_ID__c,product_family,Product_Group__c,ListPrice,pricebookid,pricebook_name \n';
        finalstrPBE = PBEheader ;
        String OppPartnerHeader = 'Partnerid,Accountid,Partner_Account_Name,Role,Is_Primary,Registered,Opportunity_id,Opportunity_Name \n';
        OppPartnerString = OppPartnerHeader;
        String PartnerHeader = 'Partnerid,Accountid,Partner_Account_Name,Role,Is_Primary,Is_Reseller,Opportunity_id \n';
        finalPartnerString = PartnerHeader;
        String PartnerHeader2 = 'Partnerid,Accountid,Partner_Account_Name,Role,Is_Primary,Is_Reseller,Opportunity_id \n';
        finalPartnerString2 = PartnerHeader2;
        String TeamHeader = 'id,Opportunityid,Opportunity_Name,Team_Member_Name,MemberID,TeamMemberRole,OpportunityAccessLevel \n';
        finalTeamString = TeamHeader;
        String ChannelHeader = 'id,OpportunityID,Opportunity_Name,DistributorName,DistributorID,ResellerName,ResellerID \n';
        finalchannelString = ChannelHeader;
        String QtchannelHeader = 'Distributor,Reseller \n';
        finalQtchannelString = QtchannelHeader;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        /*
        List<String> includesList = new List<String>{'\'PRINTER\'','\'A3 COPIER\'','\'FAX/MFP\'','\'SOLUTION S/W\''};
        Id RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('IT').getRecordTypeId();
        queryString = ' SELECT ' + String.join( fieldNames, ',' ) +  ' FROM ' + Name + ' WHERE ProductGroupTest__c INCLUDES(';
        for(String includeValue :includesList){
            queryString += includeValue + ',';
        }    
        queryString = queryString.removeEnd(',') +')'+' and recordtypeid=:RecordTypeId and (CALENDAR_YEAR(Roll_Out_Start__c)>=2016 or CALENDAR_YEAR(createddate)>=2016) order by createddate limit '+num;
        List<Opportunity> opplist = (List<Opportunity>)Database.query(queryString);
        */
        queryString = 'select Name,id,Opportunity_No__c from Printer_Data_Export__c Where Batch_Number__c= '+num;
        List<Printer_Data_Export__c> PDElist = (List<Printer_Data_Export__c>)Database.query(queryString);
        return Database.getQueryLocator(queryString);
        
    }
    
    global void execute (Database.Batchablecontext BC, list<Printer_Data_Export__c> scope){
       
       set<id> opid=new set<id>();
       set<id> acid=new set<id>();
       set<id> qtid=new set<id>();
       
       List<String> OppNumList = new List<String>(); 
       for(Printer_Data_Export__c p : Scope){
           OppNumList.add(p.Opportunity_No__c);
       }
       if(sample==false){
           for(Opportunity o:[select id,name,Opportunity_Number__c,ownerid,Accountid,stagename,amount,closedate,type,recordtype.name,ProductGroupTest__c,Pricebook2id,Roll_Out_Start__c,Rollout_Duration__c,Roll_Out_End_Formula__c,ForecastAmount__c,QuotePlanVariance__c,Roll_Out_Status__c,Is_Pilot_Order__c,Plan_Variance__c,Reason__c,Deal_Registration_Approval__c,Primary_Distributor__c,Primary_Reseller__c,LeadSource,Product_Information__c,CompetitiveInformation__c,Managed_Print_Services_Applicable__c,Historical_Comments__c,createddate,createdbyid,lastmodifieddate from Opportunity where Opportunity_Number__c=:OppNumList]){
               opid.add(o.id);
               acid.add(o.Accountid);
           }
           
           for(OpportunityLineItem oli:[select id,Opportunityid,ProductGroup__c,ProductType__c,Opportunity.Name,Product2.Name,productId__c,SAP_Material_Id__c,UnitPrice,Quantity,ListPrice,TotalPrice,Discount__c,Requested_Price__c,Alternative__c from OpportunityLineItem where Opportunity.id in :opid ]){
               string recordString1 = oli.id + ',' + oli.Opportunityid+','+(oli.Opportunity.name).replace(',','')+','+oli.productId__c+','+oli.SAP_Material_Id__c+','+oli.ProductGroup__c+','+oli.ProductType__c+','+oli.Alternative__c+','+oli.UnitPrice+','+oli.Quantity+','+oli.ListPrice +','+oli.Requested_Price__c+','+oli.TotalPrice+','+oli.Discount__c +'\n'; 
               finalstr1 = finalstr1 + recordString1;
           }
           
           for(opportunitycompetitor c : [select id, opportunityid,Opportunity.name, competitorname, strengths, weaknesses from opportunitycompetitor where opportunityid=:opid]){
               string strengths;
               if(c.strengths==null||c.strengths==''){
                   strengths='null';
                   
               }else{
                   strengths = (c.strengths).replace('\r\n','-');
                   strengths = strengths.replace('\n','-');
                   strengths = strengths.replace('\r','-');
                   strengths = strengths.replace(',',''); 
                   
               }
               /*if(c.Weaknesses==null||c.Weaknesses==''){
                   c.Weaknesses='null';
               }else{
                   c.Weaknesses='null';
               }*/
               //string compRecordString = c.id +','+c.opportunityid+','+(c.competitorname).replace(',','') +','+(c.strengths).replace(',','')+','+(c.weaknesses).replace(',','')+'\n';
               string compRecordString = c.id +','+c.opportunityid+','+(c.Opportunity.name).replace(',','')+','+(c.competitorname).replace(',','') +','+strengths+'\n';
               finalCompString = finalCompString + compRecordString;     
           }
           
           for(order o : [select id,name,opportunityid,status,OrderNumber,type,EffectiveDate,Opportunity.name,RequestedShipmentDate__c,Product_Group__c,CustomerAuthorizedByid,CustomerAuthorizedBy.name,ShipToContactid,ShipToContact.name,BillToContactid,BillToContact.name,ShippingStreet,ShippingCity,ShippingState,ShippingCountry,ShippingPostalcode,createddate from order where opportunityid=:opid]){
               string EffectiveDate = String.valueOf(o.EffectiveDate);
                string ShippingStreet;
               if(o.ShippingCity==null||o.ShippingCity=='')
                   o.ShippingCity='null';
               if(o.ShippingCountry==null||o.ShippingCountry=='')
                   o.ShippingCountry='null';
               if(o.ShippingPostalcode==null||o.ShippingPostalcode=='')
                   o.ShippingPostalcode='null';
               if(o.ShippingState==null||o.ShippingState=='')
                   o.ShippingState='null';
               if(o.ShippingStreet==null||o.ShippingStreet==''){
                   o.ShippingStreet='null';
               }else{
                   ShippingStreet = (o.ShippingStreet).replace('\r\n','-');
                   ShippingStreet = ShippingStreet.replace('\n','-');
                   ShippingStreet = ShippingStreet.replace('\r','-');
                   ShippingStreet = ShippingStreet.replace(',','');
               }
               String DemoRecordString = o.id+','+(o.name).replace(',','')+','+o.Opportunityid+','+(o.Opportunity.name).replace(',','')+','+o.status+','+o.OrderNumber+','+o.Type+','+EffectiveDate+','+o.RequestedShipmentDate__c+','+o.Product_Group__c+','+o.CustomerAuthorizedByid+','+o.CustomerAuthorizedBy.name+','+o.ShipToContactid+','+o.ShipToContact.name+','+o.BillToContactid+','+o.BillToContact.name+','+ShippingStreet+','+o.ShippingCity+','+o.ShippingState+','+o.ShippingCountry+','+o.ShippingPostalcode+','+o.createddate+'\n';
               finalDemoString = finalDemoString + DemoRecordString;
           }
           /*
           for(OpportunityContactRole ocr : [select id, opportunityid, Contactid , role from OpportunityContactRole where opportunityid=:opid]){
               String ocrString = ocr.id+','+ocr.opportunityid+','+ocr.Contactid+','+ocr.role+'\n';
               finalOCRstring = finalOCRstring + ocrString;
           }
           */
           for(Partner__c p : [select id,partner__c,PartnerName__c,Role__c,Is_Primary__c,Role_is_Reseller__c,Registered__c,Opportunity__c,Opportunity__r.name from Partner__c where Opportunity__c=:opid]){
               String PartnerName;
               if(p.PartnerName__c==null||p.PartnerName__c=='')
                   PartnerName='Null';
               else
                   PartnerName = p.PartnerName__c;
               string partnerRecString = p.id +','+ p.Partner__c +','+PartnerName.replace(',','')+','+p.Role__c+','+p.Is_Primary__c+','+p.Registered__c+','+p.Opportunity__c+','+(p.Opportunity__r.name).replace(',','')+'\n';
               OppPartnerString = OppPartnerString + partnerRecString;
           }
           /*
           for(Partner__c p : [select id,partner__c,PartnerName__c,Role__c,Is_Primary__c,Role_is_Reseller__c,Opportunity__c from Partner__c where Partner__r.id=:acid]){
               string partnerRecordString = p.id +','+ p.Partner__c +','+(p.PartnerName__c).replace(',','')+','+p.Role__c+','+p.Is_Primary__c+','+p.Role_is_Reseller__c+','+p.Opportunity__c+'\n';
               finalPartnerString = finalPartnerString + partnerRecordString;
           }
           /*
           for(Partner__c p : [select id,partner__c,PartnerName__c,Role__c,Is_Primary__c,Role_is_Reseller__c,Opportunity__c,Opportunity__r.name from Partner__c where Partner__r.RecordType.name='Indirect' and Opportunity__c=:opid]){
               string partnerRecordString2 = p.id +','+ p.Partner__c +','+(p.PartnerName__c).replace(',','')+','+p.Role__c+','+p.Is_Primary__c+','+p.Role_is_Reseller__c+','+p.Opportunity__c+'\n';
               finalPartnerString2 = finalPartnerString2 + partnerRecordString2;
           }
           */
           
           for(Channel__c p : [select id,name,Opportunity__c,Opportunity__r.name,DistributorName__c,Distributor__c,Partner_Distributor__c,Partner_Reseller__c,Reseller__c,ResellerName__c from Channel__c where Opportunity__r.id=:opid]){
               string ResellerName;
               string DistributorName;
               if(p.DistributorName__c==null||p.DistributorName__c==''){
                   DistributorName='null';
               }else{
                   DistributorName = (p.DistributorName__c).replace(',','');
               }
               if(p.ResellerName__c==null||p.ResellerName__c==''){
                   ResellerName='null';
               }else{
                   ResellerName = (p.ResellerName__c).replace(',','');
               }
               string channelRecordString = p.id +','+p.Opportunity__c+','+(p.Opportunity__r.name).replace(',','')+','+DistributorName+','+p.Distributor__c+','+ResellerName+','+p.Reseller__c+'\n';
               //string channelRecordString = p.id +','+p.Opportunity__c+','+p.Distributor__c+','+p.DistributorName__c+','+p.Reseller__c+','+p.ResellerName__c+'\n';
               finalchannelString = finalchannelString + channelRecordString;
           }
           
           for(OpportunityTeamMember team : [SELECT id,Opportunityid,Opportunity.name,name,userid,teammemberrole,OpportunityAccessLevel FROM OpportunityTeamMember where opportunityid=:opid]){
               string TeamRecordString = team.id +','+team.opportunityid+','+(team.Opportunity.name).replace(',','')+','+(team.name).replace(',','') +','+team.userid+','+(team.teammemberrole).replace(',','')+','+team.OpportunityAccessLevel+'\n';
               finalTeamString = finalTeamString + TeamRecordString;     
           }
           
           for(Opportunity opp : [select id,(SELECT id,accountid,account.name,whatid,what.name,whoid,activitydate,ActivityType,CallType,EndDateTime,IsTask,OwnerId,PrimaryWhoId,PrimaryWho.name,Status,Subject,isclosed FROM ActivityHistories),(select id,accountid,account.name,whatid,what.name,whoid,activitydate,ActivityType,CallType,EndDateTime,IsTask,OwnerId,PrimaryWhoId,PrimaryWho.name,Status,Subject,isclosed from OpenActivities) from opportunity where id=:opid]){
               for(ActivityHistory a: opp.getSObjects('ActivityHistories')){
                   string subject;
                   string activitydate;
                   if(a.activitydate==null){
                        activitydate='null';
                    }
                    else{
                        Date dt = Date.newInstance(a.activitydate.year(),a.activitydate.month(),a.activitydate.day());
                        activitydate = String.valueOf(dt);
                    }
                   if(a.Subject==null||a.Subject==''){
                       subject='null';
                   }else{
                       subject = (a.Subject).replace(',','');
                   }
                   string ActivityrecordString = a.id + ',' + a.accountid+','+(a.account.name).replace(',','') +','+a.whatid+','+(a.what.name).replace(',','')+','+a.whoid+','+activitydate+','+a.ActivityType+','+subject+','+a.Status+','+a.EndDateTime+','+a.OwnerId+','+a.IsTask+','+a.PrimaryWhoId+','+a.PrimaryWho.name+','+a.isclosed+'\n'; 
                   finalactivityHeader = finalactivityHeader + ActivityrecordString;
               }
               for(OpenActivity a: opp.OpenActivities){
                   string subject;
                   string activitydate;
                   if(a.activitydate==null){
                        activitydate='null';
                    }
                    else{
                        Date dt = Date.newInstance(a.activitydate.year(),a.activitydate.month(),a.activitydate.day());
                        activitydate = String.valueOf(dt);
                    }
                   if(a.Subject==null||a.Subject==''){
                       subject='null';
                   }else{
                       subject = (a.Subject).replace(',','');
                   }
                   string OpenActrecordString = a.id + ',' + a.accountid+','+(a.account.name).replace(',','') +','+a.whatid+','+(a.what.name).replace(',','')+','+a.whoid+','+activitydate+','+a.ActivityType+','+subject+','+a.Status+','+a.EndDateTime+','+a.OwnerId+','+a.IsTask+','+a.PrimaryWhoId+','+a.PrimaryWho.name+','+a.isclosed+'\n'; 
                   finalactivityHeader = finalactivityHeader + OpenActrecordString;
               }
           }
           
       }
       else{
           for(Opportunity o:[select id,name,Opportunity_Number__c,ownerid,Accountid,stagename,amount,closedate,type,recordtype.name,ProductGroupTest__c,Pricebook2id,Roll_Out_Start__c,Rollout_Duration__c,Roll_Out_End_Formula__c,ForecastAmount__c,QuotePlanVariance__c,Roll_Out_Status__c,Is_Pilot_Order__c,Plan_Variance__c,Reason__c,Deal_Registration_Approval__c,Primary_Distributor__c,Primary_Reseller__c,LeadSource,Product_Information__c,CompetitiveInformation__c,Managed_Print_Services_Applicable__c,Historical_Comments__c,createddate,createdbyid,lastmodifieddate from Opportunity where Opportunity_Number__c=:OppNumList]){
               opid.add(o.id);
               acid.add(o.Accountid);
           }
           
           for(OpportunityLineItem oli:[select id,Opportunityid,Opportunity.Name,Product2.Name,productId__c,SAP_Material_Id__c,UnitPrice,Quantity,ListPrice from OpportunityLineItem where Opportunity.id in :opid ]){
               oli.UnitPrice=1;
               oli.Quantity=1;
               integer LstPrice=1;
               integer reqprice=1;
               integer Discount=0;
               integer TotalPrice=1;
               String OppName = 'OppName'+oli.Opportunityid;
               string recordString1 = oli.id + ',' + oli.Opportunityid +','+OppName+','+oli.productId__c+','+oli.SAP_Material_Id__c+','+oli.ProductGroup__c+','+oli.ProductType__c+','+oli.Alternative__c+','+oli.UnitPrice+','+oli.Quantity+','+LstPrice +','+reqprice+','+TotalPrice+','+Discount +'\n'; 
               finalstr1 = finalstr1 + recordString1;
           }
           
           for(opportunitycompetitor c : [select id, opportunityid, competitorname, strengths, weaknesses from opportunitycompetitor where opportunityid=:opid]){
               string strengths;
               if(c.strengths==null||c.strengths==''){
                   strengths='null';
               }else{
                   strengths = (c.strengths).replace('\r\n','-');
                   strengths = strengths.replace('\n','-');
                   strengths = strengths.replace('\r','-');
                   strengths = strengths.replace(',',''); 
                   
               }
               String OppName = 'OppName'+c.Opportunityid;
               //c.Weaknesses='null';
               //string compRecordString = c.id +','+c.opportunityid+','+(c.competitorname).replace(',','') +','+(c.strengths).replace(',','')+','+(c.weaknesses).replace(',','')+'\n';
               string compRecordString = c.id +','+c.opportunityid+','+OppName+','+(c.competitorname).replace(',','') +','+strengths+'\n';
               finalCompString = finalCompString + compRecordString;     
           }
           
           for(order o : [select id,name,opportunityid,status,OrderNumber,type,EffectiveDate,createddate,RequestedShipmentDate__c,Product_Group__c,CustomerAuthorizedByid,ShipToContactid,BillToContactid,ShippingStreet,ShippingCity,ShippingState,ShippingCountry,ShippingPostalcode from order where opportunityid=:opid]){
               o.name = 'demo'+o.id;
               o.shippingstreet = 'ShippingStreet';
               o.shippingcity = 'ShippingCity';
               o.ShippingState = 'ShippingState';
               o.ShippingCountry = 'ShippingCountry';
               o.ShippingPostalcode = 'ShippingPostalcode';
               string OppName = 'OppName'+o.opportunityid;
               string Contact_Name = 'ConatctName'+o.CustomerAuthorizedByid;
               string ShipToContact = 'ConatctName'+o.ShipToContactid;
               string BillToContact = 'ConatctName'+o.BillToContactid;
               
               string EffectiveDate = String.valueOf(o.EffectiveDate);
               String DemoRecordString = o.id+','+(o.name).replace(',','')+','+o.Opportunityid+','+OppName+','+o.status+','+o.OrderNumber+','+o.Type+','+EffectiveDate+','+o.RequestedShipmentDate__c+','+o.Product_Group__c+','+o.CustomerAuthorizedByid+','+Contact_Name+','+o.ShipToContactid+','+ShipToContact+','+o.BillToContactid+','+BillToContact+','+o.ShippingStreet+','+o.ShippingCity+','+o.ShippingState+','+o.ShippingCountry+','+o.ShippingPostalcode+','+o.createddate+'\n';
               finalDemoString = finalDemoString + DemoRecordString;
           }
           
           for(Partner__c p : [select id,partner__c,PartnerName__c,Role__c,Is_Primary__c,Registered__c,Role_is_Reseller__c,Opportunity__c,Opportunity__r.name from Partner__c where Opportunity__c=:opid]){
               string partnername='Account'+p.Partner__c; 
               String OppName = 'OppName'+p.Opportunity__c;
               string partnerRecString = p.id +','+ p.Partner__c +','+partnername+','+p.Role__c+','+p.Is_Primary__c+','+p.Registered__c+','+p.Opportunity__c+','+OppName+'\n';
               OppPartnerString = OppPartnerString + partnerRecString;
           }
           
           for(Partner__c p : [select id,partner__c,PartnerName__c,Role__c,Is_Primary__c,Role_is_Reseller__c,Opportunity__c,Opportunity__r.name from Partner__c where Partner__r.RecordType.name='Indirect' and Opportunity__c=:opid]){
               string partnername='Account'+p.Partner__c;
               string partnerRecordString2 = p.id +','+ p.Partner__c +','+partnername+','+p.Role__c+','+p.Is_Primary__c+','+p.Role_is_Reseller__c+','+p.Opportunity__c+','+'\n';
               finalPartnerString2 = finalPartnerString2 + partnerRecordString2;
           }
           for(Channel__c p : [select id,name,Opportunity__c,Opportunity__r.name,DistributorName__c,Distributor__c,Partner_Distributor__c,Partner_Reseller__c,Reseller__c,ResellerName__c from Channel__c where Opportunity__r.id=:opid]){
               string ResellerName='name'+p.Reseller__c;
               string DistributorName='name'+p.Distributor__c;
               String OppName = 'OppName'+p.Opportunity__c;
               string channelRecordString = p.id +','+p.Opportunity__c+','+OppName+','+DistributorName+','+p.Distributor__c+','+ResellerName+','+p.Reseller__c+'\n';
               //string channelRecordString = p.id +','+ p.name +','+p.Opportunity__c+','+p.Distributor__c+','+p.Reseller__c+','+p.Partner_Distributor__c+','+p.Partner_Reseller__c+'\n';
               finalchannelString = finalchannelString + channelRecordString;
           }
           
           for(OpportunityTeamMember team : [SELECT id,Opportunityid,name,userid,teammemberrole,OpportunityAccessLevel FROM OpportunityTeamMember where opportunityid=:opid]){
               string username = 'username'+team.userid;
               string OppName = 'OppName'+team.opportunityid;
               string TeamRecordString = team.id +','+team.opportunityid+','+OppName+','+username +','+team.userid+','+(team.teammemberrole).replace(',','')+','+team.OpportunityAccessLevel+'\n';
               finalTeamString = finalTeamString + TeamRecordString;     
           }
           
           for(Opportunity opp : [select id,(SELECT id,accountid,whatid,what.name,who.name,activitydate,ActivityType,CallType,EndDateTime,IsTask,OwnerId,PrimaryWhoId,Status,Subject,isclosed FROM ActivityHistories),(select id,accountid,whatid,what.name,who.name,activitydate,ActivityType,CallType,EndDateTime,IsTask,OwnerId,PrimaryWhoId,Status,Subject,isclosed from OpenActivities) from opportunity where id=:opid]){
               for(ActivityHistory a: opp.ActivityHistories){
                   string activitydate;
                   string WhatName = 'Name'+a.whatid;
                   string AccountName = 'Name'+a.accountid;
                   string PrimarywhoName = 'Name'+a.PrimaryWhoId;
                   if(a.activitydate==null){
                        activitydate='null';
                    }
                    else{
                        Date dt = Date.newInstance(a.activitydate.year(),a.activitydate.month(),a.activitydate.day());
                        activitydate = String.valueOf(dt);
                    }
                   string subject;
                   if(a.Subject==null||a.Subject==''){
                       subject='null';
                   }else{
                       subject = (a.Subject).replace(',','');
                   }
                   string ActivityrecordString = a.id + ',' + a.accountid +','+AccountName+','+a.whatid+','+WhatName+','+a.who.name+','+activitydate+','+a.ActivityType+','+subject+','+a.Status+','+a.EndDateTime+','+a.OwnerId+','+a.IsTask+','+a.PrimaryWhoId+','+PrimarywhoName+','+a.isclosed+'\n'; 
                   finalactivityHeader = finalactivityHeader + ActivityrecordString;
               }
               for(OpenActivity a: opp.OpenActivities){
                   string subject;
                   string activitydate;
                   string WhatName = 'Name'+a.whatid;
                   string AccountName = 'Name'+a.accountid;
                   string PrimarywhoName = 'Name'+a.PrimaryWhoId;
                   if(a.activitydate==null){
                        activitydate='null';
                    }
                    else{
                        Date dt = Date.newInstance(a.activitydate.year(),a.activitydate.month(),a.activitydate.day());
                        activitydate = String.valueOf(dt);
                    }
                   if(a.Subject==null||a.Subject==''){
                       subject='null';
                   }else{
                       subject = (a.Subject).replace(',','');
                   }
                   string OpenActrecordString = a.id + ',' + a.accountid+','+AccountName +','+a.whatid+','+WhatName+','+a.who.name+','+activitydate+','+a.ActivityType+','+subject+','+a.Status+','+a.EndDateTime+','+a.OwnerId+','+a.IsTask+','+a.PrimaryWhoId+','+PrimarywhoName+','+a.isclosed+'\n'; 
                   finalactivityHeader = finalactivityHeader + OpenActrecordString;
               }
           }
           
       }
       batchcount++;
    }
        
    global void finish(Database.Batchablecontext BC){
        system.debug('BatchCount------->'+batchcount);
        //for PBEs
        if(Sample==false){
            for(PricebookEntry p : [select id,product2id,product2.SAP_Material_ID__c,pricebook2id,pricebook2.name,pricebook2.SAP_Price_Account_Code__c,product2.family,product2.Product_Group__c,UnitPrice from PricebookEntry where (pricebook2id='01s36000006dVCZ' or pricebook2id='01s36000006dVCa' or pricebook2id='01s36000006e12c' or pricebook2id='01s36000006dVCb' or pricebook2id='01s36000006dVCW') and product2.family='PRINTER [C2]']){
                string PBErecordstring = p.id +','+ p.product2id +','+ p.product2.SAP_Material_ID__c +','+ p.product2.family +','+ p.product2.Product_Group__c +','+ p.UnitPrice +','+ p.pricebook2id +','+ p.pricebook2.name +'\n';
                finalstrPBE =finalstrPBE + PBErecordstring;
            }
        }
        else{
            for(PricebookEntry p : [select id,product2id,product2.SAP_Material_ID__c,pricebook2id,pricebook2.name,pricebook2.SAP_Price_Account_Code__c,product2.family,product2.Product_Group__c,UnitPrice from PricebookEntry where (pricebook2id='01s36000006dVCZ' or pricebook2id='01s36000006dVCa' or pricebook2id='01s36000006e12c' or pricebook2id='01s36000006dVCb' or pricebook2id='01s36000006dVCW') and product2.family='PRINTER [C2]']){
                decimal UnitPrice = 0.99;
                string PBErecordstring = p.id +','+ p.product2id +','+ p.product2.SAP_Material_ID__c +','+ p.product2.family +','+ p.product2.Product_Group__c +','+ UnitPrice +','+ p.pricebook2id +','+ p.pricebook2.name +'\n';
                finalstrPBE =finalstrPBE + PBErecordstring;
            }
        }
        Blob b = Blob.valueof(finalstrPBE);
        Messaging.EmailFileAttachment efa1 = new Messaging.EmailFileAttachment();
        efa1.setFileName('PBE_List.csv');
        efa1.setBody(b);
        
        //Demos
            Blob b8 = Blob.valueof(finalDemoString);
            Messaging.EmailFileAttachment efa8 = new Messaging.EmailFileAttachment();
            efa8.setFileName('Opp_DemoList.CSV');
            efa8.setBody(b8);
        
        //Contact Roles
            Blob b9 = Blob.valueof(finalOCRstring);
            Messaging.EmailFileAttachment efa9 = new Messaging.EmailFileAttachment();
            efa9.setFileName('Opp_ContactRoles.CSV');
            efa9.setBody(b9);
        
        //Competitors
            Blob b10 = Blob.valueof(finalCompString );
            Messaging.EmailFileAttachment efa10 = new Messaging.EmailFileAttachment();
            efa10.setFileName('Opp_CompetitorList.CSV');
            efa10.setBody(b10);
            
        //Opp Partners
            Blob oppptr = Blob.valueof(OppPartnerString);
            Messaging.EmailFileAttachment efaoppptr = new Messaging.EmailFileAttachment();
            efaoppptr.setFileName('Opp_Partner.CSV');
            efaoppptr.setBody(oppptr);
            
        //Acc Partners
            Blob Accptr = Blob.valueof(finalPartnerString);
            Messaging.EmailFileAttachment efaAccptr = new Messaging.EmailFileAttachment();
            efaAccptr.setFileName('Acc_Partner.CSV');
            efaAccptr.setBody(Accptr);    
            
        //Opp Channel relationship
            Blob channel = Blob.valueof(finalchannelString);
            Messaging.EmailFileAttachment efaChannel = new Messaging.EmailFileAttachment();
            efaChannel.setFileName('Opp_Channel_Relationship.CSV');
            efaChannel.setBody(channel);    
        
        //Opp Activity
            Blob ptr = Blob.valueof(finalactivityHeader);
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName('Opp_Activity.CSV');
            efa.setBody(ptr);
         
         //Opportunity Line Items
            Blob b14 = Blob.valueof(finalstr1);
            Messaging.EmailFileAttachment efa14 = new Messaging.EmailFileAttachment();
            efa14.setFileName('Opp_Lineitem.CSV');
            efa14.setBody(b14);
            
        //finalPartnerString2
            Blob fps = Blob.valueof(finalPartnerString2);
            Messaging.EmailFileAttachment efafps = new Messaging.EmailFileAttachment();
            efafps.setFileName('Partner.CSV');
            efafps.setBody(fps);
            Messaging.SingleEmailMessage emailfps = new Messaging.SingleEmailMessage();
            list<string> emailaddressesfps = new list<string>{Email};
            emailfps.setSubject( 'Printer data files' );
            emailfps.setToAddresses( emailaddressesfps );
            string bodyfps ='Hi'+'\n\n'+'Thanks';
            emailfps.setPlainTextBody( 'Hi ' );
           
         //Team Members
            Blob b15 = Blob.valueof(finalTeamString);
            Messaging.EmailFileAttachment efa15 = new Messaging.EmailFileAttachment();
            efa15.setFileName('Opp_Team.CSV');
            efa15.setBody(b15);
            Messaging.SingleEmailMessage email15 = new Messaging.SingleEmailMessage();
            list<string> emailaddresses15 = new list<string>{Email};
            email15.setSubject( 'Printer data files' );
            email15.setToAddresses( emailaddresses15 );
            string body15 ='Hi'+'\n\n'+'Thanks';
            email15.setPlainTextBody( 'Hi ' );
            email15.setFileAttachments(new Messaging.EmailFileAttachment[] {efa15,efaoppptr,efa10,efa8,efa1,efa14,efaChannel,efa});
            Messaging.SendEmailResult [] r15 = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email15});
    }
}