//Batch for Downloading all the Printer Opportunity Rollout Schedules
Global class BatchforCSV3 implements Database.Batchable<sObject>, Database.stateful{
    global Map<String, Schema.SObjectField> objectFields;
    global List<String> fieldNames;
    global String Name;
    global String finalrollschHeader;
    global integer batchcount;
    global integer batchHeaps;
    global integer num;
    global string Email;
    
    // Batch Constructor
    global BatchforCSV3(integer num,string Email){
        this.num=num;
        this.Email = Email;
        objectFields = Schema.getGlobalDescribe().get('Opportunity').getDescribe().fields.getMap();
        DescribeSObjectResult describeResult = Opportunity.getSObjectType().getDescribe();      
        //fieldNames = new List<String>( describeResult.fields.getMap().keySet() );
        fieldnames = new List<String>(objectFields.keySet());
        Name = describeResult.getName();
        batchcount=0;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        /*
        List<String> includesList = new List<String>{'\'PRINTER\'','\'A3 COPIER\'','\'FAX/MFP\'','\'SOLUTION S/W\''};
        Id RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('IT').getRecordTypeId();
        String queryString = ' SELECT ' + String.join( fieldNames, ',' ) +  ' FROM ' + Name + ' WHERE ProductGroupTest__c INCLUDES(';
        for(String includeValue :includesList){
            queryString += includeValue + ',';
        }    
        queryString = queryString.removeEnd(',') +')'+'and recordtypeid=:RecordTypeId and (CALENDAR_YEAR(Roll_Out_Start__c)>=2016 or CALENDAR_YEAR(createddate)>=2016) order by createddate limit '+num;
        List<Opportunity> opplist = (List<Opportunity>)Database.query(queryString);
        */
        string queryString = 'select id,Opportunity_No__c from Printer_Data_Export__c  Where Batch_Number__c= '+num;
        List<Printer_Data_Export__c> PDElist = (List<Printer_Data_Export__c>)Database.query(queryString);
        return Database.getQueryLocator(queryString);
    }
    
    global void execute (Database.Batchablecontext BC, list<Printer_Data_Export__c> scope){
       string rollschHeader = 'id,product,Plan_Date,Plan_Quantity,Plan_Revenue,Quote_Quantity,MonthYear,Number of Roll Out Schedules,OpportunityProductID,OpportunityID \n';
       finalrollschHeader = rollschHeader ;
       set<id> opid=new set<id>();
       List<String> OppNumList = new List<String>(); 
       for(Printer_Data_Export__c p : Scope){
           OppNumList.add(p.Opportunity_No__c);
       }
       for(Opportunity o:[select id,Opportunity_Number__c from Opportunity where Opportunity_Number__c=:OppNumList]){
           opid.add(o.id);
       }
       for(Roll_Out_Schedule__c rollsch : [select id,Roll_Out_Product__r.SAP_Material_Code__c,Plan_Date__c,Plan_Quantity__c,Plan_Revenue__c,Quote_Quantity__c,Actual_Quantity__c,Actual_Revenue__c,fiscal_week__c,fiscal_month__c,MonthYear__c,year__c,Invoice_Date__c,Roll_Out_Product__r.Number_of_Roll_Out_Schedules__c,roll_out_product__r.roll_out_plan__r.opportunity__r.name,Roll_Out_Product__r.Opportunity_Line_Item__c from roll_out_schedule__c where roll_out_product__r.roll_out_plan__r.opportunity__r.id=:opid]){
           string Plan_Date = String.valueOf(rollsch.Plan_Date__c);
           //string name = rollsch.roll_out_product__r.roll_out_plan__r.opportunity__r.name;
           //string rollschrecordString = rollsch.id + ',' + rollsch.Roll_Out_Product__r.SAP_Material_Code__c +','+rollsch.Plan_Date__c+','+rollsch.Plan_Quantity__c+','+rollsch.Plan_Revenue__c+','+rollsch.Quote_Quantity__c+','+rollsch.Actual_Quantity__c+','+rollsch.Actual_Revenue__c+','+rollsch.fiscal_week__c+','+rollsch.fiscal_month__c+','+rollsch.MonthYear__c+','+rollsch.year__c+','+rollsch.Invoice_Date__c+','+rollsch.Roll_Out_Product__r.Number_of_Roll_Out_Schedules__c+','+rollsch.Roll_Out_Product__r.Opportunity_Line_Item__c +','+rollsch.roll_out_product__r.roll_out_plan__r.opportunity__c+ '\n'; 
           string rollschrecordString = rollsch.id + ',' + rollsch.Roll_Out_Product__r.SAP_Material_Code__c +','+Plan_Date+','+rollsch.Plan_Quantity__c+','+rollsch.Plan_Revenue__c+','+rollsch.Quote_Quantity__c+','+rollsch.MonthYear__c+','+rollsch.Roll_Out_Product__r.Number_of_Roll_Out_Schedules__c+','+rollsch.Roll_Out_Product__r.Opportunity_Line_Item__c +','+rollsch.roll_out_product__r.roll_out_plan__r.opportunity__c+ '\n'; 
           finalrollschHeader = finalrollschHeader + rollschrecordString;
       }
       
       batchcount++;
       //Roll_Out_Schedules
            Blob b7 = Blob.valueof(finalrollschHeader);
            Messaging.EmailFileAttachment efa7 = new Messaging.EmailFileAttachment();
            efa7.setFileName('RollOutSchedules'+batchcount+'.CSV');
            efa7.setBody(b7);
            Messaging.SingleEmailMessage email7 = new Messaging.SingleEmailMessage();
            list<string> emailaddresses7 = new list<string>{Email};
            email7.setSubject( 'Rollout Schedules_'+batchcount+' LIST');
            email7.setToAddresses( emailaddresses7 );
            string body7 ='Hi'+'\n\n'+'Thanks';
            email7.setPlainTextBody( 'Hi ' );
            email7.setFileAttachments(new Messaging.EmailFileAttachment[] {efa7});
            Messaging.SendEmailResult [] r7 = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email7});
    }
        
    global void finish(Database.Batchablecontext BC){
        system.debug('BatchCount------->'+batchcount);
    }
}