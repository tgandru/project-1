//Batch for Contract integration. Getting BO Number from Sales Portal for the HA Win/Completed Opportunity
global class BatchforHAContractIntegrationCall implements Database.Batchable<sObject>, Database.AllowsCallouts, Schedulable {
    
    global void execute(SchedulableContext SC) {
        BatchforHAContractIntegrationCall ba = new BatchforHAContractIntegrationCall();
        database.executeBatch(ba,1);
    }
    
    global id HArecordtypeid;
    global BatchforHAContractIntegrationCall(){
        HArecordtypeid =  Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('HA Builder').getRecordTypeId();
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        //return Database.getQueryLocator(query);
        database.Querylocator ql = database.getquerylocator([SELECT MSGSTATUS__c,  Status__c, retry_counter__c, Integration_Flow_Type__c, CreatedBy.Name, Identification_Text__c, ParentMQ__c,ParentMQ__r.status__c, ERRORTEXT__c FROM message_queue__c where Integration_Flow_Type__c='Sales Portal' and (Status__c='Not Started' or Status__c='failed-retry') limit 10]);
        return ql;
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        List<message_queue__c> mqUpsert = new List<message_queue__c>();
        Set<string> QuoteList = new Set<String>();
        for(message_queue__c mq: (List<message_queue__c>)scope) {
            System.debug('lclc in '+mq.Integration_Flow_Type__c + mq.Id);
            QuoteList.add(mq.Identification_Text__c);
            try{
                if(mq.Integration_Flow_Type__c=='Sales Portal'){
                    HAContractIntegrationCall.calloutportal(mq);
                }
            }
            catch(Exception ex){
                String errmsg = 'MSG='+ex.getmessage() + ' - '+ex.getStackTraceString();
                system.debug('## getmessage'+errmsg);
            }
        }
        /*for(Opportunity o : [select id,SBQQ__PrimaryQuote__c,stagename,Sales_Portal_BO_Number__c from Opportunity where recordtypeid=:HArecordtypeid and SBQQ__PrimaryQuote__c=:QuoteList]){
            if(o.Sales_Portal_BO_Number__c!=null){
                message_queue__c m = new message_queue__c(Identification_Text__c=o.Sales_Portal_BO_Number__c, Integration_Flow_Type__c='SO Number',
                                                            retry_counter__c=0,Status__c='not started', Object_Name__c='SO Number');
                mqUpsert.add(m);
            }
        }
        upsert mqUpsert;*/
    }
    
    global void finish(Database.BatchableContext BC) {
        // Sending BO Number to get SO Number.
        //Database.executeBatch(new BatchforHAbalancedqtyCallout(),100);
    }
}