@isTest
private class BatchforHAContractIntegrationCallTest{
    
    static Id HADistyRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HA Builder Distributor').getRecordTypeId(); 
    static Id HART = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HA Builder').getRecordTypeId(); 
    public Static String CRON_EXP = '0 0 11 15 12 ?'; //To execute schedulable 
     
    
    @testSetup static void setup() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='Flow-051';
        iep2.endPointURL__c = 'https://www.google.com';
        iep2.layout_id__c = 'LAY024197';
        iep2.partial_endpoint__c = '/SFDC_IF_US_051';
        insert iep2;
        
        list<account> accts=new list<account>();
        account a2=TestDataUtility.createAccount('dpba stuff');
        a2.RecordTypeId = HADistyRT;
        a2.sap_company_code__C='5554444';
        accts.add(a2);
        insert accts;
        
        id opprtid = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('HA Builder').getRecordTypeId();
        Opportunity opp = new Opportunity(name='HA',Accountid=a2.id,recordtypeid=opprtid,closedate=Date.valueOf(System.Today()),stagename='Proposal',Project_Name__c='Test',Expected_Close_Date__c=Date.valueOf(System.Today()),Number_of_Living_Units__c=100);
        insert opp;
        
        id qtrtid = Schema.SObjectType.SBQQ__Quote__c.getRecordTypeInfosByName().get('HA Builder').getRecordTypeId();
        SBQQ__Quote__c qt = new SBQQ__Quote__c(SBQQ__Type__c='Quote',SBQQ__ExpirationDate__c=Date.valueOf(System.Today()),Expected_Shipment_Date__c=Date.valueOf(System.Today())+10,Last_Shipment_Date__c=Date.valueOf(System.Today())+30,recordtypeid=qtrtid,SBQQ__Opportunity2__c=opp.id);
        insert qt;
        
        //Creating custom Pricebook
        PriceBook2 pb = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(4)); 
        insert pb;
        
        //Create Products
        List<Product2> prods=new List<Product2>();
        Product2 standaloneProd=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Standalone Prod1', 'LAUNDRY', 'LAUNDRY', 'H/W');
        prods.add(standaloneProd);
        Product2 parentProd1=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Parent Prod1', 'DISH', 'DISH', 'H/W');
        prods.add(parentProd1);
        
        insert prods;

        Id pricebookId = Test.getStandardPricebookId();

        //CreatePBEs
        //create the pricebook entries for the custom pricebook
        List<PriceBookEntry> custompbes=new List<PriceBookEntry>();
        PriceBookEntry standaloneProdPBE=TestDataUtility.createPriceBookEntry(standaloneProd.Id, pb.Id);
        custompbes.add(standaloneProdPBE);
        PriceBookEntry parentProd1PBE=TestDataUtility.createPriceBookEntry(parentProd1.Id, pb.Id);
        custompbes.add(parentProd1PBE);
        insert custompbes;
        
        List<SBQQ__QuoteLine__c> quotelines = new List<SBQQ__QuoteLine__c>();
        SBQQ__QuoteLine__c line1 = new SBQQ__QuoteLine__c(SBQQ__Quote__c=qt.id,SBQQ__Product__c=standaloneProd.id,SBQQ__Quantity__c=50,Package_Option__c='BASE');
        quotelines .add(line1);
        SBQQ__QuoteLine__c line2 = new SBQQ__QuoteLine__c(SBQQ__Quote__c=qt.id,SBQQ__Product__c=parentProd1.id,SBQQ__Quantity__c=40,Package_Option__c='BASE');
        quotelines .add(line2);
        insert quotelines;
    }
    @isTest static void test_method_one() {
        SBQQ__Quote__c quot=[select Id,SBQQ__Status__c from SBQQ__Quote__c limit 1];
        quot.SBQQ__Status__c='Approved';
        update quot;
        Opportunity Opp =[select id,stagename from Opportunity limit 1];
        //opp.stagename='Won';
        opp.Sales_Portal_BO_Number__c='BO0000034';
        update Opp;
        
        List<message_queue__c> insertList = new List<message_queue__c>();
        message_queue__c mq = new message_queue__c(Identification_Text__c=quot.id, Integration_Flow_Type__c='Sales Portal',retry_counter__c=0,Status__c='not started', Object_Name__c='HA Quote');
        insertList.add(mq);
        
        insert insertList;
        
        Test.startTest();
        
        //List<message_queue__c> mqList = [SELECT MSGSTATUS__c,  Status__c, retry_counter__c, Integration_Flow_Type__c, CreatedBy.Name, Identification_Text__c, ParentMQ__c,ParentMQ__r.status__c, ERRORTEXT__c FROM message_queue__c where Integration_Flow_Type__c='Sales Portal' and (Status__c='Not Started' or Status__c='failed-retry') limit 10];
        
        id jobId = System.schedule('TestScheduledBatch',CRON_EXP, new BatchforHAContractIntegrationCall());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];  
        System.assertEquals(CRON_EXP, ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
        String SCHEDULE_NAME = 'TestScheduledBatch'; 
        
        Test.stopTest();    
        
    }
    
}