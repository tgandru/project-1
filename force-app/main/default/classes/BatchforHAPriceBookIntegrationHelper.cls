global class BatchforHAPriceBookIntegrationHelper implements Database.Batchable<sObject>, Database.AllowsCallouts{

  //public static String query = 'SELECT name FROM PriceBook2 where name=\'HA Builder EDCP\'';
  //global boolean updatepbe;
  global Database.QueryLocator start(Database.BatchableContext BC) {
        database.Querylocator ql = database.getquerylocator([select id, name,pricebook2.name,Product2.Id, Product2.SAP_Material_ID__c
                                        from PriceBookEntry 
                                        where PriceBook2.name='HA Builder EDCP' or PriceBook2.name='HA Special Price Book']);
        return ql;
        //return Database.getQueryLocator(query);
  }
    
  global void execute(Database.BatchableContext BC, List<sObject> scope) {
        
     /*for(pricebook2 p: (list<pricebook2>) scope){
          if(Label.Update_PBE == True){
              updatepbe = True;
          }else{
              updatepbe = False;
          }
          */
          HAPriceBookIntegrationHelper_v2 obj= new HAPriceBookIntegrationHelper_v2();
          obj.updatePriceBook((list<PriceBookEntry>) scope);
          //obj.updatePriceBook((list<PriceBookEntry>) scope, updatepbe);
        //}

  }
    
  global void finish(Database.BatchableContext BC) {
  
  }
}