/* 
Batch for Updating the Rollout Schedules Actuals and Opp Lines with Shipped quantity from GERP
Author : Thiru
Deployed Date  : 01/10/2019

Version Changes:
By:Thiru
Date: 06/13/2019
Change Made: Replace DO_Created_Date__c with Req_Delivery_Date__c for updating Rollout Actual Qty
By:Thiru
Date: 10/18/2019
Change Made: Add logic to update ROS's Shipped_Quantity__c(new field) with SO Line's DO_Qty__c
*/

global class BatchforHARolloutActualsUpdateMigration implements Database.Batchable<AggregateResult>,Database.stateful {
    global Map<id,string> rolloutidNameMap;
    
    global BatchforHARolloutActualsUpdateMigration(){
        rolloutidNameMap = new Map<id,string>();
    }
    
    global Iterable<AggregateResult> start(Database.BatchableContext BC) {
        string query = 'select count(id) cnt, Opportunity__c from so_lines__c group by Opportunity__c';
        return new AggregateResultIterable(query);
    }
    
    global void execute(Database.BatchableContext bc, List<sObject> scope) {
        List<Roll_Out_Schedule__c> rosList = new List<Roll_Out_Schedule__c>();
        Map<id,Roll_Out_Schedule__c> RosMap = new Map<id,Roll_Out_Schedule__c>();
        Set<String> oppIds = new Set<String>();
        
        Map<String,List<SO_Lines__c>> prodSoLineMap = new Map<String,List<SO_Lines__c>>();
        
        for(sObject sObj : scope){
            AggregateResult res = (AggregateResult)sObj;
            String OppId        = (id)res.get('Opportunity__c');
            system.debug('oppty Id---->'+OppId);
            integer soLineCount = (Integer)res.get('cnt');
            
            if(soLineCount>0){
                oppIds.add(OppId);
            }
        }
        
        for(so_lines__c line : [Select id,Opportunity__c,Opportunity__r.Roll_out_start__c,DO_Qty__c,Order_Qty__c,
                                       Shipped_QTY__c,Delivery_Order_Date__c,DO_Created_Date__c,SAP_Material_ID__c,
                                       Req_Delivery_Date__c 
                                from SO_Lines__c 
                                where Opportunity__c=:oppIds])
        {
            if(prodSoLineMap.containsKey(line.SAP_Material_ID__c)){
                List<SO_Lines__c> soLines = prodSoLineMap.get(line.SAP_Material_ID__c);
                soLines.add(line);
                prodSoLineMap.put(line.SAP_Material_ID__c,soLines);
            }else{
                prodSoLineMap.put(line.SAP_Material_ID__c,new List<SO_Lines__c>{line});
            }
            
        }
        
        for(Roll_Out_Schedule__c ros : [select id,Roll_Out_Product__c,Roll_Out_Product__r.Roll_out_plan__c,Roll_Out_Product__r.Roll_out_plan__r.name,
                                              Roll_Out_Product__r.SAP_Material_Code__c,Roll_Out_Product__r.Roll_out_plan__r.Opportunity__c,
                                              Roll_Out_Product__r.Roll_out_plan__r.Opportunity__r.Roll_out_start__c,Opportunity_Id__c,
                                              Actual_Quantity__c,Actual_Revenue__c,Invoice_Date__c,Plan_Date__c,Plan_Quantity__c,Shipped_Quantity__c 
                                              from Roll_Out_Schedule__c 
                                              where Roll_Out_Product__r.Roll_out_plan__r.Opportunity__c=:oppIds 
                                                and Roll_Out_Product__r.SAP_Material_Code__c=:prodSoLineMap.keySet()  
                                              order by Roll_Out_Product__r.Roll_out_plan__r.Opportunity__c asc])
        {
            
            string oppid                 = ros.Roll_Out_Product__r.Roll_out_plan__r.Opportunity__c;
            date   oppRollOutStartMonday = RosMiscUtilities.getWeekBeginDate(ros.Roll_Out_Product__r.Roll_out_plan__r.Opportunity__r.Roll_out_start__c);
            date   weekStartDate         = ros.Plan_Date__c;
            date   weekendDate           = ros.Plan_Date__c+6;
            integer shipqty              = 0;
            
            integer doQty                = 0; //Added by Thiru--10/18/2019
            
            if(prodSoLineMap.get(ros.Roll_Out_Product__r.SAP_Material_Code__c)!=null){
                for(SO_Lines__c line : prodSoLineMap.get(ros.Roll_Out_Product__r.SAP_Material_Code__c)) {
                    if(line.Opportunity__c==ros.Roll_Out_Product__r.Roll_out_plan__r.Opportunity__c){
                        //integer DO_Qty = integer.valueOf(line.DO_Qty__c);
                        //if(line.DO_Qty__c!=null && DO_Qty!=0){
                        integer Order_Qty = integer.valueOf(line.Order_Qty__c);
                        if(line.Order_Qty__c!=null && Order_Qty!=0){
                            if(line.Req_Delivery_Date__c!=null && line.Req_Delivery_Date__c<oppRollOutStartMonday && weekStartDate==oppRollOutStartMonday){//Thiru 06/13/2019::Added to put all DO_QTY before the rollout start date in first rollout schedule
                                shipqty = shipqty + integer.valueOf(line.Order_Qty__c);//+ integer.valueOf(line.DO_Qty__c);
                            }
                            if(line.Req_Delivery_Date__c!=null && line.Req_Delivery_Date__c>=weekStartDate && line.Req_Delivery_Date__c<=weekendDate){//Added on 06/13/2019
                                shipqty = shipqty + integer.valueOf(line.Order_Qty__c);//+ integer.valueOf(line.DO_Qty__c);
                            }
                        }
                        
                        //Added by Thiru--10/18/2019--Starts
                        integer DO_Qty = integer.valueOf(line.DO_Qty__c);
                        if(line.DO_Qty__c!=null && DO_Qty!=0){
                            if(line.Req_Delivery_Date__c!=null && line.Req_Delivery_Date__c<oppRollOutStartMonday && weekStartDate==oppRollOutStartMonday){//Thiru 06/13/2019::Added to put all DO_QTY before the rollout start date in first rollout schedule
                                doQty   = doQty   + integer.valueOf(line.DO_Qty__c);
                            }
                            if(line.Req_Delivery_Date__c!=null && line.Req_Delivery_Date__c>=weekStartDate && line.Req_Delivery_Date__c<=weekendDate){//Added on 06/13/2019
                                doQty   = doQty   + integer.valueOf(line.DO_Qty__c);
                            }
                        }
                        //Added by Thiru--10/18/2019--Ends
                    }
                }
            }
            
            if(ros.Actual_Quantity__c==null){
                ros.Actual_Quantity__c=0;
            }
            
            //Commented Below by Thiru--10/18/2019
            /*if(ros.Actual_Quantity__c!=shipqty){
                ros.Actual_Quantity__c = shipqty;
                rosList.add(ros);
                RosMap.put(ros.id,ros);
            }*/
            
            //Added by Thiru--10/18/2019--Starts
            if(ros.Shipped_Quantity__c==null){
                ros.Shipped_Quantity__c=0;
            }
            
            if(ros.Actual_Quantity__c!=shipqty || ros.Shipped_Quantity__c!=doQty){
                if(ros.Actual_Quantity__c!=shipqty){
                    ros.Actual_Quantity__c = shipqty;
                }
                if(ros.Shipped_Quantity__c!=doQty){
                    ros.Shipped_Quantity__c = doQty;
                }
                rosList.add(ros);
                RosMap.put(ros.id,ros);
            }
            //Added by Thiru--10/18/2019--Ends
            
        }
        
        if(rosList.size()>0){
            Database.SaveResult[] SaveResult = Database.update(rosList, false);
            for (Database.SaveResult sr : SaveResult) {
                if(sr.isSuccess()){
                    string successId = sr.getId();
                    if(RosMap.size()>0 && RosMap.containsKey(successId)){
                        Roll_Out_Schedule__c ros = RosMap.get(successId);
                        rolloutidNameMap.put(ros.Roll_Out_Product__r.Roll_out_plan__c, ros.Roll_Out_Product__r.Roll_out_plan__r.name);
                    }
                }
            }
        }
        
        //Block for updating Opportunity Products with shipped quantity
        List<OpportunityLineItem> oliUpdateList = new List<OpportunityLineItem>();
        List<OpportunityLineItem> OliList = new List<OpportunityLineItem>();
       
        for(OpportunityLineitem oli : [Select id,Opportunityid,Sale_Portal_Shipped_Quantity__c,SAP_Material_Id__c,Product2.SAP_Material_ID__c 
                                       from Opportunitylineitem 
                                       where OpportunityId=:oppIds and Product2.SAP_Material_ID__c=:prodSoLineMap.keySet() order by OpportunityId asc])
        {
            integer shipqty = 0;
            
            if(prodSoLineMap.get(oli.Product2.SAP_Material_ID__c)!=null){
                for(SO_Lines__c line : prodSoLineMap.get(oli.Product2.SAP_Material_ID__c)){
                    if(line.Opportunity__c==oli.Opportunityid){
                        /*
                        integer DO_Qty = integer.valueOf(line.DO_Qty__c);
                        if(line.DO_Qty__c!=null && DO_Qty!=0){
                            shipqty = shipqty + integer.valueOf(line.DO_Qty__c);
                        }
						*/
                        integer Order_Qty = integer.valueOf(line.Order_Qty__c);
                        if(line.Order_Qty__c!=null && Order_Qty!=0){
                            shipqty = shipqty + integer.valueOf(line.Order_Qty__c);
                        }
                    }
                }
            }
            
            if(oli.Sale_Portal_Shipped_Quantity__c==null){
                oli.Sale_Portal_Shipped_Quantity__c = 0;
            }
            
            if(oli.Sale_Portal_Shipped_Quantity__c!=shipqty){
                oli.Sale_Portal_Shipped_Quantity__c = shipqty;
                oliUpdateList.add(oli);
            }
        }
        if(oliUpdateList.size()>0){
            Database.update(oliUpdateList, False);
        }
        
    }
    
    global void finish(Database.BatchableContext BC) {
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        //system.debug('Rollout record Size ' + rolloutidNameMap.size());
        if(rolloutidNameMap.size()>0){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String toEmails = Label.HA_Rollout_Actuals_Update_Notification_Emails;
            list<string> toAddresses = toEmails.split(';');
            mail.setToAddresses(toAddresses);
            mail.setSubject('HA Rollout Actuals Update Notification');
            String htmlBody = '';
            htmlBody = '<table width="100%" border="0" cellspacing="0" cellpadding="8" align="center" bgcolor="#F7F7F7">'+
                            +'<tr>'+
                              +'<td style="font-size: 14px; font-weight: normal; font-family:Calibri;line-height: 18px; color: #333;"><br />'+
                                   +'<br />'+
                                    +'Dear User,</td>'+
                            +'</tr>'+
                             +'<tr>'+
                              +'<td style="font-size: 14px; font-weight: normal; font-family:Calibri;line-height: 18px; color: #333;"><br />'+
                                    +'The Following List of Rollouts were updated with Actual Quantity'+ +'.</td>'+
                            +'</tr>'+
                             
                        +'</table>';
                        
            htmlBody +=  '<table border="1" style="border-collapse: collapse"><tr><th> Rollout Name </th></tr>'; 
            for(id r : rolloutidNameMap.keySet()){
                string Url = System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+r;
                String Name = rolloutidNameMap.get(r);
                htmlBody += '<tr><td><a href="'+Url+'">'+Name+'</a></td></tr>';
            }
            htmlBody += '</table><br>';
            mail.sethtmlBody(htmlBody);
            mails.add(mail);
            if(mails.size()>0){
                Messaging.sendEmail(mails);
            }
        }
    }
    
}


/*
global class BatchforHARolloutActualsUpdateMigration implements Database.Batchable<sObject>,Schedulable,Database.stateful {
    
    global void execute(SchedulableContext SC) {
        database.executeBatch(new BatchforHARolloutActualsUpdateMigration(),100) ;
    }
    
    global Map<id,string> rolloutidNameMap;
    global Set<id> opids;

    global BatchforHARolloutActualsUpdateMigration(){
        rolloutidNameMap = new Map<id,string>();
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        database.Querylocator ql = database.getquerylocator([Select id,Opportunity__c,Opportunity__r.Roll_out_start__c,DO_Qty__c,Shipped_QTY__c,Delivery_Order_Date__c,DO_Created_Date__c,SAP_Material_ID__c,Req_Delivery_Date__c from SO_Lines__c]);
        return ql;
    }
    
    global void execute(Database.BatchableContext BC, List<SO_Lines__c> scope) {
        List<Roll_Out_Schedule__c> rosList = new List<Roll_Out_Schedule__c>();
        Map<id,Roll_Out_Schedule__c> RosMap = new Map<id,Roll_Out_Schedule__c>();
        Map<id,List<SO_Lines__c>> OppSoLineMap = new Map<id,List<SO_Lines__c>>();
        Set<String> SOprods = new Set<String>();
        
        for(SO_Lines__c line : scope){
            system.debug('SO Line------->'+line.Id);
            SOprods.add(line.SAP_Material_ID__c);
            if(OppSoLineMap.containsKey(line.Opportunity__c)){
                List<SO_Lines__c> soLines = OppSoLineMap.get(line.Opportunity__c);
                soLines.add(line);
                OppSoLineMap.put(line.Opportunity__c,soLines);
            }else{
                OppSoLineMap.put(line.Opportunity__c,new List<SO_Lines__c>{line});
            }
        }
            
        for(Roll_Out_Schedule__c ros : [select id,Roll_Out_Product__c,Roll_Out_Product__r.Roll_out_plan__c,Roll_Out_Product__r.Roll_out_plan__r.name,
                                              Roll_Out_Product__r.SAP_Material_Code__c,Roll_Out_Product__r.Roll_out_plan__r.Opportunity__c,
                                              Roll_Out_Product__r.Roll_out_plan__r.Opportunity__r.Roll_out_start__c,Opportunity_Id__c,
                                              Actual_Quantity__c,Actual_Revenue__c,Invoice_Date__c,Plan_Date__c,Plan_Quantity__c 
                                              from Roll_Out_Schedule__c 
                                              where Roll_Out_Product__r.Roll_out_plan__r.Opportunity__c=:OppSoLineMap.keySet() and Roll_Out_Product__r.SAP_Material_Code__c=:SOprods order by Roll_Out_Product__r.Roll_out_plan__r.Opportunity__c asc]){
            
            string oppid                 = ros.Roll_Out_Product__r.Roll_out_plan__r.Opportunity__c;
            date   oppRollOutStartMonday = RosMiscUtilities.getWeekBeginDate(ros.Roll_Out_Product__r.Roll_out_plan__r.Opportunity__r.Roll_out_start__c);
            date   weekStartDate         = ros.Plan_Date__c;
            date   weekendDate           = ros.Plan_Date__c+6;
            integer shipqty              = 0;
            
            if(OppSoLineMap.get(oppid)!=null){
                for(SO_Lines__c line : OppSoLineMap.get(oppid)) {
                    integer DO_Qty = integer.valueOf(line.DO_Qty__c);
                    if(line.DO_Qty__c!=null && DO_Qty!=0){
                        //system.debug('line.SAP_Material_ID__c------->'+line.SAP_Material_ID__c);
                        //system.debug('Roll_Out_Product__r.SAP_Material_Code__c------->'+ros.Roll_Out_Product__r.SAP_Material_Code__c);
                        if(line.SAP_Material_ID__c==ros.Roll_Out_Product__r.SAP_Material_Code__c){
                            //if(line.DO_Created_Date__c!=null && line.DO_Created_Date__c<oppRollOutStartMonday && weekStartDate==oppRollOutStartMonday){//Thiru 05/20/2019::Added to put all DO_QTY before the rollout start date in first rollout schedule
                            if(line.Req_Delivery_Date__c!=null && line.Req_Delivery_Date__c<oppRollOutStartMonday && weekStartDate==oppRollOutStartMonday){//Thiru 06/13/2019::Added to put all DO_QTY before the rollout start date in first rollout schedule
                                shipqty = shipqty + integer.valueOf(line.DO_Qty__c);
                                //system.debug('Less than ROStart------->'+line.SAP_Material_ID__c);
                            }
                            //if(line.DO_Created_Date__c!=null && line.DO_Created_Date__c>=weekStartDate && line.DO_Created_Date__c<=weekendDate){//Commented on 06/13/2019
                            if(line.Req_Delivery_Date__c!=null && line.Req_Delivery_Date__c>=weekStartDate && line.Req_Delivery_Date__c<=weekendDate){//Added on 06/13/2019
                                shipqty = shipqty + integer.valueOf(line.DO_Qty__c);
                                //system.debug('Greater than ROStart------->'+line.SAP_Material_ID__c);
                            }
                        }
                    }
                }
            }
            
            if(ros.Actual_Quantity__c==null){
                ros.Actual_Quantity__c=0;
            }
            
            if(ros.Actual_Quantity__c!=shipqty){
                ros.Actual_Quantity__c = shipqty;
                rosList.add(ros);
                RosMap.put(ros.id,ros);
            }
            
        }
        
        if(rosList.size()>0){
            Database.SaveResult[] SaveResult = Database.update(rosList, false);
            for (Database.SaveResult sr : SaveResult) {
                if(sr.isSuccess()){
                    string successId = sr.getId();
                    if(RosMap.size()>0 && RosMap.containsKey(successId)){
                        Roll_Out_Schedule__c ros = RosMap.get(successId);
                        rolloutidNameMap.put(ros.Roll_Out_Product__r.Roll_out_plan__c, ros.Roll_Out_Product__r.Roll_out_plan__r.name);
                    }
                }
            }
            
        }
        
        //Block for updating Opportunity Products with shipped quantity
        List<OpportunityLineItem> oliUpdateList = new List<OpportunityLineItem>();
        List<OpportunityLineItem> OliList = new List<OpportunityLineItem>();
       
        for(OpportunityLineitem oli : [Select id,opportunityid,Sale_Portal_Shipped_Quantity__c,SAP_Material_Id__c,Product2.SAP_Material_ID__c from Opportunitylineitem where OpportunityId=:OppSoLineMap.keySet() and Product2.SAP_Material_ID__c=:SOprods]){
            integer shipqty = 0;
            
            if(OppSoLineMap.containsKey(oli.opportunityid)){
                for(SO_Lines__c line : OppSoLineMap.get(oli.opportunityid)){
                    integer DO_Qty = integer.valueOf(line.DO_Qty__c);
                    if(line.DO_Qty__c!=null && DO_Qty!=0){
                        if(line.SAP_Material_ID__c==oli.Product2.SAP_Material_ID__c){
                            shipqty = shipqty + integer.valueOf(line.DO_Qty__c);
                        }
                    }
                }
            }
            
            if(oli.Sale_Portal_Shipped_Quantity__c==null){
                oli.Sale_Portal_Shipped_Quantity__c = 0;
            }
            
            if(oli.Sale_Portal_Shipped_Quantity__c!=shipqty){
                oli.Sale_Portal_Shipped_Quantity__c = shipqty;
                oliUpdateList.add(oli);
            }
        }
        if(oliUpdateList.size()>0){
            Database.update(oliUpdateList, False);
        }

    }
    
    global void finish(Database.BatchableContext BC) {
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        system.debug('Rollout record Size ' + rolloutidNameMap.size());
        if(rolloutidNameMap.size()>0){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String toEmails = Label.HA_Rollout_Actuals_Update_Notification_Emails;
            list<string> toAddresses = toEmails.split(';');
            mail.setToAddresses(toAddresses);
            mail.setSubject('HA Rollout Actuals Update Notification');
            String htmlBody = '';
            htmlBody = '<table width="100%" border="0" cellspacing="0" cellpadding="8" align="center" bgcolor="#F7F7F7">'+
                            +'<tr>'+
                              +'<td style="font-size: 14px; font-weight: normal; font-family:Calibri;line-height: 18px; color: #333;"><br />'+
                                   +'<br />'+
                                    +'Dear User,</td>'+
                            +'</tr>'+
                             +'<tr>'+
                              +'<td style="font-size: 14px; font-weight: normal; font-family:Calibri;line-height: 18px; color: #333;"><br />'+
                                    +'The Following List of Rollouts were updated with Actual Quantity'+ +'.</td>'+
                            +'</tr>'+
                             
                        +'</table>';
                        
            htmlBody +=  '<table border="1" style="border-collapse: collapse"><tr><th> Rollout Name </th></tr>'; 
            for(id r : rolloutidNameMap.keySet()){
                string Url = System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+r;
                String Name = rolloutidNameMap.get(r);
                htmlBody += '<tr><td><a href="'+Url+'">'+Name+'</a></td></tr>';
            }
            htmlBody += '</table><br>';
            mail.sethtmlBody(htmlBody);
            mails.add(mail);
            if(mails.size()>0){
                Messaging.sendEmail(mails);
            }
        }
    }
}
*/