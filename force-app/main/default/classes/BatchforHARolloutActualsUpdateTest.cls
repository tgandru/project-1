@isTest
private class BatchforHARolloutActualsUpdateTest{
    
    static Id HADistyRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HA Builder Distributor').getRecordTypeId(); 
    static Id HART = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HA Builder').getRecordTypeId(); 
    public Static String CRON_EXP = '0 0 12 15 2 ?'; //To execute schedulable 
    public Static String CRON_EXP2 = '0 0 12 20 2 ?'; //To execute schedulable 
    public Static String CRON_EXP3 = '0 0 12 25 2 ?'; //To execute schedulable 
    
    @testSetup static void setup() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='Flow-051';
        iep2.endPointURL__c = 'https://www.google.com';
        iep2.layout_id__c = 'LAY024197';
        iep2.partial_endpoint__c = '/SFDC_IF_US_051';
        insert iep2;
        
        list<account> accts=new list<account>();
        account a2=TestDataUtility.createAccount('dpba stuff');
        a2.RecordTypeId = HADistyRT;
        a2.sap_company_code__C='5554444';
        accts.add(a2);
        
        insert accts;
        
        //Creating custom Pricebook
        PriceBook2 pb = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(4)); 
        insert pb;
        
        date dt = date.today();
        dt = dt.addDays(1);
        
        id opprtid = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('HA Builder').getRecordTypeId();
        Opportunity opp = new Opportunity(name='HA',Accountid=a2.id,recordtypeid=opprtid,closedate=Date.valueOf(System.Today()),stagename='Proposal',Project_Name__c='Test',Expected_Close_Date__c=Date.valueOf(System.Today()),Number_of_Living_Units__c=100,Roll_Out_Start__c=dt.addDays(10),Rollout_Duration__c=2); //,Roll_Out_Start__c=dt,Rollout_Duration__c=2
        insert opp;
        
        id qtrtid = Schema.SObjectType.SBQQ__Quote__c.getRecordTypeInfosByName().get('HA Builder').getRecordTypeId();
        SBQQ__Quote__c qt = new SBQQ__Quote__c(SBQQ__Type__c='Quote',SBQQ__ExpirationDate__c=Date.valueOf(System.Today()),Expected_Shipment_Date__c=Date.valueOf(System.Today())+10,Last_Shipment_Date__c=Date.valueOf(System.Today())+30,recordtypeid=qtrtid,SBQQ__Opportunity2__c=opp.id);
        insert qt;
        
        //Create Products
        List<Product2> prods=new List<Product2>();
        Product2 standaloneProd=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Standalone Prod1', 'LAUNDRY', 'LAUNDRY', 'H/W');
        prods.add(standaloneProd);
        Product2 parentProd1=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Parent Prod1', 'DISH', 'DISH', 'H/W');
        prods.add(parentProd1);
        
        insert prods;

        Id pricebookId = Test.getStandardPricebookId();

        //CreatePBEs
        //create the pricebook entries for the custom pricebook
        List<PriceBookEntry> custompbes=new List<PriceBookEntry>();
        PriceBookEntry standaloneProdPBE=TestDataUtility.createPriceBookEntry(standaloneProd.Id, pb.Id);
        custompbes.add(standaloneProdPBE);
        PriceBookEntry parentProd1PBE=TestDataUtility.createPriceBookEntry(parentProd1.Id, pb.Id);
        custompbes.add(parentProd1PBE);
        insert custompbes;
        
        List<SBQQ__QuoteLine__c> quotelines = new List<SBQQ__QuoteLine__c>();
        SBQQ__QuoteLine__c line1 = new SBQQ__QuoteLine__c(SBQQ__Quote__c=qt.id,SBQQ__Product__c=standaloneProd.id,SBQQ__Quantity__c=50,Package_Option__c='BASE',SBQQ__NetPrice__c=400);
        quotelines .add(line1);
        SBQQ__QuoteLine__c line2 = new SBQQ__QuoteLine__c(SBQQ__Quote__c=qt.id,SBQQ__Product__c=parentProd1.id,SBQQ__Quantity__c=40,Package_Option__c='BASE',SBQQ__NetPrice__c=300);
        quotelines .add(line2);
        insert quotelines;
        
       /* OpportunityLineItem oli = new OpportunityLineItem(opportunityid=opp.id,product2id=standaloneProd.id,requested_price__c=400,quantity=50,UnitPrice=400,pricebookentryid=standaloneProdPBE.id);
        insert oli;*/
        
        Roll_Out__c rollout = new Roll_Out__c(name='Test Rollout', Opportunity__c = opp.Id, Roll_Out_Start__c = dt, Rollout_Duration__c = 1);
        insert rollout;
        
        Roll_Out_Product__c rop = new Roll_Out_Product__c(Product__c=standaloneProd.id,Roll_Out_Plan__c=rollout.id);
        insert rop;
        
        Roll_Out_Schedule__c ros = new Roll_Out_Schedule__c(Roll_Out_Product__c=rop.id,Plan_Date__c=dt.addDays(10),Plan_Quantity__c=10);
        insert ros;
    }
    @isTest static void test_method_one() {
        SBQQ__Quote__c quot=[select Id,SBQQ__Status__c from SBQQ__Quote__c limit 1];
        quot.SBQQ__Status__c='Approved';
        update quot;
        Opportunity Opp =[select id,stagename from Opportunity limit 1];
        opp.winapprovalStatus__c='Approved';
        opp.stagename='Win';
        opp.Sales_Portal_BO_Number__c='BO0000034';
        update Opp;
        
        SO_Number__c so = new SO_Number__c(Opportunity__c=opp.id,SO_Number__c='1278665133');
        insert so;

        Product2 prod = [Select id,SAP_Material_ID__c from Product2 where name='Standalone Prod1' limit 1];
        SO_Lines__c soline = new SO_Lines__c(Opportunity__c=opp.id,SO_Number__c=so.id,SAP_Material_ID__c=prod.SAP_Material_ID__c,Order_Qty__c=10,DO_Qty__c=10,Delivery_Order_Date__c=System.now().addDays(11),DO_Created_Date__c=System.now().addDays(11),Req_Delivery_Date__c=System.today().addDays(11));
        insert soline;
        SO_Lines__c soline2 = new SO_Lines__c(Opportunity__c=opp.id,SO_Number__c=so.id,SAP_Material_ID__c=prod.SAP_Material_ID__c,Order_Qty__c=10,DO_Qty__c=10,Delivery_Order_Date__c=System.now().addDays(11),DO_Created_Date__c=System.now().addDays(11),Req_Delivery_Date__c=System.today().addDays(11));
        insert soline2;
        
        database.executeBatch(new BatchforHARolloutActualsUpdate(),1);
        
    }
    @isTest static void test_method_two() {
        SBQQ__Quote__c quot=[select Id,SBQQ__Status__c from SBQQ__Quote__c limit 1];
        quot.SBQQ__Status__c='Approved';
        update quot;
        Opportunity Opp =[select id,stagename from Opportunity limit 1];
        opp.winapprovalStatus__c='Approved';
        opp.stagename='Win';
        opp.Sales_Portal_BO_Number__c='BO0000034';
        update Opp;
        
        SO_Number__c so = new SO_Number__c(Opportunity__c=opp.id,SO_Number__c='1278665133');
        insert so;
        
        /*account a3=TestDataUtility.createAccount('dpba stuff2');
        a3.RecordTypeId = HADistyRT;
        a3.sap_company_code__C='5554445';
        insert a3;
        
        id opprtid = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('HA Builder').getRecordTypeId();
        date dt = date.today();
        dt = dt.addDays(1);
        Opportunity opp2 = new Opportunity(name='HA',Accountid=a3.id,recordtypeid=opprtid,closedate=Date.valueOf(System.Today()),stagename='Proposal',Project_Name__c='Test',Expected_Close_Date__c=Date.valueOf(System.Today()),Number_of_Living_Units__c=100,Roll_Out_Start__c=dt.addDays(10),Rollout_Duration__c=2); //,Roll_Out_Start__c=dt,Rollout_Duration__c=2
        insert opp2;
        
        SO_Number__c so2 = new SO_Number__c(Opportunity__c=opp.id,SO_Number__c='1278665134');
        insert so2;*/

        Product2 prod = [Select id,SAP_Material_ID__c from Product2 where name='Standalone Prod1' limit 1];
        SO_Lines__c soline = new SO_Lines__c(Opportunity__c=opp.id,SO_Number__c=so.id,SAP_Material_ID__c=prod.SAP_Material_ID__c,Order_Qty__c=12,DO_Qty__c=12,Delivery_Order_Date__c=System.now().addDays(11),DO_Created_Date__c=System.now().addDays(11),Req_Delivery_Date__c=System.today().addDays(11));
        insert soline;
        SO_Lines__c soline2 = new SO_Lines__c(Opportunity__c=opp.id,SO_Number__c=so.id,SAP_Material_ID__c=prod.SAP_Material_ID__c,Order_Qty__c=12,DO_Qty__c=12,Delivery_Order_Date__c=System.now().addDays(11),DO_Created_Date__c=System.now().addDays(11),Req_Delivery_Date__c=System.today().addDays(11));
        insert soline2;
        /*SO_Lines__c soline3 = new SO_Lines__c(Opportunity__c=opp2.id,SO_Number__c=so2.id,SAP_Material_ID__c=prod.SAP_Material_ID__c,DO_Qty__c=10,Delivery_Order_Date__c=System.now().addDays(11),DO_Created_Date__c=System.now().addDays(11),Req_Delivery_Date__c=System.today().addDays(11));
        insert soline3;*/
        
        database.executeBatch(new BatchforHARolloutActualsUpdateMigration(),1);

    }
    @isTest static void test_method_three() {
        SBQQ__Quote__c quot=[select Id,SBQQ__Status__c from SBQQ__Quote__c limit 1];
        quot.SBQQ__Status__c='Approved';
        update quot;
        Opportunity Opp =[select id,stagename from Opportunity limit 1];
        opp.winapprovalStatus__c='Approved';
        opp.stagename='Win';
        opp.Sales_Portal_BO_Number__c='BO0000034';
        update Opp;
        set<id> oppids = new set<id>();
        oppids.add(Opp.id);
        
        SO_Number__c so = new SO_Number__c(Opportunity__c=opp.id,SO_Number__c='1278665133');
        insert so;

        Product2 prod = [Select id,SAP_Material_ID__c from Product2 where name='Standalone Prod1' limit 1];
        SO_Lines__c soline = new SO_Lines__c(Opportunity__c=opp.id,SO_Number__c=so.id,SAP_Material_ID__c=prod.SAP_Material_ID__c,Order_Qty__c=14,DO_Qty__c=14,Delivery_Order_Date__c=System.now().addDays(11),DO_Created_Date__c=System.now().addDays(11),Req_Delivery_Date__c=System.today().addDays(11));
        insert soline;
        SO_Lines__c soline2 = new SO_Lines__c(Opportunity__c=opp.id,SO_Number__c=so.id,SAP_Material_ID__c=prod.SAP_Material_ID__c,Order_Qty__c=14,DO_Qty__c=14,Delivery_Order_Date__c=System.now().addDays(11),DO_Created_Date__c=System.now().addDays(11),Req_Delivery_Date__c=System.today().addDays(11));
        insert soline2;
        database.executeBatch(new BatchforHARolloutActualsUpdate_v2(oppids),1);

    }
    
}