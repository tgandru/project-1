//Batch for Getting SO Lines by sending SO number of Opportunity callout to GERP.
global class BatchforHAShippedQuantityUpdate implements Database.Batchable<sObject>, Database.AllowsCallouts, Schedulable {
    
    global void execute(SchedulableContext SC) {
        BatchforHAShippedQuantityUpdate ba = new BatchforHAShippedQuantityUpdate();
        database.executeBatch(ba,1);
    }
    
    global id HArecordtypeid;
    global BatchforHAShippedQuantityUpdate(){
        HArecordtypeid =  Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('HA Builder').getRecordTypeId();
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        //return Database.getQueryLocator(query);
        database.Querylocator ql = database.getquerylocator([SELECT MSGSTATUS__c,  Status__c, retry_counter__c, Integration_Flow_Type__c, CreatedBy.Name, Identification_Text__c, ParentMQ__c,ParentMQ__r.status__c, ERRORTEXT__c FROM message_queue__c where Integration_Flow_Type__c='SO Line' and (Status__c='Not Started' or Status__c='failed-retry')]);
        return ql;
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        for(message_queue__c mq: (List<message_queue__c>)scope) {
            System.debug('lclc in '+mq.Integration_Flow_Type__c + mq.Id);
            try{
                if(mq.Integration_Flow_Type__c=='SO Line'){
                    HAShippedQuantityUpdate.callout(mq);
                }
            }
            catch(Exception ex){
                String errmsg = 'MSG='+ex.getmessage() + ' - '+ex.getStackTraceString();
                system.debug('## getmessage'+errmsg);
            }
        }
        //update scope; 
    }
    
    global void finish(Database.BatchableContext BC) {
        // nothing here
    }
}