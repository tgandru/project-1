//Batch for Getting SO Number by sending Conformation number of Opportunity callout to Salesportal.
global class BatchforHAbalancedqtyCallout implements Database.Batchable<sObject>, Database.AllowsCallouts, Schedulable {
    
    global void execute(SchedulableContext SC) {
        BatchforHAbalancedqtyCallout ba = new BatchforHAbalancedqtyCallout();
        database.executeBatch(ba,1);
    }
    
    global id HArecordtypeid;

    global BatchforHAbalancedqtyCallout(){
        HArecordtypeid =  Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('HA Builder').getRecordTypeId();
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        //database.Querylocator ql = database.getquerylocator([SELECT MSGSTATUS__c,  Status__c, retry_counter__c, Integration_Flow_Type__c, CreatedBy.Name, Identification_Text__c, ParentMQ__c,ParentMQ__r.status__c, ERRORTEXT__c,Object_Name__c FROM message_queue__c where Integration_Flow_Type__c='SO Number' and (Status__c='Not Started' or Status__c='failed-retry')]);
        database.Querylocator ql = database.getquerylocator([SELECT id,SBQQ__PrimaryQuote__c,stagename,Sales_Portal_BO_Number__c,(select id from SO_Numbers__r) from Opportunity where recordtypeid=:HArecordtypeid and Sales_Portal_BO_Number__c!=null and stagename=:Label.oppStageRollout and Stagename!=:Label.oppStageCompleted]);
        return ql;
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        /*List<message_queue__c> mqUpsert = new List<message_queue__c>();
        Set<string> SONumberList = new Set<String>();
        for(message_queue__c mq: (List<message_queue__c>)scope) {
            System.debug('lclc in :'+mq.Integration_Flow_Type__c + mq.Id);
            SONumberList.add(mq.Identification_Text__c);
            try{
                if(mq.Integration_Flow_Type__c=='SO Number'){
                    HAbalancedqtyCallout.calloutportal(mq);
                }
            }
            catch(Exception ex){
                String errmsg = 'MSG='+ex.getmessage() + ' - '+ex.getStackTraceString();
                system.debug('## getmessage'+errmsg);
            }
        }
        for(Opportunity o : [select id,SBQQ__PrimaryQuote__c,stagename,Sales_Portal_BO_Number__c,(select id from SO_Numbers__r) from Opportunity where recordtypeid=:HArecordtypeid and Sales_Portal_BO_Number__c=:SONumberList]){
            if(o.SO_Numbers__r.size()>0){
                message_queue__c m = new message_queue__c(Identification_Text__c=o.id, Integration_Flow_Type__c='SO Line',
                                                            retry_counter__c=0,Status__c='not started', Object_Name__c='SO Line');
                mqUpsert.add(m);
            }
        }
        upsert mqUpsert;*/
        Set<String> BOnums = new Set<String>();
        for(Opportunity opp : (List<Opportunity>)scope){
            BOnums.add(opp.Sales_Portal_BO_Number__c);
        }
        
        List<message_queue__c> existingMQs = [SELECT id, Status__c, retry_counter__c, Integration_Flow_Type__c, Identification_Text__c FROM message_queue__c where Integration_Flow_Type__c='SO Number' and (Status__c='Not Started' or Status__c='failed-retry') and Identification_Text__c=:BOnums];
        Map<String,message_queue__c> boMQmap = new Map<String,message_queue__c>();
        for(message_queue__c mq : existingMQs){
            boMQmap.put(mq.Identification_Text__c, mq);
        }
        
        List<message_queue__c> mqUpsert = new List<message_queue__c>();
        for(Opportunity opp : (List<Opportunity>)scope){
            if(boMQmap!=null && !boMQmap.containsKey(opp.Sales_Portal_BO_Number__c)){
                message_queue__c m = new message_queue__c(Identification_Text__c=opp.Sales_Portal_BO_Number__c, Integration_Flow_Type__c='SO Number',
                                                                    retry_counter__c=0,Status__c='not started', Object_Name__c='SO Number');
                mqUpsert.add(m);
            }
        }
        upsert mqUpsert;
    }
    
    global void finish(Database.BatchableContext BC) {
        //Sending SO Number to get SO Lines.
        //Database.executeBatch(new BatchforHAShippedQuantityUpdate(),1);
    }
}