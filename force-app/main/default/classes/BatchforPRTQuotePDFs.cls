Global class BatchforPRTQuotePDFs implements Database.Batchable<sObject>, Database.stateful{

    global String finalqdocstring;
    global String queryString;
    global integer batchcount;
    global integer num;
    global string Email;

    // Batch Constructor
    global BatchforPRTQuotePDFs(integer num,string Email){
        this.num = num;
        this.Email = Email;
        String qdochheader = 'ID,Quoteid,Name,Discount,Grand_Total \n';
        finalqdocstring = qdochheader;
        batchcount = 1;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        Set<string> oppNos = new Set<string>();
        Set<id> opid = new Set<id>();
        for(Printer_Data_Export__c p : [select id,Opportunity_No__c from Printer_Data_Export__c]){
            oppNos.add(p.Opportunity_No__c);
        }
        for(Opportunity opp : [select id from Opportunity where Opportunity_Number__c=:oppNos]){
            opid.add(opp.id);
        }
        queryString = 'select id,quoteid,name,discount,grandtotal,document from QuoteDocument WHERE Quoteid IN (Select Id from Quote where Opportunityid=:opid) limit '+num;
        return Database.getQueryLocator(queryString);
    }
    
    global void execute (Database.Batchablecontext BC, list<QuoteDocument> scope){
        
        List<Messaging.EmailFileAttachment> emailQuotePFDs = new List<Messaging.EmailFileAttachment>();
        for(QuoteDocument qd : scope){
            Messaging.EmailFileAttachment qtpdf = new Messaging.EmailFileAttachment();
            qtpdf.setFileName('QuoteAttachment'+qd.id);
            qtpdf.setBody(qd.Document);
            emailQuotePFDs.add(qtpdf);
            string qdocstring = qd.id+','+qd.quoteid+','+(qd.name).replace(',','')+','+qd.Discount+','+qd.GrandTotal+'\n';
            finalqdocstring = finalqdocstring + qdocstring;        
        }
        Messaging.SingleEmailMessage emailPDF = new Messaging.SingleEmailMessage();
        String[] toAddressesPDF = new String[]{email};
        emailPDF.setToAddresses(toAddressesPDF);
        emailPDF.setSubject('QuoteAttachment_'+batchcount+' List');
        emailPDF.setPlainTextBody('PFA - Printer Quote PDFs');
        emailPDF.setFileAttachments(emailQuotePFDs);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {emailPDF});
        batchcount++;

    }
        
    global void finish(Database.Batchablecontext BC){
        system.debug('the batch count----->'+batchcount);
        
        Blob b9 = Blob.valueof(finalqdocstring);
        Messaging.EmailFileAttachment efa9 = new Messaging.EmailFileAttachment();
        efa9.setFileName('Quote_AttachmentsList.CSV');
        efa9.setBody(b9);
        Messaging.SingleEmailMessage email9 = new Messaging.SingleEmailMessage();
        list<string> emailaddresses9 = new list<string>{email};
        email9.setSubject( 'Printer Quote attachments' );
        email9.setToAddresses( emailaddresses9 );
        string body9 ='Printer Quote attachments'+'\n\n'+'Thanks';
        email9.setPlainTextBody( 'PFA - Printer Quote Attachments records ' );
        email9.setFileAttachments(new Messaging.EmailFileAttachment[] {efa9});
        Messaging.SendEmailResult [] r9 = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email9});
    }
}