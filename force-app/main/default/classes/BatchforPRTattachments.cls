Global class BatchforPRTattachments implements Database.Batchable<sObject>, Database.stateful{

    global String finalattstring;
    global String queryString;
    global integer batchcount;
    global integer num;
    global string Email;

    // Batch Constructor
    global BatchforPRTattachments(integer num,string Email){
        this.num = num;
        this.Email = Email;
        String attchheader = 'ID,Name,OpportunityID \n';
        finalattstring = attchheader;
        batchcount = 1;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        Set<string> oppNos = new Set<string>();
        for(Printer_Data_Export__c p : [select id,Opportunity_No__c from Printer_Data_Export__c]){
            oppNos.add(p.Opportunity_No__c);
        }
        queryString = 'SELECT Id,Parentid, Name, Body, ContentType FROM Attachment WHERE Parentid IN (Select Id from Opportunity WHERE Opportunity_Number__c =:oppNos) limit '+num;
        return Database.getQueryLocator(queryString);
    }
    
    global void execute (Database.Batchablecontext BC, list<Attachment> scope){
        
        Messaging.SingleEmailMessage email1 = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[]{email};
        email1.setToAddresses(toAddresses);
        email1.setSubject('OppAttachments_'+batchcount+' List');
        email1.setPlainTextBody('PFA - Printer Attachments');
        
        List<Messaging.EmailFileAttachment> emailAttachments = new List<Messaging.EmailFileAttachment>();
        for(Attachment att : scope) {
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName('OppAttachment'+att.id);
            efa.setBody(att.Body);
            emailAttachments.add(efa);
            string attstring = att.id+','+(att.name).replace(',','')+','+att.parentid+'\n';
            finalattstring = finalattstring + attstring;
        }
        email1.setFileAttachments(emailAttachments);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email1});
        batchcount++;

    }
        
    global void finish(Database.Batchablecontext BC){
        system.debug('the batch count----->'+batchcount);
        
        Blob b9 = Blob.valueof(finalattstring);
        Messaging.EmailFileAttachment efa9 = new Messaging.EmailFileAttachment();
        efa9.setFileName('Opp_AttachmentsList.CSV');
        efa9.setBody(b9);
        Messaging.SingleEmailMessage email9 = new Messaging.SingleEmailMessage();
        list<string> emailaddresses9 = new list<string>{email};
        email9.setSubject( 'Printer Oppty attachments' );
        email9.setToAddresses( emailaddresses9 );
        string body9 ='Printer attachments'+'\n\n'+'Thanks';
        email9.setPlainTextBody( 'PFA - Printer Attachments records ' );
        email9.setFileAttachments(new Messaging.EmailFileAttachment[] {efa9});
        Messaging.SendEmailResult [] r9 = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email9});
    }
}