//Batch class to send SVN emails to avoid apex email limits.
Global class BatchforSVNEmail implements Database.Batchable<sObject>,database.stateful{
    global Document document;
    global string Subject;
    global string bcc;
    global string AdditionalTo;
    global string AdditionalCc;
    global String EmailTempateId;
    global OrgWideEmailAddress FromEmail;
    global id selectedFromEmail;
    global List<SelectOption> FromEmails;
    global String docId;
    global string imageid;
    global string orgid;
    global string htmlbody;
    global integer TotalAccounts;
    global integer TotalContacts;
    global integer SuccessEmailCount;
    global String EmailErrors;
    global Set<id> accids;
    
    global BatchforSVNEmail(Document doc,string AddTo,string AddCc,string sub, id fromEmaiId, string html, Set<id> accids){
        this.accids = accids;
        this.document=doc;
        this.selectedFromEmail=fromEmaiId;
        this.Subject=sub;
        this.AdditionalTo=AddTo;
        this.AdditionalCc=AddCc;
        imageid = label.SVN_Email_DocumentId;
        this.htmlbody = html;
        system.debug('Subject------>'+subject);
        
        TotalAccounts = 0;
        TotalContacts = 0;
        SuccessEmailCount = 0;
        EmailErrors = 'ContactId,Contact Name,Account Name,Error Code,Error Message\n';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        //database.Querylocator ql = database.getquerylocator([select id,owner.email,Service_Account_Manager__c,Service_Account_Manager__r.email,(select id from Entitlements where asset.product2.SVN_Opt_Out__c=false),(select teammemberrole,user.email from Accountteammembers where TeamMemberRole='Technical Pre-sales'),(select id,accountid,account.name,contactid,contact.name from AccountContactRoles where role='Security Notification' and contact.Active__c=true) from Account where id=:accids]);
        database.Querylocator ql = database.getquerylocator([select id,owner.email,Service_Account_Manager__c,Service_Account_Manager__r.email,(select id from Entitlements where asset.product2.SVN_Opt_Out__c=false),(select teammemberrole,user.email from Accountteammembers where TeamMemberRole='Technical Pre-sales'),(select id,accountid,account.name,contactid,contact.name from AccountContactRelations where roles='Security Notification' and contact.Active__c=true) from Account where id=:accids]);
        return ql;
    }
    
    global void execute(Database.Batchablecontext BC,List<Account> scope){
        
        Map<string,Account> ConAccMap = new Map<string,Account>();
        Map<id,Account> AccMap = new Map<id,Account>(scope);

        Messaging.Email[] emailList = new Messaging.Email[0];
        Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
        if(document.body!= null){
            efa.setFileName(document.name);
            efa.setBody(document.body);
        }
        for(Account acc : scope){
            set<string> ccids = new set<string>();
            if(AdditionalCc!=null && AdditionalCc!=''){
                List<String> AddCC = AdditionalCc.split(';');
                for(string s : addCC){
                    ccids.add(s);
                }
            }
            //if(acc.entitlements.size()>0){ //Commented--10/22/2018--As Biz want to add CCs irrespective of Account has Entitlements 
                ccids.add(acc.ownerid);
                if(acc.Service_Account_Manager__c!=null){
                    ccids.add(acc.Service_Account_Manager__c);
                }
                for(AccountTeamMember member : acc.AccountTeamMembers){
                    ccids.add(member.userid);
                }
            //}
            set<string> ToContactids = new set<string>();
            
            system.debug('contact------>'+acc.contacts);
            //for(AccountContactRole con : acc.AccountContactRoles){
            for(AccountContactRelation con : acc.AccountContactRelations){
                ConAccMap.put(con.contactid,acc);
                system.debug('contact------>'+con);
                ToContactids.add(con.Contactid);
                TotalContacts++;
            }
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setOrgWideEmailAddressId(selectedFromEmail);
            //Adding contact ids and additionalto to ToAddress.
            List<string> ToAddresses = new List<string>();
            ToAddresses.addAll(ToContactids);
            system.debug(ToAddresses);
            mail.setToAddresses(ToAddresses);
            mail.setTargetObjectId(ToAddresses[0]);
            if(ccids.size()>0){
                List<String> ccAddresses = new list<string>();
                ccAddresses.addAll(ccids);
                mail.setCcAddresses(ccAddresses);
            }
            mail.setSaveAsActivity(true);
            system.debug('Subject------>'+subject);
            mail.setSubject(subject);
            mail.setHtmlBody(htmlBody);
            mail.setUseSignature(false);
            if(document.body!= null){
                mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
            }
            emailList.add(mail);
            TotalAccounts++;
            
        }
        system.debug('emailList size-------->'+emailList.size());
        
        if(emailList.size()>0){
            system.debug('Email Size--->'+emailList.size());
            //Messaging.sendEmail(emailList,false);
            Messaging.SendEmailResult[] results = Messaging.sendEmail( emailList,false );
            Messaging.SendEmailError[] errors = new List<Messaging.SendEmailError>();
            for( Messaging.SendEmailResult currentResult : results ) {
                system.debug(currentResult.isSuccess());
                if(currentResult.isSuccess()){
                    SuccessEmailCount++;
                    system.debug(SuccessEmailCount);
                }
                else{
                    errors = currentResult.getErrors();
                    if( null != errors ) {
                        for( Messaging.SendEmailError currentError : errors ) {
                            string targetId = currentError.getTargetObjectId();
                            system.debug('targetId-------->'+targetId);
                            if(ConAccMap.containsKey(targetId)){
                                //List<AccountContactRole> ConRoles = ConAccMap.get(targetId).AccountContactRoles;
                                //for(AccountContactRole con : ConRoles){
                                List<AccountContactRelation> ConRoles = ConAccMap.get(targetId).AccountContactRelations;
                                for(AccountContactRelation con : ConRoles){
                                    string accName = con.account.name;
                                    accName = accName.replace(',','');
                                    EmailErrors = EmailErrors +con.contactid+','+con.contact.name+','+accName+','+currentError.getStatusCode()+','+currentError.getMessage()+'\n';
                                }
                            }
                        }
                    }
                }
            }
        }
        
    }
    
    global void finish(Database.Batchablecontext BC){
        //Logic for sending email to Additional to emails.
        Messaging.EmailFileAttachment efa1 = new Messaging.EmailFileAttachment();
        if(document.body!= null){
            efa1.setFileName(document.name);
            efa1.setBody(document.body);
        }
        List<String> AddToEmails = new List<String>();
        if(AdditionalTo!=null && AdditionalTo!=''){//Logic to send emails to AdditionalTo emails
            List<String> AddTo = AdditionalTo.split(';');
            for(string s : addTo){
                AddToEmails.add(s);
            }
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setOrgWideEmailAddressId(selectedFromEmail);
            //Adding additionalto to ToAddress.
            mail.setToAddresses(AddToEmails);
            mail.setSaveAsActivity(true);
            mail.setSubject(subject);
            mail.setHtmlBody(htmlBody);
            mail.setUseSignature(false);
            if(document.body!= null){
                mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa1});
            }
            Messaging.SendEmailResult [] res = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
        }
        
        
        //Logic to send the Success and Error result of SVN Emails
        integer errorEmailCount = TotalAccounts-SuccessEmailCount;
        String body = 'Total '+ TotalContacts+ ' Contacts. \n'+'The emails were successfully sent to all Contacts';
        
        Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
        if(errorEmailCount>0){
            body = 'Total '+ TotalAccounts+ ' Accounts. \n'+'The emails were sent to '+SuccessEmailCount+' Accounts. \n'+'Emails were not sent for '+errorEmailCount+' Accounts. Please find attached errored Contact list';
            efa.setFileName('Error.csv');
            efa.setBody(Blob.valueOf(EmailErrors));
        }
        
        String UserEmail = UserInfo.getUserEmail(); 
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage(); 
            
        email.setSubject('SVN Email Result');
        email.setToAddresses( new String[] {UserEmail} );
        email.setPlainTextBody( body );
        if(errorEmailCount>0){
            email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
        }
        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
        
    }
}