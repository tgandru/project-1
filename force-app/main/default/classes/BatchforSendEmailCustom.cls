//Batch class to send emails to avoid apex email limits.
Global class BatchforSendEmailCustom implements Database.Batchable<sObject>,database.stateful{
    global string Subject;
    global List<string> bcc;
    global List<string> AdditionalTo;
    global List<string> cc;
    global OrgWideEmailAddress FromEmail;
    global id selectedFromEmail;
    global string orgid;
    global string htmlbody;
    global id relatedto;
    global string TemplateId;
    global string AddToHtmlBody;
    global string AddToSubject;
    global integer i;

    global Set<id> ContactIds;
    
    global BatchforSendEmailCustom(Set<id> contctIds,List<string> AddTo,List<string> cc,List<string> bcc,string sub, id fromEmaiId, string html, id relatedtoId,string templateid){
        this.selectedFromEmail=fromEmaiId;
        this.Subject=sub;
        this.AdditionalTo=AddTo;
        this.cc=cc;
        this.bcc=bcc;
        this.htmlbody = html;
        this.relatedto = relatedtoId;
        this.templateid = templateid;
        system.debug('Subject------>'+subject);
        
        this.ContactIds = contctIds;
        i=0;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        database.Querylocator ql = database.getquerylocator([select id,Name,AccountId,Email from Contact where id=:ContactIds]);
        return ql;
    }
    
    global void execute(Database.Batchablecontext BC,List<Contact> scope){
        Messaging.Email[] emailList = new Messaging.Email[0];
        for(Contact c : scope){
            i++;
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            if(selectedFromEmail!=UserInfo.getUserId()){
                mail.setOrgWideEmailAddressId(selectedFromEmail);
            }
            List<string> ToAddresses = new List<string>();
            ToAddresses.add(c.id);
            mail.setToAddresses(ToAddresses);
            mail.setTargetObjectId(c.id);
            mail.setWhatId(relatedto);
            mail.setTemplateId(templateId);
            if(cc.size()>0){
                mail.setCcAddresses(cc);
            }
            if(bcc.size()>0){
                system.debug('bcc----->'+bcc);
                mail.setBccAddresses(bcc);
            }
            mail.setSaveAsActivity(true);
            mail.setUseSignature(false);
            if(i==1){
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
                AddToHtmlBody=mail.getHtmlBody();
                AddToSubject=mail.getSubject();
            }else{
                emailList.add(mail);
            }
        }
        system.debug('Template Body-------->'+AddToHtmlBody);
        system.debug('emailList size-------->'+emailList.size());
        
        if(emailList.size()>0){
            Messaging.SendEmailResult[] results = Messaging.sendEmail( emailList,false );
            Messaging.SendEmailError[] errors = new List<Messaging.SendEmailError>();
            for( Messaging.SendEmailResult currentResult : results ) {
                system.debug(currentResult.isSuccess());
                if(currentResult.isSuccess()){
                    system.debug('Success');
                }
                else{
                    errors = currentResult.getErrors();
                    if( null != errors ) {
                        for( Messaging.SendEmailError currentError : errors ) {
                            string targetId = currentError.getTargetObjectId();
                            string EmailErrors = currentError.getTargetObjectId()+','+currentError.getStatusCode()+','+currentError.getMessage()+'\n';
                            system.debug('Email Error---->'+EmailErrors);
                        }
                    }
                }
            }
        }
    }
    
    global void finish(Database.Batchablecontext BC){
        Messaging.Email[] emailList = new Messaging.Email[0];
        if(AdditionalTo.size()>0){//Logic to send emails to AdditionalTo emails
            for(String s : AdditionalTo){
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                if(selectedFromEmail!=UserInfo.getUserId()){
                    mail.setOrgWideEmailAddressId(selectedFromEmail);
                }
                string[] toaddress = New String[] {s};
                //Adding additionalto to ToAddress
                mail.setToAddresses(toaddress);
                if(cc.size()>0){
                    mail.setCcAddresses(cc);
                }
                if(bcc.size()>0){
                    mail.setBccAddresses(bcc);
                }
                //mail.setWhatId(relatedto);
                mail.setSubject(AddToSubject);
                mail.setHtmlBody(AddToHtmlBody);
                //mail.setTemplateId(templateId);
                mail.setUseSignature(false);
                emailList.add(mail);
            }
            Messaging.SendEmailResult [] res = Messaging.sendEmail( emailList,false );
        }
    }
}