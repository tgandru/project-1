public class BoxSSOTabController {
public String  accesstoken {get; set;}
public String folderid {get; set;}
Public boolean showfiles {get; set;}
public List<BOX_SSO__c> b {get; set;}
  public BoxSSOTabController(){
      
      showfiles=false;
      b= New list<BOX_SSO__C>([Select id,name,Client_ID__c,Client_Secret__c,Private_Key__c,Public_Key__c,User_ID__c from BOX_SSO__C limit 1]);
      if(b.size()>0){
          
           if(b[0].User_ID__c =='' || b[0].User_ID__c ==null){
               ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'You are not allowed to use BOX.com');
               ApexPages.addmessage(myMsg);
              System.debug('*********Box User Id is Blank**********');
               return;
           }
           Try{
                 String userId= b[0].User_ID__c;
                 String publicKeyId= b[0].Public_Key__c;
                 String privatekey= b[0].Private_Key__c;
                 String clientId= b[0].Client_ID__c;
                 String clientSecret= b[0].Client_Secret__c;
                 BoxJwtEncryptionPreferences.EncryptionAlgorithm algorithm;
                 BoxJwtEncryptionPreferences preferences = new BoxJwtEncryptionPreferences();
                 preferences.setPublicKeyId(publicKeyId);
                 preferences.setPrivateKey(privateKey);
               
                 if(!Test.isRunningTest()){
                 BoxPlatformApiConnection api = BoxPlatformApiConnection.getAppUserConnection(userId, clientId, clientSecret, preferences);
                 System.debug('returned connection' + api);
                 System.debug(api.accesstoken);
                 this.accesstoken=api.accesstoken;
                  if(api.accesstoken != null && api.accesstoken !=''){
                      showfiles=true;
                   }
                 }else{
                      this.accesstoken='TESTAPITocken';
                 }
                 
                 
                 
               
           }catch(exception ex){
                    ApexPages.addMessages(ex);
           }
          
      }
  }
}