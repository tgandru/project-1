/*

 * This class should be scheduled to run has batch job.

 * This class invokes the class BatchForCIOpportunitySyncV2. 

 * Author: Bala Rajasekharan 

 */

global class CIPOSScheduler implements Schedulable{

    global void execute(SchedulableContext sc) {

        try{           

            //Call batchJob

            if(!Test.isRunningTest()){                

                Database.executeBatch(new BatchForCIOpportunitySyncV2(), 100);  

            }

        }

        catch(Exception ex){

            System.debug('BatchForCIOpportunitySyncV2 Scheduled Job failed : ' + ex.getMessage());                  

        }

        Integer rqmi = 24;

        DateTime timenow = DateTime.now().addHours(rqmi);

		CIPOSScheduler rqm = new CIPOSScheduler();
		
		String seconds = '0';
		String minutes = String.valueOf(timenow.minute());
		String hours = String.valueOf(timenow.hour());
		
		//Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
		String sch = seconds + ' ' + minutes + ' ' + hours + ' * * ?';
		String jobName = 'CI POS scheduler - ' + hours + ':' + minutes;
         if(!Test.isRunningTest()){
			system.schedule(jobName, sch, rqm);
         }
		
		if (sc != null)	{	
			system.abortJob(sc.getTriggerId());			
		}

    }

}