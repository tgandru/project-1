global class CISPOSUpdateSchedular implements Schedulable{
    global void execute(SchedulableContext sc) {
        try{           
            //Call Minute batchJob
            if(!Test.isRunningTest()){
                Database.executeBatch(new BatchForCISPOSUpdate(), 100); 
                
            }
        }
        catch(Exception ex){
            System.debug('CIS POS Scheduled Job failed : ' + ex.getMessage());                  
        }
        
        Integer rqmi = 24;

        DateTime timenow = DateTime.now().addHours(rqmi);
    
        CISPOSUpdateSchedular rqm = new CISPOSUpdateSchedular();
        
        String seconds = '0';
        String minutes = String.valueOf(timenow.minute());
        String hours = String.valueOf(timenow.hour());
        
        //Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
        String sch = seconds + ' ' + minutes + ' ' + hours + ' * * ?';
        String jobName = 'CIS POS Update scheduler - ' + hours + ':' + minutes;
        system.schedule(jobName, sch, rqm);
        
        if (sc != null)  {  
          system.abortJob(sc.getTriggerId());      
        }
        /*CISPOSUpdateSchedular ciPOS = new CISPOSUpdateSchedular();
        
        String seconds = '0';
        String minutes = '0';
        String hours = '23';
        string day = '1';
        
        //Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
        String sch = seconds + ' ' + minutes + ' ' + hours +' '+day+ ' * ?';
        String jobName = 'CIS POS Batch Monthly - ' + hours + ':' + day;
        system.schedule(jobName,sch,ciPOS);
*/

    }
}