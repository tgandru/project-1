/*
Considers olny Sales Team
*/
global class CalculateForecastAmount2 implements Database.Batchable<sObject>,Database.Stateful,Schedulable {
    
    
   global void execute(SchedulableContext SC) {
          Integer rqmi = 2;
    DateTime timenow = DateTime.now().addHours(rqmi);
    CalculateForecastAmount2 rqm = new CalculateForecastAmount2();
    String seconds = '0';
    String minutes = String.valueOf(timenow.minute());
    String hours = String.valueOf(timenow.hour());
    String sch = seconds + ' ' + minutes + ' ' + hours + ' * * ?';
    String jobName = 'CalculateForecastAmount2 scheduler - ' + hours + ':' + minutes;
    system.schedule(jobName, sch, rqm);
          database.executeBatch(new CalculateForecastAmount2()) ;
          if (sc != null)  {  
      system.abortJob(sc.getTriggerId());      
    }
       
   }
    
  global map<string, decimal> finalAmounts;
  global map<string, decimal> finalAmounts2;
   global list<period> periods;
    global map<string, string> salesTeamMapping;
   global CalculateForecastAmount2(){
       finalAmounts = new map<string, decimal>();
	   finalAmounts2 = new map<string, decimal>();
       salesTeamMapping = new map<string, string>();
       periods = new list<Period>([select startdate ,endDate,QuarterLabel,type,Number,FullyQualifiedLabel from period where (startdate=THIS_QUARTER or startdate=NEXT_N_QUARTERS:2  ) and type='Quarter']);//startdate=LAST_N_QUARTERS:3 or 
       
   } 

   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator([select Plan_Date__c,Roll_Out_Product__r.Opportunity_Owner_Sales_Team__c,Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Account.Co_owner_sales_team__c,Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.AccountId,name,Plan_Quantity__c,Plan_Revenue__c,Roll_Out_Product__r.name,Roll_Out_Product__r.Product_Category__c,Roll_Out_Product__r.Sales_Team__c from Roll_Out_Schedule__c
                                        where (Plan_Date__c=this_Quarter or Plan_Date__c=NEXT_N_QUARTERS:2	 ) and Plan_Quantity__c !=0 and (Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.stageName = 'Commit' or Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.stageName = 'Win') ]);
   }

 //salesTeam-ProdCategory-year-quaretr - format for combinations 

   global void execute(Database.BatchableContext BC, List<Roll_Out_Schedule__c> rollOutProds){
      set<string> combinations = new set<string>();
      
       
      for(Roll_Out_Schedule__c rp : rollOutProds){
          for(Period prd:periods){
              if(prd.startDate <=rp.Plan_Date__c && prd.EndDate >=rp.Plan_Date__c){
                  string key; //= string.valueOF(prd.startDate.year())+'-'+string.valueOF(prd.Number)+'-'+rp.Roll_Out_Product__r.Sales_Team__c+'-'+rp.Roll_Out_Product__r.Product_Category__c;//+rp.Product_Category__c
                     key = string.valueOF(prd.startDate.year())+'-'+string.valueOF(prd.Number)+'-'+rp.Roll_Out_Product__r.Sales_Team__c+'-';//+rp.Product_Category__c 
                  decimal finalAmount = rp.Plan_Revenue__c;
                  if(finalAmounts.get(key) != null ){
                      finalAmount = finalAmount + finalAmounts.get(key);
                  }
                  finalAmounts.put(key,finalAmount);
                   if(rp.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.AccountId != null){
                      salesTeamMapping.put(key,rp.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.AccountId);
                  }
              }
          }
      }
	  //New  for Co-Owner 
	  for(Roll_Out_Schedule__c rp2 : rollOutProds){
          for(Period prd2:periods){
              if(prd2.startDate <=rp2.Plan_Date__c && prd2.EndDate >=rp2.Plan_Date__c){
                  string key2;
                     key2 = string.valueOF(prd2.startDate.year())+'-'+string.valueOF(prd2.Number)+'-'+rp2.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Account.Co_owner_sales_team__c+'-';//+rp.Product_Category__c 
                 
                  decimal finalAmount2 = rp2.Plan_Revenue__c;
                  if(finalAmounts2.get(key2) != null ){
                      finalAmount2 = finalAmount2 + finalAmounts2.get(key2);
                  }
                  finalAmounts2.put(key2,finalAmount2);
                   
              }
          }
      }
       
   }

   global void finish(Database.BatchableContext BC){
       
            if(finalAmounts.size() > 0){
           set<string> combinations = new set<string>();
           combinations.addAll(finalAmounts.keySet());
		    set<string> combinations2 = new set<string>();
           combinations2.addAll(finalAmounts2.keySet());
           list<Planing_and_Forecasting__c> plannings = new list<Planing_and_Forecasting__c>( [select Account__c,Total_Forecast_Amount__c,Combination__c from Planing_and_Forecasting__c where Combination__c=:combinations ]);
		   list<Planing_and_Forecasting__c> plannings2 = new list<Planing_and_Forecasting__c>( [select Account__c,Forecast_Amount_Co_Owner__c,Total_Forecast_Amount__c,Combination__c from Planing_and_Forecasting__c where Combination__c=:combinations2 ]);
           for(Planing_and_Forecasting__c p : plannings){
               if(finalAmounts.get(p.Combination__c) != null){
                   p.Total_Forecast_Amount__c = finalAmounts.get(p.Combination__c);
               }
               if(salesTeamMapping.get(p.Combination__c) != null){
                   p.Account__c = salesTeamMapping.get(p.Combination__c);
               }
           }
		   for(Planing_and_Forecasting__c p2 : plannings2){
               if(finalAmounts2.get(p2.Combination__c) != null){
                   p2.Forecast_Amount_Co_Owner__c = finalAmounts2.get(p2.Combination__c);
               }
               
           }
           if(plannings.size() > 0){
               update plannings;
			   update plannings2;
           }
       
       }
     Database.executeBatch(new CalculateForecastAmount());  
       
   }
   
   
   
}