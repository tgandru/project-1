@isTest
private class CalculateForecastAmount2test {
    
    static Id endUserRT = TestDataUtility.retrieveRecordTypeId('End_User', 'Account'); 
    static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');

   
    @testSetup static void setup() {
        
        
        Test.loadData(fiscalCalendar__c.sObjectType, 'FiscalCalendarTestData');

        
        //Channelinsight configuration
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();

        Zipcode_Lookup__c zip=TestDataUtility.createZipcode();
        insert zip;
        
        List<Roll_Out_Schedule_Constants__c> roscs=new List<Roll_Out_Schedule_Constants__c>();
        Roll_Out_Schedule_Constants__c rosc1=new Roll_Out_Schedule_Constants__c(Name='Roll Out Schedule Parameters',Average_Week_Count__c=4.33,DML_Row_Limit__c=3200);
        roscs.add(rosc1);
        insert roscs;
        
        //Creating test Account
        Account acct=TestDataUtility.createAccount(TestDataUtility.generateRandomString(5));
        acct.RecordTypeId = endUserRT;
        insert acct;
        
        
        //Creating the Parent Account for Partners
        Account paacct=TestDataUtility.createAccount(TestDataUtility.generateRandomString(7));
        acct.RecordTypeId = directRT;
        insert paacct;
        
        
        //Creating custom Pricebook
        PriceBook2 pb = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(4)); 
        insert pb;
        
        
        //Creating list of Opportunities
        List<Opportunity> opps=new List<Opportunity>();
        Opportunity oppOpen=TestDataUtility.createOppty('New Opp from Test setup',acct, pb, 'RFP', 'IT', 'FAX/MFP', 'Identified');
        opps.add(oppOpen);
        insert opps;
        
        //Creating the partner for that opportunity
        Partner__c partner = TestDataUtility.createPartner(oppOpen,acct,paacct,'Distributor');
        insert partner;

        //Creating Products
        List<Product2> prods=new List<Product2>();
        Product2 standaloneProd=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Standalone Prod1', 'Printer[C2]', 'FAX/MFP', 'H/W');
        prods.add(standaloneProd);
        Product2 parentProd1=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Parent Prod1', 'Printer[C2]', 'FAX/MFP', 'H/W');
        prods.add(parentProd1);
        Product2 childProd1=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod1', 'Printer[C2]', 'PRINTER', 'Service pack');
        prods.add(childProd1);
        Product2 childProd2=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod2', 'Printer[C2]', 'PRINTER', 'Service pack');
        prods.add(childProd2);
        Product2 parentProd2=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Parent Prod2', 'Printer[C2]', 'FAX/MFP', 'H/W');
        prods.add(parentProd2);
        Product2 childProd3=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod3', 'Printer[C2]', 'PRINTER', 'Service pack');
        prods.add(childProd3);
        Product2 childProd4=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod4', 'Printer[C2]', 'PRINTER', 'Service pack');
        prods.add(childProd4);
        Product2 prod8=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Prod8', 'Printer[C2]', 'PRINTER', 'Service pack');
        prods.add(prod8);
        Product2 prod9=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Prod9', 'Printer[C2]', 'PRINTER', 'Service pack');
        prods.add(prod9);
        Product2 prod10=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Prod10', 'Printer[C2]', 'PRINTER', 'Service pack');
        prods.add(prod10);
        Product2 prod11=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Prod11', 'Printer[C2]', 'PRINTER', 'Service pack');
        prods.add(prod11);
        Product2 prod12=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Prod12', 'Printer[C2]', 'PRINTER', 'Service pack');
        prods.add(prod12);
        Product2 prod13=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Prod13', 'Printer[C2]', 'PRINTER', 'Service pack');
        prods.add(prod13);
        Product2 prod14=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Prod14', 'Printer[C2]', 'PRINTER', 'Service pack');
        prods.add(prod14);
        Product2 prod15=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Prod15', 'Printer[C2]', 'PRINTER', 'Service pack');
        prods.add(prod15);
        Product2 prod16=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Prod16', 'Printer[C2]', 'PRINTER', 'Service pack');
        prods.add(prod16);
        Product2 prod17=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Prod17', 'Printer[C2]', 'PRINTER', 'Service pack');
        prods.add(prod17);
        
        insert prods;
        
       
        List<PriceBookEntry> pbes=new List<PriceBookEntry>();
        

       
        PriceBookEntry standaloneProdPBE=TestDataUtility.createPriceBookEntry(standaloneProd.Id, pb.Id);
        pbes.add(standaloneProdPBE);
        PriceBookEntry parentProd1PBE=TestDataUtility.createPriceBookEntry(parentProd1.Id,pb.Id);
        pbes.add(parentProd1PBE);
        PriceBookEntry childProd1PBE=TestDataUtility.createPriceBookEntry(childProd1.Id, pb.Id);
        pbes.add(childProd1PBE);
        PriceBookEntry childProd2PBE=TestDataUtility.createPriceBookEntry(childProd2.Id, pb.Id);
        pbes.add(childProd2PBE);
        PriceBookEntry parentProd2PBE=TestDataUtility.createPriceBookEntry(parentProd2.Id, pb.Id);
        pbes.add(parentProd2PBE);
        PriceBookEntry childProd3PBE=TestDataUtility.createPriceBookEntry(childProd3.Id, pb.Id);
        pbes.add(childProd3PBE);
        PriceBookEntry childProd4PBE=TestDataUtility.createPriceBookEntry(childProd4.Id, pb.Id);
        pbes.add(childProd4PBE);
        PriceBookEntry prod8PBE=TestDataUtility.createPriceBookEntry(prod8.Id, pb.Id);
        pbes.add(prod8PBE);
        PriceBookEntry prod9PBE=TestDataUtility.createPriceBookEntry(prod9.Id, pb.Id);
        pbes.add(prod9PBE);
        PriceBookEntry prod10PBE=TestDataUtility.createPriceBookEntry(prod10.Id, pb.Id);
        pbes.add(prod10PBE);
        PriceBookEntry prod11PBE=TestDataUtility.createPriceBookEntry(prod11.Id, pb.Id);
        pbes.add(prod11PBE);
        PriceBookEntry prod12PBE=TestDataUtility.createPriceBookEntry(prod12.Id, pb.Id);
        pbes.add(prod12PBE);
        PriceBookEntry prod13PBE=TestDataUtility.createPriceBookEntry(prod13.Id, pb.Id);
        pbes.add(prod13PBE);
        PriceBookEntry prod14PBE=TestDataUtility.createPriceBookEntry(prod14.Id, pb.Id);
        pbes.add(prod14PBE);
        PriceBookEntry prod15PBE=TestDataUtility.createPriceBookEntry(prod15.Id, pb.Id);
        pbes.add(prod15PBE);
        PriceBookEntry prod16PBE=TestDataUtility.createPriceBookEntry(prod16.Id, pb.Id);
        pbes.add(prod16PBE);
        PriceBookEntry prod17PBE=TestDataUtility.createPriceBookEntry(prod17.Id, pb.Id);
        pbes.add(prod17PBE);
        insert pbes;
        
        
        
        //Creating opportunity Line Item
        
        List<OpportunityLineItem> olis=new List<OpportunityLineItem>();
        OpportunityLineItem standaloneProdOLI=TestDataUtility.createOpptyLineItem(standaloneProd, standaloneProdPBE, oppOpen);
        standaloneProdOLI.Quantity = 10;
        standaloneProdOLI.TotalPrice = standaloneProdOLI.Quantity * standaloneProdPBE.UnitPrice;
        olis.add(standaloneProdOLI);
        OpportunityLineItem parentProd1OLI=TestDataUtility.createOpptyLineItem(parentProd1, parentProd1PBE, oppOpen);
        parentProd1OLI.Quantity = 10;
        parentProd1OLI.TotalPrice = parentProd1OLI.Quantity * parentProd1PBE.UnitPrice;
        olis.add(parentProd1OLI);
        OpportunityLineItem childProd1OLI=TestDataUtility.createOpptyLineItem(childProd1, childProd1PBE, oppOpen);
        childProd1OLI.Quantity = 10;
        childProd1OLI.TotalPrice = childProd1OLI.Quantity * childProd1PBE.UnitPrice;
        childProd1OLI.parent_product__c=parentProd1.Id;
        olis.add(childProd1OLI);
        OpportunityLineItem childProd2OLI=TestDataUtility.createOpptyLineItem(childProd2, childProd2PBE, oppOpen);
        childProd2OLI.Quantity = 700;
        childProd2OLI.TotalPrice = childProd2OLI.Quantity * childProd2PBE.UnitPrice;
        childProd1OLI.parent_product__c=parentProd1.Id;
        olis.add(childProd2OLI);
        OpportunityLineItem parentProd2OLI=TestDataUtility.createOpptyLineItem(parentProd2, parentProd2PBE, oppOpen);
        parentProd2OLI.Quantity = 10;
        parentProd2OLI.TotalPrice = parentProd2OLI.Quantity * parentProd2PBE.UnitPrice;
        olis.add(parentProd2OLI);
         OpportunityLineItem childProd3OLI=TestDataUtility.createOpptyLineItem(childProd3, childProd3PBE, oppOpen);
        childProd3OLI.Quantity = 10;
        childProd3OLI.TotalPrice = childProd3OLI.Quantity * childProd3PBE.UnitPrice;
        childProd3OLI.parent_product__c=parentProd2.Id;
        olis.add(childProd3OLI);
        OpportunityLineItem childProd4OLI=TestDataUtility.createOpptyLineItem(childProd4, childProd4PBE, oppOpen);
        childProd4OLI.Quantity = 700;
        childProd4OLI.TotalPrice = childProd4OLI.Quantity * childProd4PBE.UnitPrice;
        childProd4OLI.parent_product__c=parentProd2.Id;
        olis.add(childProd4OLI);
        OpportunityLineItem prod8OLI=TestDataUtility.createOpptyLineItem(prod8, prod8PBE, oppOpen);
        prod8OLI.Quantity = 10;
        prod8OLI.TotalPrice = prod8OLI.Quantity * prod8PBE.UnitPrice;
        olis.add(prod8OLI);
        OpportunityLineItem prod9OLI=TestDataUtility.createOpptyLineItem(prod9, prod9PBE, oppOpen);
        prod9OLI.Quantity = 10;
        prod9OLI.TotalPrice = prod9OLI.Quantity * prod9PBE.UnitPrice;
        olis.add(prod9OLI);
        OpportunityLineItem prod10OLI=TestDataUtility.createOpptyLineItem(prod10, prod10PBE, oppOpen);
        prod10OLI.Quantity = 10;
        prod10OLI.TotalPrice = prod10OLI.Quantity * prod10PBE.UnitPrice;
        olis.add(prod10OLI);
        OpportunityLineItem prod11OLI=TestDataUtility.createOpptyLineItem(prod11, prod11PBE, oppOpen);
        prod11OLI.Quantity = 10;
        prod11OLI.TotalPrice = prod11OLI.Quantity * prod11PBE.UnitPrice;
        olis.add(prod11OLI);
        OpportunityLineItem prod12OLI=TestDataUtility.createOpptyLineItem(prod12, prod12PBE, oppOpen);
        prod12OLI.Quantity = 10;
        prod12OLI.TotalPrice = prod12OLI.Quantity * prod12PBE.UnitPrice;
        olis.add(prod12OLI);
        OpportunityLineItem prod13OLI=TestDataUtility.createOpptyLineItem(prod13, prod13PBE, oppOpen);
        prod13OLI.Quantity = 10;
        prod13OLI.TotalPrice = prod13OLI.Quantity * prod13PBE.UnitPrice;
        olis.add(prod13OLI);
        OpportunityLineItem prod14OLI=TestDataUtility.createOpptyLineItem(prod14, prod14PBE, oppOpen);
        prod14OLI.Quantity = 10;
        prod14OLI.TotalPrice = prod14OLI.Quantity * prod14PBE.UnitPrice;
        olis.add(prod14OLI);
        OpportunityLineItem prod15OLI=TestDataUtility.createOpptyLineItem(prod15, prod15PBE, oppOpen);
        prod15OLI.Quantity = 10;
        prod15OLI.TotalPrice = prod15OLI.Quantity * prod15PBE.UnitPrice;
        olis.add(prod15OLI);
        OpportunityLineItem prod16OLI=TestDataUtility.createOpptyLineItem(prod16, prod16PBE, oppOpen);
        prod16OLI.Quantity = 10;
        prod16OLI.TotalPrice = prod16OLI.Quantity * prod16PBE.UnitPrice;
        olis.add(prod16OLI);
        OpportunityLineItem prod17OLI=TestDataUtility.createOpptyLineItem(prod17, prod17PBE, oppOpen);
        prod17OLI.Quantity = 10;
        prod17OLI.TotalPrice = prod17OLI.Quantity * prod17PBE.UnitPrice;
        olis.add(prod17OLI);
        insert olis;
    }
        static void createPrimaryParter(Opportunity opp){
       Partner__c tp = new Partner__c();
        tp.Opportunity__c = opp.id;
        tp.Role__c = 'Distributor';
        tp.is_Primary__c = true;
        insert tp;
    }
    

  

    @isTest static void test_edit_actuals(){

        Account acct=[select Id from Account limit 1];
        Opportunity opp = [select Id, StageName from Opportunity Limit 1];
        system.debug('Opp StageName ' + opp.StageName);
        Account partnerAcc=TestDataUtility.createAccount('partner acct');
        insert partnerAcc;
        Partner__c  partner= TestDataUtility.createPartner(opp, acct, partnerAcc, 'Distributor');
        partner.Is_Primary__c=true;
        insert partner; 
        Date closeDate = [Select EndDate From Period Where type = 'Quarter' and StartDate = THIS_FISCAL_QUARTER].EndDate;
        Date StartDate = [Select StartDate From Period Where type = 'Quarter' and StartDate = THIS_FISCAL_QUARTER].StartDate;
        integer  Month = Date.Today().Month();
        integer   quarter = ((Month / 3) + 1);
        string qt=String.valueOf(quarter);
        if(opp.StageName != 'Win')
        {
            opp.StageName = 'Win';
            opp.Roll_Out_Start__c=StartDate;
            opp.Rollout_Duration__c=4;
            opp.CloseDate=StartDate.addDays(-15);
            update opp;
        }
       Roll_Out__c rollOut=[select id  from Roll_Out__c where 	Opportunity__c=:opp.Id limit 1];
        //List<Roll_Out_Product__c> rollOutpro=[select id  from Roll_Out_Product__c where Roll_Out_Plan__c=:rollOut.Id];
        List<Roll_Out_Product__c> rollOutpro=[select id,Roll_Out_Plan__c,Product__c,Roll_Out_Start__c,Rollout_Duration__c,quote_quantity__c  from Roll_Out_Product__c where Roll_Out_Plan__c=:rollOut.Id];
        Map<id,Roll_Out_Product__c> rollOutproMap=new Map<id,Roll_Out_Product__c>();
        for(Roll_Out_Product__c rop : rollOutpro){
            rollOutproMap.put(rop.Id, rop);
        }
        
        List<Roll_Out_Schedule__c> insertRollOutSchedules = new List<Roll_Out_Schedule__c>();
            
        //call uility method to create ROS: HA_RosMiscUtilities
        for(Roll_Out_Schedule__c s: RosMiscUtilities.createROS(rollOutproMap)){
            insertRollOutSchedules.add(s);
        }

        insert insertRollOutSchedules;
        List<Roll_Out_Schedule__c> rollOutsch=[select id  from Roll_Out_Schedule__c where Roll_Out_Product__r.id in :rollOutpro];
        system.debug('rollOut***'+rollOut);
        system.debug('rollOutpro***'+rollOutpro);
        system.debug('rollOutsch***'+rollOutsch);
         system.debug('rollOutsch-Size***'+rollOutsch.size());
            Planing_and_Forecasting__c pf =new  Planing_and_Forecasting__c(Account__c=acct.id, Plan_Start_Date__c=StartDate,Sales_Team1__c='Enterprise West',Plan_End_Date__c=closeDate,   Quarter__c=qt );
          insert pf;
         test.startTest();
            Integer rqmi = 12;
    		DateTime timenow = DateTime.now().addHours(rqmi);
    		CalculateForecastAmount2 rqm = new CalculateForecastAmount2();
    		String seconds = '0';
    		String minutes = String.valueOf(timenow.minute());
    		String hours = String.valueOf(timenow.hour());
    		String sch = seconds + ' ' + minutes + ' ' + hours + ' * * ?';
    		String jobName = 'test CalculateForecastAmount2 scheduler - ' + hours + ':' + minutes;
    		system.schedule(jobName, sch, rqm);
            database.executeBatch(new CalculateForecastAmount2()) ;
        test.stopTest();
       
        
    }
     @isTest static void test_edit_actuals2(){

        Account acct=[select Id from Account limit 1];
        Opportunity opp = [select Id, StageName from Opportunity Limit 1];
        system.debug('Opp StageName ' + opp.StageName);
        Account partnerAcc=TestDataUtility.createAccount('partner acct');
        insert partnerAcc;
        Partner__c  partner= TestDataUtility.createPartner(opp, acct, partnerAcc, 'Distributor');
        partner.Is_Primary__c=true;
        insert partner; 
        Date closeDate = [Select EndDate From Period Where type = 'Quarter' and StartDate = THIS_FISCAL_QUARTER].EndDate;
        Date StartDate = [Select StartDate From Period Where type = 'Quarter' and StartDate = THIS_FISCAL_QUARTER].StartDate;
        integer  Month = Date.Today().Month();
        integer   quarter = ((Month / 3) + 1);
        string qt=String.valueOf(quarter);
        if(opp.StageName != 'Win')
        {
            opp.StageName = 'Win';
            opp.Roll_Out_Start__c=StartDate;
            opp.Rollout_Duration__c=4;
            opp.CloseDate=StartDate.addDays(-15);
            update opp;
        }
       Roll_Out__c rollOut=[select id  from Roll_Out__c where 	Opportunity__c=:opp.Id limit 1];
        List<Roll_Out_Product__c> rollOutpro=[select id,Roll_Out_Plan__c,Product__c,Roll_Out_Start__c,Rollout_Duration__c,quote_quantity__c  from Roll_Out_Product__c where Roll_Out_Plan__c=:rollOut.Id];
        Map<id,Roll_Out_Product__c> rollOutproMap=new Map<id,Roll_Out_Product__c>();
        for(Roll_Out_Product__c rop : rollOutpro){
            rollOutproMap.put(rop.Id, rop);
        }
        
        List<Roll_Out_Schedule__c> insertRollOutSchedules = new List<Roll_Out_Schedule__c>();
            
        //call uility method to create ROS: HA_RosMiscUtilities
        for(Roll_Out_Schedule__c s: RosMiscUtilities.createROS(rollOutproMap)){
            insertRollOutSchedules.add(s);
        }

        insert insertRollOutSchedules;
        
        List<Roll_Out_Schedule__c> rollOutsch=[select id  from Roll_Out_Schedule__c where Roll_Out_Product__r.id in :rollOutpro];
        system.debug('rollOut***'+rollOut);
        system.debug('rollOutpro***'+rollOutpro);
        system.debug('rollOutsch***'+rollOutsch);
         system.debug('rollOutsch-Size***'+rollOutsch.size());
            Planing_and_Forecasting__c pf =new  Planing_and_Forecasting__c(Account__c=acct.id, Plan_Start_Date__c=StartDate,Sales_Team1__c='Enterprise West',Plan_End_Date__c=closeDate,   Quarter__c=qt );
          insert pf;
          test.startTest();
            /*Integer rqmi = 13;
    		DateTime timenow = DateTime.now().addHours(rqmi);
    		CalculateForecastAmount rqm = new CalculateForecastAmount();
    		String seconds = '0';
    		String minutes = String.valueOf(timenow.minute());
    		String hours = String.valueOf(timenow.hour());
    		String sch = seconds + ' ' + minutes + ' ' + hours + ' * * ?';
    		String jobName = 'test CalculateForecastAmount scheduler - ' + hours + ':' + minutes;
    		system.schedule(jobName, sch, rqm);*/
            database.executeBatch(new CalculateForecastAmount()) ;
            test.stopTest();
       
        
    }


}