public class CallReportController {
    
    @auraEnabled
    public static Call_Report__c getCallReport(string crId)
    {
        Call_Report__c cr;
        if(crId!=null && crId!='')
        {
            List<Call_Report__c> crList =[select Id,Name,Primary_Account__c,Primary_Account__r.Name,
                                          Meeting_Topics__c,Status__c,Agenda__c,Competitive_Intelligence__c,
                                          Meeting_Date__c,Meeting_Location__c,Meeting_Summary__c,
                                          recordTypeId,recordType.Name,Competitor_Compliance__c,Opportunities__c,
                                          Are_You_Sure_You_Would_Like_to_Submit__c,Number_of_Attendees__c,Meeting_Sub_Type__c,
                                          OwnerId,Owner.Name,createddate,createdbyId,createdby.Name,
                                          Lastmodifieddate,lastmodifiedbyId,lastmodifiedby.Name,
                                          Primary_Category__c,Timing__c,Week_Number__c,
                                          Sell_Through__c,WOS__c,BOS__c,Flooring__c,Online__c,Sell_in__c,
                                          Building_Blocks__c,
                                          Business_Performance_Notes__c,
                                          Product_Sub_Category__c,
                                          Meeting_Tone__c 
                                          from Call_Report__c where id=:crId];
                                           /*KMD: Removed Open_Issues__c and Post_CI_To_Account__c*/
                                           /*Thiru: Added Primary_Category__c,Timing__c,Week_Number__c,Sell_Through__c,WOS__c,BOS__c,Flooring__c,Online__c,Sell_in__c,Buiding_Blocks__c*/
            if(crList!=null && crList.size()>0)
            {
                cr= crList[0];
                if(cr.Agenda__c==null || cr.Agenda__c==''){
                    //cr.Agenda__c = '<ul><li>Insert Topic</li><li>Insert Topic</li><li>Insert Topic</li></ul>';
                    //cr.Meeting_Summary__c = 'Please limit text to 200 characters';
                }
            }
        } else {
            cr= new Call_Report__c();
            //cr.Agenda__c = '<ul><li>Insert Topic</li><li>Insert Topic</li><li>Insert Topic</li></ul>';
            //cr.Meeting_Summary__c = 'Please limit text to 200 characters';
            //cr.RecordTypeId='0120x0000004JV1';
            system.debug('cr-------------->'+cr);
        }
        return cr;
        
    }
    //Method used for getting B2B Call Report
    @auraEnabled
    public static Call_Report__c getCallReportB2B(string crId)
    {
        Call_Report__c cr;
        if(crId!=null && crId!='')
        {
            List<Call_Report__c> crList =[select Id,Name,Primary_Account__c,Primary_Account__r.Name,
                                          Meeting_Topics__c,Status__c,Agenda__c,Competitive_Intelligence__c,
                                          Meeting_Date__c,Meeting_Location__c,Meeting_Summary__c,
                                          recordTypeId,recordType.Name,Competitor_Compliance__c,Opportunities__c,Meeting_Sub_Type__c,
                                          Are_You_Sure_You_Would_Like_to_Submit__c,Number_of_Attendees__c,
                                          OwnerId,Owner.Name,createddate,createdbyId,createdby.Name,
                                          Lastmodifieddate,lastmodifiedbyId,lastmodifiedby.Name
                                          from Call_Report__c where id=:crId];
                                           /*KMD: Removed Open_Issues__c and Post_CI_To_Account__c*/
            if(crList!=null && crList.size()>0)
            {
                cr= crList[0];
            }
        } else {
            cr= new Call_Report__c();
            cr.Division__c='B2B';
            system.debug('cr-------------->'+cr);
        }
        return cr;
        
    }
    
    @auraEnabled
    public static Account getAccount(String accId) {
        /*Map<String,String> mapParams = ApexPages.CurrentPage().getParameters();
        String cId;
        for (String key:mapParams.keySet()) {
            System.debug('key='+key);
            System.debug('val='+mapParams.get(key));
            if (key.contains('_lkid')) {
               cId = mapParams.get(key); 
            }
        }*/
        System.debug('accId='+accId);
        Account acc=null;
        
        if(accId!=null && accId!=''){
            List<Account> accList=[select Id,Name from Account where id=:accId];
            if (accList!=null && accList.size()>0) {
                acc = accList[0];
                System.debug('acc server='+acc);
            }
        }
        return acc;
    }
    
    @auraEnabled
    public static RecordType getRecordType(String recTypeId) {
        RecordType rt = null;
        if (recTypeId!=null && recTypeId!='') {
           List<RecordType> rtLst = [SELECT Id,Name FROM RecordType WHERE id=:recTypeId]; //SobjectType='Call_Report__c'
            if (rtLst!=null && !rtLst.isEmpty()) {
                rt = rtLst[0];
            } 
        }
        
         return rt;
    }
    
    @AuraEnabled
     public static List <String> getselectOptions(sObject objObject, string fld) {
          system.debug('objObject --->' + objObject);
          system.debug('fld --->' + fld);
          List < String > allOpts = new list < String > ();
          // Get the object type of the SObject.
          Schema.sObjectType objType = objObject.getSObjectType();
         
          // Describe the SObject using its object type.
          Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
         
          // Get a map of fields for the SObject
          map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
         
          // Get the list of picklist values for this field.
          list < Schema.PicklistEntry > values =
           fieldMap.get(fld).getDescribe().getPickListValues();
         
          // Add these values to the selectoption list.
          for (Schema.PicklistEntry a: values) {
              
           allOpts.add(a.getValue());
          }
          system.debug('allOpts ---->' + allOpts);
          allOpts.sort();
         
        
          return allOpts;
     }
     /*Thiru: Added below Method to get the picklist values in Picklist Field Ordering*/
     @AuraEnabled
     public static List <String> getselectOptionsNoSorting(sObject objObject, string fld) {
          system.debug('objObject --->' + objObject);
          system.debug('fld --->' + fld);
          List < String > allOpts = new list < String > ();
          // Get the object type of the SObject.
          Schema.sObjectType objType = objObject.getSObjectType();
         
          // Describe the SObject using its object type.
          Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
         
          // Get a map of fields for the SObject
          map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
         
          // Get the list of picklist values for this field.
          list < Schema.PicklistEntry > values =
          fieldMap.get(fld).getDescribe().getPickListValues();
         
          // Add these values to the selectoption list.
          for (Schema.PicklistEntry a: values) {
              
           allOpts.add(a.getValue());
          }
        
          return allOpts;
     }
    
    @auraEnabled
    public static Call_Report__c saveCallReport(Call_Report__c  callReport)
    {
        System.debug('call repo='+ callReport);
        //if(callReport.id=='') callReport.id=null;
        upsert callReport;
        return callReport;
    }
    
    @auraEnabled
    public static Call_Report__c submitCallReportB2B(Call_Report__c  callReport)
    {
        System.debug('call repo='+ callReport);
        Id SubmittedRecordTypeId = Schema.SObjectType.Call_Report__c.getRecordTypeInfosByName().get('Submitted').getRecordTypeId();
        callReport.recordtypeid=SubmittedRecordTypeId;
        if(callReport.Status__c=='Cancelled'){
        	callReport.Are_You_Sure_You_Would_Like_to_Submit__c=false;
        }else{
            callReport.Status__c='Submitted';
        	callReport.Are_You_Sure_You_Would_Like_to_Submit__c=true;
        }
        
        update callReport;
        return callReport;
    }
    
    @auraEnabled
    public static List<Task> getTasks(Call_Report__c callReport)
    {
        System.debug('crid='+callReport.Id);
        List<Task> lstTasks=new List<Task>();
        if(callReport.Id != null){
            lstTasks=[select ID,Subject,OwnerId,Owner.Name,ActivityDate,Description,Activity_Type__c,Activity_Sub_Type__c,Call_Type__c,Action_Item__c from Task
                      where WhatId=:callReport.Id];
        }
        /*if(callReport.Id != null){
            String BuildingBlockStr = Label.Call_Report_Building_Blocks;
            List<String> BuildingBlocks = BuildingBlockStr.split(',');
            for(Task t : [select ID,Subject,OwnerId,Owner.Name,ActivityDate,Description,Activity_Type__c,Activity_Sub_Type__c,Call_Type__c,Action_Item__c  
                          from Task
                          where WhatId=:callReport.Id]){
                if(!BuildingBlocks.contains(t.Subject)){
                    lstTasks.add(t);
                }    
            }
        }*/
        
        return lstTasks;
    }
    
    //Get Building Blocks Tasks--Added--03/06/2019
    @auraEnabled
    public static List<Task> getBuildingBlockTasks(Call_Report__c callReport)
    {
        System.debug('crid='+callReport.Id);
        List<Task> lstTasks=new List<Task>();
        
        if(callReport.Id != null){
            String BuildingBlockStr = Label.Call_Report_Building_Blocks;
            List<String> BuildingBlocks = BuildingBlockStr.split(',');
            for(Task t : [select ID,Subject,OwnerId,Owner.Name,ActivityDate,Description,Activity_Type__c,Activity_Sub_Type__c,Call_Type__c,Action_Item__c,Task_Status__c from Task
                          where WhatId=:callReport.Id]){
                if(BuildingBlocks.contains(t.Subject)){
                    system.debug('contains');
                    lstTasks.add(t);
                }
            }
        }
        
        return lstTasks;
    }
    
    @auraEnabled
    public static List<Task> saveTasks(List<Task> tasks,Call_Report__c callReport)
    {
        System.debug('call report id='+callReport);
        List<Task> insertTasks = new List<Task>();
        List<Task> updateTasks = new List<Task>();
        for(Task t: tasks) {
            system.debug('Task Action Item---->'+t.Action_Item__c);
            //t.OwnerId = UserInfo.getUserId();
            t.Status = 'Open';
            t.Priority='High';
            //t.Type='Meeting';
            //t.Subject='Send Letter';
            if(t.Activity_Type__c=='Call'){
                t.Call_Type__c='Sales Support';
            }
            if (t.Id!=null) {
                updateTasks.add(t);
            } else {
                t.WhatId=callReport.Id;
                insertTasks.add(t);
            }
        }
        if (insertTasks.size()>0) insert insertTasks;
        if (updateTasks.size()>0) update updateTasks;
        return tasks;
    }
    
     @auraEnabled
    public static List<Product_Category_Note__c> getProductLine(Call_Report__c callReport)
    {
        List<Product_Category_Note__c> lstPL=null;
        if(callReport.Id!=null){        
            lstPL=[select id,Name,Division__c,Product_Category__c,Notes__c,Competitors__c,Product_Sub_Category__c from Product_Category_Note__c
                   where Call_Report__c=:callReport.Id];
        }
       
        return lstPL;
    }
    
    @auraEnabled
    public static List<Product_Category_Note__c> saveProductLine(List<Product_Category_Note__c> plines,
                                                                 Call_Report__c callReport)
    {
        List<Product_Category_Note__c> insertPC = new List<Product_Category_Note__c>();
        List<Product_Category_Note__c> updatePC = new List<Product_Category_Note__c>();
        
         for(Product_Category_Note__c pc: plines) {
             system.debug('Comp Values----->'+pc.Competitors__c);
            if (pc.Id==null) {
                pc.Call_Report__c=callreport.Id;
                insertPC.add(pc);
            } else {
                updatePC.add(pc);
            }
        }
        
        if (insertPC.size()>0) insert insertPC;
        if (updatePC.size()>0) update updatePC;
        return plines;
    }
    
    @auraEnabled
    public static List<Call_Report_Attendee__c> getMeetingAttendees(Call_Report__c callReport)
    {
        List<Call_Report_Attendee__c> lst=null;
        if(callReport.Id != null){ 
            lst=[select Customer_Attendee__c,Customer_Attendee__r.Name 
                 from Call_Report_Attendee__c 
                 where Call_Report__c=:callReport.Id];
        }                                          /*KMD: Remove Samsung_Attendee__c */
        return lst;
    }
    
    @auraEnabled
    public static List<Call_Report_Attendee__c> saveMeetingAttendees(List<Call_Report_Attendee__c> ma,Call_Report__c callReport)
    {
        List<Call_Report_Attendee__c> insertAtt = new List<Call_Report_Attendee__c>();
        List<Call_Report_Attendee__c> updateAtt = new List<Call_Report_Attendee__c>();
        system.debug('CallReportID----->'+callReport.Id);
        for(Call_Report_Attendee__c cra: ma) {
            if (cra.Id==null) {
                cra.Call_Report__c=callreport.Id;
                insertAtt.add(cra);
            } else {
                updateAtt.add(cra);
            }
        }
        
        if (insertAtt.size()>0) insert insertAtt;
        if (updateAtt.size()>0) update updateAtt;
        return ma;
    }
    
    @AuraEnabled
    public static List<Call_Report_Samsung_Attendee__c> getDefaultSamsungAttendees() {
        system.debug('Logged in User ID----->'+userinfo.getUserId());
        User u = [select id,name from user where id=:userinfo.getUserId()];
        system.debug('Logged in User Record----->'+u);
        List<Call_Report_Samsung_Attendee__c> lst=new List<Call_Report_Samsung_Attendee__c>();
        Call_Report_Samsung_Attendee__c c = new Call_Report_Samsung_Attendee__c(Attendee__c=u.id);
        lst.add(c);
        system.debug('Logged in User List----->'+lst);
        return lst;
    }
    
     @auraEnabled
    public static List<Call_Report_Samsung_Attendee__c> getSamsungAttendees(Call_Report__c callReport)
    {
        List<Call_Report_Samsung_Attendee__c> lst=null;
        System.debug('Sattended Call Report------->'+callReport.Id);
        if(callReport.Id!=null){
            lst=[select Attendee__c,Attendee__r.Name from Call_Report_Samsung_Attendee__c 
                 where Call_Report__c=:callReport.Id];
        }
        return lst;
    }
    
    
    @auraEnabled
    public static String saveSamsungAttendees(List<Call_Report_Samsung_Attendee__c> sa,Call_Report__c callReport)
    {
        List<Call_Report_Samsung_Attendee__c> insertAtt = new List<Call_Report_Samsung_Attendee__c>();
        List<Call_Report_Samsung_Attendee__c> updateAtt = new List<Call_Report_Samsung_Attendee__c>();
        system.debug('CallReportID----->'+callReport.Id);
        for(Call_Report_Samsung_Attendee__c crsa: sa) {
            system.debug('Att--->'+crsa.Attendee__c);
            if (crsa.Id==null) {
                crsa.Call_Report__c=callreport.Id;
                insertAtt.add(crsa);
            } else {
                updateAtt.add(crsa);
            }
        }
        try{
            if (insertAtt.size()>0) insert insertAtt;
        	if (updateAtt.size()>0) update updateAtt;
            return 'Success';
        }catch(exception e){
            return String.valueOf(e);
        }
        //if (insertAtt.size()>0) insert insertAtt;
        //if (updateAtt.size()>0) update updateAtt;
        //return sa;
    }
    
    @auraEnabled
    public static List<Call_Report_Sub_Account__c> getSubAccount(Call_Report__c callReport)
    {
        List<Call_Report_Sub_Account__c> lstsubAcc=null;
        if(callReport.Id!=null){
            lstsubAcc=[select Sub_Account__c,Sub_Account__r.Name,Account_Type__c from Call_Report_Sub_Account__c
                       where Call_Report__c=:callReport.Id];
        }
        return lstsubAcc;
    }
    
    @auraEnabled
    public static List<Call_Report_Sub_Account__c> saveSubAccount(List<Call_Report_Sub_Account__c> subAcc,Call_Report__c callReport)
    {
        List<Call_Report_Sub_Account__c> insertSAcc = new List<Call_Report_Sub_Account__c>();
        List<Call_Report_Sub_Account__c> updateSAcc = new List<Call_Report_Sub_Account__c>();
        
        for(Call_Report_Sub_Account__c sa: subAcc) {
            if (sa.Id==null) {
                sa.Call_Report__c=callreport.Id;
                insertSAcc.add(sa);
            } else {
                updateSAcc.add(sa);
            }
        }
        
        if (insertSAcc.size()>0) insert insertSAcc;
        if (updateSAcc.size()>0) update updateSAcc;
        
        return subAcc;
    }
    
    //Method to get existing Distribution Lists of call report
    @auraEnabled
    public static List<Call_Report_Distribution_List__c> getDistributionLists(Call_Report__c callReport)
    {
        List<Call_Report_Distribution_List__c> lst=null;
        //system.debug('callReport.Id----->'+callReport.Id);
        string CallReportId = callReport.Id;
        if(CallReportId!=null && CallReportId!=''){
            system.debug('callReport.Id----->'+callReport);
            lst=[select id,Name,Call_Report__c from Call_Report_Distribution_List__c where Call_Report__c=:callReport.Id];
            system.debug('Dist Lst Size----->'+lst.size());
        }
        return lst;
    }
    
    //Method to save new Distribution Lists of call report
    @auraEnabled
    public static List<Call_Report_Distribution_List__c> saveDistributionLists(List<String> distList,Call_Report__c callReport)
    {
        List<Call_Report_Distribution_List__c> updateDL = new List<Call_Report_Distribution_List__c>();
        system.debug('Helper Distribution List IDs ----->'+distList);
        for(Call_Report_Distribution_List__c dl: [select id,name,Call_Report__c,Call_Report__r.name from Call_Report_Distribution_List__c where id=:distList]) {
            system.debug('Distribution List----->'+dl);
            system.debug('Distribution List id----->'+dl.id);
            system.debug('Distribution List call rep id----->'+callReport.id);
            dl.Call_Report__c=callReport.id;
            updateDL.add(dl);
        }
        for(Call_Report_Distribution_List__c dl : updateDL){
            system.debug('Update Distribution List ID ----->'+dl.id);
        }
        
        if (updateDL.size()>0) update updateDL;
        return updateDL;
    }
    
    //Get the Dist Lists from CallReport_Distribution_List__c (Junction Object)--12/03/2018
    @auraEnabled
    public static List<CallReport_Distribution_List__c> getCRDistributionLists(Call_Report__c callReport)
    {
        List<CallReport_Distribution_List__c> lst=null;
        //system.debug('callReport.Id----->'+callReport.Id);
        string CallReportId = callReport.Id;
        if(CallReportId!=null && CallReportId!=''){
            system.debug('callReport.Id----->'+callReport);
            lst=[select id,Name,Call_Report__c,Call_Report_Distribution_List__c,Call_Report_Distribution_List__r.name from CallReport_Distribution_List__c where Call_Report__c=:callReport.Id];
            system.debug('Dist Lst Size----->'+lst.size());
        }
        return lst;
    }
    
    
    //Method to save new CallReport Distribution Lists of call report--12/03/2018
    @auraEnabled
    public static List<CallReport_Distribution_List__c> saveCRDistributionLists(List<CallReport_Distribution_List__c> CRdistList,Call_Report__c callReport)
    {
        List<CallReport_Distribution_List__c> insertDL = new List<CallReport_Distribution_List__c>();
        system.debug('Helper Distribution List IDs ----->'+CRdistList);
        for(CallReport_Distribution_List__c dl: CRdistList) {
            system.debug('CR Distribution List ID----->'+dl.Call_Report_Distribution_List__c);
            system.debug('CR Distribution List call rep id----->'+callReport.id);
            dl.Call_Report__c=callReport.id;
            insertDL.add(dl);
        }
        if (insertDL.size()>0) upsert insertDL;
        return insertDL;
    }
    
    //Method to delete selected Distribution Lists from call report
    @auraEnabled
    public static List<Call_Report_Distribution_List__c> DeleteDistributionLists(List<String> DistListNames, Call_Report__c CallRprt)
    {
        List<Call_Report_Distribution_List__c> Deletelst = new List<Call_Report_Distribution_List__c>();
        if(CallRprt.Id!=null){
            for(Call_Report_Distribution_List__c dl : [select id,Name,Call_Report__c from Call_Report_Distribution_List__c where Call_Report__c=:CallRprt.Id]){
                if(DistListNames.contains(dl.Name)){
                    dl.Call_Report__c=null;
                    Deletelst.add(dl);
                }
            }
        }
        if(Deletelst.size()>0){
            update Deletelst;
        }
        return null;
    }
    
    @auraEnabled
    public static String deleteRecords(List<Task> tasks, List<Product_Category_Note__c> pCategories,
                                       List<Call_Report_Attendee__c> crAttendee,List<Call_Report_Samsung_Attendee__c> crSamAttendee,
                                       List<Call_Report_Sub_Account__c> crSubAcc,List<Call_Report_Notification_Member__c> crNotifyMems,
                                       List<CallReport_Distribution_List__c> crDistLists, List<Call_Report_Market_Intelligence__c> crMI, 
                                       List<Call_Report_Business_Performance__c> crBP)
    {
        if (tasks!=null) {
            delete tasks;
        }
        
        if(pCategories!=null) {
            delete pCategories;
        }
        
        if(crAttendee!=null){
            delete crAttendee;
        }
        
        if(crSamAttendee!=null)
        {
            delete crSamAttendee;
        }
        
        if(crSubAcc!=null)
        {
            delete crSubAcc;
        }
        
        if(crNotifyMems!=null){
            delete crNotifyMems;
        }
        //Delete selected CR Dist Lists--12/03/2018 
        if(crDistLists!=null){
            delete crDistLists;
        }
        //Delete Market Intelligence--Added--03/06/2019
        if(crMI!=null){
            delete crMI;
        }
        if(crBP!=null){
            delete crBP;
        }
        return null;
        
    }
    
    @AuraEnabled  
    public static Map<String,List<String>> getDependentOptionsImpl(string objApiName , string contrfieldApiName , string depfieldApiName){
        return PicklistFieldController.getDependentOptionsImpl(objApiName, contrfieldApiName, depfieldApiName);
    }
    
    @AuraEnabled  
    public static Map<String,List<String>> getCBDProdCategoryOptions(string objApiName , string contrfieldApiName , string depfieldApiName){
        Map<String,List<String>> picklistMap = PicklistFieldController.getDependentOptionsImpl(objApiName, contrfieldApiName, depfieldApiName);
        for(string str : picklistMap.keySet()){
            if(str=='IT'){ //Need to remove hard coding and Add these values to Custom Label
                picklistMap.remove(str);
            }
            if(str=='Mobile'){
                List<string> lst = new List<string>();//Need to remove hard coding and Add these values to Custom Label
                if(picklistMap.get(str)!=null){
                    for(string s : picklistMap.get(str)){
                        if(s!='Phones' && s!='Accessories'){//Need to remove hard coding and Add these values to Custom Label
                            lst.add(s);
                        }
                    }
                }
                picklistMap.put(str,lst);
            }
        }
        return picklistMap;
    }
    
    @AuraEnabled  
    public static Map<String,List<String>> getDependentOptionsImplB2B(string objApiName , string contrfieldApiName , string depfieldApiName){
        Map<String,List<String>> picklistMap = PicklistFieldController.getDependentOptionsImpl(objApiName, contrfieldApiName, depfieldApiName);
        for(string str : picklistMap.keySet()){
            if(str!='IT' && str!='Mobile'){
                picklistMap.remove(str);
            }
            if(str=='Mobile'){
                List<string> lst = new List<string>();
                lst.add('Phones');
                lst.add('Accessories');
                picklistMap.put(str,lst);
            }
        }
        return picklistMap;
    }
    
    @AuraEnabled  
    public static List<String> getOptionsCallReportDivision(sObject objObject, string fld, string objApiName , string contrfieldApiName , string depfieldApiName, string division){
        Map<String,List<String>> PicklistMap = PicklistFieldController.getDependentOptionsImpl(objApiName, contrfieldApiName, depfieldApiName);
        Map<String,List<String>> returnMap = null;
        List<String> pickLists = null;
        system.debug('Map---->'+PicklistMap);
        if(PicklistMap.containsKey(division)){
            pickLists = PicklistMap.get(division);
        }
        return pickLists;
    }
    
    //Method to display dfault samsung attendee name--Added by Thiru
    @auraEnabled
    public static String getCurrentLoginUserName()
    {
        User u = [select id,name from user where id=:userinfo.getUserId()];
        String UName = u.name;
        return UName;
    }
    
    @auraEnabled
    public static String getInstanceUrl()
    {
        String InstanceURL = URL.getSalesforceBaseUrl().toExternalForm();
        return InstanceURL;
    }
    @AuraEnabled
    public static List<ContentDocumentLink> getFiles(String parentId){
        
        List<ContentDocumentLink> lstFiles = [SELECT Id, ContentDocumentId, ContentDocument.Title, 
                                            ContentDocument.FileType, LinkedEntityId, 
                                            LinkedEntity.Name,ShareType, Visibility
                   							FROM ContentDocumentLink 
                   							WHERE LinkedEntityId = :parentId];
        System.debug('no of files ='+lstFiles.size());
        return lstFiles;
    }
    
    //Method to get existing Call Report Notification Members of call report
    @auraEnabled
    public static List<Call_Report_Notification_Member__c> getCRnotificationMembers(Call_Report__c callReport)
    {
        List<Call_Report_Notification_Member__c> lst=null;
        //system.debug('callReport.Id----->'+callReport.Id);
        string CallReportId = callReport.Id;
        if(CallReportId!=null && CallReportId!=''){
            system.debug('callReport.Id----->'+callReport);
            lst=[select id,Member__c,Member__r.Name,Call_Report__c from Call_Report_Notification_Member__c where Call_Report__c=:callReport.Id];
            system.debug('Size----->'+lst.size());
        }
        return lst;
    }
    
    //Method to save new Call Report Notification Members of call report
    @auraEnabled
    public static List<Call_Report_Notification_Member__c> saveCRnotificationMembers(List<Call_Report_Notification_Member__c> crnmList,Call_Report__c callReport)
    {
        List<Call_Report_Notification_Member__c> insertList = new List<Call_Report_Notification_Member__c>();
        List<Call_Report_Notification_Member__c> updateList = new List<Call_Report_Notification_Member__c>();
        system.debug('CallReportID----->'+callReport.Id);
        for(Call_Report_Notification_Member__c crnm: crnmList) {
            if (crnm.Id==null) {
                crnm.Call_Report__c=callreport.Id;
                insertList.add(crnm);
            } else {
                updateList.add(crnm);
            }
        }
        
        if (insertList.size()>0) insert insertList;
        if (updateList.size()>0) update updateList;
        return crnmList;
    }
    
    //Method to get the B2B call report access for current logged in user--12/12/2018
    @auraEnabled
    public static String getRecordAccess(Call_Report__c callReport)
    {
        String hasAccess=null;
        id uid = userinfo.getUserId();
        //system.debug('CallReport----->'+callReport);
        //system.debug('CallReportID----->'+callReport.Id);
        //system.debug('userID----->'+uid);
        UserRecordAccess ura=null;
        ura = [SELECT RecordId, HasEditAccess, HasReadAccess FROM UserRecordAccess WHERE UserId=:uid AND RecordId=:callReport.id];
        if(ura.HasReadAccess==true && ura.HasEditAccess==false){
            hasAccess='ReadOnly';
        }else{
            hasAccess='Read/Write';
        }
        system.debug('hasAccess----->'+hasAccess);
        return hasAccess;
    }
    
    //Method to get Call Report Market Intelligence data----Added 03/05/2019
    @auraEnabled
    public static List<Call_Report_Market_Intelligence__c> getMarketIntelligence(Call_Report__c callReport)
    {
        System.debug('crid='+callReport.Id);
        List<Call_Report_Market_Intelligence__c> CallRepMI=null;
        if(callReport.Id != null){
            CallRepMI=[select ID,Name,Intelligence_Type__c,Intelligence__c,Comments__c,Category__c,Competitor__c 
                       from Call_Report_Market_Intelligence__c 
                       where Call_Report__c=:callReport.Id order by Intelligence_Type__c,Intelligence__c];
        }
        return CallRepMI;
    }
    //Method to save Call Report Market Intelligence data----Added 03/05/2019
    @auraEnabled
    public static String saveMarketIntelligence(List<Call_Report_Market_Intelligence__c> CallRepMI,Call_Report__c callReport)
    {
        List<Call_Report_Market_Intelligence__c> upsertCallRepMI = new List<Call_Report_Market_Intelligence__c>();
        
        for(Call_Report_Market_Intelligence__c mi: CallRepMI) {
            system.debug('callreport.Id---->'+callreport.Id);
            if (mi.Id==null) {
                mi.Call_Report__c=callreport.Id;
            }
            system.debug('mi---->'+mi);
            upsertCallRepMI.add(mi);
        }
        
        //if (upsertCallRepMI.size()>0) upsert upsertCallRepMI;
        try{
            if (upsertCallRepMI.size()>0) upsert upsertCallRepMI;
            return 'Success';
        }
        catch(exception e){
            return String.valueOf(e);
        }
        //return upsertCallRepMI;
    }
    
    @auraEnabled
    public static List<Call_Report_Business_Performance__c> getBizPerformance(Call_Report__c callReport){ //Added on 08/06/2019
        List<Call_Report_Business_Performance__c> crBPList=null;
        if(callReport.Id!=null){        
            crBPList=[select Id,Call_Report__c,Product_Category__c,Product_Sub_Category__c,BOS__c,Flooring__c,Online__c,
                             Sell_In__c,Sell_Through__c,WOS__c 
                      from Call_Report_Business_Performance__c
                      where Call_Report__c=:callReport.Id];
            if(callReport.Primary_Category__c!=null && crBPList.isEmpty()){
                if(callReport.Product_Sub_Category__c!=null){
                    String subCat = callReport.Product_Sub_Category__c;
                    for(String s : subCat.split(';')){
						crBPList.add(new Call_Report_Business_Performance__c(Product_Category__c=callReport.Primary_Category__c,Product_Sub_Category__c=s));
                    }
                }else{
                    crBPList.add(new Call_Report_Business_Performance__c(Product_Category__c=callReport.Primary_Category__c));
                }
            }
        }
        return crBPList;
    }
    
    @auraEnabled
    public static String saveBizPerformance(List<Call_Report_Business_Performance__c> CallRepBP,Call_Report__c callReport){ //Added on 08/06/2019
        List<Call_Report_Business_Performance__c> upsertCRBPList = new List<Call_Report_Business_Performance__c>();
        
        for(Call_Report_Business_Performance__c bp: CallRepBP) {
            system.debug('callreport.Id---->'+callreport.Id);
            if (bp.Id==null) {
                bp.Call_Report__c=callreport.Id;
            }
            system.debug('bp---->'+bp);
            upsertCRBPList.add(bp);
        }
        try{
            if (upsertCRBPList.size()>0) upsert upsertCRBPList;
            return 'Success';
        }
        catch(exception e){
            return String.valueOf(e);
        }
        
        //return upsertCRBPList;
    }
    
    @auraEnabled
    public static Map<String,String> getMIhoverText()
    {
        Map<String,String> MIhoverMap = new Map<String,String>();
        /*
        List<String> mIList = new List<String>();
        
        Schema.sObjectType objType = Call_Report_Market_Intelligence__c.getSObjectType();
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        map <String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
        list<Schema.PicklistEntry> values = fieldMap.get('Intelligence__c').getDescribe().getPickListValues();
        for (Schema.PicklistEntry a: values) {
            mIList.add(a.getValue());
        }
        
        List<Call_Report_Market_Intelligence_Value__mdt> valHoverList = [select id,label,DeveloperName,hover_text__c from Call_Report_Market_Intelligence_Value__mdt];
        
        for(String s : mIList){
            for(Call_Report_Market_Intelligence_Value__mdt val : valHoverList){
                if(s==val.label){
                    MIhoverMap.put(s, val.hover_text__c);
                }
            }
        }
		*/
        for(Call_Report_Market_Intelligence_Value__mdt val : [select id,label,hover_text__c from Call_Report_Market_Intelligence_Value__mdt order by Display_Order__c asc]){
			MIhoverMap.put(val.label, val.hover_text__c);
        }
        return MIhoverMap;
    }
    
    //Added on 09-12-2019
    @auraEnabled
    public static Map<String,List<String>> getMIButtonValues()
    {
        Map<String,List<String>> MIhoverMap = new Map<String,List<String>>();
        for(Call_Report_Market_Intelligence_Value__mdt val : [select id,Label,Intelligence_Type__c,Display_Order__c from Call_Report_Market_Intelligence_Value__mdt order by Display_Order__c asc]){
            List<String> Intelligence = new List<String>();
            if(MIhoverMap.containskey(val.Intelligence_Type__c)){
                Intelligence = MIhoverMap.get(val.Intelligence_Type__c);
            }
            Intelligence.add(val.Label);
            MIhoverMap.put(val.Intelligence_Type__c, Intelligence);
        }
        return MIhoverMap;
    }
}