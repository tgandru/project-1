@isTest
public class CallReportControllerTest {

    static testMethod void crTest() {
        
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        
        //Profile p = [SELECT Id FROM Profile WHERE Name='CBD Sales (temp)']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US',
                          //ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com');
        
         User u1 = new User(Alias = 'standt1', Email='standarduser1@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', 
                            //ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1@testorg.com');
        
        
        
        Account testAccount = new Account();
        testAccount.Name='Test Account' ;
        insert testAccount;
        
        Contact c= new Contact();
        c.FirstName='sruthi';
        c.LastName='reddy';
        c.Email='abc@gmail.com';
        c.Phone='123-456-7890';
        c.AccountId=testAccount.id;
        insert c;
        
        Contact c1= new Contact();
        c1.FirstName='Manasa';
        c1.LastName='reddy';
        c1.Email='def@gmail.com';
        c1.Phone='321-456-7890';
        c1.AccountId=testAccount.id;
        insert c1;
        
        
        RecordType rt = new RecordType();
        //rt.SobjectType='Call_Report__c';
        //insert(rt);
        List<RecordType> rtLst = [SELECT Id,Name FROM RecordType WHERE SobjectType='Call_Report__c'];
        if (rtLst!=null && !rtLst.isEmpty()) {
            rt = rtLst[0];
        } 
        
        Test.startTest();
        
        Call_Report__c cr=new Call_Report__c();
        cr.Name='cr1';
        cr.Primary_Account__c=testAccount.Id;
        cr.Agenda__c='Agenda';
        cr.Meeting_Summary__c='Meeting_Summary';
        cr.Competitive_Intelligence__c='Competitive_Intelligence';
        CallReportController.saveCallReport(cr);
        
        List<Task> t= new List<Task>();
        Task t1=new Task();
        t1.Subject='test Task';
        t.add(t1);
        CallReportController.saveTasks(t,cr);
        
        List<Product_Category_Note__c> pc =new List<Product_Category_Note__c>();
        id ProdCatRecTypeId = Schema.SObjectType.Product_Category_Note__c.getRecordTypeInfosByName().get('CBD').getRecordTypeId();
        Product_Category_Note__c pc1 = new Product_Category_Note__c();
        pc1.recordtypeid=ProdCatRecTypeId;
        pc1.Division__c='CE';
        pc1.Product_Category__c='Memory';
        pc.add(pc1);
        CallReportController.saveProductLine(pc,cr);
        
        List<Call_Report_Sub_Account__c> crsacc= new List<Call_Report_Sub_Account__c>();
        Call_Report_Sub_Account__c crsa = new Call_Report_Sub_Account__c();
        crsa.Account_Type__c='Local Store';
        crsa.Sub_Account__c=testAccount.Id;
        crsacc.add(crsa);
        CallReportController.saveSubAccount(crsacc,cr);
        
        List<Call_Report_Attendee__c> cra = new List<Call_Report_Attendee__c>();
        Call_Report_Attendee__c crat = new Call_Report_Attendee__c();
        crat.Customer_Attendee__c=c.Id;
        cra.add(crat);
        //CallReportController.saveMeetingAttendees(cra,cr);
        
        List<Call_Report_Samsung_Attendee__c> crsat = new List<Call_Report_Samsung_Attendee__c>();
        Call_Report_Samsung_Attendee__c crs = new Call_Report_Samsung_Attendee__c();
        crs.Attendee__c=u.Id;
        crsat.add(crs);
        CallReportController.saveSamsungAttendees(crsat,cr);
        
        List<Call_Report_Notification_Member__c> crNMList = new List<Call_Report_Notification_Member__c>();
        Call_Report_Notification_Member__c crNM = new Call_Report_Notification_Member__c();
        crNM.Member__c=UserInfo.getUserId();
        crNMList.add(crNM);
        CallReportController.saveCRnotificationMembers(crNMList,cr);
        
        List<string> crDList = new List<string>();
        Call_Report_Distribution_List__c crDL = new Call_Report_Distribution_List__c();
        crDL.name='Test Disty List';
        insert crDL;
        crDList.add(crDL.id);
        CallReportController.saveDistributionLists(crDList,cr);
        
        List<CallReport_Distribution_List__c> crdlList = new List<CallReport_Distribution_List__c>();
        CallReport_Distribution_List__c crd = new CallReport_Distribution_List__c(Call_Report_Distribution_List__c=crDL.id);
        insert crd;
        crdlList.add(crd);
        CallReportController.saveCRDistributionLists(crdlList,cr);
        
        List<Call_Report_Market_Intelligence__c> intelligence = new List<Call_Report_Market_Intelligence__c>();
        Call_Report_Market_Intelligence__c cust = new Call_Report_Market_Intelligence__c(Intelligence_Type__c='Customer Intelligence',Intelligence__c='State of Business',Comments__c='Comments');
        intelligence.add(cust);
        CallReportController.saveMarketIntelligence(intelligence,cr);
        
        List<Call_Report_Market_Intelligence__c> intelligenceDelete = new List<Call_Report_Market_Intelligence__c>();
        Call_Report_Market_Intelligence__c comp = new Call_Report_Market_Intelligence__c(call_report__c=cr.id,Intelligence_Type__c='Competitive Intelligence',Intelligence__c='Competitor Changes',Comments__c='Comments');
        insert comp;
        intelligenceDelete.add(comp);

        update cr;
        
        CallReportController.getRecordAccess(cr);
        CallReportController.getAccount(testaccount.Id);
        CallReportController.getCallReport(cr.Id);
        CallReportController.getCallReportB2B(cr.Id);
        CallReportController.getTasks(cr);
        CallReportController.getBuildingBlockTasks(cr);
        CallReportController.getProductLine(cr);
        CallReportController.getSubAccount(cr);
        CallReportController.getMeetingAttendees(cr);
        CallReportController.getSamsungAttendees(cr);
        
        CallReportController.getDefaultSamsungAttendees();
        CallReportController.getDistributionLists(cr);
        CallReportController.getCRDistributionLists(cr);
        CallReportController.getCurrentLoginUserName();
        CallReportController.getInstanceUrl();
        CallReportController.getFiles(cr.id);
        CallReportController.getCRnotificationMembers(cr);

        CallReportController.getselectOptions(cr,'Meeting_Topics__c');
        CallReportController.getRecordType(rt.Id);
        CallReportController.getDependentOptionsImpl('Product_Category_Note__c', 'Division__c','Product_Category__c');
        
        CallReportController.getDependentOptionsImplB2B('Product_Category_Note__c', 'Division__c','Product_Category__c');
        CallReportController.getOptionsCallReportDivision(cr, 'Meeting_Topics__c','Call_Report__c','Division__c','Meeting_Topics__c','B2B');
        
        CallReportController.getCallReport(null);
        CallReportController.getMIhoverText();

        //update
        crsa.Account_Type__c='Regional Branch';
        CallReportController.saveSubAccount(crsacc,cr);
        
		crat.Customer_Attendee__c=c1.Id;
        CallReportController.saveMeetingAttendees(cra,cr);
        
		crs.Attendee__c=u1.Id;
        CallReportController.saveSamsungAttendees(crsat,cr);
        
		pc1.Product_Category__c='TV';
        CallReportController.saveProductLine(pc,cr);
        
        t1.Subject='test Task1';
        CallReportController.saveTasks(t,cr);
        //CallReportController.submitCallReportB2B(cr);//Added by Thiru
        List<Call_Report_Business_Performance__c> crBP = new List<Call_Report_Business_Performance__c>();
        crBP = CallReportController.getBizPerformance(cr);
        CallReportController.saveBizPerformance(crBP,cr);
        
        CallReportController.deleteRecords(t, pc, cra, crsat, crsacc, crNMList, crdlList, intelligenceDelete, crBP);

        
        Test.stopTest();
        
     }  
}