public with sharing class CallReportPDFController {

    public call_report__c callreport{get;set;}
    public List<Task> crtasks {get;set;}
    public List<Product_Category_Note__c> pCategory{get;set;}
    public List<Call_Report_Sub_Account__c> sAcc{get;set;}
    public List<Call_Report_Attendee__c> custAttendee{get;set;}
    public List<Call_Report_Samsung_Attendee__c> samsungAttendee{get;set;}
    public List<Call_Report_Market_Intelligence__c> mIntelligence{get;set;}
    public List<Call_Report_Market_Intelligence__c> custIntelligence{get;set;}
    public List<Call_Report_Market_Intelligence__c> compIntelligence{get;set;}
    public List<Call_Report_Business_Performance__c> bizPerformance{get;set;}//Added on 08/07/2019
    public String ImgURL{get;set;}
    public List<String> busPerBlocks{get;set;}
    public List<String> busPerBlocksCategory{get;set;}
    public List<String> busPerBlocksSamsung{get;set;}
    public String subCategory{get;set;}

  /*  public CallReportPDFController(){
        String crId = ApexPages.currentPage().getParameters().get('id');
        if (crId==null) {
            crId='a2h0x0000004GxOAAU';
        }*/
        
         
    public CallReportPDFController(ApexPages.StandardController controller) {
        callreport= (Call_Report__C)controller.getRecord();
   
        callreport=[select Id,Name,Primary_Account__c,Primary_Account__r.Name,Division__c,
                          Meeting_Topics__c,Status__c,Agenda__c,Competitive_Intelligence__c,
                          Meeting_Date__c,Meeting_Location__c,Meeting_Summary__c,
                          recordTypeId,recordType.Name,Competitor_Compliance__c,Opportunities__c,
                          Are_You_Sure_You_Would_Like_to_Submit__c,Number_of_Attendees__c,Meeting_Sub_Type__c,
                          OwnerId,Owner.Name,createddate,createdbyId,createdby.Name,
                          Lastmodifieddate,lastmodifiedbyId,lastmodifiedby.Name,
                          Primary_Category__c,Timing__c,Week_Number__c,
                          Sell_Through__c,WOS__c,BOS__c,Flooring__c,Online__c,Sell_in__c,
                          Building_Blocks__c,
                          Business_Performance_Notes__c,
                          Product_Sub_Category__c,
                          Meeting_Tone__c  
                    from Call_Report__C 
                    where ID=:callreport.Id];
        system.debug('call report is'+callreport);
        
        subCategory = '';
        if(callreport.Product_Sub_Category__c!=null && callreport.Product_Sub_Category__c!=''){
            String subCategoryStr = callreport.Product_Sub_Category__c;
            for(String s : subCategoryStr.split(';')){
                if(subCategory=='') subCategory = s;
                else subCategory = subCategory+'; '+s;
            }
        }
        
        String filename = callreport.Name + '.pdf' ;

        Apexpages.currentPage().getHeaders().put( 'content-disposition', 'inline; filename=' + filename );
                
        if (callreport!=null) {
            prodCategory();
            crtasks();
            subAcc();
            custAttendee();
            samsungAttendee();
            marketIntelligence();
            businessPerformance();
        }
        
        Document doc = [select id from Document where Name='SamsungSVC Logo' limit 1];
        ImgURL = URL.getSalesforceBaseUrl().toExternalForm()+'/servlet/servlet.ImageServer?id='+doc.id+'&oid='+UserInfo.getOrganizationId();
        
        busPerBlocks = new List<String>();
        String str = Label.Call_Report_Market_Intelligence;
        busPerBlocks = str.split(',');
        busPerBlocksCategory = new List<String>();
        busPerBlocksSamsung = new List<String>();
        for(String s : busPerBlocks){
            if(s=='Sell-Through' || s=='Weeks of Supply'){
                busPerBlocksCategory.add(s);
            }else{
                busPerBlocksSamsung.add(s);
            }
        }
    }
    
      
    public List<Product_Category_Note__c> prodCategory()
    {
        pCategory =[select Division__c,Product_Category__c,Notes__c,Competitors__c,Product_Sub_Category__c from Product_Category_Note__c where Call_Report__c=:callreport.Id];
        system.debug('pcategory:'+pCategory);
        return pCategory;
    }
    
    public List<Task> crtasks()
    {
        crtasks=[select Subject,OwnerId,Owner.Name,ActivityDate,Description,Activity_Type__c,Activity_Sub_Type__c from Task where 
                 WhatId=:callreport.Id order by ActivityDate desc,Id desc];
        return crtasks;
    }
    
    public List<Call_Report_Sub_Account__c> subAcc()
    {
        sAcc=[select Sub_Account__c,Sub_Account__r.Name,Account_Type__c from Call_Report_Sub_Account__c where Call_Report__c=:callreport.Id];
        return sAcc;
    }
    
    public List<Call_Report_Attendee__c> custAttendee()
    {
        custAttendee=[select Customer_Attendee__c,Customer_Attendee__r.Name from Call_Report_Attendee__c where Call_Report__c=:callreport.Id];
        return custAttendee;
    }
     
    public List<Call_Report_Samsung_Attendee__c> samsungAttendee()
    {
        samsungAttendee=[select Attendee__c,Attendee__r.Name  from Call_Report_Samsung_Attendee__c where Call_Report__c=:callreport.Id];
        return samsungAttendee;
    }
    
    public List<Call_Report_Market_Intelligence__c> marketIntelligence()
    {
        mIntelligence = [select name,Intelligence_Type__c,Intelligence__c,Comments__c,Category__c,Competitor__c from Call_Report_Market_Intelligence__c where Call_Report__c=:callreport.Id order by Intelligence_Type__c,Intelligence__c];
        custIntelligence = new List<Call_Report_Market_Intelligence__c>();
        compIntelligence = new List<Call_Report_Market_Intelligence__c>();
        for(Call_Report_Market_Intelligence__c mi : mIntelligence){
            if(mi.Intelligence_Type__c=='Customer Intelligence') custIntelligence.add(mi);
            if(mi.Intelligence_Type__c=='Competitive Intelligence') compIntelligence.add(mi);
        }
        return mIntelligence;
    }
    //Added on 08/07/2019
    public List<Call_Report_Business_Performance__c> businessPerformance()
    {
        bizPerformance = [select Id,Call_Report__c,Product_Category__c,Product_Sub_Category__c,BOS__c,Flooring__c,Online__c,
                             Sell_In__c,Sell_Through__c,WOS__c 
                          from Call_Report_Business_Performance__c
                          where Call_Report__c=:callreport.Id];
        return bizPerformance;
    }

}