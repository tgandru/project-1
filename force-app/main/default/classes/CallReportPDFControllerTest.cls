@isTest
public class CallReportPDFControllerTest {
    static testMethod void PDFTest() {

        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        
        //Profile p = [SELECT Id FROM Profile WHERE Name='CBD Sales (temp)']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US',
                          //ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com');
        

        Account testAccount = new Account();
        testAccount.Name='Test Account' ;
        insert testAccount;
        
        Contact c= new Contact();
        c.FirstName='sruthi';
        c.LastName='reddy';
        c.Email='abc@gmail.com';
        c.Phone='123-456-7890';
        c.AccountId=testAccount.id;
        insert c;
        
		
        Call_Report__c cr=new Call_Report__c();
        cr.Name='cr1';
        cr.Primary_Account__c=testAccount.Id;
		insert cr;
        
        List<Task> t= new List<Task>();
        Task t1=new Task();
        t1.Subject='test Task';
        t.add(t1);
        
        List<Product_Category_Note__c> pc =new List<Product_Category_Note__c>();
        Product_Category_Note__c pc1 = new Product_Category_Note__c();
        pc1.Division__c='CE';
        pc1.Product_Category__c='Memory';
        pc.add(pc1);
        
        List<Call_Report_Sub_Account__c> crsacc= new List<Call_Report_Sub_Account__c>();
        Call_Report_Sub_Account__c crsa = new Call_Report_Sub_Account__c();
        crsa.Account_Type__c='Local Store';
        crsa.Sub_Account__c=testAccount.Id;
        crsacc.add(crsa);
        
        List<Call_Report_Attendee__c> cra = new List<Call_Report_Attendee__c>();
        Call_Report_Attendee__c crat = new Call_Report_Attendee__c();
        crat.Customer_Attendee__c=c.Id;
        cra.add(crat);
        
        List<Call_Report_Samsung_Attendee__c> crsat = new List<Call_Report_Samsung_Attendee__c>();
        Call_Report_Samsung_Attendee__c crs = new Call_Report_Samsung_Attendee__c();
        crs.Attendee__c=u.Id;
        crsat.add(crs);
        
        Document documentObj = new Document();
        documentObj.Body = Blob.valueOf('Some Document Text');
        documentObj.ContentType = 'application/pdf';
        documentObj.DeveloperName = 'SamsungSVCLogo';
        documentObj.IsPublic = true;
        documentObj.Name = 'SamsungSVC Logo';
        documentObj.FolderId = [select id from folder where name = 'Shared Documents'].id;
        insert documentObj;
        
        Test.startTest();
        
        ApexPages.StandardController sc = new ApexPages.StandardController(cr);
        CallReportPDFController crPDF = new CallReportPDFController(sc);
        
        crPDF.prodCategory();
        crPDF.crtasks();
        crPDF.subAcc();
        crPDF.custAttendee();
        crPDF.samsungAttendee();
        
        Test.stopTest();
    }
    
}