@isTest
public class CallReportPage_B2BControllerTest {
    static testMethod void crTest() {
        
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        
        Account testAccount = new Account();
        testAccount.Name='Test Account' ;
        insert testAccount;
        
        Call_Report__c crB2B=new Call_Report__c();
        crB2B.Name='cr2';
        crB2B.Division__c='B2B';
        crB2B.Primary_Account__c=testAccount.Id;
        //crB2B.Meeting_Topics__c='Meeting';
        crB2B.Agenda__c='Test Agenda';
        crB2B.Meeting_Summary__c='Test Meeting Summary';
        crB2B.Competitive_Intelligence__c='Test Cmpetitive Intelligence';
        insert crB2B;
        
        Test.startTest();
        
        Pagereference pg2 = page.CallReportPage_B2B; 
        test.setcurrentpage(pg2);
        ApexPages.currentPage().getparameters().put('AccountId',String.valueof(testAccount.id));
        ApexPages.StandardController sc = new ApexPages.StandardController(crB2B);
        CallReportPage_B2BController c = new CallReportPage_B2BController(sc);
        
        Pagereference pg3 = page.CallReportViewRedirect; 
        test.setcurrentpage(pg2);
        ApexPages.currentPage().getparameters().put('Id',String.valueof(crB2B.id));
        ApexPages.StandardController sc2 = new ApexPages.StandardController(crB2B);
        CallReportViewRedirectExt c2 = new CallReportViewRedirectExt(sc2);
        c2.Redirect();
        
        Test.stopTest();
     }
}