public class CallReportTemplateController {
    public Call_Report__c callreport;
    public id recordId {get;set;}
    public String domainURL;
    public List<Product_Category_Note__c> pCategory;
    public List<Task> crtasks {get;set;}
    public List<Call_Report_Sub_Account__c> sAcc;
    public List<Call_Report_Attendee__c> custAttendee;
    public List<Call_Report_Samsung_Attendee__c> samsungAttendee;
    public List<Call_Report_Market_Intelligence__c> mIntelligence;
    public List<Call_Report_Business_Performance__c> bizPerformance;//Added on 08/07/2019
    public List<String> busPerBlocks;
    public List<String> busPerBlocksCategory{get;set;}
    public List<String> busPerBlocksSamsung{get;set;}
    public List<Document> IconImages;
    public String ImgURL;
    public String OrgId;
    public String subCategory{get;set;}
    
    public CallReportTemplateController(){
    	busPerBlocks = new List<String>();
        String str = Label.Call_Report_Market_Intelligence;
        busPerBlocks = str.split(',');
        busPerBlocksCategory = new List<String>();
        busPerBlocksSamsung = new List<String>();
        for(String s : busPerBlocks){
            if(s=='Sell-Through' || s=='Weeks of Supply'){
                busPerBlocksCategory.add(s);
            }else{
                busPerBlocksSamsung.add(s);
            }
        }
    }
    
    public String getImgURL(){
        ImgURL = URL.getSalesforceBaseUrl().toExternalForm()+'/servlet/servlet.ImageServer?id=';
        return ImgURL;
    }
    
    public String getOrgId() {
        if(orgId == null) {
            orgId = UserInfo.getOrganizationId();
        }
        return orgId;
    }
    
    public List<Document> getIconImages(){
        IconImages = [Select id,name from Document where Folder.Name='CBD Sharing Documents'];
        return IconImages;
    }
    
    public Call_Report__c getcallreport() {
        if(CallReport == null) {
            try {
                callreport = [select Id,Name,Primary_Account__c,Primary_Account__r.Name,
                                          Meeting_Topics__c,Status__c,Agenda__c,Competitive_Intelligence__c,
                                          Meeting_Date__c,Meeting_Location__c,Meeting_Summary__c,
                                          recordTypeId,recordType.Name,Competitor_Compliance__c,Opportunities__c,
                                          Are_You_Sure_You_Would_Like_to_Submit__c,Number_of_Attendees__c,Meeting_Sub_Type__c,
                                          OwnerId,Owner.Name,createddate,createdbyId,createdby.Name,
                                          Lastmodifieddate,lastmodifiedbyId,lastmodifiedby.Name,
                                          Primary_Category__c,Timing__c,Week_Number__c,
                                          Sell_Through__c,WOS__c,BOS__c,Flooring__c,Online__c,Sell_in__c,
                                          Building_Blocks__c,
                                          Business_Performance_Notes__c,
                                          Product_Sub_Category__c,
                                          Meeting_Tone__c 
                                          from Call_Report__c where Id = :recordId];
                subCategory = '';
                if(callreport.Product_Sub_Category__c!=null && callreport.Product_Sub_Category__c!=''){
                    String subCategoryStr = callreport.Product_Sub_Category__c;
                    for(String s : subCategoryStr.split(';')){
                        if(subCategory=='') subCategory = s;
                        else subCategory = subCategory+'; '+s;
                    }
                }
            } 
            catch(Exception e) { }
        }
        return callreport;
    }
    
    public String getdomainURL() {
        if(domainURL == null) {
            domainURL = URL.getSalesforceBaseUrl().toExternalForm();
        }
        return domainURL;
    }
    
    public List<Product_Category_Note__c> getpCategory() {
        if(CallReport == null) {
            getcallreport();
        }
        if(CallReport == null) {
            return null;
        }
        if(pCategory == null) {
            pCategory = [select Division__c,Product_Category__c,Notes__c,Competitors__c,Product_Sub_Category__c from Product_Category_Note__c where Call_Report__c=:CallReport.Id];
        }
        return pCategory;
    }
    
    public List<Task> getcrtasks() {
        if(CallReport == null) {
            getcallreport();
        }
        if(CallReport == null) {
            return null;
        }
        if(crtasks == null) {
            crtasks = [select Subject,OwnerId,Owner.Name,ActivityDate,Description,Activity_Type__c,Activity_Sub_Type__c from Task where 
                 WhatId=:CallReport.Id order by ActivityDate desc,Id desc];
        }
        return crtasks;
    }
    
    public List<Call_Report_Sub_Account__c> getsAcc() {
        if(CallReport == null) {
            getcallreport();
        }
        if(CallReport == null) {
            return null;
        }
        if(sAcc == null) {
            sAcc = [select Sub_Account__c,Sub_Account__r.Name,Account_Type__c from Call_Report_Sub_Account__c where Call_Report__c=:CallReport.Id];
        }
        return sAcc;
    }
    
    public List<Call_Report_Attendee__c> getcustAttendee() {
        if(CallReport == null) {
            getcallreport();
        }
        if(CallReport == null) {
            return null;
        }
        if(custAttendee == null) {
            custAttendee = [select Customer_Attendee__c,Customer_Attendee__r.Name from Call_Report_Attendee__c where Call_Report__c=:CallReport.Id];
        }
        return custAttendee;
    }
    
    public List<Call_Report_Samsung_Attendee__c> getsamsungAttendee() {
        if(CallReport == null) {
            getcallreport();
        }
        if(CallReport == null) {
            return null;
        }
        if(samsungAttendee == null) {
            samsungAttendee = [select Attendee__c,Attendee__r.Name  from Call_Report_Samsung_Attendee__c where Call_Report__c=:CallReport.Id];
        }
        return samsungAttendee;
    }
    
    public List<Call_Report_Market_Intelligence__c> getmIntelligence() {
        if(CallReport == null) {
            getcallreport();
        }
        if(CallReport == null) {
            return null;
        }
        if(mIntelligence == null) {
            mIntelligence = [select name,Intelligence_Type__c,Intelligence__c,Comments__c,Category__c,Competitor__c from Call_Report_Market_Intelligence__c where Call_Report__c=:CallReport.Id];
        }
        return mIntelligence;
    }
    //Added on 08/07/2019
    public List<Call_Report_Business_Performance__c> getbizPerformance()
    {
        if(CallReport == null) {
            getcallreport();
        }
        if(CallReport == null) {
            return null;
        }
        if(bizPerformance == null) {
            bizPerformance = [select Id,Call_Report__c,Product_Category__c,Product_Sub_Category__c,BOS__c,Flooring__c,Online__c,
                                 Sell_In__c,Sell_Through__c,WOS__c 
                              from Call_Report_Business_Performance__c
                              where Call_Report__c=:CallReport.Id];
        }
        return bizPerformance;
    }
    
    /*public List<String> getbusPerBlocks() {
        busPerBlocks = new List<String>();
        String str = Label.Call_Report_Market_Intelligence;
        busPerBlocks = str.split(',');
        
        system.debug('busPerBlocks----->'+busPerBlocks);
        return busPerBlocks;
    }*/
}