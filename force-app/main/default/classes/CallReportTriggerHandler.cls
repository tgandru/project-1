public class CallReportTriggerHandler {
    public static void afterInsert(List<Call_Report__c> triggerNew){
        /*List<Call_Report_Samsung_Attendee__c> DefaultSamAttendees = new List<Call_Report_Samsung_Attendee__c>();
        for(Call_Report__c callRep : triggerNew){
            if(callRep.division__c=='CBD'){
                Call_Report_Samsung_Attendee__c SamAtt = new Call_Report_Samsung_Attendee__c(Call_Report__c=callRep.id,Attendee__c=callRep.createdbyid);
                DefaultSamAttendees.add(SamAtt);
            }
        }
        if(DefaultSamAttendees.size()>0){
            insert DefaultSamAttendees;
        }*/
    }
    
    //Method to Send call report submission notification to Distribution list of call reprot
    public static void NotifyCallReportSubmission(Map<id,Call_Report__c> triggerNewMap, Map<id,Call_Report__c> TriggerOldMap){

        Map<String,Set<String>> DistMemberMap = new Map<String,Set<String>>();

        id EmailTemplateId = [select id from emailtemplate where id=:Label.Call_Report_Submission_Template_Id].id; 
        
        Messaging.Email[] emailList = new Messaging.Email[0];
        for(Call_Report_Distribution_List__c DistList : [select id,Call_Report__c,(select id,Member__c,Member__r.email,Call_Report_Distribution_List__c from Call_report_Distribution_List_Members__r) from Call_Report_Distribution_List__c where Call_Report__c=:triggerNewMap.keySet()]){
            Set<String> MemberList = new Set<String>();
            for(Call_report_Distribution_List_Member__c mem : DistList.Call_report_Distribution_List_Members__r){
                MemberList.add(mem.Member__c);
                
            }
            DistMemberMap.put(DistList.Call_Report__c,MemberList);
        }
        
        //
        for(Call_Report_Notification_Member__c crNM : [Select id,Member__c,Member__r.email,Call_Report__c from Call_Report_Notification_Member__c where Call_Report__c=:triggerNewMap.keySet()]){
            if(DistMemberMap.containskey(crNM.Call_Report__c)){
                Set<String> memberList = DistMemberMap.get(crNM.Call_Report__c);
                memberList.add(crNM.Member__c);
                DistMemberMap.put(crNM.Call_Report__c,memberList);
            }else{
                Set<String> memberList = new Set<String>();
                memberList.add(crNM.Member__c);
                DistMemberMap.put(crNM.Call_Report__c,memberList);
            }
        }
        //Added on 09/06/2019--Starts
        for(Call_Report_Samsung_Attendee__c cr : [Select id,Attendee__c,Attendee__r.email,Call_Report__c from Call_Report_Samsung_Attendee__c where Call_Report__c=:triggerNewMap.keySet()]){
            if(DistMemberMap.containskey(cr.Call_Report__c)){
                Set<String> memberList = DistMemberMap.get(cr.Call_Report__c);
                memberList.add(cr.Attendee__c);
                DistMemberMap.put(cr.Call_Report__c,memberList);
            }else{
                Set<String> memberList = new Set<String>();
                memberList.add(cr.Attendee__c);
                DistMemberMap.put(cr.Call_Report__c,memberList);
            }
        }
        //Added on 09/06/2019--Ends
        for(Call_Report__c callRep : triggerNewMap.values()){
            List<String> toAddresses = new List<String>();
            if(callRep.Status__c=='Submitted' && TriggerOldMap.get(callRep.id).Status__c!='Submitted'){
                system.debug('<--------Submitted Call Report--------->');
                if(DistMemberMap.containskey(callRep.id)){
                    if(DistMemberMap.get(callRep.id)!=null){
                        system.debug('<--------Submitted Call Report Email notification--------->');
                        toAddresses.addAll(DistMemberMap.get(callRep.id));
                        if(toAddresses.size()>0){
                            String subject = 'Call Report Submission: '+callRep.Name+' - '+UserInfo.getName();
                            String body = 'The following call report has been submitted by  '+UserInfo.getName()+'.\n\nCall Report Name: '+callRep.Name+'\n\n'+'For more details, click the following link:\n'+callRep.Record_Link__c;
                            
                            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                            mail.setToAddresses(toAddresses);
                            //mail.setSubject(subject);
                            //mail.setPlainTextBody(body);
                            mail.setTargetObjectId(toAddresses[0]);
                            mail.setWhatId(callRep.Id);
                            mail.setTemplateId(EmailTemplateId);
                            mail.setSaveAsActivity(false);
                            mail.setUseSignature(false);
                            system.debug('mail------->'+mail);
                            emailList.add(mail);
                        }
                    }
                }
            }
        }
        
        if(emailList.size()>0){
            system.debug('Email List Size--------->'+emailList.size());
            Messaging.SendEmailResult[] results = Messaging.sendEmail( emailList,false );
        }
    }
    
    //Method to Send call report submission notification to Distribution list of call reprot
    public static void NotifyB2BCallReportSubmission(Map<id,Call_Report__c> triggerNewMap, Map<id,Call_Report__c> TriggerOldMap){
        system.debug('<--In NotifyB2BCallReportSubmission-->');
        Map<String,Set<String>> DistMemberMap = new Map<String,Set<String>>();
        
        Messaging.Email[] emailList = new Messaging.Email[0];
        
        List<CallReport_Distribution_List__c> crDlist = [Select id,Call_Report_Distribution_List__c,Call_Report__c from CallReport_Distribution_List__c where Call_Report__c=:triggerNewMap.keySet()];
        
        Map<String,String> crDlistMap = new Map<String,String>();
        for(CallReport_Distribution_List__c crDL : crDlist){
            crDlistMap.put(crDL.Call_Report_Distribution_List__c,crDL.Call_Report__c);
        }
        
        system.debug('crDlistMap-->'+crDlistMap);
        
        for(Call_report_Distribution_List_Member__c mem : [Select id,Member__c,Member__r.email,Call_Report_Distribution_List__c from Call_report_Distribution_List_Member__c where Call_Report_Distribution_List__c=:crDlistMap.keySet()]){
            
            /*Set<String> MemberList = new Set<String>();
            MemberList.add(mem.Member__c);
            if(crDlistMap.containsKey(mem.Call_Report_Distribution_List__c) && crDlistMap.get(mem.Call_Report_Distribution_List__c)!=null){
                DistMemberMap.put(crDlistMap.get(mem.Call_Report_Distribution_List__c),MemberList);
            }*/
            if(crDlistMap.containsKey(mem.Call_Report_Distribution_List__c) && crDlistMap.get(mem.Call_Report_Distribution_List__c)!=null){
                string callrprtId = crDlistMap.get(mem.Call_Report_Distribution_List__c);
                if(DistMemberMap.containskey(callrprtId)){
                    Set<String> MemberList = DistMemberMap.get(callrprtId);
                    MemberList.add(mem.Member__c);
                    DistMemberMap.put(crDlistMap.get(mem.Call_Report_Distribution_List__c),MemberList);
                }else{
                    Set<String> MemberList = new Set<String>();
                    MemberList.add(mem.Member__c);
                    DistMemberMap.put(crDlistMap.get(mem.Call_Report_Distribution_List__c),MemberList);
                }
            }
        }
        system.debug('DistMemberMap-->'+DistMemberMap);
        
        for(Call_Report_Notification_Member__c crNM : [Select id,Member__c,Member__r.email,Call_Report__c from Call_Report_Notification_Member__c where Call_Report__c=:triggerNewMap.keySet()]){
            if(DistMemberMap.containskey(crNM.Call_Report__c)){
                Set<String> memberList = DistMemberMap.get(crNM.Call_Report__c);
                memberList.add(crNM.Member__c);
                DistMemberMap.put(crNM.Call_Report__c,memberList);
            }else{
                Set<String> memberList = new Set<String>();
                memberList.add(crNM.Member__c);
                DistMemberMap.put(crNM.Call_Report__c,memberList);
            }
        }
        
        for(Call_Report__c callRep : triggerNewMap.values()){
            List<String> toAddresses = new List<String>();
            if(callRep.Status__c=='Submitted' && TriggerOldMap.get(callRep.id).Status__c!='Submitted'){
                system.debug('<----Submitted Call Report---->');
                if(DistMemberMap.containskey(callRep.id)){
                    if(DistMemberMap.get(callRep.id)!=null){
                        system.debug('<----Submitted Call Report Email notification---->');
                        system.debug('DistMemberMap.get(callRep.id)====>'+DistMemberMap.get(callRep.id));
                        toAddresses.addAll(DistMemberMap.get(callRep.id));
                        
                        String subject = 'Call Report Submission: '+callRep.Name+' - '+UserInfo.getName();
                        //String body = 'The following call report has been submitted by  '+UserInfo.getName()+'.\n\nCall Report Name: '+callRep.Name+'\n\n'+'For more details, click the following link:\n'+callRep.Record_Link__c;
                        //Commented above line & Added below line by Thiru on 01/14/2019 to add Call Report Agenda field to the Notification.
                        String body = 'The following call report has been submitted by  '+UserInfo.getName()+'.<br/><br/><b>Call Report Name: </b>'+callRep.Name+'<br/><b>Agenda:</b>'+callRep.Agenda__c+'<br/>For more details, click the following link:<br/>'+callRep.Record_Link__c;
                        
                        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                        mail.setToAddresses(toAddresses);
                        mail.setSubject(subject);
                        //mail.setPlainTextBody(body);
                        //Commented above line & Added below line by Thiru on 01/14/2019 to add Call Report Agenda field to the Notification.
                        mail.setHtmlBody(body);
                        mail.setSaveAsActivity(false);
                        mail.setUseSignature(false);
                        emailList.add(mail);
                    }
                }
            }
        }
        
        if(emailList.size()>0){
            system.debug('Email List Size--------->'+emailList.size());
            Messaging.SendEmailResult[] results = Messaging.sendEmail( emailList,false );
        }
    }
    
    public static void CreateAccountTask(List<Call_Report__c> triggerNew){
        List<Task> newTaskLst = new List<Task>();
        for(Call_Report__c cr : triggerNew){
            task t = new task();
            t.Subject = cr.name;
            t.Activity_Type__c = cr.Meeting_Topics__c;
            if(cr.Meeting_Sub_Type__c!=null && cr.Meeting_Sub_Type__c!='') t.Activity_Sub_Type__c = cr.Meeting_Sub_Type__c;
            t.ActivityDate = cr.Meeting_Date__c;
            string comments = cr.Agenda__c;
            comments = comments.replaceAll('\\<.*?\\>', '');
            t.Description = comments;
            t.whatId = cr.Primary_Account__c;
            t.OwnerId = cr.OwnerId;
            newTaskLst.add(t);
        }
        if(newTaskLst.size()>0){
            System.debug('New Tasks Insert Size--->'+newTaskLst.size());
            insert newTaskLst;
        }
    } 
}