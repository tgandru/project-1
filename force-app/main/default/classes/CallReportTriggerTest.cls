@isTest
public class CallReportTriggerTest {
    static testMethod void crTest() {
        
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        
        Account testAccount = new Account();
        testAccount.Name='Test Account' ;
        insert testAccount;
        
        Test.startTest();
        
        Call_Report__c cr=new Call_Report__c();
        cr.Name='cr1';
        cr.Primary_Account__c=testAccount.Id;
        cr.Agenda__c='Test Agenda';
        cr.Meeting_Summary__c='Test Meeting Summary';
        cr.Competitive_Intelligence__c='Test Cmpetitive Intelligence';
        cr.Division__c = 'CBD';
        cr.Agenda__c='Test Agenda';
        cr.Meeting_Summary__c='Test Summary';
        cr.Meeting_Date__c = date.today();
        cr.Meeting_Topics__c = 'Weekly';
        cr.Meeting_Tone__c = 'Positive';
        cr.Primary_Category__c = 'HA';
        cr.Product_Sub_Category__c = 'Laundry';
        cr.Week_Number__c = '40';
        cr.Status__c = 'Draft';
        insert cr;
        
        Call_Report_Distribution_List__c CRDlist = new Call_Report_Distribution_List__c(name='Test Distribution List',Call_Report__c=cr.id);
        insert CRDlist;
        
        id uid = UserInfo.getUserId();
        Call_report_Distribution_List_Member__c mem1 = new Call_report_Distribution_List_Member__c(Member__c=uid,Call_Report_Distribution_List__c=CRDlist.id);
        insert mem1;
        
        Call_Report_Notification_Member__c crNM = new Call_Report_Notification_Member__c();
        crNM.Member__c=UserInfo.getUserId();
        crNM.Call_Report__c=cr.id;
        insert crNM;
        
        Contact c= new Contact();
        c.FirstName='sruthi';
        c.LastName='reddy';
        c.Email='abc@gmail.com';
        c.Phone='123-456-7890';
        c.AccountId=testAccount.id;
        insert c;
        
        Call_Report_Attendee__c crat = new Call_Report_Attendee__c();
        crat.Customer_Attendee__c=c.Id;
        crat.Call_Report__c=cr.id;
        insert crat;
        
        List<Call_Report_Market_Intelligence__c> intelligence = new List<Call_Report_Market_Intelligence__c>();
        Call_Report_Market_Intelligence__c comp = new Call_Report_Market_Intelligence__c(call_report__c=cr.id,Intelligence_Type__c='Competitive Intelligence',Intelligence__c='Competitive Changes',Comments__c='Comments');
        intelligence.add(comp);
        Call_Report_Market_Intelligence__c cust = new Call_Report_Market_Intelligence__c(call_report__c=cr.id,Intelligence_Type__c='Customer Intelligence',Intelligence__c='State of Business',Comments__c='Comments');
        intelligence.add(cust);
        insert intelligence;
        
        cr.Building_Blocks__c='Digital Growth';
        cr.Status__c='Submitted';
        update cr;
        
        Call_Report__c crB2B=new Call_Report__c();
        crB2B.Name='cr2';
        crB2B.Division__c='B2B';
        crB2B.Primary_Account__c=testAccount.Id;
        //crB2B.Meeting_Topics__c='Event';
        crB2B.Agenda__c='Test Agenda';
        crB2B.Meeting_Summary__c='Test Meeting Summary';
        crB2B.Competitive_Intelligence__c='Test Cmpetitive Intelligence';
        insert crB2B;
        
        CallReport_Distribution_List__c CRDl = new CallReport_Distribution_List__c(Call_Report__c=crB2B.id,Call_Report_Distribution_List__c=CRDlist.id);
        insert CRDl;
        
        Call_Report_Notification_Member__c crNM2 = new Call_Report_Notification_Member__c();
        crNM2.Member__c=UserInfo.getUserId();
        crNM2.Call_Report__c=crB2B.id;
        insert crNM2;
        
        update crB2B;

        Test.stopTest();
        
     }
     
     static testMethod void crTestB2B() {
        
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        
        Account testAccount = new Account();
        testAccount.Name='Test Account' ;
        insert testAccount;
        
        Test.startTest();
        
        Call_Report__c cr=new Call_Report__c();
        cr.Name='cr1';
        cr.Primary_Account__c=testAccount.Id;
        cr.Division__c = 'B2B';
        cr.Agenda__c='Test Agenda';
        cr.Meeting_Summary__c='Test Summary';
        cr.Meeting_Date__c = date.today();
        cr.Meeting_Topics__c = 'Training';
        cr.Status__c = 'Draft';
        insert cr;
        
        Call_Report_Distribution_List__c CRDlist = new Call_Report_Distribution_List__c(name='Test Distribution List',Call_Report__c=cr.id);
        insert CRDlist;
        
        id uid = UserInfo.getUserId();
        Call_report_Distribution_List_Member__c mem1 = new Call_report_Distribution_List_Member__c(Member__c=uid,Call_Report_Distribution_List__c=CRDlist.id);
        insert mem1;
        
        Call_Report_Notification_Member__c crNM = new Call_Report_Notification_Member__c();
        crNM.Member__c=UserInfo.getUserId();
        crNM.Call_Report__c=cr.id;
        insert crNM;
        
        Contact c= new Contact();
        c.FirstName='sruthi';
        c.LastName='reddy';
        c.Email='abc@gmail.com';
        c.Phone='123-456-7890';
        c.AccountId=testAccount.id;
        insert c;
        
        Call_Report_Attendee__c crat = new Call_Report_Attendee__c();
        crat.Customer_Attendee__c=c.Id;
        crat.Call_Report__c=cr.id;
        insert crat;
        
        List<Call_Report_Market_Intelligence__c> intelligence = new List<Call_Report_Market_Intelligence__c>();
        Call_Report_Market_Intelligence__c comp = new Call_Report_Market_Intelligence__c(call_report__c=cr.id,Intelligence_Type__c='Competitive Intelligence',Intelligence__c='Competitive Changes',Comments__c='Comments');
        intelligence.add(comp);
        Call_Report_Market_Intelligence__c cust = new Call_Report_Market_Intelligence__c(call_report__c=cr.id,Intelligence_Type__c='Customer Intelligence',Intelligence__c='State of Business',Comments__c='Comments');
        intelligence.add(cust);
        insert intelligence;
        
        cr.Building_Blocks__c='Digital Growth';
        cr.Status__c='Submitted';
        update cr;
        
        Call_Report__c crB2B=new Call_Report__c();
        crB2B.Name='cr2';
        crB2B.Division__c='B2B';
        crB2B.Primary_Account__c=testAccount.Id;
        //crB2B.Meeting_Topics__c='Event';
        crB2B.Agenda__c='Test Agenda';
        crB2B.Meeting_Summary__c='Test Meeting Summary';
        crB2B.Competitive_Intelligence__c='Test Cmpetitive Intelligence';
        insert crB2B;
        
        CallReport_Distribution_List__c CRDl = new CallReport_Distribution_List__c(Call_Report__c=crB2B.id,Call_Report_Distribution_List__c=CRDlist.id);
        insert CRDl;
        
        Call_Report_Notification_Member__c crNM2 = new Call_Report_Notification_Member__c();
        crNM2.Member__c=UserInfo.getUserId();
        crNM2.Call_Report__c=crB2B.id;
        insert crNM2;
        
        update crB2B;

        Test.stopTest();
        
     }
}