public class CallReportViewRedirectExt {
    public string InstanceURL {get;set;}
    public string Reportid;
    
    public CallReportViewRedirectExt(ApexPages.StandardController controller){
        InstanceURL = URL.getSalesforceBaseUrl().toExternalForm();
    }
    
    public PageReference Redirect(){
        Reportid = apexpages.currentpage().getparameters().get('id');
        return null;
    }
}