global class CampaignInfluenceMigrationBatch implements Database.Batchable<sObject>,database.stateful{
     global string newmodelId;
    global CampaignInfluenceMigrationBatch(string newmodelId){
        this.newmodelId=newmodelId;
    }
    
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        if(Test.isRunningTest()){
                 return Database.getQueryLocator([SELECT CampaignId,ContactId,Id,Influence,IsDeleted,LastModifiedById,LastModifiedDate,ModelId,OpportunityId,RevenueShare FROM CampaignInfluence where Model.IsModelLocked=true limit 5]);
  
        }else{
            return Database.getQueryLocator([SELECT CampaignId,ContactId,Id,Influence,IsDeleted,LastModifiedById,LastModifiedDate,ModelId,OpportunityId,RevenueShare FROM CampaignInfluence where Model.IsModelLocked=true]);
  
        }
   }

   global void execute(Database.BatchableContext BC, List<CampaignInfluence> scope){
       List<CampaignInfluence> newlist=new List<CampaignInfluence>();
       
       for(CampaignInfluence c:scope){
           CampaignInfluence cn=new CampaignInfluence();
           cn=c.Clone();
           cn.ModelId=newmodelId;
           newlist.add(cn);
           
       } 
       
       If(newlist.size()>0){
           Database.insert(newlist,false);
       }
   }
 
 global void finish(Database.BatchableContext BC){
      
       AsyncApexJob a = [Select Id, Status,ExtendedStatus,NumberOfErrors,     JobItemsProcessed,
        TotalJobItems, CreatedBy.Email
        from AsyncApexJob where Id =:BC.getJobId()];
          string body='Hello,'+'\n <br></br>'+' We Sucessfully Migrated the Campaing Influence Data. Please Check the Data in Salesforce. ';
           body=body+'\n <br></br>';
           
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {a.CreatedBy.Email};
            mail.setToAddresses(toAddresses);
            mail.setSubject('Campaing Migration Data is  ' + a.Status);
            mail.setHTMLBody(body );
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
       
   }
 
}