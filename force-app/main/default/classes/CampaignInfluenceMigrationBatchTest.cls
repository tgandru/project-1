@isTest(seeAllData=true)
private class CampaignInfluenceMigrationBatchTest {

	private static testMethod void test() {
        Test.startTest();
         CampaignInfluenceModel cmodel =[SELECT DeveloperName,Id,IsActive,IsModelLocked FROM CampaignInfluenceModel WHERE IsActive = True limit 1];
         database.executeBatch(new CampaignInfluenceMigrationBatch(cmodel.Id));
        
        Test.StopTest();
	}

}