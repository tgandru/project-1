public class CampaignMemberTrgHandler {
    
    public static void updatedLeads(list<CampaignMember> newCM, map<id,CampaignMember> oldMap , boolean isUpdate ){
        map<string, string> leadStatusMap = new Map<string, string>();
        List<Task> lstTask = new List<Task>();
        Task objTask;
        leadStatusMap.put('New','New');
        leadStatusMap.put('No Sales','New');
        leadStatusMap.put('Invalid','New');
        leadStatusMap.put('Pursuit','Pursuit');
        leadStatusMap.put('Active','Active');
        leadStatusMap.put('Unreachable','New');
       
        map<string, Integer> rankMap = new map<string, Integer>();
        rankMap.put('A+',1);
        rankMap.put('A1',2);
        rankMap.put('A2',3);
        rankMap.put('B1',4);
        rankMap.put('B2',5);
        rankMap.put('O1',6);
        rankMap.put('T1',7);
        rankMap.put('A3',8);
        rankMap.put('A4',9);
        rankMap.put('B3',10);
        rankMap.put('B4',11);
        rankMap.put('C1',12);
        rankMap.put('C2',13);
        rankMap.put('C3',14);
        rankMap.put('C4',15);
        rankMap.put('D1',16);
        rankMap.put('D2',17);
        rankMap.put('D3',18);
        rankMap.put('D4',19);
        
       list<Lead> leadToUpdate = new list<Lead>();
        list<contact> contactToUpdate = new list<contact>();
        set<id> leadIds = new set<id>();
        set<id> contactids = new set<id>();
        set<id> campaignIds=  new set<id>();
        for(CampaignMember cm :newCM ){
            campaignIds.add(cm.campaignId);
             if(cm.leadId !=null) {
                 leadIds.add(cm.LeadId);
                 system.debug('lead is '+leadIds);
               }
           else if(cm.contactid !=null){
                 contactids.add(cm.contactid);
            }
            else{
                cm.addError('Please provide lead/contact id .');
            }
            /*if(cm.MQLSCORE__C ==null){
             cm.addError('Please Provide The MQL Score .');   
            }*/
        }
        
        map<id, Campaign> campaignMap = new map<id,campaign>([select id,Name,Ownerid, isActive from Campaign where id=:campaignIds and isActive=true]);
        if(leadIds!= null){
            map<id, lead> leadMap = new map<id,Lead>([select id,OwnerId,createdById,Assigned_Campaign__c, IsConverted,status,AssignedCampaignID__c,LastMQLUpdate__c,MQL_Score__c,Product_Interest__c,Reason__c, Unreachable_Counter__c from Lead where id=:leadIds and IsConverted = false]);
            
            for(CampaignMember cm :newCM ){
                if(cm.Non_MQL__c==false){ 
                        CampaignMember oldCM;
                        if(oldMap != null && oldMap.get(cm.id) != null){
                            oldCM = oldMap.get(cm.id);
                        }
                             //isUpdate == true && cm.leadId != null && cm.MQLSCORE__C != null && cm.MQLSCORE__C !=oldCM.MQLSCORE__C  && leadMap.get(cm.LeadId) != null && campaignMap.get(cm.campaignId) != null) || (isUpdate == false && cm.leadId != null && cm.MQLSCORE__C != null && leadMap.get(cm.LeadId) != null && campaignMap.get(cm.campaignId) != null)
                        if( (isUpdate == true && cm.leadId != null  && cm.MQLSCORE__C !=oldCM.MQLSCORE__C  && leadMap.get(cm.LeadId) != null && campaignMap.get(cm.campaignId) != null) || (isUpdate == false && cm.leadId != null && leadMap.get(cm.LeadId) != null && campaignMap.get(cm.campaignId) != null)){
                        
                            Lead ld = leadMap.get(cm.LeadId);
                            if(ld.Status == 'No Sale' || ld.Status == 'Invalid' || ld.Status == 'Unreachable'){
                                Lead tmp = new Lead(id=ld.id,Reason__c=null,MQL_Score__c=cm.MQLSCORE__C,status='New');
                                if(leadStatusMap.get(ld.Status) != null){
                                    tmp.status= leadStatusMap.get(ld.Status) ;
                                }
                                if(cm.Product_Interest__c != null){
                                    string tmpProuct = cm.Product_Interest__c.replace('[','');
                                    tmpProuct = cm.Product_Interest__c.replace(']','');
                                    tmp.Product_Interest__c = tmpProuct;
                                }
                                tmp.Unreachable_Counter__c = 0;
                                tmp.AssignedCampaignID__c = cm.CampaignId;
                                tmp.Assigned_Campaign__c= campaignMap.get(cm.CampaignId).Name;
                                tmp.LastMQLUpdate__c = system.now();
                                if(cm.Non_MQL__c==false){
                                  objTask = new Task(); 
                                         objTask.ActivityDate=System.today()+2;
                                         objTask.Subject = cm.MQLSCORE__C+'-'+campaignMap.get(cm.CampaignId).Name;
                                         objTask.Priority = 'High';
                                         objTask.Status = 'Open';
                                         objTask.Description = cm.MQL_Comments__c;
                                         objTask.type = 'Call';
                                         if(string.valueOf(leadMap.get(cm.leadId).OwnerId).startsWith('005')){
                                             objTask.ownerId = leadMap.get(cm.leadId).OwnerId;
                                         }else{
                                             objTask.ownerId = leadMap.get(cm.leadId).createdById;
                                         }
                                         objTask.WhoID=cm.leadId;
                                         objTask.CampaignID__c = campaignMap.get(cm.CampaignId).id;
                                         objTask.IsReminderSet = false;
                                         objTask.ReminderDateTime =  System.now()+2;
                                         objTask.Call_Type__c =  'Prospecting';
                                        
                                         lstTask.add(objTask);  
                                }
                                         
                                leadToUpdate.add(tmp);
                                 Date d=System.Today();
                               
                            }else{
                                integer mqlScore = 99;
                                integer leadscore =99;
                                if(rankMap.get(cm.MQLScore__C ) != null){
                                    mqlScore = rankMap.get(cm.MQLScore__C );
                                }
                                
                                if(ld.MQL_Score__c != null && rankMap.get(ld.MQL_Score__c ) != null){
                                    leadscore = rankMap.get(ld.MQL_Score__c );
                                }
                                if(mqlScore <=leadscore){
                                    Lead tmp = new Lead(id=ld.id);
                                    tmp.MQL_Score__c = cm.MQLScore__C;
                                    tmp.Reason__c = null;
                                    tmp.AssignedCampaignID__c =cm.CampaignId;
                                    tmp.Assigned_Campaign__c= campaignMap.get(cm.CampaignId).Name;
                                    tmp.Unreachable_Counter__c = 0;
                                    if(cm.Product_Interest__c != null){
                                        string tmpProuct = cm.Product_Interest__c.replace('[','');
                                        tmpProuct = cm.Product_Interest__c.replace(']','');
                                        tmp.Product_Interest__c = tmpProuct;
                                    }
                                    tmp.LastMQLUpdate__c = system.now();
                                    if(cm.Non_MQL__c==false){
                                         objTask = new Task(); 
                                         objTask.ActivityDate=System.today()+2;
                                         objTask.Subject = cm.MQLScore__C+'-'+campaignMap.get(cm.CampaignId).Name;
                                         objTask.Priority = 'High';
                                         objTask.Status = 'Open';
                                         objTask.Description = cm.MQL_Comments__c;
                                         objTask.type = 'Call';
                                         if(string.valueOf(leadMap.get(cm.leadId).OwnerId).startsWith('005')){
                                             objTask.ownerId = leadMap.get(cm.leadId).OwnerId;
                                         }else{
                                             objTask.ownerId = leadMap.get(cm.leadId).createdById;
                                         }
                                         objTask.WhoID=cm.leadId;
                                         objTask.CampaignID__c = campaignMap.get(cm.CampaignId).id;
                                         objTask.IsReminderSet = false;
                                          objTask.Call_Type__c =  'Prospecting';
                                         objTask.ReminderDateTime = System.now()+2;//.addHours(-1);
                                         lstTask.add(objTask);
                                    } 
                                    leadToUpdate.add(tmp);
                                }else{
                                    Lead tmp = new Lead(id=ld.id);
                                    tmp.MQL_Score__c = ld.MQL_Score__c;
                                    tmp.Reason__c = null;
                                    tmp.Assigned_Campaign__c= campaignMap.get(cm.CampaignId).Name;
                                    tmp.AssignedCampaignID__c =cm.CampaignId;
                                    if(cm.Product_Interest__c != null){
                                        string tmpProuct = cm.Product_Interest__c.replace('[','');
                                        tmpProuct = cm.Product_Interest__c.replace(']','');
                                        tmp.Product_Interest__c = tmpProuct;
                                    }    
                                     if(cm.Non_MQL__c==false){
                                         objTask = new Task(); 
                                         objTask.ActivityDate=System.today()+2;
                                         objTask.Subject = ld.MQL_Score__c+'-'+campaignMap.get(cm.CampaignId).Name;
                                         objTask.Priority = 'High';
                                         objTask.Status = 'Open';
                                         objTask.Description = cm.MQL_Comments__c;
                                         objTask.type = 'Call';
                                         if(string.valueOf(leadMap.get(cm.leadId).OwnerId).startsWith('005')){
                                             objTask.ownerId = leadMap.get(cm.leadId).OwnerId;
                                         }else{
                                             objTask.ownerId = leadMap.get(cm.leadId).createdById;
                                         }
                                         objTask.WhoID=cm.leadId;
                                         objTask.CampaignID__c = campaignMap.get(cm.CampaignId).id;
                                         objTask.IsReminderSet = false;
                                          objTask.Call_Type__c =  'Prospecting';
                                          objTask.ReminderDateTime = System.now()+2;//.addHours(-1);
                                         //objTask.ReminderDateTime=DateTime.newInstance(objTask.ActivityDate,Time.newInstance(8,00,0,0).addMilliseconds(UserTimeZoneOffsets.get(objTask.OwnerId)));
                                         lstTask.add(objTask);
                                         system.debug('lstTask******'+lstTask);
                                     }
                                    leadToUpdate.add(tmp);
                                }
                            }
                        }
               }        
            }
        }
        
        if(leadToUpdate.size() > 0){
             //update leadToUpdate;
             database.update(leadToUpdate,false);
             
        }
        if(!lstTask.isEmpty()){
            insert lstTask;
            system.debug('lstTask  '+lstTask);
        }
        
       if(contactids !=null){
             system.debug('conatct id Avilable ***');
            map<id, contact> conmap = new map<id,contact>([select id,MQL_Score__c,Account.ownerId from contact where id=:contactids ]);
           for(CampaignMember cm :newCM){
                if(cm.Non_MQL__c==false){
                        CampaignMember oldCM;
                        if(oldMap != null && oldMap.get(cm.id) != null){
                            oldCM = oldMap.get(cm.id);
                        }
                        
                       if(isUpdate == true && cm.ContactId != null &&  cm.MQLSCORE__C !=oldCM.MQLSCORE__C  && conmap.get(cm.ContactId) != null && campaignMap.get(cm.campaignId) != null || (isUpdate == false && cm.contactid != null  && conmap.get(cm.contactid) != null && campaignMap.get(cm.campaignId) != null)){
                                   system.debug('condition check ***');
                                  contact cd = conmap.get(cm.contactid);
                                integer mqlScore = 99;
                                integer contactscore =99;
                                if(rankMap.get(cm.MQLScore__C ) != null){
                                    mqlScore = rankMap.get(cm.MQLScore__C );
                                }
                                
                                if(cd.MQL_Score__c != null && rankMap.get(cd.MQL_Score__c ) != null){
                                    contactscore = rankMap.get(cd.MQL_Score__c );
                                } 
                                
                             if(mqlScore <=contactscore){
                                    contact tmp = new contact(id=cd.id);
                                    tmp.MQL_Score__c = cm.MQLScore__C;
                                   if(cm.Product_Interest__c != null){
                                        string tmpProuct = cm.Product_Interest__c.replace('[','');
                                        tmpProuct = cm.Product_Interest__c.replace(']','');
                                        tmp.Product_Interest__c = tmpProuct;
                                       
                                    }
                                    tmp.LastMQLUpdate__c = system.now();
                                    if(cm.Non_MQL__c==false){
                                         Task objTask1 = new Task(); 
                                         objTask1.ActivityDate=System.today()+2;
                                         objTask1.Description = cm.MQL_Comments__c;
                                         objTask1.IsReminderSet = true;
                                         objTask1.ReminderDateTime =  System.now()+2;//.addHours(-1);
                                         objTask1.ownerId = conmap.get(cm.contactid).Account.OwnerId;
                                         objTask1.Priority = 'High';
                                         objTask1.Status = 'Open';
                                         objTask1.Subject = cm.MQLScore__C+'-'+campaignMap.get(cm.CampaignId).Name;
                                         objTask1.type = 'Call';
                                           objTask1.Call_Type__c =  'Prospecting';
                                         objTask1.WhoID=conmap.get(cm.contactid).id;
                                         objTask1.CampaignID__c = campaignMap.get(cm.CampaignId).id;
                                         lstTask.add(objTask1);
                                    }
                                    contactToUpdate.add(tmp);
                                   
                                }else{
                                    contact tmp = new contact(id=cd.id);
                                    tmp.MQL_Score__c = cd.MQL_Score__c;
                                    if(cm.Product_Interest__c != null){
                                        string tmpProuct = cm.Product_Interest__c.replace('[','');
                                        tmpProuct = cm.Product_Interest__c.replace(']','');
                                        tmp.Product_Interest__c = tmpProuct;
                                    }
                                    contactToUpdate.add(tmp);
                                    if(cm.Non_MQL__c==false){
                                        Task objTask1 = new Task(); 
                                         objTask1.ActivityDate=System.today()+2;
                                         objTask1.Description = cm.MQL_Comments__c;
                                         objTask1.IsReminderSet = true;
                                         objTask1.ReminderDateTime =  System.now()+2;//.addHours(-1);
                                         objTask1.ownerId = conmap.get(cm.contactid).Account.OwnerId;
                                         objTask1.Priority = 'High';
                                         objTask1.Status = 'Open';
                                         objTask1.Subject = cd.MQL_Score__c+'-'+campaignMap.get(cm.CampaignId).Name;
                                         objTask1.type = 'Call';
                                           objTask1.Call_Type__c =  'Prospecting';
                                         objTask1.WhoID=conmap.get(cm.contactid).id;
                                          objTask1.CampaignID__c = campaignMap.get(cm.CampaignId).id;
                                         lstTask.add(objTask1);
                                    }
                                }
                        }
                }      
           } 
            
        }
        
        if(contactToUpdate.size()>0){
            database.update(contactToUpdate,false);
            system.debug('Contact lstTask ***'+lstTask);
             insert lstTask;
        }
    }
    
}