@isTest
private class CampaignMemberTrgHandlertest {

    private static testMethod void test() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Test.startTest();

        string   CampaignTypeId = [Select Id From RecordType Where SobjectType = 'Campaign' and Name = 'MARCOM'  limit 1].Id;
        string   LeadTypeId = [Select Id From RecordType Where SobjectType = 'Lead' and Name = 'End User'  limit 1].Id;
       Campaign cmp= new Campaign(Name='Test Campaign for test class ', IsActive=true,type='Email',StartDate=system.Today(),recordtypeid=CampaignTypeId);
       insert cmp;
       List<Lead> leadLst = new list<Lead>();
       
       lead le1=new lead(lastname='test lead 123',Company='My Lead Company',Email='mylead@sds.com', MQL_Score__c='T1',Status='New',LeadSource='Others',recordtypeid=LeadTypeId);
       leadLst.add(le1);
        lead le2=new lead(lastname='test lead 1232',Company='My Lead Company2',Email='mylead@sds2.com', MQL_Score__c='A+',Status='No Sale',LeadSource='Others',recordtypeid=LeadTypeId);
        leadLst.add(le2);
        lead le3=new lead(lastname='test lead 1233',Company='My Lead Company3',Email='mylead@sds3.com',Status='Account / Oppty',Division__c='IT',ProductGroup__c='OFFICE',LeadSource='Others',recordtypeid=LeadTypeId);
       leadLst.add(le3);
        insert leadLst;
        list<CampaignMember> CampaignMemberLst = new list<CampaignMember>();
        list<Task> tsks=new list<Task>();
        for(Lead l:leadLst){
            CampaignMember cm=new CampaignMember(Leadid = l.id,  status='Sent', Product_Interest__c='Desktop Monitor',  MQLScore__c='D4', campaignid = cmp.id);
            // Task t=new task(ActivityDate=System.today()+2,Subject = 'Test Task on Lead',Priority = 'High',Status = 'Open',WhoID=l.id,IsReminderSet = true,ReminderDateTime =  System.now()+2);
               CampaignMemberLst.add(cm);
                //tsks.add(t);
        }
        insert CampaignMemberLst;
        //insert tsks;
        
        Id endCustomer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('End Customer').getRecordTypeId();
        Account acc1 = new account(recordtypeId=endCustomer,Name ='testmember', Type='Customer',BillingStreet ='85 Challenger Road',BillingCity='Ridgefield Park',BillingState='NJ',BillingCountry='US',BillingPostalCode='07660');
        insert acc1;
        list<contact> cont=new list<contact>();
         contact con = new contact( LastName='mylastnamemember',Accountid=acc1.id,Email='member@testsds.com',MQL_Score__c='T1');
          contact con1 = new contact( LastName='mylastnamemember12',Accountid=acc1.id,Email='12member@testsds.com');
            cont.add(con);
            cont.add(con1);
            insert cont;
            list<CampaignMember> CampaignMembercont = new list<CampaignMember>();
            for(contact c:cont){
                CampaignMember newMember4=new CampaignMember(ContactId = c.id,  status='Sent',  Product_Interest__c='Desktop Monitor',  MQLScore__c='D4', campaignid = cmp.id);
                CampaignMembercont.add(newMember4);
            }
            
        insert CampaignMembercont;
       
        
        
        Test.stopTest();

        
    }

}