public class CaseCommentTrgHelper{
  
    public static void sendEmailToUsers(list<CaseComment> CaseCommentLst){
        Id B2bId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('SEA Internal').getRecordTypeId();
        map<id, Case> caseMap  = new map<id, Case>();
        map<id, User> userMap = new Map<id, User>();
        List<Messaging.SingleEmailMessage> mails =  new List<Messaging.SingleEmailMessage>();
        set<id> ParentIds = new set<id>();
        set<id> userIds = new set<id>();
        for(CaseComment cc : CaseCommentLst){
            ParentIds.add(cc.ParentId);
            userIds.add(cc.createdById );        
        }
        caseMap = new map<id,Case>([select id, OwnerId, createdById,CaseNumber,Origin,RecordTypeid from case where id =:ParentIds ]);
        for(case cs: caseMap.values()){
            if(cs.RecordTypeid == B2bId){
                string s1=cs.createdById;
                if(s1.startswith('005')){
                  userIds.add(cs.createdById);  
                }
                
                string s=cs.ownerId; 
                if(s.startswith('005')){
                     userIds.add(cs.ownerId); 
                }else{
                    //cs.addError('Please Take Case Ownership Before Adding the CaseComment');
                }
              
            }
            
        }
        userMap = new Map<id, User>([select id, Name,Email from user where id =:userIds]);
        
        system.debug('userMap  '+userMap);
        
        system.debug('caseMap  '+caseMap);
     
              for(CaseComment cc : CaseCommentLst){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setSubject('Case commented');
            string bdy ='';
            id i=caseMap.get(cc.ParentId).RecordTypeid;
            string ow=caseMap.get(cc.ParentId).OwnerId;
            string org=caseMap.get(cc.ParentId).Origin;
            if(i==B2bId){
                if(ow.startswith('005') ){
                if(caseMap.get(cc.ParentId) != null && userMap.get(caseMap.get(cc.ParentId).createdById) != null && caseMap.get(cc.ParentId).OwnerId== cc.createdById ){
                mail.setToAddresses(new list<String>{userMap.get(caseMap.get(cc.ParentId).createdById).Email});
                mail.setReplyTo(userMap.get(caseMap.get(cc.ParentId).OwnerId).Email);
                mail.setSenderDisplayName(userMap.get(caseMap.get(cc.ParentId).OwnerId).Name);
                bdy = returnMailBody(cc,userMap.get(caseMap.get(cc.ParentId).OwnerId),userMap.get(caseMap.get(cc.ParentId).createdById),caseMap.get(cc.ParentId));
                
            }else if(caseMap.get(cc.ParentId) != null && userMap.get(caseMap.get(cc.ParentId).OwnerId) != null && caseMap.get(cc.ParentId).createdById== cc.createdById ){
                mail.setToAddresses(new list<String>{userMap.get(caseMap.get(cc.ParentId).OwnerId).Email});
                mail.setReplyTo(userMap.get(caseMap.get(cc.ParentId).createdById).Email);
                mail.setSenderDisplayName(userMap.get(caseMap.get(cc.ParentId).createdById).Name);
                bdy = returnMailBody(cc,userMap.get(caseMap.get(cc.ParentId).createdById),userMap.get(caseMap.get(cc.ParentId).OwnerId),caseMap.get(cc.ParentId) );
            }
            if(bdy != null){
                mail.setHtmlBody(bdy);
                mails.add(mail);
            }
            }
            else{
                cc.addError('Please Take Case Ownership Before Adding the  CaseComment');  
            }
            }
            
            
        }
    
      
        
        
        if(mails.size() > 0){
             Messaging.sendEmail(mails);
        }
        
        
    }
    
  
    public static string returnMailBody(caseComment ct, user fromUser, user toUser, case cs){
        string Url = System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+cs.id;
        string bdy = 'Hi '+toUser.Name+',';
        bdy = bdy + '<br/> <br/> <b> Case Number : </b> <a href="'+Url+'">'+cs.CaseNumber+'</a>';
        bdy = bdy + '\n \n'+ '<br/> <b>Commented by : </b> '+fromUser.Name;
        bdy = bdy + '\n \n'+ '<br/> <br/> <b> Comment : </b>'+ct.CommentBody;
        return bdy;
      }
  
  }