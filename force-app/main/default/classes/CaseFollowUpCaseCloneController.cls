public class CaseFollowUpCaseCloneController {
   
   //added an instance varaible for the standard controller
    private ApexPages.StandardController controller {get; set;}
    
     // add the instance for the variables being passed by id on the url
    private Case c {get;set;}
    
    // set the id of the record that is created -- ONLY USED BY THE TEST CLASS
    public ID newRecordId {get;set;}

    // initialize the controller
    public CaseFollowUpCaseCloneController (ApexPages.StandardController controller) {

        //initialize the stanrdard controller
        this.controller = controller;
        
        // load the current record
        c = (Case)controller.getRecord();
        
    }

    // method called from the VF's action attribute to clone the case
    public PageReference CaseFollowUpCaseClone() {
    
         // setup the save point for rollback
         Savepoint sp = Database.setSavepoint();
         Case newCase;
        
        if(c.Status == 'Closed'){
             try {
                
                  //copy the case - ONLY INCLUDE THE FIELDS YOU WANT TO CLONE
                c = [select Id,
                            AccountId,
                            AssetId,
                            BusinessHoursId,
                            CaseNumber,
                            Origin,
                            OwnerId,
                            Reason,
                            RecordTypeId,
                            SourceId,
                            IsClosedOnCreate,
                            ContactEmail,
                            ContactFax,
                            ContactMobile,
                            ContactId,
                            ContactPhone,
                            Description,
                            EntitlementId,                       
                            Priority,
                            ProductId,
                            Type,
                            Subject,
                            Status,
                            SuppliedCompany,
                            SuppliedEmail,
                            SuppliedName,
                            SuppliedPhone,
                            Android_Version__c,
                            Build_Number__c,
                            Business_Priority__c,
                            Case_Classification__c,
                            Case_Close_Comments__c,
                            Case_Comment_Changed__c,
                            Case_Progress__c,
                            Category__c,
                            Close_ETA__c,
                            Customer_Impact__c,
                            Developer_Assigned_To__c,
                            Device__c,
                            Division__c,
                            Due_Date__c,
                            EAP__c,
                            Error_Message__c,
                            External_Escalation__c,
                            Follow_Up_Case__c,
                            Frequency_CD__c,
                            GS6_Plus_ADL__c,
                            Inbound_Shipping_Status__c,
                            Inbound_Tracking_Number__c,
                            International_Device__c,
                            Issue_Description__c,
                            JIRA_Id__c,
                            Category_1__c,
                            Level_1__c,
                            Category_2__c,
                            Level2SymptomCode__c,
                            Level_3_Symptom_Code_del__c,
                            LIcense_Key__c,
                            MDM_Type__c,
                            Note_5_ADL__c,
                            Outbound_Shipping_Status__c,
                            Outbound_Tracking_Number__c,
                            Priority__c,
                            Product_Name__c,
                            Remedy__c,
                            Remote_Support__c,
                            Repair_Start_Date__c,
                            Replacement_Device_IMEI__c,
                            Resolution__c,
                            Resolution_Category__c,
                            SerialNumber__c,
                            Service_Provider_Account__c,
                            Service_Provider_Contact__c,
                            Solution_Version__c,
                            SparkAFW__c,
                            Steps_to_reproduce__c,
                            Sub_Status__c,
                            Support_SKU_Type__c,
                            Temp_Case_Comment__c,
                            URL__c                       
                      from Case 
                      where id = :c.id]; 
                     
                 newCase = c.clone(false);
                             
                 // set the Parent of the new case to the original case  
                 newCase.Original_Case__c = c.id;
                 
                 // set the Status of the new case to Assigned
                 newCase.Status = 'New';
                             
                 // mark the new case as a follow up case in the Subject  
                 newCase.Subject = 'Follow-Up:' + c.Subject;         
                 
                 // mark the new case as a follow up case  
                 newCase.Follow_Up_Case__c = true;
                 
                 // mark the new case a Urgent
                 newCase.Severity__c = '1_Urgent';
                 
                 insert newCase;
    
                 // set the id of the new case created for testing
                   newRecordId = newCase.id;
              
             
    
             } catch (Exception e){
                 // roll everything back in case of error
                Database.rollback(sp);
                ApexPages.addMessages(e);
                return null;
             }
        } 
         
        else{
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Case is NOT closed. Cannot create Follow-Up Case.'));
            return null;
        } 

        //determine if current user is a portal user
        List<Profile> currentprofile = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
        String MyProfileName = currentprofile[0].Name;
        //if(MyProfileName == 'Samsung Customer Community User'){
        if(MyProfileName.contains('Community User')){ 
            //if in the portal simply return the new Case
             return new PageReference('/'+newCase.id);
        }
        
        //otherwise redirect user to the edit page of the new Case
        return new PageReference('/'+newCase.id+'/e?retURL=%2F'+newCase.id);
    }


}