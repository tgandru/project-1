/* Author: Jeongho Lee
*  Description: Case Manager Report
*/
public with sharing class CaseManagerReport {

	public SearchObject				searchObj		{get;set;} 
	public List<DataWrapper>		dataList		{get;set;}
	public Map<String,String>		svcMap			{get;set;}
	public List<String>				columns			{get;set;}
	public List<String>				viewColumns		{get;set;}
	public Date						tempDate		{get;set;}
	public List<DataWrapper> previousWeekList {get;set;}

	public CaseManagerReport(){
		if(ApexPages.currentPage().getParameters().get('tempDate') != null){
			tempDate = Date.parse(ApexPages.currentPage().getParameters().get('tempDate'));
		}
	}

	public void init() {
		this.searchObj			=	new SearchObject();
		ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Please Select Date'));
	}
	
	public void listCaseManagerReport(){
		this.dataList 			=	new List<DataWrapper>();
		this.svcMap 			=	new Map<String, String>();
		this.columns			=	new List<String>();
		this.viewColumns		=	new List<String>();
		//this.setType			=	new set<String>();
		previousWeekList = new List<DataWrapper>();

		for (ReportService__c r : [SELECT Name, Case_Service_Type__c, Report_Service_Type__c FROM ReportService__c ORDER BY Report_Service_Type__c DESC]) {
	    	svcMap.put(r.Case_Service_Type__c, r.Report_Service_Type__c);
	    	//setType.add(r.Report_Service_Type__c);
		}
		Map<Integer, String> monMap = new Map<Integer, String> {1=>'Jan', 2=>'Feb', 3=>'Mar', 4=>'Apr', 5=>'May', 6=>'Jun', 7=>'Jul', 8=>'Aug', 9=>'Sep', 10=>'Oct', 11=>'Nov', 12=>'Dec'};

		system.debug('=======> svcMap : ' + svcMap);
		
		//currenty year , before year, weekNumber
		Integer curYear = null;
		Integer befYear = null;
		Date 	weeksAgo = null;
		Date	yearsAgo = null;
		if(tempDate != null){
			curYear		= tempDate.year();
			befYear		= tempDate.addYears(-1).year();
			weeksAgo	= tempDate.addDays(-35);
			yearsAgo	= tempDate.addDays(-365);
		}
		else{
			curYear		= searchObj.so.startDate__c.year();
			befYear		= searchObj.so.startDate__c.addYears(-1).year();
			weeksAgo	= searchObj.so.startDate__c.addDays(-35);
			yearsAgo	= searchObj.so.startDate__c.addDays(-365);
		}


		system.debug('======> Week 5 ago : ' + weeksAgo);

		columns.add(String.valueOf(befYear));
		columns.add(String.valueOf(curYear));
		
		for(Integer i = 1; i < 13; i++){
			Date tempDate = yearsAgo.addMonths(i);
			String strYear = String.valueOf(tempDate.year());
			columns.add(monMap.get(tempDate.month()) + '-' + strYear);
		}
		
		for(Integer j = 1; j < 6; j++){
			columns.add(String.valueOf(isoYearNumber(weeksAgo.addDays(j*7))) + 'Wk' + String.valueOf(isoWeekNumber(weeksAgo.addDays(j*7))));
		}

		for(String s: columns){
			DataWrapper dw = new DataWrapper();
			dw.extIds = s;
			dataList.add(dw);
			if(s.contains('Wk')) s = s.remove(s.left(4));

			viewColumns.add(s);
		}
		
		// previous week pending list.
		Integer currentWeek;
		String startWeek;
		String endWeek;
		if(tempDate != null){
			currentWeek = isoWeekNumber(tempDate);
			startWeek = String.valueOf(currentWeek - 5);
			endWeek = String.valueOf(currentWeek);
		}
		else{
			currentWeek = isoWeekNumber(searchObj.so.startDate__c);
			startWeek = String.valueOf(currentWeek - 5);
			endWeek = String.valueOf(currentWeek);
		}

		List<AggregateResult> groupedResults = [
			SELECT 
				C_Report_Week__c, Count(Id) cnt FROM Case_SnapShot__c 
			WHERE
				C_Report_Week__c < :endWeek AND C_Report_Week__c >= :startWeek
				AND Status__c != 'Closed'
				AND CALENDAR_YEAR(Case_CreatedDate__c) >=: curYear
			GROUP BY C_Report_Week__c
			ORDER BY C_Report_Week__c
		];

		for (AggregateResult a : groupedResults) {
			DataWrapper dw = new DataWrapper();
			dw.extIds = (String)a.get('C_Report_Week__c');
			dw.preWeekCount = (Integer)a.get('cnt');

			previousWeekList.add(dw);
		}
		List<Case_SnapShot__c> cssList = [SELECT Id
											, Name
											, Case_CreatedDate__c
											, EntitlementId__r.Asset.Product2.SC_Service_Type__c 
											, Status__c
											, C_Report_Week__c
											, Origin__c
											FROM Case_SnapShot__c 
											WHERE CALENDAR_YEAR(Case_CreatedDate__c) >=: befYear];

		system.debug('======> cssList : ' + cssList.size());	



		for(Case_SnapShot__c css : cssList){
			for(DataWrapper dw : dataList){
				if(svcMap.get(css.EntitlementId__r.Asset.Product2.SC_Service_Type__c) != null){

					//year
					if(svcMap.get(css.EntitlementId__r.Asset.Product2.SC_Service_Type__c) == 'Signature' && dw.extIds == String.valueOf(css.Case_CreatedDate__c.year())){
						dw.year = String.valueOf(css.Case_CreatedDate__c.year());
						dw.signature = ++dw.signature;
						dw.totalNewCase = ++dw.totalNewCase;
						
						if(css.status__c == 'Closed') dw.sbsClose = ++dw.sbsClose;
						else dw.pendingTotal = ++dw.pendingTotal;

						if(css.Origin__c == 'Phone') dw.voiceSignature = ++dw.voiceSignature;
						if(css.Origin__c == 'Email') dw.emailSignature = ++dw.emailSignature;
						if(css.Origin__c == 'Web') dw.portalSignature = ++dw.portalSignature;
						if(css.Origin__c == 'Phone' || css.Origin__c == 'Email' || css.Origin__c == 'Web') dw.inboundTotal = ++dw.inboundTotal;
					}
					//year
					if(svcMap.get(css.EntitlementId__r.Asset.Product2.SC_Service_Type__c) == 'Premier' && dw.extIds == String.valueOf(css.Case_CreatedDate__c.year())){
						dw.year = String.valueOf(css.Case_CreatedDate__c.year());
						dw.premier = ++dw.premier;
						dw.totalNewCase = ++dw.totalNewCase;

						if(css.status__c == 'Closed') dw.sbsClose = ++dw.sbsClose;
						else dw.pendingTotal = ++dw.pendingTotal;

						if(css.Origin__c == 'Phone') dw.voicePremier = ++dw.voicePremier;
						if(css.Origin__c == 'Email') dw.emailPremier = ++dw.emailPremier;
						if(css.Origin__c == 'Web') dw.portalPremier = ++ dw.portalPremier;
						if(css.Origin__c == 'Phone' || css.Origin__c == 'Email' || css.Origin__c == 'Web') dw.inboundTotal = ++dw.inboundTotal;
					}
					//year
					if(svcMap.get(css.EntitlementId__r.Asset.Product2.SC_Service_Type__c) == 'Other' && dw.extIds == String.valueOf(css.Case_CreatedDate__c.year())){
						dw.year = String.valueOf(css.Case_CreatedDate__c.year());
						dw.other = ++dw.other;
						dw.totalNewCase = ++dw.totalNewCase;

						if(css.status__c == 'Closed') dw.sbsClose = ++dw.sbsClose;
						else dw.pendingTotal = ++dw.pendingTotal;

						if(css.Origin__c == 'Phone') dw.voiceOther = ++dw.voiceOther;
						if(css.Origin__c == 'Email') dw.emailOther = ++dw.emailOther;
						if(css.Origin__c == 'Web') dw.portalOther = ++ dw.portalOther;
						if(css.Origin__c == 'Phone' || css.Origin__c == 'Email' || css.Origin__c == 'Web') dw.inboundTotal = ++dw.inboundTotal;
						
					}
					//year
					//if(svcMap.get(css.EntitlementId__r.Asset.Product2.SC_Service_Type__c) == 'KNOX' && dw.extIds == String.valueOf(css.Case_CreatedDate__c.year())){
					//	dw.year = String.valueOf(css.Case_CreatedDate__c.year());
					//	dw.knox = ++dw.knox;
					//	dw.totalNewCase = ++dw.totalNewCase;

					//	if(css.status__c == 'Closed') dw.knoxClose = ++dw.knoxClose;
					//	else dw.pendingTotal = ++dw.pendingTotal;

					//	if(css.Origin__c == 'Phone') dw.voiceKnox = ++dw.voiceKnox;
					//	if(css.Origin__c == 'Email') dw.emailKnox = ++dw.emailKnox;
					//	if(css.Origin__c == 'Web') dw.portalKnox = ++ dw.portalKnox;
					//	if(css.Origin__c == 'Phone' || css.Origin__c == 'Email' || css.Origin__c == 'Web') dw.inboundTotal = ++dw.inboundTotal;
						
					//}
					//month
					if(svcMap.get(css.EntitlementId__r.Asset.Product2.SC_Service_Type__c) == 'Signature' && dw.extIds == String.valueOf(monMap.get(css.Case_CreatedDate__c.month()) +'-'+String.valueOf(css.Case_CreatedDate__c.year()))){
						dw.year = String.valueOf(css.Case_CreatedDate__c.year());
						dw.month = String.valueOf(css.Case_CreatedDate__c.month());
						dw.signature = ++dw.signature;
						dw.totalNewCase = ++dw.totalNewCase;

						if(css.status__c == 'Closed') dw.sbsClose = ++dw.sbsClose;
						else dw.pendingTotal = ++dw.pendingTotal;

						if(css.Origin__c == 'Phone') dw.voiceSignature = ++dw.voiceSignature;
						if(css.Origin__c == 'Email') dw.emailSignature = ++dw.emailSignature;
						if(css.Origin__c == 'Web') dw.portalSignature = ++ dw.portalSignature;
						if(css.Origin__c == 'Phone' || css.Origin__c == 'Email' || css.Origin__c == 'Web') dw.inboundTotal = ++dw.inboundTotal;
						
					}
					//month
					if(svcMap.get(css.EntitlementId__r.Asset.Product2.SC_Service_Type__c) == 'Premier' && dw.extIds == String.valueOf(monMap.get(css.Case_CreatedDate__c.month()) +'-'+String.valueOf(css.Case_CreatedDate__c.year()))){
						dw.year = String.valueOf(css.Case_CreatedDate__c.year());
						dw.month = String.valueOf(css.Case_CreatedDate__c.month());
						dw.signature = ++dw.premier;
						dw.totalNewCase = ++dw.totalNewCase;

						if(css.status__c == 'Closed') dw.sbsClose = ++dw.sbsClose;
						else dw.pendingTotal = ++dw.pendingTotal;

						if(css.Origin__c == 'Phone') dw.voicePremier = ++dw.voicePremier;
						if(css.Origin__c == 'Email') dw.emailPremier = ++dw.emailPremier;
						if(css.Origin__c == 'Web') dw.portalPremier = ++ dw.portalPremier;
						if(css.Origin__c == 'Phone' || css.Origin__c == 'Email' || css.Origin__c == 'Web') dw.inboundTotal = ++dw.inboundTotal;
						
					}
					//month
					if(svcMap.get(css.EntitlementId__r.Asset.Product2.SC_Service_Type__c) == 'Other' && dw.extIds == String.valueOf(monMap.get(css.Case_CreatedDate__c.month()) +'-'+String.valueOf(css.Case_CreatedDate__c.year()))){
						dw.year = String.valueOf(css.Case_CreatedDate__c.year());
						dw.month = String.valueOf(css.Case_CreatedDate__c.month());
						dw.signature = ++dw.other;
						dw.totalNewCase = ++dw.totalNewCase;

						if(css.status__c == 'Closed') dw.sbsClose = ++dw.sbsClose;
						else dw.pendingTotal = ++dw.pendingTotal;
						
						if(css.Origin__c == 'Phone') dw.voiceOther = ++dw.voiceOther;
						if(css.Origin__c == 'Email') dw.emailOther = ++dw.emailOther;
						if(css.Origin__c == 'Web') dw.portalOther = ++ dw.portalOther;
						if(css.Origin__c == 'Phone' || css.Origin__c == 'Email' || css.Origin__c == 'Web') dw.inboundTotal = ++dw.inboundTotal;
					}
					//month
					//if(svcMap.get(css.EntitlementId__r.Asset.Product2.SC_Service_Type__c) == 'KNOX' && dw.extIds == String.valueOf(monMap.get(css.Case_CreatedDate__c.month()) +'-'+String.valueOf(css.Case_CreatedDate__c.year()))){
					//	dw.year = String.valueOf(css.Case_CreatedDate__c.year());
					//	dw.month = String.valueOf(css.Case_CreatedDate__c.month());
					//	dw.signature = ++dw.knox;
					//	dw.totalNewCase = ++dw.totalNewCase;

					//	if(css.status__c == 'Closed') dw.knoxClose = ++dw.knoxClose;
					//	else dw.pendingTotal = ++dw.pendingTotal;
						
					//	if(css.Origin__c == 'Phone') dw.voiceKnox = ++dw.voiceKnox;
					//	if(css.Origin__c == 'Email') dw.emailKnox = ++dw.emailKnox;
					//	if(css.Origin__c == 'Web') dw.portalKnox = ++ dw.portalKnox;
					//	if(css.Origin__c == 'Phone' || css.Origin__c == 'Email' || css.Origin__c == 'Web') dw.inboundTotal = ++dw.inboundTotal;
					//}
					//week
					if(svcMap.get(css.EntitlementId__r.Asset.Product2.SC_Service_Type__c) == 'Signature' && dw.extIds == String.valueOf(isoYearNumber(css.Case_CreatedDate__c.date())) +  'Wk' + css.C_Report_Week__c){
						dw.year = String.valueOf(isoYearNumber(css.Case_CreatedDate__c.date()));
						dw.month = String.valueOf(css.Case_CreatedDate__c.month());
						dw.week = css.C_Report_Week__c;
						dw.signature = ++dw.signature;
						dw.totalNewCase = ++dw.totalNewCase;

						if(css.status__c == 'Closed') dw.sbsClose = ++dw.sbsClose;
						else dw.pendingTotal = ++dw.pendingTotal;

						if(css.Origin__c == 'Phone') dw.voiceSignature = ++dw.voiceSignature;
						if(css.Origin__c == 'Email') dw.emailSignature = ++dw.emailSignature;
						if(css.Origin__c == 'Web') dw.portalSignature = ++ dw.portalSignature;
						if(css.Origin__c == 'Phone' || css.Origin__c == 'Email' || css.Origin__c == 'Web') dw.inboundTotal = ++dw.inboundTotal;
						
					}
					//week
					if(svcMap.get(css.EntitlementId__r.Asset.Product2.SC_Service_Type__c) == 'Premier' && dw.extIds == String.valueOf(isoYearNumber(css.Case_CreatedDate__c.date())) + 'Wk' + css.C_Report_Week__c){
						dw.year = String.valueOf(isoYearNumber(css.Case_CreatedDate__c.date()));
						dw.month = String.valueOf(css.Case_CreatedDate__c.month());
						dw.week = css.C_Report_Week__c;
						dw.signature = ++dw.signature;
						dw.totalNewCase = ++dw.totalNewCase;

						if(css.status__c == 'Closed') dw.sbsClose = ++dw.sbsClose;
						else dw.pendingTotal = ++dw.pendingTotal;

						if(css.Origin__c == 'Phone') dw.voicePremier = ++dw.voicePremier;
						if(css.Origin__c == 'Email') dw.emailPremier = ++dw.emailPremier;
						if(css.Origin__c == 'Web') dw.portalPremier = ++ dw.portalPremier;
						if(css.Origin__c == 'Phone' || css.Origin__c == 'Email' || css.Origin__c == 'Web') dw.inboundTotal = ++dw.inboundTotal;
						
					}
					//week
					if(svcMap.get(css.EntitlementId__r.Asset.Product2.SC_Service_Type__c) == 'Other' && dw.extIds == String.valueOf(isoYearNumber(css.Case_CreatedDate__c.date())) + 'Wk' + css.C_Report_Week__c){
						dw.year = String.valueOf(isoYearNumber(css.Case_CreatedDate__c.date()));
						dw.month = String.valueOf(css.Case_CreatedDate__c.month());
						dw.week = css.C_Report_Week__c;
						dw.signature = ++dw.signature;
						dw.totalNewCase = ++dw.totalNewCase;

						if(css.status__c == 'Closed') dw.sbsClose = ++dw.sbsClose;
						else dw.pendingTotal = ++dw.pendingTotal;

						if(css.Origin__c == 'Phone') dw.voiceOther = ++dw.voiceOther;
						if(css.Origin__c == 'Email') dw.emailOther = ++dw.emailOther;
						if(css.Origin__c == 'Web') dw.portalOther = ++ dw.portalOther;
						if(css.Origin__c == 'Phone' || css.Origin__c == 'Email' || css.Origin__c == 'Web') dw.inboundTotal = ++dw.inboundTotal;
						
					}
					//week
					//if(svcMap.get(css.EntitlementId__r.Asset.Product2.SC_Service_Type__c) == 'KNOX' && dw.extIds == 'Wk' + css.C_Report_Week__c){
					//	dw.year = String.valueOf(css.Case_CreatedDate__c.year());
					//	dw.month = String.valueOf(css.Case_CreatedDate__c.month());
					//	dw.week = css.C_Report_Week__c;
					//	dw.signature = ++dw.signature;
					//	dw.totalNewCase = ++dw.totalNewCase;

					//	if(css.status__c == 'Closed') dw.knoxClose = ++dw.knoxClose;
					//	else dw.pendingTotal = ++dw.pendingTotal;
						
					//	if(css.Origin__c == 'Phone') dw.voiceKnox = ++dw.voiceKnox;
					//	if(css.Origin__c == 'Email') dw.emailKnox = ++dw.emailKnox;
					//	if(css.Origin__c == 'Web') dw.portalKnox = ++ dw.portalKnox;
					//	if(css.Origin__c == 'Phone' || css.Origin__c == 'Email' || css.Origin__c == 'Web') dw.inboundTotal = ++dw.inboundTotal;
					//}
				}
			}
		}
	}

	public class DataWrapper {
		public Integer				signature			{get;set;}
		public Integer				premier				{get;set;}
		public Integer				other				{get;set;}
		public Integer				knox   				{get;set;}
    	public Integer				totalNewCase   		{get;set;}
    	public Integer				sbsClose			{get;set;}
    	public Integer				knoxClose			{get;set;}
    	public Integer				pendingTotal		{get;set;}
    	public String				year				{get;set;}
    	public String				month				{get;set;}
    	public String				week				{get;set;}
    	public String				extIds				{get;set;}
    	public Integer				portalSignature		{get;set;}
    	public Integer				portalPremier		{get;set;}
    	public Integer				portalOther			{get;set;}
    	public Integer				portalKnox			{get;set;}
    	public Integer				emailSignature		{get;set;}
    	public Integer				emailPremier		{get;set;}
    	public Integer				emailOther			{get;set;}
    	public Integer				emailKnox			{get;set;}
    	public Integer				voiceSignature		{get;set;}
    	public Integer				voicePremier		{get;set;}
    	public Integer				voiceOther			{get;set;}
    	public Integer				voiceKnox			{get;set;}
    	public Integer				inboundTotal		{get;set;}

		public Integer				preWeekCount		{get;set;}

		public DataWrapper() {
			this.signature			= 0;
			this.premier			= 0;
			this.other				= 0;
			this.knox				= 0;
			this.totalNewCase		= 0;
			this.sbsClose			= 0;
			this.knoxClose			= 0;
			this.pendingTotal		= 0;
			this.portalSignature	= 0;
			this.portalPremier		= 0;
			this.portalOther		= 0;
			this.portalKnox			= 0;
			this.emailSignature		= 0;
			this.emailPremier		= 0;
			this.emailOther			= 0;
			this.emailKnox			= 0;
			this.voiceSignature		= 0;
			this.voicePremier		= 0;
			this.voiceOther			= 0;
			this.voiceKnox			= 0;
			this.inboundTotal		= 0;

			this.preWeekCount		= 0;
    	}
  	}
  	public class SearchObject {
	    public SearchObject__c	so          	{get; set;}

		public SearchObject() {
			this.so          	=   new SearchObject__c(); 
	    }
  	}


  	public static Integer isoWeekNumber(Date value) {

		Integer daysSince1900_01_07 = Date.newInstance(1900, 1, 7).daysBetween(value);
		Integer dayNumber = Math.mod(daysSince1900_01_07, 7) + 1;
		Date dateForYear = value.addDays(Math.mod(8 - dayNumber, 7) - 3);
		Integer year = dateForYear.year();
		Date year_01_01 = Date.newInstance(year, 1, 1);
		Integer week = (Integer)Math.floor((year_01_01.daysBetween(value) + Math.mod((Math.mod(Date.newInstance(1900, 1, 7).daysBetween(year_01_01), 7) + 1) + 1, 7) - 3) / 7 + 1);
		return week;
	}
	public static Integer isoYearNumber(Date value){
		Integer daysSince1900_01_07 = Date.newInstance(1900, 1, 7).daysBetween(value);
		Integer dayNumber = Math.mod(daysSince1900_01_07, 7) + 1;
		Date dateForYear = value.addDays(Math.mod(8 - dayNumber, 7) - 3);
		Integer year = dateForYear.year();
		return year;
	}
}