@isTest
private class CaseManagerReport_Test
{
    private static Product2 p;
    private static void setupData() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        p = new Product2();
        
        p.Name = 'Test01';
        //p.SC_Service_Type__c = 'Signature';
        p.SAP_Material_ID__c = '123456789';

        insert p;

        p = [SELECT Id, Name FROM Product2 LIMIT 1];

        Account acc = new Account();

        acc.Name = 'Test01';
        //acc.Service_ID_Numeric__c = 1;
        acc.Service_ID__c = '100011';

        insert acc;

        acc = [SELECT Id, Name FROM Account LIMIT 1];

        Asset ass = new Asset();

        ass.Name = 'Test Asset';
        ass.AccountId = acc.Id;
        ass.Product2Id = p.Id;

        insert ass;

        ass = [SELECT Id, Name FROM Asset LIMIT 1];

        Entitlement en = new Entitlement();

        en.Name = 'Test Entitlement';
        en.AccountId = acc.Id;
        en.AssetId = ass.Id;
        en.IS_Trial__c = true;

        insert en;

        en = [SELECT Id, Name FROM Entitlement LIMIT 1];

        List<ReportService__c> l = new List<ReportService__c>();

        ReportService__c a = new ReportService__c();
        a.Name = 'RS-00001';
        a.Report_Service_Type__c = 'Signature';
        a.Case_Service_Type__c = 'Signature';

        l.add(a);

        a = new ReportService__c();
        a.Name = 'RS-00002';
        a.Report_Service_Type__c = 'Premier';
        a.Case_Service_Type__c = 'Premier';

        l.add(a);

        a = new ReportService__c();
        a.Name = 'RS-00003';
        a.Report_Service_Type__c = 'Other';
        a.Case_Service_Type__c = 'Other';

        l.add(a);

        insert l;

        List<Case_SnapShot__c> l2 = new List<Case_SnapShot__c>();

        Case_SnapShot__c b = new Case_SnapShot__c();

        //b.Name = 'CS# 00000003';
        b.Case_CreatedDate__c = Datetime.newInstanceGmt(2017, 5, 5, 17, 30, 0);
        b.C_Report_Week__c = '18';
        b.Origin__c = 'Email';
        b.EntitlementId__c = en.Id;

        l2.add(b);

        b = new Case_SnapShot__c();

        //b.Name = 'CS# 00000004';
        b.Case_CreatedDate__c = Datetime.newInstanceGmt(2017, 4, 28, 19, 38, 0);
        b.C_Report_Week__c = '17';
        b.Origin__c = 'Email';
        b.EntitlementId__c = en.Id;

        l2.add(b);

        insert l2;
    }

    @isTest
    static void itShould()
    {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        setupData();

        CaseManagerReport a = new CaseManagerReport();
        a.init();

        a.searchObj.so.startDate__c = Date.newInstance(2017, 5, 11);

        //Product2 p = [SELECT Id, Name, SC_Service_Type__c FROM Product2 LIMIT 1];
        p.SC_Service_Type__c = 'Signature';

        update p;

        a.listCaseManagerReport();

        p.SC_Service_Type__c = 'Premier';

        update p;
        
        a.listCaseManagerReport();

        p.SC_Service_Type__c = 'Other';

        update p;
        
        a.listCaseManagerReport();

        /*
        bad value for restricted picklist field: KNOX: [SC_Service_Type__c]
        */
        //p.SC_Service_Type__c = 'KNOX';

        //update p;
        
        //a.listCaseManagerReport();

    }
}