global class CaseMilestoneTimeCalculator implements Support.MilestoneTriggerTimeCalculator {   
     global Integer calculateMilestoneTriggerTime(String caseId, String milestoneTypeId){
     
      //Find the when the case was created and milestone name 
        Case c = [SELECT CreatedDate FROM Case WHERE Id=:caseId];
        MilestoneType mt = [SELECT Name FROM MilestoneType WHERE Id=:milestoneTypeId];
        
      //Find the business Exchange Business Hours
        BusinessHours bh = [SELECT Id, Name FROM BusinessHours WHERE Name = 'Exchange Business Hours 24x5'];
        
      //Find whether the case has been created during the Exchange business hours
        Boolean isWithin= BusinessHours.isWithin(bh.id,c.CreatedDate);
        
      //Find the next start date of the business hours after the case has been created
        Datetime nextStart = BusinessHours.nextStartDate(bh.id, c.CreatedDate);
        
      //Calculate the difference between the CreatedDate and the milestone next start date then convert to minutes
        Long nextMilMin =  (nextStart.getTime()-c.CreatedDate.getTime())/1000/60;
        Integer nMM = nextMilMin.IntValue()+1; 
                
      //Convert CreatedDate from GMT to CST, retreive the converted hours and convert the hours into mins
      //Then calculate how many minutes passed before the case was created that day  
        Integer cST = (Integer.valueOf(c.CreatedDate.format('kk','CST').left(2)) * 60) + c.CreatedDate.minute();
               
      //Create a variable to store the mins between 12 am of CreatedDate until tomorrow 11pm CST
        Integer mDl1 = 2820;
        
      //Calculate how many mins are between the created date and 11pm tomorrow night
        Integer mST1 = mDl1 - cST; 
        
      //Create a variable to store the mins between 12 am of CreatedDate until day after tomorrow 11pm CST
        Integer mDl2 = 4260;
        
      //Calculate how many mins are between the created date and 11pm the next day
        Integer mST2 = mDl2 - cST; 
       
      //If the case is created during business hours BEFORE 2pm CST set Milestone target date to 11 pm tomorrow  
        if (cST < 840 && mt.Name != null && mt.Name.equals('Exchange SLA')){
              return mST1 - nMM;
        }else{
              return mST2 - nMM;            
            }
        }
    }