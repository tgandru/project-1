@isTest 
private class CaseMilestoneTimeCalculatorTest {
    static testMethod void testCaseMilestoneTimeCalculator() {        
      
        // Select an existing milestone type to test with
        MilestoneType[] mtLst = [SELECT Id, Name FROM MilestoneType LIMIT 1];      
        if(mtLst.size() == 0) { return; }
        MilestoneType mt = mtLst[0];
        
        // Create case data.
        Case c = new Case(priority = 'High');
        c.createdDate = DateTime.newInstance(1998, 11, 21, 0, 3, 3);
        insert c;
        
        Integer cST = (Integer.valueOf(c.CreatedDate.format('kk','CST').left(2)) * 60) + c.CreatedDate.minute();
        
        CaseMilestoneTimeCalculator calculator = new CaseMilestoneTimeCalculator();
        Integer actualTriggerTime = calculator.calculateMilestoneTriggerTime(c.Id, mt.Id);
        
        if(cST < 840 && mt.Name != null && mt.Name.equals('Exchange SLA')) {
            System.assertEquals(4260,actualTriggerTime);
        }
        else {
            //System.assertEquals( 2876, actualTriggerTime);
            System.assertEquals( 0, actualTriggerTime);
        }
        
        c.priority = 'Low';
        update c;
        actualTriggerTime = calculator.calculateMilestoneTriggerTime(c.Id, mt.Id);
        System.assertEquals(0,actualTriggerTime);
    }
}