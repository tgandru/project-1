/* Author: Jeongho Lee
*  Description: Weekly Case SnapShot Schedular
*/
public class CaseSnapShotSchedular implements Schedulable {
	public void execute(SchedulableContext sc) {
		BatchForCaseSnapShot b = new BatchForCaseSnapShot();
		database.executebatch(b);
	}
}