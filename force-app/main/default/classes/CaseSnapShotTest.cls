/* Author: Jeongho Lee
*  Description: Weekly Case SnapShotTest
*/
@isTest
private class CaseSnapShotTest {
    @testSetup
    static void setData(){
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        //RecordType
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }
        
        //Product 
        List<Product2> prodList = new List<Product2>();
        for(Integer i = 0; i < 3; i ++){
            Product2 prod                           = new Product2();
            prod.RecordTypeId                       = rtMap.get('Product2'+'B2B');
            prod.ProductCode                        = 'testProduct' + i;
            prod.Name                               = 'testProduct' + i;
            if(i == 0) prod.SC_Service_Type__c      = 'Elite';              //Signature
            if(i == 1) prod.SC_Service_Type__c      = 'Named_caller_exp';   //Other
            if(i == 2) prod.SC_Service_Type__c      = 'Premier';            //Premier
            prod.SAP_Material_ID__c                 = 'test' + i;

            prodList.add(prod);
        }
        insert prodList;
        
        //Address insert
        List<Zipcode_Lookup__c> zipList = new List<Zipcode_Lookup__c>(); 
        Zipcode_Lookup__c  zip1 = new Zipcode_Lookup__c(Name='07660',City_Name__c='RIDGEFIELD PARK', Country_Code__c='US', State_Code__c='NJ',State_Name__c='New Jersey');
        zipList.add(zip1);
        Zipcode_Lookup__c  zip2 = new Zipcode_Lookup__c(Name='07650',City_Name__c='PALISADES PARK', Country_Code__c='US', State_Code__c='NJ',State_Name__c='New Jersey');
        zipList.add(zip2);
        insert zipList;
        
        //Account
        List<Account> accList = new List<Account>();
        for(Integer i = 0; i< 3; i++){
            Account acc             = new Account();
            acc.Name                = 'testAcc' + i;
            acc.RecordTypeId        = rtMap.get('Account'+'End_User');
            acc.BillingCity         = 'PALISADES PARK';
            acc.BillingCountry      = 'US';
            acc.BillingPostalCode   = '07650';
            acc.BillingState        = 'NJ';
            acc.BillingStreet       = '411 E Brinkerhoff Ave, Palisades Park';

            accList.add(acc);
        }   
        insert accList;

        //Contact
        List<Contact> conList = new List<Contact>();
        for(Integer i = 0; i<3; i++){
            Contact con             = new Contact();
            con.RecordTypeId        = rtMap.get('Contact'+'SEA_Service_Contact');
            con.FirstName           = 'FirstName' + i;
            con.LastName            = 'conTest' + i;
            con.AccountId           = accList[i].Id;
            con.Email               = i+'test@aa.com';
            con.Named_Caller__c     = true;
            con.MailingCity         = accList[i].BillingCity;
            con.MailingCountry      = accList[i].BillingCountry;
            con.MailingPostalCode   = accList[i].BillingPostalCode;
            con.MailingState        = accList[i].BillingState;
            con.MailingStreet       = accList[i].BillingStreet;

            conList.add(con);       

        }
        insert conList;
        //Asset
        List<Asset> assList = new List<Asset>();
        for(Integer i = 0; i<3; i++){
            Asset ass               = new Asset();
            ass.Name                = 'testAsset' + i;
            ass.AccountId           = accList[i].Id;
            ass.Product2Id          = prodList[i].Id;
            ass.Quantity            = 1;

            assList.add(ass);

        }
        insert assList;

        //Entitlement
        List<Entitlement> entList = new List<Entitlement>();
        for(Integer i = 0; i<3; i++){
            Entitlement ent     = new Entitlement();
            ent.Name            = 'testEntName' + i;
            ent.AssetId         = assList[i].Id;
            ent.AccountId       = accList[i].Id;
            ent.StartDate       = Date.today();

            entList.add(ent);
        }
        insert entList;
        //Case
        List<Case> caseList = new List<Case>();
        for(Integer i = 0; i<3; i++){
            Case ca = new Case();
            ca.RecordTypeId     = rtMap.get('Case'+'Technical_Support');
            ca.AccountId        = accList[i].Id;
            ca.AssetId          = assList[i].Id;
            ca.ContactId        = conList[i].Id;
            ca.EntitlementId    = entList[i].Id;
            ca.Shipping_Address_1__c='100 challenger rd';
            ca.Shipping_City__c='RIDGEFIELD PARK';
            ca.Shipping_Country__c='US';
            ca.Shipping_State__c='NJ';
            ca.Shipping_Zip_Code__c= '07660';
            caseList.add(ca);
        }
        insert caseList;
    }  
    static testMethod void StartCaseSnapShotBatch() {
        // TO DO: implement unit test
        String  CRON_EXP = '0 0 0 4 10 ? '  + (System.now().year() + 1) ; // Next Year
        Test.startTest();
        System.schedule('CaseSnapShot Weekly Report',CRON_EXP,new CaseSnapShotSchedular());
        Test.stopTest();
    }
}