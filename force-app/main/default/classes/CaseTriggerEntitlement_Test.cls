/**
 * Created by ryan on 7/31/2017.
 */

@IsTest
private class CaseTriggerEntitlement_Test {

    static Device__c dev1;
    static Device__c dev2;
    static Case cse1;
    static Case cse2;
    static ContentDocumentLink contentDocumentLink;
    static ContentVersion contentVersion;

    @testSetup static void testData(){
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        //get record type ids for account
        Id deviceRT = Schema.SObjectType.Device__c.getRecordTypeInfosByName().get('Edit Device').getRecordTypeId();
        Id acctRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Direct').getRecordTypeId();
        Id caseRT = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Exchange').getRecordTypeId();
        Id prodRT = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('B2B').getRecordTypeId();
        BusinessHours bh12 = [SELECT Id FROM BusinessHours WHERE name = 'B2B Service Hours 12x5'];

        Account acc = new Account(Name = 'Test Account', RecordTypeId = acctRT, Type = 'Distributor');
        insert acc;
        Contact con = new Contact(LastName = 'Test Contact', 
                        Email = 'test@email.com', Account = acc, MailingCountry = 'US',
                        Firstname = 'Test Contact',
                        MailingState = 'NJ');
        insert con;
        RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];
        Product2 prod1 = new Product2(name ='MI-test', SAP_Material_ID__c = 'MI-test', sc_service_type__c = 'Incident', sc_service_type_ranking__c = '6', RecordTypeId = prodRT);
        insert prod1;
        Asset ast = new Asset(name ='Assert-MI-Test', AccountId = acc.id, Product2Id = prod1.id);
        insert ast;
        Slaprocess sla = [select id from SlaProcess where name like 'Enhanc%' and isActive = true limit 1 ];
        Entitlement ent = new Entitlement(Name ='Test Entitlement', AccountId=acc.id, assetid = ast.id, BusinessHoursId = bh12.id, StartDate = System.today().addDays(-14), EndDate = System.today().addDays(14), slaprocessid = sla.id, Units_Allowed__c = 10, Units_Exchanged__c = 0);
        insert ent;
        dev1 = new Device__c(RecordTypeId= deviceRT,Account__c = acc.Id, IMEI__c = '7259863232145', Status__c = 'Verified', Entitlement__c = ent.Id);
        insert dev1;
        dev2 = new Device__c(RecordTypeId= deviceRT,Account__c = acc.Id, IMEI__c = '7259863232146', Status__c = 'Verified', Entitlement__c = ent.Id);
        insert dev2;
        cse1 = new Case(AccountId = acc.Id, ContactId = con.Id, Subject = 'Test Case', Origin = 'Web', Severity__c = '4-Low', EntitlementId = ent.Id, Device__c = dev1.Id, Service_Order_Number__c = '4100167438', Status = 'New');
        insert cse1;
    }

    static testMethod void testUpdateUnderCapLimit(){
        Account acc = [SELECT Id, Name FROM Account WHERE Name = 'Test Account' LIMIT 1];
        Contact con = [SELECT Id, LastName FROM Contact WHERE LastName = 'Test Contact' LIMIT 1];
        Entitlement ent = [SELECT Id, Name, Units_Allowed__c, Units_Exchanged__c, Units_Available__c FROM Entitlement WHERE Name = 'Test Entitlement' LIMIT 1];
        dev2 = [SELECT Id, Status__c FROM Device__c WHERE IMEI__c = '7259863232146' LIMIT 1];

        //assert that there are 9 available cases that can be counted against the cap
        //after we upsert 2 cases there will be 8 available spots
        System.assertEquals(ent.Units_Allowed__c, 10);
        System.assertEquals(ent.Units_Exchanged__c, 1);
        System.assertEquals(ent.Units_Available__c, 9);

        //get the case that we want to update
        cse1 = [SELECT Id, Subject FROM Case WHERE Subject = 'Test Case' LIMIT 1];
        cse1.Subject = 'Test Case 1';
        //create a new case that we want to insert
        cse2 = new Case(AccountId = acc.Id, ContactId = con.Id, Subject = 'Test Case 2', Origin = 'Web', Severity__c = '4-Low', EntitlementId = ent.Id, Device__c = dev2.Id, Service_Order_Number__c = '4100167439', Status = 'New');

        //add them to the list
        List<Case> casesToUpsert = new List<Case>();
        casesToUpsert.add(cse1);
        casesToUpsert.add(cse2);

        Test.startTest();
            upsert casesToUpsert;
        Test.stopTest();

        //requery the entitlement
        ent = [SELECT Id, Name, Units_Allowed__c, Units_Exchanged__c, Units_Available__c FROM Entitlement WHERE Name = 'Test Entitlement' LIMIT 1];
        //assert that there are now only 8 available cases that can be counted against the cap
        System.assertEquals(ent.Units_Allowed__c, 10);
        System.assertEquals(ent.Units_Exchanged__c, 2);
        System.assertEquals(ent.Units_Available__c, 8);

    }

    static testMethod void testUpdateOverCapLimit(){
        Account acc = [SELECT Id, Name FROM Account WHERE Name = 'Test Account' LIMIT 1];
        Contact con = [SELECT Id, LastName FROM Contact WHERE LastName = 'Test Contact' LIMIT 1];
        Entitlement ent = [SELECT Id, Name, Units_Allowed__c, Units_Exchanged__c, Units_Available__c FROM Entitlement WHERE Name = 'Test Entitlement' LIMIT 1];
        dev2 = [SELECT Id, Status__c FROM Device__c WHERE IMEI__c = '7259863232146' LIMIT 1];

        //change the entitlment cap limit to 1
        ent.Units_Allowed__c = 1;
        try{
            update ent;
        }catch(DmlException e){
            System.debug(e.getMessage());
        }

        ent = [SELECT Id, Name, Units_Allowed__c, Units_Exchanged__c, Units_Available__c FROM Entitlement WHERE Name = 'Test Entitlement' LIMIT 1];
        //assert that there are 0 available cases that can be counted against the cap
        //after we upsert 2 cases there will be still be 0 available spots
        System.assertEquals(ent.Units_Allowed__c, 1);
        System.assertEquals(ent.Units_Exchanged__c, 1);
        System.assertEquals(ent.Units_Available__c, 0);

        //get the case that we want to update
        cse1 = [SELECT Id, Subject FROM Case WHERE Subject = 'Test Case' LIMIT 1];
        cse1.Subject = 'Test Case 1';
        //create a new case that we want to insert
        cse2 = new Case(AccountId = acc.Id, ContactId = con.Id, Subject = 'Test Case 2', Origin = 'Web', Severity__c = '4-Low', EntitlementId = ent.Id, Device__c = dev2.Id, Service_Order_Number__c = '4100167439', Status = 'New');

        //add them to the list
        List<Case> casesToUpsert = new List<Case>();
        casesToUpsert.add(cse1);
        casesToUpsert.add(cse2);

        Test.startTest();
            try{
                upsert casesToUpsert;
            }catch(DmlException e){
                System.debug(e.getMessage());
            }
        Test.stopTest();

        //assert that the second case wasn't created
        List<Case> cse2List = [SELECT Id, Subject FROM Case WHERE Subject = 'Test Case 2' LIMIT 1];
        System.assertEquals(cse2List.size(), 0);
        //requery the entitlement
        ent = [SELECT Id, Name, Units_Allowed__c, Units_Exchanged__c, Units_Available__c FROM Entitlement WHERE Name = 'Test Entitlement' LIMIT 1];
        //assert that there are still 0 available cases that can be counted against the cap, and only 1 case
        System.assertEquals(ent.Units_Allowed__c, 1);
        System.assertEquals(ent.Units_Exchanged__c, 1);
        System.assertEquals(ent.Units_Available__c, 0);

    }

    static testMethod void checkSumValidStatus(){
        
        Id repairId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Repair').getRecordTypeId();
        Contact con = [SELECT Id, LastName, Named_Caller__c FROM Contact WHERE LastName = 'Test Contact' LIMIT 1];
        con.Named_Caller__c = true;
        cse1 = [SELECT Id, Subject, Origin, RecordTypeId, Case_Device_Status__c FROM Case WHERE Subject = 'Test Case' LIMIT 1];

        cse1.RecordTypeId = repairId;
        cse1.SerialNumber__c = 'ABCDE123456'; //this is a long serial number, which will cause the Case Device Status to be 'Valid'
        cse1.Service_Order_Number__c=null;

        Test.startTest();
            update con;
            update cse1;
        Test.stopTest();

        cse1 = [SELECT Id, Subject, RecordTypeId, Case_Device_Status__c FROM Case WHERE Subject = 'Test Case' LIMIT 1];
        System.assertEquals(cse1.Case_Device_Status__c, 'Valid');
    }

    static testMethod void checkSumInvalidStatus(){
        Id repairId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Repair').getRecordTypeId();
        Contact con = [SELECT Id, LastName, Named_Caller__c FROM Contact WHERE LastName = 'Test Contact' LIMIT 1];
        cse1 = [SELECT Id, Subject, Origin, RecordTypeId, Case_Device_Status__c FROM Case WHERE Subject = 'Test Case' LIMIT 1];

        con.Named_Caller__c = true;

        cse1.RecordTypeId = repairId;
        cse1.SerialNumber__c = 'ABCDE'; //this is a short serial number, which will cause the Case Device Status to be 'Invalid'
        cse1.Service_Order_Number__c=null;

        Test.startTest();
            update con;
            update cse1;
        Test.stopTest();

        cse1 = [SELECT Id, Subject, RecordTypeId, Case_Device_Status__c FROM Case WHERE Subject = 'Test Case' LIMIT 1];
        System.assertEquals(cse1.Case_Device_Status__c, 'Invalid');
    }
}