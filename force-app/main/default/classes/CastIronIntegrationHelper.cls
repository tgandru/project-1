public with sharing class CastIronIntegrationHelper {

public static final integer recordsPerBatch = 80;
public static final Map<string,Integration_EndPoints__c> integrationCodes = Integration_EndPoints__c.getAll(); 

public static List<String> missingFields=new List<String>();

@future(callout=true)
public static void sendMQrealTime(String quoteId){
    if([select id from message_queue__c where Integration_Flow_Type__c = 'submit quote for approval' and Identification_Text__c = :quoteId and Status__c='not started'].isEmpty()){
        message_queue__c mq=new message_queue__c(Identification_Text__c=quoteId, Integration_Flow_Type__c='submit quote for approval',
                                                        retry_counter__c=0,Status__c='not started', Object_Name__c='Quote');
        mq.IFID__c=(!Test.isRunningTest() && Integration_EndPoints__c.getInstance('Flow-008').layout_id__c!=null) ? Integration_EndPoints__c.getInstance('Flow-008').layout_id__c : '';
        receiveQuoteLineItemsForApproval(mq);
    }
}

@future(callout=true)
public static void sendMQrealTime008to012(String quoteId){
    message_queue__c mq=new message_queue__c(Identification_Text__c=quoteId, Integration_Flow_Type__c='008 to 012',
                                                        retry_counter__c=0,Status__c='not started', Object_Name__c='Quote');
    mq.IFID__c=(!Test.isRunningTest() && Integration_EndPoints__c.getInstance('Flow-008').layout_id__c!=null) ? Integration_EndPoints__c.getInstance('Flow-008').layout_id__c : '';
    receiveQuoteLineItemsForApproval(mq);
}

//@future(callout=true)
public static void receiveQuoteLineItemsForApproval(message_queue__c omq){

    List<OpportunityLineItem> oliToUpdate=new List<OpportunityLineItem>();
    Set<PricebookEntry> pbeToUpdate=new Set<PricebookEntry>();
    List<QuoteLineItem> qliToUpdate=new List<QuoteLineItem>();

    Long sendTime=0;
    Long receiveTime=0;
    Long updateTime=0;
    DateTime startTime=DateTime.now();

    integer errCount = 0;
    String soldToShipTo='';
    //String distribChannelId='';
    //String salesOrgId=' ';
    //String salesDivId=''; 

    //get all fields necessary to complete transfer, all fields from pricebook entry that aren't there. 
    List<QuoteLineItem> qliRecords=[select Id, Product2Id, PricebookEntryId, PricebookEntry.salesOrg__c, PricebookEntry.Currency_Key__c,
                                            PricebookEntry.Sales_Document_Type__c, PricebookEntry.ProductCode, PricebookEntry.msguid__c,
                                            PricebookEntry.ifid__c, PricebookEntry.ifdate__c, PricebookEntry.UnitPrice, 
                                            PricebookEntry.SAP_Material_Id__c,  PricebookEntry.BY_Pass_Price_Integration__c,Product2.ProductCode, OLIID__c, Product2.Name,
                                            QuoteId
                                            from QuoteLineItem where QuoteId =:omq.Identification_Text__c]; //PricebookEntry.Ship_To_Party PricebookEntry.Sold_To_Party, 

    Quote quoteRecord=[select Id, Name,
                        Approval_Requestor_Id__c, OpportunityId, Pricebook2Id, Integration_Status__c, Valid_From_Date__c, 
                        Pricebook2.Order_Reason__c, Pricebook2.Distribution_Channel__c, Pricebook2.Sales_Document_Type__c,
                        Pricebook2.Sales_Organization__c, Pricebook2.Sales_Division_Code__c, Pricebook2.Plant__c,Division__c
                        from Quote where Id=:omq.Identification_Text__c limit 1];//Total_Lines_For_Update__c, Successful_Update_Number__c, Failed_Update_Number__c, 

    if(quoteRecord==null){
        messageQueueHelper cont=new messageQueueHelper();
        cont.failed(omq, null,null, 'no quote found',startTime);
        return;
    }

    Set<Id> qliRecordIds=new Set<Id>();
    Set<Id> qliOLIIds=new Set<Id>();
    for(QuoteLineItem qli:qliRecords){
        qliRecordIds.add(qli.Id);
        qliOLIIds.add(qli.OLIID__c);
    } 

    if(qliRecordIds.size()!=qliOLIIds.size()){
        messageQueueHelper cont=new messageQueueHelper();
        cont.failed(omq, null,quoteRecord, 'connections from quote line item to opportunity line item not found for all quote line items - lineItems='+qliRecordIds+', OLIIds='+qliOLIIds,startTime);
        return;
    }

    //look up account filed SAP_Company_Code__c via opportunity line item and put it in as ship to sold to fields
    List<OpportunityLineItem> oliRecords=[select Id, Unit_Cost__c, OpportunityId, Opportunity.AccountId, SAP_Material_Id__c,
                                            PriceBookEntry.Product2.SAP_Material_ID__c
                                            from OpportunityLineItem 
                                            where Id in :qliOLIIds];

    if(oliRecords.isEmpty()){
        messageQueueHelper cont=new messageQueueHelper();
        cont.failed(omq, null, quoteRecord, 'no opportunity line item found',startTime);
        //quoteRecord.addError('There are no related opportunity line items on this quote. Please try again.');
        return;
    }

    Map<QuoteLineItem,OpportunityLineItem> qliToOliMap=new Map<QuoteLineItem,OpportunityLineItem>();
    for(QuoteLineItem qli: qliRecords){
        for(OpportunityLineItem oli:oliRecords){
            if(qli.OLIID__c==oli.Id){
                qliToOliMap.put(qli,oli);
            }
        }
    }

    //@TODO see if oppty has distributor, if not then do this. 

   List<Partner__c> partnerRecord=[select Id, Partner__c, Partner__r.SAP_Company_Code__c, Partner__r.distribution_channel_id__c,
                                        Partner__r.sales_organization__c, Partner__r.sales_division_id__c
                                        from Partner__c 
                                        where Role__c='Distributor' and Is_Primary__c=true and Opportunity__c=:quoteRecord.OpportunityId
                                        limit 1]; 
    if(!partnerRecord.isEmpty()){
        soldToShipTo=partnerRecord[0].Partner__r.SAP_Company_Code__c;
        //distribChannelId=partnerRecord[0].Partner__r.distribution_channel_id__c;
        //salesOrgId=partnerRecord[0].Partner__r.sales_organization__c;
        //salesDivId=partnerRecord[0].Partner__r.sales_division_id__c;
    }else{
         DirectPriceBookAssign__c dpbaObj=new DirectPriceBookAssign__c();
         System.debug('lclc 1');
        if(![select Id, Price_Book__c, Direct_Account__c 
                                            from DirectPriceBookAssign__c 
                                            where Price_Book__c=:quoteRecord.Pricebook2Id limit 1].isEmpty()){
            System.debug('lclc 2');
            dpbaObj=[select Id, Price_Book__c, Direct_Account__c 
                                            from DirectPriceBookAssign__c 
                                            where Price_Book__c=:quoteRecord.Pricebook2Id limit 1];
                                            SYSTEM.DEBUG('LCLC 3 '+dpbaObj);
        }else{
        

        //if(dpbaObj==null){
            messageQueueHelper cont=new messageQueueHelper();
            cont.failed(omq, null,quoteRecord, 'no Direct Price Book Assign or Partner found',null);
            //quoteRecord.addError('There is no primary distributor partner or direct price book assign records related to this quote. Please check and try again.');
            return;
        }

        Account acctRecord=[select Id, SAP_Company_Code__c, sales_division_id__c, distribution_channel_id__c,
                            sales_organization__c
                            from Account where Id=:dpbaObj.Direct_Account__c limit 1];//Owner.Division__c Id=:oliRecord.Opportunity.AccountId; Price_Book__c=:quoteRecord.Pricebook2Id 
        System.debug('lclc 4 acct rec '+acctRecord );
        if(acctRecord==null){
            messageQueueHelper cont=new messageQueueHelper();
            cont.failed(omq, null,quoteRecord, 'no account found',null);
            //quoteRecord.addError('There is no primary distributor partner or direct price book assign records-Accounts related to this quote. Please check and try again.');
            return;
        }      
        soldToShipTo=acctRecord.SAP_Company_Code__c;
        //soldToShipTo=acctRecord.distribution_channel_id__c;
        //salesOrgId=acctRecord.sales_organization__c;
        //salesDivId=acctRecord.sales_division_id__c;
    }
    System.debug('lclc is there soldToShipTo '+soldToShipTo);
 
    //call out to request stub like below
    /*EodbQueueHelper.eodbInputs Inputs = (EodbQueueHelper.eodbInputs) JSON.deserialize(omq.OutgoingMessage__c,EodbQueueHelper.eodbInputs.class);
    string endpoint = Inputs.urlEndpoint;
    */
    //string endpoint='http://www.mocky.io/v2/56f044b9100000f9008ef210';  //this doesnt work.
    //string endpoint='http://www.mocky.io/v2/5716840f110000e32187daac';//@TODO use custom setting use get
    string endpoint=Integration_EndPoints__c.getInstance('Flow-008').endPointURL__c;
    
    string response = '';

    //create body
    cihStub.UpdateInvoicePriceRequest request=new cihStub.UpdateInvoicePriceRequest();
    cihStub.InvoiceBodyRequest outerRecStub=new cihStub.InvoiceBodyRequest();

    List<cihStub.CreditMemoLineItemsRequest> creditMemoLines=new List<cihStub.CreditMemoLineItemsRequest>();

    //determine if there are missing values
    if( soldToShipTo =='' || quoteRecord.Valid_From_Date__c==null ){
        quoteRecord.addError('There are missing fields. Assure  you have a valid from date on the quote and check for a Partner or direct price book assign record related to the quote.');
        return;
    }

    Map<String, QuoteLineItem> itemNumberQLIMap=new Map<String, QuoteLineItem>();
    Integer count=1;

    for(QuoteLineItem qli:qliRecords){

        String c=string.valueOf(count).leftPad(6).replace(' ','0');
        itemNumberQLIMap.put(c,qli);

        cihStub.CreditMemoLineItemsRequest innerRecStub=new cihStub.CreditMemoLineItemsRequest();

        innerRecStub.itemNumberSdDoc=c;
        OpportunityLineItem oli=qliToOliMap.get(qli);
        //innerRecStub.materialCode=oli.SAP_Material_Id__c;
        innerRecStub.materialCode =oli.PriceBookEntry.Product2.SAP_Material_ID__c;
        innerRecStub.quantity=string.valueOf(1);
        innerRecStub.werks=quoteRecord.Pricebook2.Plant__c;

        creditMemoLines.add(innerRecStub);
        count++;

    }

    string layoutId = Test.isRunningTest() ? '' : Integration_EndPoints__c.getInstance('Flow-008').layout_id__c;
    cihStub.msgHeadersRequest headers = new cihStub.msgHeadersRequest(layoutId);
    request.inputHeaders = headers;

    outerRecStub.salesOrg=quoteRecord.Pricebook2.Sales_Organization__c;
    outerRecStub.distributionChannel=quoteRecord.Pricebook2.Distribution_Channel__c;
    outerRecStub.division=quoteRecord.Pricebook2.Sales_Division_Code__c;
    outerRecStub.soldToParty=soldToShipTo;
    outerRecStub.shipToParty=soldToShipTo;
    outerRecStub.salesDocType=quoteRecord.Pricebook2.Sales_Document_Type__c;
    outerRecStub.orderReason=quoteRecord.Pricebook2.Order_Reason__c;
    outerRecStub.currencyKey='USD';
    Datetime dt=datetime.newInstance(quoteRecord.Valid_From_Date__c.year(), quoteRecord.Valid_From_Date__c.month(),quoteRecord.Valid_From_Date__c.day());
    outerRecStub.validFromDate=dt.formatGMT('yyyy-MM-dd');
    outerRecStub.lines=creditMemoLines;

    request.body=outerRecStub;
    System.debug('lclc request '+request);

    try{
        sendTime=DateTime.now().getTime();
        if (Test.isRunningTest()) {  
                response='{"inputHeaders":{"MSGGUID":"a6bba321-2183-5fb4-ff75-4d024e39a98e","IFID":"TBD","IFDate":"160317","MSGSTATUS":"S","ERRORTEXT":null,"ERRORCODE":null},"body":{"lines":[{"unitCost":"20.1","materialCode":"CLX-4195FN/XAA","quantity":"1","invoicePrice":"20.5","currencyKey":"1","itemNumberSdDoc":"000001"},{"unitCost":"330.1","materialCode":"CLX-4195FN/XAB","quantity":"1","invoicePrice":"30.5","currencyKey":"1","itemNumberSdDoc":"000002"}]}}';
            }else{
               response=webcallout(JSON.serialize(request),endpoint);
            }
        
        
        receiveTime=DateTime.now().getTime();
        System.debug('lclc response '+response);
    }catch(Exception ex){
        messageQueueHelper cont=new messageQueueHelper();
        cont.generalFail(omq, ex, quoteRecord, null,startTime);
        System.debug('LCLC 1 ERROR IN RESPONSE ---- '+ex);
        return;
    }

    if(response == null){
        messageQueueHelper cont=new messageQueueHelper();
        cont.success(omq, quoteRecord, 'nothing came thru',(receiveTime - sendTime),null,startTime);
        return;
    }
    
    //parse response
    cihStub.UpdateInvoicePriceResponse stubResponse = new cihStub.UpdateInvoicePriceResponse();    //lclc delete later and change responsefake to response:
    //String responsefake='http://www.mocky.io/v2/56f0667410000050018ef23b';
    String responsefake='{"inputHeaders":{"MSGGUID":"a6bba321-2183-5fb4-ff75-4d024e39a98e","IFID":"TBD","IFDate":"160317","MSGSTATUS":"S","ERRORTEXT":null,"ERRORCODE":null},"body":{"lines":[{"unitCost":"20.1","materialCode":"CLX-4195FN/XAA","quantity":"1","invoicePrice":"20.5","currencyKey":"1","itemNumberSdDoc":"1"},{"unitCost":"330.1","materialCode":"CLX-4195FN/XAB","quantity":"1","invoicePrice":"30.5","currencyKey":"1","itemNumberSdDoc":"2"}]}}';
    try{
        //stubResponse = (cihStub.UpdateInvoicePriceResponse) json.deserialize(responsefake, cihStub.UpdateInvoicePriceResponse.class);
        stubResponse=(cihStub.UpdateInvoicePriceResponse) json.deserialize(response, cihStub.UpdateInvoicePriceResponse.class);
        System.debug('lclc stubResponse '+stubResponse);
    }
    catch(exception ex){
        System.debug('2 ERROR IN RESPONSESTUB ---- '+ex);
        messageQueueHelper cont=new messageQueueHelper();
        cont.generalFail(omq, ex, quoteRecord, null,startTime);
        return;         
    }
    System.debug('lclc errorcode '+stubResponse.inputHeaders.ERRORCODE);
    System.debug('lclc errortext '+stubResponse.inputHeaders.ERRORTEXT);
    if(  ( (stubResponse.inputHeaders.ERRORCODE!=null && stubResponse.inputHeaders.ERRORCODE!='null') && stubResponse.inputHeaders.ERRORCODE.toUpperCase().contains('E')) 
        || ( (stubResponse.inputHeaders.ERRORTEXT!=null && stubResponse.inputHeaders.ERRORTEXT!='null') && !(stubResponse.inputHeaders.ERRORTEXT.toLowerCase()).contains('success') )) {
        messageQueueHelper cont=new messageQueueHelper();
        cont.generalFail(omq, null, quoteRecord, stubResponse.inputHeaders.ERRORCODE+' '+stubResponse.inputHeaders.ERRORTEXT,startTime);
        return;
    }
    
    //prepare for upsert
    if(stubResponse.body!=null){
        try {
          for(cihStub.CreditMemoLineItemsResponse res: stubResponse.body.lines){
              QuoteLineItem qli=itemNumberQLIMap.get(res.itemNumberSdDoc);
              System.debug('lclc itemNumberQLIMap 2 '+res.itemNumberSdDoc+' '+qli);
              if(res.unitCost!=null){
                  OpportunityLineItem oli=qliToOliMap.get(qli);
                  oli.Unit_Cost__c=decimal.valueof(res.unitCost);
                  oli.Item_Number__c=res.itemNumberSdDoc;
                  System.debug('lclc oli to update '+oli);
                  oliToUpdate.add(oli);
              }else{
                  OpportunityLineItem oli=qliToOliMap.get(qli);
                  oli.Unit_Cost__c=0;
                  oli.Item_Number__c=res.itemNumberSdDoc;
                  System.debug('lclc oli to update '+oli);
                  oliToUpdate.add(oli);
              }
              if(res.invoicePrice!=null){
                  
                  PriceBookEntry pbe=new PriceBookEntry(Id=qli.PricebookEntryId);
                  if(qli.PricebookEntry.BY_Pass_Price_Integration__c==false){
                       pbe.UnitPrice=decimal.valueof(res.invoicePrice);
                  }
                 
                  System.debug('lclc pbe to update '+pbe);
                  pbeToUpdate.add(pbe);
                  
              }else{
                  PriceBookEntry pbe=new PriceBookEntry(Id=qli.PricebookEntryId);
                    if(qli.PricebookEntry.BY_Pass_Price_Integration__c==false){
                      pbe.UnitPrice=0;
                    }
                  System.debug('lclc pbe to update '+pbe);
                  pbeToUpdate.add(pbe);
              }
              if(res.itemNumberSdDoc!=null){
                  qli.Item_Number__c=res.itemNumberSdDoc;
                  qliToUpdate.add(qli);
              }
          }
          System.debug('lclc pbeToUpdate '+pbeToUpdate.size()+ ' oliToUpdate '+oliToUpdate.size()+' qliToUpdate '+qliToUpdate.size());
        } catch(Exception ex) {
          // Exception handling for invalid response by hong.jeon (12/9/2016)
          String errmsg = 'ERROR='+ex.getmessage() + ' - '+ex.getStackTraceString();
            messageQueueHelper cont=new messageQueueHelper();
            cont.failed(omq, ex, quoteRecord, errmsg,null);
            
            //quoteRecord.addError('Fail to handle response data. - '+ex.getmessage()+'. Please contact System Administrator.');
          return;
        }
        //do the upsert
        try{
            String tempstring='';

            if(!pbeToUpdate.isEmpty()){
                 List<PriceBookEntry> pbeToUpdateLst=new List<PriceBookEntry>();
                pbeToUpdateLst.addAll(pbeToUpdate);
                List<Database.SaveResult> uResultsPBE=Database.Update(pbeToUpdateLst,false);

                for(Database.SaveResult sr:uResultsPBE){
                    if(!sr.isSuccess()){
                        errCount=errCount+1;
                        tempstring = tempstring + ' ***** ' + string.valueof(sr.getErrors());
                        //lclc delete later
                        /*for(Database.Error err : sr.getErrors()) {
                            System.debug('lclc The following error has occurred. w PBE');                   
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            System.debug('fields that affected this error: ' + err.getFields());
                        }*/
                    }
                }
            }
            
            if(!oliToUpdate.isEmpty()){
                List<Database.SaveResult> uResultsOLI=Database.Update(oliToUpdate,false);

                for(Database.SaveResult sr:uResultsOLI){
                    if(!sr.isSuccess()){
                        errCount=errCount+1;
                        tempstring = tempstring + ' ***** ' + string.valueof(sr.getErrors());
                        //lclc delete later
                        /*for(Database.Error err : sr.getErrors()) {
                            System.debug('lclc The following error has occurred. w OLI');                   
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            System.debug('fields that affected this error: ' + err.getFields());
                        }*/
                    }
                }
            }

            if(!qliToUpdate.isEmpty()){
                 List<Database.SaveResult> uResultsQLI=Database.Update(qliToUpdate,false);

                for(Database.SaveResult sr:uResultsQLI){
                    if(!sr.isSuccess()){
                        errCount=errCount+1;
                        tempstring = tempstring + ' ***** ' + string.valueof(sr.getErrors());
                        //lclc delete later
                        /*for(Database.Error err : sr.getErrors()) {
                            System.debug('lclc The following error has occurred. w QLI');                   
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            System.debug('fields that affected this error: ' + err.getFields());
                        }*/

                    }
                }
            }

            updateTime=DateTime.now().getTime();

            if(tempstring!=''){
                messageQueueHelper cont=new messageQueueHelper();
                cont.generalFail(omq, null, quoteRecord, tempstring,startTime);
                return;
            }else if(pbeToUpdate.isEmpty() || oliToUpdate.isEmpty()){
                messageQueueHelper cont=new messageQueueHelper();
                cont.generalFail(omq, null, quoteRecord, 'opportunity line items to update or price book entries to update null',startTime);
                return;
            }
            else{
                messageQueueHelper cont=new messageQueueHelper();
                //SYstem.debug('lclc in success '+quoteRecord.Successful_Update_Number__c+' '+quoteRecord.Total_Lines_for_Update__c);
                cont.success(omq, quoteRecord, null,(receiveTime - sendTime),(updateTime - receiveTime),startTime);
                return;
            }
            
        }catch(exception ex){
            System.debug('4 ERROR --- '+ex);
             messageQueueHelper cont=new messageQueueHelper();
            cont.generalFail(omq, ex, quoteRecord, null,startTime);

            return;
        }

    }else{
        System.debug('lclc error no body');
        messageQueueHelper cont=new messageQueueHelper();
        cont.generalFail(omq, null, quoteRecord, 'no body returned',startTime);
        return;
    }

}

/**********************************HELPER**********************************/
public static string webcallout(String bodyVar, string endpoint){
    
    string partialEndpoint = Test.isRunningTest() ? '' : Integration_EndPoints__c.getInstance('Flow-008').partial_endpoint__c;
    return RoiIntegrationHelper.webcallout( bodyVar, partialEndpoint);
   
    
/*    Http h = new Http();
    HttpRequest req = new HttpRequest();
    //req.setClientCertificate(clientCert_x,clientCertPasswd_x);
    req.setBody(bodyVar);
    req.setHeader('content-type', 'application/json');
    req.setEndpoint(endpoint);
    req.setMethod('POST');
    req.setTimeout(120000);
    HttpResponse res = h.send(req);
    //system.debug('>>>>>'+res.getBody());
    return res.getBody(); */
}

public static map<integer,list<string>> splitLongList(list<string> listString){
    
    integer numberOfQueueMsg = math.round(listString.size()/recordsPerBatch);
    map<integer,list<string>> mapOfOrderIDs = new map<integer,list<string>>();
                
    for(integer i = 0; i <= numberOfQueueMsg; i++){
        mapOfOrderIDs.put(i,new list<string>());
    } 
                            
    integer index = 0;
    for(string s : listString){
        mapOfOrderIDs.get(math.round(index/recordsPerBatch)).add(s);
        index = index + 1;
    }
    return mapOfOrderIDs;
}

//Helper method to make a priceBookEntry web callout
public static string pbeWebcallout(string reqbody, string endPoint, message_queue__c mqRec){ 

    string partialEndpoint = Test.isRunningTest() ? 'asdf' : Integration_EndPoints__c.getInstance('Flow-009_1').partial_endpoint__c;
 //  return RoiIntegrationHelper.webcallout( reqbody, partialEndpoint);
    return RoiIntegrationHelper.webcallout2( reqbody, partialEndpoint);

}

// MK 7/7/2016 Mock service
/*public static string testCallOut(string reqbody, string endPoint, Message_Queue__c mq) {
    System.Httprequest req = new System.Httprequest();
    req.setMethod('GET');
    req.setBody(reqBody);
    req.setEndPoint(endPoint);
    req.setHeader('Content-Type', 'application/json');
    req.setHeader('Accept', 'application/json');
    
    HttpResponse res = new HttpResponse();
    
    Http httpRequest = new Http();
    res = httpRequest.send(req);
    System.debug('***************************');
    System.debug('Mock Response -- ' + res.getBody());
    System.debug('***************************');
    
    return res.getBody(); 
   
}*/

//Helper method to receive PriceBook Entries   
public static void receivePBEEntries(message_queue__c mqRec){
    string reqbody = '';
    string errorMessage = '';
    DateTime responseTime = System.now();
    DateTime lastSuccessfulRun = integrationCodes.get('Flow-009_1').last_successful_run__c;
    String endPointPath = integrationCodes.get('Flow-009_1').endPointURL__c;
    List<Message_Queue__c> mqRecordsToInsert = new List<message_queue__c>();
    Integer start = System.now().millisecond();    
    // hong.jeon added this one line for debuging    
    mqRec.last_successful_run__c = lastSuccessfulRun;    
    //Get the serialized Json string
    reqbody = serializePBEJsonString(mqRec.Id, lastSuccessfulRun);
    //string endpoint='http://www.mocky.io/v2/5716840f110000e32187daac';
    //Make a call out
        try{  
            //string response = pbeWebcallout(reqbody,endPointPath,mqRec);
            String response;
            if (Test.isRunningTest()) {  
               response= '{"data" : {"inputHeaders" : {"MSGGUID" : "d6a2106d-f050-8f05-5dcc-2269d53b8dc7", "IFID" : "LAY024255", "IFDate" : "20180316183516", "MSGSTATUS" : "S", "ERRORTEXT" : "", "ERRORCODE" : ""}, "body" : {"pricingRecords" : [{"materialNumber" : "MZ-7KM480NE", "accountCode" : "T320/01", "productPriceGroup" : "105", "invoicePrice" : "297.00000", "currency" : "USD", "category" : "IT"},{"materialNumber" : "MZ-7KM480NE", "accountCode" : "T320/01", "productPriceGroup" : "MOB", "invoicePrice" : "297.00000", "currency" : "USD", "category" : "Mobile"}]}}}';

            }else{
               response = pbeWebcallout(reqbody,endPointPath,mqRec);
            }
            //String response = '{"data" : {"inputHeaders" : {"MSGGUID" : "60f29c72-4f03-ab0b-e0e3-c17297c6be54", "IFID" : "LAY024255", "IFDate" : "20180202150053", "MSGSTATUS" : "S", "ERRORTEXT" : "", "ERRORCODE" : ""}, "body" : ""}}';
            System.debug('## In try httpRes:: '+response);
            
            //Debug statements
            if(response != null ) {
                System.debug(' *********************** Response Received Inside receivePBEEntries************************');
                //System.debug('Response Substring -- ' + response.subString(0, 50));
                System.debug(' *********************** Response Received Inside receivePBEEntries ************************');
                // MK 7/7/2016 Remove this block before GO-LIVE
                /*
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                Messaging.EmailFileAttachment fileAttachment = new Messaging.EmailFileAttachment();
                Blob b = Blob.valueOf(response);
                // Create the email attachment
                Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                efa.setFileName('response.txt');
                efa.setBody(b);
                
                String[] toAddresses = new String[] {'dalsnawy@acumensolutions.com', 'brajasekharan@acumensolutions.com', 'mkhan@acumensolutions.com'};
                mail.setToAddresses(toAddresses);
                mail.setSubject('PBE JSON Response');
                mail.setPlainTextBody('Please review the JSON response');
                mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
                
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                */
            }
            
            
            
            
            if(response != null && response != ''){
               
                if(response.contains('body') && response.subString(response.indexOf('body')+5,response.lastindexof('}')).length() <= 6)   {
                 //Empty block******************************
                       List<Message_Queue__c> mqs = new List<Message_Queue__c>();
                // Get the Parent MQ from child and also mark the child as Success
                        if(mqRec.ParentMQ__c != null){
                            Message_Queue__c parentMessageQueue = new Message_Queue__c(Id=mqRec.ParentMQ__c);
                            parentMessageQueue.Status__c = 'Completed';
                            mqs.add(parentMessageQueue);
                        }
                        //Message_Queue__c childMessageQueue = new Message_Queue__c(Id=mqRec.Id);
                        system.debug('Assigning status value : before....'+mqRec);
                        mqRec.Status__c = 'success';
                        mqRec.retry_counter__c = 0;
                        system.debug('Assigning status value : after.....'+mqRec);
                        //mqRec.External_Id__c = 'End of the cycle.';
                        
                        
                        mqs.add(mqRec);
                        
                        system.debug('Update list....'+mqs);
                        
                        update mqs;
                        
                        if(!Test.isRunningTest()) {
                            List<Integration_EndPoints__c> pbIntegEndpoints = !Test.IsRunningTest() ? Integration_EndPoints__c.getAll().values() : new List<Integration_EndPoints__c>();
                            System.debug('********************************************');
                            System.debug('Accessing Custom Settings' + pbIntegEndpoints);
                            System.debug('********************************************'); 
                        
                            for(Integration_EndPoints__c pbIntegSetting : pbIntegEndpoints) {
                            
                                String parentStatus='';
                                if(mqRec.ParentMQ__c !=null){
                                    parentStatus = [select Status__c from Message_Queue__c where id = :mqRec.ParentMQ__c].Status__c;
                                }
                                system.debug('Parent status.....'+parentStatus);
                                if(pbIntegSetting.Name == 'Flow-009_1' && parentStatus == 'Completed') {
                                    pbIntegSetting.last_successful_run__c = responseTime;
                                }
                            }
                            System.debug('********************************************');
                            System.debug('Accessing Custom Settings Post Assignment' + pbIntegEndpoints);
                            System.debug('********************************************'); 
                            update pbIntegEndpoints; 
                        
                            System.debug('## pbIntegEndpoints'+pbIntegEndpoints); 
                        } 
                    
                }  
                //Not empty
                else{
                    
                    if(response.length() > 2000000) {
                    errorMessage = 'Response exceeded maximum allowed limit of 2000000 characters';
                    RoiIntegrationHelper.handleError(mqRec, null, null, errorMessage);
                    }
                    else {
                       // Max 1 minute timout 
                       //while(System.now().millisecond() < start + 6000) {
                         PBEIntegration.processPBE(response,mqRec,responseTime);
                         
                       //}
                    }
                
                }  
                
            }
            
        }
        catch(Exception ex) {
           system.debug('## Exception occurred either invoking the service or parsing the response -- '+ex.getmessage());
           
           if(mqRec.retry_counter__c<=5){
            mqRec.status__c = 'failed-retry';
            mqRec.retry_counter__c += 1;
           }
           else {
            mqRec.status__c = 'failed';
            mqRec.retry_counter__c += 1;               
           }
            
            upsert mqRec;
    }
}

//Helper method to get the serialized Json priceBook Entry (009_1) string
public static string serializePBEJsonString(string mqId, DateTime lastRun){
    
    string layoutId = Test.isRunningTest() ? 'asdf' : Integration_EndPoints__c.getInstance('Flow-009_1').layout_id__c;
    
    cihStub.msgHeadersRequest headers = new cihStub.msgHeadersRequest(layoutId); 
    cihStub.timeWindowReq twReq = new cihStub.timeWindowReq();       
    twReq.fromTime = lastRun.format('yyMMddHHmmss', 'America/New_York');//lastRun.format('YYMMddHHmmss', 'America/New_York'); - Modified by vijay on 1/22/2019 to Send Calender year value instead of week year
    twReq.toTime = datetime.now().format('YYMMddHHmmss', 'America/New_York');
  //  twReq.jobId = mqId;
    twReq.inputHeaders = headers;
    System.debug('## Serialized PBE_009 JSON String :: '+json.serializePretty(twReq));
    
    return json.serializePretty(twReq);
}

//Commented the below code as it is longer used
 /* public static map<integer,list<PBEIntegration.pricingRecords>> splitLongJSonPBEList(list<PBEIntegration.pricingRecords> listPBE){

    // PBE list into chunks of 1000 per mapOfOrderId key
    integer total = 0; // total list size
    integer range = 1000; // range for each key
    integer startIndex = 0; // start index
    integer endIndex = 0 + range; // end index
    total = listPBE.size();
    map<integer,list<PBEIntegration.pricingRecords>> mapOfOrderIDs = new map<integer,list<PBEIntegration.pricingRecords>>();
    List<PBEIntegration.pricingRecords> pbeList = new List<PBEIntegration.pricingRecords>();
    while(startIndex < total) {
        // Evaluate end index
           endIndex = (total<endIndex?total:endIndex);
           System.debug('Processing PBEInegration List : ' + startIndex +
                        ' to ' + endIndex + ' of a total of ' + total);

        // Splice list into exactly 1000 chunks or less
           for(Integer i=startIndex; i<endIndex; i++) {
                pbeList.add(listPBE.get(i));
           }
            
           System.debug(' PBEIntegration pbeList size ' +  pbeList.size());
          
            for(integer i = 0; i <endIndex; i++){
                mapOfOrderIDs.put(i,new list<PBEIntegration.pricingRecords>());
            } 
                        
               Integer index = startIndex; // to make sure the starting index is current
            for(PBEIntegration.pricingRecords pbe : pbeList){
                mapOfOrderIDs.get(startIndex).add(pbe);
                index = index + 1;
            }
            
              // Reset indices
              startIndex = endIndex;
              endIndex = startIndex + range;
    }
    
    return mapOfOrderIDs;
  }*/
        
    /**
     * Commenting out this logic as the Math.round function
     * would not achieve the desired functionality of breaking
     * the list into 1000 values / key 
     *
    integer recsPerBatch = 1000;//@Todo: make it to 1000 later
    System.debug('## listPBE size::'+listPBE.size());
    integer numberOfQueueMsg = math.round(listPBE.size()/recsPerBatch);
    numberOfQueueMsg+=1;
    System.debug('## numberOfQueueMsg::'+numberOfQueueMsg );
    map<integer,list<PBEIntegration.pricingRecords>> mapOfOrderIDs = new map<integer,list<PBEIntegration.pricingRecords>>();
          
    for(integer i = 0; i <numberOfQueueMsg; i++){
      mapOfOrderIDs.put(i,new list<PBEIntegration.pricingRecords>());
    } 
                
    integer index = 0;
    system.debug('asas---678');
    for(PBEIntegration.pricingRecords pbe : listPBE){
      mapOfOrderIDs.get(math.round(index/recsPerBatch)).add(pbe);
      index = index + ;
    }
    
    system.debug('asas---683');
    return mapOfOrderIDs;
    
  }
  */
//Helper method to process Partner Accounts
  public static void processPartnerInserts(message_queue__c mqRec){
  
  List<Account> accToUpdate = new List<Account>();
  
  for(Account acc:[Select Id, Name, Trigger_WF_003__c, Last_Modified_Date_By_CIS__c, LastModifiedById from Account where Integration_Status__c ='Not In Sync']){
    //check if the time  window is more than 24 hours or 1 day
    Long milliSeconds =  System.now().getTime() - acc.Last_Modified_Date_By_CIS__c.getTime();
    Long hours = milliSeconds/3600000;
    
    System.debug('## hours'+hours);
    if(hours >=24.0){
        System.debug('## Inside partner if block');
        acc.Trigger_WF_003__c = True;   
        accToUpdate.add(acc);
    }
  }
  try{
      if(accToUpdate.size()>0)
        update accToUpdate;
    }
  Catch(Exception ex){
    System.debug('## Exception in Partner Accounts Update ::'+ex);
    }

  }

    public static void dummyMethodCodeCovereage(){
        string x = '';
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        x = x;
        
    }
}