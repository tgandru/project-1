///note to up coverage more figure out how to insert an isr user. 
@isTest
private class CastIronIntegrationHelperTest {

    static Id endUserRT = TestDataUtility.retrieveRecordTypeId('End_User', 'Account'); 
    static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
    
    @testSetup static void setup() {

        //Jagan: Test data for fiscalCalendar__c
        Test.loadData(fiscalCalendar__c.sObjectType, 'FiscalCalendarTestData');

        //Channelinsight configuration
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();

        List<Integration_EndPoints__c> ieps = new List<Integration_EndPoints__c>();
        /*Integration_EndPoints__c iep = new Integration_EndPoints__c();
        iep.name ='Flow-012';
        iep.endPointURL__c = 'https://www.google.com';
        iep.layout_id__c = 'LAY024197';
        iep.partial_endpoint__c = '/SFDC_IF_012';
        ieps.add(iep);*/
        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='Flow-008';
        iep2.endPointURL__c = 'https://www.google.com';
        iep2.layout_id__c = 'LAY024197';
        iep2.partial_endpoint__c = '/SFDC_IF_012';
        ieps.add(iep2);
        
        Integration_EndPoints__c iep3 = new Integration_EndPoints__c();
        iep3.name ='Flow-009_1';
        iep3.endPointURL__c = 'https://www.google.com';
        iep3.layout_id__c = 'LAY024255';
        iep3.partial_endpoint__c = '/SFDC_IF_US_009_1';
        iep3.last_successful_run__c = Datetime.now();
        ieps.add(iep3);
        insert ieps;

        List<Roll_Out_Schedule_Constants__c> roscs=new List<Roll_Out_Schedule_Constants__c>();
        Roll_Out_Schedule_Constants__c rosc1=new Roll_Out_Schedule_Constants__c(Name='Roll Out Schedule Parameters',Average_Week_Count__c=4.33,DML_Row_Limit__c=3200);
        roscs.add(rosc1);
        insert roscs;

        Zipcode_Lookup__c zip=TestDataUtility.createZipcode();
        insert zip;

        //Creating test Account
        list<account> accts=new list<account>();
        Account acct=TestDataUtility.createAccount(TestDataUtility.generateRandomString(5));
        acct.RecordTypeId = endUserRT;
        accts.add(acct);
        account a2=TestDataUtility.createAccount('dpba stuff');
        a2.RecordTypeId = endUserRT;
        a2.sap_company_code__C='5554444';
        accts.add(a2);
        insert accts;
        
        
        //Creating the Parent Account for Partners
        Account paacct=TestDataUtility.createAccount(TestDataUtility.generateRandomString(7));
        acct.RecordTypeId = directRT;
        insert paacct;
        
        
        //Creating custom Pricebook
        PriceBook2 pb = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(4)); 
        insert pb;

        //create direct pricebook assign
         DirectPriceBookAssign__c dpba=TestDataUtility.createDPBA(a2, pb);
        insert dpba;
        
        
        //Creating list of Opportunities
        List<Opportunity> opps=new List<Opportunity>();
        Opportunity oppOpen=TestDataUtility.createOppty('New Opp from Test setup',acct, pb, 'RFP', 'IT', 'FAX/MFP', 'Identification');
        //oppOpen.Rollout_Duration__c=6;
        //oppOpen.Roll_Out_Start__c=system.today();
        opps.add(oppOpen);
        /*Opportunity oppClosed=TestDataUtility.createOppty('Closed Oppty',acct, pb, 'RFP', 'IT', 'FAX/MFP', 'Drop');
        opps.add(oppClosed);*/
        Opportunity oppOpen2=TestDataUtility.createOppty('New Opp from Test setup',acct, pb, 'RFP', 'WE', 'FAX/MFP', 'Identification');
        opps.add(oppOpen2);
        
        insert opps;
        

        //Creating the partner for that opportunity
        Partner__c partner = TestDataUtility.createPartner(oppOpen,acct,paacct,'Distributor');
        insert partner;
        
        Partner__c partner1 = TestDataUtility.createPartner(oppOpen2,acct,paacct,'Distributor');
        insert partner1;


        /* Creating Products: SAP_Material_Id__C should be unique and case sensitive ID
        Used the utility method to generate random string for the SAP_Material_ID as it should be unique 
        when every time you passed on
        */
        
        //Creating Products
        List<Product2> prods=new List<Product2>();
        Product2 standaloneProd=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Standalone Prod1', 'Printer[C2]', 'FAX/MFP', 'H/W');
        prods.add(standaloneProd);
        Product2 parentProd1=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Parent Prod1', 'Printer[C2]', 'FAX/MFP', 'H/W');
        prods.add(parentProd1);/*
        Product2 childProd1=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod1', 'Printer[C2]', 'FAX/MFP', 'Service pack');
        prods.add(childProd1);
        Product2 childProd2=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod2', 'Printer[C2]', 'FAX/MFP', 'Service pack');
        prods.add(childProd2);
        Product2 parentProd2=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Parent Prod2', 'Printer[C2]', 'FAX/MFP', 'H/W');
        prods.add(parentProd2);
        Product2 childProd3=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod3', 'Printer[C2]', 'FAX/MFP', 'Service pack');
        prods.add(childProd3);
        Product2 childProd4=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod4', 'Printer[C2]', 'FAX/MFP', 'Service pack');
        prods.add(childProd4);
        Product2 childProd5Standalone=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12), 'child AND standalone prod5', 'Printer[C2]', 'FAX/MFP', 'Service pack');
        prods.add(childProd5Standalone);*/
        insert prods;

        Id pricebookId = Test.getStandardPricebookId();

        //CreatePBEs
        //create the pricebook entries for the custom pricebook
        List<PriceBookEntry> custompbes=new List<PriceBookEntry>();
        PriceBookEntry standaloneProdPBE=TestDataUtility.createPriceBookEntry(standaloneProd.Id, pb.Id);
        custompbes.add(standaloneProdPBE);
        PriceBookEntry parentProd1PBE=TestDataUtility.createPriceBookEntry(parentProd1.Id, pb.Id);
        custompbes.add(parentProd1PBE);/*
        PriceBookEntry parentProd2PBE=TestDataUtility.createPriceBookEntry(parentProd2.Id, pb.Id);
        custompbes.add(parentProd2PBE);
        PriceBookEntry childProd1PBE=TestDataUtility.createPriceBookEntry(childProd1.Id, pb.Id);
        custompbes.add(childProd1PBE);
        PriceBookEntry childProd2PBE=TestDataUtility.createPriceBookEntry(childProd2.Id, pb.Id);
        custompbes.add(childProd2PBE);
        PriceBookEntry childProd3PBE=TestDataUtility.createPriceBookEntry(childProd3.Id, pb.Id);
        custompbes.add(childProd3PBE);
        PriceBookEntry childProd4PBE=TestDataUtility.createPriceBookEntry(childProd4.Id, pb.Id);
        custompbes.add(childProd4PBE);
        PriceBookEntry childProd5StandalonePBE=TestDataUtility.createPriceBookEntry(childProd5Standalone.Id, pb.Id);
        custompbes.add(childProd5StandalonePBE);*/
        insert custompbes;

        //insert product relationship
       /* List<Product_Relationship__c> prs=new List<Product_Relationship__c>();
        Product_Relationship__c pr1=TestDataUtility.createProductRelationships(childProd1, parentProd1);
        prs.add(pr1);
        Product_Relationship__c pr2=TestDataUtility.createProductRelationships(childProd2, parentProd1);
        prs.add(pr2);
        Product_Relationship__c pr3=TestDataUtility.createProductRelationships(childProd3, parentProd2);
        prs.add(pr3);
        Product_Relationship__c pr4=TestDataUtility.createProductRelationships(childProd4, parentProd2);
        prs.add(pr4);
        Product_Relationship__c pr5=TestDataUtility.createProductRelationships(childProd5Standalone, parentProd2);
        prs.add(pr5);
        insert prs;*/

        // Creating opportunity Line item
        List<OpportunityLineItem> olis=new List<OpportunityLineItem>();
        OpportunityLineItem standaloneProdOLI2=TestDataUtility.createOpptyLineItem(standaloneProd, standaloneProdPBE, oppOpen2);
        olis.add(standaloneProdOLI2);
        OpportunityLineItem standaloneProdOLI=TestDataUtility.createOpptyLineItem(standaloneProd, standaloneProdPBE, oppOpen);
        olis.add(standaloneProdOLI);
        OpportunityLineItem parentProd1OLI=TestDataUtility.createOpptyLineItem(parentProd1, parentProd1PBE, oppOpen);
        parentProd1OLI.Parent_Product__c=parentProd1.Id;
        olis.add(parentProd1OLI);/*
        OpportunityLineItem childProd1OLI=TestDataUtility.createOpptyLineItem(childProd1, childProd1PBE, oppOpen);
        childProd1OLI.Parent_Product__c=parentProd1.Id;
        olis.add(childProd1OLI);
        OpportunityLineItem childProd2OLI=TestDataUtility.createOpptyLineItem(childProd2, childProd2PBE, oppOpen);
        childProd2OLI.Parent_Product__c=parentProd1.Id;
        olis.add(childProd2OLI);
        OpportunityLineItem parentProd2OLI=TestDataUtility.createOpptyLineItem(parentProd2, parentProd2PBE, oppOpen);
        parentProd2OLI.Parent_Product__c=parentProd2.Id;
        olis.add(parentProd2OLI);
        OpportunityLineItem childProd3OLI=TestDataUtility.createOpptyLineItem(childProd3, childProd3PBE, oppOpen);
        childProd3OLI.Parent_Product__c=parentProd2.Id;
        olis.add(childProd3OLI);
        OpportunityLineItem childProd4OLI=TestDataUtility.createOpptyLineItem(childProd4, childProd4PBE, oppOpen);
        childProd4OLI.Parent_Product__c=parentProd2.Id;
        olis.add(childProd4OLI);
        OpportunityLineItem childProd5OLI=TestDataUtility.createOpptyLineItem(childProd5Standalone, childProd5StandalonePBE, oppOpen);
        childProd5OLI.Parent_Product__c=parentProd2.Id;
        olis.add(childProd5OLI);*/
        
        insert olis;


        //OpportunityLineItem childProd5StandaloneOLI=TestDataUtility.createOpptyLineItem(childProd5Standalone, childProd5StandalonePBE, oppOpen);
        //insert childProd5StandaloneOLI;

        //create quote
        List<Quote> qtlist = new List<Quote>();
        Quote quot = TestDataUtility.createQuote('Test Quote', oppOpen, pb);
        quot.ROI_Requestor_Id__c=UserInfo.getUserId();
        quot.Status='Draft';
        quot.Division__c='IT';
        quot.Valid_From_Date__c=System.today();
        qtlist.add(quot);
        //insert quot;
        Quote quot2 = TestDataUtility.createQuote('Test Quote', oppOpen2, pb);
        quot2.ROI_Requestor_Id__c=UserInfo.getUserId();
        quot2.Status='Draft';
        quot2.Division__c='W/E';
        quot2.Valid_From_Date__c=System.today();
        quot2.ProductGroupMSP__c='INTERNET_INFRA';
        qtlist.add(quot2);
        
        insert qtlist;

        //create qlis
        List<QuoteLineItem> qlis=new List<QuoteLineItem>();
        QuoteLineItem standaloneProdQLI=TestDataUtility.createQuoteLineItem(quot.Id, standaloneProd, standaloneProdPBE, standaloneProdOLI);
        qlis.add(standaloneProdQLI);
        QuoteLineItem standaloneProdQLI2=TestDataUtility.createQuoteLineItem(quot2.Id, standaloneProd, standaloneProdPBE, standaloneProdOLI);
        qlis.add(standaloneProdQLI2);
       QuoteLineItem parentProd1QLI=TestDataUtility.createQuoteLineItem(quot.Id, parentProd1, parentProd1PBE, parentProd1OLI);
        parentProd1QLI.Parent_Product__c=parentProd1.Id;
        qlis.add(parentProd1QLI);/*
        QuoteLineItem childProd3QLI=TestDataUtility.createQuoteLineItem(quot.Id, childProd3, childProd3PBE, childProd3OLI);
        childProd3QLI.Parent_Product__c=parentProd2.Id;
        qlis.add(childProd3QLI);
        QuoteLineItem childProd4QLI=TestDataUtility.createQuoteLineItem(quot.Id, childProd4, childProd4PBE, childProd4OLI);
        childProd4QLI.Parent_Product__c=parentProd2.Id;
        qlis.add(childProd4QLI);
        QuoteLineItem childProd5QLI=TestDataUtility.createQuoteLineItem(quot.Id, childProd5Standalone, childProd5StandalonePBE, childProd5OLI);
        childProd5QLI.Parent_Product__c=parentProd2.Id;
        qlis.add(childProd5QLI);
        QuoteLineItem childProd5StandaloneQLI=TestDataUtility.createQuoteLineItem(quot.Id, childProd5Standalone, childProd5StandalonePBE, childProd5StandaloneOLI);
        qlis.add(childProd5StandaloneQLI);
        System.debug('lclc for childProd5StandaloneQLI whats pbe  '+childProd5StandaloneQLI.pricebookentryId);*/
        insert qlis;

    }
    
    @isTest static void test_method_one() {
        Quote quot=[select Id from Quote where Division__c='IT' limit 1];

        QuoteSubmitApprovalButtonCustomExtension cont=new QuoteSubmitApprovalButtonCustomExtension(new ApexPages.StandardController(quot));
        Test.setMock(HttpCalloutMock.class, new MockResponseGenerator('"S"'));
        cont.submitApproval();
     /*   cont.submitApproval2();
        cont.submitApprovalMobile();
        cont.returnToQuote();
        cont.postToChatterNegative(quot);
        cont.postToChatterPositive(quot);
*/
        //Test.startTest();
        

        //Test.stopTest();

    }
    
    
    @isTest static void test_method_one_1() {
        Quote quot=[select Id from Quote where Division__c='W/E' limit 1];

        QuoteSubmitApprovalButtonCustomExtension cont=new QuoteSubmitApprovalButtonCustomExtension(new ApexPages.StandardController(quot));
        Test.setMock(HttpCalloutMock.class, new MockResponseGenerator('"S"'));
        cont.submitApproval();
    }
    
    @isTest static void test_method_three() {
        DateTime startTime=DateTime.now();
        Quote quot=[select Id from Quote where Division__c='IT' limit 1];

        QuoteSubmitApprovalButtonCustomExtension cont=new QuoteSubmitApprovalButtonCustomExtension(new ApexPages.StandardController(quot));
        Test.setMock(HttpCalloutMock.class, new MockResponseGenerator('"E"'));
          cont.isInMobile=true;
        //cont.recieveListQLI();
        cont.submitApproval();
        cont.submitApproval2();
        cont.submitApprovalMobile();
        cont.returnToQuote();
        cont.postToChatterNegative(quot);
        cont.postToChatterPositive(quot);

    
        //Manually calling this method since there is no other way
        // to invoke it
        message_queue__c omq=new message_queue__c(retry_counter__c=0);
        messageQueueHelper queueHelper=new messageQueueHelper();
        queueHelper.failed(omq, null, null, 'no body returned',startTime);
    }

    //try to pass error in webcallout
    @isTest static void test_method_four_castiron(){
        Quote quot=[select Id from Quote where Division__c='IT' limit 1];

        QuoteSubmitApprovalButtonCustomExtension cont=new QuoteSubmitApprovalButtonCustomExtension(new ApexPages.StandardController(quot));
         Test.setMock(HttpCalloutMock.class, new MockResponseGenerator('"E"'));
           cont.isInMobile=true;
        cont.submitApproval();
        cont.submitApproval2();
        cont.submitApprovalMobile();
        cont.returnToQuote();
        cont.postToChatterNegative(quot);
        cont.postToChatterPositive(quot);


    }

    //for quotesubmit approval to test submitapproval
    @isTest static void test_method_one_submitapproval() {
        DateTime startTime=DateTime.now();
        Quote quot=[select Id from Quote where Division__c='IT' limit 1];

        QuoteSubmitApprovalButtonCustomExtension cont=new QuoteSubmitApprovalButtonCustomExtension(new ApexPages.StandardController(quot));
        Test.setMock(HttpCalloutMock.class, new MockErrorResponseGenerator('"S"'));
        cont.isInMobile=true;
        cont.submitApproval();
        cont.submitApproval2();
        cont.submitApprovalMobile();
        cont.returnToQuote();
        cont.postToChatterNegative(quot);
        cont.postToChatterPositive(quot);


   
        
        //Manually calling this method since there is no other way
        // to invoke it
        message_queue__c omq=new message_queue__c(retry_counter__c=0);
        messageQueueHelper queueHelper=new messageQueueHelper();
        queueHelper.failed(omq, null, null, 'no body returned',startTime);
    }

    //for quotesubmitapproval to test submit approval 2
    @isTest static void test_method_two_submitapproval() {
        DateTime startTime=DateTime.now();
        Quote quot=[select Id from Quote where Division__c='IT' limit 1];
        quot.Last_Update_from_CastIron__c=system.today().addDays(-2);
        update quot;

        QuoteSubmitApprovalButtonCustomExtension cont=new QuoteSubmitApprovalButtonCustomExtension(new ApexPages.StandardController(quot));
        Test.setMock(HttpCalloutMock.class, new MockErrorResponseGenerator('"S"'));
        cont.isInMobile=true;
        cont.submitApproval();
        cont.submitApproval2();
        cont.submitApprovalMobile();
        cont.returnToQuote();
        cont.postToChatterNegative(quot);
        cont.postToChatterPositive(quot);


      
        
        //Manually calling this method since there is no other way
        // to invoke it
        message_queue__c omq=new message_queue__c(retry_counter__c=0);
        messageQueueHelper queueHelper=new messageQueueHelper();
        queueHelper.failed(omq, null, null, 'no body returned',startTime);
    }

    //for quotesubmitapproval to test mobile.
    @isTest static void test_method_three_submitapproval() {
        DateTime startTime=DateTime.now();
        Quote quot=[select Id from Quote where Division__c='IT' limit 1];

        QuoteSubmitApprovalButtonCustomExtension cont=new QuoteSubmitApprovalButtonCustomExtension(new ApexPages.StandardController(quot));
        Test.setMock(HttpCalloutMock.class, new MockErrorResponseGenerator('"S"'));
        cont.isInMobile=true;
        cont.submitApproval();
        cont.submitApproval2();
        cont.submitApprovalMobile();
        cont.returnToQuote();
        cont.postToChatterNegative(quot);
        cont.postToChatterPositive(quot);



        cont.submitApprovalMobile();

        
        //Manually calling this method since there is no other way
        // to invoke it
        message_queue__c omq=new message_queue__c(retry_counter__c=0);
        messageQueueHelper queueHelper=new messageQueueHelper();
        queueHelper.failed(omq, null, null, 'no body returned',startTime);
    }

    class MockResponseGenerator implements HttpCalloutMock {
        String responseStatus;
        MockResponseGenerator(String responseStatus) {
            this.responseStatus = responseStatus;
        }
    // Implement this interface method
        public HTTPResponse respond(HTTPRequest req) {
            // Optionally, only send a mock response for a specific endpoint
            // and method.
            System.assertEquals('callout:X012_013_ROI', req.getEndpoint());
            System.assertEquals('POST', req.getMethod());
            
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            if(responseStatus=='"S"'){
                res.setBody('{' +
                            '"inputHeaders": {'+
                            '"MSGGUID": "ada747b2-dda9-1931-acb2-14a2872d4358",' +
                            '"IFID": "TBD",' +
                            '"IFDate": "20160628194634",' +
                            '"MSGSTATUS":' + responseStatus + ',' +
                            '"ERRORTEXT": "Success",' +
                            '"ERRORCODE": "S"' +
                                            '},' +
                            '"body": {'+
                                '"lines": ['+

                                    '{"unitCost":"52.92",'+
                                    '"materialCode":"CLT-Y04s",'+
                                    '"quantity":"1.0",'+
                                    '"invoicePrice":"65.55",'+
                                    '"currencyKey":"USD",'+
                                    '"itemNumberSdDoc":"000001"},'+

                                    '{"unitCost":null,'+
                                    '"materialCode":"CLT-L04s",'+
                                    '"quantity":"1.0",'+
                                    '"invoicePrice":null,'+
                                    '"currencyKey":"USD",'+
                                    '"itemNumberSdDoc":"000002"}'+
                                ']'+
                            
                            '}'+
                         '}');
            }else{
                res.setBody('{' +
                            '"inputHeaders": {'+
                            '"MSGGUID": "ada747b2-dda9-1931-acb2-14a2872d4358",' +
                            '"IFID": "TBD",' +
                            '"IFDate": "20160628194634",' +
                            '"MSGSTATUS":' + responseStatus + ',' +
                            '"ERRORTEXT": "Error",' +
                            '"ERRORCODE": "E"' +
                                            '},' +
                            '"body": {'+
                                '"lines": ['+

                                    '{"unitCost":"52.92",'+
                                    '"materialCode":"CLT-Y04s",'+
                                    '"quantity":"1.0",'+
                                    '"invoicePrice":"65.55",'+
                                    '"currencyKey":"USD",'+
                                    '"itemNumberSdDoc":"000001"},'+

                                    '{"unitCost":null,'+
                                    '"materialCode":"CLT-L04s",'+
                                    '"quantity":"1.0",'+
                                    '"invoicePrice":null,'+
                                    '"currencyKey":"USD",'+
                                    '"itemNumberSdDoc":"000002"}'+
                                ']'+
                            
                            '}'+
                         '}');
            }
            
            res.setStatusCode(200);
            return res;



        }
    }
    
    class MockErrorResponseGenerator implements HttpCalloutMock {
        String responseStatus;
        MockErrorResponseGenerator(String responseStatus) {
            this.responseStatus = responseStatus;
        }
    // Implement this interface method
        public HTTPResponse respond(HTTPRequest req) {
            // Optionally, only send a mock response for a specific endpoint
            // and method.
            System.assertEquals('callout:X012_013_ROI', req.getEndpoint());
            System.assertEquals('POST', req.getMethod());
            
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{' +
                            '"inputHeaders": {'+
                            '"MSGGUID": "ada747b2-dda9-1931-acb2-14a2872d4358",' +
                            '"IFID": "TBD",' +
                            '"IFDate": "20160628194634",' +
                            '"MSGSTATUS":' + responseStatus + ',' +
                            '"ERRORTEXT": null,' +
                            '"ERRORCODE": null' +
                                            '},' +
                            '"body": {'+
                                '"lines": ['+
                                    // Forcing test to fail to increase 
                                    // test coverage for other helper classes
                                    '{"uniCost":"52.92",'+
                                    '"materialCode":"CLT-Y04s",'+
                                    '"quantity":"1.0",'+
                                    '"invoicePrice":"65.55",'+
                                    '"currencyKey":"USD",'+
                                    '"itemNumberSdDoc":"000001"}'+
                                ']'+
                            
                            '}'+
                         '}');
            res.setStatusCode(200);
            return res;



        }
    }

    //for quote submit for approval to test mobile without approver.
    @isTest static void test_method_mobileCaseSubmitApprovalNoApprover() {
        Quote quot=[select Id from Quote where Division__c='IT' limit 1];
        Test.startTest();
            PageReference pageRef = Page.QuoteSubmitApprovalButtonCustom;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('id',quot.Id);
            ApexPages.currentPage().getParameters().put('isdtp','p1');
            QuoteSubmitApprovalButtonCustomExtension cont = new QuoteSubmitApprovalButtonCustomExtension(new ApexPages.StandardController(quot));
            
        Test.stopTest();
    }

    //for quote submit for approval to test mobile with approver.
    @isTest static void test_method_mobileCaseSubmitApprovalWithApprover() {
        Quote quot=[select Id from Quote where Division__c='IT' limit 1];
        //Create user for profile as approver user
        Profile p = [SELECT Id FROM Profile WHERE Name='B2B ISR Sales User - Mobile']; 
        User u = new User(Alias = 'standt', Email='Approveruser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='TestingApproval', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='Approveruser@testorg.com', IsActive = true);
        insert u;
        
        Test.startTest();
            PageReference pageRef = Page.QuoteSubmitApprovalButtonCustom;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('id',quot.Id);
            ApexPages.currentPage().getParameters().put('isdtp','p1');
            QuoteSubmitApprovalButtonCustomExtension cont = new QuoteSubmitApprovalButtonCustomExtension(new ApexPages.StandardController(quot));
            
        Test.stopTest();
    }

    //for quote submit for approval to test non-mobile/desktop without approver.
    @isTest static void test_method_desktopCaseSubmitApprovalNoApprover() {
        Quote quot=[select Id from Quote where Division__c='IT' limit 1];
        quot.Division__c='Office';
        update quot;
        
        Test.startTest();
            PageReference pageRef = Page.QuoteSubmitApprovalButtonCustom;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('id',quot.Id);
            QuoteSubmitApprovalButtonCustomExtension cont = new QuoteSubmitApprovalButtonCustomExtension(new ApexPages.StandardController(quot));
            cont.submitApproval();
            cont.submitApproval2();
            cont.submitApprovalMobile();
            cont.returnToQuote();
            cont.postToChatterNegative(quot);
            cont.postToChatterPositive(quot);

        Test.stopTest();
    }

    //for quote submit for approval to test non-mobile/desktop with approver.
    @isTest static void test_method_desktopCaseSubmitApprovalWithApprover() {
        Quote quot=[select Id from Quote where Division__c='IT' limit 1];
        quot.Division__c='Office';
        update quot;
        
        //Create user for profile as approver user
        Profile p = [SELECT Id FROM Profile WHERE Name='B2B ISR Sales User - Mobile']; 
        User u = new User(Alias = 'standt', Email='Approveruser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='TestingApproval', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='Approveruser@testorg.com', IsActive = true);
        insert u;
        
        Test.startTest();
            PageReference pageRef = Page.QuoteSubmitApprovalButtonCustom;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('id',quot.Id);
            ApexPages.currentPage().getParameters().put('isdtp','p1');
            QuoteSubmitApprovalButtonCustomExtension cont = new QuoteSubmitApprovalButtonCustomExtension(new ApexPages.StandardController(quot));
            cont.submitApproval();
            cont.submitApproval2();
            cont.submitApprovalMobile();
            cont.returnToQuote();
            cont.postToChatterNegative(quot);
            cont.postToChatterPositive(quot);

            cont.selectedISRRep = u.Id;
            cont.submitApprovalMobile();
            cont.needCallout = true;
        Test.stopTest();
    }

    //for quote submit for approval to test CastIronIntegrationHelper methods.
    @isTest static void test_method_CastIronIntegrationHelper_one() {
        Quote quot=[select Id from Quote where Division__c='IT' limit 1];
        List<String> stringList = new List<String>();
        stringList.add('test1');
        stringList.add('test2');
        stringList.add('test3');
        Account acct = [Select Id, Name, sap_company_code__C From Account Where sap_company_code__C='5554444'];
        acct.Integration_Status__c ='Not In Sync';
        acct.Last_Modified_Date_By_CIS__c = Datetime.newInstance(2016, 7,6);
        update acct;
        message_queue__c testQueue = new message_queue__c(retry_counter__c=0);
        Test.startTest();
            CastIronIntegrationHelper.sendMQrealTime008to012(quot.Id);
            CastIronIntegrationHelper.receivePBEEntries(testQueue);
            CastIronIntegrationHelper.processPartnerInserts(testQueue);
        Test.stopTest();
        System.assertEquals('test1',CastIronIntegrationHelper.splitLongList(stringList).get(0).get(0));
        
    }
    
    @isTest static void test_method_CastIronIntegrationHelper_two() {
        Quote quot=[select Id from Quote where Division__c='IT' limit 1];
         Account acct = [Select Id, Name, sap_company_code__C From Account Where sap_company_code__C='5554444'];
         acct.sap_company_code__C='';
         update acct;
       string s='S';
        Test.setMock(HttpCalloutMock.class, new MockResponseGenerator(s));
        Test.startTest();
        List<QuoteLineItem> qli=new list<QuoteLineItem>([select id from QuoteLineItem where QuoteId=:quot.ID]);
        delete qli;
            CastIronIntegrationHelper.sendMQrealTime(quot.Id);
            
        CastIronIntegrationHelper.dummyMethodCodeCovereage();
        Test.stopTest();
       
        
    } 
    
}