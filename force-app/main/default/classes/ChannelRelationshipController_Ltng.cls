global class ChannelRelationshipController_Ltng{
    
    //Attributes
    global Id opporunityId {get;set;}
    public String retUrl {get;set;}
    public String partnerDistributorIds {get;set;}
    public boolean saveandAddproducts{get;set;}
    
    //Constructors
    public ChannelRelationshipController_Ltng(ApexPages.StandardController con) {
        opporunityId = ApexPages.currentPage().getParameters().get('id');
        retUrl = ApexPages.currentPage().getParameters().get('retUrl');
        //Added by Jagan: If the redirect is from partner creation then do not display normal save button
        //and display Save and Add Products instead.
        if(retUrl != null && retUrl.contains('OpportunityPartnerCreation'))
            saveandAddproducts= TRUE;
        else
            saveandAddproducts= FALSE;
        partnerDistributorIds = ApexPages.currentPage().getParameters().get('partnerIds');
    }
    
    //Methods
    
    //Fetch related Opportunity details
    @RemoteAction
    global static Opportunity fetchOpportunityDetails(Id oppId) {
        return [Select Id, Name, Opportunity_Number__c, AccountId, ProductGroupTest__c, Description, Account.Name, Type from Opportunity where Id =:oppId];
    }
    
    //Fetch all the Resellers and Distributors linked to Opportunity
    @RemoteAction
    global static List<Partner__c> fetchResellersAndDistributors(Id oppId, String partnerIds) {
        //System.debug('Mayank1 '+ApexPages.currentPage().getParameters().get('partnerIds'));
        System.debug('Mayank2 '+partnerIds);
        
        List<String> distributorRoles = Label.Distributor_Roles.split('; ');
        List<String> resellerRoles = Label.Reseller_Roles.split('; ');
        /*
        List<Partner__c> partners;
        if(partnerIds != null) {
            List<Id> pIds = partnerIds.split(';');
            partners = [Select Partner__c, Partner__r.Name, Role__c, Partner__r.Type, Partner__r.Partner_Top_Tier__c from Partner__c where Partner__c in :pIds And (Role__c in :distributorRoles Or Role__c in :resellerRoles)];
        } else {
            partners = [Select Partner__c, Partner__r.Name, Role__c, Partner__r.Type, Partner__r.Partner_Top_Tier__c from Partner__c where Opportunity__c = :oppId And (Role__c in :distributorRoles Or Role__c in :resellerRoles)];    
        }
        */
       return [Select Id, Partner__c, Partner__r.Name, Contact__c, Role__c, Partner__r.Type, Partner__r.Partner_Top_Tier__c, Partner__r.RecordType.Name, Partner__r.Parent.RecordType.Name from Partner__c where Opportunity__c = :oppId And (Role__c in :distributorRoles Or Role__c in :resellerRoles)];//partners;
        
    }
    
    //Fetch existing Channel Relations
    @RemoteAction
    global static List<Channel__c> fetchExistingChannelRelations(Id oppId) {
        return [Select Id, Opportunity__c, Reseller__c, Distributor__c from Channel__c where Opportunity__c = :oppId];
    }
    
    //Add/Update/Delete Channel Relations
    @RemoteAction
    global static String saveChannelRelationships(Id oppId, List<Channel__c> channelRelations) {
        List<Channel__c> existingRelations = new List<Channel__c>();
        List<Channel__c> toBeDeletedRelations = new List<Channel__c>();
        
        //Fetch existing Channel Relations
        existingRelations = fetchExistingChannelRelations(oppId);
        
        for(Integer i=0; i<existingRelations.size(); i++) {
            Boolean found = false;
            for(Channel__c cr : channelRelations) {
                if(existingRelations[i].Distributor__c == cr.Distributor__c && existingRelations[i].Reseller__c == cr.Reseller__c) {
                    //Link the existing Channel with Channel came from page if Distributor and Reseller Ids are same
                    cr.Id = existingRelations[i].Id;
                    found = true;
                    break;
                }
            }   
            if(!found) {
                //Delete the relation if any of the existing channels have to be deleted
                toBeDeletedRelations.add(existingRelations[i]);
            }
        }
        
        try {
            upsert channelRelations;
            delete toBeDeletedRelations;
            return 'Relationships created successfully';
        } catch(Exception e) {
            return e.getMessage();
        }
    }
}