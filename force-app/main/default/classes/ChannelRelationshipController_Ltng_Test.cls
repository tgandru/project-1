@isTest
private class ChannelRelationshipController_Ltng_Test {

	 static Id endUserRT = TestDataUtility.retrieveRecordTypeId('End_User', 'Account');
    static Id inDirectRT = TestDataUtility.retrieveRecordTypeId('Indirect', 'Account');
    static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
    @testSetUp static void createTestData()
    {
        //Channelinsight configuration
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        
        //Creating test Account
        Account acct=TestDataUtility.createAccount('Main Account');
        acct.RecordTypeId = endUserRT;
        insert acct;
        
        
       //Creating the Parent Account for Distributor as Partners
        Account paDistAcct=TestDataUtility.createAccount('Distributor Account');
        paDistAcct.RecordTypeId = directRT;
        paDistAcct.Type= 'Distributor';
        insert paDistAcct;

        //Creating the Parent Account for Reseller as Partners
        Account paResellAcct=TestDataUtility.createAccount('Reseller Account');
        paResellAcct.RecordTypeId = inDirectRT;
        paResellAcct.Type = 'Corporate Reseller';
        insert paResellAcct;
        
        
         //Creating list of Opportunities
        List<Opportunity> opps=new List<Opportunity>();
        Opportunity oppOpen=TestDataUtility.createOppty('New Opp from Test setup',acct, null, 'RFP', 'IT', 'FAX/MFP', 'Identified');
        opps.add(oppOpen);
        insert opps;
    

        //Creating the partners
           Partner__c partner1 = TestDataUtility.createPartner(oppOpen,acct,paDistAcct,'Distributor');
           insert partner1 ;

           Partner__c partner2 = TestDataUtility.createPartner(oppOpen,acct,paResellAcct,'Corporate Reseller');
           insert partner2 ;

         // Create Channel
         List<Channel__c> channels = new List<Channel__c>();
         Channel__c channel = TestDataUtility.createChannel(oppOpen, paDistAcct, paResellAcct, partner1, partner2);
         channels.add(channel);
         insert channels;


    }
    
    
     @isTest static void test_method_one() {
        // Implement test code
        Opportunity opp = [select Id from Opportunity where stageName = 'Identified' Limit 1];
         
         
         Opportunity op=ChannelRelationshipController_Ltng.fetchOpportunityDetails(opp.Id);
          List<Partner__c>  prts=new  List<Partner__c> ();
          prts=ChannelRelationshipController_Ltng.fetchResellersAndDistributors(opp.Id,'');
          
          List<Channel__c> existchannels=new List<Channel__c>();
          existchannels=ChannelRelationshipController_Ltng.fetchExistingChannelRelations(opp.Id);
          
          
          String  result=ChannelRelationshipController_Ltng.saveChannelRelationships(opp.Id,existchannels);
          
         
     }
     
      @isTest static void test_method_two() {
        // Implement test code
        Opportunity opp = [select Id from Opportunity where stageName = 'Identified' Limit 1];
         
         ChannelRelationshipController_Ltng channelController = new ChannelRelationshipController_Ltng(new ApexPages.StandardController(opp));
        channelController.retUrl= null;
        channelController.opporunityId = opp.Id;
         
          
         
     }

}