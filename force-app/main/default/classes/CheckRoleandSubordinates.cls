public class CheckRoleandSubordinates {
    // To get all sub roles of IT B2B.
    public static Set<ID> getAllSubRoleIdsIT(Set<ID> roleIds) {
        Set<ID> currentRoleIds = new Set<ID>();
       
        for(UserRole userRole :[select Id from UserRole where ParentRoleId 
             IN :roleIds AND Name !='HTV']) {
            currentRoleIds.add(userRole.Id);
        }
    
        if(currentRoleIds.size() > 0) {
            currentRoleIds.addAll(getAllSubRoleIdsIT(currentRoleIds));
        }
    
        return currentRoleIds;
    }
    // To get all Parent Roles.
    /* public static Set<ID> getParentRoleId(Set<ID> roleIds) {

        Set<ID> currentRoleIds = new Set<ID>();
       for(UserRole ur :[select Id, ParentRoleId from UserRole where Id IN: roleIds]) {
            currentRoleIds.add(ur.ParentRoleId);
        }
        if(currentRoleIds.size() > 0) {
            currentRoleIds.addAll(getParentRoleId(currentRoleIds));
        }
    
        return currentRoleIds;
    }
    public static Set<ID> getAllSubRoleIds(Set<ID> roleIds) {
        Set<ID> currentRoleIds = new Set<ID>();
       
        for(UserRole userRole :[select Id from UserRole where ParentRoleId 
             IN :roleIds]) {
            currentRoleIds.add(userRole.Id);
        }
    
        if(currentRoleIds.size() > 0) {
            currentRoleIds.addAll(getAllSubRoleIds(currentRoleIds));
        }
    
        return currentRoleIds;
    }
    
    */
}