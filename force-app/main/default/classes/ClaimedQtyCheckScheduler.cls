//Scheduler class to schedule Claimed Qty check batch
global class ClaimedQtyCheckScheduler implements Schedulable{

	global void execute(SchedulableContext sc) {
		try{
		    //String Frm = '2016-07-18';
		    String Frm = String.valueOf(system.today()-90);
            String To = String.valueOf(system.today());
			Database.executeBatch(new DV_Check_Claimed_Qty(Frm, To));				
		}
		catch(Exception ex){
			System.debug('Scheduled Job failed : ' + ex.getMessage());
		}
	}
}