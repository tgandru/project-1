public class ContentDocumentLinkTriggerHandler {
    
    public static void insertBeforeContentDocumentLink(List<ContentDocumentLink> newContentDocumentLinkList) {
    	System.debug('ContentDocumentLinkTriggerHandler.insertBeforeContentDocumentLink(Trigger.New) called');
        List<ContentDocumentLink> doclinks = newContentDocumentLinkList;
        List<Id> linkIds = new List<Id>();
        for(ContentDocumentLink doclink : doclinks){
            linkIds.add(doclink.LinkedEntityId); 
        }
        Map<Id, Milestone__c> milestoneList = new  Map<Id, Milestone__c>([SELECT Name FROM Milestone__c  
                                                                          WHERE Id IN :linkIds AND Milestone_Status__c = 'Completed' AND Installation_Site__c != null]);
        
        System.debug('Milestone =====> '+milestoneList);
        String  prfName=[SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId()].Name;
        for(ContentDocumentLink doclink : doclinks){
            if(milestoneList.get(doclink.LinkedEntityId) != null && prfName!='System Administrator'){ //&& prfName!='System Administrator' <== To test for System Administrator profile.
                System.debug('This is called');
                SObject  actualRecord = doclink;
                actualRecord.addError('Cannot insert this file since the milestone ' + milestoneList.get(doclink.LinkedEntityId).Name + ' has been completed already.');
                
            }
        }
    }
    
    public static void deleteBeforeContentDocumentLink(Map<Id, ContentDocumentLink> oldContentDocumentLinkMap) {
		System.debug('ContentDocumentLinkTriggerHandler.deleteBeforeContentDocumentLink(Trigger.OldMap) called');
        List<ContentDocumentLink> doclinks = oldContentDocumentLinkMap.values();
        List<Id> linkIds = new List<Id>();
        for(ContentDocumentLink doclink : doclinks){
            linkIds.add(doclink.LinkedEntityId); 
        }
        
        Map<Id, Milestone__c> milestoneList = new  Map<Id, Milestone__c>([SELECT Name FROM Milestone__c  
                                                                          WHERE Id IN :linkIds AND Milestone_Status__c = 'Completed' AND Installation_Site__c != null]);
        
        System.debug('Milestone =====> '+milestoneList);
        String  prfName=[SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId()].Name;
        for(ContentDocumentLink doclink : doclinks){
            if(milestoneList.get(doclink.LinkedEntityId) != null && prfName!='System Administrator'){ //&& prfName!='System Administrator' <== To test for System Administrator profile.
                System.debug('This is called');
                SObject  actualRecord = doclink;
                actualRecord.addError('Cannot delete this file since the milestone ' + milestoneList.get(doclink.LinkedEntityId).Name + ' has been completed already.');
                
            }
        }
    }

}