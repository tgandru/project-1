@isTest
public class ContentDocumentLinkTriggerHandler_Test {
	@isTest(seeAllData=true)
    static void triggersTest() {
        Milestone__c ms = new Milestone__c();
        insert ms;
        
        ms = [SELECT ID FROM Milestone__c WHERE Id =: ms.Id];
        
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        System.debug('documents =====> ' + documents);
        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.LinkedEntityId = ms.id;
        cdl.ContentDocumentId = documents[0].id;
        cdl.ShareType = 'V';
      	insert cdl;
        
        cdl = [SELECT ID FROM ContentDocumentLink WHERE Id =: cdl.Id];
        delete cdl;
    }
}