public class ContentDocumentTriggerHandler {
     public static void updateBeforeContentDocument(Map<Id,ContentDocument> newContentDocumentMap){
        System.debug('newContentDocumentMap =========> ' + newContentDocumentMap.values());
        Set<Id> oldIds = new Set<Id>();
        oldIds = newContentDocumentMap.keySet();
        System.debug('oldIds =========> '+oldIds);
        List<Id> linkIds = new List<Id>();
        List<ContentDocumentLink> doclinks = [SELECT LinkedEntityId, ContentDocumentId FROM ContentDocumentLink 
                                              WHERE ContentDocumentId IN: oldIds ];
        System.debug('docLinks =======> ' + doclinks);
        for(ContentDocumentLink doclink : doclinks){
            linkIds.add(doclink.LinkedEntityId); 
        }
        
        Map<Id, Milestone__c> milestoneList = new  Map<Id, Milestone__c>([SELECT Name FROM Milestone__c  
                                                                          WHERE Id IN :linkIds AND Milestone_Status__c = 'Completed' AND Installation_Site__c != null]);
        System.debug('Milestone =====> '+milestoneList);
        String  prfName=[SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId()].Name;
        for(ContentDocumentLink doclink : doclinks){
            if(milestoneList.get(doclink.LinkedEntityId) != null && prfName!='System Administrator'){ //&& prfName!='System Administrator' <== To test for System Administrator profile.
                SObject  actualRecord = newContentDocumentMap.get(doclink.ContentDocumentId);
                actualRecord.addError('Cannot update this file since the milestone ' + milestoneList.get(doclink.LinkedEntityId).Name + ' has been completed already.');
            }
        }
    }
    
    public static void deleteBeforeContentDocument(Map<Id,ContentDocument> oldContentDocumentsMap) {
        
        System.debug('This ContentDocumentTriggerHandler is called');
        System.debug('oldContentDocumentsMap =========> ' + oldContentDocumentsMap.values());
        Set<Id> oldIds = new Set<Id>();
        oldIds = oldContentDocumentsMap.keySet();
        System.debug('oldIds =========> '+oldIds);
        List<Id> linkIds = new List<Id>();
        List<ContentDocumentLink> doclinks = [SELECT LinkedEntityId, ContentDocumentId FROM ContentDocumentLink 
                                              WHERE ContentDocumentId IN: oldIds ];
        System.debug('docLinks =======> ' + doclinks);
        for(ContentDocumentLink doclink : doclinks){
            linkIds.add(doclink.LinkedEntityId); 
        }
        
        Map<Id, Milestone__c> milestoneList = new  Map<Id, Milestone__c>([SELECT Name FROM Milestone__c  
                                                                          WHERE Id IN :linkIds AND Milestone_Status__c = 'Completed' AND Installation_Site__c != null]);
        System.debug('Milestone =====> '+milestoneList);
        String  prfName=[SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId()].Name;
        for(ContentDocumentLink doclink : doclinks){
            if(milestoneList.get(doclink.LinkedEntityId) != null && prfName!='System Administrator' ){ //&& prfName!='System Administrator' <== To test for System Administrator profile.
                System.debug('This is called');
                SObject  actualRecord = oldContentDocumentsMap.get(doclink.ContentDocumentId);
                actualRecord.addError('Cannot delete this file since the milestone ' + milestoneList.get(doclink.LinkedEntityId).Name + ' has been completed already.');
                
            }
        }
        
    }
}