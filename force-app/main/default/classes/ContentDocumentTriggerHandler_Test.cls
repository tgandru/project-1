@isTest
public class ContentDocumentTriggerHandler_Test {
	@isTest(seeAllData=true)
    static void triggersTest() {
        Milestone__c ms = new Milestone__c();
        insert ms;
        
        ms = [SELECT ID FROM Milestone__c WHERE Id =: ms.Id];
        
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        System.debug('documents =====> ' + documents);
        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.LinkedEntityId = ms.id;
        cdl.ContentDocumentId = documents[0].id;
        cdl.ShareType = 'V';
      	insert cdl;
        
        ContentDocument cd = [SELECT ID FROM ContentDocument  WHERE Id =: cdl.ContentDocumentId];
        cd.Description = 'update test';
        update cd;
        
		cd = [SELECT ID FROM ContentDocument  WHERE Id =: cdl.ContentDocumentId];        
        delete cd;
    }
}