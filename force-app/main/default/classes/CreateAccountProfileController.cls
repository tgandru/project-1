public class CreateAccountProfileController {
    //reference the api name of the flow record
    Public Flow.Interview.Create_Account_Profile myAutoFlow { get; set; }
    Public CreateAccountProfileController() {
        displayDetail = FALSE;
    }
    public boolean displayDetail {get;set;}
    
    public void DisplayDetailBlock(){
        if(!getSalesforceOneCheck()) displayDetail = TRUE;
    }
    
    public String getmyID() {

        if (myAutoFlow==null)
            return '';

        else 
        //Put flow variable that represents the id of newly created record
            return myAutoFlow.vAccountProfileId;
        }


    public PageReference getNextPage(){
    PageReference p = new PageReference('/' + getmyID() );
    p.setRedirect(true);
    return p;
    }
    
    public static Boolean getSalesforceOneCheck(){
         
           String retUrl = ApexPages.currentPage().getParameters().get('retURL');
         
           return String.isNotBlank(ApexPages.currentPage().getParameters().get('sfdcIFrameHost')) ||
           String.isNotBlank(ApexPages.currentPage().getParameters().get('sfdcIFrameOrigin')) ||
           ApexPages.currentPage().getParameters().get('isdtp') == 'p1' ||
           (String.isNotBlank(retUrl) && retURL.contains('projectone'));
         
    }
}