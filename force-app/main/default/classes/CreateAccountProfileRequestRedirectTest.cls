@isTest
private class CreateAccountProfileRequestRedirectTest {
    
    @isTest static void test_method_one() {
        // Implement test code
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');

        Account account = TestDataUtility.createAccount('Main Account');
        account.RecordTypeId = directRT;
        account.Type= 'Distributor';
        account.NumberOfEmployees=100;
        account.AnnualRevenue=50000.15;
        insert account;

        Account_Profile__c profileAcc = TestDataUtility.createAccountProfile(account);
        insert profileAcc;

        Test.setCurrentPageReference(new PageReference('Page.CreateAccountProfileRequest')); 
        System.currentPageReference().getParameters().put('id', profileAcc.Id);
        System.currentPageReference().getParameters().put('RetURL', '/home/home.jsp');
        CreateAccountProfileRequestRedirect profileController = new CreateAccountProfileRequestRedirect(new ApexPages.StandardController(profileAcc));
        profileController.redirect();
    }
    
    
    
}