public with sharing class CreateCreditMemoExt {
    //reference the api name of the flow record
    Public Flow.Interview.Create_Credit_Memo_And_Line_Items myAutoFlow { get; set; }
    Public CreateCreditMemoExt(ApexPages.StandardController controller) {}



  public String getmyID() {

    if (myAutoFlow==null)
       return '';

    else 
        //Put flow variable that represents the id of newly created record
      return myAutoFlow.varCreditMemoID;
    }


  public PageReference getNextPage(){
  PageReference p = new PageReference('/' + getmyID() );
  p.setRedirect(true);
  return p;
  }

}