public with sharing class CreateCreditMemoRedirect {

    public string RecordID{get;set;}
    public string URL{get;set;}

    // we are extending the Order controller, so we query to get the parent record id
    public CreateCreditMemoRedirect(ApexPages.StandardController controller) {
        URL=ApexPages.currentPage().getParameters().get('retURL');       
        URL = URL.removeStart('/');                    
        RecordID = URL;   
     }
    
    // then we redirect to our desired page with the Record Id in the URL
    public pageReference redirect(){
        PageReference pg=new Pagereference('/apex/CreateCreditMemo?id=' + RecordID);
        pg.setRedirect(true);
        return pg;
    }
}