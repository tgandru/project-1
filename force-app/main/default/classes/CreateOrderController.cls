/*
Controller for OrderRequestComponent

*/
public with sharing class CreateOrderController {
    //reference the api name of the flow record
    public Flow.Interview.Create_Order_and_Line_Items myAutoFlow { get; set; }
    public boolean displayDetail {get;set;}
    public String redirectId;
    
    public boolean isPOCRecordtypeAvilable {get; set;} // Sets to true, if logged in user having access to POC record type,e lse false
    
    Public CreateOrderController() {
        displayDetail = FALSE;
        isPOCRecordtypeAvilable = false;
        profile profileObj = [select id, Name from profile where id=:userInfo.getProfileId() limit 1];
        
        if(profileObj.Name !='B2B ISR Sales User - Mobile' && profileObj.Name !='B2B CBS Sales User' && profileObj.Name !='B2B Sales Leader - Mobile' && profileObj.Name !='B2B Sales User - Mobile'&& profileObj.Name !='B2B ISR Sales User - IT' && profileObj.Name !='B2B Sales Leader - IT' && profileObj.Name !='B2B Sales User - IT' ){
            isPOCRecordtypeAvilable = true;
        }
        
    }
    public void DisplayDetailBlock(){
        if(!getSalesforceOneCheck()) displayDetail = TRUE;
           
    }
    
    public String getmyID() {
        if (myAutoFlow==null)
            return '';
        else 
        //Put flow variable that represents the id of newly created record
        return myAutoFlow.varOrderId;
    }

    
    public String getredirectId(){
        if(redirectid==null && myAutoFlow != null)
             redirectid =  myAutoFlow.varOrderId;
        return redirectid;
    }
    
    public PageReference NextPage{
         get {
         if(myAutoFlow != null){
             redirectid =  myAutoFlow.varOrderId;
         }
         system.debug('Redirect id.....'+redirectid);
         if(redirectid != null){
             String accountKeyPrefix = Account.sObjectType.getDescribe().getKeyPrefix();
             String demoKeyPrefix = Order.sObjectType.getDescribe().getKeyPrefix();
             String opportunityKeyPrefix = Opportunity.sObjectType.getDescribe().getKeyPrefix();
             
             String redirectURL = '';
             if(redirectid.startsWith(accountKeyPrefix)){
                redirectURL = redirectid;
             } else if (redirectid.startsWith(opportunityKeyPrefix)) {
                redirectURL = redirectid;
             } else if (redirectid.startsWith(demoKeyPrefix)) {
                if (!getSalesforceOneCheck()) {
                redirectURL = 'apex/DemoProductEntry?id='+redirectid;
                } else {
                    redirectURL = redirectid;
                }
             }
             
             PageReference pg = new PageReference('/'+redirectURL);
             system.debug('PageReference...'+pg);
             
             pg.setRedirect(true);
             return pg;
         }
         else
             return null;
         }
         set { NextPage = value; }
    
    }
    
    public static Boolean getSalesforceOneCheck(){
        
        String retUrl = ApexPages.currentPage().getParameters().get('retURL');
        return String.isNotBlank(ApexPages.currentPage().getParameters().get('sfdcIFrameHost')) ||
        String.isNotBlank(ApexPages.currentPage().getParameters().get('sfdcIFrameOrigin')) ||
        ApexPages.currentPage().getParameters().get('isdtp') == 'p1' ||
        (String.isNotBlank(retUrl) && retURL.contains('projectone'));
    }  
    
    
}