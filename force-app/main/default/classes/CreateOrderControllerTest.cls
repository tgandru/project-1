@isTest
private class CreateOrderControllerTest {
	static Account testMainAccount;
	static Opportunity testOpportunity;
	static Order orderRecord1;
	
	private static void setup() {
        //Jagan: Test data for fiscalCalendar__c
        Test.loadData(fiscalCalendar__c.sObjectType, 'FiscalCalendarTestData');
        
        //Channelinsight configuration
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();

        Zipcode_Lookup__c zip=TestDataUtility.createZipcode();
        insert zip;
        
        testMainAccount = TestDataUtility.createAccount('TestAccount');
        insert testMainAccount;
        
        //Creating custom Pricebook
        Pricebook2 pb = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(4)); 
        insert pb;
        
        testOpportunity = TestDataUtility.createOppty('Open Oppty',testMainAccount, pb, 'RFP', 'IT', 'FAX/MFP', 'Identified');
        insert testOpportunity;
        
        Id demoAcctRtId = TestDataUtility.retrieveRecordTypeId('ADL','Order');
        
        Contact contactRecord = TestDataUtility.createContact(testMainAccount);
        insert contactRecord;
        
        orderRecord1 = TestDataUtility.createOrder(testMainAccount, testOpportunity);
	        orderRecord1.RecordTypeId = demoAcctRtId;
	        orderRecord1.Pricebook2 = pb;
	        orderRecord1.Pricebook2Id = pb.Id;
	        orderRecord1.Product_Group__c = 'ACCESSARY; SMART_PHONE';
	        orderRecord1.CustomerAuthorizedById = contactRecord.Id;
	        orderRecord1.RequestedShipmentDate__c = Date.today();
	        OrderRecord1.EffectiveDate = Date.today();
	        orderRecord1.BusinessReason__c = 'test OrderRecord1';
	        orderRecord1.Type = 'Seed Unit';
	    //insert orderRecord1; 
	}
    
    @isTest static void test_method_one() {
    	setup();
    	
        CreateOrderController orderController = new CreateOrderController();       
        //orderController.myAutoFlow = new Flow.Interview.Create_Order_and_Line_Items (new Map<String, Object>());
        //orderController.myAutoFlow.start();
        orderController.getredirectId();
        orderController.getMyId();
        
        orderController.redirectId = testMainAccount.id;
        PageReference nextp = orderController.NextPage;
        CreateOrderController.getSalesforceOneCheck();
        System.debug(' acct pr '+nextp);
        System.assertNotEquals(null, nextp);
        orderController.redirectId = testOpportunity.id;
        PageReference nextpOpp = orderController.NextPage;
        CreateOrderController.getSalesforceOneCheck();
        System.debug(' opp pr '+nextpOpp); 
        System.assertNotEquals(null, nextpOpp);
        orderController.redirectId = orderRecord1.id;
        PageReference nextpOrd = orderController.NextPage;
        CreateOrderController.getSalesforceOneCheck();
        System.debug(' order pr '+nextpOrd);
        orderController.DisplayDetailBlock();
        
    }
   
    
}