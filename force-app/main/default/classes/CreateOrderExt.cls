public class CreateOrderExt {
    //reference the api name of the flow record
    Public Flow.Interview.Create_Order_and_Line_Items myAutoFlow { get; set; }
    Public CreateOrderExt(ApexPages.StandardController controller) {}



	public String getmyID() {

		if (myAutoFlow==null)
 			return '';

		else 
        //Put flow variable that represents the id of newly created record
			return myAutoFlow.varOrderId;
		}


	public PageReference getNextPage(){
	PageReference p = new PageReference('/' + getmyID() );
	p.setRedirect(true);
	return p;
	}

}