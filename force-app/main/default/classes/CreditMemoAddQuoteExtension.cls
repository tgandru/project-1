/*
 * Credit Memo Add Quote Controller includes search functionality based on SPID
 * Author: Paul Mraz
 * Edited - 25APRIL2016: Eric Vennaro
 * Edited - 09May2016: Kavitha Reddy - Added Division logic on Search Results
**/

public class CreditMemoAddQuoteExtension {
    public Credit_Memo__c creditMemo {get;set;}
    public String creditMemoId {get;set;}
    public String searchString {get;set;}
    public List<Credit_Memo_Quote__c> shoppingCart {get;set;}
    public List<QuoteWrapper> availableQuotesWrapper {get;set;}
    public String quoteToUnselect {get; set;}
    public Boolean hasLineItems {get;set;}
    public List<CreditMemoQuoteWrapper> initialCart{get;set;}
    public Boolean isCreditMemoProcessed {get; set;}
    private Set<Id> initialCartIds {get;set;}
    public String creditMemoDivision {get;set;}
    public String unMatchingDivision {get;set;}
    public Boolean overLimit {get;set;}
    

    public CreditMemoAddQuoteExtension(ApexPages.StandardController controller) {
        availableQuotesWrapper = new List<QuoteWrapper>();        
        initialCartIds = new Set<Id>();
        initialCart = new List<CreditMemoQuoteWrapper>();
        creditMemo = getCreditMemoForId(controller.getRecord().Id);
        unMatchingDivision = '';
        creditMemoDivision = creditMemo.Division__c;
        setInitialCart();
        setCreditMemoStatus();
    }
                
    public void updateAvailableList() {    
        List<String> spas = getSPASFromSearchString();
        List<Quote> availableQuotes = new List<Quote>();
        if(spas.size() > 0){    
            if(availableQuotesWrapper != null) {
                availableQuotesWrapper.clear(); 
            }
            QuoteWrapperDAO wrapDAO = new QuoteWrapperDAO(creditMemoDivision);            
            QuoteWrapperFacade quoteFacade = new QuoteWrapperFacade(wrapDAO);
            try {
                availableQuotesWrapper = quoteFacade.getAvailableQuotes(initialCart, spas);
            }catch(QuoteDAOException e){
                System.debug('Exception querying for rows' + e.getMessage());
            }catch(Exception e) {
                System.debug('Error occured processing search query' + e.getMessage());
            }
            unMatchingDivision = wrapDAO.DivisionInfoMsg;
        }       
        if(String.isNotBlank(unMatchingDivision))
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,unMatchingDivision));
    }

    public void addCheckedItemstoCreditMemo(){
        if(!isCreditMemoProcessed){
            for(QuoteWrapper quoteWrapper : availableQuotesWrapper){                                                         
                if(quoteWrapper.isSelected){
                    initialCartIds.add(quoteWrapper.quote.Id);
                    CreditMemoQuoteWrapper creditMemoQuoteWrapper = new CreditMemoQuoteWrapper(
                        new Credit_Memo_Quote__c(
                            Quote__c = quoteWrapper.quote.Id, 
                            Credit_Memo__c = creditMemo.id
                        ), quoteWrapper);
                    initialCart.add(creditMemoQuoteWrapper);
                }
            }
            availableQuotesWrapper.clear();
            if(searchString != null && searchString != ''){
                updateAvailableList();  
            }
            unselectAvailableItems();   
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'This Credit Memo is processed and can no longer be modified.'));
        }
    }                  
    
    public void removeFromCreditMemo(){
        if(!isCreditMemoProcessed){
            List<CreditMemoQuoteWrapper> updatedWrapper = new List<CreditMemoQuoteWrapper>();
            if(initialCartIds.contains(Id.valueOf(quoteToUnselect))) {
                initialCartIds.remove(Id.valueOf(quoteToUnselect));
                for(CreditMemoQuoteWrapper quoteWrapper : initialCart) {
                    if(String.valueOf(quoteWrapper.quoteId) != quoteToUnselect) {
                        updatedWrapper.add(quoteWrapper);
                    }
                }
                initialCart = updatedWrapper;
            }
            if(searchString != null && searchString != ''){
                updateAvailableList();  
            }
            unselectAvailableItems();  
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Record Removed.'));
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'This Credit Memo is processed and can no longer be modified.'));
        }
    }

    public PageReference onSave(){
        if(!isCreditMemoProcessed){
            saveRecords();
            return new PageReference('/' + creditMemo.Id);
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'This Credit Memo is processed and can no longer be modified.'));
            return null;
        }
    }
    
    public PageReference onSaveAndContinue() {
        if(!isCreditMemoProcessed){
            saveRecords();
            return new PageReference('/apex/CreditMemo?id=' + creditMemo.Id); 
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'This Credit Memo is processed and can no longer be modified.'));
            return null;
        }     
    }
    
    public PageReference onCancel(){
        return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
    }

    private void saveRecords() {
        List<Credit_Memo_Quote__c> listToUpsert = new List<Credit_Memo_Quote__c>();
        for(CreditMemoQuoteWrapper creditMemoQuoteWrapper : initialCart) {
            initialCartIds.remove(creditMemoQuoteWrapper.quoteId);
            listToUpsert.add(creditMemoQuoteWrapper.creditMemoQuote);
        }
        try {
            upsert listToUpsert;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Records updated.'));
        } catch (DmlException e) {
            System.debug('Caught DmlException: ' + e.getMessage());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Error occured updating credit memo.'));
        } catch (Exception e) {
            System.debug('Caught unknown exception ' + e.getMessage());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'This Credit Memo is processed and can no longer be modified.'));    
        }
    }

    private void unselectAvailableItems(){
        for(QuoteWrapper quoteWrap : availableQuotesWrapper){
            quoteWrap.isSelected = false;
        }   
    }

    private List<String> getSPASFromSearchString() {
        return searchString.split(',');
    }

    private Credit_Memo__c getCreditMemoForId(Id id) {
        return [SELECT Id, 
                       Status__c, 
                       Is_Approved__c, 
                       Direct_Partner__c, 
                       Comments__c, 
                	   Division__c,
                       Integration_Status__c
                FROM Credit_Memo__c 
                WHERE Id = :id 
                LIMIT 1];
    }

    private void setInitialCart() {
        List<Credit_Memo_Quote__c> initialList =  [SELECT Id, 
                                                           Credit_Memo__c, 
                                                           Customer__c, 
                                                           Quote__c, Quote__r.Name, Quote__r.Id, 
                                                           Opportunity_Number__c, 
                                                           SP_Number__c, 
                                                           Valid_From__c, 
                                                           Valid_To__c
                                                    FROM Credit_Memo_Quote__c 
                                                    WHERE Credit_Memo__c = :creditMemo.Id];
        for(Credit_Memo_Quote__c entry : initialList) {
            initialCart.add(new CreditMemoQuoteWrapper(entry, entry.Quote__r.Id));
        }
    }

    private void setCreditMemoStatus() {
        if(creditMemo.Status__c == 'Processed'){
            isCreditMemoProcessed = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'This Credit Memo has been processed and can no longer be modified.'));
        } else {
            isCreditMemoProcessed = false;
        }
    }
}