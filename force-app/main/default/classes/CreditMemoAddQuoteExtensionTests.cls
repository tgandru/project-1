@isTest
private class CreditMemoAddQuoteExtensionTests{
    private static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
    @testSetup static void setup() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Account account = TestDataUtility.createAccount('Marshal Mathers');
        insert account;
        Account partnerAcc = TestDataUtility.createAccount('Distributor account');
        partnerAcc.RecordTypeId = directRT;
        insert partnerAcc;
        PriceBook2 priceBook = TestDataUtility.createPriceBook('Shady Records');
        insert pricebook;
        Opportunity opportunity = TestDataUtility.createOppty('New', account, pricebook, 'Managed', 'IT', 'product group',
                                    'Identified');
        insert opportunity;
        Quote quote = TestDataUtility.createQuote('Walkman', opportunity, priceBook);
        quote.External_SPID__c = 'SP-00000009-0';
        insert quote;
        Quote quote2 = TestDataUtility.createQuote('Ipod', opportunity, pricebook);
        quote2.External_SPID__c = 'SP-00000010-0';
        insert quote2;
        Credit_Memo__c  creditMemo = TestDataUtility.createCreditMemo();
        creditMemo.InvoiceNumber__c = TestDataUtility.generateRandomString(6);
        creditMemo.Direct_Partner__c = partnerAcc.id;
        insert creditMemo;
        
    }

    @isTest static void testPageRedirectOnCancel() {
        Credit_Memo__c  creditMemo = [SELECT Id FROM Credit_Memo__c  LIMIT 1];
        ApexPages.StandardController sc = new ApexPages.StandardController(creditMemo);
        CreditMemoAddQuoteExtension creditMemoController = new CreditMemoAddQuoteExtension(sc);
        PageReference pageRef = creditMemoController.onCancel();
        System.assertNotEquals(null, pageRef);
        Map<String,String> pageParameters = pageRef.getParameters();
        System.assertEquals(0, pageParameters.values().size());
    }

    @isTest static void testUpdateAvailableList() {
        Credit_Memo__c  creditMemo = [SELECT Id FROM Credit_Memo__c  LIMIT 1];
        List<Quote> quotes = [SELECT Id, External_SPID__c, Status FROM Quote];
        String searchString = quotes.get(0).External_SPID__c  + ', ' + quotes.get(1).External_SPID__c; 
        ApexPages.StandardController sc = new ApexPages.StandardController(creditMemo);
        CreditMemoAddQuoteExtension creditMemoController = new CreditMemoAddQuoteExtension(sc);
        creditMemoController.searchString = searchString;
        creditMemoController.updateAvailableList();
        System.assertEquals(0, creditMemoController.availableQuotesWrapper.size());
        quotes.get(0).Status = 'Approved';
        update quotes;
        creditMemoController.updateAvailableList();
        System.assertEquals(1, creditMemoController.availableQuotesWrapper.size());
    }
    
    @isTest static void testAddingCheckedItemsToCreditMemo() {
        Credit_Memo__c  creditMemo = [SELECT Id FROM Credit_Memo__c  LIMIT 1];
        List<Quote> quotes = [SELECT Id, External_SPID__c, Status FROM Quote];
        String searchString = quotes.get(0).External_SPID__c  + ', ' + quotes.get(1).External_SPID__c; 
        ApexPages.StandardController sc = new ApexPages.StandardController(creditMemo);
        CreditMemoAddQuoteExtension creditMemoController = new CreditMemoAddQuoteExtension(sc);
        creditMemoController.searchString = searchString;
        creditMemoController.updateAvailableList();
        quotes.get(0).Status = 'Approved';
        update quotes;
        creditMemoController.updateAvailableList();
        creditMemoController.availableQuotesWrapper.get(0).isSelected = true;
        creditMemoController.addCheckedItemstoCreditMemo();
        creditMemoController.onSave();
        creditMemoController.onSaveAndContinue();
    }
    
    @isTest static void testRemovingItemsFromCreditMemo() {
        Credit_Memo__c  creditMemo = [SELECT Id FROM Credit_Memo__c  LIMIT 1];
        List<Quote> quotes = [SELECT Id, External_SPID__c, Status FROM Quote];
        String searchString = quotes.get(0).External_SPID__c  + ', ' + quotes.get(1).External_SPID__c; 
        ApexPages.StandardController sc = new ApexPages.StandardController(creditMemo);
        CreditMemoAddQuoteExtension creditMemoController = new CreditMemoAddQuoteExtension(sc);
        creditMemoController.searchString = searchString;
        creditMemoController.updateAvailableList();
        quotes.get(0).Status = 'Approved';
        update quotes;
        creditMemoController.updateAvailableList();
        creditMemoController.availableQuotesWrapper.get(0).isSelected = true;
        String quoteId = creditMemoController.availableQuotesWrapper.get(0).quote.Id;
        creditMemoController.addCheckedItemstoCreditMemo();
        creditMemoController.quoteToUnselect = quoteId;
        // This method is not being used on the VF page
        creditMemoController.removeFromCreditMemo();
        // Can't assert on the initial Cart Id Set since it is defined as private in the Extension
        //System.assertEquals(0, creditMemoController.initialCartIds.size());
    }
    
    @isTest static void testQuoteWrapperDAO() {
        Credit_Memo__c  creditMemo = [SELECT Id, Division__c FROM Credit_Memo__c  LIMIT 1];
        
        String queryString = 'SELECT Id, Quote.Account.Name, External_SPID__c, Quote_Number__c, ExpirationDate, Valid_From_Date__c,Product_Division__c,ProductGroupMSP__c,Division__c, Name ' + 
                             'FROM Quote ' + 
                             'WHERE (Status = '+'\'Approved\' OR Status = '+'\'Presented\') ' +
                             'ORDER BY External_SPID__c LIMIT 100';
        QuoteWrapperDAO quoteDAO = new QuoteWrapperDAO('Test 1234');
        quoteDAO.DivisionInfoMsg = '';
        quoteDAO.getAvailableQuotes(queryString);
    } 
    
    @isTest static void testsetInitialCart() {
        Credit_Memo__c  creditMemo = [SELECT Id, Division__c FROM Credit_Memo__c  LIMIT 1];
        Quote quote = [select id , status from Quote limit 1];
        quote.status = 'Approved';
        update quote;
        Credit_Memo_Quote__c cmq = new Credit_Memo_Quote__c(Quote__c =quote.id, Credit_Memo__c = creditMemo.id, Name = 'Test' );
        insert cmq;
        ApexPages.StandardController sc = new ApexPages.StandardController(creditMemo);

        CreditMemoAddQuoteExtension obj1 = new CreditMemoAddQuoteExtension(sc);
    }
}