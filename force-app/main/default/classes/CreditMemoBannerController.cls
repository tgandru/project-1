/** 
.
**/

public with sharing class CreditMemoBannerController{
    
    public boolean showlateMesg {get;set;}
    public String integrationStatus {get;set;}
    public String CredMemoStatus {get;set;}
    public String creditmemoid {get; set;}
    
    public CreditMemoBannerController(ApexPages.StandardController controller) {
        showlateMesg = FALSE; 
        Credit_Memo__c q=(Credit_Memo__c)controller.getRecord();
        q=[select id,Integration_Status__c,Status__c from Credit_Memo__c where id=:q.id];
        integrationStatus = q.integration_status__c;
        CredMemoStatus  = q.Status__c;
        creditmemoid = q.id;
        if(integrationstatus !=null)
            integrationstatus = integrationstatus.tolowercase();
        if(CredMemoStatus !=null)
            CredMemoStatus = CredMemoStatus.tolowercase();    
    }
    public pageReference switchMessage(){
        system.debug('inside switch message...');
        showlateMesg = TRUE;
        return null;
    }

}