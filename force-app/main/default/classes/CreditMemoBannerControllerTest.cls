/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CreditMemoBannerControllerTest {
	
	static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');

    @testSetup static void setup() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Account account = TestDataUtility.createAccount('Marshal Mathers');
        insert account;
        Account partnerAcc = TestDataUtility.createAccount('Distributor account');
        partnerAcc.RecordTypeId = directRT;
        insert partnerAcc;
         //Creating custom Pricebook
        PriceBook2 pb = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(4)); 
        insert pb;
        Opportunity opportunity = TestDataUtility.createOppty('New', account, pb, 'Managed', 'IT', 'product group',
                                    'Identified');
        insert opportunity;
        Product2 product = TestDataUtility.createProduct2('SL-SCF5300/SEG', 'Standalone Printer', 'Printer[C2]', 'H/W', 'FAX/MFP');
        insert product;
        Id pricebookId = Test.getStandardPricebookId();
        //PricebookEntry pricebookEntry1 = TestDataUtility.createPriceBookEntry(product.Id, pricebookId);
        //insert pricebookEntry1;
        ///^^ del bc System.DmlException: Insert failed. First exception on row 0; first error: DUPLICATE_VALUE, This price definition already exists in this price book: []
        PricebookEntry pricebookEntry = TestDataUtility.createPriceBookEntry(product.Id, pb.Id);
        insert pricebookEntry;

        List<OpportunityLineItem> opportunityLineItems = new List<OpportunityLineItem>();
        OpportunityLineItem op1 = TestDataUtility.createOpptyLineItem(product, pricebookEntry, opportunity);
        op1.Quantity = 100;
        op1.TotalPrice = 1000;
        op1.Requested_Price__c = 100;
        op1.Claimed_Quantity__c = 45;     
        opportunityLineItems.add(op1);
        insert opportunityLineItems;
        System.debug('Insert OLI success');

        Quote quot = TestDataUtility.createQuote('Test Quote', opportunity, pb);
        quot.ROI_Requestor_Id__c=UserInfo.getUserId();
        insert quot;
        
        List<QuoteLineItem> qlis = new List<QuoteLineItem>();
        QuoteLineItem standardQLI = TestDataUtility.createQuoteLineItem(quot.Id, product, pricebookEntry, op1);
        standardQLI.Quantity = 25;
        standardQLI.UnitPrice = 100;
        standardQLI.Requested_Price__c=100;
        standardQLI.OLIID__c = op1.id;
        qlis.add(standardQLI);
        insert qlis;


        Credit_Memo__c cm = TestDataUtility.createCreditMemo();
        //cm.Credit_Memo_Number__c = TestDataUtility.generateRandomString(5);
        cm.InvoiceNumber__c = TestDataUtility.generateRandomString(6);
        cm.Direct_Partner__c = partnerAcc.id;
        insert cm;
      
        List<Credit_Memo_Product__c> creditMemoProducts = new List<Credit_Memo_Product__c>();
        Credit_Memo_Product__c cmProd = TestDataUtility.createCreditMemoProduct(cm, op1);
        cmProd.Request_Quantity__c = 45;
        creditMemoProducts.add(cmProd);
        insert creditMemoProducts;

    }
    
    @isTest static void testCreditMemoBanner() {
		
		Test.startTest();
		Credit_Memo__c creditMemo = [select Id, status__c from Credit_Memo__c Limit 1];
		Credit_Memo_Product__c product = [select Id, Request_Quantity__c from Credit_Memo_Product__c Limit 1];
		ApexPages.StandardController sc = new ApexPages.StandardController(creditMemo);
		CreditMemoBannerController controller = new CreditMemoBannerController(sc);
		controller.switchMessage();
		Test.stopTest();
    }
		
}