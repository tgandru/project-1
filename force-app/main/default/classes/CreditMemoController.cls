/*
 * Credit Memo Controller for CreditMemo.page.  Updates a credit memo's line items.
 * Author: Eric Vennaro
**/

public class CreditMemoController {
    public CreditMemo creditMemo {get;set;}
    public Map<Id, CreditMemoProduct> creditMemoProducts {get;set;}
    public String selectedProductId {get;set;}
    //public List<Credit_Memo_Product__c> creditMemoProd {get;set;}
    
    //Added by Jagan
    public boolean showlateMesg {get;set;}
    public String integrationStatus {get;set;}
    public String CredMemoStatus {get;set;}
    
    
    public CreditMemoController(ApexPages.StandardController controller) {
        creditMemo = new CreditMemo(getCreditMemoFor(controller));
        creditMemoProducts = new Map<Id, CreditMemoProduct>();
        for(Credit_Memo_Product__c cmp : getCreditMemoProducts()) {
            creditMemoProducts.put(cmp.Id, new CreditMemoProduct(cmp));
        }
        //Added by Jagan
        showlateMesg = FALSE; 
        Credit_Memo__c q=(Credit_Memo__c)controller.getRecord();
        q=[select id,Integration_Status__c,Status__c from Credit_Memo__c where id=:q.id];
        
        integrationStatus = q.integration_status__c;
        CredMemoStatus  = q.Status__c;
        if(integrationstatus !=null)
            integrationstatus = integrationstatus.tolowercase();
        if(CredMemoStatus !=null)
            CredMemoStatus = CredMemoStatus.tolowercase();   
    }



    public void updatePricing(){ 
        /*Message_Queue__c mq = new Message_Queue__c(Integration_Flow_Type__c = 'Flow-008', Retry_Counter__c = 0, Identification_Text__c = creditMemo.id);
        ApexPages.Message message = new ApexPages.Message(ApexPages.Severity.INFO, 'Updating Pricing');
        ApexPages.addMessage(message);*/
        //get list of quotes included in credit memo products
        // showlateMesg = TRUE;

        unwrapAndSaveCMP(creditMemoProducts.values());

        if([select id from message_queue__c where Integration_Flow_Type__c='invoicing 8' and status__c='not started' and Identification_Text__c=:creditMemo.Id].isEmpty()){
            String cmId='';
            for(Credit_Memo_Product__c cmp:getCreditMemoProducts()){
                if(cmId==''){
                    cmId=cmp.Credit_Memo__c;
                }
            }
            
            if(cmId!=''){
                Credit_Memo__c cm=[select id, Integration_Status__c from Credit_Memo__c where id=:cmId];
                cm.Integration_Status__c='Pending';
                update cm;
                                if(System.IsBatch() == false && System.isFuture() == false){ 
                                        callOutFlow008(cm.Id);
                                } else {
                        doRunFlow008(cm.Id, null);
                                }
            }
        }else{
            //pop back error to user @TODO
        }
            
    }

    public void deleteProduct() {
        if(selectedProductId != null) {
            try {
                Delete [SELECT Id 
                        FROM Credit_Memo_Product__c 
                        WHERE id = :selectedProductId];
                creditMemoProducts.remove(Id.valueOf(selectedProductId));
            } catch(DMLException e) {
                System.debug('DMLException occured when deleting record with id: ' + selectedProductId);
                System.debug('DMLException occured: ' + e.getMessage());
            } catch(Exception e) {
                System.debug('Unknown exception occured deleting record with id: ' + selectedProductId);
                System.debug('Unknown exception occured ' + e.getMessage());
                displayErrorMessage('Could not delete record, please contact admin if the problem persists');
            }
        } else {
            displayErrorMessage('Could not delete record, please contact admin if the problem persists');
            System.debug('Invalid product id was detected');
        }
    }
    
    public static void displayErrorMessage(String messageText){
        ApexPages.Message message = new ApexPages.Message(ApexPages.Severity.ERROR, messageText);
        ApexPages.addMessage(message);
    }

    private Credit_Memo__c getCreditMemoFor(ApexPages.StandardController controller) {
        return [SELECT  Id, 
                        Name, 
                        Credit_Memo_Number__c, 
                        Direct_Partner__r.Name, 
                        Status__c, 
                        Comments__c, 
                        CreatedDate,
                        LastModifiedDate,
                        SAP_Request_Number__c, 
                        SAP_Billing_Number__c, 
                        SAP_Response__c, 
                        TotalCreditAmount__c,
                        Assignment_Number__c
                FROM Credit_Memo__c
                WHERE Id = :controller.getRecord().Id];
    }

    private List<Credit_Memo_Product__c> getCreditMemoProducts(){
        return [SELECT  Id, 
                        Name, 
                        Claimed_Quantity__c,
                        Unclaimed_Quantity__c,
                        Opportunity_Number__c,
                        Quote__r.Name,
                        SAP_Material_ID__c, 
                        Quote__c, 
                        Product_Name__c, 
                        Original_Quantity__c, 
                        Request_Quantity__c, 
                        Invoice_Price__c,
                        Approved_Price__c, 
                        Credit_Price__c, 
                        TotalClaimAmount__c,
                        Quote_Number__c, OLI_ID__c, Credit_Memo__c, QLI_ID__c
                FROM Credit_Memo_Product__c
                WHERE Credit_Memo__c = :creditMemo.id];
    }

    public static void fromBatchMinSched(message_queue__c mq){
        
        /*for(Credit_Memo_Product__c cmp:[select id, quote__c from Credit_Memo_Product__c WHERE Credit_Memo__c = :mq.Identification_Text__c]){
            quoteIds.add(cmp.Quote__c);
        }*/
                if(System.IsBatch() == false && System.isFuture() == false){ 
                        callOutFlow008(mq.Identification_Text__c);      
                } else {
                        doRunFlow008(mq.Identification_Text__c, mq);    
                }
    }

    /****************************************CALLOUT****************************************/

    @future(callout=true)
    public static void callOutFlow008(String cmId){
        doRunFlow008(cmId, null);
    }
    
    public static void doRunFlow008(String cmId, message_queue__c omq){

        integer errCount = 0;

        List<Credit_Memo_Product__c> cmpToUpdate=new List<Credit_Memo_Product__c>();
        List<Credit_Memo_Product__c> creditMemoProducts = new List<Credit_Memo_Product__c>();
        if(cmId != null){
            creditMemoProducts=[SELECT  Id, Name, 
                                        Claimed_Quantity__c,
                                        Unclaimed_Quantity__c,
                                        Opportunity_Number__c,
                                        Quote__r.Name,
                                        SAP_Material_ID__c, 
                                        Quote__c, 
                                        Product_Name__c, 
                                        Original_Quantity__c, 
                                        Request_Quantity__c, 
                                        Invoice_Price__c,
                                        Approved_Price__c, 
                                        Credit_Price__c, 
                                        TotalClaimAmount__c,
                                        Quote_Number__c, OLI_ID__c, Credit_Memo__c,
                                        Credit_Memo__r.Total_Quote_Number__c, QLI_ID__c,
                                        Credit_Memo__r.SoldToAccount__r.sales_organization__c, 
                                        Credit_Memo__r.SoldToAccount__r.Distribution_Channel_Id__c,
                                        Credit_Memo__r.SoldToAccount__r.Sales_Division_Id__c,
                                        Credit_Memo__r.SoldToAccount__r.Account__r.SAP_Company_Code__c,
                                        Credit_Memo__r.SoldToAccount__r.Plant__c
                                FROM Credit_Memo_Product__c
                                WHERE Credit_Memo__c=:cmId and Request_Quantity__c >0 and Request_Quantity__c !=null];
        }
        
        


        //message_queue__c omq=new message_queue__c();
        string layoutId = (Test.isRunningTest() || Integration_EndPoints__c.getInstance('Flow-008').layout_id__c==null )? 'asdf' : Integration_EndPoints__c.getInstance('Flow-008').layout_id__c;
        if(omq==null){
                omq=new message_queue__c();
            omq.retry_counter__c=0;
            omq.Integration_Flow_Type__c='invoicing 8';
            omq.Object_Name__c='Credit Memo'; 
            if(creditMemoProducts.size() > 0){
                omq.Identification_Text__c=creditMemoProducts[0].Credit_Memo__c;
            }
            
            omq.status__c='not started'; 
            omq.IFID__c=layoutId;
        }//else{
         //   omq.id=mqId;
         //}

         string endpoint=Test.isRunningTest() ? '' : Integration_EndPoints__c.getInstance('Flow-008').endPointURL__c;

        string response = '';

        //create body
        cihStub.UpdateInvoicePriceRequest request=new cihStub.UpdateInvoicePriceRequest();
        cihStub.InvoiceBodyRequest outerRecStub=new cihStub.InvoiceBodyRequest();

        List<cihStub.CreditMemoLineItemsRequest> creditMemoLines=new List<cihStub.CreditMemoLineItemsRequest>();
        Map<String,Credit_Memo_Product__c> itemNumberCMPMap=new Map<String,Credit_Memo_Product__c>();
        Integer count=1;

        for(Credit_Memo_Product__c cmp:creditMemoProducts){
            String c=string.valueOf(count).leftPad(6).replace(' ','0');
            itemNumberCMPMap.put(c,cmp);
            
            cihStub.CreditMemoLineItemsRequest innerRecStub=new cihStub.CreditMemoLineItemsRequest();

            innerRecStub.itemNumberSdDoc=c;
            innerRecStub.materialCode=cmp.SAP_Material_ID__c;
            innerRecStub.quantity=string.valueOf(1);
            innerRecStub.werks=cmp.Credit_Memo__r.SoldToAccount__r.Plant__c;

            creditMemoLines.add(innerRecStub);
            count++;
        }

        cihStub.msgHeadersRequest headers = new cihStub.msgHeadersRequest(layoutId);
        request.inputHeaders = headers;

        
        outerRecStub.distributionChannel=CreditMemoIntegration__c.getInstance('Distribution Channel Value').Distribution_Channel__c;
        if(creditMemoProducts.size() > 0){
            outerRecStub.salesOrg=creditMemoProducts[0].Credit_Memo__r.SoldToAccount__r.sales_organization__c;
            outerRecStub.division=creditMemoProducts[0].Credit_Memo__r.SoldToAccount__r.Sales_Division_Id__c;
            outerRecStub.soldToParty=creditMemoProducts[0].Credit_Memo__r.SoldToAccount__r.Account__r.SAP_Company_Code__c;
            outerRecStub.shipToParty=creditMemoProducts[0].Credit_Memo__r.SoldToAccount__r.Account__r.SAP_Company_Code__c;
        }
        
        outerRecStub.salesDocType='YN01';
        outerRecStub.orderReason='2A9';
        outerRecStub.currencyKey='USD';
        Datetime dt=System.now();
        outerRecStub.validFromDate=dt.formatGMT('yyyy-MM-dd');
        outerRecStub.lines=creditMemoLines;

        request.body=outerRecStub;
        System.debug('lclc request '+request);

        try{
            response=webcallout(JSON.serialize(request),endpoint);
            System.debug('lclc response '+response);
        }catch(Exception ex){
            String errmsg = ex.getmessage() + ' - '+ex.getStackTraceString();
            System.debug('lclc ERROR IN 1 omq '+errmsg);
            handleError(omq,null,'error in res '+errmsg);
            return;
        }

        if(response == null){
            handleError(omq,null,'no response returned');
            return;
        }
        
        //parse response
        cihStub.UpdateInvoicePriceResponse stubResponse = new cihStub.UpdateInvoicePriceResponse();
        try{
            //stubResponse = (cihStub.UpdateInvoicePriceResponse) json.deserialize(responsefake, cihStub.UpdateInvoicePriceResponse.class);
            stubResponse=(cihStub.UpdateInvoicePriceResponse) json.deserialize(response, cihStub.UpdateInvoicePriceResponse.class);
            System.debug('lclc stubResponse '+stubResponse);
        }catch(exception ex){
            String errmsg = ex.getmessage() + ' - '+ex.getStackTraceString();
            System.debug('ERROR IN RESPONSESTUB ---- '+errmsg);
            handleError(omq,stubResponse,'error in response stub '+errmsg);
            return;           
        }

        if((stubResponse.inputHeaders.MSGSTATUS).contains('E')){
            System.debug('ERROR IN RESPONSESTUB 2 ----  MSG STATUS E');
            handleError(omq,stubResponse,null);
            return;  
        }

        if(stubResponse.body!=null){
            for(cihStub.CreditMemoLineItemsResponse res: stubResponse.body.lines){
                if(res.invoicePrice!=null){
                    Credit_Memo_Product__c cmp=itemNumberCMPMap.get(res.itemNumberSdDoc);
                    cmp.Invoice_Price__c=decimal.valueOf(res.invoicePrice);
                    System.debug('lclc cmp1. invoice price '+cmp.Invoice_Price__c);
                    cmpToUpdate.add(cmp);
                }
            }
        }else{
            handleError(omq,null,'response stub body null');
            return;
        }

        //do the upsert
        try{
            String tempstring='';

            if(cmpToUpdate != null && !cmpToUpdate.isEmpty()){
                System.debug('lclc cmpToUpdate.size() '+cmpToUpdate.size());
                List<Database.SaveResult> uResultsCMP=Database.Update(cmpToUpdate ,false);

                for(Database.SaveResult sr:uResultsCMP){
                    System.debug('lclc in sr '+sr);
                    if(!sr.isSuccess()){
                        system.debug('If block for failed to update the  records ');
                        errCount=errCount+1;
                        tempstring = tempstring + ' ***** ' + string.valueof(sr.getErrors());
                        System.debug('The failed records error  - '+tempstring);
                    }
                }
            }

            //if(tempstring!='' || (cmpToUpdate == null || cmpToUpdate.isEmpty()) ){
            if((tempstring!=''&& tempstring!=null)|| (cmpToUpdate == null || cmpToUpdate.isEmpty()) ){
                system.debug('Calling Error Handler');
                handleError(omq,stubResponse,null);
                system.debug('Error while executing error handling');
                return;
            }else{
                system.debug('calling Success Handler');
                handleSuccess(omq,stubResponse);
                system.debug('Error while executing success handling');
                return;    
            }
        }catch(exception ex){
            String errmsg = ex.getmessage() + ' - '+ex.getStackTraceString();
            system.debug('exception : '+errmsg);
            handleError(omq,stubResponse,errmsg);
            return;
        }
        System.debug('lclc  hmm');

    }

    public static void handleError(message_queue__c mq, cihStub.UpdateInvoicePriceResponse res,String otherError){
        System.debug('lclc in error '+mq);
        if(mq.retry_counter__c != null){
            mq.retry_counter__c += 1;    
        }else{
            mq.retry_counter__c = 1;
        }
        
        mq.status__c = mq.retry_counter__c > 5 ? 'failed' : 'failed-retry';
        if(res!=null && res.inputHeaders!=null){
            mq.ERRORTEXT__c=res.inputHeaders.ERRORTEXT;
            if(otherError!=null){
                mq.errortext__c+=otherError;
            }
            mq.ERRORCODE__c=res.inputHeaders.ERRORCODE;
        }else{
            mq.errortext__c=otherError;
        }
        upsert mq;

        if(mq.Status__c=='failed'){
            ///what to do if positive
            Credit_Memo__c cm=[select id, Integration_Status__c from Credit_Memo__c where id=:mq.Identification_Text__c];
            cm.Integration_Status__c='Error';
            update cm;
        }
    } 

    public static void handleSuccess(message_queue__c mq, cihStub.UpdateInvoicePriceResponse res){
        System.debug('lclc in success ');
        mq.status__c = 'success';
        mq.MSGSTATUS__c = res.inputHeaders.MSGSTATUS;
        mq.MSGUID__c = res.inputHeaders.msgGUID;
        mq.IFID__c = res.inputHeaders.ifID;
        mq.IFDate__c = res.inputHeaders.ifDate;      
        mq.ERRORCODE__c=null;
        mq.ERRORTEXT__c=null;      
        upsert mq;
        if(!Test.isRunningTest())
        {
            ///what to do if positive
            Credit_Memo__c cm=[select id, Integration_Status__c from Credit_Memo__c where id=:mq.Identification_Text__c];
            cm.Integration_Status__c='Success';
            SYstem.debug('lclc about to update cm '+cm.Integration_Status__c+' '+cm.id);
            update cm;
            SYstem.debug('lclc just update cm '+cm.Integration_Status__c+' '+cm.id);
        }
    }

    public static string webcallout(String bodyVar, string endpoint){
    
        string partialEndpoint = Test.isRunningTest() ? '' : Integration_EndPoints__c.getInstance('Flow-008').partial_endpoint__c;
        System.debug('lclc partialEndpoint '+partialEndpoint);
        return RoiIntegrationHelper.webcallout( bodyVar, partialEndpoint);

    }

/****************************************HELPER****************************************/

    public void unwrapAndSaveCMP(List<creditMemoProduct> cmpsVar){
        List<Credit_Memo_Product__c> cmpsToUpdate=new List<Credit_Memo_Product__c>();

        for(creditMemoProduct cmp:cmpsVar){
            Credit_Memo_Product__c c=new Credit_Memo_Product__c(Id=cmp.id, Request_Quantity__c=cmp.requestQuantity, Invoice_Price__c=cmp.currentPrice );
            cmpsToUpdate.add(c);
        }

        if(!cmpsToUpdate.isEmpty()){
            update cmpsToUpdate;
        }
    }
 /****************************************PageRef****************************************/
    
    //Added by Jagan
    public pageReference switchMessage(){
        system.debug('inside switch message...');
        showlateMesg = TRUE;
        return null;
    }
    
    public PageReference save() {
        unwrapAndSaveCMP(creditMemoProducts.values());

        return new PageReference('/'+ creditMemo.id);
    }
    
    //no page redirect just save
    public void saveAndCalculate(){ }

    public PageReference cancel(){
        return new PageReference('/'+ creditMemo.id);
    }

/****************************************WRAPPER CLASS****************************************/

/*public Decimal requestQuantity {
            get { return creditMemoProduct.Request_Quantity__c; }
            set {
                creditMemoProduct.Request_Quantity__c = value;
                try {
                    upsert creditMemoProduct;
                } catch(DMLException e) {
                    System.debug('DMLException occured updating the credit memo request quantity: ' + value);
                } catch(Exception e) {
                    System.debug('Unknown exception occured upserting credit memo quantity value: ' + value);
                    System.debug('Unknown exception occured ' + e.getMessage());
                    CreditMemoController.displayErrorMessage('Could not update the quantity, please contact admin if the problem persists');
                }
            }
        }

        public Decimal currentPrice {
            get { return creditMemoProduct.Invoice_Price__c; }
            set{
                //@TODO better way to do this?
                CreditMemoProduct.Invoice_Price__c=value;
                try{
                    upsert CreditMemoProduct;
                }catch(DMLException e) {
                    System.debug('DMLException occured updating the credit memo current price: ' + value);
                } catch(Exception e) {
                    System.debug('Unknown exception occured upserting credit memo current price value: ' + value);
                    System.debug('Unknown exception occured ' + e.getMessage());
                    CreditMemoController.displayErrorMessage('Could not update the current price, please contact admin if the problem persists');
                }
            }
        }*/

    public class CreditMemoProduct{
        public Credit_Memo_Product__c creditMemoProduct {get;set;}
        public Decimal requestQuantity {get;set;}
        public Decimal currentPrice {get;set;}

        public CreditMemoProduct(Credit_Memo_Product__c creditMemoProduct) {
            this.creditMemoProduct = creditMemoProduct;
            System.debug('lclc CreditMemoProduct invoice pirce '+CreditMemoProduct.invoice_price__c);
            this.requestQuantity=creditMemoProduct.Request_Quantity__c;
            this.currentPrice=creditMemoProduct.Invoice_Price__c;
        }

        public Id id {
            get { return creditMemoProduct.Id; }
        }

        public String opportunityNumber {
            get { return creditMemoProduct.Opportunity_Number__c; }
        }

        public String quoteName {
            get { return creditMemoProduct.Quote__r.Name; }
        }

        public String quoteNumber{
            get { return creditMemoProduct.Quote_Number__c; }
        }

        public String sapMaterialId {
            get { return creditMemoProduct.SAP_Material_ID__c; }
        }

        public String modelName {
            get { return creditMemoProduct.Product_Name__c; }
        }

        public Decimal quantity {
            get { return creditMemoProduct.Original_Quantity__c;}
        }

        public Decimal claimedQuantity {
            get { return creditMemoProduct.Claimed_Quantity__c; }
        }

        public Decimal unclaimedQuantity {
            get { return creditMemoProduct.Unclaimed_Quantity__c; }
        }

        public Decimal approvedPrice {
            get { return creditMemoProduct.Approved_Price__c; }
        }

        public Decimal creditPrice {
            get { return creditMemoProduct.Credit_Price__c; }
        }

        public Decimal creditAmount {
            get { return creditMemoProduct.TotalClaimAmount__c; }
        }
    }
    public class CreditMemo {
        private Credit_Memo__c creditMemo;

        public CreditMemo(Credit_Memo__c creditMemo) {
            this.creditMemo = creditMemo;
        }

        public Id id {
            get { return creditMemo.Id; }
        }

        public String creditMemoNumber {
            get { return creditMemo.Credit_Memo_Number__c; }
        }

        public String directPartner {
            get { return creditMemo.Direct_Partner__r.Name; }
        }

        public String status {
            get { return creditMemo.Status__c; }
        }

        public String assignmentNumber {
            get { return creditMemo.Assignment_Number__c; }
        }

        public String comments {
            get { return creditMemo.Comments__c; }
            set { 
                System.debug('EV: set comment');
                creditMemo.Comments__c = value;
                try {
                    update creditMemo;
                } catch(DMLException e) {
                    System.debug('DMLException occured updating the credit memo request quantity: ' + value);
                } catch(Exception e) {
                    System.debug('Unknown exception occured upserting credit memo quantity value: ' + value);
                    System.debug('Unknown exception occured ' + e.getMessage());
                    CreditMemoController.displayErrorMessage('Could not update the quantity, please contact admin if the problem persists');
                }
            }
        }

        public String createdDate {
            get { return String.valueOf(creditMemo.CreatedDate); }
        }

        public String lastModifiedDate {
            get { return String.valueOf(creditMemo.LastModifiedDate); }
        }

        public String sapRequestNumber {
            get { return creditMemo.SAP_Request_Number__c; }
        }

        public String sapBillingNumber {
            get { return creditMemo.SAP_Billing_Number__c; }
        }

        public String sapResponse {
            get { return creditMemo.SAP_Response__c; }
        }

        public Decimal totalCreditAmount {
            get { return creditMemo.TotalCreditAmount__c; }
        }
    }





}