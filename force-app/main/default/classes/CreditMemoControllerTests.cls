//Modified By : Aarthi R C
// Modified Date : Apr - 22-2016

@isTest
private class CreditMemoControllerTests {

	 static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');

   @testSetup static void setup() {

   		//Jagan: Test data for fiscalCalendar__c
        Test.loadData(fiscalCalendar__c.sObjectType, 'FiscalCalendarTestData');

        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();

        List<Integration_EndPoints__c> ieps = new List<Integration_EndPoints__c>();
        /*Integration_EndPoints__c iep = new Integration_EndPoints__c();
        iep.name ='Flow-012';
        iep.endPointURL__c = 'https://www.google.com';
        iep.layout_id__c = 'LAY024197';
        iep.partial_endpoint__c = '/SFDC_IF_012';
        ieps.add(iep);*/
        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='Flow-008';
        iep2.endPointURL__c = 'https://www.google.com';
        iep2.layout_id__c = 'LAY024197';
        iep2.partial_endpoint__c = '/SFDC_IF_012';
        ieps.add(iep2);
        insert ieps;


        CreditMemoIntegration__c cmi=new CreditMemoIntegration__c(Distribution_Channel__c='11',Name='Distribution Channel Value');
        insert cmi;

        Account account = TestDataUtility.createAccount('Marshal Mathers');
        insert account;
        Account partnerAcc = TestDataUtility.createAccount('Distributor account');
        partnerAcc.RecordTypeId = directRT;
        insert partnerAcc;
         //Creating custom Pricebook
        PriceBook2 pb = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(4)); 
        insert pb;
        Opportunity opportunity = TestDataUtility.createOppty('New', account, pb, 'Managed', 'IT', 'product group',
                                    'Identified');
        insert opportunity;
        Product2 product = TestDataUtility.createProduct2('SL-SCF5300/SEG', 'Standalone Printer', 'Printer[C2]', 'H/W', 'FAX/MFP');
        insert product;
        Id pricebookId = Test.getStandardPricebookId();
        //PricebookEntry pricebookEntry1 = TestDataUtility.createPriceBookEntry(product.Id, pricebookId);
        //insert pricebookEntry1;
        ///^^ del bc System.DmlException: Insert failed. First exception on row 0; first error: DUPLICATE_VALUE, This price definition already exists in this price book: []
        PricebookEntry pricebookEntry = TestDataUtility.createPriceBookEntry(product.Id, pb.Id);
        insert pricebookEntry;

        List<OpportunityLineItem> opportunityLineItems = new List<OpportunityLineItem>();
        OpportunityLineItem op1 = TestDataUtility.createOpptyLineItem(product, pricebookEntry, opportunity);
        op1.Quantity = 100;
        op1.TotalPrice = 1000;
        op1.Requested_Price__c = 100;
        op1.Claimed_Quantity__c = 45;     
        opportunityLineItems.add(op1);
        insert opportunityLineItems;
        System.debug('Insert OLI success');

        Quote quot = TestDataUtility.createQuote('Test Quote', opportunity, pb);
        quot.ROI_Requestor_Id__c=UserInfo.getUserId();
        insert quot;
        
        List<QuoteLineItem> qlis = new List<QuoteLineItem>();
        QuoteLineItem standardQLI = TestDataUtility.createQuoteLineItem(quot.Id, product, pricebookEntry, op1 );
        standardQLI.Quantity = 25;
        standardQLI.UnitPrice = 100;
        standardQLI.Requested_Price__c=100;
        standardQLI.OLIID__c = op1.id;
        qlis.add(standardQLI);
        insert qlis;


        Credit_Memo__c cm = TestDataUtility.createCreditMemo();
        //cm.Credit_Memo_Number__c = TestDataUtility.generateRandomString(5);
        cm.InvoiceNumber__c = TestDataUtility.generateRandomString(6);
        cm.Direct_Partner__c = partnerAcc.id;
        insert cm;
      
        List<Credit_Memo_Product__c> creditMemoProducts = new List<Credit_Memo_Product__c>();
        Credit_Memo_Product__c cmProd = TestDataUtility.createCreditMemoProduct(cm, op1);
        cmProd.Request_Quantity__c = 45;
        creditMemoProducts.add(cmProd);
        insert creditMemoProducts;
        

    }


	@isTest static void testGetQuantity() {
		/*Credit_Memo__c creditMemo = new Credit_Memo__c();
		upsert creditMemo;

		Credit_Memo_Product__c product = new Credit_Memo_Product__c();
		product.Credit_Memo__c = creditMemo.Id;
		product.Request_Quantity__c = 100;
		upsert product;*/
		Test.startTest();
		Credit_Memo__c creditMemo = [select Id, status__c from Credit_Memo__c Limit 1];
		Credit_Memo_Product__c product = [select Id, Request_Quantity__c from Credit_Memo_Product__c Limit 1];
		ApexPages.StandardController sc = new ApexPages.StandardController(creditMemo);
		CreditMemoController controller = new CreditMemoController(sc);
		
		System.assertEquals(controller.creditMemoProducts.get(product.Id).requestQuantity, 45);
		//update quantity
		controller.creditMemoProducts.get(product.Id).requestQuantity = 200;
		System.assertEquals(controller.creditMemoProducts.get(product.Id).requestQuantity, 200);
		//invalid quantity update
		controller.selectedProductId = 'invalid';
		controller.deleteProduct();
		List<ApexPages.Message> msgList = ApexPages.getMessages();
		for(ApexPages.Message msg :  ApexPages.getMessages()) {
			System.assert(msg.getDetail().contains('Could not delete record'));
		}
		//delete product
		controller.selectedProductId = String.valueOf(product.Id);
		controller.deleteProduct();
		System.assertEquals([SELECT COUNT() FROM Credit_Memo_Product__c], 0);
		Test.stopTest();
	}

	// method 2

	@isTest static void test_method_two()
	{	
		Test.startTest();
		Credit_Memo__c creditMemo = [select Id, status__c,Comments__c from Credit_Memo__c Limit 1];
		creditMemo.Comments__c = 'from testing';
		creditMemo.SAP_Request_Number__c = 'req12234567';
		creditMemo.SAP_Billing_Number__c = 'bill12234567';
		creditMemo.SAP_Response__c = 'response2345';
		update creditMemo;

		Credit_Memo_Product__c cmProduct = [select Id, Request_Quantity__c from Credit_Memo_Product__c Limit 1];
		cmProduct.Invoice_Price__c = 56.0;
		cmProduct.Approved_Price__c = 51.0;
		cmProduct.Total_Claim_Amount__c = 500.00;
		update cmProduct;

		ApexPages.StandardController sc = new ApexPages.StandardController(creditMemo);
		CreditMemoController controller = new CreditMemoController(sc);
		creditMemo.comments__c='testing comment';
		controller.creditMemoProducts.get(cmProduct.Id).requestQuantity = 200;
		controller.save();
		controller.saveAndCalculate();
		//controller.updatePricing();
		controller.cancel();
		//controller.updatePricing();

		// For code coverage calling all the defined variables on the inner class
		CreditMemoController.CreditMemo cMemo = new CreditMemoController.CreditMemo(creditMemo);
		system.assertEquals(cMemo.id, creditMemo.Id);
		system.debug('Calling all the inner varaibales defined' + cMemo.creditMemoNumber + cMemo.directPartner + cMemo.lastModifiedDate + cMemo.comments + cMemo.createdDate + cMemo.status + cMemo.sapResponse + cMemo.sapBillingNumber + cMemo.sapRequestNumber + cMemo.totalCreditAmount);
		
		CreditMemoController.CreditMemoProduct innerProduct = new CreditMemoController.CreditMemoProduct(cmProduct);
		system.assertEquals(innerProduct.id, cmProduct.id);
		system.debug('CAlling all the inner varaiables defined' +  innerProduct.opportunityNumber + innerProduct.quoteNumber + innerProduct.quoteName + innerProduct.creditPrice + innerProduct.sapMaterialId + innerProduct.modelName + innerProduct.quantity + innerProduct.claimedQuantity + innerProduct.unclaimedQuantity + innerProduct.currentPrice + innerProduct.approvedPrice + innerProduct.creditAmount );

		Test.stopTest();

	}

	@isTest static void test_callout(){
		Credit_Memo__c creditMemo = [select Id, status__c from Credit_Memo__c Limit 1];

		ApexPages.StandardController sc = new ApexPages.StandardController(creditMemo);
		CreditMemoController controller = new CreditMemoController(sc);

		Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockResponseGenerator());

		controller.updatePricing();
		Test.stopTest();

	}

	class MockResponseGenerator implements HttpCalloutMock {
    // Implement this interface method
        public HTTPResponse respond(HTTPRequest req) {
            // Optionally, only send a mock response for a specific endpoint
            // and method.
            System.assertEquals('callout:X012_013_ROI', req.getEndpoint());
            System.assertEquals('POST', req.getMethod());
            
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{' +
                            '"inputHeaders": {'+
                            '"MSGGUID": "ada747b2-dda9-1931-acb2-14a2872d4358",' +
                            '"IFID": "TBD",' +
                            '"IFDate": "20160628194634",' +
                            '"MSGSTATUS": "S",' +
                            '"ERRORTEXT": null,' +
                            '"ERRORCODE": null' +
                                            '},' +
                            '"body": {'+
                            	'"lines": ['+

                            		'{"unitCost":"52.92",'+
                            		'"materialCode":"CLT-Y04s",'+
                            		'"quantity":"1.0",'+
                            		'"invoicePrice":"65.55",'+
                            		'"currencyKey":"USD",'+
                            		'"itemNumberSdDoc":"000001"}'+
                            	']'+
                            
                            '}'+
                         '}');
            res.setStatusCode(200);
            return res;



        }
    }
}