/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 
 /**
  * @Author: Mir Khan
  * @Info: Test class for CreditMemoIntegrationHelper 
  *
  */
@isTest(SeeAllData=true) 
private class CreditMemoIntegrationHelperTest {
    
    private static String messageQueueId  = '007';
    private static String messageStatus  = 'Test';
    private static String messageQueueStatus = 'Not Started';
    private static Decimal messageQueueRetryCounter = 1;
    private static String messageQueueIntegrationFlowType = '';
    private static String prmLeadId = '0000';
    private static Integer messageTypeIndex = 0;
    private static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
    
    
    /**
     * Creating Leads and Message Queues with different
     * Integration Flow Types
     **/ 
    static Message_Queue__c createMessageQueue() {
        
        
        message_queue__c mq = new message_queue__c(MSGUID__c = messageQueueId, 
                                                    MSGSTATUS__c = messageStatus,  
                                                    Status__c = messageQueueStatus, 
                                                    retry_counter__c = messageQueueRetryCounter);
        
        mq.Integration_Flow_Type__c = messageTypeIndex == 1  ? messageQueueIntegrationFlowType = 'Flow-009_1' :
                                      messageTypeIndex == 2  ? messageQueueIntegrationFlowType = 'Flow-008' :
                                      messageTypeIndex == 3  ? messageQueueIntegrationFlowType = 'Flow-010' :
                                      messageTypeIndex == 4  ? messageQueueIntegrationFlowType = 'Flow-008_Quote' :
                                      messageTypeIndex == 5  ? messageQueueIntegrationFlowType = '008 to 012' :
                                      messageTypeIndex == 6  ? messageQueueIntegrationFlowType = '012' :
                                      messageTypeIndex == 7  ? messageQueueIntegrationFlowType = '003' :
                                      messageTypeIndex == 8  ? messageQueueIntegrationFlowType = '004' :
                                      messageTypeIndex == 9  ? messageQueueIntegrationFlowType = 'Flow-016' :
                                      messageTypeIndex == 10 ? messageQueueIntegrationFlowType = 'Flow-016_1' :
                                      messageTypeIndex == 11 ? messageQueueIntegrationFlowType = 'invoicing 8' : null;
        return mq;
        
    }

    /**
     * Flow-010
     * CreditMemoIntegrationHelper
     **/
    static testMethod void testCallout1() {
        
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Account account = TestDataUtility.createAccount('Marshal Mathers');
        insert account;
        Account partnerAcc = TestDataUtility.createAccount('Distributor account');
        partnerAcc.RecordTypeId = directRT;
        insert partnerAcc;
        //Pricebook2 standardPB = [select id from Pricebook2 where isStandard=true];
         //Creating custom Pricebook
        PriceBook2 pb = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(4)); 
        insert pb;
        Opportunity opportunity = TestDataUtility.createOppty('New', account, pb, 'Managed', 'IT', 'product group',
                                    'Identified');
        insert opportunity;
        Product2 product = TestDataUtility.createProduct2('SL-SCF5300/SEG1', 'Standalone Printer2', 'Printer[C2]', 'H/W', 'FAX/MFP');
        insert product;
        Id pricebookId = Test.getStandardPricebookId();
        //PricebookEntry standardPBEntry = TestDataUtility.createPriceBookEntry(product.Id, pricebookId);
        //insert standardPBEntry;
        ///^^ del bc System.DmlException: Insert failed. First exception on row 0; first error: DUPLICATE_VALUE, This price definition already exists in this price book: []
        PricebookEntry pricebookEntry = TestDataUtility.createPriceBookEntry(product.Id, pb.Id);
        pricebookEntry.UnitPrice = 112;
        insert pricebookEntry;

        List<OpportunityLineItem> opportunityLineItems = new List<OpportunityLineItem>();
        OpportunityLineItem op1 = TestDataUtility.createOpptyLineItem(product, pricebookEntry, opportunity);
        op1.Quantity = 100;
        op1.TotalPrice = 1000;
        op1.Requested_Price__c = 100;
        op1.Claimed_Quantity__c = 45;     
        opportunityLineItems.add(op1);
        insert opportunityLineItems;
        System.debug('Insert OLI success');

        Quote quot = TestDataUtility.createQuote('Test Quote', opportunity, pb);
        quot.ROI_Requestor_Id__c=UserInfo.getUserId();
        insert quot;
        
        List<QuoteLineItem> qlis = new List<QuoteLineItem>();
        QuoteLineItem standardQLI = TestDataUtility.createQuoteLineItem(quot.Id, product, pricebookEntry, op1);
        standardQLI.Quantity = 25;
        standardQLI.UnitPrice = 100;
        standardQLI.Requested_Price__c=100;
        standardQLI.OLIID__c = op1.id;
        qlis.add(standardQLI);
        insert qlis;


        Credit_Memo__c cm = TestDataUtility.createCreditMemo();
        //cm.Credit_Memo_Number__c = TestDataUtility.generateRandomString(5);
        cm.InvoiceNumber__c = TestDataUtility.generateRandomString(6);
        cm.Direct_Partner__c = partnerAcc.id;
        cm.Division__c = 'Mobile';
        insert cm;
      
        List<Credit_Memo_Product__c> creditMemoProducts = new List<Credit_Memo_Product__c>();
        Credit_Memo_Product__c cmProd = TestDataUtility.createCreditMemoProduct(cm, op1);
        cmProd.Request_Quantity__c = 45;
        creditMemoProducts.add(cmProd);
        insert creditMemoProducts;
        
        
        Message_Queue__c mq = createMessageQueue();
        mq.Identification_Text__c = cm.Id;
        
        Account_Sold_To__c accountSoldTo = new Account_Sold_To__c(Account__c = partnerAcc.Id, Assignment__c = partnerAcc.Id, Sales_Organization__c='3105', distribution_channel_id__c = '7', sales_division_id__c = '7');
        insert accountSoldTo;
        
        
        cm.Comments__c = 'from testing';
        cm.SAP_Request_Number__c = 'req12234567';
        cm.SAP_Billing_Number__c = 'bill12234567';
        cm.SAP_Response__c = 'response2345';
        cm.SoldToAccount__c = accountSoldTo.Id;
        
        System.debug('Sold To Account Details -- ' + accountSoldTo);
        System.debug('Credit Memo Details -- ' + cm);
        update cm;

        cmProd.Invoice_Price__c = 56.0;
        cmProd.Approved_Price__c = 51.0;
        cmProd.Total_Claim_Amount__c = 500.00;
        update cmProd;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockResponseGenerator('"S"'));
        CreditMemoIntegrationHelper.callCreditMemoEndpoint(mq);
        
        Test.setMock(HttpCalloutMock.class, new MockResponseGenerator('"E"'));
        CreditMemoIntegrationHelper.callCreditMemoEndpoint(mq);  
        Test.stopTest(); 
         
    }
    
    /**
     * Flow-010
     * CreditMemoIntegrationHelper
     **/
    static testMethod void testCallout2() {
        
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Account account = TestDataUtility.createAccount('Marshal Mathers');
        insert account;
        Account partnerAcc = TestDataUtility.createAccount('Distributor account');
        partnerAcc.RecordTypeId = directRT;
        insert partnerAcc;
        //Pricebook2 standardPB = [select id from Pricebook2 where isStandard=true];
         //Creating custom Pricebook
        PriceBook2 pb = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(4)); 
        insert pb;
        Opportunity opportunity = TestDataUtility.createOppty('New', account, pb, 'Managed', 'IT', 'product group',
                                    'Identified');
        insert opportunity;
        Product2 product = TestDataUtility.createProduct2('SL-SCF5300/SEG1', 'Standalone Printer2', 'Printer[C2]', 'H/W', 'FAX/MFP');
        insert product;
        Id pricebookId = Test.getStandardPricebookId();
        //PricebookEntry standardPBEntry = TestDataUtility.createPriceBookEntry(product.Id, pricebookId);
        //insert standardPBEntry;
        ///^^ del bc System.DmlException: Insert failed. First exception on row 0; first error: DUPLICATE_VALUE, This price definition already exists in this price book: []
        PricebookEntry pricebookEntry = TestDataUtility.createPriceBookEntry(product.Id, pb.Id);
        pricebookEntry.UnitPrice = 112;
        insert pricebookEntry;

        List<OpportunityLineItem> opportunityLineItems = new List<OpportunityLineItem>();
        OpportunityLineItem op1 = TestDataUtility.createOpptyLineItem(product, pricebookEntry, opportunity);
        op1.Quantity = 100;
        op1.TotalPrice = 1000;
        op1.Requested_Price__c = 100;
        op1.Claimed_Quantity__c = 45;     
        opportunityLineItems.add(op1);
        insert opportunityLineItems;
        System.debug('Insert OLI success');

        Quote quot = TestDataUtility.createQuote('Test Quote', opportunity, pb);
        quot.ROI_Requestor_Id__c=UserInfo.getUserId();
        insert quot;
        
        List<QuoteLineItem> qlis = new List<QuoteLineItem>();
        QuoteLineItem standardQLI = TestDataUtility.createQuoteLineItem(quot.Id, product, pricebookEntry, op1);
        standardQLI.Quantity = 25;
        standardQLI.UnitPrice = 100;
        standardQLI.Requested_Price__c=100;
        standardQLI.OLIID__c = op1.id;
        qlis.add(standardQLI);
        insert qlis;


        Credit_Memo__c cm = TestDataUtility.createCreditMemo();
        //cm.Credit_Memo_Number__c = TestDataUtility.generateRandomString(5);
        cm.InvoiceNumber__c = TestDataUtility.generateRandomString(6);
        cm.Direct_Partner__c = partnerAcc.id;
        cm.Division__c = 'Mobile';
        insert cm;
      
        List<Credit_Memo_Product__c> creditMemoProducts = new List<Credit_Memo_Product__c>();
        Credit_Memo_Product__c cmProd = TestDataUtility.createCreditMemoProduct(cm, op1);
        cmProd.Request_Quantity__c = 45;
        creditMemoProducts.add(cmProd);
        insert creditMemoProducts;
        
        
        Message_Queue__c mq = createMessageQueue();
        mq.Identification_Text__c = cm.Id;
        
        Account_Sold_To__c accountSoldTo = new Account_Sold_To__c(Account__c = partnerAcc.Id, Assignment__c = partnerAcc.Id, Sales_Organization__c='3105', distribution_channel_id__c = '7', sales_division_id__c = '7');
        insert accountSoldTo;
        
        
        cm.Comments__c = 'from testing';
        cm.SAP_Request_Number__c = 'req12234567';
        cm.SAP_Billing_Number__c = 'bill12234567';
        cm.SAP_Response__c = 'response2345';
        cm.SoldToAccount__c = accountSoldTo.Id;
        
        System.debug('Sold To Account Details -- ' + accountSoldTo);
        System.debug('Credit Memo Details -- ' + cm);
        update cm;

        cmProd.Invoice_Price__c = 56.0;
        cmProd.Approved_Price__c = 51.0;
        cmProd.Total_Claim_Amount__c = 500.00;
        update cmProd;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockResponseGenerator('"E"'));
        CreditMemoIntegrationHelper.callCreditMemoEndpoint(mq);  
        Test.stopTest(); 
         
    }
    
 /**
  * Inner MockSuccessResponse class
  */   
 class MockResponseGenerator implements HttpCalloutMock {
    String responseStatus;
    MockResponseGenerator(String responseStatus) {
        this.responseStatus = responseStatus;
    }
    // Implement this interface method
    public HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        System.assertEquals('callout:X012_013_ROIasdf', req.getEndpoint());
        System.assertEquals('POST', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{' +
                        '"inputHeaders": {'+
                            '"MSGGUID": "a6bba321-2183-5fb4-ff75-4d024e39a98e",' +
                            '"IFID": "TBD",' +
                            '"IFDate": "160317",' +
                            '"MSGSTATUS":' + responseStatus + ',' +
                            '"ERRORTEXT": null,' +
                            '"ERRORCODE": null' +
                        '},' +
                        '"body": {' +
                             '"results" : [ {' +
                            '"type" : "S",' +
                            '"id" : "V1",' +
                            '"message" : "CR(Sales deduction) 108425581 has been saved",' +
                            '"messageV1" : "CR(Sales deduction)",' +
                            '"messageV2" : "108425581"'+
                            '}, {' +
                            '"type" : "S",' +
                            '"id" : "VF",' +
                            '"message" : "Document 5019828254 has been saved",' +
                            '"messageV1" : "5019828254",' +
                            '"messageV2" : ""' +
                            '} ],' +
                            '"lines": [{' +
                                '"unitCost": "20.1",' +
                                '"materialCode": "CLX-4195FN/XAA",' +
                                '"quantity": "1",' +
                                '"invoicePrice": "20.5",' +
                                '"currencyKey": "1",' +
                                '"itemNumberSdDoc": "001"' +
                            '}, {' +
                                '"unitCost": "330.1",' +
                                '"materialCode": "CLX-4195FN/XAB",' +
                                '"quantity": "1",' +
                                '"invoicePrice": "30.5",' +
                                '"currencyKey": "1",' +
                                '"itemNumberSdDoc": "002"' +
                            '}]' +
                    '}' +
                     '}');
        res.setStatusCode(200);
        return res;
    }
  }
  
}