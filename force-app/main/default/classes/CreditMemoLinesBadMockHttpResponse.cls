@isTest
global class CreditMemoLinesBadMockHttpResponse implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        System.assertEquals('http://api.salesforce.com/foo/bar', req.getEndpoint());
        System.assertEquals('POST', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{' +
                        '"wrong": {'+
                            '"MSGGUID": "a6bba321-2183-5fb4-ff75-4d024e39a98e",'+
                            '"IFID": "TBD",'+
                            '"IFDate": "160317",'+
                            '"MSGSTATUS": "S",'+
                            '"ERRORTEXT": null,'+
                            '"ERRORCODE": null'+
                        '},'+
                        '"body": {'+
                            '"lines": [{'+
                                '"unitCost": "20.1",'+
                                '"materialCode": "CLX-4195FN/XAA",'+
                                '"quantity": "1",'+
                                '"invoicePrice": "20.5",'+
                                '"currencyKey": "1",'+
                                '"itemNumberSdDoc": "001"'+
                            '}, {'+
                                '"unitCost": "330.1",'+
                                '"materialCode": "CLX-4195FN/XAB",'+
                                '"quantity": "1",'+
                                '"invoicePrice": "10.5",'+
                                '"currencyKey": "1",'+
                                '"itemNumberSdDoc": "002"'+
                            '}]'+
                        '}'+
                    '}');
        res.setStatusCode(200);
        return res;
    }
}