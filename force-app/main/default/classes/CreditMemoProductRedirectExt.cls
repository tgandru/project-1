public class CreditMemoProductRedirectExt {
    Id CMId;
    Credit_Memo_Product__c CMP;
    //string tempId;

    // we are extending the Order controller, so we query to get the parent record id
    public CreditMemoProductRedirectExt(ApexPages.StandardController controller) {
        CMP = (Credit_Memo_Product__c)controller.getRecord();
        
        if(CMP !=null && CMP.id !=null)
            CMId = [select Id, Credit_Memo__c from Credit_Memo_Product__c where Id = :CMP.id limit 1].Credit_Memo__c;
        else if(CMP !=null && CMP.Credit_Memo__c !=null)
            CMId = CMP.Credit_Memo__c;
            
        /*if(tempId.startsWithIgnoreCase('a0C')){
            CMId = [select Id, Credit_Memo__c from Credit_Memo_Product__c where Id = :tempId limit 1].Credit_Memo__c;
        } else if(tempId.startsWithIgnoreCase('a0G')) {
            Credit_Memo_Product__c cmq = [select Credit_Memo__c from Credit_Memo_Product__c where Id = :tempId limit 1];
            CMId = cmq.Credit_Memo__c;
        }*/
        
        
        
        
    }
    
    // then we redirect to our desired page with the Opportunity Id in the URL
    public pageReference redirect(){
        
        /*if(tempId.startsWithIgnoreCase('a0C')){
            return new PageReference('/apex/CreditMemo?id=' + CMId);
        } else if(tempId.startsWithIgnoreCase('a0G')) {
            return new PageReference('/apex/CreditMemo?id=' + CMId);
        }
        return null;
        */
        if(CMId != null)
            return new PageReference('/apex/CreditMemo?id=' + CMId);
        else
            return null;
    }
}