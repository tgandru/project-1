/* Extension for CreditMemoQuoteRedirect VF page */
public class CreditMemoQuoteRedirectExt {
    
    Id CMId;
    Id CMQId;
    
    Credit_Memo_Quote__c cmq;
    // we are extending the Order controller, so we query to get the parent record id
    public CreditMemoQuoteRedirectExt(ApexPages.StandardController controller) {
        cmq = (Credit_Memo_Quote__c)controller.getRecord();
        system.debug('Credit Memo quote....'+cmq);
        if(cmq !=null && cmq.id <>null)
            CMId = [select Id, Credit_Memo__c from Credit_Memo_Quote__c where Id = :cmq.id limit 1].Credit_Memo__c; 
        else if(cmq != null && cmq.Credit_Memo__c != null)
            CMId = cmq.Credit_Memo__c;
            
    }
    
    // then we redirect to our desired page with the Opportunity Id in the URL
    public pageReference redirect(){
        if(CMId != null)
            return new PageReference('/apex/CreditMemoAddQuotes?id=' + CMId);
        else
            return null;
    }
}