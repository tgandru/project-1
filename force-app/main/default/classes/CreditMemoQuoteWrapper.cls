/*
 * Model for Credit Memo Quote used in CreditMemoAddQuoteExtension.cls
 * Author: Eric Vennaro
 * 
*/
public class CreditMemoQuoteWrapper {
    public Credit_Memo_Quote__c creditMemoQuote {get;set;}
    public Id quoteId {get;set;}
    public String opportunityNumber {get;set;}
    public String spid {get;set;}
    public String customer {get;set;}
    public String validFrom {get;set;}
    public String validTo {get;set;}

    public CreditMemoQuoteWrapper(Credit_Memo_Quote__c creditMemoQuote, Id quoteId) {
        this.creditMemoQuote = creditMemoQuote;
        this.quoteId = quoteId;
        this.opportunityNumber = creditMemoQuote.Opportunity_Number__c;
        this.spid = creditMemoQuote.SP_Number__c;
        this.customer = creditMemoQuote.Customer__c;
        this.validFrom = String.valueOf(creditMemoQuote.Valid_From__c) != null ? creditMemoQuote.Valid_From__c.format() : '';
        this.validTo = String.valueOf(creditMemoQuote.Valid_To__c) != null ? creditMemoQuote.Valid_To__c.format() : '';
    }
    /* MK 6/29/2016 Unused constructor 
    public CreditMemoQuoteWrapper(Credit_Memo_Quote__c creditMemoQuote, Id quoteId, String opportunityNumber, String spid, String customer, Date validFrom, Date validTo) {
        this.creditMemoQuote = creditMemoQuote;
        this.quoteId = quoteId;
        this.opportunityNumber = opportunityNumber;
        this.spid = spid;
        this.customer = customer;
        this.validFrom = String.valueOf(validFrom);
        this.validTo = String.valueOf(validTo);
    }
    */
    public CreditMemoQuoteWrapper(Credit_Memo_Quote__c creditMemoQuote, QuoteWrapper quoteWrapper) {
        this.creditMemoQuote = creditMemoQuote;
        this.quoteId = quoteWrapper.quote.Id;
        this.opportunityNumber = quoteWrapper.quote.Quote_Number__c;
        this.spid = quoteWrapper.quote.External_SPID__c;
        this.customer = quoteWrapper.quote.Account.Name;
        if(quoteWrapper.quote != null && quoteWrapper.quote.Valid_From_Date__c != null){
            this.validFrom =  quoteWrapper.quote.Valid_From_Date__c.format();
        }else{
            this.validFrom = '';
        }
        if(quoteWrapper.quote != null && quoteWrapper.quote.ExpirationDate != null){
            this.validTo =  quoteWrapper.quote.ExpirationDate.format();
        }else{
            this.validTo = '';
        }
        
    }
}