/*
 * Facade class for CreditMemoTrigger.trigger links with CreditMemoUpdateTriggeHelper.cls
 * Author: Eric Vennaro
**/

public class CreditMemoUpdateTriggerFacade {
    private List<Credit_Memo__c> creditMemosFromTrigger;
    private Map<Id, Credit_Memo__c> oldMap;
    
    public CreditMemoUpdateTriggerFacade(List<Credit_Memo__c> creditMemosFromTrigger, Map<Id, Credit_Memo__c> oldMap) {
        this.creditMemosFromTrigger = creditMemosFromTrigger;
        this.oldMap = oldMap;
    }

    public void updateOpportunityProductClaimQuantity() {
        Map<Id, Credit_Memo__c> creditMemosToUpdate = new Map<Id, Credit_Memo__c>();
        for(Credit_Memo__c creditMemo : creditMemosFromTrigger) {
            Credit_Memo__c oldCreditMemo = oldMap.get(creditMemo.Id);
            if(creditMemo.Status__c != null && oldCreditMemo.Status__c != null) {
                if(creditMemo.Status__c.equals('Approved') && !oldCreditMemo.Status__c.equals('Approved')){
                    System.debug('lclc found credit memo change '+creditMemo);
                    creditMemosToUpdate.put(creditMemo.Id, creditMemo);
                }
            }
        }
        System.debug('lclc creditMemosToUpdate '+creditMemosToUpdate.size());
        if(creditMemosToUpdate.size() > 0) {
            CreditMemoUpdateTriggerHelper creditMemoUpdate = new CreditMemoUpdateTriggerHelper(creditMemosToUpdate);
            creditMemoUpdate.updateLineItems();
            //creditMemoUpdate.updateOpportunityLineItems();
        }
    }
}