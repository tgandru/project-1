public class CreditMemoUpdateTriggerHelper {
    private Map<Id, Credit_Memo__c> creditMemos;
    public Set<Id> creditMemoProductSet;

    public CreditMemoUpdateTriggerHelper(Map<Id, Credit_Memo__c> creditMemos) {
        this.creditMemos = creditMemos;
        creditMemoProductSet = new Set<Id>();
        if(creditMemos.size() > 0){
            processMessageQueueRecs();
            creditMemoProductSet = getCreditMemoProductIds();
        }
    }

    //Create Message Queue records
    public void processMessageQueueRecs(){
        System.debug('lclc in process '+creditMemos.size());
        List<Message_Queue__c> mqList = new List<Message_Queue__c>();
        string layoutId = (Test.isRunningTest() || Integration_EndPoints__c.getInstance('Flow-010').layout_id__c==null )? 'asdf' : Integration_EndPoints__c.getInstance('Flow-010').layout_id__c;
        for(Id creditMemoId : creditMemos.keySet()){
            Message_Queue__c mq = new Message_Queue__c(Integration_Flow_Type__c = 'Flow-010', retry_counter__c = 0, Identification_Text__c = creditMemoId, Status__c='not started',Object_Name__c='Credit Memo', IFID__c=layoutId);
            mqList.add(mq);
        }
        try {
            insert mqList;
            System.debug('Message Queue Insertion' + mqList);
        } catch(DmlException e) {
            System.debug('DML Exception occured inserting Message Queue' + e.getMessage());
        }catch(Exception ex){
            System.debug('Exception in Message Queue Insertion' + ex);
        }   
    }

    //Process OpportunityLineItems, QuoteLineItems
    public void updateLineItems() {
        List<OpportunityLineItem> opportunityLineItems=new List<OpportunityLineItem>();
        List<QuoteLineItem> quoteLineItems = new List<QuoteLineItem>();
        List<OpportunityLineItem> opptyLineItemsToUpdate = new List<OpportunityLineItem>();
        List<QuoteLineItem> quoteLineItemsToUpdate = new List<QuoteLineItem>();
        Map<String,Integer> creditMemoProducts = getCreditMemoProductsMap(creditMemoProductSet);
        Map<String,Integer> creditMemoQuotes = getCreditMemoQuotesMap(creditMemoProductSet);
    
    try{
        if(!creditMemoQuotes.isEmpty()){
                database.executebatch(new QLI_OLI_Claimed_Qty_Update(creditMemoQuotes.keyset()),1);
            }
    }catch(DmlException e) {
            System.debug('Exception occured-->' + e.getMessage());
        }
        /*
    if(!creditMemoProducts.isEmpty()){
            opportunityLineItems = [SELECT Claimed_Quantity__c, Id
                                    FROM OpportunityLineItem
                                    WHERE Id IN : creditMemoProducts.keySet()];
            quoteLineItems = [select Id,Claimed_Quantity__c from QuoteLineItem where Id IN: creditMemoQuotes.keySet()];
        }
        //Fetch the list of OpptyLineItems to be updated
        
    if(!opportunityLineItems.isEmpty()){
            for(OpportunityLineItem oli : opportunityLineItems){
                if(creditMemoProducts.containsKey(oli.Id)){
                    if(creditMemoProducts.get(oli.Id) != null) {
                        System.debug('lclc updating claimed qty on oli '+creditMemoProducts.get(oli.Id));
                        if(oli.Claimed_Quantity__c==null) oli.Claimed_Quantity__c=0; //Added this condition to SUM QLI claimed qty with new Creditmemoprod reuest qty--04/11/2018 by thiru.
                        oli.Claimed_Quantity__c = oli.Claimed_Quantity__c + creditMemoProducts.get(oli.Id);
                        //oli.Claimed_Quantity__c = creditMemoProducts.get(oli.Id);
                        opptyLineItemsToUpdate.add(oli);
                    }                    
                }
            }
        }
        
        //Fetch the list of QuoteLineItems to be updated
         if(!quoteLineItems.isEmpty()){
            for(QuoteLineItem qli : quoteLineItems){
                if(creditMemoQuotes.containsKey(qli.Id)){
                    if(creditMemoQuotes.get(qli.Id) != null) {
                        System.debug('lclc updating claimed qty on qli '+creditMemoQuotes.get(qli.Id));
                        if(qli.Claimed_Quantity__c==null) qli.Claimed_Quantity__c=0; //Added this condition to SUM QLI claimed qty with new Creditmemoprod reuest qty--04/11/2018 by thiru.
                        qli.Claimed_Quantity__c = qli.Claimed_Quantity__c + creditMemoQuotes.get(qli.Id);
                        //qli.Claimed_Quantity__c = creditMemoQuotes.get(qli.Id);
                        quoteLineItemsToUpdate.add(qli);
                    }                    
                }
            }
        }
        
        //Process OpportunityLineItems, QuoteLineItems
        try {
          if(opptyLineItemsToUpdate.size() > 0) {
               Database.update(opptyLineItemsToUpdate,false);
            } 
            if(quoteLineItemsToUpdate.size() > 0) {
               Database.update(quoteLineItemsToUpdate,false);
            }
        }
        catch(DmlException e) {
            System.debug('DML Exception occured inserting Message Queue' + e.getMessage());
        }catch(Exception ex){
            System.debug('Exception in Message Queue Insertion' + ex);
        }*/  
    }
    
    public Set<Id> getCreditMemoProductIds(){
        Set<Id> creditMemoProds = new Set<Id>();
        for(Credit_Memo_Product__c prod: [Select Id from Credit_Memo_Product__c where Credit_Memo__c IN :creditMemos.keySet()]) {   
          creditMemoProds.add(prod.Id);
            }    
        return creditMemoProds;
    }
    private Map<String, Integer> getCreditMemoProductsMap(Set<Id> cmProdSet){
        Map<String,Integer> creditMemoOpportunityMap = new Map<String,Integer>();
        Map<String,Integer> creditMemoOppMap = new Map<String,Integer>();
        Set<String> oliIdSet = new Set<String>();
        for(Credit_Memo_Product__c tempOli:[Select Id,OLI_ID__c,Request_Quantity__c from Credit_Memo_Product__c where Id IN:cmProdSet]){
           oliIdSet.add(tempOli.OLI_ID__c);  
           string key=tempOli.OLI_ID__c;
           decimal rq=tempOli.Request_Quantity__c;
           
            if(creditMemoOppMap.containsKey(key)){
                rq = rq + creditMemoOppMap.get(key);
                creditMemoOppMap.put(key,integer.valueOf(rq));//Added to SUM OLI related CreditmemoProd reuest qty--04/11/2018 by thiru.
            }else{
                creditMemoOppMap.put(key,integer.valueOf(rq));//Added to SUM OLI related CreditmemoProd reuest qty--04/11/2018 by thiru.
            }    
            creditMemoOpportunityMap.put(key,integer.valueof(rq));
        }
        //return creditMemoOpportunityMap;
        //Aggregated values of Request Quantity group by OLI ID
        /*AggregateResult[] oliAggregatedResults = [SELECT SUM(Request_Quantity__c)qtyTotal, OLI_ID__c
                      FROM Credit_Memo_Product__c where OLI_ID__c IN:oliIdSet GROUP BY OLI_ID__c];
        for(AggregateResult oliResult : oliAggregatedResults){
            system.debug('OLI_ID----->'+oliResult.get('OLI_ID__c'));
            system.debug('Total Req Qty----->'+oliResult.get('qtyTotal'));
            creditMemoOpportunityMap.put(string.valueOf(oliResult.get('OLI_ID__c')),Integer.valueOf(oliResult.get('qtyTotal')));
        }*/

        return creditMemoOpportunityMap;
    }
    
    private Map<String, Integer> getCreditMemoQuotesMap(Set<Id> cmProdSet){
        Map<String,Integer> creditMemoQuoteMap = new Map<String,Integer>();
        Map<String,Integer> creditMemoQTMap = new Map<String,Integer>();
        Set<String> qliIdSet = new Set<String>();
        for(Credit_Memo_Product__c tempOli:[Select Id,QLI_ID__c,Request_Quantity__c from Credit_Memo_Product__c where Id IN:cmProdSet]){
           qliIdSet.add(tempOli.QLI_ID__c);
           string key=tempOli.QLI_ID__c;
           decimal rq=tempOli.Request_Quantity__c;
           
            if(creditMemoQTMap.containsKey(key)){
                rq = rq + creditMemoQTMap.get(key);
                creditMemoQTMap.put(key,integer.valueOf(rq));//Added to SUM QLI related CreditmemoProd reuest qty--04/11/2018 by thiru.
            }else{
                creditMemoQTMap.put(key,integer.valueOf(rq));//Added to SUM QLI related CreditmemoProd reuest qty--04/11/2018 by thiru.
            }
            
            creditMemoQuoteMap.put(key,integer.valueof(rq));
           
        }
        //Aggregated values of Request Quantity group by QLI ID
        // MK 6/27/2016 Changed query filter from Id to QLI_ID__C
        /*AggregateResult[] qliAggregatedResults = [SELECT SUM(Request_Quantity__c)qtyTotal, QLI_ID__c
                      FROM Credit_Memo_Product__c where QLI_ID__c IN:qliIdSet GROUP BY QLI_ID__c];
        for(AggregateResult qliResult : qliAggregatedResults){
            system.debug('QLI_ID----->'+qliResult.get('QLI_ID__c'));
            system.debug('Total Req Qty----->'+qliResult.get('qtyTotal'));
            creditMemoQuoteMap.put(string.valueOf(qliResult.get('QLI_ID__c')),Integer.valueOf(qliResult.get('qtyTotal')));
        }*/  

        return creditMemoQuoteMap;
    }    
}