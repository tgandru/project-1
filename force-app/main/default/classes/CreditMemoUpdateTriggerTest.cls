/*
 * Test for Facade class and for CreditMemoTrigger.trigger links with CreditMemoUpdateTriggeHelper.cls
 * Author: Eric Vennaro
 * Edited - 22APRIL2016 : Aarthi R C
 * Edited - 29APRIL2016 : Eric Vennaro
**/

@isTest
private class CreditMemoUpdateTriggerTest {
    static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');

   @testSetup static void setupCreditMemo() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Account account = TestDataUtility.createAccount('Marshal Mathers');
        insert account;
        Account partnerAcc = TestDataUtility.createAccount('Distributor account');
        partnerAcc.RecordTypeId = directRT;
        insert partnerAcc;
        PriceBook2 priceBook = TestDataUtility.createPriceBook('Shady Records');
        insert pricebook;
        Opportunity opportunity = TestDataUtility.createOppty('New', account, pricebook, 'Managed', 'IT', 'product group',
                                    'Identified');
        insert opportunity;
        Product2 product = TestDataUtility.createProduct2('SL-SCF5300/SEG', 'Standalone Printer', 'Printer[C2]', 'H/W', 'FAX/MFP');
        insert product;
       /* Id pricebookId = Test.getStandardPricebookId();
        PricebookEntry pricebookEntry1 = TestDataUtility.createPriceBookEntry(product.Id, pricebookId);
        insert pricebookEntry1;*/
        PricebookEntry pricebookEntry = TestDataUtility.createPriceBookEntry(product.Id, priceBook.Id);
        insert pricebookEntry;

        List<OpportunityLineItem> opportunityLineItems = new List<OpportunityLineItem>();
        OpportunityLineItem op1 = TestDataUtility.createOpptyLineItem(product, pricebookEntry, opportunity);
        op1.Quantity = 100;
        op1.TotalPrice = 1000;
        op1.Requested_Price__c = 100;
        op1.Claimed_Quantity__c = 45;     
        opportunityLineItems.add(op1);
        insert opportunityLineItems;

        Quote quot = TestDataUtility.createQuote('Test Quote', opportunity, pricebook);
        quot.ROI_Requestor_Id__c=UserInfo.getUserId();
        insert quot;
        
        List<QuoteLineItem> qlis = new List<QuoteLineItem>();
        QuoteLineItem standardQLI = TestDataUtility.createQuoteLineItem(quot.Id, product, pricebookEntry, op1);
        standardQLI.Quantity = 25;
        standardQLI.UnitPrice = 100;
        standardQLI.Requested_Price__c=100;
        standardQLI.OLIID__c = op1.id;
        qlis.add(standardQLI);
        insert qlis;


        Credit_Memo__c cm = TestDataUtility.createCreditMemo();
        cm.InvoiceNumber__c = TestDataUtility.generateRandomString(6);
        cm.Direct_Partner__c = partnerAcc.id;
        insert cm;
        //OpportunityLineItem olid =[select id from OpportunityLineItem where Claimed_Quantity__c = 45 limit 1];
        List<Credit_Memo_Product__c> creditMemoProducts = new List<Credit_Memo_Product__c>();
        Credit_Memo_Product__c cmProd = TestDataUtility.createCreditMemoProduct(cm, op1);
        cmProd.Request_Quantity__c = 45;
        cmProd.QLI_ID__c = standardQLI.Id;
        //cmProd.OLI_ID__c  = olid.Id;
        cmProd.OLI_ID__c  = op1.Id;
        creditMemoProducts.add(cmProd);
        insert creditMemoProducts;
    }

    @isTest static void testApprovedUpdateClaimQuantity(){ 
        Credit_Memo__c cm1 = [SELECT Status__c 
                              FROM Credit_Memo__c 
                              LIMIT 1];
        cm1.Status__c = 'Approved';
        update cm1;
        Test.startTest();
        OpportunityLineItem oli = [SELECT Claimed_Quantity__c 
                                   FROM OpportunityLineItem 
                                   LIMIT 1];
        //System.assertEquals(oli.Claimed_Quantity__c, 45);
        Test.stopTest();
    }

    @isTest static void testUnapprovedCreditMemo(){
        Test.startTest();
        OpportunityLineItem oli = [SELECT Claimed_Quantity__c 
                                   FROM OpportunityLineItem 
                                   LIMIT 1];
        System.assertEquals(oli.Claimed_Quantity__c, null);
        Test.stopTest();
    }

    @isTest static void testNullOLI() {
        OpportunityLineItem oli = [SELECT Claimed_Quantity__c 
                                   FROM OpportunityLineItem 
                                   LIMIT 1];
       // oli.Claimed_Quantity__c = null;
       // update oli;
        Credit_Memo__c cm1 = [SELECT Status__c 
                              FROM Credit_Memo__c 
                              LIMIT 1];
        cm1.Status__c = 'Approved';
        Credit_Memo_Product__c c =[Select Id,QLI_ID__c,Request_Quantity__c from Credit_Memo_Product__c where Credit_Memo__c =:cm1.id ];
        c.QLI_ID__c=oli.id;
        update c;
        update cm1;
        Test.startTest();
        OpportunityLineItem oliFinal = [SELECT Claimed_Quantity__c 
                                   FROM OpportunityLineItem 
                                   LIMIT 1];
        //System.assertEquals(oliFinal.Claimed_Quantity__c, 45);
        Test.stopTest();
    }
}