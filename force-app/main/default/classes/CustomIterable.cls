/*
Custom interator class for roll out pagination
Created by : Jagan
*/

public class  CustomIterable implements Iterator<list<Roll_Out_Product__c>>
{ 
   list<Roll_Out_Product__c> InnerList{get; set;}
   list<Roll_Out_Product__c> ListRequested{get; set;}

   Integer i {get; set;} 
   Integer j {get;set;}
   public Integer setPageSize {get; set;} 

   Integer totalScheds {get;set;}
   List<Integer> monthDurationRequested{get;set;} //give first and last

   public CustomIterable(List<Roll_Out_Product__c> lstAccWr, Integer numSchedVar)
   {
       InnerList = new list<Roll_Out_Product__c >(); 
       ListRequested = new list<Roll_Out_Product__c >();     
       InnerList = lstAccWr;
        totalScheds=numSchedVar;
       monthDurationRequested=new List<Integer>();
       
       setPageSize = Integer.Valueof(Label.RollOutPageSize);
       i = 0; 
       j=0;

   }   

   public boolean hasNext(){ 
    System.debug('lclc i '+i+'  inner list '+InnerList.size());
       if(i >= InnerList.size()) {
           return false; 
       } else {
           return true; 
       }
   } 
   
   public boolean hasPrevious(){ 
       system.debug('I am in hasPrevious' + i);
       if(i <= setPageSize) {
           return false; 
       } else {
           return true; 
       }
   }   

   public boolean hasNextMonths(){ 
    system.debug('lclc j '+j+' total scheds '+totalScheds);
       if(j >= totalScheds) {
           return false; 
       } else {
           return true; 
       }
   } 
   
   public boolean hasPreviousMonths(){ 
       system.debug('I am in hasPreviousMonths' + i);
       if(j <= 36) {
           return false; 
       } else {
           return true; 
       }
   }  

   public list<Roll_Out_Product__c> next(){       
       system.debug('i value is ' + i);
       ListRequested = new list<Roll_Out_Product__c>(); 
       integer startNumber;
       integer size = InnerList.size();
       if(hasNext())
       {  
           if(size <= (i + setPageSize))
           {
               startNumber = i;
               i = size;
           }
           else
           {
               i = (i + setPageSize);
               startNumber = (i - setPageSize);
           }
           
           
           for(integer start = startNumber; start < i; start++)
           {
               ListRequested.add(InnerList[start]);
           }
       } 
       return ListRequested;
   } 

   public List<Integer> nextMonths(){
      System.debug('j value is '+j);
      monthDurationRequested=new List<Integer>();

      integer startNumber;
      integer size=totalScheds;

      if(hasNextMonths()){
          if(size<= (j+36)){
            startNumber=j;
            j=size;
          }else{
            j=j+36;
            startNumber=j-36;
          }

          monthDurationRequested.add(startNumber);
          monthDurationRequested.add(j-1);
      }
      return monthDurationRequested;

   }
   
   public list<Roll_Out_Product__c > previous(){      
       ListRequested = new list<Roll_Out_Product__c >(); 
       
       integer size = InnerList.size(); 
       if(i == size)
       {
           if(math.mod(size, setPageSize) > 0)
           {    
               i = size - math.mod(size, setPageSize);
           }
           else
           {
               i = (size - setPageSize);
           } 
       }
       else
       {
           i = (i - setPageSize);
       }
       
       for(integer start = (i - setPageSize); start < i; ++start)
       {
           ListRequested.add(InnerList[start]);
       } 
       return ListRequested;
   }   



   public List<Integer> previousMonths(){
    System.debug('lclc j '+j);
      monthDurationRequested=new List<Integer>();

      Integer size=totalScheds;
      if(j==size){

          if(math.mod(size,36)>0){
              j= size - math.mod(size,36);
          }else{
              j=(size - 36);
          }

      }else{
        j=(j - 36);
      }

      monthDurationRequested.add(j - 36);
      monthDurationRequested.add(j - 1);

      return monthDurationRequested;

   }
}