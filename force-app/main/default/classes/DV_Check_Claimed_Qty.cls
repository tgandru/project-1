/*
Batch Program for getting the unequal CreditMemoProducts Request Quantity with QLI claimed quantity.
Author: Steve Park.
Date  : 02/12/2018.
*/
global class DV_Check_Claimed_Qty implements Database.Batchable<sObject>, Database.stateful{
    global String result_string;
    global Map<string,integer> cmpmap;
    
    global datetime FromDate;
    global datetime ToDate;
    
    global DV_Check_Claimed_Qty(string FromDate, string ToDate){
        cmpmap = new Map<string,integer>();
        
        String Frm = FromDate+' 00:00:00.000+0000';
        String To = ToDate+' 00:00:00.000+0000';
        
        this.FromDate = DateTime.valueOf(Frm);
        this.ToDate = DateTime.valueOf(To);
        
        system.debug('FromDate-------->'+this.FromDate);
        system.debug('ToDate---------->'+this.ToDate);
        
        result_string = 'QuoteLineItemID,Quote Request Type,QLI CreatedDate,QLI LastModifiedDate,QLI Quantity,QLI Claimed Qty,CM RequestQuantity SUM,Recent CM Qty,OpportunityId,Opportunity Type,Account Type,Claimed Qty Diff\n';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        database.Querylocator ql = database.getquerylocator([select id,Claimed_quantity__c,OLIID__c,Quantity,quote.RequestType__c,createddate,lastmodifieddate,quote.OpportunityId,quote.opportunity.type,quote.Account.Recordtype.name from Quotelineitem where quote.Status!='Cancelled' and createddate>=:FromDate and Createddate<=:ToDate order by createddate asc]);
        return ql;
    }
    
    global void execute (Database.Batchablecontext BC, list<QuoteLineItem> qli_claimed){
        
        Map<String, Double> CM_req_qty = new Map<String, Double>();
        Map<id, QuoteLineItem> qli_claimed_qty = new Map<id, QuoteLineItem>(qli_claimed);
        
        for(Credit_Memo_Product__c cmp : [select id,QLI_ID__c,Credit_Memo__c,Credit_Memo__r.Status__c,request_quantity__c from Credit_Memo_Product__c 
                                          where (Credit_Memo__r.Status__c = 'Approved' or Credit_Memo__r.Status__c = 'Processed') AND QLI_ID__c IN :qli_claimed_qty.keySet() order by Credit_Memo__r.LastModifiedDate desc]){
            if(!cmpmap.containskey(cmp.QLI_ID__c)){
                cmpmap.put(cmp.QLI_ID__c,integer.valueOf(cmp.request_quantity__c));
            }
        }
        
        List<AggregateResult> CM_Product = new List <AggregateResult>();

        for ( AggregateResult p : [SELECT QLI_ID__c, SUM(Request_Quantity__c) sum_req_qty FROM Credit_Memo_Product__c where (Credit_Memo__r.Status__c = 'Approved' or Credit_Memo__r.Status__c = 'Processed') AND QLI_ID__c IN :qli_claimed_qty.keySet() GROUP BY QLI_ID__c ]) {
            String tempid = (String)p.get('QLI_ID__c');
            Id qlid = (id) tempid;
            Double sum_req_qty = (Double) p.get('sum_req_qty');
            CM_req_qty.put(qlid, sum_req_qty);
        }

        for (Id cm_qlid : CM_req_qty.keySet() ) 
        {
            
            if(qli_claimed_qty.containsKey(cm_qlid)){
                string reqType;
                if(qli_claimed_qty.get(cm_qlid).quote.RequestType__c!=null && qli_claimed_qty.get(cm_qlid).quote.RequestType__c!=''){
                    reqType = qli_claimed_qty.get(cm_qlid).quote.RequestType__c;
                }
                integer recentCMqty;
                if(cmpmap.containsKey(cm_qlid)){
                    system.debug('recent---->'+cmpmap.get(cm_qlid));
                    recentCMqty=cmpmap.get(cm_qlid);
                }
                integer ClaimedQty;
                integer ClaimedQtyDiff;//
                integer reqQtySum = integer.valueof(CM_req_qty.get(cm_qlid));//
                if(qli_claimed_qty.get(cm_qlid).Claimed_Quantity__c!=null){
                    ClaimedQty = integer.valueOf(qli_claimed_qty.get(cm_qlid).Claimed_Quantity__c);
                }
                if(ClaimedQty!=null){//
                    ClaimedQtyDiff = ClaimedQty-reqQtySum;
                }else{
                    ClaimedQtyDiff = reqQtySum;
                }//
                quotelineitem qli = qli_claimed_qty.get(cm_qlid);
                //result_string = result_string + cm_qlid + ',' +reqType+ ',' +qli_claimed_qty.get(cm_qlid).createddate+ ',' +qli_claimed_qty.get(cm_qlid).lastmodifieddate+ ',' + qli_claimed_qty.get(cm_qlid).quantity+ ',' +ClaimedQty+ ',' +CM_req_qty.get(cm_qlid)+ ',' +recentCMqty+','++','++','++','+'\r\n';
                result_string = result_string+cm_qlid+','+reqType+','+qli.createddate+','+qli.lastmodifieddate+','+qli.quantity+','+ClaimedQty+','+CM_req_qty.get(cm_qlid)+','+recentCMqty+','+qli.quote.OpportunityId+','+qli.quote.opportunity.type+','+qli.quote.account.recordtype.name+','+ClaimedQtyDiff+'\r\n';
            }
        }
    }
    
    global void finish(Database.Batchablecontext BC){
        system.debug('Used heap size------->'+Limits.getHeapSize());
        system.debug('Used heap size------->'+Limits.getLimitHeapSize());

        Blob b8 = Blob.valueof(result_string);
        Messaging.EmailFileAttachment efa8 = new Messaging.EmailFileAttachment();
        efa8.setFileName('Claim Qty Integrity Chck '+FromDate+' - '+ToDate+'.csv');
        efa8.setBody(b8);
        
        Messaging.SingleEmailMessage remail = new Messaging.SingleEmailMessage();
        list<string> emailaddresses15 = new list<string>();
        emailaddresses15.add('stevepark@samsung.com');
        emailaddresses15.add('t.gandru@partner.samsung.com');
        remail.setSubject( 'Claim Qty Integrity Chck '+FromDate+' - '+ToDate);
        remail.setToAddresses( emailaddresses15 );
        string body15 ='Hi\n\n Attached file includes Quotelineitems created between '+FromDate+' - '+ToDate+'.\n\n'+'Thanks';
        remail.setPlainTextBody(body15);
        remail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa8});
        Messaging.SendEmailResult [] r15 = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {remail});
    }

}