@isTest
public class DV_Check_Claimed_Qty_Test {
    static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
    @testSetup static void setup(){
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        
        CreditMemoIntegration__c cmi=new CreditMemoIntegration__c(Distribution_Channel__c='11',Name='Distribution Channel Value');
        insert cmi;
        
        Account account = TestDataUtility.createAccount('Marshal Mathers');
        insert account;
        Account partnerAcc = TestDataUtility.createAccount('Distributor account');
        partnerAcc.RecordTypeId = directRT;
        insert partnerAcc;
        
        PriceBook2 pb = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(4)); 
        insert pb;
        Opportunity opportunity = TestDataUtility.createOppty('New', account, pb, 'Managed', 'IT', 'product group',
                                    'Identified');
        insert opportunity;
        Product2 product = TestDataUtility.createProduct2('SL-SCF5300/SEG', 'Standalone Printer', 'Printer[C2]', 'H/W', 'FAX/MFP');
        insert product;
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry pricebookEntry = TestDataUtility.createPriceBookEntry(product.Id, pb.Id);
        insert pricebookEntry;

        List<OpportunityLineItem> opportunityLineItems = new List<OpportunityLineItem>();
        OpportunityLineItem op1 = TestDataUtility.createOpptyLineItem(product, pricebookEntry, opportunity);
        op1.Quantity = 100;
        op1.TotalPrice = 1000;
        op1.Requested_Price__c = 100;
        op1.Claimed_Quantity__c = 45;     
        opportunityLineItems.add(op1);
        insert opportunityLineItems;
        System.debug('Insert OLI success');

        Quote quot = TestDataUtility.createQuote('Test Quote', opportunity, pb);
        quot.ROI_Requestor_Id__c=UserInfo.getUserId();
        insert quot;
        
        List<QuoteLineItem> qlis = new List<QuoteLineItem>();
        QuoteLineItem standardQLI = TestDataUtility.createQuoteLineItem(quot.Id, product, pricebookEntry, op1 );
        standardQLI.Quantity = 25;
        standardQLI.UnitPrice = 100;
        standardQLI.Requested_Price__c=100;
        standardQLI.Claimed_Quantity__c=5;
        standardQLI.OLIID__c = op1.id;
        qlis.add(standardQLI);
        insert qlis;


        Credit_Memo__c cm = TestDataUtility.createCreditMemo();
        //cm.Credit_Memo_Number__c = TestDataUtility.generateRandomString(5);
        cm.InvoiceNumber__c = TestDataUtility.generateRandomString(6);
        cm.Direct_Partner__c = partnerAcc.id;
        cm.Status__c = 'Approved';
        insert cm;
      
        List<Credit_Memo_Product__c> creditMemoProducts = new List<Credit_Memo_Product__c>();
        Credit_Memo_Product__c cmProd = TestDataUtility.createCreditMemoProduct(cm, op1);
        cmProd.Request_Quantity__c = 35;
        cmProd.QLI_ID__c = standardQLI.id;
        creditMemoProducts.add(cmProd);
        insert creditMemoProducts;
        
    }
    
    @isTest static void RecordsToProcessInBatch(){
        Test.startTest();
            string fromDate = string.valueof(system.today()-1);
            fromDate = fromDate.left(10);
            string todate = string.valueof(system.today()+1);
            todate = todate.left(10);
            DV_Check_Claimed_Qty ba = new DV_Check_Claimed_Qty(fromDate,todate);
            Database.executeBatch(ba);
        Test.stopTest();
    }
}