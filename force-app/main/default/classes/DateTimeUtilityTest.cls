/* Test class for DateTimeUtility
 * Author: Bala Rajasekharan
 */
@isTest
private class DateTimeUtilityTest {

    static testMethod void testLocaleToTimeFormatMap() {       
        
        Map<String, String> results = DateTimeUtility.LocaleToTimeFormatMap();
        System.assertEquals(results.get('da_DK'), 'dd-MM-yyyy HH:mm');
        
        
    }
}