global with sharing class DeleteAssetClass {

	static Integer deviceListSize;

	static webservice String deleteSelectedDevice(List<String> deviceIdList)
	{
		List<Device__c> deviceList = [Select Id FROM Device__c WHERE Id IN: deviceIdList AND Status__c = 'Invalid'];
		deleteDevice(deviceList);
		return deviceListSize + ' Device records were successfully deleted';
	}

	static webservice String deleteAllInvalidDevice(String acctId)
	{
		List<Device__c> deviceList = [Select Id FROM Device__c WHERE Account__c =: acctId AND Status__c = 'Invalid'];
		deleteDevice(deviceList);
		return deviceListSize + ' Device records were successfully deleted';
	}

	private static void deleteDevice(List<Device__c> deviceList)
	{
		deviceListSize = deviceList.size();
		
		if(!deviceList.isEmpty())
			delete deviceList;
	}
}