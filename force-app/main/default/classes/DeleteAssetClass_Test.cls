@isTest(SeeAllData = false)
public with sharing class DeleteAssetClass_Test {
    
    static testmethod void testDeleteAsset()
    {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        List<Device__c> devList = new List<Device__c>();
        
        Id tempAcctId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Temporary').getRecordTypeId();

        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = tempAcctId,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        );
        insert testAccount1;

        Device__c dev = new Device__c(Account__c = testAccount1.Id, IMEI__c = '123456789011');
        Device__c dev1 = new Device__c(Account__c = testAccount1.Id, IMEI__c = '12345678901113');
        
        devList.add(dev);
        devList.add(dev1);
        insert devList;
        
        List<String> devIdList = new List<String>{dev.Id};
        
        test.startTest();
        DeleteAssetClass.deleteSelectedDevice(devIdList);
        DeleteAssetClass.deleteAllInvalidDevice(testAccount1.Id);
        test.StopTest();
    }

}