public class DeleteCreditMemoQuoteExt {
    
    //reference the api name of the flow record
    Public Flow.Interview.Delete_Credit_Memo_Line_Items_and_Quote myAutoFlow { get; set; }
    Public DeleteCreditMemoQuoteExt(ApexPages.StandardController controller) {}



  public String getmyID() {

    if (myAutoFlow==null)
       return '';

    else 
        //Put flow variable that represents the id of newly created record
      return myAutoFlow.varCreditMemoID;
    }


  public PageReference getNextPage(){
  PageReference p = new PageReference('/' + getmyID() );
  p.setRedirect(true);
  return p;
  }

}