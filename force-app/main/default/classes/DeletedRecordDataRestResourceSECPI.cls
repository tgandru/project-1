/**
 *  https://samsungsea--qa.cs65.my.salesforce.com/services/apexrest/DeletedRecordDataSECPI
 */
@RestResource(urlMapping='/DeletedRecordDataSECPI/*')
global with sharing class DeletedRecordDataRestResourceSECPI {
    @HttpPost
    global static SFDCStubSECPI.DeleteRecordResponseSECPI getDeleteRecordDataListSECPI() {
        integer pageNum;
        
        integer pageLimit = integer.valueOf(label.SECPI_DeletedData_I_F_Page_Limit);
        Integration_EndPoints__c endpoint = Integration_EndPoints__c.getInstance(SFDCStubSECPI.FLOW_084);
        String layoutId = !Test.isRunningTest() ? endpoint.layout_id__c : SFDCStubSECPI.LAYOUTID_084;
        
        RestRequest req = RestContext.request;
        String postBody = req.requestBody.toString();
        
        SFDCStubSECPI.DeleteRecordResponseSECPI response = new SFDCStubSECPI.DeleteRecordResponseSECPI();
        
        SFDCStubSECPI.DeleteRecordRequest request = null;
        try {
            request = (SFDCStubSECPI.DeleteRecordRequest)json.deserialize(postBody, SFDCStubSECPI.DeleteRecordRequest.class);
            system.debug('Request======>'+request);
        } catch(Exception e) {
            response.inputHeaders.MSGGUID = ''; response.inputHeaders.IFID  = ''; response.inputHeaders.IFDate    = ''; response.inputHeaders.MSGSTATUS = 'F';
            response.inputHeaders.ERRORTEXT = 'Invalid Request Message. - '+postBody; response.inputHeaders.ERRORCODE = '';
            
            return response;
        }
        response.inputHeaders.MSGGUID = request.inputHeaders.MSGGUID;
        response.inputHeaders.IFID    = request.inputHeaders.IFID;
        response.inputHeaders.IFDate  = request.inputHeaders.IFDate;
        
        string dates = 'From:'+request.body.SEARCH_DTTM_FROM+', To:'+request.body.SEARCH_DTTM_TO;
        
        List<message_queue__c> mqList = new List<message_queue__c>();
        message_queue__c mq = new message_queue__c(MSGUID__c = response.inputHeaders.MSGGUID, 
                                                    MSGSTATUS__c = 'S',  
                                                    Integration_Flow_Type__c = SFDCStubSECPI.FLOW_084,
                                                    IFID__c = response.inputHeaders.IFID,
                                                    IFDate__c = response.inputHeaders.IFDate,
                                                    External_Id__c = request.body.CONSUMER,
                                                    Status__c = 'success',
                                                    Object_Name__c = 'DeletedRecordData', 
                                                    retry_counter__c = 0,
                                                    Identification_Text__c = dates);
                                                    
        //logic for PAGENO Limit
        if(request.body.PAGENO!=null){
            pageNum = request.body.PAGENO;
            System.debug('PageNum----->'+pageNum);
        }else{
            String errmsg = 'Page Number is Missing in the Request';
            response.inputHeaders.MSGSTATUS = 'F';
            response.inputHeaders.ERRORTEXT = errmsg;
            response.inputHeaders.ERRORCODE = '';
            
            mq.Status__c = 'failed';
            mq.MSGSTATUS__c = 'F';
            mq.ERRORTEXT__c = errmsg;
            insert mq;
            
            return response;
        }
        
        if(pageNum>pageLimit){
            Datetime new_SEARCH_DTTM = endpoint.Last_Successful_Run_Last_Modified_Date__c;
            System.debug('new SERACH_DTTM----->'+new_SEARCH_DTTM);
            String errmsg = 'SFDC PAGE LIMIT EXCEEDED. Please use new SEARCH_DTTM_FROM:'+new_SEARCH_DTTM+' and start from PAGENO:1 ' ;
            
            response.inputHeaders.MSGSTATUS = 'F';
            response.inputHeaders.ERRORTEXT = errmsg;
            response.inputHeaders.ERRORCODE = 'PAGE_LIMIT_EXCEEDED';
            response.inputHeaders.SEARCH_DTTM_FROM = SFDCStubSECPI.DtTimetoString(new_SEARCH_DTTM);
            
            mq.Status__c = 'failed';
            mq.MSGSTATUS__c = 'F';
            mq.ERRORTEXT__c = errmsg;
            insert mq;
            
            return response;
        }
        //---Ends Here
        
        //Block the callout if the Sent data delete batch is in process.
        List<AsyncApexJob> DeleteBatchProcessing = [select id,TotalJobItems,status,createddate from AsyncApexJob where apexclass.name='Batch_DeletedDataToSECPI_Delete' and (status='Holding' or status='Queued' or status='Preparing' or status='Processing') order by createddate desc limit 1];
        if(DeleteBatchProcessing.size()>0){    
            response.inputHeaders.MSGSTATUS = 'F'; response.inputHeaders.ERRORTEXT = 'The Sent Data Delete Batch is in Process. Try callout after 10 mins'; response.inputHeaders.ERRORCODE = '';
            mq.Status__c = 'failed'; mq.MSGSTATUS__c = 'F'; mq.ERRORTEXT__c = 'The Sent Data Delete Batch is in Process. Try callout after 10 mins';
            insert mq;
            
            return response;
        }
        
        string DateFormat = 'yyyymmddhhmmss';
        string DateFormat2 = 'yyyymmdd';
        
        //Added the logic for New request format. Request includes FROM and TO SEARCH_DTTM--05/16/2018
        //Checking the date format.
        if((request.body.SEARCH_DTTM_FROM!=null && request.body.SEARCH_DTTM_FROM!='' && request.body.SEARCH_DTTM_FROM.length()!=DateFormat.length() && request.body.SEARCH_DTTM_FROM.length()!=DateFormat2.length())
            || (request.body.SEARCH_DTTM_TO!=null && request.body.SEARCH_DTTM_TO!='' && request.body.SEARCH_DTTM_TO.length()!=DateFormat.length() && request.body.SEARCH_DTTM_TO.length()!=DateFormat2.length())){
            String errmsg = 'Incorrect SEARCH_DTTM format. The Date Format must be yyyymmddhhmmss or yyyymmdd';
            response.inputHeaders.MSGSTATUS = 'F';
            response.inputHeaders.ERRORTEXT = errmsg;
            response.inputHeaders.ERRORCODE = 'Incorrect SEARCH_DTTM format';
            
            mq.Status__c = 'failed';
            mq.MSGSTATUS__c = 'F';
            mq.ERRORTEXT__c = errmsg;
            insert mq;
            
            return response;
        }
        
        //Check the Missng SEARCH_DTTM_TO and SEARCH_DTTM_FROM
        if((((request.body.SEARCH_DTTM_TO==null || request.body.SEARCH_DTTM_TO=='') && request.body.SEARCH_DTTM_FROM!=null && request.body.SEARCH_DTTM_FROM!=''))
            || (((request.body.SEARCH_DTTM_FROM==null || request.body.SEARCH_DTTM_FROM=='') && request.body.SEARCH_DTTM_TO!=null && request.body.SEARCH_DTTM_TO!=''))){
            String errmsg = 'SEARCH_DTTM_FROM or SEARCH_DTTM_TO is missing. The request should include both FROM and TO dates while sending TimeStamps';
            response.inputHeaders.MSGSTATUS = 'F';
            response.inputHeaders.ERRORTEXT = errmsg;
            response.inputHeaders.ERRORCODE = '';
            
            mq.Status__c = 'failed';
            mq.MSGSTATUS__c = 'F';
            mq.ERRORTEXT__c = errmsg;
            insert mq;
            
            return response;
        }
        
        if(request.body.SEARCH_DTTM_FROM!=null && request.body.SEARCH_DTTM_FROM!='' && request.body.SEARCH_DTTM_FROM.length()==DateFormat2.length()){
            request.body.SEARCH_DTTM_FROM=request.body.SEARCH_DTTM + '000000';
        }
        if(request.body.SEARCH_DTTM_TO!=null && request.body.SEARCH_DTTM_TO!='' && request.body.SEARCH_DTTM_TO.length()==DateFormat2.length()){
            request.body.SEARCH_DTTM_TO=request.body.SEARCH_DTTM + '000000';
        }
        
        DateTime lastSuccessfulRun = endpoint.last_successful_run__c;
        DateTime currentDateTime = datetime.now();
        DateTime SearchDateTo;
        DateTime SearchDateFrom;
        
        //Set From and To timestamps for Query
        if((request.body.SEARCH_DTTM_FROM!=null && request.body.SEARCH_DTTM_FROM!='')
            &&(request.body.SEARCH_DTTM_TO!=null && request.body.SEARCH_DTTM_TO!='')){
            SearchDateTo = setDateFromString(request.body.SEARCH_DTTM_TO);
            system.debug('SearchDateTo----->'+SearchDateTo);
            SearchDateFrom = setDateFromString(request.body.SEARCH_DTTM_FROM);
            system.debug('SearchDateFrom----->'+SearchDateFrom);
        }else{
            SearchDateTo = currentDateTime;
            system.debug('SearchDateTo----->'+SearchDateTo);
            SearchDateFrom = lastSuccessfulRun;
        }
        
        //Checing the SEARCH_DTTM_FROM and SEARCH_DTTM_TO time difference
        integer SecsDiff = Integer.valueOf((SearchDateTo.getTime() - SearchDateFrom.getTime())/(1000));
        if(SecsDiff>86400){
            String errmsg = 'Difference between SEARCH_DTTM_FROM and SEARCH_DTTM_TO should be less or Equal to 1 day';
            response.inputHeaders.MSGSTATUS = 'F';
            response.inputHeaders.ERRORTEXT = errmsg;
            response.inputHeaders.ERRORCODE = '';
            
            mq.Status__c = 'failed';
            mq.MSGSTATUS__c = 'F';
            mq.ERRORTEXT__c = errmsg;
            insert mq;
            
            return response;
        }
        
        Integer limitCnt = SFDCStubSECPI.DelRec_LIMIT_COUNT;//Set no. of records to send per page
        
        //Get all sent data
        Set<string> sentIds = new Set<string>();
        List<Deleted_Data_To_SECPI__c> AllData = Deleted_Data_To_SECPI__c.getall().values();
        system.debug('size------->'+AllData.size());
        for(Deleted_Data_To_SECPI__c dataSend : AllData){
            sentIds.add(dataSend.name);
        }
        
        List<Deleted_Data_To_SECPI__c> storeIds = new List<Deleted_Data_To_SECPI__c>();
        
        List<Opportunity> data = null;
                       
        response.inputHeaders.MSGSTATUS = 'S';
        response.inputHeaders.ERRORTEXT = '';
        response.inputHeaders.ERRORCODE = '';
        
        response.body.PAGENO = request.body.PAGENO;
        response.body.ROWCNT = limitCnt;

        DateTime LastModifiedDate = null;
        string status;
        integer DataSize=0;

        for(Deleted_Record_History__c delRec : [SELECT Id, Record_ID__c, Object__c, Record_CreatedDate__c, Record_LastModifiedDate__c, Record_SystemModStamp__c, SystemModStamp 
                                                FROM Deleted_Record_History__c 
                                                WHERE Record_Type__c!='HA Builder' and 
                                                      systemModStamp>=:SearchDateFrom and 
                                                      systemModStamp<=:SearchDateTo and 
                                                      id!=:sentIds 
                                                      ORDER BY systemModStamp ASC LIMIT :limitCnt])
        {
            try{
                SFDCStubSECPI.WrapDeleteRecordSECPI rec = new SFDCStubSECPI.WrapDeleteRecordSECPI();
                rec.Id = delRec.Record_ID__c;
                rec.ObjectName = delRec.Object__c;
                rec.Createddate = SFDCStubSECPI.toString(delRec.Record_CreatedDate__c);
                rec.Lastmodifieddate = SFDCStubSECPI.toString(delRec.Record_LastModifiedDate__c);
                rec.systemModStamp = SFDCStubSECPI.toString(delRec.Record_SystemModStamp__c);
                
                SFDCStubSECPI.DeleteRecordSECPI obj = new SFDCStubSECPI.DeleteRecordSECPI(rec);
                response.body.deleteRecordData.add(obj);
                //system.debug('Record--------->'+response.body.deleteRecordData);
                
                Deleted_Data_To_SECPI__c storeId = new Deleted_Data_To_SECPI__c(name=delRec.id);
                if(!sentIds.contains(delRec.id)){
                    storeIds.add(storeId);   
                }
                DataSize++;
                
                LastModifiedDate = delRec.SystemModStamp;
                
            }catch(Exception e) {
                String errmsg = 'Fail to create an DeletedRecordData - '+e.getmessage() + ' - '+e.getStackTraceString();
                message_queue__c pemsg = new message_queue__c(MSGUID__c = response.inputHeaders.MSGGUID,  MSGSTATUS__c = 'F', Integration_Flow_Type__c = SFDCStubSECPI.FLOW_084, IFID__c = response.inputHeaders.IFID, IFDate__c = response.inputHeaders.IFDate, Status__c = 'failed', Object_Name__c = 'DeletedRecordData', retry_counter__c = 0);
                pemsg.ERRORTEXT__c = errmsg; pemsg.Identification_Text__c = delRec.Id;
                mqList.add(pemsg);
                System.debug(errmsg);
            }
        }
        
        /*if(DataSize<limitCnt){
            integer rosQueryLimit = DataSize-limitCnt;*/
            string HArecTypeId = Label.HA_Builder_Recordtypeid;
            
            for(Roll_Out_Schedule__c ros : [SELECT Id, Plan_Date__c, Plan_Revenue__c, MonthYear__c, Plan_Quantity__c, Createddate, Lastmodifieddate, systemModStamp 
                                            FROM Roll_Out_Schedule__c 
                                            WHERE id!=:sentIds and 
                                                  isdeleted=true and 
                                                  Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.RecordTypeId!=:HArecTypeId and 
                                                  systemModStamp>=:SearchDateFrom and 
                                                  systemModStamp<=:SearchDateTo 
                                                  ORDER BY systemModStamp ASC LIMIT :limitCnt ALL ROWS])
            {
                try{
                    SFDCStubSECPI.WrapDeleteRecordSECPI rec = new SFDCStubSECPI.WrapDeleteRecordSECPI();
                    rec.Id = ros.Id;
                    rec.ObjectName = 'Rollout Schedule';
                    rec.Createddate = SFDCStubSECPI.toString(ros.Createddate);
                    rec.Lastmodifieddate = SFDCStubSECPI.toString(ros.Lastmodifieddate);
                    rec.systemModStamp = SFDCStubSECPI.toString(ros.systemModStamp);
                    
                    SFDCStubSECPI.DeleteRecordSECPI obj = new SFDCStubSECPI.DeleteRecordSECPI(rec);
                    response.body.deleteRecordData.add(obj);
                    LastModifiedDate = ros.SystemModStamp;
    
                    Deleted_Data_To_SECPI__c storeId = new Deleted_Data_To_SECPI__c(name=ros.id);
                    if(!sentIds.contains(ros.id)){
                        storeIds.add(storeId);   
                    }
                    DataSize++;
                    
                }catch(Exception e) {
                    String errmsg = 'Fail to create an DeletedRecordData - '+e.getmessage() + ' - '+e.getStackTraceString();
                    message_queue__c pemsg = new message_queue__c(MSGUID__c = response.inputHeaders.MSGGUID, MSGSTATUS__c = 'F', Integration_Flow_Type__c = SFDCStubSECPI.FLOW_084, IFID__c = response.inputHeaders.IFID, IFDate__c = response.inputHeaders.IFDate, Status__c = 'failed', Object_Name__c = 'DeletedRecordData', retry_counter__c = 0);
                    pemsg.ERRORTEXT__c = errmsg; pemsg.Identification_Text__c = ros.Id;
                    mqList.add(pemsg);
                    System.debug(errmsg);
                }
            }
        //}
        system.debug('Data Size----->'+DataSize);
        response.body.COUNT  = DataSize;
        
        mq.last_successful_run__c = currentDateTime;
        mq.Object_Name__c = mq.Object_Name__c + ', ' + request.body.PAGENO + ', ' + response.body.COUNT;
        mqList.add(mq);
        if(mqList.size()>0){
            insert mqList;
        }
        
        endpoint.Last_Successful_Run_Last_Modified_Date__c = LastModifiedDate;
        
        if(!Test.isRunningTest() && response.body.COUNT!=0 && response.body.ROWCNT<=response.body.COUNT && pageNum<pageLimit) {
            insert storeIds;
            system.debug('<-------Insert sent data in Page------->Page::'+pageNum);
        }else if(response.body.COUNT!=0 && (response.body.ROWCNT>response.body.COUNT || pageNum==pageLimit)) {
            endpoint.last_successful_run__c = currentDateTime;
            update endpoint;
            
            //Delete sent data for final page
            integer count= database.countQuery('select count() from Deleted_Data_To_SECPI__c');
            if(count>10000){
                Batch_DeletedDataToSECPI_Delete ba = new Batch_DeletedDataToSECPI_Delete();
                Database.executeBatch(ba);
            }else{
                List<Deleted_Data_To_SECPI__c> deleteData = [select id,name from Deleted_Data_To_SECPI__c];
                delete deleteData;
            }
            system.debug('<-------Final Page------->Page::'+pageNum);
        }
        //system.debug('Response==============>'+response);
        return response;
    }
    
    //Method to convert string to datetime
    public static datetime setDateFromString(string dt){
        string yyyymm = dt.left(6);
        string mm = yyyymm.right(2);
        string yyyy = dt.left(4);
        string yyyymmdd = dt.left(8);
        string dd = yyyymmdd.right(2);
        string finalstr = mm+'/'+dd+'/'+yyyy;
        date d = Date.parse(finalstr);
        system.debug('date----->'+d);
        
        string hhmmss = dt.right(6);
        string hhmm   = hhmmss.left(4);
        integer hh = integer.valueOf(hhmm.left(2));
        integer min = integer.valueOf(hhmm.right(2));
        integer ss = integer.valueOf(hhmmss.right(2));
        
        time t = time.newInstance(hh,min,ss, 0);
        datetime dtime= DateTime.newinstance(d,t);
        
        Timezone tz = Timezone.getTimeZone('America/New_York');
        Integer timeDiff = tz.getOffset(dtime);
        if(timeDiff==-18000000){
            dtime = dtime.addhours(-5);
        }else{
            dtime = dtime.addhours(-4);
        }
        System.debug('dtime----->'+dtime);
        return dtime;
    }
    
}