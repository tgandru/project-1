@isTest(seealldata=true)
private class DeletedRecordDataRestResourceSECPITest {
	private static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
   
    static testMethod void getDeleteRecordDataListSECPITest() {
        
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Set<Id> oliSetIds = new Set<Id>();
        Map<Id, Id> ropToOLIMap = new Map<Id,Id>();
        Account account = TestDataUtility.createAccount('Marshal Mathers');
        insert account;
        Account partnerAcc = TestDataUtility.createAccount('Distributor account');
        partnerAcc.RecordTypeId = directRT;
        insert partnerAcc;
        
        //Creating custom Pricebook
        PriceBook2 pb = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(4)); 
        insert pb;
        Opportunity opportunity = TestDataUtility.createOppty('New', account, pb, 'Managed', 'IT', 'product group', 'Identified');
        insert opportunity;
        Partner__c  partner= TestDataUtility.createPartner(opportunity, account, partnerAcc, 'Distributor');
        partner.Is_Primary__c=true;
        insert partner; 
        Product2 product = TestDataUtility.createProduct2('SL-SCF5300/SEG1', 'Standalone Printer', 'Printer[C2]', 'H/W', 'FAX/MFP');
        insert product;
        Product2 product1 = TestDataUtility.createProduct2('SL-SCF5301/SEG1', 'Standalone Printer', 'Printer[C2]', 'H/W', 'FAX/MFP');
        insert product1;
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry pricebookEntry = TestDataUtility.createPriceBookEntry(product.Id, pb.Id);
        pricebookEntry.unitprice=110;
        insert pricebookEntry;
        
        List<OpportunityLineItem> opportunityLineItems = new List<OpportunityLineItem>();
        OpportunityLineItem op1 = TestDataUtility.createOpptyLineItem(product, pricebookEntry, opportunity);
        op1.Quantity = 100;
        op1.TotalPrice = 1000;
        op1.Requested_Price__c = 100;
        op1.Claimed_Quantity__c = 45;
        opportunityLineItems.add(op1);
        insert opportunityLineItems;
        
        opportunity.StageName = 'Qualified';
        opportunity.CloseDate=System.today();
        opportunity.Roll_Out_Start__c=System.today().addDays(61);
        opportunity.Rollout_Duration__c=5;
        
        update opportunity;
        
        Deleted_Record_History__c delRec = new Deleted_Record_History__c(Record_ID__c='00K000000AcFrTV', Object__c='Opportunity Product', Record_CreatedDate__c=System.today(), Record_LastModifiedDate__c=System.today(), Record_SystemModStamp__c=System.today());
        insert delRec;
        
        Roll_Out__c rollout = TestDataUtility.createRollOuts('RollOut-New',opportunity);
        
        string SearchDtmTo = string.valueOf(system.now().addHours(4));
        SearchDtmTo = SearchDtmTo.replaceAll( '\\s+', '');
        SearchDtmTo = SearchDtmTo.remove(':');
        SearchDtmTo = SearchDtmTo.remove('-');
        
        string SearchDtmFrom = string.valueOf(system.now().addHours(-10));
        SearchDtmFrom = SearchDtmFrom.replaceAll( '\\s+', '');
        SearchDtmFrom = SearchDtmFrom.remove(':');
        SearchDtmFrom = SearchDtmFrom.remove('-');
        
        SFDCStubSECPI.DeleteRecordRequest reqData1 = new SFDCStubSECPI.DeleteRecordRequest();
        
        reqData1.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData1.inputHeaders.IFID 	 = SFDCStubSECPI.LAYOUTID_084;
        reqData1.body.CONSUMER = 'SECPIMSTR';
        reqData1.body.SEARCH_DTTM_FROM = SearchDtmFrom;
        reqData1.body.SEARCH_DTTM_TO = SearchDtmTo;
        reqData1.body.PAGENO = 1;
        
        
        Test.startTest();
        
	   	//DeletedRecordDataSECPI
	   	String jsonMsg = JSON.serialize(reqData1);
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/DeletedRecordDataSECPI';  //Request URL
        req1.httpMethod = 'POST';//HTTP Request Type
        req1.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req1;
        RestContext.response= res1;
        
        SFDCStubSECPI.DeleteRecordResponseSECPI resData1 = DeletedRecordDataRestResourceSECPI.getDeleteRecordDataListSECPI();
        System.debug(resData1.inputHeaders.MSGSTATUS);
        System.debug(resData1.inputHeaders.ERRORTEXT);
        System.assertEquals('S', resData1.inputHeaders.MSGSTATUS);
	   	
	   	Test.stopTest();
    }
    
    static testMethod void MissingFromDateTest() {
        
        string SearchDtmTo = string.valueOf(system.now().addHours(4));
        SearchDtmTo = SearchDtmTo.replaceAll( '\\s+', '');
        SearchDtmTo = SearchDtmTo.remove(':');
        SearchDtmTo = SearchDtmTo.remove('-');
        
        SFDCStubSECPI.DeleteRecordRequest reqData1 = new SFDCStubSECPI.DeleteRecordRequest();
        
        reqData1.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData1.inputHeaders.IFID 	 = SFDCStubSECPI.LAYOUTID_084;
        reqData1.body.CONSUMER = 'SECPIMSTR';
        reqData1.body.SEARCH_DTTM_FROM = '';
        reqData1.body.SEARCH_DTTM_TO = SearchDtmTo;
        reqData1.body.PAGENO = 1;
        
        Test.startTest();
	   	
	   	//DeletedRecordDataSECPI
	   	string jsonMsg = JSON.serialize(reqData1);
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/DeletedRecordDataSECPI';  //Request URL
        req1.httpMethod = 'POST';//HTTP Request Type
        req1.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req1;
        RestContext.response= res1;
        
        SFDCStubSECPI.DeleteRecordResponseSECPI resData1 = DeletedRecordDataRestResourceSECPI.getDeleteRecordDataListSECPI();
        System.assertEquals('F', resData1.inputHeaders.MSGSTATUS);
	   	
	   	Test.stopTest();
    }
    
    static testMethod void MissingToDateTest() {
        
        string SearchDtmFrom = string.valueOf(system.now().addHours(-10));
        SearchDtmFrom = SearchDtmFrom.replaceAll( '\\s+', '');
        SearchDtmFrom = SearchDtmFrom.remove(':');
        SearchDtmFrom = SearchDtmFrom.remove('-');
        
        SFDCStubSECPI.DeleteRecordRequest reqData1 = new SFDCStubSECPI.DeleteRecordRequest();
        
        reqData1.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData1.inputHeaders.IFID 	 = SFDCStubSECPI.LAYOUTID_084;
        reqData1.body.CONSUMER = 'SECPIMSTR';
        reqData1.body.SEARCH_DTTM_FROM = SearchDtmFrom;
        reqData1.body.SEARCH_DTTM_TO = '201805220000';
        reqData1.body.PAGENO = 1;
        
        Test.startTest();
	   	
	   	//DeletedRecordDataSECPI
	   	string jsonMsg = JSON.serialize(reqData1);
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/DeletedRecordDataSECPI';  //Request URL
        req1.httpMethod = 'POST';//HTTP Request Type
        req1.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req1;
        RestContext.response= res1;
        
        SFDCStubSECPI.DeleteRecordResponseSECPI resData1 = DeletedRecordDataRestResourceSECPI.getDeleteRecordDataListSECPI();
        System.assertEquals('F', resData1.inputHeaders.MSGSTATUS);
	   	
	   	Test.stopTest();
    }
    
    static testMethod void MissingPageNoTest() {
        
        string SearchDtmTo = string.valueOf(system.now().addHours(4));
        SearchDtmTo = SearchDtmTo.replaceAll( '\\s+', '');
        SearchDtmTo = SearchDtmTo.remove(':');
        SearchDtmTo = SearchDtmTo.remove('-');
        
        string SearchDtmFrom = string.valueOf(system.now().addHours(-26));
        SearchDtmFrom = SearchDtmFrom.replaceAll( '\\s+', '');
        SearchDtmFrom = SearchDtmFrom.remove(':');
        SearchDtmFrom = SearchDtmFrom.remove('-');
        
        SFDCStubSECPI.DeleteRecordRequest reqData1 = new SFDCStubSECPI.DeleteRecordRequest();
        
        reqData1.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData1.inputHeaders.IFID 	 = SFDCStubSECPI.LAYOUTID_084;
        reqData1.body.CONSUMER = 'SECPIMSTR';
        reqData1.body.SEARCH_DTTM_FROM = SearchDtmFrom;
        reqData1.body.SEARCH_DTTM_TO = SearchDtmTo;
        
        Test.startTest();
	   	
	   	//DeletedRecordDataSECPI
	   	string jsonMsg = JSON.serialize(reqData1);
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/DeletedRecordDataSECPI';  //Request URL
        req1.httpMethod = 'POST';//HTTP Request Type
        req1.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req1;
        RestContext.response= res1;
        
        SFDCStubSECPI.DeleteRecordResponseSECPI resData1 = DeletedRecordDataRestResourceSECPI.getDeleteRecordDataListSECPI();
        System.assertEquals('F', resData1.inputHeaders.MSGSTATUS);
	   	
	   	Test.stopTest();
    }
    
    static testMethod void PageLimitTest() {
        
        string SearchDtmTo = string.valueOf(system.now().addHours(4));
        SearchDtmTo = SearchDtmTo.replaceAll( '\\s+', '');
        SearchDtmTo = SearchDtmTo.remove(':');
        SearchDtmTo = SearchDtmTo.remove('-');
        
        string SearchDtmFrom = string.valueOf(system.now().addHours(-26));
        SearchDtmFrom = SearchDtmFrom.replaceAll( '\\s+', '');
        SearchDtmFrom = SearchDtmFrom.remove(':');
        SearchDtmFrom = SearchDtmFrom.remove('-');
        
        SFDCStubSECPI.DeleteRecordRequest reqData1 = new SFDCStubSECPI.DeleteRecordRequest();
        
        reqData1.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData1.inputHeaders.IFID 	 = SFDCStubSECPI.LAYOUTID_084;
        reqData1.body.CONSUMER = 'SECPIMSTR';
        reqData1.body.SEARCH_DTTM_FROM = SearchDtmFrom;
        reqData1.body.SEARCH_DTTM_TO = SearchDtmTo;
        reqData1.body.PAGENO = 501;
        
        Test.startTest();
	   	
	   	//DeletedRecordDataSECPI
	   	string jsonMsg = JSON.serialize(reqData1);
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/DeletedRecordDataSECPI';  //Request URL
        req1.httpMethod = 'POST';//HTTP Request Type
        req1.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req1;
        RestContext.response= res1;
        
        SFDCStubSECPI.DeleteRecordResponseSECPI resData1 = DeletedRecordDataRestResourceSECPI.getDeleteRecordDataListSECPI();
        System.assertEquals('F', resData1.inputHeaders.MSGSTATUS);
	   	
	   	Test.stopTest();
    }
    
    static testMethod void DateLimitTest() {
        
        string SearchDtmTo = string.valueOf(system.now().addHours(4));
        SearchDtmTo = SearchDtmTo.replaceAll( '\\s+', '');
        SearchDtmTo = SearchDtmTo.remove(':');
        SearchDtmTo = SearchDtmTo.remove('-');
        
        string SearchDtmFrom = string.valueOf(system.now().addDays(-2));
        SearchDtmFrom = SearchDtmFrom.replaceAll( '\\s+', '');
        SearchDtmFrom = SearchDtmFrom.remove(':');
        SearchDtmFrom = SearchDtmFrom.remove('-');
        
        SFDCStubSECPI.DeleteRecordRequest reqData1 = new SFDCStubSECPI.DeleteRecordRequest();
        
        reqData1.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData1.inputHeaders.IFID 	 = SFDCStubSECPI.LAYOUTID_084;
        reqData1.body.CONSUMER = 'SECPIMSTR';
        reqData1.body.SEARCH_DTTM_FROM = SearchDtmFrom;
        reqData1.body.SEARCH_DTTM_TO = SearchDtmTo;
        reqData1.body.PAGENO = 1;
        
        Test.startTest();
	   	
	   	//DeletedRecordDataSECPI
	   	string jsonMsg = JSON.serialize(reqData1);
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/DeletedRecordDataSECPI';  //Request URL
        req1.httpMethod = 'POST';//HTTP Request Type
        req1.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req1;
        RestContext.response= res1;
        
        SFDCStubSECPI.DeleteRecordResponseSECPI resData1 = DeletedRecordDataRestResourceSECPI.getDeleteRecordDataListSECPI();
        System.assertEquals('F', resData1.inputHeaders.MSGSTATUS);
	   	
	   	Test.stopTest();
    }
}