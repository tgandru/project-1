public class DemoApprovalRequestController_ltng {
   
   @AuraEnabled
    public Static String SubmitdemoforApproval(String recordid){
        String message='';
        Order ord=[Select id,Name,CustomerAuthorizedById,HasLineItems__c from Order where id=:recordid];
        if(ord.CustomerAuthorizedById ==null){//Check Demo Contact Present or Not
            message='Demo Contact is missing. Please select Demo Contact and submit for Approval';
        }else if(ord.HasLineItems__c !=null && ord.HasLineItems__c >0){
            try{
                 Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                   req1.setObjectId(ord.Id);
                   req1.setComments('Discount Approval Process.');
                   Approval.ProcessResult processResult = Approval.process(req1);
                if(processResult.isSuccess()){
                    message='The Demo has been successfully submitted for approval.'; 
                }else{
                    message='Failed to submit for approval because  '+processResult.getErrors(); 
                }
                
                
            }catch(exception ex){
              message='Failed to submit for approval because  '+ex.getMessage();  
            } 
        }else{
            message='Please enter demo products for this demo to be eligible for approval.';
        }
        
        return message;
    }
    
}