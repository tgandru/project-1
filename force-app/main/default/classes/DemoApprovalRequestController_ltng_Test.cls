@isTest
private class DemoApprovalRequestController_ltng_Test {

static Account testMainAccount;
    static Account testCustomerAccount;
    static List<Opportunity> testOpportunityList;
    static Order orderRecord1;
    static Order orderRecord2;
    static Order orderRecord3;
    static Pricebook2 pb;
    static PricebookEntry standaloneProdPBE;
    static Id endUserRT = TestDataUtility.retrieveRecordTypeId('End_User', 'Account'); 
    static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
    static Id demoAcctRtId = TestDataUtility.retrieveRecordTypeId('ADL','Order');
    static Id demoOppRtId1 = TestDataUtility.retrieveRecordTypeId('Evaluation','Order');
    
    private static void setup() {
        //Jagan: Test data for fiscalCalendar__c
        Test.loadData(fiscalCalendar__c.sObjectType, 'FiscalCalendarTestData');
        
        //Channelinsight configuration
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();

        Zipcode_Lookup__c zip=TestDataUtility.createZipcode();
        insert zip;
        
        //Creating test Account
        testMainAccount = TestDataUtility.createAccount(TestDataUtility.generateRandomString(5));
        testMainAccount.RecordTypeId = endUserRT;
        insert testMainAccount;
        
        testCustomerAccount = TestDataUtility.createAccount(TestDataUtility.generateRandomString(5));
        testCustomerAccount.RecordTypeId = directRT;
        insert testCustomerAccount;
        
        //Creating custom Pricebook
        pb = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(4)); 
        insert pb;
        
        testOpportunityList = new List<Opportunity>();
            Opportunity oppOpen=TestDataUtility.createOppty('Open Oppty',testMainAccount, pb, 'RFP', 'IT', 'FAX/MFP', 'Identification');
            testOpportunityList.add(oppOpen);
            Opportunity noPB = TestDataUtility.createOppty('NoPriceBook', testCustomerAccount, null, 'RFP', 'IT', 'FAX/MFP', 'Identification');
            testOpportunityList.add(noPB);
        insert testOpportunityList;
        
        //Creating Products
        List<Product2> prods=new List<Product2>();
            Product2 standaloneProd=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Standalone Prod1', 'Printer[C2]', 'OFFICE', 'H/W');
            prods.add(standaloneProd);
            Product2 parentProd1=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Parent Prod1', 'Printer[C2]', 'OFFICE', 'H/W');
            prods.add(parentProd1);
            Product2 childProd1=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod1', 'Printer[C2]', 'SMART_PHONE', 'Service pack');
            prods.add(childProd1);
            Product2 childProd2=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod2', 'Printer[C2]', 'SMART_PHONE', 'Service pack');
            prods.add(childProd2);
            Product2 parentProd2=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Parent Prod2', 'Printer[C2]', 'FAX/MFP', 'H/W');
            prods.add(parentProd2);
            Product2 childProd3=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod3', 'Printer[C2]', 'PRINTER', 'Service pack');
            prods.add(childProd3);
            Product2 childProd4=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod4', 'Printer[C2]', 'ACCESSARY', 'Service pack');
            prods.add(childProd4);
        insert prods;
        
        List<PriceBookEntry> pbes=new List<PriceBookEntry>();
            standaloneProdPBE=TestDataUtility.createPriceBookEntry(standaloneProd.Id, pb.Id);
            standaloneProdPBE.Product2 = standaloneProd;
            pbes.add(standaloneProdPBE);
            PriceBookEntry parentProd1PBE=TestDataUtility.createPriceBookEntry(parentProd1.Id,pb.Id);
            parentProd1PBE.Product2 = parentProd1;
            pbes.add(parentProd1PBE);
            PriceBookEntry childProd1PBE=TestDataUtility.createPriceBookEntry(childProd1.Id, pb.Id);
            childProd1PBE.Product2 = childProd1;
            pbes.add(childProd1PBE);
            PriceBookEntry childProd2PBE=TestDataUtility.createPriceBookEntry(childProd2.Id, pb.Id);
            childProd2PBE.Product2 = childProd2;
            pbes.add(childProd2PBE);
            PriceBookEntry parentProd2PBE=TestDataUtility.createPriceBookEntry(parentProd2.Id, pb.Id);
            parentProd2PBE.Product2 = parentProd2;
            pbes.add(parentProd2PBE);
        insert pbes;
        
        Contact contactRecord = TestDataUtility.createContact(testMainAccount);
        insert contactRecord;
        
        List<Order> orderList = new List<Order>();
            orderRecord1 = TestDataUtility.createOrder(testMainAccount, oppOpen);
            orderRecord1.RecordTypeId = demoAcctRtId;
            orderRecord1.Pricebook2 = pb;
            orderRecord1.Pricebook2Id = pb.Id;
            orderRecord1.Product_Group__c = 'ACCESSARY; SMART_PHONE';
            orderRecord1.CustomerAuthorizedById = contactRecord.Id;
            orderRecord1.RequestedShipmentDate__c = Date.today();
            OrderRecord1.EffectiveDate = Date.today();
            orderRecord1.BusinessReason__c = 'test OrderRecord1';
            orderRecord1.Type = 'Seed Unit';
            orderRecord1.Customer_Comments__c='Test Class';
            orderRecord1.Next_Steps__c='Test Class';
            orderRecord1.CYOD_Availability_Date__c=Date.today();
            orderRecord1.Reason_for_Future_CYOD_Date__c='Pricing';
            orderList.add(orderRecord1);
            
            orderRecord2 = TestDataUtility.createOrder(testMainAccount, oppOpen);
            orderRecord2.RecordTypeId = demoOppRtId1;
            orderRecord2.Pricebook2=pb;
            orderRecord2.Pricebook2Id = pb.Id;
            orderRecord2.Product_Group__c = 'ACCESSARY; SMART_PHONE; OFFICE';
           // orderRecord2.CustomerAuthorizedById = contactRecord.Id;
            orderRecord2.RequestedShipmentDate__c = Date.today();
            orderRecord2.EffectiveDate = Date.today();
            orderRecord2.BusinessReason__c = 'test OrderRecord2';
            orderRecord2.Type = 'Seed Unit';
            orderRecord2.Customer_Comments__c='Test Class';
            orderRecord2.Next_Steps__c='Test Class';
            orderRecord2.CYOD_Availability_Date__c=Date.today();
            orderRecord2.Reason_for_Future_CYOD_Date__c='Pricing';
            orderList.add(orderRecord2);
            
          
        insert orderList;
        
        List<OrderItem> orderItemList = new List<OrderItem>();
            OrderItem orderItem1 = TestDataUtility.createOrderItem(standaloneProdPBE, orderRecord1, 9.0);
            orderItemList.add(orderItem1);
            
            OrderItem orderItem2 = TestDataUtility.createOrderItem(standaloneProdPBE, orderRecord2, 10.0);
            orderItemList.add(orderItem2);
        insert orderItemList;
    }
    
    @isTest static void Testmethod1() {
        setup();
        Test.startTest();
        Order demo1=[select id from Order where CustomerAuthorizedById !=null  limit 1];
        String submitresult=DemoApprovalRequestController_ltng.SubmitdemoforApproval(demo1.id);
        Test.StopTest();
    }
    
     @isTest static void Testmethod2() {
        setup();
        Test.startTest();
        Order demo1=[select id from Order where CustomerAuthorizedById =null  limit 1];
        String submitresult=DemoApprovalRequestController_ltng.SubmitdemoforApproval(demo1.id);
        Test.StopTest();
    }
}