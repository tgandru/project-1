/*
Author: Fang Zhou
Description: 
1. Initial Development of T15806 and T15817 Add Product of Demo[Order] object, related VF page: DemoProductEntry.page
*/
public with sharing class DemoProductEntryExtension {
    //Properties
    public Order demoRecord {get;set;}
    public boolean isAccountUI {get;set;}
    public boolean isOpportunityUI {get;set;}
    public List<OrderItem> shoppingCart {get;set;}
    
    public List<shoppingCartWrapper> shoppingCartLst {get;set;}
    public List<OrderItem> initialCart {get;set;}
    public String toUnselect {get; set;}
    public PricebookEntry[] AvailableProducts {get;set;}
    public List<priceBookEntryWrapperCls> lstAvailableProducts {get;set;}
    public Pricebook2 theBook {get;set;} 
    public String selectedProdGroup {get;set;}
    public String searchString {get;set;}
    public List<String> prodGroups {get;set;}
    public List<SelectOption> prodGroupPicklist {get;set;}
    public Boolean overLimit {get;set;}
    public List<PricebookEntry> productsSelectedPBEs=new List<PriceBookEntry>();
    public Set<Id> productsSelected=new Set<Id>();
    public Set<String> prodIdsInShoppingCart=new Set<String>();
    public Set<Id> addedPBES=new Set<Id>();
    public String pbeIdInOppty='';
    public id demoid;
    
    public Boolean duplicatesExist=false;
    private Boolean forcePricebookSelection = false;
    private List<OrderItem> forDeletion = new List<OrderItem>();
    private Set<String> prodIdsInShoppingCartREMOVED=new Set<String>(); //will be prodid,bundleCount ; will be prodid,null for standalone

    public DemoProductEntryExtension(ApexPages.StandardController controller) {
        // Get information about the Demo (Order) being worked on
        demoid=ApexPages.currentPage().getParameters().get('Id');
        demoRecord = [Select o.Type, o.Product_Group__c, o.Pricebook2Id, o.Opportunity.ProductGroupTest__c, 
                        o.Opportunity.Pricebook2Id, o.Opportunity.CloseDate, o.Opportunity.Amount, 
                        o.Opportunity.StageName, o.Opportunity.Name, o.OpportunityId, o.Name, o.RecordTypeId,
                        o.CustomerAuthorizedBy.Name, o.CustomerAuthorizedById, o.Account.Name, o.AccountId,
                        o.RelatedCarriers__c, o.Pricebook2.Name, o.EffectiveDate, o.EndDate
                        From Order o Where Id =: controller.getRecord().Id limit 1];
        System.debug(' demo record '+demoRecord);
        Id demoAcctRtId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('EAP').getRecordTypeId();
        Set<Id> demoOppRtIds = new Set<Id>();
        demoOppRtIds.add(Schema.SObjectType.Order.getRecordTypeInfosByName().get('Evaluation').getRecordTypeId());
        demoOppRtIds.add(Schema.SObjectType.Order.getRecordTypeInfosByName().get('POC').getRecordTypeId());
        
        if(demoRecord.RecordTypeId == demoAcctRtId){
            isAccountUI = true;
            isOpportunityUI = false;
        } else if (demoOppRtIds.contains(demoRecord.RecordTypeId)) {
            isAccountUI = false;
            isOpportunityUI = true;
        }

        //get all pbeids of the olis
        if(demoRecord.RecordTypeId!=demoAcctRtId){
            List<String> lstIds=new List<String>();
            for(OpportunityLineItem oli:[select Id, PriceBookEntryId from OpportunityLineItem where OpportunityId=:demoRecord.OpportunityId]){
                lstIds.add('\''+oli.PriceBookEntryId+'\'');
            }
            pbeIdInOppty=String.join(lstIds,',');
        }
        
        
        //Initiate the existing product items for the Demo/Order objects
        initialCart = [Select o.UnitPrice, o.SAP_Material_ID__c, o.Quantity, o.ProductGroup__c, 
                        o.PricebookEntry.Product2Id, o.PricebookEntry.Name, o.PricebookEntryId, 
                        o.OrderId, o.ListPrice, o.Id, o.Description, o.ProductType__c  
                        From OrderItem o Where OrderId =: controller.getRecord().Id];
        System.debug(' initialCart '+initialCart);
        shoppingCartLst = new List<shoppingCartWrapper>();
        shoppingCart=new List<OrderItem>();
        lstAvailableProducts = new List<PriceBookEntryWrapperCls>();
        prodGroupPicklist=new List<SelectOption>();
        prodGroups = new List<String>(); 
        AvailableProducts = new List<PricebookEntry>();

        wrapInitial(initialCart);
        
        
        System.debug('lclc afer wrapp ');
        // Check if Demo/Order has a pricebook associated yet
        if(demoRecord.Pricebook2Id == null){
            Pricebook2[] activepbs = [select Id, Name from Pricebook2 where isActive = true limit 2];
            if(activepbs.size() == 2){
                forcePricebookSelection = true;
                theBook = new Pricebook2();
            }
            else{
                theBook = activepbs[0];
            }
        }
        else{
            theBook = demoRecord.Pricebook2;
            //theBook = [select Id, Name from Pricebook2 where Id =: demoRecord.Pricebook2Id Limit 1];
        }
         
        //collate all possible picklist values for prod group

        String ordProdGroup=demoRecord.Product_Group__c;
        System.debug(' ordProdGroup '+ordProdGroup);
        
        if(ordProdGroup!=null){
            prodGroups=ordProdGroup.split(';');
            prodGroupPicklist.add(new SelectOption('All','All'));
            for(String s:prodGroups){
                prodGroupPicklist.add(new SelectOption(s,s));
            }
        }else{
            prodGroupPicklist.add(new SelectOption('All','All'));
        }
        //added by thiru 12/13/2016
        selectedProdGroup='All';
        updateAvailableList();
    }
    
    // this is the 'action' method on the page
    public PageReference priceBookCheck(){
    
        if(forcePricebookSelection){        
            return changePricebook();
        }
        else{
        
            //if there is only one active pricebook we go with it and save the opp
            if(demoRecord.pricebook2Id != theBook.Id){
                try{
                    demoRecord.Pricebook2Id = theBook.Id;
                    update(demoRecord);
                }
                catch(Exception e){
                    ApexPages.addMessages(e);
                }
            }
            
            return null;
        }
    
        
    }
    
    public PageReference changePricebook(){
    
        // This simply returns a PageReference to the standard Pricebook selection screen
        // Note that is uses retURL parameter to make sure the user is sent back after they choose
    
        PageReference ref = new PageReference('_ui/busop/orderitem/ChooseOrderPricebook/e');
        ref.getParameters().put('id',demoRecord.Id);
        ref.getParameters().put('retURL','/apex/demoProductEntry?id=' + demoRecord.Id);
        ref.getParameters().put('cancelURL',demoRecord.Id);
        
        return ref;
    }
    
    public PageReference onSave(){
        //Possible to restrict the action based on Order Status value, and populate error message
        
        // Previously selected products may have new quantities and amounts, and we may have new products listed, so we use upsert here
        //NOTE currently upserting all but may cause perfomrance issues later. 
        shoppingCart.clear();
        try{
            if(shoppingCartLst.size()>0){
                for(shoppingCartWrapper scw:shoppingCartLst){
                    if(scw.quantity < 1){
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'A product\'s quantity can not be less than 1. If you\'d like 0 quantity of a product, please remove it.'));
                        return null;
                    }
                    if(scw.isNew==false){
                        //send off to recreate oppli
                        OrderItem oli1=unwrapLi(scw,false);
                        shoppingCart.add(oli1);
                    }else{
                        OrderItem oli1=unwrapLi(scw,true);
                        scw.isNew=false;
                        shoppingCart.add(oli1);
                    }
                    system.debug(' scw '+scw);
                }
            }
            System.debug(' shoppingCart '+shoppingCart);
            if(!shoppingCart.isEmpty())
            upsert(shoppingCart);
    
        }
        catch(Exception e){
            ApexPages.addMessages(e);
            return null;
        } 
        
        //When the end user clicks Remove button, the item is not removed until the Save button is clicked afterward. 
        try{
            if(forDeletion.size()>0){
                delete(forDeletion);
                prodIdsInShoppingCart.removeAll(prodIdsInShoppingCartREMOVED);
                forDeletion.clear();
                prodIdsInShoppingCartREMOVED.clear();
            }
        }catch(Exception e){
            ApexPages.addMessages(e);
            return null;
        }
    
    
        shoppingCart.clear();
        shoppingCartLst.clear();
        //return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
        return new PageReference('/apex/CustomRefreshPage?Id='+demoid);
    }

    public PageReference onCancel(){
    
        // If user hits cancel we commit no changes and return them to the Demo/Order object  
        return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
    }   

    public PageReference removeAllFromShoppingCart(){
    
        //Possible to restrict the action based on Order Status value, and populate error message
    
        for(shoppingCartWrapper d: shoppingCartLst){                        
            if(d.isNew == false){
                System.debug('lclc adding for deletion 3 '+d.productName+' '+d.productEntry);
                forDeletion.add(d.ordLi); 
                prodIdsInShoppingCartREMOVED.add(d.productEntry+','+null);                          
            }
        }
        shoppingCart.clear();
        shoppingCartLst.clear();
        return null;
    
    }

    public PageReference removeFromShoppingCart(){
        
        //Possible to restrict the action based on Order Status value, and populate error message
    
        // This function runs when a user hits "remove" on an item in the "Selected Products" section
        Integer count = 0;
    
        for(shoppingCartWrapper d : shoppingCartLst){
            if((String)d.pbEntryId==toUnselect){
                Set<Integer> countsToRemove=new Set<Integer>();
                
                if(d.isNew==false ){
                    System.debug('lclc adding for deletion 2 '+d.productName+' '+' '+d.productEntry);
                    forDeletion.add(d.ordLi);
                    prodIdsInShoppingCartREMOVED.add(d.productEntry+','+null);
                }
                countsToRemove.add(count);
    
                System.debug('lclc countsToRemove '+countsToRemove);
    
                for(Integer i=shoppingCartLst.size()-1; i>=0; i--){
                    if(countsToRemove.contains(i)){
                        shoppingCartLst.remove(i);
                    } 
                }
                
                break;
            }
            count++;
        }
        
        if(searchString!=null){
            updateAvailableList();
        }
       
        UnselectAvailableItems();
        
        return null;
    }

    public void UnselectAvailableItems()
    {
        for(PriceBookEntryWrapperCls d : lstAvailableProducts)
        {
            d.isSelected = false;
        }   
    }
    
            
    public void updateAvailableList() {
        //Preload all line items if opportunity does not have line items - HasLineItems
                
        // We dynamically build a query string and exclude items already in the shopping cart...Comment out to allow multiple entries of same item
                                    //Price Book             //Service Value (Add File Folder)
    
        String qString = 'select Id, Pricebook2Id, IsActive, Product2.Name, Product2.Family, Product2.Product_Group__c, '+
                            'Product2.Product_Type__c,Product2.IsActive, Product2.Description, UnitPrice, Product2.Pet_Name__c,'+
                            ' Product2.SAP_Material_ID__c from PricebookEntry where IsActive=true and Pricebook2Id = \'' + theBook.Id + '\'';
        System.debug(' theBook.Id '+theBook.Id);

        system.debug('lclc test '+pbeIdInOppty);

        if(pbeIdInOppty!=''){
            qString+='and Id IN ('+pbeIdInOppty+') ';
        }

        if(selectedProdGroup!=null && demoRecord.Product_Group__c != null){    
            if(selectedProdGroup=='All'){
                qString+=' and ( Product2.Product_Group__c = ';
                Integer count=1;
                for(String s:prodGroups){
                    
                    qString+='\''+s+'\'';
                    if(count<prodGroups.size()){
                        qString+=' OR Product2.Product_Group__C= ';
                    }else{
                        qString+=' ) ';
                    }
                    count++;
                } 
            }else{
                qString+='and Product2.Product_Group__c = \''+selectedProdGroup+'\' ';
            }
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please add Product Group on the Demo before attempting to add Products.'));
            return;
        }
    
        // note that we are looking for the search string entered by the user in the name OR product family
        // modify this to search other fields if desired
        if(searchString!=null && searchString!=''){
            qString+= ' and (Product2.Name like \'%' + searchString + 
                        '%\' or Product2.Description like \'%' + searchString + 
                        '%\' or Product2.Product_Group__c like \'%' + searchString + 
                        '%\' or Product2.Pet_Name__c like \'%' + searchString + 
                        '%\' or Product2.SAP_Material_ID__c like \'%' + searchString + 
                        '%\' or Product2.Product_Type__c like \'%' + searchString + '%\')';
        }
        
        Set<Id> selectedEntries = new Set<Id>();
        for(shoppingCartWrapper d:shoppingCartLst){
            selectedEntries.add(d.pbEntryId);
        }
        
        //This code use to remove the cart items from the available list of items. 
        if(selectedEntries.size()>0){
            String tempFilter = ' and Id not in (';
            for(Id i : selectedEntries){
                tempFilter+= '\'' + (String)i + '\',';
            }
            String extraFilter = tempFilter.substring(0,tempFilter.length()-1);
            extraFilter+= ')';
            
            qString+= extraFilter;
        }
        
        qString+= ' order by Product2.Name';
        qString+= ' limit 101';
        
        System.debug(' *** before query string '+qString);
        AvailableProducts = database.query(qString);
        System.debug(' query string '+qString);
        System.debug(' AvailableProducts '+AvailableProducts);
        Set<Id> availableProductsIds=new Set<Id>();
        for(PriceBookEntry pbe:AvailableProducts){
            availableProductsIds.add(pbe.Product2Id);
        }
        
        if(AvailableProducts.size()>100){
            overLimit=true;             
        }else{
            overLimit=false;
        }
        
        lstAvailableProducts.Clear();
    
        for(PricebookEntry d : AvailableProducts)
        {
            Boolean relatedProds=true; //related products is true if there are NO related products
            PriceBookEntryWrapperCls temp = new PriceBookEntryWrapperCls(d,relatedProds);            
            lstAvailableProducts.add(temp);          
        }   
    }   
    
    private void wrapInitial(List<OrderItem> initCart){
        System.debug('lclc in wrap initial ');
        if(!initCart.isEmpty()){
            System.debug(' order item size '+initCart.size());
            for(OrderItem ordLi:initCart){ //iff there are no children, everything is a standalone.
                System.debug(' ordLi '+ordLi);
                shoppingCartLst.add(new shoppingCartWrapper(ordLi,4,false,null,null,false));
               
                if(prodIdsInShoppingCart.contains(ordLi.PricebookEntry.Product2Id+','+null)){
                    duplicatesExist=true;
                }else{
                    prodIdsInShoppingCart.add(ordLi.PricebookEntry.Product2Id+','+null);
                }
            }
        }
        System.debug(' shoppingCartLst '+shoppingCartLst);  
    }

    private OrderItem unwrapLi(shoppingCartWrapper scw, Boolean isOLI){
        OrderItem oli;
        if(isOLI==false){
            oli=new OrderItem(Id=scw.orderItemId, OrderId=demoRecord.Id, PriceBookEntry=scw.pbEntry,PriceBookEntryId=scw.pbEntryId,
                              Description=scw.productDescription, Quantity=scw.quantity);
            oli.UnitPrice=scw.requestedPrice;
        }else{
            oli=new OrderItem(OrderId=demoRecord.Id, PriceBookEntry=scw.pbEntry,PriceBookEntryId=scw.pbEntryId,
                              Description=scw.productDescription, Quantity=scw.quantity);
            oli.UnitPrice=scw.requestedPrice;
        }
        return oli;
    }


    public void addCheckedItemsToShoppingCart(){
        
        //track parents, standalones added.
        
        productsSelectedPBEs.clear();
        productsSelected.clear();
        prodIdsInShoppingCart.clear();
    
        for(PriceBookEntryWrapperCls d : lstAvailableProducts)
        {      
        System.debug('lclc first for '+d);                                                   
            if(d.isSelected)
            {   
                System.debug('lclc d is sleected '+d);
                for(PriceBookEntry pbe1:AvailableProducts){
                    if(d.cPriceBookEntry.Id==pbe1.Id){
                        if(d.relatedProducts==false){ //related products is TRUE if there are NO related products. it is FALSE if tehre ARE related products
                            //shoppingCartLst.add(new shoppingCartWrapper(pbe1,1,pbe1.Product2Id,null)); //do this later.
                            //prepare query for child products now. add parent products after bundlecount is completed.
                            productsSelected.add(pbe1.Product2Id);
                            productsSelectedPBEs.add(pbe1);
                        }else{
                            shoppingCartLst.add(new shoppingCartWrapper(pbe1,3,null,null));
                            addedPBES.add(pbe1.Id);
                            if(prodIdsInShoppingCart.contains(pbe1.Product2Id+','+null)){
                                duplicatesExist=true;
                            }else{
                                prodIdsInShoppingCart.add(pbe1.Product2Id+','+null);
                            }
                        }
                        
                    }
    
                }
    
            }    
            
        }
    
         System.debug('lclc productsSelectedPBEs '+productsSelectedPBEs);
    
        //prepare to query for child products @TODO double check this is functional when its above. 
        for(shoppingCartWrapper scw1:shoppingCartLst){
            productsSelected.add(scw1.productEntry);
        }
        System.debug(' productsSelected '+productsSelected);
        
    
        System.debug('clcl addedPBES '+addedPBES);
        System.debug('lclc addedpbes size '+addedPBES.size()+'shopping cart size '+shoppingCartLst.size());
        //check to make sure all selectedproductspbes are in cart. we have only added them if they have children so far. if no pr's were found for them then they wont be in there. 
        for(priceBookEntry pbe1:productsSelectedPBEs){
            if(!addedPBES.contains(pbe1.Id)){
                shoppingCartLst.add(new shoppingCartWrapper(pbe1,3,null,null));
                prodIdsInShoppingCart.add(pbe1.Product2Id+','+null);
                addedPBES.add(pbe1.Id);
            }
        }
        System.debug(' shoppingCartLst '+shoppingCartLst);
        System.debug('lclc shoppinglistsize at end '+shoppingCartLst.size());
    
        updateAvailableList();  
        UnselectAvailableItems();
        
    }
    
    /******************************WRAPPER*********************************/

    public class shoppingCartWrapper{
        public OrderItem ordli {get;set;}
        public String orderItemId{get;set;}
        public PriceBookEntry pbEntry {get;set;}
        public String pbEntryId {get;set;}
        public Integer productTypeInt {get;set;} //parent:1, child:2, standalone:3, no children in map: 4
        public String productEntry {get;set;}
        public Integer quantity {get;set;}
        public Decimal salesPrice{get;set;}
        public Decimal listPrice{get;set;}
        public Decimal totalPrice{get;set;}
        public Decimal requestedPrice{get;set;}
        public String productGroup{get;set;}
        public String productName{get;set;}
        public String productDescription{get;set;}
        public String productType{get;set;}
        public Boolean isNew{get;set;}
        public String sapMaterialId{get;set;}
        public Boolean isAmpvPT{get;set;}
        public Boolean isBundlePT{get;set;}
    
        //for existing items
        //convert from oli to wrapper
        public shoppingCartWrapper(orderItem oli, Integer productTypeInteger, Boolean isOppClosedBoolean, String parentProdIdVar, Integer bundleCountVar, Boolean isExpiredPRVar){// Map<Id,Id> parentToChildProdMap
            System.debug('lclc in it '+oli);
            ordli=oli;
            if(ordli.Id!=null){
                orderItemId=ordli.Id;
            }
            productGroup=ordLi.ProductGroup__c != null ? ordLi.ProductGroup__c : '';
            productType=ordLi.ProductType__c != null ? ordLi.ProductType__c : '';
            productName=ordLi.PriceBookEntry.Name != null ? ordLi.PriceBookEntry.Name : '';
            isAmpvPT=false;
            isBundlePT=false;
            productDescription=ordLi.Description;
            productEntry=ordLi.PricebookEntry.Product2Id;
            productTypeInt=productTypeInteger;
            pbEntryId=ordLi.PricebookEntryId;
            pbEntry=ordLi.PriceBookEntry != null ? ordLi.PriceBookEntry: null;
            quantity=ordLi.Quantity != null ? ordLi.Quantity.intValue() : 0;
            totalPrice=ordLi.UnitPrice != null && ordLi.Quantity != null ? ordLi.UnitPrice * ordLi.Quantity : 0.0;
            isNew=false;
            requestedPrice=ordLi.UnitPrice;
            sapMaterialId=ordLi.SAP_Material_ID__c != null ? ordLi.SAP_Material_ID__c: '';
            listPrice=ordLi.ListPrice != null ? ordLi.ListPrice: 0.0;
            salesPrice=ordLi.UnitPrice != null ? ordLi.UnitPrice: 0.0;
    
        }
        
        //for new items
        //convert from pricebookentry to wrapper
        public shoppingCartWrapper(PriceBookEntry pbe, Integer productTypeInteger, String parentProdIdVar, Integer bundleCountVar){
            pbEntry=pbe;
            if(pbEntry.Id!=null){
                pbEntryId=pbEntry.Id;
            }
            productEntry=pbEntry.Product2Id;
            productTypeInt=productTypeInteger;
            productGroup=pbEntry.Product2.Product_Group__c;
            productType=pbEntry.Product2.Product_Type__c;
            productName=pbEntry.Product2.Name;
            isAmpvPT=false;
            isBundlePT=false;
            productDescription=pbEntry.Product2.Description;
            quantity=1;
            requestedPrice=pbEntry.UnitPrice;
            listPrice=pbEntry.UnitPrice;
            salesPrice=pbEntry.UnitPrice;
            isNew=true;
            ordLi=null;
            sapMaterialId=pbEntry.Product2.SAP_Material_ID__c;
    
            }
        }
}