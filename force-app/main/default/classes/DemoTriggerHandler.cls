// Apex Class to Handle all demo Trigger Actions
public class DemoTriggerHandler {



public static Boolean isFirstTime = true;// Static Variable to stop recurresive Methods

// Method to Check demo Contact is Registered under Opportunity Contact Roles or not. Demo Contact Shuold be Registered as Opportunity Contact role.
Public static void CheckDemoContact(List<Order> newlist){
    Set<id> oppids=new Set<id>();// Set of Opportunity ID's
    Map<id,Set<String>> contactroleMap=new  Map<id,Set<String>>();
    for(Order o: newlist){//Capture All Opportunity ID's first
        if(O.OpportunityId !=null){
            oppids.add(o.OpportunityId);
        }
    }
    
    
    if(oppids.Size()>0){//Capture all related Contact roles
        List<OpportunityContactRole> contactroles=new List<OpportunityContactRole>([Select id,ContactId,OpportunityId from OpportunityContactRole where OpportunityId in:oppids]);
        for(OpportunityContactRole con: contactroles){
            if(contactroleMap.containsKey(con.OpportunityId)){
                Set<String> existed=contactroleMap.get(con.OpportunityId);
                  existed.add(con.ContactId);
                  contactroleMap.put(con.OpportunityId,existed);
            }else{
                contactroleMap.put(con.OpportunityId,new set<String>{con.ContactId});
            }
        }
    }
    
    //Check the Current Demo Contact 
    
    for(Order o:newlist){
        if(o.OpportunityId !=null && o.CustomerAuthorizedById != null){
             if(contactroleMap.containsKey(o.OpportunityId)){
                 Set<String> contactids=contactroleMap.get(o.OpportunityId);
                  if(contactids.contains(o.CustomerAuthorizedById)){
                      // Contact Registered as Contact Role.
                  }else{
                      o.adderror('Only Opportunity Contact roles Can be Attached as Demo Contact. Please Register Contact into Opportunity Contact Roles first.');
                  }
             }
            
        }
    }
    
    isFirstTime=false;
}

}