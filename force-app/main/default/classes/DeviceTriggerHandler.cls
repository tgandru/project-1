public with sharing class DeviceTriggerHandler {

    public static String invalidRecordTypeId;
    public static String editRecordTypeId;

    public static void validateModeCode(List<Device__c> triggerNew, Map<Id, Device__c> triggerOldMap)
    {
        Map<String, Device__c> modelNameDeviceMap = new Map<String, Device__c>();
        Set<String> modelNames = new Set<String>();

        for(Device__c dev: triggerNew)
        {
            if(String.isNotBlank(dev.Model_Name__c) && 
                    (!triggerOldMap.containsKey(dev.Id) || 
                            (triggerOldMap.containsKey(dev.Id) && dev.Model_Name__c != triggeroldMap.get(dev.Id).Model_Name__c)))
            {
                modelNameDeviceMap.put(dev.Model_Name__c, dev);
            }
        }

        if(!modelNameDeviceMap.isEmpty())
        {
            List<Product2> productList = [SELECT SAP_Material_ID__c 
                                          FROM Product2 
                                          WHERE SAP_Material_ID__c IN: modelNameDeviceMap.keySet()];

            for(Product2 prod: productList)
                modelNames.add(prod.SAP_Material_ID__c);

            for(String s: modelNameDeviceMap.keySet())
            {
                if(!modelNames.contains(s))
                {
                    modelNameDeviceMap.get(s).addError('Unable to validate Model Code ' + s + '. Please check and try again.');
                }
            }
        }
    }

    public static void calculateTotalNumberofDevice(List<Device__c> triggerNew, Map<Id, Device__c> triggerOldMap, Boolean isInsert, Boolean isUpdate, Boolean isDelete)
    {
        Map<Id, Integer> entitlementDevCntMap = new Map<Id, Integer>();
        if(isInsert || isUpdate){
            for(Device__c dev: triggerNew)
            {
                if(isInsert && dev.Entitlement__c != null)
                {
                    changeCntMap(entitlementDevCntMap, dev.Entitlement__c, 1);
                }
                else if(isUpdate && triggerOldMap.get(dev.Id).Entitlement__c != dev.Entitlement__c)
                {
    
                    if(dev.Entitlement__c != null)
                        changeCntMap(entitlementDevCntMap, dev.Entitlement__c, 1);
    
                    if(triggerOldMap.get(dev.Id).Entitlement__c != null)
                        changeCntMap(entitlementDevCntMap, triggerOldMap.get(dev.Id).Entitlement__c, -1);
                }
            }
        }
        if(isDelete)
        {
            for(Device__c dev: triggerOldMap.values())
            {
                if(dev.Entitlement__c != null)      
                    changeCntMap(entitlementDevCntMap, dev.Entitlement__c, -1);
            }
        }

        if(!entitlementDevCntMap.isEmpty())
        {
            List<Entitlement> entList = [SELECT Id, Total_of_Devices__c FROM Entitlement WHERE Id IN: entitlementDevCntMap.keySet()];

            for(Entitlement ent: entList)
            {       
                if(entitlementDevCntMap.containsKey(ent.Id))
                    ent.Total_of_Devices__c = ent.Total_of_Devices__c + entitlementDevCntMap.get(ent.Id);
            }

            update entList;
        }
    }

    private static void changeCntMap(Map<Id, Integer> entitlementDevCntMap, String entId, Integer cnt)
    {
        Integer newCnt;
        if(entitlementDevCntMap.containsKey(entId))
        {
            newCnt = entitlementDevCntMap.get(entId)+cnt;
        }
        else
        {
            newCnt = cnt;
        }
        entitlementDevCntMap.put(entId, newCnt);

    }

    public static void checkDevice(List<Device__c> triggerNew, Map<Id, Device__c> triggerOldMap)
    {
        editRecordTypeId = Schema.SObjectType.Device__c.getRecordTypeInfosByName().get('Edit Device').getRecordTypeId();
        invalidRecordTypeId = Schema.SObjectType.Device__c.getRecordTypeInfosByName().get('Invalid Device').getRecordTypeId();

        List<Device__c> checkDeviceList = new List<Device__c>();
        Set<String> imeiSNs = new Set<String>();
        Set<String> alreadyExistingIMEISN = new Set<String>();
        Map<string,List<Device__c>> alreadyExistingIMEISNMap=new Map<string,List<Device__c>>();

        for(Device__c dev: triggerNew)
        {
            //If this is a new device or the device IEMI or serial number is updated//  !triggeroldMap.containsKey(dev.Id) ||
            
          if(dev.Status__c == 'New' || 
                    (triggeroldMap.containsKey(dev.Id) && (dev.IMEI__c != triggerOldMap.get(dev.Id).IMEI__c || dev.Serial_Number__c != triggerOldMap.get(dev.Id).Serial_Number__c)))
            {
                if(dev.IMEI__c != null && (!dev.IMEI__c.isNumeric() || dev.IMEI__c.length() < 14 || dev.IMEI__c.length() > 15))
                {
                    setDeviceasInvalid(dev, 'IMEI check-sum failed');
                    continue;
                }

                if(dev.Serial_Number__c != null && dev.IMEI__c == null && dev.Serial_Number__c.length() < 10)
                {
                    setDeviceasInvalid(dev, 'Serial Number minimum digits check failed');
                    continue;
                }
                else if(dev.Serial_Number__c != null && dev.IMEI__c == null && dev.Serial_Number__c.length() >= 10)
                {
                    checkDeviceList.add(dev);
                    imeiSNs.add(dev.Serial_Number__c);
                    continue;
                }

                if(dev.IMEI__c != null && dev.IMEI__c.length() == 14)
                {
                    checkDeviceList.add(dev);
                    imeiSNs.add(dev.IMEI__c);
                    continue;
                }

                if(dev.IMEI__c != null && dev.IMEI__c.length() == 15)
                {
                    Integer sum = 0;
                    for(Integer i = 0;i < dev.IMEI__c.length()-1; i++) {
                        if(Math.mod(i,2) == 1){
                            String s = String.valueof(integer.valueof(dev.IMEI__c.substring(i,i+1))*2);
                            if(s.length()==2)
                            {
                                sum += integer.valueof(s.substring(0,1)) + integer.valueof(s.substring(1,2));
                            }
                            else
                            {
                                sum += integer.valueof(s);
                            }
                        }
                        else
                        {
                            sum += integer.valueof(dev.IMEI__c.substring(i,i+1));
                        }
                    }

                    if((10 - Math.Mod(sum,10) == integer.valueof(dev.IMEI__c.substring(14,15))) ||
                        (10 - Math.Mod(sum,10)==10 && integer.valueof(dev.IMEI__c.substring(14,15))==0))
                    {
                        checkDeviceList.add(dev);
                        imeiSNs.add(dev.IMEI__c);
                    }
                    else
                    {
                        setDeviceasInvalid(dev, 'IMEI check-sum failed');
                    }
                }

            }

        }

        if(!checkDeviceList.isEmpty())
        {
            
            List<Device__c> existingDeviceList = [SELECT IMEI__c, Serial_Number__c 
                                                  FROM Device__c 
                                                  WHERE (IMEI__c IN: imeiSNs OR Serial_Number__c IN: imeiSNs)
                                                  AND (IMEI__c != NULL OR Serial_Number__c != null)
                                                  AND (Status__c = 'Valid' OR Status__c = 'Verified' OR Status__c = 'Registered')];

            for(Device__c dev: existingDeviceList)
            {
                if(dev.Serial_Number__c != null){
                    alreadyExistingIMEISN.add(dev.Serial_Number__c);
                    if(alreadyExistingIMEISNMap.containsKey(dev.Serial_Number__c)){
                            List<Device__c> cs = alreadyExistingIMEISNMap.get(dev.Serial_Number__c);
                                        cs.add(dev);
                                       alreadyExistingIMEISNMap.put(dev.Serial_Number__c, cs);
                     }else{
                               alreadyExistingIMEISNMap.put(dev.Serial_Number__c, new List<Device__c> { dev }); 
                        }
                    
                    
                }
                if(dev.IMEI__c != null){
                    alreadyExistingIMEISN.add(dev.IMEI__c);
                    if(alreadyExistingIMEISNMap.containsKey(dev.IMEI__c)){
                            List<Device__c> cs = alreadyExistingIMEISNMap.get(dev.IMEI__c);
                                        cs.add(dev);
                                       alreadyExistingIMEISNMap.put(dev.IMEI__c, cs);
                     }else{
                               alreadyExistingIMEISNMap.put(dev.IMEI__c, new List<Device__c> { dev }); 
                        }
                    
                }
            }

            for(Device__c dev: checkDeviceList)
            {
              //--------Added By Vijay Starts here 11/5/2018  
              // Purpose - in Old Logic, this validation is firing on the same record
               if(dev.IMEI__c != null && alreadyExistingIMEISNMap.containsKey(dev.IMEI__c)){
                    try{
                        List<Device__c> dv=alreadyExistingIMEISNMap.get(dev.IMEI__c);
                        if(dv.size()==1 && dv[0].id==dev.id){
                           System.debug('Same Record'); 
                        }else{
                              setDeviceasInvalid(dev, 'IMEI Duplication check failed');
                              continue;
                        }
                    }catch(Exception ex){
                        
                    }
                    
                }
                
                 
                if(dev.Serial_Number__c != null && alreadyExistingIMEISNMap.containsKey(dev.Serial_Number__c)){
                     try{
                        List<Device__c> dv=alreadyExistingIMEISNMap.get(dev.Serial_Number__c);
                        if(dv.size()==1 && dv[0].id==dev.id){
                           System.debug('Same Record'); 
                        }else{
                              setDeviceasInvalid(dev, 'Serial Number Duplication check failed');
                              continue;
                        }
                    }catch(Exception ex){
                        
                    }
                    
                }
                
                
               //------------Ends Here 
                
               /* if(dev.IMEI__c != null && alreadyExistingIMEISN.contains(dev.IMEI__c))
                {
                    
                    setDeviceasInvalid(dev, 'IMEI Duplication check failed');
                    continue;
                }

                if(dev.Serial_Number__c != null && alreadyExistingIMEISN.contains(dev.Serial_Number__c))
                {
                    setDeviceasInvalid(dev, 'Serial Number Duplication check failed');
                    continue;
                } */

                dev.Status__c = 'Valid';
                dev.Verification_Failure_Reason__c = '';
                dev.RecordTypeId = editRecordTypeId;
            }
        }
        
        
        // Added By Vijay Starts Here -11/5/2018
        // Purpose to Capture Who Approve the Device record
      /*  for(Device__c d:triggerNew){
           Device__c oldd=triggerOldMap.get(d.id);
            if(d.Status__c=='Registered' && oldd.Status__c != 'Registered'){
                d.Approved_By__c=UserInfo.getUserId();
            }
            
        }*/
        // -----------Ends Here 
        
        
    }

    private static void setDeviceasInvalid(Device__c dev, String failReason)
    {
        dev.Status__c = 'Invalid';
        dev.Verification_Failure_Reason__c = failReason;
        dev.RecordTypeId = invalidRecordTypeId;
    }
}