global without sharing class EntitlementTriggerHandler {

    public static void capLimitSendEmail(List<Entitlement> entList, Map<Id, Entitlement> mapOld){
        system.debug('111');
        List<Messaging.SingleEmailMessage> mails =  new List<Messaging.SingleEmailMessage>();
        Id usId = UserInfo.getUserId();
        EmailTemplate et = [SELECT Id, Subject, HtmlValue, Body, BrandTemplateId FROM EmailTemplate WHERE Name = 'Entitlement Cap Limit Notification'];
        Domain dm = [SELECT Domain,DomainType FROM Domain];
        Document dc;
        if(!Test.isRunningTest()) dc = [SELECT Id, Name FROM Document WHERE Name = 'samsung_login_logo.png'];
        OrgWideEmailAddress owda = [SELECT Id, Address FROM OrgWideEmailAddress WHERE Displayname = 'SBS Support'];

        String customlink = '';
        String imgstr = '';
        String header = '';
        String footer = '';

        if(!Test.isRunningTest()){
            customlink = Url.getSalesforceBaseUrl().toExternalForm() +'/';
            imgstr = '<img src="'+Url.getSalesforceBaseUrl().toExternalForm()+'/servlet/servlet.ImageServer?id='+dc.id+'&oid='+UserInfo.getOrganizationId()+'" height="44" width="250"/><br/>';
            header = '<tr valign = "top"><td style="background-color:#AAAAFF; bEditID:r2st1; bLabel:accent1; height:5;"></td></tr><tr valign="top" height="400">';
            footer = '<tr valign = "top"><td style="background-color:#AAAAFF; bEditID:r2st1; bLabel:accent1; height:5;"></td></tr></table>';
        }
        else{
            customlink = 'testData';
            imgstr = 'testData';
            header = 'testData';
            footer =  'testData';
        }
        Map<String, Entitlement> entMap = new Map<String, Entitlement>();

        for(Entitlement ent : [SELECT Id, Name
                            , Account.Name
                            , Account.Service_Account_Manager__c
                            , Account.Service_Account_Manager__r.Name 
                            , Account.Service_Account_Manager__r.Email
                            FROM Entitlement WHERE Id IN: mapOld.keySet()]){
            entMap.put(ent.Id, ent);
        }

        for(Entitlement ent : entList){
            system.debug('entList : ' + entList);
            Entitlement oldEnt = mapOld.get(ent.Id);
            if(entMap.get(ent.Id) != null && entMap.get(ent.Id).Account.Service_Account_Manager__c != null 
                && ent.Units_Available__c == 0
                && (nvlDec(ent.Units_Allowed__c) - nvlDec(ent.Units_Exchanged__c) != nvlDec(oldEnt.Units_Allowed__c) - nvlDec(oldEnt.Units_Exchanged__c))){
                system.debug('Entitlement update Trigger start');
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                List<String> to = new List<String>();
                to.add(entMap.get(ent.Id).Account.Service_Account_Manager__r.Email);
                mail.setToAddresses(to);
                mail.setReplyTo(owda.Address);

                String body = '';
                body = imgstr;
                body += et.HtmlValue;
                body = body.remove('<![CDATA[');
                body = body.remove(']]>');
                body = body.replace('<tr valign="top" height="400" >', header);
                body = body.replace('</table>', footer);
//
                //system.debug('after body : ' + body);             body = body.replace('{AccountManager}', entMap.get(ent.Id).Account.Service_Account_Manager__r.Name + '<br/><br/>');
                body = body.replace('{AccountManager}', entMap.get(ent.Id).Account.Service_Account_Manager__r.Name);
                body = body.replace('{AccountName}', entMap.get(ent.Id).Account.Name);
                body = body.replace('{EntitlementName}', ent.Name);
                body = body.replace('{CustomLink}', customlink + ent.Id);
                body = body.replace('{TotalDevice}', String.valueOf(ent.Total_of_Devices__c));
                body = body.replace('{UnitsAllowed}', String.valueOf(ent.Units_Allowed__c));
                body = body.replace('{UnitsExchanged}', String.valueOf(ent.Units_Exchanged__c));
                body = body.replace('{UnitsAvailable}', String.valueOf(ent.Units_Available__c));

                String subject = et.Subject;
                subject = subject.replace('{AccountName}', entMap.get(ent.Id).Account.Name);
                
                mail.setSubject(subject);
                mail.setTargetObjectId(usId);
                mail.setSaveAsActivity(false);
                mail.setHtmlBody(body);
                mail.setTreatBodiesAsTemplate(false);
                mail.setOrgWideEmailAddressId(owda.Id);
                mail.setTemplateId(et.Id);

                mails.add(mail);
                system.debug(mail);
            }
            
        }
        if(!mails.isEmpty()) Messaging.sendEmail(mails);
    }
    global static Decimal nvlDec(Decimal o) {
        return nvlDec(o, 0);
    }
    global static Decimal nvlDec(Decimal o, Decimal r) {
        return o==null?r:o;
    } 
}