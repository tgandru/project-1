@isTest
private class EntitlementTriggerHandler_Test
{
    @isTest
    static void sendEmailTest(){
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }
        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
        );
        insert u;

        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660',
            Service_Account_Manager__c = u.id
            
        );

        insert testAccount1;

        Product2 prod1 = new Product2(
                name ='MI-test',
                SAP_Material_ID__c = 'MI-test',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = rtMap.get('Product2'+'B2B')
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            Units_Allowed__c = 5000,
            Units_Exchanged__c = 5000,
            startdate = system.today()-1
        );
        insert ent;

        Entitlement ent2 = new Entitlement(
            name ='test Entitlement2',
            accountid=testAccount1.id,
            assetid = ast.id,
            Units_Allowed__c = 5000,
            Units_Exchanged__c = 4999,
            startdate = system.today()-1
        );
        insert ent2;

        List<Entitlement> entList = [SELECT Id, Name
                            , Account.Name
                            , Account.Service_Account_Manager__c
                            , Account.Service_Account_Manager__r.Name 
                            , Account.Service_Account_Manager__r.Email
                            , Units_Available__c
                            , Units_Allowed__c
                            , Units_Exchanged__c
                            , Total_of_Devices__c 
                            FROM Entitlement
                            WHERE Id =: ent.Id];

        Map<Id, Entitlement> entMap = new Map<Id, Entitlement>();
        entMap.put(ent.Id,ent2);

        EntitlementTriggerHandler.capLimitSendEmail(entList, entMap);
    }
}