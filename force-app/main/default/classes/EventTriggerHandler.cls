public with sharing class EventTriggerHandler {
	public static Boolean isFirstTime = true;
	public static void svc_CompleteMileStones(List<Event> newEvents) {
        DateTime completionDate = System.now();
        Map<ID, ID> CaseWhoMap = new Map<ID,ID>();
       
        for (Event e:newEvents){
            string s=string.valueOf(e.whatid);
             if(s !=null && s !=''){
                if (s.startsWith('500') ){ //email is handled in EmailMessage trigger
               
                CaseWhoMap.put(e.whatid, e.whoid);
                } 
             }
           
        }
        if (CaseWhoMap.size()>0){
            Set <Id> CaseIds = new Set<Id>();
            CaseIds = CaseWhoMap.keySet();
            List<Case> caseList = [Select c.Id, c.ContactId, c.Contact.Email,
                                  c.OwnerId, c.Status,
                                  c.EntitlementId,
                                  c.SlaStartDate, c.SlaExitDate
                               From Case c where c.Id IN:CaseIds];
                if (caseList.isEmpty()==false){
                    List<Id> updateCases = new List<Id>();
                    for (Case caseObj:caseList) {
                        // consider an outbound email to the contact on the case a valid first response
                        if ((CaseWhoMap.get(caseObj.Id)==caseObj.ContactId)&&
                           (caseObj.EntitlementId!= null)&&
                           (caseObj.SlaStartDate <= completionDate)&&
                           (caseObj.SlaStartDate!= null)&&
                           (caseObj.SlaExitDate == null))
                            updateCases.add(caseObj.Id);
                    }
                    if(updateCases.isEmpty() == false)
                        SVC_milestoneUtils.completeMilestone(updateCases, 'First Response Time', completionDate);
                        SVC_milestoneUtils.completeMilestone(updateCases, 'Status Update', completionDate);
                }

        }
        
         
    }
    
    
    public static void UpdateOpportunityUpdateDate(List<Event> newEvents){
           List<String> OppIds=new List<String>();
           for (Event e:newEvents){
            string s=string.valueOf(e.whatid);
             if(s !=null && s !=''){
                 if(s.startsWith('006') && (e.Activity_Type__c=='Meeting' || e.Activity_Type__c =='Call')){
                    OppIds.add(e.WhatID);
                }
             }
           
        }
        
        if(OppIds.size()>0){
                 UpdateOppLastUpdateValue(OppIds); 
           }
        
    }
    
      public static void CopyTypeFromActivityType(List<Event> nweve,Map<id,Event> oldMap){
      for(Event t:nweve){
            If(t.Type==null && t.Activity_Type__c !=null){
                t.Type=t.Activity_Type__c;
            }else if(t.Type!=null && t.Activity_Type__c ==null){
                t.Activity_Type__c=t.Type;
            }
            
            try{
                Event oldeve=oldMap.get(t.Id);
                If(t.Type==oldeve.Type && t.Activity_Type__c !=oldeve.Activity_Type__c){
                    t.type=t.Activity_Type__c;
                }else if(t.Type!=oldeve.Type && t.Activity_Type__c ==oldeve.Activity_Type__c){
                     t.Activity_Type__c=t.Type;
                }
                
            }catch(Exception ex){
                
            }
            
        }
    }
    
       @future 
    Public Static void UpdateOppLastUpdateValue(List<string> oppids){
        List<Opportunity> Opp=new List<Opportunity>([Select id,Name,Last_Modified_Date_Time__c,last_updated_Date__c from Opportunity where id in:oppids]);
        for(Opportunity o:opp){
            o.last_updated_Date__c=System.now();
        }
        
        if(Opp.size()>0){
            update opp;
        }
    }
    
    
    
}