@isTest
private class EventTriggerHandlerTest {
    
    @isTest static void testEventTriggerHandler() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Profile adminProfile = [SELECT Id FROM Profile Where Name ='B2B Service System Administrator'];

        User admin1 = new User (
            FirstName = 'admin1',
            LastName = 'admin1',
            Email = 'admin1seasde@example.com',
            Alias = 'admint1',
            Username = 'admin1seaa@example.com',
            ProfileId = adminProfile.Id,
            TimeZoneSidKey = 'America/New_York',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US'
        );
        
        insert admin1;

        BusinessHours bh24 = [SELECT Id FROM BusinessHours WHERE name = 'B2B Service Hours 24x7'];
        BusinessHours bh12 = [SELECT Id FROM BusinessHours WHERE name = 'B2B Service Hours 12x5'];
    
        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        );

        insert testAccount1;

        RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];
        Product2 prod1 = new Product2(
                name ='MI-test',
                SAP_Material_ID__c = 'MI-test',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = productRT.id
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        

        Slaprocess sla = [select id from SlaProcess where name like 'Enhanc%' 
            and isActive = true limit 1 ];

        Entitlement ent = new Entitlement(
            name ='test Entitlement1',
            accountid=testAccount1.id,
            assetid = ast.id,
            BusinessHoursId = bh12.id,
            startdate = system.today()-1,
            slaprocessid = sla.id
            );
        insert ent;

       
        
        Contact contact = new Contact(); 
        contact.AccountId = testAccount1.id; 
        contact.Email = 'svcTest@svctest.com'; 
        contact.LastName = 'Named Caller'; 
        contact.Named_Caller__c = true;
        contact.FirstName = 'Named Caller'; 
        Contact.mailingState = 'NJ';
        Contact.mailingCountry = 'US';


        insert contact; 

        String entlId;
        if (ent != null)
        entlId = ent.Id; 

        Test.startTest();
        Group q= [select id from group where type='Queue' limit 1 ];
        List<Case> cases = new List<Case>{};
        if (entlId != null){
            Case c1 = new Case(Subject = 'Test Case with Entitlement ', 
            EntitlementId = entlId, ContactId = contact.id,
            origin = 'Phone',
            ownerid=admin1.id);
            
            insert c1;

            Case c2 = new Case(Subject = 'Test Case with Entitlement ', 
            EntitlementId = entlId, ContactId = contact.id,
            origin = 'Web',
            ownerid=q.id);
            
            insert c2;

            Event evt = new Event(
                Subject = 'Hello World',
                type = 'Meeting',
                startdatetime = system.now(),
                enddatetime= system.now()+1,
                WhatId = c2.Id,
                whoid = contact.id);
            insert evt;

            Event evt2 = new Event(
                Subject = 'Hello World',
                type='Meeting',
                startdatetime = system.now(),
                enddatetime = system.now()+1,
                WhatId = c2.Id
                );
            insert evt2;
            evt2.whoid=contact.id;
            update evt2;

            
        }
        Test.stopTest();
        
    }
    
    
    
}