public class ExchangeInventoryCheck {
public string searchmode {get; set;}
public Case cs {get; set;}
public Boolean showlookup {get; set;}
public integer selectedcnt {get; set;}
Public List<ResopnseRecord> responselist {get; set;}
 public class ResopnseRecord {
       public string Werks {get;set;}
        public string Lgort {get;set;}
        public string Matnr {get;set;}
        public string Maktx {get;set;}
        public decimal Clabs {get;set;}
        public decimal Avqty {get;set;}
        public decimal ExchageFee {get;set;}
        public string Meins {get; set;}
        public string Charg {get; set;}
       public Boolean selected {get; set;}
        public ResopnseRecord (string pl,string stloc,string sku,string skuinfo,decimal totl,decimal aviqty,decimal prprice,string unitmesr,string grad){
           Werks=pl;
           Lgort=stloc;
           Matnr=sku;
           Maktx=skuinfo;
           Clabs=totl;
           Avqty=aviqty;
           ExchageFee=prprice;
           Meins=unitmesr;
           charg=grad;
           selected=False;
        }
        
    }
    
     public class AvilableQTYRequestbody {
        public msgHeadersRequest inputHeaders {get;set;}        
        public RequestBody body {get;set;}
    }
    
     public class AvilableQTYResponsebody {
        public msgHeadersResponse inputHeaders {get;set;}        
        public responseBody body {get;set;}
    }
    
      public class responseBody {
          public Etstockbody EtStock {get;set;}
          Public String EvCode {get; set;}
          Public String EvMessage {get; set;}
      }
     public class Etstockbody {
          public list<ResopnseRecord> item {get;set;}
     }
    
      public class RequestBody {
          Public String ivCompany {get; set;}
          Public String ivExSloc {get; set;}
          Public String ivSearchModel {get; set;}
          Public String ivExPlant {get; set;}
          Public String ivCuky {get; set;}
          Public String ivSalesOrg {get; set;}
          Public String ivOriPrice {get; set;}
          Public String ivAccount {get; set;}
          Public String ivModel {get; set;}
          Public String ivExGrade {get; set;}
          Public String ivExchType {get; set;}
          Public String ivSflag {get; set;}
      }
    
     public class msgHeadersRequest{
        public string MSGGUID {get{string s = giveGUID(); return s;}set;} // GUID for message
        public string IFID {get{return 'IF_SVC_10';}set;} // interface id, created automatically, so need logic
        public string IFDate {get{return datetime.now().formatGMT('YYYYMMddHHmmss');}set;} // interface date
        
        public msgHeadersRequest(){
            
        }
    
        
    } 
    
    public class msgHeadersResponse {
        public string MSGGUID {get;set;} // GUID for message
        public string IFID {get;set;} // interface id, created automatically, so need logic
        public string IFDate {get;set;} // interface date
        public string MSGSTATUS {get;set;} // start, success, failure (ST , S, F)
        public string ERRORCODE {get;set;} // error code
        public string ERRORTEXT {get;set;} // error message
    }  
    
   public static string giveGUID(){
        Blob b = Crypto.GenerateAESKey(128);
        String h = EncodingUtil.ConvertTohex(b);
        String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
        return guid;
    }
    
    
    public ExchangeInventoryCheck(ApexPages.StandardController controller) {
             responselist= new List<ResopnseRecord>();
              cs=(Case) controller.getRecord();
              showlookup=true;
             // selectedcnt=0;
              if(cs.id != Null){
                 cs=[select id,Device_Type_f__c,Model_Code__c,Selected_Model_Code__c,Selected_Storage_Location__c,Available_QTY__c,Selected_Model_Grade__c,RA_Request_Number__c,Bypass__c,Parent_Case__c from case where id=:cs.id];
                 searchmode=cs.Model_Code__c;
                 
                 if(cs.RA_Request_Number__c !=null&& cs.RA_Request_Number__c !=''){
                     
                     ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'This case already Submitted for RA Request.'));
                     showlookup=false;
                 }
                 
                   if(cs.Parent_Case__c){
                     
                     ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Inventory Check Should be done on Each Child Case, not on the Parent Case.'));
                     showlookup=false;
                 }
                 
                  if(cs.Device_Type_f__c=='WiFi'){
                     
                     ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Inventory Check Should be done on Connected Devices(IMEI). '));
                     showlookup=false;
                 }
               }
    }
    
    
    
    
    
    public static string webcallout(string body){ 
         system.debug('************************************** INSIDE CALLOUT METHOD');
         System.debug('##requestBody ::'+body);
          string partialEndpoint = Test.isRunningTest() ? 'asdf' : Integration_EndPoints__c.getInstance('SVC-10').partial_endpoint__c;
        System.Httprequest req = new System.Httprequest();
        
        HttpResponse res = new HttpResponse();
        req.setMethod('POST');
        req.setBody(body);
        req.setEndpoint('callout:X012_013_ROI'+partialEndpoint); 
        
        req.setTimeout(120000);
         req.setHeader('Content-Type', 'application/json');
        req.setHeader('Accept', 'application/json');
    
        Http http = new Http();  
        
                     if(!Test.isRunningTest()){
                          res = http.send(req);
                        }else{
                           res = Testrespond(req);
                        }
        
        
        
       
        System.debug('******** my resonse' + res);
        system.debug('*****Response body::' + res.getBody());
       
        return res.getBody();     
    }
    
    
    public PageReference makecallout(){
        SeacrhforAvailablelist();
           return null;
    }
    
     public void SelectandContinue(){
         Boolean selected=False;
         selectedcnt=0;
       for(ResopnseRecord d: responselist) 
        {
            if(d.selected  == true)
            {
                if(selected==False){
                    selected=True;
                    selectedcnt=selectedcnt+1;
                     String bypass =  cs.Bypass__c;
                    cs.Selected_Model_Code__c=d.Matnr;
                    cs.Selected_Storage_Location__c=d.Lgort;
                    cs.Available_QTY__c=d.Avqty;
                    cs.Selected_Model_Grade__c=d.charg;
                      cs.Bypass__c=(bypass == 'N') ? 'Y' : 'N'; 
                }else{
                   ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'You Can Select Only One Line item.'));
                   selected=False;
                   break;
                }
               
            }
        }
        
        if(selected=True && selectedcnt ==1){
            update cs;
            
            //return new PageReference('javascript:window.close();');
        }
        
       //  return null;
     }
     
     
     public void SeacrhforAvailablelist(){
        AvilableQTYRequestbody request=new AvilableQTYRequestbody();
        request.inputHeaders = new msgHeadersRequest();
         RequestBody bdy=new RequestBody();
         String company;
          String org;
        if ('Connected'.equals(cs.Device_Type_f__c)) {
            company = 'C370';
            org='3107';
        } else if ('WiFi'.equals(cs.Device_Type_f__c)) {
            company = 'C310';
            org='3101';
        }
         bdy.ivExSloc='WS40';
         bdy.ivExPlant ='V319';
         bdy.ivCompany =company;
         
         String mdl=cs.Model_Code__c;
         mdl=mdl.Substring(0,4)+'%'+mdl.right(3);
         String mdllength=String.valueOf(mdl.length());
         bdy.ivSearchModel =mdl;// We need to pass like "SM-G%ATT"
         bdy.ivCuky ='USD';
         bdy.ivExchType ='ET010';
         bdy.ivAccount ='0001901224';//
         bdy.ivModel ='';
         bdy.ivExGrade ='';
         bdy.ivOriPrice ='';
         bdy.ivSalesOrg =org;
         bdy.ivSflag='10';//'10';
         
         request.body = bdy;
          String serialized = JSON.serialize(request);
          system.debug('request :: ' + serialized);
          
          
         String response= webcallout(serialized);// Calling Web Callout Method 
         if(response !='' && response !=null){
              AvilableQTYResponsebody res = (AvilableQTYResponsebody) json.deserialize(response,AvilableQTYResponsebody.class);
                system.debug('##  Response ::'+res);
                if(res.inputHeaders.MSGSTATUS == 'S'){
                     handleSuccess(res,serialized);
                 }else{
                     String er=res.inputHeaders.ERRORTEXT;
                   //  handleError(mq,er);
                   handleError(res,serialized);
                 }
             
         }else{
             
         }
         
         
     } 
     
     
     public void handleSuccess(AvilableQTYResponsebody res,string req){
         if(res.body.EvCode=='S'){
               for(ResopnseRecord r:res.body.EtStock.item){
                     responselist.add(new ResopnseRecord(r.Werks,r.Lgort,r.Matnr,r.Maktx,r.Clabs,r.Avqty,r.ExchageFee,r.Meins,r.charg));  
                }  
         }else{
             System.debug('Error ::'+res.body.EvMessage);
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,res.body.EvMessage));
           }
         
         
         message_queue__c mq=new message_queue__c();
             mq.Object_Name__c='Case';
             mq.Integration_Flow_Type__c='SVC-10';
             mq.Status__c='sucess';
             mq.MSGUID__c=res.inputHeaders.MSGGUID;
             mq.Response_Body__c=String.valueOf(res);
             mq.Request_Body__c=req;
             mq.Identification_Text__c=cs.id;
             insert mq;
     }
     
      public void handleError(AvilableQTYResponsebody res,string req){
         if(res.body.EvCode=='S'){
               for(ResopnseRecord r:res.body.EtStock.item){
                     responselist.add(new ResopnseRecord(r.Werks,r.Lgort,r.Matnr,r.Maktx,r.Clabs,r.Avqty,r.ExchageFee,r.Meins,r.charg));  
                }  
         }else{
             String err='Something went wrong. Please contact System Admin. Error:'+res.inputHeaders.ERRORTEXT;
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,res.inputHeaders.ERRORTEXT));
           }
         
         
         message_queue__c mq=new message_queue__c();
             mq.Object_Name__c='Case';
             mq.Integration_Flow_Type__c='SVC-10';
             mq.Status__c='failed';
             mq.MSGUID__c=res.inputHeaders.MSGGUID;
             mq.Response_Body__c=String.valueOf(res);
             mq.Request_Body__c=req;
             mq.Identification_Text__c=cs.id;
             insert mq;
     }
     
     
      public Static HTTPResponse Testrespond(HTTPRequest req) {          
            String outputBodyString = '{"inputHeaders":{"MSGGUID":"da8132a7-04d5-0ca3-9e09-30d6928ab2a2","IFID":"IF_SVC_10","IFDate":"20181218074255","MSGSTATUS":"S","ERRORTEXT":null},"body":{"EtStock":{"item":[{"Werks":"V319","Lgort":"WS40","Charg":"A","Matnr":"SM-G900VZKSVZW","Maktx":"MOBILE,SM-G900V,BLACK,VZW","Clabs":"8.0","Cinsm":"0.0","Cspem":"0.0","Avqty":"6.0","Verpr":"0.0","Waers":null,"Meins":"PC","Netpr":"0.0","ExchageFee":"0.0"},{"Werks":"V319","Lgort":"WS40","Charg":"R","Matnr":"SM-G900VZKSVZW","Maktx":"MOBILE,SM-G900V,BLACK,VZW","Clabs":"9.0","Cinsm":"0.0","Cspem":"0.0","Avqty":"7.0","Verpr":"0.0","Waers":null,"Meins":"PC","Netpr":"0.0","ExchageFee":"0.0"},{"Werks":"V319","Lgort":"WS40","Charg":"A","Matnr":"SM-G920VZKAVZW","Maktx":"MOBILE,SM-G920V,BLACK,VZW","Clabs":"95.0","Cinsm":"0.0","Cspem":"0.0","Avqty":"91.0","Verpr":"0.0","Waers":null,"Meins":"PC","Netpr":"0.0","ExchageFee":"0.0"},{"Werks":"V319","Lgort":"WS40","Charg":"R","Matnr":"SM-G920VZKAVZW","Maktx":"MOBILE,SM-G920V,BLACK,VZW","Clabs":"100.0","Cinsm":"0.0","Cspem":"0.0","Avqty":"99.0","Verpr":"0.0","Waers":null,"Meins":"PC","Netpr":"0.0","ExchageFee":"0.0"}]},"EvCode":"S","EvMessage":"Success."}}';    
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatus('2');
            res.setStatusCode(200);
            res.setBody(outputBodyString);
            
            return res;                
      } 
    

}