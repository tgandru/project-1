@isTest
private class ExchangeInventoryCheckTest {

	private static testMethod void testmethod1() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Account acc1 = new account(Name ='TestAcc1', Type='Customer',SAP_Company_Code__c = 'AAA',BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07650');
        insert acc1;
        Id rtId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Exchange').getRecordTypeId();
         Zipcode_Lookup__c zip  = new Zipcode_Lookup__c(
            Name = '07650',
            City_Name__c = 'Palisades Park',
            Country_Code__c = 'US',
            State_Code__c = 'NJ',
            State_Name__c = 'New Jersey'
        );
        insert zip;

        Product2 prod1 = new Product2(
                name ='MI-test',
                SAP_Material_ID__c = 'MI-test',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6'
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = acc1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=acc1.id,
            assetid = ast.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent;

        Contact contact1 = new Contact( 
            AccountId = acc1.id, 
            Email = 'svcTest2@svctest.com', 
            LastName = 'Named Caller2',
            Named_Caller__c = true,
            MailingStreet = '1234 Main',
            MailingCity = 'Chicago',
            MailingState = 'IL',
            MailingCountry = 'US',
            FirstName = 'Named Caller2');
        insert contact1;

        Device__c dev1 = new Device__c(
            Account__c = acc1.id,
            Entitlement__c = ent.id,
            Status__c = 'Registered',
            Serial_Number__c = '111111111111111',
            Model_Name__c = 'MI-test'
            );
        insert dev1;
        
         Case c1 = new Case(
            AccountId = acc1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtid,
            Shipping_Address_1__c = '411 E Brinkerhoff Ave, Palisades Park', 
            Shipping_City__c = 'PALISADES PARK', 
            Shipping_Country__c = 'US', 
            Shipping_State__c = 'NJ', 
            Shipping_Zip_Code__c = '07650',
            status='New',
            Device__c = dev1.id
            );
        insert c1;
                Test.startTest();
                 Pagereference pf= Page.ExchangeInventoryCheck;
                   pf.getParameters().put('id',c1.id);
                   test.setCurrentPage(pf);
               
                 apexPages.standardcontroller sc = new apexPages.standardcontroller(c1);
                 ExchangeInventoryCheck cls=new ExchangeInventoryCheck(sc);
                 cls.makecallout();
                 cls.SelectandContinue();
                Test.StopTest();
        
	}

}