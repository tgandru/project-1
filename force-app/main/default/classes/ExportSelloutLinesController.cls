public with sharing class ExportSelloutLinesController {
public list<Sellout_Products__c> prdcts{get; set;}
public string headers{get; set;}
public string filename{get; set;}
public Sellout__c so{get; set;}
public boolean isplanrec {get; set;}
public List<wrapper> lstwrapper {get; set;}
  public class wrapper{
        public string Carrier{get; set;}
        public string Carriersku{get; set;}
        public string SamsungSKU{get; set;}
        public string Quantity{get; set;}
        public string ILCL{get; set;}
        public string soDate{get; set;}
        public string status{get; set;}
        public string precheckresult{get; set;}
                
    }
    public ExportSelloutLinesController(ApexPages.StandardController controller) {
          lstwrapper = new List<wrapper>();
      prdcts =[select id,name,IL_CL__c,Carrier__c,Carrier_SKU__c,Quantity__c,Samsung_SKU__c,Sellout_Date__c,Sold_To_Code__c,Status__c,sellout__r.RecordType.Name,Pre_Check_Result__c from Sellout_Products__c where sellout__c=:controller.getRecord().Id];
      so=[select RecordType.Name,id,name,Approval_Status__c,Closing_Month_Year1__c,Description__c,Has_Attachement__c,Total_CL_Qty__c,Total_IL_Qty__c,Subsidiary__c from Sellout__c where id=:controller.getRecord().Id limit 1];
      filename='SelloutData_'+so.RecordType.Name+'_'+so.Closing_Month_Year1__c;
      
      if(so.RecordType.Name =='Plan'){
          headers='Carrier,Samsung SKU,Quantity,IL/CL,SellOut Date,Status,Pre-Check Result';
          isplanrec =true;
      }else{
          headers='Carrier,Samsung SKU,Quantity,IL/CL,SellOut Date,Status,Pre-Check Result';
          isplanrec =false;
      }
    }
    
      public void exportToExcel(){
    
            if(prdcts.size()>0){
                for(Sellout_Products__c p:prdcts){
               // string s=p.Sellout_Date__c.format('mmddyyy') ;
                    String yr=String.valueof(p.Sellout_Date__c.Year());
                    String mm=String.valueof(p.Sellout_Date__c.Month());
                    String dd=String.valueof(p.Sellout_Date__c.day());
                    if(mm.length()==1){
                      mm= '0' + mm;
                    }
                    if(dd.length()==1){
                      dd= '0' + dd;
                    }
                    wrapper w = new wrapper();
                    w.Carrier=p.Carrier__c;
                    w.Carriersku=p.Carrier_SKU__c;
                    w.SamsungSKU=p.Samsung_SKU__c;
                    w.Quantity=String.valueof(p.Quantity__c);
                    w.ILCL=p.IL_CL__c;
                    w.soDate=mm+dd+yr;
                    w.status=p.Status__c;
                    w.precheckresult=p.Pre_Check_Result__c ;
                  lstwrapper.add(w);   
                }
            
            }
    
    }

}