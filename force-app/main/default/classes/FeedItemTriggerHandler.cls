public with sharing class FeedItemTriggerHandler
{
	public static void afterInsert(List<FeedItem> triggerNew){
		//trigger milestone completion
		List<ID> CaseIds = new List<ID>();
		for (FeedItem fi:triggerNew){
		      string s=string.valueOf(fi.parentid);
		      system.debug('Parent Record ID'+s);
		     if(s !=null && s !=''){
                if (s.startsWith('500') ){ 
                      	if (fi.Visibility == 'AllUsers'){
            				CaseIds.add(fi.parentid);
            			}
                }  
             }
		    
		
			
			
			
		}
		system.debug(CaseIds.size());
		if (CaseIds.isEmpty() == false){
			SVC_MilestoneUtils.completeMilestone(CaseIds, 'First Response Time', System.now());
			SVC_MilestoneUtils.completeMilestone(CaseIds, 'Status Update', System.now());
		}	

	}

	public static void afterUpdate(List<FeedItem> triggerNew, Map<ID,FeedItem> oldMap){
		List<ID> CaseIds = new List<ID>();

		for (FeedItem fi:triggerNew){
		         string s=string.valueOf(fi.parentid);
		          system.debug('Parent Record ID'+s);
		     if(s !=null && s !=''){
                if (s.startsWith('500') ){ 
                      	if (fi.Visibility == 'AllUsers'){
            				if (oldMap.get(fi.id) != null && (oldMap.get(fi.id)).Visibility != 'AllUsers'  ){
            					CaseIds.add(fi.parentid);
            				}
            			}
                }  
             }
		    
		    
			
			
			
		}
         	system.debug(CaseIds.size());
		if (CaseIds.isEmpty() == false){
			SVC_MilestoneUtils.completeMilestone(CaseIds, 'First Response Time', System.now());
			SVC_MilestoneUtils.completeMilestone(CaseIds, 'Status Update', System.now());
		}	
		
	}
}