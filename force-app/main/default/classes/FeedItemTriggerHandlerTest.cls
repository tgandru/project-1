@isTest
private class FeedItemTriggerHandlerTest {
    
    @isTest static void test_FeedItemTrigger_and_handler() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Profile adminProfile = [SELECT Id FROM Profile Where Name ='B2B Service System Administrator'];

        User admin1 = new User (
            FirstName = 'admin1',
            LastName = 'admin1',
            Email = 'admin1seasde@example.com',
            Alias = 'admint1',
            Username = 'admin1seaa@example.com',
            ProfileId = adminProfile.Id,
            TimeZoneSidKey = 'America/New_York',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US'
        );
        
        insert admin1;

        BusinessHours bh24 = [SELECT Id FROM BusinessHours WHERE name = 'B2B Service Hours 24x7'];
        BusinessHours bh12 = [SELECT Id FROM BusinessHours WHERE name = 'B2B Service Hours 12x5'];
    
        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        );

        insert testAccount1;

        Account testAccount2 = new Account (
            Name = 'TestAcc2',
            RecordTypeId = accountRT.Id,
            BillingStreet = '123 main St',
            BillingCity ='Lisle',
            BillingState ='IL',
            BillingCountry='US',
            BillingPostalCode='60532'
            
        );

        insert testAccount2;

        RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];
        Product2 prod1 = new Product2(
                name ='MI-test',
                SAP_Material_ID__c = 'MI-test',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = productRT.id
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Asset ast2 = new Asset(
            name ='Assert-MI-Test2',
            Accountid = testAccount2.id,
            Product2Id = prod1.id
            );
        insert ast2;

        Slaprocess sla = [select id from SlaProcess where name like 'Enhanc%' 
            and isActive = true limit 1 ];

        Entitlement ent = new Entitlement(
            name ='test Entitlement1',
            accountid=testAccount1.id,
            assetid = ast.id,
            BusinessHoursId = bh12.id,
            startdate = system.today()-1,
            slaprocessid = sla.id
            );
        insert ent;

        Entitlement ent2 = new Entitlement(
            name ='test Entitlement2',
            accountid=testAccount2.id,
            assetid = ast2.id,
            BusinessHoursId = bh12.id,
            startdate = system.today()-1
            );
        insert ent2;
        
        Contact contact = new Contact(); 
        contact.AccountId = testAccount1.id; 
        contact.Email = 'svcTest@svctest.com'; 
        contact.LastName = 'Named Caller'; 
        contact.Named_Caller__c = true;
        contact.FirstName = 'Named Caller';
        contact.MailingStreet = '1234 Main';
        contact.MailingCity = 'Chicago';
        contact.MailingState = 'IL';
        contact.MailingCountry = 'US';
        insert contact; 

        String entlId;
        if (ent != null)
        entlId = ent.Id; 

        Test.startTest();
        Group q= [select id from group where type='Queue' limit 1 ];
        List<Case> cases = new List<Case>{};
        if (entlId != null){
            Case c1 = new Case(Subject = 'Test Case with Entitlement ', 
            EntitlementId = entlId, ContactId = contact.id,
            origin = 'Phone',
            ownerid=admin1.id);
            
            insert c1;

            Case c2 = new Case(Subject = 'Test Case with Entitlement ', 
            EntitlementId = entlId, ContactId = contact.id,
            origin = 'Web',
            ownerid=q.id);
            
            insert c2;

            Feeditem item = new Feeditem(
                parentid=c2.id,
                body = 'test chatter',
                Visibility = 'AllUsers'
                );
            insert item;

            Feeditem item2 = new Feeditem(
                parentid=c2.id,
                body = 'test chatter',
                Visibility = 'InternalUsers'
                );
            insert item2;

            item2.Visibility = 'AllUsers';
            update item2;
        }
        Test.stopTest();
    }
    
}