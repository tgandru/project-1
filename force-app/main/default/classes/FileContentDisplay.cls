public class FileContentDisplay {
    @AuraEnabled
    public static List<ContentDocumentLink> getFiles(String parentId){
        
        List<ContentDocumentLink> lstFiles = [SELECT Id, ContentDocumentId, ContentDocument.Title, 
                                            ContentDocument.FileType, LinkedEntityId, 
                                            LinkedEntity.Name,ShareType, Visibility
                   							FROM ContentDocumentLink 
                   							WHERE LinkedEntityId = :parentId];
        /*
        List<Attachment> lstFiles = [select Id, Name, ContentType from Attachment 
                                     where ParentId=:parentId];
		*/
        System.debug('no of files ='+lstFiles.size());
        return lstFiles;
    }
    
    //Added by Thiru--To add delete file feature--04/04/2019
    @AuraEnabled
    public static string deleteFiles(String fileId){
        ContentDocumentLink f = [SELECT Id,ContentDocument.Title FROM ContentDocumentLink WHERE id=:fileId];
        System.debug('Delete File Id--->'+f.Id);
		Delete f;
        return f.ContentDocument.Title;
    }
}