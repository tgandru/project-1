@isTest
public class FileContentDisplayTest {
    
    static testMethod void contentDisplayTest() {
        
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        
        Account testAccount = new Account();
        testAccount.Name='Test Account' ;
        insert testAccount;
        
        Call_Report__c cr=new Call_Report__c();
        cr.Name='cr1';
        cr.Primary_Account__c=testAccount.Id;
        insert cr;
        
        string base64Data = EncodingUtil.urlDecode('Lightning', 'UTF-8');
        string fileName='LightningFile';
 		ContentVersion cv = new ContentVersion();
        cv.Title=fileName;
        cv.PathOnClient= fileName;
        cv.VersionData= EncodingUtil.base64Decode(base64Data);
        insert cv;
        
        cv = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :cv.Id LIMIT 1];
        
        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.ContentDocumentId = cv.ContentDocumentId;
        cdl.LinkedEntityId = cr.id; // Entity Id to link to
        cdl.ShareType = 'V';
        insert cdl;
        
        Test.startTest();
            FileContentDisplay.getFiles(cr.Id);
            FileContentDisplay.deleteFiles(cdl.id);
        Test.stopTest();
        
    }
}