@isTest
public class FileContentUploadControllerTest {

    static testMethod void fileContentUploadTest() {
        
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        
        Account testAccount = new Account();
        testAccount.Name='Test Account' ;
        insert testAccount;
        
        Call_Report__c cr=new Call_Report__c();
        cr.Name='cr1';
        cr.Primary_Account__c=testAccount.Id;
        insert cr;
        
        Test.startTest();
        Id fileId = FileContentUploadController.saveChunk(cr.Id, 'TestFile', 'test', 'text','');
        FileContentUploadController.saveChunk(cr.Id, 'TestFile', 'test', 'text',fileId);     
        Test.stopTest();
                
	}
}