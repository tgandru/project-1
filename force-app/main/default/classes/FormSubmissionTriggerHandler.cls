public class FormSubmissionTriggerHandler {



Public Static void afterInsertupdate(List<Form_Submission__c> triggernew){
    Map<id,Opportunity> oppmap=new Map<id,Opportunity>();
    
    For(Form_Submission__c f:triggernew){
        
        If(f.Opportunity__c !=null){
            oppmap.put(f.Opportunity__c,new Opportunity(Id=f.Opportunity__c,Has_Project_Information__c	=True ));
        }
    }
    
    If(oppmap.Size()>0){
        Database.Update(oppmap.Values(), false);
    }
}

Public Static void afterDelete(List<Form_Submission__c> triggernew){
    Map<id,Opportunity> oppmap=new Map<id,Opportunity>();
    
    For(Form_Submission__c f:triggernew){
        
        If(f.Opportunity__c !=null ){
            oppmap.put(f.Opportunity__c,new Opportunity(Id=f.Opportunity__c,Has_Project_Information__c	=False ));
        }
    }
    
    If(oppmap.Size()>0){
        Database.Update(oppmap.Values(), false);
    }
}

}