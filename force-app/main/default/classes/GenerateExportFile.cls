global class GenerateExportFile implements Database.Batchable<sObject>,Database.stateful{
      global String condition;
    global integer linescount;
    global string Attachmentid;
    global string header;
    global string parentid;
    global Set<string> keys;
    
    global integer futureMonth;
    global integer futureYear;
    
    global GenerateExportFile(string condition){
        date furtureDate = System.today().addMonths(12);
        futureMonth = furtureDate.month();
        futureYear = furtureDate.year();
        system.debug('futureMonth::'+futureMonth+'----futureYear::'+futureYear);
        
        this.condition=condition;
        linescount=0;
        keys=new Set<string>();
        keys.add('Opportunity Number');
        keys.add('Project Name');
        keys.add('Project Type');
        keys.add('Shipping Plant');//Thiru::Added Shippping Plant to below query--04/23/2019
        keys.add('Owner Name');
        keys.add('Owner Role');
        keys.add('Status');
        keys.add('Builder');
        keys.add('Customer');
        keys.add('Rollout Start Date');
        keys.add('Rollout End Date');
        keys.add('Number of Living Units');
        keys.add('Opportunity Last modified date');
        keys.add('Category');
        keys.add('ProductOption');
        keys.add('Model Code');
        keys.add('Net Unit Price');
        //keys.add('Buffer');
        keys.add('Quote Line QTY');
        keys.add('Quote Line %');
        keys.add('Total Actual QTY');
        keys.add('Total Plan QTY');
        Integer yr=System.now().Year();
        Integer mnth=System.now().Month();
       Set<string> actul=new Set<string>();
       Set<string> pln=new Set<string>();
      
        //for(date d=System.today(); d <= System.today().addMonths(24); d=d.addMonths(1)){
        for(date d=System.today(); d <= System.today().addMonths(12); d=d.addMonths(1)){//Thiru: Added 12 months--04/22/2019
               Integer mth=d.Month();     
               Integer year=d.Year();
               if(mth<10){
                 String val=String.valueOf(year)+'0'+String.valueOf(mth);  
                 pln.add(val);
               } else{
                    String val=String.valueOf(year)+String.valueOf(mth);  
                 pln.add(val);
               }
        }
        
        for(date d=System.today().addMonths(-6); d <= System.today(); d=d.addMonths(1)){
               Integer mth=d.Month();     
               Integer year=d.Year();
               if(mth<10){
                 String val='Actual -'+String.valueOf(year)+'0'+String.valueOf(mth);  
                 actul.add(val);
               } else{
                    String val='Actual -'+String.valueOf(year)+String.valueOf(mth);  
                 actul.add(val);
               }
        }
        keys.addAll(actul);
        keys.addAll(pln);
        message_queue__c mq=new message_queue__c(Status__c='Sucess',Integration_Flow_Type__c='Export HA OPP Data');
         insert mq;
         parentid=mq.id;
    }
    
     global Database.QueryLocator start(Database.BatchableContext BC){
                 String cntquery = '';
                 //Thiru::Added Shippping_Plant__c to below query--04/23/2019
                 cntquery += ' Select id,name,CloseDate,StageName,Owner.Name,Owner.UserRole.Name,Project_Type__c,Roll_Out_Start__c,Roll_Out_End_Formula__c,Lastmodifieddate,Rollout_Duration__c,Number_of_Living_Units__c,Opportunity_Number__c,HA_Shipping_Plant__c from Opportunity where   ';
                 cntquery +=condition;
                 return Database.getQueryLocator(cntquery);
     }
     
     global void execute (Database.Batchablecontext BC, list<Opportunity> scope){
        Map<Id,Opportunity> oppmap=new Map<Id,Opportunity>();
         for(Opportunity o:scope){
             oppmap.put(o.id,o);
         }
           Date endate=System.today().addMonths(14);
          Integer numberOfDays = Date.daysInMonth(endate.year(), endate.month());
           Date enddate=Date.newInstance(endate.year(), endate.month(), numberOfDays);
          date stdate=System.today().addMonths(-6);
          Date startdate=Date.newInstance(stdate.year(), stdate.month(), 1);
         List<AggregateResult> arlist=new List<AggregateResult>([SELECT YearMonth__c ym, 
                                                                      Roll_Out_Product__r.Product__r.SAP_Material_ID__c rop,
                                                                      Sum(Plan_Quantity__c) qty ,
                                                                      Avg(Roll_Out_Product__r.SalesPrice__c) netpr  , 
                                                                      Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.StageName stg, 
                                                                      Sum(Actual_Quantity__c) actqty, 
                                                                      Sum(Actual_Revenue__c) actrev, Sum(Plan_Revenue__c) rev, 
                                                                      Roll_Out_Product__r.Package_Option__c optn,
                                                                      Roll_Out_Product__r.Product__r.Family catg ,
                                                                      Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Builder__r.Name bldr,
                                                                      Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Account.Name custmr ,
                                                                      Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Roll_Out_Start__c strt,
                                                                      Sum(Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Rollout_Duration__c) durtn ,
                                                                      Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__c opp,
                                                                      Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Owner.Name ownr, 
                                                                      avg(Roll_Out_Product__r.Roll_Out_Percent__c) rpper,
                                                                      avg(Roll_Out_Product__r.Actual_Quantity__c) ttlactqty, 
                                                                      avg(Roll_Out_Product__r.SBQQ_QuoteLine__r.SBQQ__Quantity__c) qutqty,
                                                                      avg(Roll_Out_Product__r.Plan_Quantity__c) plnqty,
                                                                      Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Project_State__c prjst,
                                                                      Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Project_Name__c prj  
                                                                      FROM Roll_Out_Schedule__c 
                                                                      WHERE  Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__c in:oppmap.Keyset() 
                                                                             and (Plan_Date__c<=:enddate and Plan_Date__c >=:startdate)
                                                                             
                                                                       GROUP BY YearMonth__c, 
                                                                                Roll_Out_Product__r.Product__r.SAP_Material_ID__c,
                                                                                Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__c,
                                                                                Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Owner.Name,
                                                                                Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Project_State__c,
                                                                                Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Project_Name__c,
                                                                                Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Roll_Out_Start__c,
                                                                                Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.StageName,
                                                                                Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Builder__r.Name,
                                                                                Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Account.Name,
                                                                                Roll_Out_Product__r.Package_Option__c ,
                                                                                Roll_Out_Product__r.Product__r.Family 
                                                                       ORDER BY Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__c,
                                                                                YearMonth__c asc]);
     
         String csvstring='';
         String months='';
         integer count=0;
         Map<String,String> keymap=new Map<String,String>();
            for(AggregateResult ar: arlist){
                
                months=months+','+String.valueOf(ar.get('ym'));
                                 String startline='{';
                                  if(count>0){
                                      startline=',{';
                                  }
                                   String endline='}';
                                   
                               if(oppmap.containsKey(String.valueOf(ar.get('opp')))){
                                     Opportunity op=oppmap.get(String.valueOf(ar.get('opp')));
                                     String key=op.Opportunity_Number__c+'-'+String.valueOf(ar.get('rop'))+'-'+String.valueOf(ar.get('optn'));
                                   if(keymap.containsKey(key)){
                                       String value=keymap.get(key);
                                       value=value+',"'+String.valueOf(ar.get('ym'))+'":"'+String.valueOf(ar.get('qty'))+'"';
                                      value=value+',"Actual -'+String.valueOf(ar.get('ym'))+'":"'+String.valueOf(ar.get('actqty'))+'"';
                                        keymap.put(key,value);
                                   }else{
                                       String cust='';
                                       String bulider='';
                                       String prjName='';
                                       String OppName=String.valueOf(op.Name).replaceAll(',','-');
                                       if(String.valueOf(ar.get('custmr')) !=null && String.valueOf(ar.get('custmr')) !=''){
                                           cust=String.valueOf(ar.get('custmr')).replaceAll(',','-');
                                       }
                                       
                                        if(String.valueOf(ar.get('bldr')) !=null && String.valueOf(ar.get('bldr')) !=''){
                                          bulider=String.valueOf(ar.get('bldr')).replaceAll(',','-');
                                       }
                                       
                                        if(String.valueOf(ar.get('prj')) !=null && String.valueOf(ar.get('prj')) !=''){
                                           prjName=String.valueOf(ar.get('prj')).replaceAll(',','-');
                                       }
                                     
                                       
                                       String monthyr=String.valueOf(ar.get('ym'));
                                       String rolloutend=String.valueOf(op.Roll_Out_End_Formula__c.month())+'/'+String.valueOf(op.Roll_Out_End_Formula__c.day())+'/'+String.valueOf(op.Roll_Out_End_Formula__c.year());
                                        String rolloutstart=String.valueOf(op.Roll_Out_Start__c.month())+'/'+String.valueOf(op.Roll_Out_Start__c.day())+'/'+String.valueOf(op.Roll_Out_Start__c.year());
                                        
                                        Datetime last = op.lastmodifieddate;
                                        Integer offset = UserInfo.getTimezone().getOffset(last);
                                        Datetime usertime = last.addSeconds(offset/1000);

                                  //Thiru::Added Shippping Plant line below--04/23/2019       
                                  String line='"Opportunity Number":"'+op.Opportunity_Number__c+'","Opportunity Name":"'+OppName+'","Project Name":"'+prjName+'","Project State":"'+String.valueOf(ar.get('prjst'))+'","Owner Name":"'+String.valueOf(ar.get('ownr'))+'","Owner Role":"'+op.owner.UserRole.Name+'"';
                                   line=line+',"Shipping Plant":"'+op.HA_Shipping_Plant__c+'"';
                                   line=line+',"Status":"'+String.valueOf(ar.get('stg'))+'"';
                                  line=line+',"Builder":"'+bulider+'"';
                                  line=line+',"Customer":"'+cust+'"';
                                  line=line+',"Rollout Start Date":"'+rolloutstart+'"';
                                   line=line+',"Project Type":"'+op.Project_Type__c+'"';
                                   line=line+',"Net Unit Price":"'+String.valueOf(ar.get('netpr'))+'"';
                                  line=line+',"Rollout End Date":"'+rolloutend+'"';
                                  //line=line+',"Buffer":"'+String.valueOf(ar.get('buff'))+'"';
                                  line=line+',"Number of Living Units":"'+String.valueOf(op.Number_of_Living_Units__c)+'"';
                                   line=line+',"Opportunity Last modified date":"'+usertime+'"';
                                   line=line+',"Actual -'+monthyr+'":"'+String.valueOf(ar.get('actqty'))+'"';
                                   line=line+',"Total Actual QTY":"'+String.valueOf(ar.get('ttlactqty'))+'"';
                                  line=line+',"Category":"'+String.valueOf(ar.get('catg'))+'","ProductOption":"'+String.valueOf(ar.get('optn'))+'"';
                                  line=line+',"Model Code":"'+String.valueOf(ar.get('rop'))+'","Quote Line QTY":"'+String.valueOf(ar.get('qutqty'))+'","Quote Line %":"'+String.valueOf(ar.get('rpper'))+'","Total Plan QTY":"'+String.valueOf(ar.get('plnqty'))+'","'+String.valueOf(ar.get('ym'))+'":"'+String.valueOf(ar.get('qty'))+'"';
                                //  line=line.replaceAll('null', 'N/A');
                                
                                       keymap.put(key,line);
                                   }
                                
                               }
                           
                       }
                       
                       
                       for(String s:keymap.values()){
                           String startline='{';
                                  if(count>0){
                                      startline=',{';
                                  }
                                   String endline='}';
                               csvstring=csvstring+startline+s+endline;    
                                  count++;  
                                   
                       }
                       
                        String jsondata='{ "lines":['+csvstring+']}'; 
         System.debug('jsondata::'+jsondata);
         System.debug('months::'+months);
                         Map<String, Object> meta = (Map<String, Object>) JSON.deserializeUntyped(jsondata);
         list<Object> lines=(list<Object>)meta.get('lines'); 
        
        List<Map<String, Object>> mapList = new List<Map<String, Object>>();
        Set<String> keySet = new Set<String>();
        
        for (Object entry : lines) {
            Map<String, Object> m = (Map<String, Object>)(entry);
            keySet.addAll(m.keySet());
            mapList.add(m);
        }
        
       
        List<List<String>> csvLines = new List<List<String>>();
        
        for (Integer i = 0; i <= mapList.size(); i++) {
            csvLines.add(new List<String>());
        }
        
        for (String key : keys) {
            csvLines.get(0).add('"' + key + '"');
            
            for (Integer i = 1; i <= mapList.size(); i++) {
                csvLines.get(i).add('"' + (String)(mapList.get(i - 1).get(key)) + '"');
            }
        }
        
        String csvFile = '';
    
        for (List<String> csvLine : csvLines) {
            String line = '';
            for (Integer i = 0; i < csvLine.size() - 1; i++) {
                
                
                  line += csvLine.get(i) + ',';  
            
            }
            line += csvLine.get(csvLine.size() - 1);
            line=line.replaceAll('null','N/A');
            
         
            csvFile += line + '\n';
        }
                    if(Attachmentid !=null && Attachmentid!=''){
                           Attachment att=[Select id,name,body from Attachment where id=:Attachmentid];
                           String sCurrentBody = att.Body.toString();
                           List<string> linestr=csvFile.split('\n');
                                String body='';
                             for(Integer i=1;i<linestr.size();i++){
                                   body += linestr[i] + '\n';
                             }
                             
                           sCurrentBody=sCurrentBody+ body; 
                            att.Body = Blob.valueOf(sCurrentBody);
                            update att;
                           
                       }else{
                          Attachment attch=new Attachment();
                         attch.ParentId=parentid;
                         attch.Name='HA_Rollout_Data_Export.csv';
                         attch.Description=BC.getJobId();
                         attch.ContentType='application/vnd.ms-excel';
                         string body=csvFile;
                         attch.body=blob.valueOf(body);
                         insert attch;
           
                         Attachmentid=attch.id;
                       }
        
     }
     
     global void finish(Database.Batchablecontext BC){
      /*   
         if(Attachmentid !=null && Attachmentid !=''){
         
          Attachment att=[Select id,name,body from Attachment where id=:Attachmentid];
                           String sCurrentBody = att.Body.toString();
            String jsondata='{ "lines":['+sCurrentBody+']}';               
         
         
         Map<String, Object> meta = (Map<String, Object>) JSON.deserializeUntyped(jsondata);
         list<Object> lines=(list<Object>)meta.get('lines'); 
        
        List<Map<String, Object>> mapList = new List<Map<String, Object>>();
        Set<String> keySet = new Set<String>();
        
        for (Object entry : lines) {
            Map<String, Object> m = (Map<String, Object>)(entry);
            keySet.addAll(m.keySet());
            mapList.add(m);
        }
        
        List<String> keys = new List<String>(keySet);
        //keys.sort();
        
        List<List<String>> csvLines = new List<List<String>>();
        
        for (Integer i = 0; i <= mapList.size(); i++) {
            csvLines.add(new List<String>());
        }
        
        for (String key : keys) {
            csvLines.get(0).add('"' + key + '"');
            
            for (Integer i = 1; i <= mapList.size(); i++) {
                csvLines.get(i).add('"' + (String)(mapList.get(i - 1).get(key)) + '"');
            }
        }
        
        String csvFile = '';
        for (List<String> csvLine : csvLines) {
            String line = '';
            for (Integer i = 0; i < csvLine.size() - 1; i++) {
                line += csvLine.get(i) + ',';
            }
            line += csvLine.get(csvLine.size() - 1);
            line=line.replaceAll('null','N/A');
            csvFile += line + '\n';
        }
    
          if(csvFile !='' && csvFile !=null){
              
              Attachment attch=new Attachment();
                         attch.ParentId=parentid;
                         attch.Name='HA_Rollout_Data_Export.csv';
                         attch.Description=BC.getJobId();
                         attch.ContentType='application/vnd.ms-excel';
                         attch.body=blob.valueOf(csvFile);
                         insert attch;
           
          }
            
         } 
       */ 
     }
}