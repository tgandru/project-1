global class GetITb2brolesBatch implements Database.Batchable<sObject>,Schedulable {

    global void execute(SchedulableContext SC) {
         database.executeBatch(new GetITb2brolesBatch()) ;
    }
      global Database.QueryLocator start(Database.BatchableContext BC)
    {
       
        return Database.getQueryLocator([select Id,Name from UserRole where  Name ='IT B2B']);
    }
    
    
    global void execute(Database.BatchableContext BC, List<UserRole> scope)
    {
        List<IT_B2B_Roles__c> newrec=new List<IT_B2B_Roles__c>();
        set<id> roleid=new set<id>();
        for(UserRole u:scope){
            roleid.add(u.id);
        }
        if(roleid.size()>0){
             Set<id> subroleids=CheckRoleandSubordinates.getAllSubRoleIdsIT(roleid);
               if(subroleids.size()>0){
                   for(UserRole u:[select id,name from UserRole  where id in:subroleids or id in:roleid]){
                       IT_B2B_Roles__c i=new IT_B2B_Roles__c();
                       i.Name=u.Name;
                       i.Role_Name__c=u.Name;
                       i.Role_ID__c=u.ID;
                       newrec.add(i);
                   } 
               }
             
             
        }
       
        if(newrec.size()>0){
            Database.insert(newrec,false);
        }
        
        
        
        
    }
    
    
    global void finish(Database.BatchableContext BC) {
        
    }

}