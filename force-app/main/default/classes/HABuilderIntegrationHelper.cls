public class HABuilderIntegrationHelper {
    private static Integration_EndPoints__c contractEndpoint = Integration_EndPoints__c.getInstance('Flow-050');
    private static Integration_EndPoints__c shippedQtyEndpoint = Integration_EndPoints__c.getInstance('Flow-051'); 
    
    public static void createContract(Message_Queue__c mq){
        Map<Id, List<SBQQ__QuoteLine__c>> opptyQLineMap = new Map<Id, List<SBQQ__QuoteLine__c>>();
        List<SBQQ__QuoteLine__c> qLines = [
            select
            BD_Requested_Price_c__c
            ,Number_Field_by_10__c
            ,Package_Option__c
            ,SBQQ__ListPrice__c
            ,SBQQ__NetPrice__c
            ,SBQQ__Product__r.SAP_Material_ID__c
            ,SBQQ__ProductName__c
            ,SBQQ__Quantity__c
            ,SBQQ__Quote__r.Contract_Order_Number__c
            ,SBQQ__Quote__r.Name
            ,SBQQ__Quote__r.SBQQ__Account__r.Parent.SAP_Company_Code__c
            ,SBQQ__Quote__r.SBQQ__Account__r.SAP_Company_Code__c
            ,SBQQ__Quote__r.SBQQ__EndDate__c
            ,SBQQ__Quote__r.SBQQ__Opportunity2__r.Account.Name
            ,SBQQ__Quote__r.SBQQ__Opportunity2__r.Builder__c
            ,SBQQ__Quote__r.SBQQ__Opportunity2__r.CloseDate
            ,SBQQ__Quote__r.SBQQ__Opportunity2__r.Number_of_Living_Units__c
            ,SBQQ__Quote__r.SBQQ__Opportunity2__r.Owner.Name
            ,SBQQ__Quote__r.SBQQ__Opportunity2__r.Project_City__c
            ,SBQQ__Quote__r.SBQQ__Opportunity2__r.Project_Name__c
            ,SBQQ__Quote__r.SBQQ__Opportunity2__r.Project_State__c
            ,SBQQ__Quote__r.SBQQ__Opportunity2__r.Project_Street__c
            ,SBQQ__Quote__r.SBQQ__Opportunity2__r.Project_Type__c
            ,SBQQ__Quote__r.SBQQ__Opportunity2__r.Project_Zip_Code__c
            ,SBQQ__Quote__r.SBQQ__StartDate__c
            ,SBQQ__TotalDiscountAmount__c
            from
            SBQQ__QuoteLine__c
            where
            SBQQ__Quote__c = :mq.Identification_Text__c];
        
        if(qLines != null && !qLines.isEmpty()){
            
            SBQQ__Quote__c quote = [
                select 
                Contract_Order_Number__c
                ,SBQQ__Status__c
                from
                SBQQ__Quote__c
                where
                id = :qLines[0].SBQQ__Quote__c];
            
            if(
                !String.isEmpty(quote.Contract_Order_Number__c)
                || !'Approved'.equals(quote.SBQQ__Status__c)
            ) {
                mq.MSGSTATUS__c = 'failed';
                mq.ERRORCODE__c = 'E';
                mq.ERRORTEXT__c = 'GERP contract was already created for this quote.';
                update mq;
                return;
            }
            
            //TODO - Setup endpoint
            //string layoutId = mq.IFID__c;
            string layoutId = contractEndpoint.layout_id__c;
            HABuilderIntegrationRequest request = new HABuilderIntegrationRequest();
            //Header
            request.inputHeaders = new cihStub.msgHeadersRequest(layoutId);
            
            //Body
            request.body = new HABuilderIntegrationRequestBody();            
            request.body.piZsysno = 'SYS1738';
            request.body.piLegacyUserId = '95088394';
            request.body.piIfKey = cihStub.giveGUID(); //'10000000000001';
            
            //pisHeader
            request.body.pisHeader = new HABuilderIntegrationHeader();
            request.body.pisHeader.salesOrg = '3101';
            request.body.pisHeader.salesOff = '3106';
            request.body.pisHeader.reqDateH = formatDate(System.today());
            request.body.pisHeader.purchNoC = qLines[0].SBQQ__Quote__r.Name;
            request.body.pisHeader.priceDate = formatDate(System.today());
            request.body.pisHeader.docType = 'YQC1';
            request.body.pisHeader.division = '00';
            request.body.pisHeader.distrChan = '10';
            request.body.pisHeader.ctValidT = formatDate(qLines[0].SBQQ__Quote__r.SBQQ__Opportunity2__r.CloseDate == null?null:qLines[0].SBQQ__Quote__r.SBQQ__Opportunity2__r.CloseDate.addYears(2));
            request.body.pisHeader.ctValidF = formatDate(qLines[0].SBQQ__Quote__r.SBQQ__Opportunity2__r.CloseDate);
            
            request.body.pitSchedule = new List<HABuilderIntegrationSchedule>();
            request.body.pitPartner = new List<HABuilderIntegrationPartner>();
            request.body.pitItem = new List<HABuilderIntegrationItem>();
            request.body.pitCondition = new List<HABuilderIntegrationCondition>();
            
            //pisPartner
            //for(Integer i=1; i<=2; i++){
            String partnerCode = qLines[0].SBQQ__Quote__r.SBQQ__Account__r.Parent != null && qLines[0].SBQQ__Quote__r.SBQQ__Account__r.Parent.SAP_Company_Code__c != null ? qLines[0].SBQQ__Quote__r.SBQQ__Account__r.Parent.SAP_Company_Code__c : qLines[0].SBQQ__Quote__r.SBQQ__Account__r.SAP_Company_Code__c;
            request.body.pitPartner.add(new HABuilderIntegrationPartner('AG', partnerCode));
            request.body.pitPartner.add(new HABuilderIntegrationPartner('WE', partnerCode));//TODO - Account_id (Account object: Sold To Distro Only), Account_id (Account object: Ship To Distro Only)?
            //}
            //PisSchedule, pisItem and pisCondition
            for(SBQQ__QuoteLine__c qLine : qLines){
                request.body.pitSchedule.add(new HABuilderIntegrationSchedule((Integer)qLine.SBQQ__Quantity__c, qLine.SBQQ__Quote__r.SBQQ__StartDate__c, String.valueOf(qLine.Number_Field_by_10__c)));
                request.body.pitItem.add(new HABuilderIntegrationItem((Integer)qLine.SBQQ__Quantity__c, qLine.SBQQ__Product__r.SAP_Material_ID__c, String.valueOf(qLine.Number_Field_by_10__c)));
                request.body.pitCondition.add(new HABuilderIntegrationCondition(
                    String.valueOf(qLine.Number_Field_by_10__c)
                    , qLine.SBQQ__NetPrice__c - qLine.SBQQ__ListPrice__c
                    ,'16M1'
                    ,1));                
            }
            
            try{
                String responseBody = callContractAPI(request);
                system.debug(' ## Flow050 response from  mock call out' + responseBody);
                if(string.isNotBlank(responseBody)){ 
                    HABuilderIntegrationResponse response = (HABuilderIntegrationResponse) JSON.deserialize(responseBody, HABuilderIntegrationResponse.class);
                    if(response.inputHeaders!=null){
                        if(response.inputHeaders.MSGSTATUS == 'S'){ 
                            String businessError = getBusinessError(response);
                            if(businessError != null){
                                handlerBusinessError(mq, response, null, businessError); 
                            }else{
                                handleSuccess(mq, response, quote);
                            }
                        } else {                        
                            system.debug('## Flow050 Error');          
                            handlerErrorWithResponse(mq, response,null);
                        }
                    }else{
                        System.debug('## Flow050 Else block ');
                        handleError(mq);
                    }
                } else {
                    System.debug('## Flow050 Outer Else block');
                    handleError(mq);
                }
            } catch(exception ex){
                System.debug('## Flow050 ERROR IN RESPONSE ');
                system.debug(ex.getmessage()); 
                handleError(mq);
            }
        }
    }
    
    private static String getBusinessError(HABuilderIntegrationResponse response){
        String businessError = null;
        if(response != null && response.body != null 
           && response.body.petBapiReturn != null && !response.body.petBapiReturn.isEmpty()){
               List<String> errList = new List<String>();
               for(PetBapiReturnResponse bapiRet : response.body.petBapiReturn){
                   if(bapiRet.typeN == 'E'){
                       errList.add(bapiRet.message);
                   }
               }
               if(!errList.isEmpty()){
                   businessError = String.join(errList, ',');
                   if(businessError.length() > 255){
                       businessError = businessError.left(255);
                   }
               }
           }
        return businessError;
    }
    
    public static String callContractAPI(HABuilderIntegrationRequest request){
        //TODO - Setup endpoint
        string partialEndpoint = Test.isRunningTest() ? 'asdf' : contractEndpoint.partial_endpoint__c;
        
        system.debug(JSON.serialize(request));
        
        System.Httprequest req = new System.Httprequest();        
        HttpResponse res = new HttpResponse();
        req.setMethod('POST');
        req.setBody(JSON.serialize(request).replaceAll('currency_x','currency'));
        
        req.setEndpoint('callout:X012_013_ROI'+partialEndpoint); 
        req.setTimeout(120000);
        
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Accept', 'application/json');
        
        Http http = new Http();     
        res = http.send(req);
        system.debug(res.getBody());
        return res.getBody();            
    }
    
    //Response Handlers
    public static void handleError(message_queue__c mq){
        mq.retry_counter__c += 1;
        mq.status__c = mq.retry_counter__c > 5 ? 'failed' : 'failed-retry';
        upsert mq;      
    }
    
    public static void handlerErrorWithResponse(message_queue__c mq, HABuilderIntegrationResponse response, HAShippedQuantityResponse response1){
        mq.ERRORCODE__c = (response!=null)?response.inputHeaders.errorCode:response1.inputHeaders.errorCode; 
        mq.ERRORTEXT__c = (response!=null)?response.inputHeaders.errorText:response1.inputHeaders.errorText;
        mq.MSGSTATUS__c = (response!=null)?response.inputHeaders.MSGSTATUS:response1.inputHeaders.MSGSTATUS; // TODO: Check why Shipped Qty has STATUS instead of MSGStatus
        mq.MSGUID__c = (response!=null)?response.inputHeaders.msgGUID:response1.inputHeaders.msgGUID;
        mq.IFID__c = (response!=null)?response.inputHeaders.ifID:response1.inputHeaders.ifID;
        mq.IFDate__c = (response!=null)?response.inputHeaders.ifDate:response1.inputHeaders.ifDate;
        handleError(mq);
    }
    
    public static void handlerBusinessError(message_queue__c mq, HABuilderIntegrationResponse response, HAShippedQuantityResponse response1, String businessError){
        mq.ERRORTEXT__c = businessError;
        mq.MSGSTATUS__c = 'E';
        mq.MSGUID__c = (response!= null)?response.inputHeaders.msgGUID:response1.inputHeaders.msgGUID;
        mq.IFID__c = (response!= null)?response.inputHeaders.ifID:response1.inputHeaders.ifID;
        mq.IFDate__c = (response!= null)?response.inputHeaders.ifDate:response1.inputHeaders.ifDate;
        handleError(mq);
    }
    
    public static void handleSuccess(message_queue__c mq, HABuilderIntegrationResponse response, SBQQ__Quote__c quote){
        mq.status__c = 'success';
        mq.MSGSTATUS__c = response.inputHeaders.MSGSTATUS;
        mq.MSGUID__c = response.inputHeaders.msgGUID;
        mq.External_Id__c = response.body.peVbeln;
        mq.IFDate__c = response.inputHeaders.ifDate;
        quote.Contract_Order_Number__c = response.body.peVbeln;
        contractEndpoint.last_successful_run__c = System.now();
        //update quote;
        List<Database.SaveResult> saveResults = Database.update(new List<SObject>{contractEndpoint, mq, quote});
        // Iterate through each returned result
        for (Database.SaveResult sr : saveResults) {
            if (!sr.isSuccess()) {
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Fields that affected this error: ' + err.getFields());
                }
            }
        }        
    }
    
    //Request/Response Objects
    public Class HABuilderIntegrationRequest{
        public cihStub.msgHeadersRequest inputHeaders {get;set;}        
        public HABuilderIntegrationRequestBody body {get;set;}        
    }
    public Class HABuilderIntegrationRequestBody{
        String piZsysno {get; set;}//Quote
        String piLegacyUserId {get; set;}//Quote
        String piIfKey {get; set;}//Quote
        HABuilderIntegrationHeader pisHeader {get; set;}
        List<HABuilderIntegrationSchedule> pitSchedule {get; set;}
        List<HABuilderIntegrationPartner> pitPartner {get; set;}
        List<HABuilderIntegrationItem> pitItem {get; set;}
        List<HABuilderIntegrationCondition> pitCondition {get; set;}
        
    }
    public Class HABuilderIntegrationHeader{
        String salesOrg {get; set;}
        String salesOff {get; set;}
        String reqDateH {get; set;}
        String purchNoC {get; set;}
        String priceDate {get; set;}
        String docType {get; set;}
        String division {get; set;}
        String distrChan {get; set;}
        String ctValidT {get; set;}
        String ctValidF {get; set;}        
    }
    public Class HABuilderIntegrationSchedule{
        Integer reqQty {get; set;}
        String reqDate {get; set;}
        String itemNumber {get; set;}
        public HABuilderIntegrationSchedule(Integer reqQty, Date reqDate, String itemNumber){
            this.reqQty = reqQty;
            this.reqDate = formatDate(reqDate);
            this.itemNumber = itemNumber;
        }
    }    
    public Class HABuilderIntegrationPartner{
        String partnRole {get; set;}
        String partnNumb {get; set;}
        public HABuilderIntegrationPartner( String partnRole, String partnNumb){
            this.partnRole = partnRole;
            this.partnNumb = partnNumb;
        }        
    }     
    public Class HABuilderIntegrationItem{
        Integer targetQty {get; set;}
        String material {get; set;}
        String itmNumber {get; set;}
        public HABuilderIntegrationItem(Integer targetQty, String material, String itmNumber){
            this.targetQty = targetQty;
            this.material = material;
            this.itmNumber = itmNumber;
        }        
    }        
    public Class HABuilderIntegrationCondition{
        String itmNumber {get; set;}
        Decimal condValue {get; set;}
        String condType {get; set;}      
        Integer condPUnt {get; set;}
        String currency_x = 'USD';
        public HABuilderIntegrationCondition(String itmNumber, Decimal condValue, String condType, Integer condPUnt){
            this.itmNumber = itmNumber;
            this.condValue = condValue;
            this.condType = condType;
            this.condPUnt = condPUnt;
        }        
    }           
    public class HABuilderIntegrationResponse{
        public cihStub.msgHeadersResponse inputHeaders {get;set;}  
        public HABuilderIntegrationResponseBody body {get; set;}
    }    
    public class HABuilderIntegrationResponseBody{
        public String peVbeln {get; set;}
        public List<PetBapiReturnResponse> petBapiReturn {get; set;}
        List<HABuilderIntegrationSchedule> pitSchedule {get; set;}
        List<HABuilderIntegrationPartner> pitPartner {get; set;}
        List<HABuilderIntegrationItem> pitItem {get; set;}
        List<HABuilderIntegrationCondition> pitCondition {get; set;}        
    }
    public class PetBapiReturnResponse{
        String typeN {get; set;}
        String id {get; set;}
        String numberN {get; set;}
        String message {get; set;}
        String logMsgNo {get; set;}
        String messageV1 {get; set;}
        String messageV2 {get; set;}
        String parameter {get; set;}
        String row {get; set;}
        String systemN {get; set;}
    }
    
    /********************************************************************* SHIPPED QUANTITY ***************************************/
    
    public static void updateShippedQuantity(Message_Queue__c mq){
        
        string layoutId = shippedQtyEndpoint.layout_id__c;
        HAShippedQuantityRequest request = new HAShippedQuantityRequest();
        //Header
        request.inputHeaders = new cihStub.msgHeadersRequest(layoutId);
        
        //Body
        request.body = new HAShippedQuantityRequestBody();            
        request.body.piZsysno = 'SYS1738'; 
        request.body.piLegacyUserId = '95088394'; 
        request.body.piIfKey = cihStub.giveGUID(); //'10000000000001'; 
        request.body.piFlag = '02'; 
        request.body.piSalesOrg = '3101'; 
        request.body.piPurchNo = ''; 
        request.body.piSoldTo = ''; 
        request.body.piCreateOnFrom = '20090101' ; 
        request.body.piCreateOnTo = '99991231' ; 
        
        
        //pitSalesOrder
        request.body.pitSalesOrder = new List<HAShippedQuantitySalesOrder>();
        request.body.pitSalesOrder.add(new HAShippedQuantitySalesOrder(mq.Identification_Text__c));
        
        //pitSelectionFlag
        String strFlag= '';
        request.body.pitSelectionFlag = new HAShippedQuantitySelectionFlag(strFlag);
        
        try{
            //Call - 1
            String responseBody = callShippedQuantityAPI(request);
            system.debug(' ## Flow051 response from  mock call out' + responseBody);
            
            if(string.isNotBlank(responseBody)){ 
                HAShippedQuantityResponse response = (HAShippedQuantityResponse) JSON.deserialize(responseBody, HAShippedQuantityResponse.class);
                System.debug('HAShippedQuantityResponse ' +response);
                if(response.inputHeaders!=null){
                    if(response.inputHeaders.MSGSTATUS == 'S'){ //used MSGSTATUS instead of STATUS as given in JSON //DOUBLE CHECK
                        String businessError = getShippedQtyBusinessError(response);
                        if(businessError != null){
                            handlerBusinessError(mq, null,response, businessError); 
                        }else{
                            //Call - 2
                            callBillingQuantityForOrders(mq, request, response);
                        }
                    } else {                        
                        system.debug('## Flow051 Error');          
                        handlerErrorWithResponse(mq, null, response);
                    }
                }else{
                    System.debug('## Flow051 Else block ');
                    handleError(mq);
                }
            } else {
                System.debug('## Flow051 Outer Else block');
                handleError(mq);
            }
            
        } catch(exception ex){
            System.debug('## Flow051 ERROR IN RESPONSE ');
            system.debug(ex.getmessage()); 
            handleError(mq);
        }
    }
    
    
    public static String callShippedQuantityAPI(HAShippedQuantityRequest request){
        //TODO - Setup endpoint
        string partialEndpoint = Test.isRunningTest() ? 'asdf' : shippedQtyEndpoint.partial_endpoint__c;  
        
        system.debug(JSON.serialize(request));
        
        System.Httprequest req = new System.Httprequest();        
        HttpResponse res = new HttpResponse();
        req.setMethod('POST');
        req.setBody(JSON.serialize(request));
        
        req.setEndpoint('callout:X012_013_ROI'+partialEndpoint); 
        req.setTimeout(120000);
        
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Accept', 'application/json');
        
        Http http = new Http();     
        res = http.send(req);
        system.debug(res.getBody());
        return res.getBody();            
    }
    
    public static void callBillingQuantityForOrders(message_queue__c mq, HAShippedQuantityRequest request, HAShippedQuantityResponse response){
        
        //pitSalesOrder - retrieving the order number from the Reponse and forming the 2nd request
        set<string> oredercontract=new set<string>();
        request.body.pitSalesOrder = new List<HAShippedQuantitySalesOrder>();
        if(response != null && response.body != null 
           && response.body.petSalesOrderFlow != null && !response.body.petSalesOrderFlow.isEmpty()){
               for(HAShippedQuantitySalesOrderFlowResponse sof : response.body.petSalesOrderFlow){
                   //add the Order number only when the subsequent document type is 'C'
                   if(sof.VBELN!=null && sof.VBTYP_N=='C'){
                       request.body.pitSalesOrder.add(new HAShippedQuantitySalesOrder(sof.VBELN));
                      string s=string.valueOf(sof.VBELN)+string.valueOf(sof.POSNN);
                      system.debug('VBLEN+POSNN __'+s);
                      oredercontract.add(s);
                   }
               }
           }
        
        //pitSelectionFlag
        String strFlag= '';
        request.body.pitSelectionFlag = new HAShippedQuantitySelectionFlag(strFlag);     
        
        try{
            String responseBody = callShippedQuantityAPI(request);
            system.debug(' ## Flow051 response from  mock call out' + responseBody);
            
            if(string.isNotBlank(responseBody)){ 
                HAShippedQuantityResponse response2 = (HAShippedQuantityResponse) JSON.deserialize(responseBody, HAShippedQuantityResponse.class);
                if(response.inputHeaders!=null){
                    if(response.inputHeaders.MSGSTATUS == 'S'){
                        String businessError = getShippedQtyBusinessError(response);
                        if(businessError != null){
                            handlerBusinessError(mq,null, response2, businessError); 
                        }else{
                           //doubtful - Need to Check with Jeff
                            SBQQ__Quote__c quote = [select id, Contract_Order_Number__c from SBQQ__Quote__C where Contract_Order_Number__c =:mq.Identification_Text__c];
                            handleShippedQtySuccess(mq, response2, quote,oredercontract);
                        }
                    } else {                        
                        system.debug('## Flow051 Error');          
                        handlerErrorWithResponse(mq, null, response);
                    }
                }else{
                    System.debug('## Flow051 Else block ');
                    handleError(mq);
                }
            } else {
                System.debug('## Flow051 Outer Else block');
                handleError(mq);
            }
            
        } catch(exception ex){
            System.debug('## Flow051 ERROR IN RESPONSE ');
            system.debug(ex.getmessage()); 
            handleError(mq);
        }
    }
    
    //Sucess Handling for Shipped quantity
    public static void handleShippedQtySuccess(message_queue__c mq, HAShippedQuantityResponse response, SBQQ__Quote__c quote,set<string> oredercontract){
        mq.status__c = 'success';
        mq.MSGSTATUS__c = response.inputHeaders.MSGSTATUS;
        mq.MSGUID__c = response.inputHeaders.msgGUID;
        mq.IFDate__c = response.inputHeaders.ifDate;
        shippedQtyEndpoint.last_successful_run__c = System.now();
        
        List<SBQQ__QuoteLine__c> qLines = [select Id, Number_Field_by_10__c, SBQQ__Product__r.SAP_Material_ID__c, Total_Order_Quantity__c
                                           from SBQQ__QuoteLine__c where SBQQ__Quote__c=:quote.Id];
        
        List<SBQQ__QuoteLine__c> qLinestoUpdate = new List<SBQQ__QuoteLine__c>();
        
        Map<String,Decimal> orderQtyMap = new Map<String,Decimal>();
        if(response != null && response.body != null 
           && response.body.petSalesOrderFlow != null && !response.body.petSalesOrderFlow.isEmpty()){
               for(HAShippedQuantitySalesOrderFlowResponse sof : response.body.petSalesOrderFlow){
                   Decimal qtyValue = sof.RFMNG == null || sof.RFMNG.trim() == ''? 0 : Decimal.valueOf(sof.RFMNG);
                   String s2=string.valueOf(sof.VBELV)+string.valueOf(sof.POSNV);
                   system.debug('2nd response values '+s2);
                   if(oredercontract.contains(s2)){
                      if(!orderQtyMap.containsKey(sof.POSNV)){
                       if(sof.VBTYP_N=='M'){
                           orderQtyMap.put(sof.POSNV, qtyValue);
                       }
                       if(sof.VBTYP_N=='N' || sof.VBTYP_N=='O'){
                           orderQtyMap.put(sof.POSNV, qtyValue * -1);
                       }
                   }else{
                       if(sof.VBTYP_N=='M'){
                           orderQtyMap.put(sof.POSNV, orderQtyMap.get(sof.POSNV) + qtyValue) ;
                       }
                       if(sof.VBTYP_N=='N' || sof.VBTYP_N=='O'){
                           orderQtyMap.put(sof.POSNV,orderQtyMap.get(sof.POSNV) - qtyValue ) ;
                       }                       
                   } 
                   }
                   
               }
           }
        
        if(!qLines.isEmpty()){
            for(SBQQ__QuoteLine__c qLine : qLines){ 
                String qLineNumberStr = String.valueOf(qLine.Number_Field_by_10__c);//Vijay
                String qLineNumberStr1 = qLineNumberStr.leftPad(6).replace(' ', '0');
                if(orderQtyMap.containsKey(qLineNumberStr1)){
                    qLine.Total_Order_Quantity__c = orderQtyMap.get(qLineNumberStr1) != null ? orderQtyMap.get(qLineNumberStr1) : null;
                    qLinestoUpdate.add(qLine);
                }
            }
        }
        
        List<SObject> newListUpdate = new List<SObject>();
        newListUpdate.add(mq);
        newListUpdate.add(shippedQtyEndpoint);
        newListUpdate.addAll((List<Sobject>) qLinestoUpdate);
        
        
        //update quoteLine, mq and shippedQtyEndpoint
        List<Database.SaveResult> saveResults = Database.update(newListUpdate);
        
        // Iterate through each returned result
        for (Database.SaveResult sr : saveResults) {
            if (!sr.isSuccess()) {
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Fields that affected this error: ' + err.getFields());
                }
            }
        }        
    }
    
    //Shipped Quantity - getBusinessError
    private static String getShippedQtyBusinessError(HAShippedQuantityResponse response){
        String businessError = null;
        if(response != null && response.body != null && response.body.pesReturn != null){
            if(response.body.pesReturn.type == 'E'){
                businessError = response.body.pesReturn.message;
            }
        }
        return businessError;
    }
    
    
    //Request/Response Objects
    //HAShippedQuantityRequest
    public Class HAShippedQuantityRequest{
        public cihStub.msgHeadersRequest inputHeaders {get;set;}        
        public HAShippedQuantityRequestBody body {get;set;}        
    }
    
    //body
    public Class HAShippedQuantityRequestBody{
        String piZsysno {get; set;}//Quote
        String piLegacyUserId {get; set;}//Quote
        String piIfKey {get; set;}//Quote
        String piFlag {get;set;}
        String piSalesOrg {get;set;}
        String piPurchNo {get;set;}
        String piSoldTo {get;set;}
        String piCreateOnFrom {get;set;}
        String piCreateOnTo {get;set;}
        
        List<HAShippedQuantitySalesOrder> pitSalesOrder {get; set;}
        HAShippedQuantitySelectionFlag pitSelectionFlag {get; set;}
        
    }
    
    //pitSalesOrder
    public Class HAShippedQuantitySalesOrder{
        String SALES_ORDER_NO {get; set;}     
        public HAShippedQuantitySalesOrder(String SALES_ORDER_NO){
            this.SALES_ORDER_NO = SALES_ORDER_NO;
        }
    }    
    
    //pitSelectionFlag
    public Class HAShippedQuantitySelectionFlag{
        public String flag {get; set;}
        public List<String> tableName {get; set;}   
        
        public HAShippedQuantitySelectionFlag(String flag ){
            this.flag = flag;
            this.tableName = new List<String>{'PET_SALES_ORDER_FLOW'};
                }
    }
    
    //HAShippedQuantityResponse
    public class HAShippedQuantityResponse{
        public cihStub.msgHeadersResponse inputHeaders {get;set;}  
        public HAShippedQuantityResponseBody body {get; set;}
    }    
    
    //Body
    public class HAShippedQuantityResponseBody{
        HAShippedQuantityPesReturnResponse pesReturn {get; set;}
        List<HAShippedpetTrackingRepository> petTrackingRepository {get;set;} 
        public List<HAShippedQuantitySalesOrderFlowResponse> petSalesOrderFlow {get; set;}
    }
    
    //pesReturn
    public class HAShippedQuantityPesReturnResponse{
        String TYPE {get; set;}
        String ID {get; set;}
        String NumberN {get; set;} //system is a reserved word - shall we use numberN
        String MESSAGE {get; set;}
        String LOG_NO {get; set;}
        String LOG_MSG_NO {get; set;}
        String MESSAGE_V1 {get; set;}
        String MESSAGE_V2 {get; set;}
        String MESSAGE_V3 {get; set;}
        String MESSAGE_V4 {get; set;}
        String PARAMETER {get; set;}
        String ROW {get; set;}
        String FIELD {get; set;}
        String SystemN {get; set;} //system is a reserved word - shall we use systemN
    }
    //petTrackingRepository
    public class HAShippedpetTrackingRepository{
        
    }
    //petSalesOrderFlow
    public class HAShippedQuantitySalesOrderFlowResponse{
        String VBELV {get; set;}
        String POSNV {get; set;}
        String VBELN {get; set;}
        String POSNN {get; set;}
        String VBTYP_N {get; set;}
        String RFMNG {get; set;}
        String MATNR {get; set;}
    }
    
    
    //Utility Methods
    public static String formatDate(Date dt){
        return dt == null? null:((DateTime)dt).format('YYYYMMdd');
    }
}