@isTest
global class HABuilderMockHttpResponseGen implements HttpCalloutMock {
    private String respType{get;set;}
    public HABuilderMockHttpResponseGen(String err){
        respType = err;
    }
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        System.assertEquals('POST', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        if(respType == 'Success'){
            res.setBody(  '   {  '  + 
                        '   	"inputHeaders": {  '  + 
                        '   		"MSGGUID": "35b2e563-e5b6-a96c-f86c-d2e7994057a7",  '  + 
                        '   		"IFID": "IF_SFDC_KR_1085",  '  + 
                        '   		"IFDate": "20161026114153",  '  + 
                        '   		"MSGSTATUS": "S",  '  + 
                        '   		"ERRORTEXT": "",  '  + 
                        '   		"ERRORCODE": ""  '  + 
                        '   	},  '  + 
                        '   	"body": {  '  + 
                        '   		"peVbeln": "113169361",  '  + 
                        '   		"petBapiReturn": [{  '  + 
                        '   			"typeN": "S",  '  + 
                        '   			"id": "V4",  '  + 
                        '   			"numberN": "233",  '  + 
                        '   			"message": "SALES_HEADER_IN has been processed successfully",  '  + 
                        '   			"logMsgNo": "000000",  '  + 
                        '   			"messageV1": "VBAKKOM",  '  + 
                        '   			"messageV2": "C001",  '  + 
                        '   			"parameter": "SALES_HEADER_IN",  '  + 
                        '   			"row": "0",  '  + 
                        '   			"systemN": "GMPCLNT100"  '  + 
                        '   		}, {  '  + 
                        '   			"typeN": "S",  '  + 
                        '   			"id": "V4",  '  + 
                        '   			"numberN": "233",  '  + 
                        '   			"message": "SALES_ITEM_IN has been processed successfully",  '  + 
                        '   			"logMsgNo": "000000",  '  + 
                        '   			"messageV1": "VBAPKOM",  '  + 
                        '   			"messageV2": "000010",  '  + 
                        '   			"parameter": "SALES_ITEM_IN",  '  + 
                        '   			"row": "1",  '  + 
                        '   			"systemN": "GMPCLNT100"  '  + 
                        '   		}, {  '  + 
                        '   			"typeN": "S",  '  + 
                        '   			"id": "V4",  '  + 
                        '   			"numberN": "233",  '  + 
                        '   			"message": "SALES_ITEM_IN has been processed successfully",  '  + 
                        '   			"logMsgNo": "000000",  '  + 
                        '   			"messageV1": "VBAPKOM",  '  + 
                        '   			"messageV2": "000020",  '  + 
                        '   			"parameter": "SALES_ITEM_IN",  '  + 
                        '   			"row": "2",  '  + 
                        '   			"systemN": "GMPCLNT100"  '  + 
                        '   		}, {  '  + 
                        '   			"typeN": "S",  '  + 
                        '   			"id": "V4",  '  + 
                        '   			"numberN": "233",  '  + 
                        '   			"message": "SALES_ITEM_IN has been processed successfully",  '  + 
                        '   			"logMsgNo": "000000",  '  + 
                        '   			"messageV1": "VBAPKOM",  '  + 
                        '   			"messageV2": "000030",  '  + 
                        '   			"parameter": "SALES_ITEM_IN",  '  + 
                        '   			"row": "3",  '  + 
                        '   			"systemN": "GMPCLNT100"  '  + 
                        '   		}, {  '  + 
                        '   			"typeN": "S",  '  + 
                        '   			"id": "V4",  '  + 
                        '   			"numberN": "233",  '  + 
                        '   			"message": "SALES_CONDITIONS_IN has been processed successfully",  '  + 
                        '   			"logMsgNo": "000000",  '  + 
                        '   			"messageV1": "KONVKOM",  '  + 
                        '   			"messageV2": "C001",  '  + 
                        '   			"parameter": "SALES_CONDITIONS_IN",  '  + 
                        '   			"row": "1",  '  + 
                        '   			"systemN": "GMPCLNT100"  '  + 
                        '   		}, {  '  + 
                        '   			"typeN": "S",  '  + 
                        '   			"id": "V4",  '  + 
                        '   			"numberN": "233",  '  + 
                        '   			"message": "SALES_CONDITIONS_IN has been processed successfully",  '  + 
                        '   			"logMsgNo": "000000",  '  + 
                        '   			"messageV1": "KONVKOM",  '  + 
                        '   			"messageV2": "C001",  '  + 
                        '   			"parameter": "SALES_CONDITIONS_IN",  '  + 
                        '   			"row": "2",  '  + 
                        '   			"systemN": "GMPCLNT100"  '  + 
                        '   		}, {  '  + 
                        '   			"typeN": "S",  '  + 
                        '   			"id": "V4",  '  + 
                        '   			"numberN": "233",  '  + 
                        '   			"message": "SALES_CONDITIONS_IN has been processed successfully",  '  + 
                        '   			"logMsgNo": "000000",  '  + 
                        '   			"messageV1": "KONVKOM",  '  + 
                        '   			"messageV2": "C001",  '  + 
                        '   			"parameter": "SALES_CONDITIONS_IN",  '  + 
                        '   			"row": "3",  '  + 
                        '   			"systemN": "GMPCLNT100"  '  + 
                        '   		}, {  '  + 
                        '   			"typeN": "S",  '  + 
                        '   			"id": "V1",  '  + 
                        '   			"numberN": "311",  '  + 
                        '   			"message": "Quantity Contract 113169361 has been saved",  '  + 
                        '   			"logMsgNo": "000000",  '  + 
                        '   			"messageV1": "Quantity Contract",  '  + 
                        '   			"messageV2": "113169361",  '  + 
                        '   			"parameter": "SALES_HEADER_IN",  '  + 
                        '   			"row": "1",  '  + 
                        '   			"systemN": "GMPCLNT100"  '  + 
                        '   		}]  '  + 
                        '   	}  '  + 
                        '  }  ' ) ;             
        }else if(respType == 'Error'){
            res.setBody( '   {  '  + 
 '   	"inputHeaders": {  '  + 
 '   		"MSGGUID": "35b2e563-e5b6-a96c-f86c-d2e7994057a7",  '  + 
 '   		"IFID": "IF_SFDC_KR_1085",  '  + 
 '   		"IFDate": "20161026114153",  '  + 
 '   		"STATUS": "E",  '  + 
 '   		"ERRORTEXT": "Error",  '  + 
 '   		"ERRORCODE": "Error"  '  + 
 '   	}  ');
        }else if(respType == 'BusinessError'){
            res.setBody(  '   {  '  + 
                        '   	"inputHeaders": {  '  + 
                        '   		"MSGGUID": "35b2e563-e5b6-a96c-f86c-d2e7994057a7",  '  + 
                        '   		"IFID": "IF_SFDC_KR_1085",  '  + 
                        '   		"IFDate": "20161026114153",  '  + 
                        '   		"MSGSTATUS": "S",  '  + 
                        '   		"ERRORTEXT": "",  '  + 
                        '   		"ERRORCODE": ""  '  + 
                        '   	},  '  + 
                        '   	"body": {  '  + 
                        '   		"peVbeln": "113169361",  '  + 
                        '   		"petBapiReturn": [{  '  + 
                        '   			"typeN": "S",  '  + 
                        '   			"id": "V4",  '  + 
                        '   			"numberN": "233",  '  + 
                        '   			"message": "SALES_HEADER_IN has been processed successfully",  '  + 
                        '   			"logMsgNo": "000000",  '  + 
                        '   			"messageV1": "VBAKKOM",  '  + 
                        '   			"messageV2": "C001",  '  + 
                        '   			"parameter": "SALES_HEADER_IN",  '  + 
                        '   			"row": "0",  '  + 
                        '   			"systemN": "GMPCLNT100"  '  + 
                        '   		}, {  '  + 
                        '   			"typeN": "S",  '  + 
                        '   			"id": "V4",  '  + 
                        '   			"numberN": "233",  '  + 
                        '   			"message": "SALES_ITEM_IN has been processed successfully",  '  + 
                        '   			"logMsgNo": "000000",  '  + 
                        '   			"messageV1": "VBAPKOM",  '  + 
                        '   			"messageV2": "000010",  '  + 
                        '   			"parameter": "SALES_ITEM_IN",  '  + 
                        '   			"row": "1",  '  + 
                        '   			"systemN": "GMPCLNT100"  '  + 
                        '   		}, {  '  + 
                        '   			"typeN": "S",  '  + 
                        '   			"id": "V4",  '  + 
                        '   			"numberN": "233",  '  + 
                        '   			"message": "SALES_ITEM_IN has been processed successfully",  '  + 
                        '   			"logMsgNo": "000000",  '  + 
                        '   			"messageV1": "VBAPKOM",  '  + 
                        '   			"messageV2": "000020",  '  + 
                        '   			"parameter": "SALES_ITEM_IN",  '  + 
                        '   			"row": "2",  '  + 
                        '   			"systemN": "GMPCLNT100"  '  + 
                        '   		}, {  '  + 
                        '   			"typeN": "S",  '  + 
                        '   			"id": "V4",  '  + 
                        '   			"numberN": "233",  '  + 
                        '   			"message": "SALES_ITEM_IN has been processed successfully",  '  + 
                        '   			"logMsgNo": "000000",  '  + 
                        '   			"messageV1": "VBAPKOM",  '  + 
                        '   			"messageV2": "000030",  '  + 
                        '   			"parameter": "SALES_ITEM_IN",  '  + 
                        '   			"row": "3",  '  + 
                        '   			"systemN": "GMPCLNT100"  '  + 
                        '   		}, {  '  + 
                        '   			"typeN": "S",  '  + 
                        '   			"id": "V4",  '  + 
                        '   			"numberN": "233",  '  + 
                        '   			"message": "SALES_CONDITIONS_IN has been processed successfully",  '  + 
                        '   			"logMsgNo": "000000",  '  + 
                        '   			"messageV1": "KONVKOM",  '  + 
                        '   			"messageV2": "C001",  '  + 
                        '   			"parameter": "SALES_CONDITIONS_IN",  '  + 
                        '   			"row": "1",  '  + 
                        '   			"systemN": "GMPCLNT100"  '  + 
                        '   		}, {  '  + 
                        '   			"typeN": "E",  '  + 
                        '   			"id": "V4",  '  + 
                        '   			"numberN": "233",  '  + 
                        '   			"message": "Error",  '  + 
                        '   			"logMsgNo": "000000",  '  + 
                        '   			"messageV1": "KONVKOM",  '  + 
                        '   			"messageV2": "C001",  '  + 
                        '   			"parameter": "SALES_CONDITIONS_IN",  '  + 
                        '   			"row": "2",  '  + 
                        '   			"systemN": "GMPCLNT100"  '  + 
                        '   		}, {  '  + 
                        '   			"typeN": "S",  '  + 
                        '   			"id": "V4",  '  + 
                        '   			"numberN": "233",  '  + 
                        '   			"message": "SALES_CONDITIONS_IN has been processed successfully",  '  + 
                        '   			"logMsgNo": "000000",  '  + 
                        '   			"messageV1": "KONVKOM",  '  + 
                        '   			"messageV2": "C001",  '  + 
                        '   			"parameter": "SALES_CONDITIONS_IN",  '  + 
                        '   			"row": "3",  '  + 
                        '   			"systemN": "GMPCLNT100"  '  + 
                        '   		}, {  '  + 
                        '   			"typeN": "S",  '  + 
                        '   			"id": "V1",  '  + 
                        '   			"numberN": "311",  '  + 
                        '   			"message": "Quantity Contract 113169361 has been saved",  '  + 
                        '   			"logMsgNo": "000000",  '  + 
                        '   			"messageV1": "Quantity Contract",  '  + 
                        '   			"messageV2": "113169361",  '  + 
                        '   			"parameter": "SALES_HEADER_IN",  '  + 
                        '   			"row": "1",  '  + 
                        '   			"systemN": "GMPCLNT100"  '  + 
                        '   		}]  '  + 
                        '   	}  '  + 
                        '  }  ' ) ;              
        }
        
        res.setStatusCode(200);
        return res;
    }
}