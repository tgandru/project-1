@istest(seealldata=true)
public class HACloneOppCtlrTest{
    public static testmethod void cloneopp(){
        Opportunity op = [select id from Opportunity where recordtype.name='HA Builder' and SBQQ__PrimaryQuote__c!=null order by createddate desc limit 1];
        //ApexPages.currentPage().getparameters().put('id','0060q000001tWw0');
        ApexPages.currentPage().getparameters().put('id',op.id);
        Opportunity Opp = new Opportunity();
        ApexPages.StandardController Scontroller = new ApexPages.StandardController(opp);  
        HACloneOppCtrl opportunityCloneObj = new HACloneOppCtrl(Scontroller);
        PageReference pageRef=page.Clone_Opp;
        system.Test.setCurrentPage(pageRef);
        PageReference Ref = opportunityCloneObj.save();
    }
}