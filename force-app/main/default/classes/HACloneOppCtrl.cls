public class HACloneOppCtrl {
    public opportunity cloneOpp {get;set;}
    public string oldOpportunityId{get; set;}
    
    public HACloneOppCtrl(ApexPages.StandardController controller){
        cloneOpp =  new Opportunity();
        oldOpportunityId = ApexPages.currentPage().getParameters().get('Id');
        Opportunity opp = [Select Name, AccountId,Description, Number_of_Living_Units__c,
                           Roll_Out_End__c,Roll_Out_Plan__c,ForecastAmount__c,StageName ,Plan_Variance__c,QuotePlanVariance__c,
                           Rollout_Duration__c,Important_Notes__c,Project_Type__c,Stage_Description__c, 
                           Last_Modified_Date_Time__c, Last_Modified_By__c,Roll_Out_Start__c, Roll_Out_Status__c, 
                           Primary_Distributor__c, External_Opportunity_ID__c, Historical_Comments__c,OwnerID,CloseDate,
                           Drop_Reason__c,Builder__c,Project_Name__c,Project_State__c,Project_City__c,Project_Street__c,
                           Project_Zip_Code__c, Product_Information__c,Legacy_Id__c,SBQQ__QuotePricebookId__c,Pricebook2ID, 
                           RecordTypeId, Probability from Opportunity Where Id =: oldOpportunityId Limit 1];
        cloneOpp.Name = opp.Name; 
        cloneOpp.AccountId = opp.AccountId; 
        cloneOpp.StageName = 'Qualified'; 
        cloneOpp.Description = opp.Description; 
        cloneOpp.Number_of_Living_Units__c = opp.Number_of_Living_Units__c; 
        //cloneOpp.Roll_Out_End__c = opp.Roll_Out_End__c; 
        //cloneOpp.Roll_Out_Plan__c = opp.Roll_Out_Plan__c; 
        //cloneOpp.ForecastAmount__c = opp.ForecastAmount__c; 
        //cloneOpp.Plan_Variance__c = opp.Plan_Variance__c; 
        //cloneOpp.QuotePlanVariance__c = opp.QuotePlanVariance__c; 
        //cloneOpp.Rollout_Duration__c = opp.Rollout_Duration__c; 
        cloneOpp.Important_Notes__c = opp.Important_Notes__c; 
        cloneOpp.Project_Type__c = opp.Project_Type__c; 
        cloneOpp.Stage_Description__c = opp.Stage_Description__c; 
        cloneOpp.Last_Modified_Date_Time__c = opp.Last_Modified_Date_Time__c; 
        cloneOpp.Last_Modified_By__c = opp.Last_Modified_By__c; 
        //cloneOpp.Roll_Out_Start__c = opp.Roll_Out_Start__c; 
        cloneOpp.Roll_Out_Status__c = opp.Roll_Out_Status__c; 
        cloneOpp.Primary_Distributor__c = opp.Primary_Distributor__c; 
        cloneOpp.External_Opportunity_ID__c = opp.External_Opportunity_ID__c; 
        cloneOpp.Historical_Comments__c = opp.Historical_Comments__c; 
        cloneOpp.OwnerID = opp.OwnerID; 
        cloneOpp.CloseDate = opp.CloseDate; 
        cloneOpp.Drop_Reason__c = opp.Drop_Reason__c; 
        cloneOpp.Builder__c = opp.Builder__c; 
        cloneOpp.Project_Name__c = opp.Project_Name__c; 
        cloneOpp.Project_State__c = opp.Project_State__c; 
        cloneOpp.Project_City__c = opp.Project_City__c; 
        cloneOpp.Project_Street__c = opp.Project_Street__c; 
        cloneOpp.Project_Zip_Code__c = opp.Project_Zip_Code__c; 
        cloneOpp.Product_Information__c = opp.Product_Information__c; 
        //cloneOpp.Legacy_Id__c = opp.Legacy_Id__c; 
        //cloneOpp.SBQQ__QuotePricebookId__c = opp.SBQQ__QuotePricebookId__c; 
        //cloneOpp.Pricebook2ID = opp.Pricebook2ID; 
        cloneOpp.RecordTypeId = opp.RecordTypeId; 
        cloneOpp.Probability = opp.Probability; 
    }
    
    public PageReference save(){
        List<OpportunityLineItem> lstNewLineItems = new List<OpportunityLineItem>(); 
        List<PricebookEntry> lstPriceBooks = new List<PricebookEntry>();
        Map<String, String> pricebookEntryMap = new Map<String, String>();
        Map<Id, OpportunityLineItem> oldLineItemMap = new Map<Id, OpportunityLineItem>();
        
         try{
            insert cloneOpp;
            system.debug('Cloned Opp record--------->'+cloneOpp);
            //createQuoteAndLineItem(cloneOpp.Id, cloneOpp.Number_of_Living_Units__c, cloneOpp.Pricebook2ID);
         }catch(exception ee){
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'cloneOpp==>'+cloneOpp.Id+ee.getMessage()+'line==>'+ee.getLineNumber()));
             return null;
         }
         
         cloneOpp = [Select id, Name, AccountId, StageName,Description, Number_of_Living_Units__c,
                           Roll_Out_End__c,Roll_Out_Plan__c,ForecastAmount__c,Plan_Variance__c,QuotePlanVariance__c,
                           Rollout_Duration__c,Important_Notes__c,Project_Type__c,Stage_Description__c, 
                           Last_Modified_Date_Time__c, Last_Modified_By__c,Roll_Out_Start__c, Roll_Out_Status__c, 
                           Primary_Distributor__c, External_Opportunity_ID__c, Historical_Comments__c,OwnerID,CloseDate,
                           Drop_Reason__c,Builder__c,Project_Name__c,Project_State__c,Project_City__c,Project_Street__c,
                           Project_Zip_Code__c, Product_Information__c,Legacy_Id__c,SBQQ__QuotePricebookId__c,Pricebook2ID, 
                           RecordTypeId, Probability from Opportunity where Id =: cloneOpp.id];
         
         system.debug('Clone Opp Pricebook--------->'+cloneOpp.Pricebook2Id);
         createQuoteAndLineItem(cloneOpp.Id, cloneOpp.Number_of_Living_Units__c, cloneOpp.Pricebook2ID);
        /*
         for(PricebookEntry pbe : [select id,Product2Id from PricebookEntry where Pricebook2Id =: cloneOpp.Pricebook2Id]){
            pricebookEntryMap.put(pbe.Product2Id, pbe.Id);
         }
        
        for(OpportunityLineItem oli : [select OpportunityId,UnitPrice,Quantity,PricebookEntryId ,Product2Id,Requested_Price__c from OpportunityLineItem where OpportunityId =: oldOpportunityId ]){
            oldLineItemMap.put(oli.Product2Id, oli);
        }
        //Added to remove the inactive PBEs
        //Set<id> prodids = new set<id>();
        
        for(PricebookEntry pbe : [select id,IsActive,product2id,pricebook2id from PricebookEntry where Pricebook2id=:cloneOpp.Pricebook2Id and Product2id=:oldLineItemMap.keySet()]){
            if(pbe.IsActive==false){
                id prodid  = pbe.product2id;
                oldLineItemMap.remove(prodid);
            }
        }
        system.debug('map size------->'+oldLineItemMap.size());
        for(String Key: oldLineItemMap.keySet()){ 
            OpportunityLineItem oldLineItem = oldLineItemMap.get(Key);
            if(!pricebookEntryMap.containsKey(oldLineItem.Product2Id)){
                PricebookEntry pbEntry = new PricebookEntry(
                    Pricebook2Id = cloneOpp.Pricebook2Id,
                    Product2Id = oldLineItem.Product2Id,
                    UnitPrice = oldLineItem.UnitPrice,
                    IsActive = true
                );
                lstPriceBooks.add(pbEntry);
            }else{
                OpportunityLineItem newLineItem = new OpportunityLineItem();
                newLineItem.OpportunityId =  cloneOpp.Id;
                newLineItem.PricebookEntryId =  pricebookEntryMap.get(oldLineItem.Product2Id);
                //newLineItem.Product2Id =  oldLineItem.Product2Id;
                newLineItem.UnitPrice =  oldLineItem.UnitPrice;
                newLineItem.Quantity =  oldLineItem.Quantity;
                //newLineItem.Requested_Price__c =  oldLineItem.Requested_Price__c;
                lstNewLineItems.add(newLineItem);
            }
        }
       
        // if Product doesnot exist
        if(lstPriceBooks.size() > 0){
            insert lstPriceBooks;
            for(PricebookEntry priceBookEntryObj : lstPriceBooks){
                if(oldLineItemMap.containsKey(priceBookEntryObj.Product2Id)){
                    OpportunityLineItem oldLineItem = oldLineItemMap.get(priceBookEntryObj.Product2Id);
                    OpportunityLineItem newLineItem = new OpportunityLineItem();
                    newLineItem.OpportunityId =  cloneOpp.Id;
                    newLineItem.PricebookEntryId =  priceBookEntryObj.Id;
                    //newLineItem.Product2Id =  oldLineItem.Product2Id;
                    newLineItem.UnitPrice =  oldLineItem.UnitPrice;
                    newLineItem.Quantity =  oldLineItem.Quantity;
                    newLineItem.Requested_Price__c =  oldLineItem.Requested_Price__c;
                    lstNewLineItems.add(newLineItem);
                }
            }
        }
        if(lstNewLineItems.size() > 0){
            Savepoint sp = Database.setSavepoint();
           try{
               system.debug('lstNewLineItems==>'+lstNewLineItems.size());
                //insert lstNewLineItems;
                //createQuoteAndLineItem(cloneOpp.Id, cloneOpp.Number_of_Living_Units__c, cloneOpp.Pricebook2ID);
            }catch(Exception ee){
                Database.rollback( sp );
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ee.getMessage()+'line==>'+cloneOpp.Id+'id<=='+ee.getLineNumber()));
                return null;
            } 
        }
        */
        return new PageReference('/' + cloneOpp.Id);
    }    
    private void createQuoteAndLineItem(Id cloneOppId, Decimal qn, Id Pricebook2Id){
       
        Map<Id, Id> pricebookEntryMap = new Map<Id, Id>();
        List<SBQQ__Quote__c> cloneQuotes = new List<SBQQ__Quote__c>();
        List<SBQQ__QuoteLine__c> newQuoteItems = new List<SBQQ__QuoteLine__c>();
        for(PricebookEntry pbe : [select id,Product2Id,IsActive from PricebookEntry where Pricebook2Id =: Pricebook2Id]){
            if(pbe.IsActive==true){
                pricebookEntryMap.put(pbe.Product2Id, pbe.Id);
            }
        }

        Map<id, SBQQ__Quote__c> oldQuoteWithClone = new Map<Id, SBQQ__Quote__c>();
        Map<Id, List<SBQQ__QuoteLine__c>> oldQuoteWithLineItem = new Map<Id, List<SBQQ__QuoteLine__c>>();
        for(SBQQ__Quote__c quote :[SELECT id,SBQQ__PriceBook__c,Name, SBQQ__Opportunity2__c,SBQQ__Primary__c,SBQQ__SalesRep__c,SBQQ__PrimaryContact__c,recordTypeId,SBQQ__Account__c,
                                  SBQQ__Type__c,SBQQ__ExpirationDate__c,Expected_Shipment_Date__c,Last_Shipment_Date__c,SBQQ__Opportunity2__r.AccountId,
                                  (SELECT id,SBQQ__ListPrice__c,SBQQ__Product__c,SBQQ__Number__c,SBQQ__Quantity__c,SBQQ__ProductOption__c,SBQQ__PricingMethod__c,SBQQ__Renewal__c,SBQQ__PricingMethodEditable__c,
                                  SBQQ__CostEditable__c,SBQQ__Hidden__c,SBQQ__PriceEditable__c, SBQQ__Description__c,Package_Option__c FROM SBQQ__LineItems__r) FROM SBQQ__Quote__c WHERE SBQQ__Opportunity2__c =: oldOpportunityId and SBQQ__Primary__c=true]){
            //Created the clone quote for the new opportunity                                
            SBQQ__Quote__c cloneQuote = new SBQQ__Quote__c();
            cloneQuote.SBQQ__Opportunity2__c = cloneOppId; 
            //cloneQuote.Name= quote.Name; 
            cloneQuote.SBQQ__Primary__c = quote.SBQQ__Primary__c; 
            cloneQuote.SBQQ__SalesRep__c = quote.SBQQ__SalesRep__c; 
            cloneQuote.SBQQ__PrimaryContact__c = quote.SBQQ__PrimaryContact__c; 
            cloneQuote.recordTypeId = quote.recordTypeId; 
            //cloneQuote.SBQQ__Account__c = quote.SBQQ__Account__c;
            cloneQuote.SBQQ__Account__c = cloneOpp.AccountId;
            cloneQuote.SBQQ__Type__c = quote.SBQQ__Type__c; 
            //cloneQuote.SBQQ__Status__c = quote.SBQQ__Status__c;
            cloneQuote.SBQQ__ExpirationDate__c = quote.SBQQ__ExpirationDate__c; 
            cloneQuote.Expected_Shipment_Date__c = quote.Expected_Shipment_Date__c; 
            cloneQuote.Last_Shipment_Date__c = quote.Last_Shipment_Date__c;                           
            cloneQuote.SBQQ__PriceBook__c = quote.SBQQ__PriceBook__c;
            cloneQuotes.add(cloneQuote);
            //Created the quote line items for the quote 
            oldQuoteWithClone.put(quote.Id, cloneQuote);
            for(SBQQ__QuoteLine__c lineItem :quote.SBQQ__LineItems__r){
                if(oldQuoteWithLineItem.containsKey(quote.Id)){
                    oldQuoteWithLineItem.get(quote.Id).add(lineItem);
                }else{
                    oldQuoteWithLineItem.put(quote.Id, new List<SBQQ__QuoteLine__c>{lineItem});   
                }                           
            } 
        }
        insert cloneQuotes;
        system.debug('quote--'+cloneQuotes); 
        if(oldQuoteWithLineItem.size() >0){
            for(Id oldQuoteId :oldQuoteWithLineItem.keySet()){
                for(SBQQ__QuoteLine__c lineItem :oldQuoteWithLineItem.get(oldQuoteId)){
                    if(oldQuoteWithClone.containsKey(oldQuoteId)){
                        if(pricebookEntryMap.containsKey(lineItem.SBQQ__Product__c)){
                            SBQQ__QuoteLine__c newQuoteItem = new SBQQ__QuoteLine__c();
                            newQuoteItem.SBQQ__Quote__c =  oldQuoteWithClone.get(oldQuoteId).Id;
                            newQuoteItem.SBQQ__ListPrice__c =  lineItem.SBQQ__ListPrice__c;
                            newQuoteItem.SBQQ__Quantity__c =  lineItem.SBQQ__Quantity__c;
                            newQuoteItem.Package_Option__c = lineItem.Package_Option__c;
                            //if(pricebookEntryMap.containsKey(lineItem.SBQQ__Product__c)){
                                newQuoteItem.SBQQ__Product__c =  lineItem.SBQQ__Product__c;
                            //}
                            newQuoteItems.add(newQuoteItem);
                        }
                    }
                }
            }
        }
        system.debug('newQuoteItems--->'+newQuoteItems.size());
        system.debug('qn--'+qn); 
        //system.debug('newQuoteItems--'+newQuoteItems[0].SBQQ__Quote__c);
        insert newQuoteItems;
        
        system.debug('newQuoteItems--'+newQuoteItems);
    }
}