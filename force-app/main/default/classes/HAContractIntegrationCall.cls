public class HAContractIntegrationCall {
  public static final Map<string,Integration_EndPoints__c> integrationCodes = Integration_EndPoints__c.getAll();

  Public Static void calloutportal(message_queue__c mq){
      system.debug(mq.Identification_Text__c);
      String qtid=String.valueOf(mq.Identification_Text__c);
      list<SBQQ__QuoteLine__c> qlist =new list<SBQQ__QuoteLine__c>([select id,SBQQ__Quote__c,name,Number_Field_by_10__c,SBQQ__Product__r.Name,SBQQ__Product__r.SAP_Material_ID__c,
                  SBQQ__Product__r.Category__c,SBQQ__Product__r.Description,SBQQ__Product__r.Family,SBQQ__NetPrice__c from SBQQ__QuoteLine__c where  SBQQ__Quote__c=: qtid and SBQQ__Product__r.HA_Product_Type__c!='Dacor' and SBQQ__Product__r.End_Of_Life__c=False]);
      List<SBQQ__Quote__c> qt=new list<SBQQ__Quote__c>([select id,name,SBQQ__Status__c,Expected_Shipment_Date__c,Last_Shipment_Date__c,SBQQ__Opportunity2__r.Sales_Portal_BO_Number__c,SBQQ__Opportunity2__r.Account.Name,SBQQ__Opportunity2__r.Opportunity_Number__c,SBQQ__Opportunity2__r.Parent_Account__c,SBQQ__Opportunity2__r.Account.SAP_Company_Code__c,SBQQ__ExpirationDate__c,SBQQ__Opportunity2__r.Number_of_Living_Units__c,SBQQ__SalesRep__r.Name,  SBQQ__Opportunity2__r.Project_Name__c,Contract_Order_Number__c,  SBQQ__PrimaryContact__r.Name,SBQQ__PrimaryContact__r.email,Rollout_End_Date__c,Rollout_Start_Date__c,SBQQ__Opportunity2__r.stagename from SBQQ__Quote__c where id=: qtid]);              

      String xmlbody=HAIntegrationbodyContract.integrationbody(qt,qlist);
      System.debug('XML Body ---->'+xmlbody);
      
      String endPointPathProd = integrationCodes.get('Sales Portal Contract - Prod').endPointURL__c;
      String endPointPathQA = integrationCodes.get('Sales Portal Contract - QA').endPointURL__c;
      String ProdOrgId = '00D36000000JfLi';
      String currentOrgId = UserInfo.getOrganizationId();
      currentOrgId = currentOrgId.left(15);
      system.debug('current OrgId 15 digit---------->'+currentOrgId);
      
      Http h = new Http();
      System.HttpRequest request = new System.HttpRequest();
      //request.setEndpoint('http://206.67.236.61/BOTransmission.do'); //QA End point
      //request.setEndpoint('https://spna.sec.samsung.com/BOTransmission.do'); //Prod End Point
      if(currentOrgId==ProdOrgId){
          request.setEndpoint(endPointPathProd);
      }else{
          request.setEndpoint(endPointPathQA);
      }
      request.setTimeout(120000);
      request.setHeader('Content-Type', 'application/xml;charset=UTF-8'); 
      request.setMethod('POST');
      request.setBody(xmlbody); 
      HttpResponse res=new HttpResponse();
      String resp='';
      
      List<message_queue__c> MqUpsert = New List<message_queue__c>();
      try{
          system.debug('XML Request :::'+request);
       res= h.send(request);
       system.debug(res);
       resp=res.getBody();
         system.debug('XML Response :::'+res);
         system.debug('XML Response Body :::'+resp);
      }catch (exception e){
          System.Debug('error  '+e);
      }
      if(resp!=null && String.isNotBlank(resp)){
          //if(resp.containsAny('<')){
          system.debug('response--------->'+res);
          system.debug('response body--------->'+resp.length());
          XmlStreamReader reader = res.getXmlStreamReader();
             Dom.Document doc = res.getBodyDocument();
             Dom.XMLNode info = doc.getRootElement();
             String contnum = info.getChildElement('ConfirmNo', null).getText();
            
             String rtcode = info.getChildElement('ReturnCode', null).getText();
             String rtmsg = info.getChildElement('ReturnMessage', null).getText();
             
             if(rtcode=='0'){
                // Integration sucess 
                 mq.Status__c='Success';
                 mq.MSGSTATUS__c='S';
                 mq.ERRORTEXT__c=rtmsg;
                qt[0].Contract_Order_Number__c=contnum;
                Opportunity  opp=[select id,Sales_Portal_BO_Number__c,Sales_Portal_Last_Successful_Run__c from Opportunity where id=:qt[0].SBQQ__Opportunity2__c];
                Opp.Sales_Portal_BO_Number__c=contnum;
                Opp.Sales_Portal_Last_Successful_Run__c=System.now();
                Opp.Sales_Portal_Error__c=null;
                Opp.Sales_Portal_Status__c='Success';
                mqUpsert.add(mq);
                message_queue__c m = new message_queue__c(Identification_Text__c=opp.Sales_Portal_BO_Number__c, Integration_Flow_Type__c='SO Number',
                                                                retry_counter__c=0,Status__c='not started', Object_Name__c='SO Number');
                mqUpsert.add(m);
                Database.update(opp,false);
                //Database.update(mq,false);
                Database.upsert(mqUpsert,false);
                
             }else{
                 mq.Status__c='failed';
                 mq.MSGSTATUS__c='F';
                 mq.ERRORTEXT__c=rtmsg;
                 Opportunity  opp=[select id,Sales_Portal_BO_Number__c,Sales_Portal_Last_Successful_Run__c from Opportunity where id=:qt[0].SBQQ__Opportunity2__c];
                 Opp.Sales_Portal_Error__c=rtmsg;
                 Opp.Sales_Portal_Status__c='Error';
                 Opp.Sales_Portal_Last_Successful_Run__c=System.now();
                 update opp;
                 
                 Database.update(mq,false);
             }
      }
  }

}