/*
Version Changes:
Thiru: changes on 10/08/2019--Remove Number_Field_by_10__c and generate using Code.
*/
public class HAIntegrationbodyContract {
   
   public static string integrationbody(List<SBQQ__Quote__c> qth,list<SBQQ__QuoteLine__c> qlist){

    XmlStreamWriter w = new XmlStreamWriter();
    w.writeStartDocument('UTF-8','1.0');
    w.writeStartElement(null,'ContractTransmissionRequest',null);
    w.writeStartElement(null,'ContractInformation',null);
     for(SBQQ__Quote__c qt:qth){
         //Date vldfr=qt.Expected_Shipment_Date__c;
         Date vldfr=qt.Rollout_Start_Date__c-60;
            String sMonth = String.valueof(vldfr.month());
            String sDay = String.valueof(vldfr.day());
            if(sMonth.length()==1){
              sMonth = '0' + sMonth;
            }
            if(sDay.length()==1){
              sDay = '0' + sDay;
            }
         String vdate = String.valueof(vldfr.year()) + sMonth + sDay ;
          
         //Date exp= qt.Last_Shipment_Date__c;
         Date exp=qt.Rollout_End_Date__c;
            String sMonth1 = String.valueof(exp.month());
            String sDay1 = String.valueof(exp.day());
            if(sMonth1.length()==1){
              sMonth1 = '0' + sMonth1;
            }
            if(sDay1.length()==1){
              sDay1 = '0' + sDay1;
            }
         String expr = String.valueof(exp.year()) + sMonth1 + sDay1 ;
         
         
         Date dateToday = Date.today();
         String sToday = String.valueof(dateToday.year()) + String.valueof(dateToday.month()) + String.valueof(dateToday.day());
           w.writeStartElement(null,'ContractHeader',null);
           w.writeStartElement(null,'HeaderPart',null);
           
           w.writeStartElement(null,'HeaderStatus',null);
            string sta;
            if(qt.SBQQ__Status__c=='Approved' && qt.SBQQ__Opportunity2__r.Sales_Portal_BO_Number__c==null){
               sta='AP';
            }else{
               sta='CH';
            }
            if(qt.SBQQ__Status__c=='Approved' && qt.SBQQ__Opportunity2__r.stagename=='Completed'){
               sta='CP';    
            }
           w.writeCharacters(String.valueOf(sta));  
           w.writeEndElement();
           
           w.writeStartElement(null,'BONumber',null);
             w.writeCharacters(String.valueOf(qt.SBQQ__Opportunity2__r.Opportunity_Number__c));
           w.writeEndElement();
           
           w.writeStartElement(null,'ProjectName',null);
             string ProjectName = String.valueOf(qt.name) +' '+ String.valueOf(qt.SBQQ__Opportunity2__r.Project_Name__c);
             //w.writeCharacters(String.valueOf(qt.SBQQ__Opportunity2__r.Project_Name__c));
             w.writeCharacters(ProjectName);
           w.writeEndElement();
           
           w.writeStartElement(null,'ApprovedNumber',null);
             w.writeCharacters(String.valueOf(qt.name));
           w.writeEndElement(); 
           
           w.writeStartElement(null,'ValidFromDate',null);
             date d= system.today();
             w.writeCharacters(String.valueOf(vdate));
           w.writeEndElement(); 
           
           w.writeStartElement(null,'ValidToDate',null);
             w.writeCharacters(String.valueOf(expr));
           w.writeEndElement(); 
           
           w.writeStartElement(null,'TotalItemQty',null);
             w.writeCharacters(String.valueOf(qt.SBQQ__Opportunity2__r.Number_of_Living_Units__c));
           w.writeEndElement();
           
           w.writeStartElement(null,'CustomerCode',null);
               if(qt.SBQQ__Opportunity2__r.Parent_Account__c !=null){
                   string pa=String.valueOf(qt.SBQQ__Opportunity2__r.Parent_Account__c);
                   String pacc=pa.replaceFirst( '^0+', '');
                    w.writeCharacters(pacc);
               }else{
                   string sap=String.valueOf(qt.SBQQ__Opportunity2__r.Account.SAP_Company_Code__c);
                   String sapacc=sap.replaceFirst( '^0+', '');
                  w.writeCharacters(sapacc);  
               }
           w.writeEndElement();
           
           w.writeStartElement(null,'IssuedDate',null);
                  w.writeCharacters(String.valueOf(sToday));
           w.writeEndElement();
           
           w.writeStartElement(null,'RequestUserName',null);
                  w.writeCharacters(String.valueOf(qt.SBQQ__SalesRep__r.Name));
           w.writeEndElement();
           
           w.writeStartElement(null,'HeaderRemark',null);
               String hre=String.valueOf(qt.SBQQ__Opportunity2__r.Project_Name__c);
               w.writeCData(hre);
           w.writeEndElement();
           
           w.writeEndElement();
           w.writeEndElement();
        /*w.writeStartElement(null,'ContactPerson',null);
         if(qt.SBQQ__PrimaryContact__r.Name == null){
             string nl='';
             w.writeCharacters(String.valueOf(nl)); 
         }else{
           w.writeCharacters(String.valueOf(qt.SBQQ__PrimaryContact__r.Name));  
         }
              
           w.writeEndElement();
          w.writeStartElement(null,'ContactEmailAddress',null);
         if(qt.SBQQ__PrimaryContact__r.Email == null){
             string nl='';
             w.writeCharacters(String.valueOf(nl)); 
         }else{
           w.writeCharacters(String.valueOf(qt.SBQQ__PrimaryContact__r.Email));  
         }
              
         w.writeEndElement(); */
         
         w.writeStartElement(null,'ContractItem',null); 
         //if(qlist.size()>0){
         
         integer lineNumber = 0;//Added--10/08/2019
         
         for(SBQQ__QuoteLine__c qlis:qlist){
             if(qlis.SBQQ__Quote__c ==qt.id){
                 
                 lineNumber++;//Added--10/08/2019
                 integer quoteLineNumber = lineNumber*10;//Added--10/08/2019
                 
                w.writeStartElement(null,'ItemPart',null); 
                 w.writeStartElement(null,'LineItemNumber',null);
                 //w.writeCharacters(String.valueOf(qlis.Number_Field_by_10__c));//Number_Field_by_10__c //commented--10/08/2019
                 w.writeCharacters(String.valueOf(quoteLineNumber)); //Added--10/08/2019
                w.writeEndElement();   
                 
                w.writeStartElement(null,'SamsungModelCode',null);
                 w.writeCharacters(String.valueOf(qlis.SBQQ__Product__r.SAP_Material_ID__c));
                w.writeEndElement();
                 
                w.writeStartElement(null,'CustomerModelCode',null);
                 w.writeCharacters(String.valueOf(qlis.SBQQ__Product__r.SAP_Material_ID__c));
                w.writeEndElement();
                 
                w.writeStartElement(null,'ItemRequestPrice',null);
                 w.writeCharacters(String.valueOf(qlis.SBQQ__NetPrice__c));
                w.writeEndElement();
                
                w.writeStartElement(null,'ItemCurrency',null);
                 w.writeCharacters(String.valueOf('USD'));
                w.writeEndElement();
               
                w.writeStartElement(null,'ItemRemark',null);
                 string itre=String.valueOf(qlis.SBQQ__Product__r.Description);
                 w.writeCData(itre);
                 //w.writeCharacters(String.valueOf(qlis.SBQQ__Product__r.Description));
                w.writeEndElement();
                
                w.writeStartElement(null,'ItemModelGroup',null);
                 string pg;
                 if(qlis.SBQQ__Product__r.Family=='REFRIGERATION'){
                     pg='REF';
                 }else if(qlis.SBQQ__Product__r.Family=='LAUNDRY'){
                     pg='WM';
                     System.debug('Laundry Family---->'+pg);
                 }
                 else if(qlis.SBQQ__Product__r.Family=='VACUUM'){
                     pg='VC';
                 }else if(qlis.SBQQ__Product__r.Family=='MICROWAVE'){
                     pg='MWO';
                 }else{
                     pg=String.valueOf(qlis.SBQQ__Product__r.Family);
                 }
                 if(pg!=null && pg!=''){
                     w.writeCharacters(String.valueOf(pg));
                 }
                 //w.writeCharacters(String.valueOf(pg));
                w.writeEndElement();
                
                w.writeStartElement(null,'ItemCategory',null);
                  string cat='';
                  if(qlis.SBQQ__Product__r.Category__c !=null){
                      cat=qlis.SBQQ__Product__r.Category__c;
                  }
                  w.writeCharacters(String.valueOf(cat));
                  w.writeEndElement();
                w.writeEndElement(); 
             }
        }
        w.writeEndElement();
     }
     w.writeEndElement();
     w.writeEndElement();
     w.writeEndDocument();
     string xml = w.getXmlString();
     w.close();
     system.debug(xml); 
     return xml;
     }
   
}