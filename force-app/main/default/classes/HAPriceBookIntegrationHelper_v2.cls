public class HAPriceBookIntegrationHelper_v2 {

public static final String FLOW_TYPE= 'Flow-052';
    public static final Integer UNIT_PROCESS_COUNT = 200;
    public List<PriceBookEntry> pbelist = new List<PriceBookEntry>();
    public static List<message_queue__c> mqList = new List<message_queue__c>();
    /**
     * PriceBookName = 'HA Builder EDCP'
     */
     public string priceBookName;
    public void updatePriceBook(List<priceBookEntry> data) {
    //public void updatePriceBook(String priceBookName) {
        //message_queue__c omq=new message_queue__c();
        string layoutId = (Test.isRunningTest() || Integration_EndPoints__c.getInstance('Flow-008').layout_id__c==null )? 'asdf' : Integration_EndPoints__c.getInstance('Flow-008').layout_id__c;
        priceBookName = data[0].pricebook2.name;
        List<DirectPriceBookAssign__c> assigns = [select Direct_Account__r.Id 
                                                    from DirectPriceBookAssign__c 
                                                    where Price_Book__r.name=:priceBookName];
        /* 
        List<DirectPriceBookAssign__c> assigns = [select Direct_Account__r.Id 
                                                    from DirectPriceBookAssign__c 
                                                    where Price_Book__r.name='HA Builder EDCP'];  
        */                                                                                    
                                                    
        String accountId = null;
        if(assigns.size()==0) {
            messageQueue(false,'Cannot find DirectPriceBookAssign object. - '+priceBookName, 'HAPriceBook', '', null, null);
        } else {
            DirectPriceBookAssign__c assign = assigns.get(0);
            accountId = assign.Direct_Account__r.Id;
        }
        
        //create request body
        cihStub.UpdateInvoicePriceRequest request=new cihStub.UpdateInvoicePriceRequest();
        
        cihStub.InvoiceBodyRequest outerRecStub=new cihStub.InvoiceBodyRequest();
        request.body = outerRecStub;
        outerRecStub.salesDocType='YN01';
        outerRecStub.orderReason='2A9';
        outerRecStub.currencyKey='USD';
        outerRecStub.validFromDate=System.now().formatGMT('yyyy-MM-dd');
        
        outerRecStub.distributionChannel=CreditMemoIntegration__c.getInstance('HA Distribution Channel').Distribution_Channel__c;
        List<Account_Sold_To__c> soldToAccounts = [select sales_organization__c, Sales_Division_Id__c, Plant__c, Account__r.SAP_Company_Code__c 
                                                        from Account_Sold_To__c WHERE Account__c = :accountId];
        String plant = null;                                            
        if(soldToAccounts.size()>0) {
            Account_Sold_To__c soldToAcc = soldToAccounts.get(0);
            outerRecStub.salesOrg=soldToAcc.sales_organization__c;
            outerRecStub.division=soldToAcc.Sales_Division_Id__c;
            outerRecStub.soldToParty=soldToAcc.Account__r.SAP_Company_Code__c;
            outerRecStub.shipToParty=soldToAcc.Account__r.SAP_Company_Code__c;
            plant = soldToAcc.Plant__c;
        }
            
        List<cihStub.CreditMemoLineItemsRequest> creditMemoLines=new List<cihStub.CreditMemoLineItemsRequest>();
        /*List<PriceBookEntry> data = [select id, name,  Product2.Id, Product2.SAP_Material_ID__c
                                        from PriceBookEntry 
                                        where PriceBook2.name=:priceBookName];*/
        //Integer UNIT_PROCESS_COUNT = data.size();                               
        List<String> requestMsgs = new List<String>();
        Integer count=1;
        for(PriceBookEntry entry : data) {
            String c=string.valueOf(count).leftPad(6).replace(' ','0');
            cihStub.CreditMemoLineItemsRequest innerRecStub=new cihStub.CreditMemoLineItemsRequest();

            innerRecStub.itemNumberSdDoc=c;
            innerRecStub.materialCode=entry.Product2.SAP_Material_ID__c;
            innerRecStub.quantity=string.valueOf(1);
            innerRecStub.werks = plant;
            
            creditMemoLines.add(innerRecStub);
            //count++;
        //}
            if(math.mod(count,UNIT_PROCESS_COUNT)==0) {
                request.body.lines=creditMemoLines;
                request.inputHeaders = new cihStub.msgHeadersRequest(layoutId);
                String requestMsg = JSON.serialize(request);
                requestMsgs.add(requestMsg);
                creditMemoLines=new List<cihStub.CreditMemoLineItemsRequest>();
                count = 0;
            }
            count++;
        }    

        if(creditMemoLines.size()>0) {
            request.body.lines=creditMemoLines;
            request.inputHeaders = new cihStub.msgHeadersRequest(layoutId);
            String requestMsg = JSON.serialize(request);
            requestMsgs.add(requestMsg);
            //callHAPriceBook(priceBookName, requestMsg);
        }
        callHAPriceBook(priceBookName, requestMsgs[0]);
        for(String requestMsg : requestMsgs) {
            /*for(integer i=0;i<requestMsgs.size();i++){
                callHAPriceBook(priceBookName, requestMsgs[i]);
            }*/
            System.debug('##@- '+requestMsg);
            //callHAPriceBook(priceBookName, requestMsg);
        }
        //if(updatepbes==True){
        if(Label.HA_PBE_Update=='True'){
            Update pbelist;
        }
        Upsert mqList;
    }
    
    //@future (callout=true)
    private void callHAPriceBook(String priceBookName, String requestMsg) {
        String partialEndpoint = Test.isRunningTest() ? '' : Integration_EndPoints__c.getInstance('Flow-008').partial_endpoint__c;
        System.debug('lclc partialEndpoint '+partialEndpoint);
        
        String response = RoiIntegrationHelper.webcallout( requestMsg, partialEndpoint);
        System.debug('### '+response);
        cihStub.UpdateInvoicePriceResponse stubResponse = new cihStub.UpdateInvoicePriceResponse();
        Id parentId = null;
        try{
            stubResponse=(cihStub.UpdateInvoicePriceResponse) json.deserialize(response, cihStub.UpdateInvoicePriceResponse.class);
            System.debug('lclc stubResponse '+stubResponse);
            // E or F
            if((stubResponse.inputHeaders.MSGSTATUS).contains('S')){
                parentId = messageQueue(true, priceBookName, 'HAPriceBook', '', null, stubResponse.inputHeaders);  
            } else {
                parentId = messageQueue(false,'Error response.('+priceBookName+') -'+stubResponse.inputHeaders.ERRORTEXT, 'HAPriceBook', '', null, stubResponse.inputHeaders);
            }
        }catch(exception ex){
            String errmsg = ex.getmessage() + ' - '+ex.getStackTraceString();
            messageQueue(false,'Fail to request a pricebook request.('+priceBookName+') - '+errmsg, 'HAPriceBook', '', null, stubResponse.inputHeaders);
            return;
        }
        
        
        if(stubResponse.body!=null){
            Map<String,Double> priceMap = new Map<String,Double>();
            for(cihStub.CreditMemoLineItemsResponse res: stubResponse.body.lines){
                double price = 0.0;
                try {
                    price = decimal.valueOf(res.invoicePrice);
                } catch(Exception e) {
                    messageQueue(false,'Invalid price from GERP.('+priceBookName+') - '+res.materialCode+'='+res.invoicePrice, 'HAPriceBookEntry', null, parentId, stubResponse.inputHeaders);
                    continue;
                }
                priceMap.put(res.materialCode, price);
            }
            
            List<String> materialCodeList = new List<String>(priceMap.keySet());
            List<PriceBookEntry> entries = [select id, UnitPrice, PriceBook2.name, Product2.name, Product2.SAP_Material_ID__c
                                                from PriceBookEntry where PriceBook2.name=:priceBookName and Product2.name in :materialCodeList];
            for(PriceBookEntry entry : entries) {
                Double price = priceMap.get(entry.Product2.name);
                if(price>0.0) {
                    entry.unitPrice = price;
                    pbelist.add(entry);
                    //messageQueue(true,priceBookName, 'HAPriceBookEntry', entry.id, parentId, stubResponse.inputHeaders);
                    messageQueue(true,'Price from GERP('+priceBookName+') - '+entry.Product2.name+'='+price, 'HAPriceBookEntry', entry.id, parentId, stubResponse.inputHeaders);
                } else {
                    messageQueue(false,'Zero price from GERP('+priceBookName+') - '+entry.Product2.name+'='+price, 'HAPriceBookEntry', entry.id, parentId, stubResponse.inputHeaders);
                }
            }
            //update pbelist;
        }else{
            messageQueue(false,'Empy Response body.'+response, 'HAPriceBook', '', null, stubResponse.inputHeaders);
            return;
        }
    }
    /*
    private static Id messageQueue(boolean success, String errorText, String objectName, String objectId, Id parentId, cihStub.msgHeadersResponse headers) {
        message_queue__c mq = new message_queue__c(Identification_Text__c=objectId, 
                                                          Integration_Flow_Type__c = FLOW_TYPE,
                                                          retry_counter__c=0,
                                                          Status__c=(success?'success':'failed'),
                                                          MSGSTATUS__c=(success?'S':'E'), 
                                                          Object_Name__c=objectName);  
        if(headers!=null) {
            mq.MSGUID__c    = headers.MSGGUID;
            mq.IFID__c      = headers.IFID;
            mq.IFDate__c    = headers.IFDate;
        }
        mq.errortext__c = errorText;
        if(parentId!=null) {
            mq.ParentMQ__c = parentId;
        }
        insert mq; 
        return mq.Id; 
    }
    */
    private static Id messageQueue(boolean success, String errorText, String objectName, String objectId, Id parentId, cihStub.msgHeadersResponse headers) {
        message_queue__c mq = new message_queue__c(Identification_Text__c=objectId, 
                                                          Integration_Flow_Type__c = FLOW_TYPE,
                                                          retry_counter__c=0,
                                                          Status__c=(success?'success':'failed'),
                                                          MSGSTATUS__c=(success?'S':'E'), 
                                                          Object_Name__c=objectName);  
        if(headers!=null) {
            mq.MSGUID__c    = headers.MSGGUID;
            mq.IFID__c      = headers.IFID;
            mq.IFDate__c    = headers.IFDate;
        }
        mq.errortext__c = errorText;
        if(parentId!=null) {
            mq.ParentMQ__c = parentId;
        }
        mqList.add(mq);
        return null;
    }
}