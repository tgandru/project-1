///note to up coverage more figure out how to insert an isr user. 
@isTest
private class HAPriceBookIntegrationHelper_v2Test{

    static Id endUserRT = TestDataUtility.retrieveRecordTypeId('End_User', 'Account'); 
    static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
    
    @testSetup static void setup() {

        //Test data for fiscalCalendar__c
        Test.loadData(fiscalCalendar__c.sObjectType, 'FiscalCalendarTestData');

        //Channelinsight configuration
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();

        List<Integration_EndPoints__c> ieps = new List<Integration_EndPoints__c>();
        
        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='Flow-008';
        iep2.endPointURL__c = 'https://www.google.com';
        iep2.layout_id__c = 'LAY024197';
        iep2.partial_endpoint__c = '/SFDC_IF_012';
        ieps.add(iep2);
        
        Integration_EndPoints__c iep3 = new Integration_EndPoints__c();
        iep3.name ='Flow-009_1';
        iep3.endPointURL__c = 'https://www.google.com';
        iep3.layout_id__c = 'LAY024255';
        iep3.partial_endpoint__c = '/SFDC_IF_US_009_1';
        iep3.last_successful_run__c = Datetime.now();
        ieps.add(iep3);
        insert ieps;

        List<Roll_Out_Schedule_Constants__c> roscs=new List<Roll_Out_Schedule_Constants__c>();
        Roll_Out_Schedule_Constants__c rosc1=new Roll_Out_Schedule_Constants__c(Name='Roll Out Schedule Parameters',Average_Week_Count__c=4.33,DML_Row_Limit__c=3200);
        roscs.add(rosc1);
        insert roscs;

        Zipcode_Lookup__c zip=TestDataUtility.createZipcode();
        insert zip;

        //Creating test Account
        list<account> accts=new list<account>();
        Account acct=TestDataUtility.createAccount(TestDataUtility.generateRandomString(5));
        acct.RecordTypeId = endUserRT;
        accts.add(acct);
        account a2=TestDataUtility.createAccount('dpba stuff');
        a2.RecordTypeId = endUserRT;
        a2.sap_company_code__C='5554444';
        accts.add(a2);
        insert accts;
        
        //Creating the Parent Account for Partners
        Account paacct=TestDataUtility.createAccount(TestDataUtility.generateRandomString(7));
        acct.RecordTypeId = directRT;
        insert paacct;
        
        //Creating custom Pricebook
        PriceBook2 pb = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(4)); 
        insert pb;
        
        PriceBook2 HApb = new Pricebook2(name='HA Builder EDCP');
        insert HApb;

        //create direct pricebook assign
        DirectPriceBookAssign__c dpba=TestDataUtility.createDPBA(a2,HApb);
        insert dpba;
        
        Account_Sold_To__c accSoldTo = new Account_Sold_To__c(name='test',Account__c=a2.id,Distribution_Channel_ID__c='12',External_Sold_To_ID__c='23456hgsn4ewegbsfb',Sales_Division_ID__c='34',Sales_Organization__c='tfbg');
        insert accSoldTo;
        
        //Creating Products
        List<Product2> prods=new List<Product2>();
        Product2 standaloneProd=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Standalone Prod1', 'Printer[C2]', 'FAX/MFP', 'H/W');
        prods.add(standaloneProd);
        Product2 parentProd1=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Parent Prod1', 'Printer[C2]', 'FAX/MFP', 'H/W');
        prods.add(parentProd1);
        Product2 parentProd2=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Parent Prod2', 'MOBILE PHONE [G1]', 'KNOX', 'H/W');
        prods.add(parentProd2);
        insert prods;

        Id pricebookId = Test.getStandardPricebookId();

        //CreatePBEs
        //create the pricebook entries for the custom pricebook
        List<PriceBookEntry> custompbes=new List<PriceBookEntry>();
        PriceBookEntry standaloneProdPBE=TestDataUtility.createPriceBookEntry(standaloneProd.Id, pb.Id);
        custompbes.add(standaloneProdPBE);
        PriceBookEntry parentProd1PBE=TestDataUtility.createPriceBookEntry(parentProd1.Id, pb.Id);
        custompbes.add(parentProd1PBE);
        PriceBookEntry parentProd2PBE=TestDataUtility.createPriceBookEntry(parentProd2.Id, HApb.Id);
        custompbes.add(parentProd2PBE);
        insert custompbes;

        insert new CreditMemoIntegration__c(Name='HA Distribution Channel', Distribution_Channel__c = 'Value');
    }
    
     @isTest static void test_method_HAPricebookIntegrationHelper() {
        List<Pricebookentry> pbes = [select id, name,Pricebook2.name, Product2.Id, Product2.SAP_Material_ID__c from Pricebookentry];
        integer UNIT_PROCESS_COUNT = 3;
        String pricebookName = 'pb';
        message_queue__c testQueue = new message_queue__c(retry_counter__c=0);
        Test.startTest();
            HAPriceBookIntegrationHelper_v2 HA = new HAPriceBookIntegrationHelper_v2();
            Test.setMock(HttpCalloutMock.class, new MockResponseGenerator('"S"'));
            HA.updatePriceBook(pbes);
            BatchforHAPriceBookIntegrationHelper batch = new BatchforHAPriceBookIntegrationHelper();
            database.executeBatch(batch);
        Test.stopTest();
    }
    
    class MockResponseGenerator implements HttpCalloutMock {
        String responseStatus;
        MockResponseGenerator(String responseStatus) {
            this.responseStatus = responseStatus;
        }
    // Implement this interface method
        public HTTPResponse respond(HTTPRequest req) {
            // Optionally, only send a mock response for a specific endpoint
            // and method.
            System.assertEquals('callout:X012_013_ROI', req.getEndpoint());
            System.assertEquals('POST', req.getMethod());
            
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            if(responseStatus=='"S"'){
                res.setBody('{' +
                            '"inputHeaders": {'+
                            '"MSGGUID": "ada747b2-dda9-1931-acb2-14a2872d4358",' +
                            '"IFID": "TBD",' +
                            '"IFDate": "20160628194634",' +
                            '"MSGSTATUS":' + responseStatus + ',' +
                            '"ERRORTEXT": "Success",' +
                            '"ERRORCODE": "S"' +
                                            '},' +
                            '"body": {'+
                                '"lines": ['+

                                    '{"unitCost":"52.92",'+
                                    '"materialCode":"CLT-Y04s",'+
                                    '"quantity":"1.0",'+
                                    '"invoicePrice":"65.55",'+
                                    '"currencyKey":"USD",'+
                                    '"itemNumberSdDoc":"000001"},'+

                                    '{"unitCost":null,'+
                                    '"materialCode":"CLT-L04s",'+
                                    '"quantity":"1.0",'+
                                    '"invoicePrice":null,'+
                                    '"currencyKey":"USD",'+
                                    '"itemNumberSdDoc":"000002"}'+
                                ']'+
                            
                            '}'+
                         '}');
            }else{
                res.setBody('{' +
                            '"inputHeaders": {'+
                            '"MSGGUID": "ada747b2-dda9-1931-acb2-14a2872d4358",' +
                            '"IFID": "TBD",' +
                            '"IFDate": "20160628194634",' +
                            '"MSGSTATUS":' + responseStatus + ',' +
                            '"ERRORTEXT": "Error",' +
                            '"ERRORCODE": "E"' +
                                            '},' +
                            '"body": {'+
                                '"lines": ['+

                                    '{"unitCost":"52.92",'+
                                    '"materialCode":"CLT-Y04s",'+
                                    '"quantity":"1.0",'+
                                    '"invoicePrice":"65.55",'+
                                    '"currencyKey":"USD",'+
                                    '"itemNumberSdDoc":"000001"},'+

                                    '{"unitCost":null,'+
                                    '"materialCode":"CLT-L04s",'+
                                    '"quantity":"1.0",'+
                                    '"invoicePrice":null,'+
                                    '"currencyKey":"USD",'+
                                    '"itemNumberSdDoc":"000002"}'+
                                ']'+
                            
                            '}'+
                         '}');
            }  
            res.setStatusCode(200);
            return res;
        }
    }
    
    class MockErrorResponseGenerator implements HttpCalloutMock {
        String responseStatus;
        MockErrorResponseGenerator(String responseStatus) {
            this.responseStatus = responseStatus;
        }
    // Implement this interface method
        public HTTPResponse respond(HTTPRequest req) {
            // Optionally, only send a mock response for a specific endpoint
            // and method.
            System.assertEquals('callout:X012_013_ROI', req.getEndpoint());
            System.assertEquals('POST', req.getMethod());
            
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{' +
                            '"inputHeaders": {'+
                            '"MSGGUID": "ada747b2-dda9-1931-acb2-14a2872d4358",' +
                            '"IFID": "TBD",' +
                            '"IFDate": "20160628194634",' +
                            '"MSGSTATUS":' + responseStatus + ',' +
                            '"ERRORTEXT": null,' +
                            '"ERRORCODE": null' +
                                            '},' +
                            '"body": {'+
                                '"lines": ['+
                                    // Forcing test to fail to increase 
                                    // test coverage for other helper classes
                                    '{"uniCost":"52.92",'+
                                    '"materialCode":"CLT-Y04s",'+
                                    '"quantity":"1.0",'+
                                    '"invoicePrice":"65.55",'+
                                    '"currencyKey":"USD",'+
                                    '"itemNumberSdDoc":"000001"}'+
                                ']'+
                            
                            '}'+
                         '}');
            res.setStatusCode(200);
            return res;
        }
    }
}