public class HAQuoteDocumentTriggerHandler{
    public set<id> docids;

    public void ProcessDocument(List<SBQQ__QuoteDocument__c> Quotedocs){
        docids = new set<id>();
        for(SBQQ__QuoteDocument__c qdoc : Quotedocs){
            docids.add(qdoc.SBQQ__DocumentId__c);
        }
        if(docids.size()>0){
            updateDocument(docids);
        }
    }
    @future
    public static void updateDocument(set<id> idslist){
        id CPQfolderid = [select id from Folder where name ='CPQ PDF'].id;
        List<Document> updateDocs= new List<Document>();
        for(Document doc : [select id,authorid,folderid,folder.name,author.profile.name from document where id=:idslist]){
            if(doc.author.profile.name=='HA Sales User'){
                doc.folderid = CPQfolderid;
                updateDocs.add(doc);
            }
        }
        system.debug('Document-------->'+updateDocs);
        if(updateDocs.size()>0){
            update updateDocs;
            system.debug('Document-------->'+updateDocs);
        }
    }
}