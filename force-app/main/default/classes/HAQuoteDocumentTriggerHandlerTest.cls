@istest(seealldata=true)
public class HAQuoteDocumentTriggerHandlerTest{
    public static testmethod void test1(){
        user u =[select id from user where name='Adam Krimkowitz' and isactive=true limit 1];
        system.runAs(u){
            List<SBQQ__QuoteDocument__c> testQuoteDoc = [select id,SBQQ__DocumentId__c,SBQQ__Quote__c,SBQQ__Version__c from SBQQ__QuoteDocument__c where Createdbyid=:u.id limit 1];
            testQuoteDoc[0].SBQQ__Version__c = 2;
            update testQuoteDoc;
            SBQQ__QuoteDocument__c newdoc = new SBQQ__QuoteDocument__c(Name='newdoc',SBQQ__Quote__c=testQuoteDoc[0].SBQQ__Quote__c,SBQQ__Version__c=2);
            insert newdoc;
            HAQuoteDocumentTriggerHandler HAqd = new HAQuoteDocumentTriggerHandler();
            HAqd.ProcessDocument(testQuoteDoc);
        }
    }
}