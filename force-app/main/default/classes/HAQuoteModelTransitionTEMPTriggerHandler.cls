/*
Created By -  Vijay Kamani 04/04/2019
Division   - HA CPQ
Purpoase   - To Attache New product ID and Quote IDs


*/

public class HAQuoteModelTransitionTEMPTriggerHandler {
    
    Public Static void GetNewSKUInformation(List<HA_Quote_Model_Transition_TEMP__c> newlist){
       Set<string> qtnumbers=new Set<string>();
       Set<string> newskucode=new Set<string>();
       Map<String,PricebookEntry> newskuMap=new Map<String,PricebookEntry>();
       for(HA_Quote_Model_Transition_TEMP__c rec: newlist){
             qtnumbers.add(rec.Quote_Number__c);
             newskucode.add(rec.New_SKU_Code__c);
       }
       
       Map<String,String> qtMap=new Map<String,String>();
       if(!qtnumbers.isEmpty()){
           for(SBQQ__Quote__c qt:[Select id,Name from SBQQ__Quote__c where name in:qtnumbers]){
               qtMap.put(qt.Name,qt.ID);
           }
       }
       
        Map<String,String> skuMap=new Map<String,String>();
       if(!newskucode.isEmpty()){
           for(Product2 p:[Select id,Name,SAP_Material_ID__c from Product2 where SAP_Material_ID__c in:newskucode]){
               skuMap.put(p.SAP_Material_ID__c,p.ID);
           }
       }
       
        // Attach Quote ID and Product ID to Temp Table records
         for(HA_Quote_Model_Transition_TEMP__c rec: newlist){
             if(qtMap.containsKey(rec.Quote_Number__c)){
                 rec.Quote_ID__c=qtMap.get(rec.Quote_Number__c);
             }
             
             if(skuMap.containsKey(rec.New_SKU_Code__c)){
                 rec.New_SKU_Product_ID__c=skuMap.get(rec.New_SKU_Code__c);
             }
         }
      
    }

}