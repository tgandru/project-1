global class HAQuotesMassCloning implements Database.Batchable<sObject>,Database.Stateful {
     global map<string, string> oldnewskus;// To Store old and new sku ids
     global map<string, string> oldnewQid;
     global map<string, string> oldskuid;
     global map<string, string> newskuid;
     global map<string, string> oldstatus;
     global string qutinfo;
     global string qutinfoError;
      global HAQuotesMassCloning(){
          oldnewskus  = New map<string,string>();
          List<HA_QLI_Replaced_Products__c> skus = HA_QLI_Replaced_Products__c.getall().values();
          for(HA_QLI_Replaced_Products__c s:skus){
              oldnewskus.put(s.Old_Product_Code__c,s.New_Product_Code__c);
          }
          //this.oldnewskus=oldnewskus;
          oldnewQid  = New map<string,string>();
          oldstatus  = New map<string,string>();
          oldskuid  = New map<string,string>();
          newskuid  = New map<string,string>();
          string header='Old Quote ID , New Quote ID \n';
          string headerError='Old Quote ID , Error Message \n';
          qutinfo=header;
            qutinfoError=headerError;
       }
      
       global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([select id,SBQQ__Product__r.Name,SBQQ__Quote__c,SBQQ__Quote__r.Name,SBQQ__Quote__r.CreatedDate,SBQQ__Quote__r.Createdbyid,SBQQ__Quote__r.SBQQ__Status__c from SBQQ__QuoteLine__c where SBQQ__Product__r.Name in :oldnewskus.keyset() and SBQQ__Quote__r.SBQQ__Status__c !='Rejected' And SBQQ__Quote__r.SBQQ__Opportunity2__r.Stagename !='Drop' and SBQQ__Quote__r.SBQQ__Primary__c=true]); // SBQQ__Product__r.Name in :oldnewskus.keyset() and SBQQ__Quote__r.SBQQ__Status__c !='' And SBQQ__Quote__r.SBQQ__Opportunity2__r.Stagename !='Drop' and SBQQ__Quote__r.SBQQ__Primary__c=true
       }  
      global void execute(Database.BatchableContext BC, List<SBQQ__QuoteLine__c> scope){
          if(!oldnewskus.isEmpty()){
             For(Product2 pd:[select id,name,SAP_Material_ID__c from Product2 where SAP_Material_ID__c in :oldnewskus.keyset()]){
                 oldskuid.put(pd.ID,pd.SAP_Material_ID__c);
             }
             For(Product2 pd:[select id,name,SAP_Material_ID__c from Product2 where SAP_Material_ID__c in :oldnewskus.values()]){
                 newskuid.put(pd.SAP_Material_ID__c,pd.ID);
             }
              
          }
          
          set<id> qtid=new set<id>();
          for(SBQQ__QuoteLine__c ql:scope){
               if(oldnewQid.keyset().contains(ql.SBQQ__Quote__c) == true && oldnewQid.get(ql.SBQQ__Quote__c) != null){
                   
               }else{
                    SBQQ__Quote__c  qt=new SBQQ__Quote__c(id=ql.SBQQ__Quote__c);
                    sObject originalSObject = (sObject) qt;

                    List<sObject> originalSObjects = new List<sObject>{originalSObject};
                          
                    List<sObject> clonedSObjects = SObjectAllFieldCloner.cloneObjects(
                                                          originalSobjects,
                                                          originalSobject.getsObjectType());
                    
                    SBQQ__Quote__c  qtcl= (SBQQ__Quote__c)clonedSObjects.get(0);
                    qtcl.Contract_Order_Number__c=null;
                    qtcl.SBQQ__OriginalQuote__c=ql.SBQQ__Quote__c;
                    if(UserInfo.getProfileId()=='00e36000000OfxiAAC'){
                    qtcl.CreatedDate=ql.SBQQ__Quote__r.CreatedDate;
                    qtcl.Createdbyid=ql.SBQQ__Quote__r.Createdbyid;
                        
                    }
                    
                   Database.SaveResult SR = Database.insert(qtcl, False);
                    if(SR.isSuccess()){
                        oldnewQid.put(ql.SBQQ__Quote__c,qtcl.id); 
                        //oldstatus.put(ql.SBQQ__Quote__c,qtcl.SBQQ__Quote__r.SBQQ__Status__c); 
                        qutinfo=qutinfo+ql.SBQQ__Quote__c+','+qtcl.id+'\n';
                        qtid.add(ql.SBQQ__Quote__c);
                    }else{
                        qutinfoError=qutinfoError+ql.SBQQ__Quote__c+','+SR.getErrors()+'\n';
                    }
                    
                    //qtid.add(ql.SBQQ__Quote__c);
               }
           }
           List<Quote_Legacy_Approval_His__c> legacyhistory =new list<Quote_Legacy_Approval_His__c>();
           
           for(ProcessInstance p:[SELECT Id, Status, CreatedDate, CompletedDate, TargetObjectId, 
                                                (SELECT Id, StepStatus, Actorid, ElapsedTimeInDays, ElapsedTimeInMinutes, 
                                                CreatedDate, ProcessNodeId, ProcessNode.Name, Comments 
                                                FROM StepsAndWorkitems order by CreatedDate desc) from ProcessInstance 
                                                Where TargetObjectId in :qtid order by CreatedDate desc ]){
                                                    
                        for(ProcessInstanceHistory s: p.StepsAndWorkitems){
                            if(s.StepStatus !='NoResponse'){
                                Quote_Legacy_Approval_His__c apl=new Quote_Legacy_Approval_His__c();
                                apl.Quote__c=oldnewQid.get(p.TargetObjectId);
                                apl.Status__c=s.StepStatus;
                                if(s.StepStatus=='Started'){
                                 apl.Action__c='Approval Request Submitted';   
                                 apl.Status__c='Submitted';
                                }else{
                                apl.Action__c=s.ProcessNode.Name;}
                                apl.Completion_Date__c=s.CreatedDate;
                                apl.Comments__c=s.Comments;
                                apl.User_Name__c=s.Actorid;
                                legacyhistory.add(apl);
                            }
                            
                            
                            
                        }                            
               
               
           }
           
           if(legacyhistory.size()>0){
             database.insert(legacyhistory,false);  
           }
           List<SBQQ__QuoteLine__c> qlis = new List<SBQQ__QuoteLine__c>([select id,name,CreatedDate,Createdbyid,SBQQ__Product__c from SBQQ__QuoteLine__c where SBQQ__Quote__c in :qtid]);
           Map<string,SBQQ__QuoteLine__c> qlimap=new Map<string,SBQQ__QuoteLine__c>();
           if(qlis.size()>0){
               
              for(SBQQ__QuoteLine__c ql:qlis){
                qlimap.put(ql.SBQQ__Product__c,ql);
              }
               
              List<sObject> clonedSObjects = SObjectAllFieldCloner.cloneObjects(
                                      qlis,
                                      qlis.getsObjectType());

               List<SBQQ__QuoteLine__c> qlicl= (List<SBQQ__QuoteLine__c>)clonedSObjects;
               for(SBQQ__QuoteLine__c ql:qlicl){
                   ql.SBQQ__Quote__c =oldnewQid.get(ql.SBQQ__Quote__c);
                   string p=ql.SBQQ__Product__c;
                  
                   
                   
                   system.debug('Product ID '+p);
                   try{
                       if(UserInfo.getProfileId()=='00e36000000OfxiAAC'){
                        ql.CreatedDate=qlimap.get(p).CreatedDate;
                        ql.Createdbyid=qlimap.get(p).Createdbyid;
                       }
                       if(p !=null){
                         String  pid=oldskuid.get(p);
                          system.debug('Product SKU ID '+pid);
                             If(oldnewskus.keyset().contains(pid) == true && oldnewskus.get(pid) != null){
                                 string npid=newskuid.get(oldnewskus.get(pid));
                                 system.debug('New Product ID '+npid);
                               ql.SBQQ__Product__c=newskuid.get(oldnewskus.get(pid));
                           }
                           
                       }
                   }catch(exception ex){
                       
                   }
               }
               
               insert qlicl;
           } 
           
           
           
          
      }
       
       global void finish(Database.BatchableContext BC){
          /* List<SBQQ__QuoteLine__c> qlis = new List<SBQQ__QuoteLine__c>([select id,name from SBQQ__QuoteLine__c where SBQQ__Quote__c in :oldnewQid.keyset()]);
           if(qlis.size()>0){
               List<sObject> clonedSObjects = SObjectAllFieldCloner.cloneObjects(
                                      qlis,
                                      qlis.getsObjectType());

               List<SBQQ__QuoteLine__c> qlicl= (List<SBQQ__QuoteLine__c>)clonedSObjects;
               for(SBQQ__QuoteLine__c ql:qlicl){
                   ql.SBQQ__Quote__c =oldnewQid.get(ql.SBQQ__Quote__c);
                   string p=ql.SBQQ__Product__c;
                   system.debug('Product ID '+p);
                   try{
                       if(p !=null){
                         String  pid=oldskuid.get(p);
                          system.debug('Product SKU ID '+pid);
                             If(oldnewskus.keyset().contains(pid) == true && oldnewskus.get(pid) != null){
                                 string npid=newskuid.get(oldnewskus.get(pid));
                                 system.debug('New Product ID '+npid);
                               ql.SBQQ__Product__c=newskuid.get(oldnewskus.get(pid));
                           }
                           
                       }
                   }catch(exception ex){
                       
                   }
               }
               
               insert qlicl;
           } 
           List<SBQQ__Quote__c>  qtstatus=new List<SBQQ__Quote__c>();
           for(SBQQ__Quote__c qt:[select id,SBQQ__Status__c,SBQQ__Primary__c from SBQQ__Quote__c where id in:oldnewQid.keyset()]){
               
               SBQQ__Quote__c q=new SBQQ__Quote__c(id=oldnewQid.get(qt.id));
               //q.SBQQ__Status__c=qt.SBQQ__Status__c;
               q.SBQQ__Primary__c=true;
               qtstatus.add(q);
           }
           if(qtstatus.size()>0){
               database.update(qtstatus,false);
               
           }*/
           
           Blob Rec = Blob.valueof(qutinfo);
            Blob Rec2 = Blob.valueof(qutinfoError);
            Messaging.EmailFileAttachment efaUser = new Messaging.EmailFileAttachment();
            efaUser.setFileName('Record_Sucess_List.csv');
            efaUser.setBody(Rec);
            Messaging.EmailFileAttachment efaUser2 = new Messaging.EmailFileAttachment();
            efaUser2.setFileName('Record_Error_List.csv');
            efaUser2.setBody(Rec2);
             Messaging.SingleEmailMessage email9 = new Messaging.SingleEmailMessage();
            list<string> emailaddresses9 = new list<string>{'vijayskamani@gmail.com','v.kamani@partner.samsung.com'};
            email9.setSubject( 'Result Files' );
            email9.setToAddresses( emailaddresses9 );
            string body9 ='PFA-Files for Original and New Quote IDs'+'\n\n'+'Thanks';
            email9.setPlainTextBody( 'Hi ' );
            email9.setFileAttachments(new Messaging.EmailFileAttachment[] {efaUser,efaUser2});
            Messaging.SendEmailResult [] r9 = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email9});
           
           
           
       }
    
}