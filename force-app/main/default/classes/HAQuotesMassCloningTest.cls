@isTest
private class HAQuotesMassCloningTest {

	private static testMethod void testmethod1() {
        
         Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Id HARecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HA Builder').getRecordTypeId();
        Id OppHARecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('HA Builder').getRecordTypeId();
        Id QuoteHARecordTypeId = Schema.SObjectType.SBQQ__Quote__c.getRecordTypeInfosByName().get('HA Builder').getRecordTypeId();

        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            //RecordTypeId = rtMap.get('Account' + 'HA_Builder'),
            RecordTypeId = HARecordTypeId,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        
        );
        insert testAccount1;

        Opportunity testOpp1 = new Opportunity (
            Name = 'TestOpp1',
            //RecordTypeId = rtMap.get('Opportunity' + 'HA_Builder'),
            RecordTypeId = OppHARecordTypeId,
            Project_Type__c = 'SFNC',
            Project_Name__c = 'TestOpp1',
            AccountId = testAccount1.Id,
            StageName = 'Identified',
            CloseDate = Date.today(),
            Roll_Out_Start__c = Date.today(),
            Number_of_Living_Units__c = 10000,
            Rollout_Duration__c = 5

        );
        insert testOpp1;
        PriceBook2 pricebook = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(4)); 
    	
        insert pricebook;

        SBQQ__Quote__c sbq1 = new SBQQ__Quote__c (
            SBQQ__PriceBook__c=pricebook.id,
            SBQQ__Primary__c = TRUE,
            SBQQ__Opportunity2__c = testOpp1.Id,
            SBQQ__Account__c = testAccount1.Id,
            SBQQ__Type__c = 'Quote',
            SBQQ__Status__c = 'Draft',
            Expected_Shipment_Date__c = Date.today(),
            Last_Shipment_Date__c = Date.today(),
            //RecordTypeId = rtMap.get('SBQQ__Quote__c'+'HA_Builder')
            RecordTypeId = QuoteHARecordTypeId
        );
        insert sbq1;
         List<Product2> pdlist= new list<Product2>();
            Product2 pd=new  Product2(Name='HA-Clone-Batch',SAP_Material_ID__c='HA-Clone-Batch',isActive=true,Family='LAUNDRY',SAP_SW_Product_Code__c='AER0A');          
            pdlist.add(pd);
            Product2 pd2=new  Product2(Name='HA-Clone-Batch12',SAP_Material_ID__c='HA-Clone-Batch12',isActive=true,Family='LAUNDRY',SAP_SW_Product_Code__c='AER0A');          
            pdlist.add(pd2); 
            insert pdlist;
             List<PriceBookEntry> pbelist= new list<PriceBookEntry>();
          PriceBookEntry pbe=new PriceBookEntry(isActive=true,UnitPrice=5000, Pricebook2Id=pricebook.id,
                                        Product2Id=pdlist[0].id);  
          PriceBookEntry pbe2=new PriceBookEntry(isActive=true,UnitPrice=5000, Pricebook2Id=pricebook.id,
                                        Product2Id=pdlist[1].id); 
                                        
                pbelist.add(pbe);
                pbelist.add(pbe2);
            insert pbelist;
        SBQQ__QuoteLine__c sbqList = new SBQQ__QuoteLine__c(SBQQ__Product__c=pd.id,Package_Option__c='BASE',SBQQ__Quantity__c=50,SBQQ__Quote__c=sbq1.id,SBQQ__ListPrice__c = 5000,SBQQ__NetPrice__c = 5000);
          insert sbqList;
       
        HA_QLI_Replaced_Products__c chng=new HA_QLI_Replaced_Products__c(Name='100',Old_Product_Code__c='HA-Clone-Batch',New_Product_Code__c='HA-Clone-Batch12');
        insert chng;
      Test.startTest();  
       database.executeBatch(new HAQuotesMassCloning(),1); 
    Test.stopTest();    
	}

}