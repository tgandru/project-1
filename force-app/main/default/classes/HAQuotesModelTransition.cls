/*
Created By - Vijay Kamani 4/4/2019
Division   - HA CPQ
Purpose    - To Add New Line item with Different SKU to Quote by cloning the one Line Item.

Note: While running the Batch Please set batch size as 1 because of SBQQ Limitations

*/

global class HAQuotesModelTransition implements Database.Batchable<sObject>,Database.Stateful {
    global Database.QueryLocator start(Database.BatchableContext BC){
        // Query Records from Temp table loaded by user/admin
        return Database.getQueryLocator([select id,Added_to_Quote__c,New_SKU_Code__c,Old_SKU_Code__c,Option__c,Quote_Line_Item_ID__c,Quote_Number__c from HA_Quote_Model_Transition_TEMP__c	 where Added_to_Quote__c=false]); 
    }
       
     global void execute(Database.BatchableContext BC, List<HA_Quote_Model_Transition_TEMP__c> scope){
       // Collect all Line Items ID
        Map<String,HA_Quote_Model_Transition_TEMP__c> tempMap=new Map<String,HA_Quote_Model_Transition_TEMP__c>();
      // Collect New Product with PriceBookEntry
       Map<String,PriceBookEntry> productMap=new  Map<String,PriceBookEntry>();
       // Collect PriceBook with PriceBookEntry
       Map<String,List<PriceBookEntry>> pricebookMap=new  Map<String,List<PriceBookEntry>>();
      // collect All New SKU ID's
      Set<String> newskus=new Set<String>();
         for(HA_Quote_Model_Transition_TEMP__c rec:scope){
              tempMap.Put(rec.Quote_Line_Item_ID__c,rec);
              newskus.add(rec.New_SKU_Code__c);
         }
          
          // Query and Collect all PricebookEntry
          for(PricebookEntry p:[Select id,Product2id,Pricebook2Id,Product2.SAP_Material_ID__C from PricebookEntry where Product2.SAP_Material_ID__C in :newskus]){
               if(pricebookMap.containsKey(p.Product2.SAP_Material_ID__C)){
                   List<PricebookEntry> pbes=pricebookMap.get(p.Product2.SAP_Material_ID__C);
                   pbes.add(p);
                   pricebookMap.put(p.Product2.SAP_Material_ID__C,pbes);
               }else{
                    pricebookMap.put(p.Product2.SAP_Material_ID__C,new list<PricebookEntry>{p});
               }
          }
             
         // Now Query the Quote Line Records
         List<SBQQ__QuoteLine__c> insertlist=new List<SBQQ__QuoteLine__c>();
         for(SBQQ__QuoteLine__c qli:[Select id,name,SBQQ__Quote__r.SBQQ__PriceBook__c from SBQQ__QuoteLine__c where id in:tempMap.keyset()]){
                     String lineid=qli.id;
                     lineid=lineid.left(15);// Convert 18 digit ID to 15 because user will load only 15 digit Number.
                     HA_Quote_Model_Transition_TEMP__c rec=tempMap.get(lineid);
                   
                    SBQQ__QuoteLine__c  qt=new SBQQ__QuoteLine__c(id=qli.id);
                    sObject originalSObject = (sObject) qt; 
                    List<sObject> originalSObjects = new List<sObject>{originalSObject};
                          
                    List<sObject> clonedSObjects = SObjectAllFieldCloner.cloneObjects(
                                                          originalSobjects,
                                                          originalSobject.getsObjectType());
                    
                    SBQQ__QuoteLine__c  qlicln= (SBQQ__QuoteLine__c)clonedSObjects.get(0);
                    
                    if(pricebookMap.containsKey(rec.New_SKU_Code__c)){
                        list<PricebookEntry> pbes=pricebookMap.get(rec.New_SKU_Code__c);
                        for(PricebookEntry p:pbes){
                            System.debug('**********************'+p.Pricebook2Id +'&&'+qli.SBQQ__Quote__r.SBQQ__PriceBook__c);
                            if(p.Pricebook2Id ==qli.SBQQ__Quote__r.SBQQ__PriceBook__c){
                                qlicln.SBQQ__Product__c	=p.Product2id;
                                qlicln.SBQQ__PricebookEntryId__c=p.Id;
                                qlicln.Package_Option__c=rec.Option__c;
                                
                                insertlist.add(qlicln);
                            }
                        }
                    }
                  
                    
         }
         // Insert New Line Items and Mark as Added in Temp Table
         Set<String> sucesskey=new Set<String>();
         if(!insertlist.isEmpty()){
            Database.UpsertResult[] sr = Database.upsert(insertlist, false);
              for (Integer i=0; i< sr.size(); i++) {// Collect all Sucess List 
                   if (sr[i].isSuccess()) {
                        String sku=insertlist[i].SBQQ__Product__c;
                        String qut=insertlist[i].SBQQ__Quote__c;
                        String key=qut+'-'+sku;
                        sucesskey.add(key);
                   }
                  
              }
            
         }
         
         
         if(!sucesskey.isEmpty()){// Update Temp record as Added to Quote.
             List<HA_Quote_Model_Transition_TEMP__c> updatelist=new List<HA_Quote_Model_Transition_TEMP__c>();
             for(HA_Quote_Model_Transition_TEMP__c rec:[select id,Added_to_Quote__c,New_SKU_Code__c,Old_SKU_Code__c,Option__c,Quote_Line_Item_ID__c,Quote_Number__c from HA_Quote_Model_Transition_TEMP__c	 where External_ID__c in:sucesskey and Added_to_Quote__c=false ]){
                 rec.Added_to_Quote__c=True;
                 updatelist.add(rec);
             }
             
             if(!updatelist.isEmpty()){
                 Update updatelist;
             }
         }
         
         
     }    
    global void finish(Database.BatchableContext BC){
        
    }
}