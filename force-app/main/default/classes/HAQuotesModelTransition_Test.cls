@isTest
private class HAQuotesModelTransition_Test {

	private static testMethod void test() {
        Test.startTest();
                 
         Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Id HARecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HA Builder').getRecordTypeId();
        Id OppHARecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('HA Builder').getRecordTypeId();
        Id QuoteHARecordTypeId = Schema.SObjectType.SBQQ__Quote__c.getRecordTypeInfosByName().get('HA Builder').getRecordTypeId();

        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            //RecordTypeId = rtMap.get('Account' + 'HA_Builder'),
            RecordTypeId = HARecordTypeId,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        
        );
        insert testAccount1;

        Opportunity testOpp1 = new Opportunity (
            Name = 'TestOpp1',
            //RecordTypeId = rtMap.get('Opportunity' + 'HA_Builder'),
            RecordTypeId = OppHARecordTypeId,
            Project_Type__c = 'SFNC',
            Project_Name__c = 'TestOpp1',
            AccountId = testAccount1.Id,
            StageName = 'Identified',
            CloseDate = Date.today(),
            Roll_Out_Start__c = Date.today(),
            Number_of_Living_Units__c = 10000,
            Rollout_Duration__c = 5

        );
        insert testOpp1;
        PriceBook2 pricebook = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(4)); 
    	
        insert pricebook;
        Id pricebookId = '01s36000006U2yLAAS';

        SBQQ__Quote__c sbq1 = new SBQQ__Quote__c (
            SBQQ__PriceBook__c=pricebookId,
            SBQQ__Primary__c = TRUE,
            SBQQ__Opportunity2__c = testOpp1.Id,
            SBQQ__Account__c = testAccount1.Id,
            SBQQ__Type__c = 'Quote',
            SBQQ__Status__c = 'Draft',
            Expected_Shipment_Date__c = Date.today(),
            Last_Shipment_Date__c = Date.today(),
          
            //RecordTypeId = rtMap.get('SBQQ__Quote__c'+'HA_Builder')
            RecordTypeId = QuoteHARecordTypeId
        );
        insert sbq1;
         List<Product2> pdlist= new list<Product2>();
            Product2 pd=new  Product2(Name='HA-Clone-Batch',SAP_Material_ID__c='HA-Clone-Batch',isActive=true,Family='LAUNDRY',SAP_SW_Product_Code__c='AER0A');          
            pdlist.add(pd);
            Product2 pd2=new  Product2(Name='HA-Clone-Batch12',SAP_Material_ID__c='HA-Clone-Batch12',isActive=true,Family='LAUNDRY',SAP_SW_Product_Code__c='AER0A');          
            pdlist.add(pd2); 
            insert pdlist;
             List<PriceBookEntry> pbelist= new list<PriceBookEntry>();
             
             for(Product2 p:pdlist){
                  PriceBookEntry pbe=new PriceBookEntry(isActive=true,UnitPrice=5000, Pricebook2Id=pricebookId,
                                        Product2Id=p.id);  
                                        pbelist.add(pbe);
             }
       
            insert pbelist;
            
            Product2 p=[Select id,Name from Product2 where SAP_Material_ID__c='HA-Clone-Batch' limit 1];
        SBQQ__QuoteLine__c sbqList = new SBQQ__QuoteLine__c(SBQQ__Product__c=p.id,Package_Option__c='BASE',SBQQ__Quantity__c=50,SBQQ__Quote__c=sbq1.id,SBQQ__ListPrice__c = 5000,SBQQ__NetPrice__c = 5000);
          insert sbqList;
          
           SBQQ__Quote__c qt=[Select id,Name from  SBQQ__Quote__c where id=:sbq1.Id];
           
          HA_Quote_Model_Transition_TEMP__c temprec=new HA_Quote_Model_Transition_TEMP__c();
          temprec.New_SKU_Code__c='HA-Clone-Batch12';
          String qliid=sbqList.Id;
          temprec.Quote_Line_Item_ID__c=qliid.left(15);
          temprec.Quote_Number__c=qt.Name;
          
          insert temprec;
          database.executeBatch(new HAQuotesModelTransition(),1);
        Test.stopTest();
	}

}