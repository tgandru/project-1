global class HARolloutDataLoadingBatch implements Database.Batchable<sObject>,database.stateful{
    global string refNum;
    global HARolloutDataLoadingBatch(string refNum){
        this.refNum=refNum;
    }
    
     global Database.QueryLocator start(Database.BatchableContext BC){
       return Database.getQueryLocator([Select id,Name,Applied_to_ROS__c,Message__c,External_ID__c,Opportunity_Number__c,Rollout_Month_Frm__c,Product_Option__c,Quantity__c,Rollout_Month__c,Roll_Out_Percent__c,SAP_Material_Code__c,Status__c from HA_Rollout_Temp__c where Reference_Number__c=:refNum  order by Opportunity_Number__c]);//And Applied_to_ROS__c=false
   }

   global void execute(Database.BatchableContext BC, List<HA_Rollout_Temp__c> scope){
       List<Roll_Out_Schedule__c> updatelistros=new list<Roll_Out_Schedule__c>();
       map<String,List<HA_Rollout_Temp__c>> harollouttempmap=new map<String,List<HA_Rollout_Temp__c>>();
       Set<string> skus=new set<string>();
       for(HA_Rollout_Temp__c rl:scope){
           skus.add(rl.SAP_Material_Code__c);
           if(harollouttempmap.containsKey(rl.Opportunity_Number__c)){
               List<HA_Rollout_Temp__c> rllst=harollouttempmap.get(rl.Opportunity_Number__c);
               rllst.add(rl);
               harollouttempmap.put(rl.Opportunity_Number__c,rllst);
           }else{
             harollouttempmap.put(rl.Opportunity_Number__c,new list<HA_Rollout_Temp__c>{rl});  
           }
       }
       
       if(harollouttempmap.size()>0){
           List<Roll_Out_Product__c> roplst=new List<Roll_Out_Product__c>([Select id,name,Roll_Out_Plan__c,Roll_Out_Start__c,Roll_Out_End__c,Rollout_Year_Month__c,SAP_Material_Code__c,Number_of_Roll_Out_Schedules__c,Package_Option__c,Roll_Out_Plan__r.Opportunity__r.Opportunity_Number__c,(Select id,name,SAP_Material_Code__c,Opportunity_No__c,Plan_Date__c,Plan_Quantity__c,MonthYear__c,YearMonth__c from Roll_Out_Schedules__r order by Plan_Date__c asc) from Roll_Out_Product__c where Roll_Out_Plan__r.Opportunity__r.Opportunity_Number__c in:harollouttempmap.keyset() and SAP_Material_Code__c in:skus]);
           for(Roll_Out_Product__c rp:roplst){
                if(harollouttempmap.containsKey(rp.Roll_Out_Plan__r.Opportunity__r.Opportunity_Number__c)){
                    List<HA_Rollout_Temp__c> tempdata=harollouttempmap.get(rp.Roll_Out_Plan__r.Opportunity__r.Opportunity_Number__c);
                    for(HA_Rollout_Temp__c rotemp:tempdata){
                        
                        try{
                             if(rp.SAP_Material_Code__c==rotemp.SAP_Material_Code__c && rp.Package_Option__c == rotemp.Product_Option__c){
                             // Same SKU
                             Integer schdulescount=0;
                             for(Roll_Out_Schedule__c ros: rp.Roll_Out_Schedules__r){
                                 if(rotemp.Rollout_Month__c==ros.YearMonth__c){
                                     // Same Month Year
                                     schdulescount++;
                                 }
                             }
                             
                             Integer qty=Integer.valueOf(rotemp.Quantity__c);
                             Integer evenqty=Integer.valueOf(qty/schdulescount);
                             integer remnQty=integer.valueOf(qty-(evenqty*schdulescount));
                                                         Integer rowcnt=1;
                             for(Roll_Out_Schedule__c ros: rp.Roll_Out_Schedules__r){
                                  if(rotemp.Rollout_Month__c==ros.YearMonth__c){
                                      // Same Month Year
                                       If(remnQty==0){
                                            ros.Plan_Quantity__c=evenqty;
                                       }else{
                                          if(rowcnt==schdulescount){
                                                 ros.Plan_Quantity__c=evenqty+remnQty;
                                                   System.debug('Last Schedule');
                                            }else{
                                            ros.Plan_Quantity__c=evenqty;
                                                
                                            } 
                                       }
                                      rowcnt++;
                                      updatelistros.add(ros); 
                                  }
                             }
                         }
                        }catch(exception ex){
                            System.debug('EXCEPTION ::'+ex.getmessage());
                        }
                        
                        
                        
                        
                    }
                }
           }
           
           
       }
       
       Set<String> sucesslst=new Set<String>();
         if(updatelistros.Size()>0){
              System.debug('Updating the ROS'+updatelistros.size());
            // update updatelistros;
             Database.UpsertResult[] sr = Database.upsert(updatelistros, false);
              for (Integer i=0; i< sr.size(); i++) {
                   if (sr[i].isSuccess()) {
                          String Matrialcode=updatelistros[i].SAP_Material_Code__c;
                          String monthyer=updatelistros[i].YearMonth__c;
                          String Oppnum=updatelistros[i].Opportunity_No__c;
                          String key=Oppnum+'-'+Matrialcode+'-'+monthyer;
                          System.debug('key::'+key);
                          sucesslst.add(key);
                        }
              }
         }
         
         List<HA_Rollout_Temp__c> tempsucesslst=new List<HA_Rollout_Temp__c>();
         for(HA_Rollout_Temp__c rl:scope){
             if(sucesslst.contains(rl.External_ID__c)){
               rl.Applied_to_ROS__c=true;  
               tempsucesslst.add(rl);
             }else{
                 
             }
         }
         
         if(tempsucesslst.size()>0){
             update tempsucesslst;
         }
       
   }
   
   global void finish(Database.BatchableContext BC){
      String reportlink=Label.HA_Rollout_Error_Report+refNum;
       AsyncApexJob a = [Select Id, Status,ExtendedStatus,NumberOfErrors,     JobItemsProcessed,
        TotalJobItems, CreatedBy.Email
        from AsyncApexJob where Id =:BC.getJobId()];
          string body='Hello,'+'\n <br></br>'+' We Sucessfully Updated the Rollout Data based on your Input file. Please Check the Data in Salesforce. ';
           body=body+'\n <br></br>';
            body=body+'To See Errored Records Please Use Below Report Link';
           body=body+'\n <br></br>'+reportlink;
            body=body+'\n </br></br>'+'Thanks,SFDC Admin Team';
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {a.CreatedBy.Email};
            mail.setToAddresses(toAddresses);
            mail.setSubject('Uploading Rollout Data ' + a.Status);
            mail.setHTMLBody(body );
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
       
   }

}