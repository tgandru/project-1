public class HARolloutFileTransferIntegration {

  Public Static void SendfiletoCIH(message_queue__c mq){
       List<Attachment>  attch =[SELECT Body,BodyLength,ContentType,CreatedById,CreatedDate,Description,Id,IsDeleted,IsPrivate,LastModifiedById,LastModifiedDate,Name,OwnerId,ParentId FROM Attachment where ParentId=:mq.id ];
       String response='';
       
          for (Attachment d : attch) {
               String boundary = '';//----------------------------741e90d31eff
                String header = '';
                header += '--'+boundary+'\r\n';
                String footer = '';         
                String headerEncoded = '';//EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
                while(headerEncoded.endsWith('=')) {
                    header += ' ';
                    headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
                }
              String attachmentsStr = '';
                String lastPrepend = '';  
              
              
              Blob fileBlob = d.Body;
              string filename = d.Name;
               String fHeader = '';//lastPrepend + '--'+boundary+'\r\n';
                        //fHeader += 'Content-Disposition: form-data; name="fileInfo"; filename="'+filename+'"\r\n';
                        String fHeaderEncoded = EncodingUtil.base64Encode(Blob.valueOf(fheader+'\r\n\r\n'));
                        while(fHeaderEncoded.endsWith('=')) {
                            fHeader += ' ';
                            fHeaderEncoded = EncodingUtil.base64Encode(Blob.valueOf(fHeader+'\r\n\r\n'));
                        }
                        String fbodyEncoded = EncodingUtil.base64Encode(fileBlob);
                        
                           String last4Bytes = fbodyEncoded.substring(fbodyEncoded.length()-4,fbodyEncoded.length());
                        if(last4Bytes.endsWith('==')) {
                            last4Bytes = last4Bytes.substring(0,2) + '0K';
                            fbodyEncoded = fbodyEncoded.substring(0,fbodyEncoded.length()-4) + last4Bytes;
                            lastPrepend = '';
                        } else if(last4Bytes.endsWith('=')) {
                            last4Bytes = last4Bytes.substring(0,3) + 'N';
                            fbodyEncoded = fbodyEncoded.substring(0,fbodyEncoded.length()-4) + last4Bytes;
                            lastPrepend = '\n';
                        } else {
                            lastPrepend = '\r\n';
                        }
                        attachmentsStr += fHeaderEncoded + fbodyEncoded;
                        
                         footer = lastPrepend + footer;
                     String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
                     system.debug('headerEncoded=='+headerEncoded);
                      system.debug('attachmentsStr=='+attachmentsStr);
                       system.debug('footerEncoded=='+footerEncoded);
                     Blob bodyBlob = EncodingUtil.base64Decode(headerEncoded+attachmentsStr+footerEncoded);
                          
                     String resp= webcallout(bodyBlob);
                     
                     if(resp=='OK' || resp=='Ok' || resp=='Sucess' || resp =='sucess'){
                         response=response+filename+'- Sent'+'\n';
                     }else{
                         response=response+filename+'- Error--'+resp+'\n';
                     }
              
          }       
                
        mq.Response_Body__c=response;
        mq.Status__c='success';
        update mq;
      
  }
  
  
  
    
    public static string webcallout(blob body){ 
          string partialEndpoint = Test.isRunningTest() ? 'asdf' : Integration_EndPoints__c.getInstance('Flow-040').partial_endpoint__c;
        String boundary = '----------------------------741e90d31eff';
       System.debug('##requestBody ::'+body);
        System.Httprequest req = new System.Httprequest();
        system.debug('************************************** INSIDE CALLOUT METHOD');
        HttpResponse res = new HttpResponse();
        req.setMethod('POST');
        req.setBodyAsBlob(body);
        system.debug(body);
       
        req.setEndpoint('callout:X012_013_ROI'+partialEndpoint); //+namedEndpoint); todo, make naming consistent on picklist values and endpoints and use that here
        
        req.setTimeout(120000);
         req.setHeader('Content-Type', 'multipart/form-data; boundary='+boundary);
          req.setHeader('Accept', 'application/json');
    
        Http http = new Http();  
       
        
         if(!Test.isRunningTest()){
         res = http.send(req);
         }else{
          res = Testrespond(req);
          }
        System.debug('******** my resonse' + res);
         System.debug('******** my request' + req);
        system.debug('*****Response body::' + res.getBody());
      
        return res.getStatus();  
       
      
  }
  
  
  
   public Static HTTPResponse Testrespond(HTTPRequest req) {          
        string response='';// Testing Purpose 
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatus('OK');
            res.setStatusCode(200);
            res.setBody(response);
            
            return res;                
      } 

}