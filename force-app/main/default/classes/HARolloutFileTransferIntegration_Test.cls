@isTest
private class HARolloutFileTransferIntegration_Test {

	private static testMethod void testmethod1() {
        test.startTest();
        
         message_queue__c mq=new message_queue__c(Status__c='Hold',Integration_Flow_Type__c='Flow-040',Response_Body__c='');
         insert mq;
                Attachment attach=new Attachment();   	
                	attach.Name='Unit Test Attachment';
                	Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
                	attach.body=bodyBlob;
                    attach.parentId=mq.id;
                    insert attach;
              HARolloutFileTransferIntegration.SendfiletoCIH(mq);      
       
        test.stopTest();
	}

}