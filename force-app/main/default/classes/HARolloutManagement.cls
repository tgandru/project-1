public class HARolloutManagement {
Public Date fromdate {get; set;}
Public Date todate {get; set;}
Public String datefilter{get; set;}
Public String selectedRegion {get; set;}
Public String[] selectedstages {get; set;}
public transient blob contentFile {get; set;}
public transient string exportstring {get; set;}
public string  batchid {get; set;}
public string  attachid {get; set;}
public String fileName {get; set;}
public boolean showdownload {get; set;}
public boolean showoptions {get; set;}
public string refNum  {get; set;}
public string OppNO {get; set;}
    public HARolloutManagement(){
     Date d = system.today();
     Integer quarterStartMonth = (((Math.ceil((Decimal.valueOf(d.month()) / 12) * 4))*3) - 2).intValue();
     fromdate = Date.newInstance(d.year(), quarterStartMonth, 1);
     todate=System.today();//fromdate.addMonths(3);
     datefilter='CloseDate';
     selectedRegion ='';
     OppNO ='';
     showdownload =false;
     showoptions =true;
     batchid= ApexPages.currentPage().getParameters().get('BatchID');
     DateTime pr=system.now();
     refNum=UserInfo.getUserId()+String.valueof(pr.Year())+String.valueof(pr.Month())+String.valueof(pr.day())+String.valueof(pr.hour())+String.valueof(pr.minute());

      if(batchid !=null && batchid !=''){
      showoptions =false;
        AsyncApexJob asjob=[Select ID, Status, MethodName, ApexClass.Name from AsyncApexJob where ApexClass.Name='GenerateExportFile' and id=:batchid limit 1];
          if(asjob.Status=='Completed'){
            List<Attachment> attch=new List<Attachment>([Select id from Attachment where Name='HA_Rollout_Data_Export.csv' and description=:asjob.id order by createddate desc limit 1]);
               if(attch.size()>0){
                   attachid =attch[0].id;
                   showdownload =true;
               
               }
              
          }
      } 
     
    }
  
  public List<SelectOption> getDateOptions() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('CreatedDate','Opportunity Created Date'));
        options.add(new SelectOption('Roll_Out_Start__c','Rollout Start Date'));
        return options;
    }
    
    public List<SelectOption> getRegions() {
        List<SelectOption> options = new List<SelectOption>();
     
        options.add(new SelectOption('Central','Central'));
        options.add(new SelectOption('North','Northeast'));
         options.add(new SelectOption('South','Southeast'));
          options.add(new SelectOption('West','West'));
        return options;
    }
    
    public List<SelectOption> getStageNames()
        {
          List<SelectOption> options = new List<SelectOption>();
                
           Schema.DescribeFieldResult fieldResult =
         Opportunity.StageName.getDescribe();
           List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                
           for( Schema.PicklistEntry f : ple)
           {
              options.add(new SelectOption(f.getLabel(), f.getValue()));
           }       
           return options;
        }
        
        
    public PageReference uploaddata(){
   if(String.isBlank(fileName))
        {
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please upload a file'));
            return null;
        }

        if(String.isNotBlank(fileName) && (!fileName.contains('.csv') && !fileName.contains('.CSV')))
        {
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please upload a valid CSV file'));
            return null;
        }
        
        List<HA_Rollout_Temp__c> templist=new List<HA_Rollout_Temp__c>();
        String contentFileStr = contentFile.toString();
        /*   List<String> UploadStringList = contentFileStr.split('\n');
            for(Integer i=1;i<UploadStringList.size()- 1;i++){
                String[] inputvalues = new String[]{};
                inputvalues = UploadStringList[i].split(',');
                String[] headervalues = new String[]{};
                headervalues = UploadStringList[0].split(','); 
                System.debug('Header Size'+headervalues.size());
                System.debug('inputvalues Size'+inputvalues.size());
                 for(integer l=33;l<inputvalues.size();l++){
                   
                     
                     HA_Rollout_Temp__c temprec=new HA_Rollout_Temp__c();
                     temprec.Opportunity_Number__c=inputvalues[0];
                     temprec.SAP_Material_Code__c=inputvalues[13];
                     temprec.Product_Option__c=inputvalues[12];
                     temprec.Reference_Number__c=refNum;
                     //temprec.Roll_Out_Percent__c=integer.valueof(inputvalues[3]);
                     String qty=inputvalues[l].trim();
                     if(qty !='N/A' && qty !=null){
                        temprec.Quantity__c=integer.valueof(qty);
                        temprec.Rollout_Month__c=headervalues[l];
                        templist.add(temprec);
                     }
               
                 }
            }
           */
        
    if(contentFileStr !=null){
       insert templist;
       database.executeBatch(new UploadHARolloutDataIntoTempBatch(contentFile));
       ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Info, 'We are Processing the file, and we will send email Notification once loading finishes.'));

    }
    
    return null;
}    




/*
@RemoteAction
    @readOnly
public static String preparecsvbody(string condition){
    
   
        String query = '';

                query += ' SELECT YearMonth__c ym    ';
                query += '              , Roll_Out_Product__r.Product__r.SAP_Material_ID__c rop       ';
                query += '              , Sum(Plan_Quantity__c) qty  ';
                query += '              , Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.StageName stg   ';
                query += '              , Sum(Actual_Quantity__c) actqty    ';
                query += '              , Sum(Actual_Revenue__c) actrev   ';
                query += '              , Sum(Plan_Revenue__c) rev                                      ';
                query += '              , Roll_Out_Product__r.Package_Option__c optn,Roll_Out_Product__r.Product__r.Family catg         ';
                query += '              ,Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Builder__r.Name bldr       ';
                query += '              ,Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Account.Name custmr        ';
                query += '              ,       Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Roll_Out_Start__c strt,Sum(Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Rollout_Duration__c) durtn ';
                query += '              , Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__c opp,Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Owner.Name ownr,Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Project_State__c prjst,Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Project_Name__c prj                                ';
                query += '      FROM Roll_Out_Schedule__c                                               ';
                query += '      WHERE  '+ condition;
            query += '  GROUP BY YearMonth__c, Roll_Out_Product__r.Product__r.SAP_Material_ID__c ,Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__c,Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Owner.Name,Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Project_State__c,Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Project_Name__c           ';
                query += '      ,Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Roll_Out_Start__c,Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.StageName    ';
                query += '      ,Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Builder__r.Name,Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Account.Name   ';
                query += '      ,Roll_Out_Product__r.Package_Option__c ,Roll_Out_Product__r.Product__r.Family   ';
                query += '      ORDER BY YearMonth__c asc                                               ';
       
         System.debug('query::'+query);
                List<AggregateResult> arList = (List<AggregateResult>)Database.query(query);
                String csvstring='{"lines": [';
                integer count=0;
                Set<id> oppids=new set<id>();
                          for(AggregateResult ar : arList){
                               oppids.add(String.valueOf(ar.get('opp')));  
                          }
                          map<string,Opportunity> oppmap=new map<string,Opportunity>();
                          for(Opportunity o:[Select id,name,CloseDate,StageName,Owner.Name,Owner.UserRole.Name,Roll_Out_Start__c,Roll_Out_End_Formula__c,Rollout_Duration__c,Number_of_Living_Units__c,Opportunity_Number__c from Opportunity where id in:oppids]){
                              oppmap.put(o.id,o);
                          }
                          
                  for(AggregateResult ar : arList){
                      String startline='{';
                      if(count>0){
                          startline=',{';
                      }
                       String endline='}';
                       
                       if(oppmap.containsKey(String.valueOf(ar.get('opp')))){
                           Opportunity op=oppmap.get(String.valueOf(ar.get('opp')));
                       
                       
                      String line='"Opportunity Name":"'+op.Name+'","Project Name":"'+String.valueOf(ar.get('prj'))+'","Project State":"'+String.valueOf(ar.get('prjst'))+'","Owner Name":"'+String.valueOf(ar.get('ownr'))+'","Owner Role":"'+op.owner.UserRole.Name+'"';
                       line=line+',"Status":"'+String.valueOf(ar.get('stg'))+'"';
                      line=line+',"Builder":"'+String.valueOf(ar.get('bldr'))+'"';
                      line=line+',"Customer":"'+String.valueOf(ar.get('custmr'))+'"';
                      line=line+',"Rollout Start Date":"'+op.Roll_Out_Start__c+'"';
                      line=line+',"Rollout End Date":"'+op.Roll_Out_End_Formula__c+'"';
                      line=line+',"Number of Living Units":'+op.Number_of_Living_Units__c;
                      line=line+',"Category":"'+String.valueOf(ar.get('catg'))+'","ProductOption":"'+String.valueOf(ar.get('optn'))+'"';
                      line=line+',"Model Code":"'+String.valueOf(ar.get('rop'))+'","'+String.valueOf(ar.get('ym'))+'":'+Integer.valueOf(ar.get('qty'));
                      csvstring=csvstring+startline+line+endline;
                      count++;
                       }
                  }
                 
                csvstring=csvstring+']}';
                System.debug('JSON String::'+csvstring);
                return csvstring; 
             
}

*/

public PageReference Createfiles(){
       
   if(selectedRegion !=null && selectedRegion !='' && OppNo ==''){
      String stg='';
    
     Stg='\'Commit\''+','+'\'Win\'';
    
    String dtcondition=' Roll_Out_End_Formula__c >= Today  ';
    
    String stagecondition1='  StageName in'+'('+stg+')';
     String regioncondition1='  owner.userRole.Name LIKE '+'\'%'+selectedRegion+'%\'';
    String condition1 =stagecondition1 +'    AND   '+regioncondition1+'      AND RecordType.Name = '+'\'HA Builder\''+ '     And   '+dtcondition;
    
     id jobId=  database.executeBatch(new GenerateExportFile(condition1),1);
      PageReference pageRef = new PageReference('/apex/HARolloutManagement?BatchID='+jobId);
        pageRef.setRedirect(true);
        return pageRef; 
   }else if((selectedRegion ==null || selectedRegion =='' ) && OppNo !=''){
      String OppNoCondition = '  Opportunity_Number__c ='+'\''+OppNo+'\'';
       id jobId=  database.executeBatch(new GenerateExportFile(OppNoCondition),1);
      PageReference pageRef = new PageReference('/apex/HARolloutManagement?BatchID='+jobId);
        pageRef.setRedirect(true);
        return pageRef; 
   }else{
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please Select Region Or Provide Opportunity Number to Export Rollout Data.'));
 
       return null;
   }
    
  //  String jsonst=testjsonstring(condition1 );
  //  return null;
}


public PageReference refreshpage(){
  if(ApexPages.currentPage().getParameters().get('BatchID') != null){
      showoptions =false;
       batchid= ApexPages.currentPage().getParameters().get('BatchID');
       AsyncApexJob asjob=[Select ID, Status, MethodName, ApexClass.Name from AsyncApexJob where ApexClass.Name='GenerateExportFile' and id=:batchid limit 1];
          if(asjob.Status=='Completed'){
              List<Attachment> attch=new List<Attachment>([Select id from Attachment where  description=:asjob.id order by createddate desc limit 1]);
               if(attch.size()>0){
                   attachid =attch[0].id;
                   showdownload =true;
                //   return null;
               //}else{
                PageReference pageRef = new PageReference('/apex/HARolloutManagement?BatchID='+batchid);
                pageRef.setRedirect(true);
                return pageRef;
               
              }
          }else{
             PageReference pageRef = new PageReference('/apex/HARolloutManagement?BatchID='+batchid);
                pageRef.setRedirect(true);
                return pageRef;
                  
          }
        
  }
return null;
}

        
}