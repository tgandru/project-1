@isTest
private class HARolloutManagement_Test {

	private static testMethod void test() {
          profile p = [select id, Name from profile where name= 'System Administrator' limit 1];
           UserRole rl=[Select id from userRole where name='RSM - North' limit 1];
      
        
        User u1 = new User(Alias = 'standt', Email='mycaseTriggerTestwww@ss.com', 
                  EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                  LocaleSidKey='en_US', ProfileId = p.Id, userRoleId=rl.id,
                  TimeZoneSidKey='America/Los_Angeles', UserName='abc11234@abc.com');
          
          insert u1;
          System.runAs(u1){
           Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
       Test.startTest();
		Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        Account testAccount1 = new Account (
        	Name = 'TestAcc1',
            RecordTypeId = rtMap.get('Account' + 'HA_Builder'),
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        
        );
        insert testAccount1;

        Opportunity testOpp1 = new Opportunity (
        	Name = 'TestOpp1',
        	RecordTypeId = rtMap.get('Opportunity' + 'HA_Builder'),
        	Project_Type__c = 'SFNC',
        	Project_Name__c = 'TestOpp1',
        	AccountId = testAccount1.Id,
        	StageName = 'Commit',
        	CloseDate = Date.today(),
        	Roll_Out_Start__c = Date.newInstance(2019, 4, 4),
        	Number_of_Living_Units__c = 10000,
        	Rollout_Duration__c = 5

        );
        insert testOpp1;

      

       Product2 standaloneProd=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Standalone Prod1', 'DISH', 'AEE1A', 'H/W');
       insert standaloneProd;

        Roll_Out__c testRo = new Roll_Out__c (
        	Opportunity__c = testOpp1.Id,
        	Opportunity_RecordType_Name__c = 'HA Builder',
        	Roll_Out_Start__c = Date.newInstance(2017, 1, 2)
        
        );
        insert testRo;

        Roll_Out_Product__c testRop = new Roll_Out_Product__c (
        	Name = 'testProduct',
        	Roll_Out_Plan__c = testRo.Id,
        	Opportunity_RecordType_Name__c = 'HA Builder',
        	Product__c = standaloneProd.Id,
        	Roll_Out_Start__c = Date.newInstance(2019, 4, 4),
        	Rollout_Duration__c = 5,
        	Roll_Out_Percent__c = 40,
        	Package_Option__c = 'Base',
        	Quote_Quantity__c = 50

        );
        insert testRop;

        Roll_Out_Schedule__c ros=new Roll_Out_Schedule__c(
            Plan_Date__c=Date.newInstance(2019, 4, 4),
            Roll_Out_Product__c=testrop.id,
            Plan_Quantity__c=15,
            year__c='2019',
            fiscal_week__c='04',
            YearMonth__c='201904'
            );
            
            insert ros;
            Product2 pd=[Select id,Name,SAP_Material_ID__c from Product2 where id=:standaloneProd.id];
          PageReference VFPage = Page.HARolloutManagement;
         Test.setCurrentPageReference(VFPage); 
         
         HARolloutManagement contr = new HARolloutManagement();
          contr.getDateOptions();
          contr.getRegions();
          contr.getStageNames();
          contr.selectedRegion='North';
          contr.Createfiles();
          Opportunity opp=[Select id, name,Opportunity_Number__c from Opportunity where id=:testOpp1.id limit 1];
          String file='Opportunity Number,Project Name,Project Type,Owner Name,Owner Role,Status,Plant,Builder,Customer,Rollout Start Date,Rollout End Date,Number of Living Units,Opportunity Last modified date,Category,ProductOption,Model Code,Net Unit Price,Buffer,Quote Line QTY,Quote Line %,Total Actual QTY,Total Plan QTY,Actual -201809,Actual -201810,Actual -201811,Actual -201812,Actual -201901,Actual -201902,Actual -201903,201904 \n';
           file=file+opp.Opportunity_Number__c+',Collier New Construction 2018,SFNC,Kevin Karas,Account Manager - Northeast,Win,NJ,Collier Construction,FERGUSON ENTERPRISES- INC.,11/5/2018,12/5/2019,55.0,2018-12-05 17:44:32,LAUNDRY,Base,'+pd.SAP_Material_ID__c+',475.0,false,55.0,50.0,0.0,28.0,N/A,N/A,0.0,N/A,N/A,N/A,N/A,2 \n';

          AsyncApexJob asjob=[Select ID, Status, MethodName, ApexClass.Name from AsyncApexJob where ApexClass.Name='GenerateExportFile'  limit 1];
             ApexPages.currentPage().getParameters().put('BatchID', asjob.Id);
          contr.batchid=asjob.id;
           contr.refreshpage();
           contr.contentFile=blob.valueOf(file);
            contr.fileName='Test.CSV';
            contr.uploaddata();
      Test.stopTest();
	}
	    
	}
	
		private static testMethod void test3() {
          profile p = [select id, Name from profile where name= 'System Administrator' limit 1];
           UserRole rl=[Select id from userRole where name='RSM - North' limit 1];
      
        
        User u1 = new User(Alias = 'standt', Email='mycaseTriggerTestwww@ss.com', 
                  EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                  LocaleSidKey='en_US', ProfileId = p.Id, userRoleId=rl.id,
                  TimeZoneSidKey='America/Los_Angeles', UserName='abc11234@abc.com');
          
          insert u1;
          System.runAs(u1){
           Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
       Test.startTest();
		Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        Account testAccount1 = new Account (
        	Name = 'TestAcc1',
            RecordTypeId = rtMap.get('Account' + 'HA_Builder'),
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        
        );
        insert testAccount1;

        Opportunity testOpp1 = new Opportunity (
        	Name = 'TestOpp1',
        	RecordTypeId = rtMap.get('Opportunity' + 'HA_Builder'),
        	Project_Type__c = 'SFNC',
        	Project_Name__c = 'TestOpp1',
        	AccountId = testAccount1.Id,
        	StageName = 'Commit',
        	CloseDate = Date.today(),
        	Roll_Out_Start__c = Date.newInstance(2019, 4, 4),
        	Number_of_Living_Units__c = 10000,
        	Rollout_Duration__c = 5

        );
        insert testOpp1;

      

       Product2 standaloneProd=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Standalone Prod1', 'DISH', 'AEE1A', 'H/W');
       insert standaloneProd;

        Roll_Out__c testRo = new Roll_Out__c (
        	Opportunity__c = testOpp1.Id,
        	Opportunity_RecordType_Name__c = 'HA Builder',
        	Roll_Out_Start__c = Date.newInstance(2017, 1, 2)
        
        );
        insert testRo;

        Roll_Out_Product__c testRop = new Roll_Out_Product__c (
        	Name = 'testProduct',
        	Roll_Out_Plan__c = testRo.Id,
        	Opportunity_RecordType_Name__c = 'HA Builder',
        	Product__c = standaloneProd.Id,
        	Roll_Out_Start__c = Date.newInstance(2019, 4, 4),
        	Rollout_Duration__c = 5,
        	Roll_Out_Percent__c = 40,
        	Package_Option__c = 'Base',
        	Quote_Quantity__c = 50

        );
        insert testRop;

        Roll_Out_Schedule__c ros=new Roll_Out_Schedule__c(
            Plan_Date__c=Date.newInstance(2019, 4, 4),
            Roll_Out_Product__c=testrop.id,
            Plan_Quantity__c=15,
            year__c='2019',
            fiscal_week__c='04',
            YearMonth__c='201904'
            );
            
            insert ros;
            Product2 pd=[Select id,Name,SAP_Material_ID__c from Product2 where id=:standaloneProd.id];
          PageReference VFPage = Page.HARolloutManagement;
         Test.setCurrentPageReference(VFPage); 
            Opportunity opp=[Select id, name,Opportunity_Number__c from Opportunity where id=:testOpp1.id limit 1];
         HARolloutManagement contr = new HARolloutManagement();
          contr.getDateOptions();
          contr.getRegions();
          contr.getStageNames();
          contr.OppNo=opp.Opportunity_Number__c;
          contr.Createfiles();
       
          String file='Opportunity Number,Project Name,Project Type,Owner Name,Owner Role,Status,Builder,Customer,Rollout Start Date,Rollout End Date,Number of Living Units,Opportunity Last modified date,Category,ProductOption,Model Code,Net Unit Price,Buffer,Quote Line QTY,Quote Line %,Total Actual QTY,Total Plan QTY,Actual -201809,Actual -201810,Actual -201811,Actual -201812,Actual -201901,Actual -201902,Actual -201903,201904 \n';
           file=file+opp.Opportunity_Number__c+',Collier New Construction 2018,SFNC,Kevin Karas,Account Manager - Northeast,Win,Collier Construction,FERGUSON ENTERPRISES- INC.,11/5/2018,12/5/2019,55.0,2018-12-05 17:44:32,LAUNDRY,Base,'+pd.SAP_Material_ID__c+',475.0,false,55.0,50.0,0.0,28.0,N/A,N/A,0.0,N/A,N/A,N/A,N/A,2 \n';

          AsyncApexJob asjob=[Select ID, Status, MethodName, ApexClass.Name from AsyncApexJob where ApexClass.Name='GenerateExportFile'  limit 1];
             ApexPages.currentPage().getParameters().put('BatchID', asjob.Id);
          contr.batchid=asjob.id;
           contr.refreshpage();
           contr.contentFile=blob.valueOf(file);
            
            contr.uploaddata();
      Test.stopTest();
	}
	    
	}
	
		private static testMethod void test2() {
          profile p = [select id, Name from profile where name= 'System Administrator' limit 1];
           UserRole rl=[Select id from userRole where name='RSM - North' limit 1];
      
        
        User u1 = new User(Alias = 'standt', Email='mycaseTriggerTestwww@ss.com', 
                  EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                  LocaleSidKey='en_US', ProfileId = p.Id, userRoleId=rl.id,
                  TimeZoneSidKey='America/Los_Angeles', UserName='abc11234@abc.com');
          
          insert u1;
          System.runAs(u1){
           Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
       Test.startTest();
		Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        Account testAccount1 = new Account (
        	Name = 'TestAcc1',
            RecordTypeId = rtMap.get('Account' + 'HA_Builder'),
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        
        );
        insert testAccount1;

        Opportunity testOpp1 = new Opportunity (
        	Name = 'TestOpp1',
        	RecordTypeId = rtMap.get('Opportunity' + 'HA_Builder'),
        	Project_Type__c = 'SFNC',
        	Project_Name__c = 'TestOpp1',
        	AccountId = testAccount1.Id,
        	StageName = 'Commit',
        	CloseDate = Date.today(),
        	Roll_Out_Start__c = Date.newInstance(2019, 4, 4),
        	Number_of_Living_Units__c = 10000,
        	Rollout_Duration__c = 5

        );
        insert testOpp1;

      

       Product2 standaloneProd=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Standalone Prod1', 'DISH', 'AEE1A', 'H/W');
       insert standaloneProd;

        Roll_Out__c testRo = new Roll_Out__c (
        	Opportunity__c = testOpp1.Id,
        	Opportunity_RecordType_Name__c = 'HA Builder',
        	Roll_Out_Start__c = Date.newInstance(2017, 1, 2)
        
        );
        insert testRo;

        Roll_Out_Product__c testRop = new Roll_Out_Product__c (
        	Name = 'testProduct',
        	Roll_Out_Plan__c = testRo.Id,
        	Opportunity_RecordType_Name__c = 'HA Builder',
        	Product__c = standaloneProd.Id,
        	Roll_Out_Start__c = Date.newInstance(2019, 4, 4),
        	Rollout_Duration__c = 5,
        	Roll_Out_Percent__c = 40,
        	Package_Option__c = 'Base',
        	Quote_Quantity__c = 50

        );
        insert testRop;

        Roll_Out_Schedule__c ros=new Roll_Out_Schedule__c(
            Plan_Date__c=Date.newInstance(2019, 4, 4),
            Roll_Out_Product__c=testrop.id,
            Plan_Quantity__c=15,
            year__c='2019',
            fiscal_week__c='04',
            YearMonth__c='201904'
            );
            
            insert ros;
            Product2 pd=[Select id,Name,SAP_Material_ID__c from Product2 where id=:standaloneProd.id];
          PageReference VFPage = Page.HARolloutManagement;
         Test.setCurrentPageReference(VFPage); 
         
         HARolloutManagement contr = new HARolloutManagement();
          contr.getDateOptions();
          contr.getRegions();
          contr.getStageNames();
          contr.selectedRegion='North';
          contr.Createfiles();
          Opportunity opp=[Select id, name,Opportunity_Number__c from Opportunity where id=:testOpp1.id limit 1];
          String file='Opportunity Number,Project Name,Project Type,Owner Name,Owner Role,Status,Builder,Customer,Rollout Start Date,Rollout End Date,Number of Living Units,Opportunity Last modified date,Category,ProductOption,Model Code,Net Unit Price,Buffer,Quote Line QTY,Quote Line %,Total Actual QTY,Total Plan QTY,Actual -201809,Actual -201810,Actual -201811,Actual -201812,Actual -201901,Actual -201902,Actual -201903,201904 \n';
           file=file+opp.Opportunity_Number__c+',Collier New Construction 2018,SFNC,Kevin Karas,Account Manager - Northeast,Win,Collier Construction,FERGUSON ENTERPRISES- INC.,11/5/2018,12/5/2019,55.0,2018-12-05 17:44:32,LAUNDRY,Base,'+pd.SAP_Material_ID__c+',475.0,false,55.0,50.0,0.0,28.0,N/A,N/A,0.0,N/A,N/A,N/A,N/A,2 \n';

          AsyncApexJob asjob=[Select ID, Status, MethodName, ApexClass.Name from AsyncApexJob where ApexClass.Name='GenerateExportFile'  limit 1];
             ApexPages.currentPage().getParameters().put('BatchID', asjob.Id);
          contr.batchid=asjob.id;
           contr.refreshpage();
           contr.contentFile=blob.valueOf(file);
            
            contr.uploaddata();
      Test.stopTest();
	}
	    
	}

}