public class HARolloutPageController_Ltng {


@AuraEnabled(cacheable=true)
Public Static Roll_Out__c getRolloutInformation(String recordId){
    
    Roll_Out__c rollout=[SELECT Id
              , Name
              , Opportunity__c
              , Opportunity__r.Name
              ,Opportunity__r.CloseDate
              ,Opportunity__r.Owner.Name
              , Opportunity__r.AccountId
              , Opportunity__r.Account.Name
              , Opportunity__r.Opportunity_Number__c
              , Opportunity__r.StageName
              , Opportunity__r.Amount
              , Opportunity__r.Project_Type__c
              , Opportunity__r.Number_of_Living_Units__c
              , ForecastAmount__c
              , Roll_Out_Status__c  
              , Roll_Out_End_Formula__c
              , Roll_Out_Start__c
              , Rollout_Duration__c
              , Quote__r.SBQQ__Status__c
              , Quote__r.SBQQ__NetAmount__c
              , Quote__r.SBQQ__Type__c 
              , Quote__r.Name
              , Quote__c
              , Editable__c
              FROM Roll_Out__c 
              WHERE Id =: recordId
        
        
                         ];
           return rollout;              
                         
    
}

@AuraEnabled(cacheable=true)
Public Static Opportunity getOpportunityInformation(String recordId){
    
    Opportunity opp=[SELECT Id
               , Name
               ,CloseDate
              ,Owner.Name
              , AccountId
              , Account.Name
              , Opportunity_Number__c
              , StageName
              , Amount
              , Project_Type__c
              , Number_of_Living_Units__c
              , ForecastAmount__c
              , Roll_Out_Status__c  
              , Roll_Out_End_Formula__c
              , Roll_Out_Start__c
              , Rollout_Duration__c
              , SBQQ__PrimaryQuote__r.SBQQ__Status__c
              , SBQQ__PrimaryQuote__r.SBQQ__NetAmount__c
              , SBQQ__PrimaryQuote__r.SBQQ__Type__c 
              , SBQQ__PrimaryQuote__r.Name
              , SBQQ__PrimaryQuote__c
              ,Roll_Out_Plan__c
              
              FROM Opportunity 
              WHERE Id =: recordId
        
        
                         ];
           return opp;              
                         
    
}

@AuraEnabled(cacheable=true)
Public Static List<PBEListWrapper> getCategoryList(){
    List<PBEListWrapper> retnlist=new List<PBEListWrapper>();
     List<String> prodFamilyList = new List<String>();
     for(ProductFamily__c s : [SELECT Id, Name FROM ProductFamily__c]){
            if(s.Name=='MICROWAVE'){
                retnlist.add(new PBEListWrapper(true,s.Name));
            }else{
                retnlist.add(new PBEListWrapper(false,s.Name));
            }
      }
        /* for(ProductFamilyandGroupMap__c pfgm : ProductFamilyandGroupMap__c.getall().values()){
        if(prodFamilyList.contains(pfgm.Product_Family__c)){
            string productCode = pfgm.Field_Name_1_Value__c;
            productCode = productCode.replace(' OR ', ',');
            for(String pcode : productCode.split(',')){
                retnlist.add(pcode);
            }
        }
    } */
    
    return retnlist;
}


 public class PBEListWrapper {
        @AuraEnabled public boolean selected {get;set;}
        @AuraEnabled public  String name {get;set;}
     public PBEListWrapper(Boolean selected,string name){
         this.selected=selected;
         this.name=name;
     }
 }

Public Static List<Object> getRolloutInfo(String recordId){
    
    String query ='SELECT+Actual_Quantity__c,Actual_Quote__c,Actual_Revenue__c,Allocate__c,Do_Not_Allocate__c,Do_Not_Edit__c,Id,Invoice_Price__c,Name,Number_of_Roll_Out_Schedules__c,OpportunityId__c,Opportunity_Line_Item__c,Opportunity_Owner_Sales_Team__c,Opportunity_RecordType_Name__c,Package_Option__c,Plan_Quantity__c,Plan_Quote_Qty__c,Plan_Quote_Rev__c,Plan_Revenue__c,Plan_Variance__c,Product_Category__c,Product_Family__c,Product__c,Project_Type__c,Quote_Quantity__c,Quote_Revenue__c,Remain_Quantity__c,Rollout_Duration__c,Rollout_Year_Month__c,Roll_Out_End_Formula__c,Roll_Out_End__c,Roll_Out_Percent__c,Roll_Out_Plan__c,Roll_Out_Start__c,SalesPrice__c,SAP_Material_Code__c,Shipped_Quantity__c,Unplanned_Quantity__c,Unshipped_Quantity__c,(SELECT+Actual_Quantity__c,Actual_Revenue__c,Do_Not_Edit__c,fiscal_month__c,fiscal_week__c,Id,MonthYear__c,Opportunity_Id__c,Opportunity_No__c,Plan_Date__c,Plan_Quantity__c,Plan_Revenue__c,Quote_Quantity__c,Roll_Out_Product__c,SAP_Material_Code__c,YearMonth__c,year__c+FROM+Roll_Out_Schedules__r)+FROM+Roll_Out_Product__c+where+OpportunityId__c=';
    query=query+'\''+recordId;
    query=query+'\'';
    
    String Endpoint= URL.getSalesforceBaseUrl().toExternalForm()+'/services/data/v45.0/query/?q=';
    Endpoint=Endpoint+query;
     HttpRequest req = new HttpRequest(); 
        
        req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionID()); 
        req.setHeader('Content-Type', 'application/json'); 
        req.setEndpoint(Endpoint); 
        req.setMethod('GET'); 
        Http h = new Http(); 
        HttpResponse res = h.send(req); 
        system.debug(res.getBody());
        
        Map<String, Object> fieldMap = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
        system.debug('fcha>>'+fieldMap);
        List<Object> recordsMap =   (List<Object>)fieldMap.get('records');
       
        List<Roll_Out_Product__c> rtnlist=new List<Roll_Out_Product__c>();
        
        return recordsMap;
    
    
    
}

@AuraEnabled(cacheable=true)
public static List<ROPListWrapper> getRolloutProductInfo(String recordId){
    
    System.debug('Plan ID::'+recordId);
    List<ROPListWrapper> retnlist=new List<ROPListWrapper>();
    List<Roll_Out_Product__c> roplist=new List<Roll_Out_Product__c>();
         roplist=[ SELECT Actual_Quantity__c,Actual_Quote__c,SBQQ_QuoteLine__r.SBQQ__Quantity__c,Actual_Revenue__c,Allocate__c,AP1__c,Do_Not_Allocate__c,Do_Not_Edit__c,Id,Invoice_Price__c,Name,Number_of_Roll_Out_Schedules__c,OpportunityId__c,Opportunity_Line_Item__c,Opportunity_RecordType_Name__c,Package_Option__c,Plan_Quantity__c,Plan_Quote_Qty__c,Plan_Quote_Rev__c,Plan_Revenue__c,
                          Plan_Variance__c,Product_Category__c,Product_Family__c,Product__r.Product_Category__c,Product__c,Project_Type__c,Quote_Quantity__c,Quote_Revenue__c,Remain_Quantity__c,Rollout_Duration__c,Rollout_Year_Month__c,Roll_Out_End_Formula__c,Roll_Out_End__c,Roll_Out_Percent__c,Roll_Out_Plan__c,Roll_Out_Start__c,SalesPrice__c,Sales_Team__c,SAP_Material_Code__c,
                          SBQQ_QuoteLine__c,Shipped_Quantity__c,TotalPlanRevenue_ASP__c,Unplanned_Quantity__c,Unshipped_Quantity__c FROM Roll_Out_Product__c WHERE Roll_Out_Plan__c =:recordId ORDER BY Product_Family__c, SBQQ_QuoteLine__r.Package_Option__c
              
                    ];    
                    
                    
    for(Roll_Out_Product__c r:roplist){
        retnlist.add(new ROPListWrapper(r));
    } 
    return retnlist;
}
    
@AuraEnabled(cacheable=true)
public static List<ROPListWrapper> getRolloutProductInfoByFamily(String recordId,String family){
    
    System.debug('Plan ID::'+recordId);
    List<ROPListWrapper> retnlist=new List<ROPListWrapper>();
    List<Roll_Out_Product__c> roplist=new List<Roll_Out_Product__c>();
         roplist=[ SELECT Actual_Quantity__c,Actual_Quote__c,SBQQ_QuoteLine__r.SBQQ__Quantity__c,Actual_Revenue__c,Allocate__c,AP1__c,Do_Not_Allocate__c,Do_Not_Edit__c,Id,Invoice_Price__c,Name,Number_of_Roll_Out_Schedules__c,OpportunityId__c,Opportunity_Line_Item__c,Opportunity_RecordType_Name__c,Package_Option__c,Plan_Quantity__c,Plan_Quote_Qty__c,Plan_Quote_Rev__c,Plan_Revenue__c,
                          Plan_Variance__c,Product_Category__c,Product_Family__c,Product__r.Product_Category__c,Product__c,Project_Type__c,Quote_Quantity__c,Quote_Revenue__c,Remain_Quantity__c,Rollout_Duration__c,Rollout_Year_Month__c,Roll_Out_End_Formula__c,Roll_Out_End__c,Roll_Out_Percent__c,Roll_Out_Plan__c,Roll_Out_Start__c,SalesPrice__c,Sales_Team__c,SAP_Material_Code__c,
                          SBQQ_QuoteLine__c,Shipped_Quantity__c,TotalPlanRevenue_ASP__c,Unplanned_Quantity__c,Unshipped_Quantity__c FROM Roll_Out_Product__c WHERE Roll_Out_Plan__c =:recordId AND Product_Family__c=:family ORDER BY Product_Family__c, SBQQ_QuoteLine__r.Package_Option__c
              
                    ];    
                    
                    
    for(Roll_Out_Product__c r:roplist){
        retnlist.add(new ROPListWrapper(r));
    } 
    return retnlist;
}


public class ROPListWrapper {
        @AuraEnabled public Roll_Out_Product__c rop {get;set;}
        @AuraEnabled public  decimal quotelineQty  {get;set;}
        @AuraEnabled public  string category  {get;set;}
         Public ROPListWrapper(Roll_Out_Product__c rop){
             this.rop=rop;
             this.quotelineQty=rop.SBQQ_QuoteLine__r.SBQQ__Quantity__c;
             this.category=rop.Product__r.Product_Category__c;
         }
 }
 
 
 public class ROSListWrapper {
     @AuraEnabled public  Id ROPID  {get;set;}
     @AuraEnabled public  string category  {get;set;}
     @AuraEnabled public  string PrOption  {get;set;}
     @AuraEnabled public  string SAPMaterial  {get;set;}
     @AuraEnabled public  decimal quotelineQty  {get;set;}
     @AuraEnabled public  decimal quotelinepercent  {get;set;}
     @AuraEnabled public  decimal qty  {get;set;}
     @AuraEnabled public  decimal remainqty  {get;set;}
     @AuraEnabled public  boolean allocate  {get;set;}
     @AuraEnabled public  List<RosMonthMap> monthdata  {get;set;}
 }
 
 Public class RosMonthMap{
     @AuraEnabled public  string month  {get;set;}
     @AuraEnabled public  string year  {get;set;}
     @AuraEnabled public  string yearMonth  {get;set;}
     @AuraEnabled public  decimal planqty  {get;set;}
     @AuraEnabled public  decimal planrevenue  {get;set;}
     @AuraEnabled public  decimal actualqty  {get;set;}
     @AuraEnabled public  decimal actualrevenue  {get;set;}
 }
 
 @ReadOnly
    @RemoteAction
 @AuraEnabled(cacheable=true)
 Public Static List<ROSListWrapper> getROSInformation(String recordId){
     
     List<ROPListWrapper> ropwrapper=new List<ROPListWrapper>();
    List<Roll_Out_Product__c> roplist=new List<Roll_Out_Product__c>();
         roplist=[ SELECT Actual_Quantity__c,Actual_Quote__c,SBQQ_QuoteLine__r.SBQQ__Quantity__c,Actual_Revenue__c,Allocate__c,AP1__c,Do_Not_Allocate__c,Do_Not_Edit__c,Id,Invoice_Price__c,Name,Number_of_Roll_Out_Schedules__c,OpportunityId__c,Opportunity_Line_Item__c,Opportunity_RecordType_Name__c,Package_Option__c,Plan_Quantity__c,Plan_Quote_Qty__c,Plan_Quote_Rev__c,Plan_Revenue__c,
                          Plan_Variance__c,Product_Category__c,Product_Family__c,Product__r.Product_Category__c,Product__c,Project_Type__c,Quote_Quantity__c,Quote_Revenue__c,Remain_Quantity__c,Rollout_Duration__c,Rollout_Year_Month__c,Roll_Out_End_Formula__c,Roll_Out_End__c,Roll_Out_Percent__c,Roll_Out_Plan__c,Roll_Out_Start__c,SalesPrice__c,Sales_Team__c,SAP_Material_Code__c,
                          SBQQ_QuoteLine__c,Shipped_Quantity__c,TotalPlanRevenue_ASP__c,Unplanned_Quantity__c,Unshipped_Quantity__c FROM Roll_Out_Product__c WHERE Roll_Out_Plan__c =:recordId
              
                    ];    
                    
                    
    for(Roll_Out_Product__c r:roplist){
        ropwrapper.add(new ROPListWrapper(r));
    } 
     
     
     
     
     List<ROSListWrapper> rtnlist=new List<ROSListWrapper>();
     Map<id,List<RosMonthMap>> monthMap=new Map<id,List<RosMonthMap>>();
    
     if(ropwrapper.Size() <=200 && ropwrapper.Size()>0){
         Set<id> ropid=new set<ID>();
         for(ROPListWrapper r: ropwrapper){
             ropid.add(r.rop.Id);
         }
    
         if(ropid.size()>0){
              List<AggregateResult>  aggresult=new List<AggregateResult>([SELECT YearMonth__c ym, 
                                                                        Roll_Out_Product__r.Id rop,
                                                                        Sum(Plan_Quantity__c) qty ,
                                                                        Sum(Actual_Quantity__c) actqty, 
                                                                        Sum(Actual_Revenue__c) actrev,
                                                                        Sum(Plan_Revenue__c) rev
                                                                        FROM Roll_Out_Schedule__c 
                                                                        WHERE  Roll_Out_Product__c in :ropid
                                                                        GROUP BY YearMonth__c, 
                                                                        Roll_Out_Product__r.Id
                                                                        ORDER BY 
                                                                        YearMonth__c asc]);
                                                                        
                for(AggregateResult a:aggresult){
                     String rop=String.valueOf(a.get('rop'));
                     if(monthMap.containsKey(rop)){
                         List<RosMonthMap> roslist=monthMap.get(rop);
                         RosMonthMap newros=new RosMonthMap();
                         newros.yearMonth=String.valueOf(a.get('ym'));
                         newros.planqty=(Decimal)a.get('qty');
                         newros.planrevenue=(Decimal)a.get('rev');
                         newros.actualqty=(Decimal)a.get('actqty');
                         newros.actualrevenue=(Decimal)a.get('actrev');
                         roslist.add(newros);
                         monthmap.put(rop,roslist);
                         
                     }else{
                           List<RosMonthMap> roslst=new List<RosMonthMap>();
                         RosMonthMap nwros= new RosMonthMap();
                         nwros.yearMonth=String.valueOf(a.get('ym'));
                         nwros.planqty=(Decimal)a.get('qty');
                         nwros.planrevenue=(Decimal)a.get('rev');
                         nwros.actualqty=(Decimal)a.get('actqty');
                         nwros.actualrevenue=(Decimal)a.get('actrev');
                         roslst.add(nwros);
                           monthmap.put(rop,roslst);
                     }
                }
                
                
                for(ROPListWrapper r: ropwrapper){
                    ROSListWrapper ros=new ROSListWrapper();
                    ros.ROPID=r.rop.Id;
                    ros.category=r.rop.Product_Family__c;
                    ros.PrOption=r.rop.Package_Option__c;
                    ros.SAPMaterial=r.rop.SAP_Material_Code__c;
                    ros.quotelineQty=r.quotelineQty;
                    ros.quotelinepercent=r.rop.Roll_Out_Percent__c;
                    ros.qty=r.rop.Quote_Quantity__c;
                    ros.remainqty=r.rop.Remain_Quantity__c;
                    ros.allocate=r.rop.Allocate__c;
                    List<RosMonthMap> schedules=monthmap.get(r.rop.ID);
                    
                    ros.monthdata=schedules;
                    rtnlist.add(ros);
                }
                
         }
         
     }else if(ropwrapper.Size()>0){
         Set<ID> processRop=new set<ID>();
         for(ROPListWrapper r: ropwrapper){
             processRop.add(r.rop.Id);
         }
         List<Id> processRoplist = new List<Id>(processRop);
         for(integer i=0;i<4; i++){
             Set<Id> ROPids=new set<Id>();
             for(Integer x=0;x<200;x++){
                 integer val=x+(i*200);
                 if(val < processRoplist.size()){
                   String ropid=  processRoplist[val]; 
                     ROPids.add(ropid);
                 }else{
                     break;
                 }
             }
             
             if(ROPIds.size()>0){
                                  System.debug('Products size:'+ROPids.size());
                     
                 List<AggregateResult>  aggresult=new List<AggregateResult>([SELECT YearMonth__c ym, 
                                                                        Roll_Out_Product__r.Id rop,
                                                                        Sum(Plan_Quantity__c) qty ,
                                                                        Sum(Actual_Quantity__c) actqty, 
                                                                        Sum(Actual_Revenue__c) actrev,
                                                                        Sum(Plan_Revenue__c) rev
                                                                        FROM Roll_Out_Schedule__c 
                                                                        WHERE  Roll_Out_Product__c in :ROPIds
                                                                        GROUP BY YearMonth__c, 
                                                                        Roll_Out_Product__r.Id
                                                                        ORDER BY 
                                                                        YearMonth__c asc]);
                                                                        
                for(AggregateResult a:aggresult){
                     String rop=String.valueOf(a.get('rop'));
                     if(monthMap.containsKey(rop)){
                         List<RosMonthMap> roslist=monthMap.get(rop);
                         RosMonthMap newros=new RosMonthMap();
                         newros.yearMonth=String.valueOf(a.get('ym'));
                         newros.planqty=(Decimal)a.get('qty');
                         newros.planrevenue=(Decimal)a.get('rev');
                         newros.actualqty=(Decimal)a.get('actqty');
                         newros.actualrevenue=(Decimal)a.get('actrev');
                         roslist.add(newros);
                         monthmap.put(rop,roslist);
                         
                     }else{
                           List<RosMonthMap> roslst=new List<RosMonthMap>();
                         RosMonthMap nwros= new RosMonthMap();
                         nwros.yearMonth=String.valueOf(a.get('ym'));
                         nwros.planqty=(Decimal)a.get('qty');
                         nwros.planrevenue=(Decimal)a.get('rev');
                         nwros.actualqty=(Decimal)a.get('actqty');
                         nwros.actualrevenue=(Decimal)a.get('actrev');
                         roslst.add(nwros);
                           monthmap.put(rop,roslst);
                     }
                }
                
                
                for(ROPListWrapper r: ropwrapper){
                    ROSListWrapper ros=new ROSListWrapper();
                    ros.ROPID=r.rop.Id;
                    ros.category=r.rop.Product_Family__c;
                    ros.PrOption=r.rop.Package_Option__c;
                    ros.SAPMaterial=r.rop.SAP_Material_Code__c;
                    ros.quotelineQty=r.quotelineQty;
                    ros.quotelinepercent=r.rop.Roll_Out_Percent__c;
                    ros.qty=r.rop.Quote_Quantity__c;
                    ros.remainqty=r.rop.Remain_Quantity__c;
                    ros.allocate=r.rop.Allocate__c;
                    List<RosMonthMap> schedules=monthmap.get(r.rop.ID);
                    
                    ros.monthdata=schedules;
                    rtnlist.add(ros);
                }
             
             }else{
                 break;
             }
         }
         
     }
     
     return rtnlist;
 }
   @AuraEnabled(cacheable=true)
    Public Static List<ROSListWrapper> getROSInformationHelper (List<id> ROPids){
         List<ROPListWrapper> ropwrapper=new List<ROPListWrapper>();
    List<Roll_Out_Product__c> roplist=new List<Roll_Out_Product__c>();
         roplist=[ SELECT Actual_Quantity__c,Actual_Quote__c,SBQQ_QuoteLine__r.SBQQ__Quantity__c,Actual_Revenue__c,Allocate__c,AP1__c,Do_Not_Allocate__c,Do_Not_Edit__c,Id,Invoice_Price__c,Name,Number_of_Roll_Out_Schedules__c,OpportunityId__c,Opportunity_Line_Item__c,Opportunity_RecordType_Name__c,Package_Option__c,Plan_Quantity__c,Plan_Quote_Qty__c,Plan_Quote_Rev__c,Plan_Revenue__c,
                          Plan_Variance__c,Product_Category__c,Product_Family__c,Product__r.Product_Category__c,Product__c,Project_Type__c,Quote_Quantity__c,Quote_Revenue__c,Remain_Quantity__c,Rollout_Duration__c,Rollout_Year_Month__c,Roll_Out_End_Formula__c,Roll_Out_End__c,Roll_Out_Percent__c,Roll_Out_Plan__c,Roll_Out_Start__c,SalesPrice__c,Sales_Team__c,SAP_Material_Code__c,
                          SBQQ_QuoteLine__c,Shipped_Quantity__c,TotalPlanRevenue_ASP__c,Unplanned_Quantity__c,Unshipped_Quantity__c FROM Roll_Out_Product__c WHERE ID in :ROPIds
              
                    ];    
                    
                    
    for(Roll_Out_Product__c r:roplist){
        ropwrapper.add(new ROPListWrapper(r));
    } 
         List<ROSListWrapper> rtnlist=new List<ROSListWrapper>();
         Map<id,List<RosMonthMap>> monthMap=new Map<id,List<RosMonthMap>>();
          if(ROPIds.size()>0){
                 System.debug('Products size:'+ROPids.size());
                   List<AggregateResult>  aggresult=new List<AggregateResult>([SELECT YearMonth__c ym, 
                                                                        Roll_Out_Product__r.Id rop,
                                                                        Sum(Plan_Quantity__c) qty ,
                                                                        Sum(Actual_Quantity__c) actqty, 
                                                                        Sum(Actual_Revenue__c) actrev,
                                                                        Sum(Plan_Revenue__c) rev
                                                                        FROM Roll_Out_Schedule__c 
                                                                        WHERE  Roll_Out_Product__c in :ROPIds
                                                                        GROUP BY YearMonth__c, 
                                                                        Roll_Out_Product__r.Id
                                                                        ORDER BY 
                                                                        YearMonth__c asc]);
                                                                        
                for(AggregateResult a:aggresult){
                     String rop=String.valueOf(a.get('rop'));
                     if(monthMap.containsKey(rop)){
                         List<RosMonthMap> roslist=monthMap.get(rop);
                         RosMonthMap newros=new RosMonthMap();
                         newros.yearMonth=String.valueOf(a.get('ym'));
                         newros.planqty=(Decimal)a.get('qty');
                         newros.planrevenue=(Decimal)a.get('rev');
                         newros.actualqty=(Decimal)a.get('actqty');
                         newros.actualrevenue=(Decimal)a.get('actrev');
                         roslist.add(newros);
                         monthmap.put(rop,roslist);
                         
                     }else{
                           List<RosMonthMap> roslst=new List<RosMonthMap>();
                         RosMonthMap nwros= new RosMonthMap();
                         nwros.yearMonth=String.valueOf(a.get('ym'));
                         nwros.planqty=(Decimal)a.get('qty');
                         nwros.planrevenue=(Decimal)a.get('rev');
                         nwros.actualqty=(Decimal)a.get('actqty');
                         nwros.actualrevenue=(Decimal)a.get('actrev');
                         roslst.add(nwros);
                           monthmap.put(rop,roslst);
                     }
                }
                
                
                for(ROPListWrapper r: ropwrapper){
                    ROSListWrapper ros=new ROSListWrapper();
                    ros.ROPID=r.rop.Id;
                    ros.category=r.rop.Product_Family__c;
                    ros.PrOption=r.rop.Package_Option__c;
                    ros.SAPMaterial=r.rop.SAP_Material_Code__c;
                    ros.quotelineQty=r.quotelineQty;
                    ros.quotelinepercent=r.rop.Roll_Out_Percent__c;
                    ros.qty=r.rop.Quote_Quantity__c;
                    ros.remainqty=r.rop.Remain_Quantity__c;
                    ros.allocate=r.rop.Allocate__c;
                    List<RosMonthMap> schedules=monthmap.get(r.rop.ID);
                    
                    ros.monthdata=schedules;
                    rtnlist.add(ros);
                }
                 
             }
        
        return rtnlist;
    }
}