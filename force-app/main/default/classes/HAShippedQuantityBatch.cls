global class HAShippedQuantityBatch implements database.batchable<sObject>,Schedulable{
    global void execute(SchedulableContext sc) {
          Integer rqmi = 60*24;
        DateTime timenow = DateTime.now().addMinutes(rqmi);
        HAShippedQuantityBatch rqm = new HAShippedQuantityBatch();
        String seconds = '0';
        String minutes = String.valueOf(timenow.minute());
        String hours = String.valueOf(timenow.hour());
        String sch = seconds + ' ' + minutes + ' ' + hours + ' * * ?';
        String jobName = 'HAShippedQuantityBatch scheduler - ' + timenow;
        system.schedule(jobName, sch, rqm);
              database.executeBatch(new HAShippedQuantityBatch()) ;
              if (sc != null)  {  
          system.abortJob(sc.getTriggerId());      
        } 
    }
        
    global database.querylocator start(database.batchablecontext bc){
       Id haBuilderRecTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('HA Builder').getRecordTypeId();
       Set<String> currentlyProcessingQuotes = new Set<String>();
        for(Message_Queue__c mq : [select Identification_Text__c from message_queue__c where Integration_Flow_Type__c = 'Flow-051' and Status__c!='success']){
            currentlyProcessingQuotes.add(mq.Identification_Text__c);
        }
       database.Querylocator ql = database.getquerylocator([select id, Contract_Order_Number__c, Total_Quantity__c, Shipped_Quantity__c, SBQQ__Opportunity2__r.closeDate from SBQQ__Quote__c 
                                                            where Contract_Order_Number__c != null and SBQQ__Opportunity2__r.recordTypeId = :haBuilderRecTypeId 
                                                            and id != :currentlyProcessingQuotes]);
       return ql;
    }
    
    global void execute(database.batchablecontext bc, list<SBQQ__Quote__c> quoteList){
        List<Message_Queue__c> messageQueueList = new List<Message_Queue__c>();
        Integration_EndPoints__c endpoint = Integration_EndPoints__c.getInstance('Flow-051');
        string layoutId = endpoint.layout_id__c; 
        
        for(SBQQ__Quote__c qt : quoteList){
               if((qt.SBQQ__Opportunity2__r.closeDate != null && 
                  date.today() >= qt.SBQQ__Opportunity2__r.closeDate.addYears(-3))
                 || qt.Total_Quantity__c != qt.Shipped_Quantity__c ){
					
                    messageQueueList.add(new message_queue__c(Identification_Text__c=qt.Contract_Order_Number__c, 
                                                              Integration_Flow_Type__c = 'Flow-051',
                                                              retry_counter__c=0,
                                                              Status__c='not started', 
                                                              Object_Name__c='Quote',
                                                              IFID__c = layoutId));   
                     
               }
        }
        if(!messageQueueList.isEmpty()){
            Database.insert(messageQueueList, false);
        }
    }
    
    global void finish(database.batchablecontext bc){
    }
}