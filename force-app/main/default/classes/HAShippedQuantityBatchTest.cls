@isTest
public class HAShippedQuantityBatchTest {
    static Account testMainAccount;
    static Account testCustomerAccount;
    static List<Opportunity> testOpportunityList;
    static Opportunity oppOpen;
    static Pricebook2 pb;
    static PricebookEntry standaloneProdPBE;
    static SBQQ__Quote__c quot;
    static Id haBuilderDistRT = TestDataUtility.retrieveRecordTypeId('HA_Builder_Distributor', 'Account'); 
    static Id haBuilderOpptyRT = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('HA Builder').getRecordTypeId();        
    static Id haBuilderDistRTQuote = Schema.SObjectType.SBQQ__Quote__c.getRecordTypeInfosByName().get('HA Builder').getRecordTypeId();    
    
    
    private static void setup() {
        
        //Custom Settings
        Integration_EndPoints__c intgEndpoint = new Integration_EndPoints__c(name='Flow-050');
        insert intgEndpoint;
        
        Integration_EndPoints__c intgEndpoint51 = new Integration_EndPoints__c(name='Flow-051');
        insert intgEndpoint51;
        
        Roll_Out_Schedule_Constants__c rolloutConstants = 
            new Roll_Out_Schedule_Constants__c(name='Roll Out Schedule Parameters',
                                               Average_Week_Count__c = 4.33,
                                               DML_Row_Limit__c = 3200);
        insert rolloutConstants;
        
        Test.loadData(fiscalCalendar__c.sObjectType, 'FiscalCalendarTestData');
        
        //Channelinsight configuration
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        
        Zipcode_Lookup__c zip=TestDataUtility.createZipcode();
        insert zip;
        
        //Creating test Account
        testMainAccount = TestDataUtility.createAccount(TestDataUtility.generateRandomString(5));
        testMainAccount.RecordTypeId = haBuilderDistRT;
        insert testMainAccount;
        
        testCustomerAccount = TestDataUtility.createAccount(TestDataUtility.generateRandomString(5));
        testCustomerAccount.RecordTypeId = haBuilderDistRT;
        insert testCustomerAccount;
        
        //Creating custom Pricebook
        pb = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(4)); 
        insert pb;
        
        testOpportunityList = new List<Opportunity>();
        oppOpen = new Opportunity(Name='Open Oppty',Project_Name__c = '1234',PriceBook2Id=pb.Id,
                                  StageName='Identified', CloseDate=System.today().addDays(5), recordTypeId=haBuilderOpptyRT,
                                  Amount=0, Description = 'Test', Roll_Out_Start__c = System.today().addDays(30),
                                  Rollout_Duration__c = 5);
        oppOpen.Number_of_Living_Units__c = 1000;
        testOpportunityList.add(oppOpen);
        
        insert testOpportunityList;
        
        //Creating Products
        List<Product2> prods=new List<Product2>();
        Product2 standaloneProd=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Standalone Prod1', 'Printer[C2]', 'HOSPITALITY_DISPLAY', 'H/W');
        standaloneProd.SAP_Material_ID__c='114734191';
        prods.add(standaloneProd);
        insert prods;
        
        List<PriceBookEntry> pbes=new List<PriceBookEntry>();
        List<PriceBookEntry> stdpbes=new List<PriceBookEntry>();
        Id stdPBId = Test.getStandardPricebookId();
        PriceBookEntry standaloneProdPBE_std=TestDataUtility.createPriceBookEntry(standaloneProd.Id, stdPBId);
        standaloneProdPBE_std.Product2 = standaloneProd;
        stdpbes.add(standaloneProdPBE_std);
        //insert stdpbes;
        
        standaloneProdPBE=TestDataUtility.createPriceBookEntry(standaloneProd.Id, pb.Id);
        standaloneProdPBE.Product2 = standaloneProd;
        pbes.add(standaloneProdPBE);
        insert pbes;
        
        Contact contactRecord = TestDataUtility.createContact(testMainAccount);
        insert contactRecord;
        
        quot = new SBQQ__Quote__c( SBQQ__Opportunity2__c = oppOpen.Id, SBQQ__Type__c = 'quote', recordTypeId=haBuilderDistRTQuote);
        quot.Contract_Order_Number__c = '1234';
        quot.SBQQ__Status__c = 'draft';
        insert quot;
        
        List<SBQQ__QuoteLine__c> qlis = new List<SBQQ__QuoteLine__c>();
        SBQQ__QuoteLine__c standardQLI = new SBQQ__QuoteLine__c(SBQQ__Quote__c = quot.Id, SBQQ__Product__c = standaloneProd.Id, SBQQ__Quantity__c = 1);
        qlis.add(standardQLI);
        insert qlis;
        
        quot.SBQQ__Status__c = 'Approved';
        update quot;
        
        oppOpen.AccountId = testMainAccount.Id;
        oppOpen.StageName = 'Win';
        oppOpen.CloseDate = System.today() - 30;
        oppOpen.Roll_Out_Start__c = System.today() - 20;
        oppOpen.Roll_Out_Start__c = System.today();
        oppOpen.Rollout_Duration__c = 20;        
        update oppOpen;
        
        //OpptyTriggerHandler.processHABuilderOpptys(new List<Opportunity>{oppOpen});
        
        
    }
    
    
    static testMethod void testBatch(){
        setup();
        Test.startTest();
        Integer rqmi = 60*24;
        DateTime timenow = DateTime.now().addMinutes(rqmi);
        HAShippedQuantityBatch rqm = new HAShippedQuantityBatch();
        String seconds = '0';
        String minutes = String.valueOf(timenow.minute());
        String hours = String.valueOf(timenow.hour());
        String sch = seconds + ' ' + minutes + ' ' + hours + ' * * ?';
        String jobName = 'HAShippedQuantityBatch TEST- ' + hours + ':' + minutes;
        system.schedule(jobName, sch, rqm);
              database.executeBatch(new HAShippedQuantityBatch()) ;
      //  HAShippedQuantityBatch bt = new HAShippedQuantityBatch();
        //Database.executeBatch(bt);
        Test.stopTest();
        System.assertEquals(1, [select id from message_queue__c where Integration_Flow_Type__c = 'Flow-051'].size());
    }    
    
}