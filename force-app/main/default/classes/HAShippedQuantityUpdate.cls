/*
  New Class to get the Number of Shipped Qunatity for GERP 
  We need to Call this class once SO Number Integration Batch finish
  Order of excution for this class is 3 ;
*/

public class HAShippedQuantityUpdate {
    private static Integration_EndPoints__c contractEndpoint = Integration_EndPoints__c.getInstance('Flow-050');
    private static Integration_EndPoints__c shippedQtyEndpoint = Integration_EndPoints__c.getInstance('Flow-051');
    
    public Class HAShippedQuantityRequest{
        public cihStub.msgHeadersRequest inputHeaders {get;set;}        
        public HAShippedQuantityRequestBody body {get;set;}        
    }
    
     public Class HAShippedQuantityRequestBody{
        String piZsysno {get; set;}//Quote
        String piLegacyUserId {get; set;}//Quote
        String piIfKey {get; set;}//Quote
        String piFlag {get;set;}
        String piSalesOrg {get;set;}
        String piPurchNo {get;set;}
        String piSoldTo {get;set;}
        String piCreateOnFrom {get;set;}
        String piCreateOnTo {get;set;}
        
        List<HAShippedQuantitySalesOrder> pitSalesOrder {get; set;}
        HAShippedQuantitySelectionFlag pitSelectionFlag {get; set;}
        
    }
    
     public Class HAShippedQuantitySalesOrder{
        String SALES_ORDER_NO {get; set;}     
        public HAShippedQuantitySalesOrder(String SALES_ORDER_NO){
            this.SALES_ORDER_NO = SALES_ORDER_NO;
        }
    }  
    
     public Class HAShippedQuantitySelectionFlag{
        public String flag {get; set;}
        public List<String> tableName {get; set;}   
        
        public HAShippedQuantitySelectionFlag(String flag ){
            this.flag = flag;
            //this.tableName = new List<String>{'PET_SALES_ORDER_FLOW'};
            this.tableName = new List<String>{'PET_TRACKING_REPOSITORY'};
        }
    }
    
      public class HAShippedQuantityResponse{
        public cihStub.msgHeadersResponse inputHeaders {get;set;}  
        public HAShippedQuantityResponseBody body {get; set;}
    }    
    
    //Body
    public class HAShippedQuantityResponseBody{
        HAShippedQuantityPesReturnResponse pesReturn {get; set;}
        List<HAShippedpetTrackingRepository> petTrackingRepository {get;set;} 
        //public List<HAShippedQuantitySalesOrderFlowResponse> petSalesOrderFlow {get; set;}
    }
    
    //pesReturn
    public class HAShippedQuantityPesReturnResponse{
        String TYPE {get; set;}
        String ID {get; set;}
        String NumberN {get; set;} //system is a reserved word - shall we use numberN
        String MESSAGE {get; set;}
        String LOG_NO {get; set;}
        String LOG_MSG_NO {get; set;}
        String MESSAGE_V1 {get; set;}
        String MESSAGE_V2 {get; set;}
        String MESSAGE_V3 {get; set;}
        String MESSAGE_V4 {get; set;}
        String PARAMETER {get; set;}
        String ROW {get; set;}
        String FIELD {get; set;}
        String SystemN {get; set;} //system is a reserved word - shall we use systemN
    }
    //petTrackingRepository
    public class HAShippedpetTrackingRepository{
        String VBELN_SO {get; set;}
        String POSNR_SO {get; set;}
        String BSTKD {get; set;}
        String VDATU {get; set;}
        String AUART {get; set;}
        String KUNAG {get; set;}
        String MATNR {get; set;}
        String KWMENG {get; set;}
        String OPEN_QTY {get; set;}
        String ERDAT_SO {get; set;}
        String ERZET_SO {get; set;}
        String ERDAT_SO_LO {get; set;}
        String ERZET_SO_LO {get; set;}
        String EDATU {get; set;}//Added on 07-12-2019
        String VBELN_DO {get; set;}
        String POSNR_DO {get; set;}
        String LFIMG {get; set;}
        String BLDAT {get; set;}
        String ERDAT_DO {get; set;}
        String ERZET_DO {get; set;}
        String ERDAT_DO_LO {get; set;}
        String ERZET_DO_LO {get; set;}
        String VBELN_BL {get; set;}
        String POSNR_BL {get; set;}
        String FKIMG {get; set;}
        String FKDAT {get; set;}
        String ERDAT_BL {get; set;}
        String ERZET_BL {get; set;}
        String ERDAT_BL_LO {get; set;}
        String ERZET_BL_LO {get; set;}
    }
    //petSalesOrderFlow
    /*public class HAShippedQuantitySalesOrderFlowResponse{
        String VBELV {get; set;}
        String POSNV {get; set;}
        String VBELN {get; set;}
        String POSNN {get; set;}
        String VBTYP_N {get; set;}
        String RFMNG {get; set;}
        String MATNR {get; set;}
    }*/
    
    
    //Utility Methods
    public static String formatDate(Date dt){
        return dt == null? null:((DateTime)dt).format('YYYYMMdd');
    }
    
    //------CallOut Method -----//
    Public static Void callout(message_queue__c mq){
        //We need to Query the SO Number for Opportunity - If No of So's is greater than or equal to 1 
        List<SO_Number__c> sonums=new List<SO_Number__c>([select id,name,SO_Number__c,Opportunity__c,Number_Of_Lines__c,(select id,SO_Number__c,DO_Qty__c,Order_Qty__c from SO_Lines__r) from SO_Number__c where Opportunity__c=:mq.Identification_Text__c]);
        
        //05-22-2019--Thiru::Added to exclude SO numbers having all SO Lines with equal Order_Qty----Starts here 
        List<SO_Number__c> soNumsToGerp = new List<SO_Number__c>();
        for(SO_Number__c soNum : sonums){
            if(soNum.SO_Lines__r.size()>0){
                for(SO_Lines__c line : soNum.SO_Lines__r){
                    if(line.DO_Qty__c!=line.Order_Qty__c){
                        soNumsToGerp.add(soNum);
                        break;
                    }
                }
            }else{
                system.debug('<==else block SO Number==>'+soNum.SO_Number__c);
                soNumsToGerp.add(soNum);
            }
            //soNumsToGerp.add(soNum);
        }
        
        sonums.clear();
        sonums.addAll(soNumsToGerp);
        system.debug('<==sonums size==>'+sonums.size());
        //----Ends here----
        
        set<id> soids= new set<Id>();
        map<string,id> SO_Num_ID_Map = new map<string,id>();
        map<string,id> SO_Oppty_Map = new map<string,id>();
        map<string,List<SO_Lines__c>> SOExistingLinesMap = new map<string,List<SO_Lines__c>>();
        if(sonums.size()>0){
          for(SO_Number__c s:sonums){
              SO_Num_ID_Map.put(s.SO_Number__c,s.id);
              SO_Oppty_Map.put(s.SO_Number__c,s.Opportunity__c);
              if(s.Number_Of_Lines__c >0){
                  soids.add(s.id);
                  SOExistingLinesMap.put(s.SO_Number__c,s.SO_Lines__r);
              }  
          }  
        }
        
        List<SO_Lines__c> solinesList = new List<SO_Lines__c>();//[select id,SO_Number__c from SO_Lines__c where SO_Number__c=:soids];
        for(SO_Lines__c s : [select id,SO_Number__c from SO_Lines__c where SO_Number__c=:soids]){
            solinesList.add(s);
        }
        
        string layoutId = shippedQtyEndpoint.layout_id__c;
        HAShippedQuantityRequest request = new HAShippedQuantityRequest();
        //Header
        request.inputHeaders = new cihStub.msgHeadersRequest(layoutId);
        
        //Body
        request.body = new HAShippedQuantityRequestBody();            
        request.body.piZsysno = 'SYS1738'; 
        request.body.piLegacyUserId = '95088394'; 
        request.body.piIfKey = cihStub.giveGUID();  
        request.body.piFlag = '00'; 
        request.body.piSalesOrg = '3101'; 
        request.body.piPurchNo = ''; 
        request.body.piSoldTo = ''; 
        request.body.piCreateOnFrom = '20090101' ; 
        request.body.piCreateOnTo = '99991231' ; 
        
        
        //pitSalesOrder
        request.body.pitSalesOrder = new List<HAShippedQuantitySalesOrder>();
        for(SO_Number__c s:sonums){
             request.body.pitSalesOrder.add(new HAShippedQuantitySalesOrder(s.SO_Number__c));
            
        }
         String strFlag= '';
        request.body.pitSelectionFlag = new HAShippedQuantitySelectionFlag(strFlag);
        
        System.debug('request body-->'+request);
         system.debug(JSON.serialize(request));
        
        try{
            
            String response=callShippedQuantityAPI(request);
            system.debug('response body-->'+response);
            if(string.isNotBlank(response)){ 
                 HAShippedQuantityResponse response2 = (HAShippedQuantityResponse) JSON.deserialize(response, HAShippedQuantityResponse.class);
                   if(response2.inputHeaders!=null){
                          if(response2.inputHeaders.MSGSTATUS == 'S'){
                                //Integration Sucess
                                String businessError = getShippedQtyBusinessError(response2);
                                if(businessError != null){
                                    handlerBusinessError(mq,response2, businessError); 
                                }else{
                                    handlesucess(mq,response2,SO_Oppty_Map,solinesList,SO_Num_ID_Map);
                                }
                                  
                          }else{//Intergraion Error 
                              System.debug('Msg Status is not S');
                              handleError(mq);
                          }
                       
                   }else{
                    System.debug('## Flow051 Else block--No inputHeaders ');
                    handleError(mq);
                }
                
            }else{
               System.debug('No response ');
                handleError(mq); 
            }
            
            
        }catch(exception ex){
            system.debug(ex.getmessage()); 
            handleError(mq); 
        }
        
        
    }
    
     public static String callShippedQuantityAPI(HAShippedQuantityRequest request){
        //TODO - Setup endpoint
        string partialEndpoint = Test.isRunningTest() ? 'asdf' : shippedQtyEndpoint.partial_endpoint__c;  
        
        system.debug(JSON.serialize(request));
        
        System.Httprequest req = new System.Httprequest();        
        HttpResponse res = new HttpResponse();
        req.setMethod('POST');
        req.setBody(JSON.serialize(request));
        
        req.setEndpoint('callout:X012_013_ROI'+partialEndpoint);
        system.debug('callout:X012_013_ROI'+partialEndpoint);
        req.setTimeout(120000);
        
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Accept', 'application/json');
        
        Http http = new Http();     
        res = http.send(req);
        system.debug(res.getBody());
        return res.getBody();            
    }
    
    private static String getShippedQtyBusinessError(HAShippedQuantityResponse response){
        String businessError = null;
        if(response != null && response.body != null && response.body.pesReturn != null){
            if(response.body.pesReturn.type == 'E'){
                businessError = response.body.pesReturn.message;
            }
        }
        return businessError;
    }
     public static void handlerBusinessError(message_queue__c mq, HAShippedQuantityResponse response1, String businessError){
        mq.ERRORTEXT__c = businessError;
        mq.MSGSTATUS__c = 'E';
        mq.status__c = 'Success' ;
        mq.MSGUID__c = response1.inputHeaders.msgGUID;
        mq.IFID__c = response1.inputHeaders.ifID;
        mq.IFDate__c = response1.inputHeaders.ifDate;
        upsert mq;
    }
     
    public static void handleError(message_queue__c mq){
        mq.retry_counter__c += 1;
        mq.status__c = 'failed' ;
        upsert mq;      
    } 
    
    //Public static void handlesucess(message_queue__c mq, HAShippedQuantityResponse response,List<SO_Number__c> sonms){
    Public static void handlesucess(message_queue__c mq, HAShippedQuantityResponse response1, Map<string,id> SO_Oppty_Map,List<SO_Lines__c> SOExistingLines,Map<string,id> SO_Num_Map){
        System.debug('Sucess Handling');
        mq.status__c = 'Success';
        mq.MSGSTATUS__c = response1.inputHeaders.MSGSTATUS;
        mq.MSGUID__c = response1.inputHeaders.msgGUID;
        mq.IFID__c = response1.inputHeaders.ifID;
        mq.IFDate__c = response1.inputHeaders.ifDate;
        upsert mq;
        
        List<SO_Lines__c> solines = new List<SO_Lines__c>();
        if(response1!=null && response1.body != null && response1.body.petTrackingRepository != null && !response1.body.petTrackingRepository.isEmpty()){
            Savepoint sp = Database.setSavepoint();
            try{
                for(HAShippedpetTrackingRepository res : response1.body.petTrackingRepository){
                    system.debug('res---->'+res);
                    system.debug('Res SO Number---->'+res.VBELN_SO);
                    system.debug('Res DO Number---->'+res.VBELN_DO);
                    //if(res.VBELN_DO!=''&&res.VBELN_DO!=null){ //07-18-2019::Thiru::Commented this IF condition to store SO Lines irrespective of VBELN_DO
                        SO_Lines__c soline = new SO_Lines__c();
                        //soline.SO_Number__c            =string.valueOf(res.VBELN_SO);
                        if(res.POSNR_SO!=''&&res.POSNR_SO!=null)                          soline.SO_Line_Seq__c          =Decimal.valueOf(res.POSNR_SO);
                        if(res.BSTKD!=''&&res.BSTKD!=null)                                soline.Customer_PO_NO__c       =res.BSTKD;
                        //if(res.VDATU!=''&&res.VDATU!=null)                                soline.Req_Delivery_Date__c    =setDate(res.VDATU);
                        if(res.EDATU!=''&&res.EDATU!=null)                                soline.Req_Delivery_Date__c    =setDate(res.EDATU);//Added on 07-12-2019
                        if(res.AUART!=''&&res.AUART!=null)                                soline.SO_Type__c              =res.AUART;
                        if(res.KUNAG!=''&&res.KUNAG!=null)                                soline.Sold_To_Code__c         =res.KUNAG;
                        if(res.MATNR!=''&&res.MATNR!=null)                                soline.SAP_Material_ID__c      =res.MATNR;
                        if(res.KWMENG!=''&&res.KWMENG!=null)                              soline.Order_Qty__c            =Decimal.valueOf(res.KWMENG);
                        if(res.OPEN_QTY!=''&&res.OPEN_QTY!=null)                          soline.Open_Order_Qty__c       =Decimal.valueOf(res.OPEN_QTY);
                        if(res.ERDAT_SO!=''&&res.ERDAT_SO!=null)                          soline.Order_Created_Date__c   =setDateFromString(res.ERDAT_SO,res.ERZET_SO);
                        if(res.VBELN_DO!=''&&res.VBELN_DO!=null)                          soline.DO_Number__c            =res.VBELN_DO;
                        if(res.POSNR_DO!=''&&res.POSNR_DO!=null)                          soline.DO_Line_Seq__c          =Decimal.valueOf(res.POSNR_DO);
                        if(res.LFIMG!=''&&res.LFIMG!=null)                                soline.DO_Qty__c               =Decimal.valueOf(res.LFIMG);
                        if(res.BLDAT!=''&&res.BLDAT!=null&&res.BLDAT!='00000000')         soline.DO_Date__c              =setDate(res.BLDAT);
                        if(res.ERDAT_DO!=''&&res.ERDAT_DO!=null&&res.ERDAT_DO!='00000000') soline.DO_Created_Date__c     =setDateFromString(res.ERDAT_DO,res.ERZET_DO) ;
                        if(res.VBELN_BL!=''&&res.VBELN_BL!=null)                          soline.Billing_Number__c       =res.VBELN_BL;
                        if(res.POSNR_BL!=''&&res.POSNR_BL!=null)                          soline.Billing_Line_Seq__c     =Decimal.valueOf(res.POSNR_BL);
                        if(res.FKIMG!=''&&res.FKIMG!=null)                                soline.Billing_Qty__c          =Decimal.valueOf(res.FKIMG);
                        if(res.FKDAT!=''&&res.FKDAT!=null&&res.FKDAT!='00000000')         soline.Billing_Date__c         =setDate(res.FKDAT);
                        if(res.ERDAT_BL!=''&&res.ERDAT_BL!=null&&res.ERDAT_BL!='00000000') soline.Billing_Created_Date__c=setDateFromString(res.ERDAT_BL,res.ERZET_BL);
                        if(res.ERDAT_BL_LO!=''&&res.ERDAT_BL_LO!=null&&res.ERDAT_BL_LO!='00000000') soline.BL_LO_Date__c =setDateFromString(res.ERDAT_BL_LO,res.ERZET_BL_LO);
                        if(res.ERDAT_SO_LO!=''&&res.ERDAT_SO_LO!=null)                              soline.Sales_Order_Date__c    =setDateFromString(res.ERDAT_SO_LO,res.ERZET_SO_LO);
                        if(res.ERDAT_DO_LO!=''&&res.ERDAT_DO_LO!=null&&res.ERDAT_DO_LO!='00000000') soline.Delivery_Order_Date__c =setDateFromString(res.ERDAT_DO_LO,res.ERZET_DO_LO);
                        if(SO_Oppty_Map.containsKey(res.VBELN_SO)){
                            soline.SO_Number__c    =SO_Num_Map.get(res.VBELN_SO);
                            soline.Opportunity__c  =SO_Oppty_Map.get(res.VBELN_SO);
                        }
                        solines.add(soline);
                    //}
                }
                if(solines.size()>0){
                    insert solines;
                    if(SOExistingLines.size()>0){
                        delete SOExistingLines;
                    }
                }
            }catch(Exception ex){
                Database.rollback(sp);
                system.debug('## Exception -- '+ex.getmessage());
            }
        }
        
    }
    
    public static datetime setDateFromString(string dt, string tm){
        string yyyymm = dt.left(6);
        string mm = yyyymm.right(2);
        string yyyy = dt.left(4);
        string dd = dt.right(2);
        string finalstr = mm+'/'+dd+'/'+yyyy;
        date d = Date.parse(finalstr);
        system.debug('date----->'+d);
        
        string hhmm = tm.left(4);
        integer i1 = integer.valueOf(tm.left(2));
        integer i2 = integer.valueOf(hhmm.right(2));
        integer i3 = integer.valueOf(tm.right(2));
        
        time t = time.newInstance(i1,i2,i3, 0);
        datetime dtime= DateTime.newinstance(d,t);
        if(tm.contains('PM')){
            dtime = dtime.addhours(7);
        }
        System.debug('dtime----->'+dtime);
        return dtime;
    }
    
    public static date setDate(string dt){
        string yyyymm = dt.left(6);
        string mm = yyyymm.right(2);
        string yyyy = dt.left(4);
        string dd = dt.right(2);
        string finalstr = mm+'/'+dd+'/'+yyyy;
        date d = Date.parse(finalstr);
        system.debug('date----->'+d);
        return d;
    }
    
}