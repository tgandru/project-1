@istest
Private class HAShippedquantityUpdateTest{
    static Id HADistyRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HA Builder Distributor').getRecordTypeId(); 
    static Id HART = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HA Builder').getRecordTypeId();
    @testSetup static void setup() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        List<Integration_EndPoints__c> ieps = new List<Integration_EndPoints__c>();
        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='Flow-051';
        iep2.endPointURL__c = 'https://www.google.com';
        iep2.layout_id__c = 'LAY024197';
        iep2.partial_endpoint__c = '/SFDC_IF_US_051';
        //insert iep2;
        ieps.add(iep2);
        
        Integration_EndPoints__c iep3 = new Integration_EndPoints__c();
        iep3.name ='Sales Portal Contract - Prod';
        iep3.endPointURL__c = 'https://www.google.com';
        //insert iep3;
        ieps.add(iep3);
        
        Integration_EndPoints__c iep4 = new Integration_EndPoints__c();
        iep4.name ='Sales Portal Contract - QA';
        iep4.endPointURL__c = 'https://www.google.com';
        //insert iep4;
        ieps.add(iep4);
        
        Integration_EndPoints__c iep5 = new Integration_EndPoints__c();
        iep5.name ='Sales Portal Balanced Qty - Prod';
        iep5.endPointURL__c = 'https://www.google.com';
        //insert iep4;
        ieps.add(iep5);
        
        Integration_EndPoints__c iep6 = new Integration_EndPoints__c();
        iep6.name ='Sales Portal Balanced Qty - QA';
        iep6.endPointURL__c = 'https://www.google.com';
        //insert iep4;
        ieps.add(iep6);
        
        insert ieps;
        
        list<account> accts=new list<account>();
        account a2=TestDataUtility.createAccount('dpba stuff');
        a2.RecordTypeId = HADistyRT;
        a2.sap_company_code__C='5554444';
        accts.add(a2);
        insert accts;
        
        id opprtid = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('HA Builder').getRecordTypeId();
        Opportunity opp = new Opportunity(name='HA',Accountid=a2.id,recordtypeid=opprtid,closedate=Date.valueOf(System.Today()),stagename='Proposal',Project_Name__c='Test',Expected_Close_Date__c=Date.valueOf(System.Today()),Number_of_Living_Units__c=100,Roll_Out_Start__c=Date.valueOf(System.Today()),Rollout_Duration__c=1);
        insert opp;
        
        id qtrtid = Schema.SObjectType.SBQQ__Quote__c.getRecordTypeInfosByName().get('HA Builder').getRecordTypeId();
        SBQQ__Quote__c qt = new SBQQ__Quote__c(SBQQ__Type__c='Quote',SBQQ__ExpirationDate__c=Date.valueOf(System.Today()),Expected_Shipment_Date__c=Date.valueOf(System.Today())+10,Last_Shipment_Date__c=Date.valueOf(System.Today())+30,recordtypeid=qtrtid,SBQQ__Opportunity2__c=opp.id);
        insert qt;
        
        //Creating custom Pricebook
        PriceBook2 pb = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(4)); 
        insert pb;
        
        //Create Products
        List<Product2> prods=new List<Product2>();
        Product2 standaloneProd=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Standalone Prod1', 'LAUNDRY', 'LAUNDRY', 'H/W');
        standaloneProd.Family='LAUNDRY';
        prods.add(standaloneProd);
        
        insert prods;
        system.debug('StandAloneProd------->'+standaloneProd);

        Id pricebookId = Test.getStandardPricebookId();

        //CreatePBEs
        //create the pricebook entries for the custom pricebook
        List<PriceBookEntry> custompbes=new List<PriceBookEntry>();
        PriceBookEntry standaloneProdPBE=TestDataUtility.createPriceBookEntry(standaloneProd.Id, pb.Id);
        custompbes.add(standaloneProdPBE);
        insert custompbes;
        
        List<SBQQ__QuoteLine__c> quotelines = new List<SBQQ__QuoteLine__c>();
        SBQQ__QuoteLine__c line1 = new SBQQ__QuoteLine__c(SBQQ__Quote__c=qt.id,SBQQ__Product__c=standaloneProd.id,SBQQ__Quantity__c=50,Package_Option__c='BASE',SBQQ__NetPrice__c=100.00);
        quotelines .add(line1);
        insert quotelines;
        system.debug('Prod Family----->'+line1.SBQQ__ProductFamily__c);
    }
    
    @isTest static void HAContractIntegrationCallTest(){
        SBQQ__Quote__c quot=[select Id,SBQQ__Status__c from SBQQ__Quote__c limit 1];
        quot.SBQQ__Status__c='Approved';
        update quot;
        Opportunity Opp =[select id,stagename from Opportunity limit 1];
        opp.stagename='Won';
        opp.Sales_Portal_BO_Number__c='BO0000060';
        update Opp;
        
        SO_Number__c so = new SO_Number__c(Opportunity__c=opp.id,SO_Number__c='1278665133');
        insert so;
        
        List<message_queue__c> insertList = new List<message_queue__c>();
        message_queue__c mq = new message_queue__c(Identification_Text__c=quot.id, Integration_Flow_Type__c='Sales Portal',retry_counter__c=0,Status__c='not started', Object_Name__c='HA Quote');
        insertList.add(mq);
        
        insert insertList;
        
        Test.startTest();
         Test.setMock(HttpCalloutMock.class, new MockResponseGenerator2('"S"'));
         HAContractIntegrationCall.calloutportal(mq);
        Test.stopTest(); 
    }
    
    @isTest static void HAbalancedqtyCalloutTest(){
        Opportunity Opp =[select id,stagename from Opportunity limit 1];
        opp.Sales_Portal_BO_Number__c='BO0000060';
        update Opp;
        
        SO_Number__c so = new SO_Number__c(Opportunity__c=opp.id,SO_Number__c='1002059285');
        insert so;
        
        SO_Number__c so2 = new SO_Number__c(Opportunity__c=opp.id,SO_Number__c='1002059457');
        insert so2;
        
        message_queue__c mq2 = new message_queue__c(Identification_Text__c=opp.Sales_Portal_BO_Number__c, Integration_Flow_Type__c='SO Number',retry_counter__c=0,Status__c='not started', Object_Name__c='SO Number');
        insert mq2;
        Test.startTest();
         Test.setMock(HttpCalloutMock.class, new MockResponseGenerator3('"S"'));
         HAbalancedqtyCallout.calloutportal(mq2);
        Test.stopTest();    
    }
    
    @isTest static void HAbalancedqtyCalloutFailTest(){
        Opportunity Opp =[select id,stagename from Opportunity limit 1];
        opp.Sales_Portal_BO_Number__c='BO0000060';
        update Opp;
        message_queue__c mq2 = new message_queue__c(Identification_Text__c=opp.Sales_Portal_BO_Number__c, Integration_Flow_Type__c='SO Number',retry_counter__c=0,Status__c='not started', Object_Name__c='SO Number');
        insert mq2;
        Test.startTest();
         Test.setMock(HttpCalloutMock.class, new MockResponseGenerator3Fail('"F"'));
         HAbalancedqtyCallout.calloutportal(mq2);
        Test.stopTest();    
    }
    
    /*@isTest static void HAbalancedqtyCalloutNoResponse(){
        Opportunity Opp =[select id,stagename from Opportunity limit 1];
        opp.Sales_Portal_BO_Number__c='BO0000060';
        update Opp;
        message_queue__c mq2 = new message_queue__c(Identification_Text__c=opp.Sales_Portal_BO_Number__c, Integration_Flow_Type__c='SO Number',retry_counter__c=0,Status__c='not started', Object_Name__c='SO Number');
        insert mq2;
        Test.startTest();
         Test.setMock(HttpCalloutMock.class, new MockResponseGenerator3NoResponse('"S"'));
         HAbalancedqtyCallout.calloutportal(mq2);
        Test.stopTest();    
    }*/
    
    @isTest static void ShippedQuantityCalloutTest(){
        Opportunity Opp =[select id,stagename from Opportunity limit 1];
        opp.Sales_Portal_BO_Number__c='BO0000060';
        update Opp;
        SO_Number__c so = new SO_Number__c(Opportunity__c=opp.id,SO_Number__c='1278665133');
        insert so;
        message_queue__c mq3 = new message_queue__c(Identification_Text__c=opp.id, Integration_Flow_Type__c='SO Line',retry_counter__c=0,Status__c='not started', Object_Name__c='SO Line');
        insert mq3;
        Test.startTest();
         Test.setMock(HttpCalloutMock.class, new MockResponseGenerator('"S"'));
         HAShippedQuantityUpdate.callout(mq3);
        Test.stopTest();    
    }
    
    class MockResponseGenerator implements HttpCalloutMock {
        String responseStatus;
        MockResponseGenerator(String responseStatus) {
            this.responseStatus = responseStatus;
        }
    // Implement this interface method
        public HTTPResponse respond(HTTPRequest req) {
            // Optionally, only send a mock response for a specific endpoint
            // and method.
            //System.assertEquals('callout:X012_013_ROI', req.getEndpoint());
            System.assertEquals('POST', req.getMethod());
            
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            if(responseStatus=='"S"'){
                res.setBody('{"inputHeaders":{"MSGGUID":"8ca356e0-58e9-279a-f732-d1abaceb57a5","IFID":"LAY024865","IFDate":"20171206053408","MSGSTATUS":"S","ERRORTEXT":""},"body":{"pesReturn":{"TYPE":"S","NUMBER":"000","MESSAGE":"Successfully found 0 items","LOG_MSG_NO":"000000","ROW":"0"},"petTrackingRepository":[{"VBELN_SO":"1278665133","POSNR_SO":"000010","BSTKD":"314464","VDATU":"20170322","AUART":"YNC1","KUNAG":"0001431924","MATNR":"DW80J3020US/AA","KWMENG":4.000,"OPEN_QTY":"0.000","NETPR_SO":365.00,"ERDAT_SO":"20170315","ERZET_SO":"220618","VBELN_DO":"8367608990","POSNR_DO":"000010","LFIMG":"4.000","BLDAT":"20170315","ERDAT_DO":"20170316","ERZET_DO":"041054","VBELN_BL":"3255412535","POSNR_BL":"000010","FKIMG":4.000,"NETWR_BL":1460.00,"FKDAT":"20170327","ERDAT_BL":"20170328","ERZET_BL":"093850"},{"VBELN_SO":"1278665133","POSNR_SO":"000020","BSTKD":"314464","VDATU":"20170322","AUART":"YNC1","KUNAG":"0001431924","MATNR":"NX58H5600SS/AA","KWMENG":4.000,"OPEN_QTY":"0.000","NETPR_SO":563.00,"ERDAT_SO":"20170315","ERZET_SO":"220618","VBELN_DO":"8367608990","POSNR_DO":"000020","LFIMG":"4.000","BLDAT":"20170315","ERDAT_DO":"20170316","ERZET_DO":"041054","VBELN_BL":"3255412535","POSNR_BL":"000020","FKIMG":4.000,"NETWR_BL":2252.00,"FKDAT":"20170327","ERDAT_BL":"20170328","ERZET_BL":"093850"},{"VBELN_SO":"1278665133","POSNR_SO":"000030","BSTKD":"314464","VDATU":"20170322","AUART":"YNC1","KUNAG":"0001431924","MATNR":"ME18H704SFS/AA","KWMENG":4.000,"OPEN_QTY":"0.000","NETPR_SO":177.00,"ERDAT_SO":"20170315","ERZET_SO":"220618","VBELN_DO":"8367608990","POSNR_DO":"000030","LFIMG":"4.000","BLDAT":"20170315","ERDAT_DO":"20170316","ERZET_DO":"041054","VBELN_BL":"3255412535","POSNR_BL":"000030","FKIMG":4.000,"NETWR_BL":708.00,"FKDAT":"20170327","ERDAT_BL":"20170328","ERZET_BL":"093850"}]}}');
            }else{
                res.setBody('{"inputHeaders":{"MSGGUID":"8ca356e0-58e9-279a-f732-d1abaceb57a5","IFID":"LAY024865","IFDate":"20171206053408","MSGSTATUS":"S","ERRORTEXT":""},"body":{"pesReturn":{"TYPE":"E","NUMBER":"000","MESSAGE":"items not found","LOG_MSG_NO":"000000","ROW":"0"},"petTrackingRepository":[{"VBELN_SO":"1278665133","POSNR_SO":"000010","BSTKD":"314464","VDATU":"20170322","AUART":"YNC1","KUNAG":"0001431924","MATNR":"DW80J3020US/AA","KWMENG":4.000,"OPEN_QTY":"0.000","NETPR_SO":365.00,"ERDAT_SO":"20170315","ERZET_SO":"220618","VBELN_DO":"8367608990","POSNR_DO":"000010","LFIMG":"4.000","BLDAT":"20170315","ERDAT_DO":"20170316","ERZET_DO":"041054","VBELN_BL":"3255412535","POSNR_BL":"000010","FKIMG":4.000,"NETWR_BL":1460.00,"FKDAT":"20170327","ERDAT_BL":"20170328","ERZET_BL":"093850"},{"VBELN_SO":"1278665133","POSNR_SO":"000020","BSTKD":"314464","VDATU":"20170322","AUART":"YNC1","KUNAG":"0001431924","MATNR":"NX58H5600SS/AA","KWMENG":4.000,"OPEN_QTY":"0.000","NETPR_SO":563.00,"ERDAT_SO":"20170315","ERZET_SO":"220618","VBELN_DO":"8367608990","POSNR_DO":"000020","LFIMG":"4.000","BLDAT":"20170315","ERDAT_DO":"20170316","ERZET_DO":"041054","VBELN_BL":"3255412535","POSNR_BL":"000020","FKIMG":4.000,"NETWR_BL":2252.00,"FKDAT":"20170327","ERDAT_BL":"20170328","ERZET_BL":"093850"},{"VBELN_SO":"1278665133","POSNR_SO":"000030","BSTKD":"314464","VDATU":"20170322","AUART":"YNC1","KUNAG":"0001431924","MATNR":"ME18H704SFS/AA","KWMENG":4.000,"OPEN_QTY":"0.000","NETPR_SO":177.00,"ERDAT_SO":"20170315","ERZET_SO":"220618","VBELN_DO":"8367608990","POSNR_DO":"000030","LFIMG":"4.000","BLDAT":"20170315","ERDAT_DO":"20170316","ERZET_DO":"041054","VBELN_BL":"3255412535","POSNR_BL":"000030","FKIMG":4.000,"NETWR_BL":708.00,"FKDAT":"20170327","ERDAT_BL":"20170328","ERZET_BL":"093850"}]}}');
            }
            res.setStatusCode(200);
            return res;
        }
    }
    
    class MockResponseGenerator2 implements HttpCalloutMock{
        String responseStatus;
        MockResponseGenerator2(String responseStatus) {
            this.responseStatus = responseStatus;
        }
    // Implement this interface method
        public HTTPResponse respond(HTTPRequest req) {
            
            System.assertEquals('POST', req.getMethod());
            
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('<?xml version="1.0" encoding="UTF-8"?><ContractTransmissionResponse xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="ContractTransmissionResponse.xsd"><ReturnCode>0</ReturnCode><ConfirmNo>BO00000060</ConfirmNo><BONumber>OPP-000116974</BONumber><ReturnMessage>WA40J3000AW/AA Model Code does not exist in Sales Portal.</ReturnMessage></ContractTransmissionResponse>');
            res.setStatusCode(200);
            return res;
        }
    }
    
    class MockResponseGenerator3 implements HttpCalloutMock{
        String responseStatus;
        MockResponseGenerator3(String responseStatus) {
            this.responseStatus = responseStatus;
        }
    // Implement this interface method
        public HTTPResponse respond(HTTPRequest req) {
            
            System.assertEquals('POST', req.getMethod());
            
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('<?xml version="1.0" encoding="UTF-8"?><BalancedInformationResponse xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="BalancedInformationResponse.xsd"><BalancedList><returnCode>0</returnCode><returnMessage></returnMessage><BalancedInfo><SO_NUMBER>1002059285</SO_NUMBER><SO_ITEMNO>10</SO_ITEMNO><SO_MODELCD>WA52J8700AW/A2</SO_MODELCD><SO_QTY>100</SO_QTY><SO_DATE>20171117</SO_DATE></BalancedInfo><BalancedInfo><SO_NUMBER>1002059457</SO_NUMBER><SO_ITEMNO>10</SO_ITEMNO><SO_MODELCD>DV52J8700EW/A2</SO_MODELCD><SO_QTY>120</SO_QTY><SO_DATE>20171117</SO_DATE></BalancedInfo></BalancedList></BalancedInformationResponse>');
            res.setStatusCode(200);
            return res;
        }
    }
    
    class MockResponseGenerator3Fail implements HttpCalloutMock{
        String responseStatus;
        MockResponseGenerator3Fail(String responseStatus) {
            this.responseStatus = responseStatus;
        }
    // Implement this interface method
        public HTTPResponse respond(HTTPRequest req) {
            
            System.assertEquals('POST', req.getMethod());
            
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('<?xml version="1.0" encoding="UTF-8"?><BalancedInformationResponse xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="BalancedInformationResponse.xsd"><BalancedList><returnCode>1</returnCode><returnMessage>The Sales Order is not generated for this BO</returnMessage></BalancedList></BalancedInformationResponse>');
            res.setStatusCode(200);
            return res;
        }
    }
    
}