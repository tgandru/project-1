@isTest
global class HAShpQtyMockHttpResponseGen implements HttpCalloutMock {
    private String respType{get;set;}
    private String qLineNumber{get;set;}    
    public HAShpQtyMockHttpResponseGen(String err){
        respType = err;
        qLineNumber = '"10",';
    }
    public HAShpQtyMockHttpResponseGen(String err, String qLine){
        respType = err;
        qLineNumber = qLine;
    }
    
    // Implement this interface method
   global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        System.assertEquals('POST', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
       res.setHeader('Content-Type', 'application/json');
       if(respType == 'Success'){
           res.setBody(  ' {' + 
                       '    "inputHeaders":{' + 
                       '       "MSGGUID":"35b2e563-e5b6-a96c-f86c-d2e7994057a7",' + 
                       '       "IFID":"IF_SFDC_KR_1085",' + 
                       '       "IFDate":"20161026114153",' + 
                       '       "MSGSTATUS":"S",' + 
                       '       "ERRORTEXT":"",' + 
                       '       "ERRORCODE":""' + 
                       '    },' + 
                       '    "body":{' + 
                       '       "pesReturn":{' + 
                       '          "TYPE":"S",' + 
                       '          "ID":"Q1",' + 
                       '          "NumberN":"1234",' + 
                       '          "MESSAGE":"testShippedQty",' + 
                       '          "LOG_NO":"00000",' + 
                       '          "LOG_MSG_NO":"Test abcd",' + 
                       '          "MESSAGE_V1":"Message v1",' + 
                       '          "MESSAGE_V2":"Message v2",' + 
                       '          "MESSAGE_V3":"Message v3",' + 
                       '          "MESSAGE_V4":"Message v4",' + 
                       '          "PARAMETER":"test Parameter",' + 
                       '          "ROW":"0",' + 
                       '          "FIELD":"1",' + 
                       '          "SystemN":"GMPCLNT100"' + 
                       '       },' + 
                       '       "petTrackingRepository":[' + 
                       '          {' + 
                       ' ' + 
                       '          }' + 
                       '       ],' + 
                       '       "petSalesOrderFlow":[' + 
                       '          {' + 
                       '             "VBELV":"123",' + 
                       '             "POSNV":' + qLineNumber + 
                       '             "VBELN":"114734189",' + 
                       '             "POSNN":"12302",' + 
                       '             "VBTYP_N":"C",' + 
                       '             "RFMNG":"3",' + 
                       '             "MATNR":"114734191"' + 
                       '          },' + 
                       '          {' + 
                       '             "VBELV":"234",' + 
                       '             "POSNV":' + qLineNumber + 
                       '             "VBELN":"114734190",' + 
                       '             "POSNN":"23402",' + 
                       '             "VBTYP_N":"M",' + 
                       '             "RFMNG":"7",' + 
                       '             "MATNR":"114734191"' + 
                       '          },' + 
                       '          {' + 
                       '             "VBELV":"345",' + 
                       '             "POSNV":' + qLineNumber + 
                       '             "VBELN":"114734191",' + 
                       '             "POSNN":"34502",' + 
                       '             "VBTYP_N":"N",' + 
                       '             "RFMNG":"1",' + 
                       '             "MATNR":"114734191"' + 
                       '          },' + 
                       '          {' + 
                       '             "VBELV":"456",' + 
                       '             "POSNV":' + qLineNumber + 
                       '             "VBELN":"114734192",' + 
                       '             "POSNN":"45602",' + 
                       '             "VBTYP_N":"O",' + 
                       '             "RFMNG":"2",' + 
                       '             "MATNR":"114734191"' + 
                       '          }' + 
                       '       ]' + 
                       '    }' + 
                       ' }' ) ;             
        }else if(respType == 'Error'){
            res.setBody( '   {  '  + 
 '   	"inputHeaders": {  '  + 
 '   		"MSGGUID": "35b2e563-e5b6-a96c-f86c-d2e7994057a7",  '  + 
 '   		"IFID": "IF_SFDC_KR_1085",  '  + 
 '   		"IFDate": "20161026114153",  '  + 
 '   		"STATUS": "E",  '  + 
 '   		"ERRORTEXT": "Error",  '  + 
 '   		"ERRORCODE": "Error"  '  + 
 '   	}  ');
        }else if(respType == 'BusinessError'){
               res.setBody(  '   {  '  + 
                        '   	"inputHeaders": {  '  + 
                        '   		"MSGGUID": "35b2e563-e5b6-a96c-f86c-d2e7994057a7",  '  + 
                        '   		"IFID": "IF_SFDC_KR_1085",  '  + 
                        '   		"IFDate": "20161026114153",  '  + 
                        '   		"MSGSTATUS": "S",  '  + 
                        '   		"ERRORTEXT": "",  '  + 
                        '   		"ERRORCODE": ""  '  + 
                        '   	},  '  + 
                        '   	"body": {  '  + 
                        '   		"pesReturn": {  '  + 
                        '   			"TYPE": "E",  '  + 
                        '   			"ID": "Q1",  '  + 
                        '   			"NumberN": "1234",  '  + 
                        '   			"MESSAGE": "testShippedQty",  '  + 
                        '   			"LOG_NO": "00000",  '  + 
                        '   			"LOG_MSG_NO": "Test abcd",  '  + 
                        '   			"MESSAGE_V1": "Message v1",  '  + 
                        '   			"MESSAGE_V2": "Message v2",  '  + 
                        '   			"MESSAGE_V3": "Message v3",  '  + 
                        '   			"MESSAGE_V4": "Message v4"  '  + 
						'   			"PARAMETER": "test Parameter"  '  + 
						'   			"ROW": "0"  '  + 
						'   			"FIELD": "1"  '  + 
						'   			"SystemN": "GMPCLNT100"  '  + 
                        '   		}, ' + 
                        '			"petTrackingRepository":[{'+ ''+
                        '			}],'+
                        '			 "petSalesOrderFlow	": [{ ' +
                        '         		"VBELV" : "123", '+ 
                        '		        "POSNV" : "12301", '+
                        '       		"VBELN" : "114734189", '+
                        '				"POSNN" : "12302", '+
                        '      			"VBTYP_N" : "C",'+
                        '       		"RFMNG" : "3",  '+
                        '        		"MATNR" : "114734189" '+
                        '			},'+
                        '			 "petSalesOrderFlow	": { ' +
                        '         		"VBELV" : "234", '+ 
                        '		        "POSNV" : "23401", '+
                        '       		"VBELN" : "114734190", '+
                        '				"POSNN" : "23402", '+
                        '      			"VBTYP_N" : "Y",'+
                        '       		"RFMNG" : "2",  '+
                        '        		"MATNR" : "114734190" '+
                        '			},'+
                        '			 "petSalesOrderFlow	": { ' +
                        '         		"VBELV" : "345", '+ 
                        '		        "POSNV" : "34501", '+
                        '       		"VBELN" : "114734191", '+
                        '				"POSNN" : "34502", '+
                        '      			"VBTYP_N" : "C",'+
                        '       		"RFMNG" : "5",  '+
                        '        		"MATNR" : "114734191" '+
                        '			},'+
                        '			 "petSalesOrderFlow	": { ' +
                        '         		"VBELV" : "456", '+ 
                        '		        "POSNV" : "45601", '+
                        '       		"VBELN" : "114734192", '+
                        '				"POSNN" : "45602", '+
                        '      			"VBTYP_N" : "Y",'+
                        '       		"RFMNG" : "6",  '+
                        '        		"MATNR" : "114734192" '+
                        '			}],'+
                        '   	}  '  + 
                        '  }  ' ) ;             
        }
        
        res.setStatusCode(200);
        return res;
    }
}