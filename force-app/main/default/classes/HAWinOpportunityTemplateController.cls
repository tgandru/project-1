public class HAWinOpportunityTemplateController {
    
    
  
    //Public String template {get; set;} 
    public Opportunity Opp {get;set;}
    //Public Form_Submission__c submssion {get; set;}
    //Public Form_Layout__c frm {get; set;}
    public  List<response> responsemap {get; set;}
    public ID OppId;
	public ID getOppId(){ return OppId; }
    Public String rolloutlink {get; set;}
    
	public void setOppId(ID s){
		OppId = s;
		HAWinOpportunityTemplateController();
	} 
   public void HAWinOpportunityTemplateController(){
        try{if(opp==null){
               Opp=[ Select id,Name,AccountId,Account.Name,Account.parent_sap_company_code__c,HA_Win_Reason__c,Opportunity_Number__c,Builder__c,Builder__r.Name, Project_Name__c,Number_of_Living_Units__c,Description,Owner.Name,CloseDate,
                       Project_Type__c,Shipment_Street__c,Shipment_City__c,Shipment_State__c,Shipment_Zip_Code__c,Roll_Out_Plan__c,Roll_Out_Plan__r.Name,HA_Shipping_Plant__c,Roll_Out_Start__c,Roll_Out_End_Formula__c,SBQQ__PrimaryQuote__c,Warranty__c From Opportunity Where Id=:oppid	
                         
                   
                   ];
                   
                String baseurl= system.URL.getSalesforceBaseUrl().toExternalForm();
                rolloutlink=baseurl+'/'+opp.Roll_Out_Plan__c;
            
        }
           }catch(exception e){}
        if(responsemap==null){
                 try{
               responsemap=new List<response>();
          Form_Submission__c submssion=[ Select Id,Name,Form_Name__c,Form_Name__r.Form_Template__c,Response_JSON__c From Form_Submission__c where Opportunity__c=:oppid order by createddate desc limit 1 ];
            
              if(submssion.Response_JSON__c != null){
                  map<string,string> submissionresponse=new map<String,String>();
                List<Questionnaire_Response__c> qtner =[Select Id,Name,Question__c,Response__c from Questionnaire_Response__c where Form_Submission__c=:submssion.Id] ;
                for(Questionnaire_Response__c q:qtner){
                    if(q.Response__c !=null && q.Response__c !=''){
                        submissionresponse.put(q.Question__c,q.Response__c);
                    }
                    
                }
                
                String intialtemplate = submssion.Form_Name__r.Form_Template__c;
                String json='{ "data":'+submssion.Form_Name__r.Form_Template__c +'}';
                results r=new results();
                  r = parse(json);
                List<result> data=r.data;
               map<string,boolean> labelnamemap=new map<string,boolean>();
                    for(Result  result: data) {
                         System.debug('****'+result.label);
                          labelnamemap.put(result.label, result.subsection);
                     }
                System.debug('Label Map'+labelnamemap.keyset().size());
                   for(String s:labelnamemap.keyset()){
                      System.debug('****'+s);
                     if(s !=null &&submissionresponse.containsKey(s)){
                         String ans='';
                        
                             ans=submissionresponse.get(s);
                             ans=ans.removeEnd(',');
                        
                        responsemap.add(new response(s,ans,labelnamemap.get(s)));
                     }else{
                         string header ='&nbsp;&nbsp; '+s;
                         responsemap.add(new response(s,'',labelnamemap.get(s)));
                     }
                   }  
                
              }
         
                  
           }catch(exception e){
               System.debug('Exception'+e);
           }
           } 
       
           
   }
  /* public List<response> getresponsemap(){
       List<response> responsemap=new List<response>();
        try{
               
          Form_Submission__c submssion=[ Select Id,Name,Form_Name__c,Form_Name__r.Form_Template__c,Response_JSON__c From Form_Submission__c where Opportunity__c=:oppid order by createddate desc limit 1 ];
            
              if(submssion.Response_JSON__c != null){
                  map<string,string> submissionresponse=new map<String,String>();
                List<Questionnaire_Response__c> qtner =[Select Id,Name,Question__c,Response__c from Questionnaire_Response__c where Form_Submission__c=:submssion.Id] ;
                for(Questionnaire_Response__c q:qtner){
                    submissionresponse.put(q.Question__c,q.Response__c);
                }
                
                String intialtemplate = submssion.Form_Name__r.Form_Template__c;
                String json='{ "data":'+submssion.Form_Name__r.Form_Template__c +'}';
                results r=new results();
                  r = parse(json);
                List<result> data=r.data;
               map<string,boolean> labelnamemap=new map<string,boolean>();
                    for(Result  result: data) {
                         System.debug('****'+result.label);
                          labelnamemap.put(result.label, result.subsection);
                     }
                System.debug('Label Map'+labelnamemap.keyset().size());
                   for(String s:labelnamemap.keyset()){
                      System.debug('****'+s);
                     if(s !=null &&submissionresponse.containsKey(s)){
                         String ans=submissionresponse.get(s);
                         ans=ans.removeEnd(',');
                        responsemap.add(new response(s,ans,labelnamemap.get(s)));
                     }else{
                         string header ='&nbsp;&nbsp; '+s;
                         responsemap.add(new response(s,'',labelnamemap.get(s)));
                     }
                   }  
                
              }
         
                  
           }catch(exception e){
               System.debug('Exception'+e);
           }
       return responsemap;
   }
   */
   
 /*  Public Opportunity getOpp(){
       System.debug('Get OPP INFO');
         if(Opp==null){
           try{
               Opp=[ Select id,Name,AccountId,Account.Name,Account.parent_sap_company_code__c,Builder__c,HA_Win_Reason__c,Opportunity_Number__c,Builder__r.Name, Project_Name__c,Number_of_Living_Units__c,Description,Owner.Name,CloseDate,
                       Project_Type__c,Shipment_Street__c,Shipment_City__c,Shipment_State__c,Shipment_Zip_Code__c,HA_Shipping_Plant__c,Roll_Out_Start__c,Roll_Out_End_Formula__c,SBQQ__PrimaryQuote__c,SBQQ__PrimaryQuote__r.Warranty__c From Opportunity Where Id=:oppid	

                   
                   ];
           }catch(exception e){}
         }
       return Opp;
   }
*/
    public class results{
     public Result[] data;
     }
    public class Result {
        public String label,name;
        public Boolean subsection;
        }
     
     public class value {
         public String label,value;
         public string selected;
         public subType[] subTypes;
     }
     public class subType {
          public String type, label,name,value;
     }
    public static results parse(String jsonString) {
        return (results)JSON.deserialize(jsonString, results.class);
    }
 
       
    public class response {
        public String question{get;set;}
        public String answer{get;set;}
        public boolean showspace {get;set;}
        public response(String qt,String ans, Boolean space){
            answer=ans.removeStart(',');
            question=qt;
            showspace=space;
        }
     }
   
  
}