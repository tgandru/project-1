public class HAWonQuoteApprovalController {
    
    @AuraEnabled
    public static String submitForWonApproval(String OppId){
        String returnval='';
        try{
            Opportunity opp=[Select ID, Name,StageName ,AccountID,Account.SAP_Company_Code__c,Account.ParentID,Account.parent_sap_company_code__c,HA_Win_Reason__c  from Opportunity Where ID=:OppId ]; 
            
            If(Opp.StageName !='Commit'){
                returnval='You need to Set Opportunity stage to Commit before Approval request';
            }else if(Opp.Account.parent_sap_company_code__c==null || Opp.Account.parent_sap_company_code__c==''){
                 returnval='Account_ID of Parent Account of Opportunity\'s Account is required to make Opportunity Win';
            }else if((Opp.Account.parent_sap_company_code__c==null || Opp.Account.parent_sap_company_code__c=='')&&( Opp.Account.SAP_Company_Code__c==null||Opp.Account.SAP_Company_Code__c=='')){
                returnval='Account_ID of Opportunity\'s Account is required to make Opportunity Win';
            }else if(Opp.HA_Win_Reason__c==null || Opp.HA_Win_Reason__c==''){
                 returnval='Please Pervide the WIn Reason before going to Won Approval';
            }else{
                Approval.ProcessSubmitRequest req1=new Approval.ProcessSubmitRequest();
                    req1.setObjectId(OppId);
                    req1.setSubmitterId(UserInfo.getUserId());
                //    req1.comments = 'Automated SEA B2B Project Registration';
                    Approval.ProcessResult result=Approval.process(req1);
                    
                    returnval = 'Success';
                    return returnval;
                
                
            }
            
            
            
            
        }catch(exception ex){
            returnval='Error Occur while Submitting for Won Approval. Error:-'+ex.getMessage();
        }
        
        return returnval;
    }

}