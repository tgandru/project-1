global class HA_CheckMissingRolloutDataBatch implements Database.Batchable<sObject>, Database.Stateful {
    
    global List<Opportunity> OpportunityList = new List<Opportunity>();
    global List<Integer> quoteLineItemCountPerOpporunity = new List<Integer>();
    global List<Set<Id>> quoteLineItemIdsPerOpportunity = new List<Set<Id>>();
    global List<Integer> rolloutProductCountPerOpporunity = new List<Integer>();
    global Integer OpportunityRecordsProcessed = 0;
    global Integer quotelineitemCount = 0;
    global Integer rolloutProductCount = 0;
    
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator('SELECT Id, CreatedDate FROM Opportunity WHERE recordType.name=\'HA Builder\' order by createddate desc');
    }
    global void execute(Database.BatchableContext bc, List<Opportunity> scope) {
        
        transient Map<id,Opportunity> opportunityMap = new Map<id,Opportunity>();
        
        if(scope.size() > 0) {
            for(Opportunity op: scope) {
                opportunityMap.put(op.id, op);
                OpportunityList.add(op);
                OpportunityRecordsProcessed = OpportunityRecordsProcessed + 1;
            }
        }
        
        if(opportunityMap.size() > 0) {
                transient List<SBQQ__QuoteLine__c> quotelineitems = [select Id, SBQQ__Quote__r.SBQQ__Primary__c, SBQQ__Product__r.HA_Product_Type__c, SBQQ__Quote__r.SBQQ__Opportunity2__c from SBQQ__QuoteLine__c where SBQQ__Quote__r.SBQQ__Opportunity2__c in:opportunityMap.keyset() and SBQQ__Quote__r.SBQQ__Primary__c = true and SBQQ__Product__r.HA_Product_Type__c != 'Dacor'];
                transient List<Roll_Out_Product__c>  rolloutProducts = [select Id, Roll_Out_Plan__r.Opportunity__c, SBQQ_QuoteLine__c from Roll_Out_Product__c where Roll_Out_Plan__r.Opportunity__c in:opportunityMap.keyset()];
            
                for(Opportunity opp: opportunityMap.values()){
                    Set<Id> quoteLineItemIdsPerOpporunityTemp = new Set<Id>();
                    for(SBQQ__QuoteLine__c quotelineitem: quotelineitems) {
                        if(quotelineitem.SBQQ__Quote__r.SBQQ__Opportunity2__c == opp.id){
                            quotelineitemCount = quotelineitemCount + 1;
                            quoteLineItemIdsPerOpporunityTemp.add(quotelineitem.Id);
                        }
                     }
                     quoteLineItemCountPerOpporunity.add(quotelineitemCount);
                     quotelineitemCount = 0;
        
                    Set<Id> rolloutQuoteLineItemIdsPerOpporunityTemp = new Set<Id>();
                    for(Roll_Out_Product__c rolloutProduct : rolloutProducts) {
                        if(rolloutProduct.Roll_Out_Plan__r.Opportunity__c == opp.id) {
                            rolloutProductCount = rolloutProductCount +1; 
                            rolloutQuoteLineItemIdsPerOpporunityTemp.add(rolloutProduct.SBQQ_QuoteLine__c);
                        }
                    }
                    rolloutProductCountPerOpporunity.add(rolloutProductCount);
                    rolloutProductCount = 0;
                    
                    Set<ID> oppQuoteLineItemIdsPerOpporunityTemp = new Set<ID>();
                    for(ID qliId : quoteLineItemIdsPerOpporunityTemp) {
                        if(!rolloutQuoteLineItemIdsPerOpporunityTemp.contains(qliId)) {
                            oppQuoteLineItemIdsPerOpporunityTemp.add(qliId);
                        }
                    }
                    quoteLineItemIdsPerOpportunity.add(oppQuoteLineItemIdsPerOpporunityTemp);
                }
        }
    }
    global void finish(Database.BatchableContext bc) {
        //Send an email to the User after your batch completes
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        transient Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        transient String toEmails = Label.HA_Rollout_Actuals_Update_Notification_Emails;
        list<string> toAddresses = toEmails.split(';');
        //list<string> toAddresses = new List<String>{'joe.jung1@partner.samsung.com','p.kabalai@partner.samsung.com'};
        mail.setToAddresses(toAddresses);
        mail.setSubject('Batch Apex Job is done: HA Rollout Product Count vs Quote Line Item Count');
        
        String htmlBody = '';
        htmlBody = '<table width="100%" border="0" cellspacing="0" cellpadding="8" align="center" bgcolor="#F7F7F7">'
                            +'<tr>'
                              +'<td style="font-size: 14px; font-weight: normal; font-family:Calibri;line-height: 18px; color: #333;"><br />'
                                   +'<br />'
                                    +'Dear User,</td>'
                            +'</tr>'
                             +'<tr>'
                              +'<td style="font-size: 14px; font-weight: normal; font-family:Calibri;line-height: 18px; color: #333;"><br />'
                                    +'The attached is the # of Quote Line Item vs # of Rollout Products per HA Opportunity.<br/>'
             						+'If either # of Quote Line Item or # of Rollout Products per HA Opportunity is 0, the opportunity is not included in the file.<br/>'
                              + '</td>'
                            +'</tr>'
                        +'</table>';
            //HTML Body:
            /*            
            htmlBody +=  '<table border="1" style="border-collapse: collapse"><tr><th>Opportunity Id</th><th style="padding-right:5px; padding-left:5px">Opportunity Created Date</th><th style="padding-right:5px; padding-left:5px">Quote Line Item IDs</th><th style="padding-right:5px; padding-left:5px">Quote Line Item Count</th><th style="padding-right:5px; padding-left:5px">Rollout Product Count</th></tr>'; 
             for(Integer i = 0; i < OpportunityList.size(); i++) {
                if(quoteLineItemCountPerOpporunity[i] != 0 && rolloutProductCountPerOpporunity[i]!=0) {
                    if(quoteLineItemCountPerOpporunity[i] != rolloutProductCountPerOpporunity[i]) {
                        htmlBody += '<tr><td style="color:red">' + OpportunityList[i].id + '</td><td style="color:red">' + OpportunityList[i].createddate + '</td><td style="color:red">' + quoteLineItemIdsPerOpportunity[i] + '</td><td style="color:red">' + quoteLineItemCountPerOpporunity[i] + '</td><td style="color:red">' + rolloutProductCountPerOpporunity[i]+'</td></tr>';
                    } else {
                        htmlBody += '<tr><td>' + OpportunityList[i].id + '</td><td>' + OpportunityList[i].createddate + '</td><td>' +  ' ' + '</td><td>' + quoteLineItemCountPerOpporunity[i] + '</td><td>' + rolloutProductCountPerOpporunity[i]+'</td></tr>';
                    }
                }
            }
			*/
            htmlBody += '</table><br>Called from CheckHAMissingRolloutData Class <br/>';
        	
        	//Excel attachment:
        	transient string header ='Opportunity Id, Opportunity Created Date, Quote Line Item IDs, Quote Line Item Count, Rollout Product Count, Count Difference\n';
			String finalstr = header;
			transient String Recordstring;
			String stringList = '';
        	for(Integer i = 0; i < OpportunityList.size(); i++){
                if(quoteLineItemCountPerOpporunity[i] != 0 && rolloutProductCountPerOpporunity[i]!=0) {
                    Integer diff = quoteLineItemCountPerOpporunity[i] - rolloutProductCountPerOpporunity[i];
                    if(quoteLineItemCountPerOpporunity[i] != rolloutProductCountPerOpporunity[i] && diff > 0) {		
                        for(ID qliIds : quoteLineItemIdsPerOpportunity[i]) {
                            	stringList += qliIds + '; ';
                        }
                    }
                    Recordstring = OpportunityList[i].id+','+OpportunityList[i].createddate+','+stringList+','+quoteLineItemCountPerOpporunity[i]+','+rolloutProductCountPerOpporunity[i]+','+ diff +'\n';
                    finalstr = finalstr+Recordstring;
                    stringList= '';
                }
            }
        
            Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
            blob excel = blob.valueOf(finalstr);
            //system.debug('excelblob111'+excel);

            attach.setBody(excel);
        	attach.setFileName('ExcelfileSC.csv');
        	mail.setFileAttachments(new Messaging.EmailFileAttachment[]{attach});
        	
            mail.sethtmlBody(htmlBody);
            mails.add(mail);
            if(mails.size()>0){
                Messaging.sendEmail(mails);
            }
    }
}