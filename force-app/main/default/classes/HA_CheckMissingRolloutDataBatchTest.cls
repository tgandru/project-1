@isTest
public class HA_CheckMissingRolloutDataBatchTest {
    
    @testSetup
    static void setUp() {
        
        Account ac = new Account(name='Joe Account Test');
        
        List<Opportunity> oppList1 = new List<Opportunity>();
        for(Integer i = 0; i < 2; i ++) {
            oppList1.add(new Opportunity(Name = 'Joe Opp'+i, accountId=ac.id, StageName = 'Proposal', closedate=Date.Today(), RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('HA Builder').getRecordTypeId()));
        }

        insert oppList1;
    }
    
	@isTest
    static void test() {
        List<Opportunity> oppList = [SELECT Id, CreatedDate FROM Opportunity WHERE recordType.name='HA Builder' order by createddate desc];

        
        Test.startTest();
        HA_CheckMissingRolloutDataBatch bt = new HA_CheckMissingRolloutDataBatch();
        if(oppList.size() > 0) {
            bt.execute(null,oppList);
        }
        
        Database.executeBatch(bt,30); 
        Test.stopTest();
    }
}