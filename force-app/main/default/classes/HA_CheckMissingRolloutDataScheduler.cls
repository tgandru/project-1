/*
Author:  Joe Jung
Date:    07/17/2019
Purpose: To schedule the HA_CheckMissingRolloutDataBatch that checks for the rollout product # to the quote line item #.
*/
global class HA_CheckMissingRolloutDataScheduler implements Schedulable {
    
     global void execute(SchedulableContext sc) {
         
         HA_CheckMissingRolloutDataBatch rb = new HA_CheckMissingRolloutDataBatch();
         
         database.executeBatch(rb,30);
     }

}