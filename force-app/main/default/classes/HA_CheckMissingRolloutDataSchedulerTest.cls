@isTest
public class HA_CheckMissingRolloutDataSchedulerTest {
    @isTest static void testCheckMissingRolloutDataScheduler() {
        String CRON_EXP = '0 0 3 28 8 ? 2019';
        HA_CheckMissingRolloutDataScheduler schedulerObj = new HA_CheckMissingRolloutDataScheduler();
        Test.startTest();
        String jobId = System.schedule('HA_CheckMissingRolloutDataSchedulerTest',
            CRON_EXP, 
             schedulerObj);
        schedulerObj.execute(null);
          // Get the information from the CronTrigger API object
          CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
             NextFireTime
             FROM CronTrigger WHERE id = :jobId];
        
          // Verify the expressions are the same
          System.assertEquals(CRON_EXP, 
             ct.CronExpression);
        
          // Verify the job has not run
          System.assertEquals(0, ct.TimesTriggered);
        
          // Verify the next time the job will run
          System.assertEquals('2019-08-28 03:00:00', 
             String.valueOf(ct.NextFireTime));
        
        Test.stopTest();

    }

}