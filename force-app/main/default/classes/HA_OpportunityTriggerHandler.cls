/**
 * Created by ms on 2017-11-03.
 *
 * author : JeongHo.Lee, I2MAX
 */
public class HA_OpportunityTriggerHandler {
    
    public static String HA_Builder_Recordtype =Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('HA Builder').getRecordTypeId();//Added by  vijay  to bypass  the HA record validation 
    public static Boolean HA_Won_Changed = false; //Added By Joe Jung to keep track if the method: incrementHAWonCount has been called already.

	//public static Boolean isFirstTime = true;
    public static boolean createRolloutCheck = FALSE;

	public static void rolloutCreate(List<Opportunity> newList, Map<Id, Opportunity> oldQMap) {
	    if(!createRolloutCheck){
	        createRolloutCheck = TRUE;
    		system.debug('## HA_Rollout Create Section');
    		Set<Id> 			oppIdSet			= new Set<Id>();
    		list<Roll_Out__c> 	newRollOuts 		= new list<Roll_Out__c>();
    		List<Opportunity>	opptyToUpdateList	= new List<Opportunity>();
    		
    		List<Opportunity> newOpps = new List<Opportunity>();
    		List<Opportunity> oldOpps = new List<Opportunity>();
    		
    		for(Opportunity opp : newList){
    		    if(opp.HA_Won_Count__c>0){
    		        if((opp.Rollout_Duration__c != null && opp.Rollout_Duration__c > oldQMap.get(opp.Id).Rollout_Duration__c && opp.Roll_Out_Start__c == oldQMap.get(opp.Id).Roll_Out_Start__c)){
        			    newOpps.add(opp);
        			    oldOpps.add(oldQMap.get(opp.Id));
        			    system.debug('## In Add Rollout Plan');
        			}else if(opp.Roll_Out_Start__c != oldQMap.get(opp.Id).Roll_Out_Start__c || opp.Rollout_Duration__c < oldQMap.get(opp.Id).Rollout_Duration__c){
        			    oppIdSet.add(opp.Id);
        
        				newRollOuts.add(
        					new Roll_Out__c(Name = opp.Opportunity_Number__c+ '- Rollout', Opportunity__c = opp.Id, Roll_Out_Status__c = opp.Roll_Out_Status__c,
        						Roll_Out_Start__c = opp.Roll_Out_Start__c, Rollout_Duration__c = opp.Rollout_Duration__c, Opportunity_RecordType_Name__c = 'HA Builder'));
        				system.debug('## In Won Oppty Reset Rollout Plan');
        			}
    		    }else{
        			if((opp.Rollout_Duration__c != null && opp.Rollout_Duration__c != oldQMap.get(opp.Id).Rollout_Duration__c) 
        				|| (opp.Roll_Out_Start__c != null && opp.Roll_Out_Start__c != oldQMap.get(opp.Id).Roll_Out_Start__c)){
        				oppIdSet.add(opp.Id);
        
        				newRollOuts.add(
        					new Roll_Out__c(
        						Name 							= opp.Opportunity_Number__c+ '- Rollout',
        						Opportunity__c 					= opp.Id,
        						Roll_Out_Start__c 				= opp.Roll_Out_Start__c,
        						Rollout_Duration__c 			= opp.Rollout_Duration__c, 
        						Roll_Out_Status__c 				= opp.Roll_Out_Status__c,
        						Opportunity_RecordType_Name__c	= 'HA Builder'
        				));
        				system.debug('## In Reset Rollout Plan');
        			}
    		    }	
    		}
    		system.debug('## oppIdSet size: ' + oppIdSet.size());
    		
    		if(newOpps.size()>0){
    		    HA_RollOutSchedulesBatch_MonthsChange ba = new HA_RollOutSchedulesBatch_MonthsChange(newOpps,oldOpps);
            	Database.executeBatch(ba, 100);
    		}
    		
    		if(!oppIdSet.isEmpty()){
    		    
    		    //07-02-2019..Added by Thiru--Starts..To fix new Rollouts Deletion issue
    		    Set<id> existingRollouts = new Set<id>();
    		    for(Roll_Out__c rollout : [Select Id from Roll_Out__c where Opportunity__c=:oppIdSet]){
    		        existingRollouts.add(rollout.Id);
    		    }
    			if(existingRollouts.size()>0){
    			    HA_RollOutDeleteBatch ba = new HA_RollOutDeleteBatch(existingRollouts);
            	    Database.executeBatch(ba, 1);
    			}
    			//07-02-2019..Added by Thiru--Ends
    			//07-02-2019..Commented below 2 lines by Thiru
    			//HA_RollOutDeleteBatch ba = new HA_RollOutDeleteBatch(oppIdSet);
            	//Database.executeBatch(ba, 1);
    
    			for(SBQQ__Quote__c sb : [SELECT Id , SBQQ__Opportunity2__C
    										FROM SBQQ__Quote__c
    										WHERE SBQQ__Primary__c = TRUE
    										AND SBQQ__Opportunity2__C IN: oppIdSet]){ 
    			 	for(Roll_Out__c ro : newRollOuts){
    			 		if(sb.SBQQ__Opportunity2__c == ro.Opportunity__c) ro.Quote__c = sb.Id;
    			 	}            
    			}
    
    			if(!newRollOuts.isEmpty()){           
    	            System.debug('##JeongHo Insert newRollOuts'+newRollOuts.size()+'::'+newRollOuts);
    	            insert newRollOuts;
    	        }
    
    	        for(Opportunity o: [Select Id,Roll_Out_Plan__c from Opportunity where Id IN:oppIdSet]) {
    	            for(Roll_Out__c r: newRollOuts){
    	                if(o.Id == r.Opportunity__c){
    	                	o.Roll_Out_Plan__c = r.Id;
    	                	opptyToUpdateList.add(o);
    	            	}
    	            }
    	         }
    	        if(!opptyToUpdateList.isEmpty()){
    	            update opptyToUpdateList;
    	        }
    	    }
	    }
	}
    
        //Added By Joe Jung on 2019.07.15 -- start
    //If an opportunity StageName changes to 'Win', increment HA_Won_Count__c by 1 for the validation rule HA_Block_Edit_Based_On_Won_Count to be in effect.
    public static void incrementHAWonCount(List<Opportunity> newOpps, Map<Id, Opportunity> oldOppsMap) {
        for (Opportunity opp : newOpps) {
            Opportunity oldOpp = oldOppsMap.get(opp.Id);
            if(opp.RecordTypeId == HA_Builder_Recordtype && opp.StageName == Label.oppStageRollout && oldOpp.StageName != Label.oppStageRollout) {
                if(opp.HA_Won_Count__c == oldOpp.HA_Won_Count__c) {
                    if(!HA_Won_Changed) {
                        if(opp.HA_Won_Count__c!=null){ //If else Added by Thiru--08/08/2019
                            opp.HA_Won_Count__c = oldOpp.HA_Won_Count__c + 1;
                        }else{
                            opp.HA_Won_Count__c = 1;
                        }
                        //opp.HA_Won_Count__c = oldOpp.HA_Won_Count__c + 1;
                        HA_Won_Changed = true;
                    }
                }
            } 
        }
    }
    //Added By Joe Jung on 2019.07.15 -- end

    //Added By Joe Jung on 2019.07.30 -- start
    //To Correspondingly Update HA Shipping Plant field based on the value of Shipment State field. 
    //Not ready for deployment yet.
    public static void updateHAShippingPlant(List<Opportunity> newOpps, Map<Id, Opportunity> oldOppsMap) {
         Set<String> zipcodes=new set<String>();
        for (Opportunity opp : newOpps) {
            Opportunity oldOpp = oldOppsMap.get(opp.Id);
            if(opp.RecordTypeId == HA_Builder_Recordtype && opp.Shipment_Zip_Code__c != oldOpp.Shipment_Zip_Code__c && opp.Shipment_Zip_Code__c !=null && opp.Shipment_Zip_Code__c!='') {
             zipcodes.add(opp.Shipment_Zip_Code__c);
                /*    if(opp.Shipment_State__c == 'AZ' ||
                   opp.Shipment_State__c == 'CA' ||
                   opp.Shipment_State__c == 'HI' ||
                   opp.Shipment_State__c == 'ID' ||
                   opp.Shipment_State__c == 'MT' ||
                   opp.Shipment_State__c == 'NV' ||
                   opp.Shipment_State__c == 'UT' ||
                   opp.Shipment_State__c == 'WY') opp.HA_Shipping_Plant__c = 'S357';
                
                else if (opp.Shipment_State__c == 'IA' ||
                         opp.Shipment_State__c == 'IL' ||
                         opp.Shipment_State__c == 'IN' ||
                         opp.Shipment_State__c == 'KS' ||
                         opp.Shipment_State__c == 'KY' ||
                         opp.Shipment_State__c == 'MI' ||
                         opp.Shipment_State__c == 'MN' ||
                         opp.Shipment_State__c == 'MO' ||
                         opp.Shipment_State__c == 'ND' ||
                         opp.Shipment_State__c == 'NE' ||
                         opp.Shipment_State__c == 'OH' ||
                         opp.Shipment_State__c == 'SD' ||
                         opp.Shipment_State__c == 'WI' ||
                         opp.Shipment_State__c == 'WV') opp.HA_Shipping_Plant__c = 'S358';
                
                else if (opp.Shipment_State__c == 'AR' ||
                         opp.Shipment_State__c == 'CO' ||
                         opp.Shipment_State__c == 'LA' ||
                         opp.Shipment_State__c == 'MS' ||
                         opp.Shipment_State__c == 'NM' ||
                         opp.Shipment_State__c == 'OK' ||
                         opp.Shipment_State__c == 'TX') opp.HA_Shipping_Plant__c = 'S359';
                
                else if (opp.Shipment_State__c == 'CT' ||
                         opp.Shipment_State__c == 'DE' ||
                         opp.Shipment_State__c == 'MA' ||
                         opp.Shipment_State__c == 'MD' ||
                         opp.Shipment_State__c == 'ME' ||
                         opp.Shipment_State__c == 'NC' ||
                         opp.Shipment_State__c == 'NH' ||
                         opp.Shipment_State__c == 'NJ' ||
                         opp.Shipment_State__c == 'NY' ||
                         opp.Shipment_State__c == 'PA' ||
                         opp.Shipment_State__c == 'RI' ||
                         opp.Shipment_State__c == 'VA' ||
                         opp.Shipment_State__c == 'VT') opp.HA_Shipping_Plant__c = 'S360';
                
                else if (opp.Shipment_State__c == 'OR' ||
                         opp.Shipment_State__c == 'WA') opp.HA_Shipping_Plant__c = 'S362';
                
                else if (opp.Shipment_State__c == 'FL') opp.HA_Shipping_Plant__c = 'S366';
                
                else if (opp.Shipment_State__c == 'AL' || 
                         opp.Shipment_State__c == 'GA' ||
                         opp.Shipment_State__c == 'SC' ||
                         opp.Shipment_State__c == 'TN') opp.HA_Shipping_Plant__c = 'S367';
            
            */
            }
        }
        
        If(zipcodes.size()>0){
            Map<String,Zipcode_Lookup__c> zipcodemap=new map<String,Zipcode_Lookup__c>();
            For(Zipcode_Lookup__c zipRec: [select Id,Name,City_Name__c,HA_Shipping_Plant__c, Country_Code__c, State_Code__c,State_Name__c from Zipcode_Lookup__c where Name IN: zipcodes and HA_Shipping_Plant__c!=null ]){
                zipcodemap.put(zipRec.Name,zipRec);
             }
            
            For(Opportunity opp : newOpps){
                   Opportunity oldOpp = oldOppsMap.get(opp.Id);
                if(opp.RecordTypeId == HA_Builder_Recordtype && opp.Shipment_Zip_Code__c != oldOpp.Shipment_Zip_Code__c && opp.Shipment_Zip_Code__c !=null && opp.Shipment_Zip_Code__c!=''){
                    if(zipcodemap.containsKey(opp.Shipment_Zip_Code__c)){
                        Zipcode_Lookup__c zip=zipcodemap.get(opp.Shipment_Zip_Code__c);
                        Opp.HA_Shipping_Plant__c=Zip.HA_Shipping_Plant__c;
                    }
                }
            }
            
        }
        
    }
    //Added By Joe Jung on 2019.07.30 -- end
    
    //Added By Joe Jung on 2019.08.08 -- Start
    //When a new opportunity gets created, this method looks for the Shipment_State__c value and maps corresponding value to the HA_Shipping_Plant__c field.
    //If there is a manually input value in the HA_Shipping_Plant__c field, the field retain the manually input value.
    //Not ready for deployment yet.
    public static void insertHAShippingPlant(List<Opportunity> newOpps) {
        Set<String> zipcodes=new set<String>();
        for (Opportunity opp : newOpps) {
            if(opp.RecordTypeId == HA_Builder_Recordtype && (opp.HA_Shipping_Plant__c == '' || opp.HA_Shipping_Plant__c ==null) && opp.Shipment_Zip_Code__c !=null && opp.Shipment_Zip_Code__c !='') {
                  zipcodes.add(opp.Shipment_Zip_Code__c);
                
               /* if(opp.Shipment_State__c == 'AZ' ||
                   opp.Shipment_State__c == 'CA' ||
                   opp.Shipment_State__c == 'HI' ||
                   opp.Shipment_State__c == 'ID' ||
                   opp.Shipment_State__c == 'MT' ||
                   opp.Shipment_State__c == 'NV' ||
                   opp.Shipment_State__c == 'UT' ||
                   opp.Shipment_State__c == 'WY') opp.HA_Shipping_Plant__c = 'S357';
                
                else if (opp.Shipment_State__c == 'IA' ||
                         opp.Shipment_State__c == 'IL' ||
                         opp.Shipment_State__c == 'IN' ||
                         opp.Shipment_State__c == 'KS' ||
                         opp.Shipment_State__c == 'KY' ||
                         opp.Shipment_State__c == 'MI' ||
                         opp.Shipment_State__c == 'MN' ||
                         opp.Shipment_State__c == 'MO' ||
                         opp.Shipment_State__c == 'ND' ||
                         opp.Shipment_State__c == 'NE' ||
                         opp.Shipment_State__c == 'OH' ||
                         opp.Shipment_State__c == 'SD' ||
                         opp.Shipment_State__c == 'WI' ||
                         opp.Shipment_State__c == 'WV') opp.HA_Shipping_Plant__c = 'S358';
                
                else if (opp.Shipment_State__c == 'AR' ||
                         opp.Shipment_State__c == 'CO' ||
                         opp.Shipment_State__c == 'LA' ||
                         opp.Shipment_State__c == 'MS' ||
                         opp.Shipment_State__c == 'NM' ||
                         opp.Shipment_State__c == 'OK' ||
                         opp.Shipment_State__c == 'TX') opp.HA_Shipping_Plant__c = 'S359';
                
                else if (opp.Shipment_State__c == 'CT' ||
                         opp.Shipment_State__c == 'DE' ||
                         opp.Shipment_State__c == 'MA' ||
                         opp.Shipment_State__c == 'MD' ||
                         opp.Shipment_State__c == 'ME' ||
                         opp.Shipment_State__c == 'NC' ||
                         opp.Shipment_State__c == 'NH' ||
                         opp.Shipment_State__c == 'NJ' ||
                         opp.Shipment_State__c == 'NY' ||
                         opp.Shipment_State__c == 'PA' ||
                         opp.Shipment_State__c == 'RI' ||
                         opp.Shipment_State__c == 'VA' ||
                         opp.Shipment_State__c == 'VT') opp.HA_Shipping_Plant__c = 'S360';
                
                else if (opp.Shipment_State__c == 'OR' ||
                         opp.Shipment_State__c == 'WA') opp.HA_Shipping_Plant__c = 'S362';
                
                else if (opp.Shipment_State__c == 'FL') opp.HA_Shipping_Plant__c = 'S366';
                
                else if (opp.Shipment_State__c == 'AL' || 
                         opp.Shipment_State__c == 'GA' ||
                         opp.Shipment_State__c == 'SC' ||
                         opp.Shipment_State__c == 'TN') opp.HA_Shipping_Plant__c = 'S367';
               */
            }
        }
        
        If(zipcodes.size()>0){
            Map<String,Zipcode_Lookup__c> zipcodemap=new map<String,Zipcode_Lookup__c>();
            For(Zipcode_Lookup__c zipRec: [select Id,Name,City_Name__c,HA_Shipping_Plant__c, Country_Code__c, State_Code__c,State_Name__c from Zipcode_Lookup__c where Name IN: zipcodes and HA_Shipping_Plant__c!=null ]){
                zipcodemap.put(zipRec.Name,zipRec);
             }
            
            For(Opportunity opp : newOpps){
                if(opp.RecordTypeId == HA_Builder_Recordtype && (opp.HA_Shipping_Plant__c == '' || opp.HA_Shipping_Plant__c ==null) && opp.Shipment_Zip_Code__c !=null && opp.Shipment_Zip_Code__c !=''){
                    if(zipcodemap.containsKey(opp.Shipment_Zip_Code__c)){
                        Zipcode_Lookup__c zip=zipcodemap.get(opp.Shipment_Zip_Code__c);
                        Opp.HA_Shipping_Plant__c=Zip.HA_Shipping_Plant__c;
                    }
                }
            }
            
        }
        
    }
	//Added By Joe Jung on 2019.08.08 -- End
}