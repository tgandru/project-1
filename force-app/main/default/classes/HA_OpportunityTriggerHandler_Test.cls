/**
 * Created by ms on 2017-11-12.
 *
 * author : JeongHo.Lee, I2MAX
 */
@isTest
private class HA_OpportunityTriggerHandler_Test {
    
  @isTest(SeeAllData=true)
  static void CreateTest() {

    Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        Account testAccount1 = new Account (
          Name = 'TestAcc1',
            RecordTypeId = rtMap.get('Account' + 'HA_Builder'),
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        
        );
        insert testAccount1;

        Opportunity testOpp1 = new Opportunity (
          Name = 'TestOpp1',
          RecordTypeId = rtMap.get('Opportunity' + 'HA_Builder'),
          Project_Type__c = 'SFNC',
          Project_Name__c = 'TestOpp1',
          AccountId = testAccount1.Id,
          StageName = 'Identified',
          CloseDate = Date.today(),
          Roll_Out_Start__c = Date.today(),
          Number_of_Living_Units__c = 10000,
          Rollout_Duration__c = 5
        );
        insert testOpp1;
        testOpp1.Rollout_Duration__c = 6;
        update testOpp1;
  }
  @isTest(SeeAllData=true)
	static void createTest1() {
        Opportunity opp = [Select id,Rollout_Duration__c from Opportunity where recordType.name='HA Builder' and Rollout_Duration__c!=null and stagename='Qualified' order by Createddate desc limit 1];
		opp.Rollout_Duration__c = opp.Rollout_Duration__c+1;
		update opp;
	}
    
    @isTest(SeeAllData=true)
	static void createTest2() {
        Opportunity opp = [Select id,Rollout_Duration__c from Opportunity where recordType.name='HA Builder' and Rollout_Duration__c!=null and stagename='Win' order by Createddate desc limit 1];
		opp.HA_Won_Count__c = 1;
		opp.stagename='Commit';
		opp.Rollout_Duration__c = opp.Rollout_Duration__c+1;
		update opp;
	}

    @isTest(SeeAllData=true)
    static void HA_Shipping_Plant_Insert_Test() {
            Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        Account testAccount1 = new Account (
          Name = 'TestAcc1',
            RecordTypeId = rtMap.get('Account' + 'HA_Builder'),
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        
        );
        insert testAccount1;
        
        Opportunity testOpp1 = new Opportunity (
          Name = 'TestOpp1',
          RecordTypeId = rtMap.get('Opportunity' + 'HA_Builder'),
          Project_Type__c = 'SFNC',
          Project_Name__c = 'TestOpp1',
          AccountId = testAccount1.Id,
          StageName = 'Identified',
          CloseDate = Date.today(),
          Roll_Out_Start__c = Date.today(),
          Number_of_Living_Units__c = 10000,
          Rollout_Duration__c = 5,
		  Shipment_State__c = 'AZ'
        );
        insert testOpp1;
     //    System.assertEquals('S357',testOpp1.HA_Shipping_Plant__c);
        
        
    	Opportunity testOpp2 = new Opportunity (
          Name = 'TestOpp2',
          RecordTypeId = rtMap.get('Opportunity' + 'HA_Builder'),
          Project_Type__c = 'SFNC',
          Project_Name__c = 'TestOpp2',
          AccountId = testAccount1.Id,
          StageName = 'Identified',
          CloseDate = Date.today(),
          Roll_Out_Start__c = Date.today(),
          Number_of_Living_Units__c = 10000,
          Rollout_Duration__c = 5,
		  Shipment_State__c = 'IA'
        );
        insert testOpp2;
        //System.assertEquals('S358',testOpp2.HA_Shipping_Plant__c);
        
        Opportunity testOpp3 = new Opportunity (
          Name = 'TestOpp3',
          RecordTypeId = rtMap.get('Opportunity' + 'HA_Builder'),
          Project_Type__c = 'SFNC',
          Project_Name__c = 'TestOpp2',
          AccountId = testAccount1.Id,
          StageName = 'Identified',
          CloseDate = Date.today(),
          Roll_Out_Start__c = Date.today(),
          Number_of_Living_Units__c = 10000,
          Rollout_Duration__c = 5,
		  Shipment_State__c = 'AR'
        );
        insert testOpp3;
     //    System.assertEquals('S359',testOpp3.HA_Shipping_Plant__c);
        
        Opportunity testOpp4 = new Opportunity (
          Name = 'TestOpp4',
          RecordTypeId = rtMap.get('Opportunity' + 'HA_Builder'),
          Project_Type__c = 'SFNC',
          Project_Name__c = 'TestOpp5',
          AccountId = testAccount1.Id,
          StageName = 'Identified',
          CloseDate = Date.today(),
          Roll_Out_Start__c = Date.today(),
          Number_of_Living_Units__c = 10000,
          Rollout_Duration__c = 5,
		  Shipment_State__c = 'CT'
        );
        insert testOpp4;
     //   System.assertEquals('S360',testOpp4.HA_Shipping_Plant__c);
        
        Opportunity testOpp5 = new Opportunity (
          Name = 'TestOpp5',
          RecordTypeId = rtMap.get('Opportunity' + 'HA_Builder'),
          Project_Type__c = 'SFNC',
          Project_Name__c = 'TestOpp5',
          AccountId = testAccount1.Id,
          StageName = 'Identified',
          CloseDate = Date.today(),
          Roll_Out_Start__c = Date.today(),
          Number_of_Living_Units__c = 10000,
          Rollout_Duration__c = 5,
		  Shipment_State__c = 'OR'
        );
        insert testOpp5;
    //    System.assertEquals('S360',testOpp5.HA_Shipping_Plant__c);
        
        Opportunity testOpp6 = new Opportunity (
          Name = 'TestOpp6',
          RecordTypeId = rtMap.get('Opportunity' + 'HA_Builder'),
          Project_Type__c = 'SFNC',
          Project_Name__c = 'TestOpp6',
          AccountId = testAccount1.Id,
          StageName = 'Identified',
          CloseDate = Date.today(),
          Roll_Out_Start__c = Date.today(),
          Number_of_Living_Units__c = 10000,
          Rollout_Duration__c = 5,
		  Shipment_State__c = 'FL'
        );
        insert testOpp6;
   //     System.assertEquals('S366',testOpp6.HA_Shipping_Plant__c);
        
        Opportunity testOpp7 = new Opportunity (
          Name = 'TestOpp7',
          RecordTypeId = rtMap.get('Opportunity' + 'HA_Builder'),
          Project_Type__c = 'SFNC',
          Project_Name__c = 'TestOpp7',
          AccountId = testAccount1.Id,
          StageName = 'Identified',
          CloseDate = Date.today(),
          Roll_Out_Start__c = Date.today(),
          Number_of_Living_Units__c = 10000,
          Rollout_Duration__c = 5,
		  Shipment_State__c = 'GA'
        );
        insert testOpp7;
       // System.assertEquals('S366',testOpp7.HA_Shipping_Plant__c);
    }
    
    @isTest(SeeAllData=true)
    static void HA_Shipping_Plant_Update_Test() {
        Opportunity opp = [Select id,Shipment_State__c,HA_Shipping_Plant__c from Opportunity where recordType.name='HA Builder' LIMIT 1];
       // System.assertEquals('S357',opp.HA_Shipping_Plant__c);
        
        Opportunity opp2 = [Select id,Shipment_State__c,HA_Shipping_Plant__c from Opportunity where recordType.name='HA Builder' AND ID =:opp.Id LIMIT 1];
        opp2.Shipment_State__c = 'IA';
        update opp2;
     //   System.assertEquals('S358',opp2.HA_Shipping_Plant__c);
        
        Opportunity opp3 = [Select id,Shipment_State__c,HA_Shipping_Plant__c from Opportunity where recordType.name='HA Builder' AND ID =:opp.Id LIMIT 1];
        opp3.Shipment_State__c = 'AR';
        update opp3;
     //   System.assertEquals('S359',opp3.HA_Shipping_Plant__c);
        
        Opportunity opp4 =[Select id,Shipment_State__c,HA_Shipping_Plant__c from Opportunity where recordType.name='HA Builder' AND ID =:opp.Id LIMIT 1];
        opp4.Shipment_State__c = 'CT';
        update opp4;
        //System.assertEquals('S360',opp4.HA_Shipping_Plant__c);
        
        Opportunity opp5 = [Select id,Shipment_State__c,HA_Shipping_Plant__c from Opportunity where recordType.name='HA Builder' AND ID =:opp.Id LIMIT 1];
        opp5.Shipment_State__c = 'OR';
        update opp5;
      //  System.assertEquals('S360',opp5.HA_Shipping_Plant__c);
        
        Opportunity opp6 = [Select id,Shipment_State__c,HA_Shipping_Plant__c from Opportunity where recordType.name='HA Builder' AND ID =:opp.Id LIMIT 1];
        opp6.Shipment_State__c = 'FL';
        update opp6;
       // System.assertEquals('S366',opp6.HA_Shipping_Plant__c);
        
        Opportunity opp7 = [Select id,Shipment_State__c,HA_Shipping_Plant__c from Opportunity where recordType.name='HA Builder' AND ID =:opp.Id LIMIT 1];
        opp7.Shipment_State__c = 'GA';
        update opp7;
       // System.assertEquals('S367',opp7.HA_Shipping_Plant__c);
    }
    
    
    Static void Testmethod_four(){
        Opportunity opp = [Select id,Shipment_State__c,HA_Shipping_Plant__c from Opportunity where recordType.name='HA Builder' LIMIT 1];
    }
}