/**
 * Created by ms on 2017-11-07.
 *
 * author : JeongHo.Lee, I2MAX
 */
global without sharing class HA_RollOutDeleteBatch implements Database.Batchable<sObject>, Database.Stateful {
	public Set<Id> 		opptySet;

	global HA_RollOutDeleteBatch(Set<Id> ids) {
		opptySet = ids;
	}
	global Database.QueryLocator start(Database.BatchableContext BC) {
		String query = '';

		query += ' SELECT Id FROM Roll_Out__c ';
		//query += ' WHERE Opportunity__c IN: opptySet AND Editable__c = true';
		query += ' WHERE Id IN: opptySet AND Editable__c = true';
		return Database.getQueryLocator(query);
	
	}
	global void execute(Database.BatchableContext BC, List<Roll_Out__c> scope) {
		List<Roll_Out__c> deleteList = scope;
		if(!deleteList.isEmpty()) delete deleteList;
	}
	global void finish(Database.BatchableContext BC) {
		system.debug('## HA Rollout Delete finish');
	}
}