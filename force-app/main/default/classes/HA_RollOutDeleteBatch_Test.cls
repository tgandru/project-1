/**
 * Created by ms on 2017-11-12.
 *
 * author : JeongHo.Lee, I2MAX
 */
@isTest
private class HA_RollOutDeleteBatch_Test {
	@isTest(SeeAllData=true)
	static void DeleteTest() {

		Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        Account testAccount1 = new Account (
        	Name = 'TestAcc1',
            RecordTypeId = rtMap.get('Account' + 'HA_Builder'),
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        
        );
        insert testAccount1;

        Opportunity testOpp1 = new Opportunity (
        	Name = 'TestOpp1',
        	RecordTypeId = rtMap.get('Opportunity' + 'HA_Builder'),
        	Project_Type__c = 'SFNC',
        	Project_Name__c = 'TestOpp1',
        	AccountId = testAccount1.Id,
        	StageName = 'Identified',
        	CloseDate = Date.today(),
        	Roll_Out_Start__c = Date.newInstance(2017, 1, 2),
        	Number_of_Living_Units__c = 10000,
        	Rollout_Duration__c = 5

        );
        insert testOpp1;

        Roll_Out__c testRo = new Roll_Out__c (
        	Opportunity__c = testOpp1.Id,
        	Opportunity_RecordType_Name__c = 'HA Builder',
        	Roll_Out_Start__c = Date.newInstance(2017, 1, 2),
        	Editable__c = true
        );
        insert testRo;

        Set<Id> oppIdSet = new Set<Id>();
        //oppIdSet.add(testOpp1.Id);
        oppIdSet.add(testRo.Id);
        HA_RollOutDeleteBatch ba = new HA_RollOutDeleteBatch(oppIdSet);
        Database.executeBatch(ba, 1);
	}
}