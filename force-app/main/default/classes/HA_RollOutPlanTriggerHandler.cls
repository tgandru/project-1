/**
 * Created by ms on 2017-10-27.
 *
 * author : JeongHo.Lee, I2MAX
 */
public class HA_RollOutPlanTriggerHandler {

	public static boolean createRolloutProductCheck = FALSE;
    public static boolean updateRolloutProductCheck = FALSE;

	public static void createRolloutProduct(List<Roll_Out__c> insertedPlans) {
		if(!createRolloutProductCheck){ 
			createRolloutProductCheck = TRUE;
			System.debug('## In HA_Rollout Product create block');

			List<Roll_Out_Product__c> insertRolloutProducts = new List<Roll_Out_Product__c>();
         	Map<Id,Roll_out__c> oppIdToPlanMap = new Map<Id,Roll_out__c>();
         	Map<Id, Id> planIdToProductIdMap = new Map<Id, Id>(); 
         	Map<String, Integer> cntMap		= new Map<String, Integer>();
         	Map<String, Integer> remainCntMap = new Map<String, Integer>();
         	Map<String, String>  familyBaseMap = new Map<String, String>();

         	for(Roll_Out__c ro: insertedPlans){
            	oppIdToPlanMap.put(ro.Opportunity__c, ro);
        	}
        	/*System.debug('oppIdToPlanMap : ' + oppIdToPlanMap);*/

        	Map<String, Decimal> prType = new Map<String, Decimal>();
			for(ProjectType__c pt : [SELECT Name, Percent__c FROM ProjectType__c]){
				//ex) 'Hospitality' + ':' + 'Base'
				prType.put(pt.Name, pt.Percent__c);
			}
			system.debug('prType' + prType.size());

			//Confirm the mapping
			for(SBQQ__QuoteLine__c sb : [SELECT Id
										, SBQQ__Quote__r.SBQQ__Opportunity2__r.Name
										, SBQQ__Quote__r.SBQQ__Opportunity2__r.Project_Type__c
										, SBQQ__Quote__r.SBQQ__Opportunity2__r.Roll_Out_Plan__c
										, SBQQ__Quote__r.SBQQ__Opportunity2__r.Roll_Out_Start__c
										, SBQQ__Quote__r.SBQQ__Opportunity2__r.Rollout_Duration__c
										, SBQQ__Quote__r.SBQQ__Opportunity2__r.RecordType.Name
										, SBQQ__Quote__r.SBQQ__Opportunity2__r.Number_of_Living_Units__c
										, SBQQ__Product__c
										, SBQQ__ProductFamily__c
										, Package_Option__c
										, SBQQ__Quantity__c
										, SBQQ__ListPrice__c
										, SBQQ__NetPrice__c
										FROM SBQQ__QuoteLine__c 
										WHERE Type__c!='Dacor' and SBQQ__Quote__r.SBQQ__Primary__c=true and SBQQ__Quote__r.SBQQ__Opportunity2__c IN:oppIdToPlanMap.keySet()
										ORDER BY SBQQ__ProductFamily__c, Package_Option__c]){
				Roll_Out_Product__c rop = new Roll_Out_Product__c();

				//rop.Opportunity_Line_Item__c = oli.Id;
	            rop.Product__c = sb.SBQQ__Product__c;
	            //rop.Invoice_Price__c = SBQQ__Product__c;
	            //rop.Quote_Revenue__c = oli.TotalPrice;
	            //rop.SalesPrice__c = (oli.TotalPrice)/(oli.Quantity);
	            rop.Roll_Out_Plan__c = oppIdToPlanMap.get(sb.SBQQ__Quote__r.SBQQ__Opportunity2__c).Id;//sb.SBQQ__Quote__r.SBQQ__Opportunity2__r.Roll_Out_Plan__c;
	            rop.Roll_Out_Start__c = oppIdToPlanMap.get(sb.SBQQ__Quote__r.SBQQ__Opportunity2__c).Roll_Out_Start__c;//sb.SBQQ__Quote__r.SBQQ__Opportunity2__r.Roll_Out_Start__c;
	            rop.Rollout_Duration__c = oppIdToPlanMap.get(sb.SBQQ__Quote__r.SBQQ__Opportunity2__c).Rollout_Duration__c;//sb.SBQQ__Quote__r.SBQQ__Opportunity2__r.Rollout_Duration__c;
	            rop.Opportunity_RecordType_Name__c = sb.SBQQ__Quote__r.SBQQ__Opportunity2__r.RecordType.Name;
	            rop.Product_Family__c = sb.SBQQ__ProductFamily__c;
	            rop.Package_Option__c = sb.Package_Option__c;
	            rop.Project_Type__c = sb.SBQQ__Quote__r.SBQQ__Opportunity2__r.Project_Type__c;
	            rop.SBQQ_QuoteLine__c = sb.Id;
	            rop.Quote_Quantity__c = sb.SBQQ__Quantity__c;
	            if(rop.Package_Option__c == 'Option' || rop.Package_Option__c == 'Upgrade') rop.Quote_Quantity__c =sb.SBQQ__Quote__r.SBQQ__Opportunity2__r.Number_of_Living_Units__c;

	            if(prType.get(rop.Project_Type__c +':'+rop.Package_Option__c) != null && rop.Package_Option__c == 'Base'){

	            	familyBaseMap.put(sb.SBQQ__ProductFamily__c, sb.SBQQ__ProductFamily__c);
			            
		            rop.Roll_Out_Percent__c = prType.get(rop.Project_Type__c +':'+rop.Package_Option__c);
		            //if(rop.Roll_Out_Percent__c == null) rop.Roll_Out_Percent__c = 100.00;
		            rop.Quote_Quantity__c = Math.Round(sb.SBQQ__Quantity__c *(rop.Roll_Out_Percent__c/100.00));
		        }
		        if(rop.Package_Option__c == 'Option' || rop.Package_Option__c == 'Upgrade'){

		        	if(rop.Product_Family__c != null && cntMap.containsKey(rop.Product_Family__c)){
		        		cntMap.put(rop.Product_Family__c, Integer.valueOf(cntMap.get(rop.Product_Family__c)+1));
		        	}
		        	if(rop.Product_Family__c != null && !cntMap.containsKey(rop.Product_Family__c)){
		        		cntMap.put(rop.Product_Family__c, 1);
		        	}
		        }
		        rop.SalesPrice__c = sb.SBQQ__NetPrice__c;
		        insertRolloutProducts.add(rop);
			}

	        for(Roll_Out_Product__c rop : insertRolloutProducts){
	        	if(rop.Product_Family__c != null && rop.Package_Option__c != null &&rop.Package_Option__c != 'Base' && cntMap.get(rop.Product_Family__c) != null){

	        		if(remainCntMap.containsKey(rop.Product_Family__c)){
        				remainCntMap.put(rop.Product_Family__c, Integer.valueOf(remainCntMap.get(rop.Product_Family__c)+1));
					}
					if(!remainCntMap.containsKey(rop.Product_Family__c)){
						remainCntMap.put(rop.Product_Family__c, 1);
					}
	        		
	        		if(prType.get(rop.Project_Type__c+':Base') != null && familyBaseMap.get(rop.Product_Family__c) != null){
	        			rop.Roll_Out_Percent__c = Math.floor((100.00 - prType.get(rop.Project_Type__c+':Base')) / cntMap.get(rop.Product_Family__c));
	        			if(cntMap.get(rop.Product_Family__c) == remainCntMap.get(rop.Product_Family__c) && Math.mod((100 - Integer.valueOf(prType.get(rop.Project_Type__c+':Base'))), cntMap.get(rop.Product_Family__c)) != 0)
	        			rop.Roll_Out_Percent__c = rop.Roll_Out_Percent__c + (Math.mod(100 - Integer.valueOf(prType.get(rop.Project_Type__c+':Base')), cntMap.get(rop.Product_Family__c)));
	        		}
	        		else{
	        			rop.Roll_Out_Percent__c = Math.floor(100.00 / cntMap.get(rop.Product_Family__c));
	        			if(cntMap.get(rop.Product_Family__c) == remainCntMap.get(rop.Product_Family__c) && Math.mod(100, cntMap.get(rop.Product_Family__c)) != 0)
	        				rop.Roll_Out_Percent__c = rop.Roll_Out_Percent__c + Math.mod(100, cntMap.get(rop.Product_Family__c));
	        		}
	        		if(rop.Roll_Out_Percent__c == null) rop.Roll_Out_Percent__c = 100.00;
	        		rop.Quote_Quantity__c = Math.Round(rop.Quote_Quantity__c *(rop.Roll_Out_Percent__c/100.00));
	        	}
	        	rop.Quote_Revenue__c = rop.Quote_Quantity__c * rop.SalesPrice__c;
	        }
			try{
				if(insertRolloutProducts.size()>0) insert insertRolloutProducts;
			}
			catch(Exception e){
            	system.debug('## insertHA_RolloutProducts'+e);
			}
     	}
	}
	/*public static void updateRolloutProduct(List<Roll_Out__c> updatedPlans, Map<Id, Roll_Out__c> oldMap) {
		if(!updateRolloutProductCheck){
			updateRolloutProductCheck = TRUE;
			System.debug('## In HA_Rollout Product update block');
			
			List<Roll_Out_Product__c> updateRolloutProducts = new List<Roll_Out_Product__c>();
        	Map<Id,Roll_out__c> roIdToPlanMap = new Map<Id,Roll_out__c>();
        	Map<Id, Id> planIdToProductIdMap = new Map<Id, Id>();

        	for(Roll_Out__c ro:updatedPlans) {
            	if(ro.Roll_Out_Start__c !=oldMap.get(ro.Id).Roll_Out_Start__c || ro.Rollout_Duration__c !=oldMap.get(ro.Id).Rollout_Duration__c)
                	roIdToPlanMap.put(ro.Id, ro);
        	}

        	for(Roll_Out_Product__c rop: [select Id, Roll_Out_Plan__c, Roll_Out_Start__c, Roll_Out_End__c,Rollout_Duration__c, Do_Not_Edit__c from Roll_Out_Product__c where Roll_Out_Plan__c IN: roIdToPlanMap.keySet()]){
            
	            if(rop.Do_Not_Edit__c){
	                rop.Rollout_Duration__c = roIdToPlanMap.get(rop.Roll_Out_Plan__c).Rollout_Duration__c;
	            
	            }
	            else {
	                rop.Roll_Out_Start__c = roIdToPlanMap.get(rop.Roll_Out_Plan__c).Roll_Out_Start__c;
	                rop.Rollout_Duration__c = roIdToPlanMap.get(rop.Roll_Out_Plan__c).Rollout_Duration__c;
	            }
	            updateRolloutProducts.add(rop);
	        }

	        try{
	            if(updateRolloutProducts.size()>0)
	             	update updateRolloutProducts;
	        }
	        catch(Exception e){
	            system.debug('## Inside updateRolloutProducts'+ e);
	        }
    	}
	}*/
}