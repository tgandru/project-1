/**
 * Created by ms on 2017-11-12.
 *
 * author : JeongHo.Lee, I2MAX
 */
 @isTest
private class HA_RollOutPlanTriggerHandler_Test {

	@isTest(SeeAllData=true)
	static void CreateTest() {
		Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }
        Account testAccount1 = new Account (
        	Name = 'TestAcc1',
            RecordTypeId = rtMap.get('Account' + 'HA_Builder'),
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        
        );
        insert testAccount1;

        Opportunity testOpp1 = new Opportunity (
        	Name = 'TestOpp1',
        	RecordTypeId = rtMap.get('Opportunity' + 'HA_Builder'),
        	Project_Type__c = 'SFNC',
        	Project_Name__c = 'TestOpp1',
        	AccountId = testAccount1.Id,
        	StageName = 'Identification',
        	CloseDate = Date.today(),
        	Roll_Out_Start__c = Date.newInstance(2017, 1, 2),
        	Number_of_Living_Units__c = 10000,
        	Rollout_Duration__c = 5

        );
        insert testOpp1;

        SBQQ__Quote__c sbq1 = new SBQQ__Quote__c (
        	SBQQ__Primary__c = TRUE,
        	SBQQ__Opportunity2__c = testOpp1.Id,
        	SBQQ__Account__c = testAccount1.Id,
        	SBQQ__Type__c = 'Quote',
        	SBQQ__Status__c = 'Draft',
        	RecordTypeId = rtMap.get('SBQQ__Quote__c'+'HA_Builder')
        );
        insert sbq1;

        List<Product2> prodList = [SELECT Id FROM Product2 WHERE Family =: 'DISH' LIMIT 5];

        List<SBQQ__QuoteLine__c> sbqList = new List<SBQQ__QuoteLine__c>();

        for(integer i = 0; i < 5; i++){
        	SBQQ__QuoteLine__c sbq = new SBQQ__QuoteLine__c();
        	sbq.SBQQ__Product__c = prodList[i].Id;
        	if(i ==0) sbq.Package_Option__c = 'Base';
        	else sbq.Package_Option__c = 'Option';
        	sbq.SBQQ__Quantity__c = 50;
        	sbq.SBQQ__Quote__c = sbq1.Id;
        	sbq.SBQQ__ListPrice__c = 5000;
        	sbqList.add(sbq);
        }
        insert sbqList;

        Roll_Out__c testRo = new Roll_Out__c (
        	Opportunity__c = testOpp1.Id,
        	Opportunity_RecordType_Name__c = 'HA Builder',
        	Roll_Out_Start__c = Date.today(),
        	Quote__c = sbq1.Id
        );
        insert testRo;
	}
}