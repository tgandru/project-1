/**
 * Created by ms on 2017-11-12.
 *
 * author : JeongHo.Lee, I2MAX
 */
@isTest
private class HA_RollOutPlan_Test {
	@isTest(seeAlldata=true)
	static void CreateTest() {

		/*Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }*/
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        
        Id HARecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HA Builder').getRecordTypeId();
        Id OppHARecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('HA Builder').getRecordTypeId();
        Id QuoteHARecordTypeId = Schema.SObjectType.SBQQ__Quote__c.getRecordTypeInfosByName().get('HA Builder').getRecordTypeId();
        
        Account testAccount1 = new Account (
        	Name = 'TestAcc1',
            //RecordTypeId = rtMap.get('Account' + 'HA_Builder'),
            RecordTypeId = HARecordTypeId,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        
        );
        insert testAccount1;
    
        Opportunity testOpp1 = new Opportunity (
        	Name = 'TestOpp1',
        	//RecordTypeId = rtMap.get('Opportunity' + 'HA_Builder'),
        	RecordTypeId = OppHARecordTypeId,
        	Project_Type__c = 'SFNC',
        	Project_Name__c = 'TestOpp1',
        	AccountId = testAccount1.Id,
        	StageName = 'Identified',
        	CloseDate = Date.today(),
        	Roll_Out_Start__c = Date.newInstance(2019, 1, 2),
        	Number_of_Living_Units__c = 10000,
        	Rollout_Duration__c = 1

        );
        insert testOpp1;

        SBQQ__Quote__c sbq1 = new SBQQ__Quote__c (
        	SBQQ__Primary__c = TRUE,
        	SBQQ__Opportunity2__c = testOpp1.Id,
        	SBQQ__Account__c = testAccount1.Id,
        	SBQQ__Type__c = 'Quote',
        	SBQQ__Status__c = 'Draft',
            Expected_Shipment_Date__c = Date.today(),
            Last_Shipment_Date__c = Date.today(),
        	//RecordTypeId = rtMap.get('SBQQ__Quote__c'+'HA_Builder')
        	RecordTypeId = QuoteHARecordTypeId
        );
        insert sbq1;
        
        List<Product2> prodInsert = new List<Product2>();
        product2 prod1 = new product2(Name='DB3710DB1',SAP_Material_ID__c='DB3710DB1',ProductCode='DB3710DB1',Family='DISH',SAP_SW_Product_Code__c='AEE0A');
        product2 prod2 = new product2(Name='DB3710DB2',SAP_Material_ID__c='DB3710DB2',ProductCode='DB3710DB2',Family='DISH',SAP_SW_Product_Code__c='AEE0A');
        product2 prod3 = new product2(Name='DB3710DB3',SAP_Material_ID__c='DB3710DB3',ProductCode='DB3710DB3',Family='DISH',SAP_SW_Product_Code__c='AEE0A');
        product2 prod4 = new product2(Name='DB3710DB4',SAP_Material_ID__c='DB3710DB4',ProductCode='DB3710DB4',Family='DISH',SAP_SW_Product_Code__c='AEE0A');
        product2 prod5 = new product2(Name='DB3710DB5',SAP_Material_ID__c='DB3710DB5',ProductCode='DB3710DB5',Family='DISH',SAP_SW_Product_Code__c='AEE0A');
        prodInsert.add(prod1);
        prodInsert.add(prod2);
        prodInsert.add(prod3);
        prodInsert.add(prod4);
        prodInsert.add(prod5);
        insert prodInsert;

        //List<Product2> prodList = [SELECT Id FROM Product2 WHERE Family =: 'DISH' LIMIT 5];

        List<SBQQ__QuoteLine__c> sbqList = new List<SBQQ__QuoteLine__c>();

        for(integer i = 0; i < 5; i++){
        	SBQQ__QuoteLine__c sbq = new SBQQ__QuoteLine__c();
        	//sbq.SBQQ__Product__c = prodList[i].Id;
        	sbq.SBQQ__Product__c = prodInsert[i].Id;
        	if(i ==0) sbq.Package_Option__c = 'Base';
        	else sbq.Package_Option__c = 'Option';
        	sbq.SBQQ__Quantity__c = 50;
        	sbq.SBQQ__Quote__c = sbq1.Id;
        	sbq.SBQQ__ListPrice__c = 5000;
            sbq.SBQQ__NetPrice__c = 5000;
        	sbqList.add(sbq);
        }
        insert sbqList;

        Roll_Out__c testRo = new Roll_Out__c (
        	Opportunity__c = testOpp1.Id,
        	Opportunity_RecordType_Name__c = 'HA Builder',
        	Roll_Out_Start__c = Date.newInstance(2019, 1, 2),
        	Quote__c = sbq1.Id
        );
        insert testRo;

        List<Roll_Out_Product__c> ropList = new List<Roll_Out_Product__c>();
        for(Integer i = 0; i< 5; i++){
            Roll_Out_Product__c rop = new Roll_Out_Product__c();
        	rop.Name = 'testProduct';
        	rop.Roll_Out_Plan__c = testRo.Id;
        	rop.Opportunity_RecordType_Name__c = 'HA Builder';
        	//rop.Product__c = prodList[i].Id;
        	rop.Product__c = prodInsert[i].Id;
        	rop.Roll_Out_Start__c = Date.newInstance(2019, 1, 2);
        	rop.Rollout_Duration__c = 1;
        	
        	if(i == 0){
                rop.Package_Option__c = 'Base';
                rop.Roll_Out_Percent__c = 100;
            }
            else{
                rop.Package_Option__c = 'Option';
                rop.Roll_Out_Percent__c = 10;    
            } 
        	rop.Quote_Quantity__c = 50;

            ropList.add(rop);
        }
        insert ropList;
        Map<Id, Roll_Out_Product__c> ropMap = new Map<Id, Roll_Out_Product__c>();
        for(Roll_Out_Product__c rop : ropList){
            ropMap.put(rop.Id, rop);
        }
        
        List<Roll_Out_Schedule__c> insertRollOutSchedules = new List<Roll_Out_Schedule__c>();
            
        //call uility method to create ROS: HA_RosMiscUtilities
        for(Roll_Out_Schedule__c s: HA_RosMiscUtilities.createROS(ropMap)){
            insertRollOutSchedules.add(s);
        }

        insert insertRollOutSchedules;

        ApexPages.currentPage().getParameters().put('id', testRo.Id);
        HA_RollOutPlan ha = new HA_RollOutPlan();
        
        ha.doInit();
        ha.doNext();
        ha.doPrevious();
        ha.doDisplayQty();
        ha.doDisplayRev();
        ha.doDisplayAll();
        ha.doCancel();
        ha.category = 'DISH';
        ha.doSearch();
        ha.doEditMode();

        ha.doInit();
        ha.category = 'ACCESSARY';
        ha.doSearch();
        ha.doSave();
        ha.doAllocate();

        ha.doEditHeaderMode();
        ha.dummyMethodForTestClass();

	}
    @isTest(seeAlldata=true)
    static void UpdateTest_1() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        /*Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }*/
        Id HARecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HA Builder').getRecordTypeId();
        Id OppHARecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('HA Builder').getRecordTypeId();
        Id QuoteHARecordTypeId = Schema.SObjectType.SBQQ__Quote__c.getRecordTypeInfosByName().get('HA Builder').getRecordTypeId();

        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            //RecordTypeId = rtMap.get('Account' + 'HA_Builder'),
            RecordTypeId = HARecordTypeId,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        
        );
        insert testAccount1;

        Opportunity testOpp1 = new Opportunity (
            Name = 'TestOpp1',
            //RecordTypeId = rtMap.get('Opportunity' + 'HA_Builder'),
            RecordTypeId = OppHARecordTypeId,
            Project_Type__c = 'SFNC',
            Project_Name__c = 'TestOpp1',
            AccountId = testAccount1.Id,
            StageName = 'Identified',
            CloseDate = Date.today(),
            Roll_Out_Start__c = Date.today(),
            Number_of_Living_Units__c = 10000,
            Rollout_Duration__c = 1

        );
        insert testOpp1;

        SBQQ__Quote__c sbq1 = new SBQQ__Quote__c (
            SBQQ__Primary__c = TRUE,
            SBQQ__Opportunity2__c = testOpp1.Id,
            SBQQ__Account__c = testAccount1.Id,
            SBQQ__Type__c = 'Quote',
            SBQQ__Status__c = 'Draft',
            Expected_Shipment_Date__c = Date.today(),
            Last_Shipment_Date__c = Date.today(),
            //RecordTypeId = rtMap.get('SBQQ__Quote__c'+'HA_Builder')
            RecordTypeId = QuoteHARecordTypeId
        );
        insert sbq1;
        
        List<Product2> prodInsert = new List<Product2>();
        product2 prod1 = new product2(Name='DB3710DB1',SAP_Material_ID__c='DB3710DB1',ProductCode='DB3710DB1',Family='DISH',SAP_SW_Product_Code__c='AEE0A');
        product2 prod2 = new product2(Name='DB3710DB2',SAP_Material_ID__c='DB3710DB2',ProductCode='DB3710DB2',Family='DISH',SAP_SW_Product_Code__c='AEE0A');
        product2 prod3 = new product2(Name='DB3710DB3',SAP_Material_ID__c='DB3710DB3',ProductCode='DB3710DB3',Family='DISH',SAP_SW_Product_Code__c='AEE0A');
        product2 prod4 = new product2(Name='DB3710DB4',SAP_Material_ID__c='DB3710DB4',ProductCode='DB3710DB4',Family='DISH',SAP_SW_Product_Code__c='AEE0A');
        product2 prod5 = new product2(Name='DB3710DB5',SAP_Material_ID__c='DB3710DB5',ProductCode='DB3710DB5',Family='DISH',SAP_SW_Product_Code__c='AEE0A');
        prodInsert.add(prod1);
        prodInsert.add(prod2);
        prodInsert.add(prod3);
        prodInsert.add(prod4);
        prodInsert.add(prod5);
        insert prodInsert;

        //List<Product2> prodList = [SELECT Id, Family FROM Product2 WHERE Family =: 'DISH' LIMIT 5];

        List<SBQQ__QuoteLine__c> sbqList = new List<SBQQ__QuoteLine__c>();

        for(integer i = 0; i < 5; i++){

            SBQQ__QuoteLine__c sbq = new SBQQ__QuoteLine__c();
            //sbq.SBQQ__Product__c = prodList[i].Id;
        	sbq.SBQQ__Product__c = prodInsert[i].Id;
            sbq.Package_Option__c = 'Base';
            sbq.SBQQ__Quantity__c = 50;
            sbq.SBQQ__Quote__c = sbq1.Id;
            sbq.SBQQ__ListPrice__c = 5000;
            sbq.SBQQ__NetPrice__c = 5000;
            sbqList.add(sbq);
        }
        insert sbqList;

        Roll_Out__c testRo = new Roll_Out__c (
            Opportunity__c = testOpp1.Id,
            Opportunity_RecordType_Name__c = 'HA Builder',
            Roll_Out_Start__c = Date.today(),
            Quote__c = sbq1.Id,
            Roll_Out_Status__c = 'Not Started',
            Editable__c = true
        );
        insert testRo;

        List<Roll_Out_Product__c> ropList = new List<Roll_Out_Product__c>();
        for(Integer i = 0; i< 5; i++){
            Roll_Out_Product__c rop = new Roll_Out_Product__c();
            rop.Name = 'testProduct';
            rop.Roll_Out_Plan__c = testRo.Id;
            rop.Opportunity_RecordType_Name__c = 'HA Builder';
            rop.Product__c = prodInsert[i].Id;
            rop.Roll_Out_Start__c = Date.today();
            rop.Rollout_Duration__c = 1;
            rop.SBQQ_QuoteLine__c = sbqList[i].Id;
            rop.SalesPrice__c = 5000;            
            rop.Package_Option__c = 'Base';
            rop.Roll_Out_Percent__c = 100.00;
            rop.Allocate__c = true;
            rop.Quote_Quantity__c = 50;
            rop.Product_Family__c = 'DISH';

            ropList.add(rop);
        }
        insert ropList;

        Map<Id, Roll_Out_Product__c> ropMap = new Map<Id, Roll_Out_Product__c>();
        for(Roll_Out_Product__c rop : ropList){
            ropMap.put(rop.Id, rop);
        }
        
        List<Roll_Out_Schedule__c> insertRollOutSchedules = new List<Roll_Out_Schedule__c>();
            
        //call uility method to create ROS: HA_RosMiscUtilities
        for(Roll_Out_Schedule__c s: HA_RosMiscUtilities.createROS(ropMap)){
            insertRollOutSchedules.add(s);
        }

        insert insertRollOutSchedules;
        
        test.startTest();
        
        ApexPages.currentPage().getParameters().put('id', testRo.Id);
        HA_RollOutPlan ha = new HA_RollOutPlan();
        List<selectOption> temp = HA_RollOutPlan.getItems();

        ha.rolloutId = testRo.Id;
        //ha.category = 'DISH';
        //ha.doInit();
        ha.startDate = Date.today();
        ha.doSearch();
        ha.doEditMode();
        
        testOpp1.StageName = 'Commit';
        update testOpp1;

        ha.rolloutId = testRo.Id;
        ha.doInit();
        ha.startDate = Date.today();
        ha.doEditMode();
        ha.doSave();

        ApexPages.currentPage().getParameters().put('id', testRo.Id);
        HA_RollOutPlan ha2 = new HA_RollOutPlan();
        ha2.rolloutId = testRo.Id;
        //ha2.doInit();       
        ha2.doEditMode();
        
        ha2.dataList[0].isAllocate = true;
        ha2.dataList[1].isAllocate = true;
        ha2.dataList[2].isAllocate = true;
        ha2.dataList[3].isAllocate = true;
        ha2.dataList[4].isAllocate = true;
        ha2.doAllocate();
        
        test.stopTest();
    }
}