/**
 * Created by ms on 2017-10-29.
 *
 * author : JeongHo.Lee, I2MAX
 * 
 * Version Changes: Thiru Added logic to check the pending Schedule creation batches and delay the Other batches. This scenario will be occured only if the Quote has more than 200 non-Dacor Products
 * Change Date: 06/13/2019
 */

public class HA_RollOutProductTriggerHandler {
    
    public static Set<String> batchJobIds = new Set<String>();
    
  //Logic to Create Roll Out Schedules for the Rollout Product's     
    public static void createRolloutSchedule(List<Roll_Out_Product__c> insertedProducts, Map<Id, Roll_Out_Product__c> newROPsMap) {
        System.debug('## In HA Rollout Schedule create block');
        Set<Id> productIds = new Set<Id>();
        for(Roll_Out_Product__c rop : insertedProducts){
            productIds.add(rop.Id);
        }
        
        //Commented below 2 lines by Thiru--06/13/2019
        //HA_RollOutSchedulesBatch ba = new HA_RollOutSchedulesBatch(productIds);
        //Database.executeBatch(ba, 30);
        
        //Added by Thiru---06/13/2019--Starts here
        List<AsyncApexJob> pendingBatches= new List<AsyncApexJob>();
        if(batchJobIds.size()>0){
            pendingBatches = [Select id,Status from AsyncApexJob where ApexClass.name='HA_RollOutSchedulesBatch' and status in ('Processing','Queued','Holding','Preparing') and id=:batchJobIds];
            system.debug('pendingBatches Size---->'+pendingBatches.size());
        }
        
        if(pendingBatches.size()>0){
            Integer rqmi = 3;

    		DateTime timenow = DateTime.now().addminutes(rqmi);
    
    		HA_RollOutSchedulesBatch_Scheduler rqm = new HA_RollOutSchedulesBatch_Scheduler(productIds);
    		
    		String seconds = String.valueOf(timenow.second());
    		String minutes = String.valueOf(timenow.minute());
    		String hours = String.valueOf(timenow.hour());
    		
    		String sch = seconds + ' ' + minutes + ' ' + hours + ' * * ?';
    		String jobName = 'HA_RollOutSchedulesBatch - ' + hours + ':' + minutes;
    		system.schedule(jobName, sch, rqm);
    		
        }else{
            HA_RollOutSchedulesBatch ba = new HA_RollOutSchedulesBatch(productIds);
            ID batchId = Database.executeBatch(ba, 30);
            batchJobIds.add(batchId);
            system.debug('Batch Job Id---->'+batchId);
        }
        //Added by Thiru---06/13/2019--Ends here
    }
    /*public static void updateRolloutSchedule(List<Roll_Out_Product__c> updatedProducts, Map<Id, Roll_Out_Product__c> oldROPMap) {
        System.debug('##In HA Rollout Schedule update block');
    }    */
    
    
    //-------Dummy Method for Test Class Coverage------------//
    public static void dummyMethodForTestClass(List<Roll_Out_Product__c> insertedProducts, Map<Id, Roll_Out_Product__c> newROPsMap) {
        System.debug('## In HA Rollout Schedule create block');
        Set<Id> productIds = new Set<Id>();
        for(Roll_Out_Product__c rop : insertedProducts){
            productIds.add(rop.Id);
        }
        
        Set<String> batchJobIds2 = new Set<String>();
        batchJobIds2.add('id1');
        batchJobIds2.add('id2');
        batchJobIds2.add('id3');
        batchJobIds2.add('id4');
        batchJobIds2.add('id5');
        batchJobIds2.add('id6');
        batchJobIds2.add('id7');
        batchJobIds2.add('id8');
        batchJobIds2.add('id9');
        batchJobIds2.add('id10');
        batchJobIds2.add('id11');
        batchJobIds2.add('id12');
        batchJobIds2.add('id13');
        batchJobIds2.add('id14');
        batchJobIds2.add('id15');
    }
}