/**
 * Created by ms on 2017-11-06.
 *
 * author : JeongHo.Lee, I2MAX
 */
/*
Version Changes:
05/30/2019:::: "Thiru" Added logic to update Rollout Actuals up on Rollout Reset.
*/
global without sharing class HA_RollOutSchedulesBatch implements Database.Batchable<sObject>, Database.Stateful {
    public Set<Id>      productIds;
    public String       allfields;
    public Set<Id>      planIds;
    public List<DataWrapper> dataList;

    global HA_RollOutSchedulesBatch(Set<Id> ids) {
        dataList = new List<DataWrapper>();
        productIds = ids;
        planIds = new Set<Id>();
        
        Map<String, Schema.SObjectField> fields = Schema.getGlobalDescribe().get('Roll_Out_Product__c').getDescribe().SObjectType.getDescribe().fields.getMap();
  
        List<String> accessiblefields = new List<String>();
        for(Schema.SObjectField field : fields.values()){
            if(field.getDescribe().isAccessible())
                accessiblefields.add(field.getDescribe().getName());
        }
        allfields='';
  
        for(String fieldname : accessiblefields){
            if(fieldname!='LastViewedDate' && fieldname!='LastReferencedDate'){//Added by Thiru to removed LastViewedDate and LastReferencedDate from Query--10/31/2018.
                allfields += fieldname+',';
            }
        }
        allfields = allfields.subString(0,allfields.length()-1);

    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = '';

        query += ' SELECT ' + allfields + ' FROM Roll_Out_Product__c ';
        query += ' WHERE Id IN: productIds ';

        system.debug('## HA Rolloutschedule query : ' + query);

        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Roll_Out_Product__c> scope) {

        Map<Id, Roll_Out_Product__c> ropMap = new Map<Id, Roll_Out_Product__c>();


        for(Roll_Out_Product__c rop : scope){
            ropMap.put(rop.Id, rop);
            planIds.add(rop.Roll_Out_Plan__c);
        }

        List<Roll_Out_Schedule__c> insertRollOutSchedules = new List<Roll_Out_Schedule__c>();
        
        //call uility method to create ROS: HA_RosMiscUtilities
        for(Roll_Out_Schedule__c s: HA_RosMiscUtilities.createROS(ropMap)){
            insertRollOutSchedules.add(s) ;
        }
        try {
            if(insertRollOutSchedules.size()>0)
            insert insertRollOutSchedules;   
        }
        catch(Exception ex){
            System.debug('##ex inside New HA ROS Insert ::'+ex);
        }

    }
    global void finish(Database.BatchableContext BC) {
        system.debug('## HA RollOutScheduleBatch Finish');
        Set<id> opptyIds = new Set<id>();//Added by Thiru--05/30/2019
        List<Roll_Out__c> roList = [SELECT Id, Editable__c, Opportunity__c, Opportunity__r.Owner.Email, Opportunity__r.Name FROM Roll_Out__c WHERE Id IN: planIds];
        for(Roll_Out__c ro : roList){
            ro.Editable__c = true;
            DataWrapper dw = new DataWrapper();
            dw.userEmail = ro.Opportunity__r.Owner.Email;
            //dw.userEmail = 't.gandru@partner.samsung.com';
            dw.opptyName = ro.Opportunity__r.Name;
            dw.rolloutId = ro.Id;
            dataList.add(dw);
            
            opptyIds.add(ro.Opportunity__c);//Added by Thiru--05/30/2019
        }
        if(!roList.isEmpty()){
            update roList;
            if(!HA_Email_Notification__c.getInstance(UserInfo.getUserId()).Disable_Email_Notification__c) sendEmail(dataList);
        }
        
        Database.executeBatch(new BatchforHARolloutActualsUpdate_V2(opptyIds),100);//Added by Thiru--05/30/2019
    }
    public class DataWrapper{
        public String userEmail;
        public String opptyName;
        public String rolloutId;
    }
    public static void sendEmail(List<DataWrapper> dtList) {
        //if(Label.HA_Send_Rollout_Creation_Email=='True'){
            String customlink = Url.getSalesforceBaseUrl().toExternalForm();
            List<Messaging.SingleEmailMessage> mails =  new List<Messaging.SingleEmailMessage>();
            for(DataWrapper dw : dtList){
                
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    
                List<String> to = new List<String>();
                to.add(dw.userEmail);
    
                mail.setToAddresses(to); // List<String> param1
    
                String supportEmailCCAddress = Label.CC_Email_address_for_SAM_Notification;
                List<String> supportEmailCCAddressList = new List<String>();
    
                if(String.isNotBlank(supportEmailCCAddress))
                    supportEmailCCAddressList = supportEmailCCAddress.split(',');
    
                if (supportEmailCCAddressList.isEmpty() == false) {
                    mail.setCcAddresses(supportEmailCCAddressList);
                }
    
                String disPlayName = 'HA Sales Support';
                String subject = dw.opptyName + ' Rollout Created.';
                mail.setReplyTo(dw.userEmail);
                mail.setSenderDisplayName(disPlayName);
                mail.setSubject(subject);
    
                String body ='';
                body += 'Dear User,  Rollout Created. \n\n';
                body += 'Opportunity Name : '+ dw.opptyName +'\n\n';
                body += 'Click on The link below to see Rollout Detail.\n\n';
                body +=  customlink +'/'+ dw.rolloutId +'\n\n';
                body += 'Thank you! \n\n';
                body += 'Samsung Business Support.\n\n';
    
                mail.setPlainTextBody(body);
    
                mails.add(mail);
            }
            Messaging.sendEmail(mails);
        //}
    }
}