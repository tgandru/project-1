/**
 * Created by ms on 2017-11-12.
 *
 * author : JeongHo.Lee, I2MAX
 */
@isTest
public class HA_RollOutSchedulesBatch_MonthChangeTest {
    
	@isTest(SeeAllData=true)
	static void CreateTest() {
        Opportunity opp = [Select id,Rollout_Duration__c from Opportunity where recordType.name='HA Builder' and Rollout_Duration__c!=null and stagename='Win' order by Createddate desc limit 1];
		opp.HA_Won_Count__c = 1;
		opp.stagename='Commit';
		opp.Rollout_Duration__c = opp.Rollout_Duration__c+1;
		update opp;
	}
}