/**
 * Created by Thiru on 07-16-2019.
 *
 * author : Thiru
 */

global without sharing class HA_RollOutSchedulesBatch_MonthsChange implements Database.Batchable<sObject>, Database.Stateful {
    public String       allfields;
    public Set<Id>      planIds;
    public List<DataWrapper> dataList;
    public Set<Id>      oppIds;
    public Map<Id,Opportunity> newOppMap;
    public Map<Id,Opportunity> oldOppMap;

    global HA_RollOutSchedulesBatch_MonthsChange(List<Opportunity> triggerNew, List<Opportunity> triggerOld) {
        dataList = new List<DataWrapper>();
        planIds = new Set<Id>();
        
        Map<String, Schema.SObjectField> fields = Schema.getGlobalDescribe().get('Roll_Out_Product__c').getDescribe().SObjectType.getDescribe().fields.getMap();
  
        List<String> accessiblefields = new List<String>();
        for(Schema.SObjectField field : fields.values()){
            if(field.getDescribe().isAccessible())
                accessiblefields.add(field.getDescribe().getName());
        }
        allfields='';
  
        for(String fieldname : accessiblefields){
            if(fieldname!='LastViewedDate' && fieldname!='LastReferencedDate'){
                allfields += fieldname+',';
            }
        }
        allfields = allfields.subString(0,allfields.length()-1);
        
        newOppMap =  new Map<Id,Opportunity>(triggerNew);
        oldOppMap =  new Map<Id,Opportunity>(triggerOld);
        oppIds = new Set<Id>();
        
        oppIds.addAll(newOppMap.keySet()); 

    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = '';

        query += ' SELECT Roll_Out_Plan__r.Opportunity__c,' + allfields + ',(SELECT Id,Plan_Date__c from Roll_Out_Schedules__r order by Plan_Date__c desc LIMIT 1) FROM Roll_Out_Product__c ';
        query += ' WHERE Roll_Out_Plan__r.Opportunity__c=:oppIds';

        system.debug('## HA Rollout Product query : ' + query);

        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Roll_Out_Product__c> scope) {

        Map<Id, Roll_Out_Product__c> ropMap = new Map<Id, Roll_Out_Product__c>();
        Map<Date,SCM_Calendar__c> fiscalWeekMap = new Map<Date,SCM_Calendar__c>();
        Map<Date,SCM_Calendar__c> OrderedfiscalWeekMap = new Map<Date,SCM_Calendar__c>();
        Map<Id,Integer> ropIdToFiscalWeekMap = new Map<Id,Integer>();
        List<Roll_Out_Schedule__c> insertRollOutSchedules = new List<Roll_Out_Schedule__c>();
        List<Roll_Out_Product__c> updateRollOutProducts = new List<Roll_Out_Product__c>();
        
        //List to sort the dates by fiscal week begin date
        List<Date> sortLst = new List<Date>();
        //Loop through all the fiscal weeks (2007 to 2040 and store it in map
        for(SCM_Calendar__c fc: HA_RosMiscUtilities.lstFiscCal)
            fiscalWeekMap.put(fc.WeekStartDate__c,fc);
        
        //Add all the keys (begin dates) to the list
        sortLst.addAll(fiscalWeekMap.keyset());
        //Sort the list
        sortLst.sort();
        
        //Loop through the sorted list and put the records ordered by begin date
        for(Date d:sortLst){
            OrderedfiscalWeekMap.put(d,fiscalWeekMap.get(d));
        }
        
        for(Roll_Out_Product__c rop: scope){ 
            
            ropMap.put(rop.Id, rop);
            planIds.add(rop.Roll_Out_Plan__c);
            
            integer newDuration = (Integer)newOppMap.get(rop.Roll_Out_Plan__r.Opportunity__c).Rollout_Duration__c;
            integer oldDuration = (Integer)rop.Rollout_Duration__c;
            system.debug('old Duration---->'+oldDuration);
            
            Date startDate = (rop.Roll_Out_Start__c).addMonths(oldDuration).addDays(7);
            if(rop.Roll_Out_Schedules__r.size()>0){
                //System.debug('<===In Adding new ROS based on existing Last ROS===>');
                startDate = rop.Roll_Out_Schedules__r[0].Plan_Date__c.addDays(7);
            }
            Date EndDateCalculated = (rop.Roll_Out_Start__c).addMonths(newDuration);
            
            Date weekBeginForStartDate = HA_RosMiscUtilities.getWeekBeginDate(startDate);
            Date weekBeginforEndDate = HA_RosMiscUtilities.getWeekBeginDate(EndDateCalculated);
            
            //Set to prevent duplicates being added to list
            Set<Date> duplicatePrev = new Set<Date>();
            
            system.debug('Start Date........................'+startDate);
            system.debug('End date calculated...............'+EndDateCalculated);
            //system.debug('Week Begin for Start Date.........'+weekBeginForStartDate);
            //system.debug('Week Begin for End Date...........'+weekBeginforEndDate);

            Boolean fiscalPeriodStart = FALSE;
            Boolean fiscalPeriodEnd = FALSE;
            //loop over the sorted fiscal week map
            for(Date d : OrderedfiscalWeekMap.keyset()){
               
                //start week of the roll out plan start date
                if(d == weekBeginForStartDate){
                    //System.debug('*****************START OF THE FISCAL WEEK...............'+d);
                    System.debug('First date of the week of Roll out plan start date.......'+weekBeginForStartDate);
                    
                    fiscalPeriodStart = TRUE;
                    if(!duplicatePrev.contains(d)) {
                        insertRollOutSchedules.add(new Roll_Out_Schedule__c(
                        Roll_Out_Product__c = rop.Id,  
                        Plan_Date__c = d, 
                        fiscal_week__c =  OrderedfiscalWeekMap.get(d).FiscalWeek__c,
                        fiscal_month__c = OrderedfiscalWeekMap.get(d).FiscalMonth__c,
                        YearMonth__c = OrderedfiscalWeekMap.get(d).FiscalYear__c + HA_RosMiscUtilities.strMM(OrderedfiscalWeekMap.get(d).FiscalMonth__c),
                        year__c= OrderedfiscalWeekMap.get(d).FiscalYear__c,
                        Plan_Quantity__c = 0));
                        duplicatePrev.add(d);
                    }                     
                }
                //if the date is within in the fiscal period then create a new roll out schedule to insert
                if(fiscalPeriodStart == TRUE && fiscalPeriodEnd == FALSE ){
                    //System.debug('******INSIDE FISCAL PERIOD..............'+d);
                    if(!duplicatePrev.contains(d)){
                        insertRollOutSchedules.add(new Roll_Out_Schedule__c(
                        Roll_Out_Product__c = rop.Id,  
                        Plan_Date__c = d, 
                        fiscal_week__c = OrderedfiscalWeekMap.get(d).FiscalWeek__c,
                        fiscal_month__c = OrderedfiscalWeekMap.get(d).FiscalMonth__c,
                        YearMonth__c = OrderedfiscalWeekMap.get(d).FiscalYear__c + HA_RosMiscUtilities.strMM(OrderedfiscalWeekMap.get(d).FiscalMonth__c),
                        year__c= OrderedfiscalWeekMap.get(d).FiscalYear__c,
                        Plan_Quantity__c = 0));
                        duplicatePrev.add(d);
                    }
                }
                
                if(d == weekBeginforEndDate){
                    //System.debug('*****************END OF THE FISCAL WEEK...............'+d);
                    system.debug('First date of the week of Roll out plan end date.......'+weekBeginForEndDate);
                    fiscalPeriodEnd = TRUE;
                    break;
                }
            }
            rop.Rollout_Duration__c = newDuration;
        }
        
        try{
            update scope;
            
            if(insertRollOutSchedules.size()>0) insert insertRollOutSchedules;   
        }
        catch(Exception ex){
            System.debug('##ex inside New HA ROS Insert ::'+ex);
        }

    }
    global void finish(Database.BatchableContext BC) {
        system.debug('## HA RollOutScheduleBatch Finish');
        Set<id> opptyIds = new Set<id>();
        List<Roll_Out__c> roList = [SELECT Id, Editable__c, Opportunity__c, Opportunity__r.Owner.Email, Opportunity__r.Name FROM Roll_Out__c WHERE Id IN: planIds];
        for(Roll_Out__c ro : roList){
            ro.Editable__c = true;
            ro.Rollout_Duration__c = newOppMap.get(ro.Opportunity__c).Rollout_Duration__c;
            
            DataWrapper dw = new DataWrapper();
            dw.userEmail = ro.Opportunity__r.Owner.Email;
            dw.opptyName = ro.Opportunity__r.Name;
            dw.rolloutId = ro.Id;
            dataList.add(dw);
            
            opptyIds.add(ro.Opportunity__c);
        }
        if(!roList.isEmpty()){
            update roList;
            if(!HA_Email_Notification__c.getInstance(UserInfo.getUserId()).Disable_Email_Notification__c){
                sendEmail(dataList);
            }
        }
        
        Database.executeBatch(new BatchforHARolloutActualsUpdate_V2(opptyIds),100);
    }
    public class DataWrapper{
        public String userEmail;
        public String opptyName;
        public String rolloutId;
    }
    public static void sendEmail(List<DataWrapper> dtList) {

        String customlink = Url.getSalesforceBaseUrl().toExternalForm();
        List<Messaging.SingleEmailMessage> mails =  new List<Messaging.SingleEmailMessage>();
        for(DataWrapper dw : dtList){
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

            List<String> to = new List<String>();
            to.add(dw.userEmail);

            mail.setToAddresses(to); // List<String> param1

            String supportEmailCCAddress = Label.CC_Email_address_for_SAM_Notification;
            List<String> supportEmailCCAddressList = new List<String>();

            if(String.isNotBlank(supportEmailCCAddress))
                supportEmailCCAddressList = supportEmailCCAddress.split(',');

            if (supportEmailCCAddressList.isEmpty() == false) {
                mail.setCcAddresses(supportEmailCCAddressList);
            }

            String disPlayName = 'HA Sales Support';
            String subject = dw.opptyName + ' Rollout Created.';
            mail.setReplyTo(dw.userEmail);
            mail.setSenderDisplayName(disPlayName);
            mail.setSubject(subject);

            String body ='';
            body += 'Dear User,  Rollout Created. \n\n';
            body += 'Opportunity Name : '+ dw.opptyName +'\n\n';
            body += 'Click on The link below to see Rollout Detail.\n\n';
            body +=  customlink +'/'+ dw.rolloutId +'\n\n';
            body += 'Thank you! \n\n';
            body += 'Samsung Business Support.\n\n';

            mail.setPlainTextBody(body);

            mails.add(mail);
        }
        Messaging.sendEmail(mails);
    }
}