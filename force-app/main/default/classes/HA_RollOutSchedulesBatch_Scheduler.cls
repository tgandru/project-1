/*
Author:  Thiru
Date:    06/13/2019
Purpose: To schedule the Rollout Schedules creation batch if any Schedule creation bacthes are in process related to same Rollout.
*/
global class HA_RollOutSchedulesBatch_Scheduler implements Schedulable{
    public Set<Id> productIds;

    global HA_RollOutSchedulesBatch_Scheduler(Set<Id> ids) {
        productIds = ids;
    }
    
	global void execute(SchedulableContext sc) {
        HA_RollOutSchedulesBatch ba = new HA_RollOutSchedulesBatch(productIds);
        database.executeBatch(ba,30);
        
        if (sc != null)	{	
			system.abortJob(sc.getTriggerId());			
		}
    }
}