public class HA_RosMiscUtilities{
    
    //Get the whole calendar at once
    public static List<SCM_Calendar__c> lstFiscCal = SCM_Calendar__c.getall().values();
    
    //This method is called from RollOutProductTriggerHandler class
    //This will return the fiscal week number for a given date
    public static integer giveFiscalWeek(date d){
        
        Date weekbegindate = getWeekBeginDate(d);
        Map<Date,SCM_Calendar__c> fiscalWeekMap = new Map<Date,SCM_Calendar__c>();
        //List<SCM_Calendar__c> lstFiscCal = SCM_Calendar__c.getall().values();
        System.debug('Total fiscal week calendar size...'+lstFiscCal.size());
        for(SCM_Calendar__c fc: lstFiscCal)
        fiscalWeekMap.put(fc.WeekStartDate__c,fc);
        
        if(fiscalWeekMap.containsKey(weekbegindate))
            return integer.valueof(fiscalWeekMap.get(weekbegindate).FiscalWeek__c);
        else
            return null;
    }
    
    //This method is called from RollOutProductTriggerHandler class
    //This will return the week start date for a given week number in a given year
    public static Date giveDateForFiscalWeek(String weeknoplusYear){
        
        Map<String,SCM_Calendar__c> fiscalWeekMap = new Map<String,SCM_Calendar__c>();
        //List<SCM_Calendar__c> lstFiscCal = SCM_Calendar__c.getall().values();
        
        for(SCM_Calendar__c fc: lstFiscCal)
        fiscalWeekMap.put(fc.FiscalWeek__c+fc.FiscalYear__c,fc);
        
        if(fiscalWeekMap.containsKey(String.valueof(weeknoplusYear)))
            return fiscalWeekMap.get(String.valueof(weeknoplusYear)).WeekStartDate__c;
        else
            return null;
    }
    
    //This method is called from RollOutProductTriggerHandler class
    //This will return the fiscal month for a given fiscal week in a given year
    public static String giveFiscalMonth(String weeknoplusYear){
        
        Map<String,SCM_Calendar__c> fiscalWeekMap = new Map<String,SCM_Calendar__c>();
        
        for(SCM_Calendar__c fc: lstFiscCal)
        fiscalWeekMap.put(fc.FiscalWeek__c+fc.FiscalYear__c,fc);
        
        if(fiscalWeekMap.containsKey(String.valueof(weeknoplusYear)))
            return fiscalWeekMap.get(String.valueof(weeknoplusYear)).FiscalMonth__c;
        else
            return null;
    }
    
    /** Calculation to get the start of the week **/
    public static Date getWeekBeginDate(Date startorEndDate){
        
        //Standard date for calculation
        Date stndDate = date.newInstance(1900, 1, 7);
        
        //Calculate "begin date" of the week for the given date
        Integer stdf = stndDate.daysBetween(startorEndDate);
        Integer stdfmod = Math.MOD( stdf, 7);   
        //This will give start date as sunday
        Date startDate = startorEndDate-(stdfmod);
        //so if the start date is same as given date, we should take the previous
        //monday as start date, else, next day as start date
        if(startDate == startorEndDate)
            return (startorEndDate - 6);
        else
            return (startDate + 1);
    }
    
    //This method is called from RollOutProductTriggerHandler class
    public static list<Roll_Out_Schedule__c> createROS(Map<Id, Roll_Out_Product__c> newROPMap) {
        
        List<Roll_Out_Schedule__c> insertRollOutSchedules = new List<Roll_Out_Schedule__c>();
        
        Map<Date,SCM_Calendar__c> fiscalWeekMap = new Map<Date,SCM_Calendar__c>();
        Map<Date,SCM_Calendar__c> OrderedfiscalWeekMap = new Map<Date,SCM_Calendar__c>();
        Map<Id,Integer> ropIdToFiscalWeekMap = new Map<Id,Integer>();
        
        //List to sort the dates by fiscal week begin date
        List<Date> sortLst = new List<Date>();
        //Loop through all the fiscal weeks (2007 to 2040 and store it in map
        for(SCM_Calendar__c fc: lstFiscCal)
            fiscalWeekMap.put(fc.WeekStartDate__c,fc);
        
        //Add all the keys (begin dates) to the list
        sortLst.addAll(fiscalWeekMap.keyset());
        //Sort the list
        sortLst.sort();
        
        //Loop through the sorted list and put the records ordered by begin date
        for(Date d:sortLst){
            OrderedfiscalWeekMap.put(d,fiscalWeekMap.get(d));
        }
        
        //Calculate the total number of weeks for each roll out product
        for(Roll_Out_Product__c rop: newROPMap.values()){ 
            
            Date startDate = rop.Roll_Out_Start__c;
            Date EndDateCalculated = (rop.Roll_Out_Start__c).addMonths((Integer)rop.Rollout_Duration__c);
            
            Date weekBeginForStartDate = getWeekBeginDate(startDate);
            Date weekBeginforEndDate = getWeekBeginDate(EndDateCalculated);
            
            //Set to prevent duplicates being added to list
            Set<Date> duplicatePrev = new Set<Date>();
            
            Boolean fiscalPeriodStart = FALSE;
            Boolean fiscalPeriodEnd = FALSE;
            
            Integer totalCount = 0;
            //loop over the sorted fiscal week map
            for(Date d : OrderedfiscalWeekMap.keyset()){
                
                //start counting at start of week of the roll out plan start date
                if(d == weekBeginForStartDate){
                    fiscalPeriodStart = TRUE;
                    if(!duplicatePrev.contains(d)){
                        duplicatePrev.add(d);      
                        totalCount++;
                    }               
                }
                //If the date is within in the fiscal period -> keep counting
                if(fiscalPeriodStart == TRUE && fiscalPeriodEnd == FALSE ){
                    if(!duplicatePrev.contains(d)){
                        duplicatePrev.add(d);
                        totalCount++;
                    }
                }
                //Break it at the end
                if(d == weekBeginforEndDate){
                    fiscalPeriodEnd = TRUE;
                    if(!duplicatePrev.contains(d)){
                        duplicatePrev.add(d);
                        totalCount++;
                    }
                    break;
                }
            }
            
            //Total number of fiscal weeks from start to end date for the roll out
            ropIdToFiscalWeekMap.put(rop.Id, totalCount);
            
        }
        
        for(Roll_Out_Product__c rop: newROPMap.values()){ 
            
            Date startDate = rop.Roll_Out_Start__c;
            Date EndDateCalculated = (rop.Roll_Out_Start__c).addMonths((Integer)rop.Rollout_Duration__c);
            //Set to prevent duplicates being added to list
            Set<Date> duplicatePrev = new Set<Date>();
             
            Integer TotalQty = 0; 
            Integer Totalweeks = 0;
            Integer planQtyCntr = 0;
            Integer equalPlanQnty = 0;
            Integer remainingVal = 0;
            Date weekBeginForStartDate = getWeekBeginDate(startDate);
            Date weekBeginforEndDate = getWeekBeginDate(EndDateCalculated);
            
            TotalQty = (Integer)rop.Quote_Quantity__c;
            Totalweeks = ropIdToFiscalWeekMap.get(rop.Id);
            Boolean TwgtQt = FALSE;
            Boolean eqPlnQt = FALSE;
            if(Totalweeks >= TotalQty){
                //totalweeks greater than product quantity
                TwgtQt = TRUE;
            }
            else{
                if(math.mod(TotalQty,Totalweeks) == 0){
                    eqPlnQt = TRUE;
                    equalPlanQnty = TotalQty/Totalweeks;
                }
                else{
                    equalPlanQnty = TotalQty/Totalweeks;
                    remainingVal = math.mod(TotalQty,Totalweeks);
                } 
            }
            system.debug('Start Date........................'+startDate);
            system.debug('End date calculated...............'+EndDateCalculated);
            system.debug('Week Begin for Start Date.........'+weekBeginForStartDate);
            system.debug('Week Begin for End Date...........'+weekBeginforEndDate);
            system.debug('Rop qunatity.......'+rop.Quote_Quantity__c+'......and #of fiscal weeks for this ROP....'+ropIdToFiscalWeekMap.get(rop.Id));
            
            Boolean fiscalPeriodStart = FALSE;
            Boolean fiscalPeriodEnd = FALSE;
            //loop over the sorted fiscal week map
            for(Date d : OrderedfiscalWeekMap.keyset()){
               
                //System.debug('Fiscal week start date..............'+d);
                Integer finalPlanQnt = 0;
                //start week of the roll out plan start date
                if(d == weekBeginForStartDate){
                    System.debug('*****************START OF THE FISCAL WEEK...............'+d);
                    system.debug('First date of the week of Roll out plan start date.......'+weekBeginForStartDate);
                    
                    fiscalPeriodStart = TRUE;
                    if(!duplicatePrev.contains(d)) {
                        planQtyCntr++;
                        if(rop.Quote_Quantity__c>0){
                            if(TwgtQt)
                                finalPlanQnt = 1;
                            else
                                finalPlanQnt = equalPlanQnty;
                        }    
                        insertRollOutSchedules.add(new Roll_Out_Schedule__c(
                        Roll_Out_Product__c = rop.Id,  
                        Plan_Date__c = d, 
                        fiscal_week__c =  OrderedfiscalWeekMap.get(d).FiscalWeek__c,
                        fiscal_month__c = OrderedfiscalWeekMap.get(d).FiscalMonth__c,
                        YearMonth__c = OrderedfiscalWeekMap.get(d).FiscalYear__c + strMM(OrderedfiscalWeekMap.get(d).FiscalMonth__c),
                        year__c= OrderedfiscalWeekMap.get(d).FiscalYear__c,
                        Plan_Quantity__c = finalPlanQnt));
                        duplicatePrev.add(d);
                    }                     
                }
                //if the date is within in the fiscal period then create a new roll out schedule to insert
                if(fiscalPeriodStart == TRUE && fiscalPeriodEnd == FALSE ){
                    System.debug('******INSIDE FISCAL PERIOD..............'+d);
                    if(!duplicatePrev.contains(d)){
                        planQtyCntr++;
                        if(TwgtQt && planQtyCntr < = TotalQty)//if total weeks is greater than quantity
                            finalPlanQnt = 1;//put plan quantity as one until the counter is equal to total quantity
                        else if(TwgtQt)
                            finalPlanQnt = 0;//if the counter runs out, then just keep 0 for all the remaining weeks
                        else{
                            //if quantity is greater than total weeks then slice the total quantity and distribute the slice
                            //between all the weeks, if slices are equal (ex:20%5 = 0) distribute equal slice for each week else
                            //Get the remaining slice (ex: 24%5 = 4) and distribute it equally for the weeks at the end of duration.
                            if(remainingVal != 0){
                                //distribute the mod value equally among the weeks
                                if(math.mod(((TotalWeeks - planQtyCntr)+1),remainingVal) == 0){
                                    finalPlanQnt = equalPlanQnty+1;
                                    remainingVal--;
                                 }
                                 else{
                                     finalPlanQnt = equalPlanQnty;
                                 }
                            }
                            else
                                finalPlanQnt = equalPlanQnty;
                            
                        }    
                        insertRollOutSchedules.add(new Roll_Out_Schedule__c(
                        Roll_Out_Product__c = rop.Id,  
                        Plan_Date__c = d, 
                        fiscal_week__c = OrderedfiscalWeekMap.get(d).FiscalWeek__c,
                        fiscal_month__c = OrderedfiscalWeekMap.get(d).FiscalMonth__c,
                        YearMonth__c = OrderedfiscalWeekMap.get(d).FiscalYear__c + strMM(OrderedfiscalWeekMap.get(d).FiscalMonth__c),
                        year__c= OrderedfiscalWeekMap.get(d).FiscalYear__c,
                        Plan_Quantity__c = finalPlanQnt));
                        duplicatePrev.add(d);
                    }
                }
                
                if(d == weekBeginforEndDate){
                    System.debug('*****************END OF THE FISCAL WEEK...............'+d);
                    system.debug('First date of the week of Roll out plan end date.......'+weekBeginForEndDate);
                    fiscalPeriodEnd = TRUE;
                    break;
                }
            }
        }
        
        system.debug('Total number of roll out schedules to be inserted.........'+insertRollOutSchedules.size());
        return insertRollOutSchedules;
    }

    /*
    //This method is called from RollOutProductTriggerHandler class
    public static list<Roll_Out_Schedule__c> createROS(Map<Id, Roll_Out_Product__c> newROPMap) {
        
        List<Roll_Out_Schedule__c> insertRollOutSchedules = new List<Roll_Out_Schedule__c>();
        
        //All fiscal dates are stored in the SCM_Calendar__c custom settings 
        //List<SCM_Calendar__c> lstFiscCal = SCM_Calendar__c.getall().values();
        
        Map<Date,SCM_Calendar__c> fiscalWeekMap = new Map<Date,SCM_Calendar__c>();
        Map<Date,SCM_Calendar__c> OrderedfiscalWeekMap = new Map<Date,SCM_Calendar__c>();
        Map<Id,Integer> ropIdToFiscalWeekMap = new Map<Id,Integer>();
        Map<String, Integer> monthCntMap = new Map<String, Integer>();
        
        //List to sort the dates by fiscal week begin date
        List<Date> sortLst = new List<Date>();
        //Loop through all the fiscal weeks (2007 to 2040 and store it in map
        for(SCM_Calendar__c fc: lstFiscCal)
        fiscalWeekMap.put(fc.WeekStartDate__c,fc);
        
        //Add all the keys (begin dates) to the list
        sortLst.addAll(fiscalWeekMap.keyset());
        //Sort the list
        sortLst.sort();
        
        //Loop through the sorted list and put the records ordered by begin date
        for(Date d:sortLst){
            OrderedfiscalWeekMap.put(d,fiscalWeekMap.get(d));
        }
        
        //Calculate the total number of weeks for each roll out product
        for(Roll_Out_Product__c rop: newROPMap.values()){ 
            
            Date startDate = rop.Roll_Out_Start__c;
            Date EndDateCalculated;
            if((Integer)rop.Rollout_Duration__c == 1){
            	Date tempDate = startDate.addMonths(1);
            	EndDateCalculated = tempDate.toStartOfMonth().addDays(-1);
            }
            else if((Integer)rop.Rollout_Duration__c > 1){
            	EndDateCalculated = (rop.Roll_Out_Start__c).addMonths((Integer)rop.Rollout_Duration__c);
                EndDateCalculated = EndDateCalculated.toStartOfMonth().addDays(-1);
            }

            Date weekBeginForStartDate = getWeekBeginDate(startDate);
            Date weekBeginforEndDate = getWeekBeginDate(EndDateCalculated);
            if(String.valueOf(EndDateCalculated.month()) != OrderedfiscalWeekMap.get(weekBeginforEndDate).FiscalMonth__c)
                weekBeginforEndDate = getWeekBeginDate(EndDateCalculated.addDays(-7));

            Integer temp = weekBeginForStartDate.monthsBetween(weekBeginforEndDate);
            if(temp > ((Integer)rop.Rollout_Duration__c-1) && (Integer)rop.Rollout_Duration__c > 1 && String.valueOf(startDate.month()) != OrderedfiscalWeekMap.get(weekBeginForStartDate).FiscalMonth__c)
                weekBeginforEndDate = getWeekBeginDate(weekBeginforEndDate.addMonths(-1));

            Set<Date> duplicatePrev = new Set<Date>();
            
            Boolean fiscalPeriodStart = FALSE;
            Boolean fiscalPeriodEnd = FALSE;
            
            Integer totalCount = 0;
            //loop over the sorted fiscal week map
            for(Date d : OrderedfiscalWeekMap.keyset()){
                
                //start counting at start of week of the roll out plan start date
                if(d == weekBeginForStartDate){
                    fiscalPeriodStart = TRUE;
                    if(!duplicatePrev.contains(d)){
                        duplicatePrev.add(d);      
                        totalCount++;
                    }               
                }
                //If the date is within in the fiscal period -> keep counting
                if(fiscalPeriodStart == TRUE && fiscalPeriodEnd == FALSE ){
                    if(!duplicatePrev.contains(d)){
                        duplicatePrev.add(d);
                        totalCount++;
                    }
                }
                //Break it at the end
                if(d == weekBeginforEndDate){
                    fiscalPeriodEnd = TRUE;
                    if(!duplicatePrev.contains(d)){
                        duplicatePrev.add(d);
                        totalCount++;
                    }
                    break;
                }
            }
            
            //Total number of fiscal weeks from start to end date for the roll out
            ropIdToFiscalWeekMap.put(rop.Id, totalCount);
        }

        for(Roll_Out_Product__c rop: newROPMap.values()){ 
            
            Date startDate = rop.Roll_Out_Start__c;
            Date EndDateCalculated;
            if((Integer)rop.Rollout_Duration__c == 1){
            	Date tempDate = startDate.addMonths(1);
            	EndDateCalculated = tempDate.toStartOfMonth().addDays(-1);
            }
            else if((Integer)rop.Rollout_Duration__c > 1){
            	EndDateCalculated = (rop.Roll_Out_Start__c).addMonths((Integer)rop.Rollout_Duration__c);
                EndDateCalculated = EndDateCalculated.toStartOfMonth().addDays(-1);
            }

            //Set to prevent duplicates being added to list
            Set<Date> duplicatePrev = new Set<Date>();
             
            Integer TotalQty = 0; 
            Integer Totalweeks = 0;
            Integer TotalMonth = 0;
            Integer planQtyCntr = 0;
            Integer equalPlanQnty = 0;
            Integer remainingVal = 0;
            Date weekBeginForStartDate = getWeekBeginDate(startDate);
            Date weekBeginforEndDate = getWeekBeginDate(EndDateCalculated);

            if(String.valueOf(EndDateCalculated.month()) != OrderedfiscalWeekMap.get(weekBeginforEndDate).FiscalMonth__c)
                weekBeginforEndDate = getWeekBeginDate(EndDateCalculated.addDays(-7));

            Integer temp = weekBeginForStartDate.monthsBetween(weekBeginforEndDate);
            if(temp > ((Integer)rop.Rollout_Duration__c-1) && (Integer)rop.Rollout_Duration__c > 1 && String.valueOf(startDate.month()) != OrderedfiscalWeekMap.get(weekBeginForStartDate).FiscalMonth__c)
                weekBeginforEndDate = getWeekBeginDate(weekBeginforEndDate.addMonths(-1));        
            
            TotalQty = (Integer)rop.Quote_Quantity__c;
            Totalweeks = ropIdToFiscalWeekMap.get(rop.Id);
            TotalMonth = (Integer)rop.Rollout_Duration__c;

            Boolean TwgtQt = FALSE;
            Boolean TmgtQt = FALSE;
            Boolean eqPlnQt = FALSE;
            if(TotalMonth >= TotalQty && TotalQty != 0){
                //totalweeks greater than product quantity
                TmgtQt = TRUE;
            }
            
            system.debug('Start Date........................'+startDate);
            system.debug('End date calculated...............'+EndDateCalculated);
            system.debug('Week Begin for Start Date.........'+weekBeginForStartDate);
            system.debug('Week Begin for End Date...........'+weekBeginforEndDate);
            system.debug('Rop qunatity.......'+rop.Quote_Quantity__c+'......and #of fiscal weeks for this ROP....'+ropIdToFiscalWeekMap.get(rop.Id));
            
            Boolean fiscalPeriodStart = FALSE;
            Boolean fiscalPeriodEnd = FALSE;
            String tempStr = '';
            //loop over the sorted fiscal week map
            for(Date d : OrderedfiscalWeekMap.keyset()){

                Integer finalPlanQnt = 0;
                //start week of the roll out plan start date
                if(d == weekBeginForStartDate){
                    System.debug('*****************START OF THE FISCAL WEEK...............'+d);
                    system.debug('First date of the week of Roll out plan start date.......'+weekBeginForStartDate);
                    
                    fiscalPeriodStart = TRUE;
                    if(!duplicatePrev.contains(d)) {
                        
                        if(TmgtQt){
                            finalPlanQnt = 1;
                            TotalQty--;
                            tempStr = OrderedfiscalWeekMap.get(d).FiscalMonth__c;
                        }
                            
                        insertRollOutSchedules.add(new Roll_Out_Schedule__c(
                        Roll_Out_Product__c = rop.Id,  
                        Plan_Date__c = d, 
                        fiscal_week__c =  OrderedfiscalWeekMap.get(d).FiscalWeek__c,
                        fiscal_month__c = OrderedfiscalWeekMap.get(d).FiscalMonth__c,
                        YearMonth__c = OrderedfiscalWeekMap.get(d).FiscalYear__c + strMM(OrderedfiscalWeekMap.get(d).FiscalMonth__c),
                        year__c= OrderedfiscalWeekMap.get(d).FiscalYear__c,
                        Plan_Quantity__c = finalPlanQnt));
                        duplicatePrev.add(d);
                    }                     
                }
                //if the date is within in the fiscal period then create a new roll out schedule to insert
                if(fiscalPeriodStart == TRUE && fiscalPeriodEnd == FALSE ){
                    System.debug('******INSIDE FISCAL PERIOD..............'+d);
                    if(!duplicatePrev.contains(d)){
                        
                        if(tempStr != OrderedfiscalWeekMap.get(d).FiscalMonth__c && TotalQty != 0){
                            finalPlanQnt = 1;
                            TotalQty--;
                            tempStr = OrderedfiscalWeekMap.get(d).FiscalMonth__c;
                        }
                        else
                            finalPlanQnt = 0;
                        //if(TmgtQt && OrderedfiscalWeekMap.get(d).FiscalWeek__c == '1' && TotalMonth != 0){
                            
                        //}    
                        //else{
                            //finalPlanQnt = 0;
                        //}

                        insertRollOutSchedules.add(new Roll_Out_Schedule__c(
                        Roll_Out_Product__c = rop.Id,  
                        Plan_Date__c = d, 
                        fiscal_week__c = OrderedfiscalWeekMap.get(d).FiscalWeek__c,
                        fiscal_month__c = OrderedfiscalWeekMap.get(d).FiscalMonth__c,
                        year__c= OrderedfiscalWeekMap.get(d).FiscalYear__c,
                        YearMonth__c = OrderedfiscalWeekMap.get(d).FiscalYear__c + strMM(OrderedfiscalWeekMap.get(d).FiscalMonth__c),
                        Plan_Quantity__c = finalPlanQnt));
                        duplicatePrev.add(d);
                    }
                }
                
                if(d == weekBeginforEndDate){
                    System.debug('*****************END OF THE FISCAL WEEK...............'+d);
                    system.debug('First date of the week of Roll out plan end date.......'+weekBeginForEndDate);
                    fiscalPeriodEnd = TRUE;
                    break;
                }
            }
        }

        for(Roll_Out_Schedule__c ros : insertRollOutSchedules){
        	
        	if(monthCntMap.containsKey(ros.Roll_Out_Product__c + ros.YearMonth__c)){
        		monthCntMap.put(ros.Roll_Out_Product__c + ros.YearMonth__c, Integer.valueOf(monthCntMap.get(ros.Roll_Out_Product__c + ros.YearMonth__c)+1));
        	}
        	if(!monthCntMap.containsKey(ros.Roll_Out_Product__c + ros.YearMonth__c)){
        		monthCntMap.put(ros.Roll_Out_Product__c + ros.YearMonth__c, 1);
        	}
        }

        for(Roll_Out_Product__c rop: newROPMap.values()){
        	Integer TotalMonth	= (Integer)rop.Rollout_Duration__c;
        	Integer TotalQty	= (Integer)rop.Quote_Quantity__c;
        	Integer MonthQty	= TotalQty/TotalMonth;
        	Integer RemainQty	= Math.mod(TotalQty,TotalMonth);
        	Integer MaxYearMonth = 0;
        	Boolean TmgtQt = FALSE;

        	if(TotalMonth >= TotalQty && TotalQty != 0){
                TmgtQt = TRUE;
            }
            if(!TmgtQt){
            	
            	for(Roll_Out_Schedule__c ros : insertRollOutSchedules){
            		if(rop.Id == ros.Roll_Out_Product__c){
            			if(MaxYearMonth < Integer.valueOf(ros.YearMonth__c))
            				MaxYearMonth = Integer.valueOf(ros.YearMonth__c);
            		}
            	}
            	Map<String, Integer> remainCntMap = new Map<String, Integer>();

	        	for(Roll_Out_Schedule__c ros : insertRollOutSchedules){
	        		if(rop.Id == ros.Roll_Out_Product__c){

	        			if(remainCntMap.containsKey(ros.Roll_Out_Product__c + ros.YearMonth__c)){
        					remainCntMap.put(ros.Roll_Out_Product__c + ros.YearMonth__c, Integer.valueOf(remainCntMap.get(ros.Roll_Out_Product__c + ros.YearMonth__c)+1));
    					}
    					if(!remainCntMap.containsKey(ros.Roll_Out_Product__c + ros.YearMonth__c)){
    						remainCntMap.put(ros.Roll_Out_Product__c + ros.YearMonth__c, 1);
    					}

	        			ros.Plan_Quantity__c = MonthQty / monthCntMap.get(ros.Roll_Out_Product__c + ros.YearMonth__c);

                        if(MonthQty < monthCntMap.get(ros.Roll_Out_Product__c + ros.YearMonth__c) && ros.YearMonth__c != String.valueOf(MaxYearMonth)){
                            ros.Plan_Quantity__c = 1;
                            if(MonthQty < remainCntMap.get(ros.Roll_Out_Product__c + ros.YearMonth__c)) ros.Plan_Quantity__c = 0;
                        }

                        else if(monthCntMap.get(ros.Roll_Out_Product__c + ros.YearMonth__c) != null &&
                            remainCntMap.get(ros.Roll_Out_Product__c + ros.YearMonth__c) != null && 
                            Math.mod(MonthQty,monthCntMap.get(ros.Roll_Out_Product__c + ros.YearMonth__c)) != 0 && ros.YearMonth__c != String.valueOf(MaxYearMonth)){
                            
                            if(monthCntMap.get(ros.Roll_Out_Product__c + ros.YearMonth__c) == remainCntMap.get(ros.Roll_Out_Product__c + ros.YearMonth__c))
                                ros.Plan_Quantity__c = ros.Plan_Quantity__c + Math.mod(MonthQty,monthCntMap.get(ros.Roll_Out_Product__c + ros.YearMonth__c));                               
                        }

                        if(monthCntMap.get(ros.Roll_Out_Product__c + ros.YearMonth__c) != null &&
                            remainCntMap.get(ros.Roll_Out_Product__c + ros.YearMonth__c) != null && 
                            Math.mod(MonthQty,monthCntMap.get(ros.Roll_Out_Product__c + ros.YearMonth__c)) != 0 && ros.YearMonth__c == String.valueOf(MaxYearMonth)){
                            
                            //if(monthCntMap.get(ros.Roll_Out_Product__c + ros.YearMonth__c) == remainCntMap.get(ros.Roll_Out_Product__c + ros.YearMonth__c) && MonthQty > monthCntMap.get(ros.Roll_Out_Product__c + ros.YearMonth__c))
                            if(monthCntMap.get(ros.Roll_Out_Product__c + ros.YearMonth__c) == remainCntMap.get(ros.Roll_Out_Product__c + ros.YearMonth__c))
                                ros.Plan_Quantity__c = ros.Plan_Quantity__c + Math.mod(MonthQty,monthCntMap.get(ros.Roll_Out_Product__c + ros.YearMonth__c)); 
                            //else if(monthCntMap.get(ros.Roll_Out_Product__c + ros.YearMonth__c) == remainCntMap.get(ros.Roll_Out_Product__c + ros.YearMonth__c) && MonthQty < monthCntMap.get(ros.Roll_Out_Product__c + ros.YearMonth__c))
                              //  ros.Plan_Quantity__c = ros.Plan_Quantity__c + MonthQty;
                            //else if(monthCntMap.get(ros.Roll_Out_Product__c + ros.YearMonth__c) == remainCntMap.get(ros.Roll_Out_Product__c + ros.YearMonth__c) && MonthQty == monthCntMap.get(ros.Roll_Out_Product__c + ros.YearMonth__c))
                              //  ros.Plan_Quantity__c = ros.Plan_Quantity__c + 1;                    
                        }
                        if(RemainQty != 0 && ros.YearMonth__c == String.valueOf(MaxYearMonth)){
                            //if(MonthQty + RemainQty > monthCntMap.get(ros.Roll_Out_Product__c + ros.YearMonth__c))
                                ros.Plan_Quantity__c = (MonthQty + RemainQty) / monthCntMap.get(ros.Roll_Out_Product__c + ros.YearMonth__c);
                            //else if(MonthQty + RemainQty < monthCntMap.get(ros.Roll_Out_Product__c + ros.YearMonth__c))
                                //ros.Plan_Quantity__c = MonthQty + RemainQty;
                        }
                        
                        if(monthCntMap.get(ros.Roll_Out_Product__c + ros.YearMonth__c) != null &&
                            remainCntMap.get(ros.Roll_Out_Product__c + ros.YearMonth__c) != null && 
                            Math.mod(MonthQty+RemainQty,monthCntMap.get(ros.Roll_Out_Product__c + ros.YearMonth__c)) != 0 && RemainQty != 0 && ros.YearMonth__c == String.valueOf(MaxYearMonth)){
                            
                            if(monthCntMap.get(ros.Roll_Out_Product__c + ros.YearMonth__c) == remainCntMap.get(ros.Roll_Out_Product__c + ros.YearMonth__c))
                                ros.Plan_Quantity__c = ros.Plan_Quantity__c + Math.mod(MonthQty+RemainQty,monthCntMap.get(ros.Roll_Out_Product__c + ros.YearMonth__c));
                        }
	        		}
	        	}
	        }	
        }
        
        system.debug('Total number of roll out schedules to be inserted.........'+insertRollOutSchedules.size());
        return insertRollOutSchedules;
    }
    */
    public static String strMM(String s) {
        if (s == null) return null;

        String p = '0';
        while(s.length() < 2) {
            s = p + s;
        }
        return s;
    }

}