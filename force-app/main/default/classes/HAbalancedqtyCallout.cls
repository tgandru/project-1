public class HAbalancedqtyCallout {
   public static final Map<string,Integration_EndPoints__c> integrationCodes = Integration_EndPoints__c.getAll();
   Public static void calloutPortal(message_queue__c mq){// We need to pass the Message as parameter to this method. the Identification text should be salesportal bo number 
       system.debug(mq.Identification_Text__c);
       String bonum=String.valueOf(mq.Identification_Text__c);
       List<Opportunity> opp=new list<Opportunity>([select id,name,Opportunity_Number__c,Sales_Portal_BO_Number__c,stagename from Opportunity where Sales_Portal_BO_Number__c =:bonum]);
       String xmlbody= requestbody(bonum ,opp[0].Opportunity_Number__c);
       system.debug('xmlbody-->'+xmlbody);
       string resp = callout(xmlbody);
       system.debug('Response Body from Slaes Portal--> '+resp);
       
       If(resp !=null && resp !=''){
               Dom.Document doc = new Dom.Document();
               doc.load(resp);
               Dom.XMLNode rootElement = doc.getRootElement();
               Dom.XMLNode ycharge=rootElement.getChildElement('BalancedList',null);
               string retcode=ycharge.getChildElement('returnCode', null).getText();
               if(retcode=='0'){
                    sucessmethod(resp,mq,opp[0]);  
               }else{
                   string rettext=ycharge.getChildElement('returnMessage', null).getText();
                  errormethod(mq,rettext,opp[0]); 
               }
           
         
            
       }
       
   }
    
    
   Public static string requestbody(string portalbo,string sfbonum){
     	String xml = '';
     	xml += '<?xml version="1.0" encoding="UTF-8"?>';
     	xml +='<BalancedInformationRequest>';
     	xml +='<CnfrmNo>'+portalbo+'</CnfrmNo>';
     	xml +='<BONumber>'+sfbonum+'</BONumber>';
     	xml +='</BalancedInformationRequest>';
     return xml;
   }
   
   Public static String callout(string body){
      
      String endPointPathProd = integrationCodes.get('Sales Portal Balanced Qty - Prod').endPointURL__c;
      String endPointPathQA = integrationCodes.get('Sales Portal Balanced Qty - QA').endPointURL__c;
      String ProdOrgId = '00D36000000JfLiEAK';
      String currentOrgId = UserInfo.getOrganizationId();
      //currentOrgId = currentOrgId.left(15);
      system.debug('OrgId---------->'+currentOrgId);
      
      System.HttpRequest request = new System.HttpRequest();
      //request.setEndpoint('http://206.67.236.61/BalancedInfo.do');//QA End point
      //request.setEndpoint('https://spna.sec.samsung.com/BalancedInfo.do');//Prod End Point
      //if(currentOrgId==ProdOrgId){
      if(currentOrgId.equals(ProdOrgId)){
          request.setEndpoint(endPointPathProd);
          system.debug('<---Prod--->');
      }else{
          request.setEndpoint(endPointPathQA);
          system.debug('<---QA--->');
      }
      request.setTimeout(120000);
      request.setHeader('Content-Type', 'application/xml;charset=UTF-8'); 
      request.setMethod('POST');
      request.setBody(body); 
      HttpResponse res=new HttpResponse();
       Http h = new Http();
       res =h.send(request);
      System.debug('-Response -'+res);
       return res.getBody();
   }
   
   Public static void sucessmethod(string res,message_queue__c mq,Opportunity op){
       system.debug(res);
       set<string> SONumSet = new Set<string>();
       List<SO_Number__c> sonumlist = new list<SO_Number__c>();
        List<SO_Number__c> solist= new list<SO_Number__c>([select id,name,Opportunity__c,SO_Number__c,Last_Successful_Run__c,(select id,SO_Number__c,DO_Qty__c,Order_Qty__c from SO_Lines__r) from SO_Number__c where Opportunity__r.id=:op.id]);
         set<string> setso=new set<string>();
         set<string> ExistingSOs = new set<string>();
        if(solist.size()>0){
            for(SO_Number__c s:solist){
                string so= string.valueOf(s.SO_Number__c);
               setso.add(so); 
            }
        }
         
           Dom.Document doc = new Dom.Document();
           doc.load(res);
           Dom.XMLNode rootElement = doc.getRootElement();
           Dom.XMLNode ycharge=rootElement.getChildElement('BalancedList',null);
           datetime dtnow = System.now(); 
           for(Dom.XmlNode childelement : ycharge.getChildElements()){
                  
               String currentNodeName =childelement.getName();
               if(currentNodeName == 'BalancedInfo'){
                   system.debug(currentNodeName);
                   string snumber= childelement.getChildElement('SO_NUMBER', null).getText();
                   system.debug(snumber);
                   if(snumber!=null && snumber!=''){//Added by Thiru--05/30/2019
                       if(setso.size()>0){ //snumber!=null && snumber!='' && 
                           if(setso.contains(snumber)){
                               ExistingSOs.add(snumber);
                           }else{
                              //sonumlist.add(new SO_Number__c(Opportunity__c=op.id,SO_Number__c=snumber));
                              SONumSet.add(snumber);
                           }
                       }else{ //if(snumber!=null && snumber!='')
                            //sonumlist.add(new SO_Number__c(Opportunity__c=op.id,SO_Number__c=snumber));
                            SONumSet.add(snumber);
                       }
                   }
               }
                 
          }
          system.debug(SONumSet);
          
          for(SO_Number__c so : solist){
              if(ExistingSOs.contains(so.SO_Number__c)){
                  so.Last_Successful_Run__c=dtnow;
                  sonumlist.add(so);
              }
          }
          
          for(string str : SONumSet){
              sonumlist.add(new SO_Number__c(Opportunity__c=op.id,SO_Number__c=str,Last_Successful_Run__c=dtnow));
          }

          if(sonumlist.size()>0){
              
              mq.Status__c='Success';
              mq.MSGSTATUS__c='S';
              Update mq;
              
              //Create new Message Queue to call Sales Portal for SO Numbers until Opportunity is Completed
              
              /*if(op.stagename!='Completed'){
                  List<message_queue__c> UpsertList = new List<message_queue__c>();
                  
                  message_queue__c newMQ = new message_queue__c();
                  newMQ.Status__c=mq.Status__c;
                  newMQ.Integration_Flow_Type__c=mq.Integration_Flow_Type__c;
                  newMQ.Identification_Text__c=mq.Identification_Text__c;
                  newMQ.Object_Name__c=mq.Object_Name__c;
                  newMQ.retry_counter__c=mq.retry_counter__c;

                  UpsertList.add(newMQ);
                  
                  mq.Status__c='Success';
                  mq.MSGSTATUS__c='S';
                  UpsertList.add(mq);
                  
                  if(UpsertList.size()>0){
                      upsert UpsertList;
                  }
              }else{
                  mq.Status__c='Success';
                  mq.MSGSTATUS__c='S';
                  Update mq;
              }*/
              
              //insert sonumlist;
              system.debug(sonumlist.size());
              Upsert sonumlist;
              
              //Create SO Line MQs
              /*List<message_queue__c> SOLineMQs = new List<message_queue__c>();
              for(SO_Number__c so : sonumlist){
                  message_queue__c m = new message_queue__c(Identification_Text__c=so.Opportunity__c, Integration_Flow_Type__c='SO Line',
                                                            retry_counter__c=0,Status__c='not started', Object_Name__c='SO Line');
                  SOLineMQs.add(m);
              }
              if(SOLineMQs.size()>0){
                  insert SOLineMQs;
              }*/
              if(solist.size()!=sonumlist.size()){
                  message_queue__c m = new message_queue__c(Identification_Text__c=op.id, Integration_Flow_Type__c='SO Line',
                                                                retry_counter__c=0,Status__c='not started', Object_Name__c='SO Line');
                  insert m;
              }else{
                  List<SO_Number__c> soNumsToGerp = new List<SO_Number__c>();
                  for(SO_Number__c so : sonumlist){
                      integer lineCount = 0;
                      for(SO_Lines__c line : so.SO_Lines__r){
                          lineCount++;
                      }
                      if(lineCount>0){
                          for(SO_Lines__c line : so.SO_Lines__r){
                              if(line.DO_Qty__c!=line.Order_Qty__c){
                                  soNumsToGerp.add(so);
                                  break;
                              }
                          }
                      }else{
                          soNumsToGerp.add(so);
                      }
                      /*for(SO_Lines__c line : so.SO_Lines__r){
                          if(line.DO_Qty__c!=line.Order_Qty__c){
                              soNumsToGerp.add(so);
                              break;
                          }
                      }*/
                  }
                  if(soNumsToGerp.size()>0){
                      message_queue__c m = new message_queue__c(Identification_Text__c=op.id, Integration_Flow_Type__c='SO Line',
                                                                retry_counter__c=0,Status__c='not started', Object_Name__c='SO Line');
                      insert m;
                  }
              }
              
          } 
       
   }
   
   Public Static void errormethod(message_queue__c mq,string er,Opportunity op){
       mq.Status__c='failed';
       mq.ERRORTEXT__c=er;
       Update mq;
       
       //Create SO Line MQs
       List<message_queue__c> SOLineMQs = new List<message_queue__c>();
       for(Opportunity o : [select id,SBQQ__PrimaryQuote__c,stagename,Sales_Portal_BO_Number__c,(select id from SO_Numbers__r) from Opportunity where recordtype.name='HA Builder' and Sales_Portal_BO_Number__c=:mq.Identification_Text__c]){
           if(o.SO_Numbers__r.size()>0){
               message_queue__c m = new message_queue__c(Identification_Text__c=o.id, Integration_Flow_Type__c='SO Line',
                                                        retry_counter__c=0,Status__c='not started', Object_Name__c='SO Line');
               SOLineMQs.add(m);
           }
        }
        if(SOLineMQs.size()>0){
            insert SOLineMQs;
        }
       
       /*if(op.stagename!='Completed'){
          List<message_queue__c> UpsertList = new List<message_queue__c>();
          
          message_queue__c newMQ = new message_queue__c();
          newMQ.Status__c=mq.Status__c;
          newMQ.Integration_Flow_Type__c=mq.Integration_Flow_Type__c;
          newMQ.Identification_Text__c=mq.Identification_Text__c;
          newMQ.Object_Name__c=mq.Object_Name__c;
          newMQ.retry_counter__c=mq.retry_counter__c;

          UpsertList.add(newMQ);
          
          mq.Status__c='failed';
          mq.ERRORTEXT__c=er;
          UpsertList.add(mq);
          
          if(UpsertList.size()>0){
              upsert UpsertList;
          }
       }else{
          mq.Status__c='failed';
          mq.ERRORTEXT__c=er;
          Update mq;
       }*/
   }
   

}