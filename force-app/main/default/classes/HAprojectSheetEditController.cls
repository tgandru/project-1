public class HAprojectSheetEditController {
     Public Opportunity opp {get; set;}
     Public Form_Submission__c submssion {get; set;}
     Public Form_Layout__c frm {get; set;}
     Public String template {get; set;} 
     Public string body {get; set;}
     Public map<string,string> labelnamemap{get; set;}
     public  List<Questionnaire_Response__c> deletlist {get; set;}
  Public HAprojectSheetEditController(){
      body='';
      labelnamemap=new map<string,string>();
        if(ApexPages.currentPage().getParameters().get('Id') !=null){
           Opp=[Select id,Name,StageName,Amount from Opportunity Where ID=:ApexPages.currentPage().getParameters().get('Id')];
            
            List<Form_Submission__c> submissionlist=new List<Form_Submission__c>([Select Id,Name,Form_Name__c,Response_JSON__c From Form_Submission__c where Opportunity__c=:ApexPages.currentPage().getParameters().get('Id') order by createddate desc]);
            frm  =[select id, name, Form_Name__c,Form_Template__c from Form_Layout__c where id=:ApexPages.currentPage().getParameters().get('Form')];
            submssion =new Form_Submission__c();
            deletlist=new List<Questionnaire_Response__c>();
            if(submissionlist.size()>0){
                submssion =submissionlist[0];
                String oldsubmission =String.valueOf(submissionlist[0].Response_JSON__c);
                oldsubmission='{ "data":'+oldsubmission +'}';
                 oldsubmission=oldsubmission.replaceAll('&quot;', '"');
                responses resp=parseresponse(oldsubmission);
                 Map<String,String> submissionMap=new Map<String,String>();
                  for(response r:resp.data){
                      submissionMap.put(r.name,r.value);
                  }
                  System.debug('Map Size'+submissionMap.keySet());
                String intialtemplate = frm.Form_Template__c;
                String json='{ "data":'+frm.Form_Template__c +'}';
                results r=new results();
                  r = parse(json);
                    for(Result  result: r.data) {
                        if(result.type !='header'){
                        labelnamemap.put(result.name, result.label);
                        }
                        
                        
                        if(result.type=='text' || result.type=='textarea' ){
                            if(submissionMap.containskey(result.name)){
                                result.value=submissionMap.get(result.name);
                            }
                        }else if(result.type=='radio-group'){
                            if(submissionMap.containskey(result.name)){
                              String selectedval=submissionMap.get(result.name);
                                for(value v: result.values){
                                    if(selectedval==v.value){
                                        v.selected='checked';
                                    }
                                    
                                    
                                    if(v.subTypes !=null){
                                          for(subType s:v.subTypes){
                                                if(s.name !='' && s.name !=null){
                                                   if(submissionMap.containskey(s.name)){
                                                        s.value=submissionMap.get(s.name);
                                                    }   
                                                }
                                               
                                            }
                                        
                                    }
                                  
                                    
                                }
                            }
                        }
                    }
                
              template =SYSTEM.JSON.serialize(r.data);
               
                System.debug('******'+String.valueOf(r.data));
            }else{ 
              template =frm.Form_Template__c ;
               String json='{ "data":'+frm.Form_Template__c +'}';
                   results  r = parse(json);
                    Map<String, String> myMap = new Map<String, String>();
                    for(Result  result: r.data) {
                      if(result.type !='header'){
                        labelnamemap.put(result.name, result.label);
                        }
                   }
                  
            }
           
        }
  
  }
                        public class results{
                         public Result[] data;
                         }
                        public class Result {
                            public String type, label,name,value;
                            public value[] values;
                         }
                         
                         public class value {
                             public String label,value;
                             public string selected;
                             public subType[] subTypes;
                         }
                         public class subType {
                              public String type, label,name,value;
                         }
                        public static results parse(String jsonString) {
                            return (results)JSON.deserialize(jsonString, results.class);
                        }
                        
                           public class responses{
                         public response[] data;
                         }
                        public class response{
                            public String value,name;
                         }
                        public static responses parseresponse(String jsonString) {
                            return (responses)JSON.deserialize(jsonString, responses.class);
                        }
    
            public PageReference savereslut2(){
                    system.debug(body);
                    List<Questionnaire_Response__c> insertlst=new List<Questionnaire_Response__c>();
                    String json='{ "data":'+body +'}';
                    responses r = parseresponse(json);
                    Map<String, String> responsemap= new Map<String, String>();
                     Map<String, List<String>> intialresponsemap= new Map<String,List< String>>();
                    for(response resp: r.data) {
                    String name=resp.name;
                       if(intialresponsemap.containskey(name)){
                           list<string> existed=intialresponsemap.get(name);
                           existed.add(resp.value);
                           intialresponsemap.put(name,existed);
                       }else{
                          intialresponsemap.put(name,new List<string>{resp.value}); 
                       }
                   
                    }
                    
                    
                    if(intialresponsemap.size()>0){
                    
                    for(string qtn: intialresponsemap.keyset()){
                        
                       List<String> values=intialresponsemap.get(qtn);
                        String value='';
                        for(String s:values){
                            value=value+s;
                        }
                      String key=qtn+'-'+value;
                      key=key.trim();
                    if(intialresponsemap.containskey(key)){
                         String finalval='';
                         for(String s:intialresponsemap.get(key)){
                            finalval=finalval+''+s;
                        }
                        finalval=value+''+finalval;
                        responsemap.put(qtn,finalval);
                    }else{
                        responsemap.put(qtn,value); 
                        
                    }
                    
                    }
                    
                    
                    
                    
                    if(submssion.ID !=null){
                    
                    submssion.Response_JSON__c=body;
                    submssion.Form_Name__c=frm.id;
                    update submssion;
                    deletlist=new List<Questionnaire_Response__c>([Select id from Questionnaire_Response__c where Form_Submission__c= :submssion.Id ]);
                    for(string qtn:responsemap.keyset()){
                    Questionnaire_Response__c qt=new Questionnaire_Response__c();
                    
                    qt.Question__c=labelnamemap.get(qtn);
                    qt.Response__c=responsemap.get(qtn);
                    // qt.Opportunity__c=oppid;
                    //qt.Form_Layout__c=frmid;
                    qt.Form_Submission__c=submssion.id;
                    if(labelnamemap.get(qtn) !='' && labelnamemap.get(qtn) !=null){
                    insertlst.add(qt);
                    }
                    
                    } 
                    }else{
                    Form_Submission__c submission=new Form_Submission__c();
                    submission.Opportunity__c=opp.id;
                    submission.Response_JSON__c=body;
                    submission.Form_Name__c=frm.id;
                    insert submission;
                    
                    for(string qtn: responsemap.keyset()){
                    Questionnaire_Response__c qt=new Questionnaire_Response__c();
                    qt.Question__c=labelnamemap.get(qtn);
                    String respns=responsemap.get(qtn);
                    qt.Response__c=respns.removeEnd(',');
                    // qt.Opportunity__c=oppid;
                    //qt.Form_Layout__c=frmid;
                    qt.Form_Submission__c=submission.id;
                    if(labelnamemap.get(qtn) !='' && labelnamemap.get(qtn) !=null){
                    insertlst.add(qt);
                    }
                    }
                    }
                    
                    
                    
                    }
                    
                    
                    if(insertlst.size()>0){
                    insert insertlst;
                    if(deletlist.size()>0){
                    delete deletlist;
                    }
                    }
                    // return null;
                    PageReference pageRef = new PageReference('/'+opp.id);
                    pageRef.setRedirect(true);
                    return pageRef;
            }
    
    
    
        public PageReference savereslut(){
    system.debug(body);
     List<Questionnaire_Response__c> insertlst=new List<Questionnaire_Response__c>();
       String json='{ "data":'+body +'}';
       responses r = parseresponse(json);
        Map<String, String> myMap = new Map<String, String>();
        for(response resp: r.data) {
          String name=resp.name;
          String qtn=labelnamemap.get(name);
          
           if(qtn==null || qtn==''){
              for(string qt:labelnamemap.keyset()){
                    if(name.Contains(qt)){
                       qtn=labelnamemap.get(qt); 
                       break;
                    }
              }
          }  
          
          if(myMap.containskey(qtn)){
              String respn=myMap.get(qtn)+resp.value;
              myMap.put(qtn,respn);
          }else{
          myMap.put(qtn,resp.value);
          }
         
        
       }
       
       if(myMap.size()>0){
           
           
           if(submssion.ID !=null){
            
                  submssion.Response_JSON__c=body;
                  submssion.Form_Name__c=frm.id;
                   update submssion;
             deletlist=new List<Questionnaire_Response__c>([Select id from Questionnaire_Response__c where Form_Submission__c= :submssion.Id ]);
           for(string qtn:myMap.keyset()){
              Questionnaire_Response__c qt=new Questionnaire_Response__c();
               qt.Question__c=qtn;
               qt.Response__c=myMap.get(qtn);
              // qt.Opportunity__c=oppid;
               //qt.Form_Layout__c=frmid;
               qt.Form_Submission__c=submssion.id;
               insertlst.add(qt);
           } 
           }else{
               Form_Submission__c submission=new Form_Submission__c();
                  submission.Opportunity__c=opp.id;
                  submission.Response_JSON__c=body;
                  submission.Form_Name__c=frm.id;
                   insert submission;
               
                   for(string qtn:myMap.keyset()){
                      Questionnaire_Response__c qt=new Questionnaire_Response__c();
                       qt.Question__c=qtn;
                       String respns=myMap.get(qtn);
                        qt.Response__c=respns.removeEnd(',');
                      // qt.Opportunity__c=oppid;
                       //qt.Form_Layout__c=frmid;
                       qt.Form_Submission__c=submission.id;
                       insertlst.add(qt);
                   }
           }
           
          
       
       }
       
       
       if(insertlst.size()>0){
        insert insertlst;
          if(deletlist.size()>0){
              delete deletlist;
          }
       }
      // return null;
       PageReference pageRef = new PageReference('/'+opp.id);
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    public PageReference cancelbtn(){
      PageReference pageRef = new PageReference('/'+opp.id);
        pageRef.setRedirect(true);
        return pageRef;  
    }
}