@isTest
private class HPSchedulerCSVTest {
	public Static String CRON_EXP = '0 0 12 15 2 ?';
 static testMethod void HPtestScheduledJob() {
        
        Test.startTest();
        //Executing Schedulable Class   
        String jobId = System.schedule('TestScheduledBatch',CRON_EXP, new SchedulerBatchCSV2 ());
        
        // Get the information from the CronTrigger API object 
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        
        // Verify the expressions are the same     
        System.assertEquals(CRON_EXP, ct.CronExpression);
        
        // Verify the job has not run 
        
        System.assertEquals(0, ct.TimesTriggered);    
        Test.stopTest();    
    }
    
     static testMethod void HPtestScheduledJobv2() {
        
        Test.startTest();
        //Executing Schedulable Class   
        String jobId = System.schedule('TestScheduledBatch',CRON_EXP, new SchedulerBatchCSV2V2 ());
        
        // Get the information from the CronTrigger API object 
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        
        // Verify the expressions are the same     
        System.assertEquals(CRON_EXP, ct.CronExpression);
        
        // Verify the job has not run 
        
        System.assertEquals(0, ct.TimesTriggered);    
        Test.stopTest();    
    }
    
     static testMethod void HPtestScheduledJobv2_1() {
        
        Test.startTest();
        //Executing Schedulable Class   
        String jobId = System.schedule('TestScheduledBatch',CRON_EXP, new SchedulerBatchCSV_V2 ());
        
        // Get the information from the CronTrigger API object 
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        
        // Verify the expressions are the same     
        System.assertEquals(CRON_EXP, ct.CronExpression);
        
        // Verify the job has not run 
        
        System.assertEquals(0, ct.TimesTriggered);    
        Test.stopTest();    
    }
    
     static testMethod void HPtestScheduledJobv2_2() {
        
        Test.startTest();
        //Executing Schedulable Class   
        String jobId = System.schedule('TestScheduledBatch',CRON_EXP, new SchedulerBatchCSV_V2V2 ());
        
        // Get the information from the CronTrigger API object 
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        
        // Verify the expressions are the same     
        System.assertEquals(CRON_EXP, ct.CronExpression);
        
        // Verify the job has not run 
        
        System.assertEquals(0, ct.TimesTriggered);    
        Test.stopTest();    
    }

}