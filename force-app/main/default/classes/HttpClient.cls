/*
 * Instantiated by AcountTrigger.trigger, and checks for what accounts have been updatedbased on checkbox 
 * Author: Eric Vennaro
**/
public class HttpClient {
    private String endpoint;

    public HttpClient(String endpoint) {
        this.endpoint = endpoint;
    }

    public String performCallout(String body, String flowNumber){
        Http http = new Http();
        try {
            HTTPResponse response = http.send(getRequest(body));
            return response.getBody();
        } catch(Exception e) {
            //throw custom exception since Apex doesn't support throws
            System.debug('Exception occured in webcallout method');
            //throw new HttpException('Exception occured making web callout' + e.getMessage());
            return e.getMessage();
        }
    }

    public Message_Queue__c handleError(Message_Queue__c mq) {
        mq.Status__C = 'failed';
        mq.Retry_Counter__C += 1;
        return mq;
    }

    private HttpRequest getRequest(String body) {
        System.HttpRequest request = new System.HttpRequest();
        request.setMethod('POST');
        request.setBody(body);
        request.setEndpoint(endpoint);
        request.setTimeout(120000);
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('Accept', 'application/json');

        return request;
    }
}