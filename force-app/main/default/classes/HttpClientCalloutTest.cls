@isTest
private class HttpClientCalloutTest {
    static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');

    @isTest static void testCallout() {
        Test.setMock(HttpCalloutMock.Class, new MockHttpResponseGenerator());
        HttpClient httpClient = new HttpClient('http://api.salesforce.com/foo/bar');
        String response = httpClient.performCallout('xxx', 'xxx');
        System.assert(response.length() > 0);
        CiHStub.UpdateInvoicePriceResponse responseWrapper = (CiHStub.UpdateInvoicePriceResponse) JSON.deserialize(response, CiHStub.UpdateInvoicePriceResponse.Class);
        System.assertEquals(responseWrapper.body.lines.get(0).unitCost, '20.1');
        System.assertEquals(responseWrapper.body.lines.get(0).materialCode, 'CLX-4195FN/XAA');
        System.assertEquals(responseWrapper.body.lines.get(0).quantity, '1');
        System.assertEquals(responseWrapper.body.lines.get(0).invoicePrice, '20.5');
        System.assertEquals(responseWrapper.body.lines.get(0).currencyKey, '1');
        System.assertEquals(responseWrapper.body.lines.get(0).itemNumberSdDoc, '001');
    }

    @isTest static void testCalloutMessageQueue() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();

        Account partnerAcc = TestDataUtility.createAccount('Distributor account');
        partnerAcc.RecordTypeId = directRT;
        insert partnerAcc;
        
        Credit_Memo__c creditMemo = TestDataUtility.createCreditMemo();
        creditMemo.InvoiceNumber__c = TestDataUtility.generateRandomString(6);
        creditMemo.Direct_Partner__c = partnerAcc.id;
        insert creditMemo;
        
        Message_Queue__c messageQueue = new Message_Queue__c(Integration_Flow_Type__c = 'Flow-008', Retry_Counter__c = 0, Identification_Text__c = creditMemo.id);
        insert messageQueue;

        HttpClient httpClient = new HttpClient('http://api.salesforce.com/foo/bar');
        Message_Queue__c mq = [SELECT Id, 
                                      Retry_Counter__c 
                               FROM Message_Queue__c 
                               LIMIT 1];
        System.assertEquals(mq.Retry_Counter__c, 0);
        Message_Queue__c updatedMq = httpClient.handleError(mq);
        System.assertEquals(updatedMq.Retry_Counter__c, 1);
    }
}