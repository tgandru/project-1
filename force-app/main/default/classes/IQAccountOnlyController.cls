public class IQAccountOnlyController {
public string leadId;
  public string mqlid{get;set;}
  public Inquiry__c iq{get; set;}
  private final string EXISTING = 'Attach to existing: ';
   Public lead leadToConvert {get;set;}
    public List<SelectOption> accounts {get{
                                         if(accounts==null){
                                             populateAccounts();
                                         }
                                         return accounts;
                                         } set;}
    public string selectedAccount {get; set;}
    public IQAccountOnlyController() {
     if(ApexPages.currentPage().getParameters().get('leadId') != null){
       leadId= ApexPages.currentPage().getParameters().get('leadId');
                     leadToConvert = [SELECT Id, Status, OwnerId, Name, Company, Owner.Name, Owner.Type, RecordType.Name FROM Lead WHERE Id = :leadId];
                     
     }
                    if(ApexPages.currentPage().getParameters().get('mqlid') != null){
                              mqlid= ApexPages.currentPage().getParameters().get('mqlid');
                              iq=[select id,Account__c,Contact__c,Inquiry_Status__c,IsActive__c,RecordType.Name,Opportunity__c from Inquiry__c where id=:mqlid];
                            }
    }

    public Contact contactID {
        get {
            if (contactId == null) {
                contactID = new Contact(OwnerId = UserInfo.getUserId() );
            }
            return contactId;
        }
        set;
    }
    private Account [] findCompany (string companyName) {
        
        //perform the SOSL query
        List<List<SObject>> searchList = [
            FIND :companyName 
            IN NAME FIELDS 
            RETURNING 
            Account(
                Id, 
                Name
            )
        ];
        
        List <Account> accountsFound = new List<Account>();
        
        for (List <sobject> sObjs : searchList) {
            
            for (sObject s : sObjs) {
                
                //add the account that was found to the list of found accounts
                accountsFound.add((Account) s);
            }   
        }
                // return the list of found accounts
        return accountsFound;
    }
    
    //populate the list of Accounts in the dropdown
    private void populateAccounts() {
        
        if (leadToConvert != null) {
                
            string company = leadToConvert.Company;
            
            // find any accounts that match the SOSL query in the findCompany() method  
            Account [] accountsFound = findCompany(company + '*');
            
            accounts = new List<selectOption>();
            
            if (accountsFound != null && accountsFound.size() > 0) {
                
                // if there is at least 1 account found add a NONE option and a Create New Account option
                accounts.add(new SelectOption('NONE', '-None-'));
                
                accounts.add(new SelectOption('NEW', 'Create New Account: ' + company ));
                
                // for each account found, add an option to attach to the existing account
                for (Account a : accountsFound) {
                    
                    accounts.add(new SelectOption(a.Id, EXISTING + a.Name));
                }
                
            }
            
            else {
                
                // if no accounts matched then simply add a Create New Account option
                accounts.add(new SelectOption('NEW', 'Create New Account: ' + company ));
                
                system.debug('no account matches on company ' + company);
            }
            
           
        }
        
        else system.debug('leadToConvert = null');
            
    }
      
    public PageReference convertLead() {
    
      try{
              Database.LeadConvert leadConvert = new database.LeadConvert();
               leadConvert.setLeadId(leadToConvert.Id); 
               if(iq.RecordType.Name=='HA'){
                leadConvert.setConvertedStatus('Account / Oppty');
                leadConvert.setDoNotCreateOpportunity(false); 
 
               }else{
                    leadConvert.setConvertedStatus('Account Only');
                    leadConvert.setDoNotCreateOpportunity(true);
               }
               
                if (selectedAccount == 'NONE')
                 {
                PrintError('Please select an Account.');
                return null;
                
                  } 
                // otherwise set the account id
                else if (selectedAccount != 'NEW') {
                    leadConvert.setAccountId(selectedAccount);
                } 
               leadConvert.setOwnerId(contactId.ownerID);
               
            Database.LeadConvertResult leadConvertResult = Database.convertLead(leadConvert);
             if (leadConvertResult.success)
            {
                                iq.contact__c=leadConvertResult.getContactId();
                                iq.Account__c=leadConvertResult.getAccountId();
                                iq.Lead__c=null;
                                iq.From_Lead__c=true;
                                iq.Is_Converted__c=true;
                                iq.IsActive__c=false;
                                if(iq.Inquiry_Status__c!='Account Only'){
                                  iq.Inquiry_Status__c='Account Only';
                                }
                                if(iq.RecordType.Name=='HA'){
                                    iq.Opportunity__c=leadConvertResult.getOpportunityId();
                                     if(iq.Inquiry_Status__c!='Account/Opportunity'){
                                          iq.Inquiry_Status__c='Account/Opportunity';
                                        }
                                }
                                 
                                
                                  updaterelatedtaskrecords(iq); //Added By Vijay on 5/15
                                update iq;   
            
            }
            
             PageReference pageRef = new PageReference('/' + leadConvertResult.getAccountId());
                
                pageRef.setRedirect(true);
                
                return pageRef;
               
      } catch(Exception e){
            if(e.getMessage().contains('DUPLICATE_VALUE') ){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'You\'re creating a duplicate record. We recommend you use an existing record instead.'));
            }else{
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,e.getMessage()));
            }
            
            String err =''+e;
            System.debug('## exception in Lead Conversion catch'+e);
            //KR:062016 - Commented "FIELD_CUSTOM_VALIDATION_EXCEPTION" the below code which is causing the duplicate pagemessage - T16576
            
            if(err.contains('DUPLICATES_DETECTED')){
                PrintError(err.subString(101,err.length()-4));
            }
            else if(err.contains('INSUFFICIENT_ACCESS')) {//INSUFFICIENT_ACCESS_ON_CROSS_REFERENCE_ENTITY, You do not have the required permission. To continue, you must have the 'Edit' permission on accounts
                 System.debug('## Contact Err'+err);
                PrintError(err.subString(127,err.length()-6));
               
            }
            else if(err.contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')){
                PrintError(err.subString(114,err.length()-4));
                return null;
            }
        }
    return null;
    
    }   
   public void PrintErrors(Database.Error[] errors)
    {
        for(Database.Error error : errors)
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, error.message,'');
            ApexPages.addMessage(msg);
        }
    }
    
    //This method will put an error into the PageMessages on the page
    public void PrintError(string error) {
        System.debug('## in print error string'+error);
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, error,'');
        ApexPages.addMessage(msg);
    } 
    
  public PageReference cancelconv() {
  PageReference pageRef = new PageReference('/' + mqlid);
                
              
                
                return pageRef;
  }
  public PageReference accountLookedUp() {
        system.debug('!!! Account looked up --> ' + contactId.AccountId );
        
        //find the Id and Nmae of the Account that was looked up        
        Account [] a = [
            SELECT Id, Name 
            FROM Account WHERE Id = :contactId.AccountId];
        
        if (a.size() > 0) {
            
            
            accounts.add(new SelectOption(a[0].Id, EXISTING + a[0].Name));
            selectedAccount = a[0].Id;
           
        }
        
        return null;
    } 
  
   public PageReference convertLeadopp() {
    
      try{
              Database.LeadConvert leadConvert = new database.LeadConvert();
               leadConvert.setLeadId(leadToConvert.Id); 
               if(iq.RecordType.Name=='HA'){
                leadConvert.setConvertedStatus('Account / Oppty');
                leadConvert.setDoNotCreateOpportunity(false); 
 
               }else{
                    leadConvert.setConvertedStatus('Account Only');
                    leadConvert.setDoNotCreateOpportunity(true);
               }
                if (selectedAccount == 'NONE')
                 {
                PrintError('Please select an Account.');
                return null;
                
                  } 
                // otherwise set the account id
                else if (selectedAccount != 'NEW') {
                    leadConvert.setAccountId(selectedAccount);
                } 
               leadConvert.setOwnerId(contactId.ownerID);
               
            Database.LeadConvertResult leadConvertResult = Database.convertLead(leadConvert);
             if (leadConvertResult.success)
            {
                                iq.contact__c=leadConvertResult.getContactId();
                                iq.Account__c=leadConvertResult.getAccountId();
                                iq.Is_Converted__c=true;
                                iq.Lead__c=null;
                                iq.From_Lead__c=true;
                               if(iq.RecordType.Name=='HA'){
                                    iq.Opportunity__c=leadConvertResult.getOpportunityId();
                                     if(iq.Inquiry_Status__c!='Account/Opportunity'){
                                          iq.Inquiry_Status__c='Account/Opportunity';
                                        }
                                }
                                
                                update iq;   
            
            }
               if(iq.RecordType.Name=='HA'){
                   PageReference redirect = new PageReference('/' + leadConvertResult.getAccountId());
                                    redirect.setRedirect(true);
                                    return redirect;
               }else{
                   PageReference redirect = new PageReference('/apex/InquiryOppCreation?mqlid=' + iq.Id);
                                    redirect.setRedirect(true);
                                    return redirect;
                   
               }
               
      } catch(Exception e){
            if(e.getMessage().contains('DUPLICATE_VALUE') ){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'You\'re creating a duplicate record. We recommend you use an existing record instead.'));
            }else{
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,e.getMessage()));
            }
            
            String err =''+e;
            System.debug('## exception in Lead Conversion catch'+e);
            //KR:062016 - Commented "FIELD_CUSTOM_VALIDATION_EXCEPTION" the below code which is causing the duplicate pagemessage - T16576
            
            if(err.contains('DUPLICATES_DETECTED')){
                PrintError(err.subString(101,err.length()-4));
            }
            else if(err.contains('INSUFFICIENT_ACCESS')) {//INSUFFICIENT_ACCESS_ON_CROSS_REFERENCE_ENTITY, You do not have the required permission. To continue, you must have the 'Edit' permission on accounts
                 System.debug('## Contact Err'+err);
                PrintError(err.subString(127,err.length()-6));
               
            }
            else if(err.contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')){
                PrintError(err.subString(114,err.length()-4));
                return null;
            }
        }
    return null;
    
    }   
    
     Public void updaterelatedtaskrecords(Inquiry__c iq){//Added By Vijay on 5/15 
    //This Method is for Attaching the Activites under inquiry to New Account/Opportunity
        List<task> tk= new list<task>([select id,whatId,whoID,Inquiry__c from Task where whatid=:iq.id]);
        List<Event> evn=new list<Event>([select id,whatId,whoID,Inquiry__c from Event where whatid=:iq.id]);
        if(tk.size()>0){
            for(task t:tk){
                t.Inquiry__c=iq.id;
                t.whatid=iq.Account__c;
                t.whoid=iq.contact__c;
            }
            
            Database.update(tk,false);
            
        }
        
        if(evn.size()>0){
            for(Event e:evn){
                e.Inquiry__c=iq.id;
                e.whatid=iq.Account__c;
                e.whoid=iq.contact__c;
            }
            
            Database.update(evn,false);
            
        }
        
    }
    
    
    
}