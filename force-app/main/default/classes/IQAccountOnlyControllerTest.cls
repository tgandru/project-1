@isTest
private class IQAccountOnlyControllerTest {

    private static testMethod void test() {
        Test.startTest();
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Account newAccount= TestDataUtility.createAccount(TestDataUtility.generateRandomString(5));
        newAccount.name ='Test Account';
        //acc.recordTypeId = endUserRT;
        newAccount.SAP_Company_Code__c='99999888';
        insert newAccount;
        Zipcode_Lookup__c z=new Zipcode_Lookup__c(City_Name__c='Ridgefield Park', Country_Code__c='US', State_Code__c='NJ', State_Name__c='New Jersey', Name='07660');
        insert z;
        
         string   CampaignTypeId = [Select Id From RecordType Where SobjectType = 'Campaign' and Name = 'Sales'  limit 1].Id;
        string   LeadTypeId = [Select Id From RecordType Where SobjectType = 'Lead' and Name = 'End User'  limit 1].Id;
       Campaign cmp= new Campaign(Name='Test Campaign for test class ', IsActive=true,type='Email',StartDate=system.Today(),recordtypeid=CampaignTypeId);
       insert cmp;
        lead le1=new Lead(
                        Company = 'Test Account', LastName= 'Test Lead',
                        LeadSource = 'Web', Email='test@testabc.com',Division__c='IT',ProductGroup__c='A3 COPIER',
                        Status = 'Account Only',street='123 sample st',city='Ridgefield Park',state='NJ', Country='US',PostalCode='07660',recordtypeid=LeadTypeId);
        insert le1;
         Inquiry__c iq=new Inquiry__c(Lead__c = le1.id,Product_Solution_of_Interest__c='Monitors',   campaign__c = cmp.id);
         insert iq;
         PageReference VFPage = Page.IQAccountOnly;
         Test.setCurrentPageReference(VFPage); 
         ApexPages.currentPage().getParameters().put('leadId',le1.id);
         ApexPages.currentPage().getParameters().put('mqlid', iq.Id);
         IQAccountOnlyController contr = new IQAccountOnlyController();
         contr.leadToConvert = le1;
         contact c1=new contact(lastName=le1.lastname,AccountId=newAccount.id,OwnerId = UserInfo.getUserId());
         contr.contactID = c1;
         List<SelectOption> selOpts=contr.accounts;
         contr.selectedAccount = newAccount.Name;
          contr.accountLookedUp();
         contr.convertLead();
        
          contr.cancelconv();

          contr.PrintErrors(new List<Database.Error>());
        contr.PrintError('Test');
        
         
         
         
         Test.stopTest(); 


    }
    
    
    private static testMethod void test1() {
        Test.startTest();
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Account newAccount= TestDataUtility.createAccount(TestDataUtility.generateRandomString(5));
        newAccount.name ='Test Account';
        //acc.recordTypeId = endUserRT;
        newAccount.SAP_Company_Code__c='99999888';
        insert newAccount;
        Zipcode_Lookup__c z=new Zipcode_Lookup__c(City_Name__c='Ridgefield Park', Country_Code__c='US', State_Code__c='NJ', State_Name__c='New Jersey', Name='07660');
        insert z;
        
         string   CampaignTypeId = [Select Id From RecordType Where SobjectType = 'Campaign' and Name = 'Sales'  limit 1].Id;
        string   LeadTypeId = [Select Id From RecordType Where SobjectType = 'Lead' and Name = 'HA'  limit 1].Id;
         string   IQTypeId = [Select Id From RecordType Where SobjectType = 'Inquiry__c' and Name = 'HA'  limit 1].Id;
       Campaign cmp= new Campaign(Name='Test Campaign for test class ', IsActive=true,type='Email',StartDate=system.Today(),recordtypeid=CampaignTypeId);
       insert cmp;
        lead le1=new Lead(
                        Company = 'Test Account', LastName= 'Test Lead',
                        LeadSource = 'Web', Email='test@testabc.com',Division__c='IT',ProductGroup__c='A3 COPIER',
                        Status = 'Account Only',street='123 sample st',city='Ridgefield Park',state='NJ', Country='US',PostalCode='07660',recordtypeid=LeadTypeId);
        insert le1;
         Inquiry__c iq=new Inquiry__c(Lead__c = le1.id,Product_Solution_of_Interest__c='Samsung Home Appliance',   campaign__c = cmp.id,recordtypeid=IQTypeId);
         insert iq;
         PageReference VFPage = Page.IQAccountOnly;
         Test.setCurrentPageReference(VFPage); 
         ApexPages.currentPage().getParameters().put('leadId',le1.id);
         ApexPages.currentPage().getParameters().put('mqlid', iq.Id);
         IQAccountOnlyController contr = new IQAccountOnlyController();
         contr.leadToConvert = le1;
         contact c1=new contact(lastName=le1.lastname,AccountId=newAccount.id,OwnerId = UserInfo.getUserId());
         contr.contactID = c1;
         List<SelectOption> selOpts=contr.accounts;
         contr.selectedAccount = newAccount.Name;
          contr.accountLookedUp();
         contr.convertLead();
        
          contr.cancelconv();
         
         Test.stopTest();
	}
	
	
    private static testMethod void testmethodcon() {
        Test.startTest();
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Account newAccount= TestDataUtility.createAccount(TestDataUtility.generateRandomString(5));
        newAccount.name ='Test Account';
        //acc.recordTypeId = endUserRT;
        newAccount.SAP_Company_Code__c='99999888';
        insert newAccount;
        Zipcode_Lookup__c z=new Zipcode_Lookup__c(City_Name__c='Ridgefield Park', Country_Code__c='US', State_Code__c='NJ', State_Name__c='New Jersey', Name='07660');
        insert z;
        
         string   CampaignTypeId = [Select Id From RecordType Where SobjectType = 'Campaign' and Name = 'Sales'  limit 1].Id;
        string   LeadTypeId = [Select Id From RecordType Where SobjectType = 'Lead' and Name = 'End User'  limit 1].Id;
       Campaign cmp= new Campaign(Name='Test Campaign for test class ', IsActive=true,type='Email',StartDate=system.Today(),recordtypeid=CampaignTypeId);
       insert cmp;
        lead le1=new Lead(
                        Company = 'Test Account', LastName= 'Test Lead',
                        LeadSource = 'Web', Email='test@testabc.com',Division__c='IT',ProductGroup__c='A3 COPIER',
                        Status = 'Account Only',street='123 sample st',city='Ridgefield Park',state='NJ', Country='US',PostalCode='07660',recordtypeid=LeadTypeId);
        insert le1;
         Inquiry__c iq=new Inquiry__c(Lead__c = le1.id,Product_Solution_of_Interest__c='Monitors',   campaign__c = cmp.id);
         insert iq;
         PageReference VFPage = Page.IQAccountOnly;
         Test.setCurrentPageReference(VFPage); 
         ApexPages.currentPage().getParameters().put('leadId',le1.id);
         ApexPages.currentPage().getParameters().put('mqlid', iq.Id);
         IQAccountOnlyController contr = new IQAccountOnlyController();
         contr.leadToConvert = le1;
         contact c1=new contact(lastName=le1.lastname,AccountId=newAccount.id,OwnerId = UserInfo.getUserId());
         //contr.contactID = c1;
         List<SelectOption> selOpts=contr.accounts;
         contr.selectedAccount = newAccount.Name;
          contr.accountLookedUp();
         contr.convertLead();
        
          contr.cancelconv();

          contr.PrintErrors(new List<Database.Error>());
        contr.PrintError('Test');
        
         
         
         
         Test.stopTest(); 


    }
    private static testMethod void testmethod2() {
        Test.startTest();
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Account newAccount= TestDataUtility.createAccount(TestDataUtility.generateRandomString(5));
        newAccount.name ='Test Account';
        //acc.recordTypeId = endUserRT;
        newAccount.SAP_Company_Code__c='99999888';
        insert newAccount;
        Zipcode_Lookup__c z=new Zipcode_Lookup__c(City_Name__c='Ridgefield Park', Country_Code__c='US', State_Code__c='NJ', State_Name__c='New Jersey', Name='07660');
        insert z;
        
         string   CampaignTypeId = [Select Id From RecordType Where SobjectType = 'Campaign' and Name = 'Sales'  limit 1].Id;
        string   LeadTypeId = [Select Id From RecordType Where SobjectType = 'Lead' and Name = 'End User'  limit 1].Id;
       Campaign cmp= new Campaign(Name='Test Campaign for test class ', IsActive=true,type='Email',StartDate=system.Today(),recordtypeid=CampaignTypeId);
       insert cmp;
        lead le1=new Lead(
                        Company = 'Test Account', LastName= 'Test Lead',
                        LeadSource = 'Web', Email='test@testabc.com',Division__c='IT',ProductGroup__c='A3 COPIER',
                        Status = 'Account Only',street='123 sample st',city='Ridgefield Park',state='NJ', Country='US',PostalCode='07660',recordtypeid=LeadTypeId);
        insert le1;
         Inquiry__c iq=new Inquiry__c(Lead__c = le1.id,Product_Solution_of_Interest__c='Monitors',   campaign__c = cmp.id);
         insert iq;
      
         PageReference VFPage = Page.IQAccountOnly;
         Test.setCurrentPageReference(VFPage); 
         ApexPages.currentPage().getParameters().put('leadId',le1.id);
         ApexPages.currentPage().getParameters().put('mqlid', iq.Id);
         IQAccountOnlyController contr = new IQAccountOnlyController();
         contr.leadToConvert = le1;
         contact c1=new contact(lastName=le1.lastname,AccountId=newAccount.id,OwnerId = UserInfo.getUserId());
        // contr.contactID = c1;
         List<SelectOption> selOpts=contr.accounts;
         contr.selectedAccount = newAccount.Name;
          contr.accountLookedUp();
          contr.convertLeadopp();
          contr.cancelconv();

          contr.PrintErrors(new List<Database.Error>());
        contr.PrintError('Test');
        
     
         
         Test.stopTest(); 


    }
        private static testMethod void testmethod3() {
        Test.startTest();
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Account newAccount= TestDataUtility.createAccount(TestDataUtility.generateRandomString(5));
        newAccount.name ='Test Account';
        //acc.recordTypeId = endUserRT;
        newAccount.SAP_Company_Code__c='99999888';
        insert newAccount;
        Zipcode_Lookup__c z=new Zipcode_Lookup__c(City_Name__c='Ridgefield Park', Country_Code__c='US', State_Code__c='NJ', State_Name__c='New Jersey', Name='07660');
        insert z;
        
         string   CampaignTypeId = [Select Id From RecordType Where SobjectType = 'Campaign' and Name = 'Sales'  limit 1].Id;
        string   LeadTypeId = [Select Id From RecordType Where SobjectType = 'Lead' and Name = 'End User'  limit 1].Id;
       Campaign cmp= new Campaign(Name='Test Campaign for test class ', IsActive=true,type='Email',StartDate=system.Today(),recordtypeid=CampaignTypeId);
       insert cmp;
        lead le1=new Lead(
                        Company = 'Test Account', LastName= 'Test Lead',
                        LeadSource = 'Web', Email='test@testabc.com',Division__c='IT',ProductGroup__c='A3 COPIER',
                        Status = 'Account Only',street='123 sample st',city='Ridgefield Park',state='NJ', Country='US',PostalCode='07660',recordtypeid=LeadTypeId);
        insert le1;
         Inquiry__c iq=new Inquiry__c(Lead__c = le1.id,Product_Solution_of_Interest__c='Monitors',   campaign__c = cmp.id);
         insert iq;
      
         PageReference VFPage = Page.IQAccountOnly;
         Test.setCurrentPageReference(VFPage); 
         ApexPages.currentPage().getParameters().put('leadId',le1.id);
         ApexPages.currentPage().getParameters().put('mqlid', iq.Id);
         IQAccountOnlyController contr = new IQAccountOnlyController();
         contr.leadToConvert = le1;
         contact c1=new contact(lastName=le1.lastname,AccountId=newAccount.id,OwnerId = UserInfo.getUserId());
        contr.contactID = c1;
         List<SelectOption> selOpts=contr.accounts;
         contr.selectedAccount = newAccount.Name;
          contr.accountLookedUp();
          contr.convertLeadopp();
          contr.cancelconv();

          contr.PrintErrors(new List<Database.Error>());
        contr.PrintError('Test');
        
     
         
         Test.stopTest(); 


    }

}