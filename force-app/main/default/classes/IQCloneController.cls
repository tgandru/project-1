public class IQCloneController {

public String IQId {get; set;}
public Inquiry__c newIQ {get; set;}

    public IQCloneController(ApexPages.StandardController controller) {
      IQId = apexPages.currentPage().getParameters().get('id');
       user  curusr=[select id,sales_team__c,profileid,Profile.Name from user where id=: UserInfo.getUserId()];
      System.debug('HAS ID'+IQId);
      newIQ =new Inquiry__C();
        if(IQId  != null){
             Inquiry__c  oldiq =new Inquiry__c (id=IQId); 
                    sObject originalSObject = (sObject) oldiq;

                    List<sObject> originalSObjects = new List<sObject>{originalSObject};
                          
                    List<sObject> clonedSObjects = SObjectAllFieldCloner.cloneObjects(
                                                          originalSobjects,
                                                          originalSobject.getsObjectType());
                   newIQ  =(Inquiry__c)clonedSObjects.get(0);
                   newIq.Lead__C=null;
                   newIq.Contact__c=null;
                   newIq.Account__c=null;
                   newIq.Opportunity__c=null;
                   newIq.Original_Inquiry__c=IQId;
                   newIq.Reassignment_Time_8hrs__c=null;
                   newIq.Reassignment_Time_16hrs__c=null; 
                   newIq.Inquiry_Status__c='Assigned'; 
                   newIq.MQL_End_Date__c=null;
                   newIq.SAL_Start_Date__c=null;
                   newIq.SAL_End_Date__c=null;
                   newIq.SQL_Start_Date__c=null;
                   newIq.SQL_End_Date__c=null;
                   newIq.SQO_Start_Date__c=null;
                    if(curusr.sales_team__c=='Marketstar'){
                           newIq.Campaign__c=newIq.Last_Campaign__c;
                           newIq.Last_Campaign__c=label.M_Campaign_ID;
                            
                    }else{
                    
                    
                    }
                
           
        }
    }
    
    
    
    
    public pageReference saveIQ(){
        try{
          
            
            Insert newIQ ;
            return  new pageReference('/'+newIQ.id);
        }catch(exception e){
            ApexPages.addMessages(e);
            return null;
        }
    } 
    
    
     public pageReference cancelCloneIQ(){
      return  new pageReference('/'+IQId);
     
     }

}