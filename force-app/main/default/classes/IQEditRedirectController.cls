public class IQEditRedirectController {
 Public Inquiry__c iq;
 public string iqid;
    public IQEditRedirectController(ApexPages.StandardController controller) {

    }
    
  Public pagereference redirect()
    { 
        iqid = apexpages.currentpage().getparameters().get('id');
        if(iqid != null && iqid != null){
            iq= [select id,name,recordtype.name from Inquiry__c where id =:iqid];
        }
        if(iq !=null){
            if(iq.recordtype.name=='PRM Project Registraion' || iq.recordtype.name=='PRM SPA Request' || iq.recordtype.name =='HA'){
                
                pagereference pg = new pagereference('/' +iqid+'/e?retURL=%2F'+iqid+'%3Fnooverride%3D1&nooverride=1');               
                pg.setredirect(true);
                return pg; 
            }
            else{
                pagereference pg = new pagereference('/apex/IQEdit?id='+iqid);           
                pg.setredirect(true);
                return pg;
            }
        }
        
     return null;
    }  

}