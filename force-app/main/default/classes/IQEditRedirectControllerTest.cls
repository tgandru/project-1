@isTest
private class IQEditRedirectControllerTest {

	private static testMethod void test1() {
        Test.startTest();
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Account newAccount= TestDataUtility.createAccount(TestDataUtility.generateRandomString(5));
        newAccount.name ='Test Account';
        //acc.recordTypeId = endUserRT;
        newAccount.SAP_Company_Code__c='99999888';
        insert newAccount;
        Zipcode_Lookup__c z=new Zipcode_Lookup__c(City_Name__c='Ridgefield Park', Country_Code__c='US', State_Code__c='NJ', State_Name__c='New Jersey', Name='07660');
        insert z;
        
         string   CampaignTypeId = [Select Id From RecordType Where SobjectType = 'Campaign' and Name = 'Sales'  limit 1].Id;
        string   LeadTypeId = [Select Id From RecordType Where SobjectType = 'Lead' and Name = 'HA'  limit 1].Id;
         string   IQTypeId = [Select Id From RecordType Where SobjectType = 'Inquiry__c' and Name = 'HA'  limit 1].Id;
       Campaign cmp= new Campaign(Name='Test Campaign for test class ', IsActive=true,type='Email',StartDate=system.Today(),recordtypeid=CampaignTypeId);
       insert cmp;
        lead le1=new Lead(
                        Company = 'Test Account', LastName= 'Test Lead',
                        LeadSource = 'Web', Email='test@testabc.com',Division__c='IT',ProductGroup__c='A3 COPIER',
                        Status = 'Account Only',street='123 sample st',city='Ridgefield Park',state='NJ', Country='US',PostalCode='07660',recordtypeid=LeadTypeId);
        insert le1;
         Inquiry__c iq=new Inquiry__c(Lead__c = le1.id,Product_Solution_of_Interest__c='Samsung Home Appliance',   campaign__c = cmp.id,recordtypeid=IQTypeId);
         insert iq;
         PageReference VFPage = Page.IQEditRedirect;
         Test.setCurrentPageReference(VFPage); 
         ApexPages.currentPage().getParameters().put('Id',iq.id);
          ApexPages.StandardController sc = new ApexPages.StandardController(iq);
            IQEditRedirectController ac = new IQEditRedirectController(sc);
      
         ac.redirect();
         
         
         Test.stopTest();
	}
	
		private static testMethod void test2() {
        Test.startTest();
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Account newAccount= TestDataUtility.createAccount(TestDataUtility.generateRandomString(5));
        newAccount.name ='Test Account';
        //acc.recordTypeId = endUserRT;
        newAccount.SAP_Company_Code__c='99999888';
        insert newAccount;
        Zipcode_Lookup__c z=new Zipcode_Lookup__c(City_Name__c='Ridgefield Park', Country_Code__c='US', State_Code__c='NJ', State_Name__c='New Jersey', Name='07660');
        insert z;
        
         string   CampaignTypeId = [Select Id From RecordType Where SobjectType = 'Campaign' and Name = 'Sales'  limit 1].Id;
        string   LeadTypeId = [Select Id From RecordType Where SobjectType = 'Lead' and Name = 'HA'  limit 1].Id;
       Campaign cmp= new Campaign(Name='Test Campaign for test class ', IsActive=true,type='Email',StartDate=system.Today(),recordtypeid=CampaignTypeId);
       insert cmp;
        lead le1=new Lead(
                        Company = 'Test Account', LastName= 'Test Lead',
                        LeadSource = 'Web', Email='test@testabc.com',Division__c='IT',ProductGroup__c='A3 COPIER',
                        Status = 'Account Only',street='123 sample st',city='Ridgefield Park',state='NJ', Country='US',PostalCode='07660',recordtypeid=LeadTypeId);
        insert le1;
         Inquiry__c iq=new Inquiry__c(Lead__c = le1.id,Product_Solution_of_Interest__c='Samsung Home Appliance',   campaign__c = cmp.id);
         insert iq;
         PageReference VFPage = Page.IQEditRedirect;
         Test.setCurrentPageReference(VFPage); 
         ApexPages.currentPage().getParameters().put('Id',iq.id);
          ApexPages.StandardController sc = new ApexPages.StandardController(iq);
            IQEditRedirectController ac = new IQEditRedirectController(sc);
      
         ac.redirect();
         
         
         Test.stopTest();
	}

}