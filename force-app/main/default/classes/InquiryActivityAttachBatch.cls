global class InquiryActivityAttachBatch implements Database.Batchable<sObject>,Database.Stateful {
    global map<id,list<task>> ldtask;
     global InquiryActivityAttachBatch(){
         ldtask=new map<id,list<task>>();
     }
     
      global Database.QueryLocator start(Database.BatchableContext BC){
           return Database.getQueryLocator([select id,lead__c,campaign__c from Inquiry__c ]);
   }
    global void execute(Database.BatchableContext BC,list<Inquiry__c> scope){
        set<id> lid=new set<id>();
        set<task> updatelist =new set<task>();
     map<id,list<task>> ldtk=new map<id,list<task>>();
      for(Inquiry__c i:scope){
          lid.add(i.lead__c);   
      }  
       if(lid.size()>0){
           list<task> tk =new list<task>([select id,whatid,whoid,CampaignID__c,Inquiry__c from task where whoid !=null and  whoid in:lid  ]);
           for(task t:tk){
               if(ldtk.containsKey(t.whoid)) {
                        		List<task> ti = ldtk.get(t.whoid);
                        		ti.add(t);
                        		ldtk.put(t.whoid,ti);
                        	} else {
                        	    	ldtk.put(t.whoid, new List<task> {t});
                        		
                	}
           }
         
         
         for(Inquiry__c i:[select id,lead__c,campaign__c from Inquiry__c where lead__c in:ldtk.keyset()]){
            list<task> tkl=ldtk.get(i.lead__c);
            system.debug(tkl);
            if(tkl.size()>0){
                 for(task t:tkl){
                 system.debug('task id ***'+i.id);
                system.debug('task id ***'+t.id);
                if(t.Inquiry__c==null){
                  if(t.CampaignID__c==i.Campaign__c){
                  t.Inquiry__c=i.id;
                
                }else {
                    t.Inquiry__c=i.id; 
                }  
                }
                
               updatelist.add(t); 
            }
            }
           
            
            
             
         }  
           
           
       } 
       if(updatelist.size()>0){
           list<task> finallist=new list<task>();
           for(task t:updatelist){
             finallist.add(t);  
           }
           update finallist;
       }
    }
    
     global void finish(Database.BatchableContext BC){
         
     }
    
}