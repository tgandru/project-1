public class InquiryDetailPage {
   public Inquiry__c iq{get; set;}
    public InquiryDetailPage(ApexPages.StandardController controller) {
        string iqid= ApexPages.currentPage().getParameters().get('id');
       iq=[select id, name, street__c,city__c,state__c,Country__c,Zip_Postal_Code__c from Inquiry__c where id=:iqid]; 
    }

   public pagereference saveNadd(){
        try{
            update iq;
            pageReference pf= new pagereference('/apex/IQDetail?id='+iq.id);
            return pf;
        }catch(exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()));
            return null;
        }
    }

}