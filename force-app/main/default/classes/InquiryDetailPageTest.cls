@isTest
private class InquiryDetailPageTest {

	private static testMethod void testmethod1() {
     Test.startTest();
         string   CampaignTypeId = [Select Id From RecordType Where SobjectType = 'Campaign' and Name = 'Sales'  limit 1].Id;
	    string   LeadTypeId = [Select Id From RecordType Where SobjectType = 'Lead' and Name = 'End User'  limit 1].Id;
       Campaign cmp= new Campaign(Name='Test Campaign for test class ',	IsActive=true,type='Email',StartDate=system.Today(),recordtypeid=CampaignTypeId);
       insert cmp;
       List<Lead> leadLst = new list<Lead>();
       
       lead le1=new lead(lastname='test lead 123',Company='My Lead Company',Email='mylead@sds.com',	MQL_Score__c='T1',Status='New',LeadSource='Others',recordtypeid=LeadTypeId);
	   	    insert le1;
	   
	        Inquiry__c iq=new Inquiry__c(Lead__c = le1.id,Product_Solution_of_Interest__c='Monitors',	 campaign__c = cmp.id);

	    insert iq; 
	    Pagereference pf= Page.IQDetail;
        pf.getParameters().put('id',iq.id);
        test.setCurrentPage(pf);
        
        apexPages.standardcontroller sc = new apexPages.standardcontroller(iq);
        InquiryDetailPage obj = new InquiryDetailPage(sc);
       
      obj.saveNadd();
    Test.stopTest();     
	}
	
		private static testMethod void testmethod2() {
     Test.startTest();
         string   CampaignTypeId = [Select Id From RecordType Where SobjectType = 'Campaign' and Name = 'Sales'  limit 1].Id;
	    string   LeadTypeId = [Select Id From RecordType Where SobjectType = 'Lead' and Name = 'End User'  limit 1].Id;
       Campaign cmp= new Campaign(Name='Test Campaign for test class ',	IsActive=true,type='Email',StartDate=system.Today(),recordtypeid=CampaignTypeId);
       insert cmp;
       List<Lead> leadLst = new list<Lead>();
       
       lead le1=new lead(lastname='test lead 123',Company='My Lead Company',Email='mylead@sds.com',	MQL_Score__c='T1',Status='New',LeadSource='Others',recordtypeid=LeadTypeId);
	   	    insert le1;
	   
	        Inquiry__c iq=new Inquiry__c(Lead__c = le1.id,Product_Solution_of_Interest__c='Monitors',	 campaign__c = cmp.id);

	    insert iq; 
	    Pagereference pf= Page.IQDetail;
        pf.getParameters().put('id',iq.id);
        test.setCurrentPage(pf);
       
        apexPages.standardcontroller sc = new apexPages.standardcontroller(iq);
        InquiryDetailPage obj = new InquiryDetailPage(sc);
        iq.Zip_Postal_Code__c='6661644116144161446131646116161616161611161616161611161161611616222222255525255541166161616161121641616145616196446149616161616';
      obj.saveNadd();
      
    Test.stopTest();     
	}
	

}