/*  This batch class is for To load the data from CampaignMember to Inquiry__c , to run this batch need member crateddate from and to*/
global class InquiryImportBatch implements Database.Batchable<sObject>,Database.Stateful {
          
  global List<Inquiry__c> newlstiq;
  global string susid;
  global Decimal  failcount ;
  global  map<string, string> PIMap;
   global  map<string, string> PFMap;
  global string sucesslist;
  global string faillist;
    global set<id> insid;
  global datetime fromd;
  global datetime tod;
  global InquiryImportBatch(){
     
      insid=new set<id>();
      faillist='Error , Fields \n';
        PIMap =new  map<string, string>();
      PIMap.put('Desktop Monitor','Monitors');
      PIMap.put('Professional Large Format Display 32','Monitors');
      PIMap.put('Desktop Color and Mono Laser Printers','Printers');
      PIMap.put('Multi-Function Printer/Copier','Printers');
      PIMap.put('Mobile PC','Mobile Computing (Laptops/2-in-1)');
      PIMap.put('Vitual Desktop Infrastructure Endpoints','VDI Endpoints');
      PIMap.put('Galaxy Tab & Note (WiFi only)','Tablets');
      PIMap.put('Memory and Storage','Memory & Storage');
      PIMap.put('Hotel and Hospitality TV','Hospitality TVs');
      PIMap.put('Enterprise Communication and WLAN','Wireless LAN');
       PIMap.put('Wireless LAN','Wireless LAN');
      PIMap.put('Mobile Phones','Mobile Phones');
       PIMap.put('Mobile Security','Mobile Security');
        PIMap.put('Displays &amp','Monitors');
         PIMap.put('Mobile Computing','Mobile Computing (Laptops/2-in-1)');
        PFMap =new  map<string, string>();
      PFMap.put('Desktop Monitor','IT');
      PFMap.put('Professional Large Format Display 32','IT');
      PFMap.put('Desktop Color and Mono Laser Printers','IT');
      PFMap.put('Multi-Function Printer/Copier','IT');
      PFMap.put('Mobile PC','IT');
      PFMap.put('Vitual Desktop Infrastructure Endpoints','IT');
      PFMap.put('Galaxy Tab & Note (WiFi only)','IT');
      PFMap.put('Memory and Storage','IT');
      PFMap.put('Hotel and Hospitality TV','IT');
      PFMap.put('Enterprise Communication and WLAN','IT');
      PFMap.put('Mobile Phones','Mobile');
       PFMap.put('Mobile Security','Mobile');
        PFMap.put('Displays &amp','IT');
         PFMap.put('Mobile Computing','IT');
         PIMap.put('Wireless LAN','IT');
   } 
   global Database.QueryLocator start(Database.BatchableContext BC){
       system.debug('****'+fromd);
        system.debug('****'+tod);
      return Database.getQueryLocator([select CreatedByID,CreatedBy.Name,Lead.MQL_Score__c,LastModifiedbyID,LastModifiedDate ,createddate,id,Name,lead.LeadSource,lead.SystemSource__c,DeviceType__c,Industry__c,LeadId,lead.Product_Information__c,lead.Customer_Comments__c,lead.Comments__c,ContactId,Lead.OwnerId,lead.Lead_Score__c,Lead_Score__c,Contact.OwnerID,Lead.Status,Lead.Reason__c,Product_Interest__c,MQLScore__c,Campaignid,Carrier__c,MQL_Comments__c,NumberofDevices__c,
                                        lead.Purchase_Timeframe__c,Marketing_Source_Most_Recent__c,Marketing_Source_Original__c,lead.email,contact.email,lead.Product_Interest__c,lead.Street,lead.city,lead.State,lead.country,lead.Postalcode ,
                                       Contact.MailingStreet,Contact.MailingCity,Contact.MailingState,Contact.MailingCountry,Contact.MailingPostalCode,Contact.AccountID,Contact.Product_Interest__c from CampaignMember where CreatedDate=LAST_90_DAYS and Inquiry_Created__c=false and lead.isconverted=false]);
   }

//CreatedDate=LAST_90_DAYS and Inquiry_Created__c=false and lead.isconverted=false
   global void execute(Database.BatchableContext BC,list<CampaignMember> scope){
       list<Inquiry__c> iqlist=new List<Inquiry__c>();
       
       for(CampaignMember c:scope){
           if(c.leadid!=null){
               
        if(c.lead.Status !='No Sale' && c.lead.Status !='Invalid' && c.lead.Status !='Unreachable' && c.lead.Status !='Passed to Carrier'){
            if(c.CreatedBy.Name=='Eloqua Marketing'&& c.MQLScore__c !=null){
                Inquiry__c iq = new Inquiry__c();
                           iq.Campaign_Member_Id__c=c.id;
                           iq.Campaign__c=c.CampaignID;
                          
                                 iq.MQL_Score__c=c.MQLScore__c;
                            
                           if(c.MQL_Comments__c !=null){
                              iq.MQL_Comments__c=c.MQL_Comments__c; 
                           }else if(c.lead.Customer_Comments__c !=null){
                              iq.MQL_Comments__c=c.lead.Customer_Comments__c; 
                           }
                         
                           iq.Device_Type__c=c.DeviceType__c;
                           if(c.NumberofDevices__c !=null){
                               iq.Number_of_Devices__c=decimal.valueOf(c.NumberofDevices__c);
                           }
                         string s='';
                           if(c.lead.Comments__c !=null){
                              iq.Inquiry_Comments__c=c.lead.Comments__c; 
                              s=s+c.lead.Comments__c;
                           }
                           if(c.lead.Product_Information__c !=null){
                               
                              iq.Inquiry_Comments__c =s+ '--'+c.lead.Product_Information__c; 
                           }
                        
                           iq.Industry__c=c.Industry__c;
                           iq.Marketing_Lead_Source_Most_Recent__c=c.Marketing_Source_Most_Recent__c;
                           iq.Marketing_Lead_Source_Original__c=c.Marketing_Source_Original__c;
                            iq.Source__c=c.lead.LeadSource;
                            iq.System_Source__c=c.lead.SystemSource__c;
                           
                                             if(c.lead.Purchase_Timeframe__c !=null){
                                               iq.Purchase_Timeframe_Months__c=decimal.valueOf(c.lead.Purchase_Timeframe__c);
                                           }
                                        
                                           iq.Lead__C=c.LeadID;
                                           iq.Inquiry_Status__c=c.lead.Status;
                                           if(c.lead.Status=='Invalid'){
                                               iq.Reason_invalid__c=c.lead.Reason__c;
                                           }else if(c.lead.Status=='No Sale'){
                                                iq.Reason_No_Sale__c=c.lead.Reason__c;
                                           }
                                           
                                           iq.OwnerID=c.Lead.OwnerID;
                                           iq.Street__c=c.lead.Street;
                                           iq.City__c=c.Lead.City;
                                           iq.State__c=c.Lead.State;
                                           iq.Zip_Postal_Code__c=c.lead.Postalcode;
                                           iq.Country__c=c.lead.country;
                                           iq.Eloqua_ID__c=c.lead.email; 
                                           if(c.Lead_Score__c !=null){
                                               iq.Lead_Score__c=c.Lead_Score__c;
                                           }else if(c.lead.Lead_Score__c !=null){
                                               iq.Lead_Score__c=c.lead.Lead_Score__c;
                                           }
                                           
                                           if(c.Product_Interest__c !=null){
                                              iq.Product_Solution_of_Interest__c=c.Product_Interest__c;
                                              
                                           }else if(c.LeadID !=null && c.Lead.Product_Interest__c !=null){
                                              
                                                iq.Product_Solution_of_Interest__c=c.Lead.Product_Interest__c;
                                           }else {
                                              
                                                iq.Product_Solution_of_Interest__c='No Product';
                                           }
                                    if(UserInfo.getProfileId()=='00e36000000OfxiAAC'){
                                         iq.createddate=c.createddate;
                                         iq.CreatedById=c.CreatedById;
                                         iq.LastModifiedDate=c.LastModifiedDate;
                                         iq.LastModifiedbyID=c.LastModifiedbyID;
                                       }
                                iqlist.add(iq); 
                
            }else if(c.CreatedBy.Name!='Eloqua Marketing'){
                Inquiry__c iq = new Inquiry__c();
                           iq.Campaign_Member_Id__c=c.id;
                           iq.Campaign__c=c.CampaignID;
                            
                                 iq.MQL_Score__c=c.MQLScore__c;
                                   
                            
                           
                           if(c.MQL_Comments__c !=null){
                              iq.MQL_Comments__c=c.MQL_Comments__c; 
                           }else if(c.lead.Customer_Comments__c !=null){
                              iq.MQL_Comments__c=c.lead.Customer_Comments__c; 
                           }
                         
                           iq.Device_Type__c=c.DeviceType__c;
                           if(c.NumberofDevices__c !=null){
                               iq.Number_of_Devices__c=decimal.valueOf(c.NumberofDevices__c);
                           }
                         string s='';
                           if(c.lead.Comments__c !=null){
                              iq.Inquiry_Comments__c=c.lead.Comments__c; 
                              s=s+c.lead.Comments__c;
                           }
                           if(c.lead.Product_Information__c !=null){
                               
                              iq.Inquiry_Comments__c =s+ '--'+c.lead.Product_Information__c; 
                           }
                        
                           iq.Industry__c=c.Industry__c;
                           iq.Marketing_Lead_Source_Most_Recent__c=c.Marketing_Source_Most_Recent__c;
                           iq.Marketing_Lead_Source_Original__c=c.Marketing_Source_Original__c;
                            iq.Source__c=c.lead.LeadSource;
                            iq.System_Source__c=c.lead.SystemSource__c;
                           
                                             if(c.lead.Purchase_Timeframe__c !=null){
                                               iq.Purchase_Timeframe_Months__c=decimal.valueOf(c.lead.Purchase_Timeframe__c);
                                           }
                                        
                                           iq.Lead__C=c.LeadID;
                                           iq.Inquiry_Status__c=c.lead.Status;
                                           if(c.lead.Status=='Invalid'){
                                               iq.Reason_invalid__c=c.lead.Reason__c;
                                           }else if(c.lead.Status=='No Sale'){
                                                iq.Reason_No_Sale__c=c.lead.Reason__c;
                                           }
                                           
                                           iq.OwnerID=c.Lead.OwnerID;
                                           iq.Street__c=c.lead.Street;
                                           iq.City__c=c.Lead.City;
                                           iq.State__c=c.Lead.State;
                                           iq.Zip_Postal_Code__c=c.lead.Postalcode;
                                           iq.Country__c=c.lead.country;
                                           iq.Eloqua_ID__c=c.lead.email; 
                                           if(c.Lead_Score__c !=null){
                                               iq.Lead_Score__c=c.Lead_Score__c;
                                           }else if(c.lead.Lead_Score__c !=null){
                                               iq.Lead_Score__c=c.lead.Lead_Score__c;
                                           }
                                           
                                           if(c.Product_Interest__c !=null){
                                              iq.Product_Solution_of_Interest__c=c.Product_Interest__c;
                                              
                                           }else if(c.LeadID !=null && c.Lead.Product_Interest__c !=null){
                                              
                                                iq.Product_Solution_of_Interest__c=c.Lead.Product_Interest__c;
                                           }else {
                                              
                                                iq.Product_Solution_of_Interest__c='No Product';
                                           }
                                    if(UserInfo.getProfileId()=='00e36000000OfxiAAC'){
                                         iq.createddate=c.createddate;
                                         iq.CreatedById=c.CreatedById;
                                         iq.LastModifiedDate=c.LastModifiedDate;
                                         iq.LastModifiedbyID=c.LastModifiedbyID;
                                       }
                                iqlist.add(iq); 
                
                }
                           
             }
         }  
         
           
          
           
       }
        if(iqlist.size()>0){
          Database.SaveResult[] srList = Database.insert(iqlist, false);
          set<id> susid=new set<id>();
          map<id,id> cmpiq=new  map<id,id>();
            for(Database.SaveResult sr : srList){
                if (sr.isSuccess()) {
                    insid.add(sr.getId());
                    susid.add(sr.getID());
                }else{
                     for(Database.Error err : sr.getErrors()) {
                         string er=err.getMessage();
                         system.debug('error'+er);
                         string flds=string.valueOf(err.getFields());
                         string row=er+flds+'\n';
                         faillist=faillist+row;
                         
                     }

                }
                
            }
            
            if(susid.size()>0){
                list<Inquiry__c> susiqlist=new  list<Inquiry__c>([select id,Campaign_Member_Id__c from Inquiry__c where id in:susid]);
              for(Inquiry__c i:susiqlist){
                   cmpiq.put(i.Campaign_Member_Id__c,i.ID);
                }
                  list<CampaignMember> cmplist=new list<CampaignMember>([select id,	Inquiry_Id__c,Inquiry_Created__c from CampaignMember where id in:cmpiq.keySet()]);      
                  for(CampaignMember c:cmplist){
                      c.Inquiry_Created__c=true;
                      c.Inquiry_Id__c=cmpiq.get(c.id);
                }
                  
                  update cmplist; 
          }
            
            
      }
     
     
   }

   global void finish(Database.BatchableContext BC){
     
     /*  map<id,List<id>> cmpiq=new  map<id,list<id>>();
     
      if(insid.size()>0){
          list<Inquiry__c> susiqlist=new  list<Inquiry__c>([select id,Campaign_Member_Id__c from Inquiry__c where id in:insid]);
              for(Inquiry__c i:susiqlist){
                   if(cmpiq.containsKey(i.Campaign_Member_Id__c)) {
                        		List<Id> iId = cmpiq.get(i.Campaign_Member_Id__c);
                        		iId.add(i.Id);
                        		cmpiq.put(i.Campaign_Member_Id__c, iId);
                        	} else {
                        		cmpiq.put(i.Campaign_Member_Id__c, new List<Id> { i.Id });
                	}
                }
          list<CampaignMember> cmplist=new list<CampaignMember>([select id,	Inquiry_Id__c,Inquiry_Created__c from CampaignMember where id in:cmpiq.keySet()]);      
          for(CampaignMember c:cmplist){
              c.Inquiry_Created__c=true;
              list<id> iqinfo=cmpiq.get(c.id);
              string ids='';
              for(id i:iqinfo){
                  ids=ids+i+',';
              }
              c.Inquiry_Id__c=ids;
        }
          
          update cmplist;    
      } */
    
    Blob b = Blob.valueof(faillist);
        Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
        efa.setFileName('FailRecords.CSV');
        efa.setBody(b);
        
      
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        list<string> emailaddresses = new list<string>{'vijayskamani@gmail.com'};
        email.setSubject( 'FailRecords  Data' );
        email.setToAddresses(emailaddresses);
        string body ='List of FailRecords '+'\n\n'+'Thanks';
        email.setPlainTextBody( 'Check the Records ' );
        email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
       
       
   }
   
   
}