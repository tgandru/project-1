@isTest
private class InquiryImportBatchTest {
 private static testMethod void testmethod00() {
         string   CampaignTypeId = [Select Id From RecordType Where SobjectType = 'Campaign' and Name = 'Sales'  limit 1].Id;
      string   LeadTypeId = [Select Id From RecordType Where SobjectType = 'Lead' and Name = 'End User'  limit 1].Id;
       Campaign cmp= new Campaign(Name='Test Campaign for test class ',  IsActive=true,type='Email',StartDate=system.Today(),recordtypeid=CampaignTypeId);
       insert cmp;
       List<Lead> leadLst = new list<Lead>();
       
       lead le1=new lead(lastname='test lead 123',Company='My Lead Company',Email='mylead@sds.com',  MQL_Score__c='T1',Status='New',LeadSource='Others',recordtypeid=LeadTypeId);
     leadLst.add(le1);
      lead le2=new lead(lastname='test lead 1232',Company='My Lead Company2',Email='mylead@sds2.com',  MQL_Score__c='A+',Status='No Sale',LeadSource='Others',recordtypeid=LeadTypeId);
      leadLst.add(le2);
      lead le3=new lead(lastname='test lead 1233',Company='My Lead Company3',Email='mylead@sds3.com',Status='Account / Oppty',Division__c='IT',ProductGroup__c='OFFICE',LeadSource='Others',recordtypeid=LeadTypeId);
     leadLst.add(le3);
      insert leadLst;
      list<CampaignMember> CampaignMemberLst = new list<CampaignMember>();
      list<Task> tsks=new list<Task>();
      for(Lead l:leadLst){
          CampaignMember cm=new CampaignMember(Leadid = l.id,  status='Sent',  Product_Interest__c='Desktop Monitor',  MQLScore__c='D4', campaignid = cmp.id,Inquiry_Created__c=false);
           CampaignMemberLst.add(cm);
      }
      insert CampaignMemberLst;
      Test.startTest();
      InquiryImportBatch b=new InquiryImportBatch();
        database.executeBatch(b);

       Test.stopTest();
      
  }
	private static testMethod void testmethod1() {
         
	    Test.startTest();
	     Profile p = [SELECT Id FROM Profile WHERE Name='Integration User']; 
      User u = new User(Alias = 'standt', Email='Integrationuser@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Eloqua Marketing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName='eloqtestclass@testorg.com');

      System.runAs(u) 
      {string   CampaignTypeId = [Select Id From RecordType Where SobjectType = 'Campaign' and Name = 'Sales'  limit 1].Id;
	    string   LeadTypeId = [Select Id From RecordType Where SobjectType = 'Lead' and Name = 'End User'  limit 1].Id;
       Campaign cmp= new Campaign(Name='Test Campaign for test class ',	IsActive=true,type='Email',StartDate=system.Today(),recordtypeid=CampaignTypeId);
       insert cmp;
       List<Lead> leadLst = new list<Lead>();
       
       lead le1=new lead(lastname='test lead 123',Company='My Lead Company',Email='mylead@sds.com',	MQL_Score__c='T1',Status='New',LeadSource='Others',recordtypeid=LeadTypeId);
	   leadLst.add(le1);
	    lead le2=new lead(lastname='test lead 1232',Company='My Lead Company2',Email='mylead@sds2.com',	MQL_Score__c='A+',Status='No Sale',LeadSource='Others',recordtypeid=LeadTypeId);
	    leadLst.add(le2);
	    lead le3=new lead(lastname='test lead 1233',Company='My Lead Company3',Email='mylead@sds3.com',Status='Account / Oppty',Division__c='IT',ProductGroup__c='OFFICE',LeadSource='Others',recordtypeid=LeadTypeId);
	   leadLst.add(le3);
	    insert leadLst;
	    list<CampaignMember> CampaignMemberLst = new list<CampaignMember>();
	    list<Task> tsks=new list<Task>();
	    for(Lead l:leadLst){
	        CampaignMember cm=new CampaignMember(Leadid = l.id,  status='Sent',	Product_Interest__c='Desktop Monitor',	MQLScore__c='D4', campaignid = cmp.id,Inquiry_Created__c=false);
    		   CampaignMemberLst.add(cm);
	    }
	    insert CampaignMemberLst;
       
	    InquiryImportBatch b=new InquiryImportBatch();
        database.executeBatch(b);

	     Test.stopTest();
      }
	}
	
		private static testMethod void testmethod2() {
        string   CampaignTypeId = [Select Id From RecordType Where SobjectType = 'Campaign' and Name = 'Sales'  limit 1].Id;
	    string   LeadTypeId = [Select Id From RecordType Where SobjectType = 'Lead' and Name = 'End User'  limit 1].Id;
       Campaign cmp= new Campaign(Name='Test Campaign for test class ',	IsActive=true,type='Email',StartDate=system.Today(),recordtypeid=CampaignTypeId);
       insert cmp;
       List<Lead> leadLst = new list<Lead>();
       
       lead le1=new lead(lastname='test lead 123',Company='My Lead Company',Email='mylead@sds.com',	MQL_Score__c='T1',Status='New',LeadSource='Others',recordtypeid=LeadTypeId);
	   leadLst.add(le1);
	    lead le2=new lead(lastname='test lead 1232',Company='My Lead Company2',Email='mylead@sds2.com',	MQL_Score__c='A+',Status='No Sale',LeadSource='Others',recordtypeid=LeadTypeId);
	    leadLst.add(le2);
	    lead le3=new lead(lastname='test lead 1233',Company='My Lead Company3',Email='mylead@sds3.com',Status='Account / Oppty',Division__c='IT',ProductGroup__c='OFFICE',LeadSource='Others',recordtypeid=LeadTypeId);
	   leadLst.add(le3);
	    insert leadLst;  
         Test.startTest();
	    InquiryImportFromLeadBatch b=new InquiryImportFromLeadBatch();
        database.executeBatch(b);

	     Test.stopTest();
     }
     
     	private static testMethod void testmethod3() {
        string   CampaignTypeId = [Select Id From RecordType Where SobjectType = 'Campaign' and Name = 'Sales'  limit 1].Id;
	    string   LeadTypeId = [Select Id From RecordType Where SobjectType = 'Lead' and Name = 'End User'  limit 1].Id;
       Campaign cmp= new Campaign(Name='Test Campaign for test class ',	IsActive=true,type='Email',StartDate=system.Today(),recordtypeid=CampaignTypeId);
       insert cmp;
       List<Lead> leadLst = new list<Lead>();
       
       lead le1=new lead(lastname='test lead 123',Company='My Lead Company',Email='mylead@sds.com',	MQL_Score__c='T1',Status='New',LeadSource='Others',recordtypeid=LeadTypeId);
	   leadLst.add(le1);
	    lead le2=new lead(lastname='test lead 1232',Company='My Lead Company2',Email='mylead@sds2.com',	MQL_Score__c='A+',Status='No Sale',LeadSource='Others',recordtypeid=LeadTypeId);
	    leadLst.add(le2);
	    lead le3=new lead(lastname='test lead 1233',Company='My Lead Company3',Email='mylead@sds3.com',Status='Account / Oppty',Division__c='IT',ProductGroup__c='OFFICE',LeadSource='Others',recordtypeid=LeadTypeId);
	   leadLst.add(le3);
	    insert leadLst;  
	    list<Inquiry__C> iqlist = new list<Inquiry__C>();
	    list<Task> tsks=new list<Task>();
	    for(Lead l:leadLst){
	        Inquiry__c cm=new Inquiry__c(Lead__c = l.id, 	Product_Solution_of_Interest__c='Desktop Monitor', campaign__c = cmp.id);
	        Task t=new task(ActivityDate=System.today()+2,Subject = 'Test Task on Lead',Priority = 'High',Status = 'Open',WhoID=l.id,IsReminderSet = true,ReminderDateTime =  System.now()+2);
    		   iqlist.add(cm);
    		    tsks.add(t);
	    }
	    insert iqlist;
	    insert tsks;
	    
	    
         Test.startTest();
	    InquiryActivityAttachBatch b=new InquiryActivityAttachBatch();
        database.executeBatch(b);

	     Test.stopTest();
     }
   	private static testMethod void testmethod4() {
         Test.startTest();
	     Profile p = [SELECT Id FROM Profile WHERE Name='Integration User']; 
      User u = new User(Alias = 'standt', Email='Integrationuser@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Eloqua Marketing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName='eloqtestclass@testorg.com');

      System.runAs(u) 
      {string   CampaignTypeId = [Select Id From RecordType Where SobjectType = 'Campaign' and Name = 'Sales'  limit 1].Id;
	    string   LeadTypeId = [Select Id From RecordType Where SobjectType = 'Lead' and Name = 'End User'  limit 1].Id;
       Campaign cmp= new Campaign(Name='Test Campaign for test class ',	IsActive=true,type='Email',StartDate=system.Today(),recordtypeid=CampaignTypeId);
       insert cmp;
       List<Lead> leadLst = new list<Lead>();
       
       lead le1=new lead(lastname='test lead 123',Company='My Lead Company',Email='mylead@sds.com',	MQL_Score__c='T1',Status='New',LeadSource='Others',recordtypeid=LeadTypeId);
	   leadLst.add(le1);
	    lead le2=new lead(lastname='test lead 1232',Company='My Lead Company2',Email='mylead@sds2.com',	MQL_Score__c='A+',Status='Assigned',LeadSource='Others',recordtypeid=LeadTypeId);
	    leadLst.add(le2);
	    lead le3=new lead(lastname='test lead 1233',Company='My Lead Company3',Email='mylead@sds3.com',Status='Pursuit',Division__c='IT',ProductGroup__c='OFFICE',LeadSource='Others',recordtypeid=LeadTypeId);
	   leadLst.add(le3);
	    insert leadLst;
	     date d1=system.today();
         date d2=system.today();
         InquiryImportFromLeadBatchNonMQLELQ b=new InquiryImportFromLeadBatchNonMQLELQ(d1,d2);
        database.executeBatch(b);
	   
	     Test.stopTest();
     }
       
   	}  
}