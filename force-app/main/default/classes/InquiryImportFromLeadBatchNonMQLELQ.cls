global class InquiryImportFromLeadBatchNonMQLELQ implements Database.Batchable<sObject>,Database.Stateful {
          
  global List<Inquiry__c> newlstiq;
  global string susid;
  global Decimal  failcount ;
  global  map<string, string> PIMap;
   global  map<string, string> PFMap;
  global string sucesslist;
  global string faillist;
    global set<id> insid;
  global datetime fromd;
  global datetime tod;
  global string dummycamp;
  global InquiryImportFromLeadBatchNonMQLELQ(date fr,date td ){
     dummycamp=Label.Dummy_Campaign;
     fromd=DateTime.newInstance(fr.year(),fr.month(),fr.day(),-4,00,00);
     tod=DateTime.newInstance(td.year(),td.month(),td.day(),19,59,59);
      insid=new set<id>();
      faillist='Error , Fields \n';
      sucesslist='ID \n';
        PIMap =new  map<string, string>();
      PIMap.put('Desktop Monitor','Monitors');
      PIMap.put('Professional Large Format Display 32','Monitors');
      PIMap.put('Desktop Color and Mono Laser Printers','Printers');
      PIMap.put('Multi-Function Printer/Copier','Printers');
      PIMap.put('Mobile PC','Mobile Computing (Laptops/2-in-1)');
      PIMap.put('Vitual Desktop Infrastructure Endpoints','VDI Endpoints');
      PIMap.put('Galaxy Tab & Note (WiFi only)','Tablets');
      PIMap.put('Memory and Storage','Memory & Storage');
      PIMap.put('Hotel and Hospitality TV','Hospitality TVs');
      PIMap.put('Enterprise Communication and WLAN','Wireless LAN');
       PIMap.put('Wireless LAN','Wireless LAN');
      PIMap.put('Mobile Phones','Mobile Phones');
       PIMap.put('Mobile Security','Mobile Security');
        PIMap.put('Displays &amp','Monitors');
         PIMap.put('Mobile Computing','Mobile Computing (Laptops/2-in-1)');
        PFMap =new  map<string, string>();
      PFMap.put('Desktop Monitor','IT');
      PFMap.put('Professional Large Format Display 32','IT');
      PFMap.put('Desktop Color and Mono Laser Printers','IT');
      PFMap.put('Multi-Function Printer/Copier','IT');
      PFMap.put('Mobile PC','IT');
      PFMap.put('Vitual Desktop Infrastructure Endpoints','IT');
      PFMap.put('Galaxy Tab & Note (WiFi only)','IT');
      PFMap.put('Memory and Storage','IT');
      PFMap.put('Hotel and Hospitality TV','IT');
      PFMap.put('Enterprise Communication and WLAN','IT');
      PFMap.put('Mobile Phones','Mobile');
       PFMap.put('Mobile Security','Mobile');
        PFMap.put('Displays &amp','IT');
         PFMap.put('Mobile Computing','IT');
         PIMap.put('Wireless LAN','IT');
   } 
   global Database.QueryLocator start(Database.BatchableContext BC){
       system.debug('****'+fromd);
        system.debug('****'+tod);
      return Database.getQueryLocator([select createdbyid,LastModifiedDate,LastModifiedbyID,createdby.name,Carrier__c,id,SystemSource__c,	LeadSource,status,Product_Information__c,	NumberofDevices__c,AssignedCampaignID__c,MQL_Score__c,createddate,ownerid,Product_Interest__c,Purchase_Timeframe__c,Comments__c,Customer_Comments__c,
                                              Industry,Street,city,state,Postalcode,country,email,Lead_Score__c,DeviceType__c from lead where (CreatedDate>=:fromd and CreatedDate<=:tod) and createdby.name='Eloqua Marketing' and Recordtype.Name!='HTV'  and Migrated_IQ__c=null and MQL_Score__c=null ]);
   }

//createddate=last_90_days and createdby.name!='Eloqua Marketing' and Recordtype.Name!='HTV' and AssignedCampaignID__c=null and Migrated_IQ__c=null
   global void execute(Database.BatchableContext BC,list<lead> scope){
       list<Inquiry__c> iqlist=new List<Inquiry__c>();
      for(lead l:scope){
         if(l.status=='New' || l.Status=='Assigned' || l.Status=='Pursuit' || l.status=='Active'){
              Inquiry__c iq = new Inquiry__c();
           iq.Campaign_Member_Id__c=l.id;
           if(l.AssignedCampaignID__c !=null){
                 iq.Campaign__c=l.AssignedCampaignID__c;
           }else{
               iq.Campaign__c=dummycamp;
           }
         
           iq.MQL_Score__c=l.MQL_Score__c;
           if(l.Customer_Comments__c !=null){
              iq.MQL_Comments__c=l.Customer_Comments__c; 
           }
            string s='';
           if(l.Comments__c !=null){
              iq.Inquiry_Comments__c=l.Comments__c; 
              s=s+l.Comments__c; 
           }
           if(l.Product_Information__c !=null){
              iq.Inquiry_Comments__c=s+'--'+l.Product_Information__c; 
           }
         
           iq.Device_Type__c=l.DeviceType__c;
           if(l.NumberofDevices__c !=null){
               iq.Number_of_Devices__c=decimal.valueOf(l.NumberofDevices__c);
           }
        
           iq.Industry__c=l.Industry;
                    if(l.Purchase_Timeframe__c !=null){
                               iq.Purchase_Timeframe_Months__c=decimal.valueOf(l.Purchase_Timeframe__c);
                           }
                        
                           iq.Lead__C=l.id;
                                         if(l.Status=='Pursuit'){
                                               iq.Inquiry_Status__c='In Pursuit';
                                           }else{
                                                iq.Inquiry_Status__c=l.Status;
                                           }
                           if(l.Status=='Invalid'){
                               iq.Reason_invalid__c=l.Reason__c;
                           }else if(l.Status=='No Sale'){
                                iq.Reason_No_Sale__c=l.Reason__c;
                           }
                           iq.Source__c=l.LeadSource;
                           iq.System_Source__c=l.SystemSource__c;
                           iq.OwnerID=l.OwnerID;
                           iq.Street__c=l.Street;
                           iq.City__c=l.City;
                           iq.State__c=l.State;
                           iq.Zip_Postal_Code__c=l.Postalcode;
                           iq.Country__c=l.country;
                           iq.Eloqua_ID__c=l.email; 
                           if(l.Lead_Score__c !=null){
                               iq.Lead_Score__c=l.Lead_Score__c;
                           }
                           
                           if(l.Product_Interest__c !=null){
                              iq.Product_Solution_of_Interest__c=l.Product_Interest__c;
                              
                           }else {
                              
                                iq.Product_Solution_of_Interest__c='No Product';
                           }
               if(UserInfo.getProfileId()=='00e36000000OfxiAAC'){
                 iq.createddate=l.createddate;
                 iq.createdbyid=l.createdbyid;
                  iq.LastModifiedDate=l.LastModifiedDate;
                  iq.LastModifiedbyID=l.LastModifiedbyID;
               }
               
               iqlist.add(iq);
               sucesslist=sucesslist+l.id;
            
           }
             
         } 
         
        if(iqlist.size()>0){
            
            
         set<id> sid=new set<id>();
          Database.SaveResult[] srList = Database.insert(iqlist, false);
            for(Database.SaveResult sr : srList){
                if (sr.isSuccess()) {
                    sid.add(sr.getId());
                    sucesslist=sucesslist+sr.getId();
                }else{
                     for(Database.Error err : sr.getErrors()) {
                         string er=err.getMessage();
                         system.debug('error'+er);
                         string flds=string.valueOf(err.getFields());
                         string row=er+flds+'\n';
                         faillist=faillist+row;
                         
                     }

                }
                
            }
            
            
             map<id,id> ldiq=new map<id,id>();
                   if(sid.size()>0){
                      list<Inquiry__c> susiqlist=new  list<Inquiry__c>([select id,Campaign_Member_Id__c from Inquiry__c where id in:sid]);
                          for(Inquiry__c i:susiqlist){
                               ldiq.put(i.Campaign_Member_Id__c,i.id);
                            }
                      list<lead> cmplist=new list<lead>([select id,	Migrated_IQ__c  from lead where id in:ldiq.keySet()]);      
                      for(lead c:cmplist){
                          c.Migrated_IQ__c=ldiq.get(c.id);
                          
                    }
                      
                      update cmplist;    
                  }
          
      } 
      
      
      
      }
   

   global void finish(Database.BatchableContext BC){
    
      Blob b = Blob.valueof(faillist);
        Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
        efa.setFileName('FailRecords.CSV');
        efa.setBody(b);
        
      
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        list<string> emailaddresses = new list<string>{'vijayskamani@gmail.com'};
        email.setSubject( 'FailRecords  Data' );
        email.setToAddresses(emailaddresses);
        string body ='List of FailRecords '+'\n\n'+'Thanks';
        email.setPlainTextBody( 'Check the Records ' );
        email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
      
   } 
   
}