@isTest
private class InquiryImportFromLeadBatchTest {

	private static testMethod void testmethod2() {
        string   CampaignTypeId = [Select Id From RecordType Where SobjectType = 'Campaign' and Name = 'Sales'  limit 1].Id;
	    string   LeadTypeId = [Select Id From RecordType Where SobjectType = 'Lead' and Name = 'End User'  limit 1].Id;
       Campaign cmp= new Campaign(Name='Test Campaign for test class ',	IsActive=true,type='Email',StartDate=system.Today(),recordtypeid=CampaignTypeId);
       insert cmp;
       List<Lead> leadLst = new list<Lead>();
       
       lead le1=new lead(lastname='test lead 123',Company='My Lead Company',Email='mylead@sds.com',	MQL_Score__c='T1',Status='New',LeadSource='Others',recordtypeid=LeadTypeId);
	   leadLst.add(le1);
	    lead le2=new lead(lastname='test lead 1232',Company='My Lead Company2',Email='mylead@sds2.com',	MQL_Score__c='A+',Status='No Sale',LeadSource='Others',recordtypeid=LeadTypeId);
	    leadLst.add(le2);
	    lead le3=new lead(lastname='test lead 1233',Company='My Lead Company3',Email='mylead@sds3.com',Status='Account / Oppty',Division__c='IT',ProductGroup__c='OFFICE',LeadSource='Others',recordtypeid=LeadTypeId);
	   leadLst.add(le3);
	    insert leadLst;  
         Test.startTest();
	    InquiryImportFromLeadBatch b=new InquiryImportFromLeadBatch();
        database.executeBatch(b);

	     Test.stopTest();
     }
}