public class InquiryOppCreation {
   public opportunity opp{get; set;}
   public List<SelectOption> DivLst;
   //public List<SelectOption> PgLst;
   public String selectedDiv{get;set;}
   public String selectedPG {get;set;}
    public String selectedpb{get;set;}
   public string mqlid;
        string id;
      public Inquiry__c iq {get; set;}
    public InquiryOppCreation(ApexPages.StandardController controller) {
       opp=new Opportunity();
       
                 if(ApexPages.currentPage().getParameters().get('id') != null){
                       id = ApexPages.currentpage().getParameters().get('id');
                       
                    }
        
             if(ApexPages.currentPage().getParameters().get('mqlid') != null){
                      mqlid= ApexPages.currentPage().getParameters().get('mqlid');
                     
                    }
    }
    
    public List<SelectOption> getPriceBookList(){
        System.debug('## currentOppty.Division__c'+selectedDiv);
        List<SelectOption> recordTypeOptions = new List<SelectOption>();
        List<Pricebook2> pbRecords = [SELECT Id, Name, Division__c,IsActive FROM Pricebook2 where Division__c=:selectedDiv and IsActive=true ORDER BY Name ASC];
        set<Id> pricebookIDs = new set<Id>(); 
        for(PriceBook2 pb: pbRecords){
            pricebookIDs.add(pb.Id);
        }

        List<UserRecordAccess> listUserRecordAccess = [SELECT RecordId, HasReadAccess, HasEditAccess, HasDeleteAccess FROM UserRecordAccess WHERE UserId=:UserInfo.getUserId() AND RecordId IN: pricebookIDs];

        Map<Id,boolean> accessMap = new Map<Id,boolean>();

        for(UserRecordAccess recAccess : listUserRecordAccess){
            accessMap.put(recAccess.RecordId, recAccess.HasReadAccess);
        }
        
        
        recordTypeOptions.add(new selectOption( '', '-Select a Price Book-'));
        for(Pricebook2 R : pbRecords){
            if(accessMap.get(R.Id)){
                recordTypeOptions.add(new SelectOption(R.Id, R.Name));
            }
        }
        
        if(recordTypeOptions.size() == 1){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING, 'You do not have a price book set up.  Please contact customer support.');
            ApexPages.addMessage(myMsg); 
            return null;  
        }
        else {
             System.debug('## recordTypeOptions'+recordTypeOptions);
            return recordTypeOptions;
        }

    }
    
    
    public List<SelectOption> getDivLst(){
    Schema.DescribeFieldResult f = Opportunity.Division__c.getDescribe();
    List<SelectOption> DivLst=new List<SelectOption>();
     DivLst.add(new SelectOption('--None--','--None--'));
        List<Schema.PicklistEntry> p = f.getPicklistValues();
           for(Schema.PicklistEntry e:p){
            DivLst.add(new SelectOption(e.getLabel(),e.getValue()));
        }
        return DivLst;
    }
   public void PgLst(){
         if(selectedDiv !=null){
               string recname;
               if(selectedDiv=='IT'){
                   recname='IT';
               }else if(selectedDiv=='Mobile'){
                   recname='Mobile';
               }else if(selectedDiv=='SBS'){
                   recname='Services';
               }else if(selectedDiv=='W/E'){
                   recname='Wireless Enterprise';
               }
          opp.RecordTypeId=[Select Id, Name from RecordType Where sobjectType = 'Opportunity' and Name=:recname].Id;
         }
         
    }
  public pagereference saveopp(){
  //system.debug('****'+iq.Account__c);
  if(selectedDiv ==null || selectedDiv=='--None--'){
    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please Select Division.'));
    return null;
  }
   if(opp.ProductGroupTest__c==null || selectedDiv=='--None--'){
    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please Select Product Group.'));
    return null;
  } 
  if(selectedpb==null || selectedpb=='-Select a Price Book-'){
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please Select PriceBook .'));
    return null; 
  }
  system.debug('**selectedDiv**'+selectedDiv);
  if(mqlid !=null){
  iq=[select id,Account__c,Contact__c,IsActive__c, Campaign__c,Inquiry_Status__c,SQL_End_Date__c,SQO_Start_Date__c,From_Lead__c,Source__c,PRM_Deal_Req_ID__c,PRM_Project_Registration_Detail__c,Last_Campaign__c	 from Inquiry__c where id=:mqlid];
   Date closeDate = [Select EndDate From Period Where type = 'Quarter' and StartDate = THIS_FISCAL_QUARTER].EndDate;
             
             String source='';
             if(iq.Source__c !=null){
                 source=iq.Source__c;
             }else{
                 source='Sales Prospecting';
             }
             
                     
                       opp.Accountid=iq.Account__c;
                       opp.Name=iq.Account__c;
                       if(iq.Last_Campaign__c !=null){
                            opp.CampaignId=iq.Last_Campaign__c;
                       }else{
                            opp.CampaignId=iq.Campaign__c;
                       }
                      
                       opp.Division__c=selectedDiv;
                       opp.StageName = 'Identified';
                       opp.CloseDate =closeDate ; 
                       opp.pricebook2Id = selectedpb; 
                       opp.LeadSource=source;
                       opp.PRM_Deal_Req_ID__c=iq.PRM_Deal_Req_ID__c;
                       opp.Inquiry_ID__c=iq.id;
                       opp.PRM_Project_Registration_Detail__c=iq.PRM_Project_Registration_Detail__c;
                     system.debug('**New Opp Detail ****'+opp);
                      insert opp;
                      Id oppid=opp.ID;
                      iq.Opportunity__c=oppid;
                      iq.SQL_End_Date__c=system.today();
                      iq.SQO_Start_Date__c=system.today();
                       iq.Is_Converted__c=true;
                      
                      system.debug('1.Status'+ iq.Inquiry_Status__c);
                      if( (iq.Inquiry_Status__c != 'Account/Opportunity') &&  (iq.Inquiry_Status__c != 'Opportunity')){
                            if(iq.From_Lead__c==true){
                                  iq.Inquiry_Status__c='Account/Opportunity';
                            }else if(iq.From_Lead__c==false){
                                  iq.Inquiry_Status__c='Opportunity';
                            }
                        
                      }
                      system.debug('2.Status'+ iq.Inquiry_Status__c);
                    update iq ;
                     updaterelatedtaskrecords(iq);//Added By Vijay on 5/15
      }
  
        PageReference retpage= new PageReference('/' + opp.id);            
   
     return retpage;
  }
  
  Public void updaterelatedtaskrecords(Inquiry__c iq){//Added By Vijay on 5/15 
   //This Method is for Attaching the Activites under inquiry to New Account/Opportunity
        List<task> tk= new list<task>([select id,whatId,whoID,Inquiry__c from Task where whatid=:iq.id]);
         List<Event> evn=new list<Event>([select id,whatId,whoID,Inquiry__c from Event where whatid=:iq.id]);
        if(tk.size()>0){
            for(task t:tk){
                t.Inquiry__c=iq.id;
                t.whatid=iq.Opportunity__c;
                t.whoid=iq.contact__c;
            }
            
            Database.update(tk,false);
            
        }
         if(evn.size()>0){
            for(Event e:evn){
                e.Inquiry__c=iq.id;
                e.whatid=iq.Opportunity__c;
                e.whoid=iq.contact__c;
            }
            
            Database.update(evn,false);
            
        }
        
        
    }
    
    
    Public pagereference createOpportunityforHA(){
       if(ApexPages.currentPage().getParameters().get('mqlid') != null){
           String recid=ApexPages.currentPage().getParameters().get('mqlid');
           Inquiry__c inq=[select id,RecordType.Name,Account__c,Contact__c,Last_Campaign__c,Contact__r.AccountId,IsActive__c,Project_Name__c,Project_Street__c,Project_City__c,Project_State__c,Project_Zipcode__c, Campaign__c,Inquiry_Status__c,SQL_End_Date__c,SQO_Start_Date__c,From_Lead__c,Source__c,PRM_Deal_Req_ID__c,PRM_Project_Registration_Detail__c from Inquiry__c where id=:recid];
          if(inq.RecordType.Name=='HA'){
              if(inq.Contact__r.AccountId !=null){
                    Date closeDate = [Select EndDate From Period Where type = 'Quarter' and StartDate = THIS_FISCAL_QUARTER].EndDate;
                     Opportunity  haopp=new Opportunity();
                       haopp.Accountid=inq.Contact__r.AccountId;
                       haopp.Name=inq.Account__c;
                       haopp.CampaignId=inq.Last_Campaign__c;
                       haopp.CloseDate =closeDate ;
                       haopp.Project_Name__c=inq.Project_Name__c;
                       haopp.Project_Street__c=inq.Project_Street__c;
                       haopp.Project_City__c=inq.Project_City__c;
                       haopp.Project_State__c=inq.Project_State__c;
                       haopp.Project_Zip_Code__c=inq.Project_Zipcode__c;
                       haopp.StageName = 'Identified';
                      if(!Test.isRunningTest()){
                       insert haopp;
                       inq.Opportunity__c=haopp.id;
                      }
                       inq.SQL_End_Date__c=system.today();
                       inq.SQO_Start_Date__c=system.today();
                       inq.Is_Converted__c=true;
                       inq.Inquiry_Status__c='Account/Opportunity';
                       
                       update inq;
                       
                       PageReference retpage= new PageReference('/' + haopp.id);            
   
                       return retpage;
              }else{
                   ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Contact Is Missing On this Inquiry .'));
              return null;
              }
          }
           
       }
    
      return null;     
         
    }
    
    
 
}