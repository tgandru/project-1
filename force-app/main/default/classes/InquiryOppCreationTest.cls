@isTest(seeAllData=true)
private class InquiryOppCreationTest {

	private static testMethod void testmethod1() {
             Test.startTest();
	    Account newAccount= TestDataUtility.createAccount(TestDataUtility.generateRandomString(5));
        newAccount.name ='Test Account';
        //acc.recordTypeId = endUserRT;
        newAccount.SAP_Company_Code__c='99999888';
        insert newAccount;
        Zipcode_Lookup__c z=new Zipcode_Lookup__c(City_Name__c='Ridgefield Park', Country_Code__c='US', State_Code__c='NJ', State_Name__c='New Jersey', Name='07660');
        insert z;
        
         string   CampaignTypeId = [Select Id From RecordType Where SobjectType = 'Campaign' and Name = 'Sales'  limit 1].Id;
	    string   LeadTypeId = [Select Id From RecordType Where SobjectType = 'Lead' and Name = 'End User'  limit 1].Id;
       Campaign cmp= new Campaign(Name='Test Campaign for test class ',	IsActive=true,type='Email',StartDate=system.Today(),recordtypeid=CampaignTypeId);
       insert cmp;
        Id endCustomer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('End Customer').getRecordTypeId();
        Account acc1 = new account(recordtypeId=endCustomer,Name ='mqlinquirytest', Type='Customer',BillingStreet ='85 Challenger Road',BillingCity='Ridgefield Park',BillingState='NJ',BillingCountry='US',BillingPostalCode='07660');
        insert acc1;
        list<contact> cont=new list<contact>();
         contact con = new contact( LastName='contactinq',Accountid=acc1.id,Email='oppcreationclass@conatc926.com',MQL_Score__c='T1');
          insert con;
         Inquiry__c iq=new Inquiry__c(Contact__c = con.id,Product_Solution_of_Interest__c='Monitors',	 campaign__c = cmp.id);
         insert iq;
         
         task t = new task();
         t.subject='Subject';
         t.whatid=iq.id;
         t.Activity_type__c='Training';
         t.ownerid=userinfo.getUserId();
         insert t;
         
         event e = new event();
         e.subject='Subject';
         e.whatid=iq.id;
         e.Activity_type__c='Meeting';
         e.ownerid=userinfo.getUserId();
         e.StartDateTime=system.now();
         e.EndDateTime=system.now();
         insert e;
         
         Pricebook2 pbk1 = [select id,name from Pricebook2 where name='IT Distributor'  limit 1];
      
         PageReference myVfPage = Page.InquiryOppCreation;
            Test.setCurrentPage(myVfPage);
            
            ApexPages.currentPage().getParameters().put('mqlid',iq.Id);
            
            ApexPages.StandardController sc = new ApexPages.StandardController(iq);
            InquiryOppCreation ac = new InquiryOppCreation(sc);
            ac.selectedDiv='IT';
              List<SelectOption> selOptspb=ac.getPriceBookList();
              List<SelectOption> selOptsdv=ac.getDivLst();
             ac.PgLst();
                ac.selectedDiv='IT';
             ac.opp.ProductGroupTest__c='LCD_MONITOR';
             ac.selectedpb=pbk1.id;
           
            ac.saveopp();
         
         //added by thiru
         ac.selectedDiv='SBS';
              List<SelectOption> selOptspb2=ac.getPriceBookList();
              List<SelectOption> selOptsdv2=ac.getDivLst();
             ac.PgLst();
                ac.selectedDiv='SBS';
             ac.opp.ProductGroupTest__c='ACCESSORY';
             ac.selectedpb=pbk1.id;
         //added by thiru
         ac.selectedDiv='W/E';
              List<SelectOption> selOptspb3=ac.getPriceBookList();
              List<SelectOption> selOptsdv3=ac.getDivLst();
             ac.PgLst();
                ac.selectedDiv='SBS';
             ac.opp.ProductGroupTest__c='SOLUTION';
             ac.selectedpb=pbk1.id;
           
         
         
         
         Test.stopTest(); 
	}
	
		private static testMethod void testmethod2() {
             Test.startTest();
	    Account newAccount= TestDataUtility.createAccount(TestDataUtility.generateRandomString(5));
        newAccount.name ='Test Account';
        //acc.recordTypeId = endUserRT;
        newAccount.SAP_Company_Code__c='99999888';
        insert newAccount;
        Zipcode_Lookup__c z=new Zipcode_Lookup__c(City_Name__c='Ridgefield Park', Country_Code__c='US', State_Code__c='NJ', State_Name__c='New Jersey', Name='07660');
        insert z;
        
         string   CampaignTypeId = [Select Id From RecordType Where SobjectType = 'Campaign' and Name = 'Sales'  limit 1].Id;
	    string   LeadTypeId = [Select Id From RecordType Where SobjectType = 'Lead' and Name = 'End User'  limit 1].Id;
       Campaign cmp= new Campaign(Name='Test Campaign for test class ',	IsActive=true,type='Email',StartDate=system.Today(),recordtypeid=CampaignTypeId);
       insert cmp;
        Id endCustomer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('End Customer').getRecordTypeId();
        Account acc1 = new account(recordtypeId=endCustomer,Name ='mqlinquirytest', Type='Customer',BillingStreet ='85 Challenger Road',BillingCity='Ridgefield Park',BillingState='NJ',BillingCountry='US',BillingPostalCode='07660');
        insert acc1;
        list<contact> cont=new list<contact>();
         contact con = new contact( LastName='contactinq',Accountid=acc1.id,Email='oppcreationclass@conatc926.com',MQL_Score__c='T1');
          insert con;
         Inquiry__c iq=new Inquiry__c(Contact__c = con.id,Product_Solution_of_Interest__c='Monitors',	 campaign__c = cmp.id);
         insert iq;
         Pricebook2 pbk1 = [select id,name from Pricebook2 where name='Mobile'  limit 1];
      
         PageReference myVfPage = Page.InquiryOppCreation;
            Test.setCurrentPage(myVfPage);
            
            ApexPages.currentPage().getParameters().put('mqlid',iq.Id);
            
            ApexPages.StandardController sc = new ApexPages.StandardController(iq);
            InquiryOppCreation ac = new InquiryOppCreation(sc);
           ac.selectedDiv='Mobile';
              List<SelectOption> selOptspb=ac.getPriceBookList();
              List<SelectOption> selOptsdv=ac.getDivLst();
             ac.PgLst();
                ac.selectedDiv='Mobile';
             ac.opp.ProductGroupTest__c='LCD_MONITOR';
             ac.selectedpb=pbk1.id;
           
            ac.saveopp();
         
         
         
         
         
         Test.stopTest(); 
	}
	
	
		private static testMethod void testmethod3() {
             Test.startTest();
                string   CampaignTypeId = [Select Id From RecordType Where SobjectType = 'Campaign' and Name = 'Sales'  limit 1].Id;
	     Campaign cmp= new Campaign(Name='Test Campaign for test class ',	IsActive=true,type='Email',StartDate=system.Today(),recordtypeid=CampaignTypeId);
       insert cmp;
       profile p = [select id, Name from profile where name= 'HA Sales User' limit 1];
       User u1 = new User(Alias = 'standt', Email='mycaseTriggerTestwww@ss.com', 
                  EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                  LocaleSidKey='en_US', ProfileId = p.Id, 
                  TimeZoneSidKey='America/Los_Angeles', UserName='abc11234@abc.com');
                  insert u1;
                  
            System.runAs(u1){
                Id endCustomer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('End Customer').getRecordTypeId();
            Account acc1 = new account(Name ='mqlinquirytest', Type='Customer',BillingStreet ='85 Challenger Road',BillingCity='Ridgefield Park',BillingState='NJ',BillingCountry='US',BillingPostalCode='07660');
            insert acc1;
        list<contact> cont=new list<contact>();
         contact con = new contact( LastName='contactinq',Accountid=acc1.id,Email='oppcreationclass@conatc926.com',MQL_Score__c='T1');
          insert con;
           string   IQTypeId = [Select Id From RecordType Where SobjectType = 'Inquiry__c' and Name = 'HA'  limit 1].Id;
         Inquiry__c iq=new Inquiry__c(Contact__c = con.id,Product_Solution_of_Interest__c='Monitors',	 campaign__c = cmp.id, recordtypeid=IQTypeId);
         insert iq;
         PageReference myVfPage = Page.InquiryOppCreation;
            Test.setCurrentPage(myVfPage);
            
            ApexPages.currentPage().getParameters().put('mqlid',iq.Id);
            
            ApexPages.StandardController sc = new ApexPages.StandardController(iq);
            InquiryOppCreation ac = new InquiryOppCreation(sc);
           
            ac.createOpportunityforHA();
                
                
            }      
      
      
         
         
         
         
         
         Test.stopTest(); 
	}

}