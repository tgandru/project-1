public class InquiryTriggerHandler {
  public static void beforeinsert(List<Inquiry__c> niq){//This Method is for to populate Account id if Contact is present 
       Set<String> mbpsi = new Set<String>(Label.Mobile_Interest.split('; '));
       Set<String> itpsi = new Set<String>(Label.IT_Interest.split('; '));
       Set<String> htvpsi = new Set<String>(Label.HTV_Product_Interest.split('; '));
      user  curusr=[select id,sales_team__c,profileid,Profile.Name from user where id=: UserInfo.getUserId()];
        Map<string, Contact> conaccMap = new Map<string,Contact>{};
        Map<string, lead> leMap = new Map<string, lead>{};
       set<id> contid=new set<id>();
       set<id> lid=new set<id>();
       set<id> parentids=new set<id>();
      map<id,inquiry__c> clinqmap=new map<id,inquiry__c>();
       List<Inquiry__c> cliq=new list<Inquiry__c>();
      for(Inquiry__c i:niq){// Added By Vijay on 5/17 -Starts
          if(i.isClone()){
              parentids.add(i.getCloneSourceId());
          }
      }
      if(parentids.size()>0){
          for(Inquiry__c iq:[select id,Last_Campaign__c,Campaign__c from inquiry__c where id in :parentids]){
              clinqmap.put(iq.id,iq);
          }
      }      // -- Ends Here 
       for(Inquiry__c i:niq){
          i.Completed_SLA_Number__c='0';//on creation the completed sla number will be 0
            if(i.isClone()){// inserting the reocod by cloning any record. --Starts Here
              i.Original_Inquiry__c=i.getCloneSourceId();
              i.Inquiry_Assigned_Date__c=system.today(); // reason - for cloning records Upate trigger is not firing so we are updationg these time stamps here 
             // i.MQL_End_Date__c=system.today();
              //i.SAL_Start_Date__c=system.today();
                if(curusr.sales_team__c=='Marketstar'){//Added By Vijay on 5/14 checking the Current User Sales Team 
                 i.Last_Campaign__c=label.M_Campaign_ID;  // For Cloned Inquiry We need fill last campaign value as M* campaign 
                  // For M* user when cloning Record ,Fisrt touch campaign will be source record last touch campaign 
                   try{
                        inquiry__c pinq=clinqmap.get(i.getCloneSourceId());
                         i.Campaign__c=pinq.Last_Campaign__c;
                    }catch(exception ex){
                        
                    }
                }else if(i.Last_Campaign__c==null){
                    try{
                        inquiry__c pinq=clinqmap.get(i.getCloneSourceId());
                         i.Last_Campaign__c=pinq.Last_Campaign__c;
                    }catch(exception ex){
                        
                    } 
                }
                system.debug('Last Campaign Id =='+i.Last_Campaign__c);
              i.Reassignment_Time_8hrs__c=null;
              i.Reassignment_Time_16hrs__c=null;  
              i.Opportunity__c=null;
            }//Ends Here
          
         List<string> psi=i.Product_Solution_of_Interest__c.Split(';');
           string itp='';
           string mbp='';
           string prt='';
           string htvp='';
           if(psi.size()>0){
               for(string p:psi){
                    if(p!=null&&p!=''){
                       if(itpsi.contains(p)){
                            If(p=='Printers'&&itp==''){
                                prt=prt+p;
                            }else if(p=='Printers'&&itp !=''){
                              itp=itp;  
                            }else{
                               itp=itp+p+';'; 
                            }
                      }else if(mbpsi.contains(p)){
                                mbp=mbp+p+';';
                           }else if(htvpsi.contains(p)){
                                 htvp=htvp+p+';';
                           }
                        
                    }
                           
                   
               }
               system.debug(itp+'*********'+mbp); 
               system.debug(itp+'*********'+itp); 
    
           }
           if(mbp !='' && itp==''){
               i.Product_Family__c='Mobile';
                i.Product_Solution_of_Interest__c=mbp;
           }else if(mbp =='' && itp!=''){
                i.Product_Family__c='IT';
                i.Product_Solution_of_Interest__c=itp;
           }else if(prt !=''){
                i.Product_Family__c='Printer';
           }else if(htvp !='' && itp=='' && mbp ==''){
                i.Product_Family__c='HTV';
                i.Product_Solution_of_Interest__c=htvp;
            }
           if(mbp !='' && itp!=''){
               //-Starts 
               if(curusr.Profile.Name=='Eloqua Integration User'&&(i.ownerid ==null|| i.ownerID==curusr.id )){
                    i.Product_Family__c='Mobile';
                     i.Product_Solution_of_Interest__c=mbp;
                     cliq.add(new Inquiry__c(Lead__c=i.Lead__C,Campaign__c=i.Campaign__c,Contact__c=i.Contact__c,Product_Family__c='IT',Product_Solution_of_Interest__c=itp,Industry__c=i.Industry__c,
                                       MQL_Comments__c=i.MQL_Comments__c,Lead_Score__c=i.Lead_Score__c,MQL_Score__c=i.MQL_Score__c,Source__c=i.Source__c,System_Source__c=i.System_Source__c,
                                        Marketing_Channel__c=i.Marketing_Channel__c,Marketing_Lead_Source_Most_Recent__c=i.Marketing_Lead_Source_Most_Recent__c,Marketing_Lead_Source_Original__c=i.Marketing_Lead_Source_Original__c,
                                        Device_Type__c=i.Device_Type__c,Last_Campaign__c=i.Campaign__c,Eloqua_ID__c=i.Eloqua_ID__c,Number_of_Devices__c=i.Number_of_Devices__c,Purchase_Timeframe_Months__c=i.Purchase_Timeframe_Months__c,No_of_Employees__c=i.No_of_Employees__c
                                        
                                        
                                        ));
               }else{
                   i.Product_Family__c='Mobile and IT';
               }
               
                                     
           }
       }
        for(Inquiry__c i : niq){
                 if(i.Contact__c != null && i.Lead__c== null){
                      contid.add(i.Contact__c);
                 }
                 if(i.Contact__c == null && i.Lead__c!= null){
                      lid.add(i.Lead__c);
                 }
        }
                       if(contid.size()>0){
                               for(Contact con:[select id,OwnerId,AccountId,email,MailingStreet,MailingCity,MailingState,MailingCountry,MailingPostalCode from Contact where id IN :contid]){
                                  conaccMap.put(con.Id,con);
                               }
                       }
                       if(lid.size()>0){
                               for(Lead ld:[select id,email,Status,street,city,state,Postalcode,country from lead where id IN:lid]){
                                  leMap.put(ld.Id,ld);
                               }
                       }
           for(Inquiry__c i : niq){
                   if(i.Last_Campaign__c == null){
                        i.Last_Campaign__c=i.Campaign__c;
                    }
               if(i.Contact__c != null ){
                    contact  cont=conaccMap.get(i.Contact__c);
                    i.Account__c=cont.AccountId;
                    i.Street__c=cont.MailingStreet;
                    i.State__c=cont.MailingState;
                    i.city__c=cont.MailingCity;
                    i.country__c=cont.MailingCountry;
                    i.Zip_Postal_Code__c=cont.MailingPostalCode;
                    if(i.Eloqua_ID__c==null){
                        i.Eloqua_ID__c=cont.email; 
                    }
                 }  
                 
                if(i.Lead__c !=null){
                    Lead led=leMap.get(i.Lead__c);
                    system.debug('*****'+led);
                     system.debug('*****'+led.street);
                    i.Street__c=led.street;
                    i.State__c=led.State;
                    i.city__c=led.city;
                    i.country__c=led.country;
                    i.Zip_Postal_Code__c=led.Postalcode;
                    if(i.Eloqua_ID__c==null){
                        i.Eloqua_ID__c=led.email;
                        system.debug('Lead Email *****'+led.email);
                    }
                     if(led.Status=='Invalid'){
                     i.addError('The Lead Is Invalid,You Can Create Inquiry only for valid Leads.');   
                    }
                } 
           }                  
        if(cliq.size()>0){
            database.insert(cliq,false);
           //insert cliq;
       }                 
  }
  
 Public static void updatetimestamps(list<Inquiry__c> niq,map<id,Inquiry__c> oldmap){// To Update some time stamps 
      id Currentuser = UserInfo.getUserId();
      String profileName=[Select Id,Name from Profile where Id=:userinfo.getProfileId()].Name;
      for(Inquiry__c i:niq){
                
          Inquiry__c oldiq=oldmap.get(i.Id);
          string status =oldiq.Inquiry_Status__c;
          if(i.Inquiry_Status__c=='Unreachable'){
                i.isActive__c=false;
              if(status=='Account/Opportunity'||status=='Opportunity'){
                  i.SQL_End_Date__c=system.today();
              }else if(status=='Active'||status=='In Pursuit'){
                 i.SAL_End_Date__c=system.today(); 
              }else if(status=='Assigned' || status=='New'){
                   i.MQL_End_Date__c=system.today();
                  i.SAL_Start_Date__c=system.today();
                 i.SAL_End_Date__c=system.today();
                 if(i.Inquiry_Assigned_Date__c==null){ //Added by thiru - 12/04/2017
                      i.Inquiry_Assigned_Date__c=system.today();
                      i.ownerid = Currentuser;
                 }
              }
              
          }else if(i.Inquiry_Status__c=='Invalid Prospect'|| i.Inquiry_Status__c=='Invalid Inquiry' || i.Inquiry_Status__c=='Passed to Carrier'){
                i.isActive__c=false;
                 string ow=i.OwnerId;
              if(ow.startsWith('00G') && profileName !='System Administrator'){
                  i.OwnerId=UserInfo.getUserId();
              }
              
              //if condition added by thiru - to populate Inquiry_Assigned_Date__c when status from 'New'.
              if(status=='New'){ 
                  if(i.Inquiry_Assigned_Date__c==null){
                      i.Inquiry_Assigned_Date__c=system.today();
                      if(profileName !='System Administrator'){
                           i.ownerid = Currentuser;
                      }
                     
                  }
              } 
              
              if(i.MQL_End_Date__c ==null){
                 i.MQL_End_Date__c=system.today();
              }
              if(status=='Account/Opportunity'||status=='Opportunity'){
                  i.SQL_End_Date__c=system.today();
              }else if(status=='Active'||status=='In Pursuit'||status=='Account Only'){
                 i.SAL_End_Date__c=system.today(); 
              }
                  if(i.SAL_Start_Date__c !=null && i.SAL_End_Date__c==null){
                        i.SAL_End_Date__c=system.today();
                  }
                  if(i.SQL_Start_Date__c !=null&& i.SQL_End_Date__c==null){
                        i.SQL_End_Date__c=system.today();
                  }
                  
             
          }    else if(i.Inquiry_Status__c=='Active' || i.Inquiry_Status__c=='In Pursuit'){
              //if condition added by thiru - to populate Inquiry_Assigned_Date__c when status from 'New'.
              if(status=='New'){ 
                  if(i.Inquiry_Assigned_Date__c==null){
                      i.Inquiry_Assigned_Date__c=system.today();
                    if(profileName !='System Administrator'){
                           i.ownerid = Currentuser;
                      }
                  }
              } 
              if(i.SAL_Start_Date__c==null){
                  i.SAL_Start_Date__c=system.today();
              }
               if(i.MQL_End_Date__c ==null){
                 i.MQL_End_Date__c=system.today();
              }
              if( i.Inquiry_Assigned_Date__c==null){
                  i.Inquiry_Assigned_Date__c=system.today(); 
              }
               i.SAL_End_Date__c=null;
               i.SQL_End_Date__c=null;
               i.SQL_Start_Date__c=null;
              if(i.Sub_Status__c=='BANT Qualified'){
                   if(i.SQL_Start_Date__c ==null){
                          i.SAL_End_Date__c=system.today();
                          i.SQL_Start_Date__c=system.today();
                   }
               }  
          }else if(i.Inquiry_Status__c=='No Sale' || i.Inquiry_Status__c=='Account Only'){
              //if condition added by thiru - to populate Inquiry_Assigned_Date__c when status from 'New'.
                i.isActive__c=false;
              if(status=='New'){ 
                  if(i.Inquiry_Assigned_Date__c==null){
                      i.Inquiry_Assigned_Date__c=system.today();
                     if(profileName !='System Administrator'){
                           i.ownerid = Currentuser;
                      }
                  }
              } 
                if(i.SAL_Start_Date__c==null){
                 i.SAL_Start_Date__c=system.today();
                 i.SAL_End_Date__c=system.today(); 
                }
                if(i.MQL_End_Date__c ==null){
                 i.MQL_End_Date__c=system.today();
              }
              if(i.SAL_Start_Date__c!=null){
                     i.SAL_End_Date__c=system.today(); 
                }
               if(i.SQL_Start_Date__c!=null){
                     i.SQL_End_Date__c=system.today(); 
                }
                
          }else if(i.Inquiry_Status__c=='Account/Opportunity' || i.Inquiry_Status__c=='Opportunity'){
                i.isActive__c=false;
                  if(i.SAL_End_Date__c==null){
                       i.SAL_End_Date__c=system.today(); 
                       i.SQL_Start_Date__c=system.today(); 
                
                  }else{ 
                     if( i.SQL_Start_Date__c== null){
                         i.SQL_Start_Date__c=system.today();
                     } 
                  }
                  if(i.Opportunity__c == null){
                      i.SQL_End_Date__c=null; 
                      i.SQO_Start_Date__c=null; 
                  }
                  if(i.SAL_Start_Date__c==null){
                 i.SAL_Start_Date__c=system.today();
                 i.SAL_End_Date__c=system.today();
                  i.SQL_Start_Date__c=system.today(); 
                }
                  if(i.MQL_End_Date__c ==null){
                 i.MQL_End_Date__c=system.today();
                }
                //if condition added by thiru - to populate Inquiry_Assigned_Date__c when status from 'New'.
                if(status=='New'){ 
                  if(i.Inquiry_Assigned_Date__c==null){
                      i.Inquiry_Assigned_Date__c=system.today();
                      if(profileName !='System Administrator'){
                           i.ownerid = Currentuser;
                      }
                  }
                } 
              
          }else if(i.Inquiry_Status__c=='Assigned'){
              if(status !='Assigned'){
                 i.Inquiry_Assigned_Date__c=system.today();
              }
          }else if(i.Inquiry_Status__c=='Passed to Reseller'){  //Condition added by thiru - to populate Inquiry_Assigned_Date__c when status from 'New'.
              if(status=='New'){ 
                  if(i.Inquiry_Assigned_Date__c==null){
                      i.Inquiry_Assigned_Date__c=system.today();
                     if(profileName !='System Administrator'){
                           i.ownerid = Currentuser;
                      }
                  }
              } 
          } 
      }
      
  }
   public static void afterinsert(List<Inquiry__c> niq){//This Method IS to send email to contact owners after insert 
             List<Messaging.SingleEmailMessage> mails =  new List<Messaging.SingleEmailMessage>();
             Map<Id, User> ownerMap = new Map<Id, User>{};
             Map<Id, string> conMap = new Map<Id, string>{};
             Map<Id, User> itownerMap = new Map<Id, User>{};
             Map<Id, string> itconMap = new Map<Id, string>{};
              Map<Id, Campaign> camp = new Map<Id, Campaign>{};
              Map<Id, Contact> contAcc = new Map<Id, Contact>{};
              Map<Id, string> conOwnerEmailMap = new Map<Id, string>();
             set<id> contid=new set<id>();
         // id  templateId = [select id, name from EmailTemplate where developername ='Inquiry_Creation'].id;
         List<string> clonedrec=new list<string>();
             for(Inquiry__c i : niq){
                 
                 if(i.Original_Inquiry__c != null){
                     clonedrec.add(i.id);
                 }
                 
                 
                 if(i.Contact__c != null && i.Lead__c== null){
                     camp.put(i.Campaign__c,null);
                      contid.add(i.Contact__c);
                 }
               }
                       if(contid.size()>0){
                           
                           for(Contact con:[select id,OwnerId,Owner.Email,Name,Account.OWnerID,Account.owner2__c,Account.Name from Contact where id IN :contid]){
                              contAcc.put(con.id,con);
                              ownerMap.put(con.Account.OWnerID,null); 
                              conMap.put(con.Id,con.Account.OWnerID); 
                              conOwnerEmailMap.put(con.id,con.Owner.Email);
                              if(con.Account.owner2__c !=null){
                               itownerMap.put(con.Account.owner2__c,null); 
                              itconMap.put(con.Id,con.Account.owner2__c); 
                              }
                           }
                       }
                ownerMap.putAll([SELECT Id,Email,Name FROM User WHERE Id IN :ownerMap.keySet()]);
                 itownerMap.putAll([SELECT Id,Email,Name FROM User WHERE Id IN :itownerMap.keySet()]);
                camp.putAll([select id,name from Campaign where id IN:camp.keyset()]);
              for(Inquiry__c i : niq){
                 if(i.Contact__c != null && i.Lead__c== null){
                      
                     try{
                        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                           string bdy ='';
                             mail.setSubject('New Inquiry is Created ');
                        string owi=conMap.get(i.Contact__c);
                        string itowi=itconMap.get(i.Contact__c);
                      
                        if(i.Product_Family__c=='Mobile'){
                           mail.setToAddresses(new list<String>{ownerMap.get(owi).Email});
                         
                        }else if(i.Product_Family__c=='IT'){
                          
                            if(itownerMap.keySet().Size()>0){
                                 mail.setToAddresses(new list<String>{itownerMap.get(itowi).Email});
                                  
                            }else{
                                mail.setToAddresses(new list<String>{ownerMap.get(owi).Email});
                                }
                          
                        }else if(i.Product_Family__c=='HTV'){
                             mail.setToAddresses(new list<String>{conOwnerEmailMap.get(i.Contact__C)});
                        }
                       
                          bdy = returnMailBody(i,ownerMap.get(owi),camp.get(i.Campaign__c),contAcc.get(i.Contact__c)); 
                          system.debug(''+i.Contact__r.Name);
                          system.debug('***'+bdy);
                        mail.setHtmlBody(bdy);
                        mails.add(mail);
                     
                     }catch(exception ex){
                         
                     }
                 }  
              }
              
              if(clonedrec.size()>0){
                  UpdateActiviestoClonedInquiry(clonedrec);
              }
              
              if(mails.size() > 0){
                  //SendEmailResult[] results = connection.sendEmailMessage(mails);

                   Messaging.sendEmail(mails,false);
                }
            
   }
   
   Public static void afterupdate(List<Inquiry__c> niq,map<id,Inquiry__c> oldMap){
       set<id> lid=new set<id>();
       List<Lead> ld=new list<lead>();
       map<string,lead> ldmap=new map<string,Lead>();
       for(Inquiry__c i : niq){
           Inquiry__c oldiq = oldMap.get(i.id);
           if(i.Lead__c !=null){
                if((i.Street__c != oldiq.Street__c || i.city__c != oldiq.city__c || i.State__c != oldiq.State__c || i.Zip_Postal_Code__c != oldiq.Zip_Postal_Code__c|| i.country__c != oldiq.country__c) ){
                 ldmap.put(i.Lead__C,null);
                }
           }
          
         
       }
       ldmap.PutAll([select id,street,city,State,country,Postalcode from lead where id in:ldmap.Keyset()]);
       
       for(Inquiry__c i : niq){
           Inquiry__c oldiq = oldMap.get(i.id);
           if(i.Lead__c !=null){
                if((i.Street__c != oldiq.Street__c || i.city__c != oldiq.city__c || i.State__c != oldiq.State__c || i.Zip_Postal_Code__c != oldiq.Zip_Postal_Code__c|| i.country__c != oldiq.country__c)){// && i.By_Pass__c==false
                    Lead l=ldmap.get(i.Lead__C);  
                    l.Street=i.Street__c;
                    if(i.city__c !=null){
                       l.city=i.city__c.toUpperCase(); 
                    }
                    if(i.State__c !=null){
                        l.State=i.State__c.toUpperCase();
                    }
                    if(i.country__c !=null){
                        l.country=i.country__c.toUpperCase();
                    }
                    if(i.Zip_Postal_Code__c !=null){
                        l.Postalcode=i.Zip_Postal_Code__c;
                    }
                    
                    
                    ld.Add(l);
                    
                }
           }
          
          
       }
      
      if(ld.Size()>0){
          Set<lead> unld=new set<lead>();
          for(lead l:ld){
              unld.add(l);
          }
          List<lead> unqld=new list<Lead>();
         for(lead l:unld){
             unqld.add(l);
         }
         database.update(unqld,false);
          //update unqld;
         
      } 
   }
   
  public static void updatethestatus(List<Inquiry__c> niq,map<id,Inquiry__c> oldMap){//To Update the Status if owner from QUEUE to User 
      for(Inquiry__c newiq : niq){
            Inquiry__c oldiq = oldMap.get(newiq.id);
            
            if(newiq.OwnerId != oldiq.OwnerId && string.valueOf(newiq.ownerId).startsWith('005')  && newiq.Inquiry_Status__c == 'New'){
                newiq.Inquiry_Status__c = 'Assigned';
                 system.debug('******'+newiq.Inquiry_Status__c);
            }
           
        }
      
  }
 public static string returnMailBody(Inquiry__c iq,user toUser,Campaign cp,Contact c){//To Prepare Email Body.
     
        string Url = System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+iq.id;
      
        string bdy = 'Hi '+toUser.Name+',';
        bdy = bdy + '<br/> <br/> <b></b>A contact from one of your accounts has an inquiry requiring your attention. Please see below for details and direct link to the inquiry '+iq.Name;
         bdy = bdy + '\n \n <br/>';
         bdy = bdy + '\n \n <br/>';
         bdy = bdy + '\n \n'+ '<br/><b> WHO :</b> '+c.Name+' At '+c.Account.Name;
        bdy = bdy + '\n \n'+ '<br/><b>Product Interest :</b> '+iq.Product_Solution_of_Interest__c;
        bdy = bdy + '\n \n'+ '<br/><b> Campaign : </b>'+cp.Name;
        
          bdy = bdy + '\n \n'+ '<br/><br/><br/><br/><b>Please click on the link below to view the details of this record in salesforce.com:</b>';
           bdy = bdy + '\n \n'+ '<br/> '+Url;
         bdy = bdy + '\n \n'+'<br/><b>';
        bdy = bdy + '\n \n'+ '<br/><br/><br/><br/><b>Regards</b>,';
         bdy = bdy + '\n \n'+ '<br/><b>Samsung Admin Team</b>';
        return bdy;
 } 
 
 public static void UpdateProductFamily(List<Inquiry__c> niq){//This Method is to update the product family based on the Product interest. 
       Set<String> mbpsi = new Set<String>(Label.Mobile_Interest.split('; '));
       Set<String> itpsi = new Set<String>(Label.IT_Interest.split('; '));
       Set<String> htvpsi = new Set<String>(Label.HTV_Product_Interest.split('; '));
       List<Inquiry__c> cliq=new list<Inquiry__c>();
       
       for(Inquiry__c i:niq){
           List<string> psi=i.Product_Solution_of_Interest__c.Split(';');
           string itp='';
           string mbp='';
           string prt='';
           string htvp='';
           if(psi.size()>0){
               for(string p:psi){
                    if(p!=null&&p!=''){
                       if(itpsi.contains(p)){
                            If(p=='Printers'&&itp==''){
                                prt=prt+p;
                            }else if(p=='Printers'&&itp !=''){
                              itp=itp;  
                            }else{
                               itp=itp+p+';'; 
                            }
                       }else if(mbpsi.contains(p)){
                                mbp=mbp+p+';';
                       }else if(htvpsi.contains(p)){
                            htvp=htvp+p+';';
                       }  
                    }
               }
               system.debug(itp+'*********'+mbp); 
               system.debug(itp+'*********'+itp); 
           }
           if(mbp !='' && itp==''){
               i.Product_Family__c='Mobile';
                //i.Product_Solution_of_Interest__c=mbp;
           }else if(mbp =='' && itp!=''){
                i.Product_Family__c='IT';
                //i.Product_Solution_of_Interest__c=itp;
           }else if(prt !=''){
                i.Product_Family__c='Printer';
           }else if(mbp =='' && itp=='' && htvp !=''){
               i.Product_Family__c='HTV';
           }
           
           if(mbp !='' && itp!=''){
               i.Product_Family__c='Mobile and IT';
               //i.Product_Solution_of_Interest__c=mbp;
           }
       }
  }
  public static void checkduplicaterecords(List<Inquiry__c> niq,string pro){//Added By Vijay 4/26 to check the Duplicate records 
      map<string,list<Inquiry__c>> inqmap=new map<string,list<Inquiry__c>>();
      set<string> prospectid=new set<string>();
      for(Inquiry__c i:niq){
          if(i.Lead__c != null&& i.isActive__c==true){
              prospectid.add(i.lead__c);
          }else if(i.Contact__c !=null&& i.isActive__c==true){
              prospectid.add(i.Contact__c);
          }
      }
      if(prospectid.size()>0){
          for(Inquiry__c i:[select id,name,isActive__c,Lead__c,contact__c from Inquiry__c where (Lead__c in:prospectid or Contact__c in:prospectid) and isActive__c=true]){
                      string s;
                      if(i.Lead__c != null){
                          s=i.lead__c;
                      }else if(i.Contact__c !=null){
                          s=i.Contact__c;
                      }  
                           
                           if(inqmap.containsKey(s)){
                                List<Inquiry__c> ol = inqmap.get(s);
                                        ol.add(i);
                                       inqmap.put(s, ol);
                            }else{
                               inqmap.put(s, new List<Inquiry__c> {i}); 
                            }   
                            
                            
          }
     
      
      for(Inquiry__c i:niq){
            if(i.isActive__c==true){// Added on 5/25 - vijay
                    string s;
                      if(i.Lead__c != null){
                          s=i.lead__c;
                      }else if(i.Contact__c !=null){
                          s=i.Contact__c;
                      } 
                   try{
                       boolean duprec=true;
                       List<Inquiry__c> iq=inqmap.get(s);
                       if(i.isClone()){
                           string orgID=i.getCloneSourceId();
                          i.Original_Inquiry__c=orgID;
                           system.debug('Clone Process -Original Iq ID- '+i.Original_Inquiry__c);
                           system.debug('Clone Process Dup ID -'+iq[0].id);
                           if(iq.size()<=1 &&(orgID==iq[0].id || i.id==iq[0].id)){
                               duprec=false;
                           }
                       }
                       if(pro=='Update'){
                           if(iq.size()<=1 &&i.id==iq[0].id){
                               duprec=false;
                           }
                       }
                       
                    if(iq.size()>0&& duprec==true){
                           string rec='';
                           for(Inquiry__c inqu:iq){
                               rec=rec+' ,'+inqu.name;
                           }
                          String err=label.Inquiry_Duplicate_Error;
                         i.addError(err+rec);  
                       }
                       
                   }catch(exception ex){
                       
                   }   
                      
      }           
                      
      }
  }
      
  }
  Public Static void CalculatetheSLATimes(List<Inquiry__c> niq,map<id,Inquiry__c> oldMap){
      //This Method is to start the SLA timer for 8hr and 16hr time 
      
       BusinessHours stdBusinessHours = [select id from businesshours where Name='Inquiry SLA Business Hours'];
        
         
         for(Inquiry__c i :niq){
             Inquiry__c oldiq = oldMap.get(i.id);
             
                   if(i.Inquiry_Status__c=='Assigned' && i.Product_Family__c !='HTV'){
                                 if(oldiq.Inquiry_Status__c!='Assigned'){
                                   i.Reassignment_Time_8hrs__c=BusinessHours.addGmt (stdBusinessHours.id, System.now(), 8 * 60 * 60 * 1000L);//8 * 60 * 60 * 1000L
                                   i.Reassignment_Time_16hrs__c=BusinessHours.addGmt (stdBusinessHours.id, System.now(), 16 * 60 * 60 * 1000L); //16 * 60 * 60 * 1000L
                                     i.Completed_SLA_Number__c='0';
                                 }else{
                                    //Don't Change the value  
                                 }
                 
                 
                     }else if(i.Inquiry_Status__c=='Active' && i.source__c=='866-SAM4BIZ(Inbound)'){//5days reminder is only for SAM4BIZ inquiries 
                         if(oldiq.Inquiry_Status__c!='Active'){
                             i.X5_Days_Reminder_Time__c=BusinessHours.addGmt (stdBusinessHours.id, System.now(), 5*9*60*60*1000L);//5*9*60*60*1000L
                         }
                     }else{
                          i.X5_Days_Reminder_Time__c=null;
                     }
             
                 if(i.ownerId !=oldiq.ownerId&&i.Product_Family__c !='HTV'&& i.Is_Converted__c==false){
                         i.Reassignment_Time_8hrs__c=BusinessHours.addGmt (stdBusinessHours.id, System.now(), 8 * 60 * 60 * 1000L);//8 * 60 * 60 * 1000L
                         i.Reassignment_Time_16hrs__c=BusinessHours.addGmt (stdBusinessHours.id, System.now(), 16 * 60 * 60 * 1000L); //16 * 60 * 60 * 1000L
                         i.Completed_SLA_Number__c='0';
                     }
                     
             
              if((i.Inquiry_Status__c=='Account Only' ||i.Inquiry_Status__c=='Account/Opportunity' || i.Inquiry_Status__c=='Opportunity') && i.Product_Family__c=='HTV' && i.Is_Converted__c==false){
                       if(oldiq.Inquiry_Status__c!='Account Only'&&oldiq.Inquiry_Status__c!='Account/Opportunity'&&oldiq.Inquiry_Status__c!='Opportunity'){
                       i.Reassignment_Time_8hrs__c=BusinessHours.addGmt (stdBusinessHours.id, System.now(), 8 * 60 * 60 * 1000L);//8 * 60 * 60 * 1000L
                       i.Reassignment_Time_16hrs__c=BusinessHours.addGmt (stdBusinessHours.id, System.now(), 16 * 60 * 60 * 1000L);
                       }
             }
             
             
             if(i.Inquiry_Status__c=='Invalid Prospect'|| i.Inquiry_Status__c=='Invalid Inquiry' || i.Inquiry_Status__c=='Passed to Carrier' ||i.Inquiry_Status__c=='Unreachable'||i.Inquiry_Status__c=='No Sale'){
                i.Reassignment_Time_8hrs__c=null;
                i.Reassignment_Time_16hrs__c=null; 
                i.X5_Days_Reminder_Time__c=null;
             }
                     
         }
       
       
       
       
  } 
  
  @future
  public static void UpdateActiviestoClonedInquiry(list<id> inqid){
      /*
This Method is to Update the cloned inquiry with Parent Inquiry activites and Acivites from Lead/contact

 Created Date - 4/4/2018
  Intial Assumption - Instaed of moving the Inquires from Lead/Contact , it's better to update the Inquiry__c field value with new Inquiry on Activity Object 

*/
     list<task> updatetasklst=new list<task>();
     list<Inquiry__c> updateIQlst=new list<Inquiry__c>();
      Map<string,string> clonedids=new map<string,string>();
      Map<string,string> oldiqids=new map<string,string>();
      Map<string,string> mapids=new map<string,string>();
      for(Inquiry__c i: [select id, name,Lead__c,Contact__c,Cloned_Inquiry__c,Original_Inquiry__c from Inquiry__c where ID in :inqid ]){
          oldiqids.put(i.Original_Inquiry__c,i.id);
          clonedids.put(i.Original_Inquiry__c,i.name);
         if(i.Lead__c !=null){
              mapids.put(i.Lead__c,i.Id);
         }else{
             mapids.put(i.contact__C,i.Id);
         }
         
       }
      
      for(task t:[select id,whatid,whoid,Inquiry__c from task where whoid in :mapids.Keyset() or whatid in :oldiqids.Keyset()]){
           if(t.whatid !=null){
             
             string s=t.whatid;
             if(s.Startswith('a2g')){
                 t.whatid=oldiqids.get(t.whatid);//Transfering any Task Attached with old Inquiry to new inquiry 
             }
               
           }
               t.Inquiry__c=mapids.get(t.whoid); // Updating the task attached with lead/contact with new inquiry. 
         updatetasklst.add(t);
          
      }
      for(Inquiry__c i:[select id,name,Inquiry_Status__c,Reason_invalid__c,isActive__c,Cloned_Inquiry__c,Sub_Status__c from Inquiry__c where id in:clonedids.keyset()]){
          
          i.Cloned_Inquiry__c=clonedids.get(i.id);
          i.isActive__c=false;
          i.Inquiry_Status__c='Invalid Inquiry';
          i.Reason_invalid__c='Cloned';
          i.Sub_Status__c='Cloned';
          updateIQlst.add(i);
                    
      }
      if(updateIQlst.size()>0){
          update  updateIQlst;
      }
      
      if(updatetasklst.size()>0){
          update  updatetasklst;
      }
 
  } 
  
  
}