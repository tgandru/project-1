@isTest
private class InquiryTriggerHandlerTest {

	private static testMethod void testmethod1() {
	     Test.startTest();
         string   CampaignTypeId = [Select Id From RecordType Where SobjectType = 'Campaign' and Name = 'Sales'  limit 1].Id;
	    string   LeadTypeId = [Select Id From RecordType Where SobjectType = 'Lead' and Name = 'End User'  limit 1].Id;
       Campaign cmp= new Campaign(Name='Test Campaign for test class ',	IsActive=true,type='Email',StartDate=system.Today(),recordtypeid=CampaignTypeId);
       insert cmp;
       List<Lead> leadLst = new list<Lead>();
       
       lead le1=new lead(lastname='test lead 123',Company='My Lead Company',Email='mylead@sds.com',	MQL_Score__c='T1',Status='New',LeadSource='Others',recordtypeid=LeadTypeId);
	   leadLst.add(le1);
	    lead le2=new lead(lastname='test lead 1232',Company='My Lead Company2',Email='mylead@sds2.com',	MQL_Score__c='A+',Status='NEW',LeadSource='Others',recordtypeid=LeadTypeId);
	    leadLst.add(le2);
	    lead le3=new lead(lastname='test lead 1233',Company='My Lead Company3',Email='mylead@sds3.com',Status='Account / Oppty',Division__c='IT',ProductGroup__c='OFFICE',LeadSource='Others',recordtypeid=LeadTypeId);
	   leadLst.add(le3);
	    insert leadLst;
	     list<Inquiry__c> iqLst = new list<Inquiry__c>();
	    for(Lead l:leadLst){
	        Inquiry__c iq=new Inquiry__c(Lead__c = l.id,Product_Solution_of_Interest__c='Monitors',	 campaign__c = cmp.id);
    		   iqLst.add(iq);
	    }
	    insert iqLst; 
	      Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
	     Id endCustomer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('End Customer').getRecordTypeId();
        Account acc1 = new account(recordtypeId=endCustomer,Name ='mqlinquirytest', Type='Customer',BillingStreet ='85 Challenger Road',BillingCity='Ridgefield Park',BillingState='NJ',BillingCountry='US',BillingPostalCode='07660');
        insert acc1;
        list<contact> cont=new list<contact>();
         contact con = new contact( LastName='contactinq',Accountid=acc1.id,Email='member@conatc926.com',MQL_Score__c='T1');
          contact con1 = new contact( LastName='mylastnamemember12',Accountid=acc1.id,Email='12member@926email.com');
            cont.add(con);
            cont.add(con1);
            insert cont;
         list<Inquiry__c> iqLstcon = new list<Inquiry__c>();
	    for(contact c:cont){
	        Inquiry__c iq=new Inquiry__c(Contact__c = c.id,Product_Solution_of_Interest__c='Mobile Phones',	 campaign__c = cmp.id);
    		   iqLstcon.add(iq);
	    }
	    insert iqLstcon;    
         Test.stopTest();     
	    
	    
	}
	private static testMethod void testmethod2() {
	     Test.startTest();
         string   CampaignTypeId = [Select Id From RecordType Where SobjectType = 'Campaign' and Name = 'Sales'  limit 1].Id;
	    string   LeadTypeId = [Select Id From RecordType Where SobjectType = 'Lead' and Name = 'End User'  limit 1].Id;
       Campaign cmp= new Campaign(Name='Test Campaign for test class ',	IsActive=true,type='Email',StartDate=system.Today(),recordtypeid=CampaignTypeId);
       insert cmp;
       List<Lead> leadLst = new list<Lead>();
       
       lead le1=new lead(lastname='test lead 123',Company='My Lead Company',Email='mylead@sds.com',	MQL_Score__c='T1',Status='New',LeadSource='Others',recordtypeid=LeadTypeId);
	   leadLst.add(le1);
	    lead le2=new lead(lastname='test lead 1232',Company='My Lead Company2',Email='mylead@sds2.com',	MQL_Score__c='A+',Status='NEW',LeadSource='Others',recordtypeid=LeadTypeId);
	    leadLst.add(le2);
	    lead le3=new lead(lastname='test lead 1233',Company='My Lead Company3',Email='mylead@sds3.com',Status='Account / Oppty',Division__c='IT',ProductGroup__c='OFFICE',LeadSource='Others',recordtypeid=LeadTypeId);
	   leadLst.add(le3);
	    insert leadLst;
	    
	    list<Inquiry__c> iqLst = new list<Inquiry__c>();
	    
	    for(Lead l:leadLst){
	        Inquiry__c iq=new Inquiry__c(Lead__c = l.id,Product_Solution_of_Interest__c='Monitors; Printers; Mobile Phones',	 campaign__c = cmp.id);
    		   iqLst.add(iq);
	    }
	    insert iqLst; 
	     
	       Inquiry__c updateid=[select id,	Inquiry_Status__c from Inquiry__c limit 1];
	       updateid.Inquiry_Status__c='Active';
	       updateid.Sub_Status__c='Attempting contact';
	        updateid.Street__c='85 Challenger Road';
	         updateid.City__c='Ridgefield Park';
	          updateid.State__c='NJ';
	           updateid.Country__c='US';
	            updateid.Zip_Postal_Code__c='07660';
	       
	       update updateid;
	       
         Test.stopTest();     
	    
	    
	}
	private static testMethod void testmethod3() {
	     Test.startTest();
         string   CampaignTypeId = [Select Id From RecordType Where SobjectType = 'Campaign' and Name = 'Sales'  limit 1].Id;
	    string   LeadTypeId = [Select Id From RecordType Where SobjectType = 'Lead' and Name = 'End User'  limit 1].Id;
       Campaign cmp= new Campaign(Name='Test Campaign for test class ',	IsActive=true,type='Email',StartDate=system.Today(),recordtypeid=CampaignTypeId);
       insert cmp;
       List<Lead> leadLst = new list<Lead>();
       
       lead le1=new lead(lastname='test lead 123',Company='My Lead Company',Email='mylead@sds.com',	MQL_Score__c='T1',Status='New',LeadSource='Others',recordtypeid=LeadTypeId);
	   leadLst.add(le1);
	    lead le2=new lead(lastname='test lead 1232',Company='My Lead Company2',Email='mylead@sds2.com',	MQL_Score__c='A+',Status='NEW',LeadSource='Others',recordtypeid=LeadTypeId);
	    leadLst.add(le2);
	    lead le3=new lead(lastname='test lead 1233',Company='My Lead Company3',Email='mylead@sds3.com',Status='Account / Oppty',Division__c='IT',ProductGroup__c='OFFICE',LeadSource='Others',recordtypeid=LeadTypeId);
	   leadLst.add(le3);
	    insert leadLst;
	     list<Inquiry__c> iqLst = new list<Inquiry__c>();
	     Inquiry__c iq=new Inquiry__c(Lead__c = le1.id,Product_Solution_of_Interest__c='Monitors; Printers; Mobile Phones',	 campaign__c = cmp.id);
	    iqLst.add(iq);
	    Inquiry__c iq2=new Inquiry__c(Lead__c = le2.id,Product_Solution_of_Interest__c='Monitors; Printers; Mobile Phones',	 campaign__c = cmp.id);
	    iqLst.add(iq2);
	    Inquiry__c iq3=new Inquiry__c(Lead__c = le3.id,Product_Solution_of_Interest__c='Monitors; Printers; Mobile Phones',	 campaign__c = cmp.id);
	    iqLst.add(iq3);
	    
	    /*for(Lead l:leadLst){
	        Inquiry__c iq=new Inquiry__c(Lead__c = l.id,Product_Solution_of_Interest__c='Monitors; Printers; Mobile Phones',	 campaign__c = cmp.id);
    		   iqLst.add(iq);
	    }*/
	    insert iqLst; 
	    
	    list<Inquiry__c> iqUpdateLst = new list<Inquiry__c>(); 
	       Inquiry__c updateid=[select id,	Inquiry_Status__c from Inquiry__c where id=:iq.id];
	       updateid.Inquiry_Status__c='Unreachable';
	        updateid.Street__c='85 Challenger Road';
	         updateid.City__c='Ridgefield Park';
	          updateid.State__c='NJ';
	           updateid.Country__c='US';
	            updateid.Zip_Postal_Code__c='07660';
	       iqUpdateLst.add(updateid);
	       
	       Inquiry__c updateid2=[select id,	Inquiry_Status__c from Inquiry__c where id=:iq2.id];
	       updateid2.Inquiry_Status__c='Invalid';
	        updateid2.Sub_Status__c='Spam';
	       updateid2.Reason_invalid__c = 'Invalid';
	       iqUpdateLst.add(updateid2);
	       
	       Inquiry__c updateid3=[select id,	Inquiry_Status__c from Inquiry__c where id=:iq3.id];
	       updateid3.Inquiry_Status__c='No Sale';
	       updateid3.Reason_No_Sale__c = 'No Sale';
	       updateid3.Sub_Status__c='Competitor Pricing';
	       iqUpdateLst.add(updateid3);
	       
	       update iqUpdateLst;
	       
         Test.stopTest();     
	    
	    
	}
	
		private static testMethod void testmethod4() {
	     Test.startTest();
         string   CampaignTypeId = [Select Id From RecordType Where SobjectType = 'Campaign' and Name = 'Sales'  limit 1].Id;
	    string   LeadTypeId = [Select Id From RecordType Where SobjectType = 'Lead' and Name = 'End User'  limit 1].Id;
       Campaign cmp= new Campaign(Name='Test Campaign for test class ',	IsActive=true,type='Email',StartDate=system.Today(),recordtypeid=CampaignTypeId);
       insert cmp;
       List<Lead> leadLst = new list<Lead>();
       
       lead le1=new lead(lastname='test lead 123',Company='My Lead Company',Email='mylead@sds.com',	MQL_Score__c='T1',Status='New',LeadSource='Others',recordtypeid=LeadTypeId);
	   leadLst.add(le1);
	    
	    insert leadLst;
	    
	     Inquiry__c iq=new Inquiry__c(Lead__c = le1.id,Product_Solution_of_Interest__c='Monitors; Printers; Mobile Phones',	 campaign__c = cmp.id,isActive__c=true);
	      insert iq;
	    
	    
	   Inquiry__c iq2=iq.clone();
	   iq2.isActive__c=true;
	    insert iq2;
	    
	    
	       
         Test.stopTest();     
	    
	    
	}

}