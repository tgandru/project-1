/*
 * Created By: Joe Jung
 * Date: 2019.09.06
 * Description: Trigger Handler for the InstallationSiteTrigger.
 */
public class InstallationSiteTriggerHandler {
    public static void createNewInstallationSite(List<Installation_Site__c> newInstallationSiteList) {
        List<Installation_Site__c> sameZipCodeInstallationSiteList = [SELECT Id, Zip_Code__c, InstallationSiteAsset__c, Installation_Key__c, Name, Status__c FROM Installation_Site__c WHERE Zip_code__c =: newInstallationSiteList[0].Zip_Code__c ORDER BY CreatedDate ASC];
        List<Installation_Site__c> newZipCodeInstallationSiteList = new List<Installation_Site__c>();

        for(Installation_Site__c i : sameZipCodeInstallationSiteList) {
            if(i.InstallationSiteAsset__c == newInstallationSiteList[0].InstallationSiteAsset__c){
                newZipCodeInstallationSiteList.add(i);
            }
        }
        
        if(newZipCodeInstallationSiteList.size() > 0) {
            if(newInstallationSiteList[0].Status__c == 'Complete'){
                newInstallationSiteList[0].addError('Installation Site Status cannot be \'Complete\' at the time of creation.');
            }
          	String keyOfLastZipCodeInstallationSite = newZipCodeInstallationSiteList[newZipCodeInstallationSiteList.size()-1].Installation_Key__c;
        	String[] nextSiteNumString = keyOfLastZipCodeInstallationSite.split('-');
        	Integer nextSiteNum = Integer.valueof(nextSiteNumString[2])+1;
            newInstallationSiteList[0].Installation_Key__c = newInstallationSiteList[0].Name + '-' + newInstallationSiteList[0].Zip_Code__c + '-' + nextSiteNum;
        } else {
            if(newInstallationSiteList[0].Status__c == 'Complete'){
                newInstallationSiteList[0].addError('Installation Site Status cannot be \'Complete\' at the time of creation.');
            }
            newInstallationSiteList[0].Installation_Key__c = newInstallationSiteList[0].Name + '-' + newInstallationSiteList[0].Zip_Code__c + '-' + 0;
        }
    }
    
    public static void sendEmail(Map<Id,Installation_Site__c> triggerNewMap) {
        
        
        Set<Id> assetId = new Set<Id>();
        for(Installation_Site__c iSite : triggerNewMap.values()) {
            assetId.add(iSite.InstallationSiteAsset__c);
        }
        
        Asset installationAsset = [SELECT Id, End_Customer__c, Opportunity_Owner__c, Installation_Partner__C FROM Asset WHERE Id in: assetId];
        
         for(Installation_Site__c iSite : triggerNewMap.values()) {
             try{
                 List<String> emailList = new List<String>();
                 emailList = Label.IT_Installation_Site_Creation_Email_Notice.split(';');
                 Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                 mail.setToAddresses(emailList);
                 System.debug('emailList ===>  ' + emailList);
                 String subject = '[Installation Site Notification]';
                 mail.setSubject(subject);
                 String body = '<span style="color:red;font-weight:bold;">[Installation Site Notification]</span>';
                 body += '<br/>';
                 body += 'A new IT Installation Site for ' + '<span style="color:#0072c6;">&lt;' + installationAsset.end_customer__c + '&gt;</span>' + ' has been created with the following details: ';
                 body += '<br/>';
                 body += '<span style="color:#0072c6;">&lt;Created Date&gt;:</span> ' + iSite.createddate;
                 body += '<br/>';
                 body += '<span style="color:#0072c6;">&lt;Opportunity Owner&gt;:</span> ' + installationAsset.Opportunity_Owner__c;
                 body += '<br/>';
                 body += '<span style="color:#0072c6;">&lt;Pixel Pitch&gt;:</span> ' + iSite.Pixel_Pitch__c;
                 body += '<br/>';
                 body += '<span style="color:#0072c6;">&lt;Installation Partner&gt;:</span> ' + installationAsset.Installation_Partner__c;
                 body += '<br/>';
                 body += '<span style="color:#0072c6;">&lt;Installation Site&gt;:</span> ' + iSite.Name;
                 body += '<br/>';
                 body += '<span style="color:#0072c6;">&lt;Start Date&gt;:</span> ' + iSite.Start_Date__c;
                 body += '<br/>';
                 body += '<br/>';
                 body += 'You can access it by clicking on this link: ';
                 body += '<br/>';
                 String objectlink = URL.getSalesforceBaseUrl().toExternalForm()+'/'+ iSite.id;
                 body += '<a href='+ objectlink +'>' + objectlink + '</a>';
                 mail.setHtmlBody(body);
                 Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
             } catch(Exception e) {
                 ApexPages.addMessage(New ApexPages.Message(ApexPages.Severity.Error,''+e));
             } 
        }
    }
    
    public static void updateInstallationSite(Map<Id,Installation_Site__c> newTriggerMap, Map<Id,Installation_Site__c> oldTriggerMap) {
        Map<Id,Installation_Site__c> isMap = new Map<Id,Installation_Site__c>([SELECT Id, Zip_Code__c, InstallationSiteAsset__c, Installation_Key__c, Name, Status__c, (SELECT Milestone_Status__c FROM Milestones__r) FROM Installation_Site__c WHERE ID in: oldTriggerMap.keyset() ORDER BY CreatedDate ASC]);
        Map<String,Installation_Site__c> isZipMap = new Map<String,Installation_Site__c>();
        List<Milestone__c> milestoneList = [SELECT ID, Milestone_Status__c FROM Milestone__C WHERE Installation_Site__c in: isMap.keyset()];
        
        for(Installation_Site__c is: isMap.values()) {
            isZipMap.put(is.Zip_Code__c,is);
        }
        
        
        for(Installation_Site__c is:newTriggerMap.values()) {
            Integer countCompletedMilestone = 0;
            //Checks if the new instalation site has any milestone and if more than 3 milestones are completed, then you can create the asset.
            for(Milestone__c ms : milestoneList) {
                if(ms.Milestone_Status__c == 'Completed') {
                    countCompletedMilestone++;
                }
            }
            System.debug('Count Completed Milestone ========> ' + countCompletedMilestone);
            System.debug('Installation Site Status ========> ' + is.Status__c);
            
            if(oldTriggerMap.containsKey(is.Id)) {
                Installation_Site__c oldIs = oldTriggerMap.get(is.Id);
                
                if(is.Zip_Code__c == oldIs.Zip_Code__c) {
                    if(is.Status__c == 'Complete' && countCompletedMilestone < 3){
                        is.addError('Installation Site Status cannot be \'Complete\' if it has less than 3 completed milestones.');
                    }
                    
                    String[] siteNumString = oldIs.Installation_Key__c.split('-');
                    Integer siteNum = Integer.valueof(siteNumString[2]);
                    is.Installation_Key__c = is.Name + '-' + is.Zip_Code__c + '-' + siteNum;
                } else{
                    List<Installation_Site__c> sameZipCodeInstallationSiteList = [SELECT Id, Installation_Key__c, InstallationSiteAsset__c FROM Installation_Site__c WHERE Zip_Code__c =: is.Zip_Code__c ORDER BY CreatedDate ASC];
                    List<Installation_Site__c> newZipCodeInstallationSiteList = new List<Installation_Site__c>();
                    for(Installation_Site__c i : sameZipCodeInstallationSiteList) {
                        if(i.InstallationSiteAsset__c == is.InstallationSiteAsset__c){
                            newZipCodeInstallationSiteList.add(i);
                        }
                    }
                    
                    if(is.Status__c == 'Complete' && countCompletedMilestone < 3){
                        is.addError('Installation Site Status cannot be \'Complete\' if it has less than 3 completed milestones.');
                    }
                    
                    if(newZipCodeInstallationSiteList.size() > 0) {
                        System.debug('newZipCodeInstallationSiteList.size() ===> ' + newZipCodeInstallationSiteList.size());
                        System.debug('newZipCodeInstallationSiteList[newZipCodeInstallationSiteList ===> ' + newZipCodeInstallationSiteList);
                        String keyOfLastZipCodeInstallationSite = newZipCodeInstallationSiteList[newZipCodeInstallationSiteList.size()-1].Installation_Key__c;
                        System.debug('keyOfLastZipCodeInstallationSite ===> ' + keyOfLastZipCodeInstallationSite);
                        String[] nextSiteNumString = keyOfLastZipCodeInstallationSite.split('-');
                        Integer nextSiteNum = Integer.valueof(nextSiteNumString[2])+1;
                        is.Installation_Key__c = is.Name + '-' + is.Zip_Code__c + '-' + nextSiteNum;
                    } else {
                        is.Installation_Key__c = is.Name + '-' + is.Zip_Code__c + '-' + 0;
                    }
                }
            } 
        }
    }
    
    public static void blockInstallationSiteDelete(Map<Id,Installation_Site__c> triggerOldMap) {
        List<Milestone__C> milestoneList = [SELECT Id, Installation_Site__c FROM Milestone__C WHERE Installation_Site__c in: triggerOldMap.keyset()];
        
        Set<Id> installationSiteToBlockDeleteSet = new Set<Id>();
        for(Milestone__C m : milestoneList) {
            if(triggerOldMap.containsKey(m.Installation_Site__c)) {
                installationSiteToBlockDeleteSet.add(m.Installation_Site__c);
            }
        }
        
        for(Installation_Site__c is : triggerOldMap.values()) {
            if(installationSiteToBlockDeleteSet.contains(is.Id))
            	is.adderror('The Installation Site cannot be deleted since it has at least one milestone.');
        }
    }
       
    
    public static void AddressValidation(List<Installation_Site__c> triggernew){
        Map<string,List<Installation_Site__c>> zipToAccMap = new Map<string,List<Installation_Site__c>>();
        Map<string,List<Zipcode_Lookup__c>> zipMap = new Map<string,List<Zipcode_Lookup__c>>();
        Map<String,Set<String>> zipToCity = new Map<String,Set<String>>();
        Map<String,String> zipToStateName = new Map<String,String>();
        Map<String,string> zipToStateCode = new Map<String,String>();
        Map<String,String> zipToCountryCode = new Map<String,String>(); 
        Boolean noCityMsg = false;
        Boolean hasStateMsg = false;
        Boolean hasCountryMsg = false;
        String tempCityMsg = '';
        for(Installation_Site__c a:triggernew){
            if(string.isNotBlank(a.Zip_Code__c)){ 
                if(!zipToAccMap.containsKey(a.Zip_Code__c)){
                    zipToAccMap.put(a.Zip_Code__c, new List<Installation_Site__c>());
                }   
                zipToAccMap.get(a.Zip_Code__c).add(a);
            }
        }
        
        if(zipToAccMap.size()>0){
            
            for(Zipcode_Lookup__c zipRec: [select Id,Name,City_Name__c, Country_Code__c, State_Code__c,State_Name__c from Zipcode_Lookup__c where Name IN: zipToAccMap.keySet()]){               
                
                //Map to hold Zip to CityName 
                if(!zipToCity.containsKey(zipRec.Name)) {
                    zipToCity.put(zipRec.Name, new Set<String>());
                }               
                zipToCity.get(zipRec.Name).add(zipRec.City_Name__c);  
                
                //Map to hold Zip to StateName                 
                zipToStateName.put(zipRec.Name,zipRec.State_Name__c); 
                
                //Map to hold Zip to StateCode                  
                zipToStateCode.put(zipRec.Name,zipRec.State_Code__c); 
                
                //Map to hold Zip to Country                 
                zipToCountryCode.put(zipRec.Name,zipRec.Country_Code__c); 
                
                //Overall Zipcode map
                if(!zipMap.containsKey(zipRec.Name)){
                    zipMap.put(zipRec.Name, new List<Zipcode_Lookup__c>());
                }                  
                zipMap.get(zipRec.Name).add(zipRec);
            }
        }
        String msg = 'For entered Zipcode :';
        Map<String,Schema.RecordTypeInfo> rtMapByName = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
        Id directRT = rtMapByName.get(Label.AcctDirectRecordType).getRecordTypeId();
        Id temporaryRT = rtMapByName.get(Label.AcctTemporaryRecordType).getRecordTypeId();
        //for(String zip:zipToAccMap.keySet()){
        String currentUserProf = [select name from Profile where id=:userinfo.getProfileId()].name;
        system.debug('Current user profile........'+currentUserProf);
        for(Installation_Site__c a:triggernew){    
                
                if( String.isNotBlank(a.Zip_Code__c) && String.isNotBlank(a.State__c) && String.isNotBlank(a.City__c)){
                    if(!zipMap.containsKey(a.Zip_Code__c)){
                        for(Installation_Site__c acc: zipToAccMap.get(a.Zip_Code__c)){
                            acc.addError('Invalid Zipcode');
                        }
                    }
                    else {
                        for(Installation_Site__c acc: zipToAccMap.get(a.Zip_Code__c)){
                            if(zipToCountryCode.get(a.Zip_Code__c) != 'US'){
                                msg+=' Country must be '+zipToCountryCode.get(a.Zip_Code__c);
                                hasCountryMsg = true;
                            }
                            if(zipToStateCode.get(a.Zip_Code__c) != acc.State__c && zipToStateName.get(a.Zip_Code__c) != acc.State__c){
                                if(hasCountryMsg){
                                    msg+=',';    
                                }
                                msg+=' State must be '+zipToStateCode.get(a.Zip_Code__c);
                                hasStateMsg = true; 
                            }
                            if(zipToCity.containsKey(a.Zip_Code__c)){
                                Integer i=0;
                                for(String tempCity : zipToCity.get(a.Zip_Code__c)){
                                    if(tempCity == acc.City__c){
                                        noCityMsg = true;
                                        continue;
                                    }
                                    else {
                                        i+=1;
                                        if(i<=1)
                                            tempCityMsg+= tempCity;
                                        else
                                            tempCityMsg+=', '+tempCity;
                                    }
                                }
                            }
                            if(noCityMsg == false){
                                if(hasStateMsg || hasCountryMsg)
                                    msg+=', City must be '+tempCityMsg;
                                else
                                    msg+=' City must be '+tempCityMsg;
                            }
                            if(msg.length()>21)acc.addError(msg);
                        }
                    }
                }
                else {
                    a.addError('Please populate complete  Address');
                }
            
        }
    }
}