@isTest
public class InstallationSiteTriggerHandler_Test {
	static Id ITRT = TestDataUtility.retrieveRecordTypeId('IT', 'Opportunity');
    
    @isTest(SeeAllData=true)
    public static void createNewInstallationSiteTest() {

    	Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }
        Account testAccount1 = new Account (
          	Name = 'testAccount1',
            RecordTypeId = rtMap.get('Account' + 'End_User'),
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        
        );
        insert testAccount1;
        
        Account installationPartnerAccount = new Account (
          	Name = 'installationPartnerAccount',
            RecordTypeId = rtMap.get('Account' + 'Indirect'),
            Type = 'Installation Partner',
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        );
        insert installationPartnerAccount;
        
        Account distributorAccount = new Account (
          	Name = 'DistributorAccount',
            RecordTypeId = rtMap.get('Account' + 'Indirect'),
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        );
        insert distributorAccount;
        
        PriceBook2 pb = TestDataUtility.createPriceBook('IT Distributor'); 
        pb.Division__c='IT';
        pb.SAP_Price_Account_Code__c='1234500';
        pb.SAP_Price_Group_Code__c = '9999';
        insert pb;

        Opportunity testOpp1 = new Opportunity (
          Name = 'testOpp1',
          RecordTypeId = rtMap.get('Opportunity' + 'IT'),
          //Project_Type__c = 'SFNC',
          Project_Name__c = 'TestOpp1',
          AccountId = testAccount1.Id,
          StageName = 'Identified',
          CloseDate = Date.today(),
          Roll_Out_Start__c = Date.today(),
          Rollout_Duration__c = 5,
          Number_of_Living_Units__c = 10000,
          Division__c = 'IT',
          ProductGroupTest__c = 'SMART_SIGNAGE',
          Pricebook2Id = pb.Id
        );
        insert testOpp1;
        
        Opportunity oppForAsset = [SELECT ID, StageName, Primary_Distributor__c,Primary_Reseller__c, Roll_Out_Start__c, Rollout_Duration__c From Opportunity WHERE ID =: testOpp1.Id];
       	
        Partner__c PrimaryDistributor = new Partner__c();
        PrimaryDistributor.Opportunity__c = oppForAsset.Id;
        PrimaryDistributor.Partner__C = installationPartnerAccount.Id;
        PrimaryDistributor.Is_Primary__c = true;
        PrimaryDistributor.Role__c = 'Distributor';
        insert PrimaryDistributor;

        oppForAsset.Primary_Distributor__c = PrimaryDistributor.Partner__C;
        
        
        Partner__c PrimaryReseller = new Partner__c();
        PrimaryReseller.Opportunity__c = oppForAsset.Id;
        PrimaryReseller.Partner__C = installationPartnerAccount.Id;
        PrimaryReseller.Is_Primary__c = true;
        PrimaryReseller.Role__c = 'Reseller';
        insert PrimaryReseller;
        
        oppForAsset.Primary_Reseller__C = PrimaryReseller.Partner__C;
        
        List<String> installationskus=new List<String>();
        installationskus=Label.IT_Installation_SKU_Code.Split(';');
        System.debug('Installationskus ====> '+installationskus);
        
        Product2 standaloneProd = [SELECT Id, Description, Unit_Cost__c, Product_Group__c,Product_Type__c FROM Product2 WHERE SAP_Material_ID__C in: installationskus AND NAme ='DC32E'];
        
        //create the pricebook entries for the custom pricebook
        PriceBookEntry standaloneProdPBE=TestDataUtility.createPriceBookEntry(standaloneProd.Id, pb.Id);
        insert standaloneProdPBE;

        standaloneProdPBE = [SELECT ID,Name,UnitPrice FROM PriceBookEntry WHERE ID =: standaloneProdPBE.Id];
        System.debug('PriceBookEntry ===> ' + standaloneProdPBE.Name);
        
        //Creating opportunity Line Item
        OpportunityLineItem standaloneProdOLI=TestDataUtility.createOpptyLineItem(standaloneProd, standaloneProdPBE, oppForAsset);
        standaloneProdOLI.Quantity = 10;
        standaloneProdOLI.TotalPrice = standaloneProdOLI.Quantity * standaloneProdPBE.UnitPrice;
        insert standaloneProdOLI;
        standaloneProdOLI = [SELECT ID,Name FROM OpportunityLineItem WHERE ID =: standaloneProdOLI.Id];
        System.debug('OpportunityLineItem ===> ' + standaloneProdOLI.Name);
        
        oppForAsset.StageName = 'Qualified';
        
        System.debug('JoeTest Opportunity StageName ===> ' + oppForAsset.StageName);
        System.debug('JoeTest Opportunity Roll_Out_Start__c ===> ' + oppForAsset.Roll_Out_Start__c);
        System.debug('JoeTest Opportunity Rollout_Duration__c ===> ' + oppForAsset.Rollout_Duration__c);
        System.debug('JoeTest Opportunity Primary_Distributor__c ===> ' + oppForAsset.Primary_Distributor__c);
        System.debug('JoeTest Opportunity Primary_Reseller__C ===> ' + oppForAsset.Primary_Reseller__C);
        Test.StartTest();
        update oppForAsset;
        
        oppForAsset = [SELECT ID, AccountId, StageName, Primary_Distributor__c,Primary_Reseller__c, Roll_Out_Start__c, Rollout_Duration__c From Opportunity WHERE ID =: oppForAsset.Id];
        
        Asset asset = new Asset();
        asset.Name = 'testasset';
		asset.Project_Name__c = 'testasset';
        asset.AccountId = oppForAsset.AccountId;
        asset.Opportunity__c = oppForAsset.Id;
        asset.Installation_Partner__C = oppForAsset.Primary_Reseller__c;
        asset.Installation_Start_Date__c = date.today();
        asset.Installation_End_Date__c = date.today();
        insert asset;
        
        //Testing when there is no previous Installation Site record.
        Installation_Site__C is = new Installation_Site__C();
        is.InstallationSiteAsset__c = asset.Id;
        is.name= 'testsite';
        is.Pixel_Pitch__c = 'IW008J';
        is.Start_Date__c = date.today();
        is.End_Date__C = date.today();
        is.Status__c = 'Not Scheduled';
        is.Installer_Phone__c = '123-456-7890';
        is.Installer_Email__c = 'test@gmail.com';
        is.Street__c = '100 Challenger Rd';
        is.City__c = 'Ridgefield Park';
        is.State__c = 'NJ';
        is.Zip_Code__c = '07660'; 
        is.LED_Wall_X_Cabinets_Wide__c = 1;
        is.LED_Wall_Y_Cabinets_High__c = 1;
        is.Customer_Spares__c = 1;
        is.Type__c = 'Mounting System';
        
        insert is;
        
        is = [SELECT Id, Name,Installation_Key__c FROM Installation_Site__C WHERE Id =: is.Id];
        System.assertEquals('testsite-07660-0', is.Installation_Key__c);
        
        //Testing when there is more than one Installation Site record.
        Installation_Site__C is2 = new Installation_Site__C();
        is2.InstallationSiteAsset__c = asset.Id;
        is2.name= 'testsite2';
        is2.Pixel_Pitch__c = 'IW008J';
        is2.Start_Date__c = date.today();
        is2.End_Date__C = date.today();
        is2.Status__c = 'Not Scheduled';
        is2.Installer_Phone__c = '123-456-7890';
        is2.Installer_Email__c = 'test@gmail.com';
        is2.Street__c = '100 Challenger Rd';
        is2.City__c = 'Ridgefield Park';
        is2.State__c = 'NJ';
        is2.Zip_Code__c = '07660';   
        is2.LED_Wall_X_Cabinets_Wide__c = 1;
        is2.LED_Wall_Y_Cabinets_High__c = 1;
        is2.Customer_Spares__c = 1;
        is2.Type__c = 'Mounting System';
        
        insert is2;
        
        is2 = [SELECT Id, Name, Installation_Key__c FROM Installation_Site__C WHERE Id =: is2.Id];
        System.assertEquals('testsite2-07660-1',is2.Installation_Key__c);
        
        is2.City__c = 'Leonia';
        is2.Zip_Code__c = '07605';
        update is2;
        is2 = [SELECT Id, Name, Installation_Key__c, status__C FROM Installation_Site__C WHERE Id =: is2.Id];
        System.assertEquals('testsite2-07605-0',is2.Installation_Key__c);
        
        is2.Status__c = 'Complete';
        
        List<milestone__C> msList = new List<Milestone__c>();
        for(Integer i = 0; i < 3; i++) {
            Milestone__c ms = new Milestone__C(Milestone_type__c = 'Received SOW', Milestone_Status__c='Completed', Installation_site__c = is2.Id);
            msList.add(ms);
        }
        
        insert msList;
        
        update is2;
        
        is2 = [SELECT Id, Name, Installation_Key__c, status__C FROM Installation_Site__C WHERE Id =: is2.Id];
        System.assertEquals('Complete',is2.status__c);
        
        delete is;
        
        //Testing when there is more than one Installation Site record.
        /*
        Installation_Site__C is3 = new Installation_Site__C();
        is3.InstallationSiteAsset__c = asset.Id;
        is3.name= 'testsite3';
        is3.Pixel_Pitch__c = 'IW008J';
        is3.Start_Date__c = date.today();
        is3.End_Date__C = date.today();
        is3.Status__c = 'Complete';
        is3.Installer_Phone__c = '123-456-7890';
        is3.Installer_Email__c = 'test@gmail.com';
        is3.Street__c = '100 Challenger Rd';
        is3.City__c = 'Ridgefield Park';
        is3.State__c = 'NJ';
        is3.Zip_Code__c = '07660';   
        is3.LED_Wall_X_Cabinets_Wide__c = 1;
        is3.LED_Wall_Y_Cabinets_High__c = 1;
        is3.Customer_Spares__c = 1;
        is3.Type__c = 'Mounting System';
        
        insert is3;
		*/
        Test.StopTest();
    }
}