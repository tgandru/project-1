@isTest
private class IntegrationUpdateAccountTest {
	static Id endUserRT = TestDataUtility.retrieveRecordTypeId('End_User', 'Account'); 
    static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
    static Id indirectRT = TestDataUtility.retrieveRecordTypeId('Indirect','Account');
    //static Id HART = TestDataUtility.retrieveRecordTypeId('HA Builder','Account');

	@testSetup static void setup() {
        //Channelinsight configuration
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();

        List<Integration_EndPoints__c> ieps = new List<Integration_EndPoints__c>();
        Integration_EndPoints__c iep = new Integration_EndPoints__c();
        iep.name ='Flow-012';
        iep.endPointURL__c = 'https://www.google.com';
        iep.layout_id__c = 'LAY024197';
        iep.partial_endpoint__c = '/SFDC_IF_012';
        ieps.add(iep);
        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='Flow-003';
        iep2.endPointURL__c = 'https://www.google.com';
        iep2.layout_id__c = 'LAY024197';
        iep2.partial_endpoint__c = '/SFDC_IF_012';
        ieps.add(iep2);
        insert ieps;

        List<Account> accts=new List<Account>();
        Account acct=TestDataUtility.createAccount('Test Acct');
        acct.integration_Status__c=null;
        acct.Business_Partner_Tier__c=null;
        acct.Service_Partner_Tier__c=null;
        acct.Key_Account__c=false;
        acct.Account_Level__c='Level5';
        insert acct;

	}
		@isTest static void test_method_insert1() {
	


          Profile p1 = [SELECT Id FROM Profile WHERE Name='Integration User']; 
          
          list<user> ur=new list<User>();
            User u1 = new User(Alias = 'standt', Email='sysAdminuser@testorg.com', 
              EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
              LocaleSidKey='en_US', ProfileId = p1.Id, 
              TimeZoneSidKey='America/Los_Angeles', UserName=TestDataUtility.generateRandomString(7)+'@testorg.com');
              
                ur.add(u1);
             
                 insert ur;
            System.runAs(u1) {
               Account a = TestDataUtility.createAccount('Account 1');
               a.SAP_Company_Code__c='99900668';
              a.Company_Code__c='c460';
              a.Phone='(555) 555-5555';
              a.Fax='4444444444';
              a.Website='www.salesforce.com';
              a.Email__c='bjyy024@163.com';
              a.Direct_Account__c=false;
              a.Indirect_Account__c=false;
              a.End_Customer_Account__c=false;
              a.External_Top_Tier__c='ExTop';
              a.Business_Partner_Tier__c='BTier';
              a.Service_Partner_Tier__c='Servic';
              a.Solution_Partner_Tier__c='Soluti';
              a.external_industry_type__c='ind';
              a.external_business_partner_type__c='com';
              a.External_Industry__c='IT';
              a.Key_Account__c=true;
              a.recordtypeid=endUserRT;
              a.Account_Level__c='Level1';
              //a.parent_sap_company_code__c='1234';
              //a.Parent_Account_Level__c='Level4';
            
              insert a;
              Account a1 = a;
              a1.recordtypeid=indirectRT;
              
        System.assertEquals(p1.Id, u1.ProfileId);
      }
       
		    
		}
	@isTest static void test_method_insert() {
		//find inserted mq for 3 and send it off
		List<Account> accts1=new List<Account>();
         Account acct=new Account(Name ='Test 1',Key_Account__c=false, Type='Customer',recordtypeid=endUserRT,Account_Level__c='Level3',Approval_Status__c='Approved');
         Account acct1=new Account(Name ='Test 2',Key_Account__c=false, Type='Customer',recordtypeid=endUserRT,Account_Level__c='Level2',DUP_AFLAG__c = 'X',DUP_REMARK__c = 'SFDC Id has been Created',DUP_ADATE__c =  Datetime.now().format('yyyyMMdd'));
         Account acct2=new Account(Name ='Test 3', Type='Customer',recordtypeid=endUserRT,Account_Level__c='Level4',DUP_AFLAG__c = 'X',DUP_REMARK__c = 'SFDC Id has been Created',DUP_ADATE__c =  Datetime.now().format('yyyyMMdd'));
          Account acct3=new Account(Name ='Test 3', Type='Customer',recordtypeid=endUserRT,Account_Level__c='Level1',DUP_AFLAG__c = 'X',DUP_REMARK__c = 'SFDC Id has been Created',DUP_ADATE__c =  Datetime.now().format('yyyyMMdd'));
         accts1.add(acct);
         accts1.add(acct1);
         accts1.add(acct2);
         accts1.add(acct3);
         insert accts1;
		message_queue__c mq=[select id,Integration_Flow_Type__c, Identification_Text__c, retry_counter__c,Status__c,Object_Name__c from message_queue__c where Integration_Flow_Type__c='003' and Identification_Text__c=:acct.id];
		message_queue__c mq1=[select id,Integration_Flow_Type__c, Identification_Text__c, retry_counter__c,Status__c,Object_Name__c from message_queue__c where Integration_Flow_Type__c='003' and Identification_Text__c=:acct1.id];
		message_queue__c mq2=[select id,Integration_Flow_Type__c, Identification_Text__c, retry_counter__c,Status__c,Object_Name__c from message_queue__c where Integration_Flow_Type__c='003' and Identification_Text__c=:acct2.id];
		message_queue__c mq3=[select id,Integration_Flow_Type__c, Identification_Text__c, retry_counter__c,Status__c,Object_Name__c from message_queue__c where Integration_Flow_Type__c='003' and Identification_Text__c=:acct3.id];

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockResponseGenerator());
		//IntegrationUpdateAccountTriggerHelper cont=new IntegrationUpdateAccountTriggerHelper();
		IntegrationUpdateAccountTriggerHelper.recieveInsertedAccts(mq);
		IntegrationUpdateAccountTriggerHelper.recieveInsertedAccts(mq1,acct1);
		IntegrationUpdateAccountTriggerHelper.recieveInsertedAccts(mq2,acct2);
		IntegrationUpdateAccountTriggerHelper.recieveInsertedAccts(mq3,acct3);

        /*message_queue__c mq2=[select id,Integration_Flow_Type__c, Identification_Text__c, retry_counter__c,Status__c,Object_Name__c from message_queue__c where Integration_Flow_Type__c='004'];
        system.debug('lclc mq udpate? '+mq2);

        Test.setMock(HttpCalloutMock.class, new MockResponseGenerator());
        //IntegrationUpdateAccountTriggerHelper cont=new IntegrationUpdateAccountTriggerHelper();
        IntegrationUpdateAccountTriggerHelper.recieveUpdatedAccts(mq);*/
        Test.stopTest();
	}

    @isTest static void test_method_update(){
        account acc=[select id,phone,Business_Partner_Tier__c from account];
        System.debug('lclc before '+acc.Business_Partner_Tier__c);
        acc.Business_Partner_Tier__c='Registered';
        System.debug('lclc after '+acc.Business_Partner_Tier__c);
        acc.Integration_Status__c=null;
        acc.SAP_Company_Code__c='99999888';
        update acc;
        System.debug('lclc acc '+acc);

        message_queue__c mq=[select id,Integration_Flow_Type__c, Identification_Text__c, retry_counter__c,Status__c,Object_Name__c from message_queue__c where Integration_Flow_Type__c='004'];
        system.debug('lclc mq udpate? '+mq);

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockResponseGenerator());
        //IntegrationUpdateAccountTriggerHelper cont=new IntegrationUpdateAccountTriggerHelper();
        IntegrationUpdateAccountTriggerHelper.recieveUpdatedAccts(mq);
        Test.stopTest();
    }

    class MockResponseGenerator implements HttpCalloutMock {
    // Implement this interface method
        public HTTPResponse respond(HTTPRequest req) {
            // Optionally, only send a mock response for a specific endpoint
            // and method.
            //System.debug('END POINT '+req.getEndpoint());
            //System.assertEquals('callout:X012_013_ROI', req.getEndpoint());
            //System.assertEquals('POST', req.getMethod());
            
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{' +
                            '"inputHeaders": {'+
                            '"MSGGUID": "ada747b2-dda9-1931-acb2-14a2872d4358",' +
                            '"IFID": "TBD",' +
                            '"IFDate": "160317",' +
                            '"MSGSTATUS": "S",' +
                            '"ERRORTEXT": null,' +
                            '"ERRORCODE": null' +
                                            '},' +
                            '"body": {'+
                            '"sapCompanyCode" : "9289493"'+
                                            '}'+
                         '}');
            res.setStatusCode(200);
            return res;



        }
    }
	
}