public with sharing class IntegrationUpdateAccountTriggerHelper {


public static Profile integrationUser{get;set;}
    public static String  userProf {get;set;}

    public static String directRT=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Direct').getRecordTypeId();
    public static String tempRT=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Temporary').getRecordTypeId();
    public static String HA_Builder_Recordtype =Schema.SObjectType.Account.getRecordTypeInfosByName().get('HA Builder').getRecordTypeId(); // added by vijay 
    public static String HA_Builder_Distributor_recordtype =Schema.SObjectType.Account.getRecordTypeInfosByName().get('HA Builder Distributor').getRecordTypeId();

    public void onBeforeInsert(List<Account> newAcctLst){
        if(integrationUser == null){
            integrationUser=[select Id from Profile where Name='Integration User' limit 1];
            userProf=UserInfo.getProfileId();
        }

        if(userProf == integrationUser.Id && UserInfo.getUserId()!='0051Q00000GpV4aQAF'){
            return;
        }else{
            for(Account acc:newAcctLst){
                if(acc.RecordTypeId!=tempRT && acc.RecordTypeId!=directRT){
                    acc.Integration_Status__c='003 In Process';
                }
            }
        }
    }

    public void onInsert(List<Account> newAcctLst){
        if(integrationUser == null){
            integrationUser=[select Id from Profile where Name='Integration User' limit 1];
            userProf=UserInfo.getProfileId();
        }
        if(userProf == integrationUser.Id && UserInfo.getUserId()!='0051Q00000GpV4aQAF'){
            return;
        }else{
            Map<Id, message_queue__c> mqExisting=new Map<Id,message_queue__c>(); //acct id to message queue
            Set<Id> accIds=new Set<Id>();
            for(Account acc:newAcctLst){
                if(acc.RecordTypeId!=tempRT && acc.RecordTypeId!=directRT ){
                    accIds.add(acc.Id);
                }
            }
            List<message_queue__c> mqLst=[select id,Identification_Text__c,Integration_Flow_Type__c, Status__c 
                                                            from message_queue__c 
                                                            where Identification_Text__c In :accIds AND Integration_Flow_Type__c='004' 
                                                            AND (Status__c='not started' OR Status__c='failed-retry')];
            if(!mqLst.isEmpty()){
                for(message_queue__c mq: mqLst){
                    mqExisting.put(mq.Identification_Text__c,mq);
                }
            }


            List<message_queue__c> mqs=new List<message_queue__c>();
            for(Account acc:newAcctLst){
                if(acc.RecordTypeId!=tempRT && acc.RecordTypeId!=directRT && acc.RecordTypeId != HA_Builder_Recordtype &&acc.RecordTypeId  !=HA_Builder_Distributor_recordtype  ){
                    System.debug('lclc added new mq '+acc);
                    if(mqExisting.isEmpty()){
                        message_queue__c mq=new message_queue__c(Identification_Text__c=acc.Id, Integration_Flow_Type__c='003',retry_counter__c=0, Object_Name__c='Account');
                       
                            mq.Status__c='not started';
                       
                        mq.IFID__c=(!Test.isRunningTest() && Integration_EndPoints__c.getInstance('Flow-003').layout_id__c!=null) ? Integration_EndPoints__c.getInstance('Flow-003').layout_id__c : '';
                        mqs.add(mq);
                    }else{
                        if(mqExisting.get(acc.Id)==null){
                            message_queue__c mq=new message_queue__c(Identification_Text__c=acc.Id, Integration_Flow_Type__c='003',retry_counter__c=0, Object_Name__c='Account');
                            
                            mq.Status__c='not started';
                            
                            mq.IFID__c=(!Test.isRunningTest() && Integration_EndPoints__c.getInstance('Flow-003').layout_id__c!=null) ? Integration_EndPoints__c.getInstance('Flow-003').layout_id__c : '';
                            mqs.add(mq);
                        }
                    }
                }
            }
            if(!mqs.isEmpty()){
                insert mqs;
            }
        }
    }

    public void onUpdate(List<Account> newAcctLst, Map<Id, Account> oldAcctMap){
        if(integrationUser == null){
            integrationUser=[select Id from Profile where Name='Integration User' limit 1];
            userProf=UserInfo.getProfileId();
        }
        if(userProf == integrationUser.Id && UserInfo.getUserId()!='0051Q00000GpV4aQAF'){
            return;
        }else{
            List<message_queue__c> mqs=new List<message_queue__c>();
            Map<Id, message_queue__c> mqExisting=new Map<Id,message_queue__c>(); //acct id to message queue
            Set<Id> accIds=new Set<Id>();
            for(Account acc:newAcctLst){
                if(acc.RecordTypeId!=tempRT && acc.RecordTypeId!=directRT){
                    accIds.add(acc.Id);
                }
            }
            List<message_queue__c> mqLst=[select id,Identification_Text__c,Integration_Flow_Type__c, Status__c 
                                                            from message_queue__c 
                                                            where Identification_Text__c In :accIds AND Integration_Flow_Type__c='004' 
                                                            AND (Status__c='not started' OR Status__c='failed-retry')];
            if(!mqLst.isEmpty()){
                for(message_queue__c mq: mqLst){
                    mqExisting.put(mq.Identification_Text__c,mq);
                }
            }


            for(Account acc:newAcctLst){
                if(acc.Integration_Status__c!='003 In Process' && acc.RecordTypeId!=tempRT && acc.RecordTypeId!=directRT){
                    Account oldAcc=oldAcctMap.get(acc.Id);
                    if( acc.Company_Code__c!=oldAcc.Company_Code__c || acc.Name!=oldAcc.Name || acc.BillingStreet!=oldAcc.BillingStreet ||
                        acc.BillingCity!=oldAcc.BillingCity || acc.BillingCountry!=oldAcc.BillingCountry || acc.BillingState!=oldAcc.BillingState ||
                        acc.BillingPostalCode!=oldAcc.BillingPostalCode || acc.Phone!=oldAcc.Phone || acc.Fax != oldAcc.Fax || acc.Website!=oldAcc.Website ||
                        acc.Email__c!=oldAcc.Email__c || acc.Direct_Account__c!=oldAcc.Direct_Account__c || acc.Indirect_Account__c!=oldAcc.Indirect_Account__c ||
                        acc.End_Customer_Account__c!=oldAcc.End_Customer_Account__c || acc.External_Top_Tier__c!= oldAcc.External_Top_Tier__c ||
                        acc.Business_Partner_Tier__c!=oldAcc.Business_Partner_Tier__c || acc.Service_Partner_Tier__c!=oldAcc.Service_Partner_Tier__c ||
                        acc.Solution_Partner_Tier__c!=oldAcc.Solution_Partner_Tier__c || acc.external_industry_type__c!=oldAcc.external_industry_type__c ||
                        acc.external_business_partner_type__c!=oldAcc.external_business_partner_type__c || acc.External_Industry__c!=oldAcc.External_Industry__c ||
                        acc.Key_Account__c!=oldAcc.Key_Account__c || acc.Account_Level__c!=oldAcc.Account_Level__c || acc.parent_sap_company_code__c!=oldAcc.parent_sap_company_code__c ||
                        acc.Parent_Account_Level__c!=oldAcc.Parent_Account_Level__c ){
                            
                            if(mqExisting.isEmpty()){
                                message_queue__c mq=new message_queue__c(Identification_Text__c=acc.Id, Integration_Flow_Type__c='004',retry_counter__c=0,Status__c='not started', Object_Name__c='Account');
                                mq.IFID__c=(!Test.isRunningTest() && Integration_EndPoints__c.getInstance('Flow-004').layout_id__c!=null) ? Integration_EndPoints__c.getInstance('Flow-004').layout_id__c : '';
                                mqs.add(mq);
                            }else{
                                if(mqExisting.get(acc.Id)==null){
                                    message_queue__c mq=new message_queue__c(Identification_Text__c=acc.Id, Integration_Flow_Type__c='004',retry_counter__c=0,Status__c='not started', Object_Name__c='Account');
                                    mq.IFID__c=(!Test.isRunningTest() && Integration_EndPoints__c.getInstance('Flow-004').layout_id__c!=null) ? Integration_EndPoints__c.getInstance('Flow-004').layout_id__c : '';
                                    mqs.add(mq);
                                }
                            }
                            
                            
                    }
                } 
            }

            if(!mqs.isEmpty()){
                insert mqs;
            }
        }       
        
    }
    
    
    public static void recieveInsertedAccts(message_queue__c omq, Account recvdAcc){
        

        
        Account acc=[select id, billingStreet, billingCity, billingCountry, billingState, billingPostalCode,
                        Name, SAP_Company_Code__c, Phone, Fax, Website, Email__c, Direct_Account__c,
                        Indirect_Account__c, End_Customer_Account__c, External_Top_Tier__c, Business_Partner_Tier__c,
                        Service_Partner_Tier__c, Solution_Partner_Tier__c, external_industry_type__c, External_Industry__c, Partner_top_tier__c,
                        Key_Account__c, Account_Level__c, parent_sap_company_code__c, Parent_Account_Level__c, external_business_partner_type__c,
                        RecordType.DeveloperName,DUP_AFLAG__c,DUP_REMARK__c ,DUP_ADATE__c,DUP_AUSER__c
                        from Account where Id=:omq.Identification_Text__c limit 1];
        acc.DUP_AUSER__c = recvdAcc.DUP_AUSER__c;
        acc.DUP_ADATE__c = recvdAcc.DUP_ADATE__c;
        acc.DUP_REMARK__c = recvdAcc.DUP_REMARK__c;
        acc.DUP_AFLAG__c = recvdAcc.DUP_AFLAG__c;
        if(acc.RecordType.DeveloperName.contains('End_User') || acc.RecordType.DeveloperName.contains('HTV') ){
            acc.End_Customer_Account__c=true;
        }else if(acc.RecordType.DeveloperName.contains('Indirect') || acc.RecordType.DeveloperName.contains('CBD_Indirect')){
            acc.Indirect_Account__c=true;
        }/*else{

        }*/
        //Bala: moved the try & response variable from below.
        string response='';
        try{
        string partialEndpoint = Test.isRunningTest() ? 'asdf' : Integration_EndPoints__c.getInstance('Flow-003').partial_endpoint__c;
        string layoutId = Test.isRunningTest() ? 'asdf' : Integration_EndPoints__c.getInstance('Flow-003').layout_id__c;

        cihStub.UpdateAccountRequest outerStub=new cihStub.UpdateAccountRequest();
        cihStub.msgHeadersRequest headers=new cihStub.msgHeadersRequest(layoutId);
        cihStub.UpdateAccountBodyRequest body=new cihStub.UpdateAccountBodyRequest();
        mapfields(body, acc, 'insert');
        outerStub.body=body;
        outerStub.inputHeaders=headers;

        string requestBody=json.serialize(outerStub);
        //string response='';
        //Bala: Moving the try block to the top to handle error from mapfields method
        //try{
            System.debug('lclc requestBody '+requestBody);
            response = webcallout(requestBody, partialEndpoint); 
            system.debug('lclc response ' + response);
        }catch(exception ex){
            String errmsg = 'ERROR='+ex.getmessage() + ' - '+ex.getStackTraceString();
            system.debug('error 1 '+errmsg); 
            handleError(omq, null,errmsg);
            return;
        }


        cihStub.UpdateAccountResponse stubResponse=new cihStub.UpdateAccountResponse();
        String responseFake='{"inputHeaders":{"MSGGUID":"a6bba321-2183-5fb4-ff75-4d024e39a98e","IFID":"TBD","IFDate":"160317","MSGSTATUS":"S","ERRORTEXT":null,"ERRORCODE":null},"body":{"sapCompanyCode":"9289493"}}';
        
        if(response!=null && String.isNotBlank(response)){
       //   if(responseFake!=null){
            
            //stubResponse= (cihStub.UpdateAccountResponse) json.deserialize(responseFake, cihStub.UpdateAccountResponse.class);
            stubResponse= (cihStub.UpdateAccountResponse) json.deserialize(response, cihStub.UpdateAccountResponse.class);
            System.debug('lclc stubResponse '+stubResponse);
            System.debug('lclc msgstatus '+stubResponse.inputHeaders.MSGSTATUS);
            if(stubResponse.inputHeaders.MSGSTATUS=='S'){
                System.debug('lclc 1');
                if(stubResponse.body.sapCompanyCode!=null){
                    System.debug('lclc 2');
                    acc.SAP_Company_Code__c=stubResponse.body.sapCompanyCode;
                    try{
                        acc.Integration_Status__c=null;
                        System.debug('lclc 3 begin ');
                        update acc;
                        System.debug('lclc 3 end '+acc);
                        handleSuccess(omq,stubResponse);
                        System.debug('lclc 4');
                    }catch(exception ex){
                        String errmsg = 'ERROR='+ex.getmessage() + ' - '+ex.getStackTraceString();
                        System.debug('error 2 '+errmsg);
                        handleError(omq, stubResponse, errmsg);
                    }
                }   
            }else{
                System.debug('lclc 5');
                /*if(stubResponse.inputHeaders.ERRORTEXT.contains('Duplicate')){
                    //duplicate error message like "Duplicated Codes:0004858049,584995940"
                    String[] codes=stubResponse.inputHeaders.ERRORTEXT.substringAfter(':').split(',');
                    if(codes.size()>0){
                        acc.SAP_Company_Code__c=codes[0];
                        try{
                            acc.Integration_Status__c=null;
                            update acc;
                            handleSuccess(omq,stubResponse);
                        }catch(exception ex){
                            System.debug('error 3 '+ex.getMessage());
                            handleError(omq,stubResponse);
                        }
                    }

                }else{
                    System.debug('lclc msgstatus f ');
                    handleError(omq,stubResponse);
                }*/
                String errmsg = 'ERROR=The MSGSTATUS is not S.';
                handleError(omq,stubResponse,errmsg);
            }
        }else{
            System.debug('lclc no response');
            handleError(omq,null,'ERROR=no response');
        }

    
    }

    public static void recieveInsertedAccts(message_queue__c omq){

        
        Account acc=[select id, billingStreet, billingCity, billingCountry, billingState, billingPostalCode,
                        Name, SAP_Company_Code__c, Phone, Fax, Website, Email__c, Direct_Account__c,
                        Indirect_Account__c, End_Customer_Account__c, External_Top_Tier__c, Business_Partner_Tier__c,
                        Service_Partner_Tier__c, Solution_Partner_Tier__c, external_industry_type__c, External_Industry__c, Partner_top_tier__c,
                        Key_Account__c, Account_Level__c, parent_sap_company_code__c, Parent_Account_Level__c, external_business_partner_type__c,
                        RecordType.DeveloperName,DUP_AFLAG__c,DUP_REMARK__c ,DUP_ADATE__c,DUP_AUSER__c
                        from Account where Id=:omq.Identification_Text__c limit 1];

        if(acc.RecordType.DeveloperName.contains('End_User') || acc.RecordType.DeveloperName.contains('HTV') ){
            acc.End_Customer_Account__c=true;
        }else if(acc.RecordType.DeveloperName.contains('Indirect') || acc.RecordType.DeveloperName.contains('CBD_Indirect')){
            acc.Indirect_Account__c=true;
        }/*else{

        }*/
        //Bala: moved the try & response variable from below.
        string response='';
        try{
        string partialEndpoint = Test.isRunningTest() ? 'asdf' : Integration_EndPoints__c.getInstance('Flow-003').partial_endpoint__c;
        string layoutId = Test.isRunningTest() ? 'asdf' : Integration_EndPoints__c.getInstance('Flow-003').layout_id__c;

        cihStub.UpdateAccountRequest outerStub=new cihStub.UpdateAccountRequest();
        cihStub.msgHeadersRequest headers=new cihStub.msgHeadersRequest(layoutId);
        cihStub.UpdateAccountBodyRequest body=new cihStub.UpdateAccountBodyRequest();
        mapfields(body, acc, 'insert');
        outerStub.body=body;
        outerStub.inputHeaders=headers;

        string requestBody=json.serialize(outerStub);
        //string response='';
        //Bala: Moving the try block to the top to handle error from mapfields method
        //try{
            System.debug('lclc requestBody '+requestBody);
            response = webcallout(requestBody, partialEndpoint); 
            system.debug('lclc response ' + response);
        }catch(exception ex){
            String errmsg = 'ERROR='+ex.getmessage() + ' - '+ex.getStackTraceString();
            system.debug('error 1 '+errmsg); 
            handleError(omq, null,errmsg);
            return;
        }


        cihStub.UpdateAccountResponse stubResponse=new cihStub.UpdateAccountResponse();
        String responseFake='{"inputHeaders":{"MSGGUID":"a6bba321-2183-5fb4-ff75-4d024e39a98e","IFID":"TBD","IFDate":"160317","MSGSTATUS":"S","ERRORTEXT":null,"ERRORCODE":null},"body":{"sapCompanyCode":"9289493"}}';
        
        if(response!=null && String.isNotBlank(response)){
       //   if(responseFake!=null){
            
            //stubResponse= (cihStub.UpdateAccountResponse) json.deserialize(responseFake, cihStub.UpdateAccountResponse.class);
            stubResponse= (cihStub.UpdateAccountResponse) json.deserialize(response, cihStub.UpdateAccountResponse.class);
            System.debug('lclc stubResponse '+stubResponse);
            System.debug('lclc msgstatus '+stubResponse.inputHeaders.MSGSTATUS);
            if(stubResponse.inputHeaders.MSGSTATUS=='S'){
                System.debug('lclc 1');
                if(stubResponse.body.sapCompanyCode!=null){
                    System.debug('lclc 2');
                    acc.SAP_Company_Code__c=stubResponse.body.sapCompanyCode;
                    try{
                        acc.Integration_Status__c=null;
                        System.debug('lclc 3 begin ');
                        update acc;
                        System.debug('lclc 3 end '+acc);
                        handleSuccess(omq,stubResponse);
                        System.debug('lclc 4');
                    }catch(exception ex){
                        String errmsg = 'ERROR='+ex.getmessage() + ' - '+ex.getStackTraceString();
                        System.debug('error 2 '+errmsg);
                        handleError(omq, stubResponse, errmsg);
                    }
                }   
            }else{
                System.debug('lclc 5');
                /*if(stubResponse.inputHeaders.ERRORTEXT.contains('Duplicate')){
                    //duplicate error message like "Duplicated Codes:0004858049,584995940"
                    String[] codes=stubResponse.inputHeaders.ERRORTEXT.substringAfter(':').split(',');
                    if(codes.size()>0){
                        acc.SAP_Company_Code__c=codes[0];
                        try{
                            acc.Integration_Status__c=null;
                            update acc;
                            handleSuccess(omq,stubResponse);
                        }catch(exception ex){
                            System.debug('error 3 '+ex.getMessage());
                            handleError(omq,stubResponse);
                        }
                    }

                }else{
                    System.debug('lclc msgstatus f ');
                    handleError(omq,stubResponse);
                }*/
                String errmsg = 'ERROR=The MSGSTATUS is not S.';
                handleError(omq,stubResponse,errmsg);
            }
        }else{
            System.debug('lclc no response');
            handleError(omq,null,'ERROR=no response');
        }

    }

    public static void recieveUpdatedAccts(message_queue__c omq){

        Account acc=[select id, billingStreet, billingCity, billingCountry, billingState, billingPostalCode,
                        Name, SAP_Company_Code__c, Phone, Fax, Website, Email__c, Direct_Account__c,
                        Indirect_Account__c, End_Customer_Account__c, External_Top_Tier__c, Business_Partner_Tier__c,
                        Service_Partner_Tier__c, Solution_Partner_Tier__c, external_industry_type__c, External_Industry__c,Partner_top_tier__c,
                        Key_Account__c, Account_Level__c, parent_sap_company_code__c, Parent_Account_Level__c, external_business_partner_type__c,DUP_AFLAG__c,DUP_REMARK__c ,DUP_ADATE__c,DUP_AUSER__c
                        from Account where Id=:omq.Identification_Text__c limit 1];

        string partialEndpoint = Test.isRunningTest() ? 'asdf' : Integration_EndPoints__c.getInstance('Flow-004').partial_endpoint__c;
        string layoutId = Test.isRunningTest() ? 'asdf' : Integration_EndPoints__c.getInstance('Flow-004').layout_id__c;

        cihStub.UpdateAccountRequest outerStub=new cihStub.UpdateAccountRequest();
        cihStub.msgHeadersRequest headers=new cihStub.msgHeadersRequest(layoutId);
        cihStub.UpdateAccountBodyRequest body=new cihStub.UpdateAccountBodyRequest();
        mapfields(body, acc, 'update');
        outerStub.body=body;
        outerStub.inputHeaders=headers;

        string requestBody=json.serialize(outerStub);
        string response='';

        try{
            System.debug('lclc requestBody '+requestBody);
            response = webcallout(requestBody, partialEndpoint); 
      //    system.debug(' *****************response returned from the mock call out' + response);
        }catch(exception ex){
            String errmsg = 'ERROR='+ex.getmessage() + ' - '+ex.getStackTraceString();
            system.debug('error 1 '+errmsg); 
            handleError(omq, null,errmsg);
            return;
        }
        System.debug('lclc response '+response);
        cihStub.UpdateAccountResponse stubResponse=new cihStub.UpdateAccountResponse();
        String responseFake='{"inputHeaders":{"MSGGUID":"a6bba321-2183-5fb4-ff75-4d024e39a98e","IFID":"TBD","IFDate":"160317","MSGSTATUS":"S","ERRORTEXT":null,"ERRORCODE":null},"body":{"sapCompanyCode":"9289493"}}';
        if(response!=null && string.isNotBlank(response)){
        //if(responseFake!=null){
            
            //stubResponse= (cihStub.UpdateAccountResponse) json.deserialize(responseFake, cihStub.UpdateAccountResponse.class);
            stubResponse= (cihStub.UpdateAccountResponse) json.deserialize(response, cihStub.UpdateAccountResponse.class);
            System.debug('lclc stubResponse '+stubResponse);
            if(stubResponse.inputHeaders.MSGSTATUS=='S'){
                if(stubResponse.body.sapCompanyCode!=null){
                    //acc.SAP_Company_Code__c=stubResponse.body.sapCompanyCode;
                    try{
                        
                        //update acc;
                        handleSuccess(omq,stubResponse);
                    }catch(exception ex){
                        String errmsg = 'ERROR='+ex.getmessage() + ' - '+ex.getStackTraceString();
                        System.debug('error 2 '+errmsg);
                        handleError(omq,stubResponse,errmsg);
                    }
                }   
            }else{
                /*if(stubResponse.inputHeaders.ERRORTEXT.contains('Duplicate')){
                    //duplicate error message like "Duplicated Codes:0004858049,584995940"
                    String[] codes=stubResponse.inputHeaders.ERRORTEXT.substringAfter(':').split(',');
                    if(codes.size()>0){
                        //acc.SAP_Company_Code__c=codes[0];
                        try{
                            //update acc;
                            handleSuccess(omq,stubResponse);
                        }catch(exception ex){
                            System.debug('error 3 '+ex.getMessage());
                            handleError(omq,stubResponse);
                        }
                    }

                }else{
                    System.debug('lclc msgstatus f ');
                    handleError(omq,stubResponse);
                } */
                handleErrorDup(omq,stubResponse);
            }
        }else{
            System.debug('lclc no response');
            handleError(omq,null,'ERROR=no response');
        }

        
    }

    public static void mapfields(cihStub.UpdateAccountBodyRequest body, Account acc, String insertOrUpdate){

        if(insertOrUpdate=='update'){
            body.sapCompanyCode=acc.SAP_Company_Code__c;
        }
        body.systemId='SFDC_US';
        body.requestNumber=acc.Id;
        body.companyCode='C310';
        body.accountName= (acc.Name!=null) ? (acc.Name.length()<34 ? acc.Name : acc.Name.substring(0,34)) : '';
        body.billingStreet=(acc.billingStreet!=null) ? (acc.billingStreet.length()<34 ? acc.billingStreet : acc.billingStreet.substring(0,34)) : '';
        body.billingCity=(acc.billingCity!=null) ? (acc.billingCity.length()<34 ? acc.billingCity : acc.billingCity.substring(0,34)).toUpperCase() : '';
        
        String country=acc.billingCountry;
         if(country !=null){
            country=country.toLowerCase();
        }
        if(country=='can' || country=='ca' || country=='canada'){
          body.billingCountry='CA';  
        }else{
         body.billingCountry='US';   
        }
        
        body.billingState=(acc.billingState!=null) ? (acc.billingState).toUpperCase() : '';
        body.billingPostalCode=acc.billingPostalCode;
        String phonestring=string.IsNotBlank( acc.phone ) ? acc.phone.replaceAll('\\D','').right(10) : '9999999999';
        body.phone=phonestring.substring(0,3)+'-'+phonestring.substring(3,6)+'-'+phonestring.substring(6,10);
        String faxstring= string.IsNotBlank( acc.fax ) ? acc.fax.replaceAll('\\D','').right(10) : '9999999999';
        body.fax=faxstring.substring(0,3)+'-'+faxstring.substring(3,6)+'-'+faxstring.substring(6,10);
        body.website= (acc.website!=null) ? (acc.website.length()<99 ? acc.website : acc.website.substring(0,99)) : '' ;
        body.email= (acc.Email__c!=null) ? (acc.Email__c.length()<49 ? acc.Email__c : acc.Email__c.substring(0,49)) : '';
        body.directFlag='N';
        if(acc.Indirect_Account__c){
            body.indirectFlag='Y';
        }else{
            body.indirectFlag='N';
        }
        if(acc.End_Customer_Account__c){
            body.endCustomerFlag='Y';
        }else{
            body.endCustomerFlag='N';
        }
        body.topTier=(acc.Partner_top_tier__c!=null) ? acc.Partner_top_tier__c : 'Registered' ;
        body.businessPartnerTier=acc.Business_Partner_Tier__c;
        body.servicePartnerTier=acc.Service_Partner_Tier__c;
        body.solutionPartnerTier=acc.Solution_Partner_Tier__c;
        body.industryType=acc.external_industry_type__c;
        body.businessPartnerType=acc.external_business_partner_type__c;
        body.industry=acc.External_Industry__c;
        if(acc.Key_Account__c){
            body.keyAccount='Y';
        }else{
            body.keyAccount='N';
        }
        if(acc.Account_Level__c=='Level5'){
            body.level5='Y';
            body.level4='N';
            body.level3='N';
            body.level2='N';
            body.level1='N';
        }else if(acc.Account_Level__c=='Level4'){
            body.level5='N';
            body.level4='Y';
            body.level3='N';
            body.level2='N';
            body.level1='N';
        }else if(acc.Account_Level__c=='Level3'){
            body.level5='N';
            body.level4='N';
            body.level3='Y';
            body.level2='N';
            body.level1='N';
        }else if(acc.Account_Level__c=='Level2'){
            body.level5='N';
            body.level4='N';
            body.level3='N';
            body.level2='Y';
            body.level1='N';
        }else if(acc.Account_Level__c=='Level1'){
            body.level5='N';
            body.level4='N';
            body.level3='N';
            body.level2='N';
            body.level1='Y';
        }
        body.parentSAPID=acc.parent_sap_company_code__c;
        body.parentLevel=acc.Parent_Account_Level__c;
    
        if(acc.DUP_AFLAG__c != null){
            body.DUP_AFLAG=acc.DUP_AFLAG__c;
        }
        
        if(acc.DUP_REMARK__c != null){
            body.DUP_REMARK=acc.DUP_REMARK__c;
        }
        
        if(acc.DUP_ADATE__c != null){
            body.DUP_ADATE=acc.DUP_ADATE__c;
        }
        
        if(acc.DUP_AUSER__c != null){
            body.DUP_AUSER=acc.DUP_AUSER__c;
        }
        
        
        system.debug('body  ++++++'+body);
    }

    public static string webcallout(string body, string partialEndpoint){ 

        System.Httprequest req = new System.Httprequest();
      //  system.debug('************************************** my mock class should have been called');
        HttpResponse res = new HttpResponse();
        req.setMethod('POST');
        req.setBody(body);
        req.setEndpoint('callout:X012_013_ROI'+partialEndpoint);
        req.setTimeout(120000);
            
        //    String username = 'SFDC_USER';
        //    String password = 'samsung_cih_temp';
        //    Blob headerValue = Blob.valueOf(username + ':' + password);
        //    String authorizationHeader = 'BASIC ' +
        //    EncodingUtil.base64Encode(headerValue);
        //    req.setHeader('Authorization', authorizationHeader);

        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Accept', 'application/json');

        Http http = new Http();     
        res = http.send(req);
        return res.getBody();       
    }

    public static void handleError(message_queue__c mq, cihStub.UpdateAccountResponse res, String err){
        mq.retry_counter__c += 1;
        mq.status__c = mq.retry_counter__c > 5 ? 'failed' : 'failed-retry';
        if(res!=null && res.inputHeaders!=null){
            mq.MSGSTATUS__c = res.inputHeaders.MSGSTATUS;
            mq.ERRORTEXT__c=res.inputHeaders.ERRORTEXT+' '+err;
            mq.ERRORCODE__c=res.inputHeaders.ERRORCODE;
        }else{
            mq.ERRORTEXT__c=err;
        }
        upsert mq;      
        //paul states no need for chatter update here
    }

    public static void handleErrorDup(message_queue__c mq, cihStub.UpdateAccountResponse res){
        mq.retry_counter__c += 1;
        mq.status__c = 'failed';
        if(res.inputHeaders!=null){
            mq.MSGSTATUS__c = res.inputHeaders.MSGSTATUS;
            mq.ERRORTEXT__c=res.inputHeaders.ERRORTEXT;
            mq.ERRORCODE__c=res.inputHeaders.ERRORCODE;
        }
        upsert mq;      
        //paul states no need for chatter update here
    }

    public static void handleSuccess(message_queue__c mq, cihStub.UpdateAccountResponse res){
        mq.status__c = 'success';
        if(res.inputHeaders!=null){
            mq.MSGSTATUS__c = res.inputHeaders.MSGSTATUS;
            mq.MSGUID__c = res.inputHeaders.msgGUID;
            mq.IFID__c = res.inputHeaders.ifID;
            mq.IFDate__c = res.inputHeaders.ifDate;  
        }       
        upsert mq;
        //paul states no need for chatter update here
    }
    
}