/*
Created by - Vijay Kamani 04/16/2019
Division - SEA B2B IT

Purpose - To Send VDGB Rollout Information to GSBN/GSCM  HQ System 

Note: - Intial  Batch to Prepare the Opportunity IDs Information created by Year by Passing the parameter to batch  and insert  into custom setting "VD_GMB_Opportunity_List__c"

  

Conditons :
               Division - IT
               Product Group - Smart Signage and LCD Monitor and HTV 
*/




global class IntialDataVD_GMBDataCalculations implements Database.Batchable<sObject>,database.stateful,Schedulable, Database.AllowsCallouts{
    global Set<String> processedOPPs;// set to Store the Processed OPP's from Each batch
    global Integer year;
    global void execute(SchedulableContext SC) {}
   
   global IntialDataVD_GMBDataCalculations(integer year){
       processedOPPs=new set<String>();
       this.year=year;
   }
   
    global Database.QueryLocator start(Database.BatchableContext BC) {
          if(!Test.isRunningTest()){
           return Database.getQueryLocator([Select id,OpportunityID,Opportunity.Opportunity_Number__c,Opportunity.Roll_Out_Plan__c,Opportunity.Primary_Distributor__c from OpportunityLineItem where (Product2.Product_Group__c='SMART_SIGNAGE' OR Product2.Product_Group__c='LCD_MONITOR' OR  Product2.Product_Group__c='HOSPITALITY_DISPLAY') AND ( CALENDAR_YEAR(Opportunity.CreatedDate) =:year) and Alternative__c=false Order By Opportunity.Primary_Distributor__c,Opportunity.Opportunity_Number__c  ]);
          }else{// Test Running 
           return Database.getQueryLocator([Select id,OpportunityID,Opportunity.Opportunity_Number__c,Opportunity.Roll_Out_Plan__c,Opportunity.Primary_Distributor__c from OpportunityLineItem where (Product2.Product_Group__c='SMART_SIGNAGE' OR Product2.Product_Group__c='LCD_MONITOR' OR  Product2.Product_Group__c='HOSPITALITY_DISPLAY') AND ( CALENDAR_YEAR(Opportunity.CreatedDate) =:year OR (Opportunity.StageName='Drop' AND Opportunity.Roll_Out_Plan__c=null)) and Alternative__c=false Order By Opportunity.Primary_Distributor__c,Opportunity.Opportunity_Number__c limit 10 ]);

          }
              
    }
     
    global void execute(Database.BatchableContext BC, List<OpportunityLineItem> olis) {
           Map<String,string> NoRolloutoppids=new Map<String,string>();// Set to Store OPP ID , to Process All Schedules at a time under one OPP.
           Set<String> oppids=new Set<String>();
       for(OpportunityLineItem r:olis){
           if(processedOPPs.contains(r.OpportunityID)){
               // This OPP Already processed Once   
           }else{
               oppids.add(r.OpportunityID);
               if(r.Opportunity.Roll_Out_Plan__c ==null){
                   NoRolloutoppids.put(r.OpportunityID,r.Opportunity.Primary_Distributor__c);
               }
               processedOPPs.add(r.OpportunityID);
           }
           
       }
       List<VD_GMB_Opportunity_List__c> insertlist=new List<VD_GMB_Opportunity_List__c>();
       if(oppids.size()>0){
           // Process all Rollout  Data under One OPP at a time
             List<AggregateResult> arlist=new List<AggregateResult>([Select sum(Number_of_Roll_Out_Schedules__c) cnt,Roll_Out_Plan__r.Opportunity__c opp,Roll_Out_Plan__r.Opportunity__r.Primary_Distributor__c pd from Roll_Out_Product__c where (Product__r.Product_Group__c='SMART_SIGNAGE' OR Product__r.Product_Group__c='LCD_MONITOR' OR Product__r.Product_Group__c='HOSPITALITY_DISPLAY') and Roll_Out_Plan__r.Opportunity__c in:oppids Group By Roll_Out_Plan__r.Opportunity__r.Primary_Distributor__c,Roll_Out_Plan__r.Opportunity__c]);
             for(AggregateResult a:arlist){
                 VD_GMB_Opportunity_List__c rec=new VD_GMB_Opportunity_List__c();
                 rec.Name=String.valueOf(a.get('opp'));
                 rec.No_Of_Schedules__c=Integer.valueOf(a.get('cnt'));
                 rec.Primary_Distributor__c=String.valueOf(a.get('pd'));
                 insertlist.add(rec);
             }
       }
       
       if(NoRolloutoppids.size()>0){
           for(String s:NoRolloutoppids.keyset()){
                VD_GMB_Opportunity_List__c rec=new VD_GMB_Opportunity_List__c();
                 rec.Name=s;
                 rec.No_Of_Schedules__c=0;
                 rec.Primary_Distributor__c=NoRolloutoppids.get(s);
                 insertlist.add(rec);
           }
       }
       
        if(!insertlist.isEmpty()){
            insert insertlist;
        }
        
    }
     global void finish(Database.BatchableContext BC) {
        // execute any post-processing operations
        Set<String> processedOpps=new set<String>();
         List<VD_GMB_Opportunity_List__c> insertlist=new List<VD_GMB_Opportunity_List__c>();
         List<VD_GMB_Opportunity_List__c> allrecords = VD_GMB_Opportunity_List__c.getall().values();
         for(VD_GMB_Opportunity_List__c r:allrecords){
             processedOpps.add(r.Name);
         }
         
         if(!Test.isRunningTest()){  
                        List<AggregateResult> arlist=new List<AggregateResult>([Select sum(Number_of_Roll_Out_Schedules__c) cnt,Roll_Out_Plan__r.Opportunity__c opp,Roll_Out_Plan__r.Opportunity__r.Primary_Distributor__c pd from Roll_Out_Product__c where (CALENDAR_YEAR(Roll_Out_Plan__r.Opportunity__r.CreatedDate)=:year ) AND (Product__r.Product_Group__c='SMART_SIGNAGE' OR Product__r.Product_Group__c='LCD_MONITOR' OR Product__r.Product_Group__c='HOSPITALITY_DISPLAY') and Roll_Out_Plan__r.Opportunity__c  not in:processedOpps Group By Roll_Out_Plan__r.Opportunity__r.Primary_Distributor__c,Roll_Out_Plan__r.Opportunity__c]);
                         for(AggregateResult a:arlist){
                                 VD_GMB_Opportunity_List__c rec=new VD_GMB_Opportunity_List__c();
                                 rec.Name=String.valueOf(a.get('opp'));
                                 rec.No_Of_Schedules__c=Integer.valueOf(a.get('cnt'));
                                 rec.Primary_Distributor__c=String.valueOf(a.get('pd'));
                                 insertlist.add(rec);
                             }
                   
         }else{// Test Running
                 List<AggregateResult> arlist=new List<AggregateResult>([Select sum(Number_of_Roll_Out_Schedules__c) cnt,Roll_Out_Plan__r.Opportunity__c opp,Roll_Out_Plan__r.Opportunity__r.Primary_Distributor__c pd from Roll_Out_Product__c where (CALENDAR_YEAR(Roll_Out_Plan__r.Opportunity__r.CreatedDate)=:year ) AND (Product__r.Product_Group__c='SMART_SIGNAGE' OR Product__r.Product_Group__c='LCD_MONITOR' OR Product__r.Product_Group__c='HOSPITALITY_DISPLAY') and Roll_Out_Plan__r.Opportunity__c  not in:processedOpps Group By Roll_Out_Plan__r.Opportunity__r.Primary_Distributor__c,Roll_Out_Plan__r.Opportunity__c limit 10]);
                         for(AggregateResult a:arlist){
                                 VD_GMB_Opportunity_List__c rec=new VD_GMB_Opportunity_List__c();
                                 rec.Name=String.valueOf(a.get('opp'));
                                 rec.No_Of_Schedules__c=Integer.valueOf(a.get('cnt'));
                                 rec.Primary_Distributor__c=String.valueOf(a.get('pd'));
                                 insertlist.add(rec);
                             }
                            
         }  
         
         
                       if(!insertlist.isEmpty()){
                            insert insertlist;
                        }
         
  }
  
}