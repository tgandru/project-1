Global class KNOXApprovalCancelRequest {
    @AuraEnabled
     webservice  Static String  cancelrequest(String soid){
                Sellout__c so=[select RecordTypeId,id,name,Approval_Status__c,Closing_Month_Year1__c,Comments__c,KNOX_Approval_ID__c,Description__c,Has_Attachement__c,OwnerId ,Integration_Status__c ,Total_CL_Qty__c,Total_IL_Qty__c,Subsidiary__c from Sellout__c where id=:soid limit 1];
           
           String retnst=sendcancelrequest(so);
            String rtn='';
            
            
            CancelProcessresponse outputObj = new CancelProcessresponse();
            outputObj = (CancelProcessresponse)JSON.deserialize(retnst, CancelProcessresponse.class);
            string  status = outputObj.result;
            if(status=='OK'){
             rtn='Sucessfully Canceled the Request';    
             so.Approval_Status__c='Draft';
             Update so;
             Boolean result = System.Approval.unlock(so.id).isSuccess();
             message_queue__c mq=new message_queue__c();
             mq.Object_Name__c='Sellout';
             mq.Integration_Flow_Type__c='KNOX_Request_Cancel';
             mq.Status__c='Sucess';
             mq.Response_Body__c=String.valueOf(retnst);
             mq.Identification_Text__c=so.id;
             insert mq;
             //string statusupd=KNOXApprovalStatus.getApprovalStatus(so.id);
             //rtn=rtn+' . '+statusupd;
            }else{
              rtn='Something went wrong.Please Contact System Admin. Error::'+outputObj.errorMessage;  
              message_queue__c mq=new message_queue__c();
             mq.Object_Name__c='Sellout';
             mq.Integration_Flow_Type__c='KNOX_Status_Update';
             mq.Status__c='failed';
             mq.Response_Body__c=String.valueOf(retnst);
             mq.Identification_Text__c=so.id;
             insert mq;
            }
       return rtn; 
    }
    
     public class CancelProcessWSVO {
        public String apOpinCn {get;set;} // Submitter or Approval Cancel Comments
        public String infSysId {get;set;} //System ID
        public String apInfId {get;set;} // Unique ID    
    }   
    
    
     public class CancelProcessresponse {
        public String result {get;set;} 
        public String message {get; set;}
         public String errorMessage {get; set;}
        
    }   
     
    
    
    public Static string sendcancelrequest(Sellout__c so){
                  String resBody = '';
         List<KNOX_Credentials__c> credntls = KNOX_Credentials__c.getall().values();         
         String endpoint='';
         String tkn='';
         String companycode='';
        Boolean isQA=[SELECT Id, IsSandbox  FROM Organization limit 1].IsSandbox;
        if(isQA==true){
            endpoint=KNOX_Credentials__c.getInstance('Sandbox').Base_URL__c;
            tkn=KNOX_Credentials__c.getInstance('Sandbox').Token__c;
            companycode=KNOX_Credentials__c.getInstance('Sandbox').Company_ID__c;
        }else{
            endpoint=KNOX_Credentials__c.getInstance('Production').Base_URL__c;
            tkn=KNOX_Credentials__c.getInstance('Production').Token__c;
            companycode=KNOX_Credentials__c.getInstance('Production').Company_ID__c;
        }
       
        CancelProcessWSVO bd=new CancelProcessWSVO();
        bd.apOpinCn=so.Comments__c;
        bd.apInfId=so.KNOX_Approval_ID__c;
        bd.infSysId=companycode;
        
         endpoint=endpoint+'/knoxrest/approval/sub/api/basic/v1.0/cancel';
        String jsonbody=JSON.serialize(bd);
        System.debug('Request::'+jsonbody);
          Httprequest httpReq = new Httprequest();
                        HttpResponse httpRes = new HttpResponse();
                        Http http = new Http();
                        httpReq.setMethod('POST');
                        httpReq.setBody(jsonbody);
                        httpReq.setTimeout(120000);
                       // httpReq.setEndpoint('callout:X012_013_ROI'+'/knoxrest/approval/sub/api/basic/v1.0/cancel');
                        httpReq.setEndpoint(endpoint);
                        httpReq.setHeader('Authorization', 'Bearer '+tkn); 
                        httpReq.setHeader('Content-Type', 'application/json');
                        httpReq.setHeader('Accept', 'application/json');
                        
                         if(!Test.isRunningTest()){
                          httpRes = http.send(httpReq);
                        }else{
                           httpRes = Testrespond(httpReq);
                        }
                        
                       
                       System.debug('Response ::'+httpRes);
                       System.debug('Response Body ::'+httpRes.getBody());
                     resBody=httpRes.getBody();
                        
        
        return resBody;
    }
    
    
       public Static HTTPResponse Testrespond(HTTPRequest req) {          
               String outputBodyString = '{"result":"OK","errorCode":null,"message":"getStatusByMisId success","data":{"txtTypeCd":"1","sbmDt":"20181220161542","chsetCd":"en_US.UTF-8","apInfId":null,"infSysId":"C10REST0216","ntfTypeCd":"0","urgYn":"0","apDocSecuTypeCd":"0","apStatsCd":"2","timeZone":"GMT","apTtl":"Sell-Out Actual approval request for 12/2018","wSO2AapTaplnVOs":[{"actPrssDt":"20181220161542","docOpenDt":null,"docArvDt":"20181220161542","apOpinCn":"Test For Populating KNOX EMPID from Custom Setting","apRelrCompCd":"C6A","apRelrCompNm":"Samsung SDS(America)","apRelrDeptCd":"C6AOU000","apRelrDeptNm":"Shared Service Center","apRelrEpid":"M181214205717C6A2844","apRelrMailAddr":"v.kamani@stage.partner.samsung.com","apRelrFnm":"Kamani Saradhi Vijay","apRelrJobpoCd":"D10","apRelrJobpoNm":"대리","apRelrSuborgCd":null,"apRelrSuborgNm":null,"apRlCd":"0","apSeq":"0","aplnStatsCd":"0","arbPmtYn":"N","dlgAprEpid":null,"docDelCd":null,"pathMdfyPmtYn":"N","prxAprYn":"N","txtMdfyPmtYn":"N","keepTermVal":null,"rsrvSbmDt":"","txtMdfyActYn":"N","pathMdfyActYn":"N","arbActYn":"N","strDocArvDt":"20181220161542","strActPrssDt":"20181220161542","strDocOpenDt":null},{"actPrssDt":"20181220161723","docOpenDt":null,"docArvDt":"20181220161542","apOpinCn":"","apRelrCompCd":"C6A","apRelrCompNm":"Samsung SDS(America)","apRelrDeptCd":"C6AOU000","apRelrDeptNm":"Shared Service Center","apRelrEpid":"M181214205310C6A441","apRelrMailAddr":"stevepark@stage.samsung.com","apRelrFnm":"박상희","apRelrJobpoCd":"C10","apRelrJobpoNm":"차장","apRelrSuborgCd":null,"apRelrSuborgNm":null,"apRlCd":"1","apSeq":"1","aplnStatsCd":"1","arbPmtYn":"N","dlgAprEpid":null,"docDelCd":null,"pathMdfyPmtYn":"N","prxAprYn":"N","txtMdfyPmtYn":"N","keepTermVal":null,"rsrvSbmDt":"","txtMdfyActYn":"N","pathMdfyActYn":"N","arbActYn":"N","strDocArvDt":"20181220161542","strActPrssDt":"20181220161723","strDocOpenDt":null},{"actPrssDt":"20181220161855","docOpenDt":null,"docArvDt":"20181220161723","apOpinCn":"","apRelrCompCd":"C6A","apRelrCompNm":"Samsung SDS(America)","apRelrDeptCd":"C6AOU000","apRelrDeptNm":"Shared Service Center","apRelrEpid":"M181214210521C6A5675","apRelrMailAddr":"t.gandru@stage.partner.samsung.com","apRelrFnm":"Thirupathirao Gandru","apRelrJobpoCd":"K10","apRelrJobpoNm":"과장","apRelrSuborgCd":null,"apRelrSuborgNm":null,"apRlCd":"2","apSeq":"2","aplnStatsCd":"1","arbPmtYn":"N","dlgAprEpid":null,"docDelCd":null,"pathMdfyPmtYn":"N","prxAprYn":"N","txtMdfyPmtYn":"N","keepTermVal":null,"rsrvSbmDt":"","txtMdfyActYn":"N","pathMdfyActYn":"N","arbActYn":"N","strDocArvDt":"20181220161723","strActPrssDt":"20181220161855","strDocOpenDt":null},{"actPrssDt":"20181220162003","docOpenDt":null,"docArvDt":"20181220161855","apOpinCn":"","apRelrCompCd":"C6A","apRelrCompNm":"Samsung SDS(America)","apRelrDeptCd":"C6AOU000","apRelrDeptNm":"Shared Service Center","apRelrEpid":"M181214210030C6A429","apRelrMailAddr":"p.kabalai@stage.samsung.com","apRelrFnm":"Pradeep Kabalai","apRelrJobpoCd":"D10","apRelrJobpoNm":"대리","apRelrSuborgCd":null,"apRelrSuborgNm":null,"apRlCd":"1","apSeq":"3","aplnStatsCd":"1","arbPmtYn":"N","dlgAprEpid":null,"docDelCd":null,"pathMdfyPmtYn":"N","prxAprYn":"N","txtMdfyPmtYn":"N","keepTermVal":null,"rsrvSbmDt":"","txtMdfyActYn":"N","pathMdfyActYn":"N","arbActYn":"N","strDocArvDt":"20181220161855","strActPrssDt":"20181220162003","strDocOpenDt":null},{"actPrssDt":"20181220162003","docOpenDt":null,"docArvDt":"20181220162003","apOpinCn":"","apRelrCompCd":"C6A","apRelrCompNm":"Samsung SDS(America)","apRelrDeptCd":"C6AOU000","apRelrDeptNm":"Shared Service Center","apRelrEpid":"M181214205310C6A441","apRelrMailAddr":"stevepark@stage.samsung.com","apRelrFnm":"박상희","apRelrJobpoCd":"C10","apRelrJobpoNm":"차장","apRelrSuborgCd":null,"apRelrSuborgNm":null,"apRlCd":"9","apSeq":"4","aplnStatsCd":"0","arbPmtYn":"N","dlgAprEpid":null,"docDelCd":null,"pathMdfyPmtYn":"N","prxAprYn":"N","txtMdfyPmtYn":"N","keepTermVal":null,"rsrvSbmDt":"","txtMdfyActYn":"N","pathMdfyActYn":"N","arbActYn":"N","strDocArvDt":"20181220162003","strActPrssDt":"20181220162003","strDocOpenDt":null},{"actPrssDt":"20181220162003","docOpenDt":null,"docArvDt":"20181220162003","apOpinCn":"","apRelrCompCd":"C6A","apRelrCompNm":"Samsung SDS(America)","apRelrDeptCd":"C6AOU000","apRelrDeptNm":"Shared Service Center","apRelrEpid":"M181214205717C6A2844","apRelrMailAddr":"v.kamani@stage.partner.samsung.com","apRelrFnm":"Kamani Saradhi Vijay","apRelrJobpoCd":"D10","apRelrJobpoNm":"대리","apRelrSuborgCd":null,"apRelrSuborgNm":null,"apRlCd":"9","apSeq":"5","aplnStatsCd":"0","arbPmtYn":"N","dlgAprEpid":null,"docDelCd":null,"pathMdfyPmtYn":"N","prxAprYn":"N","txtMdfyPmtYn":"N","keepTermVal":null,"rsrvSbmDt":"","txtMdfyActYn":"N","pathMdfyActYn":"N","arbActYn":"N","strDocArvDt":"20181220162003","strActPrssDt":"20181220162003","strDocOpenDt":null}]},"faultActor":null,"faultCode":null,"faultString":null}';
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatus('2');
            res.setStatusCode(200);
            res.setBody(outputBodyString);
            
            return res;                
      }

}