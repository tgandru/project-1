public class KNOXApprovalRequestCreate {
   @future(callout=true)
     public static void callknoxportaltocreateRequest (String soid,Set<id> aprvr){
        Sellout__c so=[select RecordTypeId,id,name,Approval_Status__c,Closing_Month_Year1__c,Comments__c,KNOX_Approval_ID__c,Description__c,Has_Attachement__c,OwnerId ,Integration_Status__c ,Total_CL_Qty__c,Total_IL_Qty__c,Subsidiary__c from Sellout__c where id=:soid limit 1];
         //List<Sellout_Approval_Order__c> aprvr=new list<Sellout_Approval_Order__c>([select id,name,Status__c,KNOX_EPID__c,Approval_Type__c,Sequence__c,Approval_Step_User_ID__c,KNOX_E_mail__c from Sellout_Approval_Order__c where Approval_Type__c !='99'  order by Sequence__c asc ]);
         List<Sellout_Approval__c> aprvrlst=new List<Sellout_Approval__c>([select id,Step_No__c,KNOX_Approval_ID__c,KNOX_EPID__c,Approval_Type__c,Status__c,KNOX_E_mail__c,Date__c,Comments__c from Sellout_Approval__c where id in:aprvr order by Step_No__c asc]);
         String EMPID;
          String apInfId= 'sf'+so.id+system.now().format('yyyyMMddHHmm');
         Requestclass req=new Requestclass();
         msgHeadersRequest inphd=new msgHeadersRequest();
         RequestBody bd=new RequestBody();
         bd.txtClobcn=giverequestbody(so);
         bd.sbmDt=Datetime.now().format('yyyyMMddHHmmss');
         bd.apTtl='Sell-Out Actual approval request for '+so.Closing_Month_Year1__c;
         bd.apInfId=apInfId;
         string companycode='';
          Boolean isQA=[SELECT Id, IsSandbox  FROM Organization limit 1].IsSandbox;
         if(isQA==true){
             companycode=KNOX_Credentials__c.getInstance('Sandbox').Company_ID__c;
         }else{
             companycode=KNOX_Credentials__c.getInstance('Production').Company_ID__c;
         }
          bd.infSysId=companycode;
         approvers ap=new approvers();
        // req.inputHeaders=inphd;
         
         list<approvers> aapTaplnVOLists=new list<approvers>();
         for(Sellout_Approval__c ord :aprvrlst){
               ord.KNOX_Approval_ID__c=apInfId;
             approvers apr=new approvers();
             if(ord.Step_No__c ==0){
                apr.apOpinCn = ord.Comments__c; 
                 EMPID=ord.KNOX_EPID__c;
             }
             // apr.apRelrEpid=ord.KNOX_EPID__c;
             apr.apRelrMailAddr= ord.KNOX_E_mail__c;
             apr.apSeq=String.valueOf(ord.Step_No__c);
             apr.apRlCd=ord.Approval_Type__c;
             aapTaplnVOLists.add(apr);
         }
         bd.aapTaplnVOList=aapTaplnVOLists;
         
         //req.body=bd;
         String jsonbody= JSON.serialize(bd);
        
        String resp =SendRequest(jsonbody,so,EMPID); 
         Output outputObj = new Output();
         outputObj = (Output)JSON.deserialize(resp, Output.class);
         outputObj.apInfId = apInfId; 
         outputObj.message = resp;
         Boolean cont = resp.contains('Approval Item Created');
         if(cont)outputObj.bCode = true; else outputObj.bCode = false;
         
         String retn='';
         if(cont){
             so.KNOX_Approval_ID__c=apInfId;
             so.Approval_Status__c='Pending Approval';
             update so;
             Boolean result = System.Approval.lock(so.id).isSuccess();
              
             
             update aprvrlst;
             List<message_queue__c> mqlist=new list<message_queue__c>();
              message_queue__c mq=new message_queue__c();
             mq.Object_Name__c='Sellout';
             mq.Integration_Flow_Type__c='Knox Approval Request';
             mq.Status__c='sucess';
             //mq.MSGUID__c=res.inputHeaders.MSGGUID;
             mq.Response_Body__c=String.valueOf(resp);
             mq.Request_Body__c=jsonbody;
              mq.Identification_Text__c=so.id;
             //insert mq;
             mqlist.add(mq);
               message_queue__c mq1=new message_queue__c();
             mq1.Object_Name__c='Sellout';
             mq1.Integration_Flow_Type__c='KNOX_Status_Update';
             mq1.Status__c='not started';
              mq1.retry_counter__c =0;
              mq1.Identification_Text__c=so.id;
             mqlist.add(mq1);
             insert mqlist;
             retn='Sucessfully Submitted to Approval.';
         }else{
             retn=string.valueOf(resp);
             message_queue__c mq=new message_queue__c();
             mq.Object_Name__c='Sellout';
             mq.Integration_Flow_Type__c='Knox Approval Request';
             mq.Status__c='failed';
             mq.Response_Body__c=String.valueOf(resp);
             mq.Request_Body__c=jsonbody;
              mq.Identification_Text__c=so.id;
             insert mq;
         }
         
         //return retn;
     }
     
     public class Requestclass {
      //  public msgHeadersRequest inputHeaders {get;set;}        
        public RequestBody body {get;set;}
    }
    
      public class msgHeadersRequest{
        public string MSGGUID {get{string s = giveGUID(); return s;}set;} // GUID for message
        public string IFID {get{return 'TBD';}set;} // interface id, created automatically, so need logic
        public string IFDate {get{return datetime.now().formatGMT('YYYYMMddHHmmss');}set;} // interface date
        public msgHeadersRequest(){
        }
      } 
      public class RequestBody{
          public list<approvers> aapTaplnVOList {get;set;} // Approval Steps
           public String txtClobcn {get;set;} // Approval Body MaX 500KB
           public String txtTypeCd {get{return '1';} set;} // Body Type 1 -HTML,0-TEXT
           public String apDocSecuTypeCd {get{return '000';} set;} //Security Type General -000 , Not Allowd Outside-001 , Top Secret -002
           public String ntfTypeCd {get{return '0';} set;} //Notification Option  Genetral -0, All Notification -1
           public String urgYn {get{return 'N';} set;} // Urgency Normal -N, Urgency -Y
           public String sbmDt {get;set;} // Submit Date YYYYMMDDHHmmss
           public String apTtl {get;set;} //Approval Title MaX - 60 char
           public String chsetCd {get{return 'en_US.UTF-8';}set;} // Locale and Encoding
           public String infSysId {get;set;} //System ID {return 'C10REST0216';}
           public String apInfId {get;set;} // Unique ID 
           public String docMngSaveCd {get{return '0';} set;} //Document Archiving Mangement Code
           public String timeZoneId {get{string s = givetimeZone(); return s;}set;}//Time zone
          
        }
       
        
        public class approvers{
             public String apOpinCn {get;set;} // Submitter or Approval Comments
             //public String apRelrEpid {get;set;} // Approver EPID 
             public String apRelrMailAddr {get;set;} // Approver KNOX Email
             public String apSeq {get;set;} // Approver Sequence, Submitter will 0
             public String apRlCd {get;set;}// Approval Type 0-Submitter , Approver-1, Consent -2, Post Approval-4,Notification -9
             public String aplnStatsCd {get {return '0';}set;} //Approval Result 
             public String arbPmtYn {get{return 'N';} set;} //Authorty for Arbitrary Decision 
             public String txtMdfyPmtYn {get{return 'N';} set;} //Body Mofication Permission 
             public String prxAprYn {get{return 'N';} set;} //Able to Deligate  
             public String pathMdfyPmtYn {get{return 'N';} set;} // Able to Modify the Path
             public String docDelCd {get{return '0';}set;} //Document Delete Permission
        }
    public class ResultStr{
        public String result {get;set;}
        public String errorCode {get;set;}
        public String message {get;set;}
    }
    public class OutputBody {
          public List<ResultStr> resultStr;
        public OutputBody() {
            resultStr = new List<ResultStr>();
        }
    }
    public class Output{
        public OutputBody body;
        public String result;
        public String errorCode;
        public String message;
        public String apInfId;
        public Boolean bCode;
        public Output(){
            body = new OutputBody();
        }
    }
    
      public class Responseclass {
        public msgHeadersResponse inputHeaders {get;set;}        
        public Output body {get;set;}
    }
     public class msgHeadersResponse {
        public string MSGGUID {get;set;} // GUID for message
        public string IFID {get;set;} // interface id, created automatically, so need logic
        public string IFDate {get;set;} // interface date
        public string MSGSTATUS {get;set;} // start, success, failure (ST , S, F)
        public string ERRORCODE {get;set;} // error code
        public string ERRORTEXT {get;set;} // error message
    } 
    
     public static string giveGUID(){
        Blob b = Crypto.GenerateAESKey(128);
        String h = EncodingUtil.ConvertTohex(b);
        String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
        return guid;
    }
    
    
     public static string givetimeZone(){
         Datetime dt=System.now();
         Integer offset = UserInfo.getTimezone().getOffset(dt);
         Integer hour=(offset/1000)/3600;
         String tmzn='GMT'+String.valueOf(hour);
         return tmzn;
     }
     
      Public Static string giverequestbody(Sellout__c so){
         String style='<style>#tab {  border-collapse: collapse; width: 100%;} #tab td, #tab th {border: 1px solid #ddd; padding: 8px;}#tab th {  padding-top: 12px;  padding-bottom: 12px;  text-align: center;  background-color: #2157af; color: white;} #tabttl {  padding-top: 12px;  padding-bottom: 12px;  text-align: left;  background-color: #b3e6b3;  color: black;  font-weight: bold;}</style>';
         string bdy = style+'<br/>' +'Dear All '+',';
          bdy = bdy + ' <br/>';
          bdy = bdy + ' <br/>'+'Sell-Out information for  '+so.Closing_Month_Year1__c+' '+' Awaiting your review. ';
          
          bdy = bdy + '<br/> <br/>'+'<b> Subsidiary :</b>'+ so.Subsidiary__c;
          bdy = bdy + '<br/>'+'<b> Closing Month Year :</b>'+ so.Closing_Month_Year1__c;
          bdy = bdy + ' <br/>'+'<b> Description :</b>'+ so.Description__c;
          
            bdy = bdy + '<br/>';
            bdy = bdy + ' <br/>'+'<b> Sell-Out Summary : </b>';
             List<String> args = new String[]{'0','number','###,###,##0.00'};
           AggregateResult[] ar=[select sum(Quantity__c) qty,sum(Amount__c) amt,Carrier__c cr,IL_CL__c ilcl from Sellout_Products__c where Sellout__c=:so.id and Status__C='OK' group by Carrier__c,IL_CL__c  ];
           bdy +=  '<table id=\"tab\"><tr ><th > Carrier </th><th > CL Quantity </th><th > IL Quantity </th><th> Total Quantity </th></tr>'; 
             Map<String,String> codemap=new map<string,string>();
              List<Carrier_Sold_to_Mapping__c> crr=Carrier_Sold_to_Mapping__c.getall().values();
              for(Carrier_Sold_to_Mapping__c c:crr){
                codemap.put(c.Carrier_Code__c,c.Name);  
              }
              decimal cltotal=0;
              decimal iltotal=0;
              decimal grndtotal=0;
               Map<string,list<AggregateResult>> aggregateMap=new Map<string,list<AggregateResult>>(); 
                      for(AggregateResult a:ar){
                          if(aggregateMap.containsKey((string)a.get('cr'))){
                             List<AggregateResult> arl = aggregateMap.get((string)a.get('cr'));
                                            arl.add(a);
                                           aggregateMap.put((string)a.get('cr'), arl);  
                          }else{
                              aggregateMap.put((string)a.get('cr'), new List<AggregateResult> { a });  
                          }
                      }
                      for(string s:aggregateMap.keyset()){
                          decimal clq=0;
                          decimal ilq=0;
                          decimal qt=0;
                            for(AggregateResult a:aggregateMap.get(s)){
                               if((string)a.get('ilcl')=='IL'){
                                   ilq=ilq+(decimal)a.get('qty');
                                   iltotal=iltotal+integer.valueOf(ilq);
                                 //  ilamt=ilamt+(decimal)a.get('amt');
                               }else if((string)a.get('ilcl')=='CL'){
                                  clq=clq+(decimal)a.get('qty');
                                  cltotal=cltotal+integer.valueOf(clq);
                               }
                               qt=qt+(decimal)a.get('qty');
                               
                           }
                           String crName=codemap.get(s);
                          
                          String sclq=String.format(clq.format(), args);
                           String silq=String.format(ilq.format(), args);
                           String sqt=String.format(qt.format(), args);
                          bdy += '<tr ><td>'+crName+'</td><td style="text-align:right">'+sclq+'</td><td style="text-align:right">' + silq + '</td><td style="text-align:right">' + sqt + '</td></tr>';    
                      }
                      grndtotal=cltotal+iltotal;
                           String scltotal=String.format(cltotal.format(), args);
                           String siltotal=String.format(iltotal.format(), args);
                           String sgrndtotal=String.format(grndtotal.format(), args);
                    bdy += '<tr id="tabttl"><b><td>'+'Total'+'</td><td style="text-align:right">'+scltotal+'</td><td style="text-align:right">' + siltotal + '</td><td style="text-align:right">' + sgrndtotal + '</td></b></tr>';
                    bdy += '</table><br>';
           String recordlnk = System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+so.id;
          bdy = bdy + '<br/><br/><br/>'+'To see the details, please refer to the attached summary report.';
          bdy = bdy + '<br/>'+'상세 내역은 첨부 한글 보고서를 참조 부탁드립니다.';
         
           //bdy = bdy + ' <br/>'+recordlnk;
            bdy = bdy + '<br/>';
            bdy = bdy + '<br/>';
            
         bdy = bdy +  '<br/><br/><br/><b>Thank you!</b><br/>';
         bdy = bdy + '<b>Mobile B2B Team.</b>';
           
         return bdy;
     }
     
     
     
     Public Static string SendRequest(String req,Sellout__C so,String epid ){
         List<Attachment> attchList = getAttach(so.id);
          String resBody = '';
         String boundary = '----------------------------741e90d31eff';
                String header = '';
                header += '--'+boundary+'\r\n';
               header += 'Content-Disposition: form-data;  name="apItemVO"\r\n\r\n' +  req  + '\r\n';//'+'\r\n'+' Content-Type: application/json;
                String footer ='\r\n'+ '--'+boundary+'--';
        
                String headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
                while(headerEncoded.endsWith('=')) {
                    header += ' ';
                    headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
                }
    
                String attachmentsStr = '';
                 String lastPrepend = '';
                  Integer blobSize = 0;
              // String endpoint=[SELECT DeveloperName, Endpoint FROM NamedCredential where DeveloperName='X012_013_ROI' limit 1].Endpoint;
              String endpoint='';
         String tkn='';
         String companycode='';
        Boolean isQA=[SELECT Id, IsSandbox  FROM Organization limit 1].IsSandbox;
        if(isQA==true){
            endpoint=KNOX_Credentials__c.getInstance('Sandbox').Base_URL__c;
            tkn=KNOX_Credentials__c.getInstance('Sandbox').Token__c;
            companycode=KNOX_Credentials__c.getInstance('Sandbox').Company_ID__c;
        }else{
            endpoint=KNOX_Credentials__c.getInstance('Production').Base_URL__c;
            tkn=KNOX_Credentials__c.getInstance('Production').Token__c;
            companycode=KNOX_Credentials__c.getInstance('Production').Company_ID__c;
        }
                  if(attchList.size()>0){
                    for (Attachment d : attchList) {
                        Blob file = d.body;
                        blobSize += file.size();
                    }
                    
                                       if(attchList.size()>0 && blobSize < 3670016){
                               for (Attachment d : attchList) {
                                        Blob fileBlob = d.body;
                                        String filename = d.Name;
                                        String fHeader = lastPrepend + '--'+boundary+'\r\n';
                                        fHeader += 'Content-Disposition: form-data; name="fileInfo"; filename="'+filename+'"\r\nContent-Type: application/octet-stream';
                                        String fHeaderEncoded = EncodingUtil.base64Encode(Blob.valueOf(fheader+'\r\n\r\n'));
                                        while(fHeaderEncoded.endsWith('=')) {
                                            fHeader += ' ';
                                            fHeaderEncoded = EncodingUtil.base64Encode(Blob.valueOf(fHeader+'\r\n\r\n'));
                                        }
                                        String fbodyEncoded = EncodingUtil.base64Encode(fileBlob);
                                        system.debug('++++++++++++++++++++++++++++ '+fbodyEncoded);
                                        String last4Bytes = fbodyEncoded.substring(fbodyEncoded.length()-4,fbodyEncoded.length());
                                        if(last4Bytes.endsWith('==')) {
                                            last4Bytes = last4Bytes.substring(0,2) + '0K';
                                            fbodyEncoded = fbodyEncoded.substring(0,fbodyEncoded.length()-4) + last4Bytes;
                                            lastPrepend = '';
                                        } else if(last4Bytes.endsWith('=')) {
                                            last4Bytes = last4Bytes.substring(0,3) + 'N';
                                            fbodyEncoded = fbodyEncoded.substring(0,fbodyEncoded.length()-4) + last4Bytes;
                                            lastPrepend = '\n';
                                        } else {
                                            lastPrepend = '\r\n';
                                        }
                                        attachmentsStr += fHeaderEncoded + fbodyEncoded;
                               }  
                           }else{
                               footer = '\r\n' + footer;
                           }
                           
                      System.debug('headerEncoded::'+headerEncoded);
                      System.debug('attachmentsStr::'+attachmentsStr);
                      
                           
                           String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
                      System.debug('footerEncoded::'+footerEncoded);
                        String bdyblb=headerEncoded+attachmentsStr+footerEncoded;
                      System.debug('bodyBlob::'+bdyblb);
                                Blob bodyBlob = EncodingUtil.base64Decode(bdyblb);
                      
                              endpoint=endpoint+'/knoxrest/approval/sub/api/basic/v1.0/submit/withAttach?userId='+epid;
                                HttpRequest request = new HttpRequest();
                                request.setHeader('Content-Type','multipart/form-data; boundary='+boundary);
                                request.setHeader('Authorization', 'Bearer '+tkn);
                                request.setMethod('POST');
                                request.setEndpoint(endpoint); 
                                request.setBodyAsBlob(bodyBlob);
                                
                                request.setTimeout(120000);
                                 HttpResponse httpRes = new HttpResponse();
                                Http http = new Http();
                                 
                                    if(!Test.isRunningTest()){
                                        httpRes = http.send(request);
                                    }else{
                                       httpRes = Testrespond(request);
                                    }
                            
                                 System.debug('Response ::'+httpRes);
                       System.debug('Response Body ::'+httpRes.getBody());
                    resBody=httpRes.getBody();
                                    
                                    
                }else{
                     System.debug('Request ::'+req);
                    Httprequest httpReq = new Httprequest();
                        HttpResponse httpRes = new HttpResponse();
                        Http http = new Http();
                       
                        
                        httpReq.setMethod('POST');
                        httpReq.setBody(req);
                        httpReq.setTimeout(120000);
                         endpoint=endpoint+'/knoxrest/approval/sub/api/basic/v1.0/submit/general';
                      //  httpReq.setEndpoint('callout:X012_013_ROI'+'/knoxrest/approval/sub/api/basic/v1.0/submit/general');
                        httpReq.setEndpoint(endpoint);
                        httpReq.setHeader('Authorization', 'Bearer '+tkn); 
                        httpReq.setHeader('Content-Type', 'application/json');
                        httpReq.setHeader('Accept', 'application/json');
                       // httpReq.setHeader('cid', 'C10REST0216');
                      // httpRes = http.send(httpReq);
                         if(!Test.isRunningTest()){
                            httpRes = http.send(httpReq);
                        }else{
                           httpRes = Testrespond(httpReq);
                        }
                        
                       System.debug('Response ::'+httpRes);
                       System.debug('Response Body ::'+httpRes.getBody());
                     resBody=httpRes.getBody();
                    
               
                }
                 
         
         return resBody;
     }
     
      public Static List<Attachment> getAttach(String soid){
        
        
        List<Attachment> cv = [SELECT Id, Body, BodyLength, name FROM Attachment where parentid=:soid and Description='KNOX Approval' order by createddate desc limit 1 ];
        return cv;
     }
     
     
        public Static HTTPResponse Testrespond(HTTPRequest req) {          
            String outputBodyString = '{"result":"OK","errorCode":null,"message":"Approval Item Created","data":null,"category":null,"apEnvHelperVO":null,"pageNavigation":null,"listCount":null,"zoomSize":null,"attachPosition":null,"unRead":null,"writeAttachPosition":null,"skinId":null,"userId":null,"webEditor":null,"displayedUserInfo":null,"displayedOrgInfo":null,"intTimeZoneTime":0,"timeZoneTime":0,"locale":null,"timeZone":null,"timeZoneID":null,"serviceLevel":null,"attachSize":null,"theme":null,"textFont":null,"textSize":null,"screenSize":null,"newWinYn":null,"iCode":null,"mCode":null,"imCode":null,"compId":null,"attachDrag":null,"domainSet":null,"loginId":null,"attachUseAutoUpload":false,"listSize":0,"totalCount":0,"globalTransationId":null,"globalPosition":null,"serverPosition":null,"deptId":null,"secId":null,"choiceBox":null,"maxChatMember":null,"maxNoticeMember":null,"enableVChat":null,"maxVchatMember":null,"esSearchYn":false,"commSendDisable":false,"boardSendDisable":false,"deleteDisable":false,"desktopDisable":false,"saveAllDisable":false,"use3stack":false,"autoAgreeParallel":false,"msgDisable":false,"fingerprintEnable":false,"otherCompanyDisable":false,"gknox":false,"otherCompanyBlock":false,"dmsPageReturn":false}';    
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatus('2');
            res.setStatusCode(200);
            res.setBody(outputBodyString);
            
            return res;                
      } 
     
     
     
     
}