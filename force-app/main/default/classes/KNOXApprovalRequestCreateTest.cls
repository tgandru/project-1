@isTest
private class KNOXApprovalRequestCreateTest {

	private static testMethod void testmethod1() {
	    Test.startTest();
          Sellout__C so=new Sellout__C(Closing_Month_Year1__c='10/2018',Description__c='From Test Class',Subsidiary__c='SEA',Approval_Status__c='Draft',Integration_Status__c='Draft',Has_Attachement__c=true,Request_Approval__c=true);
         insert so;
         Date d=System.today();
                  RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];

          Product2 prod1 = new Product2(
                name ='SM-TEST123',
                SAP_Material_ID__c = 'SM-TEST123',
                RecordTypeId = productRT.id
            );

        insert prod1;
          List<Sellout_Products__c> slp=new list<Sellout_Products__c>();
          Sellout_Products__c sl0=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST123',Quantity__c=100,Carrier__c='ATT',Carrier_SKU__c='Samsung Mobile',IL_CL__c='CL',Pre_Check_Result__c='OK');
          Sellout_Products__c sl1=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST123',Quantity__c=100,Carrier__c='ATT',Carrier_SKU__c='Samsung Mobile',IL_CL__c='IL',Pre_Check_Result__c='OK');
          slp.add(sl0);
          slp.add(sl1);
          insert slp;
          
          KNOX_Credentials__c kn=new KNOX_Credentials__c(Name='Sandbox',Base_URL__c='www.google.com',Token__c='abc12346590249',Company_ID__c='Sandbox');
            insert kn;
            KNOX_Credentials__c kn1=new KNOX_Credentials__c(Name='Production',Base_URL__c='www.google.com',Token__c='abc12346590249',Company_ID__c='Sandbox');
            insert kn1;
          List<Sellout_Approval_Order__c> apporder=new list<Sellout_Approval_Order__c>();
          Sellout_Approval_Order__c sellapp0=new Sellout_Approval_Order__c(Name='submitter-vj',KNOX_EPID__c='00536000002rYc2AAE',Salesforce_UserId__c='00536000002rYc2AAE',KNOX_E_mail__c='knox@testclass.com');
          Sellout_Approval_Order__c sellapp1=new Sellout_Approval_Order__c(Name='Submitter',KNOX_EPID__c='00536000002rYc2AAE',Sequence__c=0,Salesforce_UserId__c='00536000002rYc2AAE',KNOX_E_mail__c='knox@testclass.com',Approval_Type__c='0');
          Sellout_Approval_Order__c sellapp2=new Sellout_Approval_Order__c(Name='Vijay',KNOX_EPID__c='00536000002rYc2AAE',Sequence__c=1,Salesforce_UserId__c='00536000002rYc2AAE',KNOX_E_mail__c='knox@testclass.com',Approval_Type__c='1');
          apporder.add(sellapp0);
          apporder.add(sellapp1);
          apporder.add(sellapp2);
          insert apporder;
          Pagereference pf= Page.SelloutActualApproval;
           pf.getParameters().put('id',so.id);
           test.setCurrentPage(pf);
        
        apexPages.standardcontroller sc = new apexPages.standardcontroller(so);
          SelloutActualApprovalController cls=new SelloutActualApprovalController(sc);
          Attachment attach=new Attachment();   	
    	attach.Name='Unit Test Attachment';
    	Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
    	attach.body=bodyBlob;
    	cls.attachment =attach;
    	cls.Commnts='Test Class';
    	cls.InsertRecords();
    	
    	
    	String statusresult=KNOXApprovalStatus.getApprovalStatus(so.id);
    	String cancelresult=KNOXApprovalCancelRequest.cancelrequest(so.id);
    	Test.stopTest();
          
	}
	private static testMethod void testmethod2() {
	    Test.startTest();
          Sellout__C so=new Sellout__C(Closing_Month_Year1__c='10/2018',Description__c='From Test Class',Subsidiary__c='SEA',Approval_Status__c='Pending Approval',Integration_Status__c='Draft',Has_Attachement__c=true,Request_Approval__c=true);
         insert so;
         Date d=System.today();
                  RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];

          Product2 prod1 = new Product2(
                name ='SM-TEST123',
                SAP_Material_ID__c = 'SM-TEST123',
                RecordTypeId = productRT.id
            );

        insert prod1;
          List<Sellout_Products__c> slp=new list<Sellout_Products__c>();
          Sellout_Products__c sl0=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST123',Quantity__c=100,Carrier__c='ATT',Carrier_SKU__c='Samsung Mobile',IL_CL__c='CL',Pre_Check_Result__c='OK');
          Sellout_Products__c sl1=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST123',Quantity__c=100,Carrier__c='ATT',Carrier_SKU__c='Samsung Mobile',IL_CL__c='IL',Pre_Check_Result__c='OK');
          slp.add(sl0);
          slp.add(sl1);
          insert slp;
             KNOX_Credentials__c kn=new KNOX_Credentials__c(Name='Sandbox',Base_URL__c='www.google.com',Token__c='abc12346590249',Company_ID__c='Sandbox');
            insert kn;
            KNOX_Credentials__c kn1=new KNOX_Credentials__c(Name='Production',Base_URL__c='www.google.com',Token__c='abc12346590249',Company_ID__c='Sandbox');
            insert kn1;
            List<Sellout_Approval_Order__c> apporder=new list<Sellout_Approval_Order__c>();
          Sellout_Approval_Order__c sellapp0=new Sellout_Approval_Order__c(Name='submitter-vj',KNOX_EPID__c='00536000002rYc2AAE',Salesforce_UserId__c='00536000002rYc2AAE',KNOX_E_mail__c='knox@testclass.com');
          Sellout_Approval_Order__c sellapp1=new Sellout_Approval_Order__c(Name='Submitter',KNOX_EPID__c='00536000002rYc2AAE',Sequence__c=0,Salesforce_UserId__c='00536000002rYc2AAE',KNOX_E_mail__c='knox@testclass.com',Approval_Type__c='0');
          Sellout_Approval_Order__c sellapp2=new Sellout_Approval_Order__c(Name='Vijay',KNOX_EPID__c='00536000002rYc2AAE',Sequence__c=1,Salesforce_UserId__c='00536000002rYc2AAE',KNOX_E_mail__c='knox@testclass.com',Approval_Type__c='1');
          apporder.add(sellapp0);
          apporder.add(sellapp1);
          apporder.add(sellapp2);
          insert apporder;
          Pagereference pf= Page.SelloutActualApproval;
           pf.getParameters().put('id',so.id);
           test.setCurrentPage(pf);
        
        apexPages.standardcontroller sc = new apexPages.standardcontroller(so);
          SelloutActualApprovalController cls=new SelloutActualApprovalController(sc);
          Attachment attach=new Attachment();   	
    	attach.Name='Unit Test Attachment';
    	Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
    	attach.body=bodyBlob;
    	cls.attachment =attach;
    	cls.Commnts='Test Class';
    	cls.InsertRecords();
    	
    	
    	String statusresult=KNOXApprovalStatus.getApprovalStatus(so.id);
    	String cancelresult=KNOXApprovalCancelRequest.cancelrequest(so.id);
    	Test.stopTest();
          
	}
	
		private static testMethod void testmethod3() {
	    Test.startTest();
          Sellout__C so=new Sellout__C(Closing_Month_Year1__c='10/2018',Description__c='From Test Class',Subsidiary__c='SEA',Approval_Status__c='Pending Approval',Integration_Status__c='Draft',Has_Attachement__c=true,Request_Approval__c=true);
         insert so;
         Date d=System.today();
                  RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];

          Product2 prod1 = new Product2(
                name ='SM-TEST123',
                SAP_Material_ID__c = 'SM-TEST123',
                RecordTypeId = productRT.id
            );

        insert prod1;
          List<Sellout_Products__c> slp=new list<Sellout_Products__c>();
          Sellout_Products__c sl0=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST123',Quantity__c=100,Carrier__c='ATT',Carrier_SKU__c='Samsung Mobile',IL_CL__c='CL',Pre_Check_Result__c='OK');
          Sellout_Products__c sl1=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST123',Quantity__c=100,Carrier__c='ATT',Carrier_SKU__c='Samsung Mobile',IL_CL__c='IL',Pre_Check_Result__c='OK');
          slp.add(sl0);
          slp.add(sl1);
          insert slp;
          List<Sellout_Approval_Order__c> apporder=new list<Sellout_Approval_Order__c>();
          Sellout_Approval_Order__c sellapp0=new Sellout_Approval_Order__c(Name='submitter-vj',KNOX_EPID__c='00536000002rYc2AAE',Salesforce_UserId__c='00536000002rYc2AAE',KNOX_E_mail__c='knox@testclass.com');
          Sellout_Approval_Order__c sellapp1=new Sellout_Approval_Order__c(Name='Submitter',KNOX_EPID__c='00536000002rYc2AAE',Sequence__c=0,Salesforce_UserId__c='00536000002rYc2AAE',KNOX_E_mail__c='knox@testclass.com',Approval_Type__c='0');
          Sellout_Approval_Order__c sellapp2=new Sellout_Approval_Order__c(Name='Vijay',KNOX_EPID__c='00536000002rYc2AAE',Sequence__c=1,Salesforce_UserId__c='00536000002rYc2AAE',KNOX_E_mail__c='knox@testclass.com',Approval_Type__c='1');
          apporder.add(sellapp0);
          apporder.add(sellapp1);
          apporder.add(sellapp2);
          insert apporder;
             KNOX_Credentials__c kn=new KNOX_Credentials__c(Name='Sandbox',Base_URL__c='www.google.com',Token__c='abc12346590249',Company_ID__c='Sandbox');
            insert kn;
            KNOX_Credentials__c kn1=new KNOX_Credentials__c(Name='Production',Base_URL__c='www.google.com',Token__c='abc12346590249',Company_ID__c='Sandbox');
            insert kn1;
          Pagereference pf= Page.SelloutActualApproval;
           pf.getParameters().put('id',so.id);
           test.setCurrentPage(pf);
        
        apexPages.standardcontroller sc = new apexPages.standardcontroller(so);
          SelloutActualApprovalController cls=new SelloutActualApprovalController(sc);
          Attachment attach=new Attachment();   	
    	attach.Name='Unit Test Attachment';
    	Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
    	attach.body=bodyBlob;
    	cls.attachment =attach;
    	
    	cls.InsertRecords();
    	
    	
    	
    	Test.stopTest();
          
	}
	
	
	
		private static testMethod void testmethod4() {
	    Test.startTest();
          Sellout__C so=new Sellout__C(Closing_Month_Year1__c='10/2018',Description__c='From Test Class',Subsidiary__c='SEA',Approval_Status__c='Draft',Integration_Status__c='Draft',Has_Attachement__c=true,Request_Approval__c=true);
         insert so;
         Date d=System.today();
                  RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];

          Product2 prod1 = new Product2(
                name ='SM-TEST123',
                SAP_Material_ID__c = 'SM-TEST123',
                RecordTypeId = productRT.id
            );

        insert prod1;
          List<Sellout_Products__c> slp=new list<Sellout_Products__c>();
          Sellout_Products__c sl0=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST123',Quantity__c=100,Carrier__c='ATT',Carrier_SKU__c='Samsung Mobile',IL_CL__c='CL',Pre_Check_Result__c='OK');
          Sellout_Products__c sl1=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST123',Quantity__c=100,Carrier__c='ATT',Carrier_SKU__c='Samsung Mobile',IL_CL__c='IL',Pre_Check_Result__c='OK');
          slp.add(sl0);
          slp.add(sl1);
          insert slp;
          
          KNOX_Credentials__c kn=new KNOX_Credentials__c(Name='Sandbox',Base_URL__c='www.google.com',Token__c='abc12346590249',Company_ID__c='Sandbox');
            insert kn;
            KNOX_Credentials__c kn1=new KNOX_Credentials__c(Name='Production',Base_URL__c='www.google.com',Token__c='abc12346590249',Company_ID__c='Sandbox');
            insert kn1;
          List<Sellout_Approval_Order__c> apporder=new list<Sellout_Approval_Order__c>();
          Sellout_Approval_Order__c sellapp0=new Sellout_Approval_Order__c(Name='submitter-vj',KNOX_EPID__c='00536000002rYc2AAE',Salesforce_UserId__c='00536000002rYc2AAE',KNOX_E_mail__c='knox@testclass.com');
          Sellout_Approval_Order__c sellapp1=new Sellout_Approval_Order__c(Name='Submitter',KNOX_EPID__c='00536000002rYc2AAE',Sequence__c=0,Salesforce_UserId__c='00536000002rYc2AAE',KNOX_E_mail__c='knox@testclass.com',Approval_Type__c='0');
          Sellout_Approval_Order__c sellapp2=new Sellout_Approval_Order__c(Name='Vijay',KNOX_EPID__c='00536000002rYc2AAE',Sequence__c=1,Salesforce_UserId__c='00536000002rYc2AAE',KNOX_E_mail__c='knox@testclass.com',Approval_Type__c='1');
          apporder.add(sellapp0);
          apporder.add(sellapp1);
          apporder.add(sellapp2);
          insert apporder;
          Pagereference pf= Page.SelloutActualApproval;
           pf.getParameters().put('id',so.id);
           test.setCurrentPage(pf);
        
        apexPages.standardcontroller sc = new apexPages.standardcontroller(so);
          SelloutActualApprovalController cls=new SelloutActualApprovalController(sc);
          Attachment attach=new Attachment();   	
    	attach.Name='Unit Test Attachment';
    	Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
    	attach.body=bodyBlob;
    	cls.attachment =attach;
    	cls.Commnts='Test Class';
    	cls.InsertRecords();
    	
    	
    	 message_queue__c mq1=new message_queue__c();
             mq1.Object_Name__c='Sellout';
             mq1.Integration_Flow_Type__c='KNOX_Status_Update';
             mq1.Status__c='not started';
              mq1.retry_counter__c =0;
              mq1.Identification_Text__c=so.id;
              
              insert mq1;
              KNOXApprovalStatus.getApprovalStatu(mq1);
    	Test.stopTest();
          
	}

}