Global class KNOXApprovalStatus {
   Public Static void getApprovalStatu(message_queue__c mq){
         Sellout__c so=[select RecordTypeId,id,name,Approval_Status__c,Closing_Month_Year1__c,Comments__c,KNOX_Approval_ID__c,Description__c,Has_Attachement__c,OwnerId ,Integration_Status__c ,Total_CL_Qty__c,Total_IL_Qty__c,Subsidiary__c from Sellout__c where id=:mq.Identification_Text__c limit 1];
         string res= makecallout(so);
       StatusResponse resp=new StatusResponse();
             resp=(StatusResponse)JSON.deserialize(res, StatusResponse.class);
             String rtnmsg='';
             if(resp.result=='OK'){
                handleSucessFromBatch(so,resp,mq); 
                
             }else{
              
             }
   }
   @AuraEnabled
     webservice static String getApprovalStatus(String soid){
              Sellout__c so=[select RecordTypeId,id,name,Approval_Status__c,Closing_Month_Year1__c,Comments__c,KNOX_Approval_ID__c,Description__c,Has_Attachement__c,OwnerId ,Integration_Status__c ,Total_CL_Qty__c,Total_IL_Qty__c,Subsidiary__c from Sellout__c where id=:soid limit 1];
             String Res=makecallout(so);
             
             StatusResponse resp=new StatusResponse();
             resp=(StatusResponse)JSON.deserialize(res, StatusResponse.class);
             String rtnmsg='';
             if(resp.result=='OK'){
                handleSucess(so,resp); 
                rtnmsg='Sucessfully Updated the Status';
             }else{
               handleerror(so,resp);  
               rtnmsg='Something went wrong. Please Conatct Admim. Error:'+resp.message;
             }
             
     return rtnmsg;  
   }
   
   
   
   
   Public Static String makecallout(Sellout__c so){
         String endpoint='';
         String tkn='';
         String companycode='';
        Boolean isQA=[SELECT Id, IsSandbox  FROM Organization limit 1].IsSandbox;
        if(isQA==true){
            endpoint=KNOX_Credentials__c.getInstance('Sandbox').Base_URL__c;
            tkn=KNOX_Credentials__c.getInstance('Sandbox').Token__c;
            companycode=KNOX_Credentials__c.getInstance('Sandbox').Company_ID__c;
        }else{
            endpoint=KNOX_Credentials__c.getInstance('Production').Base_URL__c;
            tkn=KNOX_Credentials__c.getInstance('Production').Token__c;
            companycode=KNOX_Credentials__c.getInstance('Production').Company_ID__c;
        }
       
        RequestBody req=new RequestBody();
        req.apInfId=so.KNOX_Approval_ID__c;
        req.infSysId=companycode;  
        String bdy = JSON.serialize(req);
         
          Httprequest httpReq = new Httprequest();
                        HttpResponse httpRes = new HttpResponse();
                        Http http = new Http();
                        httpReq.setMethod('POST');
                        httpReq.setBody(bdy);
                        httpReq.setTimeout(120000);
                      //   String endpoint=[SELECT DeveloperName, Endpoint FROM NamedCredential where DeveloperName='X012_013_ROI' limit 1].Endpoint;
                          endpoint=endpoint+'/knoxrest/approval/sub/api/basic/v1.0/statusInquiry/getStatusByMisId';
                        httpReq.setEndpoint(endpoint);
                        httpReq.setHeader('Authorization', 'Bearer '+tkn); 
                        httpReq.setHeader('Content-Type', 'application/json');
                        httpReq.setHeader('Accept', 'application/json');

                      if(!Test.isRunningTest()){
                          httpRes = http.send(httpReq);
                        }else{
                           httpRes = Testrespond(httpReq);
                        }
                         System.debug('Request  ::'+bdy);
                       System.debug('Response ::'+httpRes);
                       System.debug('Response Body ::'+httpRes.getBody());
                    String resBody=httpRes.getBody();
       
       return resBody;
   }
   
    public class RequestBody{
       public String infSysId {get;set;} //System ID
       public String apInfId {get;set;} // Unique ID 
     }
     
      public class StatusResponse{
          public String result {get;set;}
          public String errorCode {get;set;}
          public String message {get;set;}
          public body data {get;set;}
      }
     
     public class body{
          public list<approvers> wSO2AapTaplnVOs {get;set;} // Approval Steps
           public String apStatsCd {get;set;} // 1 -Processing, Pending-0, Approval Complete -2, Rejected -3
           public String txtTypeCd {get; set;} // Body Type 1 -HTML,0-TEXT
           public String apDocSecuTypeCd {get; set;} //Security Type General -000 , Not Allowd Outside-001 , Top Secret -002
           public String ntfTypeCd {get; set;} //Notification Option  Genetral -0, All Notification -1
           public String urgYn {get; set;} // Urgency Normal -N, Urgency -Y
           public String sbmDt {get;set;} // Submit Date YYYYMMDDHHmmss
           public String apTtl {get;set;} //Approval Title MaX - 60 char
           public String chsetCd {get;set;} // Locale and Encoding
           public String infSysId {get;set;} //System ID
           public String apInfId {get;set;} // Unique ID 
           public String docMngSaveCd {get; set;} //Document Archiving Mangement Code
           public String timeZoneId {get;set;}//Time zone
          
        }
       
        
        public class approvers{
             public String apOpinCn {get;set;} // Submitter or Approval Comments
             public String apRelrFnm {get;set;}
             public String apRelrEpid {get;set;} // Approver EPID 
             public String apRelrMailAddr {get;set;} // Approver KNOX Email
             public String apSeq {get;set;} // Approver Sequence, Submitter will 0
             public String apRlCd {get;set;}// Approval Type 0-Submitter , Approver-1, Consent -2, Post Approval-4,Notification -9
             public String aplnStatsCd {get ; set;} //Approve -1 , Pending -0 , 
             public String actPrssDt {get ; set;}
             public String docArvDt {get ; set;}
             public String strDocArvDt {get ; set;}
             public String strActPrssDt {get ; set;}
            
        }
        
        Public Static void handleSucess(Sellout__c so,StatusResponse resp){
            List<Sellout_Approval__c> slap=new list<Sellout_Approval__c>([select id,Step_No__c,Approval_Type__c,Status__c,KNOX_Approval_ID__c,KNOX_E_mail__c,Date__c,Comments__c from Sellout_Approval__c where Sellout__c=:so.id and KNOX_Approval_ID__c=:so.KNOX_Approval_ID__c order by Step_No__c asc]);
            String hdrstatus=resp.data.apStatsCd;
             String timezn=resp.data.timeZoneId;
             String DocManage=resp.data.docMngSaveCd;
             String Comny=resp.data.apInfId;
             String Syscode=resp.data.infSysId;
             String title=resp.data.apTtl;
             String urgency =resp.data.urgYn;
            map<string,string> hdrstsmap=new map<string,string>();
            hdrstsmap.put('0','Pending Approval');
            hdrstsmap.put('1','Pending Approval');
            hdrstsmap.put('2','Approved');
            hdrstsmap.put('3','Rejected');
             hdrstsmap.put('4','Draft');
             map<string,string> lnstsmap=new map<string,string>();
             
            lnstsmap.put('0','Pending');
            lnstsmap.put('1','Approved');
            lnstsmap.put('2','Rejected');
            lnstsmap.put('3','Arbitrary Approval');
            lnstsmap.put('5','Auto Approval');
            lnstsmap.put('6','Auto Reject');
              map<string,string> approvalType=new map<string,string>();
              approvalType.put('0','Submitted');
              approvalType.put('1','Approved');
              approvalType.put('2','Consent');
              approvalType.put('9','Notification');
            
            
            So.Approval_Status__c=hdrstsmap.get(hdrstatus);
            datetime recnttime;
            for(Sellout_Approval__c sa:slap){
                 
                for(approvers ap:resp.data.wSO2AapTaplnVOs){
                      integer stepno=integer.valueOf(ap.apSeq);
                      if(integer.valueOf(sa.Step_No__c)==stepno){
                         
                          String stut=ap.aplnStatsCd;
                          string aprvdat=ap.actPrssDt;//20181219192306
                                                      //YYYYMMDDHHMMss
                          if(aprvdat !=null && aprvdat !=''){
                             String stringDate = aprvdat.substring(0,4) + '-' + aprvdat.substring(4,6) + '-' + aprvdat.substring(6,8) + ' ' +aprvdat.substring(8,10)+':'+aprvdat.substring(10,12)+':'+aprvdat.substring(12,14);
                            Datetime dtnow=System.now();
                            Integer offset = UserInfo.getTimezone().getOffset(dtnow);
                               Integer hour=(offset/1000)/3600;
                             DateTime dt = datetime.valueOf(stringDate).addHours(hour);
                             sa.Date__c=dt;
                              recnttime=dt;
                          }
                          
                          
                          sa.Status__c =lnstsmap.get(stut);
                          sa.Comments__c=ap.apOpinCn;
                           if(hdrstatus=='4' && sa.Status__c=='Pending'&&sa.Approval_Type__c !='9'){
                                sa.Status__c='Recalled';
                                sa.Date__c=recnttime;
                            }
                          if(sa.Status__c=='Approved' && sa.Approval_Type__c=='2'){
                               sa.Status__c='Consent';
                          }
                          if(sa.Status__c=='Pending' && sa.Approval_Type__c=='0'){
                               sa.Status__c='Submitted';
                          }
                          
                          if(So.Approval_Status__c=='Approved' && sa.Approval_Type__c=='9'){
                              sa.Status__c='Notification';
                          }
                          if((So.Approval_Status__c=='Rejected'||So.Approval_Status__c=='Draft') && sa.Approval_Type__c=='9'){
                              sa.Status__c='Not Delivered';
                          }
                          if((hdrstatus=='4'|| hdrstatus=='3') && sa.Status__c=='Pending'){
                               sa.Date__c=recnttime;
                          }
                          
                          break;
                      }
                }
            }
            
            update slap;
            update so;
            if(so.Approval_Status__c !='Pending Approval'){
                
                Boolean result = System.Approval.unlock(so.id).isSuccess();

            }
            
            message_queue__c mq=new message_queue__c();
             mq.Object_Name__c='Sellout';
             mq.Integration_Flow_Type__c='KNOX_Status_Update';
             mq.Status__c='sucess';
             mq.Response_Body__c=String.valueOf(resp);
             mq.Identification_Text__c=so.id;
             insert mq;
            
            
            
        }
          Public Static void handleError(Sellout__c so,StatusResponse resp){
              message_queue__c mq=new message_queue__c();
             mq.Object_Name__c='Sellout';
             mq.Integration_Flow_Type__c='KNOX_Status_Update';
             mq.Status__c='failed';
             mq.Response_Body__c=String.valueOf(resp);
             mq.Identification_Text__c=so.id;
             insert mq;
          }
        
           
            Public Static void handleSucessFromBatch(Sellout__c so,StatusResponse resp,message_queue__c mq){
            List<Sellout_Approval__c> slap=new list<Sellout_Approval__c>([select id,Step_No__c,Approval_Type__c,Status__c,KNOX_Approval_ID__c,KNOX_E_mail__c,Date__c,Comments__c from Sellout_Approval__c where Sellout__c=:so.id and KNOX_Approval_ID__c=:so.KNOX_Approval_ID__c order by Step_No__c asc]);
            String hdrstatus=resp.data.apStatsCd;
             String timezn=resp.data.timeZoneId;
             String DocManage=resp.data.docMngSaveCd;
             String Comny=resp.data.apInfId;
             String Syscode=resp.data.infSysId;
             String title=resp.data.apTtl;
             String urgency =resp.data.urgYn;
            map<string,string> hdrstsmap=new map<string,string>();
            hdrstsmap.put('0','Pending Approval');
            hdrstsmap.put('1','Pending Approval');
            hdrstsmap.put('2','Approved');
            hdrstsmap.put('3','Rejected');
             hdrstsmap.put('4','Draft');
             map<string,string> lnstsmap=new map<string,string>();
             
            lnstsmap.put('0','Pending');
            lnstsmap.put('1','Approved');
            lnstsmap.put('2','Rejected');
            lnstsmap.put('3','Arbitrary Approval');
            lnstsmap.put('5','Auto Approval');
            lnstsmap.put('6','Auto Reject');
              map<string,string> approvalType=new map<string,string>();
              approvalType.put('0','Submitted');
              approvalType.put('1','Approved');
              approvalType.put('2','Consent');
              approvalType.put('9','Notification');
            
            
            So.Approval_Status__c=hdrstsmap.get(hdrstatus);
            datetime recnttime;
            for(Sellout_Approval__c sa:slap){
                 
                for(approvers ap:resp.data.wSO2AapTaplnVOs){
                      integer stepno=integer.valueOf(ap.apSeq);
                      if(integer.valueOf(sa.Step_No__c)==stepno){
                         
                          String stut=ap.aplnStatsCd;
                          string aprvdat=ap.actPrssDt;//20181219192306
                                                      //YYYYMMDDHHMMss
                          if(aprvdat !=null && aprvdat !=''){
                             String stringDate = aprvdat.substring(0,4) + '-' + aprvdat.substring(4,6) + '-' + aprvdat.substring(6,8) + ' ' +aprvdat.substring(8,10)+':'+aprvdat.substring(10,12)+':'+aprvdat.substring(12,14);
                            Datetime dtnow=System.now();
                            Integer offset = UserInfo.getTimezone().getOffset(dtnow);
                               Integer hour=(offset/1000)/3600;
                             DateTime dt = datetime.valueOf(stringDate).addHours(hour);
                             sa.Date__c=dt;
                              recnttime=dt;
                          }
                          
                          
                          sa.Status__c =lnstsmap.get(stut);
                          sa.Comments__c=ap.apOpinCn;
                           if(hdrstatus=='4' && sa.Status__c=='Pending'&&sa.Approval_Type__c !='9'){
                                sa.Status__c='Recalled';
                                sa.Date__c=recnttime;
                            }
                          if(sa.Status__c=='Approved' && sa.Approval_Type__c=='2'){
                               sa.Status__c='Consent';
                          }
                          if(sa.Status__c=='Pending' && sa.Approval_Type__c=='0'){
                               sa.Status__c='Submitted';
                          }
                          
                          if(So.Approval_Status__c=='Approved' && sa.Approval_Type__c=='9'){
                              sa.Status__c='Notification';
                          }
                          if((So.Approval_Status__c=='Rejected'||So.Approval_Status__c=='Draft') && sa.Approval_Type__c=='9'){
                              sa.Status__c='Not Delivered';
                          }
                          if((hdrstatus=='4'|| hdrstatus=='3') && sa.Status__c=='Pending'){
                               sa.Date__c=recnttime;
                          }
                          
                          break;
                      }
                }
            }
            
            update slap;
            update so;
            if(so.Approval_Status__c !='Pending Approval'){
                
                Boolean result = System.Approval.unlock(so.id).isSuccess();

            }
            
          if(So.Approval_Status__c=='Approved' || So.Approval_Status__c=='Draft'){
               mq.Response_Body__c=String.valueOf(resp);
               mq.Status__c='sucess';
              
          }else{
               mq.Response_Body__c=String.valueOf(resp);
               mq.Status__c='not started';
          }
           update mq; 
            
            
        }
     public Static HTTPResponse Testrespond(HTTPRequest req) {          
               String outputBodyString = '{"result":"OK","errorCode":null,"message":"getStatusByMisId success","data":{"txtTypeCd":"1","sbmDt":"20181220161542","chsetCd":"en_US.UTF-8","apInfId":null,"infSysId":"C10REST0216","ntfTypeCd":"0","urgYn":"0","apDocSecuTypeCd":"0","apStatsCd":"2","timeZone":"GMT","apTtl":"Sell-Out Actual approval request for 12/2018","wSO2AapTaplnVOs":[{"actPrssDt":"20181220161542","docOpenDt":null,"docArvDt":"20181220161542","apOpinCn":"Test For Populating KNOX EMPID from Custom Setting","apRelrCompCd":"C6A","apRelrCompNm":"Samsung SDS(America)","apRelrDeptCd":"C6AOU000","apRelrDeptNm":"Shared Service Center","apRelrEpid":"M181214205717C6A2844","apRelrMailAddr":"v.kamani@stage.partner.samsung.com","apRelrFnm":"Kamani Saradhi Vijay","apRelrJobpoCd":"D10","apRelrJobpoNm":"대리","apRelrSuborgCd":null,"apRelrSuborgNm":null,"apRlCd":"0","apSeq":"0","aplnStatsCd":"0","arbPmtYn":"N","dlgAprEpid":null,"docDelCd":null,"pathMdfyPmtYn":"N","prxAprYn":"N","txtMdfyPmtYn":"N","keepTermVal":null,"rsrvSbmDt":"","txtMdfyActYn":"N","pathMdfyActYn":"N","arbActYn":"N","strDocArvDt":"20181220161542","strActPrssDt":"20181220161542","strDocOpenDt":null},{"actPrssDt":"20181220161723","docOpenDt":null,"docArvDt":"20181220161542","apOpinCn":"","apRelrCompCd":"C6A","apRelrCompNm":"Samsung SDS(America)","apRelrDeptCd":"C6AOU000","apRelrDeptNm":"Shared Service Center","apRelrEpid":"M181214205310C6A441","apRelrMailAddr":"stevepark@stage.samsung.com","apRelrFnm":"박상희","apRelrJobpoCd":"C10","apRelrJobpoNm":"차장","apRelrSuborgCd":null,"apRelrSuborgNm":null,"apRlCd":"1","apSeq":"1","aplnStatsCd":"1","arbPmtYn":"N","dlgAprEpid":null,"docDelCd":null,"pathMdfyPmtYn":"N","prxAprYn":"N","txtMdfyPmtYn":"N","keepTermVal":null,"rsrvSbmDt":"","txtMdfyActYn":"N","pathMdfyActYn":"N","arbActYn":"N","strDocArvDt":"20181220161542","strActPrssDt":"20181220161723","strDocOpenDt":null},{"actPrssDt":"20181220161855","docOpenDt":null,"docArvDt":"20181220161723","apOpinCn":"","apRelrCompCd":"C6A","apRelrCompNm":"Samsung SDS(America)","apRelrDeptCd":"C6AOU000","apRelrDeptNm":"Shared Service Center","apRelrEpid":"M181214210521C6A5675","apRelrMailAddr":"t.gandru@stage.partner.samsung.com","apRelrFnm":"Thirupathirao Gandru","apRelrJobpoCd":"K10","apRelrJobpoNm":"과장","apRelrSuborgCd":null,"apRelrSuborgNm":null,"apRlCd":"2","apSeq":"2","aplnStatsCd":"1","arbPmtYn":"N","dlgAprEpid":null,"docDelCd":null,"pathMdfyPmtYn":"N","prxAprYn":"N","txtMdfyPmtYn":"N","keepTermVal":null,"rsrvSbmDt":"","txtMdfyActYn":"N","pathMdfyActYn":"N","arbActYn":"N","strDocArvDt":"20181220161723","strActPrssDt":"20181220161855","strDocOpenDt":null},{"actPrssDt":"20181220162003","docOpenDt":null,"docArvDt":"20181220161855","apOpinCn":"","apRelrCompCd":"C6A","apRelrCompNm":"Samsung SDS(America)","apRelrDeptCd":"C6AOU000","apRelrDeptNm":"Shared Service Center","apRelrEpid":"M181214210030C6A429","apRelrMailAddr":"p.kabalai@stage.samsung.com","apRelrFnm":"Pradeep Kabalai","apRelrJobpoCd":"D10","apRelrJobpoNm":"대리","apRelrSuborgCd":null,"apRelrSuborgNm":null,"apRlCd":"1","apSeq":"3","aplnStatsCd":"1","arbPmtYn":"N","dlgAprEpid":null,"docDelCd":null,"pathMdfyPmtYn":"N","prxAprYn":"N","txtMdfyPmtYn":"N","keepTermVal":null,"rsrvSbmDt":"","txtMdfyActYn":"N","pathMdfyActYn":"N","arbActYn":"N","strDocArvDt":"20181220161855","strActPrssDt":"20181220162003","strDocOpenDt":null},{"actPrssDt":"20181220162003","docOpenDt":null,"docArvDt":"20181220162003","apOpinCn":"","apRelrCompCd":"C6A","apRelrCompNm":"Samsung SDS(America)","apRelrDeptCd":"C6AOU000","apRelrDeptNm":"Shared Service Center","apRelrEpid":"M181214205310C6A441","apRelrMailAddr":"stevepark@stage.samsung.com","apRelrFnm":"박상희","apRelrJobpoCd":"C10","apRelrJobpoNm":"차장","apRelrSuborgCd":null,"apRelrSuborgNm":null,"apRlCd":"9","apSeq":"4","aplnStatsCd":"0","arbPmtYn":"N","dlgAprEpid":null,"docDelCd":null,"pathMdfyPmtYn":"N","prxAprYn":"N","txtMdfyPmtYn":"N","keepTermVal":null,"rsrvSbmDt":"","txtMdfyActYn":"N","pathMdfyActYn":"N","arbActYn":"N","strDocArvDt":"20181220162003","strActPrssDt":"20181220162003","strDocOpenDt":null},{"actPrssDt":"20181220162003","docOpenDt":null,"docArvDt":"20181220162003","apOpinCn":"","apRelrCompCd":"C6A","apRelrCompNm":"Samsung SDS(America)","apRelrDeptCd":"C6AOU000","apRelrDeptNm":"Shared Service Center","apRelrEpid":"M181214205717C6A2844","apRelrMailAddr":"v.kamani@stage.partner.samsung.com","apRelrFnm":"Kamani Saradhi Vijay","apRelrJobpoCd":"D10","apRelrJobpoNm":"대리","apRelrSuborgCd":null,"apRelrSuborgNm":null,"apRlCd":"9","apSeq":"5","aplnStatsCd":"0","arbPmtYn":"N","dlgAprEpid":null,"docDelCd":null,"pathMdfyPmtYn":"N","prxAprYn":"N","txtMdfyPmtYn":"N","keepTermVal":null,"rsrvSbmDt":"","txtMdfyActYn":"N","pathMdfyActYn":"N","arbActYn":"N","strDocArvDt":"20181220162003","strActPrssDt":"20181220162003","strDocOpenDt":null}]},"faultActor":null,"faultCode":null,"faultString":null}';
         HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatus('2');
            res.setStatusCode(200);
            res.setBody(outputBodyString);
            
            return res;                
      } 
     
     
   
}