/**
 * This class contains unit tests for validating the behavior of LeadConvert Trigger and LeadTriggerHandler class.
 * 
 * @Author Bala Rajasekharan 
 */
@isTest
private class LeadConvertTriggerTest {
    
    

    static testMethod void testLeadTriggerHandler() {
    	
    	List<Lead> leadList = new List<Lead>();
    	Map<Id, Lead> updateLeadMap = new Map<Id,Lead>();
 
       Zipcode_Lookup__c zipCode = TestDataUtility.createZipcode();
       insert zipCode;
    	
    	//Create bunch of Leads
    	for (Integer i=0; i<6; i++){
    		Lead l = createLead(i);
            l.street='123 sample st';
            l.city='San Francisco';
            l.state='CA';
            l.Country='US';
            l.PostalCode='94102';
    		leadList.add(l);
    	}
    	
        insert leadList;

    }
    static testMethod void updateLeadTest(){
       Zipcode_Lookup__c zipCode = TestDataUtility.createZipcode();
       insert zipCode;

        Lead l = createLead(99);
            l.street='123 sample st';
            l.city='San Francisco';
            l.state='CA';
            l.Country='US';
            l.PostalCode='94102';
        	l.status = 'New';
    	
        insert l;
        
        l.Status = 'Active';
        l.PRM_Lead_Id__c ='1919';
        update l;
        
        Lead le = [Select Id, status from Lead where Id=:l.Id];
        System.assertEquals('Active',le.Status);
        
    }
    static testMethod void updateLeadNegativeTest(){
       Zipcode_Lookup__c zipCode = TestDataUtility.createZipcode();
       insert zipCode;

        Lead l = createLead(99);
            l.street='123 sample st';
            l.city='Jersey City';
            l.state='Ny';
            l.Country=null;
            l.PostalCode=null;
        	//l.status = 'New';
        try{
            test.startTest();
                insert l;
            test.stopTest();
        }
        catch(Exception e){
            system.debug('##'+e);
        }
        System.assert(l.ID<>null);
        
    }
    static testMethod void leadConvertNegativeTest(){
        Group testGroup = new Group(Name = 'Queue', Type = 'Queue');
        insert testGroup;
        
        QueueSobject testQueue = new QueueSObject(QueueId = testGroup.Id, SobjectType = 'Lead');
        	System.runAs(new User(Id = UserInfo.getUserId())) {   
            insert testQueue;
        }

        Zipcode_Lookup__c zipCode = TestDataUtility.createZipcode();
       insert zipCode;

        Lead l = createLead(109);
            l.street='123 sample st';
            l.city='San Francisco';
            l.state='CA';
            l.Country='US';
            l.PostalCode='94102';
        	l.status = 'New';
       		l.OwnerId = testGroup.Id;
        insert l;
        
        ApexPages.StandardController lc = new ApexPages.StandardController(l);
        leadConvertController testLeadCont = new leadConvertController(lc);
        
        PageReference pageRef = Page.leadConvertPage;
        pageRef.getParameters().put('id', String.valueOf(l.Id));
        Test.setCurrentPage(pageRef);
        try{
        	testLeadCont.convertLead();
        }
        catch(Exception e){
            System.debug('## leadConvert'+e);
        }
        
    }
    public static Lead createLead(Integer i) {
    	
    	return new Lead(LastName='Jones'+i, FirstName='Dow'+i, Company='Acumen', Status='inquiry', City='Herndon', PostalCode='94102');
    	
    }
    
    	private static testMethod void testmethod2() {
	     Test.startTest();
         string   CampaignTypeId = [Select Id From RecordType Where SobjectType = 'Campaign' and Name = 'Sales'  limit 1].Id;
	    string   LeadTypeId = [Select Id From RecordType Where SobjectType = 'Lead' and Name = 'End User'  limit 1].Id;
       Campaign cmp= new Campaign(Name='Test Campaign for test class ',	IsActive=true,type='Email',StartDate=system.Today(),recordtypeid=CampaignTypeId);
       insert cmp;
       List<Lead> leadLst = new list<Lead>();
       
       lead le1=new lead(lastname='test lead 123',Company='My Lead Company',Email='mylead@sds.com',	MQL_Score__c='T1',Status='New',LeadSource='Others',recordtypeid=LeadTypeId);
	   leadLst.add(le1);
	    lead le2=new lead(lastname='test lead 1232',Company='My Lead Company2',Email='mylead@sds2.com',	MQL_Score__c='A+',Status='NEW',LeadSource='Others',recordtypeid=LeadTypeId);
	    leadLst.add(le2);
	    lead le3=new lead(lastname='test lead 1233',Company='My Lead Company3',Email='mylead@sds3.com',Status='Account / Oppty',Division__c='IT',ProductGroup__c='OFFICE',LeadSource='Others',recordtypeid=LeadTypeId);
	   leadLst.add(le3);
	    insert leadLst;
	     list<Inquiry__c> iqLst = new list<Inquiry__c>();
	    for(Lead l:leadLst){
	        Inquiry__c iq=new Inquiry__c(Lead__c = l.id,Product_Solution_of_Interest__c='Monitors',	 campaign__c = cmp.id);
    		   iqLst.add(iq);
	    }
	    insert iqLst; 
	     
	     lead l=[select id,name,status,reason__c from lead limit 1];
	      l.status='Invalid'; 
	      l.Reason__c='SPAM';
	      l.Sub_Status__c='Spam';
	       update l;
	       
         Test.stopTest();     
	    
	    
	}
	
	private static testMethod void testmethod3() {
	     Test.startTest();
         string   CampaignTypeId = [Select Id From RecordType Where SobjectType = 'Campaign' and Name = 'Sales'  limit 1].Id;
	    string   LeadTypeId = [Select Id From RecordType Where SobjectType = 'Lead' and Name = 'End User'  limit 1].Id;
       Campaign cmp= new Campaign(Name='Test Campaign for test class ',	IsActive=true,type='Email',StartDate=system.Today(),recordtypeid=CampaignTypeId);
       insert cmp;
       List<Lead> leadLst = new list<Lead>();
       
       lead le1=new lead(lastname='test lead 123',Company='My Lead Company',Email='mylead@sds.com',	MQL_Score__c='T1',Status='New',LeadSource='Others',recordtypeid=LeadTypeId);
	   leadLst.add(le1);
	    lead le2=new lead(lastname='test lead 1232',Company='My Lead Company2',Email='mylead@sds2.com',	MQL_Score__c='A+',Status='NEW',LeadSource='Others',recordtypeid=LeadTypeId);
	    leadLst.add(le2);
	    lead le3=new lead(lastname='test lead 1233',Company='My Lead Company3',Email='mylead@sds3.com',Status='Account / Oppty',Division__c='IT',ProductGroup__c='OFFICE',LeadSource='Others',recordtypeid=LeadTypeId);
	   leadLst.add(le3);
	    insert leadLst;
	     list<Inquiry__c> iqLst = new list<Inquiry__c>();
	    for(Lead l:leadLst){
	        Inquiry__c iq=new Inquiry__c(Lead__c = l.id,Product_Solution_of_Interest__c='Monitors',	 campaign__c = cmp.id);
    		   iqLst.add(iq);
	    }
	    insert iqLst; 
	      LeadTriggerHandler.isFirstTime=true;
	     lead l=[select id,name,status,reason__c from lead limit 1];
	      l.status='Active'; 
	    
	       l.street='123 sample st';
            l.city='San Francisco';
            l.state='CA';
            l.Country='US';
            l.PostalCode='94102';
	       update l;
	       
         Test.stopTest();     
	    
	    
	}
	
	private static testMethod void testmethod4() {
	     Test.startTest();
         string   CampaignTypeId = [Select Id From RecordType Where SobjectType = 'Campaign' and Name = 'Sales'  limit 1].Id;
	    string   LeadTypeId = [Select Id From RecordType Where SobjectType = 'Lead' and Name = 'End User'  limit 1].Id;
       Campaign cmp= new Campaign(Name='Test Campaign for test class ',	IsActive=true,type='Email',StartDate=system.Today(),recordtypeid=CampaignTypeId);
       insert cmp;
       List<Lead> leadLst = new list<Lead>();
       
       lead le1=new lead(lastname='test lead 123',Company='My Lead Company',Email='mylead@sds.com',	MQL_Score__c='T1',Status='New',LeadSource='Others',recordtypeid=LeadTypeId);
	   leadLst.add(le1);
	    lead le2=new lead(lastname='test lead 1232',Company='My Lead Company2',Email='mylead@sds2.com',	MQL_Score__c='A+',Status='NEW',LeadSource='Others',recordtypeid=LeadTypeId);
	    leadLst.add(le2);
	    lead le3=new lead(lastname='test lead 1233',Company='My Lead Company3',Email='mylead@sds3.com',Status='Account / Oppty',Division__c='IT',ProductGroup__c='OFFICE',LeadSource='Others',recordtypeid=LeadTypeId);
	   leadLst.add(le3);
	    insert leadLst;
	     list<Inquiry__c> iqLst = new list<Inquiry__c>();
	    for(Lead l:leadLst){
	        Inquiry__c iq=new Inquiry__c(Lead__c = l.id,Product_Solution_of_Interest__c='Monitors',	 campaign__c = cmp.id);
    		   iqLst.add(iq);
	    }
	  insert iqLst;
	  LeadTriggerHandler.isFirstTime=true;
	  
	     
	     lead l=[select id,name,status,reason__c from lead limit 1];
	       l.Status='Invalid';
	       l.Reason__c='SPAM';
	       l.Sub_Status__c='Spam';
	       update l;
	       
         Test.stopTest();     
	    
	    
	}
	
		private static testMethod void HALeadAssignmenttestmethod() {
		      Test.startTest();
         string   CampaignTypeId = [Select Id From RecordType Where SobjectType = 'Campaign' and Name = 'Sales'  limit 1].Id;
	    string   LeadTypeId = [Select Id From RecordType Where SobjectType = 'Lead' and Name = 'HA'  limit 1].Id;
       Campaign cmp= new Campaign(Name='Test Campaign for test class ',	IsActive=true,type='Email',StartDate=system.Today(),recordtypeid=CampaignTypeId);
       insert cmp;
       
       
       HA_Lead_Assignment__c hl=new HA_Lead_Assignment__c(Name='1001',State__c='NJ',Zip_Postal_Code__c='07660',Account_Manager_ID__c=UserInfo.getUserId());
       insert hl;
    
      
       Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;
        
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            QueuesObject testQueue = new QueueSObject(QueueID = testGroup.id, SObjectType = 'Case');
            insert testQueue;
       
       List<Lead> leadLst = new list<Lead>();
       
       lead le1=new lead(lastname='test lead 123',Company='My Lead Company',Email='mylead@sds.com',	MQL_Score__c='T1',Status='New',LeadSource='Others',recordtypeid=LeadTypeId,PostalCode='07660',OwnerId=testGroup.Id);
	   leadLst.add(le1);
	    lead le2=new lead(lastname='test lead 1232',Company='My Lead Company2',Email='mylead@sds2.com',	MQL_Score__c='A+',Status='Hot',LeadSource='Others',recordtypeid=LeadTypeId,State='NJ',OwnerId=testGroup.Id);
	    leadLst.add(le2);
	    lead le3=new lead(lastname='test lead 1233',Company='My Lead Company3',Email='mylead@sds3.com',Status='Account / Oppty',Division__c='IT',ProductGroup__c='OFFICE',LeadSource='Others',recordtypeid=LeadTypeId,PostalCode='07670');
	   leadLst.add(le3);
	    insert leadLst;
        }
	        Test.stopTest();   
		}
		
}