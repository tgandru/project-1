/* 
Description: Handler class for Lead product trigger
Author: Jagan
1) Populate Lead__c field whenever Lead product is created with PRM Lead Id
2) Update Product_Information__c in lead to concatenate  Model_Code__c + ', ' + Quantity__c + ', ' + Requested_Price__c  of lead product
*/
public class LeadProductTriggerHandler{
    
    public Static boolean afterInsertCheck = FALSE;
    
    public static void beforeInsert(List<Lead_Product__c> triggerNew){
        set<String> prmldidset = new Set<String>();
        Map<String,Id> prmLeadMap = new Map<String,Id>();
        //Gather all PRM Lead ids
        for(Lead_Product__c lp:triggerNew){
            if(lp.PRM_Lead_Id__c != null && lp.PRM_Lead_Id__c.trim() != '') 
                prmldidset.add(lp.PRM_Lead_Id__c);
        }
        if(prmldidset.size()>0){
            List<Lead> lstLead = new List<Lead>();
            //query leads with the same PRM lead ids
            lstLead =[select id,PRM_Lead_Id__c from Lead where PRM_Lead_Id__c in: prmldidset];
            for(Lead l:lstLead)
                prmLeadMap.put(l.PRM_Lead_Id__c, l.id);
            
        }
        for(Lead_Product__c lp:triggerNew){
            //update lead products with the lead id
            if(lp.PRM_Lead_Id__c != null && lp.PRM_Lead_Id__c.trim() != '' && prmLeadMap.containsKey(lp.PRM_Lead_Id__c)){
                lp.Lead__c = prmLeadMap.get(lp.PRM_Lead_Id__c);
            }
        }
    }
    
    public static void afterInsertDelete(List<Lead_Product__c> triggerNew){
        if(!afterInsertCheck){
            //Prevent recursive update
            afterInsertCheck = TRUE;
            Set<Id> leadidset = new Set<Id>();
            for(Lead_Product__c lp:triggerNew){
                if(lp.Lead__c <> null)
                    leadidset.add(lp.Lead__c);
            }
            if(leadidset.size() > 0){
                Map<Id,List<Lead_Product__c>> leadProductMap = new Map<Id,List<Lead_Product__c>>();
                List<Lead_Product__c> allLeadProds = new List<Lead_Product__c>();
                List<Lead> leadtoUpdate = new List<Lead>();
                Set<Id> leadidstoupdate = new Set<Id>();
                //query all the lead products related to the leads int he current trigger
                allLeadProds = [select  id,Lead__c,Model_Code__c,Quantity__c,Requested_Price__c from Lead_Product__c where Lead__c in :leadidset];
                for(Lead_Product__c lp:allLeadProds){
                    if(leadProductMap.containsKey(lp.Lead__c))
                        leadProductMap.get(lp.Lead__c).add(lp);
                    else
                        leadProductMap.put(lp.Lead__c, new List<Lead_Product__c> {lp});
                }
                if(leadProductMap.size() > 0){
                    for(Id leadId:leadProductMap.keySet()){
                        String productInfo='';
                        
                        for(Lead_Product__c lp : leadProductMap.get(leadId)){
                                productInfo +=  '(' + lp.Model_Code__c + ', ' + lp.Quantity__c + ', ' + lp.Requested_Price__c + '),';
                        }
                        //Remove the comma at the end of the product info
                        if(productInfo.length() > 2)
                            productInfo=productInfo.subString(0,productInfo.length()-1);
                        Lead temp = new Lead(id=leadId);
                        temp.Product_Information__c = productInfo;
                        leadtoUpdate.add(temp);
                        //set of lead ids so that we can check which leads are part of update
                        leadidstoupdate.add(leadId);
                    }
                }
                //When a lead is not part of the update, then there are no lead products and product info should be cleared
                for(Id tmp:leadidset){
                    if(!leadidstoupdate.contains(tmp)){
                        Lead temp = new Lead(id=tmp);
                        temp.Product_Information__c = null;
                        leadtoUpdate.add(temp);
                    }
                }
                
                if(leadtoUpdate.size() > 0){
                    Database.SaveResult[] lsr =database.Update(leadtoUpdate,false);
                    for(Database.SaveResult sr : lsr)
                       if (!sr.isSuccess()) system.debug('Error occured during Update of leads....'+sr);
                    leadtoUpdate.clear();
                    leadidstoupdate.clear();
                }
            }
        }
    }
}