/**
  *   LeadPRoductTriggerHandlerTest
  *   @author: Kavitha Reddy
  **/
@isTest
private class LeadProductTriggerHandlerTest {
static Id endUserRT = TestDataUtility.retrieveRecordTypeId('End_User', 'Account'); 
static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
    
    @testSetup static void testData() {
        //Channelinsight configuration
		Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
	
        //Insert ZipCode
        Zipcode_Lookup__c zipCode = TestDataUtility.createZipcode();
        insert zipCode;

        //Creating New Lead
        Lead lead1 = new Lead(LastName='Leadx', FirstName='Testx', Company='Sample XYZ', Status='New', street='123 sample st',city='San Francisco', state='CA',Country='US',PostalCode='94102');    	
        insert lead1;
        
        Lead lead2 = new Lead(LastName='Leady', FirstName='Testy', Company='Sample ABC', Status='New', street='123 sample st',city='San Francisco', state='CA',Country='US',PostalCode='94102');    	
        insert lead2;
        
    }
    
    static testmethod void test_method_Insert() {
        //Channelinsight configuration
		Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
    	Lead leadRec = [Select Id,Product_Information__c,PRM_Lead_Id__c from Lead where firstname='Testx'];
        leadRec.PRM_Lead_Id__c='889988';
        update leadRec;
    	
         //Creating Lead Products
        Lead_Product__c lp1 = new Lead_Product__c(PRM_Lead_ID__c ='889988',Quantity__c=2.0,Requested_Price__c=18.0,Model_Code__c='a1b2c3',Item_No__c=34.30,ifid__c='TestPRM',ifdate__c=System.now());//Lead__c
       	test.startTest();
        	insert lp1;
        test.stopTest();
        Lead leadResult = [Select Id,Product_Information__c,PRM_Lead_Id__c from Lead where Id=:leadRec.Id];
        System.assert(leadResult.Product_Information__c!=null);
        
        Lead_Product__c lpResult = [Select Id,Lead__c from Lead_Product__c where Id=:lp1.Id];
        System.assertEquals(leadResult.Id,lpResult.Lead__c);
        
       // Lead_Product__c lp2 = new Lead_Product__c(PRM_Lead_ID__c ='90909',Quantity__c=2.0,Requested_Price__c=20.0,Model_Code__c='abc123',Item_No__c=44.44,ifid__c='TestPRM 2',ifdate__c=System.now());//Lead__c
       // insert lp2;
    }
        static testmethod void test_method_Delete() {
        //Channelinsight configuration
		Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
    	Lead leadRec = [Select Id,Product_Information__c,PRM_Lead_Id__c from Lead where firstname='Testx'];
        leadRec.PRM_Lead_Id__c='889988';
        update leadRec;
    	
         //Creating Lead Products
        Lead_Product__c lp1 = new Lead_Product__c(PRM_Lead_ID__c ='889988',Quantity__c=2.0,Requested_Price__c=18.0,Model_Code__c='a1b2c3',Item_No__c=34.30,ifid__c='TestPRM',ifdate__c=System.now());//Lead__c
        	insert lp1;

        Lead leadResult = [Select Id,Product_Information__c,PRM_Lead_Id__c from Lead where Id=:leadRec.Id];
        System.assert(leadResult.Product_Information__c!=null);
        
        Lead_Product__c lpResult = [Select Id,Lead__c from Lead_Product__c where Id=:lp1.Id];
        System.assertEquals(leadResult.Id,lpResult.Lead__c);
         
            test.startTest();
            	delete lp1;
            test.stopTest();

    }
}