/*
 * Batch class that associates Lead_Products with Leads through the PRM_Lead_Id field.
 * Author: Eric Vennaro
 * 7/7/2016 Lauren Callahan commented out as it is no longer used. 
**/

//global class LeadProductsBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {
global class LeadProductsBatch{
   /* global LeadProductsBatch() {

    } 

    global Database.QueryLocator start(Database.BatchableContext bc) {  
    System.debug('lclc LeadProductsBatch '+[SELECT PRM_Lead_Id__c, Lead__c, Requested_Price__c, Quantity__c, Model_Code__c 
                                         FROM Lead_Product__c 
                                         WHERE Lead__c = null]);   
        return Database.getQueryLocator([SELECT PRM_Lead_Id__c, Lead__c, Requested_Price__c, Quantity__c, Model_Code__c 
                                         FROM Lead_Product__c 
                                         WHERE Lead__c = null]);
    }

    global void execute(Database.BatchableContext BC, List<Lead_Product__c> scope) {
        LeadProductsIntegration leadProductsIntegration = new LeadProductsIntegration(scope);
        LeadProductsIntegration.LeadAndLeadProductWrapper leadAndLeadProductWrapper = leadProductsIntegration.processLeadsAndLeadProducts();

        try {
            upsert leadAndLeadProductWrapper.updatedLeads;
            upsert leadAndLeadProductWrapper.updatedLeadProducts;
        } catch(DMLException e) {
            Integer numErrors = e.getNumDml();
            System.debug('Number DML Errors: ' + numErrors);
            for(Integer i=0; i < numErrors; i++) {
                System.debug('Dml Field Names: ' + e.getDmlFieldNames(i));
                System.debug('Dml Message: ' + e.getDmlMessage(i));
            }
        } catch(Exception e) {
            System.debug('Exception occured updating LeadProducts: ' + e.getMessage());
        }
    }

    global void finish(Database.BatchableContext BC) {

    }*/
}