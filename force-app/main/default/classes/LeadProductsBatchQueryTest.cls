/* 7/7/2016 Lauren Callahan commented out as it is no longer used. */

@isTest
private class LeadProductsBatchQueryTest {
    /*private static String prmLeadId = '00000';
	private static Zipcode_Lookup__c zipRec = TestDataUtility.createZipcode();    
    
    @testSetup static void init() {
        insert zipRec;
        
        Lead lead = new Lead(PRM_Lead_Id__c = prmLeadId, LastName = 'Wright', Company = 'Ruthless Records',street='123 sample st',city='San Francisco',state='CA', Country='US',PostalCode='94102');
        insert lead;

        List<Lead_Product__c> lps=new List<Lead_Product__c>();
        Lead_Product__c leadProduct1 = new Lead_Product__c(
            PRM_Lead_Id__c = prmLeadId, 
            Model_Code__c = '000', 
            Quantity__c = 111, 
            Requested_Price__c = 222,
            Lead__c=null);
        lps.add(leadProduct1);

        Lead_Product__c leadProduct2 = new Lead_Product__c(
            PRM_Lead_Id__c = prmLeadId, 
            Model_Code__c = '000', 
            Quantity__c = 111, 
            Requested_Price__c = 222);
        lps.add(leadProduct2);

        Lead_Product__c leadProduct3 = new Lead_Product__c(
            PRM_Lead_Id__c = prmLeadId, 
            Model_Code__c = '000', 
            Quantity__c = 111, 
            Requested_Price__c = 222);
        lps.add(leadProduct3);

        insert lps;

        System.debug('lclc in test calss '+[SELECT PRM_Lead_Id__c, Lead__c, Requested_Price__c, Quantity__c, Model_Code__c 
                                         FROM Lead_Product__c ]);
    }


    @isTest
    static void testBatchQuery(){
        LeadProductsBatch bc = new LeadProductsBatch();
        
        Test.startTest();
        Database.executeBatch(bc);
        Test.stopTest();
        Integer count = [SELECT COUNT() FROM Lead_Product__c WHERE Lead__c = null];
        System.assertEquals(0, count);
    }

    //end to end of integration and batch - Integration related tests are in LeadProductsIntegrationTest.cls
    @isTest
    static void integrationAndBatch(){
        LeadProductsBatch bc = new LeadProductsBatch();
        Database.executeBatch(bc);
        String infoString = '(000, 111, 222.00),(000, 111, 222.00),(000, 111, 222.00)';
        List<Lead_Product__c> leadProducts = [SELECT PRM_Lead_Id__c,
                                                     Model_Code__c,
                                                     Quantity__c,
                                                     Requested_Price__c
                                              FROM Lead_Product__c];
        Test.startTest();
        LeadProductsIntegration integration = new LeadProductsIntegration(leadProducts);
        LeadProductsIntegration.LeadAndLeadProductWrapper wrapper = integration.processLeadsAndLeadProducts();
        List<Lead> updatedLeads = wrapper.updatedLeads;
        System.assertEquals(infoString, updatedLeads.get(0).Product_Information__c);
        for(Lead_Product__c leadProduct : wrapper.updatedLeadProducts) {
            System.assertEquals(leadProduct.Lead__c, wrapper.updatedLeads.get(0).Id);
        }
        Test.stopTest();

        
    }*/
}