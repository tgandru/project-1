/*
 * Receives batch results from LeadProductsBatch.cls and associates Lead Products with a Lead (Parent Child Relationship).
 * Author: Eric Vennaro
**/

public class LeadProductsIntegration {
	public class LeadAndLeadProductWrapper {
		public List<Lead> updatedLeads { get; set;}
		public List<Lead_Product__c> updatedLeadProducts { get; set;}
	}

	List<Lead_Product__c> leadProducts;
	
	public LeadProductsIntegration(List<Lead_Product__c> leadProducts) {
		this.leadProducts = leadProducts;
	}

	public LeadAndLeadProductWrapper processLeadsAndLeadProducts() {
		Map<Id, Lead_Product__c> updatedLeadProducts = new Map<Id, Lead_Product__c>();
        Map<Id, Lead> updatedLeads = new Map<Id, Lead>();
        List<Lead> leads = getLeads();
        String productInformation = '';
        for(Lead lead : leads) {
            for(Lead_Product__c leadProduct : leadProducts) {
                if(leadProduct.PRM_Lead_Id__c != null && leadProduct.PRM_Lead_Id__c.equals(lead.PRM_Lead_Id__c)) {
                    leadProduct.Lead__c = lead.ID;
                    productInformation += '(' + leadProduct.Model_Code__c + ', ' + leadProduct.Quantity__c + ', ' + leadProduct.Requested_Price__c + '),';
                }
                updatedLeadProducts.put(leadProduct.Id, leadProduct);
            }
            lead.Product_Information__c = productInformation.substring(0, productInformation.length()-1);
            updatedLeads.put(lead.Id, lead);
        }
        LeadAndLeadProductWrapper wrapper = new LeadAndLeadProductWrapper();

        wrapper.updatedLeads = updatedLeads.values();
        wrapper.updatedLeadProducts = updatedLeadProducts.values();

        return wrapper;
	}

	private List<Lead> getLeads() {
		Set<String> prmLeadIds = new Set<String>();
        for(Lead_Product__c leadProduct : leadProducts) {
            prmLeadIds.add(leadProduct.PRM_Lead_Id__c);
        }

        return [SELECT PRM_Lead_Id__c, 
                       Id, 
                       Product_Information__c 
                FROM Lead 
                WHERE PRM_Lead_Id__c IN :prmLeadIds];
	}
}