/*
 * Lead Products Integration Test Class
 * Author: Eric Vennaro
**/

@isTest
private class LeadProductsIntegrationTest {
	private static String prmLeadId = '00000';
	
	@testSetup static void init() {
        Zipcode_Lookup__c zipCode = TestDataUtility.createZipcode();
       insert zipCode;
        
        Lead lead = new Lead(PRM_Lead_Id__c = prmLeadId, LastName = 'Wright', Company = 'Ruthless Records');
        	lead.street='123 sample st';
            lead.city='San Francisco';
            lead.state='CA';
            lead.Country='US';
            lead.PostalCode='94102';
        	lead.status='Active';
	 	insert lead;
	}

	private static List<Lead_Product__c> getLeadProductList(){
		List<Lead_Product__c> leadProducts = new List<Lead_Product__c>();
		leadProducts.add(new Lead_Product__c(PRM_Lead_Id__c = prmLeadId));
	 	leadProducts.add(new Lead_Product__c(PRM_Lead_Id__c = prmLeadId));
	 	leadProducts.add(new Lead_Product__c(PRM_Lead_Id__c = prmLeadId));
	 	return leadProducts;
	}

	@isTest static void testIntegration() {
		List<Lead_Product__c> leadProducts = getLeadProductList();

		String modelCode = '000';
		Decimal quantity = 111;
		Decimal requestPrice = 222;
		
		String infoString = '';
		
		for(Lead_Product__c leadProduct : leadProducts){
			leadProduct.Model_Code__c = modelCode;
			leadProduct.Quantity__c = quantity;
			leadProduct.Requested_Price__c = requestPrice;
			infoString += '(' + modelCode + ', ' + String.valueOf(quantity) + ', ' + String.valueOf(requestPrice) + '),';
		}

		Test.startTest();
		LeadProductsIntegration integration = new LeadProductsIntegration(leadProducts);
		LeadProductsIntegration.LeadAndLeadProductWrapper wrapper = integration.processLeadsAndLeadProducts();
		List<Lead> updatedLeads = wrapper.updatedLeads;
		System.assertEquals(infoString.substring(0, infoString.length()-1), updatedLeads.get(0).Product_Information__c);
		for(Lead_Product__c leadProduct : wrapper.updatedLeadProducts) {
			System.assertEquals(leadProduct.Lead__c, wrapper.updatedLeads.get(0).Id);
		}
		Test.stopTest();
	}

	@isTest static void testWithNullLineItems() {
		List<Lead_Product__c> leadProducts = getLeadProductList();

		String modelCode = '000';
		Decimal quantity = 111;
		Decimal requestPrice = 222;
		String nullString = '(null, null, null),(null, null, null)';

		leadProducts.get(0).Model_Code__c = modelCode;
		leadProducts.get(0).Quantity__c = quantity;
		leadProducts.get(0).Requested_Price__c = requestPrice;
		String infoString = '(' + modelCode + ', ' + String.valueOf(quantity) + ', ' + String.valueOf(requestPrice) + '),' + nullString;
		

		Test.startTest();
		LeadProductsIntegration integration = new LeadProductsIntegration(leadProducts);
		LeadProductsIntegration.LeadAndLeadProductWrapper wrapper = integration.processLeadsAndLeadProducts();
		List<Lead> updatedLeads = wrapper.updatedLeads;
		System.assertEquals(infoString, updatedLeads.get(0).Product_Information__c);
		Test.stopTest();
	}

	@isTest static void testExtraneousLead() {
		List<Lead_Product__c> leadProducts = getLeadProductList();

         Zipcode_Lookup__c z_NJ=new Zipcode_Lookup__c(City_Name__c='RidgeField Park', Country_Code__c='US', State_Code__c='NJ', State_Name__c='New Jersey', Name='07660');
        insert z_NJ;
        Lead lead2 = new Lead(LastName = 'Wright1', Company = 'Ruthless 1Records');
        	lead2.street='123 sample st';
            lead2.city='RidgeField Park';
            lead2.state='NJ';
            lead2.Country='US';
            lead2.PostalCode='07660';
        	lead2.status='Active';
        try{
        insert lead2;
        }
        catch(Exception e){
            system.debug('## Dupe Lead'+e);
        }
		String modelCode = '000';
		Decimal quantity = 111;
		Decimal requestPrice = 222;
		String nullString = '(null, null, null),(null, null, null)';

		leadProducts.get(0).Model_Code__c = modelCode;
		leadProducts.get(0).Quantity__c = quantity;
		leadProducts.get(0).Requested_Price__c = requestPrice;
		String infoString = '(' + modelCode + ', ' + String.valueOf(quantity) + ', ' + String.valueOf(requestPrice) + '),' + nullString;

		Test.startTest();
		LeadProductsIntegration integration = new LeadProductsIntegration(leadProducts);
		LeadProductsIntegration.LeadAndLeadProductWrapper wrapper = integration.processLeadsAndLeadProducts();
		for(Lead lead : wrapper.updatedLeads){
			if(lead.Product_Information__c != null && lead.Product_Information__c != ''){
				System.assertEquals(infoString, lead.Product_Information__c);
			}
		}
		Test.stopTest();
	}

	@isTest static void testNullLeadPRMId() {
		Lead_Product__c leadProductNullPRMId = new Lead_Product__c();
		List<Lead_Product__c> leadProducts = getLeadProductList();
		String modelCode = '000';
		Decimal quantity = 111;
		Decimal requestPrice = 222;
		
		String infoString = '';
		
		for(Lead_Product__c leadProduct : leadProducts){
			leadProduct.Model_Code__c = modelCode;
			leadProduct.Quantity__c = quantity;
			leadProduct.Requested_Price__c = requestPrice;
			infoString += '(' + modelCode + ', ' + String.valueOf(quantity) + ', ' + String.valueOf(requestPrice) + '),';
		}

		leadProducts.add(leadProductNullPRMId);

		Test.startTest();
		LeadProductsIntegration integration = new LeadProductsIntegration(leadProducts);
		LeadProductsIntegration.LeadAndLeadProductWrapper wrapper = integration.processLeadsAndLeadProducts();
		List<Lead> updatedLeads = wrapper.updatedLeads;
		System.assertEquals(infoString.substring(0, infoString.length()-1), updatedLeads.get(0).Product_Information__c);
		for(Lead_Product__c leadProduct : wrapper.updatedLeadProducts) {
			if(leadProduct.Lead__c != null) {
				System.assertEquals(leadProduct.Lead__c, wrapper.updatedLeads.get(0).Id);
			}
		}
		Test.stopTest();
	}
}