/*
 * TODO: Add complete callout, work was switched from this integration to different piece of project...
 * Instantiated by LeadConvert.trigger, and checks what leads updated leads have been converted 
 * for the first time
 * Author: Eric Vennaro
**/

public class LeadTriggerHandler {
    private List<Lead> updatedLeads;
    private Map<Id, Lead> oldLeadsMap;
    public Set<Lead> prmLeadsSet;
    public List<message_queue__c> mqList;
   public static String enRT=Schema.SObjectType.Lead.getRecordTypeInfosByName().get('End User').getRecordTypeId();
   public static Boolean isFirstTime = true;
    public LeadTriggerHandler(List<Lead> updatedLeads, Map<Id, Lead> oldLeadsMap) {
        this.updatedLeads = updatedLeads;
        this.oldLeadsMap = oldLeadsMap;
        prmLeadsSet = new Set<Lead>();
        mqList = new List<message_queue__c>();
    }

    public void mapOpportunities() {
        Set<Id> opportunityIds = new Set<Id>();
        for(Lead lead : updatedLeads) {
            
            if(lead.Status!=null && lead.status !=oldLeadsMap.get(lead.Id).status){
                if(String.isNotBlank(lead.PRM_Lead_Id__c)){
                    prmLeadsSet.add(lead);
                }
            }
            
            if(!wasLeadConverted(lead.Id) && lead.isConverted) {
                opportunityIds.add(lead.ConvertedOpportunityId);
                
                }
            }
        
        if(prmLeadsSet.size()>0){
            for(Lead l: prmLeadsSet){
                message_queue__c mq = new message_queue__c(Integration_Flow_Type__c = 'Flow-016', retry_counter__c = 0, Identification_Text__c = l.Id, Status__c='not started',Object_Name__c='Lead');
                mq.IFID__c=(!Test.isRunningTest() && Integration_EndPoints__c.getInstance('Flow-016').layout_id__c!=null) ? Integration_EndPoints__c.getInstance('Flow-016').layout_id__c : '';
                mqList.add(mq);                
            }
            
            if(mqList.size()>0){
                insert mqList;
            }
            
            for(message_queue__c mqRec:[SELECT Id, MSGSTATUS__c,ERRORCODE__c,ERRORTEXT__c,MSGUID__c,IFID__c,IFDate__c,Status__c, retry_counter__c, Integration_Flow_Type__c, CreatedBy.Name, Identification_Text__c FROM message_queue__c where Id IN:mqList]){
                PRMLeadStatusHelper.callRoiEndpoint(mqRec);
            }
        }
    }
    
    public void validateAddress(){
        /*
     Map<string,List<Lead>> zipToLeadMap = new Map<string,List<Lead>>();
        Map<string,List<Zipcode_Lookup__c>> zipMap = new Map<string,List<Zipcode_Lookup__c>>();
        Map<String,Set<String>> zipToCity = new Map<String,Set<String>>();
        Map<String,String> zipToStateName = new Map<String,String>();
        Map<String,string> zipToStateCode = new Map<String,String>();
        Map<String,String> zipToCountryCode = new Map<String,String>(); 
        Boolean noCityMsg = false;
        Boolean hasStateMsg = false;
        Boolean hasCountryMsg = false;
        String tempCityMsg = '';
        for(Lead l : updatedLeads){
            if(string.isNotBlank(l.PostalCode)){ 
                if(!zipToLeadMap.containsKey(l.PostalCode)){
                    zipToLeadMap.put(l.PostalCode, new List<Lead>());
                }   
                zipToLeadMap.get(l.PostalCode).add(l);
            }
        }
        
        if(zipToLeadMap.size()>0){
            
            for(Zipcode_Lookup__c zipRec: [select Id,Name,City_Name__c, Country_Code__c, State_Code__c,State_Name__c from Zipcode_Lookup__c where Name IN: zipToLeadMap.keySet()]){               
               
                //Map to hold Zip to CityName 
                if(!zipToCity.containsKey(zipRec.Name)) {
                    zipToCity.put(zipRec.Name, new Set<String>());
                }               
                zipToCity.get(zipRec.Name).add(zipRec.City_Name__c);  
                
                //Map to hold Zip to StateName                 
                zipToStateName.put(zipRec.Name,zipRec.State_Name__c); 
                
                //Map to hold Zip to StateCode                  
                zipToStateCode.put(zipRec.Name,zipRec.State_Code__c); 
                
                //Map to hold Zip to Country                 
                zipToCountryCode.put(zipRec.Name,zipRec.Country_Code__c); 
                
                //Overall Zipcode map
                if(!zipMap.containsKey(zipRec.Name)){
                    zipMap.put(zipRec.Name, new List<Zipcode_Lookup__c>());
                }                  
                zipMap.get(zipRec.Name).add(zipRec);
            }
        }
        String msg = 'For entered Zipcode :';
        //for(String zip:zipToLeadMap.keySet()){
        for(Lead l : updatedLeads){    
            //Map<String, CountryTranslation__c> countryCodes = new Map<String, CountryTranslation__c>();
            //countryCodes =CountryTranslation__c.getAll();
            if(l.Country != 'CA' && l.Country != 'CAN' && l.Country != 'Canada' && String.isBlank(l.Jigsaw) && (l.Status=='Account Only')||(l.Status=='Account/Oppty')) {
            if( String.isNotBlank(l.PostalCode) && String.isNotBlank(l.Country) && String.isNotBlank(l.State) && String.isNotBlank(l.City)){
                if(!zipMap.containsKey(l.PostalCode)){
                    for(Lead leadRec: zipToLeadMap.get(l.PostalCode)){
                        leadRec.addError('Invalid Zipcode');
                    }
                }
                else {
                    for(Lead leadRecord: zipToLeadMap.get(l.PostalCode)){
                        if(zipToCountryCode.get(l.PostalCode) != leadRecord.Country){
                           msg+=' Country must be '+zipToCountryCode.get(l.PostalCode);
                           hasCountryMsg = true;
                        }
                        if(zipToStateCode.get(l.PostalCode) != leadRecord.State && zipToStateName.get(l.PostalCode) != leadRecord.State){
                            if(hasCountryMsg){
                                msg+=',';    
                            }
                            msg+=' State must be '+zipToStateCode.get(l.PostalCode);
                           hasStateMsg = true; 
                        }
                        if(zipToCity.containsKey(l.PostalCode)){
                            Integer i=0;
                            for(String tempCity : zipToCity.get(l.PostalCode)){
                                if(tempCity == leadRecord.City){
                                    noCityMsg = true;
                                    continue;
                                }
                                else {
                                    i+=1;
                                    if(i<=1)
                                        tempCityMsg+= tempCity;
                                    else
                                        tempCityMsg+=', '+tempCity;
                                }
                            }
                        }
                        if(noCityMsg == false){
                            if(hasStateMsg || hasCountryMsg)
                                msg+=', City must be '+tempCityMsg;
                            else
                                msg+=' City must be '+tempCityMsg;
                        }
                        if(msg.length()>21)leadRecord.addError(msg);
                    }
                } 
            }
            else {
                l.addError('Please populate complete Lead Address');
            }
        
            } 
            else if(String.isNotBlank(l.Jigsaw)){
                string trimPostal = l.PostalCode.trim();
                l.PostalCode = trimPostal.substring(0, 5);
                //l.Country = CountryTranslation__c.getInstance(l.Country).Actual_Value__c;                
                if(l.Country == 'United States' || l.Country=='USA'){
                    l.Country = 'US';
                }else if(l.Country == 'Canada' || l.Country=='CAN'){
                    l.Country = 'CA';
                }
               // l.Country = countryCodes.get(l.Country).Actual_Value__c;
                
            }
   
        }    */
    }
    
   
   Public Static void trinzipcode(list<lead> ld){
       for(lead l:ld){
           if(l.Postalcode !=null && l.RecordtypeId==enRT ){
               String s=String.valueOf(l.Postalcode);
               if(s.length()>5){
                   String zip=s.substring(0,5);
               l.Postalcode=zip;
               system.debug('************'+zip);
               }
               
           }
       }
   }
    public static  void linkconvertdetailstomql(List<lead> lst,Map<id,lead> mpold){
        system.debug('After Update ');
        
          list<Inquiry__c> inq =new list<Inquiry__c>() ;
        set<id> lid = new set<id>();
        set<id> invid = new set<id>();
        set<id> lidad = new set<id>();
        for(lead l:lst){
           if(l.isConverted==true){
               lid.add(l.id);
           } 
           if(l.status=='Invalid'){
               invid.add(l.id);
           } 
           
           lead ol=mpold.get(l.id);
           if((l.Street!=ol.Street || l.City !=ol.City  || l.Postalcode !=ol.Postalcode|| l.state !=ol.state|| l.Country !=ol.Country) && l.status !='Invalid'   ){
               lidad.add(l.id);
               
           }
        }
        if(lidad.size()>0){
            system.debug('***********'+lidad);
            List<Lead> addchan =[select id,Street,City,Postalcode,state,Country,(SELECT Id, name,By_Pass__c,Lead__c,Street__c, City__c,State__c,Country__c,Zip_Postal_Code__c FROM Inquiry__r) from Lead where id In :lidad];
            for(lead l:addchan){
                for(Inquiry__c i:l.Inquiry__r){
                    i.Street__c=l.street;
                    i.City__c=l.city;
                    i.state__c=l.state;
                    i.Zip_Postal_Code__c=l.Postalcode;
                    i.Country__c=l.country;
                    i.By_Pass__c=true;
                   inq.Add(i);
                }
            }
            
        }
        if(lid.size()>0){
           list<lead> converdlead=[select id,ConvertedAccountId,status,ConvertedContactId,ConvertedOpportunityId,(select id,name,Process_Convert__c,Account__c,Contact__c,Opportunity__c,Inquiry_Status__c,Lead__C from Inquiry__r) from lead where id in:lid];
            system.debug('************'+converdlead);
            for(lead l:converdlead){
                  for(Inquiry__c i:l.Inquiry__r){
                      if(l.ConvertedAccountId !=null){
                         i.Account__c =l.ConvertedAccountId;
                      i.Contact__c =l.ConvertedContactId;
                      i.Lead__C =null;   
                      }
                    
                     /* if(l.status=='Account / Oppty'){
                           //i.Inquiry_Status__c='Account/Opportunity';
                      }else if(l.status=='Account Only'&& i.Inquiry_Status__c!='Account Only'){
                          i.Inquiry_Status__c='Account Only';
                           i.Process_Convert__c='From Lead';
                         }
                      */
                     
                      inq.add(i);
                   }
           } 
        }
        
        if(invid.size()>0){
           list<lead> converdlead=[select id,status,Sub_Status__c,Reason__c,(select id,name,Account__c,Contact__c,Opportunity__c,Inquiry_Status__c,Sub_Status__c from Inquiry__r) from lead where id in:invid];
            for(lead l:converdlead){
                  for(Inquiry__c i:l.Inquiry__r){
                     if(i.Inquiry_Status__c=='New' ||i.Inquiry_Status__c=='Active' || i.Inquiry_Status__c=='In Pursuit'||i.Inquiry_Status__c=='Assigned'){
                         i.Inquiry_Status__c='Invalid Prospect';
                         i.Reason_invalid__c=l.Reason__c;
                          i.Sub_Status__c=l.Sub_Status__c;
                      
                      inq.add(i);
                     }
                      
                   }
           } 
        }
       if(inq.Size()>0){
           update inq;
         //database.update(inq,false);  
       }
      
    }

       /* CihStub.LeadOpportunityRequest wrapper = new CihStub.LeadOpportunityRequest();
        List<CihStub.ConvertedOpportunity> convertedOpportunities = new List<CihStub.ConvertedOpportunity>();
        for(Opportunity opportunity : getOpportunities(opportunityIds)) {
            CihStub.ConvertedOpportunity oppEntry = new CihStub.ConvertedOpportunity();
            oppEntry.id = String.valueOf(opportunity.Id);
            oppEntry.description = opportunity.description;
            oppEntry.carrierOpportunityNumber = opportunity.Carrier_OpportunityNumber__c;
            convertedOpportunities.add(oppEntry);
        }
        wrapper.inputHeaders = new CihStub.MsgHeadersRequest();
        wrapper.body = convertedOpportunities;*/
    

    private List<Opportunity> getOpportunities(Set<Id> opportunityIds) {
        return [SELECT Id, Description, Carrier_OpportunityNumber__c
                FROM Opportunity
                WHERE Id IN :opportunityIds];
    }

    private Boolean wasLeadConverted(Id id) {
        if(oldLeadsMap.get(id) != null) {
            return oldLeadsMap.get(id).isConverted;
        } else {
            return true;
        }
    }
    
    public Static void HALeadAssignment(List<Lead> triggernew){
       /*
         Added By - Vijay 6/7/2019
         Purpose - Assigning the Owner based Zip code for HA Division
      */
    
     List<HA_Lead_Assignment__c> ownermap=HA_Lead_Assignment__c.getall().values();// Fetch all custom Setting Values.
     Map<String,String> zipcodemap=new Map<String,String>();
      Map<String,Set<String>> statemap=new Map<String,Set<String>>();
        set<string> userids=new set<String>();
     for(HA_Lead_Assignment__c rec: ownermap){
         String zip=rec.Zip_Postal_Code__c.trim();
         string state=rec.state__c.trim();
         zipcodemap.put(zip,rec.Account_Manager_ID__c);
         userids.add(rec.Account_Manager_ID__c);
         if(statemap.containsKey(state)){
             Set<String> stlst=statemap.get(state);
             stlst.add(rec.Account_Manager_ID__c);
             statemap.put(state,stlst);
         }else{
             statemap.put(state,new set<String>{rec.Account_Manager_ID__c});
         }
         
     }
        Map<String,boolean> usermap=new map<string,boolean>();
        
        for(User u:[Select id,Name,IsActive from User where Id in:userids]){
            usermap.put(u.id,u.IsActive);
        }
        list<string> assignmentstatus = new list<string>();
        String status = Label.HA_Lead_Assignment_Status;
         if(String.isNotBlank(status))
                assignmentstatus = status.split(',');
        
      for(Lead l:triggernew){
           String ownerId=l.ownerId;
           System.debug(ownerId);
           if(ownerId.startsWith('00G') && assignmentstatus.contains(l.Status) ){
               
               try{
                   if(zipcodemap.containsKey(l.PostalCode)){
                       String owner=zipcodemap.get(l.PostalCode);
                       if(usermap.containsKey(owner)){
                           if(usermap.get(owner)){
                                l.ownerId=zipcodemap.get(l.PostalCode);
                           }
                       }
                    
                   }else if(statemap.containsKey(l.State)){
                        
                      Set<String> owners= statemap.get(l.State);
                       if(owners.size()==1){
                            System.debug('State Match:'+l.State+'--'+owners);
                      
                             String owner=new List<String> (owners).get(0);
                           if(usermap.containsKey(owner)){
                           if(usermap.get(owner)){
                                l.ownerId=new List<String> (owners).get(0);
                           }
                       }
                       }
                      
                       
                   }
                   
               }catch(exception ex){
                   System.debug('Exception Error::'+ex.getmessage());
               }
               
               
           }
           System.debug('Owne ID:'+l.OwnerId+ '-State:'+l.State+'-ZIP Code:'+l.PostalCode);
       }
      
       
     
    
    }
    
}