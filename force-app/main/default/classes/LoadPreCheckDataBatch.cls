global class LoadPreCheckDataBatch implements Database.Batchable<sObject>,Database.Stateful,Schedulable {
    
      global void execute(SchedulableContext SC) {
          Integer rqmi = 24;
    DateTime timenow = DateTime.now().addHours(rqmi);
    LoadPreCheckDataBatch rqm = new LoadPreCheckDataBatch();
    String seconds = '0';
    String minutes = String.valueOf(timenow.minute());
    String hours = String.valueOf(timenow.hour());
    String sch = seconds + ' ' + minutes + ' ' + hours + ' * * ?';
    String jobName = 'Sellout TEMP Data Copy scheduler - ' +timenow;
    system.schedule(jobName, sch, rqm);
          database.executeBatch(new LoadPreCheckDataBatch()) ;
          if (sc != null)  {  
      system.abortJob(sc.getTriggerId());      
    }
       
   }
    
    global LoadPreCheckDataBatch() {

    }
    
     global Database.QueryLocator start(Database.BatchableContext BC){
         return Database.getQueryLocator([select id,name,BillToCode__c,ModelCode__c,SelloutMonth__c,Version__c,ApplyYn__c,SapCompanyCode__c from Sellout_Pre_Check_Temporary__c where ApplyYn__c=false]);
     }
      global void execute(Database.BatchableContext BC,list<Sellout_Pre_Check_Temporary__c> scope){
          List<Sellout_Pre_Check_Data__c> upsertlst=new List<Sellout_Pre_Check_Data__c>(); 
          Map<String,String> codemap=new map<string,string>();
          List<Carrier_Sold_to_Mapping__c> crr=Carrier_Sold_to_Mapping__c.getall().values();
          for(Carrier_Sold_to_Mapping__c c:crr){
            codemap.put(c.Sold_To_Code__c,c.Carrier_Code__c);  
          }
        
          Map<string,List<Sellout_Pre_Check_Temporary__c>> tempDataMap=new Map<string,List<Sellout_Pre_Check_Temporary__c>>();
          for(Sellout_Pre_Check_Temporary__c s:scope){
              String key=s.Version__c+'-'+s.BillToCode__c+'-'+s.ModelCode__c+'-'+s.SelloutMonth__c;
             
              if(tempDataMap.containsKey(key)){
                    List<Sellout_Pre_Check_Temporary__c> ol = tempDataMap.get(key);
                            ol.add(s);
                           tempDataMap.put(key, ol);
                }else{
                    tempDataMap.put(key, new List<Sellout_Pre_Check_Temporary__c> { s });
                  
                }
              
          }
          Map<String,Sellout_Pre_Check_Data__c> pdMap=new  Map<String,Sellout_Pre_Check_Data__c>();
          for(Sellout_Pre_Check_Data__c s:[select id,name,Carrier__c,External_ID__c,Samsung_SKU__c,SAP_Company_Code__c,Sellout_Month__c,Sold_To_Code__c,Version__c,Temp_Data_Copy_External_ID__c from Sellout_Pre_Check_Data__c where Temp_Data_Copy_External_ID__c in :tempDataMap.keyset() ]){
             
             pdMap.put(s.Temp_Data_Copy_External_ID__c,s);
              
          }
        
          for(Sellout_Pre_Check_Temporary__c s:scope){
                String key=s.Version__c+'-'+s.BillToCode__c+'-'+s.ModelCode__c+'-'+s.SelloutMonth__c;
                if(pdMap.keyset().contains(key)){
                     Sellout_Pre_Check_Data__c spd=pdMap.get(key);
                     upsertlst.add(spd);
                }else{
                    Sellout_Pre_Check_Data__c sprd1=new Sellout_Pre_Check_Data__c();
                    sprd1.Samsung_SKU__c=s.ModelCode__c;
                    sprd1.SAP_Company_Code__c=s.SapCompanyCode__c;
                    sprd1.Sellout_Month__c=s.SelloutMonth__c;
                    sprd1.Sold_To_Code__c=s.BillToCode__c;
                    sprd1.Version__c=s.Version__c;
                    sprd1.Temp_Data_Copy_External_ID__c=key;
                    try{
                        sprd1.Carrier__c=codemap.get(s.BillToCode__c);
                    }catch(exception ex){
                        
                    }
                    upsertlst.add(sprd1);
                 }
            
          }
          List<string> sucesskeys=new list<string>();
          if(upsertlst.size()>0){
              
              Set<Sellout_Pre_Check_Data__c> unqset=new set<Sellout_Pre_Check_Data__c>();
              unqset.addAll(upsertlst);
              
              List<Sellout_Pre_Check_Data__c> finallst=new list<Sellout_Pre_Check_Data__c>();
              finallst.addAll(unqset);

              
              
              
             Database.UpsertResult[] sr = Database.upsert(finallst, false);
              for (Integer i=0; i< sr.size(); i++) {
                        if (sr[i].isSuccess()) {
                          sucesskeys.add(upsertlst[i].Temp_Data_Copy_External_ID__c);
                        }
                }
          } 
        list<Sellout_Pre_Check_Temporary__c> applylst=new list<Sellout_Pre_Check_Temporary__c>();  
          if(sucesskeys.size()>0){
              for(string  s:tempDataMap.keyset()){
                  if(sucesskeys.contains(s)){
                      List<Sellout_Pre_Check_Temporary__c> stemp=tempDataMap.get(s);
                      for(Sellout_Pre_Check_Temporary__c st:stemp){
                          
                       st.ApplyYn__c=True;
                      applylst.add(st);
                      }
                      
                  }
                  
              }
              
          }
          
          if(applylst.size()>0){
             update applylst; 
              
          }
          
        
      }
      
     global void finish(Database.BatchableContext BC){
         
     }
}