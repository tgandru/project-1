@isTest
private class LoadPreCheckDataBatchTest {

	private static testMethod void testmethod1() {
       Test.StartTest();
       list<Sellout_Pre_Check_Temporary__c> tempdata=new list<Sellout_Pre_Check_Temporary__c>();
       for(integer i=0;i<10;i++){
           Sellout_Pre_Check_Temporary__c sp=new Sellout_Pre_Check_Temporary__c(BillToCode__c='0001112223',
                                                                                ModelCode__c='Test- Model'+i,
                                                                                SapCompanyCode__c='C310',
                                                                                SelloutMonth__c='201810'
                                                                                        );
            tempdata.add(sp);
       }
       
       insert tempdata;
        Integer rqmi = 12;
    DateTime timenow = DateTime.now().addHours(rqmi);
    LoadPreCheckDataBatch rqm = new LoadPreCheckDataBatch();
    String seconds = '0';
    String minutes = String.valueOf(timenow.minute());
    String hours = String.valueOf(timenow.hour());
    String sch = seconds + ' ' + minutes + ' ' + hours + ' * * ?';
    String jobName = 'Sellout TEMP Data Copy scheduler - ' + hours + ':' + minutes;
    system.schedule(jobName, sch, rqm);
          database.executeBatch(new LoadPreCheckDataBatch()) ;
       database.executeBatch(new LoadPreCheckDataBatch());
       Test.StopTest();
	}

}