/* Controller of Lookup2 component*/
public class Lookup2 {

    @AuraEnabled(cacheable=true)
    public static String searchDB(String objectName, String fld_API_Text, String fld_API_Text2, String fld_API_Val, 
                                  Integer lim,String fld_API_Search,String fld_API_Search2,String searchText,String parentId){
        
        searchText='\'%' + String.escapeSingleQuotes(searchText.trim()) + '%\'';
        
        String query;
        if(objectName=='Opportunity' && parentId!=null){
            system.debug('AccntID------->'+parentId);
            query = 'SELECT '+fld_API_Text+' ,'+fld_API_Text2+' ,'+fld_API_Val+
            			' FROM '+objectName+
            				' WHERE ('+fld_API_Search+' LIKE '+searchText+' or '+fld_API_Search2+' LIKE '+searchText+') and id in (SELECT Opportunity__c FROM Partner__c where Partner__c=:parentId and Role_is_Reseller__c=true and Opportunity__c!=null) LIMIT '+lim;
        }else{ 
            query = 'SELECT '+fld_API_Text+' ,'+fld_API_Text2+' ,'+fld_API_Val+
            			' FROM '+objectName+
            				' WHERE '+fld_API_Search+' LIKE '+searchText+' or '+fld_API_Search2+' LIKE '+searchText+' LIMIT '+lim;
        }
        
        List<sObject> sobjList = Database.query(query);
        List<ResultWrapper> lstRet = new List<ResultWrapper>();
        
        for(SObject s : sobjList){
            ResultWrapper obj = new ResultWrapper();
            obj.objName = objectName;
            obj.text = String.valueOf(s.get(fld_API_Text)) ;
            if(fld_API_Text2=='Account.Name'){
                obj.text2 = (String)s.getSobject('Account').get('Name');
            }else{
                obj.text2 = String.valueOf(s.get(fld_API_Text2)) ;
            }
            obj.val = String.valueOf(s.get(fld_API_Val))  ;
            lstRet.add(obj);
        } 
         return JSON.serialize(lstRet) ;
    }
    
    public class ResultWrapper{
        public String objName {get;set;}
        public String text{get;set;}
        public String text2{get;set;}
        public String val{get;set;}
    }
}