public class LookupCustom {
    
    //with parent filter in lookup
    @AuraEnabled(cacheable=true)
    public static String searchDB(String objectName, String fld_API_Text, String fld_API_Val, 
                                  Integer lim,String fld_API_Search,String searchText, String parentField, String parentRecordId ){
        
        searchText='\'%' + String.escapeSingleQuotes(searchText.trim()) + '%\'';

        String query = 'SELECT '+fld_API_Text+' ,'+fld_API_Val+
            			' FROM '+objectName+
            				' WHERE ';
		system.debug('parentField---->'+parentField);
		system.debug('parentRecordId---->'+parentRecordId);
		if(parentField!=null && parentField!='' && parentRecordId!=null && parentRecordId!=''){
		    query = query + parentField+'=\''+parentRecordId+'\' and '+fld_API_Search+' LIKE '+searchText+ ' LIMIT '+lim;
		}else{
		    query = query + fld_API_Search+' LIKE '+searchText+' LIMIT '+lim;
		}
        system.debug('query--------------------->'+query);
        List<sObject> sobjList = Database.query(query);
        List<ResultWrapper> lstRet = new List<ResultWrapper>();
        
        for(SObject s : sobjList){
            ResultWrapper obj = new ResultWrapper();
            obj.objName = objectName;
            obj.text = String.valueOf(s.get(fld_API_Text)) ;
            obj.val = String.valueOf(s.get(fld_API_Val))  ;
            lstRet.add(obj);
        } 
         return JSON.serialize(lstRet) ;
    }
    
    public class ResultWrapper{
        public String objName {get;set;}
        public String text{get;set;}
        public String val{get;set;}
    }
}