public class MIWGeneratePDFExt {
    public Mobility_Innovation_Workshop__c MIWrecord{get;set;}
    public string MIWid;
    public MIWGeneratePDFExt(ApexPages.StandardController controller) {
        MIWid = ApexPages.currentpage().getParameters().get('id');
        MIWrecord  = [select id,name,ownerid,createddate,lastmodifieddate,Challenge_Statement__c,Co_Facilitator__c,Customer_Sponsor__c,Future_State_Experience__c,Future_State_Experience_2__c,Future_State_Experience_3__c,Big_Ideas__c,Lead_Facilitator__c,MAP_Summary__c,MIW_Session_Date__c,Pain_Points__c,Priority_Idea_1__c,Priority_Idea_2__c,Priority_Idea_3__c,Session_Length__c from Mobility_Innovation_Workshop__c where id=:MIWid];
    }

}