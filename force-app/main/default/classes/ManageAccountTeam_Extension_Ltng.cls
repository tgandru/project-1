public class ManageAccountTeam_Extension_Ltng {
@AuraEnabled
    public static List<AccountTeamMember> getTeamInformation (String  AccId) {
        list<AccountTeamMember> team=new List<AccountTeamMember>([SELECT AccountAccessLevel,AccountId,CaseAccessLevel,CreatedById,CreatedDate,Id,IsDeleted,
                    LastModifiedById,LastModifiedDate,OpportunityAccessLevel,PhotoUrl,SystemModstamp,TeamMemberRole,Title,
                    UserId,User.Name FROM AccountTeamMember where AccountId=:AccId]);
      
        return team;
    }
    
    @AuraEnabled
    public static List<User> getActiveUsers () {
        list<User> team=new List<User>([SELECT Id,Name from User where IsActive=true]);
      
        return team;
    }
    
     @AuraEnabled
     public static List <String> getselectOptionsNoSorting(sObject objObject, string fld) {
          system.debug('objObject --->' + objObject);
          system.debug('fld --->' + fld);
          List < String > allOpts = new list < String > ();
          // Get the object type of the SObject.
          Schema.sObjectType objType = objObject.getSObjectType();
         
          // Describe the SObject using its object type.
          Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
         
          // Get a map of fields for the SObject
          map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
         
          // Get the list of picklist values for this field.
          list < Schema.PicklistEntry > values =
          fieldMap.get(fld).getDescribe().getPickListValues();
         
          // Add these values to the selectoption list.
          for (Schema.PicklistEntry a: values) {
              
           allOpts.add(a.getValue());
          }
        
          return allOpts;
     }
    
     @AuraEnabled
    Public Static String UpdateTeamMenbers(List<AccountTeamMember> Insertlist) {
        String retrn='';
        System.debug('Insertlist::'+Insertlist);
        try{
            
            for(AccountTeamMember a:Insertlist){
                
               /* if(a.Id ==null &&(a.UserId == null || a.UserId =='')){
                    System.debug('******Empty User ID*********'+a);
                    return 'Please Select Team Member';
                }*/
                if(a.TeamMemberRole=='--- None ---' || a.TeamMemberRole=='' || a.TeamMemberRole ==null){
                    return 'Please Select Team Member Role';
                } 
            }
            Upsert Insertlist;
            retrn='Ok';
        }catch(exception ex){
            retrn=String.valueOf(ex.getMessage());
            if(retrn.contains('INACTIVE')){
                retrn='Please Select Valid ("Active") User';
            }
        }
        
        return retrn;
    }
    
    @AuraEnabled
    Public Static String DeleteTeamMenbers(List<AccountTeamMember> deletelist) {
          String retrn='';
        System.debug('deletelist::'+deletelist);
        try{
            delete deletelist;
            retrn='Ok';
        }catch(exception ex){
            retrn=String.valueOf(ex.getMessage());
        }
        
        return retrn;
    }
}