@isTest
private class ManageAccountTeam_Extension_Ltng_Test {

	private static testMethod void test() {
         Test.startTest();
         Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
          Account account = TestDataUtility.createAccount('Marshal Mathers');
          insert account;
          AccountTeamMember at=new AccountTeamMember();
             at.AccountId=account.id;
             at.AccountAccessLevel='Edit';
             at.userid=UserInfo.getUserId();
             at.TeamMemberRole='Technical Pre-Sales';
          insert at;

         List<AccountTeamMember> team=ManageAccountTeam_Extension_Ltng.getTeamInformation(account.Id);
         
         List<User> activeuser=ManageAccountTeam_Extension_Ltng.getActiveUsers();
         AccountTeamMember actteam=new AccountTeamMember();
            
         List<String> selectopts=ManageAccountTeam_Extension_Ltng.getselectOptionsNoSorting(actteam,'AccountAccessLevel');
         string upsertresult=ManageAccountTeam_Extension_Ltng.UpdateTeamMenbers(team);
                  String deleteresult=ManageAccountTeam_Extension_Ltng.DeleteTeamMenbers(team);
        Test.stopTest();    
       
	}
	
	private static testMethod void test2() {
         Test.startTest();
         Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
          Account account = TestDataUtility.createAccount('Marshal Mathers');
          insert account;
          AccountTeamMember at=new AccountTeamMember();
             at.AccountId=account.id;
             at.AccountAccessLevel='Edit';
             at.userid=UserInfo.getUserId();
          
          insert at;

         List<AccountTeamMember> team=ManageAccountTeam_Extension_Ltng.getTeamInformation(account.Id);
         
         List<User> activeuser=ManageAccountTeam_Extension_Ltng.getActiveUsers();
         AccountTeamMember actteam=new AccountTeamMember();
            
         List<String> selectopts=ManageAccountTeam_Extension_Ltng.getselectOptionsNoSorting(actteam,'AccountAccessLevel');
         string upsertresult=ManageAccountTeam_Extension_Ltng.UpdateTeamMenbers(team);
                  String deleteresult=ManageAccountTeam_Extension_Ltng.DeleteTeamMenbers(team);
        Test.stopTest();    
       
	}

}