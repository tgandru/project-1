public class ManageChannelRelationShipsExtension {
    public Channel__c objC {get;set;}
    public ManageChannelRelationShipsExtension(ApexPages.StandardController controller) {
        objC = (Channel__c) controller.getRecord(); 
        objC =[select id,Opportunity__c from Channel__c where id=:objC.id];
    }
    
    public PageReference redirect(){
        PageReference p=new PageReference('/apex/ChannelRelationships?id='+objC.opportunity__c+'&&retUrl='+objC.Opportunity__c);
        return p;
    }
}