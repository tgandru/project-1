@isTest
private class ManageChannelRelationShipsExtension_Test {

	
	 static Id endUserRT = TestDataUtility.retrieveRecordTypeId('End_User', 'Account');
    static Id inDirectRT = TestDataUtility.retrieveRecordTypeId('Indirect', 'Account');
    static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
    @testSetUp static void createTestData()
    {
        //Channelinsight configuration
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        
        //Creating test Account
        Account acct=TestDataUtility.createAccount('Main Account');
        acct.RecordTypeId = endUserRT;
        insert acct;
        
        
       //Creating the Parent Account for Distributor as Partners
        Account paDistAcct=TestDataUtility.createAccount('Distributor Account');
        paDistAcct.RecordTypeId = directRT;
        paDistAcct.Type= 'Distributor';
        insert paDistAcct;

        //Creating the Parent Account for Reseller as Partners
        Account paResellAcct=TestDataUtility.createAccount('Reseller Account');
        paResellAcct.RecordTypeId = inDirectRT;
        paResellAcct.Type = 'Corporate Reseller';
        insert paResellAcct;
        
        
         //Creating list of Opportunities
        List<Opportunity> opps=new List<Opportunity>();
        Opportunity oppOpen=TestDataUtility.createOppty('New Opp from Test setup',acct, null, 'RFP', 'IT', 'FAX/MFP', 'Identified');
        opps.add(oppOpen);
        insert opps;
    

        //Creating the partners
           Partner__c partner1 = TestDataUtility.createPartner(oppOpen,acct,paDistAcct,'Distributor');
           insert partner1 ;

           Partner__c partner2 = TestDataUtility.createPartner(oppOpen,acct,paResellAcct,'Corporate Reseller');
           insert partner2 ;

         // Create Channel
         List<Channel__c> channels = new List<Channel__c>();
         Channel__c channel = TestDataUtility.createChannel(oppOpen, paDistAcct, paResellAcct, partner1, partner2);
         channels.add(channel);
         insert channels;


    }
	
	
	private static testMethod void test() {
         Channel__c ch = [select Id,Opportunity__c from Channel__c  Limit 1];
         
         ManageChannelRelationShipsExtension channelController = new ManageChannelRelationShipsExtension(new ApexPages.StandardController(ch));
         channelController.redirect();
	}
	
		private static testMethod void test2() {
         Opportunity opp = [select Id from Opportunity Limit 1];
         
        
         
         String joinedString = '(\''+'Distributor'+'\''+')';
         
          List<Partner__c> patners= ManageChannelRelationshipsExt_Ltng.getPartnerList(opp.id,joinedString);
        
	}

}