@isTest
private class ManageChannelRelationshipControllerTest {
    static Account testMainAccount;
	static Account testCustomerAccount;
	static List<Opportunity> testOpportunityList;
	static Opportunity oppOpen;
	static Opportunity noPB;
	static Order orderRecord1;
	static Order orderRecord2;
	static Pricebook2 pb;
	static PricebookEntry standaloneProdPBE;
	static Quote quot;
	static Quote quot2;
	static Channel__c channel;
    static Id endUserRT = TestDataUtility.retrieveRecordTypeId('End_User', 'Account'); 
    static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
    static Id demoAcctRtId = TestDataUtility.retrieveRecordTypeId('ADL','Order');
    static Id demoOppRtId1 = TestDataUtility.retrieveRecordTypeId('Evaluation','Order');
    
    private static void setup() {
    	//Jagan: Test data for fiscalCalendar__c
        Test.loadData(fiscalCalendar__c.sObjectType, 'FiscalCalendarTestData');
        
        //Channelinsight configuration
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();

        Zipcode_Lookup__c zip=TestDataUtility.createZipcode();
        insert zip;
    	
    	//Creating test Account
        testMainAccount = TestDataUtility.createAccount(TestDataUtility.generateRandomString(5));
        testMainAccount.RecordTypeId = endUserRT;
        insert testMainAccount;
        
        testCustomerAccount = TestDataUtility.createAccount(TestDataUtility.generateRandomString(5));
        testCustomerAccount.RecordTypeId = directRT;
        insert testCustomerAccount;
		
		//Creating custom Pricebook
        pb = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(4)); 
        insert pb;
		
		testOpportunityList = new List<Opportunity>();
	        oppOpen=TestDataUtility.createOppty('Open Oppty',testMainAccount, pb, 'RFP', 'IT', 'HOSPITALITY_DISPLAY', 'Identified');
	        testOpportunityList.add(oppOpen);
	        
	        noPB = TestDataUtility.createOppty('NoPriceBook', testCustomerAccount, null, 'RFP', 'IT', 'FAX/MFP', 'Identified');
	        testOpportunityList.add(noPB);
		insert testOpportunityList;
		
		//Creating Products
        List<Product2> prods=new List<Product2>();
	        Product2 standaloneProd=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Standalone Prod1', 'Printer[C2]', 'HOSPITALITY_DISPLAY', 'H/W');
	        prods.add(standaloneProd);
	        Product2 parentProd1=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Parent Prod1', 'Printer[C2]', 'OFFICE', 'H/W');
	        prods.add(parentProd1);
	        Product2 childProd1=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod1', 'Printer[C2]', 'SMART_PHONE', 'Service pack');
	        prods.add(childProd1);
	        Product2 childProd2=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod2', 'Printer[C2]', 'SMART_PHONE', 'Service pack');
	        prods.add(childProd2);
	        Product2 parentProd2=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Parent Prod2', 'Printer[C2]', 'HOSPITALITY_DISPLAY', 'H/W');
	        prods.add(parentProd2);
	        Product2 childProd3=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod3', 'Printer[C2]', 'PRINTER', 'Service pack');
	        prods.add(childProd3);
	        Product2 childProd4=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod4', 'Printer[C2]', 'ACCESSARY', 'Service pack');
	        prods.add(childProd4);
        insert prods;
        
        List<PriceBookEntry> pbes=new List<PriceBookEntry>();
	        standaloneProdPBE=TestDataUtility.createPriceBookEntry(standaloneProd.Id, pb.Id);
	        standaloneProdPBE.Product2 = standaloneProd;
	        pbes.add(standaloneProdPBE);
	        PriceBookEntry parentProd1PBE=TestDataUtility.createPriceBookEntry(parentProd1.Id,pb.Id);
	        parentProd1PBE.Product2 = parentProd1;
	        pbes.add(parentProd1PBE);
	        PriceBookEntry childProd1PBE=TestDataUtility.createPriceBookEntry(childProd1.Id, pb.Id);
	        childProd1PBE.Product2 = childProd1;
	        pbes.add(childProd1PBE);
	        PriceBookEntry childProd2PBE=TestDataUtility.createPriceBookEntry(childProd2.Id, pb.Id);
	        childProd2PBE.Product2 = childProd2;
	        pbes.add(childProd2PBE);
	        PriceBookEntry parentProd2PBE=TestDataUtility.createPriceBookEntry(parentProd2.Id, pb.Id);
	        parentProd2PBE.Product2 = parentProd2;
	        pbes.add(parentProd2PBE);
        insert pbes;
        
        Contact contactRecord = TestDataUtility.createContact(testMainAccount);
        insert contactRecord;
        
        List<Order> orderList = new List<Order>();
	        orderRecord1 = TestDataUtility.createOrder(testMainAccount, oppOpen);
	        orderRecord1.RecordTypeId = demoAcctRtId;
	        orderRecord1.Pricebook2 = pb;
	        orderRecord1.Pricebook2Id = pb.Id;
	        orderRecord1.Product_Group__c = 'ACCESSARY; SMART_PHONE';
	        orderRecord1.CustomerAuthorizedById = contactRecord.Id;
	        orderRecord1.RequestedShipmentDate__c = Date.today();
	        OrderRecord1.EffectiveDate = Date.today();
	        orderRecord1.BusinessReason__c = 'test OrderRecord1';
	        orderRecord1.Type = 'Seed Unit';
	        orderList.add(orderRecord1);
	        
	        orderRecord2 = TestDataUtility.createOrder(testMainAccount, oppOpen);
	        orderRecord2.RecordTypeId = demoOppRtId1;
	        orderRecord2.Pricebook2=pb;
	        orderRecord2.Pricebook2Id = pb.Id;
	        orderRecord2.Product_Group__c = 'ACCESSARY; SMART_PHONE; OFFICE';
	        orderRecord2.CustomerAuthorizedById = contactRecord.Id;
	        orderRecord2.RequestedShipmentDate__c = Date.today();
	        orderRecord2.EffectiveDate = Date.today();
	        orderRecord2.BusinessReason__c = 'test OrderRecord2';
	        orderRecord2.Type = 'Seed Unit';
	        orderList.add(orderRecord2);
        insert orderList;
        
        List<OrderItem> orderItemList = new List<OrderItem>();
        	OrderItem orderItem1 = TestDataUtility.createOrderItem(standaloneProdPBE, orderRecord1, 9.0);
        	orderItemList.add(orderItem1);
        	
        	OrderItem orderItem2 = TestDataUtility.createOrderItem(standaloneProdPBE, orderRecord2, 10.0);
        	orderItemList.add(orderItem2);
        insert orderItemList;
        
        // Creating opportunity Line item
        List<OpportunityLineItem> olis=new List<OpportunityLineItem>();
        OpportunityLineItem standaloneProdOLI=TestDataUtility.createOpptyLineItem(standaloneProd, standaloneProdPBE, oppOpen);
        olis.add(standaloneProdOLI);
        OpportunityLineItem parentProd1OLI=TestDataUtility.createOpptyLineItem(parentProd1, parentProd1PBE, oppOpen);
        olis.add(parentProd1OLI);
        OpportunityLineItem childProd1OLI=TestDataUtility.createOpptyLineItem(childProd1, childProd1PBE, oppOpen);
        olis.add(childProd1OLI);
        OpportunityLineItem childProd2OLI=TestDataUtility.createOpptyLineItem(childProd2, childProd2PBE, oppOpen);
        olis.add(childProd2OLI);
        insert olis;
        
        ProductFamilyandGroupMap__c sapProductSetting = new ProductFamilyandGroupMap__c(
            Name = '1',
            Field_Name_1__c = 'SAP_Product__c',
            Field_Name_1_Value__c = 'HOSPITALITY_DISPLAY',
            Field_Name_2__c = 'SAP_Product_Group__c',
            Field_Name_2_Value__c = 'MONITOR',
            Product_Family__c = 'MONITOR [C1]',
            Product_Group__c = 'HOSPITALITY_DISPLAY'
        );
        
        ProductFamilyandGroupMap__c attrProductSetting = new ProductFamilyandGroupMap__c(
            Name = '10',
            Field_Name_1__c = 'ATTRIB06__c',
            Field_Name_1_Value__c = 'FEATURE',
            Field_Name_2__c = 'SAP_Product_Group__c',
            Field_Name_2_Value__c = 'MOBILE',
            Product_Family__c = 'MOBILE PHONE [G1]',
            Product_Group__c = 'FEATURE_PHONE'
        );
        insert new List<ProductFamilyandGroupMap__c>{sapProductSetting, attrProductSetting};

        quot = TestDataUtility.createQuote('Test Quote', oppOpen, pb);
        quot.ROI_Requestor_Id__c=UserInfo.getUserId();
        quot.Product_Division__c = 'MONITOR [C1]';
        quot.ProductGroupMSP__c = 'MOBILE PHONE [G1];MONITOR [C1];FEATURE_PHONE;HOSPITALITY_DISPLAY';
        insert quot;
        
        List<QuoteLineItem> qlis = new List<QuoteLineItem>();
        QuoteLineItem standardQLI = TestDataUtility.createQuoteLineItem(quot.Id, standaloneProd, standaloneProdPBE, standaloneProdOLI);
        standardQLI.Quantity = 25;
        standardQLI.UnitPrice = 100;
        standardQLI.Requested_Price__c=100;
        standardQLI.OLIID__c = standaloneProdOLI.id;
        qlis.add(standardQLI);
        insert qlis;
        
   		//Creating the partners
		Partner__c partner1 = TestDataUtility.createPartner(oppOpen, testCustomerAccount, testMainAccount,'Distributor');
		insert partner1 ;

		Partner__c partner2 = TestDataUtility.createPartner(oppOpen, testMainAccount, testCustomerAccount,'Corporate Reseller');
		insert partner2 ;

		// Create Channel
		List<Channel__c> channels = new List<Channel__c>();
		channel = TestDataUtility.createChannel(oppOpen, testMainAccount, testCustomerAccount, partner1, partner2);
		channels.add(channel);
		insert channels;
    }
    
    @isTest static void test_method_one() {
    	setup();
    	Test.StartTest();
    		PageReference pageRef = Page.ManageChannelRelationship;
            Test.setCurrentPage(pageRef);
    		ApexPages.currentPage().getParameters().put('id', channel.Id);
    		ApexPages.StandardController controller = new ApexPages.StandardController(channel);
			ManageChannelRelationshipController con = new ManageChannelRelationshipController(controller);
			con.redirect();
    	Test.StopTest();
		System.assertNotEquals(null, con.objC);
    }
    
}