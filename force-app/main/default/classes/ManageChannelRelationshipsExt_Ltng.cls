public class ManageChannelRelationshipsExt_Ltng {
    @AuraEnabled
    public static List<Partner__c> getPartnerList(String OppId, String rolesStr){
        if(OppId!=null && OppId!=''){
            List<Partner__c> Partners = new List<Partner__c>();
            String queryString = 'SELECT Role__c FROM Partner__c WHERE Role__c IN '+rolesStr+' AND Opportunity__c =:OppId';
            Partners = database.query(queryString);
            
            if(Partners.size()>0) 
                return Partners;
            else 
                return null;
        }else{
            return null;
        }
    }
}