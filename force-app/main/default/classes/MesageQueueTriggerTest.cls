@isTest
private class MesageQueueTriggerTest {

    public static testMethod void accountIdTest(){
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Account acc1 = new account(Name ='Test', Type='Customer',SAP_Company_Code__c = 'AAA');
        Account acc2 = new account(Name ='Test', Type='Customer',SAP_Company_Code__c = '873638');
        insert new List<Account>{acc1,acc2};
        
        message_queue__c mq1 = new  message_queue__c(Identification_Text__c =acc1.Id  , Integration_Flow_Type__c='003',Status__c='success' ,Object_Name__c = 'Account');
        message_queue__c mq2 = new  message_queue__c(Identification_Text__c =acc2.Id  , Integration_Flow_Type__c='003',Status__c='failed' ,Object_Name__c = 'Account' );
        insert new List<message_queue__c>{mq1,mq2};
        
    }
    public static testMethod void Testmethod2(){
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
         Account partnerAcc = TestDataUtility.createAccount('Distributor account');
        partnerAcc.RecordTypeId = directRT;
        insert partnerAcc;
         Credit_Memo__c cm = TestDataUtility.createCreditMemo();
        //cm.Credit_Memo_Number__c = TestDataUtility.generateRandomString(5);
        cm.InvoiceNumber__c = TestDataUtility.generateRandomString(6);
        cm.Direct_Partner__c = partnerAcc.id;
        cm.Division__c = 'Mobile';
        insert cm;
         message_queue__c mq1 = new  message_queue__c(Identification_Text__c =cm.Id  , Integration_Flow_Type__c='Flow-010',Status__c='success' ,Object_Name__c = 'Credit Memo');
         insert mq1;
         
         message_queue__c mq=mq1;
         mq.Status__c='failed-retry';
         update mq;
    }

}