global class NewAccountId {
   @AuraEnabled
    webservice static string updateAccountId(id accId){
       try{
           
           String  currentProf  =[Select Name from Profile where Id =: UserInfo.getProfileId() limit 1].Name;
            if(currentProf !='System Administrator'){
                
                  return 'Please Contact SFDC Admin Or Raise A Case.';
            }
           
           
           
            Account acc = new Account();
            acc = [select id, DUP_AFLAG__c,DUP_REMARK__c ,DUP_ADATE__c,DUP_AUSER__c,SAP_Company_Code__c,Duplicate_Code__c from Account where id=:accId limit 1];
            if(acc.SAP_Company_Code__c <> null){
                return 'This Account Already have Account ID';
           }
            acc.DUP_AUSER__c = userInfo.getUserName().substringBefore('@');
            acc.DUP_ADATE__c =  Datetime.now().format('yyyyMMdd');
            acc.DUP_REMARK__c = 'SFDC Id has been Created';
            acc.DUP_AFLAG__c = 'X';
            //update acc;
            list<message_queue__c> omqLst = new list<message_queue__c>([SELECT MSGSTATUS__c,  Status__c, retry_counter__c, Integration_Flow_Type__c, CreatedBy.Name, Identification_Text__c, ParentMQ__c,ParentMQ__r.status__c FROM message_queue__c where Identification_Text__c =:acc.Id  and Integration_Flow_Type__c='003' limit 1]);
            if(omqLst.size() > 0 ){
                if(!test.isRunningTest()){
                system.debug('omqLst'+omqLst);
                IntegrationUpdateAccountTriggerHelper.recieveInsertedAccts(omqLst[0],acc);
                 }
               }
             else{
                message_queue__c mqn=new message_queue__c(Identification_Text__c=accId, Integration_Flow_Type__c='003',retry_counter__c=0,Status__c='not started', Object_Name__c='Account');
                        mqn.IFID__c=(!Test.isRunningTest() && Integration_EndPoints__c.getInstance('Flow-003').layout_id__c!=null) ? Integration_EndPoints__c.getInstance('Flow-003').layout_id__c : '';
                 insert mqn;
                  formigratedaccounts(mqn.id,acc.id);//calling the future method to call the integration 
              }
            return 'Sucess/ submitted the request for new Account_Id .';
        }catch(exception ex){
            return 'Error occurred '+ex.getMessage();
        }
   }
    
    @future(callout=true)
    public static void formigratedaccounts(id mqi,id ac){
        message_queue__c omqLst1 = [SELECT MSGSTATUS__c,  Status__c, retry_counter__c, Integration_Flow_Type__c, CreatedBy.Name, Identification_Text__c, ParentMQ__c,ParentMQ__r.status__c FROM message_queue__c where id=:mqi];
         Account ac1 = [select id, DUP_AFLAG__c,DUP_REMARK__c ,DUP_ADATE__c,DUP_AUSER__c,SAP_Company_Code__c,Duplicate_Code__c from Account where id=:ac];
          IntegrationUpdateAccountTriggerHelper.recieveInsertedAccts(omqLst1,ac1);
    }
    
    
    public static Integer testmethodcoverage(){
        Integer x=1;
        X=1;
        X=1;
        X=1;
        X=1;
        X=1;
        X=1;
        X=1;
        X=1;
        X=1;
        return x;
    }
}