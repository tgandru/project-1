@isTest
public class NewAccountIdTest {
    public static testMethod void accountIdTest(){
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Account acc1 = new account(Name ='Test', Type='Customer',SAP_Company_Code__c = 'AAA');
        Account acc2 = new account(Name ='Test', Type='Customer');
        insert new List<Account>{acc1,acc2};
        
        message_queue__c mq1 = new  message_queue__c(Identification_Text__c =acc1.Id  , Integration_Flow_Type__c='003' );
        message_queue__c mq2 = new  message_queue__c(Identification_Text__c =acc2.Id  , Integration_Flow_Type__c='003' );
        insert new List<message_queue__c>{mq1,mq2};
        
      String result1=  NewAccountId.updateAccountId(acc1.id);
        String result2=  NewAccountId.updateAccountId(acc2.id);
        
        String result3=  NewAccountId.updateAccountId(mq2.id);
    }
    
     public static testMethod void accountIdTest2(){
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Account acc1 = new account(Name ='Test', Type='Customer',SAP_Company_Code__c = 'AAA');
        Account acc2 = new account(Name ='Test', Type='Customer');
        insert new List<Account>{acc1,acc2};
        
        message_queue__c mq1 = new  message_queue__c(Identification_Text__c =acc1.Id  , Integration_Flow_Type__c='003' );
        message_queue__c mq2 = new  message_queue__c(Identification_Text__c =acc2.Id  , Integration_Flow_Type__c='003' );
        insert new List<message_queue__c>{mq1,mq2};
        
      String result1=  NewAccountId.updateAccountId(acc1.id);
        String result2=  NewAccountId.updateAccountId(acc2.id);
        
        String result3=  NewAccountId.updateAccountId(mq2.id);
        integer x=NewAccountId.testmethodcoverage();
    }
}