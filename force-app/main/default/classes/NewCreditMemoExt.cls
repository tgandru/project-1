public class NewCreditMemoExt {
    
    public Credit_Memo__c currentCM{get;set;}
    public Id acctId {get;set;}
    public String companycode; //added by PM to update name and set assignment
    public String invoicenumber; //added by PM to update name and set assignment
    public String formattedDate; 
    public string soldToAccount;
    public string soldToValue{get;set;}

    public NewCreditMemoExt (ApexPages.StandardController stdcon) {
        currentCM = (Credit_Memo__c)stdcon.getRecord();
        acctId = currentCM.Direct_Partner__c;
        companycode= currentCM.Direct_Partner__r.SAP_Company_Code__c;
        soldToAccount= currentCM.SoldToAccount__r.Name;
        //invoicenumber = currentCM.InvoiceNumber__c;
        System.debug('##Current CM:   '+currentCM);               
 
    }
    
     public pageReference makingfield() {
        soldToAccount = Apexpages.currentPage().getParameters().get('param');
        System.debug('## 123 soldToAccount '+soldToAccount );
        return null;
     }
    
    public PageReference saveAndContinue(){
        try {
            formattedDate = processDate();
            System.debug('## formattedDate'+formattedDate);
            String acctName = [select Name from Account where Id =:acctId].Name;
            companycode = [select SAP_Company_Code__c from Account where Id =:acctId].SAP_Company_Code__c;
            //soldToAccount=soldToValue; 
            //soldToAccount = Apexpages.currentPage().getParameters().get('param');
            
            invoicenumber = currentCM.InvoiceNumber__c;            
            System.debug('## currentCM.SoldToAccount__r.Name'+currentCM.SoldToAccount__r.Name);

            // String tempName = acctName+'-'+companycode+'-'+invoicenumber;//updated by PM 5/6/16 
            soldToAccount =currentCM.SoldToAccount__r.Name;
            String tempAssign = soldToAccount+'-'+invoicenumber;//added by PM 5/6/16
            String tempName = acctName +'-'+tempAssign;
 
            System.debug('## tempAssign'+tempAssign); 
            System.debug('## tempName '+tempName); 
               
            if(tempName.length()>120){
               tempName = tempName.subString(0,120);
            }
            currentCM.Name = acctName+'-'+tempAssign;
            currentCM.Assignment_Number__c = tempAssign;

            
        insert currentCM;
        }
        catch(Exception e){

            //ApexPages.addMessages(e);
            PrintError('Duplicate record found with the same InvoiceNumber and SoldTo Account');
             return null;
        }
        
        PageReference cmDetailPage = new PageReference('/apex/CreditMemoAddQuotes?id=' + currentCM.Id);        
        //PageReference cmDetailPage = new PageReference('/' + currentCM.Id);        
        return cmDetailPage;
    }
    
    public PageReference cancel(){
        PageReference acctPage = new PageReference('/' + acctId);
        System.debug('redirect page: ' + acctPage);
        return acctPage; 
    }
    
     //This method will put an error into the PageMessages on the page
    public void PrintError(string error) {
        System.debug('## In print error :: '+error);
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, error,'');
        ApexPages.addMessage(msg);
    } 
    
    public string processDate(){
    Date dateToday = System.today();    
    String sMonth = String.valueof(dateToday.month());
    String sDay = String.valueof(dateToday.day());
    if(sMonth.length()==1){
      sMonth = '0' + sMonth;
    }
    if(sDay.length()==1){
      sDay = '0' + sDay;
    }
    String sToday = String.valueof(dateToday.year())+'/' + sMonth +'/' +sDay ;
    return sToday; 
    }

}