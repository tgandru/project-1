@isTest
private class NewCreditMemoExtTest {
	
	static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
	
	@isTest static void testCreateNewCreditMemo() {
		// Implement test code
		Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Account account = TestDataUtility.createAccount('Marshal Mathers');
        account.SAP_Company_Code__c=TestDataUtility.generateRandomString(12);
        insert account;
        Account partnerAcc = TestDataUtility.createAccount('Distributor account');
        partnerAcc.RecordTypeId = directRT;
        insert partnerAcc;
        PriceBook2 priceBook = TestDataUtility.createPriceBook('Shady Records');
        insert pricebook;
        Opportunity opportunity = TestDataUtility.createOppty('New', account, pricebook, 'Managed', 'IT', 'product group',
                                    'Identified');
        insert opportunity;
        Product2 product = TestDataUtility.createProduct2('SL-SCF5300/SEG', 'Standalone Printer', 'Printer[C2]', 'H/W', 'FAX/MFP');
        insert product;
        
        PricebookEntry pricebookEntry = TestDataUtility.createPriceBookEntry(product.Id, priceBook.Id);
        insert pricebookEntry;

        List<OpportunityLineItem> opportunityLineItems = new List<OpportunityLineItem>();
        OpportunityLineItem op1 = TestDataUtility.createOpptyLineItem(product, pricebookEntry, opportunity);
        op1.Quantity = 100;
        op1.TotalPrice = 1000;
        op1.Requested_Price__c = 100;
        op1.Claimed_Quantity__c = 45;     
        opportunityLineItems.add(op1);
        insert opportunityLineItems;

        Quote quot = TestDataUtility.createQuote('Test Quote', opportunity, pricebook);
        quot.ROI_Requestor_Id__c=UserInfo.getUserId();
        quot.Status='Test is Approved';
        insert quot;
        
        List<QuoteLineItem> qlis = new List<QuoteLineItem>();
        QuoteLineItem standardQLI = TestDataUtility.createQuoteLineItem(quot.Id, product, pricebookEntry, op1 );
        standardQLI.Quantity = 25;
        standardQLI.UnitPrice = 100;
        standardQLI.Requested_Price__c=100;
        standardQLI.OLIID__c = op1.id;
        qlis.add(standardQLI);
        insert qlis;


        Credit_Memo__c cm = TestDataUtility.createCreditMemo();
        cm.InvoiceNumber__c = TestDataUtility.generateRandomString(6);
        cm.Direct_Partner__c = partnerAcc.id;
        insert cm;

        NewCreditMemoExt newCreditMemoExtController = new NewCreditMemoExt(new ApexPages.StandardController(cm));
        newCreditMemoExtController.saveAndContinue();
        newCreditMemoExtController.cancel();
        newCreditMemoExtController.processDate();
        PageReference currentPageRef = Page.NewCreditMemo;
        newCreditMemoExtController.soldToAccount = 'test soldToAccount';
        currentPageRef.getParameters().put('param',newCreditMemoExtController.soldToAccount);
	    Test.setCurrentPage(currentPageRef);
        newCreditMemoExtController.makingfield();
        
	}
	
	
	
}