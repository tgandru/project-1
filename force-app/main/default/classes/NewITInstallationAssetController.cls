public class NewITInstallationAssetController {
    
    public Asset asset {get;set;}
    public Map<Id,Asset> assetMap {get;set;}
    public Opportunity opp {get;set;}
    public boolean assetCanBeCreated {get;set;}
    public Integer assetCount {get;set;}
    public static boolean testVar = true; //This variable is used to bypass all error causing if conditions for the test class: NewITInstallationAssetController_Test
    public static Id ITInstallationRecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('IT Installation').getRecordTypeId();
    
    public NewITInstallationAssetController(ApexPages.StandardController controller){
       List<String> installationskus=new List<String>();
           installationskus=Label.IT_Installation_SKU_Code.Split(';');
    
        opp = (Opportunity) controller.getRecord();
        opp = [SELECT Id, PartnerAccountId, StageName, AccountId, Name, Primary_Reseller__c, Primary_Reseller__r.Name, Pricebook2Id,(Select Id,SAP_Material_Id__c From OpportunityLineItems where SAP_Material_Id__c in:installationskus) FROM Opportunity WHERE Id=:opp.Id];
        assetMap = new Map<Id,Asset>([SELECT Id, Opportunity__c, reseller__c, Installation_SKU__c,Installation_Partner__c, 
                                      Project_Name__c,Environment__c,Project_Code__c, Buffer_Stock_qty__c, Installation_Start_Date__c,
                                      Installation_End_Date__c, Actual_Complete_Date__c, IT_Installation_Contact__c
                                      FROM Asset WHERE Opportunity__c =: opp.Id]);
        assetCount = assetMap.size() +1;
        assetCanBeCreated = true;
        //If the stage is not 'Qualified,' 'Proposal,' 'Commit,' or 'Win,' throw an error and don't show the rest of the form.
        if(opp.stagename == 'Identified' || opp.stagename == 'Drop' && testVar){
            assetCanBeCreated = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Cannot create IT Installation Asset for \'Identified\' or \'Drop\' Opportunity'));     
            return;
        } else if(Opp.OpportunityLineItems.size() <1 && testVar){
            assetCanBeCreated = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please Add Installation SKU into Opportunity before Creating the Asset.'));     
            return;
        }
        //One Opportunity cannot have more than one IT Installation Asset record.
        
        if(assetMap.size() > 0 && testVar) {
            assetCanBeCreated = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'An IT Installation Asset already exists for this opportunity.'));     
            return;
        } else{
            //Retrieves 'IT Installation' Record Type and assigns it to the newly created Asset object record.
            asset= new Asset();
            asset.recordtypeId = ITInstallationRecordTypeId;
            asset.Name = opp.name + ' Asset';
            asset.AccountId = opp.AccountId;
            asset.Opportunity__c = opp.Id;
            asset.reseller__c = opp.Primary_Reseller__r.Name;
            
            List<OpportunityLineItem> oli = [SELECT Id, Name, Product2.Name FROM OpportunityLineItem WHERE OpportunityId =: opp.Id AND Product2.SAP_Material_Id__c in: installationskus];
	  		System.debug('oli count ======> ' + oli);
            for(OpportunityLineItem ol : oli) {
                asset.Installation_SKU__c = ol.Product2.Name;
            }
            
            List<Partner__c> partners = [SELECT Id,Partner__c FROM Partner__c WHERE Opportunity__C =: opp.Id];
            Set<ID> partnerAccountIds = new Set<Id>();
            for(Partner__C p: partners) {
                partnerAccountIds.add(p.Partner__c);
            }

            List<Account> partnerAccounts = [SELECT Id, Type FROM Account WHERE ID in: partnerAccountIds];
            System.debug('partnerAccounts ======>' + partnerAccounts);
            //Account primaryResellerAccount = [SELECT Id,Type FROM Account WHERE Id =: opp.AccountId];
            
            for(Account primaryResellerAccount : partnerAccounts){
                if(primaryResellerAccount.Type == 'Reseller' && opp.Primary_Reseller__c != null ) {
                    asset.Installation_Partner__c = opp.Primary_Reseller__c; 
                } else if(primaryResellerAccount.Type == 'Installation Partner' && opp.Primary_Reseller__c != null) {
                    asset.Installation_Partner__c = primaryResellerAccount.Id;
                }
            }
        }
    }
    
    
    public pageReference save() {
        try{
            insert asset;
            sendEmailToTheCreator();
            return new pageReference('/'+asset.id);
        } catch(Exception e) {
            ApexPages.addMessage(New ApexPages.Message(ApexPages.Severity.Error,''+e));
            return null;
        }
    }
    
    public void sendEmailToTheCreator() {
        try{
            Asset assetForEmail = [SELECT Id,Name, CreatedDate, CreatedById FROM Asset WHERE ID =: asset.id];
            User creator = [SELECT firstName, lastName FROM User WHERE Id =: assetForEmail.CreatedById];
            //User user = [SELECT Email, firstName, lastName FROM User Where ID =: Userinfo.getUserId()];
            //List<String> toEmails = new List<String>();
            //toEmails.add(user.email);
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTargetObjectId(Userinfo.getUserId());
            //mail.setToAddresses(toEmails);
            mail.setSaveAsActivity(false);
            String subject = 'A new IT Installation Project: ' + assetForEmail.Name + ' has been created';
            mail.setSubject(subject);
            String body = 'A new IT Installation Project: '+ assetForEmail.Name + ' has been created with the following details: <br/>';
            body += 'Created Date: ' + assetForEmail.CreatedDate + '<br/>';
            body += 'Created By: ' + creator.firstName + ' ' + creator.lastName + '<br/>';
                        body += 'Department: ' + User.Sales_Team__c + '<br/>';
            body += '<br/>';
            body += 'You can access it by clicking on this link: <br/>';
            String objectlink = URL.getSalesforceBaseUrl().toExternalForm()+'/'+ assetForEmail.id;
            body += '<a href='+ objectlink +'>' + objectlink + '</a>';
            mail.setHtmlBody(body);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        } catch(Exception e) {
            ApexPages.addMessage(New ApexPages.Message(ApexPages.Severity.Error,''+e));
        } 
    }
 }