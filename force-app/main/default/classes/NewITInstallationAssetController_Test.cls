@isTest
public class NewITInstallationAssetController_Test {
    static Id endUserRT = TestDataUtility.retrieveRecordTypeId('End_User', 'Account'); 
    static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
    static Id HAaccRT = TestDataUtility.retrieveRecordTypeId('HA_Builder','Account');
    static Id ITRT = TestDataUtility.retrieveRecordTypeId('IT', 'Opportunity');
    static Id HART = TestDataUtility.retrieveRecordTypeId('HA_Builder', 'Opportunity');
    static Id HAQuoteRT = TestDataUtility.retrieveRecordTypeId('HA_Builder', 'SBQQ__Quote__c');
    static Opportunity oppForAsset;
    @isTest(seeAllData=true) static void testsetup()
    {
        //Channelinsight configuration
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();

        /*
         * Data Creation for IT Installation.
         */
    	Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }
        Account testAccount1 = new Account (
          	Name = 'testAccount1',
            RecordTypeId = rtMap.get('Account' + 'End_User'),
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        
        );
        insert testAccount1;
        
        Account installationPartnerAccount = new Account (
          	Name = 'installationPartnerAccount',
            RecordTypeId = rtMap.get('Account' + 'Indirect'),
            Type = 'Installation Partner',
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        );
        insert installationPartnerAccount;
        
        Account distributorAccount = new Account (
          	Name = 'DistributorAccount',
            RecordTypeId = rtMap.get('Account' + 'Indirect'),
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        );
        insert distributorAccount;
        
        PriceBook2 pb_IT_Installation = TestDataUtility.createPriceBook('IT Distributor'); 
        pb_IT_Installation.Division__c='IT';
        pb_IT_Installation.SAP_Price_Account_Code__c='1234500';
        pb_IT_Installation.SAP_Price_Group_Code__c = '9999';
        insert pb_IT_Installation;

        Opportunity testOpp1 = new Opportunity (
          Name = 'testOpp1',
          RecordTypeId = rtMap.get('Opportunity' + 'IT'),
          //Project_Type__c = 'SFNC',
          Project_Name__c = 'TestOpp1',
          AccountId = testAccount1.Id,
          StageName = 'Identified',
          CloseDate = Date.today(),
          Roll_Out_Start__c = Date.today(),
          Rollout_Duration__c = 5,
          Number_of_Living_Units__c = 10000,
          Division__c = 'IT',
          ProductGroupTest__c = 'SMART_SIGNAGE',
          Pricebook2Id = pb_IT_Installation.Id
        );
        insert testOpp1;
        
        Test.StartTest();
        oppForAsset = [SELECT ID, StageName, Primary_Distributor__c,Primary_Reseller__c, Roll_Out_Start__c, Rollout_Duration__c,AccountId From Opportunity WHERE ID =: testOpp1.Id];
       	
        
        Partner__c PrimaryDistributor = new Partner__c();
        PrimaryDistributor.Opportunity__c = oppForAsset.Id;
        PrimaryDistributor.Partner__C = installationPartnerAccount.Id;
        PrimaryDistributor.Is_Primary__c = true;
        PrimaryDistributor.Role__c = 'Distributor';
        insert PrimaryDistributor;

        oppForAsset.Primary_Distributor__c = PrimaryDistributor.Partner__C;
        
        
        Partner__c PrimaryReseller = new Partner__c();
        PrimaryReseller.Opportunity__c = oppForAsset.Id;
        PrimaryReseller.Partner__C = installationPartnerAccount.Id;
        PrimaryReseller.Is_Primary__c = true;
        PrimaryReseller.Role__c = 'Reseller';
        insert PrimaryReseller;
        
        oppForAsset.Primary_Reseller__C = PrimaryReseller.Partner__C;
        
        List<String> installationskus=new List<String>();
        installationskus=Label.IT_Installation_SKU_Code.Split(';');
        System.debug('Installationskus ====> '+installationskus);
        
        Product2 standaloneProd_IT_Installation = [SELECT Id, Description, Unit_Cost__c, Product_Group__c,Product_Type__c FROM Product2 WHERE SAP_Material_ID__C in: installationskus AND NAme ='DC32E'];
        //Product2 standaloneProd_IT_Installation =TestDataUtility.createProduct2('LH32DCEPLGA/GO','DC32E', 'MONITOR [C1]', 'SMART_SIGNAGE', 'SET');
        //insert standaloneProd_IT_Installation;
        System.debug('JoeTest standaloneProd ====> '+standaloneProd_IT_Installation);
        //create the pricebook entries for the custom pricebook
        PriceBookEntry standaloneProdPBE_IT_Installation=TestDataUtility.createPriceBookEntry(standaloneProd_IT_Installation.Id, pb_IT_Installation.Id);
        insert standaloneProdPBE_IT_Installation;

        standaloneProdPBE_IT_Installation = [SELECT ID,Name,UnitPrice FROM PriceBookEntry WHERE ID =: standaloneProdPBE_IT_Installation.Id];
        System.debug('PriceBookEntry ===> ' + standaloneProdPBE_IT_Installation.Name);
        
        //Creating opportunity Line Item
        OpportunityLineItem standaloneProdOLI_IT_Installation=TestDataUtility.createOpptyLineItem(standaloneProd_IT_Installation, standaloneProdPBE_IT_Installation, oppForAsset);
        standaloneProdOLI_IT_Installation.Quantity = 10;
        standaloneProdOLI_IT_Installation.TotalPrice = standaloneProdOLI_IT_Installation.Quantity * standaloneProdPBE_IT_Installation.UnitPrice;
        insert standaloneProdOLI_IT_Installation;
        standaloneProdOLI_IT_Installation = [SELECT ID,Name, Opportunity.Name FROM OpportunityLineItem WHERE ID =: standaloneProdOLI_IT_Installation.Id];
        System.debug('JoeTest Opportunity ===> ' + OppForAsset.id);
        System.debug('JoeTest OpportunityLineItem ===> ' + standaloneProdOLI_IT_Installation);
        
	    
        System.debug('oppForAsset ====> '+oppForAsset);
        oppForAsset.StageName ='Qualified';
        
        System.debug('JoeTest Opportunity StageName ===> ' + oppForAsset.StageName);
        System.debug('JoeTest Opportunity Roll_Out_Start__c ===> ' + oppForAsset.Roll_Out_Start__c);
        System.debug('JoeTest Opportunity Rollout_Duration__c ===> ' + oppForAsset.Rollout_Duration__c);
        System.debug('JoeTest Opportunity Primary_Distributor__c ===> ' + oppForAsset.Primary_Distributor__c);
        System.debug('JoeTest Opportunity Primary_Reseller__C ===> ' + oppForAsset.Primary_Reseller__C);
        update oppForAsset;
        
        oppForAsset = [SELECT Id, PartnerAccountId, StageName, AccountId, Name, Primary_Reseller__c,Pricebook2Id,(Select Id,SAP_Material_Id__c From OpportunityLineItems where SAP_Material_Id__c in:installationskus) FROM Opportunity WHERE ID =: oppForAsset.Id];
        System.debug('JoeTest oppForAsset =====> '+oppForAsset);
        
        	 //Opportunity testOpp1 = [Select ID, StageName, Primary_Distributor__c,Primary_Reseller__c, Roll_Out_Start__c, Rollout_Duration__c, AccountId FROM Opportunity WHERE Id =:oppForAsset.Id ]; //where SAP_Material_Id__c ='LH32DCEPLGA/GO'
        	 System.debug('JoeTest testOpp1 =====> '+oppForAsset);
             ApexPages.StandardController sc = new ApexPages.StandardController(oppForAsset);
        	 NewITInstallationAssetController.testVar = false;	
             NewITInstallationAssetController newITInstallerAssetController = new NewITInstallationAssetController(sc);
        	 newITInstallerAssetController.opp = oppForAsset;

             //System.assertEquals(0, newITInstallerAssetController.assetMap.size());
             Asset asset = new Asset();
        	 asset.name = 'asset name test jooe';
             asset.AccountId = oppForAsset.AccountId;
        	 asset.Opportunity__c = oppForAsset.Id;
        	 asset.Project_Name__c ='abc';
             asset.Installation_Partner__C = oppForAsset.Primary_Reseller__c;
        	 asset.Installation_Start_Date__c = date.today();
        	 asset.Installation_End_Date__c = date.today();
        	 newITInstallerAssetController.asset = asset;
        	 newITInstallerAssetController.save();
        	 newITInstallerAssetController.sendEmailToTheCreator();
    }
}