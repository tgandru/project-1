public with sharing class NewOpportunity {
    
    public Opportunity currentOppty{get;set;}
    public Id acctId {get;set;}
    public String formattedDate; 
    public string retUrl;
    public string tempString;
    public string tempOpp;
    public string acctRecordtypeId;
    public Opportunity accountInfo;
    public Boolean endOrHTVRecType;
    public String typeValue {get; set;}
    public string recordtypename {get; set;}
    
    public NewOpportunity (ApexPages.StandardController stdcon) {
        
        currentOppty = (Opportunity)stdcon.getRecord();
         acctId = currentOppty.AccountId;
         endOrHTVRecType = false;
         
         //Added by Thiru -- Starts
         IF(currentOppty.recordtypeid != Null)
         {
         recordtypename = Schema.SObjectType.Opportunity.getRecordTypeInfosById().get(currentOppty.recordtypeid).getname();
         }
         Else
         {
            Schema.DescribeSObjectResult dsr = Opportunity.SObjectType.getDescribe();
            Schema.RecordTypeInfo defaultRecordType;
            for(Schema.RecordTypeInfo rti : dsr.getRecordTypeInfos()) {
                if(rti.isDefaultRecordTypeMapping()) {
                    defaultRecordType = rti;
                }
            }
        System.debug('defaultid'+defaultRecordType.getname());
        recordtypename = defaultRecordType.getname();
        }
        //Added by Thiru -- Ends
        
        if(currentOppty.AccountId!=null){
            acctId = currentOppty.AccountId;
            Account acc = [select Id,RecordTypeId from Account where Id=:acctId];
            acctRecordtypeId = acc.RecordTypeId;
            System.debug('## acctId'+acctId+'Record Type'+acctRecordtypeId);
        }
        else{
             retUrl=ApexPages.currentPage().getParameters().get('retUrl');
             tempString = retUrl.substringAfter('=');
             tempOpp = tempString.substring(0,15);
             accountInfo = [select Id,AccountId,Account.RecordTypeId from Opportunity where Id =:tempOpp];
             acctId = accountInfo.AccountId;
             acctRecordtypeId = accountInfo.RecordTypeId;
             System.debug('## acctId'+acctId+'Record Type'+acctRecordtypeId);
        }
        System.debug('##Current Oppty:   '+currentOppty);
        
        Map<String,Schema.RecordTypeInfo> acctRTMapByName = Schema.Sobjecttype.Account.getRecordTypeInfosByName();
        if(acctRecordtypeId == acctRTMapByName.get(Label.AccountEndCustomerRecordType).getRecordTypeId() || acctRecordtypeId == acctRTMapByName.get(Label.AccountHTVRecordType).getRecordTypeId()){
            endOrHTVRecType = true; //Set the value of Type toTender
        }
        else {
            endOrHTVRecType = false; //else to Internal or Blanket
        }
        if(currentOppty.type!=null){
            typeValue = currentOppty.type;
        }
    }
    
    //Custom Type picklist values
    public List<SelectOption> gettypeSelectList() {
       List<SelectOption> options = new List<SelectOption>();
        
        if (endOrHTVRecType){
            options.add(new SelectOption('Tender','Tender'));
        }
        else{
            options.add(new SelectOption('', '-- Select Type --'));
            options.add(new SelectOption('Internal','Internal'));
            options.add(new SelectOption('Blanket','Blanket'));
        }
        system.debug('## Type picklist values ::'+ options);
        return options;
       }
    
    public PageReference saveButton(){
        
        try{
            formattedDate = processDate();
            System.debug('## formattedDate'+formattedDate); 
            String acctName = [select Name,RecordType.Name from Account where Id =:acctId].Name;
            String tempName = acctName+' '+formattedDate+'-'+currentOppty.Division__c;
            if(String.isNotEmpty(currentOppty.Reference__c)){
                tempName= tempName+'-'+currentOppty.Reference__c;
            }
            if(tempName.length()>120){
            tempName = tempName.subString(0,120);
            }
            currentOppty.Name = tempName;
            currentOppty.StageName = 'Identified';
            currentOppty.type = typeValue;
        
        insert currentOppty;
        }
        catch(Exception ex){
            ApexPages.addMessages(ex);
            return null;
        }
        PageReference opptyPage = new PageReference('/'+currentOppty.Id);
        opptyPage.setRedirect(true);
        return opptyPage;
    }
    
    public PageReference saveAndContinue(){
        try {
            formattedDate = processDate();
            System.debug('## formattedDate'+formattedDate);
            String acctName = [select Name from Account where Id =:acctId].Name;
            String tempName = acctName+' '+formattedDate+'-'+currentOppty.Division__c;
            if(String.isNotEmpty(currentOppty.Reference__c)){
                tempName= tempName+'-'+currentOppty.Reference__c;
            }
            if(tempName.length()>120){
            tempName = tempName.subString(0,120);
            }
            currentOppty.Name = tempName;
            currentOppty.StageName = 'Identified';
            currentOppty.type = typeValue;
        
        insert currentOppty;
        }
        catch(Exception ex){
            ApexPages.addMessages(ex);
            System.debug('## ex KRKR'+ex);
            return null;
        }
        PageReference opptyDetailPage = new PageReference('/apex/NewOpportunityExtn?id=' + currentOppty.Id);        
        return opptyDetailPage;
    }
    
    public PageReference cancel(){
        PageReference acctPage = new PageReference('/' + acctId);
        System.debug('redirect page: ' + acctPage);
        return acctPage; 
    }
    
    public string processDate(){
    Date dateToday = System.today();    
    String sMonth = String.valueof(dateToday.month());
    String sDay = String.valueof(dateToday.day());
    if(sMonth.length()==1){
      sMonth = '0' + sMonth;
    }
    if(sDay.length()==1){
      sDay = '0' + sDay;
    }
    String sToday = String.valueof(dateToday.year())+'/' + sMonth +'/' +sDay ;
    return sToday; 
    }

}