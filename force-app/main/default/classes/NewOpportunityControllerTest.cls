// NewOpportunityControllerTest
// Class to test New Opportunity page
// ------------------------------------------------------------------
//  Author          Date        Description
// ------------------------------------------------------------------
// Kavitha M      03/10/2016    Created
//
@isTest
public  class NewOpportunityControllerTest {  
   
    /*static Map <String,Schema.RecordTypeInfo> accountRecordTypes = Account.sObjectType.getDescribe().getRecordTypeInfosByName();
    static Id endUserRT = accountRecordTypes .get('End Customer').getRecordTypeId();    
    static Map <String,Schema.RecordTypeInfo> opptyRecordTypes = Opportunity.sObjectType.getDescribe().getRecordTypeInfosByName();
    static Id carrierRT = opptyRecordTypes .get('Carrier').getRecordTypeId();*/
    
    static Id endUserRT = TestDataUtility.retrieveRecordTypeId('End_User', 'Account'); 
    static Id inDirectRT = TestDataUtility.retrieveRecordTypeId('Indirect','Account');
    static Id mobileOppRT = TestDataUtility.retrieveRecordTypeId('Mobile', 'Opportunity'); 
    static Id ITOppRT = TestDataUtility.retrieveRecordTypeId('IT', 'Opportunity'); 
    static Id servicesOppRT = TestDataUtility.retrieveRecordTypeId('Services', 'Opportunity'); 
    
     static testMethod void opportunityCreationonSaveTest(){
    
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
         
        Account endCustomertAcct = TestDataUtility.createAccount('Test Customer');
        endCustomertAcct.RecordTypeId = endUserRT ;
        insert endCustomertAcct;     
         
        Test.startTest();
        Opportunity currentOppty = new Opportunity(AccountId =endCustomertAcct.Id,
                                                    RecordTypeId = ITOppRT,
                                                    Type = 'Tender',
                                                    Description = 'Test Opportunity Creation',
                                                    Division__c = 'IT',
                                                    Amount = 9999,
                                                    CloseDate = Date.valueOf(System.Today()),
                                                    ProductGroupTest__c = 'PRINTER',
                                                    LeadSource = 'External_Tradeshow',
                                                    Reference__c = 'Test Name');
        
        NewOpportunity oppCont = new NewOpportunity(new ApexPages.StandardController(currentOppty));
        PageReference opptyPage = page.NewOpportunity;
        Test.setCurrentPage(opptyPage);
        oppCont.gettypeSelectList();
        System.assert(oppCont!=null);
        String createdDateValue = System.today().format();
        oppCont.saveButton();
        Test.stopTest();
        
        System.assertEquals(oppCont.currentOppty.StageName, 'Identified');
        Opportunity oppRec = [select Id, Name, AccountId, ForecastCategoryName from Opportunity where AccountId =:endCustomertAcct.Id limit 1];
        System.assertEquals(oppRec.ForecastCategoryName,'Upside');
        System.assert(oppRec.Name!='');
        System.assertNotEquals(null, oppRec.id);
    }
     static testMethod void opportunityCreationonSaveAndContTest(){
    
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();

        Account endCustomertAcct = TestDataUtility.createAccount('Test Customer1');
        endCustomertAcct.RecordTypeId = endUserRT ;
        insert endCustomertAcct;
         
        System.assert(endCustomertAcct.Id!=null); 
        Test.startTest();
        Opportunity currentOppty = new Opportunity(AccountId=endCustomertAcct.Id,
                                                    RecordTypeId = ITOppRT,
                                                    Type = 'Tender',
                                                    Description = 'Test Opportunity mobile',
                                                    Division__c = 'IT',
                                                    Amount = 9999,
                                                    CloseDate = Date.valueOf(System.Today()),
                                                    ProductGroupTest__c = 'PRINTER',
                                                    LeadSource = 'External_TCC',
                                                    Reference__c = 'Test Mobile');
        
        NewOpportunity oppCont = new NewOpportunity(new ApexPages.StandardController(currentOppty));
        PageReference opptyPage = page.NewOpportunity;
        Test.setCurrentPage(opptyPage);
        System.assert(oppCont!=null);
        String createdDateValue = System.today().format();
        oppCont.saveAndContinue();
        Test.stopTest();
        
        System.assertEquals(oppCont.currentOppty.StageName, 'Identified');
        Opportunity oppRec = [select Id, Name, AccountId, ForecastCategoryName from Opportunity where AccountId =:endCustomertAcct.Id limit 1];
        System.assertEquals(oppRec.ForecastCategoryName,'Upside');
        System.assert(oppRec.Name!='');
        System.assertNotEquals(null, oppRec.id);
    } 
    static testMethod void opportunityCreationonCancelTest(){
    
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        
        Account indirectAcct = TestDataUtility.createAccount('Test Customer Cancel');
        indirectAcct.RecordTypeId = inDirectRT;
        indirectAcct.Type='VAR';
        insert indirectAcct;    
        
        Opportunity currentOppty = new Opportunity(AccountId=indirectAcct.Id,
                                                    RecordTypeId = ITOppRT,
                                                    Type = 'Internal');        
        Test.startTest();
        
        NewOpportunity oppCont = new NewOpportunity(new ApexPages.StandardController(currentOppty));//
        PageReference opptyPage = page.NewOpportunity;
        Test.setCurrentPage(opptyPage);

        oppCont.cancel();
        Test.stopTest();

    }
     static testMethod void opportunityCreationonNegativeTest(){
        
        Boolean pageMsgFound = false;
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        
        Account indirectAcct2 = TestDataUtility.createAccount('Test Customer Cancel');
        indirectAcct2.RecordTypeId = inDirectRT;
        indirectAcct2.Type='VAR';
        indirectAcct2.Name = 'Testing the scenario where Account Name exceeds maximum size of characters and expect the code to truncate it'; 
        insert indirectAcct2; 
        
        Opportunity currentOppty = new Opportunity(AccountId=indirectAcct2.Id,
                                                    RecordTypeId = ITOppRT,
                                                    Type = 'Internal',
                                                    Description = 'Test Opportunity mobile',
                                                    Division__c = 'IT',
                                                    Amount = 9999,
                                                    ProductGroupTest__c = 'PRINTER',
                                                    LeadSource = 'External_TCC',
                                                    Reference__c = 'Test Mobile');
        
        Test.startTest();
        NewOpportunity oppCont = new NewOpportunity(new ApexPages.StandardController(currentOppty));
        PageReference opptyPage = page.NewOpportunity;
        Test.setCurrentPage(opptyPage);
        oppCont.saveAndContinue();
        oppCont.saveButton();  
        Test.stopTest();
         
        ApexPages.Message[] pageMessages = ApexPages.getMessages();
        System.assertNotEquals(0, pageMessages.size());
        
        for(ApexPages.Message msg : ApexPages.getMessages()){
        if(msg.getSeverity() ==ApexPages.Severity.Error)
            pageMsgFound = true;
          }
          
          System.assert(pageMsgFound);
         
     }

}