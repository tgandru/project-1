public with sharing class NewOpportunityExtn {
    public Opportunity currentOppty{get;set;}
    public string groupSelected {get;set;}
    public NewOpportunityExtn (ApexPages.StandardController stdcon) {
        if (!Test.isRunningTest()) {
            stdcon.addFields(new List<String> {'Pricebook2Id'});
        }
        currentOppty = (Opportunity)stdcon.getRecord();
        System.debug('##Current Oppty:   '+currentOppty);
        if(currentOppty.Pricebook2Id!=null){
            groupSelected = currentOppty.Pricebook2Id;
        }
    }
    
    public List<SelectOption> getPriceBookList(){
        System.debug('## currentOppty.Division__c'+currentOppty.Division__c);
        List<SelectOption> recordTypeOptions = new List<SelectOption>();
		List<Pricebook2> pbRecords = [SELECT Id, Name, Division__c,IsActive FROM Pricebook2 where Division__c=: currentOppty.Division__c and IsActive=true ORDER BY Name ASC];
        set<Id> pricebookIDs = new set<Id>(); 
        for(PriceBook2 pb: pbRecords){
            pricebookIDs.add(pb.Id);
        }

        List<UserRecordAccess> listUserRecordAccess = [SELECT RecordId, HasReadAccess, HasEditAccess, HasDeleteAccess FROM UserRecordAccess WHERE UserId=:UserInfo.getUserId() AND RecordId IN: pricebookIDs];

        Map<Id,boolean> accessMap = new Map<Id,boolean>();

        for(UserRecordAccess recAccess : listUserRecordAccess){
            accessMap.put(recAccess.RecordId, recAccess.HasReadAccess);
        }
        
        
        recordTypeOptions.add(new selectOption( '', '-Select a Price Book-'));
        for(Pricebook2 R : pbRecords){
            if(accessMap.get(R.Id)){
                recordTypeOptions.add(new SelectOption(R.Id, R.Name));
            }
        }
        
        if(recordTypeOptions.size() == 1){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING, 'You do not have a price book set up.  Please contact customer support.');
            ApexPages.addMessage(myMsg); 
            return null;  
        }
        else {
             System.debug('## recordTypeOptions'+recordTypeOptions);
            return recordTypeOptions;
        }

    }
    public PageReference saveButton(){
        try {
                currentOppty.pricebook2Id = groupSelected;
                update currentOppty;                
                PageReference opptyPage = new PageReference('/'+currentOppty.Id);
                opptyPage.setRedirect(true);
                return opptyPage;
        }
        catch(Exception ex){
            ApexPages.addMessages(ex);
            return null;
        }
    }
    public PageReference saveAndContinue(){
        try {
                currentOppty.pricebook2Id = groupSelected;
                update currentOppty;
                
                PageReference opptyPage = new PageReference('/apex/OpportunityPartnerCreation?id='+currentOppty.Id+'&display=true');
                opptyPage.setRedirect(true);
                return opptyPage;                     
        }
        catch(Exception ex){
            ApexPages.addMessages(ex);
            return null;
        }
        
    }
    
    public PageReference cancel(){
        PageReference oppPage = new PageReference('/' +currentOppty.Id);
        System.debug('redirect page: ' + oppPage);
        return oppPage;
    }
    
   /* 
    //Modified: comments from Paul - This is no longer a requirement -by KR on 05/16/16     
    public Boolean validateForm(){      
         if(currentOppty.Submit_for_Deal_Registration__c && (currentOppty.Reason__c=='' || currentOppty.Reason__c == null)) {
            return false;
        }   
        else
            return true;
    }*/
    
}