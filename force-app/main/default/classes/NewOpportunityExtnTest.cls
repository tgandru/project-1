// NewOpportunityExtnTest
// Class to test Opportunity Creation Extn VF Page
// ------------------------------------------------------------------
//  Author          Date        Description
// ------------------------------------------------------------------
// Kavitha M      03/11/2016    Created
//
@isTest
public class NewOpportunityExtnTest {

    static Id endUserRT = TestDataUtility.retrieveRecordTypeId('End_User', 'Account'); 
    static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
    static Id carrierRT = TestDataUtility.retrieveRecordTypeId('Mobile', 'Opportunity'); 
    static Id ITRT = TestDataUtility.retrieveRecordTypeId('IT', 'Opportunity');
    //static Id pbBTA = TestDataUtility.retrievePricebook('BTA Dealers');
    //static Id pbIT = TestDataUtility.retrievePricebook('IT Distributor');

    
     static testMethod void opportunityCreationonSaveTest(){
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Account acct = TestDataUtility.createAccount('Test Customer');
        acct.RecordTypeId = endUserRT ;
        insert acct;
         
        Pricebook2 ITPricebook = TestDataUtility.createPriceBook('IT Distributor');
        ITPricebook.Division__c='IT';
         insert ITPricebook; 
        
        Opportunity currentOppty = new Opportunity(Name = 'Test',
                                                    AccountId =acct.Id,
                                                    RecordTypeId = ITRT,
                                                    Type = 'Tender',
                                                    Description = 'Test Opportunity Creation',
                                                    Division__c = 'IT',
                                                    Amount = 9999,
                                                    CloseDate = Date.valueOf(System.Today()),
                                                    ProductGroupTest__c = 'PRINTER',
                                                    LeadSource = 'External_Tradeshow',
                                                    Reference__c = 'Test Name',
                                                    stageName = 'Identified'); 
               
       insert currentOppty;
        currentOppty.Roll_Out_Start__c = System.today().addDays(1);
        currentOppty.Rollout_Duration__c = 1;
        currentOppty.Type = 'Managed';
        currentOppty.Channel__c = 'Indirect';
   
        Test.startTest();
        
        NewOpportunityExtn oppCont = new NewOpportunityExtn(new ApexPages.StandardController(currentOppty));
        PageReference opptyPage = page.NewOpportunityExtn;
        Test.setCurrentPage(opptyPage);
        opptyPage.getParameters().put('id',currentOppty.id);
        oppCont.getPriceBookList();
        oppCont.groupSelected = ITPricebook.Id; //Pricebook2Id
        oppCont.saveButton(); 
           
        Test.stopTest();
         
        Opportunity oppRec = [select Id, Name, AccountId, ProductGroupTest__c, Roll_Out_End_Formula__c,Rollout_Duration__c,Type,pricebook2Id from Opportunity where Id =:currentOppty.Id limit 1];
        System.assertEquals(oppRec.ProductGroupTest__c,'PRINTER');
        System.assertEquals(1, oppRec.Rollout_Duration__c);
     }
     
     static testMethod void opportunityCreationonSaveAndContinueTest(){
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Account acct = TestDataUtility.createAccount('Test Customer');
        acct.RecordTypeId = endUserRT ;
        insert acct;   
        
        Opportunity currentOppty = new Opportunity(Name = 'Test',
                                                    AccountId =acct.Id,
                                                    RecordTypeId = carrierRT,
                                                    Type = 'Tender',
                                                    Description = 'Test Opportunity Creation',
                                                    Division__c = 'Mobile',
                                                    Amount = 9999,
                                                    CloseDate = Date.valueOf(System.Today()),
                                                    ProductGroupTest__c = 'PRINTER',
                                                    StageName='Identified',
                                                    Reference__c = 'Test Name'); 
               
        insert currentOppty;
        
        currentOppty.CloseDate = Date.valueOf(System.Today());
        currentOppty.Roll_Out_Start__c = System.today().addDays(1);
        currentOppty.Rollout_Duration__c = 1;
        
        Test.startTest();

            NewOpportunityExtn oppCont = new NewOpportunityExtn(new ApexPages.StandardController(currentOppty));
            PageReference opptyPage = page.NewOpportunityExtn;
            Test.setCurrentPage(opptyPage);
            opptyPage.getParameters().put('id',currentOppty.id);
            oppCont.getPriceBookList();
            //oppCont.groupSelected = ITPricebook.Id; 
            oppCont.saveAndContinue(); 
           
        Test.stopTest();

         for(ApexPages.Message msg : ApexPages.getMessages()){
            System.assert(msg.getSummary()!=null);
            }
     }

    static testMethod void opportunityonCancelTest(){
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Account acct = TestDataUtility.createAccount('Test Customer');
        acct.RecordTypeId = endUserRT ;
        insert acct;    
        
        Opportunity currentOppty = new Opportunity(Name = 'Test',
                                                    AccountId =acct.Id,
                                                    RecordTypeId = carrierRT,
                                                    Type = 'Channel',
                                                    Description = 'Test Opportunity Creation',
                                                    Division__c = 'Mobile',
                                                    Amount = 9999,
                                                    CloseDate = Date.valueOf(System.Today()),
                                                    ProductGroupTest__c = 'PRINTER',
                                                    StageName='Identified'); 
               
        insert currentOppty;
        
        Test.startTest();
        
        NewOpportunityExtn oppCont = new NewOpportunityExtn(new ApexPages.StandardController(currentOppty));
        PageReference opptyPage = page.NewOpportunity;
        opptyPage.getParameters().put('id',currentOppty.id);
        Test.setCurrentPage(opptyPage);

        oppCont.cancel();
        Test.stopTest();

    }
    
}