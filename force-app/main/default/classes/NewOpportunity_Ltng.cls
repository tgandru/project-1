public class NewOpportunity_Ltng {
    @AuraEnabled
    public static Account getAccountInfo(String accId){
        List<Account> accList = [Select id,Name,BillingState from Account where id=:accId];
        Account acc;
        if(accList.size()>0){
            acc = accList[0];
        }else{
            acc = new Account();
        }
        return acc;
    }
}