public with sharing class NewQuoteController {
    
    public String selProductDivision {get;set;}
    public List<SelectOption> allProdDivisions {get;set;}
    public Opportunity opp {Get;set;}
    String oppProdGrp;
    public integer stepno {get;set;}
    public List<Quote> lstExisQuotes {get;set;}
    public List<QuoteWrapper> lstQtWrp {get;set;}
    public Quote selQuote {get;set;}
    public boolean GroupLineItemSection {get;set;} 
    public List<OpportunityLineItem> oppLineItem {get;set;}
    public List<oppLineItemWrapper> lstOliWrp {get;set;}
    List<ProductFamilyandGroupMap__c> lstProdGrpMap;
    //public String selprodGroup {get;set;}
    public List<SelectOption> allProdGroups {get;set;}
    public List<SelectOption> selprodGroup {get;set;}
    public List<SelectOption> selprodGroupsToUpdate {get;set;}
    public String selProdGroupText {get;set;}
    public String selectedProdGroupText {get;set;}
    public Quote newQ{get;set;}
    public boolean newQuoteCreation {get;set;}
    public boolean SavenewQuote {get;set;}
    public boolean masterselect {get;set;}
    public boolean noPrimaryDist {get;set;}
    public List<QuoteLineItem> selQtlilst {get;set;}
    public boolean noDealReg {get;set;}
    
    public NewQuoteController(ApexPages.StandardController controller) {
        opp = (Opportunity) controller.getRecord();
        opp = [select id,stagename,ProductGroupTest__c,Account.Name,Pricebook2Id,Division__c,Deal_Registration_Approval__c,WinApprovalStatus__c,(select id from opportunitylineitems where Discount__c>50) from Opportunity where id=:opp.id];
        oppProdGrp = opp.ProductGroupTest__c;
        noDealReg=false;
           List<String> excludedPBIDs=Label.PB_To_Stop_Quote_creation.split(';');
        
        //Thiru:Added stage check for Quote Creation--04/29/2019
        //Starts
        if(opp.stagename=='Win'){
            noPrimaryDist = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Cannot create Quote for Win Opportunity'));     
            return;
        }
        
        if(opp.Deal_Registration_Approval__c=='Pending Approval'||opp.WinApprovalStatus__c=='Pending Approval'){
            noPrimaryDist = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Cannot create/update Quote when Opportunity record is locked due to pending Approval.'));     
            return;
        }
        if(excludedPBIDs.contains(opp.Pricebook2Id)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Selected Price Book Is not Allowed to Create Quote.'));     
            return; 
        }
        
        noPrimaryDist = false;
        if(opp.stagename!=Label.OppStageQualification && opp.stagename!=Label.OppStageProposal && opp.stagename!=Label.OppStageCommit){
            noPrimaryDist = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Opportunity must be in Qualified, Proposal, Commit stage for Quote Creation'));     
            return;
        }else if(opp.Pricebook2Id!=null){
            noPrimaryDist = true;
            UserRecordAccess ura = [Select RecordId,HasReadAccess from UserRecordAccess where RecordId=:opp.Pricebook2Id and UserId=:UserInfo.getUserId()];
            if(ura.HasReadAccess!=true){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,''+UserInfo.getName()+' does not have access to Opportunity Pricebook'));     
                return;
            }
        }
        //Ends
        if(opp.Deal_Registration_Approval__c!='Approved' && opp.Division__c=='W/E' && opp.opportunitylineitems.size()>0){
            noDealReg=true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'One or More Opportunity Products have Discount More Than 50% . Please submit for the deal Registration for creating a new Quote'));     
            return;
        }
        
        lstQtWrp = new List<QuoteWrapper>();
        oppLineItem = new List<OpportunityLineItem>();
        lstOliWrp = new List<oppLineItemWrapper>();
        selprodGroup = new List<SelectOption>();
        selprodGroupsToUpdate = new List<SelectOption>();
        selQtlilst = new List<QuoteLineItem>();
        List<Partner__c> lstpartnr = new List<Partner__c>();
        lstpartnr = [Select Id,Role__c,Is_Primary__c,Opportunity__c from Partner__c where Opportunity__c = :opp.id AND Role__c='Distributor' and is_primary__c = true];
        noPrimaryDist = false;   
        if(lstpartnr.size() == 0){
            noPrimaryDist = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'There is no primary distributor for the related Opportunity. Please add a primary distributor before creating a new Quote.'));     
            return;
        }
        newQuoteCreation = TRUE;
        SavenewQuote = FALSE;
        masterselect = FALSE;
        
        newQ = new Quote();
        newQ.Valid_From_Date__c = system.today();
        newQ.ExpirationDate = system.today()+90;
        //----Added by Thiru----9/12/2016-----
        //Making the 'Other' defalt in MPS field.
        newQ.MPS__c = 'Other';
        lstProdGrpMap = ProductFamilyandGroupMap__c.getAll().values();
        GroupLineItemSection = FALSE;
        
        getallDivisions();
        getExistingQuotes();
        
        stepno = 1;
    }
    
    public void reloadProdGroup(){
        selprodGroup.clear();
        getProdGroups();
    }
    
    public List<SelectOption> getProdGroups(){
        Set<String> tempset = new Set<String>();
        Set<String> selpg = new Set<String>();
        if(selprodGroup!=null && selprodGroup.size() > 0)
                for(SelectOption s : selprodGroup)
                        selpg.add(s.getValue());
        
        system.debug('************************selected product group...........'+selprodGroup);
        
        allProdGroups = new List<SelectOption>();
        
        List<OpportunityLineItem > lstoli=new List<OpportunityLineItem >();
        //Jagan: removed and Alternative__c=false filter as per T20995
        lstoli= [select id,Product2Id,Product_Family__c,Product_Group__c,Quantity,UnitPrice,TotalPrice,PriceBookEntryId,Description,Subtotal,Product2.Name,Requested_Price__c,
                                AMPV_Color__c,AMPV_Mono__c,Bundle_Flag__c,SAP_Material_Id__c,Discount__c,Product_Type__c,Claimed_Quantity__c,Alternative__c,Parent_Product__c 
                                from OpportunityLineItem where Opportunityid = :opp.id];
        Set<String> olipgset = new Set<String>();
        for(OpportunityLineItem oli : lstoli){
            olipgset.add(oli.Product_Group__c); 
        }        
        
        for(ProductFamilyandGroupMap__c p : lstProdGrpMap){
            if(p.Product_Family__c == selProductDivision && p.Product_Group__c!= null && oppProdGrp != null && oppProdGrp.contains(p.Product_Group__c) && !selpg.contains(p.Product_Group__c) && olipgset.contains(p.Product_Group__c))
                tempset.add(p.Product_Group__c);
        }
        //allProdGroups.add(new SelectOption('--None--','--None--'));
        for(String s:tempset)
            allProdGroups.add(new SelectOption(s,s));
        return allProdGroups; 
    }
    
    public void getallDivisions(){
        
        Set<String> tempset = new Set<String>();
        allProdDivisions = new List<SelectOption>();
        
        
        for(ProductFamilyandGroupMap__c p : lstProdGrpMap){
            if(p.Product_Group__c!= null && oppProdGrp != null && oppProdGrp.contains(p.Product_Group__c) )
                tempset.add(p.Product_Family__c);
        }
        allProdDivisions.add(new SelectOption('--None--','--None--'));
        for(String s:tempset)
            allProdDivisions.add(new SelectOption(s,s));
        
    }
    
    
    public void PrevStep(){
        stepno--;
        getProdGroups();
        GroupLineItemSection= FALSE;
        
    }
    
    public void nextStep(){
        //Added by Thiru --- for line-134
        Set <String> mySet = new Set <String> ();
        for(SelectOption s : selprodGroup){
            myset.add(s.getvalue());
        }
        //Added by Thiru---Ends---
        if(stepno == 1 && (selProductDivision=='--None--' || selprodGroup == null || (selprodGroup!=null && selprodGroup.size() == 0)))
            ApexPages.addMessage(New ApexPages.Message(ApexPages.Severity.Error,'Please select Product Division and Product Group before proceeding to the next step.'));
        //Added by Thiru --- starts
        else if(selProductDivision=='MOBILE PHONE [G1]' && myset.contains('KNOX') && selprodGroup.size() > 1){
            ApexPages.addMessage(New ApexPages.Message(ApexPages.Severity.Error,'You cannot combine KNOX product group with any other product group Quote'));
        }
        //Added by Thiru---Ends---
        else{
            stepno++;
            selProdGroupText='';
            if(myset.contains('KNOX'))
                getExistingKnoxQuotes();
            else
                getExistingQuotes();
            if(lstQtWrp.size()> 0){
                newQuoteCreation = TRUE;
                SavenewQuote = FALSE;
                
            }
            else{
                GroupLineItemSection= TRUE;
                newQuoteCreation = FALSE;
                SavenewQuote = TRUE;
                getOppLineItems();
            }
            system.debug('Selected product groups.................'+selprodGroup);
            if(selprodGroup!=null && selprodGroup.size() > 0)
                for(SelectOption s : selprodGroup)
                    selProdGroupText += s.getValue()+';';
        }
    }
    //Added by Thiru----for getting existing Knox Quotes
    public void getExistingKnoxQuotes(){
        
        lstExisQuotes = new List<Quote>();
        lstQtWrp.clear();
        lstExisQuotes = [select id,QuoteNumber,ProductGroupMSP__c,External_SPID__c,TotalPrice,Status,Division__c,Product_Division__c from Quote where OpportunityId =:opp.id and ProductGroupMSP__c = 'KNOX'];
        if(lstQtWrp.size() == 0){
            for(Quote q: lstExisQuotes)
                lstQtWrp.add(new QuoteWrapper(q));
        }
    }
    //Added by Thiru-----Ends------
    public void getExistingQuotes(){
        
        lstExisQuotes = new List<Quote>();
        lstQtWrp.clear();
        lstExisQuotes = [select id,QuoteNumber,ProductGroupMSP__c,External_SPID__c,TotalPrice,Status,Division__c,Product_Division__c from Quote where OpportunityId =:opp.id and Product_Division__c = :selProductDivision and ProductGroupMSP__c != 'KNOX'];
        if(lstQtWrp.size() == 0){
            for(Quote q: lstExisQuotes)
                lstQtWrp.add(new QuoteWrapper(q));
        }
        /*else{
            for(QuoteWrapper qw:lstQtWrp){
                if(qw.qt.id == selectedQuoteId)
                    qw.isSelected = true;
            }
        }*/
        
    }
    
    public Id selectedQuoteId{get;set;}
    public void getSelected()
    {
        newQuoteCreation = FALSE;
        selectedQuoteId = ApexPages.currentPage().getParameters().get('quoteid');
        if(selectedQuoteId != null){
            Quote temp=[select id,QuoteNumber,ProductGroupMSP__c,External_SPID__c,TotalPrice,Status,Division__c,Product_Division__c from Quote where Id=:selectedQuoteId ];
            if(temp.Product_Division__c != selProductDivision )
                ApexPages.addMessage(New ApexPages.Message(ApexPages.Severity.Error,'Selected Product Division is different from the one selected now. Please select a different Quote if it exists or create a new Quote with this Product Division.'));
            else{                
                addGrouporLineItemToExisting();
                SavenewQuote = FALSE;
            }
        }
        
    }
    
    public void showSaveNewQuoteButton(){
       
        Set<String> expg = new Set<String>();
        Set<String> selpg = new Set<String>();
        if(selprodGroup!=null && selprodGroup.size() > 0)
                for(SelectOption s : selprodGroup)
                        selpg.add(s.getValue());      
                        
        for(Quote q: lstExisQuotes){
            if(q.ProductGroupMSP__c<>null){
                if(q.productGroupMSP__c.contains(';'))
                    expg.addAll(q.productGroupMSP__c.split(';'));
                else
                    expg.add(q.productGroupMSP__c);
            }
        }
        boolean exisquote = false;
        for(String s:selpg){
            if(expg.contains(s)){
                exisquote = true;
                ApexPages.addMessage(New ApexPages.Message(ApexPages.Severity.Error,'There is an existing Quote for one of the selected Product Group/(s). Please select an existing Quote or select a new Product Group that is not a part of any Quote.'));
                break;
            }
                
        }
        if(!exisquote){
            SavenewQuote = TRUE;
            newQuoteCreation = FALSE;
            GroupLineItemSection = TRUE;
            getOppLineItems();
        }
    }
    
    public void addGrouporLineItemToExisting(){
        
        system.debug('existing quotes............................'+lstQtWrp);
        for(QuoteWrapper q: lstQtWrp){
            if(q.qt.id == selectedQuoteId){
                selQuote = q.qt;
                q.isSelected = true;
                break;
            }
        }
        if(selQuote != null){
            GroupLineItemSection = TRUE;
            getQuoteLineItems();
            getOppLineItems();
        }
        
    }
    
    Set<Id> exisProdIdSet = new Set<Id>();
    
    public void getQuoteLineItems(){
        
        if(selQuote!= null && selQuote.id !=null && stepno == 2){
            if(selQtlilst.size() == 0){
                selQtlilst = [select id,Product2.name,ProductGroup__c,TotalPrice,Product_Division__c,Product2id,Description,QuoteId,UnitPrice,PriceBookEntryId,Quantity,Requested_Price__c,Unit_Cost__c,Parent_Product__c,SAP_Material_Id__c from QuoteLineItem where QuoteId= :selQuote.id];
                for(QuotelineItem q:selQtlilst)
                    exisProdIdSet.add(q.product2id);
            }
        }    
    }
    
    public void getOppLineItems(){
        
        if(stepno == 2){
            Set<String> prodgrpset = new Set<String>();
            if(selprodGroup!=null && selprodGroup.size() > 0)
                for(SelectOption s : selprodGroup)
                    prodgrpset.add(s.getValue());
            
            //if(selprodGroupsToUpdate!=null && selprodGroupsToUpdate.size() > 0)
           //     for(SelectOption s : selprodGroupsToUpdate)
            //        prodgrpset.add(s.getValue());
               //selQuote !=null &&      
            
            system.debug('****************Selected Quote****************'+selQuote);
            system.debug('****************existing product ids for selected Quote****************'+exisProdIdSet);
            lstOliWrp.clear();
            if(lstOliWrp.size() == 0){
                
                oppLineItem = [select id,Product2Id,Product_Family__c,Product_Group__c,Quantity,UnitPrice,TotalPrice,PriceBookEntryId,Description,Subtotal,Product2.Name,Requested_Price__c,
                                AMPV_Color__c,AMPV_Mono__c,Bundle_Flag__c,SAP_Material_Id__c,Discount__c,Product_Type__c,Claimed_Quantity__c,Alternative__c, Parent_Product__c, Unit_Cost__c
                                from OpportunityLineItem where Opportunityid = :opp.id ];//and Alternative__c=false];
                for(OpportunityLineItem ot: oppLineItem){
                    if(prodgrpset.contains(ot.Product_Group__c))
                        if(exisProdIdSet.contains(ot.product2id))
                            lstOliWrp.add(new oppLineItemWrapper(ot,true));
                        else
                            lstOliWrp.add(new oppLineItemWrapper(ot,false));
                }
            }
             
        }
    }
    
    public pageReference SaveSelectedQuotewithLineItems(){
        if(selQuote != null){
            String origProdGrp = selQuote.productGroupMSP__c;
            
            system.debug('****************Original Prod Group ****************'+origProdGrp );
            system.debug('****************Selected Products ****************'+selprodGroup);
            
            if(selprodGroup!=null && selprodGroup.size()>0){
                
                //Set<SelectOption > settemp = new Set<SelectOption >();
                //settemp.addAll(selprodGroupsToUpdate);
                //selprodGroupsToUpdate.clear();
                //selprodGroupsToUpdate.addAll(settemp);
                Integer i=0;
                if(selQuote.productGroupMSP__c != null)
                    selQuote.productGroupMSP__c +=';';   
                boolean isUpdate=false;
                for(SelectOption s : selprodGroup){
                    if(!origProdGrp.containsIgnorecase(s.getValue())){
                        isupdate = true;
                        if(i < selprodGroup.size())
                            selQuote.productGroupMSP__c += s.getValue()+';';
                        else
                            selQuote.productGroupMSP__c += s.getValue();
                    }
                    i++;
                }
                if(!isUpdate)
                    selQuote.productGroupMSP__c = origProdGrp;
            }
            try{
                update selQuote ;
                List<QuoteLineItem> lstQli = new List<QuoteLineItem>();
                Set<id> exisProdIds = new set<Id>();
                lstQli = [select id,product2id from QuoteLineItem where Quoteid=:selQuote.id];
                for(QuoteLineItem q : lstQli)
                    exisProdIds.add(q.product2id);
                
                system.debug('****************************OPP LINE ITEMS..........'+lstOliWrp);
                
                if(lstOliWrp!=null && lstOliWrp.size()>0){
                    List<QuoteLineItem> inslstqli = new List<QuoteLineItem>();
                    for(oppLineItemWrapper oiw : lstOliWrp){
                        if(oiw.isSelected== TRUE && !exisProdIds.contains(oiw.oli.Product2Id)){
                            QuoteLineItem qli = new QuoteLineItem();
                            qli.Product2id = oiw.oli.Product2Id;
                            qli.OLIID__c = oiw.oli.id;
                            qli.Description = oiw.oli.Description;
                            qli.QuoteId = selQuote.id;
                            qli.UnitPrice= oiw.oli.UnitPrice;
                            qli.PriceBookEntryId = oiw.oli.PriceBookEntryId;
                            qli.Quantity = oiw.oli.Quantity;
                            qli.Requested_Price__c = oiw.oli.Requested_Price__c;
                            qli.Unit_Cost__c = oiw.oli.Unit_Cost__c;
                            qli.SAP_Material_ID__c=oiw.oli.SAP_Material_ID__c;
                            qli.Parent_Product__c=oiw.oli.Parent_Product__c;
                            inslstqli.add(qli);
                        }
                    }
                    if(inslstqli.size() > 0)
                        insert inslstqli;
                }
                    
                return new pageReference('/'+selQuote.id);
            }
            catch(Exception e){
                 ApexPages.addMessage(New ApexPages.Message(ApexPages.Severity.Error,''+e));
                 return null;
            }
        }
        else{
            ApexPages.addMessage(New ApexPages.Message(ApexPages.Severity.Error,'No Quotes have been selected to Save.'));
            return null;
        }
    }
    
    
    
    public pageReference createNewQuote(){

        if(newQ.Valid_From_Date__c > newQ.ExpirationDate){
             ApexPages.addMessage(New ApexPages.Message(ApexPages.Severity.Error,'Valid Until Date cannot be earlier than Valid From Date.'));
             return null;
        }
        try{
            /*if(newQ.BEP__c == null && (newQ.MPS__c  == null || newQ.Supplies_Buying_Location__c ==null ) && (selProdGroupText!=null && (selProdGroupText.contains('A3 COPIER') || selProdGroupText.contains('PRINTER') || selProdGroupText.contains('FAX/MFP')))){
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.severity.Error,'MPS and Supplies Buying Location fields cannot be blank when BEP field is blank, Quote status is Draft and Product Group is either A3 COPIER, PRINTER or FAX/MFP.'));
                return null;
            }*/
            
            if(selProductDivision != 'PRINTER [C2]' && newQ.RequestType__c == null){
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.severity.Error,'Request Type is required.'));
                return null;
            }
            else if(selProductDivision == 'PRINTER [C2]' && (newQ.MPS__c == null || newQ.Supplies_Buying_Location__c== null)){
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.severity.Error,'MPS, Supplies Buying Location are required.'));
                return null;
            }
            
            if(lstOliWrp==null || ( lstOliWrp!=null && lstOliWrp.size()==0)){
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.severity.Error,'There are no products for any of the selected Product Group/(s). Please add line items to the Opportunity before performing this step.'));
                return null;    
            }
            
            Id stndid = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Standard').getRecordTypeId();
            Id prntrid = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Printer').getRecordTypeId();
            
            newQ.Name = opp.Account.name+'-'+selProdGroupText+'-'+newQ.Reference__c;
            newQ.OpportunityId = opp.id;
            newQ.productGroupMSP__c = selProdGroupText;
            newQ.Pricebook2Id = opp.Pricebook2Id;
            newQ.Division__c = opp.Division__c;
            if(opp.Division__c=='W/E'){
                newQ.status=Label.ApprovedStatus; 
            }
            
            if(selProdGroupText!=null && selProdGroupText.contains('MEMORY_BRAND')){
                //newQ.ExpirationDate = system.today()+30;//Commented by Thiru--09/25/2019
                newQ.ExpirationDate = system.today()+60; //Added by Thiru--09/25/2019
            }
            if(selProductDivision == 'PRINTER [C2]')
                newQ.Recordtypeid = prntrid;
            else
                newQ.Recordtypeid = stndid;
                
            newQ.Product_Division__c = selProductDivision;

            Boolean hasLineItem=false;
            if(!lstOliWrp.isEmpty()){
                for(oppLineItemWrapper oiw : lstOliWrp){
                    if(oiw.isSelected== TRUE){
                        hasLineItem=true;
                        break;
                    }
                }
            }

            if(lstOliWrp.isEmpty() || hasLineItem==false){
                ApexPages.addMessage(New ApexPages.Message(ApexPages.Severity.Error,'On a new quote, you must select products. Please select products and continue.'));
                return null;
            }else{
                insert newQ;
            }
            system.debug('new status'+newQ.status);
            
            if(lstOliWrp!=null && lstOliWrp.size()>0){
                List<QuoteLineItem> lstqli = new List<QuoteLineItem>();
                for(oppLineItemWrapper oiw : lstOliWrp){
                    if(oiw.isSelected== TRUE){
                        QuoteLineItem qli = new QuoteLineItem();
                        qli.Product2id = oiw.oli.Product2Id;
                        qli.Description = oiw.oli.Description;
                        qli.UnitPrice= oiw.oli.UnitPrice;
                        qli.QuoteId = newQ.id;
                        qli.OLIID__c = oiw.oli.id;
                        qli.Unit_Cost__c = oiw.oli.Unit_Cost__c;
                        qli.SAP_Material_ID__c=oiw.oli.SAP_Material_ID__c;
                        qli.Parent_Product__c=oiw.oli.Parent_Product__c;
                        if(selProductDivision == 'PRINTER [C2]'){
                            qli.AMPV_Color__c= oiw.oli.AMPV_Color__c;
                            qli.AMPV_Mono__c= oiw.oli.AMPV_Mono__c;
                            qli.Bundle_Flag__c= oiw.oli.Bundle_Flag__c; 
                        }
                        qli.PriceBookEntryId = oiw.oli.PriceBookEntryId;
                        qli.Quantity = oiw.oli.Quantity;
                        qli.Requested_Price__c=oiw.oli.Requested_Price__c;
                        //qli.TotalPrice= oiw.oli.TotalPrice;
                        //qli.Subtotal= oiw.oli.Subtotal;
                        
                        lstqli.add(qli);
                    }
                }
                if(lstqli.size() > 0){
                    insert lstqli;
                }
            }
            
            return new pageReference('/'+newQ.id);
        }
        catch(Exception e){
            ApexPages.addMessage(New ApexPages.Message(ApexPages.Severity.Error,''+e));
            return null;
        }
    }
    public class QuoteWrapper {
        public Quote qt {get;set;}
        public boolean isSelected {get;set;}
        
        public QuoteWrapper(Quote q){
            this.qt = q;
            isSelected = false;
        }
    }
    
    public void selectAll()
    {
        
        for(oppLineItemWrapper oiw : lstOliWrp)
            if(masterselect){
                if(oiw.isselected == false && oiw.isDisabled==false)
                oiw.isselected = true;
            }
            else{
                if(oiw.isselected == true)
                oiw.isselected = false;
            }
    }
    
    
    public class oppLineItemWrapper {
        public OpportunityLineItem oli {get;set;}
        public boolean isSelected {get;set;}
        public boolean isDisabled {get;set;}
        public oppLineItemWrapper(OpportunityLineItem ol,boolean disable){
            this.oli = ol;
            isSelected = false;
            isDisabled = disable;
        }
    }
}