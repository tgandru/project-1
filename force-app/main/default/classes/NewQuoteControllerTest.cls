@isTest
private class NewQuoteControllerTest {
    static Account testMainAccount;
    static Account testCustomerAccount;
    static List<Opportunity> testOpportunityList;
    static Opportunity oppOpen;
    static Opportunity noPB;
    static Order orderRecord1;
    static Order orderRecord2;
    static Pricebook2 pb;
    static PricebookEntry standaloneProdPBE;
    static Quote quot;
    static Quote quot2;
    static Id endUserRT = TestDataUtility.retrieveRecordTypeId('End_User', 'Account'); 
    static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
    static Id demoAcctRtId = TestDataUtility.retrieveRecordTypeId('ADL','Order');
    static Id demoOppRtId1 = TestDataUtility.retrieveRecordTypeId('Evaluation','Order');
    
    static void createPrimaryParter(Opportunity opp){
        Partner__c tp = new Partner__c();
        tp.Opportunity__c = opp.id;
        tp.Role__c = 'Reseller';
        tp.is_Primary__c = true;
        insert tp;
    }
    
    private static void setup() {
        //Jagan: Test data for fiscalCalendar__c
        Test.loadData(fiscalCalendar__c.sObjectType, 'FiscalCalendarTestData');
        
        //Channelinsight configuration
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();

        Zipcode_Lookup__c zip=TestDataUtility.createZipcode();
        insert zip;
        
        //Creating test Account
        testMainAccount = TestDataUtility.createAccount(TestDataUtility.generateRandomString(5));
        testMainAccount.RecordTypeId = endUserRT;
        insert testMainAccount;
        
        testCustomerAccount = TestDataUtility.createAccount(TestDataUtility.generateRandomString(5));
        testCustomerAccount.RecordTypeId = directRT;
        insert testCustomerAccount;
        
        
          Account paDistAcct=TestDataUtility.createAccount('Distributor Account');
        paDistAcct.RecordTypeId = directRT;
        paDistAcct.Type= 'Distributor';
        insert paDistAcct;

        
        //Creating custom Pricebook
        pb = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(4)); 
        insert pb;
        
        testOpportunityList = new List<Opportunity>();
            oppOpen=TestDataUtility.createOppty('Open Oppty',testMainAccount, pb, 'RFP', 'IT', 'HOSPITALITY_DISPLAY', 'Identified');
            // oppOpen.Primary_Distributor__c=paDistAcct.id;
            testOpportunityList.add(oppOpen);
            
            noPB = TestDataUtility.createOppty('NoPriceBook', testCustomerAccount, null, 'RFP', 'IT', 'FAX/MFP', 'Identified');
            testOpportunityList.add(noPB);
        insert testOpportunityList;
        
        //createPrimaryParter(oppOpen);
       // createPrimaryParter(noPB);
          Partner__c tp = new Partner__c();
        tp.Opportunity__c = oppOpen.id;
        tp.Role__c = 'Distributor';
        tp.is_Primary__c = true;
        tp.Partner__c=paDistAcct.id;
        insert tp; 
        //Creating Products
        List<Product2> prods=new List<Product2>();
            Product2 standaloneProd=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Standalone Prod1', 'Printer[C2]', 'HOSPITALITY_DISPLAY', 'H/W');
            prods.add(standaloneProd);
            Product2 parentProd1=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Parent Prod1', 'Printer[C2]', 'OFFICE', 'H/W');
            prods.add(parentProd1);
            Product2 childProd1=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod1', 'Printer[C2]', 'SMART_PHONE', 'Service pack');
            prods.add(childProd1);
            Product2 childProd2=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod2', 'Printer[C2]', 'SMART_PHONE', 'Service pack');
            prods.add(childProd2);
            Product2 parentProd2=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Parent Prod2', 'Printer[C2]', 'HOSPITALITY_DISPLAY', 'H/W');
            prods.add(parentProd2);
            Product2 childProd3=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod3', 'Printer[C2]', 'PRINTER', 'Service pack');
            prods.add(childProd3);
            Product2 childProd4=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod4', 'Printer[C2]', 'ACCESSARY', 'Service pack');
            prods.add(childProd4);
        insert prods;
        
        List<PriceBookEntry> pbes=new List<PriceBookEntry>();
            standaloneProdPBE=TestDataUtility.createPriceBookEntry(standaloneProd.Id, pb.Id);
            standaloneProdPBE.Product2 = standaloneProd;
            pbes.add(standaloneProdPBE);
            PriceBookEntry parentProd1PBE=TestDataUtility.createPriceBookEntry(parentProd1.Id,pb.Id);
            parentProd1PBE.Product2 = parentProd1;
            pbes.add(parentProd1PBE);
            PriceBookEntry childProd1PBE=TestDataUtility.createPriceBookEntry(childProd1.Id, pb.Id);
            childProd1PBE.Product2 = childProd1;
            pbes.add(childProd1PBE);
            PriceBookEntry childProd2PBE=TestDataUtility.createPriceBookEntry(childProd2.Id, pb.Id);
            childProd2PBE.Product2 = childProd2;
            pbes.add(childProd2PBE);
            PriceBookEntry parentProd2PBE=TestDataUtility.createPriceBookEntry(parentProd2.Id, pb.Id);
            parentProd2PBE.Product2 = parentProd2;
            pbes.add(parentProd2PBE);
        insert pbes;
        
        Contact contactRecord = TestDataUtility.createContact(testMainAccount);
        insert contactRecord;
        
        List<Order> orderList = new List<Order>();
            orderRecord1 = TestDataUtility.createOrder(testMainAccount, oppOpen);
            orderRecord1.RecordTypeId = demoAcctRtId;
           // orderRecord1.Pricebook2 = pb;
            orderRecord1.Pricebook2Id = pb.Id;
            orderRecord1.Product_Group__c = 'ACCESSARY; SMART_PHONE';
            orderRecord1.CustomerAuthorizedById = contactRecord.Id;
            orderRecord1.RequestedShipmentDate__c = Date.today();
            OrderRecord1.EffectiveDate = Date.today();
            orderRecord1.BusinessReason__c = 'test OrderRecord1';
            orderRecord1.Type = 'Seed Unit';
            orderRecord1.Customer_Comments__c='Test Class';
            orderRecord1.Next_Steps__c='Test Class';
            orderRecord1.CYOD_Availability_Date__c=Date.today();
            orderRecord1.Reason_for_Future_CYOD_Date__c='Pricing';

            orderList.add(orderRecord1);
            
            orderRecord2 = TestDataUtility.createOrder(testMainAccount, oppOpen);
            orderRecord2.RecordTypeId = demoOppRtId1;
         //   orderRecord2.Pricebook2=pb;
            orderRecord2.Pricebook2Id = pb.Id;
            orderRecord2.Product_Group__c = 'ACCESSARY; SMART_PHONE; OFFICE';
            orderRecord2.CustomerAuthorizedById = contactRecord.Id;
            orderRecord2.RequestedShipmentDate__c = Date.today();
            orderRecord2.EffectiveDate = Date.today();
            orderRecord2.BusinessReason__c = 'test OrderRecord2';
            orderRecord2.Type = 'Seed Unit';
            orderRecord2.Customer_Comments__c='Test Class';
            orderRecord2.Next_Steps__c='Test Class';
            orderRecord2.CYOD_Availability_Date__c=Date.today();
            orderRecord2.Reason_for_Future_CYOD_Date__c='Pricing';

            orderList.add(orderRecord2);
        insert orderList;
        
        List<OrderItem> orderItemList = new List<OrderItem>();
            OrderItem orderItem1 = TestDataUtility.createOrderItem(standaloneProdPBE, orderRecord1, 9.0);
            orderItemList.add(orderItem1);
            
            OrderItem orderItem2 = TestDataUtility.createOrderItem(standaloneProdPBE, orderRecord2, 10.0);
            orderItemList.add(orderItem2);
        insert orderItemList;
        
        // Creating opportunity Line item
        List<OpportunityLineItem> olis=new List<OpportunityLineItem>();
        OpportunityLineItem standaloneProdOLI=TestDataUtility.createOpptyLineItem(standaloneProd, standaloneProdPBE, oppOpen);
        olis.add(standaloneProdOLI);
        OpportunityLineItem parentProd1OLI=TestDataUtility.createOpptyLineItem(parentProd1, parentProd1PBE, oppOpen);
        olis.add(parentProd1OLI);
        OpportunityLineItem childProd1OLI=TestDataUtility.createOpptyLineItem(childProd1, childProd1PBE, oppOpen);
        olis.add(childProd1OLI);
        OpportunityLineItem childProd2OLI=TestDataUtility.createOpptyLineItem(childProd2, childProd2PBE, oppOpen);
        olis.add(childProd2OLI);
        insert olis;
        
        
       
       
        
        ProductFamilyandGroupMap__c sapProductSetting = new ProductFamilyandGroupMap__c(
            Name = '1',
            Field_Name_1__c = 'SAP_Product__c',
            Field_Name_1_Value__c = 'HOSPITALITY_DISPLAY',
            Field_Name_2__c = 'SAP_Product_Group__c',
            Field_Name_2_Value__c = 'MONITOR',
            Product_Family__c = 'MONITOR [C1]',
            Product_Group__c = 'HOSPITALITY_DISPLAY'
        );
        
        ProductFamilyandGroupMap__c attrProductSetting = new ProductFamilyandGroupMap__c(
            Name = '10',
            Field_Name_1__c = 'ATTRIB06__c',
            Field_Name_1_Value__c = 'FEATURE',
            Field_Name_2__c = 'SAP_Product_Group__c',
            Field_Name_2_Value__c = 'MOBILE',
            Product_Family__c = 'MOBILE PHONE [G1]',
            Product_Group__c = 'FEATURE_PHONE'
        );
        insert new List<ProductFamilyandGroupMap__c>{sapProductSetting, attrProductSetting};

        quot = TestDataUtility.createQuote('Test Quote', oppOpen, pb);
        quot.ROI_Requestor_Id__c=UserInfo.getUserId();
        quot.Product_Division__c = 'MONITOR [C1]';
        quot.ProductGroupMSP__c = 'MOBILE PHONE [G1];MONITOR [C1];FEATURE_PHONE;HOSPITALITY_DISPLAY';
        insert quot;
        
        quot2 = TestDataUtility.createQuote('Test Quote', noPB, pb);
        quot2.ROI_Requestor_Id__c=UserInfo.getUserId();
        quot2.Product_Division__c = 'MONITOR [C1]';
        quot2.ProductGroupMSP__c = 'MOBILE PHONE [G1];MONITOR [C1];FEATURE_PHONE;HOSPITALITY_DISPLAY';
        insert quot2;
        
        List<QuoteLineItem> qlis = new List<QuoteLineItem>();
        QuoteLineItem standardQLI = TestDataUtility.createQuoteLineItem(quot.Id, standaloneProd, standaloneProdPBE, standaloneProdOLI);
        standardQLI.Quantity = 25;
        standardQLI.UnitPrice = 100;
        standardQLI.Requested_Price__c=100;
        standardQLI.OLIID__c = standaloneProdOLI.id;
        qlis.add(standardQLI);
        insert qlis;
    }
    
    @isTest static void constructorTest1() {
        setup();
        Test.StartTest();
        oppOpen.StageName='Qualified';
        update oppOpen;
            PageReference pageRef = Page.NewQuotePage;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('id', oppOpen.Id);
            ApexPages.StandardController controller = new ApexPages.StandardController(oppOpen);
            NewQuoteController extension = new NewQuoteController(controller);
        Test.StopTest();
        System.assertEquals(false,extension.GroupLineItemSection);
    }
    
    @isTest static void methodTest1() {
        setup();
        Test.StartTest();
         oppOpen.StageName='Qualified';
        update oppOpen;
            PageReference pageRef = Page.NewQuotePage;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('id', oppOpen.Id);
            ApexPages.currentPage().getParameters().put('quoteid', quot.Id);
            ApexPages.StandardController controller = new ApexPages.StandardController(oppOpen);
            NewQuoteController extension = new NewQuoteController(controller);
            extension.selProductDivision = 'MOBILE PHONE [G1]';
            extension.reloadProdGroup();
            extension.PrevStep();
            extension.nextStep();
            extension.stepno = 1;
            extension.nextStep();
            extension.getSelected();
            extension.showSaveNewQuoteButton();
            extension.masterselect = true;
            extension.selectAll();
        Test.StopTest();
        System.assertEquals(true,extension.GroupLineItemSection);
    }
    
    @isTest static void methodTest2() {
        setup();
        Test.StartTest();
         oppOpen.StageName='Qualified';
        update oppOpen;
            PageReference pageRef = Page.NewQuotePage;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('id', oppOpen.Id);
            ApexPages.currentPage().getParameters().put('quoteid', quot.Id);
            ApexPages.StandardController controller = new ApexPages.StandardController(oppOpen);
            NewQuoteController extension = new NewQuoteController(controller);
            extension.selprodGroup.add(new SelectOption('Test1','Test1')); 
            extension.selprodGroup.add(new SelectOption('Test12','Test12'));
            extension.selprodGroup.add(new SelectOption('MONITOR [C1]','MONITOR [C1]')); 
            extension.selprodGroup.add(new SelectOption('HOSPITALITY_DISPLAY','HOSPITALITY_DISPLAY'));
            extension.selProductDivision = 'MONITOR [C1]';
            extension.getProdGroups();
            extension.getSelected();
            extension.showSaveNewQuoteButton();
        Test.StopTest();
        System.assertEquals(true,extension.GroupLineItemSection);
    }
    
    @isTest static void methodTest3() {
        setup();
        Test.StartTest();
         oppOpen.StageName='Qualified';
        update oppOpen;
            PageReference pageRef = Page.NewQuotePage;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('id', oppOpen.Id);
            ApexPages.currentPage().getParameters().put('quoteid', quot.Id);
            ApexPages.StandardController controller = new ApexPages.StandardController(oppOpen);
            NewQuoteController extension = new NewQuoteController(controller);
            extension.selprodGroup.add(new SelectOption('Test1','Test1')); 
            extension.selprodGroup.add(new SelectOption('Test12','Test12'));
            extension.selprodGroup.add(new SelectOption('MONITOR [C1]','MONITOR [C1]')); 
            extension.selprodGroup.add(new SelectOption('HOSPITALITY_DISPLAY','HOSPITALITY_DISPLAY'));
            extension.selProductDivision = 'MONITOR [C1]';
            extension.nextStep();
            extension.getProdGroups();
            extension.getSelected();
            extension.getExistingQuotes();
            extension.showSaveNewQuoteButton();
            extension.addGrouporLineItemToExisting();
            extension.stepno = 2;
            extension.getQuoteLineItems();
            extension.getOppLineItems();
            extension.SaveSelectedQuotewithLineItems();
            extension.createNewQuote();
        Test.StopTest();
        System.assertEquals(true,extension.GroupLineItemSection);
    }
    
    @isTest static void methodTest4() {
        setup();
        Test.StartTest();
         oppOpen.StageName='Qualified';
        update oppOpen;
            PageReference pageRef = Page.NewQuotePage;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('id', oppOpen.Id);
            ApexPages.currentPage().getParameters().put('quoteid', quot.Id);
            ApexPages.StandardController controller = new ApexPages.StandardController(oppOpen);
            NewQuoteController extension = new NewQuoteController(controller);
            extension.selprodGroup.add(new SelectOption('Test1','Test1')); 
            extension.selprodGroup.add(new SelectOption('Test12','Test12'));
            extension.selprodGroup.add(new SelectOption('MONITOR [C1]','MONITOR [C1]')); 
            extension.selprodGroup.add(new SelectOption('HOSPITALITY_DISPLAY','HOSPITALITY_DISPLAY'));
            extension.selProductDivision = 'MONITOR [C1]';
            extension.getProdGroups();
            extension.getSelected();
            extension.showSaveNewQuoteButton();
            extension.getExistingQuotes();
            extension.addGrouporLineItemToExisting();
            extension.stepno = 2;
            extension.getQuoteLineItems();
            extension.getOppLineItems();
            extension.selectAll();
            extension.SaveSelectedQuotewithLineItems();
            //Validate From Date > Expiration Date Error
            extension.newQ.Valid_From_Date__c = system.today()+95;
            extension.newQ.ExpirationDate = system.today();
            extension.createNewQuote();
            //MPS__c Null Error
            extension.newQ.ExpirationDate = system.today()+95;
            extension.newQ.Valid_From_Date__c = system.today();
            extension.selProductDivision = 'PRINTER [C2]';
            extension.newQ.RequestType__c = 'TestType';
            extension.createNewQuote();
            //MPS Not Null && Supplies_Buying_Location__c Not Null Case
            extension.newQ.Supplies_Buying_Location__c = 'Test Supplies Buying Location';
            extension.newQ.MPS__c = 'Test MPS';
            extension.createNewQuote();
            //CreateNewQuote Without Error With Empty lstOliWrp List
            extension.selProductDivision = 'MONITOR [C1]';
            extension.createNewQuote();
            //CreateNewQuote Without Error With valid lstOliWrp List
            extension.getOppLineItems();
            System.debug(' lstOliWrp '+extension.lstOliWrp);
            extension.lstOliWrp[0].isSelected = true;
            extension.selProductDivision = 'PRINTER [C2]';
            extension.createNewQuote();
            
            
        Test.StopTest();
        System.assertEquals(true,extension.GroupLineItemSection);
    }
    
    @isTest static void methodTest5() {
        setup();
        Test.StartTest();
         oppOpen.StageName='Qualified';
        update oppOpen;
        /*    PageReference pageRef = Page.NewQuotePage;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('id', noPB.Id);
            ApexPages.currentPage().getParameters().put('quoteid', quot2.Id);
            ApexPages.StandardController controller = new ApexPages.StandardController(noPB);
            NewQuoteController extension = new NewQuoteController(controller);
            extension.selprodGroup.add(new SelectOption('Test1','Test1')); 
            extension.selprodGroup.add(new SelectOption('Test12','Test12'));
            extension.selprodGroup.add(new SelectOption('MONITOR [C1]','MONITOR [C1]')); 
            extension.selprodGroup.add(new SelectOption('HOSPITALITY_DISPLAY','HOSPITALITY_DISPLAY'));
            extension.selProductDivision = 'MONITOR [C1]';
            extension.stepno = 2;
            // No Product Error
            extension.newQ.RequestType__c = 'TestType';
            extension.createNewQuote();
            //selQuote Null Error
            extension.selQuote = null;
            extension.SaveSelectedQuotewithLineItems(); */
        Test.StopTest();
        //System.assertEquals(false,extension.GroupLineItemSection);
    }
    
}