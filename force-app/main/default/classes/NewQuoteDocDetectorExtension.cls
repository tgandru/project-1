/*
 * Description: This Apex extension will be used to generate
 * Quote PDF History on the Quote PDF object.
 *
 * Created By: Mayank S
 *
 * Created Date: Mar 30, 2016
 * 
 * Revisions: NIL
*/


public class NewQuoteDocDetectorExtension {
    public Id quoteId {
        get;
        set;
    }

    public Boolean pdfDetected {
        get;
        set;
    }

    public NewQuoteDocDetectorExtension( ApexPages.StandardController controller ) {
        quoteId = controller.getRecord().Id;
    }

    public void generateHistory() {
        System.debug('QuoteID is: ' + quoteId);

        Integer noOfDocs = [
            SELECT  COUNT()
            FROM    QuoteDocument
            WHERE   QuoteId = :quoteId
        ];
        System.debug('No of current PDFs is: ' + noOfDocs );
        
        
        Integer noOfHistories = [
            SELECT  COUNT()
            FROM    QuotePDFHistory__c 
            WHERE   Quote__c = :quoteId
        ];
        System.debug('No of current quote history records is: ' + noOfHistories );

        pdfDetected = false;
        if( noOfDocs > noOfHistories ) {
            Quote updatedQuote = [
                SELECT  Distributor__r.Name,
                        Distributor__c,
                        PricingNotes__c,
                        (
                            SELECT  Partner_Reseller__r.PartnerName__c
                            FROM    Resellers__r 
                        )
                FROM    Quote
                WHERE   Id = :quoteId   
            ];
            System.debug('The Distributor and Resellers have been picked up from the Quote' + updatedQuote.Distributor__r.Name );

            QuoteDocument newQuoteDoc = [
                SELECT      Name
                FROM        QuoteDocument
                WHERE       QuoteId = :quoteId
                ORDER BY    CreatedDate DESC
                LIMIT       1
            ];
            System.debug('The Quote PDF name has been picked up from the QuoteDocument object: ' + newQuoteDoc.Name );
         
            List<String> resellers = new List<String>();
            for( Reseller__c reseller : updatedQuote.Resellers__r ) {
                resellers.add( reseller.Partner_Reseller__r.PartnerName__c );
            }
            System.debug('The resellers on the quote are: ' + resellers );

            INSERT new QuotePDFHistory__c(
                Quote_PDF_Name__c             = newQuoteDoc.Name,
                Distributor_Record_ID__c      = updatedQuote.Distributor__c,
                Quote__c                      = quoteId,
                Pricing_Notes__c              = updatedQuote.PricingNotes__c,
                Distributor__c                = updatedQuote.Distributor__r.Name,
                Resellers__c                  = String.join( resellers, ',' )
            );
            System.debug('A new Quote PDF History record has been inserted' );

            pdfDetected = true;
        }
    }
}