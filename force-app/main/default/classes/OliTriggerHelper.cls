/*automatic adding of quote line item deleted 6.18 on direction of paul mraz 
because of new ability for user to manually decide which new oli's to add as qli's */

public class OliTriggerHelper {
    
    public static id ServiceRecTypeId = Opportunity.sObjectType.getDescribe().getRecordTypeInfosByName().get('Services').getRecordTypeId();
    
    public static void BeforeInsertHelper(list<OpportunityLineItem> triggerNew){
        for(Opportunitylineitem oli : triggerNew){
            oli.Claimed_Quantity__c = null;
        }
    }
    
    public static void AfterInsertHelper(list<OpportunityLineItem> triggerNew){
        
        list<Roll_Out_Product__c> rolloutProductTOInsert = new list<Roll_Out_Product__c>();
        /*dleeted 6.18 on direction of paul mraz: list<QuoteLineItem> qliToInsert = new list<QuoteLineItem>();*/ 
        
        AfterInsertHelper(triggerNew, rolloutProductTOInsert); /*dleeted 6.18 on direction of paul mraz: , qliToInsert*/

        if(rolloutProductTOInsert.size()!=0){
            insert rolloutProductTOInsert;
        }

        /* dleeted 6.18 on direction of paul mraz
        if(qliToInsert.size()!=0){
            insert qliToInsert;
        }*/
    }



    public static void AfterInsertHelper(list<OpportunityLineItem> triggerNew, list<Roll_Out_Product__c> rolloutProductTOInsert){ /*dleeted 6.18 on direction of paul mraz, parameter : list<QuoteLineItem> qliToInsert*/
        // as fit, create rollout product line
        // as fit, create quote line item
        
        
        map<string,Quote> quoteMap = new map<string,Quote>();
        
        set<id> oppId = new set<id>();
        set<id> rolloutPlanId = new set<id>();
        list<Roll_Out__c> newRollOuts = new list<Roll_Out__c>();
        Set<Id> opptyWithNoRollOut = new Set<Id>();
        Set<Id> toUpdateOpps = new Set<Id>();
        List<Opportunity> opptyToUpdateList = new List<Opportunity>();
        
        for(OpportunityLineItem oli : triggerNew){
            if(oli.Alternative__c == true) continue;
            
            if(string.isNotBlank(oli.hasRollout__c)){
                rolloutPlanID.add(oli.hasRollout__c);
            }
            else{
                opptyWithNoRollOut.add(oli.opportunityId);
            }
            oppId.add(oli.opportunityId);           
        }
        
        //Create Roll out plan for Opportunities with start date and duration populated
        if(opptyWithNoRollOut.size()>0){
            for(Opportunity opp: [Select Id,Roll_Out_Start__c,Rollout_Duration__c,Roll_Out_Status__c,Opportunity_Number__c,Recordtype.Name from Opportunity where Id IN: opptyWithNoRollOut]){
                        if(opp.Roll_Out_Start__c!=null && opp.Rollout_Duration__c!=null && opp.Recordtype.Name !='HA Builder'){//Added Filter Condition - Vijay 
                    newRollOuts.add(new Roll_Out__c(Name = opp.Opportunity_Number__c+ '- Rollout',Opportunity__c = opp.Id, Roll_Out_Start__c = opp.Roll_Out_Start__c, Rollout_Duration__c = opp.Rollout_Duration__c, Roll_Out_Status__c = opp.Roll_Out_Status__c));    
                    toUpdateOpps.add(opp.Id);
                }
            }
            //Edit Start JeongHo 2017-10-23
            /*for(SBQQ__Quote__c sb : [SELECT Id, SBQQ__Opportunity2__c FROM SBQQ__Quote__c WHERE SBQQ__Primary__c = TRUE AND SBQQ__Opportunity2__c IN: opptyWithNoRollOut]){
                for(Roll_Out__c ro : newRollOuts){
                    if(ro.Opportunity__c == sb.SBQQ__Opportunity2__c) ro.Quote__c =  sb.Id;
                }
            }*/
            //Edit End JeongHo 2017-10-23
        }
        if(newRollOuts.size()>0){           
            System.debug('## Insert newRollOuts'+newRollOuts.size()+'::'+newRollOuts);
            insert newRollOuts;
        }
        
        // Update opportunities with Rollout Id
         for(Opportunity o: [Select Id,Roll_Out_Plan__c, (Select Id, Opportunity__c from Roll_Outs__r) from Opportunity where Id IN:toUpdateOpps]) {
            for(Roll_Out__c r: o.Roll_outs__r){
                o.Roll_Out_Plan__c = r.Id;
                opptyToUpdateList.add(o);
            }
         }
         
        if(opptyToUpdateList.size()>0){
            update opptyToUpdateList;
        }
        
        /* deleted 6.18 on direction of paul mraz
        for(Quote q : [select id, ProductGroupMSP__c, OpportunityId from Quote where OpportunityId in :oppId]){
            quoteMap.put(q.opportunityId+q.ProductGroupMSP__c, q);
        }
        
        for(OpportunityLineItem oli : triggerNew){
            if(oli.Alternative__c == true) continue;
            if(quoteMap.containsKey(oli.opportunityId+oli.Product_Group__c)){
                qliToInsert.add(createQLI(oli,quoteMap.get(oli.opportunityId+oli.Product_Group__c)));
            }
        }*/
        
        if(rolloutPlanID.size()!=0){
            map<id,Roll_Out__c> rollOutMap = new map<id,Roll_Out__c>([select id, Roll_Out_Start__c,Rollout_Duration__c,Opportunity__r.Recordtype.Name from Roll_Out__c where id in :rolloutPlanId ]); //Added Filter JeongHo
            for(OpportunityLineItem oli : triggerNew){
                if(oli.Alternative__c == true) continue;
                if(string.isNotBlank(oli.hasRollout__c)){
                   Roll_Out__c ropln=rollOutMap.get(oli.hasRollout__c);
                   if(ropln.Opportunity__r.Recordtype.Name !='HA Builder'){// Added By Vijay to Block HA OLIs on -11/15/2018
                       rolloutProductTOInsert.add(createRolloutProduct(oli,rollOutMap.get(oli.hasRollout__c)));
                   }
                    
                        
                    
                }               
            }
        }
    }
    
    
    public static void beforeUpdateHelper(map<id,OpportunityLineItem> triggerNewMap, map<id,OpportunityLineItem> triggerOldMap){
        // as fit, update rollout product line
        // as fit, update quote line item
        // as fit, delete quote line item & rollout product lines
        set<id> oliIdsToDelete = new set<id>();
        map<id,OpportunityLineItem> oliForAdds = new map<id,OpportunityLineItem>();
        map<id,OpportunityLineItem> oliForUpdatesQli = new map<id,OpportunityLineItem>();
        map<id,OpportunityLineItem> oliForUpdatesRp = new map<id,OpportunityLineItem>();
        
        //list<QuoteLineItem> qliToDelete = new list<QuoteLineItem>(); //qlitodelete here is no longer getting deleted. it is getting updated. 
        list<QuoteLineItem> qliToUpdate = new list<QuoteLineItem>();
        list<QuoteLineItem> qliToInsert = new list<QuoteLineItem>();
        list<Roll_Out_Product__c> rpDeleteList = new list<Roll_Out_Product__c>();
        list<Roll_Out_Product__c> rpUpdateList = new list<Roll_Out_Product__c>();
        list<Roll_Out_Product__c> rpInsertList = new list<Roll_Out_Product__c>();
        
        //delete count
        integer count=0;
        for(OpportunityLineItem oli : triggerNewMap.values()){
            count++;
            if(oli.Alternative__c== true){// For Alternative Products Unit Cost Should be 0
               oli.UnitPrice=0;
              }
            
            if(oli.Alternative__c == true && triggerOldMap.get(oli.id).Alternative__c == false){
                oliIdsToDelete.add(oli.id);
            } else if (oli.Alternative__c == false && triggerOldMap.get(oli.id).Alternative__c == true) {
                oliForAdds.put(oli.id,oli);
            } else if (oli.Alternative__c == false){
                if(oli.quantity != triggerOldMap.get(oli.id).quantity 
                    || oli.totalprice != triggerOldMap.get(oli.id).totalprice
                    || oli.Requested_Price__c != triggerOldMap.get(oli.id).Requested_Price__c || oli.AP1__c != triggerOldMap.get(oli.id).AP1__c ){
                    oliForUpdatesRp.put(oli.id,oli);
                }
                //oliForUpdatesQLI.put(oli.id,oli);   
            }

            /*if(oli.quantity != triggerOldMap.get(oli.id).quantity 
                    || oli.totalprice != triggerOldMap.get(oli.id).totalprice
                    || oli.Requested_Price__c != triggerOldMap.get(oli.id).Requested_Price__c
                    || oli.Alternative__c!= triggerOldMap.get(oli.Id).Alternative__c){*/
            if(oli.quantity != triggerOldMap.get(oli.id).quantity 
                    || oli.totalprice != triggerOldMap.get(oli.id).totalprice
                    || oli.Requested_Price__c != triggerOldMap.get(oli.id).Requested_Price__c
                    || oli.Alternative__c!= triggerOldMap.get(oli.Id).Alternative__c
                    || oli.Unit_Cost__c!= triggerOldMap.get(oli.Id).Unit_Cost__c){        
                oliForUpdatesQLI.put(oli.id,oli);   
            }
            
            //Added by Thiru 04/19/2018----for making updating the OLI description to QLI description
            //----Starts here---
            if(oli.Description != triggerOldMap.get(oli.id).Description){        
                oliForUpdatesQLI.put(oli.id,oli);  
            }
            //----Ends here---
            
        }
        System.debug('oliForUpdatesRp '+oliForUpdatesRp.size()+' oliForUpdatesQLI '+oliForUpdatesQLI.size());
        
        // QLI delete and update
        for(QuoteLineItem qli : [select id, OLIID__c, Alternative__c, Unit_Cost__c from QuoteLineItem where OLIID__c in :oliForUpdatesQLI.keySet()]){ //OLIID__c in :oliIdsToDelete OR 
            /*if(oliIdsToDelete.contains(qli.OLIID__c)){
                qliToDelete.add(qli); //no longer delete, just update. 
            } */ //else {
                updateQLI(oliForUpdatesQLI.get(qli.OLIID__c), qli); 
                qliToUpdate.add(qli);
                system.debug('QLI Unit Cost------>'+qli.Unit_Cost__c);
            //}
        }
        // roll out product detele and update
        for(Roll_Out_Product__c rp : [select id,Opportunity_Line_Item__c, Quote_Quantity__c, SalesPrice__c,AP1__c from Roll_Out_Product__c where Opportunity_Line_Item__c in :oliIdsToDelete OR Opportunity_Line_Item__c in :oliForUpdatesRp.keySet()]){
            if(oliIdsToDelete.contains(rp.Opportunity_Line_Item__c)){
                rpDeleteList.add(rp);
            } else {
                updateRolloutProduct(oliForUpdatesRp.get(rp.Opportunity_Line_Item__c), rp);
                rpUpdateList.add(rp);
            }
        }
        for(OpportunityLineItem oli : triggerNewMap.values()){
            oli.From_Batch__c=false;
        }
                
        // QLI and rollout product insert
        AfterInsertHelper(oliForAdds.values(), rpInsertList); /*, qliToInsert*/
        
        if(rpInsertList.size()!=0 ||rpUpdateList.size()!=0){
            rpUpdateList.addAll(rpInsertList);
            upsert rpUpdateList;
        }
        
        if(qliToInsert.size()!=0 ||qliToUpdate.size()!=0){
            //qliToUpdate.addAll(qliToInsert);
            upsert qliToUpdate;
        }
        
        //if(qliToDelete.size()!=0) delete qliToDelete; 

        /*if(!qliToDelete.isEmpty()){
            for(QuoteLineItem qli:qliToDelete){
                qli.Alternative__c=true;
            }
            update qliToDelete;
        }*/

        if(rpDeleteList.size()!=0) delete rpDeleteList;
    }
    
    public static void deleteHelper(map<id,OpportunityLineItem> triggerMap){
        // as fit, delete quote line item & rollout product lines
        list<QuoteLineItem> qliToDelete = new list<QuoteLineItem>();
        list<Roll_Out_Product__c> rpDeleteList = new list<Roll_Out_Product__c>();
        
        rpDeleteList = [select id,Opportunity_Line_Item__c from Roll_Out_Product__c where Opportunity_Line_Item__c in :triggerMap.keySet()];
        qliToDelete = [select id, OLIID__c from QuoteLineItem where OLIID__c in :triggerMap.keySet()];
        
        if(rpDeleteList!=null && rpDeleteList.size()>0) delete rpDeleteList;
        if(qliToDelete!=null && qliToDelete.size()>0) delete qliToDelete; 
    }
    

// helper methods


    public static void updateQLI(OpportunityLineItem oli, QuoteLineItem qli){
        qli.Alternative__c = oli.Alternative__c;
        qli.Claimed_Quantity__c = oli.Claimed_Quantity__c;
        qli.Closed_List_Price__c = oli.Closed_List_Price__c;
        qli.GM__c = oli.GM__c;
        if(oli.From_Batch__c!=true){
           qli.Requested_Price__c = oli.Requested_Price__c; 
        }
        
        qli.SAP_Material_Id__c = oli.SAP_Material_Id__c;
        qli.Unit_Cost__c = oli.Unit_Cost__c;
        qli.Quantity = oli.Quantity;
        qli.UnitPrice = oli.Requested_Price__c; // oli.UnitPrice;
        qli.Bundle_Flag__c=oli.Bundle_Flag__c;
        qli.Parent_Product__c=oli.Parent_Product__c;
        qli.Description = oli.Description;
        qli.Product_Type__c = oli.ProductType__c;
    }


    /*public static QuoteLineItem createQLI(OpportunityLineItem oli, Quote q){
        QuoteLineItem qli = new QuoteLineItem();
        qli.quoteId = q.id;
        qli.PricebookEntryId = oli.PricebookEntryId;
        
        qli.OLIID__c = oli.id;
        qli.Alternative__c = oli.Alternative__c;
        qli.Claimed_Quantity__c = oli.Claimed_Quantity__c;
        qli.Closed_List_Price__c = oli.Closed_List_Price__c;
        qli.GM__c = oli.GM__c;
        qli.Requested_Price__c = oli.Requested_Price__c;
        qli.SAP_Material_Id__c = oli.SAP_Material_Id__c;
        qli.Unit_Cost__c = oli.Unit_Cost__c;
        qli.Quantity = oli.Quantity;
        qli.UnitPrice = oli.Requested_Price__c; // oli.UnitPrice;
        qli.Bundle_Flag__c=oli.Bundle_Flag__c;
        qli.Parent_Product__c=oli.Parent_Product__c;
        return qli;
    }*/
    
    public static void updateRolloutProduct(OpportunityLineItem oli, Roll_Out_Product__c rp){
        rp.SalesPrice__c = oli.Requested_Price__c; //oli.totalPrice / oli.quantity;
        rp.Quote_Quantity__c = oli.quantity;
        rp.AP1__c=oli.AP1__c;
    }

    public static Roll_Out_Product__c createRolloutProduct(OpportunityLineItem oli, Roll_Out__c ro){
        Roll_Out_Product__c rp = new Roll_Out_Product__c();
        rp.Opportunity_Line_Item__c = oli.id;
        rp.Quote_Quantity__c = oli.quantity;
        rp.Roll_Out_Start__c = ro.Roll_Out_Start__c;
        rp.Roll_Out_Plan__c = ro.id;
        rp.SalesPrice__c = oli.Requested_Price__c; // (oli.TotalPrice / oli.Quantity);
        rp.Product__c = oli.productId__c;
        rp.Rollout_Duration__c = ro.Rollout_Duration__c;
        if(oli.Requested_Price__c != null ){
            rp.Quote_Revenue__c = oli.Requested_Price__c * oli.quantity;
        }
        rp.Opportunity_RecordType_Name__c=ro.Opportunity__r.Recordtype.Name;
        rp.Invoice_Price__c=oli.UnitPrice;
        rp.AP1__c=oli.AP1__c;
        return rp;
    }
    
    //Added by vijay starts here
    public static void calculateNoOfDevices(map<id, OpportunityLineItem> newOLI, map<id, OpportunityLineItem> oldOLI, boolean isDelete){
     system.debug('***Enter into Update device class logic***');
     /*
     
        map<id, OpportunityLineItem> oli = new map<id, OpportunityLineItem>();
       
        if(isDelete){
            oli.putall(oldOLI);
        }else{
            oli.putall(newOLI);
        }
        set<id> oppIds = new set<id>();
        for(OpportunityLineItem o : oli.values()){
           oppIds.add(o.opportunityID);         
        }
        system.debug('**OppIds**'+oppIds);
        map<string,Opportunity> opp = new map<string,Opportunity>();
        if(oppIds.size() > 0){
            system.debug('**Size >0**');
            set<string> productsGroups = new set<string>();
            productsGroups.add('TABLET');
            productsGroups.add('SMART_PHONE');  // and Product_Group__c =:productsGroups and  Opportunity.ProductGroupTest__c=:productsGroups
            for(AggregateResult o : [select sum(Quantity) qt,opportunityId from  OpportunityLineItem where id=:oli.keySet() and  (product2.Product_Group__c='SMART_PHONE' OR product2.Product_Group__c='TABLET'  ) and 	Alternative__c=false group by opportunityID   ]){
               double qt = double.valueOF(o.get('qt'));
               system.debug('***Quantity***'+qt);
               opp.put(string.valueOF(o.get('opportunityId')), new Opportunity(id=string.valueOF(o.get('opportunityId')),No_Of_devices__c=qt));
            }
        }
        if(opp.size() > 0){
            system.debug('finally update the field');
            update opp.values();
        }
        */
        Set<id> oppids=new set<id>();
         if(isDelete){
            for(OpportunityLineItem o : oldOLI.values()){
               oppIds.add(o.opportunityID);         
            }
                
        }else{
          for(OpportunityLineItem o : newOLI.values()){
               oppIds.add(o.opportunityID);         
            }  
        }
        map<string,Opportunity> opp = new map<string,Opportunity>();
        if(oppIds.size() > 0){
             for(AggregateResult o : [select sum(Quantity) qt,opportunityId from  OpportunityLineItem where opportunityId=:oppids and  (product2.Product_Group__c='SMART_PHONE' OR product2.Product_Group__c='TABLET'  ) and 	Alternative__c=false group by opportunityID   ]){
               double qt = double.valueOF(o.get('qt'));
               system.debug('***Quantity***'+qt);
               opp.put(string.valueOF(o.get('opportunityId')), new Opportunity(id=string.valueOF(o.get('opportunityId')),No_Of_devices__c=qt));
            }
        }
        if(opp.size() > 0){
            system.debug('finally update the field');
            update opp.values();
        }
    }
    
    //Method to store the Deleted OLIs to Deleted_Record_History Object--Added 03/07/2019
    public static void storeDeletedOLI(List<OpportunityLineItem> DeletedOLIs){
        System.debug('<----In OLI delete block---->');
        List<Deleted_Record_History__c> RecordsToInsert = new List<Deleted_Record_History__c>();
        //String HArecTypeId = Label.HA_Builder_Recordtypeid;
        Set<String> OppIds = new Set<String>();
        Map<string,string> OppRecTypeMap = new Map<string,string>();
        
        for(OpportunityLineItem oli : DeletedOLIs){
            OppIds.add(oli.OpportunityId);
        }
        for(Opportunity opp : [Select id,RecordtypeId,Recordtype.Name from Opportunity where id=:OppIds]){ // and RecordTypeId!=:HArecTypeId]){
            OppRecTypeMap.put(opp.id, opp.Recordtype.Name);
        }
        
        for(OpportunityLineItem oli : DeletedOLIs){
            if(OppRecTypeMap!=null && OppRecTypeMap.containsKey(oli.OpportunityId)){
                RecordsToInsert.add(new Deleted_Record_History__c(Object__c='Opportunity Product',Record_Type__c=OppRecTypeMap.get(oli.OpportunityId),Record_ID__c=oli.Id,Record_CreatedDate__c=oli.Createddate,Record_LastModifiedDate__c=oli.LastModifiedDate,Record_SystemModStamp__c=oli.SystemModstamp));
            }
        }
        if(RecordsToInsert.size()>0){ 
            system.debug('RecordsToInsert.size()-->'+RecordsToInsert.size());
            insert RecordsToInsert;
        }
    }
    
}