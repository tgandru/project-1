global class OneMinuteSceduler implements Schedulable{

	global void execute(SchedulableContext sc) {
		integer scheduleInterval = 1;
		
		try{
				Database.executeBatch(new BatchforHAContractIntegrationCall(), 1);				
		}
		catch(Exception ex){
			System.debug('Scheduled Job failed : ' + ex.getMessage());
		}


		Integer rqmi = scheduleInterval;

		DateTime timenow = DateTime.now().addMinutes(rqmi);

		OneMinuteSceduler rqm = new OneMinuteSceduler();
		
		String seconds = '0';
		String minutes = String.valueOf(timenow.minute());
		String hours = String.valueOf(timenow.hour());
		
		//Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
		String sch = seconds + ' ' + minutes + ' ' + hours + ' * * ?';
		String jobName = '1 min Scheduler - ' + hours + ':' + minutes;
		system.schedule(jobName, sch, rqm);
		
		if (sc != null)	{	
			system.abortJob(sc.getTriggerId());			
		}
	}
}