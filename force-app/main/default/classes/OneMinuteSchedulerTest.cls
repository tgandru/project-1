@isTest
private class OneMinuteSchedulerTest {
	
	public Static String CRON_EXP = '0 0 12 15 2 ?'; //To execute schedulable 

    private static String messageQueueId  = '007';
    private static String messageStatus  = 'Test';
    private static String messageQueueStatus = 'Not Started';
    private static Decimal messageQueueRetryCounter = 1;
    //Specifically testing PRMLeadStatusHelper callout
    private static String messageQueueIntegrationFlowType = 'Flow-016';
    private static String messageQueueIdentificationText = 'Test Message Queue';
    
	
	@testSetup static void createMessageQueue() {
	 	message_queue__c mq = new message_queue__c(Identification_Text__c = messageQueueIdentificationText, MSGUID__c = messageQueueId, 
	 												MSGSTATUS__c = messageStatus,  
	 												Status__c = messageQueueStatus, 
	 												retry_counter__c = messageQueueRetryCounter, 
	 												Integration_Flow_Type__c = messageQueueIntegrationFlowType);
	 	insert mq;
	}
    
    
    static testMethod void testScheduledJob() {
        
        Test.startTest();
        //Executing Schedulable Class   
        String jobId = System.schedule('TestScheduledBatch',CRON_EXP, new OneMinuteSceduler());
        
        // Get the information from the CronTrigger API object 
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        
        // Verify the expressions are the same     
        System.assertEquals(CRON_EXP, ct.CronExpression);
        
        // Verify the job has not run 
        
        System.assertEquals(0, ct.TimesTriggered);    
        Test.stopTest();    
    }
}