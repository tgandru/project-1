@RestResource(urlMapping='/OpportunityActivitySECPI/*')
global with sharing class OpportunityActivityRestResourceSECPI {
    public static final String RESOURCE_URI = '/OpportunityActivitySECPI/';
     
    @HttpGet
    global static SFDCStubSECPI.OpportunityActivityResponseSECPI getOpportunityActivitySECPI() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        
        String oppId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        System.debug(req.requestURI+'--> '+oppId);
        
        SFDCStubSECPI.OpportunityActivityResponseSECPI response = new SFDCStubSECPI.OpportunityActivityResponseSECPI();
        response.inputHeaders.MSGGUID = cihStub.giveGUID();
        response.inputHeaders.IFID = '';
        response.inputHeaders.IFDate = datetime.now().format('YYMMddHHmmss', 'America/New_York');
        response.inputHeaders.MSGSTATUS = 'S';
        response.inputHeaders.ERRORTEXT = '';
        response.inputHeaders.ERRORCODE = '';
        
        Integration_EndPoints__c endpoint = Integration_EndPoints__c.getInstance(SFDCStubSECPI.FLOW_083);
        DateTime lastSuccessfulRun; 
        DateTime currentDateTime = datetime.now();
        if(lastSuccessfulRun==null) {
            lastSuccessfulRun = currentDateTime - 1;
        }
        string status;
        
        Opportunity oppty = null;
        
        try {
            oppty = [SELECT Id,
                    (Select id,isalldayevent,ownerid,owner.name,isRecurrence,createdby.name,createddate,lastmodifiedby.name,lastmodifieddate,
                     EndDateTime,Location,who.name,StartDateTime,Subject,Type,Whatid,what.name,SamsungExec__c,SystemModStamp 
                     from Events),
                    (Select id,Priority,ActivityDate,CallType,Status,ReminderDateTime,ownerid,owner.name,isRecurrence,createdby.name,createddate,SystemModStamp,
                     lastmodifiedby.name,lastmodifieddate,who.name,Subject,Type,Whatid,what.name,Marketstar_Call_Duration__c,MarketStar_Call_Outcome__c,
                     Call_Outcome__c,Call_Type__c 
                     from Tasks),
                    (Select id,ActivitySubType,ActivityType,IsTask,Priority,ActivityDate,CallType,Status,ReminderDateTime,ownerid,owner.name,
                     createdby.name,createddate,lastmodifiedby.name,lastmodifieddate,EndDateTime,Location,who.name,IsAllDayEvent,
                     StartDateTime,Subject,Whatid,Marketstar_Call_Duration__c,MarketStar_Call_Outcome__c,Call_Outcome__c,Call_Type__c,
                     SamsungExec__c,what.name,SystemModStamp
                     from ActivityHistories)
                    FROM Opportunity WHERE Id = :oppId ALL ROWS];
                    
        } catch(Exception e) {
            String errmsg = 'DB Query Error - '+e.getmessage() + ' - '+e.getStackTraceString();
            response.inputHeaders.MSGGUID = cihStub.giveGUID();
            response.inputHeaders.IFID  = '';
            response.inputHeaders.IFDate    = datetime.now().format('YYMMddHHmmss', 'America/New_York');
            response.inputHeaders.MSGSTATUS = 'F';
            response.inputHeaders.ERRORTEXT = 'Fail to retrieve an Opportunity('+oppId+') - '+req.requestURI + ', errmsg=' + errmsg;
            response.inputHeaders.ERRORCODE = '';
            
            return response;
        }
        
        response.body.PAGENO = 1;
        response.body.ROWCNT = 500;
        response.body.COUNT = 1;
        
        integer NumOfRecords = 0;
        
        if(oppty.Events.size()>0){
            for(Event evnt : oppty.Events){
                NumOfRecords++;
                try {
                    SFDCStubSECPI.OppActivitySECPI p = new SFDCStubSECPI.OppActivitySECPI(evnt,null,null);
                    response.body.Activity.add(p);
                    system.debug('Record--------->'+response.body.Activity);
                } catch(Exception e) {
                    String errmsg = 'Fail to create an OpportunityWithProduct - '+e.getmessage() + ' - '+e.getStackTraceString();
                    message_queue__c pemsg = new message_queue__c(MSGUID__c = response.inputHeaders.MSGGUID, 
                                                        MSGSTATUS__c = 'F',  
                                                        Integration_Flow_Type__c = SFDCStubSECPI.FLOW_083,
                                                        IFID__c = response.inputHeaders.IFID,
                                                        IFDate__c = response.inputHeaders.IFDate,
                                                        Status__c = 'failed',
                                                        Object_Name__c = 'OpportunityActivity', 
                                                        retry_counter__c = 0);
                    pemsg.ERRORTEXT__c = errmsg;
                    pemsg.Identification_Text__c = oppty.Id;
                    insert pemsg;
                    System.debug(errmsg);
                }
            }
        }
        if(oppty.Tasks.size()>0){
            for(Task tsk : oppty.Tasks){
                NumOfRecords++;
                try {
                    SFDCStubSECPI.OppActivitySECPI p = new SFDCStubSECPI.OppActivitySECPI(null,tsk,null);
                    response.body.Activity.add(p);
                    system.debug('Record--------->'+response.body.Activity);
                } catch(Exception e) {
                    String errmsg = 'Fail to create an OpportunityWithProduct - '+e.getmessage() + ' - '+e.getStackTraceString();
                    message_queue__c pemsg = new message_queue__c(MSGUID__c = response.inputHeaders.MSGGUID, 
                                                        MSGSTATUS__c = 'F',  
                                                        Integration_Flow_Type__c = SFDCStubSECPI.FLOW_083,
                                                        IFID__c = response.inputHeaders.IFID,
                                                        IFDate__c = response.inputHeaders.IFDate,
                                                        Status__c = 'failed',
                                                        Object_Name__c = 'OpportunityActivity', 
                                                        retry_counter__c = 0);
                    pemsg.ERRORTEXT__c = errmsg;
                    pemsg.Identification_Text__c = oppty.Id;
                    insert pemsg;
                    System.debug(errmsg);
                }
            }
        }
        /*if(oppty.ActivityHistories.size()>0){
            for(ActivityHistory ahis : oppty.ActivityHistories){
                NumOfRecords++;
                try {
                    SFDCStubSECPI.OppActivitySECPI p = new SFDCStubSECPI.OppActivitySECPI(null,null,ahis);
                    response.body.Activity.add(p);
                    system.debug('Record--------->'+response.body.Activity);
                } catch(Exception e) {
                    String errmsg = 'Fail to create an OpportunityWithProduct - '+e.getmessage() + ' - '+e.getStackTraceString();
                    message_queue__c pemsg = new message_queue__c(MSGUID__c = response.inputHeaders.MSGGUID, 
                                                        MSGSTATUS__c = 'F',  
                                                        Integration_Flow_Type__c = SFDCStubSECPI.FLOW_083,
                                                        IFID__c = response.inputHeaders.IFID,
                                                        IFDate__c = response.inputHeaders.IFDate,
                                                        Status__c = 'failed',
                                                        Object_Name__c = 'OpportunityActivity', 
                                                        retry_counter__c = 0);
                    pemsg.ERRORTEXT__c = errmsg; pemsg.Identification_Text__c = oppty.Id; insert pemsg;
                    System.debug(errmsg);
                }
            }
        }*/
        
        //SFDCStubSECPI.OpportunityActivityEventSECPI p = new SFDCStubSECPI.OpportunityActivityEventSECPI(opp.Events);
        //response.body.ActivityEvent.add(p);
        
        return response;
    }
    
    @HttpPost
    global static SFDCStubSECPI.OpportunityActivityResponseSECPI getOpportunityActivityListSECPI() {
        integer pageNum;
        integer pageLimit = integer.valueOf(label.SECPI_Task_Activity_I_F_Page_Limit);
        integer EventpageLimit = integer.valueOf(label.SECPI_Event_Activity_I_F_Page_Limit);
        String RequestType;
        
        Integration_EndPoints__c endpoint = Integration_EndPoints__c.getInstance(SFDCStubSECPI.FLOW_083);
        String layoutId = !Test.isRunningTest() ? endpoint.layout_id__c : SFDCStubSECPI.LAYOUTID_083;
        
        Integration_EndPoints__c endpoint2 = Integration_EndPoints__c.getInstance(SFDCStubSECPI.FLOW_083_2);
        
        RestRequest req = RestContext.request;
        String postBody = req.requestBody.toString();
        
        SFDCStubSECPI.OpportunityActivityResponseSECPI response = new SFDCStubSECPI.OpportunityActivityResponseSECPI();
        
        SFDCStubSECPI.OpportunityActivityRequest request = null;
        
        try {
            request = (SFDCStubSECPI.OpportunityActivityRequest)json.deserialize(postBody, SFDCStubSECPI.OpportunityActivityRequest.class);
        } catch(Exception e) {
            response.inputHeaders.MSGGUID = '';
            response.inputHeaders.IFID  = '';
            response.inputHeaders.IFDate    = '';
            response.inputHeaders.MSGSTATUS = 'F';
            response.inputHeaders.ERRORTEXT = 'Invalid Request Message. - '+postBody;
            response.inputHeaders.ERRORCODE = '';
            
            return response;
        }
        
        response.inputHeaders.MSGGUID = request.inputHeaders.MSGGUID;
        response.inputHeaders.IFID    = request.inputHeaders.IFID;
        response.inputHeaders.IFDate  = request.inputHeaders.IFDate;
        
        string dates = 'From:'+request.body.SEARCH_DTTM_FROM+', To:'+request.body.SEARCH_DTTM_TO;
        
        List<message_queue__c> mqList = new List<message_queue__c>();
        message_queue__c mq = new message_queue__c(MSGUID__c = response.inputHeaders.MSGGUID, 
                                                    MSGSTATUS__c = 'S',  
                                                    Integration_Flow_Type__c = SFDCStubSECPI.FLOW_083,
                                                    IFID__c = response.inputHeaders.IFID,
                                                    IFDate__c = response.inputHeaders.IFDate,
                                                    External_Id__c = request.body.CONSUMER,
                                                    Status__c = 'Success',
                                                    Object_Name__c = 'OpportunityActivity', 
                                                    retry_counter__c = 0,
                                                    Identification_Text__c = dates);
        
        if(request.body.REQUEST_TYPE!=null && request.body.REQUEST_TYPE!='' ){
            RequestType = request.body.REQUEST_TYPE;
        }else{
            String errmsg = 'Request_Type is missing';
            response.inputHeaders.MSGSTATUS = 'F';
            response.inputHeaders.ERRORTEXT = errmsg;
            response.inputHeaders.ERRORCODE = 'REQUEST_TYPE_Missing';
            
            mq.Status__c = 'failed';
            mq.MSGSTATUS__c = 'F';
            mq.ERRORTEXT__c = errmsg;
            insert mq;
            
            return response;
        }
        
        if(request.body.PAGENO!=null){
            pageNum = request.body.PAGENO;
            System.debug('PageNum----->'+pageNum);
        }else{
            String errmsg = 'Page Number is Missing in the Request';response.inputHeaders.MSGSTATUS = 'F';response.inputHeaders.ERRORTEXT = errmsg;response.inputHeaders.ERRORCODE = '';mq.Status__c = 'failed';mq.MSGSTATUS__c = 'F';mq.ERRORTEXT__c = errmsg;
            insert mq;
            
            return response;
        }
        
        if((RequestType=='TASK' && pageNum>pageLimit)||(RequestType=='EVENT' && pageNum>EventpageLimit)){
            Datetime new_SEARCH_DTTM;
            if(RequestType=='TASK'){
                new_SEARCH_DTTM = endpoint.Last_Successful_Run_Last_Modified_Date__c;
            }
            if(RequestType=='EVENT'){
                new_SEARCH_DTTM = endpoint2.Last_Successful_Run_Last_Modified_Date__c;
            }
            System.debug('new SERACH_DTTM----->'+new_SEARCH_DTTM);
            String errmsg = 'SFDC PAGE LIMIT EXCEEDED. Please use new SEARCH_DTTM_FROM:'+new_SEARCH_DTTM+' and start from PAGENO:1 ' ;
            
            response.inputHeaders.MSGSTATUS = 'F';
            response.inputHeaders.ERRORTEXT = errmsg;
            response.inputHeaders.ERRORCODE = 'PAGE_LIMIT_EXCEEDED';
            response.inputHeaders.SEARCH_DTTM_FROM = SFDCStubSECPI.DtTimetoString(new_SEARCH_DTTM);
            mq.Object_Name__c = mq.Object_Name__c + '-'+RequestType+', '+ request.body.PAGENO;
            mq.Status__c = 'failed';
            mq.MSGSTATUS__c = 'F';
            mq.ERRORTEXT__c = errmsg;
            insert mq;
            
            return response;
        }
        
        //Block the callout if the Sent data delete batch is in process.
        List<AsyncApexJob> DeleteBatchProcessing = [select id,TotalJobItems,status,createddate from AsyncApexJob where apexclass.name='Batch_OpptyActivityDataToSECPI_Delete' and (status='Holding' or status='Queued' or status='Preparing' or status='Processing') order by createddate desc limit 1];
        if(DeleteBatchProcessing.size()>0){    
            response.inputHeaders.MSGSTATUS = 'F'; response.inputHeaders.ERRORTEXT = 'The Sent Data Delete Batch is in Process. Try callout after 10 mins'; response.inputHeaders.ERRORCODE = '';
            mq.Status__c = 'failed'; mq.MSGSTATUS__c = 'F'; mq.ERRORTEXT__c = 'The Sent Data Delete Batch is in Process. Try callout after 10 mins';
            insert mq;
            
            return response;
        }
        
        string DateFormat = 'yyyymmddhhmmss';
        string DateFormat2 = 'yyyymmdd';
        
        //Added the logic for New request format. Request includes FROM and TO SEARCH_DTTM--05/16/2018
        //Checking the date format.
        if((request.body.SEARCH_DTTM_FROM!=null && request.body.SEARCH_DTTM_FROM!='' && request.body.SEARCH_DTTM_FROM.length()!=DateFormat.length() && request.body.SEARCH_DTTM_FROM.length()!=DateFormat2.length())
            || (request.body.SEARCH_DTTM_TO!=null && request.body.SEARCH_DTTM_TO!='' && request.body.SEARCH_DTTM_TO.length()!=DateFormat.length() && request.body.SEARCH_DTTM_TO.length()!=DateFormat2.length())){
            String errmsg = 'Incorrect SEARCH_DTTM format. The Date Format must be yyyymmddhhmmss or yyyymmdd';
            response.inputHeaders.MSGSTATUS = 'F';
            response.inputHeaders.ERRORTEXT = errmsg;
            response.inputHeaders.ERRORCODE = 'Incorrect SEARCH_DTTM format';
            
            mq.Status__c = 'failed';
            mq.MSGSTATUS__c = 'F';
            mq.ERRORTEXT__c = errmsg;
            insert mq;
            
            return response;
        }
        
        //Check the Missng SEARCH_DTTM_TO and SEARCH_DTTM_FROM
        if((((request.body.SEARCH_DTTM_TO==null || request.body.SEARCH_DTTM_TO=='') && request.body.SEARCH_DTTM_FROM!=null && request.body.SEARCH_DTTM_FROM!=''))
            || (((request.body.SEARCH_DTTM_FROM==null || request.body.SEARCH_DTTM_FROM=='') && request.body.SEARCH_DTTM_TO!=null && request.body.SEARCH_DTTM_TO!=''))){
            String errmsg = 'SEARCH_DTTM_FROM or SEARCH_DTTM_TO is missing. The request should include both FROM and TO dates while sending TimeStamps';
            response.inputHeaders.MSGSTATUS = 'F';
            response.inputHeaders.ERRORTEXT = errmsg;
            response.inputHeaders.ERRORCODE = '';
            
            mq.Status__c = 'failed';
            mq.MSGSTATUS__c = 'F';
            mq.ERRORTEXT__c = errmsg;
            insert mq;
            
            return response;
        }
        
        if(request.body.SEARCH_DTTM_FROM!=null && request.body.SEARCH_DTTM_FROM!='' && request.body.SEARCH_DTTM_FROM.length()==DateFormat2.length()){
            request.body.SEARCH_DTTM_FROM=request.body.SEARCH_DTTM + '000000';
        }
        if(request.body.SEARCH_DTTM_TO!=null && request.body.SEARCH_DTTM_TO!='' && request.body.SEARCH_DTTM_TO.length()==DateFormat2.length()){
            request.body.SEARCH_DTTM_TO=request.body.SEARCH_DTTM + '000000';
        }
        
        DateTime lastSuccessfulRun;
        if(RequestType=='TASK'){
            lastSuccessfulRun= endpoint.last_successful_run__c;
        }
        if(RequestType=='EVENT'){
            lastSuccessfulRun= endpoint2.last_successful_run__c;
        }
        DateTime currentDateTime = datetime.now();
        DateTime SearchDateTo;
        DateTime SearchDateFrom;
        
        //Set From and To timestamps for Query
        if((request.body.SEARCH_DTTM_FROM!=null && request.body.SEARCH_DTTM_FROM!='')
            &&(request.body.SEARCH_DTTM_TO!=null && request.body.SEARCH_DTTM_TO!='')){
            SearchDateTo = setDateFromString(request.body.SEARCH_DTTM_TO);
            system.debug('SearchDateTo----->'+SearchDateTo);
            SearchDateFrom = setDateFromString(request.body.SEARCH_DTTM_FROM);
            system.debug('SearchDateFrom----->'+SearchDateFrom);
        }else{
            SearchDateTo = currentDateTime;
            system.debug('SearchDateTo----->'+SearchDateTo);
            SearchDateFrom = lastSuccessfulRun;
        }
        
        //Checing the SEARCH_DTTM_FROM and SEARCH_DTTM_TO time difference
        integer SecsDiff = Integer.valueOf((SearchDateTo.getTime() - SearchDateFrom.getTime())/(1000));
        if(SecsDiff>86400){
            String errmsg = 'Difference between SEARCH_DTTM_FROM and SEARCH_DTTM_TO should be less or Equal to 1 day';
            response.inputHeaders.MSGSTATUS = 'F';
            response.inputHeaders.ERRORTEXT = errmsg;
            response.inputHeaders.ERRORCODE = '';
            
            mq.Status__c = 'failed';
            mq.MSGSTATUS__c = 'F';
            mq.ERRORTEXT__c = errmsg;
            insert mq;
            
            return response;
        }
        
        Integer limitCnt = SFDCStubSECPI.LIMIT_COUNT;
        
        List<Opportunity> data = null;
        Set<id> Oppids = new Set<id>();
        
        DateTime LastModifiedDate = null;
        integer NumOfRecords = 0;
        integer NumOfEmailReords = 0;
        
        List<Oppty_Activity_Data_to_SECPI__c> storeIds = new List<Oppty_Activity_Data_to_SECPI__c>();
        List<Oppty_Activity_Event_Data_to_SECPI__c> EventstoreIds = new List<Oppty_Activity_Event_Data_to_SECPI__c>();
        
        List<String> excludedUsers = Label.Users_Excluded_from_Integration.split(',');//Added to exclude data last modified by these users----03/20/2019
        
        List<Event> eventList = new List<Event>();
        
        if(RequestType=='EVENT'){
            System.debug('In Event Request Block');
            Set<string> sentActIds = new Set<string>();
            List<Oppty_Activity_Event_Data_to_SECPI__c> AllData = Oppty_Activity_Event_Data_to_SECPI__c.getall().values();
            system.debug('size------->'+AllData.size());
            for(Oppty_Activity_Event_Data_to_SECPI__c dataSend : AllData){
                sentActIds.add(dataSend.name);
            }
            
            eventList = [Select id,isalldayevent,ownerid,owner.name,isRecurrence,createdby.name,createddate,lastmodifiedby.name,lastmodifieddate,
                                     EndDateTime,Location,who.name,StartDateTime,Subject,Type,Whatid,what.name,Call_Type__c,Call_Outcome__c,
                                     Marketstar_Call_Duration__c,MarketStar_Call_Outcome__c,SamsungExec__c,ActivityDate,ReminderDateTime,SystemModStamp 
                                     from Event 
                                     where LastModifiedById!=:excludedUsers 
                                           and what.Type='Opportunity' 
                                           and SystemModStamp >= :SearchDateFrom 
                                           and SystemModStamp <= :SearchDateTo 
                                           and id!=:sentActIds 
                                           ORDER BY SystemModStamp ASC LIMIT :limitCnt];
            system.debug('EventList Size------------->'+eventList.size());
            for(Event evnt : eventList){
                
                Oppty_Activity_Event_Data_to_SECPI__c storeId = new Oppty_Activity_Event_Data_to_SECPI__c(name=evnt.id);
                EventstoreIds.add(storeId);
                
                string whtid = evnt.whatid;
                if(whtid!=null && whtid.startsWith('006')){
                    NumOfRecords++;
                    Oppids.add(evnt.whatid);
                    try {
                        SFDCStubSECPI.OppActivitySECPI p = new SFDCStubSECPI.OppActivitySECPI(evnt,null,null);
                        response.body.Activity.add(p);
                        LastModifiedDate = p.EventInstance.SystemModStamp;
                        //system.debug('Record--------->'+response.body.Activity);
                    } catch(Exception e) {
                        String errmsg = 'Fail to create an Activity - '+e.getmessage() + ' - '+e.getStackTraceString();
                        message_queue__c pemsg = new message_queue__c(MSGUID__c = response.inputHeaders.MSGGUID, 
                                                            MSGSTATUS__c = 'F',  
                                                            Integration_Flow_Type__c = SFDCStubSECPI.FLOW_083,
                                                            IFID__c = response.inputHeaders.IFID,
                                                            IFDate__c = response.inputHeaders.IFDate,
                                                            Status__c = 'failed',
                                                            Object_Name__c = 'OpportunityActivity', 
                                                            retry_counter__c = 0);
                        pemsg.ERRORTEXT__c = errmsg;
                        pemsg.Identification_Text__c = evnt.Id;
                        mqList.add(pemsg);
                        //insert pemsg;
                        System.debug(errmsg);
                    }
                }
            }
            if(eventList.size()>0){
                system.debug('EVENT Last_Successful_Run_Last_Modified_Date__c----->'+eventList[eventList.size()-1].systemmodstamp);
                endpoint2.Last_Successful_Run_Last_Modified_Date__c = eventList[eventList.size()-1].systemmodstamp;
            }
        }
        
        List<Task> taskList = new List<Task>();
        
        if(RequestType=='TASK'){
            System.debug('In TASK Request Block');
            Set<string> sentActIds = new Set<string>();
            List<Oppty_Activity_Data_to_SECPI__c> AllData = Oppty_Activity_Data_to_SECPI__c.getall().values();
            system.debug('size------->'+AllData.size());
            for(Oppty_Activity_Data_to_SECPI__c dataSend : AllData){
                sentActIds.add(dataSend.name);
            }
            
            taskList = [Select id,Priority,ActivityDate,CallType,Status,ReminderDateTime,ownerid,owner.name,isRecurrence,createdby.name,createddate,
                                   lastmodifiedby.name,lastmodifieddate,who.name,Subject,Type,Whatid,what.name,Marketstar_Call_Duration__c,MarketStar_Call_Outcome__c,
                                   Call_Outcome__c,Call_Type__c ,SamsungExec__c,SystemModStamp
                                   from Task 
                                   where LastModifiedById!=:excludedUsers 
                                         and what.Type='Opportunity' 
                                         and SystemModStamp >= :SearchDateFrom 
                                         and SystemModStamp <= :SearchDateTo 
                                         and id!=:sentActIds 
                                         ORDER BY SystemModStamp ASC LIMIT :limitCnt];
            system.debug('TaskList Size------------->'+taskList.size());
            for(Task tsk : taskList){
                
                Oppty_Activity_Data_to_SECPI__c storeId = new Oppty_Activity_Data_to_SECPI__c(name=tsk.id);
                storeIds.add(storeId);
                
                string whtid = tsk.whatid;
                if(whtid!=null && whtid.startsWith('006')){
                    NumOfRecords++;
                    Oppids.add(tsk.whatid);
                    try {
                        SFDCStubSECPI.OppActivitySECPI p = new SFDCStubSECPI.OppActivitySECPI(null,tsk,null);
                        response.body.Activity.add(p);
                        LastModifiedDate = p.TaskInstance.SystemModStamp;
                        //system.debug('Record--------->'+response.body.Activity);
                    } catch(Exception e) {
                        String errmsg = 'Fail to create an Activity - '+e.getmessage() + ' - '+e.getStackTraceString();
                        message_queue__c pemsg = new message_queue__c(MSGUID__c = response.inputHeaders.MSGGUID, 
                                                            MSGSTATUS__c = 'F',  
                                                            Integration_Flow_Type__c = SFDCStubSECPI.FLOW_083,
                                                            IFID__c = response.inputHeaders.IFID,
                                                            IFDate__c = response.inputHeaders.IFDate,
                                                            Status__c = 'failed',
                                                            Object_Name__c = 'OpportunityActivity', 
                                                            retry_counter__c = 0);
                        pemsg.ERRORTEXT__c = errmsg;
                        pemsg.Identification_Text__c = tsk.Id;
                        //insert pemsg;
                        mqList.add(pemsg);
                        System.debug(errmsg);
                    }
                }
            }
            
            if(taskList.size()>0){
                system.debug('TASK Last_Successful_Run_Last_Modified_Date__c----->'+taskList[taskList.size()-1].systemmodstamp);
                endpoint.Last_Successful_Run_Last_Modified_Date__c = taskList[taskList.size()-1].systemmodstamp;
            }
        }
        
        response.inputHeaders.MSGSTATUS = 'S';
        response.inputHeaders.ERRORTEXT = '';
        response.inputHeaders.ERRORCODE = '';
        
        response.body.PAGENO = request.body.PAGENO;
        response.body.ROWCNT = limitCnt;
        response.body.COUNT  = NumOfRecords;
        
        mq.last_successful_run__c = currentDateTime;
        mq.Object_Name__c = mq.Object_Name__c + '-'+RequestType+', '+ request.body.PAGENO + ', ' + response.body.COUNT;
        //insert mq;
        mqList.add(mq);
        
        if(mqList.size()>0){
            insert mqList;
        }
        
        if(!Test.isRunningTest() && response.body.COUNT>=300 && pageNum<pageLimit) {
            if(storeIds.size()>0){
                insert storeIds;
            }
            if(EventstoreIds.size()>0){
                insert EventstoreIds;
            }
            system.debug('insert block');
            
        //}else if(!Test.isRunningTest() && response.body.ROWCNT!=eventList.size() && response.body.ROWCNT!=taskList.size() && response.body.ROWCNT!=emailList.size()) {
        }else if(!Test.isRunningTest() && (response.body.COUNT<300 || pageNum==pageLimit)) {
            system.debug('Delete block');
            /*endpoint.last_successful_run__c = currentDateTime;
            system.debug('New Last_Successful_Run_Last_Modified_Date__c----->'+endpoint.Last_Successful_Run_Last_Modified_Date__c);
            update endpoint;*/
            
            if(RequestType=='EVENT'){
                endpoint2.last_successful_run__c = currentDateTime;
                system.debug('New Last_Successful_Run_Last_Modified_Date__c----->'+endpoint2.Last_Successful_Run_Last_Modified_Date__c);
                update endpoint2;
                integer count= database.countQuery('select count() from Oppty_Activity_Event_Data_to_SECPI__c limit 10002');
                if(count>10000){
                    Batch_OpptyEventActDataToSECPI_Delete ba = new Batch_OpptyEventActDataToSECPI_Delete();
                    Database.executeBatch(ba);
                }else{
                    List<Oppty_Activity_Event_Data_to_SECPI__c> deleteData = [select id from Oppty_Activity_Event_Data_to_SECPI__c];
                    delete deleteData;
                }
            }
            if(RequestType=='TASK'){
                endpoint.last_successful_run__c = currentDateTime;
                system.debug('New Last_Successful_Run_Last_Modified_Date__c----->'+endpoint.Last_Successful_Run_Last_Modified_Date__c);
                update endpoint;
                integer count= database.countQuery('select count() from Oppty_Activity_Data_to_SECPI__c limit 10002');
                if(count>10000){
                    Batch_OpptyActivityDataToSECPI_Delete ba = new Batch_OpptyActivityDataToSECPI_Delete();
                    Database.executeBatch(ba);
                }else{
                    List<Oppty_Activity_Data_to_SECPI__c> deleteData = [select id from Oppty_Activity_Data_to_SECPI__c];
                    delete deleteData;
                }
            }
        }
        
        /*if(!Test.isRunningTest() && response.body.COUNT!=0 &&((offSetNo+limitCnt)>=2000 || response.body.ROWCNT!=response.body.COUNT)) {
            endpoint.last_successful_run__c = currentDateTime;
            update endpoint;
        }*/
        return response;
    }
    
    //Method to convert string to datetime
    public static datetime setDateFromString(string dt){
        string yyyymm = dt.left(6);
        string mm = yyyymm.right(2);
        string yyyy = dt.left(4);
        string yyyymmdd = dt.left(8);
        string dd = yyyymmdd.right(2);
        string finalstr = mm+'/'+dd+'/'+yyyy;
        date d = Date.parse(finalstr);
        system.debug('date----->'+d);
        
        string hhmmss = dt.right(6);
        string hhmm   = hhmmss.left(4);
        integer hh = integer.valueOf(hhmm.left(2));
        integer min = integer.valueOf(hhmm.right(2));
        integer ss = integer.valueOf(hhmmss.right(2));
        
        time t = time.newInstance(hh,min,ss, 0);
        datetime dtime= DateTime.newinstance(d,t);
        
        Timezone tz = Timezone.getTimeZone('America/New_York');
        //datetime now = datetime.now();
        //Integer timeDiff = tz.getOffset(now);
        Integer timeDiff = tz.getOffset(dtime);
        if(timeDiff==-18000000){
            dtime = dtime.addhours(-5);
        }else{
            dtime = dtime.addhours(-4);
        }
        System.debug('dtime----->'+dtime);
        return dtime;
    }
    
    public static Set<string> CustomSettingData(){
        Set<string> Ids = new Set<string>();
        List<Oppty_Activity_Data_to_SECPI__c> AllData = Oppty_Activity_Data_to_SECPI__c.getall().values();
        for(Oppty_Activity_Data_to_SECPI__c dataSend : AllData){
            Ids.add(dataSend.name);
        }
        List<Oppty_Activity_Data_to_SECPI__c> AllData1 = Oppty_Activity_Data_to_SECPI__c.getall().values();
        for(Oppty_Activity_Data_to_SECPI__c dataSend : AllData1){
            Ids.add(dataSend.name);
        }
        List<Oppty_Activity_Data_to_SECPI__c> AllData2 = Oppty_Activity_Data_to_SECPI__c.getall().values();
        for(Oppty_Activity_Data_to_SECPI__c dataSend : AllData2){
            Ids.add(dataSend.name);
        }
        return Ids;
    }
}