/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class OpportunityActivityRestResourceSECPITest {

    static testMethod void getOpportunityActivityNoData() {

        SFDCStubSECPI.OpportunityActivityRequest reqData = new SFDCStubSECPI.OpportunityActivityRequest();
        
        reqData.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData.inputHeaders.IFID    = SFDCStubSECPI.LAYOUTID_083;
        reqData.body.CONSUMER = 'GMBT';
        reqData.body.PAGENO = 1;
        
        String jsonMsg = JSON.serialize(reqData);
        
        Test.startTest();
        
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/OpportunityActivitySECPI/xxxxxx';  //Request URL
        req1.httpMethod = 'GET';//HTTP Request Type
        req1.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req1;
        RestContext.response= res1;
        
        SFDCStubSECPI.OpportunityActivityResponseSECPI resData1 = OpportunityActivityRestResourceSECPI.getOpportunityActivitySECPI();
        System.debug(resData1.inputHeaders.MSGSTATUS);
        System.debug(resData1.inputHeaders.ERRORTEXT);
        System.assertEquals('F', resData1.inputHeaders.MSGSTATUS);
        
        Test.stopTest();
    }
    
    static testMethod void get_OpportunityActivitySECPI() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Account acc1 = new account(Name ='Test', Type='Customer',SAP_Company_Code__c = 'AAA');
        insert acc1;
        Id rtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('IT').getRecordTypeId();
        
        Opportunity opp = new Opportunity(accountId=acc1.id,recordtypeId=rtId,
        StageName='Identified',Name='Test Opp',Type='Tender',Division__c='IT',ProductGroupTest__c='LCD_MONITOR',LeadSource='BTA Dealer',Closedate=system.today());
        insert opp;
        
        Id TaskRecId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Task').getRecordTypeId();
        Task tsk = new Task(recordtypeId=TaskRecId,whatid=opp.id,ownerid=UserInfo.getUserId(),status='Open',Priority='Normal');
        insert tsk;
        Task tsk2 = new Task(recordtypeId=TaskRecId,whatid=opp.id,ownerid=UserInfo.getUserId(),status='Completed',Priority='Normal');
        insert tsk2;
        
        Event evnt = new Event(whatid=opp.id,Subject='Test Class Event',startdatetime=system.now(),enddatetime=System.now().addDays(1),ownerid=UserInfo.getUserId());
        insert evnt;
        
        Opportunity oppty = [select id from Opportunity where id=:opp.id];
        
        SFDCStubSECPI.OpportunityActivityRequest reqData = new SFDCStubSECPI.OpportunityActivityRequest();
        
        reqData.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData.inputHeaders.IFID    = SFDCStubSECPI.LAYOUTID_083;
        reqData.body.CONSUMER = 'GMBT';
        reqData.body.PAGENO = 1;
        
        String jsonMsg = JSON.serialize(reqData);
        
        Test.startTest();
        
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/OpportunityActivitySECPI/'+oppty.id;  //Request URL
        req1.httpMethod = 'GET';//HTTP Request Type
        req1.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req1;
        RestContext.response= res1;
        
        SFDCStubSECPI.OpportunityActivityResponseSECPI resData1 = OpportunityActivityRestResourceSECPI.getOpportunityActivitySECPI();
        System.debug(resData1.inputHeaders.MSGSTATUS);
        System.debug(resData1.inputHeaders.ERRORTEXT);
        System.assertEquals('S', resData1.inputHeaders.MSGSTATUS);
        
        Test.stopTest();
    }
    
    static testMethod void get_OpportunityActivityListSECPI() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        
        List<Integration_EndPoints__c> ieps = new List<Integration_EndPoints__c>();
        Integration_EndPoints__c iep1 = new Integration_EndPoints__c();
        iep1.name ='SECPI Flow 083';
        iep1.layout_id__c = 'LAY0255934';
        iep1.last_successful_run__c = System.now().addHours(-12);
        ieps.add(iep1);
        
        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='SECPI Flow 083-2';
        iep2.layout_id__c = 'LAY0255934';
        iep2.last_successful_run__c = System.now().addHours(-12);
        iep2.Last_Successful_Run_Last_Modified_Date__c = System.now().addHours(-14);
        ieps.add(iep2);
        insert ieps;
        
        Account acc1 = new account(Name ='Test', Type='Customer',SAP_Company_Code__c = 'AAA');
        insert acc1;
        Id rtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('IT').getRecordTypeId();
        
        Opportunity opp = new Opportunity(accountId=acc1.id,recordtypeId=rtId,
        StageName='Identified',Name='Test Opp',Type='Tender',Division__c='IT',ProductGroupTest__c='LCD_MONITOR',LeadSource='BTA Dealer',Closedate=system.today());
        insert opp;
        
        Id TaskRecId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Task').getRecordTypeId();
        Task tsk = new Task(recordtypeId=TaskRecId,whatid=opp.id,ownerid=UserInfo.getUserId(),status='Open',Priority='Normal');
        insert tsk;
        Task tsk2 = new Task(recordtypeId=TaskRecId,whatid=opp.id,ownerid=UserInfo.getUserId(),status='Completed',Priority='Normal');
        insert tsk2;
        
        Event evnt = new Event(whatid=opp.id,Subject='Test Class Event',startdatetime=system.now(),enddatetime=System.now().addDays(1),ownerid=UserInfo.getUserId());
        insert evnt;
        
        Id ConrtId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('B2B Sales').getRecordTypeId();
        contact con = new contact(firstname='Test',lastname='Email',Email='testEmail@test.com',accountid=acc1.id,recordtypeId=ConrtId);
        insert con;
        
        Contact contct = [select id,email from Contact where id=:con.id];
        
        EmailMessage email = new EmailMessage(FromAddress=UserInfo.getUserEmail(),Subject='Email Subject',ToAddress=contct.email,RelatedToId=opp.id);
        insert email;
        
        Oppty_Activity_Data_to_SECPI__c storeId = new Oppty_Activity_Data_to_SECPI__c(name='test');
        insert storeId;
        
        Opportunity oppty = [select id from Opportunity where id=:opp.id];
        
        SFDCStubSECPI.OpportunityActivityRequest reqData = new SFDCStubSECPI.OpportunityActivityRequest();
        
        string SearchDtm = string.valueOf(system.now().addDays(1));
        SearchDtm = SearchDtm.replaceAll( '\\s+', '');
        SearchDtm = SearchDtm.remove(':');
        SearchDtm = SearchDtm.remove('-');
        
        string SearchDtmTo = string.valueOf(system.now());
        SearchDtmTo = SearchDtmTo.replaceAll( '\\s+', '');
        SearchDtmTo = SearchDtmTo.remove(':');
        SearchDtmTo = SearchDtmTo.remove('-');
        
        reqData.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData.inputHeaders.IFID    = SFDCStubSECPI.LAYOUTID_083;
        reqData.body.CONSUMER = 'SECPIMSTR';
        reqData.body.SEARCH_DTTM_FROM = SearchDtm;
        reqData.body.SEARCH_DTTM_TO = SearchDtmTo;
        reqData.body.REQUEST_TYPE = 'TASK';
        reqData.body.PAGENO = 1;
        
        String jsonMsg = JSON.serialize(reqData);
        
        Test.startTest();
        
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/OpportunityActivitySECPI';  //Request URL
        req1.httpMethod = 'POST';//HTTP Request Type
        req1.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req1;
        RestContext.response= res1;
        
        SFDCStubSECPI.OpportunityActivityResponseSECPI resData1 = OpportunityActivityRestResourceSECPI.getOpportunityActivityListSECPI();
        System.assertEquals('S', resData1.inputHeaders.MSGSTATUS);
        
        reqData.body.REQUEST_TYPE = 'EVENT';
        jsonMsg = JSON.serialize(reqData);
        req1.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req1;
        RestContext.response= res1;
        
        SFDCStubSECPI.OpportunityActivityResponseSECPI resData2 = OpportunityActivityRestResourceSECPI.getOpportunityActivityListSECPI();
        
        reqData.body.SEARCH_DTTM_FROM = '';
        jsonMsg = JSON.serialize(reqData);
        RestRequest req2 = new RestRequest(); 
        RestResponse res2 = new RestResponse();
             
        req2.requestURI = '/services/apexrest/OpportunityActivitySECPI';  //Request URL
        req2.httpMethod = 'POST';//HTTP Request Type
        req2.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req2;
        RestContext.response= res2;
        
        SFDCStubSECPI.OpportunityActivityResponseSECPI resData3 = OpportunityActivityRestResourceSECPI.getOpportunityActivityListSECPI();
        
        reqData.body.SEARCH_DTTM_FROM = '2018052504000';
        jsonMsg = JSON.serialize(reqData);
        RestRequest req3 = new RestRequest(); 
        RestResponse res3 = new RestResponse();
             
        req3.requestURI = '/services/apexrest/OpportunityActivitySECPI';  //Request URL
        req3.httpMethod = 'POST';//HTTP Request Type
        req3.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req3;
        RestContext.response= res3;
        
        SFDCStubSECPI.OpportunityActivityResponseSECPI resData4 = OpportunityActivityRestResourceSECPI.getOpportunityActivityListSECPI();
        
        reqData.body.SEARCH_DTTM_FROM = '20180522040000';
        jsonMsg = JSON.serialize(reqData);
        RestRequest req4 = new RestRequest(); 
        RestResponse res4 = new RestResponse();
             
        req4.requestURI = '/services/apexrest/OpportunityActivitySECPI';  //Request URL
        req4.httpMethod = 'POST';//HTTP Request Type
        req4.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req4;
        RestContext.response= res4;
        
        SFDCStubSECPI.OpportunityActivityResponseSECPI resData5 = OpportunityActivityRestResourceSECPI.getOpportunityActivityListSECPI();
        
        OpportunityActivityRestResourceSECPI.CustomSettingData();
        
        Test.stopTest();
    }
    
    static testMethod void OpportunityActivityWrongRequest() {

        SFDCStubSECPI.OpportunityActivityRequest reqData = new SFDCStubSECPI.OpportunityActivityRequest();
        
        reqData.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData.inputHeaders.IFID    = SFDCStubSECPI.LAYOUTID_083;
        reqData.body.CONSUMER = 'SECPIMSTR';
        reqData.body.PAGENO = 1;
        
        String jsonMsg = JSON.serialize(reqData);
        
        Test.startTest();
        
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/OpportunityActivitySECPI';  //Request URL
        req1.httpMethod = 'POST';//HTTP Request Type
        req1.requestBody = Blob.valueof('Hello World');
        RestContext.request = req1;
        RestContext.response= res1;
        
        SFDCStubSECPI.OpportunityActivityResponseSECPI resData1 = OpportunityActivityRestResourceSECPI.getOpportunityActivityListSECPI();
        System.debug(resData1.inputHeaders.MSGSTATUS);
        System.debug(resData1.inputHeaders.ERRORTEXT);
        System.assertEquals('F', resData1.inputHeaders.MSGSTATUS);
        
        Test.stopTest();
    }
    
    static testMethod void OpportunityActivityWrongDateRequest() {

        SFDCStubSECPI.OpportunityActivityRequest reqData = new SFDCStubSECPI.OpportunityActivityRequest();
        
        reqData.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData.inputHeaders.IFID    = SFDCStubSECPI.LAYOUTID_083;
        reqData.body.CONSUMER = 'SECPIMSTR';
        reqData.body.SEARCH_DTTM_FROM = '20180410000000';
        reqData.body.SEARCH_DTTM_To = '20180411000000';
        reqData.body.REQUEST_TYPE = '';
        reqData.body.PAGENO = 1;
        
        String jsonMsg = JSON.serialize(reqData);
        
        Test.startTest();
        
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/OpportunityActivitySECPI';  //Request URL
        req1.httpMethod = 'POST';//HTTP Request Type
        req1.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req1;
        RestContext.response= res1;
        
        SFDCStubSECPI.OpportunityActivityResponseSECPI resData1 = OpportunityActivityRestResourceSECPI.getOpportunityActivityListSECPI();
        System.debug(resData1.inputHeaders.MSGSTATUS);
        System.debug(resData1.inputHeaders.ERRORTEXT);
        System.assertEquals('F', resData1.inputHeaders.MSGSTATUS);
        
        Test.stopTest();
    }
    
    static testMethod void OpportunityActivityPageLimitTest() {
        List<Integration_EndPoints__c> ieps = new List<Integration_EndPoints__c>();
        Integration_EndPoints__c iep1 = new Integration_EndPoints__c();
        iep1.name ='SECPI Flow 083';
        iep1.layout_id__c = 'LAY0255934';
        iep1.last_successful_run__c = System.now().addHours(-12);
        iep1.Last_Successful_Run_Last_Modified_Date__c = System.now().addHours(-14);
        ieps.add(iep1);
        insert ieps;

        SFDCStubSECPI.OpportunityActivityRequest reqData = new SFDCStubSECPI.OpportunityActivityRequest();
        
        reqData.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData.inputHeaders.IFID    = SFDCStubSECPI.LAYOUTID_083;
        reqData.body.CONSUMER = 'SECPIMSTR';
        reqData.body.SEARCH_DTTM_FROM = '20180410000000';
        reqData.body.SEARCH_DTTM_To = '20180411000000';
        reqData.body.REQUEST_TYPE = 'TASK';
        reqData.body.PAGENO = 251;
        
        String jsonMsg = JSON.serialize(reqData);
        
        Test.startTest();
        
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/OpportunityActivitySECPI';  //Request URL
        req1.httpMethod = 'POST';//HTTP Request Type
        req1.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req1;
        RestContext.response= res1;
        
        SFDCStubSECPI.OpportunityActivityResponseSECPI resData1 = OpportunityActivityRestResourceSECPI.getOpportunityActivityListSECPI();
        System.debug(resData1.inputHeaders.MSGSTATUS);
        System.debug(resData1.inputHeaders.ERRORTEXT);
        System.assertEquals('F', resData1.inputHeaders.MSGSTATUS);
        
        Test.stopTest();
    }
    
}