public class OpportunityCustomButtons {

   public Opportunity oppty {get; set;}
   /* DW SVC Phase 2 */
   public Boolean canCreateAsset {get; set;}
   public Integer numerOfAssetCreated {get; set;}

    public OpportunityCustomButtons(apexPages.standardController sc){
        string oppId = ApexPages.currentPage().getParameters().get('id');
        oppty = [select id,HasOpportunityLineItem,How_is_the_technology_used_and_who_uses__c,How_did_we_effectively_engage_the_custom__c,What_Samsung_or_third_party_applications__c,Why_did_the_customer_choose_Samsung_ove__c,What_was_the_customer_s_problem_and_how__c from opportunity where id=:oppId];
        
        List<ID> SBSProductIDs = new List<ID>();

        if (oppty.HasOpportunityLineItem != true){
            canCreateAsset = false;
        }
        else{

            list<OpportunityLineItem> OLI = [Select UnitPrice, Quantity, PricebookEntry.Product2Id, 
                                                PricebookEntry.Product2.Name, Description,opportunity.id,  opportunity.accountId,opportunity.closeDate
                                                 From OpportunityLineItem 
                                                  where OpportunityId = :oppId and (PricebookEntry.Product2.Product_Category__c='Service' or PricebookEntry.Product2.Product_Category__c='Support') ];
      
            for(OpportunityLineItem ol: OLI){
                SBSProductIDs.add(ol.PricebookEntry.Product2Id);
            }

            List<Product2> productList = [select id from product2 where ID IN :SBSProductIDs and 
                ID not in (select product2ID from asset where opportunity__c =: oppId)];
            if (productList.size() > 0){
                canCreateAsset = true;
            }else{
                canCreateAsset = false;
            }
        }
    }
    
    public pagereference saveNSubmitForApproval(){
        try{
            update oppty;
            
            pageReference pf= new pagereference('/apex/OpportunityCustomButtonsPage?id='+oppty.id);
           // pf.setRedirect(true);
            return pf;
        }catch(exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()));
            return null;
        }
    }
    
    
    public Void createAsset() {
        system.debug('asset starts');
        numerOfAssetCreated = 0;
        String VasRecordTypeID =null;
        String ServiceRecordTypeID =null;
        try {
            List<RecordType> RecordTypes = [select id, name from recordtype where SobjectType = 'Asset'];
            for (RecordType rt:RecordTypes){
                if (rt.name == 'VAS Asset' ){
                    VasRecordTypeID = rt.id;
                }
                else{
                    ServiceRecordTypeID = rt.id;
                }
            }

            List<Asset> astList = new List<Asset>();
            Map<ID,Asset> productMap = new Map<ID, Asset>();
            List<Asset> existingAssets = [select ID,product2ID,opportunity__c from asset where opportunity__c =: oppty.id
                                    and product2ID != null];
            for (Asset ast:existingAssets){
                productMap.put(ast.product2ID,ast);
            }

            list<OpportunityLineItem> OLI = 
             [Select UnitPrice, Quantity, PricebookEntry.Product2Id, 
                                                PricebookEntry.Product2.Name, PricebookEntry.Product2.pet_name__c,
                                                Description,opportunity.id,  opportunity.accountId,opportunity.closeDate
                                                 From OpportunityLineItem 
                                                 where OpportunityId = :oppty.id
                                                 and  (PricebookEntry.Product2.Product_Category__c='Service' or PricebookEntry.Product2.Product_Category__c='Support') ];
            List<ID> productIds = new List<ID>();
            Map<ID, String> productHasEntitlements = new Map<ID,String>();
            for(OpportunityLineItem ol: OLI){
                productIds.add(ol.PricebookEntry.Product2Id);
            }

            List<ProductEntitlementTemplate>  prodEntTemplates = [select ID, Product2Id,EntitlementTemplateId 
                from ProductEntitlementTemplate where Product2Id IN:productIds
            ];

            for (ProductEntitlementTemplate pt:prodEntTemplates){
                productHasEntitlements.put(pt.Product2Id, 'Y');
            }
      
            for(OpportunityLineItem ol: OLI){
                if (!productMap.containsKey(ol.PricebookEntry.Product2Id)){
                    Asset a = new Asset();

                    if (productHasEntitlements.containsKey(ol.PricebookEntry.Product2Id)){
                        a.recordTypeId = ServiceRecordTypeID;
                    }else{
                        a.recordTypeId = VasRecordTypeID;
                    }

                    a.AccountId = ol.opportunity.AccountId;
                    a.Product2Id = ol.PricebookEntry.Product2Id;
                    a.Quantity = ol.Quantity;
                    a.Price =  ol.UnitPrice;
                    a.PurchaseDate = ol.opportunity.CloseDate;
                    a.Status = 'Active';
                    a.Description = ol.Description;
                    a.Name = (ol.PricebookEntry.Product2.pet_name__c ==null?ol.PricebookEntry.Product2.name:ol.PricebookEntry.Product2.pet_name__c);
                    a.opportunity__c = oppty.id;
                    astList.add(a);
                    numerOfAssetCreated++;
                }
            }
            if(astList.size()>0){     
                insert astList;
            }
            
            //system.debug('done');
            //pageReference pf= new pagereference('/apex/OpportunityCustomButtonsPage?id='+oppty.id);
           // pf.setRedirect(true);
            //return pf;
           } catch(Exception e) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,e.getMessage()));
            //return null;
        }
    }
    
    Public PageReference lightningCheck(){
        if(UserInfo.getUiThemeDisplayed() =='Theme4d' || UserInfo.getUiThemeDisplayed()=='Theme4t' ){
            System.debug('****LTNG****');
            return new PageReference('/lightning/r/Opportunity/' +oppty.Id+'/view?nooverride=1');
        }else{
            return null;
        }
    }
    
}