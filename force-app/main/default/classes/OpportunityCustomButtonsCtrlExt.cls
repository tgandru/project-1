public with sharing class OpportunityCustomButtonsCtrlExt{

    private boolean creditApprovalButton;
    private boolean dealRegistrationButton;
    
    public Opportunity opportunityRecord {get; private set;}

    public boolean getDealRegistrationButton(){
        return dealRegistrationButton;
    }
        
    public boolean getCreditApprovalButton(){
        return creditApprovalButton;
    }

    public OpportunityCustomButtonsCtrlExt(ApexPages.StandardController stdController) {
        this.opportunityRecord = (Opportunity)stdController.getRecord();
        enableDisableCustomButtons();
    }
    
    private void enableDisableCustomButtons(){
        // IF Submit_for_Deal_Registration__c = FALSE then disable Deal_Registration_Approval button
        if(opportunityRecord.Submit_for_Deal_Registration__c == false){
            dealRegistrationButton = true;
        }
        
        // IF SubmitforCreditApproval__c  = FALSE then disable Credit_Approval button
        if(opportunityRecord.SubmitforCreditApproval__c == false){
            creditApprovalButton = true;
            

        }
   }

}