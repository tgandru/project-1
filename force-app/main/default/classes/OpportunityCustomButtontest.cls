@istest(SeeAllData=true)
public class OpportunityCustomButtontest {
    static testmethod void CustomButtonstest(){
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }
            /* Account acc1 = new account(
              Name ='Test OPP', Type='Customer',SAP_Company_Code__c = 'OPPWIN',
              BillingStreet = '101 Montelago Blvd',
            BillingCity ='Henderson',
            BillingState ='NV',
            BillingCountry='US',
            BillingPostalCode='89011'
              );
          */
        Account acc1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = rtMap.get('Account' + 'Temporary'),
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        );
        insert acc1;

        test.startTest();

        Id rtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Mobile').getRecordTypeId();
        
        Opportunity opp = new Opportunity(accountId=acc1.id,recordtypeId=rtId,
        StageName='Identified',Name='TEst Opp',Type='Tender',Division__c='Mobile',ProductGroupTest__c='LCD_MONITOR',LeadSource='BTA Dealer',Closedate=system.today());
        insert opp;

        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount2 = new Account (
            Name = 'Partner1',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        );

        insert testAccount2;
        Id pricebookId = Test.getStandardPricebookId();  
        RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];
        Product2 prod1 = new Product2(
                name ='MI-testsvc',
                SAP_Material_ID__c = 'MI-testsvc',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = productRT.id,
                Product_Category__c='Service' 
            );

        insert prod1;

        pricebookentry pe=[SELECT id, product2id,product2.name,product2.Product_Category__c from pricebookentry where pricebook2id=:pricebookId and product2.Product_Category__c = 'Service' limit 1];
        
        /*OpportunityLineItem oi = new OpportunityLineItem(
                PricebookEntryId=pe.id, 
                OpportunityId=opp.Id, 
                UnitPrice=1723, 
                Quantity=1);
        insert oi;*/

        /*opp.Primary_Distributor__c = testAccount2.id;
        opp.StageName = 'Qualification';
        opp.Has_Partner__c = true;
        opp.Rollout_Duration__c = 12;
        opp.Roll_Out_Start__c = system.today()+20;*/

        //update opp;

         
        Pagereference pf= Page.OpportunityCustomButtonsPage;
        pf.getParameters().put('id',opp.id);
        test.setCurrentPage(pf);
        
        apexPages.standardcontroller sc = new apexPages.standardcontroller(opp);
        OpportunityCustomButtons obj = new OpportunityCustomButtons(sc);
       
      obj.saveNSubmitForApproval();
        opp.stageName='Identified';
        opp.Amount=null;
        opp.How_is_the_technology_used_and_who_uses__c='Test322';
        opp.How_did_we_effectively_engage_the_custom__c='test123';
        opp.What_Samsung_or_third_party_applications__c='';
        opp.Why_did_the_customer_choose_Samsung_ove__c='Test Hi';
        opp.What_was_the_customer_s_problem_and_how__c='N/A'; 

        obj.createAsset();
      
        try{ 
           // obj.saveNSubmitForApproval();
           integer i=1/3;
            
        }catch(DMLException  e){
                    
        }


        Opportunity oppt = [select id from Opportunity where id='0063600000DjaiVAAR' limit 1];

        pf.getParameters().put('id',oppt.id);
        test.setCurrentPage(pf);
        
        apexPages.standardcontroller sc2 = new apexPages.standardcontroller(oppt);
        OpportunityCustomButtons obj2 = new OpportunityCustomButtons(sc2);
        obj2.createAsset();
         test.stopTest();
    }

}