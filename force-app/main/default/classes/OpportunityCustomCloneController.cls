public class OpportunityCustomCloneController {
   
 public class RelatedListWrapper {
        @AuraEnabled public boolean isChecked {get;set;}
        @AuraEnabled public  String ObejctLabel {get;set;}
        @AuraEnabled public  String ObejctAPI {get;set;}
        @AuraEnabled public  String relationshipName {get;set;}
        @AuraEnabled public  Integer count {get;set;}
       public RelatedListWrapper(boolean isChecked, String ObejctLabel,String ObejctAPI,String relationshipName,integer count ){
            this.isChecked = isChecked;
            this.ObejctLabel = ObejctLabel;
            this.ObejctAPI = ObejctAPI;
            this.relationshipName=relationshipName;
            this.count = count;
           
        }
    }
   @AuraEnabled(cacheable=true)
    public static Opportunity getOppInformation (String oppid) {
        return [SELECT Id,Name,Account.name,AccountId,CloseDate,Pricebook2Id,ProductGroupTest__c,Pricebook2.Name, Type,Opportunity_Number__c,OwnerId,Owner.Name from Opportunity where id=:Oppid limit 1 ];
    }
  @AuraEnabled(cacheable=true)
    public static List<RelatedListWrapper> getRelatedlistInformation (string oppid) {
        List<RelatedListWrapper> ListWrapper = new List<RelatedListWrapper>();
          //List<Attachment> att=new List<Attachment>([Select id from Attachment where ParentId=:oppid ]);
          List<OpportunityLineItem> oli=new List<OpportunityLineItem>([Select id from OpportunityLineItem where OpportunityId=:oppid ]);
          List<OpportunityTeamMember> team=new List<OpportunityTeamMember>([Select id from OpportunityTeamMember where OpportunityId=:oppid ]);
          List<OpportunityContactRole> conRoles=new List<OpportunityContactRole>([Select id from OpportunityContactRole where OpportunityId=:oppid ]);
          List<Partner__c> prt=new List<Partner__c>([Select id from Partner__c where Opportunity__c=:oppid ]);
        /*if(att.size()>0){
             ListWrapper.add(new RelatedListWrapper(false,'Attachment','Attachment','ParentId',att.size()));
        }*/
        
         if(team.size()>0){
             ListWrapper.add(new RelatedListWrapper(false,'Team','OpportunityTeamMember','OpportunityId',team.size()));
        }
          if(conRoles.size()>0){
             ListWrapper.add(new RelatedListWrapper(false,'Contact Roles','OpportunityContactRole','OpportunityId',conRoles.size()));
        }
        
         if(oli.size()>0){
             ListWrapper.add(new RelatedListWrapper(false,'Opportunity Products','OpportunityLineItem','OpportunityId',oli.size()));
        }
        
         if(prt.size()>0){
             ListWrapper.add(new RelatedListWrapper(false,'Partner','Partner__c','Opportunity__c',prt.size()));
        }
        return ListWrapper;
    }  
    @AuraEnabled
    public Static String Saverecords (String oppid,string relatedliststring ){
        System.debug('Clone Method');
        // Clone Header Object first
         System.debug('oppid::'+oppid);
         System.debug('relatedliststring::'+relatedliststring);
        String returnmsg='';
        list<RelatedListWrapper> relatedlist=(List<RelatedListWrapper>)JSON.deserialize(relatedliststring, List<RelatedListWrapper>.class);
        Opportunity opp=new Opportunity(id=oppid); 
        sObject originalSObject = (sObject) opp;

                    List<sObject> originalSObjects = new List<sObject>{originalSObject};
                          
                    List<sObject> clonedSObjects = SObjectAllFieldCloner.cloneObjects(
                                                          originalSobjects,
                                                          originalSobject.getsObjectType());
          Opportunity newopp=(Opportunity)clonedSObjects.get(0);
              newopp.StageName='Identified';
              newopp.Roll_Out_Plan__c=null;
              newopp.Amount=null;
              newopp.Primary_Reseller__c=null;
              newopp.Primary_Distributor__c=null;
              newopp.What_was_the_customer_s_problem_and_how__c=null;
              newopp.How_did_we_effectively_engage_the_custom__c=null;
              newopp.How_is_the_technology_used_and_who_uses__c=null;
              newopp.How_was_our_product_deemed_to_be_superio__c=null;
              newopp.What_Samsung_or_third_party_applications__c=null;
              newopp.Why_did_the_customer_choose_Samsung_ove__c=null;
              newopp.External_Opportunity_ID__c= null  ;
              newopp.Legacy_Id__c =null;
                newopp.OwnerId=UserInfo.getUserId();
             Database.SaveResult SR = Database.insert(newopp, False);
        if(SR.isSuccess()){
            returnmsg=newopp.id;
            List<sobject> childObjList = new List<sobject>();
            for(RelatedListWrapper obj : relatedlist){
                if(obj.isChecked){
                        List<Sobject> clonedChildObj = UtilityClass.cloneObject(obj.ObejctAPI,oppid,false,obj.relationshipName,newopp.Id);
                        if(clonedChildObj.size() > 0){
                            childObjList.addAll(clonedChildObj);
                        }
                }
            }
            
            if(childObjList.size()>0){
                 Database.insert(childObjList,false);
                 CloneChannelRecords(newopp.Id,oppid);
            }
                
        }
        else{
            for(Database.Error err : sr.getErrors()) {
                System.debug('The following error has occurred.');                   
                System.debug(err.getStatusCode() + ': ' + err.getMessage());
                System.debug('fields that affected this error: ' + err.getFields());
                 returnmsg = 'Error Occured While Cloning the Opportunity. Error :-' + string.valueof(err.getMessage());
            }
        }
               
        return returnmsg;
    } 
    
     @AuraEnabled
     public static void CloneChannelRecords(String newOpp,String OldOpp){
         // This method to Clone the channel relationship records under Opportunity.
         
         // 1st Query old Channel reocrds
         List<Channel__c> oldchannels=new List<Channel__c>([Select id,Name,Distributor__c,Opportunity__c,Partner_Distributor__c,Partner_Distributor__r.Partner__c,Partner_Reseller__c,Partner_Reseller__r.Partner__c,Reseller__c from Channel__c where Opportunity__c=:Oldopp]);
         
         //next query query newly created Partner records
         
         List<Partner__c> newpartner=new List<Partner__c>([Select id,Name,Opportunity__c,Partner__c from Partner__c where Opportunity__c=:newOpp]);
         Map<String,string> partnermap=new Map<String,string>();
         
         for(Partner__c p: newpartner){
             partnermap.put(p.Partner__c,p.Id);
         }
         
         List<Channel__c> newchannelsinsert=new List<Channel__c>();
         
         for(Channel__c c: oldchannels){
             if(partnermap.containsKey(c.Partner_Distributor__r.Partner__c) && partnermap.containsKey(c.Partner_Reseller__r.Partner__c)){
                 Channel__c newchannel=c.clone();
                 newchannel.Opportunity__c=newOpp;
                 newchannel.Partner_Distributor__c=partnermap.get(c.Partner_Distributor__r.Partner__c);
                 newchannel.Partner_Reseller__c=partnermap.get(c.Partner_Reseller__r.Partner__c);
                 newchannelsinsert.add(newchannel);
             }
         }
         
         // Insert new Channel records
         
         if(newchannelsinsert.size()>0){
             insert newchannelsinsert;
         }
          
          
     }
    
    
    
    
}