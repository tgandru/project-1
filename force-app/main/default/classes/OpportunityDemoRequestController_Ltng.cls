public class OpportunityDemoRequestController_Ltng {
 
  @AuraEnabled(cacheable=true)
    public static Opportunity getOppInformation (String Oppid) {
        return [SELECT Id,Name,Account.name,StageName,HasOpportunityLineItem,AccountId,CloseDate,Pricebook2Id,ProductGroupTest__c,Pricebook2.Name, Type,Opportunity_Number__c,OwnerId,Owner.Name,CountofApprovedQuotes__c,IsClosed,Division__c from Opportunity where id=:Oppid limit 1 ];
    }
       @auraEnabled
    public static String getInstanceUrl()
    {
        String InstanceURL = URL.getSalesforceBaseUrl().toExternalForm();
        return InstanceURL;
    }
    
     @AuraEnabled(cacheable=true)
    public static List<ContactRoleListWrapper> getOppContactRoles (String Oppid) {
        list<ContactRoleListWrapper> returnlist=new List<ContactRoleListWrapper>();
        for(OpportunityContactRole con:[Select id,ContactId,Contact.Name,IsPrimary,Role,OpportunityId from OpportunityContactRole where OpportunityId=:Oppid]){
            returnlist.add(new ContactRoleListWrapper(con.Contact.Name,con.ContactId));
        }
        
        return returnlist;
    }
    
    public class ContactRoleListWrapper {
        @AuraEnabled public  String conactname {get;set;}
        @AuraEnabled public  String contactid {get;set;}
        public ContactRoleListWrapper(String conactname,String contactid){
            this.conactname=conactname;
            this.contactid=contactid;
        }
    }
    
     @AuraEnabled
    Public Static Map<String, String> SaveDemorecord(Order newrecord){
         Map<String,String> resultMap = new Map<String,String>();
        try{
            insert newrecord;
            System.debug('record Created'+newrecord.Id);
             resultMap.put('status', 'success');
            resultMap.put('Id',newrecord.Id) ;           
        }catch(Exception e){
            resultMap.put('status', 'error');
            resultMap.put('message',e.getMessage());
            System.debug('error'+e.getMessage());
        }
        
        return resultMap;
    }
    
    
}