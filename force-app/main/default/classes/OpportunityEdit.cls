public class OpportunityEdit {

    public opportunity opp {get; set;}
    public boolean showedit {get; set;}
    public OpportunityEdit(apexPages.StandardController sc){
        string oppId = apexPages.currentPage().getParameters().get('id');
        opp = new Opportunity();
        DescribeSObjectResult describeResult = Opportunity.getSObjectType().getDescribe();
        List<String> fieldNames = new List<String>( describeResult.fields.getMap().keySet() );
        fieldNames.add('Owner.isactive');//Adding Owner.isactive field to check the Owner
         fieldNames.add('UserRecordAccess.HasEditAccess');//Checking the user has Edit Permission or not on the current Opportunity Record - Added By Vijay on 1/8/2018 
        String query =  ' SELECT ' +  String.join( fieldNames, ',' ) + ',' + 'Recordtype.name' + ' FROM ' + describeResult.getName() +' WHERE ' +' id = :oppId ' + ' LIMIT 1 ';
        opp = database.query(query);
        UserRecordAccess ura = [select RecordId, HasEditAccess from UserRecordAccess where UserId = :UserInfo.getUserId() and RecordId = :oppId];

        if(ura.HasEditAccess){
            showedit=True;
            
        }else{
           showedit=False; 
           ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'You are not allowed to Edit This Opportunity.'));

        }
        System.debug('showedit='+showedit);
        
        
    }
    
    
    public pageReference saveOpp(){
        try{
            
            //Logic to avoid process builder flow error while closing Opp.
            if(opp.Owner.isactive==False){
                List<OpportunityTeamMember> OppTeam = [select id,User.isactive,TeamMemberRole from opportunityteammember where Opportunityid=:opp.Id and user.isactive=true and TeamMemberRole in ('Service Specialist','KNOX Specialist','Tier 3 Sales','Tier 2 Sales','Tier 1 Sales','Inside Sales Rep')];
                if(OppTeam.size()==0){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Opportunity Owner is InActive. Change the Owner to Active User in order to close'));
                    return null;
                }
            }
            
            update opp;
            return  new pageReference('/'+opp.id);
        }catch(exception e){
            ApexPages.addMessages(e);
            return null;
        }
    }
    
    public pageReference saveNCreateNewOpp(){
        try{
            update opp;
            return  new pageReference('/setup/ui/recordtypeselect.jsp?ent=Opportunity&save_new_url=006');
        }catch(exception e){
            ApexPages.addMessages(e);
            return null;
        }
    }
    
}