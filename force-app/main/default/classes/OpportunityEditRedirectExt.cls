public class OpportunityEditRedirectExt {
    public Opportunity Opp;
    public string Oppid;

    public OpportunityEditRedirectExt(ApexPages.StandardController controller) {
    
    }
    Public pagereference redirect()
    { 
        Oppid = apexpages.currentpage().getparameters().get('id');
        if(Oppid != null && Oppid != null){
            Opp = [select id,name,recordtype.name from Opportunity where id =:Oppid];
        }
        if(Opp!=null){
            if(Opp.recordtype.name=='HA Builder' || Opp.recordtype.name=='Forecast'){
                //pagereference pg = new pagereference('/' +Oppid+'/e?nooverride=1');
                pagereference pg = new pagereference('/' +Oppid+'/e?retURL=%2F'+Oppid+'%3Fnooverride%3D1&nooverride=1');               
                pg.setredirect(true);
                return pg; 
            }
            Else{
                pagereference pg = new pagereference('/apex/OpportunityEdit?id='+Oppid);           
                pg.setredirect(true);
                return pg;
            }
        }
        
     return null;
    }

}