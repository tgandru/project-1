@isTest
public class OpportunityEditTest {

    static testMethod void editTest(){
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Account acc1 = new account(Name ='Test', Type='Customer',SAP_Company_Code__c = 'AAA');
        insert acc1;
        Id rtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('IT').getRecordTypeId();
        
        User u =[select id from user where isactive=false and profile.name='B2B ISR Sales User - Mobile' limit 1];
        
        Opportunity opp = new Opportunity(accountId=acc1.id,recordtypeId=rtId,
        StageName='Identified',Name='TEst Opp',Type='Tender',Division__c='IT',ProductGroupTest__c='LCD_MONITOR',LeadSource='BTA Dealer',Closedate=system.today(),ownerid=u.id);
        insert opp;
        
        test.startTest();
        Pagereference pf= Page.OpportunityEdit;
        pf.getParameters().put('id',opp.id);
        test.setCurrentPage(pf);
        
        apexPages.standardcontroller sc = new apexPages.standardcontroller(opp);
        OpportunityEdit obj = new OpportunityEdit(sc);
        obj.saveOpp();
        obj.saveNCreateNewOpp();
        opp.stageName='Proposal';
        try{
            obj.saveOpp();
        }catch(exception e){
            
        }
        try{
            obj.saveNCreateNewOpp();
        }catch(exception e){
            
        }
        
        Opportunity opp2 = new Opportunity(accountId=acc1.id,recordtypeId=rtId,
        StageName='Identified',Name='TEst Opp',Type='Tender',Division__c='IT',ProductGroupTest__c='LCD_MONITOR',LeadSource='BTA Dealer',Closedate=system.today());
        insert opp2;
        
        ApexPages.standardcontroller sc2 = new apexPages.standardcontroller(opp2);
        OpportunityEdit obj2 = new OpportunityEdit(sc2);
        obj2.saveOpp();
        opp2.stageName='Proposal';
        obj2.saveOpp();
        test.stopTest();
        
    }
    
    static testMethod void editTest2(){
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Account acc1 = new account(Name ='Test', Type='Customer',SAP_Company_Code__c = 'AAA');
        insert acc1;
        Id rtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('IT').getRecordTypeId();
        
        Opportunity opp = new Opportunity(accountId=acc1.id,recordtypeId=rtId,
        StageName='Identified',Name='TEst Opp',Type='Tender',Division__c='IT',ProductGroupTest__c='LCD_MONITOR',LeadSource='BTA Dealer',Closedate=system.today());
        insert opp;
        
        test.startTest();
        Pagereference pf= Page.OpportunityEdit;
        pf.getParameters().put('id',opp.id);
        test.setCurrentPage(pf);
        
        apexPages.standardcontroller sc = new apexPages.standardcontroller(opp);
        OpportunityEdit obj = new OpportunityEdit(sc);
        obj.saveOpp();
        obj.saveNCreateNewOpp();
        opp.stageName='Proposal';
        try{
            obj.saveOpp();
        }catch(exception e){
            
        }
        try{
            obj.saveNCreateNewOpp();
        }catch(exception e){
            
        }
        test.stopTest();
        
    }
    
}