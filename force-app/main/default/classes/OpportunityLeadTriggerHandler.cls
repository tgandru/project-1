/*
 * TODO: Add complete callout, work was switched from this integration to different piece of project...
 * Instantiated by LeadConvert.trigger, and checks what leads updated leads have been converted 
 * for the first time
 * Author: Eric Vennaro
**/

public class OpportunityLeadTriggerHandler {
/* Commennted by Bala since this class name is changed to LeadTriggerHandler
    private List<Lead> updatedLeads;
    private Map<Id, Lead> oldLeadsMap;
    public Set<Lead> prmLeadsSet;
    public List<message_queue__c> mqList;

    public OpportunityLeadTriggerHandler(List<Lead> updatedLeads, Map<Id, Lead> oldLeadsMap) {
        this.updatedLeads = updatedLeads;
        this.oldLeadsMap = oldLeadsMap;
        prmLeadsSet = new Set<Lead>();
        mqList = new List<message_queue__c>();
    }

    public void mapOpportunities() {
        Set<Id> opportunityIds = new Set<Id>();
        for(Lead lead : updatedLeads) {
            
            if(lead.Status!=null && lead.status !=oldLeadsMap.get(lead.Id).status){
                if(String.isNotBlank(lead.PRM_Lead_Id__c)){
                    prmLeadsSet.add(lead);
                }
            }
            
            if(!wasLeadConverted(lead.Id) && lead.isConverted) {
                opportunityIds.add(lead.ConvertedOpportunityId);
                
                }
            }
        
        if(prmLeadsSet.size()>0){
            for(Lead l: prmLeadsSet){
                message_queue__c mq = new message_queue__c(Integration_Flow_Type__c = 'Flow-016', retry_counter__c = 0, Identification_Text__c = l.Id, Status__c='not started');
                mqList.add(mq);                
            }
            
            if(mqList.size()>0){
                insert mqList;
            }
            
            for(message_queue__c mqRec:[SELECT Id, MSGSTATUS__c,ERRORCODE__c,ERRORTEXT__c,MSGUID__c,IFID__c,IFDate__c,Status__c, retry_counter__c, Integration_Flow_Type__c, CreatedBy.Name, Identification_Text__c FROM message_queue__c where Id IN:mqList]){
                PRMLeadStatusHelper.callRoiEndpoint(mqRec);
            }
        }
    }
    */

       /* CihStub.LeadOpportunityRequest wrapper = new CihStub.LeadOpportunityRequest();
        List<CihStub.ConvertedOpportunity> convertedOpportunities = new List<CihStub.ConvertedOpportunity>();
        for(Opportunity opportunity : getOpportunities(opportunityIds)) {
            CihStub.ConvertedOpportunity oppEntry = new CihStub.ConvertedOpportunity();
            oppEntry.id = String.valueOf(opportunity.Id);
            oppEntry.description = opportunity.description;
            oppEntry.carrierOpportunityNumber = opportunity.Carrier_OpportunityNumber__c;
            convertedOpportunities.add(oppEntry);
        }
        wrapper.inputHeaders = new CihStub.MsgHeadersRequest();
        wrapper.body = convertedOpportunities;*/
    

   /* private List<Opportunity> getOpportunities(Set<Id> opportunityIds) {
        return [SELECT Id, Description, Carrier_OpportunityNumber__c
                FROM Opportunity
                WHERE Id IN :opportunityIds];
    }

    private Boolean wasLeadConverted(Id id) {
        if(oldLeadsMap.get(id) != null) {
            return oldLeadsMap.get(id).isConverted;
        } else {
            return true;
        }
    }
    */
}