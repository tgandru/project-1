global class OpportunityNextStepMigrationBatch implements Database.Batchable<sObject>,database.stateful{
    
     global OpportunityNextStepMigrationBatch(){}
     
      global Database.QueryLocator start(Database.BatchableContext BC){
       return Database.getQueryLocator([SELECT ID,NextStep,Next_Step__c,Bypass_Validation__c FROM Opportunity WHERE NextStep != null]);
      }
      
       global void execute(Database.BatchableContext BC, List<Opportunity> scope){
           for(Opportunity o: scope){
               o.Next_Step__c=o.NextStep;
               o.Bypass_Validation__c=true;
           }
           
           Update scope;
       }
       
       global void finish(Database.BatchableContext BC){}
}