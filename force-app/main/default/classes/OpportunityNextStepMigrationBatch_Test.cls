@isTest
private class OpportunityNextStepMigrationBatch_Test {

static Id endUserRT = TestDataUtility.retrieveRecordTypeId('End_User', 'Account'); 
static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
static Id HAaccRT = TestDataUtility.retrieveRecordTypeId('HA_Builder','Account');
static Id ITRT = TestDataUtility.retrieveRecordTypeId('IT', 'Opportunity');
static Id HART = TestDataUtility.retrieveRecordTypeId('HA_Builder', 'Opportunity');
static Id HAQuoteRT = TestDataUtility.retrieveRecordTypeId('HA_Builder', 'SBQQ__Quote__c');
   
    @testSetUp static void testsetup()
    {
        //Channelinsight configuration
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        
        //Creating test Account
        Account acct=TestDataUtility.createAccount('End user Account 90999');
        acct.recordTypeId = endUserRT;
        acct.Type = 'Customer';
        insert acct;
        system.debug('1 : Creating account' + acct);
        
         Contact contactRecord = TestDataUtility.createContact(acct);
        insert contactRecord;
        string   CampaignTypeId = [Select Id From RecordType Where SobjectType = 'Campaign' and Name = 'Sales'  limit 1].Id;
         Campaign cmp= new Campaign(Name='Test Campaign for test class ',	IsActive=true,type='Email',StartDate=system.Today(),recordtypeid=CampaignTypeId);
       insert cmp;
          Inquiry__c iq=new Inquiry__c(Contact__c = contactRecord.id,Product_Solution_of_Interest__c='Mobile Phones',	 campaign__c = cmp.id,isActive__c=true);
	      insert iq;
        
        
       //Creating the Parent Account for Partners
        Account paacct=TestDataUtility.createAccount('Partner Account');
        paacct.recordTypeId = directRT;
        paacct.SAP_Company_Code__c = '1234500';
        insert paacct;
        system.debug('2 : Creating Parent account' + paacct);
        
        //Creating the HA account
        Account HAacct=new Account();
        HAacct.name = 'HA Test Acc';
        HAacct.recordTypeId = HAaccRT;
        HAacct.Type='Customer';
        HAacct.SAP_Company_Code__c = '1234600';
        insert HAacct;
        system.debug('2 : Creating Parent account' + HAacct);
        
        //Creating custom Pricebook
        PriceBook2 pb = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(4)); 
        pb.Division__c='IT';
        pb.SAP_Price_Account_Code__c='1234500';
        pb.SAP_Price_Group_Code__c = '9999';
        insert pb;
        system.debug('2. PB created' + pb);

        List<opportunity> opplist = new List<Opportunity>();
        //EndCustomer Account with IT RT
        Opportunity oppOpen = new Opportunity(Name = 'Test 06272016a',
                                             AccountId =acct.Id,
                                             RecordTypeId = ITRT,
                                             Type = 'Tender',
                                             Description = 'Test Opportunity IT',
                                             Division__c = 'IT',
                                             Amount = 9999,
                                             pricebook2Id=pb.Id, 
                                             CloseDate = Date.valueOf(System.Today()),
                                             ProductGroupTest__c = 'PRINTER',
                                             LeadSource = 'External_Tradeshow',
                                             Reference__c = 'Test Name',
                                             stageName = 'Identified',
                                             Roll_Out_Start__c=Date.valueOf(System.Today()),
                                             Rollout_Duration__c=1,
                                             Inquiry_ID__C=iq.id,
                                              NextStep='Test'
                                             ); 
        insert oppOpen;
        system.debug('3. Created Opportunity Id' + oppOpen.Id + oppOpen.StageName);
        
        OpportunityCompetitor oppComp = new OpportunityCompetitor(OpportunityId=oppOpen.Id,CompetitorName='LG',Strengths='Strengths',Weaknesses='Weaknesses');
        insert oppComp;
        OpportunityCompetitor oppComp2 = new OpportunityCompetitor(OpportunityId=oppOpen.Id,CompetitorName='SONY',Strengths='Strengths',Weaknesses='Weaknesses');
        insert oppComp2;
        
        //Creating Products
        List<Product2> prods=new List<Product2>();
        Product2 standaloneProd=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Standalone Prod1', 'Printer[C2]', 'FAX/MFP', 'H/W');
        prods.add(standaloneProd);
        Product2 parentProd1=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Parent Prod1', 'Printer[C2]', 'FAX/MFP', 'H/W');
        prods.add(parentProd1);
        Product2 childProd1=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod1', 'Printer[C2]', 'PRINTER', 'Service pack');
        prods.add(childProd1);
        Product2 childProd2=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod2', 'Printer[C2]', 'PRINTER', 'Service pack');
        prods.add(childProd2);
        Product2 parentProd2=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Parent Prod2', 'Printer[C2]', 'FAX/MFP', 'H/W');
        prods.add(parentProd2);
        Product2 childProd3=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod3', 'Printer[C2]', 'PRINTER', 'Service pack');
        prods.add(childProd3);
        Product2 childProd4=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod4', 'Printer[C2]', 'PRINTER', 'Service pack');
        prods.add(childProd4);
        insert prods;
        system.debug('4. Created products' + prods.get(0).Id);
        
        /*First Insert the standard pricebook entry*/
        
        // Get standard price book ID.
        // This is available irrespective of the state of SeeAllData.       
        Id pricebookId = Test.getStandardPricebookId();
               
        //List of PricebookEntries for standard pricebook
        List<PriceBookEntry> pbes=new List<PriceBookEntry>();

        //create the pricebook entries for the custom pricebook
        PriceBookEntry standaloneProdPBE=TestDataUtility.createPriceBookEntry(standaloneProd.Id, pb.Id);
        pbes.add(standaloneProdPBE);
        PriceBookEntry parentProd1PBE=TestDataUtility.createPriceBookEntry(parentProd1.Id,pb.Id);
        pbes.add(parentProd1PBE);
        PriceBookEntry childProd1PBE=TestDataUtility.createPriceBookEntry(childProd1.Id, pb.Id);
        pbes.add(childProd1PBE);
        PriceBookEntry childProd2PBE=TestDataUtility.createPriceBookEntry(childProd2.Id, pb.Id);
        pbes.add(childProd2PBE);
        PriceBookEntry parentProd2PBE=TestDataUtility.createPriceBookEntry(parentProd2.Id, pb.Id);
        pbes.add(parentProd2PBE);
        insert pbes;
        system.debug('5.Created Pricebook' + pbes.get(0).Id);
        
        
        //Creating opportunity Line Item
        
        List<OpportunityLineItem> olis=new List<OpportunityLineItem>();
        OpportunityLineItem standaloneProdOLI=TestDataUtility.createOpptyLineItem(standaloneProd, standaloneProdPBE, oppOpen);
        standaloneProdOLI.Quantity = 10;
        standaloneProdOLI.TotalPrice = standaloneProdOLI.Quantity * standaloneProdPBE.UnitPrice;
        olis.add(standaloneProdOLI);
        OpportunityLineItem parentProd1OLI=TestDataUtility.createOpptyLineItem(parentProd1, parentProd1PBE, oppOpen);
        parentProd1OLI.Quantity = 10;
        parentProd1OLI.TotalPrice = parentProd1OLI.Quantity * parentProd1PBE.UnitPrice;
        olis.add(parentProd1OLI);
        OpportunityLineItem childProd1OLI=TestDataUtility.createOpptyLineItem(childProd1, childProd1PBE, oppOpen);
        childProd1OLI.Quantity = 10;
        childProd1OLI.TotalPrice = childProd1OLI.Quantity * childProd1PBE.UnitPrice;
        olis.add(childProd1OLI);
        OpportunityLineItem childProd2OLI=TestDataUtility.createOpptyLineItem(childProd2, childProd2PBE, oppOpen);
        childProd2OLI.Quantity = 10;
        childProd2OLI.TotalPrice = childProd2OLI.Quantity * childProd2PBE.UnitPrice;
        olis.add(childProd2OLI);
        OpportunityLineItem parentProd2OLI=TestDataUtility.createOpptyLineItem(parentProd2, parentProd2PBE, oppOpen);
        parentProd2OLI.Quantity = 10;
        parentProd2OLI.TotalPrice = parentProd2OLI.Quantity * parentProd2PBE.UnitPrice;
        olis.add(parentProd2OLI);
        insert olis;
        system.debug('7. Created opportunity line item ' + olis.get(0).Id );
        system.debug('*********************** Stagename'+ [select StageName from Opportunity where Id =:parentProd2OLI.OpportunityId]);
        system.debug('****************************** Roll out plan' + [select Roll_Out_Plan__c,HasOpportunityLineItem from Opportunity where Id =:parentProd2OLI.OpportunityId]);
        
        Opportunity HAoppOpen = new Opportunity(Name = 'Test 06272016a',
                                             //Accountid=HAacct.id,
                                             RecordTypeId = HART,
                                             Description = 'Test Opportunity HA',
                                             CloseDate = Date.valueOf(System.Today()),
                                             Project_Name__c = 'Testing Project Name',
                                             stageName = 'Identified',
                                             Roll_Out_Start__c = Date.newInstance(2020, 1, 2),
                                             Number_of_Living_Units__c = 10000,
                                             Rollout_Duration__c = 5); 
        insert HAoppOpen;
        
        //SBQQ__Quote__c HAQuote = new SBQQ__Quote__c(RecordTypeid=HAQuoteRT,SBQQ__Type__c='Quote',SBQQ__Opportunity2__c=HAoppOpen.id,SBQQ__Account__c=HAacct.id);
        SBQQ__Quote__c HAQuote = new SBQQ__Quote__c(RecordTypeid=HAQuoteRT,SBQQ__Type__c='Quote',SBQQ__Opportunity2__c=HAoppOpen.id);
        insert HAQuote;
        }
    
    //Scenario : When Opportunity has OLI, roll_out_start and end date  
    //Roll out plan trigger will be called to create roll out plan, product and schedule
     
     static testMethod void testOpptyWithOutRollOutPlan() {
          Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
         
        Test.startTest();
          Database.executeBatch(new OpportunityNextStepMigrationBatch());
        Test.stopTest();
     }
}