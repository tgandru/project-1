@isTest
private class OpportunityPartnerExtension_Ltng_Test {

static Id endUserRT = TestDataUtility.retrieveRecordTypeId('End_User', 'Account'); 
static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
static Id inDirectRT = TestDataUtility.retrieveRecordTypeId('Indirect','Account');
static Id mobileRT = TestDataUtility.retrieveRecordTypeId('Mobile', 'Opportunity'); 
static Id ITRT = TestDataUtility.retrieveRecordTypeId('IT', 'Opportunity');
static Id servicesRT = TestDataUtility.retrieveRecordTypeId('Services','Opportunity');
static Id WERT = TestDataUtility.retrieveRecordTypeId('Wireless_Enterprise','Opportunity');
//static Id PRODRT = TestDataUtility.retrieveRecordTypeId('B2B','Product2');
//static Id pricebookId = Test.getStandardPricebookId();    
//static Id attPricebookID = TestDataUtility.retrievePricebook('ATT');     

     @testSetup static void setup() {
            //Channelinsight configuration
            Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
      List<Account> partnerList = new List<Account>();
      List<Opportunity> Opplist = new List<Opportunity>();
           
           //Accounts
             Account acctPartner1 = TestDataUtility.createAccount('TestingPartner 1');
            acctPartner1.RecordTypeId = inDirectRT;
            acctPartner1.Allow_Direct__c = true;
            acctPartner1.type = 'Reseller';
            partnerList.add(acctPartner1);
              
            Account acctPartner2 = TestDataUtility.createAccount('TestingPartner 2');
            acctPartner2.RecordTypeId = inDirectRT;
            acctPartner2.Allow_Direct__c = true;
            acctPartner2.Type='System Integrator';
            partnerList.add(acctPartner2);
              
            Account acctPartner3 = TestDataUtility.createAccount('TestingPartner 3');
            acctPartner3.RecordTypeId = inDirectRT;
            acctPartner3.Allow_Direct__c = false;
            acctPartner3.Type='System Integrator';
            partnerList.add(acctPartner3);
         
            Account acctPartner4 = TestDataUtility.createAccount('TestingDirect 4');
            acctPartner4.RecordTypeId = directRT;
            acctPartner4.SAP_Company_Code__c = '1234510';
            acctPartner4.Type='Distributor';
            partnerList.add(acctPartner4);
            
            Account acctPartner5 = TestDataUtility.createAccount('Testing5');
            acctPartner5.RecordTypeId = directRT;
            acctPartner5.Type='Distributor';
            partnerList.add(acctPartner5);
                      
            insert partnerList;
         
           Account endCustomerAcct=TestDataUtility.createAccount(TestDataUtility.generateRandomString(2));
           endCustomerAcct.RecordTypeId = endUserRT ;
            endCustomerAcct.Type = 'Customer';
            endCustomerAcct.SAP_Company_Code__c = '1234509';
            insert endCustomerAcct;
         
           Account directAcct=TestDataUtility.createAccount(TestDataUtility.generateRandomString(2));
           directAcct.RecordTypeId = directRT ;
            directAcct.Type = 'Distributor';
            insert directAcct;
         
           Account inDirectAcct=TestDataUtility.createAccount(TestDataUtility.generateRandomString(2));
           inDirectAcct.RecordTypeId = inDirectRT; 
            inDirectAcct.Type = 'Alliance';    
            insert inDirectAcct;
            
           Account inDirectAcct1=TestDataUtility.createAccount(TestDataUtility.generateRandomString(2));
           inDirectAcct1.RecordTypeId = inDirectRT; 
            inDirectAcct1.Type = 'Reseller';    
            insert inDirectAcct1;    

            PriceBook2 pb_IT=TestDataUtility.createPriceBook('Test IT');
           pb_IT.SAP_Price_Account_Code__c='1234509';
            insert pb_IT;
            
            PriceBook2 pb_IT2=TestDataUtility.createPriceBook('Test IT2');
           pb_IT2.SAP_Price_Account_Code__c='';
            insert pb_IT2;
            
            PriceBook2 pb_IT3=TestDataUtility.createPriceBook('Test IT3');
           pb_IT3.SAP_Price_Account_Code__c='';
            insert pb_IT3;
         
            PriceBook2 pb_BTA=TestDataUtility.createPriceBook('Test BTA');
           pb_BTA.SAP_Price_Account_Code__c='1234599';
            insert pb_BTA;
         
            PriceBook2 pb_Mobile=TestDataUtility.createPriceBook('Test Mobile');
           pb_Mobile.SAP_Price_Account_Code__c='1234589';
            insert pb_Mobile;  
            
         
            //EndCustomer Account with IT RT
            Opportunity opp_IT = new Opportunity(Name = 'Test 06272016a',
                                               AccountId =endCustomerAcct.Id,
                                               RecordTypeId = ITRT,
                                               Type = 'Tender',
                                               Description = 'Test Opportunity IT',
                                               Division__c = 'IT',
                                               Amount = 9999,
                                               pricebook2Id=pb_IT.Id, 
                                               CloseDate = Date.valueOf(System.Today()),
                                               ProductGroupTest__c = 'PRINTER',
                                               LeadSource = 'External_Tradeshow',
                                               Reference__c = 'Test Name',
                                               stageName = 'Identified'); 
          // insert opp_IT;
            Opplist.add(opp_IT);    
            //EndCustomer Account with IT RT
                       
           //Direct Account with Mobile RT
            Opportunity opp_Mobile = new Opportunity(Name = 'Test 06272016b',
                                               AccountId =directAcct.Id,
                                               RecordTypeId = mobileRT,
                                               Type = 'Internal',
                                               Description = 'Test Opportunity Mobile',
                                               Division__c = 'Mobile',
                                               Amount = 9999,
                                               pricebook2Id=pb_IT2.Id, 
                                               CloseDate = Date.valueOf(System.Today()),
                                               ProductGroupTest__c = 'TABLET',
                                               LeadSource = 'External_Tradeshow',
                                               Reference__c = 'Test Name',
                                               stageName = 'Identified'); 
           //insert opp_Mobile;
           Opplist.add(opp_Mobile);
           //Indirect Account with Mobile RT
            Opportunity opp_Services = new Opportunity(Name = 'Test 06272016c',
                                               AccountId =inDirectAcct.Id,
                                               RecordTypeId = servicesRT,
                                               Type = 'Blanket',
                                               Description = 'Test Opportunity Mobile',
                                               Division__c = 'SBS',
                                               Amount = 9999,
                                               pricebook2Id=pb_IT3.Id, 
                                               CloseDate = Date.valueOf(System.Today()),
                                               ProductGroupTest__c = 'SECURITY',
                                               LeadSource = 'External_Tradeshow',
                                               Reference__c = 'Test Name',
                                               stageName = 'Identified'); 
        //   insert opp_Services;
             Opplist.add(opp_Services);
            Opportunity opp_WE = new Opportunity(Name = 'Test 06272016d',
                                               AccountId =inDirectAcct1.Id,
                                               RecordTypeId = WERT,
                                               Type = 'Blanket',
                                               Description = 'Test Opportunity IT',
                                               Division__c = 'W/E',
                                               Amount = 9999,
                                               pricebook2Id=pb_IT.Id, 
                                               CloseDate = Date.valueOf(System.Today()),
                                               ProductGroupTest__c = 'ENTERPRISE',
                                               LeadSource = 'External_Tradeshow',
                                               Reference__c = 'Test Name',
                                               stageName = 'Identified'); 
         //  insert opp_WE; 
            Opplist.add(opp_WE);
            insert Opplist;
         
            Contact con = new Contact(LastName='New Contact',Accountid=endCustomerAcct.id,Email='contact@contact.com');
            insert con;
             
            Partner__c OppPartner= new Partner__c(Opportunity__c=opp_IT .id,Partner__c=endCustomerAcct.id,Is_Primary__c=True,Role__c='Distributor',Contact__c=con.id);
            insert OppPartner;
            
            String dpaExtId = string.valueof(endCustomerAcct.id)+string.valueof(pb_IT.Id);
            DirectPriceBookAssign__c dpa1= new DirectPriceBookAssign__c(Direct_Account__c=endCustomerAcct.id,Price_Book__c=pb_IT.Id,External_ID__c=dpaExtId);
            insert dpa1;
            String dpaExtId2 = string.valueof(endCustomerAcct.id)+string.valueof(pb_IT2.Id);
            DirectPriceBookAssign__c dpa2= new DirectPriceBookAssign__c(Direct_Account__c=endCustomerAcct.id,Price_Book__c=pb_IT2.Id,External_ID__c=dpaExtId2);
            insert dpa2;
     }
    @isTest static void testOne(){
          
    Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
    Test.startTest();
        List<PriceBook2> pbList = new List<PriceBook2>();
        pbList = [Select id,SAP_Price_Account_Code__c from Pricebook2 where name='Test IT2'];
        String pbid;
        if(pbList.size()>0){
            pbList[0].SAP_Price_Account_Code__c = '';
            update pbList[0];
            pbId = String.valueof(pbList[0].id);
        }
        Opportunity opptyIT =[Select id,Name,Opportunity_Number__c,
                                                AccountId,
                                                Account.Type,Account.Name,CloseDate,Owner.Name,
                                                Type,
                                                RecordTypeId,
                                                Pricebook2Id,
                                                pricebook2.SAP_Price_Account_Code__c,
                                                stageName, 
                                                Division__c,
                                                Channel__c ,
                                                IsClosed
                                                from Opportunity where Division__c='IT'];
        
        String opptyId = opptyIT.id;
        Opportunity opp= OpportunityPartnerExtension_Ltng.getOppDetails(opptyId);
        List<Partner__c> partnerslist=OpportunityPartnerExtension_Ltng.getSelectedPartners(opptyId);
        List<OpportunityPartnerExtension_Ltng.selectedPartnerWrapper> selectedPartners = OpportunityPartnerExtension_Ltng.getSelectedPartnersWrapper2(opptyId);
        List<String> selAcntIds = new List<String>();
        for(OpportunityPartnerExtension_Ltng.selectedPartnerWrapper spw : selectedPartners){
            selAcntIds.add(spw.PartnerId);
        }
        List<Channel__c> channelList = new List<Channel__c>();
        channelList = OpportunityPartnerExtension_Ltng.getChannelList(opptyId);
        Map<Account, List<Account>> channelMap = OpportunityPartnerExtension_Ltng.getChannelMap(channelList);
        List<OpportunityPartnerExtension_Ltng.selectedPartnerWrapper> deletePartners = new List<OpportunityPartnerExtension_Ltng.selectedPartnerWrapper>();
        String processSave = OpportunityPartnerExtension_Ltng.onSave(opptyIT,selectedPartners,deletePartners,partnerslist,selAcntIds,true,null);
        String processSave2 = OpportunityPartnerExtension_Ltng.onSave(opptyIT,selectedPartners,deletePartners,partnerslist,selAcntIds,false,new List<Channel__c>());
        List<Channel__c> processChannels = OpportunityPartnerExtension_Ltng.removePartnerAndItsChannels(selectedPartners[0],channelList);
        Set<id> partnerIdSet = new Set<id>();
        partnerIdSet.add(partnerslist[0].id);
        String processConRoles = OpportunityPartnerExtension_Ltng.processContactRoles(partnerIdSet,opptyIT);
        Set<String> distributorSet = new Set<String>(Label.Distributor_Roles.split('; '));
        boolean distributorPrimaryCheck = OpportunityPartnerExtension_Ltng.distributorPrimaryCheck(partnerslist,distributorSet);
        Partner__c p = new Partner__c();
        List<String> selectOptions = OpportunityPartnerExtension_Ltng.getselectOptions(p,'Role__c');
        String theme = OpportunityPartnerExtension_Ltng.getUIThemeDescription();
        
        List<OpportunityPartnerExtension_Ltng.partnerWrapperCls> availableList = OpportunityPartnerExtension_Ltng.updateAvailableList('', 'distributor', opptyIT);
        List<OpportunityPartnerExtension_Ltng.partnerWrapperCls> availableList2 = OpportunityPartnerExtension_Ltng.updateAvailableList('t', 'distributor', opptyIT);
        List<OpportunityPartnerExtension_Ltng.partnerWrapperCls> availableList3 = OpportunityPartnerExtension_Ltng.updateAvailableList('t', null, opptyIT);
        List<OpportunityPartnerExtension_Ltng.partnerWrapperCls> availableList4 = OpportunityPartnerExtension_Ltng.updateAvailableList('t', 'Reseller', opptyIT);
        List<OpportunityPartnerExtension_Ltng.partnerWrapperCls> availableList5 = OpportunityPartnerExtension_Ltng.updateAvailableList('', 'Reseller', opptyIT);
        
        opptyIT.Type='Internal';
        
        List<OpportunityPartnerExtension_Ltng.partnerWrapperCls> availableList6 = OpportunityPartnerExtension_Ltng.updateAvailableList('', 'distributor', opptyIT);
        List<OpportunityPartnerExtension_Ltng.partnerWrapperCls> availableList7 = OpportunityPartnerExtension_Ltng.updateAvailableList('t', 'distributor', opptyIT);
        List<OpportunityPartnerExtension_Ltng.partnerWrapperCls> availableList8 = OpportunityPartnerExtension_Ltng.updateAvailableList('t', null, opptyIT);
        List<OpportunityPartnerExtension_Ltng.partnerWrapperCls> availableList9 = OpportunityPartnerExtension_Ltng.updateAvailableList('t', 'Reseller', opptyIT);
        List<OpportunityPartnerExtension_Ltng.partnerWrapperCls> availableList10 = OpportunityPartnerExtension_Ltng.updateAvailableList('', 'Reseller', opptyIT);
        
    Test.stopTest();
        
    }
    
    @isTest static void testTwo(){
          
    Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
    Test.startTest();
        Opportunity opptyIT =[Select id,Name,Opportunity_Number__c,
                                                AccountId,
                                                Account.Type,Account.Name,CloseDate,Owner.Name,
                                                Type,
                                                RecordTypeId,
                                                Pricebook2Id,
                                                pricebook2.SAP_Price_Account_Code__c,
                                                stageName, 
                                                Division__c,
                                                Channel__c ,
                                                IsClosed
                                                from Opportunity where Division__c='Mobile'];
        
        List<OpportunityPartnerExtension_Ltng.partnerWrapperCls> availableList = OpportunityPartnerExtension_Ltng.updateAvailableList('', 'distributor', opptyIT);
        List<OpportunityPartnerExtension_Ltng.partnerWrapperCls> availableList2 = OpportunityPartnerExtension_Ltng.updateAvailableList('t', 'distributor', opptyIT);
        List<OpportunityPartnerExtension_Ltng.partnerWrapperCls> availableList3 = OpportunityPartnerExtension_Ltng.updateAvailableList('t', null, opptyIT);
        List<OpportunityPartnerExtension_Ltng.partnerWrapperCls> availableList4 = OpportunityPartnerExtension_Ltng.updateAvailableList('t', 'Reseller', opptyIT);
        List<OpportunityPartnerExtension_Ltng.partnerWrapperCls> availableList5 = OpportunityPartnerExtension_Ltng.updateAvailableList('', 'Reseller', opptyIT);
        
        opptyIT.Type='Tender';
        
        List<OpportunityPartnerExtension_Ltng.partnerWrapperCls> availableList6 = OpportunityPartnerExtension_Ltng.updateAvailableList('', 'distributor', opptyIT);
        List<OpportunityPartnerExtension_Ltng.partnerWrapperCls> availableList7 = OpportunityPartnerExtension_Ltng.updateAvailableList('t', 'distributor', opptyIT);
        List<OpportunityPartnerExtension_Ltng.partnerWrapperCls> availableList8 = OpportunityPartnerExtension_Ltng.updateAvailableList('t', null, opptyIT);
        List<OpportunityPartnerExtension_Ltng.partnerWrapperCls> availableList9 = OpportunityPartnerExtension_Ltng.updateAvailableList('t', 'Reseller', opptyIT);
        List<OpportunityPartnerExtension_Ltng.partnerWrapperCls> availableList10 = OpportunityPartnerExtension_Ltng.updateAvailableList('', 'Reseller', opptyIT);
        
    Test.stopTest();
        
    }
    
    @isTest static void testThree(){
          
    Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
    Test.startTest();
        Opportunity opptyIT =[Select id,Name,Opportunity_Number__c,
                                                AccountId,
                                                Account.Type,Account.Name,CloseDate,Owner.Name,
                                                Type,
                                                RecordTypeId,
                                                Pricebook2Id,
                                                pricebook2.SAP_Price_Account_Code__c,
                                                stageName, 
                                                Division__c,
                                                Channel__c ,
                                                IsClosed
                                                from Opportunity where Division__c='SBS'];
        
        List<OpportunityPartnerExtension_Ltng.partnerWrapperCls> availableList = OpportunityPartnerExtension_Ltng.updateAvailableList('', 'distributor', opptyIT);
        List<OpportunityPartnerExtension_Ltng.partnerWrapperCls> availableList2 = OpportunityPartnerExtension_Ltng.updateAvailableList('t', 'distributor', opptyIT);
        List<OpportunityPartnerExtension_Ltng.partnerWrapperCls> availableList3 = OpportunityPartnerExtension_Ltng.updateAvailableList('t', null, opptyIT);
        List<OpportunityPartnerExtension_Ltng.partnerWrapperCls> availableList4 = OpportunityPartnerExtension_Ltng.updateAvailableList('t', 'Reseller', opptyIT);
        List<OpportunityPartnerExtension_Ltng.partnerWrapperCls> availableList5 = OpportunityPartnerExtension_Ltng.updateAvailableList('', 'Reseller', opptyIT);
        
        opptyIT.Type='Tender';
        
        List<OpportunityPartnerExtension_Ltng.partnerWrapperCls> availableList6 = OpportunityPartnerExtension_Ltng.updateAvailableList('', 'distributor', opptyIT);
        List<OpportunityPartnerExtension_Ltng.partnerWrapperCls> availableList7 = OpportunityPartnerExtension_Ltng.updateAvailableList('t', 'distributor', opptyIT);
        List<OpportunityPartnerExtension_Ltng.partnerWrapperCls> availableList8 = OpportunityPartnerExtension_Ltng.updateAvailableList('t', null, opptyIT);
        List<OpportunityPartnerExtension_Ltng.partnerWrapperCls> availableList9 = OpportunityPartnerExtension_Ltng.updateAvailableList('t', 'Reseller', opptyIT);
        List<OpportunityPartnerExtension_Ltng.partnerWrapperCls> availableList10 = OpportunityPartnerExtension_Ltng.updateAvailableList('', 'Reseller', opptyIT);
        
    Test.stopTest();
        
    }

}