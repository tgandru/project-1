public with sharing class OpportunityPartnerRedirectExtension {
     
    Id oppId;
    string recId;
    String keyPrefix;
    
    public OpportunityPartnerRedirectExtension(ApexPages.StandardController controller) {
        recId = controller.getRecord().Id;
        
        Schema.DescribeSObjectResult r = Partner__c.sObjectType.getDescribe();
        keyPrefix = r.getKeyPrefix();
        System.debug('##Partner__c Prefix'+keyPrefix);
        
        if(recId.startsWithIgnoreCase(keyPrefix)){
            oppId = [select Id, Opportunity__c from Partner__c where Id = :recId limit 1].Opportunity__c;
        } 
        
    }
    
    // Redirect to the Opportunity Partner page
    public pageReference redirect(){
        
        if(recId.startsWithIgnoreCase(keyPrefix)){
            return new PageReference('/apex/OpportunityPartnerCreation?id=' + oppId);
        } 
        return null;
    }

}