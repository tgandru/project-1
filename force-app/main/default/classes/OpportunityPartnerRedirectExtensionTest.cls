@isTest
private class OpportunityPartnerRedirectExtensionTest {
    static Id endUserRT = TestDataUtility.retrieveRecordTypeId('End_User', 'Account');
    static Id inDirectRT = TestDataUtility.retrieveRecordTypeId('Indirect', 'Account');
    static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
    
    @isTest static void test_method_one() {
        // Implement test code

        //Channelinsight configuration
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        
        //Creating test Account
        Account acct=TestDataUtility.createAccount('Main Account');
        acct.SAP_Company_Code__c=TestDataUtility.generateRandomString(12);
        acct.RecordTypeId = endUserRT;
        insert acct;
        
        
       //Creating the Parent Account for Distributor as Partners
        Account paDistAcct=TestDataUtility.createAccount('Distributor Account');
        paDistAcct.RecordTypeId = directRT;
        paDistAcct.Type= 'Distributor';
        insert paDistAcct;

        //Creating the Parent Account for Reseller as Partners
        Account paResellAcct=TestDataUtility.createAccount('Reseller Account');
        paResellAcct.RecordTypeId = inDirectRT;
        paResellAcct.Type = 'Corporate Reseller';
        insert paResellAcct;
        
        PriceBook2 priceBook = TestDataUtility.createPriceBook('Shady Records');
        insert pricebook;

        Opportunity opportunity = TestDataUtility.createOppty('New', acct, pricebook, 'Managed', 'IT', 'product group',
                                    'Identified');
        insert opportunity;

        //Creating the partners
         Partner__c partner1 = TestDataUtility.createPartner(opportunity,acct,paDistAcct,'Distributor');
         insert partner1 ;

         Partner__c partner2 = TestDataUtility.createPartner(opportunity,acct,paResellAcct,'Corporate Reseller');
         insert partner2 ;

         OpportunityPartnerRedirectExtension partnerExtensionController = new OpportunityPartnerRedirectExtension(new ApexPages.StandardController(partner1));
         partnerExtensionController.redirect();

    }
    
    
    
}