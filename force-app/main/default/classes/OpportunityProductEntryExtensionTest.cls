/*Modified by Aarthi R C*/
    @isTest
    private class OpportunityProductEntryExtensionTest {
        /*static Map <String,Schema.RecordTypeInfo> accountRecordTypes = Account.sObjectType.getDescribe().getRecordTypeInfosByName();
        static Id endUserRT = accountRecordTypes .get('End Customer').getRecordTypeId();
        static Id directRT = accountRecordTypes .get('Direct').getRecordTypeId();*/
        static Id endUserRT = TestDataUtility.retrieveRecordTypeId('End_User', 'Account'); 
        static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
        
        @testSetup static void setup() {
            //Channelinsight configuration
            Channelinsight.CI_TEST_TestUtils.createBaseInstallData();

            Account acct=TestDataUtility.createAccount('Test Acct');
            insert acct;

            PriceBook2 pb=TestDataUtility.createPriceBook('Test PB');
            insert pb;

            List<Opportunity> opps=new List<Opportunity>();
            Opportunity oppOpen=TestDataUtility.createOppty('Open Oppty',acct, pb, 'RFP', 'IT', 'FAX/MFP', 'Identified');
            opps.add(oppOpen);
            Opportunity noPB = TestDataUtility.createOppty('NoPriceBook', acct, null, 'RFP', 'IT', 'FAX/MFP', 'Identified');
            opps.add(noPB);
            Opportunity oppOpen1=TestDataUtility.createOppty('Open Oppty1',acct, pb, 'RFP', 'IT', 'LCD_MONITOR', 'Identified');
            opps.add(oppOpen1);
            //TODO change it back to 'Drop' after checking with Mayank

            /*throws the below exception when the opportunity stage name = 'drop' 
            System.DmlException: Insert failed. First exception on row 0; first error: 
            CANNOT_EXECUTE_FLOW_TRIGGER, The record couldn’t be saved because it failed to trigger a flow. 
            A flow trigger failed to execute the flow with version ID 301630000000MOy.  
            Contact your administrator for help.: []*/
            
            /*Opportunity oppClosed=TestDataUtility.createOppty('Closed Oppty',acct, pb, 'RFP', 'IT', 'FAX/MFP', 'Drop');
            opps.add(oppClosed);*/
            //Opportunity oppClosed=TestDataUtility.createOppty('Closed Oppty',acct, pb, 'RFP', 'IT', 'FAX/MFP', 'Commit');
            //opps.add(oppClosed);
            insert opps;

            /* Creating Products: SAP_Material_Id__C should be unique and case sensitive ID
            Used the utility method to generate random string for the SAP_Material_ID as it should be unique 
            when every time you passed on
            */

            List<Product2> prods=new List<Product2>();
            Product2 standaloneProd=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Standalone Prod1', 'Printer[C2]', 'FAX/MFP', 'H/W');
            prods.add(standaloneProd);
            Product2 parentProd1=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Parent Prod1', 'Printer[C2]', 'FAX/MFP', 'H/W');
            prods.add(parentProd1);
            Product2 childProd1=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod1', 'Printer[C2]', 'FAX/MFP', 'Service pack');
            prods.add(childProd1);
            Product2 childProd2=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod2', 'Printer[C2]', 'FAX/MFP', 'Service pack');
            prods.add(childProd2);
            Product2 parentProd2=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Parent Prod2', 'Printer[C2]', 'FAX/MFP', 'H/W');
            prods.add(parentProd2);
            Product2 childProd3=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod3', 'Printer[C2]', 'FAX/MFP', 'Service pack');
            prods.add(childProd3);
            Product2 childProd4=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod4', 'Printer[C2]', 'FAX/MFP', 'Service pack');
            prods.add(childProd4);
            Product2 childProd5Standalone=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12), 'child AND standalone prod5', 'Printer[C2]', 'FAX/MFP', 'Service pack');
            prods.add(childProd5Standalone);
            Product2 LCDProduct=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12), 'LCD Product', 'MONITOR [C1]', 'LCD_MONITOR', 'Service pack');
            prods.add(LCDProduct);
            insert prods;

             /* Creating the PricebookEntries
            First get standard price book ID to pass it on to create PriceBook Entries.
            This is available irrespective of the state of SeeAllData.*/
            
            Id pricebookId = Test.getStandardPricebookId();

            //Since product and the pricebook are the lookup field we are passing their ids
            //PriceBookEntry for the standard pricebook
            /*List<PriceBookEntry> pbes=new List<PriceBookEntry>();
            PriceBookEntry standaloneProdPBE1=TestDataUtility.createPriceBookEntry(standaloneProd.Id, pricebookId );
            pbes.add(standaloneProdPBE1);
            PriceBookEntry parentProd1PBE1=TestDataUtility.createPriceBookEntry(parentProd1.Id, pricebookId);
            pbes.add(parentProd1PBE1);
            PriceBookEntry childProd1PBE1=TestDataUtility.createPriceBookEntry(childProd1.Id, pricebookId);
            pbes.add(childProd1PBE1);
            PriceBookEntry childProd2PBE1=TestDataUtility.createPriceBookEntry(childProd2.Id, pricebookId);
            pbes.add(childProd2PBE1);
            PriceBookEntry parentProd2PBE1=TestDataUtility.createPriceBookEntry(parentProd2.Id, pricebookId);
            pbes.add(parentProd2PBE1);
            PriceBookEntry childProd3PBE1=TestDataUtility.createPriceBookEntry(childProd3.Id, pricebookId);
            pbes.add(childProd3PBE1);
            PriceBookEntry childProd4PBE1=TestDataUtility.createPriceBookEntry(childProd4.Id, pricebookId);
            pbes.add(childProd4PBE1);
            insert pbes;*/
            
            //create the pricebook entries for the custom pricebook
            List<PriceBookEntry> custompbes=new List<PriceBookEntry>();
            PriceBookEntry standaloneProdPBE=TestDataUtility.createPriceBookEntry(standaloneProd.Id, pb.Id);
            custompbes.add(standaloneProdPBE);
            PriceBookEntry parentProd1PBE=TestDataUtility.createPriceBookEntry(parentProd1.Id, pb.Id);
            custompbes.add(parentProd1PBE);
            PriceBookEntry childProd1PBE=TestDataUtility.createPriceBookEntry(childProd1.Id, pb.Id);
            custompbes.add(childProd1PBE);
            PriceBookEntry childProd2PBE=TestDataUtility.createPriceBookEntry(childProd2.Id, pb.Id);
            custompbes.add(childProd2PBE);
            PriceBookEntry parentProd2PBE=TestDataUtility.createPriceBookEntry(parentProd2.Id, pb.Id);
            custompbes.add(parentProd2PBE);
            PriceBookEntry childProd3PBE=TestDataUtility.createPriceBookEntry(childProd3.Id, pb.Id);
            custompbes.add(childProd3PBE);
            PriceBookEntry childProd4PBE=TestDataUtility.createPriceBookEntry(childProd4.Id, pb.Id);
            custompbes.add(childProd4PBE);
            PriceBookEntry MonitorPBE=TestDataUtility.createPriceBookEntry(LCDProduct.Id, pb.Id);
            custompbes.add(MonitorPBE);
            

            insert custompbes;
            
            //Product Relationship
            List<Product_Relationship__c> prs=new List<Product_Relationship__c>();
            Product_Relationship__c pr1=TestDataUtility.createProductRelationships(childProd1, parentProd1);
            prs.add(pr1);
            Product_Relationship__c pr2=TestDataUtility.createProductRelationships(childProd2, parentProd1);
            prs.add(pr2);
            Product_Relationship__c pr3=TestDataUtility.createProductRelationships(childProd3, parentProd2);
            prs.add(pr3);
            Product_Relationship__c pr4=TestDataUtility.createProductRelationships(childProd4, parentProd2);
            prs.add(pr4);
            Product_Relationship__c pr5=TestDataUtility.createProductRelationships(childProd5Standalone, parentProd2);
            prs.add(pr5);
            insert prs;

            // Creating opportunity Line item
            List<OpportunityLineItem> olis=new List<OpportunityLineItem>();
            OpportunityLineItem standaloneProdOLI=TestDataUtility.createOpptyLineItem(standaloneProd, standaloneProdPBE, oppOpen);
            olis.add(standaloneProdOLI);
            OpportunityLineItem parentProd1OLI=TestDataUtility.createOpptyLineItem(parentProd1, parentProd1PBE, oppOpen);
            parentProd1OLI.parent_product__c=parentProd1.Id;
            olis.add(parentProd1OLI);
            OpportunityLineItem childProd1OLI=TestDataUtility.createOpptyLineItem(childProd1, childProd1PBE, oppOpen);
            childProd1OLI.parent_product__c=parentProd1.Id;
            olis.add(childProd1OLI);
            OpportunityLineItem childProd2OLI=TestDataUtility.createOpptyLineItem(childProd2, childProd2PBE, oppOpen);
            childProd2OLI.parent_product__c=parentProd1.Id;
            olis.add(childProd2OLI);
            OpportunityLineItem MonitorOLI=TestDataUtility.createOpptyLineItem(childProd2, childProd2PBE, oppOpen1);
            olis.add(MonitorOLI);
            insert olis;


        }
        //scenario : Best case scenario of having an opportunity which has a PB and PBE & OLI 
        //and selecting products (which has a relationship) to the shopping cart

        @isTest static void test_method_two() {

            System.debug('lclc testing for pbes '+[select id from PriceBookEntry where IsActive=true and Pricebook2Id !=null and Product2.Product_Group__c = 'FAX/MFP' ].size());
            Opportunity opptyOpen =[select Id,StageName,ProductGroupTest__c from Opportunity where PriceBook2.Id != null and ProductGroupTest__c='LCD_MONITOR'];
            List<OpportunityLineItem> oppLineItems =  [select PriceBookEntryId from OpportunityLineItem where OpportunityId =: opptyOpen.Id];
            system.debug('Price book id' + oppLineItems.get(0).Id);
            opportunityProductEntryExtension controllerOpen=new opportunityProductEntryExtension(new ApexPages.StandardController(opptyOpen));
            //simluate search of product
            //controllerOpen.selectedProdGroup='All';
            controllerOpen.searchString='Parent Prod2';
            
            controllerOpen.priceBookCheck();
            controllerOpen.updateAvailableList();
            System.debug('lclc testing avalable prods '+controllerOpen.lstAvailableProducts); //assuming this has items in it...it should only have the one.
            //simulate selecting product (parent product with children)
            controllerOpen.lstAvailableProducts[0].isSelected=true;
            controllerOpen.addCheckedItemsToShoppingCart();
            //simulate removing product from shopping cart list
            controllerOpen.toUnselect=controllerOpen.shoppingCartLst[2].pbEntryId;
            //simluate save
            //controllerOpen.onSave();
            controllerOpen.onPriceCheck();
            controllerOpen.removeAllFromShoppingCart();
            controllerOpen.dummyMethod();//added on 09/05/2019
            system.assertEquals(0,controllerOpen.shoppingCartLst.size());

        }

    //Scenario : When opportunity recordtype is service, the user can see the unit cost in the shopping list
        @isTest
        static void  blanket_serviceRT_opp_creation()
        {
            Account a = [select Id from Account limit 1];
            a.RecordTypeId = directRT;
            a.SAP_Company_Code__c='99999888';
        	update a;
            Opportunity opp = [select RecordTypeId,  Id,  Account.Name, Pricebook2Id, Type, PriceBook2.Name, StageName, Name, Amount, Opportunity_Number__c, ProductGroupTest__c,Division__c from Opportunity where StageName = 'Identified' and PriceBook2.id != null and ProductGroupTest__c='LCD_MONITOR'];
            String servicesRT = Opportunity.sObjectType.getDescribe().getRecordTypeInfosByName().get('Services').getRecordTypeId();
            opp.RecordTypeId = servicesRT;
            opp.AccountId = a.Id;
            opp.Type='Blanket';
            update opp;
            opportunityProductEntryExtension controllerOpen=new opportunityProductEntryExtension(new ApexPages.StandardController(opp));
            // based on product group...the search is going to be
            // system.debug('******************************prod Group' + opp.Product_Group__c);
            controllerOpen.searchString='prod';
            
            //checking to see if the opp has the pricebook attached or not
            controllerOpen.priceBookCheck();
            //Based on the search string it brings the list of products from the pricebook
            controllerOpen.updateAvailableList();
            //system.assertEquals(1, controllerOpen.AvailableProducts.size()); failed
            system.debug('************************** available products' + controllerOpen.lstAvailableProducts );
            controllerOpen.lstAvailableProducts[0].isSelected=true;
            //from the list selected the first product
            system.debug('**********************selected product'+ controllerOpen.lstAvailableProducts[0]);
            //adding the product to the cart
            controllerOpen.addCheckedItemsToShoppingCart();
            //simulate removing product from shopping cart list
            //system.assertEquals(5, controllerOpen.shoppingCartLst.size());
            system.assertEquals(2, controllerOpen.shoppingCartLst.size());
            system.debug('******************************************* product in shopping cart' + controllerOpen.shoppingCartLst.size());
            //controllerOpen.toUnselect=controllerOpen.shoppingCartLst[2].pbEntryId;
            system.debug('******************************************* after unselect product in shopping cart' + controllerOpen.shoppingCartLst.size());
            controllerOpen.removeFromShoppingCart();
            system.debug('******************************************* after removing products from the cart' + controllerOpen.shoppingCartLst.size());
            //system.assertEquals(4, controllerOpen.shoppingCartLst.size());
            system.assertEquals(2, controllerOpen.shoppingCartLst.size());
            controllerOpen.nada();
            controllerOpen.checkValuesSave();
            controllerOpen.checkValuesSaveAndCont();
            controllerOpen.onSave();
            controllerOpen.onCancel();
        }
        
        //Secnario : If the opportunity stage is commit(then the opp is considered closed) , users can not modify the products added 
        @isTest
        static void closed_opportunity()
        {
        //First add the products to the shopping cart and make the opportunity as closed
            Opportunity opptys=[select Id,StageName from Opportunity where PriceBook2.Id != null and ProductGroupTest__c!='LCD_MONITOR'];
            opportunityProductEntryExtension controllerOpen=new opportunityProductEntryExtension(new ApexPages.StandardController(opptys));
            //simluate search of product
            controllerOpen.searchString='Parent Prod2';
            //controllerOpen.selectedProdGroup='All';
            controllerOpen.priceBookCheck();
            controllerOpen.updateAvailableList();
            controllerOpen.onSave();
            System.debug('lclc '+controllerOpen.lstAvailableProducts); //assuming this has items in it...it should only have the one.
            //simulate selecting product (parent product with children)
            controllerOpen.lstAvailableProducts[0].isSelected=true;
            controllerOpen.addCheckedItemsToShoppingCart();
            //simluate save and calculate
            controllerOpen.onPriceCheck();

            //making the opportunity to close
            opptys.StageName = 'Drop';
            opptys.Drop_Reason__c= 'Price';
            opptys.CloseDate = system.today() - 2;
            update opptys;
            
            opportunityProductEntryExtension controllerclose=new opportunityProductEntryExtension(new ApexPages.StandardController(opptys));
            controllerclose.searchString='prod';
            //checking to see if the opp has the pricebook attached or not
            controllerclose.priceBookCheck();
            //Based on the search string it brings the list of products from the pricebook
            controllerclose.updateAvailableList();
            system.debug('************************** available products' + controllerOpen.lstAvailableProducts );
            controllerclose.lstAvailableProducts[0].isSelected=true;
            //from the list selected the first product
            controllerclose.addCheckedItemsToShoppingCart();
            ApexPages.Message[] pageMessages = ApexPages.getMessages();
            System.assertNotEquals(0, pageMessages.size());
            controllerclose.onSave();   
            controllerclose.onPriceCheck();
            controllerclose.removeFromShoppingCart();
            controllerclose.removeAllFromShoppingCart();
            controllerclose.onCancel();
        }

        @isTest
        static void  blanket_Monitor_opp_creation()
        {
            Account a = [select Id from Account limit 1];
            a.RecordTypeId = directRT;
            a.SAP_Company_Code__c='99999888';
        	update a;
            Opportunity opp = [select RecordTypeId,  Id,  Account.Name, Pricebook2Id, Type, PriceBook2.Name, StageName, Name, Amount, Opportunity_Number__c, ProductGroupTest__c,Division__c from Opportunity where StageName = 'Identified' and PriceBook2.id != null and ProductGroupTest__c='LCD_MONITOR'];
            String servicesRT = Opportunity.sObjectType.getDescribe().getRecordTypeInfosByName().get('Services').getRecordTypeId();
            opp.RecordTypeId = servicesRT;
            opp.AccountId = a.Id;
            opp.Type='Blanket';
            update opp;
            opportunityProductEntryExtension controllerOpen1=new opportunityProductEntryExtension(new ApexPages.StandardController(opp));
            controllerOpen1.searchString='prod';
            //checking to see if the opp has the pricebook attached or not
            controllerOpen1.priceBookCheck();
            //Based on the search string it brings the list of products from the pricebook
            controllerOpen1.updateAvailableList();
            system.debug('************************** available products' + controllerOpen1.lstAvailableProducts );
            controllerOpen1.lstAvailableProducts[0].isSelected=true;
            //from the list selected the first product
            system.debug('**********************selected product'+ controllerOpen1.lstAvailableProducts[0]);
            //adding the product to the cart
            controllerOpen1.addCheckedItemsToShoppingCart();
            //simulate removing product from shopping cart list
            //system.assertEquals(5, controllerOpen.shoppingCartLst.size());
            //system.assertEquals(1, controllerOpen1.shoppingCartLst.size());
            system.debug('******************************************* product in shopping cart' + controllerOpen1.shoppingCartLst.size());
            controllerOpen1.removeFromShoppingCart();
            system.debug('******************************************* after removing products from the cart' + controllerOpen1.shoppingCartLst.size());
            //system.assertEquals(4, controllerOpen.shoppingCartLst.size());
            //system.assertEquals(1, controllerOpen1.shoppingCartLst.size());
            controllerOpen1.nada();
            controllerOpen1.checkValuesSave();
            controllerOpen1.checkValuesSaveAndCont();
            controllerOpen1.onSave();
            controllerOpen1.onCancel();
        }

        //Scenario 4: Creating the Opportunity without the pricebook
        @isTest
        static void opp_no_PB()
        {
            Opportunity opp = [select Id, Pricebook2Id from Opportunity where PriceBook2.Id = null limit 1];
            opportunityProductEntryExtension controller =new opportunityProductEntryExtension(new ApexPages.StandardController(opp));
            //try to change the pricebook
            controller.priceBookCheck();
            controller.changePricebook();
            controller.onSave();
            system.assertNotEquals(null, controller.theBook);
            controller.onPriceCheck();

        }

        @isTest
        static void opp_pr_change(){

            List<OpportunityLineItem> olis=[select id, parent_product__c, PriceBookEntry.product2id from OpportunityLineItem];
            String parentId='';
            for(OpportunityLineItem oli:olis){
                if(oli.parent_product__c==oli.PriceBookEntry.product2id){
                    System.debug('lclc foudn parent');
                    parentId=oli.parent_product__c;
                }
            }
            System.debug('lclc parentid '+parentId);
            system.debug('lclc test '+[select id from Product_Relationship__c].size());

            List<Product_Relationship__c> prs=[select Parent_Product__c, Child_Product__c, Is_Active__c, id from Product_Relationship__c where parent_product__c=:parentId];
            prs[0].Is_Active__c=false;
            update prs[0];

            Opportunity opptys=[select Id,StageName from Opportunity where PriceBook2.Id != null and ProductGroupTest__c='LCD_MONITOR'];
            opportunityProductEntryExtension controllerOpen=new opportunityProductEntryExtension(new ApexPages.StandardController(opptys));
        }


        @isTest
        static void opp_with_quote(){
            List<Product2> prods=[select id from product2];

            Map<id,product2> prodMap=new Map<Id,product2>();
            prodMap.putAll(prods);

            List<OpportunityLineItem> olis=[select id, parent_product__c, PriceBookEntry.product2id, PriceBookEntry.product2.Name from OpportunityLineItem];

            List<PriceBookEntry> pbes=[select id,UnitPrice, Product2Id from PriceBookEntry where Product2Id in :prodMap.keySet()];

            Map<Id, PriceBookEntry> pbemap=new Map<Id,PriceBookEntry>();
            for(PriceBookEntry pbe:pbes){
                pbemap.put(pbe.product2id,pbe);
            }

            Opportunity opp=[select id,pricebook2Id from opportunity where pricebook2.Id!=null and ProductGroupTest__c='LCD_MONITOR'];

            Pricebook2 pb=[select Id from pricebook2 where id=:opp.Pricebook2Id];

            Quote quot = TestDataUtility.createQuote('Test Quote', opp, pb);
            quot.Status='Draft';
            insert quot;

            List<QuoteLineItem> qlis=new List<QuoteLineItem>();
            for(OpportunityLineItem oli:olis){
                
                qlis.add(TestDataUtility.createQuoteLineItem(quot.Id, prodMap.get(oli.PricebookEntry.Product2Id), pbemap.get(oli.PricebookEntry.Product2Id), oli));
            }

            //insert qlis;

            quot.status='Approved';
            update quot;

            opportunityProductEntryExtension controllerOpen=new opportunityProductEntryExtension(new ApexPages.StandardController(opp));

        }
        
    }