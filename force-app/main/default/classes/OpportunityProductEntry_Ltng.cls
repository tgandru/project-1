public class OpportunityProductEntry_Ltng {
	
    @AuraEnabled(cacheable=true)
    public static List<PBEListWrapper> getSKUInformation (Opportunity opp) {
        list<String> pg=new List<String>();
        if(opp.ProductGroupTest__c !=null){
            pg=opp.ProductGroupTest__c.split(';');
        }
        
       // return [SELECT Id,Product2Id,Product2.Product_Group__c,Product2.Name,Product2.PET_Name__c,Product2.SAP_Material_ID__c,Product2.Description,UnitPrice,Product2.Product_Type__c from PricebookEntry where isActive=True and  PriceBook2Id = :opp.Pricebook2Id  and  Product2.Product_Group__c in :pg order by Product2.Product_Group__c limit 49000];
        
        List<PricebookEntry> pbe=new List<PricebookEntry>([SELECT Id,Product2Id,Product2.Product_Group__c,Product2.Name,Product2.PET_Name__c,Product2.SAP_Material_ID__c,Product2.Description,UnitPrice,Product2.Product_Type__c,Product2.Custom_Product__c from PricebookEntry where isActive=True and  PriceBook2Id = :opp.Pricebook2Id  and  Product2.Product_Group__c in :pg order by Product2.Product_Group__c limit 49000]);
         List<PBEListWrapper> ListWrapper = new List<PBEListWrapper>();
         for(PricebookEntry p:pbe){
              ListWrapper.add(new PBEListWrapper(false,p.Product2.Product_Group__c,p.Product2.Name,p.Product2.PET_Name__c,p.Product2.SAP_Material_ID__c,p.Product2.Description,p.UnitPrice,p.Product2.Product_Type__c,p,p.Product2.Custom_Product__c));
         }
         
         return ListWrapper;
    }
    
     
    @auraEnabled
    public static String getInstanceUrl()
    {
        String InstanceURL = URL.getSalesforceBaseUrl().toExternalForm();
        return InstanceURL;
    }
    
        @AuraEnabled(cacheable=true)
    public static List<PBEListWrapper> getPBEList (String selectedProdGroup,string searchString,Opportunity opp) {
        
        String qString = 'select Id, Pricebook2Id, IsActive, Product2.Name, Product2.Family, Product2.Product_Group__c, Product2.Product_Type__c,Product2.IsActive, Product2.Description, UnitPrice, Product2.Pet_Name__c, Product2.SAP_Material_ID__c, product2.Product_Category__c, Unit_Cost__c, product2.Custom_Product__c from PricebookEntry where IsActive=true and Pricebook2Id = \'' +opp.Pricebook2Id + '\'';
        
            if(selectedProdGroup!=null){    
                if(selectedProdGroup=='All'){
                    qString+=' and ( Product2.Product_Group__c = ';
                    Integer count=1;
              
                    List<String> prodGroups=opp.ProductGroupTest__c.split(';');
                          System.debug('lclc prodGroups '+prodGroups);
                    for(String s:prodGroups){
                        
                        qString+='\''+s+'\'';
                        if(count<prodGroups.size()){
                            qString+=' OR Product2.Product_Group__C= ';
                        }else{
                            qString+=' ) ';
                        }
                        count++;
                    } 
                }else{
                    qString+='and Product2.Product_Group__c = \''+selectedProdGroup+'\' ';
                }
            }
        
                   
        
        
            // note that we are looking for the search string entered by the user in the name OR product family
            // modify this to search other fields if desired
            if(searchString!=null && searchString!=''){
                qString+= ' and (Product2.Name like \'%' + searchString + 
                            '%\' or Product2.Description like \'%' + searchString + 
                            '%\' or Product2.Product_Group__c like \'%' + searchString + 
                            '%\' or Product2.Pet_Name__c like \'%' + searchString + 
                            '%\' or Product2.SAP_Material_ID__c like \'%' + searchString + 
                            '%\' or Product2.Product_Type__c like \'%' + searchString + '%\')';
            }
                
          qString+= ' order by Product2.Name';
          qString+= ' limit 101';
        system.debug('Query ::'+qString);
        
         String ProdCategoryString = label.Service_Products_Category;
        List<String> ProdCategoryList = ProdCategoryString.split(',');
        List<PricebookEntry> pbe=new List<PricebookEntry>();
        pbe=database.query(qString);
         List<PBEListWrapper> ListWrapper = new List<PBEListWrapper>();
         for(PricebookEntry p:pbe){
              boolean customproduct=false;
                  if(p.product2.Custom_Product__c==false){
                      customproduct=true;
                  }
             
             String Productdescription='';
             if(p.Product2.Description !=null){
                 Productdescription =p.Product2.Description;
             }
             if(p.Product2.Product_Category__c!=null 
           && p.Product2.Product_Category__c!='' 
           && ProdCategoryList.contains(p.Product2.Product_Category__c)){
               if(p.Product2.PET_Name__c!=null&&p.Product2.PET_Name__c!=''){
                Productdescription=p.Product2.PET_Name__c;
            }
        }
             
             
              ListWrapper.add(new PBEListWrapper(false,p.Product2.Product_Group__c,p.Product2.Name,p.Product2.PET_Name__c,p.Product2.SAP_Material_ID__c,Productdescription,p.UnitPrice,p.Product2.Product_Type__c,p,customproduct));
         }
         
         return ListWrapper;
    }
     @AuraEnabled(cacheable=true)
    public static Opportunity getOppInformation (String Oppid) {
        return [SELECT Id,Name,Amount,Account.name,StageName,AccountId,CloseDate,Pricebook2Id,ProductGroupTest__c,Pricebook2.Name, Type,Opportunity_Number__c,OwnerId,Owner.Name,CountofApprovedQuotes__c,IsClosed,Division__c,B2B_SKU_Upsell__c,Solution_Included__c from Opportunity where id=:Oppid limit 1 ];
    }
    
      @AuraEnabled(cacheable=true)
    public static Boolean getHasunitcostPermission () {
        boolean haspermission=false;
        List<PermissionSetAssignment> perms=[SELECT Id,PermissionSetId FROM PermissionSetAssignment WHERE AssigneeId= :UserInfo.getUserId() AND PermissionSet.Name = 'View_Unit_Cost_and_Gross_Margin'];
            if(!perms.isEmpty()){
                haspermission=true;
            }
        return haspermission;
    }
    
     @AuraEnabled(cacheable=true)
    public static Boolean getHasmanageAp1Permission () {
        boolean haspermission=false;
        List<PermissionSetAssignment> Ap1Permission=[SELECT Id,PermissionSetId FROM PermissionSetAssignment WHERE AssigneeId= :UserInfo.getUserId() AND PermissionSet.Name = 'Manage_AP1'];
            if(!Ap1Permission.isEmpty()){
                haspermission=true;
            }     
        return haspermission;
    }
    
     @AuraEnabled(cacheable=true)
    public static List<shoppingCartWrapper> getOliInformation (Opportunity opp) {
      List<OpportunityLineItem> olis=new List<OpportunityLineItem> ( [select Id, Product2Id, Quantity, TotalPrice, ListPrice, UnitPrice, Discount__c, Description, Alternative__c, Product_Group__c, 
                        Product_Type__c, Shipped_Quantity__c, Claimed_Quantity__c, Unit_Cost__c, GM__c,Total_Discount__c,PriceBookEntryId, 
                        Requested_Price__c, PriceBookEntry.Name, PriceBookEntry.IsActive, PriceBookEntry.Product2.Description, PriceBookEntry.Product2Id, 
                        PriceBookEntry.Product2.Name, PriceBookEntry.Product2.Product_Group__c, PriceBookEntry.Product2.Product_Type__c, 
                        PriceBookEntry.Product2.Unit_Cost__c, PriceBookEntry.PriceBook2Id, PriceBookEntry.UnitPrice, Closed_List_Price__c,
                        AMPV_Mono__c, AMPV_Color__c, PriceBookEntry.Product2.SAP_Material_ID__c, Bundle_Flag__c, Parent_Product__c, PriceBookEntry.Product2.family,
                        PriceBookEntry.Unit_Cost__c,PriceBookEntry.Product2.PET_Name__c,Opportunity.Division__c,
                        PriceBookEntry.Product2.Custom_Product__c,PriceBookEntry.Product2.Product_Category__c,Opportunity.IsClosed,AP1__c
                        from OpportunityLineItem 
                        where OpportunityId=:opp.Id ]);
                          List<shoppingCartWrapper> ListWrapper = new List<shoppingCartWrapper>();
                          Map<String,QuoteLineItem> qliMap=new Map<String,QuoteLineItem>();
              
                for(QuoteLineItem qli:[select Id, OLIID__c, QuoteId 
                                    from QuoteLineItem where Quote.Status='Approved' AND Quote.OpportunityId=:opp.Id]){
                        qliMap.put(qli.OLIID__C,qli);
                    }
            
        
        
        
            for(OpportunityLineItem o:olis){
                if(qliMap.containskey(o.Id)){
                    QuoteLineItem qli=qliMap.get(o.Id);
                    ListWrapper.add(new shoppingCartWrapper(o,o.Opportunity.IsClosed,qli.id));
                }else{
                    ListWrapper.add(new shoppingCartWrapper(o,o.Opportunity.IsClosed,''));
                }
                   
            }     
            
            System.debug(' OLI ListWrapper ::'+ListWrapper);
              return ListWrapper;       
                        
    }
       @AuraEnabled(cacheable=true)
    public static List<QuoteLineItem> getQliInformation (Opportunity opp) {
         List<QuoteLineItem> qlis=new List<QuoteLineItem> ([select Id, OLIID__c, QuoteId 
                                    from QuoteLineItem where (Quote.Status='Approved' OR Quote.Status='Presented') AND Quote.OpportunityId=:opp.Id]);
            return qlis;
    }
    
    public class PBEListWrapper {
        @AuraEnabled public boolean isChecked {get;set;}
        @AuraEnabled public  String pdgroup {get;set;}
        @AuraEnabled public  String pdname {get;set;}
        @AuraEnabled public  String pdpetname {get;set;}
        @AuraEnabled public  String pdmodelcode {get;set;}
        @AuraEnabled public  String pddescription {get;set;}
        @AuraEnabled public  Decimal invoice {get;set;}
        @AuraEnabled public  Decimal unitcost {get;set;}
        @AuraEnabled public  String pdtype {get;set;}
        @AuraEnabled public boolean unitCostReadOnly {get;set;}
        @AuraEnabled public  PricebookEntry pbe {get;set;}
        public PBEListWrapper(boolean isChecked, String pdgroup,String pdname,String pdpetname,String pdmodelcode, String pddescription,Decimal invoice,String pdtype,PricebookEntry pbe,boolean  unitCostReadOnly){
            this.isChecked = isChecked;
            this.pdgroup = pdgroup;
            this.pdname = pdname;
            this.pdpetname = pdpetname;
            this.pdmodelcode = pdmodelcode;
            this.pddescription = pddescription;
            this.invoice = invoice;
            this.pdtype = pdtype;
            this.pbe = pbe;
            this.unitcost=pbe.Unit_Cost__c;
            this.unitCostReadOnly = unitCostReadOnly;
        }
    }
    
    
    public class shoppingCartWrapper{
      @AuraEnabled public opportunityLineItem oppli {get;set;}
      @AuraEnabled public String oppLiId{get;set;}
      @AuraEnabled public PriceBookEntry pbEntry {get;set;}
      @AuraEnabled public String pbEntryId {get;set;}
      @AuraEnabled public Integer productTypeInt {get;set;}
      @AuraEnabled public String productEntry {get;set;}
      @AuraEnabled public Boolean alternative {get;set;}
      @AuraEnabled public Integer quantity {get;set;}
      @AuraEnabled public Decimal salesPrice{get;set;}
      @AuraEnabled public Decimal listPrice{get;set;}
      @AuraEnabled public Decimal totalPrice{get;set;}
      @AuraEnabled public Decimal totalDiscount{get;set;}
      @AuraEnabled public Decimal requestedPrice{get;set;}
      @AuraEnabled public String productGroup{get;set;}
      @AuraEnabled public String productName{get;set;}
      @AuraEnabled public String productDescription{get;set;}
      @AuraEnabled public String productType{get;set;}
      @AuraEnabled public String petName{get;set;}
      @AuraEnabled public Boolean isNew{get;set;}
      @AuraEnabled public Decimal closedListPrice {get;set;}
      @AuraEnabled public Decimal discount{get;set;}
      @AuraEnabled public Decimal unitCost {get;set;}
      @AuraEnabled public Decimal ampvColor{get;set;}
      @AuraEnabled public Decimal ampvMono{get;set;}
      @AuraEnabled public String sapMaterialId{get;set;}
      @AuraEnabled public Boolean isAmpvPT{get;set;}
      @AuraEnabled public Boolean isBundlePT{get;set;}
      @AuraEnabled public String parentProdId{get;set;}
      @AuraEnabled public String selectedBundleFlag{get;set;}
      @AuraEnabled public Integer bundleCount{get;set;}
      @AuraEnabled public Boolean boldAndBlack{get;set;}
      @AuraEnabled public Boolean normalAndBlack{get;set;}
      @AuraEnabled public Boolean normalAndRed{get;set;}
      @AuraEnabled Public Boolean unitCostReadOnly{get;set;}
      @AuraEnabled Public string  qliid {get;set;}
      @AuraEnabled Public integer qliquantity{get;set;}
      @AuraEnabled Public decimal qliprice {get;set;}
      @AuraEnabled Public Boolean ap1 {get;set;}
    public shoppingCartWrapper(opportunityLineItem oli, Boolean isOppClosedBoolean, String Qlineid){
        System.debug('lclc in it ');
        oppli=oli;
        if(oppli.Id!=null){
            oppLiId=oppli.Id;
        }
        productGroup=oppLi.PriceBookEntry.Product2.Product_Group__c;
        productType=oppLi.PriceBookEntry.Product2.Product_Type__c;
        productName=oppLi.PriceBookEntry.Product2.Name;
        petName=oppLi.PriceBookEntry.Product2.PET_Name__c;//Added by Thiru to show PET Name -- 04/19/2018
        isAmpvPT=false;
        isBundlePT=false;
        if(productGroup!=null && (productGroup.toUpperCase().contains('PRINTER')||productGroup.toUpperCase().contains('FAX/MFP')||productGroup.toUpperCase().contains('A3 COPIER'))){
            if(productType!=null && ( productType.toUpperCase().contains('SET') || productType.toUpperCase().contains('H/W')) && (!productType.toUpperCase().contains('CON') && !productType.toUpperCase().contains('CONSUMABLE'))){
                isAmpvPT=true;
            }else if(productType != Null && (productType.toUpperCase().contains('CON') || productType.toUpperCase().contains('CONSUMABLE')) && (!productType.toUpperCase().contains('SET') && !productType.toUpperCase().contains('H/W'))){
                isBundlePT=true;
            }
        }
        
        if(Qlineid !='' && Qlineid !=null){
            qliid=Qlineid;
            qliquantity=oppLi.Quantity.intValue();
            qliprice=oppLi.Requested_Price__c;
        }
        
        if(oppLi.Opportunity.Division__c=='SBS'){
            productDescription=oppLi.Description;
        }else{
            productDescription=oppLi.Description;
        }

        productEntry=oppLi.Product2Id;
   
        pbEntryId=oppLi.PriceBookEntryId;
        pbEntry=oppLi.PriceBookEntry;
        quantity=oppLi.Quantity.intValue();
        totalDiscount=oppLi.Total_Discount__c;
        discount=oppLi.Discount__c;
        totalPrice=oppLi.TotalPrice;
        isNew=false;
        alternative=oppLi.Alternative__c;
        ap1=oppLi.AP1__c;
        requestedPrice=oppLi.Requested_Price__c;
        closedListPrice=oppLi.Closed_List_Price__c;
        unitCost=oppLi.Unit_Cost__c;
        ampvMono=oppLi.AMPV_Mono__c;
        ampvColor=oppLi.AMPV_Color__c;
        sapMaterialId=oppLi.priceBookEntry.Product2.SAP_Material_ID__c;
        selectedBundleFlag=oppLi.Bundle_Flag__c;
     
        if(oppLi.priceBookEntry.Product2.Custom_Product__c==false){
            unitCostReadOnly=true;
        }
        
        if(isOppClosedBoolean){
            //if opportunity is closed
            listPrice=oppLi.ListPrice;
        }else{
            //if opportunity is open
            listPrice=oppLi.PriceBookEntry.UnitPrice;
        }
        

        if(alternative){
            //if alternative=true
            salesPrice=0;
        }else{
            //if alternative=false
            salesPrice=oppLi.UnitPrice;
        }
    }
}
    
    @AuraEnabled
 Public static string SaveLineItems(String lineitemstring,Opportunity opp){
     //Added by Thiru---09/03/2019--Get current user's info--Starts
     String dassProfileStr = Label.Opportunity_D_A_S_S_Validation_Profile_Ids;
     if(dassProfileStr!='' && dassProfileStr!=null){
         String profId = UserInfo.getProfileId();
         profId = profId.left(15);
         if(dassProfileStr.contains(profId) && opp.StageName!=Label.OppStageIdentification && (opp.B2B_SKU_Upsell__c==null || opp.Solution_Included__c==null)){
             return 'The following is required for Qualified Stage: D.A.S.S. Qualification Section – Solution Included & B2B SKU Upsell. Please return to the Opportunity Detail page to update these fields';
         }
     }
     //Added by Thiru---09/03/2019--Get current user's info--Ends
     
     System.debug('::'+lineitemstring);
     List<shoppingCartWrapper> wrapperlist=(List<shoppingCartWrapper>)JSON.deserialize(lineitemstring, List<shoppingCartWrapper>.class);
     List<OpportunityLineItem> Upsertlist=new List<OpportunityLineItem>();
     
     for(shoppingCartWrapper scw:wrapperlist ){
           if(scw.oppLiId !=null && scw.oppLiId !=''){
                OpportunityLineItem oli1=unwrapLi(scw,false,opp);
                        Upsertlist.add(oli1);
           }else{
                OpportunityLineItem oli1=unwrapLi(scw,true,opp);
                        Upsertlist.add(oli1);
           }
     }
     
     try{
         upsert Upsertlist;
     }catch(exception ex){
         String error=ex.getMessage();
         return error;
     }
     
     return 'Ok';
 }

    
    Public static OpportunityLineItem unwrapLi(shoppingCartWrapper scw, Boolean isOLI,Opportunity theOpp){
    OpportunityLineItem oli;
        boolean hasap1permission=false;
        List<PermissionSetAssignment> Ap1Permission=[SELECT Id,PermissionSetId FROM PermissionSetAssignment WHERE AssigneeId= :UserInfo.getUserId() AND PermissionSet.Name = 'Manage_AP1'];
            if(!Ap1Permission.isEmpty()){
                hasap1permission=true;
            }
    if(isOLI==false){
        oli=new OpportunityLineItem(Id=scw.oppLiId, OpportunityId=theOpp.Id, PriceBookEntry=scw.pbEntry,PriceBookEntryId=scw.pbEntryId,
                                        Requested_Price__c=scw.requestedPrice,Description=scw.productDescription,
                                        Product_Group__c=scw.pbEntry.Product2.Product_Group__c,Product_Type__c=scw.pbEntry.Product2.Product_Type__c,
                                        Quantity=scw.quantity,Alternative__c=scw.alternative, Unit_Cost__c=scw.unitCost,
                                        Bundle_Flag__c=scw.selectedBundleFlag, AMPV_Mono__c=scw.ampvMono, AMPV_Color__c=scw.ampvColor,
                                        SAP_Material_ID__c=scw.sapMaterialId,Parent_Product__c=scw.parentProdId); 
        system.debug(oli.Description);
        if(oli.Alternative__c){
            oli.UnitPrice=0;
        }else{
            oli.UnitPrice=scw.requestedPrice;
        }
        if(hasap1permission){
            oli.AP1__c=scw.ap1;
        }
       /* if(isOppClosed){
            oli.Closed_List_Price__c=scw.closedListPrice;
        } */
    }else{
        oli=new OpportunityLineItem(OpportunityId=theOpp.Id, PriceBookEntry=scw.pbEntry,PriceBookEntryId=scw.pbEntryId,
                                        Requested_Price__c=scw.requestedPrice,Description=scw.productDescription,
                                        Product_Group__c=scw.productGroup,Product_Type__c=scw.productType,
                                        Quantity=scw.quantity,Alternative__c=scw.alternative, Unit_Cost__c=scw.unitCost,
                                        Bundle_Flag__c=scw.selectedBundleFlag, AMPV_Mono__c=scw.ampvMono, AMPV_Color__c=scw.ampvColor,
                                        SAP_Material_ID__c=scw.sapMaterialId,Parent_Product__c=scw.parentProdId);
        system.debug(oli.Description);
        if(oli.Alternative__c){
            oli.UnitPrice=0;
        }else{
            oli.UnitPrice=scw.requestedPrice;
        }
        
        if(hasap1permission){
            oli.AP1__c=scw.ap1;
        }
    }
    return oli;
}
    
   @AuraEnabled
    Public static string DeleteLineItems(List<String> Deletelineitems){
      /* List<shoppingCartWrapper> wrapperlist=(List<shoppingCartWrapper>)JSON.deserialize(Deletelineitemstring, List<shoppingCartWrapper>.class);
       List<OpportunityLineItem> deletelist=new List<OpportunityLineItem>();
       Set<id> deleteids=new set<id>();
     for(shoppingCartWrapper scw:wrapperlist ){
           if(scw.oppLiId !=null && scw.oppLiId !=''){
            
                        deleteids.add(scw.oppLiId);
           }
     }
     */
     try{
         if(Deletelineitems.size()>0){
             delete([Select id from OpportunityLineItem where id in:Deletelineitems]);
         }
         
     }catch(exception ex){
         String error=ex.getMessage();
         return error;
     }
     
     return 'Ok'; 
    }  
    
 	@AuraEnabled
    public static String getUIThemeDescription() {
        String theme = UserInfo.getUiThemeDisplayed();
        system.debug('Theme------>'+theme);
        return theme;
    }
    
}