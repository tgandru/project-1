@isTest
private class OpportunityProductEntry_Ltng_Test {

	static Id endUserRT = TestDataUtility.retrieveRecordTypeId('End_User', 'Account'); 
        static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
        
        @testSetup static void setup() {
            //Channelinsight configuration
            Channelinsight.CI_TEST_TestUtils.createBaseInstallData();

            Account acct=TestDataUtility.createAccount('Test Acct');
            insert acct;

            PriceBook2 pb=TestDataUtility.createPriceBook('Test PB');
            insert pb;

            List<Opportunity> opps=new List<Opportunity>();
            Opportunity oppOpen=TestDataUtility.createOppty('Open Oppty',acct, pb, 'RFP', 'IT', 'FAX/MFP', 'Identified');
            opps.add(oppOpen);
           
            //TODO change it back to 'Drop' after checking with Mayank

            /*throws the below exception when the opportunity stage name = 'drop' 
            System.DmlException: Insert failed. First exception on row 0; first error: 
            CANNOT_EXECUTE_FLOW_TRIGGER, The record couldn’t be saved because it failed to trigger a flow. 
            A flow trigger failed to execute the flow with version ID 301630000000MOy.  
            Contact your administrator for help.: []*/
            
            /*Opportunity oppClosed=TestDataUtility.createOppty('Closed Oppty',acct, pb, 'RFP', 'IT', 'FAX/MFP', 'Drop');
            opps.add(oppClosed);*/
            //Opportunity oppClosed=TestDataUtility.createOppty('Closed Oppty',acct, pb, 'RFP', 'IT', 'FAX/MFP', 'Commit');
            //opps.add(oppClosed);
            insert opps;

            /* Creating Products: SAP_Material_Id__C should be unique and case sensitive ID
            Used the utility method to generate random string for the SAP_Material_ID as it should be unique 
            when every time you passed on
            */

            List<Product2> prods=new List<Product2>();
            Product2 standaloneProd=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Standalone Prod1', 'Printer[C2]', 'FAX/MFP', 'H/W');
            prods.add(standaloneProd);
            Product2 parentProd1=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Parent Prod1', 'Printer[C2]', 'FAX/MFP', 'H/W');
            prods.add(parentProd1);
            Product2 childProd1=TestDataUtility.createProduct2('Test Search','Child Prod1', 'Printer[C2]', 'FAX/MFP', 'Service pack');
            prods.add(childProd1);
            Product2 childProd2=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod2', 'Printer[C2]', 'FAX/MFP', 'Service pack');
            prods.add(childProd2);
            Product2 parentProd2=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Parent Prod2', 'Printer[C2]', 'FAX/MFP', 'H/W');
            prods.add(parentProd2);
            Product2 childProd3=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod3', 'Printer[C2]', 'FAX/MFP', 'Service pack');
            prods.add(childProd3);
            Product2 childProd4=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod4', 'Printer[C2]', 'FAX/MFP', 'Service pack');
            prods.add(childProd4);
            Product2 childProd5Standalone=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12), 'child AND standalone prod5', 'Printer[C2]', 'FAX/MFP', 'Service pack');
            prods.add(childProd5Standalone);
            Product2 LCDProduct=TestDataUtility.createProduct2('LCD Product', 'LCD Product', 'MONITOR [C1]', 'LCD_MONITOR', 'Service pack');
            prods.add(LCDProduct);
            insert prods;

             /* Creating the PricebookEntries
            First get standard price book ID to pass it on to create PriceBook Entries.
            This is available irrespective of the state of SeeAllData.*/
            
            Id pricebookId = Test.getStandardPricebookId();

            //Since product and the pricebook are the lookup field we are passing their ids
            //PriceBookEntry for the standard pricebook
            /*List<PriceBookEntry> pbes=new List<PriceBookEntry>();
            PriceBookEntry standaloneProdPBE1=TestDataUtility.createPriceBookEntry(standaloneProd.Id, pricebookId );
            pbes.add(standaloneProdPBE1);
            PriceBookEntry parentProd1PBE1=TestDataUtility.createPriceBookEntry(parentProd1.Id, pricebookId);
            pbes.add(parentProd1PBE1);
            PriceBookEntry childProd1PBE1=TestDataUtility.createPriceBookEntry(childProd1.Id, pricebookId);
            pbes.add(childProd1PBE1);
            PriceBookEntry childProd2PBE1=TestDataUtility.createPriceBookEntry(childProd2.Id, pricebookId);
            pbes.add(childProd2PBE1);
            PriceBookEntry parentProd2PBE1=TestDataUtility.createPriceBookEntry(parentProd2.Id, pricebookId);
            pbes.add(parentProd2PBE1);
            PriceBookEntry childProd3PBE1=TestDataUtility.createPriceBookEntry(childProd3.Id, pricebookId);
            pbes.add(childProd3PBE1);
            PriceBookEntry childProd4PBE1=TestDataUtility.createPriceBookEntry(childProd4.Id, pricebookId);
            pbes.add(childProd4PBE1);
            insert pbes;*/
            
            //create the pricebook entries for the custom pricebook
            List<PriceBookEntry> custompbes=new List<PriceBookEntry>();
            PriceBookEntry standaloneProdPBE=TestDataUtility.createPriceBookEntry(standaloneProd.Id, pb.Id);
            custompbes.add(standaloneProdPBE);
            PriceBookEntry parentProd1PBE=TestDataUtility.createPriceBookEntry(parentProd1.Id, pb.Id);
            custompbes.add(parentProd1PBE);
            PriceBookEntry childProd1PBE=TestDataUtility.createPriceBookEntry(childProd1.Id, pb.Id);
            custompbes.add(childProd1PBE);
            PriceBookEntry childProd2PBE=TestDataUtility.createPriceBookEntry(childProd2.Id, pb.Id);
            custompbes.add(childProd2PBE);
            PriceBookEntry parentProd2PBE=TestDataUtility.createPriceBookEntry(parentProd2.Id, pb.Id);
            custompbes.add(parentProd2PBE);
            PriceBookEntry childProd3PBE=TestDataUtility.createPriceBookEntry(childProd3.Id, pb.Id);
            custompbes.add(childProd3PBE);
            PriceBookEntry childProd4PBE=TestDataUtility.createPriceBookEntry(childProd4.Id, pb.Id);
            custompbes.add(childProd4PBE);
            PriceBookEntry MonitorPBE=TestDataUtility.createPriceBookEntry(LCDProduct.Id, pb.Id);
            custompbes.add(MonitorPBE);
            

            insert custompbes;
            
            //Product Relationship
            List<Product_Relationship__c> prs=new List<Product_Relationship__c>();
            Product_Relationship__c pr1=TestDataUtility.createProductRelationships(childProd1, parentProd1);
            prs.add(pr1);
            Product_Relationship__c pr2=TestDataUtility.createProductRelationships(childProd2, parentProd1);
            prs.add(pr2);
            Product_Relationship__c pr3=TestDataUtility.createProductRelationships(childProd3, parentProd2);
            prs.add(pr3);
            Product_Relationship__c pr4=TestDataUtility.createProductRelationships(childProd4, parentProd2);
            prs.add(pr4);
            Product_Relationship__c pr5=TestDataUtility.createProductRelationships(childProd5Standalone, parentProd2);
            prs.add(pr5);
            insert prs;

            // Creating opportunity Line item
            List<OpportunityLineItem> olis=new List<OpportunityLineItem>();
            OpportunityLineItem standaloneProdOLI=TestDataUtility.createOpptyLineItem(standaloneProd, standaloneProdPBE, oppOpen);
            olis.add(standaloneProdOLI);
            OpportunityLineItem parentProd1OLI=TestDataUtility.createOpptyLineItem(parentProd1, parentProd1PBE, oppOpen);
            parentProd1OLI.parent_product__c=parentProd1.Id;
            olis.add(parentProd1OLI);
            OpportunityLineItem childProd1OLI=TestDataUtility.createOpptyLineItem(childProd1, childProd1PBE, oppOpen);
            childProd1OLI.parent_product__c=parentProd1.Id;
            olis.add(childProd1OLI);
            OpportunityLineItem childProd2OLI=TestDataUtility.createOpptyLineItem(childProd2, childProd2PBE, oppOpen);
            childProd2OLI.parent_product__c=parentProd1.Id;
            olis.add(childProd2OLI);
          
            insert olis;


        }
        
           @isTest static void test_method_one() {
               
               Test.startTest();
                Opportunity Opp=[Select Id from Opportunity limit 1];
                opp=OpportunityProductEntry_Ltng.getOppInformation(opp.id);
                string orgurl =OpportunityProductEntry_Ltng.getInstanceUrl();
                string theme= OpportunityProductEntry_Ltng.getUIThemeDescription();
                
                List<OpportunityProductEntry_Ltng.PBEListWrapper> olilist=OpportunityProductEntry_Ltng.getSKUInformation(opp);
                
                
                List<OpportunityProductEntry_Ltng.PBEListWrapper> pbelist1=OpportunityProductEntry_Ltng.getPBEList('FAX/MFP','', opp);
                
                List<OpportunityProductEntry_Ltng.PBEListWrapper> pbelist2=OpportunityProductEntry_Ltng.getPBEList('All','Test', opp);
               Test.Stoptest();
           }
           
            @isTest static void test_method_two() {
               
               Test.startTest();
                Opportunity Opp=[Select Id from Opportunity limit 1];
                opp=OpportunityProductEntry_Ltng.getOppInformation(opp.id);
                 Boolean unitcostpermission = OpportunityProductEntry_Ltng.getHasunitcostPermission();
                 boolean ap1permission=OpportunityProductEntry_Ltng.getHasmanageAp1Permission();
                 List<OpportunityProductEntry_Ltng.shoppingCartWrapper> olilist= OpportunityProductEntry_Ltng.getOliInformation(Opp);
                 List<OpportunityProductEntry_Ltng.PBEListWrapper> pbelist2=OpportunityProductEntry_Ltng.getPBEList('FAX/MFP','Test', opp);
                 PriceBookEntry pbe=[select Id, Pricebook2Id, IsActive, Product2.Name, Product2.Family, Product2.Product_Group__c, Product2.Product_Type__c,Product2.IsActive, Product2.Description, UnitPrice, Product2.Pet_Name__c, Product2.SAP_Material_ID__c, product2.Product_Category__c, Unit_Cost__c, product2.Custom_Product__c from PricebookEntry where IsActive=true limit 1];
                  List<String> ids=new List<String>();
                  ids.add(olilist[0].oppLiId);
                 
                 olilist[0].oppLiId=null;
                 
                
                 
                 
                 String saveresult= OpportunityProductEntry_Ltng.SaveLineItems(JSON.serialize(olilist),opp);
                 String deletereskut=OpportunityProductEntry_Ltng.DeleteLineItems(ids);
               Test.Stoptest();
           }
           
           
             @isTest static void test_method_three() {
               
               Test.startTest();
                Opportunity Opp=[Select Id from Opportunity limit 1];
               
                 PriceBookEntry pbe=[select Id, Pricebook2Id, IsActive, Product2.Name, Product2.Family, Product2.Product_Group__c, Product2.Product_Type__c,Product2.IsActive, Product2.Description, UnitPrice, Product2.Pet_Name__c, Product2.SAP_Material_ID__c, product2.Product_Category__c, Unit_Cost__c, product2.Custom_Product__c from PricebookEntry where IsActive=true limit 1];
                                  OpportunityProductEntry_Ltng.PBEListWrapper peb= new OpportunityProductEntry_Ltng.PBEListWrapper(false,'Test','Test',pbe.Product2.Name,pbe.Product2.Name,'Test',pbe.UnitPrice,'Test',pbe,false);

               Test.Stoptest();
           }
        
}