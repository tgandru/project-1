public class OpportunityRecordTypeRedirectExt {

    public OpportunityRecordTypeRedirectExt(ApexPages.StandardController controller) {
        
    }
    public pagereference redirect(){
        id id1 = userinfo.getProfileId();
        String profileName =[Select Name from Profile where Id =:id1].name;
        
        id SelectRecType = ApexPages.currentPage().getParameters().get('RecordType');
        String acntid = ApexPages.currentPage().getParameters().get('accid');
        string name = ApexPages.currentPage().getParameters().get('RecordType.name');
        
        
        //if(SelectRecType == Label.HA_Builder_Recordtypeid || profileName=='HA Sales User' || profileName=='System Administrator'){
        if(((SelectRecType == Label.HA_Builder_Recordtypeid || SelectRecType == null) && ( profileName=='HA Sales User' || profileName=='HA Builder Operations User' || profileName=='HA Builder Distributor User' || profileName=='HA Builder Ops User')) ||((SelectRecType == Label.HA_Builder_Recordtypeid || SelectRecType == Label.ForeCast_Recordtypeid) && profileName.toUpperCase()=='SYSTEM ADMINISTRATOR')){
            PageReference p;
            If(SelectRecType!= Null && acntid!= Null)
            {
                p = new PageReference('/006/e?retURL=%2F'+acntid+'&accid='+acntid+'&RecordType='+selectrectype+'&ent=opportunity');
            }Else if(SelectRecType!= Null && acntid == Null)
            {
                p = new PageReference('/006/e?retURL=%2F006%2Fo%3Fnooverride%3D1&RecordType='+selectrectype+'&ent=opportunity');
            }
            Else if(SelectRecType == Null && acntid!= Null)
            {
                p = new PageReference('/006/e?accid='+acntid+'&retURL=%2F001%2Fo%3Fnooverride%3D1&ent=opportunity');
            } 
            Else
            {
                p = new PageReference('/006/e?retURL=%2F006%2Fo%3Fnooverride%3D1&ent=opportunity');
            }                            
            p.getParameters().put('nooverride','1');
            return p;
        }
        else{
            pagereference p = new pagereference('/apex/NewOpportunity?retURL=%2F001%2Fo%3Fnooverride%3D1&accid='+acntid+'&RecordType='+selectrectype+'&ent=Opportunity&save_new=1&sfdc.override=1');
            p.setredirect(true);
            return p;
        }
    }

}