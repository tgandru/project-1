@isTest
private class OpportunityRecordTypeRedirectExtTest 
{
    static Id endUserRT = TestDataUtility.retrieveRecordTypeId('End_User', 'Account');
    Static testmethod void test1()
    {
         Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
         Opportunity opp = new Opportunity();
         ApexPages.StandardController sc = new ApexPages.StandardController(opp);
         OpportunityRecordTypeRedirectExt Oppredirect = new OpportunityRecordTypeRedirectExt(sc);
         pagereference pg = page.Opportunity_RecordType_Redirect;
         pg.getParameters().Put('RecordType',Label.HA_Builder_Recordtypeid);
         test.setcurrentpage(pg);
         Oppredirect.redirect();   
         
         Opportunity opp1 = new Opportunity();
         ApexPages.StandardController sc1 = new ApexPages.StandardController(opp1);
         OpportunityRecordTypeRedirectExt Oppredirect1 = new OpportunityRecordTypeRedirectExt(sc1);
         pagereference pg1 = page.Opportunity_RecordType_Redirect;
         //pg.getParameters().Put('RecordType',Label.HA_Builder_Recordtypeid);
         test.setcurrentpage(pg1);
         Oppredirect1.redirect();
         Id HARecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('HA Builder').getRecordTypeId();
         Id ITRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('IT').getRecordTypeId();
        // String strRecordTypeId = [Select Id From RecordType Where id =:Label.HA_Builder_Recordtypeid].Id;
         //String accRecordTypeId = [Select Id From RecordType where SobjectType = 'Account' and Name = 'End Customer'].Id;
         Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('End Customer').getRecordTypeId();
         Account Acc = new Account(name='HA',Type='Customer',RecordTypeid=accRecordTypeId);
         insert Acc;
         Account endCustomerAcct=TestDataUtility.createAccount(TestDataUtility.generateRandomString(2));
         endCustomerAcct.RecordTypeId = endUserRT ;
         endCustomerAcct.Type = 'Customer';
         insert endCustomerAcct;
         list<Opportunity> Opplist = new List<Opportunity>();
         Opportunity opp2 = new Opportunity(name='Test',stagename='Identified',CloseDate=Date.today(),RecordTypeid=HARecordTypeId,Project_Name__c='Test');
         //insert opp2;
         Opplist.add(opp2);
         //String ITRecordTypeId = [Select Id From RecordType where SobjectType = 'Opportunity' and Name = 'IT'].Id;
         Opportunity opp3 = new Opportunity(accountId=endCustomerAcct.id,recordtypeId=ITRecordTypeId,StageName='Identified',Name='TEst Opp',Type='Tender',Division__c='IT',ProductGroupTest__c='LCD_MONITOR',LeadSource='BTA Dealer',Closedate=system.today());
         Opplist.add(opp3);
         insert Opplist;
         system.debug('Opp2--------::'+Opplist);
          
         pagereference pg2 = page.OpportunityViewRedirect;
         pg2.getParameters().put('id',opp2.id);   
         test.setcurrentpage(pg2);
         ApexPages.StandardController sc2 = new ApexPages.StandardController(opp2);
         OpportunityViewRecordTypeRedirect OppView = new OpportunityViewRecordTypeRedirect(sc2);
         OppView.redirect();
         
         pagereference pg4 = page.OpportunityViewRedirect;
         pg2.getParameters().put('id',opp3.id);
         pg2.getParameters().put('accid',acc.id);   
         test.setcurrentpage(pg4); 
         ApexPages.StandardController sc5 = new ApexPages.StandardController(opp3);              
         OpportunityViewRecordTypeRedirect OppView1 = new OpportunityViewRecordTypeRedirect(sc5);
         OppView1.redirect(); 
         
         pagereference pg3 = page.OpportunityEditRedirect;
         pg3.getParameters().put('id',opp2.id);
         test.setcurrentpage(pg3);  
         ApexPages.StandardController sc3 = new ApexPages.StandardController(opp2);              
         OpportunityEditRedirectExt Edit = new OpportunityEditRedirectExt(sc3);
         Edit.redirect();
         
         pagereference pg5 = page.OpportunityEditRedirect;
         pg3.getParameters().put('id',opp3.id);
         pg2.getParameters().put('accid',acc.id);
         test.setcurrentpage(pg5); 
         ApexPages.StandardController sc4 = new ApexPages.StandardController(opp3);              
         OpportunityEditRedirectExt Edit1 = new OpportunityEditRedirectExt(sc4);
         Edit1.redirect();
         
    }
}