/**
 *  https://samsungsea--qa.cs18.my.salesforce.com/services/apexrest/OpportunityRollout/a0g360000024oyJAAQ
 *  Rollout Product -   a0e36000003MHauAAG  
 *  by hong.jeon
 */
@RestResource(urlMapping='/OpportunityRollout/*')
global with sharing class OpportunityRolloutRestResource {
    
    @HttpGet
   global static SFDCStub.OpportunityRolloutResponse getOpportuntityRollout() {
     RestRequest req = RestContext.request;
     RestResponse res = RestContext.response; 
     
     String rolloutId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
     //Roll_Out__c rollOut = [SELECT Id, Opportunity__c, 
     //            (SELECT Id, SAP_Material_Code__c, Product__r.Name, Product__r.PET_Name__c 
     //             FROM Roll_Out_Products__r) 
     //             FROM Roll_Out__c WHERE Id=:rolloutId];
     List<Roll_Out_Product__c> data = null;
     try {
       if(Test.isRunningTest() && rolloutId.equals('xxxxx')) {
         rolloutId = rolloutId.substring(10);
       }
       data = [SELECT Roll_Out_Plan__r.Id, Roll_Out_Plan__r.Opportunity__c, Id, Roll_Out_Plan__r.Opportunity__r.IL_CL__c,
             SAP_Material_Code__c, Product__r.Name, Product__r.Product_Group__c, Product__r.PET_Name__c,Roll_Out_Plan__r.createddate,Roll_Out_Plan__r.lastmodifieddate,isdeleted,
                        (SELECT Id, Plan_Date__c, Plan_Revenue__c, MonthYear__c, Plan_Quantity__c FROM Roll_Out_Schedules__r)
           FROM Roll_Out_Product__c 
           WHERE Roll_Out_Plan__r.Id=:rolloutId ];//ALL ROWS
     } catch(Exception e) {
       String errmsg = 'DB Query Error - '+e.getmessage() + ' - '+e.getStackTraceString();
            SFDCStub.OpportunityRolloutResponse response = new SFDCStub.OpportunityRolloutResponse();
            response.inputHeaders.MSGGUID = cihStub.giveGUID();
       response.inputHeaders.IFID   = '';
       response.inputHeaders.IFDate   = datetime.now().format('YYMMddHHmmss', 'America/New_York');
       response.inputHeaders.MSGSTATUS = 'F';
       response.inputHeaders.ERRORTEXT = 'Fail to retrieve an RollOutProduct('+rolloutId+') - '+req.requestURI + ', errmsg=' + errmsg;
       response.inputHeaders.ERRORCODE = '';
       
       return response;
     }
     
     SFDCStub.OpportunityRolloutResponse response = new SFDCStub.OpportunityRolloutResponse();
     response.inputHeaders.MSGGUID = cihStub.giveGUID();
     response.inputHeaders.IFID = '';
     response.inputHeaders.IFDate = datetime.now().format('YYMMddHHmmss', 'America/New_York');
     response.inputHeaders.MSGSTATUS = 'S';
     response.inputHeaders.ERRORTEXT = '';
     response.inputHeaders.ERRORCODE = '';
     
     response.body.PAGENO = 1;
     response.body.ROWCNT = 500;
     response.body.COUNT = 1;
     
     Integration_EndPoints__c endpoint = Integration_EndPoints__c.getInstance(SFDCStub.FLOW_082);
     string status;
     DateTime lastSuccessfulRun;
     DateTime currentDateTime = datetime.now();
     if(lastSuccessfulRun==null) {
       lastSuccessfulRun = currentDateTime - 2;
     }
     for(Roll_Out_Product__c rop : data) {
         if(rop.Roll_Out_Plan__r.createddate>=lastSuccessfulRun){
           status = 'New';
       }
       if(rop.Roll_Out_Plan__r.createddate<lastSuccessfulRun && rop.Roll_Out_Plan__r.LastModifiedDate>=lastSuccessfulRun){
           status = 'Modified';
       }
       if(rop.isdeleted==true){
           status = 'Deleted';
       }
       SFDCStub.OpportunityRolloutProduct orp = new SFDCStub.OpportunityRolloutProduct(rop,status);
       response.body.RolloutProduct.add(orp);
     }
     
     return response;
   }
   
   @HttpPost
   global static SFDCStub.OpportunityRolloutResponse getOpportuntityRolloutList() {
     Integration_EndPoints__c endpoint = Integration_EndPoints__c.getInstance(SFDCStub.FLOW_082);
     String layoutId = !Test.isRunningTest() ? endpoint.layout_id__c : SFDCStub.LAYOUTID_082;
     
     RestRequest req = RestContext.request;
     String postBody = req.requestBody.toString();
     
     SFDCStub.OpportunityRolloutResponse response = new SFDCStub.OpportunityRolloutResponse();
     SFDCStub.OpportunityRolloutRequest request = null;
     try {
       request = (SFDCStub.OpportunityRolloutRequest)json.deserialize(postBody, SFDCStub.OpportunityRolloutRequest.class);
     } catch(Exception e) {
       response.inputHeaders.MSGGUID = '';
       response.inputHeaders.IFID   = '';
       response.inputHeaders.IFDate   = '';
       response.inputHeaders.MSGSTATUS = 'F';
       response.inputHeaders.ERRORTEXT = 'Invalid Request Message. - '+postBody;
       response.inputHeaders.ERRORCODE = '';
       
       return response;
     }
                           
     response.inputHeaders.MSGGUID = request.inputHeaders.MSGGUID;
     response.inputHeaders.IFID     = request.inputHeaders.IFID;
     response.inputHeaders.IFDate  = request.inputHeaders.IFDate;
     
     message_queue__c mq = new message_queue__c(MSGUID__c = response.inputHeaders.MSGGUID, 
                           MSGSTATUS__c = 'S',  
                           Integration_Flow_Type__c = SFDCStub.FLOW_082,
                           IFID__c = response.inputHeaders.IFID,
                           IFDate__c = response.inputHeaders.IFDate,
                           External_Id__c = request.body.CONSUMER,
                           Status__c = 'success',
                           Object_Name__c = 'OpportunityRollout', 
                           retry_counter__c = 0);
   
     DateTime lastSuccessfulRun = endpoint.last_successful_run__c;
     DateTime currentDateTime = datetime.now();
     if(lastSuccessfulRun==null) {
       lastSuccessfulRun = currentDateTime - 2;
     }
     
     Integer limitCnt = SFDCStub.LIMIT_COUNT;
     Integer offSetNo = limitCnt*(request.body.PAGENO-1);
     if(offSetNo>=2000) {
       String errmsg = 'Invalid Page Number - ('+request.body.PAGENO+'), The offset should be less than 2000.';
            response.inputHeaders.MSGSTATUS = 'F';
       response.inputHeaders.ERRORTEXT = errmsg;
       response.inputHeaders.ERRORCODE = '';
       
       mq.Status__c = 'failed';
       mq.MSGSTATUS__c = 'F';
       mq.ERRORTEXT__c = errmsg;
       insert mq;
       
       return response;
     }
     
     List<String> excludedUsers = Label.Users_Excluded_from_Integration.split(',');//Added to exclude data last modified by these users----03/20/2019
     
     List<Roll_Out_Product__c> data = null;
     try {
       data = [SELECT Roll_Out_Plan__r.Id, Roll_Out_Plan__r.Opportunity__c, Id, Roll_Out_Plan__r.LastModifiedDate,Roll_Out_Plan__r.Opportunity__r.IL_CL__c,
             SAP_Material_Code__c, Product__r.Name, Product__r.Product_Group__c, Product__r.PET_Name__c,Roll_Out_Plan__r.createddate,isdeleted,
                     (SELECT Id, Plan_Date__c, Plan_Revenue__c, MonthYear__c, Plan_Quantity__c FROM Roll_Out_Schedules__r)
          FROM Roll_Out_Product__c
          WHERE Product__r.Family='MOBILE PHONE [G1]' 
                and Roll_Out_Plan__r.Opportunity__r.Recordtype.name!='HA Builder' 
                and Roll_Out_Plan__r.Opportunity__r.Recordtype.name!='Forecast'
                and Roll_Out_Plan__r.Opportunity__r.ProductGroupTest__c includes ('SMART_PHONE','KNOX','ACCESSORY','TABLET','FEATURE_PHONE','SOLUTION [MOBILE]') 
                and Roll_Out_Plan__r.LastModifiedDate >= :lastSuccessfulRun 
                and Roll_Out_Plan__r.LastModifiedDate<=:currentDateTime 
                and Roll_Out_Plan__r.LastModifiedById!=:excludedUsers 
                ORDER BY Roll_Out_Plan__r.LastModifiedDate ASC LIMIT :limitCnt OFFSET :offsetNo];// ALL ROWS
     } catch(Exception e) {
       response.inputHeaders.MSGSTATUS = 'F';
       response.inputHeaders.ERRORTEXT = 'DB Query Error - '+e.getmessage() + ' - '+e.getStackTraceString();
       response.inputHeaders.ERRORCODE = '';
       
       mq.Status__c = 'failed';
       mq.MSGSTATUS__c = 'F';
       mq.ERRORTEXT__c = response.inputHeaders.ERRORTEXT;
       insert mq;  System.debug(mq.ERRORTEXT__c);
       return response;
     }  
              
     response.inputHeaders.MSGSTATUS = 'S';
     response.inputHeaders.ERRORTEXT = '';
     response.inputHeaders.ERRORCODE = '';
     
     response.body.PAGENO = request.body.PAGENO;
     response.body.ROWCNT = limitCnt;
     response.body.COUNT  = data.size();
     
     DateTime LastModifiedDate = null;
     string status;
     for(Roll_Out_Product__c rop : data) {
       try {
         if(rop.Roll_Out_Plan__r.createddate>=lastSuccessfulRun){
             status = 'New';
         }
         if(rop.Roll_Out_Plan__r.createddate<lastSuccessfulRun && rop.Roll_Out_Plan__r.LastModifiedDate>=lastSuccessfulRun){
             status = 'Modified';
         }
         if(rop.isdeleted==true){
             status = 'Deleted';
         }
         SFDCStub.OpportunityRolloutProduct p = new SFDCStub.OpportunityRolloutProduct(rop,status);
         response.body.RolloutProduct.add(p);
         LastModifiedDate = p.instance.Roll_Out_Plan__r.LastModifiedDate;
         system.debug('Record--------->'+response.body.RolloutProduct);
       } catch(Exception e) {
         message_queue__c pemsg = new message_queue__c(MSGUID__c = response.inputHeaders.MSGGUID, MSGSTATUS__c = 'F',  Integration_Flow_Type__c = SFDCStub.FLOW_082,
                     IFID__c = response.inputHeaders.IFID, IFDate__c = response.inputHeaders.IFDate, Status__c = 'failed', Object_Name__c = 'OpportunityRollout', retry_counter__c = 0);
         pemsg.ERRORTEXT__c = 'Fail to create an OpportunityRolloutProduct - '+e.getmessage() + ' - '+e.getStackTraceString();
         pemsg.Identification_Text__c = rop.Id;
         insert pemsg;  System.debug(pemsg.ERRORTEXT__c);
       }
     }
     
     mq.last_successful_run__c = currentDateTime;
     mq.Object_Name__c = mq.Object_Name__c + ', ' + request.body.PAGENO + ', ' + response.body.COUNT;
     insert mq;
     
     if(!Test.isRunningTest() && response.body.COUNT!=0 && ((offSetNo+limitCnt)>=2000 || response.body.ROWCNT!=response.body.COUNT)) {
       endpoint.last_successful_run__c = currentDateTime;
       update endpoint;
     }
     return response;
   }
}