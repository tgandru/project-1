/**
 *  https://samsungsea--qa.cs65.my.salesforce.com/services/apexrest/OpportunityRolloutSECPI/a0g360000024oyJAAQ
 *  Rollout Product -   a0e36000003MHauAAG  
 */
@RestResource(urlMapping='/OpportunityRolloutSECPI/*')
global with sharing class OpportunityRolloutRestResourceSECPI {
    
    @HttpGet
    global static SFDCStubSECPI.OpportunityRolloutResponse getOpportuntityRolloutSECPI() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response; 
        
        String rolloutId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        //Roll_Out__c rollOut = [SELECT Id, Opportunity__c, 
        //                      (SELECT Id, SAP_Material_Code__c, Product__r.Name, Product__r.PET_Name__c 
        //                       FROM Roll_Out_Products__r) 
        //                     FROM Roll_Out__c WHERE Id=:rolloutId];
        List<Roll_Out_Product__c> data = null;
        try {
            if(Test.isRunningTest() && rolloutId.equals('xxxxx')) {
                rolloutId = rolloutId.substring(10);
            }
            data = [SELECT Roll_Out_Plan__r.Id, Roll_Out_Plan__r.Opportunity__c, Id, createddate, lastmodifieddate,SystemModStamp,Roll_Out_Plan__r.SystemModStamp,
                        SAP_Material_Code__c, Product__r.Name, Product__r.Product_Group__c, Product__r.PET_Name__c,Roll_Out_Plan__r.createddate,Roll_Out_Plan__r.lastmodifieddate,isdeleted,
                        (SELECT Id, Plan_Date__c, Plan_Revenue__c, MonthYear__c, Plan_Quantity__c FROM Roll_Out_Schedules__r)
                    FROM Roll_Out_Product__c 
                    WHERE Roll_Out_Plan__r.Id=:rolloutId ALL ROWS];
        } catch(Exception e) {
            String errmsg = 'DB Query Error - '+e.getmessage() + ' - '+e.getStackTraceString();
            SFDCStubSECPI.OpportunityRolloutResponse response = new SFDCStubSECPI.OpportunityRolloutResponse();
            response.inputHeaders.MSGGUID = cihStub.giveGUID();
            response.inputHeaders.IFID  = '';
            response.inputHeaders.IFDate    = datetime.now().format('YYMMddHHmmss', 'America/New_York');
            response.inputHeaders.MSGSTATUS = 'F';
            response.inputHeaders.ERRORTEXT = 'Fail to retrieve an RollOutProduct('+rolloutId+') - '+req.requestURI + ', errmsg=' + errmsg;
            response.inputHeaders.ERRORCODE = '';
            
            return response;
        }
        
        SFDCStubSECPI.OpportunityRolloutResponse response = new SFDCStubSECPI.OpportunityRolloutResponse();
        response.inputHeaders.MSGGUID = cihStub.giveGUID();
        response.inputHeaders.IFID = '';
        response.inputHeaders.IFDate = datetime.now().format('YYMMddHHmmss', 'America/New_York');
        response.inputHeaders.MSGSTATUS = 'S';
        response.inputHeaders.ERRORTEXT = '';
        response.inputHeaders.ERRORCODE = '';
        
        response.body.PAGENO = 1;
        response.body.ROWCNT = 500;
        response.body.COUNT = 1;
        
        Integration_EndPoints__c endpoint = Integration_EndPoints__c.getInstance(SFDCStubSECPI.FLOW_082);
        string status;
        DateTime lastSuccessfulRun;
        DateTime currentDateTime = datetime.now();
        if(lastSuccessfulRun==null) {
            lastSuccessfulRun = currentDateTime - 1;
        }
        for(Roll_Out_Product__c rop : data) {
            if(rop.Roll_Out_Plan__r.createddate>=lastSuccessfulRun){
                status = 'New';
            }
            if(rop.Roll_Out_Plan__r.createddate<lastSuccessfulRun && rop.Roll_Out_Plan__r.SystemModStamp>=lastSuccessfulRun){
                status = 'Modified';
            }
            if(rop.isdeleted==true){
                status = 'Deleted';
            }
            SFDCStubSECPI.OpportunityRolloutProduct orp = new SFDCStubSECPI.OpportunityRolloutProduct(rop,status);
            response.body.RolloutProduct.add(orp);
        }
        
        return response;
    }
    
    @HttpPost
    global static SFDCStubSECPI.OpportunityRolloutResponse getOpportuntityRolloutListSECPI() {
        integer pageNum;
        //integer pageLimit = 10;
        integer pageLimit = integer.valueOf(label.SECPI_Rollout_I_F_Page_Limit);
        Integration_EndPoints__c endpoint = Integration_EndPoints__c.getInstance(SFDCStubSECPI.FLOW_082);
        String layoutId = !Test.isRunningTest() ? endpoint.layout_id__c : SFDCStubSECPI.LAYOUTID_082;
        
        RestRequest req = RestContext.request;
        String postBody = req.requestBody.toString();
        
        SFDCStubSECPI.OpportunityRolloutResponse response = new SFDCStubSECPI.OpportunityRolloutResponse();
        SFDCStubSECPI.OpportunityRolloutRequest request = null;
        try {
            request = (SFDCStubSECPI.OpportunityRolloutRequest)json.deserialize(postBody, SFDCStubSECPI.OpportunityRolloutRequest.class);
        } catch(Exception e) {
            response.inputHeaders.MSGGUID = '';
            response.inputHeaders.IFID  = '';
            response.inputHeaders.IFDate    = '';
            response.inputHeaders.MSGSTATUS = 'F';
            response.inputHeaders.ERRORTEXT = 'Invalid Request Message. - '+postBody;
            response.inputHeaders.ERRORCODE = '';
            
            return response;
        }
                                                    
        response.inputHeaders.MSGGUID = request.inputHeaders.MSGGUID;
        response.inputHeaders.IFID    = request.inputHeaders.IFID;
        response.inputHeaders.IFDate  = request.inputHeaders.IFDate;
        
        string dates = 'From:'+request.body.SEARCH_DTTM_FROM+', To:'+request.body.SEARCH_DTTM_TO;
        
        List<message_queue__c> mqList = new List<message_queue__c>();
        message_queue__c mq = new message_queue__c(MSGUID__c = response.inputHeaders.MSGGUID, 
                                                    MSGSTATUS__c = 'S',  
                                                    Integration_Flow_Type__c = SFDCStubSECPI.FLOW_082,
                                                    IFID__c = response.inputHeaders.IFID,
                                                    IFDate__c = response.inputHeaders.IFDate,
                                                    External_Id__c = request.body.CONSUMER,
                                                    Status__c = 'success',
                                                    Object_Name__c = 'OpportunityRollout', 
                                                    retry_counter__c = 0,
                                                    Identification_Text__c = dates);
     
        /*List<AsyncApexJob> DeleteBatchProcessing = [select id,TotalJobItems,status,createddate from AsyncApexJob where apexclass.name='Batch_RolloutDataToSECPI_Delete' and (status='Holding' or status='Queued' or status='Preparing' or status='Processing') order by createddate desc limit 1];
        if(DeleteBatchProcessing.size()>0){    
            String errmsg = 'The Sent Data Delete Batch is in Process. Try callout after 10 mins';
            response.inputHeaders.MSGSTATUS = 'F'; response.inputHeaders.ERRORTEXT = errmsg; response.inputHeaders.ERRORCODE = '';mq.Status__c = 'failed'; mq.MSGSTATUS__c = 'F'; mq.ERRORTEXT__c = errmsg;
            insert mq;
            
            return response;
        }*/
        //Added logic for PAGENO Limit --- starts here 
        if(request.body.PAGENO!=null){
            pageNum = request.body.PAGENO;
            System.debug('PageNum----->'+pageNum);
        }else{
            String errmsg = 'Page Number is Missing in the Request';
            response.inputHeaders.MSGSTATUS = 'F'; 
            response.inputHeaders.ERRORTEXT = errmsg; 
            response.inputHeaders.ERRORCODE = ''; 
            mq.Status__c = 'failed'; 
            mq.MSGSTATUS__c = 'F'; 
            mq.ERRORTEXT__c = errmsg;
            insert mq;
            
            return response;
        }
        
        if(pageNum>pageLimit){
            Datetime new_SEARCH_DTTM = endpoint.Last_Successful_Run_Last_Modified_Date__c;
            System.debug('new SERACH_DTTM----->'+new_SEARCH_DTTM);
            String errmsg = 'SFDC SFDC PAGE LIMIT EXCEEDED. Please use new SEARCH_DTTM_FROM:'+new_SEARCH_DTTM+' and start from PAGENO:1 ' ;
            
            response.inputHeaders.MSGSTATUS = 'F';
            response.inputHeaders.ERRORTEXT = errmsg;
            response.inputHeaders.ERRORCODE = 'PAGE_LIMIT_EXCEEDED';
            response.inputHeaders.SEARCH_DTTM_FROM = SFDCStubSECPI.DtTimetoString(new_SEARCH_DTTM);
            
            mq.Status__c = 'failed';
            mq.MSGSTATUS__c = 'F';
            mq.ERRORTEXT__c = errmsg;
            insert mq;
            
            return response;
        }
        //---Ends Here
        
        //Block the callout if the Sent data delete batch is in process.
        List<AsyncApexJob> DeleteBatchProcessing = [select id,TotalJobItems,status,createddate from AsyncApexJob where apexclass.name='Batch_RolloutDataToSECPI_Delete' and (status='Holding' or status='Queued' or status='Preparing' or status='Processing') order by createddate desc limit 1];
        if(DeleteBatchProcessing.size()>0){    
            String errmsg = 'The Sent Data Delete Batch is in Process. Try callout after 10 mins';
            response.inputHeaders.MSGSTATUS = 'F'; response.inputHeaders.ERRORTEXT = errmsg; response.inputHeaders.ERRORCODE = '';mq.Status__c = 'failed'; mq.MSGSTATUS__c = 'F'; mq.ERRORTEXT__c = errmsg;
            insert mq;
            
            return response;
        }
        
        string DateFormat = 'yyyymmddhhmmss';
        string DateFormat2 = 'yyyymmdd';
        
        //Added the logic for New request format. Request includes FROM and TO SEARCH_DTTM--05/16/2018
        //Checking the date format.
        if((request.body.SEARCH_DTTM_FROM!=null && request.body.SEARCH_DTTM_FROM!='' && request.body.SEARCH_DTTM_FROM.length()!=DateFormat.length() && request.body.SEARCH_DTTM_FROM.length()!=DateFormat2.length())
            || (request.body.SEARCH_DTTM_TO!=null && request.body.SEARCH_DTTM_TO!='' && request.body.SEARCH_DTTM_TO.length()!=DateFormat.length() && request.body.SEARCH_DTTM_TO.length()!=DateFormat2.length())){
            String errmsg = 'Incorrect SEARCH_DTTM format. The Date Format must be yyyymmddhhmmss or yyyymmdd';
            response.inputHeaders.MSGSTATUS = 'F';
            response.inputHeaders.ERRORTEXT = errmsg;
            response.inputHeaders.ERRORCODE = 'Incorrect SEARCH_DTTM format';
            
            mq.Status__c = 'failed'; mq.MSGSTATUS__c = 'F'; mq.ERRORTEXT__c = errmsg;
            insert mq;
            
            return response;
        }
        //Check the Missng SEARCH_DTTM_TO and SEARCH_DTTM_FROM
        if((((request.body.SEARCH_DTTM_TO==null || request.body.SEARCH_DTTM_TO=='') && request.body.SEARCH_DTTM_FROM!=null && request.body.SEARCH_DTTM_FROM!=''))
            || (((request.body.SEARCH_DTTM_FROM==null || request.body.SEARCH_DTTM_FROM=='') && request.body.SEARCH_DTTM_TO!=null && request.body.SEARCH_DTTM_TO!=''))){
            String errmsg = 'SEARCH_DTTM_FROM or SEARCH_DTTM_TO is missing. The request should include both FROM and TO dates while sending TimeStamps';
            response.inputHeaders.MSGSTATUS = 'F';
            response.inputHeaders.ERRORTEXT = errmsg;
            response.inputHeaders.ERRORCODE = '';
            
            mq.Status__c = 'failed';
            mq.MSGSTATUS__c = 'F';
            mq.ERRORTEXT__c = errmsg;
            insert mq;
            
            return response;
        }
        
        if(request.body.SEARCH_DTTM_FROM!=null && request.body.SEARCH_DTTM_FROM!='' && request.body.SEARCH_DTTM_FROM.length()==DateFormat2.length()){
            request.body.SEARCH_DTTM_FROM=request.body.SEARCH_DTTM + '000000';
        }
        if(request.body.SEARCH_DTTM_TO!=null && request.body.SEARCH_DTTM_TO!='' && request.body.SEARCH_DTTM_TO.length()==DateFormat2.length()){
            request.body.SEARCH_DTTM_TO=request.body.SEARCH_DTTM + '000000';
        }
        
        DateTime lastSuccessfulRun = endpoint.last_successful_run__c;
        DateTime currentDateTime = datetime.now();
        DateTime SearchDateTo;
        DateTime SearchDateFrom;
        
        //Set From and To timestamps for Query
        if((request.body.SEARCH_DTTM_FROM!=null && request.body.SEARCH_DTTM_FROM!='')
            &&(request.body.SEARCH_DTTM_TO!=null && request.body.SEARCH_DTTM_TO!='')){
            SearchDateTo = setDateFromString(request.body.SEARCH_DTTM_TO);
            system.debug('SearchDateTo----->'+SearchDateTo);
            SearchDateFrom = setDateFromString(request.body.SEARCH_DTTM_FROM);
            system.debug('SearchDateFrom----->'+SearchDateFrom);
        }else{
            SearchDateTo = currentDateTime;
            system.debug('SearchDateTo----->'+SearchDateTo);
            SearchDateFrom = lastSuccessfulRun;
        }
        
        //Checing the SEARCH_DTTM_FROM and SEARCH_DTTM_TO time difference
        integer SecsDiff = Integer.valueOf((SearchDateTo.getTime() - SearchDateFrom.getTime())/(1000));
        if(SecsDiff>86400){
            String errmsg = 'Difference between SEARCH_DTTM_FROM and SEARCH_DTTM_TO should be less or Equal to 1 day';
            response.inputHeaders.MSGSTATUS = 'F';
            response.inputHeaders.ERRORTEXT = errmsg;
            response.inputHeaders.ERRORCODE = '';
            
            mq.Status__c = 'failed';
            mq.MSGSTATUS__c = 'F';
            mq.ERRORTEXT__c = errmsg;
            insert mq;
            
            return response;
        }
        
        Integer limitCnt = SFDCStubSECPI.Rollout_LIMIT_COUNT;//Set no. of records to send per page
        //Integer limitCnt = SFDCStubSECPI.LIMIT_COUNT;
        
        //Get all sent data
        Set<string> sentRopids = new Set<string>();
        List<Rollout_Data_to_SECPI__c> AllData = Rollout_Data_to_SECPI__c.getall().values();
        system.debug('size------->'+AllData.size());
        for(Rollout_Data_to_SECPI__c dataSend : AllData){
            sentRopids.add(dataSend.name);
        }
        
        List<Rollout_Data_to_SECPI__c> storeIds = new List<Rollout_Data_to_SECPI__c>();
        
        List<String> excludedUsers = Label.Users_Excluded_from_Integration.split(',');//Added to exclude data last modified by these users----03/20/2019
        
        List<Roll_Out_Product__c> data = null;
        try {
            data = [SELECT Roll_Out_Plan__r.Id, Roll_Out_Plan__r.Opportunity__c, Id, Roll_Out_Plan__r.LastModifiedDate,Roll_Out_Plan__r.SystemModStamp,createddate, SystemModStamp,
                        SAP_Material_Code__c, Product__r.Name, Product__r.Product_Group__c, Product__r.PET_Name__c,Roll_Out_Plan__r.createddate,isdeleted,lastmodifieddate,
                        (SELECT Id, Plan_Date__c, Plan_Revenue__c, MonthYear__c, Plan_Quantity__c FROM Roll_Out_Schedules__r)
                    FROM Roll_Out_Product__c 
                    WHERE Roll_Out_Plan__r.LastModifiedById!=:excludedUsers 
                          and Roll_Out_Plan__r.SystemModStamp >= :SearchDateFrom 
                          and Roll_Out_Plan__r.SystemModStamp<=:SearchDateTo 
                          and id!=:sentRopids 
                          and Roll_Out_Plan__r.Opportunity__r.Recordtype.name!='HA Builder' 
                          and Roll_Out_Plan__r.Opportunity__r.Recordtype.name!='Forecast' 
                          ORDER BY Roll_Out_Plan__r.SystemModStamp ASC LIMIT :limitCnt ALL ROWS];
        } catch(Exception e) {
            response.inputHeaders.MSGSTATUS = 'F';
            response.inputHeaders.ERRORTEXT = 'DB Query Error - '+e.getmessage() + ' - '+e.getStackTraceString();
            response.inputHeaders.ERRORCODE = '';
            
            mq.Status__c = 'failed';
            mq.MSGSTATUS__c = 'F';
            mq.ERRORTEXT__c = response.inputHeaders.ERRORTEXT;
            insert mq;  System.debug(mq.ERRORTEXT__c);
            return response;
        }   
                       
        response.inputHeaders.MSGSTATUS = 'S';
        response.inputHeaders.ERRORTEXT = '';
        response.inputHeaders.ERRORCODE = '';
        
        response.body.PAGENO = request.body.PAGENO;
        response.body.ROWCNT = limitCnt;
        response.body.COUNT  = data.size();
        
        DateTime LastModifiedDate = null;
        string status;
        for(Roll_Out_Product__c rop : data) {
            Rollout_Data_to_SECPI__c storeId = new Rollout_Data_to_SECPI__c(name=rop.id);
            storeIds.add(storeId);
            try {
                if(rop.Roll_Out_Plan__r.createddate>=SearchDateFrom){
                    status = 'New';
                }
                if(rop.Roll_Out_Plan__r.createddate<SearchDateFrom && rop.Roll_Out_Plan__r.SystemModStamp>=SearchDateFrom){
                    status = 'Modified';
                }
                if(rop.isdeleted==true){
                    status = 'Deleted';
                }
                SFDCStubSECPI.OpportunityRolloutProduct p = new SFDCStubSECPI.OpportunityRolloutProduct(rop,status);
                response.body.RolloutProduct.add(p);
                LastModifiedDate = p.instance.Roll_Out_Plan__r.systemModStamp;
                system.debug('Record--------->'+response.body.RolloutProduct);
            } catch(Exception e) {
                message_queue__c pemsg = new message_queue__c(MSGUID__c = response.inputHeaders.MSGGUID, MSGSTATUS__c = 'F',  Integration_Flow_Type__c = SFDCStubSECPI.FLOW_082,
                                        IFID__c = response.inputHeaders.IFID, IFDate__c = response.inputHeaders.IFDate, Status__c = 'failed', Object_Name__c = 'OpportunityRollout', retry_counter__c = 0);
                pemsg.ERRORTEXT__c = 'Fail to create an OpportunityRolloutProduct - '+e.getmessage() + ' - '+e.getStackTraceString();
                pemsg.Identification_Text__c = rop.Id;
                //insert pemsg;
                mqList.add(pemsg);
                System.debug(pemsg.ERRORTEXT__c);
            }
        }
        
        mq.last_successful_run__c = currentDateTime;
        mq.Object_Name__c = mq.Object_Name__c + ', ' + request.body.PAGENO + ', ' + response.body.COUNT;
        //insert mq;
        mqList.add(mq);
        
        if(mqList.size()>0){
            insert mqList;
        }
        
        Roll_Out_Product__c LastRop;
        if(data.size()>0){
            LastRop  = data[data.size() - 1];
            endpoint.Last_Successful_Run_Last_Modified_Date__c = LastRop.Roll_Out_Plan__r.SystemModStamp;
        }
        if(!Test.isRunningTest() && response.body.COUNT!=0 && response.body.ROWCNT==response.body.COUNT && pageNum<pageLimit) {
            //endpoint.Last_Successful_Run_Last_Modified_Date__c = LastRop.SystemModStamp;
            //update endpoint;
            insert storeIds;
        //}else if(!Test.isRunningTest() && response.body.COUNT!=0 && ((offSetNo+limitCnt)>=2000 || response.body.ROWCNT!=response.body.COUNT)) {
        }else if(!Test.isRunningTest() && (response.body.ROWCNT!=response.body.COUNT || pageNum==pageLimit)) {
            endpoint.last_successful_run__c = currentDateTime;
            //endpoint.Last_Successful_Run_Last_Modified_Date__c = LastRop.Roll_Out_Plan__r.SystemModStamp;
            update endpoint;
            
            //Delete sent data for final page
            integer count= database.countQuery('select count() from Rollout_Data_to_SECPI__c limit 10002');
            if(count>10000){
                Batch_RolloutDataToSECPI_Delete ba = new Batch_RolloutDataToSECPI_Delete();
                Database.executeBatch(ba);
            }else{
                List<Rollout_Data_to_SECPI__c> deleteData = [select id from Rollout_Data_to_SECPI__c];
                delete deleteData;
            }
        }
        system.debug('Response---->'+response);
        return response;
    }
    
    //Method to convert string to datetime
    public static datetime setDateFromString(string dt){
        string yyyymm = dt.left(6);
        string mm = yyyymm.right(2);
        string yyyy = dt.left(4);
        string yyyymmdd = dt.left(8);
        string dd = yyyymmdd.right(2);
        string finalstr = mm+'/'+dd+'/'+yyyy;
        date d = Date.parse(finalstr);
        system.debug('date----->'+d);
        
        string hhmmss = dt.right(6);
        string hhmm   = hhmmss.left(4);
        integer hh = integer.valueOf(hhmm.left(2));
        integer min = integer.valueOf(hhmm.right(2));
        integer ss = integer.valueOf(hhmmss.right(2));
        
        time t = time.newInstance(hh,min,ss, 0);
        datetime dtime= DateTime.newinstance(d,t);
        
        Timezone tz = Timezone.getTimeZone('America/New_York');
        //datetime now = datetime.now();
        //Integer timeDiff = tz.getOffset(now);
        Integer timeDiff = tz.getOffset(dtime);
        if(timeDiff==-18000000){
            dtime = dtime.addhours(-5);
        }else{
            dtime = dtime.addhours(-4);
        }
        System.debug('dtime----->'+dtime);
        return dtime;
    }
}