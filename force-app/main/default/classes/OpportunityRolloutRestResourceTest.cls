@isTest(seealldata=true)
private class OpportunityRolloutRestResourceTest {
	private static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
	
    static testMethod void getOpportuntityRolloutForError() {
        SFDCStub.OpportunityRolloutRequest reqData = new SFDCStub.OpportunityRolloutRequest();
        
        reqData.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData.inputHeaders.IFID 	 = SFDCStub.LAYOUTID_082;
        reqData.body.CONSUMER = 'GMBT';
        reqData.body.PAGENO = 1;
        
        String jsonMsg = JSON.serialize(reqData);
        
        Test.startTest();
        
        RestRequest req = new RestRequest(); 
	   	RestResponse res = new RestResponse();
	         
	   	req.requestURI = '/services/apexrest/OpportunityRollout/xxxxx';  //Request URL
	   	req.httpMethod = 'GET';//HTTP Request Type
	   	req.requestBody = Blob.valueof(jsonMsg);
	   	RestContext.request = req;
	   	RestContext.response= res;
	   	
	   	SFDCStub.OpportunityRolloutResponse resData = OpportunityRolloutRestResource.getOpportuntityRollout();
	   	System.debug(resData.inputHeaders.MSGSTATUS);
	   	System.debug(resData.inputHeaders.ERRORTEXT);
	   	System.assertEquals('F', resData.inputHeaders.MSGSTATUS);
	   	
	   	//OpportunityRolloutSECPI
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/OpportunityRolloutSECPI/xxxxx';  //Request URL
        req1.httpMethod = 'GET';//HTTP Request Type
        req1.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req1;
        RestContext.response= res1;
        
        SFDCStubSECPI.OpportunityRolloutResponse resData1 = OpportunityRolloutRestResourceSECPI.getOpportuntityRolloutSECPI();
        System.debug(resData1.inputHeaders.MSGSTATUS);
        System.debug(resData1.inputHeaders.ERRORTEXT);
        System.assertEquals('F', resData1.inputHeaders.MSGSTATUS);
	   	
	   	Test.stopTest();
    }
    
    static testMethod void getOpportuntityRolloutNoData() {
        SFDCStub.OpportunityRolloutRequest reqData = new SFDCStub.OpportunityRolloutRequest();
        
        reqData.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData.inputHeaders.IFID 	 = SFDCStub.LAYOUTID_082;
        reqData.body.CONSUMER = 'GMBT';
        reqData.body.PAGENO = 1;
        
        String jsonMsg = JSON.serialize(reqData);
        
        Test.startTest();
        
        RestRequest req = new RestRequest(); 
	   	RestResponse res = new RestResponse();
	         
	   	req.requestURI = '/services/apexrest/OpportunityRollout/11111111111';  //Request URL
	   	req.httpMethod = 'GET';//HTTP Request Type
	   	req.requestBody = Blob.valueof(jsonMsg);
	   	RestContext.request = req;
	   	RestContext.response= res;
	   	
	   	SFDCStub.OpportunityRolloutResponse resData = OpportunityRolloutRestResource.getOpportuntityRollout();
	   	System.debug(resData.inputHeaders.MSGSTATUS);
	   	System.debug(resData.inputHeaders.ERRORTEXT);
	   	System.assertEquals('S', resData.inputHeaders.MSGSTATUS);
	   	
	   	Test.stopTest();
    }
   
    static testMethod void getOpportuntityRollout() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Set<Id> oliSetIds = new Set<Id>();
        Map<Id, Id> ropToOLIMap = new Map<Id,Id>();
        Account account = TestDataUtility.createAccount('Marshal Mathers');
        insert account;
        Account partnerAcc = TestDataUtility.createAccount('Distributor account');
        partnerAcc.RecordTypeId = directRT;
        insert partnerAcc;
        
        //Creating custom Pricebook
        PriceBook2 pb = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(4)); 
        insert pb;
        Opportunity opportunity = TestDataUtility.createOppty('New', account, pb, 'Managed', 'IT', 'product group', 'Identified');
        insert opportunity;
        Partner__c  partner= TestDataUtility.createPartner(opportunity, account, partnerAcc, 'Distributor');
        partner.Is_Primary__c=true;
        insert partner; 
        Product2 product = TestDataUtility.createProduct2('SL-SCF5300/SEG1', 'Standalone Printer', 'Printer[C2]', 'H/W', 'FAX/MFP');
        insert product;
        Product2 product1 = TestDataUtility.createProduct2('SL-SCF5301/SEG1', 'Standalone Printer', 'Printer[C2]', 'H/W', 'FAX/MFP');
        insert product1;
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry pricebookEntry = TestDataUtility.createPriceBookEntry(product.Id, pb.Id);
        pricebookEntry.unitprice=110;
        insert pricebookEntry;
        
        List<OpportunityLineItem> opportunityLineItems = new List<OpportunityLineItem>();
        OpportunityLineItem op1 = TestDataUtility.createOpptyLineItem(product, pricebookEntry, opportunity);
        op1.Quantity = 100;
        op1.TotalPrice = 1000;
        op1.Requested_Price__c = 100;
        op1.Claimed_Quantity__c = 45;
        opportunityLineItems.add(op1);
        insert opportunityLineItems;
        
        opportunity.StageName = 'Qualified';
        opportunity.CloseDate=System.today();
        opportunity.Roll_Out_Start__c=System.today().addDays(61);
        opportunity.Rollout_Duration__c=5;
        
        update opportunity;
        
        Roll_Out__c rollout = TestDataUtility.createRollOuts('RollOut-New',opportunity);
        insert rollout;
        
        SFDCStub.OpportunityRolloutRequest reqData = new SFDCStub.OpportunityRolloutRequest();
        
        reqData.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData.inputHeaders.IFID 	 = SFDCStub.LAYOUTID_082;
        reqData.body.CONSUMER = 'GMBT';
        reqData.body.PAGENO = 1;
        
        String jsonMsg = JSON.serialize(reqData);
        
        Test.startTest();
        
        RestRequest req = new RestRequest(); 
	   	RestResponse res = new RestResponse();
	    
	    Roll_Out__c r = [select id from Roll_Out__c limit 1];
	    Roll_Out_Product__c rop = new Roll_Out_Product__c(Roll_Out_Plan__c=r.id);
        insert rop;
	   	req.requestURI = '/services/apexrest/OpportunityRollout/'+r.id;  //Request URL
	   	req.httpMethod = 'GET';//HTTP Request Type
	   	req.requestBody = Blob.valueof(jsonMsg);
	   	RestContext.request = req;
	   	RestContext.response= res;
	   	
	   	SFDCStub.OpportunityRolloutResponse resData = OpportunityRolloutRestResource.getOpportuntityRollout();
	   	System.debug(resData.inputHeaders.ERRORTEXT);
	   	System.assertEquals('S', resData.inputHeaders.MSGSTATUS);
	   	
	   	//OpportunityRolloutSECPI
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/OpportunityRolloutSECPI/'+r.id;  //Request URL
        req1.httpMethod = 'GET';//HTTP Request Type
        req1.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req1;
        RestContext.response= res1;
        
        SFDCStubSECPI.OpportunityRolloutResponse resData1 = OpportunityRolloutRestResourceSECPI.getOpportuntityRolloutSECPI();
        System.debug(resData1.inputHeaders.MSGSTATUS);
        System.debug(resData1.inputHeaders.ERRORTEXT);
        System.assertEquals('S', resData1.inputHeaders.MSGSTATUS);
	   	
	   	Test.stopTest();
    }
    
    static testMethod void getOpportuntityRolloutListWrongRequest() {
        SFDCStub.OpportunityRolloutRequest reqData = new SFDCStub.OpportunityRolloutRequest();
        
        reqData.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData.inputHeaders.IFID 	 = SFDCStub.LAYOUTID_082;
        reqData.body.CONSUMER = 'GMBT';
        reqData.body.PAGENO = 1;
        
        Test.startTest();
        
        RestRequest req = new RestRequest(); 
	   	RestResponse res = new RestResponse();
	         
	   	req.requestURI = '/services/apexrest/OpportunityRollout';  //Request URL
	   	req.httpMethod = 'POST';//HTTP Request Type
	   	req.requestBody = Blob.valueof('Hello World');
	   	RestContext.request = req;
	   	RestContext.response= res;
	   	
	   	SFDCStub.OpportunityRolloutResponse resData = OpportunityRolloutRestResource.getOpportuntityRolloutList();
	   	System.debug(resData.inputHeaders.MSGSTATUS);
	   	System.debug(resData.inputHeaders.ERRORTEXT);
	   	System.assertEquals('F', resData.inputHeaders.MSGSTATUS);
	   	
	   	//OpportunityRolloutSECPI
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/OpportunityRolloutSECPI';  //Request URL
        req1.httpMethod = 'POST';//HTTP Request Type
        req1.requestBody = Blob.valueof('Hello World');
        RestContext.request = req1;
        RestContext.response= res1;
        
        SFDCStubSECPI.OpportunityRolloutResponse resData1 = OpportunityRolloutRestResourceSECPI.getOpportuntityRolloutListSECPI();
        System.debug(resData1.inputHeaders.MSGSTATUS);
        System.debug(resData1.inputHeaders.ERRORTEXT);
        System.assertEquals('F', resData1.inputHeaders.MSGSTATUS);
	   	
	   	Test.stopTest();
    }
    
    static testMethod void getOpportuntityRolloutList() {
        
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Set<Id> oliSetIds = new Set<Id>();
        Map<Id, Id> ropToOLIMap = new Map<Id,Id>();
        Account account = TestDataUtility.createAccount('Marshal Mathers');
        insert account;
        Account partnerAcc = TestDataUtility.createAccount('Distributor account');
        partnerAcc.RecordTypeId = directRT;
        insert partnerAcc;
        
        //Creating custom Pricebook
        PriceBook2 pb = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(4)); 
        insert pb;
        Opportunity opportunity = TestDataUtility.createOppty('New', account, pb, 'Managed', 'IT', 'product group', 'Identified');
        insert opportunity;
        Partner__c  partner= TestDataUtility.createPartner(opportunity, account, partnerAcc, 'Distributor');
        partner.Is_Primary__c=true;
        insert partner; 
        Product2 product = TestDataUtility.createProduct2('SL-SCF5300/SEG1', 'Standalone Printer', 'Printer[C2]', 'H/W', 'FAX/MFP');
        insert product;
        Product2 product1 = TestDataUtility.createProduct2('SL-SCF5301/SEG1', 'Standalone Printer', 'Printer[C2]', 'H/W', 'FAX/MFP');
        insert product1;
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry pricebookEntry = TestDataUtility.createPriceBookEntry(product.Id, pb.Id);
        pricebookEntry.unitprice=110;
        insert pricebookEntry;
        
        List<OpportunityLineItem> opportunityLineItems = new List<OpportunityLineItem>();
        OpportunityLineItem op1 = TestDataUtility.createOpptyLineItem(product, pricebookEntry, opportunity);
        op1.Quantity = 100;
        op1.TotalPrice = 1000;
        op1.Requested_Price__c = 100;
        op1.Claimed_Quantity__c = 45;
        opportunityLineItems.add(op1);
        insert opportunityLineItems;
        
        opportunity.StageName = 'Qualified';
        opportunity.CloseDate=System.today();
        opportunity.Roll_Out_Start__c=System.today().addDays(61);
        opportunity.Rollout_Duration__c=5;
        
        update opportunity;
        
        Roll_Out__c rollout = TestDataUtility.createRollOuts('RollOut-New',opportunity);
        
        SFDCStub.OpportunityRolloutRequest reqData = new SFDCStub.OpportunityRolloutRequest();
        
        reqData.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData.inputHeaders.IFID 	 = SFDCStub.LAYOUTID_082;
        reqData.body.CONSUMER = 'GMBT';
        reqData.body.PAGENO = 1;
        
        string SearchDtmTo = string.valueOf(system.now().addHours(4));
        SearchDtmTo = SearchDtmTo.replaceAll( '\\s+', '');
        SearchDtmTo = SearchDtmTo.remove(':');
        SearchDtmTo = SearchDtmTo.remove('-');
        
        string SearchDtmFrom = string.valueOf(system.now().addHours(-10));
        SearchDtmFrom = SearchDtmFrom.replaceAll( '\\s+', '');
        SearchDtmFrom = SearchDtmFrom.remove(':');
        SearchDtmFrom = SearchDtmFrom.remove('-');
        
        SFDCStubSECPI.OpportunityRolloutRequest reqData1 = new SFDCStubSECPI.OpportunityRolloutRequest();
        
        reqData1.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData1.inputHeaders.IFID 	 = SFDCStub.LAYOUTID_082;
        reqData1.body.CONSUMER = 'SECPIMSTR';
        reqData1.body.SEARCH_DTTM_FROM = SearchDtmFrom;
        reqData1.body.SEARCH_DTTM_TO = SearchDtmTo;
        reqData1.body.PAGENO = 1;
        
        
        Test.startTest();
        
        RestRequest req = new RestRequest(); 
	   	RestResponse res = new RestResponse();
	         
	   	req.requestURI = '/services/apexrest/OpportunityRollout';  //Request URL
	   	req.httpMethod = 'POST';//HTTP Request Type
	   	
	   	String jsonMsg = JSON.serialize(reqData);
	   	
        req.requestBody = Blob.valueof(jsonMsg);
	   	RestContext.request = req;
	   	RestContext.response= res;
	   	
	   	
	   	SFDCStub.OpportunityRolloutResponse resData = OpportunityRolloutRestResource.getOpportuntityRolloutList();
	   	System.debug(resData.inputHeaders.MSGSTATUS);
	   	System.debug(resData.inputHeaders.ERRORTEXT);
	   	System.assertEquals('S', resData.inputHeaders.MSGSTATUS);
	   	
	   	//OpportunityRolloutSECPI
	   	jsonMsg = JSON.serialize(reqData1);
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/OpportunityRolloutSECPI';  //Request URL
        req1.httpMethod = 'POST';//HTTP Request Type
        req1.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req1;
        RestContext.response= res1;
        
        SFDCStubSECPI.OpportunityRolloutResponse resData1 = OpportunityRolloutRestResourceSECPI.getOpportuntityRolloutListSECPI();
        System.debug(resData1.inputHeaders.MSGSTATUS);
        System.debug(resData1.inputHeaders.ERRORTEXT);
        System.assertEquals('S', resData1.inputHeaders.MSGSTATUS);
	   	
	   	Test.stopTest();
    }
    
    static testMethod void OffsetLimitTest() {
        
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        
        SFDCStub.OpportunityRolloutRequest reqData = new SFDCStub.OpportunityRolloutRequest();
        
        reqData.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData.inputHeaders.IFID 	 = SFDCStub.LAYOUTID_082;
        reqData.body.CONSUMER = 'GMBT';
        reqData.body.PAGENO = 8;
        
        Test.startTest();
        
        RestRequest req = new RestRequest(); 
	   	RestResponse res = new RestResponse();
	         
	   	req.requestURI = '/services/apexrest/OpportunityRollout';  //Request URL
	   	req.httpMethod = 'POST';//HTTP Request Type
	   	
	   	String jsonMsg = JSON.serialize(reqData);
	   	
        req.requestBody = Blob.valueof(jsonMsg);
	   	RestContext.request = req;
	   	RestContext.response= res;
	   	
	   	SFDCStub.OpportunityRolloutResponse resData = OpportunityRolloutRestResource.getOpportuntityRolloutList();
	   	System.debug(resData.inputHeaders.MSGSTATUS);
	   	System.debug(resData.inputHeaders.ERRORTEXT);
	   	System.assertEquals('F', resData.inputHeaders.MSGSTATUS);
	   	
	   	Test.stopTest();
    }
    
    static testMethod void getOpportuntityRolloutListMissingFromDate() {
        
        string SearchDtmTo = string.valueOf(system.now().addHours(4));
        SearchDtmTo = SearchDtmTo.replaceAll( '\\s+', '');
        SearchDtmTo = SearchDtmTo.remove(':');
        SearchDtmTo = SearchDtmTo.remove('-');
        
        SFDCStubSECPI.OpportunityRolloutRequest reqData1 = new SFDCStubSECPI.OpportunityRolloutRequest();
        
        reqData1.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData1.inputHeaders.IFID 	 = SFDCStub.LAYOUTID_082;
        reqData1.body.CONSUMER = 'SECPIMSTR';
        reqData1.body.SEARCH_DTTM_FROM = '';
        reqData1.body.SEARCH_DTTM_TO = SearchDtmTo;
        reqData1.body.PAGENO = 1;
        
        Test.startTest();
	   	
	   	//OpportunityRolloutSECPI
	   	string jsonMsg = JSON.serialize(reqData1);
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/OpportunityRolloutSECPI';  //Request URL
        req1.httpMethod = 'POST';//HTTP Request Type
        req1.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req1;
        RestContext.response= res1;
        
        SFDCStubSECPI.OpportunityRolloutResponse resData1 = OpportunityRolloutRestResourceSECPI.getOpportuntityRolloutListSECPI();
        System.assertEquals('F', resData1.inputHeaders.MSGSTATUS);
	   	
	   	Test.stopTest();
    }
    
    static testMethod void getOpportuntityRolloutListMissingToDate() {
        
        string SearchDtmFrom = string.valueOf(system.now().addHours(-10));
        SearchDtmFrom = SearchDtmFrom.replaceAll( '\\s+', '');
        SearchDtmFrom = SearchDtmFrom.remove(':');
        SearchDtmFrom = SearchDtmFrom.remove('-');
        
        SFDCStubSECPI.OpportunityRolloutRequest reqData1 = new SFDCStubSECPI.OpportunityRolloutRequest();
        
        reqData1.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData1.inputHeaders.IFID 	 = SFDCStub.LAYOUTID_082;
        reqData1.body.CONSUMER = 'SECPIMSTR';
        reqData1.body.SEARCH_DTTM_FROM = SearchDtmFrom;
        reqData1.body.SEARCH_DTTM_TO = '201805220000';
        reqData1.body.PAGENO = 1;
        
        Test.startTest();
	   	
	   	//OpportunityRolloutSECPI
	   	string jsonMsg = JSON.serialize(reqData1);
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/OpportunityRolloutSECPI';  //Request URL
        req1.httpMethod = 'POST';//HTTP Request Type
        req1.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req1;
        RestContext.response= res1;
        
        SFDCStubSECPI.OpportunityRolloutResponse resData1 = OpportunityRolloutRestResourceSECPI.getOpportuntityRolloutListSECPI();
        System.assertEquals('F', resData1.inputHeaders.MSGSTATUS);
	   	
	   	Test.stopTest();
    }
    
    static testMethod void getOpportuntityRolloutListMissingPageNo() {
        
        string SearchDtmTo = string.valueOf(system.now().addHours(4));
        SearchDtmTo = SearchDtmTo.replaceAll( '\\s+', '');
        SearchDtmTo = SearchDtmTo.remove(':');
        SearchDtmTo = SearchDtmTo.remove('-');
        
        string SearchDtmFrom = string.valueOf(system.now().addHours(-26));
        SearchDtmFrom = SearchDtmFrom.replaceAll( '\\s+', '');
        SearchDtmFrom = SearchDtmFrom.remove(':');
        SearchDtmFrom = SearchDtmFrom.remove('-');
        
        SFDCStubSECPI.OpportunityRolloutRequest reqData1 = new SFDCStubSECPI.OpportunityRolloutRequest();
        
        reqData1.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData1.inputHeaders.IFID 	 = SFDCStub.LAYOUTID_082;
        reqData1.body.CONSUMER = 'SECPIMSTR';
        reqData1.body.SEARCH_DTTM_FROM = SearchDtmFrom;
        reqData1.body.SEARCH_DTTM_TO = SearchDtmTo;
        
        Test.startTest();
	   	
	   	//OpportunityRolloutSECPI
	   	string jsonMsg = JSON.serialize(reqData1);
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/OpportunityRolloutSECPI';  //Request URL
        req1.httpMethod = 'POST';//HTTP Request Type
        req1.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req1;
        RestContext.response= res1;
        
        SFDCStubSECPI.OpportunityRolloutResponse resData1 = OpportunityRolloutRestResourceSECPI.getOpportuntityRolloutListSECPI();
        System.assertEquals('F', resData1.inputHeaders.MSGSTATUS);
	   	
	   	Test.stopTest();
    }
    
    static testMethod void OpportuntityRolloutListPageLimitTest() {
        
        string SearchDtmTo = string.valueOf(system.now().addHours(4));
        SearchDtmTo = SearchDtmTo.replaceAll( '\\s+', '');
        SearchDtmTo = SearchDtmTo.remove(':');
        SearchDtmTo = SearchDtmTo.remove('-');
        
        string SearchDtmFrom = string.valueOf(system.now().addHours(-26));
        SearchDtmFrom = SearchDtmFrom.replaceAll( '\\s+', '');
        SearchDtmFrom = SearchDtmFrom.remove(':');
        SearchDtmFrom = SearchDtmFrom.remove('-');
        
        SFDCStubSECPI.OpportunityRolloutRequest reqData1 = new SFDCStubSECPI.OpportunityRolloutRequest();
        
        reqData1.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData1.inputHeaders.IFID 	 = SFDCStub.LAYOUTID_082;
        reqData1.body.CONSUMER = 'SECPIMSTR';
        reqData1.body.SEARCH_DTTM_FROM = SearchDtmFrom;
        reqData1.body.SEARCH_DTTM_TO = SearchDtmTo;
        reqData1.body.PAGENO = 501;
        
        Test.startTest();
	   	
	   	//OpportunityRolloutSECPI
	   	string jsonMsg = JSON.serialize(reqData1);
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/OpportunityRolloutSECPI';  //Request URL
        req1.httpMethod = 'POST';//HTTP Request Type
        req1.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req1;
        RestContext.response= res1;
        
        SFDCStubSECPI.OpportunityRolloutResponse resData1 = OpportunityRolloutRestResourceSECPI.getOpportuntityRolloutListSECPI();
        System.assertEquals('F', resData1.inputHeaders.MSGSTATUS);
	   	
	   	Test.stopTest();
    }
}