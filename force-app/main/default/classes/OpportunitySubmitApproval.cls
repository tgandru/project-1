public class OpportunitySubmitApproval {
	
	public Opportunity opp;
	public OpportunitySubmitApproval(){}
	
	    public void approve() {

        Approval.ProcessSubmitRequest approve1 = new Approval.ProcessSubmitRequest();
        approve1.setComments('Submitting Opportunity for approval.');
        approve1.setObjectId(opp.id);
		approve1.setSubmitterId(UserInfo.getUserId());
        approve1.setProcessDefinitionNameOrId(null);
        approve1.setSkipEntryCriteria(true);

        Approval.ProcessResult result = Approval.process(approve1);
    }

}