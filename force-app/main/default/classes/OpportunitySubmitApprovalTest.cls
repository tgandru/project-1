@isTest
private class OpportunitySubmitApprovalTest {
	static Id endUserRT = TestDataUtility.retrieveRecordTypeId('End_User', 'Account');
	static Id inDirectRT = TestDataUtility.retrieveRecordTypeId('Indirect', 'Account');
	static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
	
	@isTest static void test_method_one() {
		// Implement test code
		Test.loadData(fiscalCalendar__c.sObjectType, 'FiscalCalendarTestData');
        
        //Channelinsight configuration
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();

        Zipcode_Lookup__c zip=TestDataUtility.createZipcode();
        insert zip;
        
	    //Creating test Account
	    Account acct=TestDataUtility.createAccount('Main Account');
	    acct.SAP_Company_Code__c=TestDataUtility.generateRandomString(12);
	    acct.RecordTypeId = endUserRT;
		insert acct;
		
	    
	   //Creating the Parent Account for Distributor as Partners
		Account paDistAcct=TestDataUtility.createAccount('Distributor Account');
		paDistAcct.RecordTypeId = directRT;
		paDistAcct.Type= 'Distributor';
		insert paDistAcct;

		//Creating the Parent Account for Reseller as Partners
		Account paResellAcct=TestDataUtility.createAccount('Reseller Account');
		paResellAcct.RecordTypeId = inDirectRT;
		paResellAcct.Type = 'Corporate Reseller';
		insert paResellAcct;
		
		PriceBook2 priceBook = TestDataUtility.createPriceBook('Shady Records');
        insert pricebook;

        Opportunity opportunity = TestDataUtility.createOppty('New', acct, pricebook, 'Managed', 'IT', 'product group',
                                    'Identified');
        insert opportunity;
        
        Test.startTest();
	        OpportunitySubmitApproval a = new OpportunitySubmitApproval();
	        a.opp = opportunity;
			a.approve();
		Test.stopTest();
        //System.debug([SELECT Id, TargetObjectId, (SELECT Id, StepStatus, Comments FROM Steps)FROM ProcessInstance]);
        Id targetId = [SELECT Id, TargetObjectId, (SELECT Id, StepStatus, Comments FROM Steps)FROM ProcessInstance].TargetObjectId;
        System.assertEquals(opportunity.Id, targetId);
	}
    
}