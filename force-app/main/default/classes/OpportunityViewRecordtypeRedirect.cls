public class OpportunityViewRecordtypeRedirect {
    public Opportunity Opp;
    public string Oppid;

    public OpportunityViewRecordtypeRedirect(ApexPages.StandardController controller) {
    
    }
    Public pagereference redirect()
    { 
        Oppid = apexpages.currentpage().getparameters().get('id');
        if(Oppid != null && Oppid != null){
            Opp = [select id,name,recordtype.name from Opportunity where id =:Oppid];
        }
        if(Opp!=null){
            if(Opp.recordtype.name=='HA Builder'|| Opp.recordtype.name=='Forecast'){
                pagereference pg = new pagereference('/' +Oppid+'?nooverride=1');               
                pg.setredirect(true);
                return pg; 
            }
            Else{
                pagereference pg = new pagereference('/apex/OpportunityCustomButtonsPage?id='+Oppid);           
                pg.setredirect(true);
                return pg;
            }
        }
        
     return null;
    }

}