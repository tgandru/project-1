/**
 *	https://samsungsea--qa.cs18.my.salesforce.com/services/apexrest/OpportunityWithProduct/0063600000DBKekAAH
 *  by hong.jeon
 */
@RestResource(urlMapping='/OpportunityWithProduct/*')
global with sharing class OpportunityWithProductRestResource {
 	public static final String RESOURCE_URI = '/OpportunityWithProduct/';
 	 
 	@HttpGet
 	global static SFDCStub.OpportunityWithProductResponse getOpportuntityWithProduct() {
 		RestRequest req = RestContext.request;
 		RestResponse res = RestContext.response;
 		
 		String oppId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
 		System.debug(req.requestURI+'--> '+oppId);
 		
 		SFDCStub.OpportunityWithProductResponse response = new SFDCStub.OpportunityWithProductResponse();
 		response.inputHeaders.MSGGUID = cihStub.giveGUID();
 		response.inputHeaders.IFID = '';
 		response.inputHeaders.IFDate = datetime.now().format('YYMMddHHmmss', 'America/New_York');
 		response.inputHeaders.MSGSTATUS = 'S';
 		response.inputHeaders.ERRORTEXT = '';
 		response.inputHeaders.ERRORCODE = '';
 		
 		Integration_EndPoints__c endpoint = Integration_EndPoints__c.getInstance(SFDCStub.FLOW_080);
 		DateTime lastSuccessfulRun; 
	 	DateTime currentDateTime = datetime.now();
	 	if(lastSuccessfulRun==null) {
	 		lastSuccessfulRun = currentDateTime - 2;
	 	}
 		string status;
 		Opportunity opp = null;
 		try {
 			opp = [SELECT Id, Amount, CloseDate, CreatedBy.Name, ExpectedRevenue, LeadSource, NextStep, Name, Probability, TotalOpportunityQuantity, 
 					StageName, Type, Channel__c, Drop_Date__c, Drop_Reason__c, ForecastAmount__c, Historical_Comments__c, Lost_Date__c, Lost_Reason__c, 
 					MDM__c, Primary_Distributor__c, Primary_Reseller__c, Roll_Out_End__c, Roll_Out_Start__c, External_Opportunity_ID__c, Won_Date__c, 
 					Owner.Department, Owner.Sales_Team__c, Owner.Name, Account.Name, CreatedDate, LastModifiedDate, isdeleted, IL_CL__c,
 					(Select Id, SAP_Material_ID__c, Product_Family__c, Product_Group__c, Alternative__c, Quantity, Discount, TotalPrice, UnitPrice,
 					 	Product2.Name, Product2.PET_Name__c, Product2.ATTRIB04__c, Product2.ATTRIB06__c FROM OpportunityLineItems)  
 				   FROM Opportunity WHERE Id = :oppId ALL ROWS];
 			if(opp.createddate>=lastSuccessfulRun){
 			    status = 'New';
 			}
 			if(opp.isdeleted==true){
 			    status = 'Deleted';
 			}
 			if(opp.createddate<lastSuccessfulRun && opp.LastModifiedDate>=lastSuccessfulRun){
 			    status = 'Modified';
 			}	   
 		} catch(Exception e) {
 			String errmsg = 'DB Query Error - '+e.getmessage() + ' - '+e.getStackTraceString();
            response.inputHeaders.MSGGUID = cihStub.giveGUID();
	 		response.inputHeaders.IFID 	= '';
	 		response.inputHeaders.IFDate 	= datetime.now().format('YYMMddHHmmss', 'America/New_York');
	 		response.inputHeaders.MSGSTATUS = 'F';
	 		response.inputHeaders.ERRORTEXT = 'Fail to retrieve an Opportunity('+oppId+') - '+req.requestURI + ', errmsg=' + errmsg;
	 		response.inputHeaders.ERRORCODE = '';
 			
 			return response;
 		}
 		
 		response.body.PAGENO = 1;
 		response.body.ROWCNT = 500;
 		response.body.COUNT = 1;
 		
 		SFDCStub.OpportunityWithProduct p = new SFDCStub.OpportunityWithProduct(opp,status);
 		response.body.Opportuntity.add(p);
 		
 		return response;
 	}
 	
 	@HttpPost
 	global static SFDCStub.OpportunityWithProductResponse getOpportuntityWithProductList() {
 		Integration_EndPoints__c endpoint = Integration_EndPoints__c.getInstance(SFDCStub.FLOW_080);
 		String layoutId = !Test.isRunningTest() ? endpoint.layout_id__c : SFDCStub.LAYOUTID_080;
 		
 		RestRequest req = RestContext.request;
 		String postBody = req.requestBody.toString();
 		
 		SFDCStub.OpportunityWithProductResponse response = new SFDCStub.OpportunityWithProductResponse();
 		
 		SFDCStub.OpportunityWithProductRequest request = null;
 		try {
 			request = (SFDCStub.OpportunityWithProductRequest)json.deserialize(postBody, SFDCStub.OpportunityWithProductRequest.class);
 		} catch(Exception e) {
 			response.inputHeaders.MSGGUID = '';
	 		response.inputHeaders.IFID 	= '';
	 		response.inputHeaders.IFDate 	= '';
	 		response.inputHeaders.MSGSTATUS = 'F';
	 		response.inputHeaders.ERRORTEXT = 'Invalid Request Message. - '+postBody;
	 		response.inputHeaders.ERRORCODE = '';
 			
 			return response;
 		}
 		
 		response.inputHeaders.MSGGUID = request.inputHeaders.MSGGUID;
 		response.inputHeaders.IFID 	  = request.inputHeaders.IFID;
 		response.inputHeaders.IFDate  = request.inputHeaders.IFDate;
 		
 		message_queue__c mq = new message_queue__c(MSGUID__c = response.inputHeaders.MSGGUID, 
	 												MSGSTATUS__c = 'S',  
	 												Integration_Flow_Type__c = SFDCStub.FLOW_080,
	 												IFID__c = response.inputHeaders.IFID,
	 												IFDate__c = response.inputHeaders.IFDate,
	 												External_Id__c = request.body.CONSUMER,
	 												Status__c = 'success',
	 												Object_Name__c = 'OpportunityWithProduct', 
	 												retry_counter__c = 0);
	 												
 		DateTime lastSuccessfulRun = endpoint.last_successful_run__c;
	 	DateTime currentDateTime = datetime.now();
	 	if(lastSuccessfulRun==null) {
	 		lastSuccessfulRun = currentDateTime - 2;
	 	}
 		
 		Integer limitCnt = SFDCStub.LIMIT_COUNT;
 		Integer offSetNo = limitCnt*(request.body.PAGENO-1);
 		if(offSetNo>=2000) {
 			String errmsg = 'Invalid Page Number - ('+request.body.PAGENO+'), The offset should be less than 2000.';
            response.inputHeaders.MSGSTATUS = 'F';
	 		response.inputHeaders.ERRORTEXT = errmsg;
	 		response.inputHeaders.ERRORCODE = '';
 			
 			mq.Status__c = 'failed';
 			mq.MSGSTATUS__c = 'F';
 			mq.ERRORTEXT__c = errmsg;
 			insert mq;
 			
 			return response;
 		}
 		
 		List<String> excludedUsers = Label.Users_Excluded_from_Integration.split(',');//Added to exclude data last modified by these users----03/20/2019
 		
 		List<Opportunity> data = null;
 		try {
 			data = [SELECT Id, Amount, CloseDate, CreatedBy.Name, ExpectedRevenue, LeadSource, NextStep, Name, Probability, TotalOpportunityQuantity, 
 						StageName, Type, Channel__c, Drop_Date__c, Drop_Reason__c, ForecastAmount__c, Historical_Comments__c, Lost_Date__c, Lost_Reason__c, 
 						MDM__c, Primary_Distributor__c, Primary_Reseller__c, Roll_Out_End__c, Roll_Out_Start__c, External_Opportunity_ID__c, Won_Date__c, 
 						Owner.Department, Owner.Sales_Team__c, Owner.Name, Account.Name, CreatedDate, LastModifiedDate,isdeleted, Roll_Out_End_Formula__c,IL_CL__c,
 						(SELECT Id, SAP_Material_ID__c, Product_Family__c, Product_Group__c, Alternative__c, Quantity, Discount, TotalPrice, UnitPrice,
 							Product2.Name, Product2.PET_Name__c, Product2.ATTRIB04__c, Product2.ATTRIB06__c FROM OpportunityLineItems where Product2.Family='MOBILE PHONE [G1]')  
 					FROM Opportunity WHERE LastModifiedById!=:excludedUsers 
 					                       and LastModifiedDate >= :lastSuccessfulRun 
 					                       and LastModifiedDate <= :currentDateTime 
 					                       and recordtype.name!='HA Builder' 
 					                       and recordtype.name!='Forecast'
 					                       and ProductGroupTest__c includes ('SMART_PHONE','KNOX','ACCESSORY','TABLET','FEATURE_PHONE','SOLUTION [MOBILE]') 
 					                       ORDER BY LastModifiedDate ASC LIMIT :limitCnt OFFSET :offsetNo ALL ROWS];
 		} catch(Exception e) {
 			String errmsg = 'DB Query Error - '+e.getmessage() + ' - '+e.getStackTraceString();
            response.inputHeaders.MSGSTATUS = 'F';
	 		response.inputHeaders.ERRORTEXT = errmsg;
	 		response.inputHeaders.ERRORCODE = '';
 			
 			mq.Status__c = 'failed';
 			mq.MSGSTATUS__c = 'F';
 			mq.ERRORTEXT__c = errmsg;
 			insert mq;
 			
 			return response;
 		}	
 					   
 		response.inputHeaders.MSGSTATUS = 'S';
 		response.inputHeaders.ERRORTEXT = '';
 		response.inputHeaders.ERRORCODE = '';
 		
 		response.body.PAGENO = request.body.PAGENO;
 		response.body.ROWCNT = limitCnt;
 		response.body.COUNT  = data.size();
 		DateTime LastModifiedDate = null;
 		string status;
 		for(Opportunity opp : data) {
 			try {
 			    if(opp.createddate>=lastSuccessfulRun){
 				    status = 'New';
 				}
 				if(opp.createddate<lastSuccessfulRun && opp.LastModifiedDate>=lastSuccessfulRun){
 				    status = 'Modified';
 				}
 				if(opp.isdeleted==true){
 				    status = 'Deleted';
 				}
 				//Added logic to send Old StageName to IDAP--10/08/2018--Starts Here
 				if(opp.StageName=='Identified'){
 				    opp.StageName='Identification';
 				}
 				if(opp.StageName=='Qualified'){
 				    opp.StageName='Qualification';
 				}
 				if(opp.StageName=='Win'){
 				    opp.StageName='Won';
 				}
 				//Ends Here
 				SFDCStub.OpportunityWithProduct p = new SFDCStub.OpportunityWithProduct(opp,status);
 				response.body.Opportuntity.add(p);
 				LastModifiedDate = p.instance.LastModifiedDate;
 				system.debug('Record--------->'+response.body.Opportuntity);
 			} catch(Exception e) {
 				String errmsg = 'Fail to create an OpportunityWithProduct - '+e.getmessage() + ' - '+e.getStackTraceString();
            	message_queue__c pemsg = new message_queue__c(MSGUID__c = response.inputHeaders.MSGGUID, 
	 												MSGSTATUS__c = 'F',  
	 												Integration_Flow_Type__c = SFDCStub.FLOW_080,
	 												IFID__c = response.inputHeaders.IFID,
	 												IFDate__c = response.inputHeaders.IFDate,
	 												Status__c = 'failed',
	 												Object_Name__c = 'OpportunityWithProduct', 
	 												retry_counter__c = 0);
            	pemsg.ERRORTEXT__c = errmsg;
	 			pemsg.Identification_Text__c = opp.Id;
	 			insert pemsg;
	 			System.debug(errmsg);
 			}
 		}
 		
 		mq.last_successful_run__c = currentDateTime;
 		mq.Object_Name__c = mq.Object_Name__c + ', ' + request.body.PAGENO + ', ' + response.body.COUNT;
 		insert mq;
 		system.debug('Test.isRunningTest()-------->'+Test.isRunningTest());
 		if(!Test.isRunningTest() && response.body.COUNT!=0 &&((offSetNo+limitCnt)>=2000 || response.body.ROWCNT!=response.body.COUNT)) {
 		    system.debug('LastModifiedDate-------->'+LastModifiedDate);
 			endpoint.last_successful_run__c = LastModifiedDate;
 			update endpoint;
 		}
 		return response;
 	}
 	
 	
}