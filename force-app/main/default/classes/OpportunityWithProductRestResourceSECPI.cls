/**
 *  https://samsungsea--qa.cs65.my.salesforce.com/services/apexrest/OpportunityWithProductSECPI/0063600000DBKekAAH
 */
@RestResource(urlMapping='/OpportunityWithProductSECPI/*')
global with sharing class OpportunityWithProductRestResourceSECPI {
    public static final String RESOURCE_URI = '/OpportunityWithProductSECPI/';
     
    @HttpGet
    global static SFDCStubSECPI.OpportunityWithProductResponseSECPI getOpportuntityWithProductSECPI() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        
        String oppId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        System.debug(req.requestURI+'--> '+oppId);
        
        SFDCStubSECPI.OpportunityWithProductResponseSECPI response = new SFDCStubSECPI.OpportunityWithProductResponseSECPI();
        response.inputHeaders.MSGGUID = cihStub.giveGUID();
        response.inputHeaders.IFID = '';
        response.inputHeaders.IFDate = datetime.now().format('YYMMddHHmmss', 'America/New_York');
        response.inputHeaders.MSGSTATUS = 'S';
        response.inputHeaders.ERRORTEXT = '';
        response.inputHeaders.ERRORCODE = '';
        
        Integration_EndPoints__c endpoint = Integration_EndPoints__c.getInstance(SFDCStubSECPI.FLOW_080);
        DateTime lastSuccessfulRun; 
        DateTime currentDateTime = datetime.now();
        if(lastSuccessfulRun==null) {
            lastSuccessfulRun = currentDateTime - 1;
        }
        string status;
        Opportunity opp = null;
        try {
            opp = [SELECT Id, Amount, CloseDate, CreatedBy.Name, ExpectedRevenue, LeadSource, NextStep, Name, Probability, TotalOpportunityQuantity, 
                    StageName, Type, Channel__c, Drop_Date__c, Drop_Reason__c, ForecastAmount__c, Lost_Date__c, Lost_Reason__c,systemModStamp, 
                    MDM__c, Primary_Distributor__c, Primary_Reseller__c, Roll_Out_End__c, Roll_Out_Start__c, External_Opportunity_ID__c, Won_Date__c,Roll_Out_End_Formula__c, 
                    Owner.Department, Owner.Sales_Team__c, Owner.Name, Account.Name, CreatedDate, LastModifiedDate, isdeleted, Campaign.name,PRM_Lead_ID__c, Roll_Out_Plan__r.Roll_Out_End__c,
                    (Select Id, SAP_Material_ID__c, Product_Family__c, Product_Group__c, Alternative__c, Quantity, Discount, TotalPrice, UnitPrice, ListPrice,
                        Product2.Name, Product2.PET_Name__c, Product2.ATTRIB04__c, Product2.ATTRIB06__c, isdeleted, Requested_Price__c FROM OpportunityLineItems)  
                   FROM Opportunity WHERE Id = :oppId ALL ROWS];
            if(opp.createddate>=lastSuccessfulRun){
                status = 'New';
            }
            if(opp.isdeleted==true){
                status = 'Deleted';
            }
            if(opp.createddate<lastSuccessfulRun && opp.systemModStamp>=lastSuccessfulRun){
                status = 'Modified';
            }      
        } catch(Exception e) {
            String errmsg = 'DB Query Error - '+e.getmessage() + ' - '+e.getStackTraceString();
            response.inputHeaders.MSGGUID = cihStub.giveGUID();
            response.inputHeaders.IFID  = '';
            response.inputHeaders.IFDate    = datetime.now().format('YYMMddHHmmss', 'America/New_York');
            response.inputHeaders.MSGSTATUS = 'F';
            response.inputHeaders.ERRORTEXT = 'Fail to retrieve an Opportunity('+oppId+') - '+req.requestURI + ', errmsg=' + errmsg;
            response.inputHeaders.ERRORCODE = '';
            
            return response;
        }
        
        response.body.PAGENO = 1;
        response.body.ROWCNT = 500;
        response.body.COUNT = 1;
        
        SFDCStubSECPI.OpportunityWithProductSECPI p = new SFDCStubSECPI.OpportunityWithProductSECPI(opp,status);
        response.body.Opportuntity.add(p);
        
        return response;
    }
    
    @HttpPost
    global static SFDCStubSECPI.OpportunityWithProductResponseSECPI getOpportuntityWithProductListSECPI() {
        integer pageNum;
        //integer pageLimit = 10;
        integer pageLimit = integer.valueOf(label.SECPI_Oppty_I_F_Page_Limit);
        Integration_EndPoints__c endpoint = Integration_EndPoints__c.getInstance(SFDCStubSECPI.FLOW_080);
        String layoutId = !Test.isRunningTest() ? endpoint.layout_id__c : SFDCStubSECPI.LAYOUTID_080;
        
        RestRequest req = RestContext.request;
        String postBody = req.requestBody.toString();
        
        SFDCStubSECPI.OpportunityWithProductResponseSECPI response = new SFDCStubSECPI.OpportunityWithProductResponseSECPI();
        
        SFDCStubSECPI.OpportunityWithProductRequest request = null;
        try {
            request = (SFDCStubSECPI.OpportunityWithProductRequest)json.deserialize(postBody, SFDCStubSECPI.OpportunityWithProductRequest.class);
            system.debug('Request======>'+request);
        } catch(Exception e) {
            response.inputHeaders.MSGGUID = '';
            response.inputHeaders.IFID  = '';
            response.inputHeaders.IFDate    = '';
            response.inputHeaders.MSGSTATUS = 'F';
            response.inputHeaders.ERRORTEXT = 'Invalid Request Message. - '+postBody;
            response.inputHeaders.ERRORCODE = '';
            
            return response;
        }
        response.inputHeaders.MSGGUID = request.inputHeaders.MSGGUID;
        response.inputHeaders.IFID    = request.inputHeaders.IFID;
        response.inputHeaders.IFDate  = request.inputHeaders.IFDate;
        
        string dates = 'From:'+request.body.SEARCH_DTTM_FROM+', To:'+request.body.SEARCH_DTTM_TO;
        
        List<message_queue__c> mqList = new List<message_queue__c>();
        message_queue__c mq = new message_queue__c(MSGUID__c = response.inputHeaders.MSGGUID, 
                                                    MSGSTATUS__c = 'S',  
                                                    Integration_Flow_Type__c = SFDCStubSECPI.FLOW_080,
                                                    IFID__c = response.inputHeaders.IFID,
                                                    IFDate__c = response.inputHeaders.IFDate,
                                                    External_Id__c = request.body.CONSUMER,
                                                    Status__c = 'success',
                                                    Object_Name__c = 'OpportunityWithProduct', 
                                                    retry_counter__c = 0,
                                                    Identification_Text__c = dates);
                                                    
        //Added logic for PAGENO Limit --- starts here
        if(request.body.PAGENO!=null){
            pageNum = request.body.PAGENO;
            System.debug('PageNum----->'+pageNum);
        }else{
            String errmsg = 'Page Number is Missing in the Request';
            response.inputHeaders.MSGSTATUS = 'F';
            response.inputHeaders.ERRORTEXT = errmsg;
            response.inputHeaders.ERRORCODE = '';
            
            mq.Status__c = 'failed';
            mq.MSGSTATUS__c = 'F';
            mq.ERRORTEXT__c = errmsg;
            insert mq;
            
            return response;
        }
        
        if(pageNum>pageLimit){
            Datetime new_SEARCH_DTTM = endpoint.Last_Successful_Run_Last_Modified_Date__c;
            System.debug('new SERACH_DTTM----->'+new_SEARCH_DTTM);
            String errmsg = 'SFDC PAGE LIMIT EXCEEDED. Please use new SEARCH_DTTM_FROM:'+new_SEARCH_DTTM+' and start from PAGENO:1 ' ;
            
            response.inputHeaders.MSGSTATUS = 'F';
            response.inputHeaders.ERRORTEXT = errmsg;
            response.inputHeaders.ERRORCODE = 'PAGE_LIMIT_EXCEEDED';
            response.inputHeaders.SEARCH_DTTM_FROM = SFDCStubSECPI.DtTimetoString(new_SEARCH_DTTM);
            
            mq.Status__c = 'failed';
            mq.MSGSTATUS__c = 'F';
            mq.ERRORTEXT__c = errmsg;
            insert mq;
            
            return response;
        }
        //---Ends Here
        
        //Block the callout if the Sent data delete batch is in process.
        List<AsyncApexJob> DeleteBatchProcessing = [select id,TotalJobItems,status,createddate from AsyncApexJob where apexclass.name='Batch_OpptyDataToSECPI_Delete' and (status='Holding' or status='Queued' or status='Preparing' or status='Processing') order by createddate desc limit 1];
        if(DeleteBatchProcessing.size()>0){    
            response.inputHeaders.MSGSTATUS = 'F'; response.inputHeaders.ERRORTEXT = 'The Sent Data Delete Batch is in Process. Try callout after 10 mins'; response.inputHeaders.ERRORCODE = '';
            mq.Status__c = 'failed'; mq.MSGSTATUS__c = 'F'; mq.ERRORTEXT__c = 'The Sent Data Delete Batch is in Process. Try callout after 10 mins';
            insert mq;
            
            return response;
        }
        
        string DateFormat = 'yyyymmddhhmmss';
        string DateFormat2 = 'yyyymmdd';
        
        //Added the logic for New request format. Request includes FROM and TO SEARCH_DTTM--05/16/2018
        //Checking the date format.
        if((request.body.SEARCH_DTTM_FROM!=null && request.body.SEARCH_DTTM_FROM!='' && request.body.SEARCH_DTTM_FROM.length()!=DateFormat.length() && request.body.SEARCH_DTTM_FROM.length()!=DateFormat2.length())
            || (request.body.SEARCH_DTTM_TO!=null && request.body.SEARCH_DTTM_TO!='' && request.body.SEARCH_DTTM_TO.length()!=DateFormat.length() && request.body.SEARCH_DTTM_TO.length()!=DateFormat2.length())){
            String errmsg = 'Incorrect SEARCH_DTTM format. The Date Format must be yyyymmddhhmmss or yyyymmdd';
            response.inputHeaders.MSGSTATUS = 'F';
            response.inputHeaders.ERRORTEXT = errmsg;
            response.inputHeaders.ERRORCODE = 'Incorrect SEARCH_DTTM format';
            
            mq.Status__c = 'failed';
            mq.MSGSTATUS__c = 'F';
            mq.ERRORTEXT__c = errmsg;
            insert mq;
            
            return response;
        }
        
        //Check the Missng SEARCH_DTTM_TO and SEARCH_DTTM_FROM
        if((((request.body.SEARCH_DTTM_TO==null || request.body.SEARCH_DTTM_TO=='') && request.body.SEARCH_DTTM_FROM!=null && request.body.SEARCH_DTTM_FROM!=''))
            || (((request.body.SEARCH_DTTM_FROM==null || request.body.SEARCH_DTTM_FROM=='') && request.body.SEARCH_DTTM_TO!=null && request.body.SEARCH_DTTM_TO!=''))){
            String errmsg = 'SEARCH_DTTM_FROM or SEARCH_DTTM_TO is missing. The request should include both FROM and TO dates while sending TimeStamps';
            response.inputHeaders.MSGSTATUS = 'F';
            response.inputHeaders.ERRORTEXT = errmsg;
            response.inputHeaders.ERRORCODE = '';
            
            mq.Status__c = 'failed';
            mq.MSGSTATUS__c = 'F';
            mq.ERRORTEXT__c = errmsg;
            insert mq;
            
            return response;
        }
        
        if(request.body.SEARCH_DTTM_FROM!=null && request.body.SEARCH_DTTM_FROM!='' && request.body.SEARCH_DTTM_FROM.length()==DateFormat2.length()){
            request.body.SEARCH_DTTM_FROM=request.body.SEARCH_DTTM + '000000';
        }
        if(request.body.SEARCH_DTTM_TO!=null && request.body.SEARCH_DTTM_TO!='' && request.body.SEARCH_DTTM_TO.length()==DateFormat2.length()){
            request.body.SEARCH_DTTM_TO=request.body.SEARCH_DTTM + '000000';
        }
        
        DateTime lastSuccessfulRun = endpoint.last_successful_run__c;
        DateTime currentDateTime = datetime.now();
        DateTime SearchDateTo;
        DateTime SearchDateFrom;
        
        //Set From and To timestamps for Query
        if((request.body.SEARCH_DTTM_FROM!=null && request.body.SEARCH_DTTM_FROM!='')
            &&(request.body.SEARCH_DTTM_TO!=null && request.body.SEARCH_DTTM_TO!='')){
            SearchDateTo = setDateFromString(request.body.SEARCH_DTTM_TO);
            system.debug('SearchDateTo----->'+SearchDateTo);
            SearchDateFrom = setDateFromString(request.body.SEARCH_DTTM_FROM);
            system.debug('SearchDateFrom----->'+SearchDateFrom);
        }else{
            SearchDateTo = currentDateTime;
            system.debug('SearchDateTo----->'+SearchDateTo);
            SearchDateFrom = lastSuccessfulRun;
        }
        
        //Checing the SEARCH_DTTM_FROM and SEARCH_DTTM_TO time difference
        integer SecsDiff = Integer.valueOf((SearchDateTo.getTime() - SearchDateFrom.getTime())/(1000));
        if(SecsDiff>86400){
            String errmsg = 'Difference between SEARCH_DTTM_FROM and SEARCH_DTTM_TO should be less or Equal to 1 day';
            response.inputHeaders.MSGSTATUS = 'F';
            response.inputHeaders.ERRORTEXT = errmsg;
            response.inputHeaders.ERRORCODE = '';
            
            mq.Status__c = 'failed';
            mq.MSGSTATUS__c = 'F';
            mq.ERRORTEXT__c = errmsg;
            insert mq;
            
            return response;
        }
        
        Integer limitCnt = SFDCStubSECPI.Oppty_LIMIT_COUNT;//Set no. of records to send per page
        
        //Get all sent data
        Set<string> sentOppids = new Set<string>();
        List<Oppty_Data_to_SECPI__c> AllData = Oppty_Data_to_SECPI__c.getall().values();
        system.debug('size------->'+AllData.size());
        for(Oppty_Data_to_SECPI__c dataSend : AllData){
            sentOppids.add(dataSend.name);
        }
        
        List<Oppty_Data_to_SECPI__c> storeIds = new List<Oppty_Data_to_SECPI__c>();
        
        List<Opportunity> data = null;
                       
        response.inputHeaders.MSGSTATUS = 'S';
        response.inputHeaders.ERRORTEXT = '';
        response.inputHeaders.ERRORCODE = '';
        
        response.body.PAGENO = request.body.PAGENO;
        response.body.ROWCNT = limitCnt;
        //response.body.COUNT  = data.size();
        DateTime LastModifiedDate = null;
        string status;
        integer DataSize=0;
        List<Opportunity> OppList = new List<Opportunity>();
        
        List<String> excludedUsers = Label.Users_Excluded_from_Integration.split(',');//Added to exclude data last modified by these users----03/20/2019

        for(Opportunity opp : [SELECT Id, Amount, CloseDate, CreatedBy.Name, ExpectedRevenue, LeadSource, NextStep, Name, Probability, TotalOpportunityQuantity, 
                                StageName, Type, Channel__c, Drop_Date__c, Drop_Reason__c, ForecastAmount__c, Lost_Date__c, Lost_Reason__c, 
                                MDM__c, Primary_Distributor__c, Primary_Reseller__c, Roll_Out_Start__c, External_Opportunity_ID__c, Won_Date__c,Roll_Out_End_Formula__c, 
                                Owner.Department, Owner.Sales_Team__c, Owner.Name, Account.Name, CreatedDate, LastModifiedDate,isdeleted, Campaign.name, systemModStamp,
                                (SELECT Id, SAP_Material_ID__c, Product_Family__c, Product_Group__c, Alternative__c, Quantity, Discount, TotalPrice, UnitPrice, ListPrice,
                                 product2.SAP_Material_Id__c, Product2.Name, Product2.PET_Name__c, Product2.ATTRIB04__c, Product2.ATTRIB06__c, isdeleted, Requested_Price__c FROM OpportunityLineItems)  
                            FROM Opportunity 
                            WHERE LastModifiedById!=:excludedUsers
                                  and systemModStamp >= :SearchDateFrom 
                                  and systemModStamp <= :SearchDateTo 
                                  and id!=:sentOppids 
                                  and recordtype.name!='HA Builder' 
                                  and recordtype.name!='Forecast' 
                                  ORDER BY systemModStamp ASC LIMIT :limitCnt ALL ROWS])
        {
            DataSize++;
            OppList.add(opp);
            Oppty_Data_to_SECPI__c storeId = new Oppty_Data_to_SECPI__c(name=opp.id);
            if(!sentOppids.contains(opp.id)){
                storeIds.add(storeId);   
            }
            try {
                system.debug('Opp Deleted---->'+Opp.isdeleted);
                if(opp.createddate>=SearchDateFrom){
                    status = 'New';
                    system.debug('New');
                }
                if(opp.createddate<SearchDateFrom && opp.systemModStamp>=SearchDateFrom){
                    status = 'Modified';
                }
                if(opp.isdeleted==true){
                    status = 'Deleted';
                }
                //Added logic to send Old StageName to IDAP--10/08/2018--Starts Here
         if(opp.StageName=='Identified'){
             opp.StageName='Identification';
         }
         if(opp.StageName=='Qualified'){
             opp.StageName='Qualification';
         }
         if(opp.StageName=='Win'){
             opp.StageName='Won';
         }
         //Ends Here
                Transient SFDCStubSECPI.OpportunityWithProductSECPI p = new SFDCStubSECPI.OpportunityWithProductSECPI(opp,status);
                response.body.Opportuntity.add(p);
                LastModifiedDate = p.instance.systemModStamp;
                //system.debug('Record--------->'+response.body.Opportuntity);
            } catch(Exception e) {
                String errmsg = 'Fail to create an OpportunityWithProduct - '+e.getmessage() + ' - '+e.getStackTraceString();
                message_queue__c pemsg = new message_queue__c(MSGUID__c = response.inputHeaders.MSGGUID, 
                                                    MSGSTATUS__c = 'F',  
                                                    Integration_Flow_Type__c = SFDCStubSECPI.FLOW_080,
                                                    IFID__c = response.inputHeaders.IFID,
                                                    IFDate__c = response.inputHeaders.IFDate,
                                                    Status__c = 'failed',
                                                    Object_Name__c = 'OpportunityWithProduct', 
                                                    retry_counter__c = 0);
                pemsg.ERRORTEXT__c = errmsg;
                pemsg.Identification_Text__c = opp.Id;
                mqList.add(pemsg);
                //insert pemsg;
                System.debug(errmsg);
            }
        }
        system.debug('Query Record Size----->'+DataSize);
        response.body.COUNT  = DataSize;
        
        mq.last_successful_run__c = currentDateTime;
        mq.Object_Name__c = mq.Object_Name__c + ', ' + request.body.PAGENO + ', ' + response.body.COUNT;
        //insert mq;
        mqList.add(mq);
        if(mqList.size()>0){
            insert mqList;
        }
        
        Opportunity LastOpp;
        if(DataSize>0){
            LastOpp  = OppList[DataSize-1];
            endpoint.Last_Successful_Run_Last_Modified_Date__c = LastOpp.SystemModStamp;
        }
        
        if(!Test.isRunningTest() && response.body.COUNT!=0 && response.body.ROWCNT==response.body.COUNT && pageNum<pageLimit) {
            insert storeIds;
            system.debug('<-------Insert sent data in Page------->Page::'+pageNum);
        //}else if(!Test.isRunningTest() && response.body.COUNT!=0 && ((offSetNo+limitCnt)>=2000 || response.body.ROWCNT!=response.body.COUNT)) {
        }else if(response.body.COUNT!=0 && (response.body.ROWCNT!=response.body.COUNT || pageNum==pageLimit)) {
            endpoint.last_successful_run__c = currentDateTime;
            //endpoint.Last_Successful_Run_Last_Modified_Date__c = LastOpp.SystemModStamp;
            update endpoint;
            
            //Delete sent data for final page
            integer count= database.countQuery('select count() from Oppty_Data_to_SECPI__c');
            if(count>10000){
                Batch_OpptyDataToSECPI_Delete ba = new Batch_OpptyDataToSECPI_Delete();
                Database.executeBatch(ba);
            }else{
                List<Oppty_Data_to_SECPI__c> deleteData = [select id,name from Oppty_Data_to_SECPI__c];
                delete deleteData;
            }
            system.debug('<-------Final Page------->Page::'+pageNum);
        }
        system.debug('Response==============>'+response);
        return response;
    }
    
    //Method to convert string to datetime
    public static datetime setDateFromString(string dt){
        string yyyymm = dt.left(6);
        string mm = yyyymm.right(2);
        string yyyy = dt.left(4);
        string yyyymmdd = dt.left(8);
        string dd = yyyymmdd.right(2);
        string finalstr = mm+'/'+dd+'/'+yyyy;
        date d = Date.parse(finalstr);
        system.debug('date----->'+d);
        
        string hhmmss = dt.right(6);
        string hhmm   = hhmmss.left(4);
        integer hh = integer.valueOf(hhmm.left(2));
        integer min = integer.valueOf(hhmm.right(2));
        integer ss = integer.valueOf(hhmmss.right(2));
        
        time t = time.newInstance(hh,min,ss, 0);
        datetime dtime= DateTime.newinstance(d,t);
        
        Timezone tz = Timezone.getTimeZone('America/New_York');
        //datetime now = datetime.now();
        //Integer timeDiff = tz.getOffset(now);
        Integer timeDiff = tz.getOffset(dtime);
        if(timeDiff==-18000000){
            dtime = dtime.addhours(-5);
        }else{
            dtime = dtime.addhours(-4);
        }
        System.debug('dtime----->'+dtime);
        return dtime;
    }
    
}