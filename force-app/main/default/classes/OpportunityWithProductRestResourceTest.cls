/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class OpportunityWithProductRestResourceTest {

    static testMethod void getOpportuntityWithProductNoData() {

        SFDCStub.OpportunityWithProductRequest reqData = new SFDCStub.OpportunityWithProductRequest();
        
        reqData.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData.inputHeaders.IFID    = SFDCStub.LAYOUTID_080;
        reqData.body.CONSUMER = 'GMBT';
        reqData.body.PAGENO = 1;
        
        String jsonMsg = JSON.serialize(reqData);
        
        Test.startTest();
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/services/apexrest/OpportunityWithProduct/xxxxxx';  //Request URL
        req.httpMethod = 'GET';//HTTP Request Type
        req.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req;
        RestContext.response= res;
        
        SFDCStub.OpportunityWithProductResponse resData = OpportunityWithProductRestResource.getOpportuntityWithProduct();
        System.debug(resData.inputHeaders.ERRORTEXT);
        System.assertEquals('F', resData.inputHeaders.MSGSTATUS);
        
        //OpportunityWithProductSECPI
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/OpportunityWithProductSECPI/xxxxxx';  //Request URL
        req1.httpMethod = 'POST';//HTTP Request Type
        req1.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req1;
        RestContext.response= res1;
        
        SFDCStubSECPI.OpportunityWithProductResponseSECPI resData1 = OpportunityWithProductRestResourceSECPI.getOpportuntityWithProductSECPI();
        System.debug(resData1.inputHeaders.MSGSTATUS);
        System.debug(resData1.inputHeaders.ERRORTEXT);
        System.assertEquals('F', resData1.inputHeaders.MSGSTATUS);
        
        Test.stopTest();
    }
    
    static testMethod void getOpportuntityWithProduct() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Account acc1 = new account(Name ='Test', Type='Customer',SAP_Company_Code__c = 'AAA');
        insert acc1;
        Id rtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('IT').getRecordTypeId();
        
        Opportunity opp = new Opportunity(accountId=acc1.id,recordtypeId=rtId,
        StageName='Identified',Name='Test Opp',Type='Tender',Division__c='IT',ProductGroupTest__c='LCD_MONITOR',LeadSource='BTA Dealer',Closedate=system.today());
        insert opp;
        
        SFDCStub.OpportunityWithProductRequest reqData = new SFDCStub.OpportunityWithProductRequest();
        
        reqData.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData.inputHeaders.IFID    = SFDCStub.LAYOUTID_080;
        reqData.body.CONSUMER = 'GMBT';
        reqData.body.PAGENO = 1;
        
        String jsonMsg = JSON.serialize(reqData);
        
        Test.startTest();
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/services/apexrest/OpportunityWithProduct/'+opp.Id;  //Request URL
        req.httpMethod = 'GET';//HTTP Request Type
        req.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req;
        RestContext.response= res;
        
        SFDCStub.OpportunityWithProductResponse resData = OpportunityWithProductRestResource.getOpportuntityWithProduct();
        System.debug(resData.inputHeaders.ERRORTEXT);
        System.assertEquals('S', resData.inputHeaders.MSGSTATUS);
        
        //OpportunityWithProductSECPI
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/OpportunityWithProductSECPI/'+opp.Id;  //Request URL
        req1.httpMethod = 'POST';//HTTP Request Type
        req1.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req1;
        RestContext.response= res1;
        
        SFDCStubSECPI.OpportunityWithProductResponseSECPI resData1 = OpportunityWithProductRestResourceSECPI.getOpportuntityWithProductSECPI();
        System.debug(resData1.inputHeaders.MSGSTATUS);
        System.debug(resData1.inputHeaders.ERRORTEXT);
        System.assertEquals('S', resData1.inputHeaders.MSGSTATUS);
        
        Test.stopTest();
    }
    
    static testMethod void getOpportuntityWithProductList() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        List<Integration_EndPoints__c> ieps = new List<Integration_EndPoints__c>();
        Integration_EndPoints__c iep1 = new Integration_EndPoints__c();
        iep1.name ='Flow-080';
        iep1.layout_id__c = 'LAY024657';
        iep1.last_successful_run__c = System.now().addHours(-4);
        ieps.add(iep1);
        
        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='SECPI Flow 080';
        iep2.layout_id__c = 'LAY025592';
        iep2.last_successful_run__c = System.now().addHours(-4);
        ieps.add(iep2);
        
        insert ieps;
        
        Account acc1 = new account(Name ='Test', Type='Customer',SAP_Company_Code__c = 'AAA');
        insert acc1;
        Id rtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('IT').getRecordTypeId();
        
        Opportunity opp = new Opportunity(accountId=acc1.id,recordtypeId=rtId,
        StageName='Identified',Name='TEst Opp',Type='Tender',Division__c='IT',ProductGroupTest__c='LCD_MONITOR;KNOX',LeadSource='BTA Dealer',Closedate=system.today());
        insert opp;
        
        SFDCStub.OpportunityWithProductRequest reqData = new SFDCStub.OpportunityWithProductRequest();
        
        reqData.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData.inputHeaders.IFID    = SFDCStub.LAYOUTID_080;
        reqData.body.CONSUMER = 'GMBT';
        reqData.body.PAGENO = 1;
        
        string SearchDtm = string.valueOf(system.now().addHours(5));
        SearchDtm = SearchDtm.replaceAll( '\\s+', '');
        SearchDtm = SearchDtm.remove(':');
        SearchDtm = SearchDtm.remove('-');
        
        string SearchDtmFrom = string.valueOf((system.now()-1).addHours(5));
        SearchDtmFrom = SearchDtmFrom.replaceAll( '\\s+', '');
        SearchDtmFrom = SearchDtmFrom.remove(':');
        SearchDtmFrom = SearchDtmFrom.remove('-');
        
        SFDCStubSECPI.OpportunityWithProductRequest reqData2 = new SFDCStubSECPI.OpportunityWithProductRequest();
        reqData2.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData2.inputHeaders.IFID    = SFDCStubSECPI.LAYOUTID_080;
        reqData2.body.CONSUMER = 'SECPIMSTR';
        reqData2.body.SEARCH_DTTM_TO = SearchDtm;
        reqData2.body.SEARCH_DTTM_FROM = SearchDtmFrom;
        reqData2.body.PAGENO = 1;
        
        String jsonMsg  = JSON.serialize(reqData);
        String jsonMsg2 = JSON.serialize(reqData2);
        
        Test.startTest();
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/services/apexrest/OpportunityWithProduct';  //Request URL
        req.httpMethod = 'POST';//HTTP Request Type
        req.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req;
        RestContext.response= res;
        
        SFDCStub.OpportunityWithProductResponse resData = OpportunityWithProductRestResource.getOpportuntityWithProductList();
        System.debug(resData.inputHeaders.ERRORTEXT);
        System.assertEquals('S', resData.inputHeaders.MSGSTATUS);
        
        //OpportunityWithProductSECPI
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/OpportunityWithProductSECPI';  //Request URL
        req1.httpMethod = 'POST';//HTTP Request Type
        req1.requestBody = Blob.valueof(jsonMsg2);
        RestContext.request = req1;
        RestContext.response= res1;
        
        SFDCStubSECPI.OpportunityWithProductResponseSECPI resData1 = OpportunityWithProductRestResourceSECPI.getOpportuntityWithProductListSECPI();
        System.debug(resData1.inputHeaders.MSGSTATUS);
        System.debug(resData1.inputHeaders.ERRORTEXT);
        System.assertEquals('S', resData1.inputHeaders.MSGSTATUS);
        
        Test.stopTest();
    }
    
    static testMethod void getOpportuntityWithProductListNoData() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        List<Integration_EndPoints__c> ieps = new List<Integration_EndPoints__c>();
        Integration_EndPoints__c iep1 = new Integration_EndPoints__c();
        iep1.name ='Flow-080';
        iep1.layout_id__c = 'LAY024657';
        iep1.last_successful_run__c = System.now().addHours(-4);
        ieps.add(iep1);
        
        insert ieps;
        
        Account acc1 = new account(Name ='Test', Type='Customer',SAP_Company_Code__c = 'AAA');
        insert acc1;
        Id rtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('IT').getRecordTypeId();
        
        Opportunity opp = new Opportunity(accountId=acc1.id,recordtypeId=rtId,
        StageName='Identified',Name='TEst Opp',Type='Tender',Division__c='IT',ProductGroupTest__c='LCD_MONITOR',LeadSource='BTA Dealer',Closedate=system.today());
        insert opp;
        
        SFDCStub.OpportunityWithProductRequest reqData = new SFDCStub.OpportunityWithProductRequest();
        
        reqData.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData.inputHeaders.IFID    = SFDCStub.LAYOUTID_080;
        reqData.body.CONSUMER = 'GMBT';
        reqData.body.PAGENO = -1;
        
        String jsonMsg  = JSON.serialize(reqData);

        Test.startTest();
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/services/apexrest/OpportunityWithProduct';  //Request URL
        req.httpMethod = 'POST';//HTTP Request Type
        req.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req;
        RestContext.response= res;
        
        SFDCStub.OpportunityWithProductResponse resData = OpportunityWithProductRestResource.getOpportuntityWithProductList();
        System.debug(resData.inputHeaders.ERRORTEXT);
        System.assertEquals('F', resData.inputHeaders.MSGSTATUS);
        
        Test.stopTest();
    }
    
    static testMethod void getOpportuntityWithProductListWrongRequest() {
        Test.startTest();
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/services/apexrest/OpportunityWithProduct';  //Request URL
        req.httpMethod = 'POST';//HTTP Request Type
        req.requestBody = Blob.valueof('Hello World');
        RestContext.request = req;
        RestContext.response= res;
        
        SFDCStub.OpportunityWithProductResponse resData = OpportunityWithProductRestResource.getOpportuntityWithProductList();
        System.debug(resData.inputHeaders.ERRORTEXT);
        System.assertEquals('F', resData.inputHeaders.MSGSTATUS);
        
        //OpportunityWithProductSECPI
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/OpportunityWithProductSECPI';  //Request URL
        req1.httpMethod = 'POST';//HTTP Request Type
        req1.requestBody = Blob.valueof('Hello world');
        RestContext.request = req1;
        RestContext.response= res1;
        
        SFDCStubSECPI.OpportunityWithProductResponseSECPI resData1 = OpportunityWithProductRestResourceSECPI.getOpportuntityWithProductListSECPI();
        System.debug(resData1.inputHeaders.MSGSTATUS);
        System.debug(resData1.inputHeaders.ERRORTEXT);
        System.assertEquals('F', resData1.inputHeaders.MSGSTATUS);
        
        Test.stopTest();
    }
    static testMethod void pageNumLimitTest() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        List<Integration_EndPoints__c> ieps = new List<Integration_EndPoints__c>();
        
        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='SECPI Flow 080';
        iep2.layout_id__c = 'LAY025592';
        iep2.last_successful_run__c = System.now().addHours(-4);
        ieps.add(iep2);
        
        insert ieps;
        
        Account acc1 = new account(Name ='Test', Type='Customer',SAP_Company_Code__c = 'AAA');
        insert acc1;
        Id rtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('IT').getRecordTypeId();
        
        Opportunity opp = new Opportunity(accountId=acc1.id,recordtypeId=rtId,
        StageName='Identified',Name='TEst Opp',Type='Tender',Division__c='IT',ProductGroupTest__c='LCD_MONITOR',LeadSource='BTA Dealer',Closedate=system.today());
        insert opp;
        
        string SearchDtm = string.valueOf(system.now().addHours(5));
        SearchDtm = SearchDtm.replaceAll( '\\s+', '');
        SearchDtm = SearchDtm.remove(':');
        SearchDtm = SearchDtm.remove('-');
        
        string SearchDtmFrom = string.valueOf((system.now()-1).addHours(5));
        SearchDtmFrom = SearchDtmFrom.replaceAll( '\\s+', '');
        SearchDtmFrom = SearchDtmFrom.remove(':');
        SearchDtmFrom = SearchDtmFrom.remove('-');
        
        SFDCStubSECPI.OpportunityWithProductRequest reqData2 = new SFDCStubSECPI.OpportunityWithProductRequest();
        reqData2.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData2.inputHeaders.IFID    = SFDCStubSECPI.LAYOUTID_080;
        reqData2.body.CONSUMER = 'SECPIMSTR';
        reqData2.body.SEARCH_DTTM_TO = SearchDtm;
        reqData2.body.SEARCH_DTTM_FROM = SearchDtm;
        reqData2.body.PAGENO = 501;
        
        String jsonMsg2 = JSON.serialize(reqData2);
        
        Test.startTest();
        
        //OpportunityWithProductSECPI
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/OpportunityWithProductSECPI';  //Request URL
        req1.httpMethod = 'POST';//HTTP Request Type
        req1.requestBody = Blob.valueof(jsonMsg2);
        RestContext.request = req1;
        RestContext.response= res1;
        
        SFDCStubSECPI.OpportunityWithProductResponseSECPI resData1 = OpportunityWithProductRestResourceSECPI.getOpportuntityWithProductListSECPI();
        System.debug(resData1.inputHeaders.MSGSTATUS);
        System.debug(resData1.inputHeaders.ERRORTEXT);
        System.assertEquals('F', resData1.inputHeaders.MSGSTATUS);
        
        Test.stopTest();
    }
    
    static testMethod void pageNumMissingTest() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        List<Integration_EndPoints__c> ieps = new List<Integration_EndPoints__c>();
        
        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='SECPI Flow 080';
        iep2.layout_id__c = 'LAY025592';
        iep2.last_successful_run__c = System.now().addHours(-4);
        ieps.add(iep2);
        
        insert ieps;
        
        Account acc1 = new account(Name ='Test', Type='Customer',SAP_Company_Code__c = 'AAA');
        insert acc1;
        Id rtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('IT').getRecordTypeId();
        
        Opportunity opp = new Opportunity(accountId=acc1.id,recordtypeId=rtId,
        StageName='Identified',Name='TEst Opp',Type='Tender',Division__c='IT',ProductGroupTest__c='LCD_MONITOR',LeadSource='BTA Dealer',Closedate=system.today());
        insert opp;
        
        string SearchDtm = string.valueOf(system.now().addHours(5));
        SearchDtm = SearchDtm.replaceAll( '\\s+', '');
        SearchDtm = SearchDtm.remove(':');
        SearchDtm = SearchDtm.remove('-');
        
        string SearchDtmFrom = string.valueOf((system.now()-1).addHours(5));
        SearchDtmFrom = SearchDtmFrom.replaceAll( '\\s+', '');
        SearchDtmFrom = SearchDtmFrom.remove(':');
        SearchDtmFrom = SearchDtmFrom.remove('-');
        
        SFDCStubSECPI.OpportunityWithProductRequest reqData2 = new SFDCStubSECPI.OpportunityWithProductRequest();
        reqData2.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData2.inputHeaders.IFID    = SFDCStubSECPI.LAYOUTID_080;
        reqData2.body.CONSUMER = 'SECPIMSTR';
        reqData2.body.SEARCH_DTTM_TO = SearchDtm;
        reqData2.body.SEARCH_DTTM_FROM = SearchDtm;
        //reqData2.body.PAGENO = 501;
        
        String jsonMsg2 = JSON.serialize(reqData2);
        
        Test.startTest();
        
        //OpportunityWithProductSECPI
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/OpportunityWithProductSECPI';  //Request URL
        req1.httpMethod = 'POST';//HTTP Request Type
        req1.requestBody = Blob.valueof(jsonMsg2);
        RestContext.request = req1;
        RestContext.response= res1;
        
        SFDCStubSECPI.OpportunityWithProductResponseSECPI resData1 = OpportunityWithProductRestResourceSECPI.getOpportuntityWithProductListSECPI();
        System.debug(resData1.inputHeaders.MSGSTATUS);
        System.debug(resData1.inputHeaders.ERRORTEXT);
        System.assertEquals('F', resData1.inputHeaders.MSGSTATUS);
        
        Test.stopTest();
    }
}