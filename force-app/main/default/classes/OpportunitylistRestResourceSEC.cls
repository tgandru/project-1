/* This API Class is to Porvide the List Opportunities Related to Input CIS Account 
SEC will call this call with CIS Code which is SAP Company Code , we send response as List of opportunity under that Account Regardless of Stage

Created By - Vijay 3/15


*/
@RestResource(urlMapping='/Opportunitylist/*')
global class OpportunitylistRestResourceSEC {
    
    @HttpPost
    global static SFDCstubSECSFDC.OpportunitylistResponseSEC getOpportunityList() {
        
        RestRequest req = RestContext.request;
        String postBody = req.requestBody.toString();
       message_queue__c mq=new message_queue__c();
       mq.Integration_Flow_Type__c='IF_SFDC_SVC_078';
         SFDCstubSECSFDC.OpportunitylistResponseSEC response = new SFDCstubSECSFDC.OpportunitylistResponseSEC();
        RestResponse res = RestContext.response;
        SFDCstubSECSFDC.OpportunityInfoRequest request = null;
        
       /* String soldto = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        system.debug('soldto'+soldto);
        string status = 'null';*/
        try {
            request = (SFDCstubSECSFDC.OpportunityInfoRequest)json.deserialize(postBody, SFDCstubSECSFDC.OpportunityInfoRequest.class);
        } catch(Exception e) {
            response.inputHeaders.MSGGUID = '';
            response.inputHeaders.IFID  = '';
            response.inputHeaders.IFDate    = '';
            response.inputHeaders.MSGSTATUS = 'F';
            response.inputHeaders.ERRORTEXT = 'Invalid Request Message. - '+postBody;
            response.inputHeaders.ERRORCODE = '';
            string F='F';
            string er='Invalid request Message';
            insertmessagequeue(F,postBody,er,mq);
            return response;
        }
        
        response.inputHeaders.MSGGUID = request.inputHeaders.MSGGUID;
        response.inputHeaders.IFID    = request.inputHeaders.IFID;
        response.inputHeaders.IFDate  = request.inputHeaders.IFDate;
        mq.MSGGUID__c=request.inputHeaders.MSGGUID;
        mq.IFID__c=request.inputHeaders.IFID;
        mq.IFDate__c=request.inputHeaders.IFDate;
        
        String cisCode;
        string OppId;
        if(request.body.cisCode!=null && request.body.cisCode!=''){
            cisCode=request.body.cisCode;
            system.debug('cisCode'+cisCode);
        }
        if(request.body.opportunityId!=null && request.body.opportunityId!=''){
            OppId=request.body.opportunityId;
        }
        
        
        
        List<Opportunity> oppls=new  List<Opportunity>();
        
        try{
            if(cisCode !=null && cisCode !='' && (OppId==null || OppId=='')){
            oppls=[select id,name,Amount,owner.name,owner.email,stagename,type,Accountid,Account.Name,Account.SAP_Company_Code__c,Division__c,Roll_Out_Start__c,Roll_Out_End_Formula__c,CloseDate,Opportunity_Number__c from Opportunity where Account.SAP_Company_Code__c =:cisCode and Stagename !='Drop'];
            }else if((cisCode !=null && cisCode !='') && (OppId!=null && OppId!='')){
                
           oppls=[select id,name,Amount,owner.name,owner.email,stagename,type,Accountid,Account.Name,Account.SAP_Company_Code__c,Division__c,Roll_Out_Start__c,Roll_Out_End_Formula__c,CloseDate,Opportunity_Number__c from Opportunity where Account.SAP_Company_Code__c =:cisCode and id=:OppId  and Stagename !='Drop'];
   
            }
        }catch(Exception e) {
            String errmsg = 'DB Query Error - '+e.getmessage() + ' - '+e.getStackTraceString();
            response.inputHeaders.MSGSTATUS = 'F';
            response.inputHeaders.ERRORTEXT = 'Fail to retrieve BO List for ('+cisCode+') - '+req.requestURI + ', errmsg=' + errmsg;
            response.inputHeaders.ERRORCODE = '';
            string F='F';
            insertmessagequeue(F,postBody,errmsg,mq);
            return response;
        }  
        
        response.inputHeaders.MSGSTATUS = 'S';
        response.inputHeaders.ERRORTEXT = '';
        response.inputHeaders.ERRORCODE = '';
        string s='S';
         SFDCstubSECSFDC.Opportunitylistjson body= new  SFDCstubSECSFDC.Opportunitylistjson();
          body.Opportunitylists = new List<SFDCstubSECSFDC.Opportunitylist>();
        
        if(oppls.size()>0){
            
          for(Opportunity o:oppls){
              SFDCstubSECSFDC.Opportunitylist bd= new  SFDCstubSECSFDC.Opportunitylist();
                    bd.opportunityName =o.Name;
                    bd.opportunityId =o.Id;
                    bd.opportunityStage =o.stageName;
                    bd.salesType =o.Type;
                    bd.CISCode =o.Account.SAP_Company_Code__c;
                    bd.opportunityOwnerName =o.owner.Name;
                    bd.opportunityOwnerEmail =o.owner.email;
                    bd.dealSize =string.valueOf(o.Amount);
                    bd.rolloutPeriod =string.valueOf(o.Roll_Out_Start__c)+' To  '+string.valueOf(o.Roll_Out_End_Formula__c);
                    bd.MonetaryUnit ='USD';
                    bd.closeDate =string.valueOf(o.CloseDate);
                    bd.opportunityNo=o.Opportunity_Number__c;
                    bd.accountName=o.Account.Name;
                    bd.division =o.Division__c;
              
               body.Opportunitylists.add(bd);
             
          }
          response.body=body;
            
        }else{
            response.inputHeaders.ERRORTEXT = 'There Is No records.';
            
        }
        
        insertmessagequeue(s,postBody,'',mq);
        
        
       
        system.debug('response--'+body);
        
        
        return response;
    }
    
    public static void insertmessagequeue(string status,string req,string err,message_queue__c mq){
         
          mq.MSGSTATUS__c=status;
          mq.ERRORTEXT__c=req;
          if(status=='F'){
           mq.Status__c='failed';   
          }else{
                mq.Status__c='success';   
          }
          
        insert mq;
    }

}