@isTest(seeAllData=true)
private class OpportunitylistRestResourceSECTest {

	private static testMethod void testmethod1() {
	    
	     SFDCstubSECSFDC.OpportunityInfoRequest reqData = new SFDCstubSECSFDC.OpportunityInfoRequest();
	        reqData.inputHeaders.MSGGUID = cihStub.giveGUID();
            reqData.inputHeaders.IFDate    = '20180319';
            reqData.body.cisCode = '0001395682';
             String jsonMsg = JSON.serialize(reqData);
             Test.startTest();
            
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
                 
            req.requestURI = '/services/apexrest/Opportunitylist';  //Request URL
            req.httpMethod = 'POST';//HTTP Request Type
            req.requestBody = Blob.valueof(jsonMsg);
            RestContext.request = req;
            RestContext.response= res;
            
            SFDCstubSECSFDC.OpportunitylistResponseSEC resData = OpportunitylistRestResourceSEC.getOpportunityList();
            System.debug(resData.inputHeaders.MSGSTATUS);
            System.debug(resData.inputHeaders.ERRORTEXT);
            System.assertEquals('S', resData.inputHeaders.MSGSTATUS);
            
              
             Test.stopTest();
         
	    

	}
   
   
   	private static testMethod void testmethod2() {
	    
	     SFDCstubSECSFDC.OpportunityInfoRequest reqData = new SFDCstubSECSFDC.OpportunityInfoRequest();
	        reqData.inputHeaders.MSGGUID = cihStub.giveGUID();
            reqData.inputHeaders.IFDate    = '20180319';
            reqData.body.cisCode = '0001395682';
             String jsonMsg = JSON.serialize(reqData);
             string s='';
             Test.startTest();
            
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
                 
            req.requestURI = '/services/apexrest/Opportunitylist';  //Request URL
            req.httpMethod = 'POST';//HTTP Request Type
            req.requestBody = Blob.valueof(s);
            RestContext.request = req;
            RestContext.response= res;
            
            SFDCstubSECSFDC.OpportunitylistResponseSEC resData = OpportunitylistRestResourceSEC.getOpportunityList();
            System.debug(resData.inputHeaders.MSGSTATUS);
            System.debug(resData.inputHeaders.ERRORTEXT);
            System.assertEquals('F', resData.inputHeaders.MSGSTATUS);
            
              
             Test.stopTest();
         
	    

	}

   
}