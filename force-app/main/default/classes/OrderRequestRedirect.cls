public with sharing class OrderRequestRedirect {

    public string RecordID{get;set;}
    public string URL{get;set;}

    // we are extending the Order controller, so we query to get the parent record id
    public OrderRequestRedirect(ApexPages.StandardController controller) {
       URL=ApexPages.currentPage().getParameters().get('retURL');
       
         
        if(URL.contains('OpportunityCustomButtonsPage'))  {
            System.debug('## Inside If'+URL);
            RecordID = URL.substring(38,53);
        }
        else{   
            System.debug('## Inside else'+URL);
            URL = URL.removeStart('/');                 
            RecordID = URL;   
        }
     }
    
    // then we redirect to our desired page with the Record Id in the URL
    public pageReference redirect(){
        PageReference pg=new Pagereference('/apex/CreateOrderRequest?id=' + RecordID);
        pg.setRedirect(true);
        return pg;
    }
}