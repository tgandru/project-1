public class PBEIntegration {
    public static Id queuablejobId; 
    public static Id messageQueueId; 
    public static Map<Integer, list<pricingRecords>> pbeMap;
    //call it from Minute Batch class
    public static void processPBE(String responseString, message_queue__c msgQueue, DateTime responseTime) {
    
        pbeMap = new Map<Integer, list<pricingRecords>>();
        messageQueueId = msgQueue.Id;
        if(string.isNotBlank(responseString)){
            try{
    
            pricebookObject pbObject = (pricebookObject) json.deserialize(responseString, pricebookObject.class);
            System.debug('## after deserialize');
            
                if(pbObject!=null && pbObject.data!=null && pbObject.data.body!=null && pbObject.data.body.pricingRecords !=null && pbObject.data.body.pricingRecords.size()>0) {
                    System.debug('## pbObject :: Body :: pricingRecords'+pbObject+' ::'+pbObject.data.body+' ::'+pbObject.data.body.pricingRecords);
                    //pbeMap = CastIronIntegrationHelper.splitLongJSonPBEList(pbObject.data.body.pricingRecords);
                        for(Integer temp=0; temp<pbeMap.size();temp++){
                            System.debug('## pbeMap'+temp+':: '+pbeMap.get(temp));
                        }
                        system.debug('asas---line 19');
                        Integer i=0;
                        // Changed ManagePriceBookEntry constructor
                        //managePriceBookEntry priceBookEntryJob = new managePriceBookEntry(pbeMap, msgQueue.Id);
                        ManagePriceBookEntry priceBookEntryJob = new ManagePriceBookEntry(pbObject.data.body.pricingRecords, msgQueue.Id);
                        //queuablejobId= System.enqueueJob(priceBookEntryJob);
                        //System.debug('## In try block :: queuablejobId:: '+ queuablejobId);
                        System.debug('********************************************');
                        System.debug('Outside of Manage Price Book Entry' + msgQueue);
                        System.debug('********************************************'); 
                        // MK 7/7/2016 Setting the original/parent MQ to in progress
                        // Parent MQ
                        if(msgQueue.ParentMQ__c == null) {
                            msgQueue.status__c='In Progress'; // <== add other stuff that is in inputheaders of the response like msguid etc... 
                            msgQueue.MSGSTATUS__c = pbObject.data.inputHeaders.MSGSTATUS;
                            msgQueue.msguid__c = pbObject.data.inputHeaders.msgGUID;
                            msgQueue.IFID__c = pbObject.data.inputHeaders.ifID;
                            msgQueue.IFDate__c = pbObject.data.inputHeaders.ifDate;
                            update msgQueue; 
                        }
                        // Child MQ
                        else {
                            msgQueue.status__c='Success'; // <== add other stuff that is in inputheaders of the response like msguid etc... 
                            msgQueue.MSGSTATUS__c = pbObject.data.inputHeaders.MSGSTATUS;
                            msgQueue.msguid__c = pbObject.data.inputHeaders.msgGUID;
                            msgQueue.IFID__c = pbObject.data.inputHeaders.ifID;
                            msgQueue.IFDate__c = pbObject.data.inputHeaders.ifDate;
                            update msgQueue;
                        }
                        
                        //Integration_EndPoints__c pbIntegSettings = [SELECT id, last_successful_run__c,endPointURL__c, Name  FROM Integration_EndPoints__c where Name = 'Flow-009_1' limit 1];
                        // MK 7/6/2016 Saving a query since the whole point of using Custom Settings is to avoid SOQL calls
                        
                        // MK 7/6/2016 Creating new Message Queues for processing next batch
                        List<Message_Queue__c> mqs = new List<Message_Queue__c>();
                        mqs.add(new Message_Queue__c(Integration_Flow_Type__c = 'Flow-009_1',
                                                        Status__c = 'not started',
                                                        MSGSTATUS__c = '009_1',
                                                        ParentMQ__c = msgQueue.ParentMQ__c !=null ? msgQueue.ParentMQ__c : msgQueue.Id, // Signifies the Parent Message Queue
                                                        Object_Name__c = 'Pricebook'));
                        insert mqs;
                        
                    }
                    else {
                        msgQueue.ERRORCODE__c = pbObject.data.inputHeaders.errorCode; 
                        msgQueue.ERRORTEXT__c = pbObject.data.inputHeaders.errorText;
                        msgQueue.MSGSTATUS__c = pbObject.data.inputHeaders.MSGSTATUS;
                        msgQueue.msguid__c = pbObject.data.inputHeaders.msgGUID;
                        msgQueue.IFID__c = pbObject.data.inputHeaders.ifID;
                        msgQueue.IFDate__c = pbObject.data.inputHeaders.ifDate;
                        handleError(msgQueue);                  
                    }
                }
                
            catch(Exception ex){
                    System.debug('##Exception in PriceBookEntryJob :: '+ex);
                    System.debug('##Exception in PriceBookEntryJob :: '+ex.getMessage());
                    List<AsyncApexJob> jobDetails = [SELECT Id, Status, NumberOfErrors FROM AsyncApexJob WHERE Id = :queuablejobId]; // <== Why ?
                    system.debug('## In Catch Block'+jobDetails);                   
                }
            
            } //if block ends 
            else {
                handleError(msgQueue);
            }
    }
    
    public static void handleError(message_queue__c mq){
            system.debug('INSIDE HANDLEERROR METHOD.....'+mq);
    
            if(mq.retry_counter__c<=5){
                mq.status__c = 'failed-retry';
                mq.retry_counter__c += 1;
           }
           else {
                mq.status__c = 'failed';
                mq.retry_counter__c += 1;               
               }
            upsert mq;      
    }
    
    public class pricebookObject {
        public envelope data{get;set;}
    } 
    
    public class envelope {
        public inputHeaders inputHeaders{get;set;}
        public body body {get;set;}
    }

    public class inputHeaders {
        public String MSGGUID {get;set;}
        public String IFID {get;set;}
        public String IFDate {get;set;}
        public String MSGSTATUS {get;set;}
        public String ERRORTEXT {get;set;}
        public String ERRORCODE {get;set;}
    }
    
    public class body{
        public List<pricingRecords> pricingRecords {get;set;}
    }
    
    public class pricingRecords{
        public String materialNumber { get; set; }
        public String accountCode { get; set; }
        public String productPriceGroup { get; set; }
        public String invoicePrice { get; set; } 
        public String category {get; set;}   
     //   public String currency{ get; set; }
    }
 
}