/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 
 /**
  * @Author: Mir Khan
  * @Info: Test class for PBEIntegration
  */
@isTest(SeeAllData=true) 
private class PBEIntegrationTest {
    
    private static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
    private static String messageQueueId  = '007';
    private static String messageStatus  = 'Test';
    private static String messageQueueStatus = 'Not Started';
    private static Decimal messageQueueRetryCounter = 1;
    private static String messageQueueIntegrationFlowType = '';
    private static Integer messageTypeIndex = 0;

     
      static testMethod void testProcessPBENegative() {

        List<Integration_EndPoints__c> endPointListToUpdate = new List<Integration_EndPoints__c>();
        List<Integration_EndPoints__c> endPointList = Integration_EndPoints__c.getAll().values();

        for(Integration_EndPoints__c ep : endPointList) {
               if(ep.Name == 'Flow-009_1')
                ep.Partial_EndPoint__c='/SFDC_IF_US_009_1';
                ep.last_successful_run__c=System.now();
                ep.layout_id__c='N/A';
                endPointListToUpdate.add(ep);
        }
        update endPointListToUpdate;
        
        message_queue__c mq = new message_queue__c(MSGUID__c = messageQueueId, 
                                                    MSGSTATUS__c = messageStatus,  
                                                    Status__c = messageQueueStatus, 
                                                    retry_counter__c = messageQueueRetryCounter,
                                                    Object_Name__c = 'Pricebook');
        
        mq.Integration_Flow_Type__c = 'Flow-009_1';
        insert mq;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockResponseGenerator2('"S"'));
        CastIronIntegrationHelper.receivePBEEntries(mq);
        PBEIntegration.handleError(mq);
        mq.retry_counter__c = 6;
        update mq;
        PBEIntegration.handleError(mq);
        //PBEIntegration.inputHeaders pih = new PBEIntegration.inputHeaders();
        //pih.ErrorText='Test error';
        //pih.errorcode ='test code';
        Test.stopTest();
        
    }
    
        
    static testMethod void testProcessPBE() {
    //Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        
        //MK - 7/4/2016 This particular custom setting's last successful run was
        // not set in production and would fail deployment. The fix was to assign
        // the last successful run time while the test class executed.
        List<Integration_EndPoints__c> endPointListToUpdate = new List<Integration_EndPoints__c>();
        List<Integration_EndPoints__c> endPointList = Integration_EndPoints__c.getAll().values();

        for(Integration_EndPoints__c ep : endPointList) {
               if(ep.Name == 'Flow-009_1')
                ep.Partial_EndPoint__c='/SFDC_IF_US_009_1';
                ep.last_successful_run__c=System.now();
                ep.layout_id__c='N/A';
                endPointListToUpdate.add(ep);
        }
        update endPointListToUpdate;
        
        message_queue__c mq = new message_queue__c(MSGUID__c = messageQueueId, 
                                                    MSGSTATUS__c = messageStatus,  
                                                    Status__c = messageQueueStatus, 
                                                    retry_counter__c = messageQueueRetryCounter,
                                                    Object_Name__c = 'Pricebook');
        
        mq.Integration_Flow_Type__c = 'Flow-009_1';
        insert mq;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockResponseGenerator('"S"'));
        CastIronIntegrationHelper.receivePBEEntries(mq);
        Test.stopTest();
        
    }
    
    /**
  * Inner MockSuccessResponse2 class
  */   
 class MockResponseGenerator2 implements HttpCalloutMock {
    String responseStatus2;
    MockResponseGenerator2(String responseStatus2) {
        this.responseStatus2 = responseStatus2;
    }
    
    // Implement this interface method
    public HTTPResponse respond(HTTPRequest req) {
        System.assertEquals('callout:CIH_USasdf', req.getEndpoint());
        System.assertEquals('POST', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{' +
                  '"data": {' +
          '"inputHeaders": {' +
          '"MSGGUID": "b336d2a9-f8cb-a399-8eea-dc11782453c9",' +
          '"IFID": "TBD",' +
          '"IFDate": "160316",' +
          '"MSGSTATUS":' + responseStatus2 + ',' +
          '"ERRORTEXT": "testE",' +
          '"ERRORCODE": "testC"' +
          '}' +
          //'"body": {' +
           // '"pricingRecords": [{}]' +
           // '}' +
        '}' +
         '}');
        res.setStatusCode(200);
        return res;
    }

 }    
    /**
  * Inner MockSuccessResponse class
  */   
 class MockResponseGenerator implements HttpCalloutMock {
    String responseStatus;
    MockResponseGenerator(String responseStatus) {
        this.responseStatus = responseStatus;
    }
    // Implement this interface method
    public HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        System.assertEquals('callout:CIH_USasdf', req.getEndpoint());
        System.assertEquals('POST', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{' +
          '"data": {' +
          '"inputHeaders": {' +
          '"MSGGUID": "b336d2a9-f8cb-a399-8eea-dc11782453c9",' +
          '"IFID": "TBD",' +
          '"IFDate": "160316",' +
          '"MSGSTATUS":' + responseStatus + ',' +
          '"ERRORTEXT": "",' +
          '"ERRORCODE": ""' +
          '},' +
          '"body": {' +
            '"pricingRecords": [ {' +
          '"materialNumber": "CLP-510D2M/SEE",' +
          '"accountCode": "0001810282",' +
          '"productPriceGroup": "105",' +
          '"invoicePrice": "64.490000",' +
          '"currency": "USD",' +
          '"category": "IT"' +
          '},' +
          '{' +
          '"materialNumber": "CLP-510D2Y/SEE",' +
          '"accountCode": "0001810282",' +
          '"productPriceGroup": "105",' +
          '"invoicePrice": "64.490000",' +
          '"currency": "USD",' +
          '"category": "IT"' +
          '},' +
          '{' +
          '"materialNumber": "CLP-510D3K/SEE",' +
          '"accountCode": "0001810282",' +
          '"productPriceGroup": "105",' +
          '"invoicePrice": "62.240000",' +
          '"currency": "USD",' +
          '"category": "IT"' +
          '},' +
          '{' +
          '"materialNumber": "CLP-510D5C/SEE",' +
          '"accountCode": "0001810282",' +
          '"productPriceGroup": "105",' +
          '"invoicePrice": "105.740000",' +
          '"currency": "USD",' +
          '"category": "IT"' + 
          '},' +
          '{' +
          '"materialNumber": "CLP-510D5M/SEE",' +
          '"accountCode": "0001810282",' +
          '"productPriceGroup": "105",' +
          '"invoicePrice": "105.740000",' +
          '"currency": "USD",' +
          '"category": "IT"' +
          '},' +
          '{' +
          '"materialNumber": "CLP-510D5Y/SEE",' +
          '"accountCode": "0001810282",' +
          '"productPriceGroup": "105",' +
          '"invoicePrice": "105.740000",' +
          '"currency": "USD",' +
          '"category": "IT"' +
          '},' +
          '{' +
          '"materialNumber": "CLP-510D7K/SEE",' +
          '"accountCode": "0001810282",' +
          '"productPriceGroup": "105",' +
          '"invoicePrice": "87.740000",' +
          '"currency": "USD",' +
          '"category": "IT"' +
          '},' +
          '{' +
          '"materialNumber": "CLP-775ND/XAC",' +
          '"accountCode": "0001810282",' +
          '"productPriceGroup": "105",' +
          '"invoicePrice": "659.990000",' +
          '"currency": "USD",' +
          '"category": "IT"' +
          '},' +
          '{' +
          '"materialNumber": "CLP-C300A/XAA",' +
          '"accountCode": "0001810282",' +
          '"productPriceGroup": "105",' +
          '"invoicePrice": "41.990000",' +
          '"currency": "USD",' +
          '"category": "IT"' +
          '},' +
          '{' +
          '"materialNumber": "CLP-C600A/SEE",' +
          '"accountCode": "0001810282",' +
          '"productPriceGroup": "105",' +
          '"invoicePrice": "107.990000",' +
          '"currency": "USD",' +
          '"category": "Mobile"' +
          '}]' +            '}' +
                        '}' +
                    '}');
        res.setStatusCode(200);
        return res;
    }
  }
}