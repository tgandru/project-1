public with sharing class PRMLeadStatusHelper {

    // This is a real time call from OpportunityLeadTriggerHandler when PRM Leads are converted
    public static void callRoiEndpoint(message_queue__c mq){
    
    string partialEndpoint = Test.isRunningTest() ? 'asdf' : Integration_EndPoints__c.getInstance('Flow-016').partial_endpoint__c;
    string layoutId = Test.isRunningTest() ? 'asdf' : Integration_EndPoints__c.getInstance('Flow-016').layout_id__c;
    

       Lead lead = [Select Id, l.Status_Reason__c, l.Status, l.PRM_Lead_Id__c, l.LastTransferDate, l.External_Lead_ID__c, l.owner.email  
                    From Lead l where id = :mq.Identification_Text__c];
                    
    cihStub.prmLeadStatus reqStub = new cihStub.prmLeadStatus();
    cihStub.msgHeadersRequest headers = new cihStub.msgHeadersRequest(layoutId);
    cihStub.prmLeadStatusRequest body = new cihStub.prmLeadStatusRequest();
    mapfields(body, lead);
    reqStub.body = body;
    reqStub.inputHeaders = headers;
    string requestBody = json.serialize(reqStub);
    string response = '';
    System.debug('## Flow016 reqStub '+reqStub);
    System.debug('## Flow016 serialized '+requestBody);

    try{
        response = webcallout(requestBody, partialEndpoint); 
        system.debug(' ## Flow016 response from  mock call out' + response);
    } catch(exception ex){
        System.debug('## Flow016 ERROR IN RESPONSE ');
        system.debug(ex.getmessage()); 
        handleError(mq);
    }
        if(string.isNotBlank(response)){ 
            cihStub.PrmLeadStatusResponse res = (cihStub.PrmLeadStatusResponse) json.deserialize(response, cihStub.PrmLeadStatusResponse.class);

        system.debug('## Flow016 response ' + res);
        if(res.inputHeaders!=null){
            if(res.inputHeaders.MSGSTATUS == 'S'){
                handleSuccess(mq, res);
            } else {

                system.debug('## Flow016 Error');
                mq.ERRORCODE__c = res.inputHeaders.errorCode; 
                mq.ERRORTEXT__c = res.inputHeaders.errorText;
                mq.MSGSTATUS__c = res.inputHeaders.MSGSTATUS;
                mq.MSGUID__c = res.inputHeaders.msgGUID;
                mq.IFID__c = res.inputHeaders.ifID;
                mq.IFDate__c = res.inputHeaders.ifDate;           
                handleError(mq);
            }
        }else{
            System.debug('## Flow016 Else block ');
            handleError(mq);
        }
    } else {
        System.debug('## Flow016 Outer Else block');
        handleError(mq);
    }


    }
    public static void handleError(message_queue__c mq){
        mq.retry_counter__c += 1;
        mq.status__c = mq.retry_counter__c > 5 ? 'failed' : 'failed-retry';
        upsert mq;      
    }

    public static void handleSuccess(message_queue__c mq, cihStub.PrmLeadStatusResponse res){
        mq.status__c = 'success';
        mq.MSGSTATUS__c = res.inputHeaders.MSGSTATUS;
        mq.MSGUID__c = res.inputHeaders.msgGUID;
        mq.IFID__c = res.inputHeaders.ifID;
        mq.IFDate__c = res.inputHeaders.ifDate;           

    }


    public static string webcallout(string body, string partialEndpoint){ 

    System.Httprequest req = new System.Httprequest();

    HttpResponse res = new HttpResponse();
    req.setMethod('POST');
    req.setBody(body);
    system.debug(body);

    req.setEndpoint('callout:X012_013_ROI'+partialEndpoint); 
    req.setTimeout(120000);

    req.setHeader('Content-Type', 'application/json');
    req.setHeader('Accept', 'application/json');

    Http http = new Http();     
    res = http.send(req);
    system.debug(res.getBody());
    return res.getBody();       
    }


    public static void mapFields(cihStub.prmLeadStatusRequest body, Lead  l){
    
       Date tempDate = l.LastTransferDate;
       DateTime dt = DateTime.newInstance(tempDate.year(), tempDate.month(), tempDate.day());
       String lastTransferDTStr = dt.format('yyyyMMddhhmmss');
       
       body.prmLeadId = l.PRM_Lead_Id__c;
       body.extLeadId = l.External_Lead_ID__c;
       body.status = l.Status;
       body.lastTransferDate = lastTransferDTStr;
       body.ifType = 'Lead';
       body.statusReason =  l.Status_Reason__c;
       body.ownerEmail = l.owner.email;
       
      
    }    
    
}