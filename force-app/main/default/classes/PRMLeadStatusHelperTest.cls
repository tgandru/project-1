/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 
 /**
  *   PRMLeadStatusHelperTest
  *   @author: Mir Khan
 **/
 
@isTest(SeeAllData=true)
private class PRMLeadStatusHelperTest {
	
	private static String messageQueueId  = '007';
    private static String messageStatus  = 'Test';
    private static String messageQueueStatus = 'Not Started';
    private static Decimal messageQueueRetryCounter = 1;
    private static String messageQueueIntegrationFlowType = '';
    private static String prmLeadId = '0000';
    private static Integer messageTypeIndex = 0;

    static testMethod void testCalloutSuccess() {
       // Creating Zip code and Message queue outside of the 
       // Mock call out to avoid "System.CalloutException: You have
       // uncommitted work pending. Please commit or rollback before calling out"
       message_queue__c mq = createMessageQueue();
     
        // Set mock callout class
        Test.startTest();
        // Call method to test.
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock.
        Test.setMock(HttpCalloutMock.class, new MockResponseGenerator('"S"'));
        PRMLeadStatusHelper.callRoiEndpoint(mq);
        Test.stopTest();
    }
    
    static testMethod void testCalloutError() {
       // Creating Zip code and Message queue outside of the 
       // Mock call out to avoid "System.CalloutException: You have
       // uncommitted work pending. Please commit or rollback before calling out"
       message_queue__c mq = createMessageQueue();
     
        // Set mock callout class
        Test.startTest();
        // Call method to test.
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock.
        Test.setMock(HttpCalloutMock.class, new MockResponseGenerator('"E"'));
        PRMLeadStatusHelper.callRoiEndpoint(mq);
        Test.stopTest();
    }
    
    /**
	 * Creating Lead, Message Queue and Zip Code
	 **/ 
	static message_queue__c createMessageQueue() {
	 	
	 	Zipcode_Lookup__c zipCode = TestDataUtility.createZipcode();
    	insert zipCode;
	 	Lead lead = new Lead(PRM_Lead_Id__c = prmLeadId, LastName = 'Doe', Company = 'Samsung', Street='1 Market St',
                        City='San Francisco', State='CA', Country='US', PostalCode=zipCode.Name);
        
        insert lead;
        
        message_queue__c mq = new message_queue__c(Identification_Text__c = lead.Id, MSGUID__c = messageQueueId, 
	 												MSGSTATUS__c = messageStatus,  
	 												Status__c = messageQueueStatus, 
	 												retry_counter__c = messageQueueRetryCounter);
	 	
	 	mq.Integration_Flow_Type__c = 'Flow-016';
	 	insert mq;
        
        return mq;
	}
  
 /**
  * Inner MockResponse class
  */   
 class MockResponseGenerator implements HttpCalloutMock {
 	String responseStatus;
 	MockResponseGenerator(String responseStatus) {
 		this.responseStatus = responseStatus;
 	}
    // Implement this interface method
    public HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        System.assertEquals('callout:X012_013_ROIasdf', req.getEndpoint());
        System.assertEquals('POST', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{' +
						'"inputHeaders": {'+
						'"MSGGUID": "ada747b2-dda9-1931-acb2-14a2872d4358",' +
						'"IFID": "TBD",' +
						'"IFDate": "160317",' +
						'"MSGSTATUS":'+ responseStatus + ',' +
						'"ERRORTEXT": null,' +
						'"ERRORCODE": null' +
										'}' +
					 '}');
        res.setStatusCode(200);
        return res;
    }
  } 

}