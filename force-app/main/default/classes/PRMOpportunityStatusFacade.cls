/*lauren changed july 7 to allow 16_1 for opp stagename identification and drop*/
public with sharing class PRMOpportunityStatusFacade {
    
    public void gatherOppsFromTrigger(List<Opportunity> newOpp, Map<Id, Opportunity> oldOppMap) {
        System.debug('lclc in opps gather ');
        List<Opportunity> oppToSend=new List<Opportunity>();
        Set<Id> oppIds=new Set<Id>();

        for(opportunity opp:newOpp){
            oppIds.add(opp.Id);
        }

        for(Opportunity opp:newOpp){
            System.debug('lclc in for '+opp.PRM_Lead_ID__c+' '+opp.is_pricing_Status_change__c+' '+opp.StageName+' '+oldOppMap.get(opp.Id).StageName+' '+opp);
            if(opp.PRM_Lead_ID__c!=null){
               if(opp.Is_Pricing_Status_Change__c){
                    if(opp.Has_Partner__c==true && opp.HasOpportunityLineItem==true){
                        oppToSend.add(opp);
                    }else if(opp.Has_Partner__c==false && opp.HasOpportunityLineItem==true){
                        opp.addError('This opportunity is related to a partner in PRM. Please add the partner to this opportunity and try this action again.');
                    }

                }else{
                    if(opp.StageName!=oldOppMap.get(opp.Id).StageName){
                        if( opp.Has_Partner__c==true){
                            oppToSend.add(opp);
                        }else if(opp.Has_Partner__c==false){
                            opp.addError('This opportunity is related to a partner in PRM. Please add the partner to this opportunity and try this action again.');
                        } 
                    }
                } 
            }         
        }

        if(!oppToSend.isEmpty()){
            List<message_queue__c> mqs=new List<message_queue__c>();

            Map<String,message_queue__c> oldIDTxtAndMQ=new Map<String,message_queue__c>();
            for(message_queue__c mq:[select id,Identification_Text__c from message_queue__c where Integration_Flow_Type__c = 'Flow-016_1' and Identification_Text__c in :oppIds and Status__c='not started']){
                oldIDTxtAndMQ.put(mq.Identification_Text__c,mq);
            }

            for(Opportunity opp:oppToSend){
                if(!oldIDTxtAndMQ.containsKey(opp.Id)){
                    message_queue__c mq=new message_queue__c(Integration_Flow_Type__c = 'Flow-016_1', retry_counter__c = 0, Identification_Text__c = opp.Id, Status__c='not started', Object_Name__c='Opportunity');
                    mq.IFID__c=(!Test.isRunningTest() &&Integration_EndPoints__c.getInstance('Flow-016_1').layout_id__c!=null) ? Integration_EndPoints__c.getInstance('Flow-016_1').layout_id__c : '';
                    mqs.add(mq);
                }
            }

            if(!mqs.isEmpty()){
                try{
                    insert mqs;
                }catch(exception e){
                    system.debug('ERROR ---- '+e);
                }
                List<String> mqsIds=new List<String>();
                for(message_queue__c mq:mqs){
                    mqsIds.add(mq.Id);
                }

                String mqIds=String.join(mqsIds,',');
                sendMQsOpp(mqIds);

    
            }
        }

    }

    @future(callout=true)
    public static void sendMQsOpp(String mqIds){
        List<String> mqIdLst=mqIds.split(',');

        for(message_queue__c mq:[SELECT Id,Initalized_Request_Time__c,Request_To_CIH_Time__c,Response_Processing_Time__c, MSGSTATUS__c,ERRORCODE__c,ERRORTEXT__c,MSGUID__c,IFID__c,IFDate__c,Status__c, retry_counter__c, Integration_Flow_Type__c, CreatedBy.Name, Identification_Text__c FROM message_queue__c where Id IN :mqIdLst]){
            PRMOpputunityStatusHelper.callprmOppEndpoint(mq);
        }  

    }

    public void gatherInsertQuotesFromTrigger(List<Quote> quotes){
        Set<Id> oppIds=new Set<Id>();

        for(Quote q:quotes){
            oppIds.add(q.OpportunityId);
        }

        List<Opportunity> opps=[select Id, Is_Pricing_Status_Change__c, PRM_Lead_ID__c, StageName, Has_Partner__c from Opportunity where Id in :oppIds];

        List<Opportunity> oppsToUpdate=new List<Opportunity>();

        for(Opportunity o:opps){
            if((o.StageName!='Identified' || o.StageName!='Drop')&&  o.Is_Pricing_Status_Change__c==false ){//&&  o.Is_Pricing_Status_Change__c==false
                o.Is_Pricing_Status_Change__c=true;
                oppsToUpdate.add(o);
            }
        }

        if(!oppsToUpdate.isEmpty()){
            Database.SaveResult[] lsr = database.update(oppsToUpdate,false);
               for(Database.SaveResult sr : lsr)
                   if (!sr.isSuccess()) system.debug('Error occured during opportunty update....'+sr);
            
        }
        //Added by Thiru---set default conditions for SBS Quotes---05/24/2018
        for(Quote qt : quotes){
            if(qt.division__c=='SBS'){
                qt.additional_conditions__c = '<ul><li>Signed NDA on file.</li><li>Applicable taxes not included.</li><li>Shipping and Handling will be invoiced according to customer requirements.</li></ul>'; 
            }
        }

    } 

    public void gatherUpdateQuotesFromTrigger(List<Quote> newQuotes, Map<Id, Quote> oldQuoteMap){
        Set<Id> oppIds=new Set<Id>();

        for(Quote q:newQuotes){
            //Added by Thiru --for chanding the status when dates changes--8/18/2016
            //Starts here
            //q.Status !='Draft' &&
            if(q.Status !='Draft' && (q.ExpirationDate != oldQuoteMap.get(q.id).ExpirationDate || q.Valid_From_Date__c!= oldQuoteMap.get(q.id).Valid_From_Date__c))
            {
             //if(q.status!='draft'){
                q.Status ='Draft';
                q.Resubmit_for_Approval__c = true;
               // }
            }
            //Ends here
            if(q.Status!=oldQuoteMap.get(q.Id).Status){
                oppIds.add(q.OpportunityId);
            }
        }
        
        updateOpps(oppids);

    }
    
    //@future
    public static void updateOpps(Set<Id> oppids){
        
        List<Opportunity> opps=[select Id, Is_Pricing_Status_Change__c, PRM_Lead_ID__c, StageName, Has_Partner__c from Opportunity where Id in :oppIds];

        List<Opportunity> oppsToUpdate=new List<Opportunity>();
        System.debug('*******Call From Process Builder While it is updated to Present******');
        for(Opportunity o:opps){
            if((o.StageName!='Identified' || o.StageName!='Drop')&&  o.Is_Pricing_Status_Change__c==false){//
                o.Is_Pricing_Status_Change__c=true;
                oppsToUpdate.add(o);
            }
        }

        if(!oppsToUpdate.isEmpty()){
            System.debug('*******Update Opp Record******');
            Database.SaveResult[] lsr = database.update(oppsToUpdate,false);
               for(Database.SaveResult sr : lsr)
                   if (!sr.isSuccess()) system.debug('Error occured during opportunty update....'+sr);
        }
    }
    
    
    Public static void UpdateCIOppExternalID(set<id> oppids){// Added By Vijay to Update the Ext ID in CI Opp 
          Map<String,List<Quote>> qtMap=New  Map<String,List<Quote>>();
           List<Channelinsight__CI_Opportunity__c> updatelist=new List<Channelinsight__CI_Opportunity__c>();
          for(Quote q:[select id,name,OpportunityID,External_SPID__c,CreatedDate,status,Legacy_id__c,QuoteNumber   from Quote where OpportunityID in :oppids and (status='Approved' or status='Presented') order By CreatedDate Asc]){
                   if(qtMap.containsKey(q.OpportunityID)){
                       List<Quote> qt=qtMap.get(q.OpportunityID);
                       qt.Add(q);
                       qtMap.Put(q.OpportunityID,qt);
                   }else{
                       qtMap.Put(q.OpportunityID,new list<Quote>{q});
                   }
                   
           }
           
           if(!qtmap.isEmpty()){
             for(Channelinsight__CI_Opportunity__c c:[select id,Channelinsight__OPP_External_Id__c,Channelinsight__OPP_Opportunity__c from Channelinsight__CI_Opportunity__c where Channelinsight__OPP_Opportunity__c in :oppids ]){
                  try{
                      List<Quote> qts=qtMap.get(c.Channelinsight__OPP_Opportunity__c);
                       if(!qts.isEmpty()){
                           if(qts[0].Legacy_id__c ==null){
                               c.Channelinsight__OPP_External_Id__c='SP-'+qts[0].QuoteNumber ;
                           }else{
                           c.Channelinsight__OPP_External_Id__c=qts[0].External_SPID__c;
                               
                           }
                           updatelist.add(c);
                       }
                  }catch(exception ex){
                      
                  }
              }
           }
           
           if(!updatelist.isEmpty()){
               update updatelist;
           }
           
           
    }
    
    
}