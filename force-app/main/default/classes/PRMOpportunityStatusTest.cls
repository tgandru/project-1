@isTest
private class PRMOpportunityStatusTest {

	static Id endUserRT = TestDataUtility.retrieveRecordTypeId('End_User', 'Account'); 
   	static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');

	@testSetup static void testDataSetup(){

		//Jagan: Test data for fiscalCalendar__c
        Test.loadData(fiscalCalendar__c.sObjectType, 'FiscalCalendarTestData');
        
		//Channelinsight configuration
		Channelinsight.CI_TEST_TestUtils.createBaseInstallData();

		List<prm_integration__c> prmis=new List<prm_integration__c>();
		prm_integration__c prmi1=new prm_integration__c(Object_Name__c='Opportunity',PRM_Value__c='Qualified',SFDC_Value__c='Proposal',Name='Opportunity-Proposal');
		prmis.add(prmi1);
		prm_integration__c prmi2=new prm_integration__c(Object_Name__c='Opportunity',PRM_Value__c='Qualified',SFDC_Value__c='Qualified',Name='Opportunity-Qualified');
		prmis.add(prmi2);
		prm_integration__c prmi3=new prm_integration__c(Object_Name__c='Quote',PRM_Value__c='Submitted',SFDC_Value__c='Draft',Name='Quote-Draft');
		prmis.add(prmi3);
		prm_integration__c prmi4=new prm_integration__c(Object_Name__c='Quote',PRM_Value__c='Approved',SFDC_Value__c='Approved',Name='Quote-Approved');
		prmis.add(prmi4);
		prm_integration__c prmi5=new prm_integration__c(Object_Name__c='Opportunity',PRM_Value__c='Identified',SFDC_Value__c='Identified',Name='Opportunity-Identified');
		prmis.add(prmi5);
		insert prmis;

		 //Creating test Account
	    Account acct=TestDataUtility.createAccount(TestDataUtility.generateRandomString(5));
	    acct.RecordTypeId = endUserRT;
		insert acct;
		
	    
	   //Creating the Parent Account for Partners
		Account paacct=TestDataUtility.createAccount(TestDataUtility.generateRandomString(7));
		acct.RecordTypeId = directRT;
		insert paacct;
		
		
		//Creating custom Pricebook
		PriceBook2 pb = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(4));
		pb.Division__c='IT'; 
		insert pb;
		
		
		//Creating list of Opportunities
		//List<Opportunity> opps=new List<Opportunity>();
		Opportunity oppOpen=TestDataUtility.createOppty('New Opp from Test setup',acct, pb, 'RFP', 'IT', 'FAX/MFP', 'Identified');
		oppOpen.PRM_Lead_ID__c='iamaprmleadid';
		//opps.add(oppOpen);
		//insert opps;
		insert oppOpen;

		System.debug('lclc test this '+[select id, stagename from opportunity]);
	    
	   //Creating the partner for that opportunity
	   Partner__c partner = TestDataUtility.createPartner(oppOpen,acct,paacct,'Distributor');
	   partner.Is_Primary__c=true;
	   insert partner;

	   //Creating Products
		List<Product2> prods=new List<Product2>();
		Product2 standaloneProd=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Standalone Prod1', 'Printer[C2]', 'FAX/MFP', 'H/W');
		prods.add(standaloneProd);
		Product2 parentProd1=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Parent Prod1', 'Printer[C2]', 'FAX/MFP', 'H/W');
		prods.add(parentProd1);
		Product2 childProd1=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod1', 'Printer[C2]', 'PRINTER', 'Service pack');
		prods.add(childProd1);
		Product2 childProd2=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod2', 'Printer[C2]', 'PRINTER', 'Service pack');
		prods.add(childProd2);
		Product2 parentProd2=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Parent Prod2', 'Printer[C2]', 'FAX/MFP', 'H/W');
		prods.add(parentProd2);
		Product2 childProd3=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod3', 'Printer[C2]', 'PRINTER', 'Service pack');
		prods.add(childProd3);
		Product2 childProd4=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod4', 'Printer[C2]', 'PRINTER', 'Service pack');
		prods.add(childProd4);
		Product2 childProd5Standalone=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12), 'child AND standalone prod5', 'Printer[C2]', 'FAX/MFP', 'Service pack');
        prods.add(childProd5Standalone);
		insert prods;
		
	    /*First Insert the standard pricebook entry*/
		
		// Get standard price book ID.
	    // This is available irrespective of the state of SeeAllData.		
	    Id pricebookId = Test.getStandardPricebookId();
	           
	    //CreatePBEs
        //create the pricebook entries for the custom pricebook
        List<PriceBookEntry> custompbes=new List<PriceBookEntry>();
        PriceBookEntry standaloneProdPBE=TestDataUtility.createPriceBookEntry(standaloneProd.Id, pb.Id);
        custompbes.add(standaloneProdPBE);
        PriceBookEntry parentProd1PBE=TestDataUtility.createPriceBookEntry(parentProd1.Id, pb.Id);
        custompbes.add(parentProd1PBE);
        PriceBookEntry parentProd2PBE=TestDataUtility.createPriceBookEntry(parentProd2.Id, pb.Id);
        custompbes.add(parentProd2PBE);
        PriceBookEntry childProd1PBE=TestDataUtility.createPriceBookEntry(childProd1.Id, pb.Id);
        custompbes.add(childProd1PBE);
        PriceBookEntry childProd2PBE=TestDataUtility.createPriceBookEntry(childProd2.Id, pb.Id);
        custompbes.add(childProd2PBE);
        PriceBookEntry childProd3PBE=TestDataUtility.createPriceBookEntry(childProd3.Id, pb.Id);
        custompbes.add(childProd3PBE);
        PriceBookEntry childProd4PBE=TestDataUtility.createPriceBookEntry(childProd4.Id, pb.Id);
        custompbes.add(childProd4PBE);
        PriceBookEntry childProd5StandalonePBE=TestDataUtility.createPriceBookEntry(childProd5Standalone.Id, pb.Id);
        custompbes.add(childProd5StandalonePBE);
        insert custompbes;

        //insert product relationship
        List<Product_Relationship__c> prs=new List<Product_Relationship__c>();
        Product_Relationship__c pr1=TestDataUtility.createProductRelationships(childProd1, parentProd1);
        prs.add(pr1);
        Product_Relationship__c pr2=TestDataUtility.createProductRelationships(childProd2, parentProd1);
        prs.add(pr2);
        Product_Relationship__c pr3=TestDataUtility.createProductRelationships(childProd3, parentProd2);
        prs.add(pr3);
        Product_Relationship__c pr4=TestDataUtility.createProductRelationships(childProd4, parentProd2);
        prs.add(pr4);
        Product_Relationship__c pr5=TestDataUtility.createProductRelationships(childProd5Standalone, parentProd2);
        prs.add(pr5);
        insert prs;

        // Creating opportunity Line item
        List<OpportunityLineItem> olis=new List<OpportunityLineItem>();
        OpportunityLineItem standaloneProdOLI=TestDataUtility.createOpptyLineItem(standaloneProd, standaloneProdPBE, oppOpen);
        olis.add(standaloneProdOLI);
        OpportunityLineItem parentProd1OLI=TestDataUtility.createOpptyLineItem(parentProd1, parentProd1PBE, oppOpen);
        parentProd1OLI.Parent_Product__c=parentProd1.Id;
        olis.add(parentProd1OLI);
        OpportunityLineItem childProd1OLI=TestDataUtility.createOpptyLineItem(childProd1, childProd1PBE, oppOpen);
        childProd1OLI.Parent_Product__c=parentProd1.Id;
        olis.add(childProd1OLI);
        OpportunityLineItem childProd2OLI=TestDataUtility.createOpptyLineItem(childProd2, childProd2PBE, oppOpen);
        childProd2OLI.Parent_Product__c=parentProd1.Id;
        olis.add(childProd2OLI);
        OpportunityLineItem parentProd2OLI=TestDataUtility.createOpptyLineItem(parentProd2, parentProd2PBE, oppOpen);
        parentProd2OLI.Parent_Product__c=parentProd2.Id;
        olis.add(parentProd2OLI);
        OpportunityLineItem childProd3OLI=TestDataUtility.createOpptyLineItem(childProd3, childProd3PBE, oppOpen);
        childProd3OLI.Parent_Product__c=parentProd2.Id;
        olis.add(childProd3OLI);
        OpportunityLineItem childProd4OLI=TestDataUtility.createOpptyLineItem(childProd4, childProd4PBE, oppOpen);
        childProd4OLI.Parent_Product__c=parentProd2.Id;
        olis.add(childProd4OLI);
        OpportunityLineItem childProd5OLI=TestDataUtility.createOpptyLineItem(childProd5Standalone, childProd5StandalonePBE, oppOpen);
        childProd5OLI.Parent_Product__c=parentProd2.Id;
        olis.add(childProd5OLI);
        insert olis;

        OpportunityLineItem childProd5StandaloneOLI=TestDataUtility.createOpptyLineItem(childProd5Standalone, childProd5StandalonePBE, oppOpen);
		insert childProd5StandaloneOLI;

		//create quote
        Quote quot = TestDataUtility.createQuote('Test Quote', oppOpen, pb);
        quot.ROI_Requestor_Id__c=UserInfo.getUserId();
        quot.Status='Draft';
        insert quot;
        System.debug('lclc quote delete '+quot.ProductGroupMSP__c);


        //create qlis
       	List<QuoteLineItem> qlis=new List<QuoteLineItem>();
        QuoteLineItem standaloneProdQLI=TestDataUtility.createQuoteLineItem(quot.Id, standaloneProd, standaloneProdPBE, standaloneProdOLI);
       	qlis.add(standaloneProdQLI);
       	QuoteLineItem parentProd2QLI=TestDataUtility.createQuoteLineItem(quot.Id, parentProd2, parentProd2PBE, parentProd2OLI);
       	parentProd2QLI.Parent_Product__c=parentProd2.Id;
       	qlis.add(parentProd2QLI);
       	QuoteLineItem childProd3QLI=TestDataUtility.createQuoteLineItem(quot.Id, childProd3, childProd3PBE, childProd3OLI);
       	childProd3QLI.Parent_Product__c=parentProd2.Id;
       	qlis.add(childProd3QLI);
       	QuoteLineItem childProd4QLI=TestDataUtility.createQuoteLineItem(quot.Id, childProd4, childProd4PBE, childProd4OLI);
       	childProd4QLI.Parent_Product__c=parentProd2.Id;
       	qlis.add(childProd4QLI);
       	QuoteLineItem childProd5QLI=TestDataUtility.createQuoteLineItem(quot.Id, childProd5Standalone, childProd5StandalonePBE, childProd5OLI);
       	childProd5QLI.Parent_Product__c=parentProd2.Id;
       	qlis.add(childProd5QLI);
       	QuoteLineItem childProd5StandaloneQLI=TestDataUtility.createQuoteLineItem(quot.Id, childProd5Standalone, childProd5StandalonePBE, childProd5StandaloneOLI);
       	qlis.add(childProd5StandaloneQLI);
       	System.debug('lclc for childProd5StandaloneQLI whats pbe  '+childProd5StandaloneQLI.pricebookentryId);
       	insert qlis;

       	System.debug('lclc whats this for opp '+[select id,stagename from opportunity]);
       	System.debug('lclc whats this for quote '+[select id, status from quote]);


	}
	
	@isTest static void test_method_OppUpdate() {
		// Implement test code
		System.debug('lclc test in method '+[select id, stagename from Opportunity ]);
		Opportunity opp=[select id, stagename from Opportunity limit 1];
		System.debug('lclc StageName '+opp.stagename);

		Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockResponseGenerator());
		opp.StageName='Proposal';
		update opp;
		Test.stopTest();
	}
	
	@isTest static void test_method_QuoteUpdate() {
		// Implement test code

		Opportunity opp=[select id, stagename from Opportunity limit 1];
		System.debug('lclc StageName '+opp.stagename);

		Quote q=[select id, opportunityId, status, External_SPID__c from quote];

		Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockResponseGenerator());
        q.External_SPID__c='000nhritnbsfrnigbn';
		q.Status='Approved';
		update q;
		Test.stopTest();


	}

	class MockResponseGenerator implements HttpCalloutMock {
    // Implement this interface method
        public HTTPResponse respond(HTTPRequest req) {
            // Optionally, only send a mock response for a specific endpoint
            // and method.
            System.assertEquals('callout:X012_013_ROIasdf', req.getEndpoint());
            System.assertEquals('POST', req.getMethod());
            
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{' +
                            '"inputHeaders": {'+
                            '"MSGGUID": "ada747b2-dda9-1931-acb2-14a2872d4358",' +
                            '"IFID": "TBD",' +
                            '"IFDate": "160317",' +
                            '"MSGSTATUS": "S",' +
                            '"ERRORTEXT": null,' +
                            '"ERRORCODE": null' +
                                            '}' +
                         '}');
            res.setStatusCode(200);
            return res;



        }
    }
	
}