/*lauren changed july 7 to allow 16_1 for opp stagename identification and drop also to modify values for opp stage name.*/
public with sharing class PRMOpputunityStatusHelper {

    public static string distributorName; 
    public static string distributorCompanyCode;
    public static string distributorContactName;
    public static string resellerName;
    public static string resellerCompanyCode;
    public static string resellerContactName;
    public static Map<Id, List<Partner__c>> partnerMap;
    // This is a real time call 
    public static void callprmOppEndpoint(message_queue__c mq){
        string partialEndpoint = Test.isRunningTest() ? 'asdf' : Integration_EndPoints__c.getInstance('Flow-016_1').partial_endpoint__c;
        string layoutId = Test.isRunningTest() ? 'asdf' : Integration_EndPoints__c.getInstance('Flow-016_1').layout_id__c;    
        partnerMap = new Map<Id, List<Partner__c>>();
        distributorName = distributorCompanyCode = distributorContactName = resellerName = resellerCompanyCode = resellerContactName = '';

        Long sendTime=0;
        Long receiveTime=0;
        Long updateTime=0;
        DateTime startTime=DateTime.now();
                    
        Opportunity opp = [Select id, PRM_Lead_ID__c, Account.SAP_COMPANY_CODE__C, Account.Name, CreatedById, CreatedBy.Name,
                            External_Opportunity_ID__c, Name,OwnerId, Owner.Name, Historical_Comments__c,StageName, External_Lead_ID__c, IsWon,
                            Deal_Registration_Approval__c, Drop_Reason__c, LastModifiedDate, LastModifiedById, LastModifiedBy.Name, IsClosed,
                            Account.Industry_type__c, Roll_Out_Start__c, Roll_Out_End__c, Owner.email, CreatedDate, Roll_Out_End_Formula__c, Is_Pricing_Status_Change__c,                           
                            (SELECT Id, SAP_Material_id__c, Quantity, Product_Group__c, Requested_Price__c, Alternative__c FROM opportunitylineitems)
                            FROM Opportunity where id = :mq.Identification_Text__c];

        if(opp.Is_Pricing_Status_Change__c && opp.opportunitylineitems.isEmpty()){
            handleSuccess(mq,null,opp, null, null, startTime);
            return;
        }

        List<Quote> quotes=[select id, Status,External_SPID__c, (select Id, OLIID__c from quotelineitems) from Quote where OpportunityId=:opp.Id];

                    
       for(Partner__c p:[SELECT Id, Role_is_Reseller__c,Role__c,Is_Primary__c,Partner__c, Partner__r.SAP_Company_code__c, Partner__r.Name, 
                        Opportunity__c, Contact__c, Contact__r.Name from Partner__c where Opportunity__c =:mq.Identification_Text__c]){
            if(!partnerMap.containsKey(p.Opportunity__c)){
                partnerMap.put(p.Opportunity__c, new List<Partner__c>());
            }
            
            partnerMap.get(p.Opportunity__c).add(p);
        }

        cihStub.PrmOppRequest reqStub = new cihStub.PrmOppRequest();
        cihStub.msgHeadersRequest headers = new cihStub.msgHeadersRequest(layoutId);
        cihStub.PrmOppBodyRequest body = new cihStub.PrmOppBodyRequest();
        mapPRMfields(body, opp, partnerMap, quotes);
        reqStub.body = body;
        reqStub.inputHeaders = headers;
        string requestBody = json.serialize(reqStub);
        string response = '';
        System.debug('lclc016_1 reqStub '+reqStub);
        System.debug('lclc016_1 serialized '+requestBody);

        try{
            sendTime=DateTime.now().getTime();
            response = webcallout(requestBody, partialEndpoint); 
            system.debug('lclc016_1 response returned from the mock call out' + response);
            receiveTime=DateTime.now().getTime();
        } 
        catch(exception ex){
            System.debug('lclc016_1 ERROR IN RESPONSE ');
            system.debug(ex.getmessage()); 
            handleError(mq,null, opp, startTime);
        }
        if(string.isNotBlank(response)){ 
            cihStub.PrmOppResponse res = (cihStub.PrmOppResponse) json.deserialize(response, cihStub.PrmOppResponse.class);
            system.debug('lclc016_1 deserialized response ' + res);
            updateTime=DateTime.now().getTime();
            if(res.inputHeaders!=null ){
                if(res.inputHeaders.MSGSTATUS == 'S'){
                    handleSuccess(mq, res,opp, (receiveTime - sendTime),(updateTime - receiveTime), startTime);
                } 
                else {
                    System.debug('lclc016_1 Else block');                   
                    handleError(mq, res, opp,startTime);
                }
            }
            else{
                System.debug('lclc016_1 Else block 2 ');
                handleError(mq, res, opp,startTime);
            }
        } 
        else {
            System.debug('lclc016_1 Else block 3');
            handleError(mq, null, opp,startTime);
        }

    }
    

    public static void mapPRMFields(cihStub.PrmOppBodyRequest body, Opportunity  opp, Map<Id, List<partner__c>> oppPartnerMap, List<Quote> quotes){
       
       String lastModifiedDTStr = opp.LastModifiedDate!=null ? formatDateTimeField(opp.LastModifiedDate) : '' ;
       String createdDTStr = opp.CreatedDate!=null ? formatDateTimeField(opp.CreatedDate) : '';
       String startDTStr = opp.Roll_Out_Start__c!=null ? formatDateField(opp.Roll_Out_Start__c) : ''; 
       String endDTStr = opp.Roll_Out_End_Formula__c!=null ? formatDateField(opp.Roll_Out_End_Formula__c) : ''; 
       
        for(Partner__c p: oppPartnerMap.get(opp.Id)){
            if(p.Role__c == 'Distributor' && p.Is_Primary__c){
                distributorName = p.Partner__r.Name;
                distributorCompanyCode = p.Partner__r.SAP_Company_code__c;
                distributorContactName = p.Contact__r.Name;
            }
            if(p.Role_is_Reseller__c && p.Is_Primary__c){               
                resellerName = p.Partner__r.Name;
                resellerCompanyCode = p.Partner__r.SAP_Company_code__c;
                resellerContactName = p.Contact__r.Name;
            }           
        }
        
        body.prmLeadId = opp.PRM_Lead_ID__c; 
        body.accountCompanyCode  = opp.Account.SAP_COMPANY_CODE__C;
        body.accountName  = opp.Account.Name;
        body.oppCreatedByName  = opp.CreatedBy.Name;
        body.oppCreatedByID = opp.CreatedById;
        body.extOppID  = opp.External_Opportunity_ID__c; //
        body.oppName  = opp.Name;
        body.oppOwner = opp.Owner.Name; //@TODO IS OWNER NAME OR OWNER?
        body.leadExtID  = opp.External_Lead_ID__c;
        body.oppHistoricalComments = opp.Historical_Comments__c;
        body.oppStageName=prm_integration__c.getinstance('Opportunity-'+opp.StageName).PRM_Value__c!=null ? prm_integration__c.getinstance('Opportunity-'+opp.StageName).PRM_Value__c : '';
       
        body.type ='Pre-Bo'; //@TODO KR:Confirm Static value 'Pre-BO'
        body.leadDealRegistrationFlag  = (opp.Deal_Registration_Approval__c=='Not Started')? 'N':'Y';//KR: Confirm
        body.oppDealRegistrationApproval  = opp.Deal_Registration_Approval__c;
        body.oppDropReason  = opp.Drop_Reason__c;
        body.oppLastModifiedDate  = lastModifiedDTStr;
        body.oppLastModifiedByName  = opp.LastModifiedBy.Name;
        body.accountIndustryType  = opp.Account.Industry_type__c;
        body.rollOutStartDate  = startDTStr;
        body.rollOutEndDate  = endDTStr;
        body.ownerEmail  = opp.Owner.email;
        body.ownerName  = opp.Owner.Name;
        body.createdDate  = createdDTStr;
        body.pyDistSapCompanyCode  = String.isNotBlank(distributorCompanyCode)? distributorCompanyCode:'';
        body.pyDistName  = String.isNotBlank(distributorName)?distributorName:'';
        body.pyContactName = String.isNotBlank(distributorContactName)?distributorContactName:'';
        body.resellerSapCompanyCode =  String.isNotBlank(resellerCompanyCode)?resellerCompanyCode:'';
        body.resellerName = String.isNotBlank(resellerName)?resellerName:'';
        body.resellerContactName = String.isNotBlank(resellerContactName)?resellerContactName:'';
        
        list<cihStub.OppurtunityLineItemRequest> opptyLines = new list<cihStub.OppurtunityLineItemRequest>();
        if(opp.opportunitylineitems != null && opp.opportunitylineitems.size()!=0){
            mapOpptyLineFields(opptyLines, opp.opportunitylineitems, opp, quotes);
        }else{
            mapOpptyLineFields(opptyLines,null,opp,null); 
        } 
        body.lines = opptyLines;
      
    }     
    public static void mapOpptyLineFields(list<cihStub.OppurtunityLineItemRequest> opptyLines, list<opportunitylineitem> oliList, Opportunity opp, List<Quote> quotes){       
        integer i=0;
        Set<Id> oliIdSet = new Set<Id>();
        Map<Id, Quote> oliToQuoteMap = new Map<Id, Quote>();

        if(oliList!=null){
            for(opportunitylineitem oli:oliList){
                if(!oli.Alternative__c){
                    oliIdSet.add(oli.Id); 
                }
            }

            //match up oli and qli
            if(!quotes.isEmpty()){
                for(Quote q:quotes){
                    for(QuoteLineItem qli:q.quotelineitems){
                        oliToQuoteMap.put(qli.OLIID__c, q);
                    }

                }
            }
            
            
            
            for(opportunitylineitem oli : oliList){
                if(oli.Alternative__c){
                    continue;
                }

                cihStub.OppurtunityLineItemRequest oliRequest = new cihStub.OppurtunityLineItemRequest();
                i+=10;
                oliRequest.seqNum = string.valueOf(i);
                oliRequest.prmLeadId = opp.PRM_Lead_ID__c;
                oliRequest.sapMaterialId = oli.SAP_Material_id__c; 
                oliRequest.quantity = string.valueOf(oli.Quantity);
                oliRequest.productGroup = oli.Product_Group__c;
                oliRequest.extOppId = opp.External_Opportunity_ID__c;
                
                if(!oliToQuoteMap.isEmpty() && oliToQuoteMap.containsKey(oli.Id)){
                    //set status.
                    Quote q=oliToQuoteMap.get(oli.Id);
                    oliRequest.quoteStatus=prm_integration__c.getinstance('Quote-'+q.Status).PRM_Value__c != null ? prm_integration__c.getinstance('Quote-'+q.Status).PRM_Value__c : '';

                    //set extspid
                    oliRequest.extSPID=q.External_SPID__c;
                }else{
                    oliRequest.quoteStatus='Other';
                    oliRequest.extSPID = ''; 
                }


                oliRequest.requestedPrice = string.valueOf(oli.Requested_Price__c);
                opptyLines.add(oliRequest);
            }


        }else{
            cihStub.OppurtunityLineItemRequest oliRequest = new cihStub.OppurtunityLineItemRequest();
            i+=10;
            oliRequest.seqNum = '';
            oliRequest.prmLeadId = opp.PRM_Lead_ID__c;
            oliRequest.sapMaterialId = ''; 
            oliRequest.quantity = '0';
            oliRequest.productGroup = 'No Lines';
            oliRequest.extOppId = opp.External_Opportunity_ID__c;
            oliRequest.quoteStatus='Other';
            oliRequest.extSPID = ''; 
            oliRequest.requestedPrice='0';

            opptyLines.add(oliRequest);

        }
        
    
    }


    public static void handleError(message_queue__c mq,cihStub.PrmOppResponse res, Opportunity opp, DateTime initializeTime){
    System.debug('lclc in fail 016_1');
    mq.retry_counter__c += 1;
    mq.status__c = mq.retry_counter__c > 5 ? 'failed' : 'failed-retry';
    mq.Initalized_Request_Time__c=initializeTime;
    if(res!=null && res.inputHeaders!=null){
        mq.ERRORCODE__c = res.inputHeaders.errorCode; 
        mq.ERRORTEXT__c = res.inputHeaders.errorText;
        mq.MSGSTATUS__c = res.inputHeaders.MSGSTATUS;
        mq.MSGUID__c = res.inputHeaders.msgGUID;
        mq.IFID__c = res.inputHeaders.ifID;
        mq.IFDate__c = res.inputHeaders.ifDate;   
    }
    if(mq.retry_counter__c > 5){
        opp.Is_Pricing_Status_Change__c=false;
        upsert opp;  
    }
    upsert mq;  
    }

    public static void handleSuccess(message_queue__c mq, cihStub.PrmOppResponse res, Opportunity opp,  Long requestTime, Long updateTime, DateTime initializeTime){
        System.debug('lclc in pass 016_1');
        mq.status__c = 'success';
        mq.Initalized_Request_Time__c=initializeTime;
        if(requestTime!=null){
            mq.Request_To_CIH_Time__c=Integer.valueOf(requestTime);
        }
        if(updateTime!=null){
            mq.Response_Processing_Time__c=Integer.valueOf(updateTime);
        }
        if(res!=null){
            mq.MSGSTATUS__c = res.inputHeaders.MSGSTATUS;
            mq.MSGUID__c = res.inputHeaders.msgGUID;
            mq.IFID__c = res.inputHeaders.ifID;
            mq.IFDate__c = res.inputHeaders.ifDate;   
        }         
        upsert mq;
        opp.Is_Pricing_Status_Change__c=false;
        upsert opp;
    }


    public static string webcallout(string body, string partialEndpoint){ 

    /*System.Httprequest req = new System.Httprequest();
    HttpResponse res = new HttpResponse();
    req.setMethod('POST');
    req.setBody(body);
    system.debug('## Flow 016 1 body'+body);
    
    req.setEndpoint('callout:X012_013_ROI'+partialEndpoint); //KR: Create partialEndPoint
    req.setTimeout(120000);
    req.setHeader('Content-Type', 'application/json');
    req.setHeader('Accept', 'application/json');

    Http http = new Http();     
    res = http.send(req);
    system.debug('## Flow 016 1 Response Body'+res.getBody());
    return res.getBody();    */

        return RoiIntegrationHelper.webcallout(body, partialEndpoint);
    }
    
    public static String formatDateField(Date d){
       DateTime tempD = DateTime.newInstance(d.year(), d.month(), d.day());
       String tempDTStr = tempD.format('yyyyMMddhhmmss'); 
       return tempDTStr; 
    }
    
    public static String formatDateTimeField(DateTime dt){
       String tempDTString = dt.format('yyyyMMddhhmmss'); 
       return tempDTString;        
    }

    /*public static String findStageName(String stageName){
        if(stageName.contains('Identification')){
            return 'Identified';
        }else if(stageName.contains('Qualification') || stageName.contains('Proposal')){
            return 'Qualified';
        }else if(stageName.contains('Commit')){
            return 'Negotiation';
        }
        else if(stageName.contains('Won')){
            return 'Win';
        }
        else if(stageName.contains('Drop')){
            return 'Drop';
        }else{
            //@TODO do i return rollout started, loss, or initial?????
        }
        return null;
    }*/

    
}