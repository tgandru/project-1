public class PRMProjectRegistrationController_ltng {
  
   public class questionnaire {
      @AuraEnabled public String Question {get;set;}
      @AuraEnabled public String Answer {get;set;}
       public questionnaire(string Question,string Answer){
           this.Question=Question;
           this.Answer=Answer;
       }
   }
   
    @AuraEnabled(cacheable=true)
    public static List<questionnaire> getquestionnaireInfo(String ObjName,String recordId){
      List<questionnaire> returnlist=new List<questionnaire>();
        String jsonstring='';
        
        if(ObjName=='Inquiry__c'){
           Inquiry__c obj =[Select id,Name,PRM_Project_Registration_Detail__c  from Inquiry__c where id=:recordId];
            jsonstring=obj.PRM_Project_Registration_Detail__c;
        }else if(ObjName=='Opportunity'){
               Opportunity obj =[Select id,Name,PRM_Project_Registration_Detail__c  from Opportunity where id=:recordId];
            jsonstring=obj.PRM_Project_Registration_Detail__c;
        }
        
        if(jsonstring !=null && jsonstring !=''){
           Map<String, Object> lst = (Map<String, Object>)JSON.deserializeUntyped(jsonstring);
	      for(String key : lst.keyset()){
                 returnlist.add(new questionnaire(key, (String)lst.get(key)) );
            }
        }
       return returnlist;
    }
   
}