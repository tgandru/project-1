@isTest
private class PRMProjectRegistrationControllerltngTEST {

	private static testMethod void test() {
	     Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Test.startTest();
         string   CampaignTypeId = [Select Id From RecordType Where SobjectType = 'Campaign' and Name = 'Sales'  limit 1].Id;
	    string   LeadTypeId = [Select Id From RecordType Where SobjectType = 'Lead' and Name = 'End User'  limit 1].Id;
       Campaign cmp= new Campaign(Name='Test Campaign for test class ',	IsActive=true,type='Email',StartDate=system.Today(),recordtypeid=CampaignTypeId);
       insert cmp;

       lead le1=new lead(lastname='test lead 123',Company='My Lead Company',Email='mylead@sds.com',	MQL_Score__c='T1',Status='New',LeadSource='Others',recordtypeid=LeadTypeId);
	   
	    insert le1;
	    Inquiry__c iq=new Inquiry__c(Lead__c = le1.id,Product_Solution_of_Interest__c='Monitors',	 campaign__c = cmp.id);

          iq.PRM_Project_Registration_Detail__c='{"Has formal RFP/RFQ/RFI or any formal request been published by the end-user?: ":"N","Will a formal RFP be published by the end-user within the next 30 days?: ":"Y","if yes, please provide estimated date: ":"05/31/19","if yes, will the RFP lead to a sole source award?: ":"Y"," When Samsung product meets spec, is your firm bidding Samsung exclusively?: ":"N","Does the End-User Point of Contact listed above have final purchasing decision authority?: ":"Y","Opportunity Name: ":"Decision Maker","Expected Close Date: ":"06/28/19","Please provide scope of product use as well as company division (i.e. Headquarters, Retail Division, et al) to which the units will be deployed: ":"HQ","Please describe how the partner has enhanced Samsungs position with the end user: ":"sell more","Is this Project for Government or Education or Hotel?: ":"Government","Pertaining to this project, does your firm hold a Samsung LOS for the Federal Government?, What type of DRM does the property intend on using?): ":"N","Pertaining to this project, does your firm hold a Samsung LOS for a Public Sector Consortium Contract? (EG NASPO, PEPPM): ":"N","Pertaining to this project, does your firm hold a Samsung LOS for a State Contract?: ":"N","Has a the End-User purchased the Samsung product(s) or received an evaluation or seed unit?: ":"N","if yes, when? (approx. date): ":"","What is the total minimum estimated value of Samsung product for this project?: ":"","What is the projects rollout? (estimated start dates): ":"06/01/19","What is the projects rollout? (estimated end ship dates): ":"05/27/22","Desktop Monitor: ":"$0 - $24,999","Signage Display: ":"","Galaxy Tab: ":"","Chromebook: ":"$125,000 - $149,999","Gear: ":"","HHP: ":"","SSD: ":"","HTV: ":"","On-Site Break Fix : ":"Y","Hardware Integration: ":"Y","Software Integration and/or development: ":"N","Samsung branded warranties- (Samsung Protection Plus): ":"Y","KNOX: ":"N","Samsung Business Services (SBS): ":"Y","Is your firm the incumbent vendor for the type of product being proposed on this project?: ":"Y","Are you trying to replace another vendors product with Samsung product?: ":"Y","Has a Samsung Business Field Sales/ Account Manager been introduced to the End-User POC named below?: ":"Y","If Yes, Who?: ":"James"}';

	   insert iq; 
	    
	    PRMProjectRegistrationController_ltng.getquestionnaireInfo('Inquiry__c',iq.Id);
	    
	    
	     Test.stopTest();     
	    
	}

}