/*
 * Description: This Apex Class will be used to create any business logic
 * for the Partners object. Please create the needed methods 
 * inside this utility class.
 *
 * Created By: Mayank S
 *
 * Created Date: Mar 02, 2016
 * 
 * Revisions: NIL
*/

public with sharing class PartnerUtilities{
    public static void afterInsert(List<Partner__c> newPartners) {
        Set<Id> oppIds = new Set<Id>();
        for(Partner__c par : newPartners) {
            oppIds.add(par.Opportunity__c);
        }
        
        List<Opportunity> linkedOpps = [Select Id, Has_Partner__c from Opportunity where Id in :oppIds And Has_Partner__c = false];
        if(linkedOpps.size()>0) {
            List<Opportunity> oppsToUpdate = new List<Opportunity>();
            for(Opportunity opp : linkedOpps) {
                opp.Has_Partner__c = true;
                oppsToUpdate.add(opp);
            }
            
            update oppsToUpdate;
        }
    }
    
    public static void afterUpdate(List<Partner__c> newPartners, Map<Id, Partner__c> oldPartners) {
        Set<Id> oldOppIds = new Set<Id>();
        Set<Id> newOppIds = new Set<Id>();
        for(Partner__c par : newPartners) {
            if(par.Opportunity__c != oldPartners.get(par.Id).Opportunity__c) {
                oldOppIds.add(oldPartners.get(par.Id).Opportunity__c);
                newOppIds.add(par.Opportunity__c);
            }
        }
        
        Map<Id, Opportunity> mappedOpps = new Map<Id, Opportunity>([Select Id, Has_Partner__c from Opportunity where Id In :oldOppIds Or Id In :newOppIds]);
        
        for(Id oppId : newOppIds) {
            if(!mappedOpps.get(oppId).Has_Partner__c) {
                mappedOpps.get(oppId).Has_Partner__c = true;
            }
        }
        
        for(Opportunity opp : mappedOpps.values()) {
            if(oldOppIds.contains(opp.Id) && !(newOppIds.contains(opp.Id))) {
                opp.Has_Partner__c = false;
            }
        }
        
        List<Partner__c> linkedPartners = [Select Id, Opportunity__c from Partner__c where Opportunity__c In :oldOppIds And Opportunity__c Not In :newOppIds];
        
        Set<Id> oppIds = new Set<Id>();
        for(Partner__c par : linkedPartners) {
            oppIds.add(par.Opportunity__c);
        }
        
        for(Opportunity opp : mappedOpps.values()) {
            if(oppIds.contains(opp.Id)) {
                opp.Has_Partner__c = true;
            }
        }
        
        update mappedOpps.values();
    }
    
    public static void afterDelete(List<Partner__c> oldPartners) {
        
        Set<Id> oldOppIds = new Set<Id>();
        
        for(Partner__c par : oldPartners) {
            oldOppIds.add(par.Opportunity__c);
        }
        
        List<Opportunity> linkedOpps = [Select Id, Has_Partner__c, (Select Id from Partners__r) from Opportunity where Id in :oldOppIds And Has_Partner__c = true];
        if(linkedOpps.size()>0) {
            List<Opportunity> oppsToUpdate = new List<Opportunity>();
        
            for(Opportunity opp : linkedOpps) {
                if(opp.Partners__r.size() == 0) {
                    opp.Has_Partner__c = false;
                    oppsToUpdate.add(opp);
                }
            }
            
            update oppsToUpdate;   
        }
    }
}