/*
 * Description: This test Class will be used for testing the methods inside
 * the PartnerUtilities.cls. Please create the needed methods 
 * inside this utility test class.
 *
 * Created By: Mayank S
 *
 * Created Date: Mar 02, 2016
 * 
   Modififed By: Aarthi R C
   Modified Date: Mar 24, 2016
   
 * Revisions: NIL
*/

@isTest
public class PartnerUtilitiesTest {
    public static testMethod void testPartnerCRUD() {
        
        //Channelinsight configuration
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        //system.debug('ChannelInsight configuration file is being called');
        
        //Due to validation rule, we have to assign the record type as 'End User' for opp type = Tender
        RecordType accountRecordType = [select Id from RecordType where sObjectType = 'Account' And DeveloperName = 'End_User'];
        system.debug('RecordTypeId = '+ accountRecordType );

        //Creating Accounts
        List<Account> accs = new List<Account>();
        for (Integer i = 0; i < 5; i++) {
            Account a = TestDataUtility.createAccount('Acc'+i);
            a.RecordTypeId =  accountRecordType.Id;
            accs.add(a);
        }
        insert accs;
        
        //Creating Opportunities
        List<Opportunity> oppList = new List<Opportunity>();
        
        for(Account acc : accs) {
            for (Integer i = 0; i <3; i++) {
                Opportunity opp = TestDataUtility.createOppty(acc.Name + ' - Opp'+i, acc, null, 'RFP', 'IT', 'FAX/MFP', 'New');
                oppList.add(opp);
            }
        }
        insert oppList;
        
        //creating Partner
        List<Partner__c> parList = new List<Partner__c>();
        Integer j=0;
        for(Opportunity opp : oppList) {
            for(Integer i=0; i<Math.mod(j,3); i++) {
                Partner__c par = new Partner__c();
                par.Partner__c = opp.AccountId;
                par.Opportunity__c = opp.Id;
                parList.add(par);  
            }
            j++;
        }
        
        Test.startTest();
        
        insert parList;
        
        Set<Id> oppIds = new Set<Id>();
        for(Opportunity opp : oppList) {
            oppIds.add(opp.Id);
        }
        
        Map<Id, Opportunity> updatedOppMap = new Map<Id, Opportunity>([Select Id, Has_Partner__c  from Opportunity where Id in :oppIds]);
        
        for(Opportunity opp : oppList) {
            opp.Has_Partner__c  = updatedOppMap.get(opp.Id).Has_Partner__c ;
        }
        
        for(Integer i=0; i<oppList.size(); i++) {
            if(Math.mod(i, 3) != 0) {
                System.assertEquals(true, oppList[i].Has_Partner__c );    
            } else {
                System.assertEquals(false, oppList[i].Has_Partner__c );
            }
        }
        
        for(Integer i=0; i<parList.size(); i=i+3) {
            
            if(i+1 < parList.size()) {
                Id tempId = parList[i].Opportunity__c;
                parList[i].Opportunity__c = parList[i+1].Opportunity__c;
                parList[i+1].Opportunity__c = tempId;   
            }
        }
        update parList;
        
        updatedOppMap = new Map<Id, Opportunity>([Select Id, Has_Partner__c  from Opportunity where Id in :oppIds]);
        
        for(Opportunity opp : oppList) {
            opp.Has_Partner__c  = updatedOppMap.get(opp.Id).Has_Partner__c ;
        }
        
        for(Integer i=0; i<oppList.size(); i++) {
            if(Math.mod(i, 3) != 0) {
                System.assertEquals(true, oppList[i].Has_Partner__c );    
            } else {
                System.assertEquals(false, oppList[i].Has_Partner__c );
            }
        }
        
        List<Partner__c> parDelete = new List<Partner__c>();
        Integer k=0;
        for(Partner__c par : parList) {
            if(Math.mod(k, 3) != 2)
                parDelete.add(par);
            k++;
        }
        delete parDelete;

        updatedOppMap = new Map<Id, Opportunity>([Select Id, Has_Partner__c  from Opportunity where Id in :oppIds]);
        
        for(Opportunity opp : oppList) {
            opp.Has_Partner__c  = updatedOppMap.get(opp.Id).Has_Partner__c ;
        }
        
        for(Integer i=0; i<oppList.size(); i++) {
            if(Math.mod(i, 3) == 2) {
                System.assertEquals(true, oppList[i].Has_Partner__c );    
            } else {
                System.assertEquals(false, oppList[i].Has_Partner__c );
            }
        }
        
        Test.stopTest();
    }
}