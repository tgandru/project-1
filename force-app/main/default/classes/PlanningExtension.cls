public class PlanningExtension {
    public list<selectOption> yearSP {get; set;}
    public list<selectOption> querters {get; set;}
    
    public string selectedYear {get; set;}
    public string selectedQuarter {get; set;}
    
    public list<Planing_and_Forecasting__c> planingsList {get; set;}
    public list<string> salesTeams;
    
    public list<selectOption> categories {get; set;}
    public string selectedcategory {get; set;}
    
    public boolean disableFilters{get; set;}
    private string recId;
    
    public planningExtension(apexPages.standardController sc){
        
        planingsList = new list<Planing_and_Forecasting__c>();
       
        salesTeams = new list<string>();
        for(Schema.PicklistEntry pe : Planing_and_Forecasting__c.Sales_Team1__c.getDescribe().getPicklistValues()){
            salesTeams.add(pe.getValue());
        }
        
        categories = new list<selectOption>();
        categories.add(new selectOption('','-None-'));
        for(Schema.PicklistEntry pe : Planing_and_Forecasting__c.Product_Category__c.getDescribe().getPicklistValues()){
            categories.add(new selectOption(pe.getValue(),pe.getValue()));
        }
        
        querters = new list<selectoption>();
        yearSP = new list<selectoption>();
        date tdy = system.today();
        Integer startYear = tdy.year()-5;
        Integer endYear = tdy.year()+5;
        
        for(integer i =startYear; i<= endYear;i++){
            yearSP.add(new selectOption(string.valueOf(i),string.valueOf(i)));
        }
        
        querters.add(new selectOption('1','Quarter 1'));
        querters.add(new selectOption('2','Quarter 2'));
        querters.add(new selectOption('3','Quarter 3'));
        querters.add(new selectOption('4','Quarter 4'));
        
        disableFilters = false;
      
            if(apexPages.currentPage().getParameters().get('id') != null){
              intialisationForExistingRecords();
            }else{
              intialisationForNewRecords();
            }
      
        
    }
    
    
    public void intialisationForExistingRecords(){
        
        Planing_and_Forecasting__c recInfo = new Planing_and_Forecasting__c();
        recId = apexPages.currentPage().getParameters().get('id');
        disableFilters = true;
        recInfo = [select Sales_Team1__c,Forecast_Amount_Co_Owner__c,Forecast_except_Won_and_Drop__c,Plan_Amount__c,Product_Category__c,year__c,Quarter__c,Total_Forecast_Amount__c from Planing_and_Forecasting__c where  id=:recId];
        selectedYear = string.valueOF(recInfo.year__c);
        selectedQuarter = string.valueOF(recInfo.Quarter__c);
        selectedcategory = recInfo.Product_Category__c;
        getExistingRecords();
     
    }
    
    public void intialisationForNewRecords(){
        selectedYear = string.valueOf(system.today().year());
        selectedQuarter = string.valueOf(1);
    }
    
    private Period prd = new period();
    public pageReference saveRecords(){
        try{
            upsert planingsList;
        }catch(exception e){
            apexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,e.getMessage()));
        }
        
        return null;
    }
    
    public pageReference prepareRecords(){
        integer Qurternumber = integer.valueOf(selectedQuarter);
        for(period p :[select startdate ,endDate,QuarterLabel,type,Number,FullyQualifiedLabel from period where  type='Quarter' and Number=:Qurternumber ]){
            if(p.FullyQualifiedLabel.contains(selectedYear)){
                prd = p;
                break;
            }
        }
          
        getExistingRecords();  
           
        return null;
    }
    
    
    public void getExistingRecords(){
       
        planingsList = new list<Planing_and_Forecasting__c>();  
       
        planingsList = [select Sales_Team1__c,Plan_Amount__c,Product_Category__c,	Forecast_Amount_Co_Owner__c,Total_Forecast_Amount__c,Forecast_except_Won_and_Drop__c from Planing_and_Forecasting__c where  year__c=:selectedYear and Quarter__c=:selectedQuarter and Product_Category__c=:selectedcategory order by Sales_Team1__c];
        
        if(planingsList.size() == 0){
            for(string s : salesTeams){
                
                Planing_and_Forecasting__c tmp = new Planing_and_Forecasting__c(Plan_Start_Date__c=prd.startdate,Plan_End_Date__c=prd.endDate,Sales_Team1__c = s,year__c=selectedYear,Quarter__c=selectedQuarter);
                   tmp.Account__c='0013600000Scmb9';
                 
                if(selectedcategory != null && selectedcategory != ''){
                    tmp.Product_Category__c = selectedcategory;
                }
                planingsList.add(tmp);
            } 
        }
        
    }

}