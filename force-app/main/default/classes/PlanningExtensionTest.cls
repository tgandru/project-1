@isTest
private class PlanningExtensionTest {
    public list<string> salesTeams;
    public string selectedYear {get; set;}
    public string selectedQuarter {get; set;}
    public list<selectOption> categories {get; set;}
    public list<Planing_and_Forecasting__c> planingsList {get; set;}
    
    private static testMethod void Inserttest() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        PageReference pf = page.PlanningsAdministration;
        test.setCurrentPage(pf);
        test.startTest();
        PlanningExtension obj = new PlanningExtension(new apexPages.standardController(new Planing_and_Forecasting__c() ));
        list<string> salesTeams = new list<string>();
        for(Schema.PicklistEntry pe : Planing_and_Forecasting__c.Sales_Team1__c.getDescribe().getPicklistValues()){
            salesTeams.add(pe.getValue());
        }  
        list<selectOption> categories = new list<selectOption>();
        categories.add(new selectOption('','-None-'));
        for(Schema.PicklistEntry pe : Planing_and_Forecasting__c.Product_Category__c.getDescribe().getPicklistValues()){
            categories.add(new selectOption(pe.getValue(),pe.getValue()));
        }
        Account acc1 = new account(Name ='Test plan  Account ', Type='Customer',SAP_Company_Code__c = 'A1A2A');
        insert acc1;
        list<Planing_and_Forecasting__c> pf1=new list<Planing_and_Forecasting__c>();
        Date closeDate = [Select EndDate From Period Where type = 'Quarter' and StartDate = THIS_FISCAL_QUARTER].EndDate;
        Date StartDate = [Select StartDate From Period Where type = 'Quarter' and StartDate = THIS_FISCAL_QUARTER].StartDate;
        integer  Month = Date.Today().Month();
        integer   quarter = ((Month / 3) + 1);
        string qt=String.valueOf(quarter);
        for(string s:salesTeams){
           Planing_and_Forecasting__c p=new Planing_and_Forecasting__c(Account__c=acc1.id,Plan_Start_Date__c=StartDate, Plan_End_Date__c=closeDate,Sales_Team1__c=s,Quarter__c=qt); 
            pf1.add(p);
        }
        obj.saveRecords();
        obj.getExistingRecords();
         obj.prepareRecords();
         //obj.intialisationForExistingRecords();
        test.stopTest();
    }
    private static testMethod void Inserttest12() {
        
        test.startTest();
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        PlanningExtension obj = new PlanningExtension(new apexPages.standardController(new Planing_and_Forecasting__c() ));
        list<string> salesTeams = new list<string>();
        for(Schema.PicklistEntry pe : Planing_and_Forecasting__c.Sales_Team1__c.getDescribe().getPicklistValues()){
            salesTeams.add(pe.getValue());
        }  
        list<selectOption> categories = new list<selectOption>();
        categories.add(new selectOption('','-None-'));
        for(Schema.PicklistEntry pe : Planing_and_Forecasting__c.Product_Category__c.getDescribe().getPicklistValues()){
            categories.add(new selectOption(pe.getValue(),pe.getValue()));
        }
        Account acc1 = new account(Name ='Test plan  Account ', Type='Customer',SAP_Company_Code__c = 'A1A2A');
        insert acc1;
        list<Planing_and_Forecasting__c> pf1=new list<Planing_and_Forecasting__c>();
        Date closeDate = [Select EndDate From Period Where type = 'Quarter' and StartDate = THIS_FISCAL_QUARTER].EndDate;
        Date StartDate = [Select StartDate From Period Where type = 'Quarter' and StartDate = THIS_FISCAL_QUARTER].StartDate;
        integer  Month = Date.Today().Month();
        integer   quarter = ((Month / 3) + 1);
        string qt=String.valueOf(quarter);
        for(string s:salesTeams){
           Planing_and_Forecasting__c p=new Planing_and_Forecasting__c(Account__c=acc1.id,Plan_Start_Date__c=StartDate, Plan_End_Date__c=closeDate,Sales_Team1__c=s,Quarter__c=qt); 
            pf1.add(p);
        }
        insert pf1;
        id p11=[select id from Planing_and_Forecasting__c limit 1].id;
        Planing_and_Forecasting__c pf12=[select id,name from Planing_and_Forecasting__c where id=:p11];
        ApexPages.CurrentPage().getParameters().put('id',p11);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(pf12);
        PageReference pf = page.PlanningsAdministration;
        
        PlanningExtension obj11 = new PlanningExtension(sc);
         obj11.intialisationForExistingRecords();
        test.stopTest();
    }


}