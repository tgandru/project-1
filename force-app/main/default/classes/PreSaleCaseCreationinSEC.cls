public class PreSaleCaseCreationinSEC {
   Public Opportunity op {get; set;}
   public string oppi {get; set;}
   public string finalurl2{get;set;}
    /*public PreSaleCaseCreationinSEC(ApexPages.StandardController controller) {
        String oppi= ApexPages.currentPage().getParameters().get('id');
        
        if(oppi!=null && oppi !=''){
           op =[select id,name,Amount,owner.name,owner.email,stagename,type,Accountid,Account.SAP_Company_Code__c,Roll_Out_Start__c,Roll_Out_End_Formula__c,CloseDate from Opportunity where id=:oppi];
      }
         
    }
    
    //Public PageReference redirecttoglobal(){
    Public void redirecttoglobal(){
     //String finalurl2='https://samsungsea--QA.cs65.my.salesforce.com/idp/login?app=0sp0m0000004CBO&RelayState=/apex/CaseCreation?source=SEA&opptyId=';
     finalurl2='';
     Boolean showerror=false;
     String oppid= ApexPages.currentPage().getParameters().get('id');
        if(oppid!=null && oppid !=''){
        Opportunity opp=[select id,name,Amount,owner.name,owner.email,stagename,type,Accountid,Account.Name,Account.SAP_Company_Code__c,Roll_Out_Start__c,Roll_Out_End_Formula__c,CloseDate from Opportunity where id=:oppid];
        string baseurl=Label.Global_Salesforce_Base_URL+'/apex/CaseCreation?';
        string cld= string.valueOf(opp.CloseDate);
        string amt=string.valueOf(opp.Amount);
        string rp='';
        if(opp.Roll_Out_Start__c !=null){
          rp=string.valueOf(opp.Roll_Out_Start__c)+' To  '+string.valueOf(opp.Roll_Out_End_Formula__c);
        }
        
          
        string parameters= 'source=SEA&opptyId='+opp.id+'&opptyName='+opp.Name+'&ciscode='+opp.Account.SAP_Company_Code__c+'&stage='+opp.StageName+'&ownerName='+opp.Owner.Name+'&ownerEmail='+opp.owner.Email+'&salesType='+opp.Type+'&acctName='+Opp.Account.Name+'&dealSize='+amt+'&monetaryUnit=USD'+'&rolloutPeriod='+rp+'&closeDate='+cld;
         string finalurl=baseurl+parameters;
         finalurl2=baseurl+EncodingUtil.urlEncode(parameters,'UTF-8');
          
          System.debug('finalurl    ::: '+finalurl2);   
         if(opp.stagename=='Drop'){
              ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'You are not alowed to create Case on Drop or Closed Opportunity.'));
              
              showerror=true;
           } 
          
           
    }
    if(showerror==true){
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'You are not alowed to create Case on Drop or Closed Opportunity.'));
    
        //return null;
        
    }else{
        //return new PageReference(finalurl2);
        //return null;
    }
    

    
    }*/

    public PreSaleCaseCreationinSEC(ApexPages.StandardController controller) {
      finalurl2='';
     Boolean showerror=false;
     String oppid= ApexPages.currentPage().getParameters().get('id');
        if(oppid!=null && oppid !=''){
        Opportunity opp=[select id,name,Amount,owner.name,owner.email,stagename,type,Accountid,Account.Name,Account.SAP_Company_Code__c,Roll_Out_Start__c,Roll_Out_End_Formula__c,CloseDate from Opportunity where id=:oppid];
        string baseurl=Label.Global_Salesforce_Base_URL+'/apex/CaseCreation?';
        string cld= string.valueOf(opp.CloseDate);
        string amt=string.valueOf(opp.Amount);
        string rp='';
        if(opp.Roll_Out_Start__c !=null){
          rp=string.valueOf(opp.Roll_Out_Start__c)+' To  '+string.valueOf(opp.Roll_Out_End_Formula__c);
        }
        
          
        string parameters= 'source=SEA&opptyId='+opp.id+'&opptyName='+opp.Name+'&ciscode='+opp.Account.SAP_Company_Code__c+'&stage='+opp.StageName+'&ownerName='+opp.Owner.Name+'&ownerEmail='+opp.owner.Email+'&salesType='+opp.Type+'&acctName='+Opp.Account.Name+'&dealSize='+amt+'&monetaryUnit=USD'+'&rolloutPeriod='+rp+'&closeDate='+cld;
         string finalurl=baseurl+parameters;
         finalurl2=baseurl+EncodingUtil.urlEncode(parameters,'UTF-8');
          
          System.debug('finalurl    ::: '+finalurl2);   
         if(opp.stagename=='Drop'){
              ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'You are not alowed to create Case on Drop or Closed Opportunity.'));
              
              showerror=true;
           } 
          
           
    }
    if(showerror==true){
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'You are not alowed to create Case on Drop or Closed Opportunity.'));
    
        //return null;
        
    }else{
        //return new PageReference(finalurl2);
        //return null;
    }
         
    }
}