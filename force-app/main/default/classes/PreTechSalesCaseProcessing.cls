/*
This Rest call is to create Pre Sales Case- Custom Object Record  from Global Salesforce
Created by - Vijay 3/15
SEC will call this webservice once new Case is created in Global salesforce. 

*/

@RestResource(urlMapping='/PreTechSalesCaseProcessing/*')
global class PreTechSalesCaseProcessing {


   @HttpPost
  global static  SFDCstubSECSFDC.CaseCreationResponse createpresalescase(){
        RestRequest req = RestContext.request;
        String postBody = req.requestBody.toString();
        system.debug('Request Body ::'+postBody);
        message_queue__c mq=new message_queue__c();
       mq.Integration_Flow_Type__c='IF_SFDC_SVC_077';
         SFDCstubSECSFDC.CaseCreationResponse response = new SFDCstubSECSFDC.CaseCreationResponse();
         SFDCstubSECSFDC.CaseCreationRequest request = null;
        try{
            request = (SFDCstubSECSFDC.CaseCreationRequest)json.deserialize(postBody, SFDCstubSECSFDC.CaseCreationRequest.class);
        }catch(Exception e) {
            response.inputHeaders.MSGGUID = '';
            response.inputHeaders.IFID  = '';
            response.inputHeaders.IFDate    = '';
            response.inputHeaders.MSGSTATUS = 'F';
            response.inputHeaders.ERRORTEXT = 'Invalid Request Message. - '+postBody;
            response.inputHeaders.ERRORCODE = '';
             string F='F';
            string er='Invalid request Message';
            insertmessagequeue(F,postBody,er,mq);
            return response;
        }
        
        response.inputHeaders.MSGGUID = request.inputHeaders.MSGGUID;
        response.inputHeaders.IFID    = request.inputHeaders.IFID;
        response.inputHeaders.IFDate  = request.inputHeaders.IFDate;
        mq.MSGGUID__c=request.inputHeaders.MSGGUID;
        mq.IFID__c=request.inputHeaders.IFID;
        mq.IFDate__c=request.inputHeaders.IFDate;
        
        if(request.body.extOpportunityId!=null && request.body.extOpportunityId!=''){//Check we are getting the Opportunity ID or Not. Opportunity id is Required 
            try{
                String casenum=request.body.caseNumber;
                List<SEC_Case__c> upsertlist=new list<SEC_Case__c>();
                  List<SEC_Case__c> seccs=new list<SEC_Case__c>([select id,name,Opportunity__c,Case_Number__c,Case_Subject__c,Case_URL__c,Product_Category__c,Status__c from SEC_Case__c where Case_Number__c =:casenum limit 1]);
                  if(seccs.size()>0){
                      for(SEC_Case__c s:seccs){
                         s.Opportunity__c=request.body.extOpportunityId;
                         s.Case_Number__c=request.body.caseNumber;
                         s.Case_Subject__c=request.body.caseSubject;
                         s.Case_URL__c=request.body.caseUrl;
                         s.Case_URL_Link__c=Label.Global_Salesforce_Base_URL+request.body.caseUrl.right(18);
                         s.Product_Category__c=request.body.caseProductCategory;
                         s.Status__c=request.body.caseStatus;
                         s.Sub_Request_Type__c=request.body.subRequestType;
                         upsertlist.add(s);
                      }
                  }else{
                         SEC_Case__c cs=new SEC_Case__c();
                        cs.Opportunity__c=request.body.extOpportunityId;
                        cs.Case_Number__c=request.body.caseNumber;
                        cs.Case_Subject__c=request.body.caseSubject;
                        cs.Case_URL__c=request.body.caseUrl;
                        cs.Case_URL_Link__c=Label.Global_Salesforce_Base_URL+request.body.caseUrl.right(18);
                        cs.Product_Category__c=request.body.caseProductCategory;
                        cs.Status__c=request.body.caseStatus;
                        cs.Sub_Request_Type__c=request.body.subRequestType;
                        upsertlist.add(cs);
            
                  }
                upsert upsertlist;
                
                
            }catch(Exception e) {
                response.inputHeaders.MSGSTATUS = 'F';
                response.inputHeaders.ERRORTEXT = 'ERROR  - '+e;
                response.inputHeaders.ERRORCODE = '';
            
               return response;     
                
            }
            
            response.inputHeaders.MSGSTATUS = 'S';
            response.inputHeaders.ERRORTEXT = '';
            response.inputHeaders.ERRORCODE = '';
             string s='S';
              insertmessagequeue(s,postBody,'',mq);
            
            
        }else{
            
            response.inputHeaders.MSGSTATUS = 'F';
            response.inputHeaders.ERRORTEXT = 'extOpportunityId is Missing ';
            response.inputHeaders.ERRORCODE = '';
             string F='F';
             string err='Opportuinyt ID is Missing';
            insertmessagequeue(F,postBody,err,mq);
            
        }
        
        
      return response;
  } 

   public static void insertmessagequeue(string status,string req,string err,message_queue__c mq){
         
          mq.MSGSTATUS__c=status;
          mq.ERRORTEXT__c=req;
          if(status=='F'){
           mq.Status__c='failed';   
          }else{
                mq.Status__c='success';   
          }
          mq.ERRORCODE__c=err;
        insert mq;
    }


}