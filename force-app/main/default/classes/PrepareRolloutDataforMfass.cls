global class PrepareRolloutDataforMfass implements Database.Batchable<sObject>,database.stateful,Schedulable{
    
     global void execute(SchedulableContext SC) {
        
   }
    global string headers;
    global string southeastbody;
    global string northeastbody;
    global string centralbody;
    global string westbody;
    global string region;
    global date frmdt;
    global PrepareRolloutDataforMfass(string region){
        headers='Opportunity NO^Region^AM^Customer^Builder^Project Name^Status^Type of Project^Living Units^Model Number^Project State^Options Base_Upgrade^Start month^Last Month^Roll Out Percent^Sales Portal BO Number^Quote Quantity^Total Plan Quantity^Plan Quantity^Actual Quantity^year^Rollout Month^Rollout Week^Actual Revenue^Plan Revenue';
        centralbody='';
        northeastbody='';
        westbody='';
        southeastbody='';
        frmdt=system.today().addDays(-2);
        this.region=region;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        String query='Select id,YearMonth__c,Plan_Revenue__c,Actual_Revenue__c,Plan_Quantity__c,year__c,fiscal_week__c,Plan_Date__c,fiscal_month__c,Actual_Quantity__c';
         query=query+',Roll_Out_Product__r.SAP_Material_Code__c,Roll_Out_Product__r.Plan_Quantity__c,Roll_Out_Product__r.Quote_Quantity__c,Roll_Out_Product__r.Roll_Out_Percent__c,Roll_Out_Product__r.Package_Option__c';
         query=query+' ,Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.StageName,Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Project_Name__c,Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Project_State__c,Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.HA_Shipping_Plant__c ';
         query=query+'  ,Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Project_Type__c,Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Roll_Out_End_Formula__c,Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Roll_Out_Start__c,Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Number_of_Living_Units__c';
         query=query+'   ,Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Opportunity_Number__c,Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Owner.Name,Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Owner.userRole.Name';  
         query=query+'    ,Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Account.name,Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Builder__r.Name, Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Sales_Portal_BO_Number__c';
         query=query+'     From Roll_Out_Schedule__c where Roll_Out_Product__r.Opportunity_RecordType_Name__c=\'HA Builder\' AND (LastModifiedDate>=:frmdt )  AND  Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.owner.userRole.Name LIKE ';
         query=query+'\'%'+region+'%\'';
         query=query+'       Order By Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Opportunity_Number__c';
         system.debug('query'+query);
         return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Roll_Out_Schedule__c> scope){
        
        for(Roll_Out_Schedule__c r:scope){
              date strt=r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Roll_Out_Start__c;
              date endt=r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Roll_Out_End_Formula__c;
               String enddt=String.valueof(endt.Year())+'-'+String.valueof(endt.Month())+'-'+String.valueof(endt.day());
               String strtdt=String.valueof(strt.Year())+'-'+String.valueof(strt.Month())+'-'+String.valueof(strt.day());
             
              String role= r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Owner.userrole.Name;
              
              
              
               
            if(role.contains('Central')){
                 String line='\n'+r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Opportunity_Number__c+'^ CENTRAL'+'^'+r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Owner.Name;
              line=line+'^'+r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Account.Name+'^'+r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Builder__r.Name;
              line=line+'^'+r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Project_Name__c+'^'+r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.StageName+'^'+r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Project_Type__c;
              line=line+'^'+r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Number_of_Living_Units__c+'^'+r.Roll_Out_Product__r.SAP_Material_Code__c+'^'+r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Project_State__c;
              line=line+'^'+r.Roll_Out_Product__r.Package_Option__c+'^'+strtdt+'^'+enddt;
              line=line+'^'+r.Roll_Out_Product__r.Roll_Out_Percent__c+'^'+r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Sales_Portal_BO_Number__c+'^'+r.Roll_Out_Product__r.Quote_Quantity__c+'^'+r.Roll_Out_Product__r.Plan_Quantity__c;
              line=line+'^'+r.Plan_Quantity__c+'^'+r.Actual_Quantity__c+'^'+r.year__c+'^'+r.fiscal_month__c+'^'+r.fiscal_week__c+'^'+r.Actual_Revenue__c+'^'+r.Plan_Revenue__c;
               line=line;
                centralbody=centralbody+line;
            }else if(role.contains('North')){
                String line='\n'+r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Opportunity_Number__c+'^ NORTHEAST'+'^'+r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Owner.Name;
              line=line+'^'+r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Account.Name+'^'+r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Builder__r.Name;
              line=line+'^'+r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Project_Name__c+'^'+r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.StageName+'^'+r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Project_Type__c;
              line=line+'^'+r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Number_of_Living_Units__c+'^'+r.Roll_Out_Product__r.SAP_Material_Code__c+'^'+r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Project_State__c;
              line=line+'^'+r.Roll_Out_Product__r.Package_Option__c+'^'+strtdt+'^'+enddt;
              line=line+'^'+r.Roll_Out_Product__r.Roll_Out_Percent__c+'^'+r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Sales_Portal_BO_Number__c+'^'+r.Roll_Out_Product__r.Quote_Quantity__c+'^'+r.Roll_Out_Product__r.Plan_Quantity__c;
              line=line+'^'+r.Plan_Quantity__c+'^'+r.Actual_Quantity__c+'^'+r.year__c+'^'+r.fiscal_month__c+'^'+r.fiscal_week__c+'^'+r.Actual_Revenue__c+'^'+r.Plan_Revenue__c;
               line=line;
                northeastbody=northeastbody+line;
            }else if(role.contains('South')){
                String line='\n'+r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Opportunity_Number__c+'^ SOUTHEAST'+'^'+r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Owner.Name;
              line=line+'^'+r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Account.Name+'^'+r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Builder__r.Name;
              line=line+'^'+r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Project_Name__c+'^'+r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.StageName+'^'+r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Project_Type__c;
              line=line+'^'+r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Number_of_Living_Units__c+'^'+r.Roll_Out_Product__r.SAP_Material_Code__c+'^'+r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Project_State__c;
              line=line+'^'+r.Roll_Out_Product__r.Package_Option__c+'^'+strtdt+'^'+enddt;
              line=line+'^'+r.Roll_Out_Product__r.Roll_Out_Percent__c+'^'+r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Sales_Portal_BO_Number__c+'^'+r.Roll_Out_Product__r.Quote_Quantity__c+'^'+r.Roll_Out_Product__r.Plan_Quantity__c;
              line=line+'^'+r.Plan_Quantity__c+'^'+r.Actual_Quantity__c+'^'+r.year__c+'^'+r.fiscal_month__c+'^'+r.fiscal_week__c+'^'+r.Actual_Revenue__c+'^'+r.Plan_Revenue__c;
               line=line;
                southeastbody=southeastbody+line;
            }else{
                String line='\n'+r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Opportunity_Number__c+'^ WEST'+'^'+r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Owner.Name;
              line=line+'^'+r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Account.Name+'^'+r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Builder__r.Name;
              line=line+'^'+r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Project_Name__c+'^'+r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.StageName+'^'+r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Project_Type__c;
              line=line+'^'+r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Number_of_Living_Units__c+'^'+r.Roll_Out_Product__r.SAP_Material_Code__c+'^'+r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Project_State__c;
              line=line+'^'+r.Roll_Out_Product__r.Package_Option__c+'^'+strtdt+'^'+enddt;
              line=line+'^'+r.Roll_Out_Product__r.Roll_Out_Percent__c+'^'+r.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Sales_Portal_BO_Number__c+'^'+r.Roll_Out_Product__r.Quote_Quantity__c+'^'+r.Roll_Out_Product__r.Plan_Quantity__c;
              line=line+'^'+r.Plan_Quantity__c+'^'+r.Actual_Quantity__c+'^'+r.year__c+'^'+r.fiscal_month__c+'^'+r.fiscal_week__c+'^'+r.Actual_Revenue__c+'^'+r.Plan_Revenue__c;
               line=line;
                westbody=westbody+line;
            }
        }
    }
    
    global void finish(Database.BatchableContext BC){
        
        message_queue__c mq=new message_queue__c(Status__c='Hold',Integration_Flow_Type__c='Flow-040');
         insert mq;
        
         DateTime pr=system.now();
         string dt=String.valueof(pr.Month())+String.valueof(pr.day())+String.valueof(pr.Year());
        System.debug('Central body'+centralbody);
         List<Attachment> attchlst=new List<Attachment>();
           
           
           if(region=='Central'){
                 String attname='SEA_HA_OPP_'+dt+'_CENTRAL.CSV';
                centralbody=headers+centralbody;
                  Attachment att=new Attachment();
                         att.ParentId=mq.id;
                         att.Name=attname;
                         att.ContentType='application/vnd.ms-excel';
                         att.body=blob.valueOf(centralbody);
                         attchlst.add(att);
           }
              
          if(region=='North'){
               String attname2='SEA_HA_OPP_'+dt+'_NORTHEAST.CSV';
                northeastbody=headers+northeastbody;
                  Attachment att2=new Attachment();
                         att2.ParentId=mq.id;
                         att2.Name=attname2;
                         att2.ContentType='application/vnd.ms-excel';
                         att2.body=blob.valueOf(northeastbody);
                         attchlst.add(att2);
         
              
          }
                if(region=='South'){
                String attname3='SEA_HA_OPP_'+dt+'_SOUTHEAST.CSV';
                southeastbody=headers+southeastbody;
                  Attachment att3=new Attachment();
                         att3.ParentId=mq.id;
                         att3.Name=attname3;
                         att3.ContentType='application/vnd.ms-excel';
                         att3.body=blob.valueOf(southeastbody);
                       attchlst.add(att3);
                }
                 if(region=='West'){
                String attname4='SEA_HA_OPP_'+dt+'_WEST.CSV';
                westbody=headers+westbody;
                  Attachment att4=new Attachment();
                         att4.ParentId=mq.id;
                         att4.Name=attname4;
                         att4.ContentType='application/vnd.ms-excel';
                         att4.body=blob.valueOf(westbody);
                         attchlst.add(att4);
                 }
                      
            if(attchlst.size()>0){
                insert attchlst;
                
            }              
        
        mq.Status__c='not started';
        mq.Object_Name__c='HA Rollout';
        Update mq;
        
        
   }
}