global class PrepareRolloutDataforMfassScheduler implements Schedulable{

	global void execute(SchedulableContext sc) {
		integer scheduleInterval = 5;
		
		try{
            database.executeBatch(new PrepareRolloutDataforMfass('North'));
            database.executeBatch(new PrepareRolloutDataforMfass('South'));
            database.executeBatch(new PrepareRolloutDataforMfass('Central'));
            database.executeBatch(new PrepareRolloutDataforMfass('West'));
		}
		catch(Exception ex){
			System.debug('Scheduled Job failed : ' + ex.getMessage());
			//Next part of the scheduler needs to run regardless of what happens in the logic above.			
		}


	 Integer rqmi = 24;
    DateTime timenow = DateTime.now().addHours(rqmi);
    PrepareRolloutDataforMfassScheduler rqm = new PrepareRolloutDataforMfassScheduler();
    String seconds = '0';
    String minutes = String.valueOf(timenow.minute());
    String hours = String.valueOf(timenow.hour());
   	String sch = seconds + ' ' + minutes + ' ' + hours + ' * * ?';
		String jobName = 'PrepareRolloutDataforMfass - ' +timenow;
		system.schedule(jobName, sch, rqm);
		
		if (sc != null)	{	
			system.abortJob(sc.getTriggerId());			
		}
	}
}