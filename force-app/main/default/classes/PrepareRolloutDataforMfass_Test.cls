@isTest
private class PrepareRolloutDataforMfass_Test {


	 @isTest  static void CreateTest() {
        profile p = [select id, Name from profile where name= 'System Administrator' limit 1];
           UserRole rl=[Select id from userRole where name='RSM - North' limit 1];
      
        
        User u1 = new User(Alias = 'standt', Email='mycaseTriggerTestwww@ss.com', 
                  EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                  LocaleSidKey='en_US', ProfileId = p.Id, userRoleId=rl.id,
                  TimeZoneSidKey='America/Los_Angeles', UserName='abc11234@abc.com');
          
          insert u1;
          System.runAs(u1){
           Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
       Test.startTest();
		Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        Account testAccount1 = new Account (
        	Name = 'TestAcc1',
            RecordTypeId = rtMap.get('Account' + 'HA_Builder'),
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        
        );
        insert testAccount1;

        Opportunity testOpp1 = new Opportunity (
        	Name = 'TestOpp1',
        	RecordTypeId = rtMap.get('Opportunity' + 'HA_Builder'),
        	Project_Type__c = 'SFNC',
        	Project_Name__c = 'TestOpp1',
        	AccountId = testAccount1.Id,
        	StageName = 'Identified',
        	CloseDate = Date.today(),
        	Roll_Out_Start__c = Date.newInstance(2017, 1, 2),
        	Number_of_Living_Units__c = 10000,
        	Rollout_Duration__c = 5

        );
        insert testOpp1;

      

       Product2 standaloneProd=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Standalone Prod1', 'DISH', 'AEE1A', 'H/W');
       insert standaloneProd;

        Roll_Out__c testRo = new Roll_Out__c (
        	Opportunity__c = testOpp1.Id,
        	Opportunity_RecordType_Name__c = 'HA Builder',
        	Roll_Out_Start__c = Date.newInstance(2017, 1, 2)
        
        );
        insert testRo;

        Roll_Out_Product__c testRop = new Roll_Out_Product__c (
        	Name = 'testProduct',
        	Roll_Out_Plan__c = testRo.Id,
        	Opportunity_RecordType_Name__c = 'HA Builder',
        	Product__c = standaloneProd.Id,
        	Roll_Out_Start__c = Date.newInstance(2017, 1, 2),
        	Rollout_Duration__c = 5,
        	Roll_Out_Percent__c = 40,
        	Package_Option__c = 'Base',
        	Quote_Quantity__c = 50

        );
        insert testRop;

        Roll_Out_Schedule__c ros=new Roll_Out_Schedule__c(
            Plan_Date__c=Date.newInstance(2017, 1, 2),
            Roll_Out_Product__c=testrop.id,
            Plan_Quantity__c=15,
            year__c='2017',
            fiscal_week__c='02'
            
            );
            
            insert ros;
        PrepareRolloutDataforMfass ba = new PrepareRolloutDataforMfass('North');
         Database.executeBatch(ba);
      Test.stopTest();
	}
	 }
	@isTest  static void CreateTest2() {
         profile p = [select id, Name from profile where name= 'System Administrator' limit 1];
         UserRole rl=[Select id from userRole where name='RSM - Central' limit 1];
      
        
        User u1 = new User(Alias = 'standt', Email='mycaseTriggerTestwww@ss.com', 
                  EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                  LocaleSidKey='en_US', ProfileId = p.Id, userRoleId=rl.id,
                  TimeZoneSidKey='America/Los_Angeles', UserName='abc11234@abc.com');
          
          insert u1;
          
           System.runAs(u1){
       Test.startTest();
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
		Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        Account testAccount1 = new Account (
        	Name = 'TestAcc1',
            RecordTypeId = rtMap.get('Account' + 'HA_Builder'),
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        
        );
        insert testAccount1;

        Opportunity testOpp1 = new Opportunity (
        	Name = 'TestOpp1',
        	RecordTypeId = rtMap.get('Opportunity' + 'HA_Builder'),
        	Project_Type__c = 'SFNC',
        	Project_Name__c = 'TestOpp1',
        	AccountId = testAccount1.Id,
        	StageName = 'Identified',
        	CloseDate = Date.today(),
        	Roll_Out_Start__c = Date.newInstance(2017, 1, 2),
        	Number_of_Living_Units__c = 10000,
        	Rollout_Duration__c = 5

        );
        insert testOpp1;

      

       Product2 standaloneProd=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Standalone Prod1', 'DISH', 'AEE1A', 'H/W');
       insert standaloneProd;

        Roll_Out__c testRo = new Roll_Out__c (
        	Opportunity__c = testOpp1.Id,
        	Opportunity_RecordType_Name__c = 'HA Builder',
        	Roll_Out_Start__c = Date.newInstance(2017, 1, 2)
        
        );
        insert testRo;

        Roll_Out_Product__c testRop = new Roll_Out_Product__c (
        	Name = 'testProduct',
        	Roll_Out_Plan__c = testRo.Id,
        	Opportunity_RecordType_Name__c = 'HA Builder',
        	Product__c = standaloneProd.Id,
        	Roll_Out_Start__c = Date.newInstance(2017, 1, 2),
        	Rollout_Duration__c = 5,
        	Roll_Out_Percent__c = 40,
        	Package_Option__c = 'Base',
        	Quote_Quantity__c = 50

        );
        insert testRop;

        Roll_Out_Schedule__c ros=new Roll_Out_Schedule__c(
            Plan_Date__c=Date.newInstance(2017, 1, 2),
            Roll_Out_Product__c=testrop.id,
            Plan_Quantity__c=15,
            year__c='2017',
            fiscal_week__c='02'
            
            );
            
            insert ros;
        PrepareRolloutDataforMfass ba = new PrepareRolloutDataforMfass('Central');
         Database.executeBatch(ba);
         
         
      Test.stopTest();
	}
	    
	}
	
	
	 @isTest  static void CreateTest3() {
        profile p = [select id, Name from profile where name= 'System Administrator' limit 1];
           UserRole rl=[Select id from userRole where name='RSM - West' limit 1];
      
        
        User u1 = new User(Alias = 'standt', Email='mycaseTriggerTestwww@ss.com', 
                  EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                  LocaleSidKey='en_US', ProfileId = p.Id, userRoleId=rl.id,
                  TimeZoneSidKey='America/Los_Angeles', UserName='abc11234@abc.com');
          
          insert u1;
          System.runAs(u1){
           Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
       Test.startTest();
		Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        Account testAccount1 = new Account (
        	Name = 'TestAcc1',
            RecordTypeId = rtMap.get('Account' + 'HA_Builder'),
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        
        );
        insert testAccount1;

        Opportunity testOpp1 = new Opportunity (
        	Name = 'TestOpp1',
        	RecordTypeId = rtMap.get('Opportunity' + 'HA_Builder'),
        	Project_Type__c = 'SFNC',
        	Project_Name__c = 'TestOpp1',
        	AccountId = testAccount1.Id,
        	StageName = 'Identified',
        	CloseDate = Date.today(),
        	Roll_Out_Start__c = Date.newInstance(2017, 1, 2),
        	Number_of_Living_Units__c = 10000,
        	Rollout_Duration__c = 5

        );
        insert testOpp1;

      

       Product2 standaloneProd=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Standalone Prod1', 'DISH', 'AEE1A', 'H/W');
       insert standaloneProd;

        Roll_Out__c testRo = new Roll_Out__c (
        	Opportunity__c = testOpp1.Id,
        	Opportunity_RecordType_Name__c = 'HA Builder',
        	Roll_Out_Start__c = Date.newInstance(2017, 1, 2)
        
        );
        insert testRo;

        Roll_Out_Product__c testRop = new Roll_Out_Product__c (
        	Name = 'testProduct',
        	Roll_Out_Plan__c = testRo.Id,
        	Opportunity_RecordType_Name__c = 'HA Builder',
        	Product__c = standaloneProd.Id,
        	Roll_Out_Start__c = Date.newInstance(2017, 1, 2),
        	Rollout_Duration__c = 5,
        	Roll_Out_Percent__c = 40,
        	Package_Option__c = 'Base',
        	Quote_Quantity__c = 50

        );
        insert testRop;

        Roll_Out_Schedule__c ros=new Roll_Out_Schedule__c(
            Plan_Date__c=Date.newInstance(2017, 1, 2),
            Roll_Out_Product__c=testrop.id,
            Plan_Quantity__c=15,
            year__c='2017',
            fiscal_week__c='02'
            
            );
            
            insert ros;
        PrepareRolloutDataforMfass ba = new PrepareRolloutDataforMfass('West');
         Database.executeBatch(ba);
      Test.stopTest();
	}
	 }
}