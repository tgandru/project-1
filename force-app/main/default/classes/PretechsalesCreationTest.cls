@isTest(seealldata=true)
private class PretechsalesCreationTest {

	private static testMethod void testmethod1() {
	    Opportunity opp=[select id,name from Opportunity limit 1];
	    String extoppid=opp.id;
	   SFDCstubSECSFDC.CaseCreationRequest reqData = new SFDCstubSECSFDC.CaseCreationRequest();
	   reqData.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData.inputHeaders.IFDate    = '20180319';
        reqData.body.extOpportunityId = extoppid;
        reqData.body.caseNumber = '00553388';
	    reqData.body.caseSubject = 'Test Class Test Method 1';
        reqData.body.caseProductCategory = 'Mobile';
        reqData.body.caseStatus = 'New';
        reqData.body.caseUrl = 'https://test.salesforce.com/5000m00000310f4AAA';
        
         String jsonMsg = JSON.serialize(reqData);
         Test.startTest();
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/services/apexrest/PreTechSalesCaseProcessing';  //Request URL
        req.httpMethod = 'POST';//HTTP Request Type
        req.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req;
        RestContext.response= res;
        
        SFDCstubSECSFDC.CaseCreationResponse resData = PreTechSalesCaseProcessing.createpresalescase();
        System.debug(resData.inputHeaders.MSGSTATUS);
        System.debug(resData.inputHeaders.ERRORTEXT);
        System.assertEquals('S', resData.inputHeaders.MSGSTATUS);
        
          
         Test.stopTest();
         
  	}
  	
  	private static testMethod void testmethod2() {
	    Opportunity opp=[select id,name from Opportunity limit 1];
	    String extoppid=opp.id;
	   SFDCstubSECSFDC.CaseCreationRequest reqData = new SFDCstubSECSFDC.CaseCreationRequest();
	   reqData.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData.inputHeaders.IFDate    = '20180319';
        reqData.body.extOpportunityId = '';
        reqData.body.caseNumber = '00553388';
	    reqData.body.caseSubject = 'Test Class Test Method 1';
        reqData.body.caseProductCategory = 'Mobile';
        reqData.body.caseStatus = 'New';
        
         String jsonMsg = JSON.serialize(reqData);
         Test.startTest();
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/services/apexrest/PreTechSalesCaseProcessing';  //Request URL
        req.httpMethod = 'POST';//HTTP Request Type
        req.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req;
        RestContext.response= res;
        
        SFDCstubSECSFDC.CaseCreationResponse resData = PreTechSalesCaseProcessing.createpresalescase();
        System.debug(resData.inputHeaders.MSGSTATUS);
        System.debug(resData.inputHeaders.ERRORTEXT);
        System.assertEquals('F', resData.inputHeaders.MSGSTATUS);
        
          
         Test.stopTest();
         
  	}


  	private static testMethod void testmethod3() {
	    Opportunity opp=[select id,name from Opportunity limit 1];
	    String extoppid=opp.id;
	   SFDCstubSECSFDC.CaseCreationRequest reqData = new SFDCstubSECSFDC.CaseCreationRequest();
	   reqData.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData.inputHeaders.IFDate    = '20180319';
        reqData.body.extOpportunityId = '001325954236';
        reqData.body.caseNumber = '00553388';
	    reqData.body.caseSubject = 'Test Class Test Method 1';
        reqData.body.caseProductCategory = 'Mobile';
        reqData.body.caseStatus = 'New';
        
         String jsonMsg = JSON.serialize(reqData);
         Test.startTest();
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/services/apexrest/PreTechSalesCaseProcessing';  //Request URL
        req.httpMethod = 'POST';//HTTP Request Type
        req.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req;
        RestContext.response= res;
        
        SFDCstubSECSFDC.CaseCreationResponse resData = PreTechSalesCaseProcessing.createpresalescase();
        System.debug(resData.inputHeaders.MSGSTATUS);
        System.debug(resData.inputHeaders.ERRORTEXT);
        System.assertEquals('F', resData.inputHeaders.MSGSTATUS);
        
          
         Test.stopTest();
         
  	}
  	
  	
  	 	private static testMethod void testmethod4() {
	    Opportunity opp=[select id,name from Opportunity limit 1];
	    String extoppid=opp.id;
	   SFDCstubSECSFDC.CaseCreationRequest reqData = new SFDCstubSECSFDC.CaseCreationRequest();
	   reqData.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData.inputHeaders.IFDate    = '20180319';
        reqData.body.extOpportunityId = extoppid;
        reqData.body.caseNumber = '00553388';
	    reqData.body.caseSubject = 'Test Class Test Method 1';
        reqData.body.caseProductCategory = 'Mobile';
        reqData.body.caseStatus = 'New';
        
         String jsonMsg = JSON.serialize(reqData);
         Test.startTest();
        String jsonMsg1 ='';
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/services/apexrest/PreTechSalesCaseProcessing';  //Request URL
        req.httpMethod = 'POST';//HTTP Request Type
        req.requestBody = Blob.valueof(jsonMsg1);
        RestContext.request = req;
        RestContext.response= res;
        
        SFDCstubSECSFDC.CaseCreationResponse resData = PreTechSalesCaseProcessing.createpresalescase();
        System.debug(resData.inputHeaders.MSGSTATUS);
        System.debug(resData.inputHeaders.ERRORTEXT);
        System.assertEquals('F', resData.inputHeaders.MSGSTATUS);
        
          
         Test.stopTest();
         
  	}
  	
  	
}