global class PriceBookEntrySchedular implements Schedulable {
    global void execute(SchedulableContext sc) {
    	
    	//integer scheduleIntervalHrs = 23;

   		try
		{
			list<message_queue__c> queueRecordsToInsert = new list<message_queue__c>();
		
			queueRecordsToInsert.add(new message_queue__c(Integration_Flow_Type__c = 'Flow-009_1',
														Status__c = 'not started',
														MSGSTATUS__c = '009_1',
                                                        Object_Name__c = 'Pricebook'));
			insert queueRecordsToInsert;
		}
		catch(Exception ex){
			system.debug(' Schedular failed'+ex);
		}
    

		Integer rqmi = 60*24;

		DateTime timenow = DateTime.now().addMinutes(rqmi);

		PriceBookEntrySchedular rqm= new PriceBookEntrySchedular();
		
		String seconds = '0';
		String minutes = String.valueOf(timenow.minute());
		String hours = String.valueOf(timenow.hour());
		
		//Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
		String sch = seconds + ' ' + minutes + ' ' + hours + ' * * ?';
		String jobName = 'Daily scheduler'+timenow;
		system.schedule(jobName, sch, rqm);
		
		if (sc != null)	{	
			system.abortJob(sc.getTriggerId());			
		}


		/*




		PriceBookEntrySchedular pbe= new PriceBookEntrySchedular();
		//Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
		DateTime timenow = DateTime.now();
		String sch = '0 0 23 * * ?';
		
		String jobName = 'Daily scheduler'+timenow;
		system.schedule(jobName, sch, pbe);
		
		if (cxt != null)	{	
			system.abortJob(cxt.getTriggerId());			
		} */
    }
}