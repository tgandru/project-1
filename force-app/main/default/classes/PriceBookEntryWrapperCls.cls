public class PriceBookEntryWrapperCls {
    
     public Boolean isSelected {get;set;}
     public PriceBookEntry cPriceBookEntry {get;set;}
     public Product2 relProd {get;set;}
     public Boolean relatedProducts {get;set;}

     public PriceBookEntryWrapperCls(PriceBookEntry cPriceBookEntry,Boolean relatedProductsBoolean)
     {
          this.cPriceBookEntry = cPriceBookEntry;
          this.relatedProducts= relatedProductsBoolean; //related products is TRUE if there are NO related products. it is FALSE if tehre ARE related products
          this.relProd=cPriceBookEntry.Product2;
          this.isSelected = false;
     }
}