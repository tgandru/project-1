public class PriceBookSelectionController_Ltng {
@AuraEnabled(cacheable=true)
    public static Opportunity getOppInformation (String Oppid) {
        return [SELECT Id,Name,Account.name,AccountId,CloseDate,Pricebook2Id,ProductGroupTest__c,Pricebook2.Name, Type,Opportunity_Number__c,OwnerId,Owner.Name,CountofApprovedQuotes__c,IsClosed,Division__c from Opportunity where id=:Oppid limit 1 ];
    }
    
    @AuraEnabled(cacheable=true)
    
    Public Static List<PBListWrapper> getAvilablePriceBooks(Opportunity opp){
        List<PriceBook2> pblist=new List<PriceBook2>([SELECT Id, Name, Division__c,IsActive FROM Pricebook2 where Division__c=:opp.Division__c and IsActive=true ORDER BY Name ASC]);
        set<Id> pricebookIDs = new set<Id>(); 
        for(PriceBook2 pb: pblist){
            pricebookIDs.add(pb.Id);
        }
        List<UserRecordAccess> listUserRecordAccess = [SELECT RecordId, HasReadAccess, HasEditAccess, HasDeleteAccess FROM UserRecordAccess WHERE UserId=:UserInfo.getUserId() AND RecordId IN: pricebookIDs];

        Map<Id,boolean> accessMap = new Map<Id,boolean>();

        for(UserRecordAccess recAccess : listUserRecordAccess){
            accessMap.put(recAccess.RecordId, recAccess.HasReadAccess);
        }
        List<PBListWrapper> returnlist=new List<PBListWrapper>();
         returnlist.add(new PBListWrapper('', '-Select a Price Book-',false));  
           for(Pricebook2 R : pblist){
            if(accessMap.get(R.Id)){
                if(R.Id==opp.Pricebook2Id){
                  returnlist.add(new PBListWrapper(R.Id, R.Name,true));  
                }else{
                    returnlist.add(new PBListWrapper(R.Id, R.Name,false));
                }
                
            }
        }
         return returnlist;
    }
    
    public class PBListWrapper {
         @AuraEnabled public  String pbid {get;set;}
         @AuraEnabled public  String pbname {get;set;}
         @AuraEnabled public  Boolean selected {get;set;}
        public PBListWrapper(string pbid,string pbname,boolean selected ){
            this.pbid = pbid;
            this.pbname = pbname;
            this.selected=selected;
        }
    }
    
    @AuraEnabled
    Public static string SaveOpportunityRecord(Opportunity Opp,String pbid){
        try{
            Opportunity op=Opp;
            op.Pricebook2Id=pbid;
            update op;
            return 'Ok';
        }catch(exception ex){
            String error=ex.getMessage();
            return error;
        }
    }
    @auraEnabled
    public static String getInstanceUrl()
    {
        String InstanceURL = URL.getSalesforceBaseUrl().toExternalForm();
        return InstanceURL;
    }
}