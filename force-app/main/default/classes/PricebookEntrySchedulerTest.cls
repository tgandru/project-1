/**
  *   pricebookEntrySchedulerTest
  *   @author: Kavitha Reddy
  **/
@isTest
private class PricebookEntrySchedulerTest {
public Static String CRON_EXP = '0 0 12 15 2 ?'; //To execute schedulable 
    
    static testMethod void testScheduledJob() {
        
        Test.startTest();
        //Executing Schedulable Class   
        String jobId = System.schedule('TestPBEScheduler',CRON_EXP, new PriceBookEntrySchedular());
        
        // Get the information from the CronTrigger API object 
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        
        // Verify the expressions are the same     
        System.assertEquals(CRON_EXP, ct.CronExpression);
        
        // Verify the job has not run 
        
        System.assertEquals(0, ct.TimesTriggered);    
        Test.stopTest();    
    }
}