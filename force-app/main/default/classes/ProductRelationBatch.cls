/*

 * Batch class that scan through all the product relations and deactivated those that are not updated today.
 * Author: Jagan
**/

global class ProductRelationBatch implements Database.Batchable<sObject> {

    global Database.QueryLocator start(Database.BatchableContext bc) {     
        //return Database.getQueryLocator([SELECT id,Is_Active__c,message_queue__c,LastModifiedDate from Product_Relationship__c]);
        //Thiru added Product_type in below Query
        return Database.getQueryLocator([SELECT id,Is_Active__c,message_queue__c,Child_Product__r.Product_Type__c,LastModifiedDate from Product_Relationship__c]);
    }

    global void execute(Database.BatchableContext BC, List<Product_Relationship__c> allProdRelations) {
       //AggregateResult[] messageQueue = [select Max(last_successful_run__c) MaxlastSuccess from message_queue__c where Integration_Flow_Type__c = '007'];
       message_queue__c[] messageQueue = [select id,last_successful_run__c from message_queue__c where Integration_Flow_Type__c = '007' and last_successful_run__c != null order by last_successful_run__c desc limit 1];
       
       if(messageQueue.size() >0){
           DateTime maxDtm = messageQueue[0].last_successful_run__c;
           system.debug('Max Last run time for 007.....'+maxDtm);
           Date maxDate = date.newinstance(maxDtm.year(), maxDtm.month(), maxDtm.day());
           
           //List to store the Product_Relationship__c records
           List<Product_Relationship__c> PrUpdList = new List<Product_Relationship__c>();
           
           for(Product_Relationship__c pr : allProdRelations){
               DateTime prLstModdtm = pr.LastModifiedDate;
               Date PrDate = date.newinstance(prLstModdtm.year(), prLstModdtm.month(), prLstModdtm.day());    
               //if(PrDate != maxDate){
               //Added by thiru with Product_type in if condition and also (PrDate != maxDate.addDays(1)).
               if(PrDate != maxDate && PrDate != maxDate.addDays(1) && pr.Child_Product__r.Product_Type__c != 'Option'){
                   pr.Is_Active__c = FALSE;
                   PrUpdList.add(pr);
               }
           }
           
           if(PrUpdList.size()>0){
               Database.SaveResult[] lsr = database.Update(PrUpdList,false);
               for(Database.SaveResult sr : lsr)
                   if (!sr.isSuccess()) system.debug('Error occured during Product relationshiop update in batch class....'+sr);
           }
       }
    }
    
    global void finish(Database.BatchableContext BC) {

    }
}