/*
Schedular class for ProductRelationBatch
Author : Jagan
*/
global class ProductRelationSchedular implements Schedulable {
    global void execute(SchedulableContext sc) {
        DataBase.ExecuteBatch(new ProductRelationBatch(),200);
    }
}