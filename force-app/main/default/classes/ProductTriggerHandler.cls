/** 
Handler class for Product trigger
**/

public with sharing class ProductTriggerHandler {
    
    //Jagan:- Added to prevent recursive operation
    public static boolean productAfterInsertCheck = FALSE;
    public static boolean productBeforeInsertUpdateCheck = FALSE;
    public static String HA_Builder_Recordtype =Schema.SObjectType.Product2.getRecordTypeInfosByName().get('HA Builder').getRecordTypeId();//Added by  vijay  to bypass  the HA record validation
    public void isInsert(List<Product2> prodLst){
        for(Product2 p:prodLst){
            if(p.Unit_Cost__c!=null){
                p.Unit_Cost_Last_Update__c=System.now();
            }
        }
    }

    public void isUpdate(List<Product2> newProd, Map<Id,Product2> oldProdMap){
        String uid =userinfo.getUserId();
        for(Product2 p:newProd){
            if(oldProdMap.get(p.Id).Unit_Cost__c!=null && oldProdMap.get(p.Id).Unit_Cost__c!=p.Unit_Cost__c){
                p.Unit_Cost_Last_Update__c=System.now();
            }
            if(P.RecordTypeId==HA_Builder_Recordtype && uid=='00536000002AuR9AAK'){
                p.Description=oldProdMap.get(p.Id).Description;
            }
        }
    }
    //Jagan:- Method to update Product Family and Product group based on SAP field values
    public static void productBeforeInsertUpdate(List<Product2> triggerNew){
        
        //if(!productBeforeInsertUpdateCheck){
            //productBeforeInsertUpdateCheck = TRUE;
            //List<ProductFamilyandGroupMap__c> lstProdGrpMap = ProductFamilyandGroupMap__c.getAll().values();
            Map<String, ProductFamilyandGroupMap__c> pgmap = ProductFamilyandGroupMap__c.getAll();
            List<String> names = new List<String>(pgmap.keySet());
            names.sort();
            List<user> userInfoLst = new list<user>([select id, Profile.Name from User where id=:userInfo.getUserId()]);
            for(Product2 prod : triggernew){
                
                prod.Product_Group__c = null;
                prod.Family = null;
                
                for(String name : names){
                  ProductFamilyandGroupMap__c pg = pgmap.get(name);
                    Boolean cond1=FALSE;
                    Boolean cond2=FALSE;
                    Boolean cond1Match=FALSE;
                    Boolean cond2Match=FALSE;
               
                    if(pg.Field_Name_1__c != null && pg.Field_Name_1__c != '' && pg.Field_Name_1_Value__c != null && pg.Field_Name_1_Value__c != ''){
                        cond1 = TRUE;
                        system.debug('Field ONE Name......'+pg.Field_Name_1__c+'.....field ONE value...'+pg.Field_Name_1_Value__c+'....product field ONE value...'+prod.get(pg.Field_Name_1__c));
                        String prodField1 = (String) prod.get(pg.Field_Name_1__c);
                        if(prodField1 != null && prodField1 != '' && pg.Field_Name_1_Value__c.containsIgnoreCase(prodField1))
                            cond1Match = TRUE;    
                    }    
                    if(pg.Field_Name_2__c != null && pg.Field_Name_2__c != '' && pg.Field_Name_2_Value__c != null && pg.Field_Name_2_Value__c != ''){
                        cond2 = TRUE;
                        system.debug('Field TWO Name......'+pg.Field_Name_2__c+'.....field TWO value...'+pg.Field_Name_2_Value__c+'....product field TWO value...'+prod.get(pg.Field_Name_2__c));
                        String prodField2 = (String) prod.get(pg.Field_Name_2__c);
                        if(prodField2 != null && prodField2 != '' && pg.Field_Name_2_Value__c.containsIgnoreCase(prodField2))
                            cond2Match = TRUE;    
                    }
                    if((cond1 && cond2 && cond1Match && cond2Match) || 
                        (cond1 && !cond2 && cond1Match) ||
                        (cond2 && !cond1 && cond2Match)
                      ){
                        prod.Product_Group__c = pg.Product_Group__c;
                        prod.Family = pg.Product_Family__c;
                        break;
                    }
                }
            }
        //}
         if(userInfoLst != null && userInfoLst.size() > 0){
            if(userInfoLst[0].profile.Name == 'Integration User'){
                for(Product2 prod : triggerNew){
                    if(trigger.oldMap != null && trigger.oldMap.get(prod.id) != null){
                        product2 oldProd = (product2)trigger.oldMap.get(prod.id);
                        if(oldProd.isActive == false&&oldProd.End_Of_Life__c==true && prod.isActive == true){
                            prod.isActive= false;
                        }
                    }
                    
                }
            }
        }
    }
    
    //Jagan :- To insert price book entries whenever the product is inserted
    public static void productAfterInsert(List<Product2> triggerNew){
        
        //if(!productAfterInsertCheck){
            //Recursive execution prevention
            //productAfterInsertCheck = TRUE;
            
            List<Pricebook2> lstpb2 = new List<Pricebook2>();
            lstpb2 = [select id,Description from Pricebook2 where IsActive = true and isStandard = true limit 1];
            List<PricebookEntry> lstPbEntry = new List<PricebookEntry>();
            Id stdPbId;
            if(lstpb2.size()>0){
                //Store the standard price book id in a separate variable
                stdPbId = lstpb2[0].id;
            }
            //if it is test method get the standard pricebook id using standard method
            if(Test.isRunningTest())
                stdPbId = Test.getStandardPricebookId();
            if(stdPbId != null){
                //For the newly created products, insert a standard price book entry
                for(Product2 prod : triggernew){
                    PricebookEntry pbEntry = new PricebookEntry();
                    pbEntry.Pricebook2Id = stdPbId;
                    pbEntry.Product2Id = prod.id;
                    pbEntry.UnitPrice = 1.0;
                    pbEntry.IsActive = true;
                    pbEntry.External_Id__c = ''+prod.id+stdPbId;
                    //pbEntry.UseStandardPrice = true;
                    lstPbEntry.add(pbEntry);    
                }
                
                if(lstPbEntry.size() > 0){
                    Database.SaveResult[] lsr = database.Insert(lstPbEntry,false);
                    for(Database.SaveResult sr : lsr)
                        if (!sr.isSuccess()) system.debug('Error occured during Pricebook entry insert....'+sr);
                }
            }
        //}
    }

    //DW - SVC after update
    public static void productAfterUpdate(List<Product2> TriggerNew,List<Product2> TriggerOld){
        Map<id,string> changedProductMap = new Map<id,string>();
        Map<ID, Product2> oldProdMap = new Map<ID,Product2>(TriggerOld);
        for (Product2 prod: TriggerNew) {
            if (  (oldProdMap.get(prod.Id).sc_service_type_ranking__c != prod.sc_service_type_ranking__c) )  {
                changedProductMap.put(prod.id, prod.sc_service_type_ranking__c);                   
            }
        }
        
        if (changedProductMap.size()>0){

            List<Entitlement> entitlements = [ select id, sc_service_type_ranking__c,SC_Product_ID__c 
                from entitlement where is_trial__c != true and
                SC_Product_ID__c in :changedProductMap.keySet()]; //trial entitlement ranking always 10
            for (entitlement e:entitlements){
                 e.sc_service_type_ranking__c = Integer.valueof(changedProductMap.get(e.SC_Product_ID__c));
            }
            update entitlements;    
        }
    }
   
}