@isTest
private class ProductTriggerHandlerTest {


    @isTest static void test_method_one() {
        
        list<ProductFamilyandGroupMap__c> prodFamily = new list<ProductFamilyandGroupMap__c>();
        prodFamily.add(new ProductFamilyandGroupMap__c(name='010',Field_Name_1__c='name',Field_Name_1_Value__c='StandaloneProd1',Field_Name_2__c='name',Field_Name_2_Value__c='StandaloneProd1',Product_Family__c='Printer[C2]',Product_Group__c='FAX/MFP'));
        insert prodFamily;
        
        // Implement test code
    List<Product2> prods=new List<Product2>();
    Product2 standaloneProd=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'StandaloneProd1', 'Printer[C2]', 'FAX/MFP', 'H/W');
    standaloneProd.Unit_Cost__c=10.0;
    prods.add(standaloneProd);
    Product2 parentProd1=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'ParentProd1', 'Printer[C2]', 'FAX/MFP', 'H/W');
    parentProd1.Unit_Cost__c=10.0;
    prods.add(parentProd1);
    Product2 childProd1=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod1', 'Printer[C2]', 'PRINTER', 'Service pack');
    childProd1.Unit_Cost__c=10.0;
    prods.add(childProd1);
    Product2 childProd2=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod2', 'Printer[C2]', 'PRINTER', 'Service pack');
    childProd2.Unit_Cost__c=10.0;
    prods.add(childProd2);
    Product2 parentProd2=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Parent Prod2', 'Printer[C2]', 'FAX/MFP', 'H/W');
    parentProd2.Unit_Cost__c=10.0;
    prods.add(parentProd2);
    Product2 childProd3=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod3', 'Printer[C2]', 'PRINTER', 'Service pack');
    childProd3.Unit_Cost__c=10.0;
    prods.add(childProd3);
    Product2 childProd4=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod4', 'Printer[C2]', 'PRINTER', 'Service pack');
    childProd4.Unit_Cost__c=10.0;
    prods.add(childProd4);
    insert prods;

    List<Product2> products = new List<Product2>();

    Product2 prod1 = [select Id, Name from Product2 where Name = 'StandaloneProd1' Limit 1];
    prod1.Unit_Cost__c=20.0;
    prod1.name = 'modified name';
    products.add(prod1);
    Product2 prod2 = [select Id, Name from Product2 where Name = 'ParentProd1' Limit 1];
    prod2.Unit_Cost__c=20.0;
    prod2.name = 'modifiednames';
    products.add(prod2);
    update products;

    }
    

    @isTest static void test_method_two() {
        // Implement test code
    }

    @isTest static void testSVCProductTrigger() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Profile adminProfile = [SELECT Id FROM Profile Where Name ='B2B Service System Administrator'];

        User admin1 = new User (
            FirstName = 'admin1',
            LastName = 'admin1',
            Email = 'admin1@example.com',
            Alias = 'admint1',
            Username = 'seaadmin1@example.com',
            ProfileId = adminProfile.Id,
            TimeZoneSidKey = 'America/New_York',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US'
        );
        
        insert admin1;

        BusinessHours bh24 = [SELECT Id FROM BusinessHours WHERE name = 'B2B Service Hours 24x7'];
        BusinessHours bh12 = [SELECT Id FROM BusinessHours WHERE name = 'B2B Service Hours 12x5'];
        
        RecordType caseRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Technical Support' 
            AND SobjectType = 'Case'];
        

        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );

        insert testAccount1;

        Account testAccount2 = new Account (
            Name = 'TestAcc2',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );

        insert testAccount2;

        RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];
        Product2 prod1 = new Product2(
                name ='MI-test',
                SAP_Material_ID__c = 'MI-test',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = productRT.id
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Asset ast2 = new Asset(
            name ='Assert-MI-Test2',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast2;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            BusinessHoursId = bh12.id,
            startdate = system.today()-1
            );
        insert ent;

        prod1.sc_service_type__c = 'Elite';
        prod1.sc_service_type_ranking__c = '1';
        update prod1;

        Entitlement ent_new  = [select sc_service_type__c,sc_service_type_ranking__c from entitlement where id=:ent.id];
        
        system.assert('Elite'==ent_new.SC_Service_Type__c);
        system.assert(1 == ent_new.SC_Service_Type_Ranking__c);
    }
    
    
}