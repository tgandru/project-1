public class ProjectRegistrationApprovalExt_Ltng {
	@AuraEnabled
    public static opportunity getOpportunity(String recordId){
        List<Opportunity> oppList = [Select id,
                                            Reason__c,
                                            StageName,
                                            Deal_Registration_Approval__c,
                                            WinApprovalStatus__c,
                                            Amount,
                                            Strategic_Initiatives__c,
                                            Competitor__c,
                                            Recordtype.Name,
                                            Division__c, 
                                            How_is_the_technology_used_and_who_uses__c, 
                                            What_Samsung_or_third_party_applications__c, 
                                            Why_did_the_customer_choose_Samsung_ove__c,
                                            What_was_the_customer_s_problem_and_how__c,
                                            How_did_we_effectively_engage_the_custom__c 
                                            from Opportunity 
                                            Where id=:recordId];
        Opportunity opp;
        if(oppList.size()>0){
            opp = oppList[0];
        }else{
            opp = new Opportunity();
        }
        return opp;
    }
    
    @AuraEnabled
    public static String submitProjectRegistration(String OppId){
        String returnString;
        try{
            Approval.ProcessSubmitRequest req1=new Approval.ProcessSubmitRequest();
            req1.setObjectId(OppId);
            req1.setSubmitterId(UserInfo.getUserId());
            req1.comments = 'Automated SEA B2B Project Registration';
            Approval.ProcessResult result=Approval.process(req1);
            
            returnString = 'Success';
            return returnString;
        }catch(Exception e){
            returnString = string.valueof(e.getMessage());
            if(returnString.contains('Process failed. First exception on row 0; first error: ')){
                returnString = returnString.replace('Process failed. First exception on row 0; first error: ','');
                returnString = returnString.replace('.: []','.');
            }
            return returnString;
        }
        
    }
    
    @AuraEnabled
    public static String submitWinApproval(Opportunity opp){
        String returnString;
        try{
            update opp;
            
            Approval.ProcessSubmitRequest req1=new Approval.ProcessSubmitRequest();
            req1.setObjectId(opp.Id);
            req1.setSubmitterId(UserInfo.getUserId());
            req1.comments = 'Automated SEA B2B Win Approval';
            Approval.ProcessResult result=Approval.process(req1);
            
            returnString = 'Success';
            return returnString;
        }catch(Exception e){
            returnString = string.valueof(e.getMessage());
            if(returnString.contains('Process failed. First exception on row 0; first error: ')){
                returnString = returnString.replace('Process failed. First exception on row 0; first error: ','');
                returnString = returnString.replace('.: []','.');
            }
            return returnString;
        }
        
    }
}