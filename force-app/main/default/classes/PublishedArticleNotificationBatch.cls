/* Batch class to Send Email to Named Callers if any atricle is published Yesterday 
  CreatedDate - 9/4/2018 -Vijay 
  
*/
global class PublishedArticleNotificationBatch implements   Database.Batchable<sObject>,Database.Stateful,Schedulable {
global list<Knowledge__kav> articles;
global string baseurl;

global void execute(SchedulableContext SC) {
    
      database.executeBatch(new PublishedArticleNotificationBatch(),50) ;
}
global PublishedArticleNotificationBatch(){
    articles=new list<Knowledge__kav>();
    Network myNetwork = [SELECT Id FROM Network WHERE Name ='Samsung services' ];
    ConnectApi.Community  myCommunity = ConnectApi.Communities.getCommunity(myNetwork.id);
     baseurl=myCommunity.siteUrl+'/s/article/';
}


global Database.QueryLocator start(Database.BatchableContext bc) {

     return Database.getQueryLocator([select id,name,email,Named_Caller__c from Contact where  Send_Articles__c=true]);//(id='0033600001CSyt2' ) and
  }
  
  
 global void execute(Database.BatchableContext BC, list<Contact> scope){
     List<user> usrlst=new list<user>();
     set<id> conid=new set<id>();
     for(Contact c:scope){
         conid.add(c.id);
     }
     
     
     
      List<Knowledge__kav> publishedArticleList = new  List<Knowledge__kav>();
       List<Messaging.SingleEmailMessage> emails=new List<Messaging.SingleEmailMessage>();
      Datetime dtBefore24 = System.now().addDays(-15);
      Date dtBeforeday = dtBefore24.date();
     For(Knowledge__kav k:[SELECT Id,Title,ArticleNumber,ArticleType,Summary,CreatedDate,FirstPublishedDate,LastPublishedDate,PublishStatus,UrlName,VersionNumber FROM Knowledge__kav WHERE PublishStatus='Online' and language ='en_US' AND IsDeleted = false  AND IsLatestVersion = true  AND FirstPublishedDate=Last_Month]){// DAY_ONLY(FirstPublishedDate) =: dtBeforeday
        publishedArticleList.add(k); 
     }
     if(publishedArticleList.size()>0){
         String body=preparebody(publishedArticleList,baseurl);
         OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where DisplayName = 'SBS Support'];
        
         for(User u:[SELECT Id,Contact.Id,Name,Email FROM User WHERE (Profile.Name = 'Samsung Customer Community User Read Only' OR Profile.Name = 'Customer Community User' OR  Profile.Name = 'Samsung Customer Community User' ) AND isActive=True AND ContactId IN :conid]){
              String[] toAddresses = new String[] {u.email};
                       Messaging.SingleEmailMessage  email = new Messaging.SingleEmailMessage();
                       email.setToAddresses(toAddresses);
                       if ( owea.size() > 0 ) {
                                email.setOrgWideEmailAddressId(owea.get(0).Id);
                            }
                       email.setTargetObjectId(u.Contact.Id);
                       email.setSubject('New Articles Published');
                       email.setHtmlBody(body);
                       email.setSaveAsActivity(false);
                       emails.add(email);
         }
     }
     if(emails.size()>0){
         Messaging.SendEmail(emails,false);
     }
     
    
 }


 global void finish(Database.BatchableContext BC){
  
     
 }
 
     Public static string preparebody(List<Knowledge__kav> kn,string bsurl){
         string st='';
         string bdy = 'Greetings,';
          bdy = bdy + '<br/> <br/> <b></b>Below is a list of recently published KB articles from the Samsung ProCare Mobility Team.  You are receiving this email because of your enrollment in the Samsung ProCare Technical Support.';
          bdy = bdy + '\n \n <br/> <br/>';
          bdy = bdy + '<b>New KB articles published:</b>';
          bdy = bdy + '\n \n <br/> <br/>';
          bdy =bdy+ '<table border="1" style="border-collapse: collapse"><tr><th>Title</th><th>Summary</th></tr>';
          for(Knowledge__kav k:kn){
              string url=bsurl+k.UrlName;
              String Summary =  k.Summary; if( k.Summary == null){Summary = '';}
              bdy += '<tr><td>' + '<a href='+url+' >'+k.Title+'</a>' + '</td><td>' + Summary + '</td></tr>'+'<br/>';

          }
           bdy=bdy+'</table>';
            bdy = bdy + '\n \n <br/>';
            bdy = bdy + '\n \n <br/>'+'If you have any technical support questions please contact us at:';
            bdy = bdy + '\n \n <br/>';
            bdy = bdy + '\n \n <br/>'+'Phone number -  1-844 227-3249, must have Service ID';
            bdy = bdy + '\n \n <br/>'+'Email — SBSsupport@sea.samsung.com';
            bdy = bdy + '\n \n <br/>'+'Portal — http://www.samsung.com/BusinessSupport';
            bdy = bdy + '\n \n <br/>';
            bdy = bdy + '\n \n <br/>';
            
         bdy = bdy + '\n \n'+ '<br/><br/><br/><b>Sincerely</b>,<br/>';
         bdy = bdy + '<b>Your Samsung ProCare Technical Support Team</b>';
         
         bdy = bdy + '\n \n <br/> <br/>';  
         bdy = bdy + '\n \n <br/> <br/>';
         string unsublink=Label.UnSubscribe_Link;
         bdy = bdy + 'If you would like to unsubscribe and stop receiving these emails click here'+'<a href='+unsublink+' >UnSubscribe</a>';
        return bdy;
       
        
        
     }


}