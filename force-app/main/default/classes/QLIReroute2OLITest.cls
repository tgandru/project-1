@isTest
private class QLIReroute2OLITest {
static Id endUserRT = TestDataUtility.retrieveRecordTypeId('End_User', 'Account'); 
static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
	
	@isTest static void test_method_one() {
				//Channelinsight configuration
	Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
	
    //Creating test Account
    Account acct=TestDataUtility.createAccount(TestDataUtility.generateRandomString(5));
    acct.RecordTypeId = endUserRT;
	insert acct;
	
    
   //Creating the Parent Account for Partners
	Account paacct=TestDataUtility.createAccount(TestDataUtility.generateRandomString(7));
	acct.RecordTypeId = directRT;
	insert paacct;
	
	
	//Creating custom Pricebook
	PriceBook2 pb = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(4));
	pb.Division__c='IT'; 
	insert pb;
	
	
	 //Creating list of Opportunities
	//List<Opportunity> opps=new List<Opportunity>();
	Opportunity oppOpen=TestDataUtility.createOppty('New Opp from Test setup',acct, pb, 'RFP', 'IT', 'FAX/MFP', 'Identified');
	//opps.add(oppOpen);
	//insert opps;
	insert oppOpen;
    


   //Creating the partner for that opportunity
   Partner__c partner = TestDataUtility.createPartner(oppOpen,acct,paacct,'Distributor');
   insert partner;
   

	//Creating Products
	List<Product2> prods=new List<Product2>();
	Product2 standaloneProd=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Standalone Prod1', 'Printer[C2]', 'FAX/MFP', 'H/W');
	prods.add(standaloneProd);
	Product2 parentProd1=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Parent Prod1', 'Printer[C2]', 'FAX/MFP', 'H/W');
	prods.add(parentProd1);
	Product2 childProd1=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod1', 'Printer[C2]', 'PRINTER', 'Service pack');
	prods.add(childProd1);
	Product2 childProd2=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod2', 'Printer[C2]', 'PRINTER', 'Service pack');
	prods.add(childProd2);
	Product2 parentProd2=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Parent Prod2', 'Printer[C2]', 'FAX/MFP', 'H/W');
	prods.add(parentProd2);
	Product2 childProd3=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod3', 'Printer[C2]', 'PRINTER', 'Service pack');
	prods.add(childProd3);
	Product2 childProd4=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod4', 'Printer[C2]', 'PRINTER', 'Service pack');
	prods.add(childProd4);
	//insert standaloneProd;
	insert prods;
	
    /*First Insert the standard pricebook entry*/
	
	// Get standard price book ID.
    // This is available irrespective of the state of SeeAllData.		
    //Id pricebookId = Test.getStandardPricebookId();
           
    //List of PricebookEntries for standard pricebook
 	
 	List<PriceBookEntry> pbes=new List<PriceBookEntry>();
   	/*PriceBookEntry standaloneProdPBE1=TestDataUtility.createPriceBookEntry(standaloneProd.Id, pricebookId);
    //insert standaloneProdPBE1;
   	pbes.add(standaloneProdPBE1);
	PriceBookEntry parentProd1PBE1=TestDataUtility.createPriceBookEntry(parentProd1.Id,pricebookId);
	pbes.add(parentProd1PBE1);
	PriceBookEntry childProd1PBE1=TestDataUtility.createPriceBookEntry(childProd1.Id, pricebookId);
	pbes.add(childProd1PBE1);
	PriceBookEntry childProd2PBE1=TestDataUtility.createPriceBookEntry(childProd2.Id, pricebookId);
	pbes.add(childProd2PBE1);
	PriceBookEntry parentProd2PBE1=TestDataUtility.createPriceBookEntry(parentProd2.Id, pricebookId);
	pbes.add(parentProd2PBE1);*/

	//create the pricebook entries for the custom pricebook
	PriceBookEntry standaloneProdPBE=TestDataUtility.createPriceBookEntry(standaloneProd.Id, pb.Id);
	pbes.add(standaloneProdPBE);
	PriceBookEntry parentProd1PBE=TestDataUtility.createPriceBookEntry(parentProd1.Id,pb.Id);
	pbes.add(parentProd1PBE);
	PriceBookEntry childProd1PBE=TestDataUtility.createPriceBookEntry(childProd1.Id, pb.Id);
	pbes.add(childProd1PBE);
	PriceBookEntry childProd2PBE=TestDataUtility.createPriceBookEntry(childProd2.Id, pb.Id);
	pbes.add(childProd2PBE);
	PriceBookEntry parentProd2PBE=TestDataUtility.createPriceBookEntry(parentProd2.Id, pb.Id);
	pbes.add(parentProd2PBE);
	insert pbes;
	//insert standaloneProdPBE;
	
	//Creating opportunity Line Item
	OpportunityLineItem standaloneProdOLI=TestDataUtility.createOpptyLineItem(standaloneProd, standaloneProdPBE, oppOpen);
	standaloneProdOLI.Quantity = 10;
    standaloneProdOLI.TotalPrice = standaloneProdOLI.Quantity * standaloneProdPBE.UnitPrice;
	//olis.add(standaloneProdOLI);
	insert standaloneProdOLI;
	
   	Quote quot = TestDataUtility.createQuote('Test Quote', oppOpen, pb);
    quot.ROI_Requestor_Id__c=UserInfo.getUserId();
    insert quot;
        
    List<QuoteLineItem> qlis = new List<QuoteLineItem>();
    QuoteLineItem standardQLI = TestDataUtility.createQuoteLineItem(quot.Id, standaloneProd, standaloneProdPBE, standaloneProdOLI );
    standardQLI.Quantity = 25;
    standardQLI.UnitPrice = 100;
    standardQLI.Requested_Price__c=100;
    standardQLI.OLIID__c = oppOpen.id;
    qlis.add(standardQLI);
    insert qlis;

    Test.startTest();
    QuoteLineItemRerouteToOppLineItem controller = new QuoteLineItemRerouteToOppLineItem(new ApexPages.StandardController(quot));
	controller.doRedirect();
	Test.stopTest();

	}
	
	@isTest static void test_method_two() {
		// Implement test code
	}
	
}