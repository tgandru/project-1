public with sharing class QLISKUstoClipBoardController {
Public List<QuoteLineItem> qlis {get; set;}
Public String qlistring {get; set;}
    public QLISKUstoClipBoardController(ApexPages.StandardController controller) {
             string QtId = ApexPages.currentPage().getParameters().get('id');
             qlistring ='';
             qlis=new List<QuoteLineItem>();
             if(QtId !=null && QtId !=''){
                qlis=[Select id,SAP_Material_ID__c FROM QuoteLineItem WHERE QuoteId =:QtId];
                for(QuoteLineItem q:qlis){
                  qlistring =qlistring+q.SAP_Material_ID__c +'\n'; 
                }
             }
    }
    
    public void loadData(){
    
    }

}