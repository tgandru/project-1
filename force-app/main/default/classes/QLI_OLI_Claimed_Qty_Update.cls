global class QLI_OLI_Claimed_Qty_Update implements database.Batchable<sObject> {
    global Set<string> QliIds;
    global String queryString;
    
    global QLI_OLI_Claimed_Qty_Update(set<string> qliIdset){
        QliIds = new Set<string>();
        QliIds = qliIdset;
    }
    global database.QueryLocator start(Database.BatchableContext BC){
        //database.Querylocator ql = database.getquerylocator([select id,Claimed_quantity__c,OLIID__c from Quotelineitem where createddate>=2016-07-18T00:00:00.000+0000 order by createddate desc limit 1000]);
        //return ql;
        
        queryString = 'select id,Claimed_quantity__c,OLIID__c from Quotelineitem where id=:QliIds';
        return Database.getQueryLocator(queryString);
    }
    
    global void execute(database.batchablecontext bc, List<quotelineitem> qlis){
        Map<id,Quotelineitem> qlimap = new Map<id,Quotelineitem>(qlis);
        List<Quotelineitem> updateQli = new List<QuoteLineItem>();
        List<Opportunitylineitem> updateOli = new List<OpportunityLineItem>();
        Map<string,integer> oliId_ReqQty_Map = new Map<string,integer>();
        
        //Commented to remove Aggregate query.
        /*for(AggregateResult cmprod : [select SUM(Request_Quantity__c)total_req,Qli_id__c from Credit_Memo_Product__c where (Credit_Memo__r.Status__c = 'Approved' or Credit_Memo__r.Status__c = 'Processed') AND Qli_id__c=:qlimap.keySet() group by Qli_id__c]){
            system.debug('Qli ID------>'+cmprod.get('QLI_ID__c')+' with Total Req Qty----->'+cmprod.get('total_req'));
            string tempid = string.valueOf(cmprod.get('QLI_ID__c'));
            Id qlid = (id)tempid;
            if(qlimap.containsKey(qlid)){
                QuoteLineItem qli = qlimap.get(qlid);
                qli.Claimed_quantity__c = integer.valueOf(cmprod.get('total_req'));
                system.debug('Update Qli ID------>'+qli.Claimed_quantity__c+' with Total Req Qty----->'+cmprod.get('total_req'));
                updateQli.add(qli);
                oliId_ReqQty_Map.put(qlimap.get(qlid).OLIID__c, integer.valueOf(cmprod.get('total_req')));
            }
        }*/
        
        //---Added below logic to replace the Aggregate query in order to increase the batch performance to run fast--09/19/2018.
        //---Starts Here
        Map<string,Integer> QLI_CmReqQty_Map = new Map<String,Integer>();
        for(Credit_Memo_Product__c cmp : [select id,Qli_id__c,Request_Quantity__c from Credit_Memo_Product__c where (Credit_Memo__r.Status__c = 'Approved' or Credit_Memo__r.Status__c = 'Processed') AND Qli_id__c=:qlimap.keySet()]){
            integer rq;
            if(cmp.Request_Quantity__c==null){
                rq = 0;
            }else{
                rq = integer.valueOf(cmp.Request_Quantity__c);
            }
             
            system.debug('Request Qty--------------->'+integer.valueOf(cmp.Request_Quantity__c));
            if(QLI_CmReqQty_Map.containsKey(cmp.Qli_id__c)){
                system.debug('Request Qty--------------->'+QLI_CmReqQty_Map.get(cmp.Qli_id__c));
                rq = rq + QLI_CmReqQty_Map.get(cmp.Qli_id__c);
                QLI_CmReqQty_Map.put(cmp.Qli_id__c, rq);
            }else{
                QLI_CmReqQty_Map.put(cmp.Qli_id__c, rq);
            }
        }
        
        for(Quotelineitem qli : qlis){
            if(QLI_CmReqQty_Map.containsKey(qli.id) && (qli.Claimed_quantity__c!=QLI_CmReqQty_Map.get(qli.id))){
                qli.Claimed_quantity__c = QLI_CmReqQty_Map.get(qli.id);
                updateQli.add(qli);
                oliId_ReqQty_Map.put(qli.OLIID__c, QLI_CmReqQty_Map.get(qli.id));
            }
        }
        //---Ends here
        
        for(OpportunityLineitem oli : [select id,Claimed_quantity__c from OpportunityLineItem where id=:oliId_ReqQty_Map.keySet()]){
            oli.Claimed_quantity__c = oliId_ReqQty_Map.get(oli.id);
            updateOli.add(oli);
        }
        System.debug('Oli Update Size---->'+updateOli.size());
        System.debug('Qli Update Size---->'+updateQli.size());
        if(updateOli.size()>0)
            update updateOli;
        if(updateQli.size()>0)
            update updateQli;
        
    }
    
    global void finish(database.batchablecontext bc){
        
    }
}