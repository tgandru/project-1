@istest(seealldata=true)
public class QLI_OLI_Claimed_Qty_Update_Test{
    public static testmethod void test1(){
        test.startTest();
            List<Quotelineitem> qliList = [select id from Quotelineitem where claimed_quantity__c>0 order by createddate desc limit 10];
            Set<string> qliIDs = new Set<string>();
            for(Quotelineitem qli : qliList){
                qliIDs.add(qli.id);
            }
            QLI_OLI_Claimed_Qty_Update ba = new QLI_OLI_Claimed_Qty_Update(qliIDs);
            database.executeBatch(ba);
        test.stopTest();
    }
}