public class QuoteChannelRelationshipsController {
    
    public List<Channel__c> channelRelationships {get;set;}
    
    public QuoteChannelRelationshipsController(ApexPages.StandardController con) {
        Id quoteId = ApexPages.currentPage().getParameters().get('id');
        
        if(quoteId != null) {
            try{
                Id oppId = [Select OpportunityId from Quote where Id = :quoteId].OpportunityId;
                channelRelationships = [Select Id, Name, Distributor__c, Distributor__r.Name, Reseller__c, Reseller__r.Name from Channel__c where Opportunity__c = :oppId];      
            } catch(Exception e) {
                System.debug('Invalid Quote Id');
            }
        }
    }
}