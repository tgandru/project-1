public with sharing class QuoteCreateRedirectExtension {

	public Quote quoteObj{get;set;}
	public String quoteId{get;set;}
	public Opportunity relatedOpp{get;set;}
	public List<SelectOption> productGroupOptions{get;set;}

	public QuoteCreateRedirectExtension(ApexPages.StandardController controller) {

		if(!test.isRunningTest()){
            controller.addFields(new List<String> {'Opportunity'});
        }
		quoteObj=(Quote)controller.getRecord();
		if(quoteObj!=null){
			quoteId=quoteObj.Id;
		}

		relatedOpp=[select Id, ProductGroupTest__c from Opportunity where Id=:quoteObj.OpportunityId limit 1];
		
	}

	public PageReference showProductGroupOptions(){
		List<String> productGroupNames=new List<String>();
		if(relatedOpp!=null){
			//put productgroup names together
		}
		return null;
	}
}