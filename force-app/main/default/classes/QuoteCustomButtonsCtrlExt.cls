public with sharing class QuoteCustomButtonsCtrlExt {

    private boolean createPDFButton;
    
    public Quote quoteRecord {get; private set;}

    public boolean getCreatePDFButton(){
        return createPDFButton;
    }

    public QuoteCustomButtonsCtrlExt(ApexPages.StandardController stdController) {
        this.quoteRecord = (Quote)stdController.getRecord();
        enableDisableCustomButtons();
    }
    
    private void enableDisableCustomButtons(){
        // IF Status = 'Approved' then enable Create PDF custom button
        if(quoteRecord.Status != 'Approved'){
            createPDFButton = true;
        }
        
   }
}