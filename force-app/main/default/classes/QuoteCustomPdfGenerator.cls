global with sharing class QuoteCustomPdfGenerator {
    private ApexPages.StandardController sc;
    public Quote currentQuote {get; private set;}
    public String id {get; private set;}
    public List<QuoteLineItem> productLineItems;
    public static string emailURL {get;set;}

    public QuoteCustomPdfGenerator(ApexPages.StandardController controller) {
        this.sc = sc;
        this.id = ApexPages.currentPage().getParameters().get('Id');
        this.currentQuote = [Select Id, Name, QuoteNumber, Account.Name, Distributor__r.Name, DistributorAddress__c, RequestType__c, PricingNotes__c,               
                            Terms__c, Valid_From_Date__c, ExpirationDate, CreatedBy.Name, CreatedBy.Email, External_SPID__c, End_Customer_Address__c, 
                            Opportunity.Opportunity_Number__c, Opportunity.Owner.Name, Opportunity.Owner.Email, Distributor_Contact_Details__c, TotalCost__c, 
                            TotalDiscount__c, Opportunity.Name, Opportunity.StageName, Opportunity.CloseDate, Opportunity.Amount, Status, Division__c,
                            Product_Division__c, TotalPrice, ProductGroupMSP__c, MIN_GM__c, MAX_DISCOUNT__c, TotalGM__c, TotalDiscountPercent__c, BEP__c,
                            MPS__c, Supplies_Buying_Location__c, Set_Marginal_Profit_Rate__c, InternalNotes__c, Opportunity.Channel__c, additional_conditions__c,
                            VersionNumber__c,contactid,Custom_Deliverable_Details__c,Samsung_Contact__c,subtotal,total_device_quantity__c,
                            Sold_To__c,Ship_To__c,Primary_Reseller__c,samsung_rep__c
                            from Quote where Id = :this.id];
    }
    
    public PageReference viewPdf() {
        return null;
    }

    webService static String createQuotePdf(String Id) {
        try {
            PageReference pageRef = new PageReference('/apex/QuoteCustomPdfView?Id='+Id);
            Blob content = pageRef.getContent();
            QuoteDocument doc = new QuoteDocument(Document = content, QuoteId = Id);
            insert doc;

            return 'SUCCESS';
        } 
        catch(exception ex) {
           System.debug('--- Error ----------'+ ex);
           return ex.getMessage();
        }
    }
    
    webService static String saveQuoteAsPDFandEmail(String QuotId)
    {  
        try{
            System.debug('Quote ID----->'+QuotId);
            PageReference pr = new PageReference('/apex/QuoteCustomPdfView?Id='+QuotId);
            Blob content = pr.getContent();
            QuoteDocument doc = new QuoteDocument(Document = content, QuoteId = QuotId);
         
            Database.SaveResult insertResult = Database.Insert(doc, false);
            // check for errors here and act according!
            System.debug('Result: ' + insertResult);
            System.debug('Quote PDF created: ' + doc.Id);  
        
            //upon completion redirect to quote
            emailURL = '/_ui/core/email/author/EmailAuthor?p3_lkid='+QuotId+'&doc_id='+doc.Id+'&retURL=%2F'+QuotId;
    
            System.debug('Quote Email URL: ' + emailURL);
            PageReference quotePage = new PageReference(emailURL);
            quotePage.setRedirect(true);
            return emailURL;  
        }
        catch(exception ex) {
           System.debug('--- Error ----------'+ ex);
           return ex.getMessage();
        }
        
    }
    
    public List<QuoteLineItem> getProductLineItems() {
        if(productLineItems == null) {
            productLineItems = [Select Product2.Name,Product2.SAP_Material_ID__c, Description, Quantity, ListPrice, UnitPrice, TotalPrice, Discount__c, 
                                Model_Name_Nickname__c, Requested_Price__c, TotalDiscount__c, Total_List_Price__c, Total_Requested_Price__c 
                                from QuoteLineItem where QuoteId = :currentQuote.Id];
            
            for(QuoteLineItem qli : productLineItems) {
                if(qli.UnitPrice == null) qli.UnitPrice = 0;
                if(qli.Total_List_Price__c == null) qli.Total_List_Price__c = 0;
                if(qli.Total_Requested_Price__c == null) qli.Total_Requested_Price__c = 0;
                if(qli.Discount__c == null) qli.Discount__c = 0;
            }
        }
        return productLineItems;
    }
    
}