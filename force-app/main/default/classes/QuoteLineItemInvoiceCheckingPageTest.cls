@isTest(seealldata=true)
private class QuoteLineItemInvoiceCheckingPageTest {

	static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');

   static testmethod void testInvoicePrice() {

   	
        Account account = TestDataUtility.createAccount('Marshal Mathers');
        insert account;
        Account partnerAcc = TestDataUtility.createAccount('Distributor account');
        partnerAcc.RecordTypeId = directRT;
        insert partnerAcc;
         //Creating custom Pricebook
        PriceBook2 pb = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(4)); 
        insert pb;
        Opportunity opportunity = TestDataUtility.createOppty('New', account, pb, 'Managed', 'IT', 'product group',
                                    'Identification');
        insert opportunity;
        Product2 product = TestDataUtility.createProduct2('SL-SCF5300/SEG', 'Standalone Printer', 'Printer[C2]', 'H/W', 'FAX/MFP');
        insert product;
        Id pricebookId = Test.getStandardPricebookId();
        //PricebookEntry pricebookEntry1 = TestDataUtility.createPriceBookEntry(product.Id, pricebookId);
        //insert pricebookEntry1;
        ///^^ del bc System.DmlException: Insert failed. First exception on row 0; first error: DUPLICATE_VALUE, This price definition already exists in this price book: []
        PricebookEntry pricebookEntry = TestDataUtility.createPriceBookEntry(product.Id, pb.Id);
        insert pricebookEntry;

        List<OpportunityLineItem> opportunityLineItems = new List<OpportunityLineItem>();
        OpportunityLineItem op1 = TestDataUtility.createOpptyLineItem(product, pricebookEntry, opportunity);
        op1.Quantity = 100;
        op1.TotalPrice = 1000;
        op1.Requested_Price__c = 100;
        op1.Claimed_Quantity__c = 45;     
        opportunityLineItems.add(op1);
        insert opportunityLineItems;
        System.debug('Insert OLI success');

        Quote quot = TestDataUtility.createQuote('Test Quote', opportunity, pb);
        quot.ROI_Requestor_Id__c=UserInfo.getUserId();
        quot.External_SPID__c='sp-0011909-1';
        insert quot;
        
        List<QuoteLineItem> qlis = new List<QuoteLineItem>();
        QuoteLineItem standardQLI = TestDataUtility.createQuoteLineItem(quot.Id, product, pricebookEntry, op1 );
        standardQLI.Quantity = 25;
        standardQLI.UnitPrice = 100;
        standardQLI.Requested_Price__c=100;
        standardQLI.OLIID__c = op1.id;
        qlis.add(standardQLI);
        insert qlis;
           
        PageReference pf = page.QuoteLineItemInvoicePriceCheckingPage;
        test.setCurrentPage(pf);
        test.startTest();
       
        QuoteLineItemInvoicePriceCheckingPageEXT obj = new QuoteLineItemInvoicePriceCheckingPageEXT(new apexPages.standardController(quot));
        quote tmpQuote = [select id, External_SPID__c from quote limit 1];
        obj.searchBoxstring = tmpQuote.External_SPID__c;
        obj.doSearch();
        pf.getParameters().put('spaid',obj.searchBoxstring);
        pf.getParameters().put('msgid',obj.msgid);
        obj.showResult();
        obj.doSearch();
        obj.updatePricing();
        obj.switchMessage();
        
        PageReference pf1 = page.QuoteLineItemInvoicePriceCheckingPage;
        test.setCurrentPage(pf1);
        pf1.getParameters().put('spaid',obj.searchBoxstring);
        pf1.getParameters().put('msgid',obj.msgid);
        QuoteLineItemInvoicePriceCheckingPageEXT obj1 = new QuoteLineItemInvoicePriceCheckingPageEXT(new apexPages.standardController(quot));
        
        obj1.showResult();
        
        PageReference pf2 = page.QuoteLineItemInvoicePriceCheckingPage;
        test.setCurrentPage(pf2);
        pf2.getParameters().put('spaid',obj.searchBoxstring);
        pf2.getParameters().put('msgid',obj.msgid);
        pf2.getParameters().put('status','success');
        QuoteLineItemInvoicePriceCheckingPageEXT obj2 = new QuoteLineItemInvoicePriceCheckingPageEXT(new apexPages.standardController(quot));
        
        obj2.showResult();
        QuoteLineItemInvoicePriceCheckingPageEXT.displayErrorMessage('msg');
          
        test.stopTest();
    
    
    }
    
    static testmethod void testInvoicePrice2() {

   	
        Account account = TestDataUtility.createAccount('Marshal Mathers');
        insert account;
        Account partnerAcc = TestDataUtility.createAccount('Distributor account');
        partnerAcc.RecordTypeId = directRT;
        insert partnerAcc;
         //Creating custom Pricebook
        PriceBook2 pb = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(4)); 
        insert pb;
        Opportunity opportunity = TestDataUtility.createOppty('New', account, pb, 'Managed', 'IT', 'product group',
                                    'Identification');
        insert opportunity;
        Partner__c partner = TestDataUtility.createPartner(opportunity, account,partnerAcc,'Distributor');
        partner.Is_Primary__c = true;
        insert partner;
        
        Product2 product = TestDataUtility.createProduct2('SL-SCF5300/SEG', 'Standalone Printer', 'Printer[C2]', 'H/W', 'FAX/MFP');
        insert product;
        Id pricebookId = Test.getStandardPricebookId();
        //PricebookEntry pricebookEntry1 = TestDataUtility.createPriceBookEntry(product.Id, pricebookId);
        //insert pricebookEntry1;
        ///^^ del bc System.DmlException: Insert failed. First exception on row 0; first error: DUPLICATE_VALUE, This price definition already exists in this price book: []
        PricebookEntry pricebookEntry = TestDataUtility.createPriceBookEntry(product.Id, pb.Id);
        insert pricebookEntry;

        List<OpportunityLineItem> opportunityLineItems = new List<OpportunityLineItem>();
        OpportunityLineItem op1 = TestDataUtility.createOpptyLineItem(product, pricebookEntry, opportunity);
        op1.Quantity = 100;
        op1.TotalPrice = 1000;
        op1.Requested_Price__c = 100;
        op1.Claimed_Quantity__c = 45;     
        opportunityLineItems.add(op1);
        insert opportunityLineItems;
        System.debug('Insert OLI success');

        Quote quot = TestDataUtility.createQuote('Test Quote', opportunity, pb);
        quot.ROI_Requestor_Id__c=UserInfo.getUserId();
        quot.External_SPID__c='sp-0011909-1';
        insert quot;
        
        List<QuoteLineItem> qlis = new List<QuoteLineItem>();
        QuoteLineItem standardQLI = TestDataUtility.createQuoteLineItem(quot.Id, product, pricebookEntry, op1 );
        standardQLI.Quantity = 25;
        standardQLI.UnitPrice = 100;
        standardQLI.Requested_Price__c=100;
        standardQLI.OLIID__c = op1.id;
        qlis.add(standardQLI);
        insert qlis;
           
        PageReference pf = page.QuoteLineItemInvoicePriceCheckingPage;
        test.setCurrentPage(pf);
        test.startTest();
       
        
        QuoteLineItemInvoicePriceCheckingPageEXT obj = new QuoteLineItemInvoicePriceCheckingPageEXT(new apexPages.standardController(quot));
        quote tmpQuote = [select id, External_SPID__c from quote limit 1];
        obj.searchBoxstring = tmpQuote.External_SPID__c;
        obj.doSearch();
        pf.getParameters().put('spaid',obj.searchBoxstring);
        pf.getParameters().put('msgid',obj.msgid);
        obj.doSearch();
        delete ([select id from Partner__c where Opportunity__c=:opportunity.id]);
        obj.updatePricing();
        obj.switchMessage();
        message_queue__c mq = [SELECT ID,Identification_Text__c, status__c,retry_counter__c FROM message_queue__c WHERE id = :obj.msgid];
        cihStub.UpdateInvoicePriceResponse stubResponse = new cihStub.UpdateInvoicePriceResponse();    //lclc delete later and change responsefake to response:
        String responsefake='{"inputHeaders":{"MSGGUID":"a6bba321-2183-5fb4-ff75-4d024e39a98e","IFID":"TBD","IFDate":"160317","MSGSTATUS":"S","ERRORTEXT":null,"ERRORCODE":null},"body":{"lines":[{"unitCost":"20.1","materialCode":"CLX-4195FN/XAA","quantity":"1","invoicePrice":"20.5","currencyKey":"1","itemNumberSdDoc":"1"},{"unitCost":"330.1","materialCode":"CLX-4195FN/XAB","quantity":"1","invoicePrice":"30.5","currencyKey":"1","itemNumberSdDoc":"2"}]}}';
        stubResponse = (cihStub.UpdateInvoicePriceResponse) json.deserialize(responsefake, cihStub.UpdateInvoicePriceResponse.class);
        QuoteLineItemInvoicePriceCheckingPageEXT.handleError(mq,stubResponse,'error');
        QuoteLineItemInvoicePriceCheckingPageEXT.handleSuccess(mq,stubResponse);
        QuoteLineItemInvoicePriceCheckingPageEXT.webcallout('','');
         QuoteLineItemInvoicePriceCheckingPageEXT.receiveQuoteLineItemsForApproval(mq); 
        test.stopTest();
    
    
    }
    static testmethod void testInvoicePrice3() {

   	
        Account account = TestDataUtility.createAccount('Marshal Mathers2');
        insert account;
        Account partnerAcc = TestDataUtility.createAccount('Distributor account2');
        partnerAcc.RecordTypeId = directRT;
        insert partnerAcc;
         //Creating custom Pricebook
        PriceBook2 pb = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(4)); 
        insert pb;
        Opportunity opportunity = TestDataUtility.createOppty('New', account, pb, 'Managed', 'IT', 'product group',
                                    'Identification');
        insert opportunity;
        Partner__c partner = TestDataUtility.createPartner(opportunity, account,partnerAcc,'Distributor');
        partner.Is_Primary__c = true;
        insert partner;
        
        Product2 product = TestDataUtility.createProduct2('SL-SCF5300/SEG', 'Standalone Printer', 'Printer[C2]', 'H/W', 'FAX/MFP');
        insert product;
        Id pricebookId = Test.getStandardPricebookId();
        //PricebookEntry pricebookEntry1 = TestDataUtility.createPriceBookEntry(product.Id, pricebookId);
        //insert pricebookEntry1;
        ///^^ del bc System.DmlException: Insert failed. First exception on row 0; first error: DUPLICATE_VALUE, This price definition already exists in this price book: []
        PricebookEntry pricebookEntry = TestDataUtility.createPriceBookEntry(product.Id, pb.Id);
        insert pricebookEntry;

        List<OpportunityLineItem> opportunityLineItems = new List<OpportunityLineItem>();
        OpportunityLineItem op1 = TestDataUtility.createOpptyLineItem(product, pricebookEntry, opportunity);
        op1.Quantity = 100;
        op1.TotalPrice = 1000;
        op1.Requested_Price__c = 100;
        op1.Claimed_Quantity__c = 45;     
        opportunityLineItems.add(op1);
        insert opportunityLineItems;
        System.debug('Insert OLI success');

        Quote quot = TestDataUtility.createQuote('Test Quote', opportunity, pb);
        quot.ROI_Requestor_Id__c=UserInfo.getUserId();
        quot.External_SPID__c='sp-0011909-1';
        insert quot;
        
        List<QuoteLineItem> qlis = new List<QuoteLineItem>();
        QuoteLineItem standardQLI = TestDataUtility.createQuoteLineItem(quot.Id, product, pricebookEntry, op1 );
        standardQLI.Quantity = 25;
        standardQLI.UnitPrice = 100;
        standardQLI.Requested_Price__c=100;
        standardQLI.OLIID__c = op1.id;
        qlis.add(standardQLI);
        insert qlis;
           
        PageReference pf = page.QuoteLineItemInvoicePriceCheckingPage;
        test.setCurrentPage(pf);
        test.startTest();
       
        
        QuoteLineItemInvoicePriceCheckingPageEXT obj = new QuoteLineItemInvoicePriceCheckingPageEXT(new apexPages.standardController(quot));
        quote tmpQuote = [select id, External_SPID__c from quote limit 1];
        obj.searchBoxstring = '1234';
        obj.doSearch();
       
        
        test.stopTest();
    
    
    }

}