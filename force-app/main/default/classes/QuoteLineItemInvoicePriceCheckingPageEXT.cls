public class QuoteLineItemInvoicePriceCheckingPageEXT{
    public class myException extends Exception {}
    public string searchboxstring{get; set;}
    public Quote qt {get; set;}
    public Account acct {get; set;}
    public Account_Sold_To__c acct_soldto {get; set;}
    public List<QuoteLineItem> qli_list {get; set;}
    public message_queue__c mq {get; set;}
    Public id msgid;
    public boolean showlateMesg {get;set;}
    public boolean showmq {get;set;} 
    public String myMessage {get; set;}
      public PageReference doSearch() {
         
         init();
         return null;
      }

    public QuoteLineItemInvoicePriceCheckingPageEXT(ApexPages.StandardController controller) {
        //displayErrorMessage('Controller Started ...');
        
        if(ApexPages.currentPage().getParameters().get('spaid') != null){
            searchBoxstring = ApexPages.currentPage().getParameters().get('spaid');
        }
        init();
        if(ApexPages.currentPage().getParameters().get('msgid') != null){
            msgid = ApexPages.currentPage().getParameters().get('msgid');
        }
        if(msgid != null){
            mq = [SELECT ID,Identification_Text__c, status__c FROM message_queue__c WHERE id = :msgid];
        }
        
    }
    public boolean showErrorMsg {get; set;}
    public void init(){
        myMessage = 'There are no records for given SPA ID';
        
        showErrorMsg = false;
        showmq=true;
        qt = new Quote();
        acct = new account();
        
        List<Account_Sold_To__c> acct_soldto_list = new List<Account_Sold_To__c>();
        qli_list = new List<QuoteLineItem>();
        if(searchBoxstring != null && searchBoxstring != ''){
          try{
              
            URL currentURL = URL.getCurrentRequestUrl();
 
              
              qt = [SELECT id, name, QuoteNumber, Opportunity.name,Quote_Number__c ,Opportunity.Primary_Distributor__c FROM Quote WHERE External_SPID__c = :searchBoxstring];
            acct = [SELECT id, name FROM Account WHERE id = :qt.Opportunity.Primary_Distributor__c];        

            acct_soldto_list = [SELECT id, account__r.name, SoldToAccountNumber__c FROM Account_Sold_To__c WHERE Account__c = :qt.Opportunity.Primary_Distributor__c and IsActive__c = TRUE];
            
            if (acct_soldto_list.size() == 0) {
                myMessage = 'Please check the SoldtoCode for this SPA \'s Account : ' + '\n' + acct.name + ' ' + acct.id;
                throw new MyException();
            }
            
    
            List<String> qlis = new List<String>();
           
            for (QuoteLineItem qli : [SELECT id, SAP_Material_ID__c , Requested_Price__c, Quantity, Valid_InvoicePrice__c  FROM QuoteLineItem WHERE QuoteId = :qt.Id]) 
            {
                qli_list.add(qli);
                //QuoteLineItemHelper qlihelper = new QuoteLineItemHelper(qli.SAP_Material_ID__c);
                //qlis.add(JSON.serialize(qlihelper));
                
            }
              
          }catch(Exception  ex){
              showErrorMsg = true;
              showmq=false;
              ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING, 'Other Error' + ex.getmessage());
               
          }  
            
        }
        
    }
     
   
    public pageReference resetmypage(){
        
            PageReference p = new PageReference('/apex/QuoteLineItemInvoicePriceCheckingPage');
            p.setRedirect(true);
            return p; 
       
    }
    public pageReference switchMessage(){
        system.debug('inside switch message...');
        showlateMesg = TRUE;
        return null;
    }
    
    public pageReference showResult(){
        system.debug('msgid'+msgid);
        boolean executed = false;
        if(ApexPages.currentPage().getParameters().get('msgid') != null){
            msgid = ApexPages.currentPage().getParameters().get('msgid');
            
        }
        if(ApexPages.currentPage().getParameters().get('status') != null && ApexPages.currentPage().getParameters().get('status') == 'success'){
             executed = true;
            
        }
        // if (mq.id != NULL) {
        if(msgid != null){
            mq = [SELECT ID,Identification_Text__c, status__c FROM message_queue__c WHERE id = :msgid];
            init();
             System.debug('0:' + mq.id + ' status :' + mq.status__c);
        }else{
            return null;
        }
             
            
        // }
        
        
        if(executed == false){
             PageReference p = new PageReference('/apex/QuoteLineItemInvoicePriceCheckingPage?spaid='+ searchBoxstring+'&msgid='+msgid+'&status='+mq.status__c);
            p.setRedirect(true);
            return p;
        }else{
            return null;
        }
       
    }    
    
    public static void displayErrorMessage(String messageText){
        ApexPages.Message message = new ApexPages.Message(ApexPages.Severity.ERROR, messageText);
        ApexPages.addMessage(message);
    }
    
    public void updatePricing(){
        //displayErrorMessage('Checking Price ...');
        //Quote qt = [SELECT id FROM Quote WHERE Id = :qt.Id];
//        displayErrorMessage('Call CallOutFlow008');

        mq = new message_queue__c(Identification_Text__c=qt.id, Integration_Flow_Type__c='Invoice Price Checking',
                                                        retry_counter__c=0,Status__c='not started', Object_Name__c='Quote');
                                                        
        mq.IFID__c=(!Test.isRunningTest() && Integration_EndPoints__c.getInstance('Flow-008').layout_id__c!=null) ? Integration_EndPoints__c.getInstance('Flow-008').layout_id__c : '';

        insert mq;
        this.msgid=mq.id;
        system.debug('msgid'+msgid);
        System.debug('1:' + mq.id);
        //displayErrorMessage('MQ Status:' + mq.Status__c);
        callOutFlow008(qt.Id,mq.id);
    }
    
    @future(callout=true)
    
    public static void callOutFlow008(String qtid, Id mqId) {
        message_queue__c mq = [SELECT ID,Identification_Text__c, status__c,retry_counter__c FROM message_queue__c WHERE id = :mqId];
        System.debug('2:' + mq.id + ' status :' + mq.status__c);
        receiveQuoteLineItemsForApproval(mq);
    }
    

    public static void receiveQuoteLineItemsForApproval(message_queue__c omq) 
    {

        System.debug('3:' + omq.id + ' status :' + omq.status__c);
        List<QuoteLineItem> qliToUpdate=new List<QuoteLineItem>();
        
        integer errCount = 0;
        String soldToShipTo='';

        Long sendTime=0;    
        Long receiveTime=0;
        Long updateTime=0;
        DateTime startTime=DateTime.now();
                
        string response = '';
 
        
        //get all fields necessary to complete transfer, all fields from pricebook entry that aren't there. 
        //List<QuoteLineItem> qliRecords=[select Id, Product2Id, PricebookEntryId, PricebookEntry.salesOrg__c, PricebookEntry.Currency_Key__c,
        //                                    PricebookEntry.Sales_Document_Type__c, PricebookEntry.ProductCode, PricebookEntry.msguid__c,
        //                                    PricebookEntry.ifid__c, PricebookEntry.ifdate__c, PricebookEntry.UnitPrice, 
        //                                    PricebookEntry.SAP_Material_Id__c, Product2.ProductCode, OLIID__c, Product2.Name,
        //                                    QuoteId
        //                                from QuoteLineItem where QuoteId =:omq.Identification_Text__c]; //PricebookEntry.Ship_To_Party PricebookEntry.Sold_To_Party,
                                        
        List<QuoteLineItem> qliRecords=[select Id, Product2Id, PricebookEntryId, PricebookEntry.salesOrg__c, PricebookEntry.Currency_Key__c,
                                            PricebookEntry.Sales_Document_Type__c, PricebookEntry.ProductCode, PricebookEntry.msguid__c,
                                            PricebookEntry.ifid__c, PricebookEntry.ifdate__c, PricebookEntry.UnitPrice, 
                                            SAP_Material_Id__c, Product2.ProductCode, OLIID__c, Product2.Name,
                                            QuoteId
                                        from QuoteLineItem where QuoteId =:omq.Identification_Text__c]; //PricebookEntry.Ship_To_Party PricebookEntry.Sold_To_Party,                                         


        Quote quoteRecord=[select Id, Name,
                            Approval_Requestor_Id__c, OpportunityId, Pricebook2Id, Integration_Status__c, Valid_From_Date__c, 
                            Pricebook2.Order_Reason__c, Pricebook2.Distribution_Channel__c, Pricebook2.Sales_Document_Type__c,
                            Pricebook2.Sales_Organization__c, Pricebook2.Sales_Division_Code__c, Pricebook2.Plant__c,Division__c
                            from Quote where Id=:omq.Identification_Text__c limit 1];//Total_Lines_For_Update__c, Successful_Update_Number__c, Failed_Update_Number__c, 

        if(quoteRecord==null){
            messageQueueHelper cont=new messageQueueHelper();
            cont.failed(omq, null,null, 'no quote found',startTime);
            return;
        }
    
        
        //@TODO see if oppty has distributor, if not then do this. 

//       displayErrorMessage('Check the data');
       List<Partner__c> partnerRecord= new List<Partner__c>([select Id, Partner__c, Partner__r.SAP_Company_Code__c, Partner__r.distribution_channel_id__c,
                                            Partner__r.sales_organization__c, Partner__r.sales_division_id__c
                                            from Partner__c 
                                            where Role__c='Distributor' and Is_Primary__c=true and Opportunity__c=:quoteRecord.OpportunityId
                                            limit 1]); 
        if(!partnerRecord.isEmpty() && !test.isRunningTest()){
            soldToShipTo=partnerRecord[0].Partner__r.SAP_Company_Code__c;
            //distribChannelId=partnerRecord[0].Partner__r.distribution_channel_id__c;
            //salesOrgId=partnerRecord[0].Partner__r.sales_organization__c;
            //salesDivId=partnerRecord[0].Partner__r.sales_division_id__c;
        }
        if(partnerRecord.isEmpty() || test.isRunningTest()){
             DirectPriceBookAssign__c dpbaObj=new DirectPriceBookAssign__c(); 
//             displayErrorMessage('lclc 1');
             System.debug('lclc 1');
            if(![select Id, Price_Book__c, Direct_Account__c 
                                                from DirectPriceBookAssign__c 
                                                where Price_Book__c=:quoteRecord.Pricebook2Id limit 1].isEmpty()){
                System.debug('lclc 2');
                dpbaObj=[select Id, Price_Book__c, Direct_Account__c 
                                                from DirectPriceBookAssign__c 
                                                where Price_Book__c=:quoteRecord.Pricebook2Id limit 1];
                                                SYSTEM.DEBUG('LCLC 3 '+dpbaObj);
            }else{
            
            //if(dpbaObj==null){
                messageQueueHelper cont=new messageQueueHelper();
                cont.failed(omq, null,quoteRecord, 'no Direct Price Book Assign or Partner found',null);
                //quoteRecord.addError('There is no primary distributor partner or direct price book assign records related to this quote. Please check and try again.');
                return;
            }
    
            Account acctRecord=[select Id, SAP_Company_Code__c, sales_division_id__c, distribution_channel_id__c,
                                sales_organization__c
                                from Account where Id=:dpbaObj.Direct_Account__c limit 1];//Owner.Division__c Id=:oliRecord.Opportunity.AccountId; Price_Book__c=:quoteRecord.Pricebook2Id 
            System.debug('lclc 4 acct rec '+acctRecord );
            if(acctRecord==null){
                messageQueueHelper cont=new messageQueueHelper();
                cont.failed(omq, null,quoteRecord, 'no account found',null);
                //quoteRecord.addError('There is no primary distributor partner or direct price book assign records-Accounts related to this quote. Please check and try again.');
                return;
            }      
            soldToShipTo=acctRecord.SAP_Company_Code__c;
            //soldToShipTo=acctRecord.distribution_channel_id__c;
            //salesOrgId=acctRecord.sales_organization__c;
            //salesDivId=acctRecord.sales_division_id__c;
        }
        
//        displayErrorMessage('lclc is there soldToShipTo '+soldToShipTo);
        System.debug('lclc is there soldToShipTo '+soldToShipTo);
    
    
        cihstub.UpdateInvoicePriceRequest request = new cihStub.UpdateInvoicePriceRequest();
        cihstub.InvoiceBodyRequest outerRecStub = new cihstub.InvoiceBodyRequest();
        
        List<cihstub.CreditMemoLineItemsRequest> quotelines = new List<cihstub.CreditMemoLineItemsRequest>();
        
            //determine if there are missing values
        if( soldToShipTo =='' || quoteRecord.Valid_From_Date__c==null ){
            quoteRecord.addError('There are missing fields. Assure  you have a valid from date on the quote and check for a Partner or direct price book assign record related to the quote.');
            return;
        }
        
        
        Map<String, QuoteLineItem> itemNumberQLIMap=new Map<String, QuoteLineItem>();
        Integer count=1;
    
        for(QuoteLineItem qli:qliRecords){
    
            String c=string.valueOf(count).leftPad(6).replace(' ','0');
//            displayErrorMessage('Counter:'+c);
            itemNumberQLIMap.put(c,qli);
    
            cihStub.CreditMemoLineItemsRequest innerRecStub=new cihStub.CreditMemoLineItemsRequest();
    
            innerRecStub.itemNumberSdDoc=c;
            //OpportunityLineItem oli=qliToOliMap.get(qli);
            innerRecStub.materialCode=qli.SAP_Material_Id__c;
//            innerRecStub.materialCode =oli.PriceBookEntry.Product2.SAP_Material_ID__c;
            innerRecStub.quantity=string.valueOf(1);
            innerRecStub.werks=quoteRecord.Pricebook2.Plant__c;
    
            quotelines.add(innerRecStub);
            count++;
    
        }
        
        
        string layoutId = Test.isRunningTest() ? '' : Integration_EndPoints__c.getInstance('Flow-008').layout_id__c;
        string endpoint=Integration_EndPoints__c.getInstance('Flow-008').endPointURL__c;
        
        cihStub.msgHeadersRequest headers = new cihStub.msgHeadersRequest(layoutId);
        request.inputHeaders = headers;
    
        outerRecStub.salesOrg=quoteRecord.Pricebook2.Sales_Organization__c;
        outerRecStub.distributionChannel=quoteRecord.Pricebook2.Distribution_Channel__c;
        outerRecStub.division=quoteRecord.Pricebook2.Sales_Division_Code__c;
        outerRecStub.soldToParty=soldToShipTo;
        outerRecStub.shipToParty=soldToShipTo;
        outerRecStub.salesDocType=quoteRecord.Pricebook2.Sales_Document_Type__c;
        outerRecStub.orderReason=quoteRecord.Pricebook2.Order_Reason__c;
        outerRecStub.currencyKey='USD';
        Datetime dt=datetime.newInstance(quoteRecord.Valid_From_Date__c.year(), quoteRecord.Valid_From_Date__c.month(),quoteRecord.Valid_From_Date__c.day());
        outerRecStub.validFromDate=dt.formatGMT('yyyy-MM-dd');
        outerRecStub.lines=quotelines;
    
        request.body=outerRecStub;
//        displayErrorMessage('lclc request '+request);
        System.debug('lclc request '+request);
        String responsefake='{"inputHeaders":{"MSGGUID":"a6bba321-2183-5fb4-ff75-4d024e39a98e","IFID":"TBD","IFDate":"160317","MSGSTATUS":"S","ERRORTEXT":null,"ERRORCODE":null},"body":{"lines":[{"unitCost":"20.1","materialCode":"CLX-4195FN/XAA","quantity":"1","invoicePrice":"20.5","currencyKey":"1","itemNumberSdDoc":"1"},{"unitCost":"330.1","materialCode":"CLX-4195FN/XAB","quantity":"1","invoicePrice":"30.5","currencyKey":"1","itemNumberSdDoc":"2"}]}}';
        try{
            sendTime=DateTime.now().getTime();
            if(!test.isrunningTest()){
                response=webcallout(JSON.serialize(request),endpoint);
            }else{
                response = responsefake;
            }
            
            receiveTime=DateTime.now().getTime();
            System.debug('mmm lclc response '+response);
        }catch(Exception ex){
            messageQueueHelper cont=new messageQueueHelper();
            cont.generalFail(omq, ex, quoteRecord, null,startTime);
            System.debug('LCLC 1 ERROR IN RESPONSE ---- '+ex);
            return;
        }
    
        if(response == null){
            messageQueueHelper cont=new messageQueueHelper();
            cont.success(omq, quoteRecord, 'nothing came thru',(receiveTime - sendTime),null,startTime);
            return;
        }
        




        //parse response

        
        cihStub.UpdateInvoicePriceResponse stubResponse = new cihStub.UpdateInvoicePriceResponse();    //lclc delete later and change responsefake to response:
        //String responsefake='http://www.mocky.io/v2/56f0667410000050018ef23b';
        
        try{
            //stubResponse = (cihStub.UpdateInvoicePriceResponse) json.deserialize(responsefake, cihStub.UpdateInvoicePriceResponse.class);
             
            stubResponse=(cihStub.UpdateInvoicePriceResponse) json.deserialize(response, cihStub.UpdateInvoicePriceResponse.class);
            System.debug('lclc stubResponse '+stubResponse);
            
        }
        catch(exception ex){
            System.debug('2 ERROR IN RESPONSESTUB ---- '+ex);
            messageQueueHelper cont=new messageQueueHelper();
            cont.generalFail(omq, ex, quoteRecord, null,startTime);
            return;         
        }
        System.debug('lclc errorcode '+stubResponse.inputHeaders.ERRORCODE);
        System.debug('lclc errortext '+stubResponse.inputHeaders.ERRORTEXT);
    
 //        public List<QuoteLineItem> nopricelist {get; set;} 
 //       nopricelist = new List<QuoteLineItem>();
        
        try {
            for(cihStub.CreditMemoLineItemsResponse res: stubResponse.body.lines)  {
                if(res.invoicePrice!=null && decimal.valueof(res.invoicePrice) > 0){
                    QuoteLineItem cmp=itemNumberQLIMap.get(res.itemNumberSdDoc);
                    cmp.Valid_InvoicePrice__c=true;
                    qliToUpdate.add(cmp);
                    System.debug('Invoice Price : ' + res.materialCode + ' invoice price:' + res.invoicePrice);
                } else {
                    QuoteLineItem cmp=itemNumberQLIMap.get(res.itemNumberSdDoc);
                    cmp.Valid_InvoicePrice__c=false;
                    qliToUpdate.add(cmp);
                    System.debug('NULL of Invoice Price : ' + res.materialCode); 
                }
            }
        } catch(Exception ex) {
                    String errmsg = 'ERROR='+ex.getmessage() + ' - '+ex.getStackTraceString();
            messageQueueHelper cont=new messageQueueHelper();
            cont.failed(omq, ex, quoteRecord, errmsg,null);
            
            //quoteRecord.addError('Fail to handle response data. - '+ex.getmessage()+'. Please contact System Administrator.');
            return;
        }   
        
        
            try{
            String tempstring='';

            if(!qliToUpdate.isEmpty()){
                System.debug('lclc cmpToUpdate.size() '+qliToUpdate.size());
                List<Database.SaveResult> uResultsCMP=Database.Update(qliToUpdate ,false);

                for(Database.SaveResult sr:uResultsCMP){
                    System.debug('lclc in sr '+sr);
                    if(!sr.isSuccess()){
                        errCount=errCount+1;
                        tempstring = tempstring + ' ***** ' + string.valueof(sr.getErrors());
                    }
                }
            }

            if(tempstring!='' || qliToUpdate.isEmpty() ){
                handleError(omq,stubResponse,null);
                return;
            }else{
                handleSuccess(omq,stubResponse);
                return;    
            }
        }catch(exception ex){
            handleError(omq,stubResponse,ex.getMessage());
            return;
        }
        System.debug('lclc  hmm');
        
    }
    
        public static void handleError(message_queue__c mq, cihStub.UpdateInvoicePriceResponse res,String otherError){
        System.debug('lclc in error '+mq);
        mq.retry_counter__c += 1;
        mq.status__c = mq.retry_counter__c > 5 ? 'failed' : 'failed-retry';
        if(res!=null && res.inputHeaders!=null){
            mq.ERRORTEXT__c=res.inputHeaders.ERRORTEXT;
            if(otherError!=null){
                mq.errortext__c+=otherError;
            }
            mq.ERRORCODE__c=res.inputHeaders.ERRORCODE;
        }else{
            mq.errortext__c=otherError;
        }
        upsert mq;

        if(mq.Status__c=='failed'){
            ///what to do if positive
            Credit_Memo__c cm=[select id, Integration_Status__c from Credit_Memo__c where id=:mq.Identification_Text__c];
            cm.Integration_Status__c='Error';
            update cm;
        }
    }

    public static void handleSuccess(message_queue__c mq, cihStub.UpdateInvoicePriceResponse res){
        System.debug('lclc in success ');
        
        mq.status__c = 'success';
        mq.MSGSTATUS__c = res.inputHeaders.MSGSTATUS;
        mq.MSGUID__c = res.inputHeaders.msgGUID;
        mq.IFID__c = res.inputHeaders.ifID;
        mq.IFDate__c = res.inputHeaders.ifDate;      
        mq.ERRORCODE__c='Test';
        mq.ERRORTEXT__c=null;      
        upsert mq;
        System.debug('4:' + mq.id + ' status :' + mq.status__c);
        // if(!Test.isRunningTest())
        // {
        //     ///what to do if positive
        //     Credit_Memo__c cm=[select id, Integration_Status__c from Credit_Memo__c where id=:mq.Identification_Text__c];
        //     cm.Integration_Status__c='Success';
        //     SYstem.debug('lclc about to update cm '+cm.Integration_Status__c+' '+cm.id);
        //     update cm;
        //     SYstem.debug('lclc just update cm '+cm.Integration_Status__c+' '+cm.id);
        // }
    }
    
    
    /**********************************HELPER**********************************/
    public static string webcallout(String bodyVar, string endpoint){
        
        string partialEndpoint = Test.isRunningTest() ? '' : Integration_EndPoints__c.getInstance('Flow-008').partial_endpoint__c;
        if(!test.isRunningTest()){
            return RoiIntegrationHelper.webcallout( bodyVar, partialEndpoint);
        }else{
            return null;
        }
        

    }

}