public with sharing class QuoteLineItemRerouteToOppLineItem {
	
	public Quote quoteObj{get;set;}
	public String quoteId{get;set;}

	public QuoteLineItemRerouteToOppLineItem(ApexPages.StandardController controller) {

		if(!test.isRunningTest()){
            controller.addFields(new List<String> {'Opportunity'});
        }
		quoteObj=(Quote)controller.getRecord();
		if(quoteObj!=null){
			quoteId=quoteObj.Id;
		}
		
	}

	public PageReference doRedirect(){
		return new PageReference('/apex/opportunityProductEntry?id=' + quoteObj.OpportunityId +'&quoteId='+quoteId);
	}
}