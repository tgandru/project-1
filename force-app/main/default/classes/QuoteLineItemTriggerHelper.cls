/* 
Helper class for Quote line item trigger
Author: Jagan
*/

public class QuoteLineItemTriggerHelper{
    
    public static boolean beforeInsertUpdate = FALSE;
    
    public static void beforeInsertUpdate(List<QuoteLineItem> triggernew,Map<Id,QuoteLineItem> triggerOldmap){
        
        //if(!beforeInsertUpdate){
            //beforeInsertUpdate = TRUE;
        
            Set<Id> qliidset = new set<Id>();
            Set<Id> pbidset = new Set<Id>();
            Set<Id> quoteIdset = new Set<Id>();
            Set<Id> prodIdset = new Set<Id>();
            Set<id> dupcheck = new Set<Id>();
            List<Quote> quotesToupdate = new List<Quote>();
            Set<Id> oppidset = new Set<Id>();
            for(QuoteLineItem qli : triggernew){
                qliidset.add(qli.id);
                pbidset.add(qli.PriceBookEntryId);
                quoteIdset.add(qli.quoteId);
                prodIdset.add(qli.Product2Id);
            }
            
            Map<Id,PriceBookEntry> pbmap = new Map<Id,PriceBookEntry>([select id,UnitPrice from PriceBookEntry where id in:pbidset]);
            Map<Id,Quote> quoteMap = new Map<Id,Quote>([select id,OpportunityId,status,Opportunity.IsClosed,ProductgroupMSP__c,Division__C from Quote where id in:quoteIdset]);
            
            for(Quote q: quoteMap.values())
                oppidset.add(q.OpportunityId);
            
            Map<Id,OpportunityLineItem> oliMap = new Map<Id,OpportunityLineitem>([select id,Product2Id, Unit_Cost__c,Description,ProductType__c,SAP_Material_ID__c from OpportunityLineItem where OpportunityId in :oppidset]);
            //To re arrange the map with key as prod id
            Map<Id,OpportunityLineItem> oliProdMap = new Map<Id,OpportunityLineitem>();
            for(OpportunityLineItem oli: oliMap.values())
                oliProdMap.put(oli.Product2Id,oli);
            
            Map<id,Product2> prodMap = new Map<Id,Product2>([select id,Unit_Cost__c,Description,Product_Type__c,SAP_Material_ID__c from Product2 where id in:prodIdset]);
            
            for(QuoteLineItem qli : triggernew){
                
                system.debug('Quantity....'+qli.Quantity+'...Pricebookentry id...'+qli.PriceBookEntry);
               
                /* Update List Price and Total Requested Price */
                qli.Total_List_Price__c = (pbmap!=null && qli.PriceBookEntryId !=null && pbmap.containsKey(qli.PriceBookEntryId) && qli.Quantity != null && pbmap.get(qli.PriceBookEntryId).UnitPrice!=null ? qli.Quantity*pbmap.get(qli.PriceBookEntryId).UnitPrice : 0);
                qli.Total_Requested_Price__c = (qli.Quantity!=null && qli.Requested_Price__c!=null ? qli.Quantity*qli.Requested_Price__c : 0);
                //INSERTED LINES 41 - 60 by PMRAZ  values need to calculated each time
                //Update discount
                        if(qli.PriceBookEntryId !=null && pbmap.containsKey(qli.PriceBookEntryId) && pbmap.get(qli.PriceBookEntryId).UnitPrice != null && qli.Requested_Price__c!=null && qli.Requested_Price__c < pbmap.get(qli.PriceBookEntryId).UnitPrice){
                            //qli.Discount__c = ((1-(qli.Requested_Price__c /pbmap.get(qli.PriceBookEntryId).UnitPrice))*100);//Commented by Thiru--09/05/2019
                            //Added by Thiru--09/05/2019--Starts
                            Decimal discPercent = ((1-(qli.Requested_Price__c /pbmap.get(qli.PriceBookEntryId).UnitPrice))*100);
                            discPercent = discPercent.setScale(2, RoundingMode.HALF_UP);
                            qli.Discount__c = discPercent;
                            //Added by Thiru--09/05/2019--Starts--Ends
                            
                            System.debug('lclc in discount 1');
                        }else if(qli.PriceBookEntryId !=null && pbmap.containsKey(qli.PriceBookEntryId) &&  pbmap.get(qli.PriceBookEntryId).UnitPrice != null && qli.Requested_Price__c!=null && qli.Requested_Price__c >= pbmap.get(qli.PriceBookEntryId).UnitPrice ){
                            qli.Discount__c=0;
                            System.debug('lclc in discount 2');
                        }
                //Update GM 
                        if(qli.Unit_Cost__c!= null && qli.Requested_Price__c != null && qli.Requested_Price__c != 0){
                            qli.GM__c = (1-(qli.Unit_Cost__c /qli.Requested_Price__c))*100; 
                            system.debug('qli.GM__c '+qli.GM__c );
                        }
                //set Sales Price as requested price
                        qli.UnitPrice = qli.Requested_Price__c;
                //set unit cost/description/product type and sap material id from related product
                system.debug('Qli unit cost----->'+qli.Unit_Cost__c);
                system.debug('req price----->'+qli.Requested_Price__c);
                Quote temp1 = new Quote(id=qli.QuoteId);
                        if(qli.Product2Id!= null && prodMap!=null && oliProdMap.containsKey(qli.Product2Id)){
                            if((quoteMap.get(qli.QuoteId).Status == 'Approved' || quoteMap.get(qli.QuoteId).Status == 'Presented' || quoteMap.get(qli.QuoteId).Status == 'Rejected') 
                                && triggerOldMap!=null 
                                && triggerOldmap.get(qli.id).Requested_Price__c == qli.Requested_Price__c 
                                && triggerOldmap.get(qli.id).Quantity == qli.Quantity
                                && (triggerOldmap.get(qli.id).Unit_Cost__c <> qli.Unit_Cost__c)){
                                //qli.Unit_Cost__c = triggerOldmap.get(qli.id).Unit_Cost__c;
                                qli.Reset_Approval__c = TRUE;
                                temp1.status = Label.DraftStatus;
                                temp1.Integration_Status__c=null;
                                if(!dupcheck.contains(qli.QuoteId)){
                                    quotesToupdate.add(temp1);  
                                    dupcheck.add(qli.QuoteId);
                                }
                            }
                            //Added by Thiru 04/19/2018----for updating the updated OLI descrition to QLI 
                            //----Starts here---
                            if(quoteMap.get(qli.QuoteId).Division__c != 'SBS'){
                                qli.Description = oliProdMap.get(qli.Product2Id).Description;  
                            }
                            //----Ends here---
                            //qli.Description = oliProdMap.get(qli.Product2Id).Description;
                            qli.Product_Type__c = oliProdMap.get(qli.Product2Id).ProductType__c;
                            qli.SAP_Material_ID__c = oliProdMap.get(qli.Product2Id).SAP_Material_ID__c ;
                        }
                        
                if( (quoteMap.get(qli.QuoteId).Status == 'Approved' || quoteMap.get(qli.QuoteId).Status == 'Presented' || quoteMap.get(qli.QuoteId).Status == 'Rejected') &&
                    ( triggerOldMap == null || 
                      (triggerOldMap!=null && (triggerOldmap.get(qli.id).Requested_Price__c <> qli.Requested_Price__c || triggerOldmap.get(qli.id).Quantity <> qli.Quantity))
                    )
                  )
                {
                    /*Various actions for open opportunity quote*/ 
                    if(!quoteMap.get(qli.QuoteId).Opportunity.IsClosed){    
                        System.debug('lclc in 1');
                        
                        //Update discount
                        if(qli.PriceBookEntryId !=null && pbmap.containsKey(qli.PriceBookEntryId) && pbmap.get(qli.PriceBookEntryId).UnitPrice != null && qli.Requested_Price__c!=null && qli.Requested_Price__c < pbmap.get(qli.PriceBookEntryId).UnitPrice){
                            //qli.Discount__c = ((1-(qli.Requested_Price__c /pbmap.get(qli.PriceBookEntryId).UnitPrice))*100);//Commented by Thiru--09/05/2019
                            //Added by Thiru--09/05/2019--Starts
                            Decimal discPercent = ((1-(qli.Requested_Price__c /pbmap.get(qli.PriceBookEntryId).UnitPrice))*100);
                            discPercent = discPercent.setScale(2, RoundingMode.HALF_UP);
                            qli.Discount__c = discPercent;
                            //Added by Thiru--09/05/2019--Starts--Ends
                            System.debug('lclc in discount 1');
                        }else if(qli.PriceBookEntryId !=null && pbmap.containsKey(qli.PriceBookEntryId) &&  pbmap.get(qli.PriceBookEntryId).UnitPrice != null && qli.Requested_Price__c!=null && qli.Requested_Price__c >= pbmap.get(qli.PriceBookEntryId).UnitPrice ){
                            qli.Discount__c=0;
                            System.debug('lclc in discount 2');
                        }
                        
                        //Update GM
                       
                        
                        //Reset approval flag only when new quantity/price is less than the original one for T16598
                        
                        Quote temp = new Quote(id=qli.QuoteId);
                        
                        
                        if( triggerOldMap == null || 
                            (triggerOldMap!=null && (triggerOldmap.get(qli.id).Requested_Price__c > qli.Requested_Price__c || triggerOldmap.get(qli.id).Quantity > qli.Quantity))
                        ) {
                            //Reset approval flag
                            qli.Reset_Approval__c = TRUE;
                            //Update quote status as Draft (T20863)
                            temp.status = Label.DraftStatus;
                            temp1.Integration_Status__c=null;
                            
                           
                        }
                        
                        string div=quoteMap.get(qli.QuoteId).Division__C;
                            if(triggerOldMap == null && div=='W/E' ){
                                qli.Reset_Approval__c = false;
                                temp.status = Label.ApprovedStatus;
                            }
                        
                        //set Sales Price as requested price
                        qli.UnitPrice = qli.Requested_Price__c;
                        
                        //set unit cost/description/product type and sap material id from related product
                        if(qli.Product2Id!= null && prodMap!=null && oliProdMap.containsKey(qli.Product2Id)){
                            //qli.Unit_Cost__c = oliProdMap.get(qli.Product2Id).Unit_Cost__c;
                            //Added by Thiru 04/19/2018----for updating the updated OLI descrition to QLI 
                            //----Starts here---
                            if(quoteMap.get(qli.QuoteId).Division__c != 'SBS'){
                                qli.Description = oliProdMap.get(qli.Product2Id).Description;  
                            }
                            //----Ends here---
                            //qli.Description = oliProdMap.get(qli.Product2Id).Description;
                            qli.Product_Type__c = oliProdMap.get(qli.Product2Id).ProductType__c;
                            qli.SAP_Material_ID__c = oliProdMap.get(qli.Product2Id).SAP_Material_ID__c ;
                        }
                        system.debug('Qli unit cost----->'+qli.Unit_Cost__c);
                        
                        
                        //Nullify quote fields if product is any of the below
                        if(qli.ProductGroup__c== Label.ProductGroupA3Copier || qli.ProductGroup__c== Label.ProductGroupFaxMFP || 
                            qli.ProductGroup__c== Label.ProductGroupPrinter || qli.ProductGroup__c== Label.ProductGroupSolution )
                        {
                            temp.BEP__c = null;
                            temp.Initial_Profit__c = null;
                            temp.Set_Loss__c = null;
                            temp.Set_Marginal_Profit_Rate__c = null;
                            temp.Total_Profit__c = null;
                        }
                        //update quote
                        if(!dupcheck.contains(qli.QuoteId)){
                            quotesToupdate.add(temp);  
                            dupcheck.add(qli.QuoteId);
                        }
                        
                    }
                    //Update discount for a closed opportuntiy
                    else if(quoteMap.get(qli.QuoteId).Opportunity.IsClosed){
                        if(qli.Requested_Price__c!=null && qli.Closed_List_Price__c != null && qli.Closed_List_Price__c !=0 && qli.Requested_Price__c < qli.Closed_List_Price__c){
                            //qli.Discount__c = ((1-(qli.Requested_Price__c /qli.Closed_List_Price__c))*100);//Commented by Thiru--09/05/2019
                            //Added by Thiru--09/05/2019--Starts
                            Decimal discPercent = ((1-(qli.Requested_Price__c /pbmap.get(qli.PriceBookEntryId).UnitPrice))*100);
                            discPercent = discPercent.setScale(2, RoundingMode.HALF_UP);
                            qli.Discount__c = discPercent;
                            //Added by Thiru--09/05/2019--Starts--Ends
                        }else if(qli.Requested_Price__c!=null && qli.Closed_List_Price__c != null && qli.Closed_List_Price__c !=0 && qli.Requested_Price__c >= qli.Closed_List_Price__c){
                            qli.Discount__c=0;
                        }
                    } 
                   
                }   
               
            }
            
            if(quotesToupdate.size()>0)
                database.Update(quotesToupdate,false);
        //}
    }
    
    //Method to update "Service Min GM" based on the custom and Standard product. "Service Min Gm" is used in routing the Approval Path
    //Aded by Thiru---05/09/2018
    public static void QuoteServiceMinGMUpdate(Map<id,QuoteLineItem> triggerNewMap){
        Set<id> QuoteIds = new Set<id>();
        List<Quote> UpdateQuotes = new List<Quote>();
        for(Quotelineitem qli : triggerNewMap.values()){
            QuoteIds.add(qli.quoteid);
        }
        
        Map<String,SKUs_Exclude_Service_Min_GM_Calculation__c> ExcludeSKUMap = SKUs_Exclude_Service_Min_GM_Calculation__c.getAll();
        Set<String> ExcludeSKUs = new Set<String>();
        for(SKUs_Exclude_Service_Min_GM_Calculation__c c : SKUs_Exclude_Service_Min_GM_Calculation__c.getAll().values()){
            ExcludeSKUs.add(c.name);
        }
        
        List<quotelineitem> standardqli = new List<quotelineitem>();
        List<quotelineitem> customqli = new List<quotelineitem>();
        List<quotelineitem> AutoApprovalQli = new List<quotelineitem>();
        
        for(Quote qt : [select id,Service_MIN_GM__c,SBS_Auto_Approval__c,Deal_Desk_Approval__c,(Select id,product2.custom_product__c,GM__c,Discount__c,product2.SAP_Material_ID__c from Quotelineitems) from Quote where id=:QuoteIds and Division__c='SBS']){
            
            for(quotelineitem qli : qt.Quotelineitems){
                system.debug('SKU------------>'+qli.product2.SAP_Material_ID__c);
                if(!ExcludeSKUs.contains(qli.product2.SAP_Material_ID__c)){
                    system.debug('Entered SKU------------>'+qli.product2.SAP_Material_ID__c);
                    if(qli.product2.custom_product__c==FALSE && qli.Discount__c==0){
                        AutoApprovalQli.add(qli);
                    }else if(qli.product2.custom_product__c==FALSE && qli.Discount__c>0){
                        standardqli.add(qli);
                    }else if(qli.product2.custom_product__c==True){
                        customqli.add(qli);
                    }
                }
            }
            List<Decimal> GMList = new List<Decimal>();
            if(standardqli.size()==0 && customqli.size()==0 && AutoApprovalQli.size()==0){
                qt.Deal_Desk_Approval__c = true;
                qt.SBS_Auto_Approval__c=false;
            }else{
                qt.Deal_Desk_Approval__c = false;
                if(standardqli.size()>0 && customqli.size()==0){
                    system.debug('<--------Auto Approved Standard-------->');
                    for(quotelineitem qli: standardqli){
                        GMList.add(qli.GM__c);
                    }
                }else if(standardqli.size()==0 && customqli.size()>0){
                    system.debug('<--------Custom-------->');
                    for(quotelineitem qli: customqli){
                        GMList.add(qli.GM__c);
                    }
                }else if(standardqli.size()>0 && customqli.size()>0){
                    system.debug('<--------Standard and Custom-------->');
                    for(quotelineitem qli: standardqli){
                        GMList.add(qli.GM__c);
                    }
                    for(quotelineitem qli: customqli){
                        GMList.add(qli.GM__c);
                    }
                }
            }
            if(GMList.size()>0){
                GMList.sort();//Sorts the list values in ascending order
                system.debug('Service Min GM-------->'+GMList[0]);
                qt.Service_MIN_GM__c = GMList[0];
            }
            system.debug('AutoApprovalQli------>'+AutoApprovalQli.size());
            system.debug('standardqli------->'+standardqli.size());
            system.debug('customqli------->'+customqli.size());
            if(AutoApprovalQli.size()>0 && standardqli.size()==0 && customqli.size()==0){
                qt.SBS_Auto_Approval__c=true;
                system.debug('Auto Approval=true');
            }else{
                qt.SBS_Auto_Approval__c=false;
                system.debug('Auto Approval=false');
            }
            UpdateQuotes.add(qt);
            standardqli.clear();
            customqli.clear();
            AutoApprovalQli.clear();
        }
        if(UpdateQuotes.size()>0){
            update UpdateQuotes;
        }
    }
    
    //Method to update Mobile Quote "Wearable Product Count" used in the Approval Path
    //Added by Thiru---05/14/2018
    public static void QuoteWearableCountUpdate(Map<id,QuoteLineItem> triggerNewMap){
        Set<id> QuoteIds = new Set<id>();
        List<Quote> UpdateQuotes = new List<Quote>();
        for(Quotelineitem qli : triggerNewMap.values()){
            QuoteIds.add(qli.quoteid);
        }
        List<quotelineitem> WearableQli = new List<quotelineitem>();
        for(Quote qt : [select id,division__c,Wearable_product_count__c,(Select id,product2.SAP_product__c from Quotelineitems) from Quote where id=:QuoteIds]){
            if(qt.division__c=='Mobile'){
                for(quotelineitem qli : qt.Quotelineitems){
                    if(qli.product2.SAP_product__c=='WEARABLE'){
                        WearableQli.add(qli);
                    }
                }
                if(WearableQli.size()>0){
                    qt.Wearable_product_count__c=WearableQli.size();
                    UpdateQuotes.add(qt);
                    WearableQli.clear();
                }else{
                    qt.Wearable_product_count__c=0;
                    UpdateQuotes.add(qt);
                }
            }
        }
        if(UpdateQuotes.size()>0){
            update UpdateQuotes;
        }
    }
    
}