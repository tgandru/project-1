public class QuotePDFGeneratorController {
Public Quote qt {get; set;}
Public String templateId {get; set;}
Public String selectedTemplateSBS {get; set;}
Public decimal size {get; set;}
Public Boolean showpdf {get; set;}
Public Boolean showerror {get; set;}
Public Boolean showtemplateSelection {get; set;}
public String pdf {get; set;}
    public id quoteId {get; set;}
Transient blob pdfblob {get; set;}
    public List<SelectOption> SBSTemplates {get; set;}

    public QuotePDFGeneratorController(ApexPages.StandardController controller) {
           showpdf =True;
           showtemplateSelection =false;
           showerror=false;
           templateId ='';
           selectedTemplateSBS='';
           pdfblob=null;
           quoteId =  controller.getId();//ApexPages.currentPage().getParameters().get('id');
           
           SBSTemplates = new List<SelectOption>();

        SBSTemplates.add(new SelectOption('Customer Quote PDF', 'Customer Quote PDF'));
        SBSTemplates.add(new SelectOption('Partner Quote PDF', 'Partner Quote PDF'));
        if(quoteId != null) {
         qt=[Select id,Name,Division__c, Status,Distributor__c,Pricebook_Name__c,Is_Approved__c from Quote Where id=:quoteId ];
         
          if(qt.Is_Approved__c ==false && !Test.isRunningTest()){
             apexpages.addMessage(new ApexPages.message(Apexpages.Severity.Error,'The Quote is not yet Approved.'));
             showpdf =false;
             showerror=true;
             
          }else if(qt.Distributor__c==null){
              apexpages.addMessage(new ApexPages.message(Apexpages.Severity.Error,'The Quote doesn\'t have any Distributors/Resellers on it. Please add them before generating the quote.'));
              showpdf =false;
              showerror=true;
          }else{
            
              if(qt.Division__c=='IT'){
                 Quote_Template_Mapping__mdt template=[Select id,Label,Division__c,Template_ID__c,Header_Size__c  from Quote_Template_Mapping__mdt where Division__c='IT'];
                  templateId =template.Template_ID__c;
                  size=template.Header_Size__c;
              } else if(qt.Division__c=='W/E'){
                 Quote_Template_Mapping__mdt template=[Select id,Label,Division__c,Template_ID__c,Header_Size__c  from Quote_Template_Mapping__mdt where Division__c='W/E'];
                  templateId =template.Template_ID__c;
                  size=template.Header_Size__c;
              } else if(qt.Division__c=='Mobile'){
                  List<QuoteLineItem> qlis=new List<QuoteLineItem >([SELECT SAP_Material_ID__c,Product2.Product_Group__c FROM QuoteLineItem WHERE QuoteId =:quoteId ]);
                  boolean smartphoneInculeded=false;
                  for(QuoteLineItem q:qlis){
                      if(q.Product2.Product_Group__c=='SMART_PHONE'){
                          smartphoneInculeded=true;
                          break;
                      }
                  }
                  
                  if(smartphoneInculeded){
                      Quote_Template_Mapping__mdt template=[Select id,Label,Division__c,Template_ID__c,Header_Size__c  from Quote_Template_Mapping__mdt where Label='Mobile Template - SMART_PHONE'];
                  templateId =template.Template_ID__c;
                  size=template.Header_Size__c;
                  }else{
                      Quote_Template_Mapping__mdt template=[Select id,Label,Division__c,Template_ID__c,Header_Size__c  from Quote_Template_Mapping__mdt where Label='Mobile Template'];
                  templateId =template.Template_ID__c;
                  size=template.Header_Size__c;
                  }
              }else if(qt.Division__c=='SBS'){
                  
                  if(qt.Pricebook_Name__c == 'SBS Indirect' || qt.Pricebook_Name__c == 'SDSA SBS'){
                       List<QuoteLineItem> qlis=new List<QuoteLineItem>([SELECT SAP_Material_ID__c,Product2.Product_Group__c FROM QuoteLineItem WHERE QuoteId =:quoteId ]);
                       Set<string> skus=new set<String>();
                       for(QuoteLineItem q: qlis){
                         skus.add(q.SAP_Material_ID__c);  
                       }
                       
                       for(SBS_External_Quote_Template_Products__mdt s:[SELECT Label FROM SBS_External_Quote_Template_Products__mdt]){
                           if(skus.contains(s.label)){
                               showtemplateSelection=true;
                                showpdf =false;
                                break;
                           }
                       }
                       
                       if(showtemplateSelection==false){
                            Quote_Template_Mapping__mdt template=[Select id,Label,Division__c,Template_ID__c,Header_Size__c  from Quote_Template_Mapping__mdt where Label='SBS Indirect Template'];
                           templateId =template.Template_ID__c;
                           size=template.Header_Size__c;
                           
                       }
                       

                  }else{
                       Quote_Template_Mapping__mdt template=[Select id,Label,Division__c,Template_ID__c,Header_Size__c  from Quote_Template_Mapping__mdt where Label='SBS Indirect Template'];
                       templateId =template.Template_ID__c;
                       size=template.Header_Size__c;
                  
                  }

              }
          
          
             
             if(templateId !=null && templateId !=''){
                 String quoteUrl = '/quote/quoteTemplateDataViewer.apexp?id=';
                quoteUrl +=qt.id ;            
                quoteUrl +='&headerHeight='+size+'&footerHeight='+size+'&summlid=';            
                quoteUrl +=templateId;            
                quoteUrl +='#toolbar=1&navpanes=0&zoom=90';
                    
                   PageReference pr = new PageReference(quoteUrl); 
        
                    QuoteDocument qd = new QuoteDocument(); 
                     Blob b;
                       if(Test.isRunningTest()) {
                          b=blob.valueOf('Unit.Test');
                       }else{
                           b=  pr.getContentAsPDF();
                       }
                    
                   
                    pdfblob=b;
                    pdf= EncodingUtil.Base64Encode(b);
                 
             }
             
          
          }
          
          
          
        }
           
    }
    
    
    Public PageReference SBSTemplateDispay(){
        if(selectedTemplateSBS !=null && selectedTemplateSBS !=''){
               if(selectedTemplateSBS=='Customer Quote PDF'){
                    String quoteUrl = '/apex/QuoteCustomPdfView?Id=';
                    quoteUrl +=qt.id ;            
                    
                    
                   PageReference pr = new PageReference(quoteUrl); 
        
                    QuoteDocument qd = new QuoteDocument(); 
                    Blob b;
                       if(Test.isRunningTest()) {
                          b=blob.valueOf('Unit.Test');
                       }else{
                           b=  pr.getContentAsPDF();
                       }
                    pdfblob=b;
                    pdf= EncodingUtil.Base64Encode(b); 
                    showtemplateSelection=false;
                    showpdf=true;
               }else{
                  Quote_Template_Mapping__mdt template=[Select id,Label,Division__c,Template_ID__c,Header_Size__c  from Quote_Template_Mapping__mdt where Label='SBS Indirect Template'];
                       templateId =template.Template_ID__c;
                       size=template.Header_Size__c;
                       
                  String quoteUrl = '/quote/quoteTemplateDataViewer.apexp?id=';
                quoteUrl +=qt.id ;            
                quoteUrl +='&headerHeight='+size+'&footerHeight='+size+'&summlid=';            
                quoteUrl +=templateId;            
                quoteUrl +='#toolbar=1&navpanes=0&zoom=90';
                    
                   PageReference pr = new PageReference(quoteUrl); 
        
                    QuoteDocument qd = new QuoteDocument(); 
                    Blob b;
                       if(Test.isRunningTest()) {
                          b=blob.valueOf('Unit.Test');
                       }else{
                           b=  pr.getContentAsPDF();
                       }
                    
                    pdfblob=b;
                    pdf= EncodingUtil.Base64Encode(b);  
                    showtemplateSelection=false;
                    showpdf=true;
                   
               }
        }else{
            apexpages.addMessage(new ApexPages.message(Apexpages.Severity.Error,'Please Select Template.'));
             showpdf =false;
        }
        
        return null;
    }
    
        public PageReference saveQuoteAsPDFandEmail()
{       
  
           String quoteUrl = '/quote/quoteTemplateDataViewer.apexp?id=';
                quoteUrl +=qt.id ;            
                quoteUrl +='&headerHeight='+size+'&footerHeight='+size+'&summlid=';            
                quoteUrl +=templateId;            
                quoteUrl +='#toolbar=1&navpanes=0&zoom=90';
                    
                   PageReference pr = new PageReference(quoteUrl); 
        
                    QuoteDocument qd = new QuoteDocument(); 
                    Blob b;
                       if(Test.isRunningTest()) {
                          b=blob.valueOf('Unit.Test');
                       }else{
                           b=  pr.getContentAsPDF();
                       }
              
             qd.Document = b;
             qd.QuoteId = qt.id;
               insert qd;     
   String emailURL = '/_ui/core/email/author/EmailAuthor?p3_lkid=' + qt.id + '&doc_id=' + qd.Id + '&retURL=%2F' + qt.id;
   
   System.debug('Quote Email URL: ' + emailURL);
   PageReference quotePage = new PageReference(emailURL);
   quotePage.setRedirect(true);
   return quotePage;       
}

public PageReference saveQuoteAsPDF()
{  
         if(templateId !=null && templateId !=''){
             String quoteUrl = '/quote/quoteTemplateDataViewer.apexp?id=';
                quoteUrl +=qt.id ;            
                quoteUrl +='&headerHeight='+size+'&footerHeight='+size+'&summlid=';            
                quoteUrl +=templateId;            
                quoteUrl +='#toolbar=1&navpanes=0&zoom=90';
                    
                   PageReference pr = new PageReference(quoteUrl); 
                   Blob b;
                   if(Test.isRunningTest()) {
                      b=blob.valueOf('Unit.Test');
                   }else{
                       b=  pr.getContentAsPDF();
                   }

                  
                   QuoteDocument qd = new QuoteDocument(); 
                   qd.Document = b;
                   qd.QuoteId = qt.id;
                    insert qd;  
         }else if(selectedTemplateSBS=='Customer Quote PDF'){
                    String quoteUrl = '/apex/QuoteCustomPdfView?Id=';
                    quoteUrl +=qt.id ;            
                    
                    
                    PageReference pr = new PageReference(quoteUrl); 
                     QuoteDocument qd = new QuoteDocument(); 
                       Blob b;
                       if(Test.isRunningTest()) {
                          b=blob.valueOf('Unit.Test');
                       }else{
                           b=  pr.getContentAsPDF();
                       }
                       qd.Document = b;
                       qd.QuoteId = qt.id;
                        insert qd; 
                 
         }
                 
                 
                   
          

   //upon completion redirect to quote
          
           PageReference quotePage = new PageReference('/apex/CustomRefreshPage?Id=' + quoteId );
            quotePage.setRedirect(true);
        
           return quotePage;
}
    
    
    public void saveQuoteAsPDF2()
{  
         if(templateId !=null && templateId !=''){
             String quoteUrl = '/quote/quoteTemplateDataViewer.apexp?id=';
                quoteUrl +=qt.id ;            
                quoteUrl +='&headerHeight='+size+'&footerHeight='+size+'&summlid=';            
                quoteUrl +=templateId;            
                quoteUrl +='#toolbar=1&navpanes=0&zoom=90';
                    
                   PageReference pr = new PageReference(quoteUrl); 
                     Blob b;
                   if(Test.isRunningTest()) {
                      b=blob.valueOf('Unit.Test');
                   }else{
                       b=  pr.getContentAsPDF();
                   }
                   QuoteDocument qd = new QuoteDocument(); 
                   qd.Document = b;
                   qd.QuoteId = qt.id;
                    insert qd;  
         }else if(selectedTemplateSBS=='Customer Quote PDF'){
                    String quoteUrl = '/apex/QuoteCustomPdfView?Id=';
                    quoteUrl +=qt.id ;            
                    
                    
                    PageReference pr = new PageReference(quoteUrl); 
                     QuoteDocument qd = new QuoteDocument(); 
                       Blob b;
                           if(Test.isRunningTest()) {
                              b=blob.valueOf('Unit.Test');
                           }else{
                               b=  pr.getContentAsPDF();
                           }
                       qd.Document = b;
                       qd.QuoteId = qt.id;
                        insert qd; 
                 
         }

}
    
    

}