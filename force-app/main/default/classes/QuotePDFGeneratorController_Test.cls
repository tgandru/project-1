@isTest(seeAllData=true)
private class QuotePDFGeneratorController_Test {

	
      @isTest static void test_method_one() {
            Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
           Quote quot=[Select Id,Name,Division__c, Status,Distributor__c,Pricebook_Name__c,Is_Approved__c from Quote where Status='Approved' and Division__c='IT' limit 1];
            
          Test.startTest();
                apexPages.standardcontroller sc = new apexPages.standardcontroller(quot);
              QuotePDFGeneratorController obj = new QuotePDFGeneratorController(sc);
          Test.stopTest();
      }
      
       @isTest static void test_method_two() {
            Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
           Quote quot=[Select Id,Name,Division__c, Status,Distributor__c,Pricebook_Name__c,Is_Approved__c from Quote where Status='Approved' and Division__c='IT' limit 1];
          Test.startTest();
                apexPages.standardcontroller sc = new apexPages.standardcontroller(quot);
              QuotePDFGeneratorController obj = new QuotePDFGeneratorController(sc);
              obj.templateId='0EH36000002S0tj';
              obj.size=100;
              obj.saveQuoteAsPDF();
              obj.saveQuoteAsPDF2();
              obj.saveQuoteAsPDFandEmail();
              
              PageReference VFPage = Page.QuoteSendEmail;
         Test.setCurrentPageReference(VFPage); 
         ApexPages.currentPage().getParameters().put('Id',quot.id);
              
                                          QuoteSendEmailRedirectController sc2=new QuoteSendEmailRedirectController(sc);

          Test.stopTest();
      }
      
        @isTest static void test_method_three() {
            Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
              Quote quot=[Select Id,Name,Division__c, Status,Distributor__c,Pricebook_Name__c,Is_Approved__c from Quote where Status='Approved' and Division__c='IT' limit 1];
          Test.startTest();
                apexPages.standardcontroller sc = new apexPages.standardcontroller(quot);
              QuotePDFGeneratorController obj = new QuotePDFGeneratorController(sc);
              obj.templateId=null;
              obj.selectedTemplateSBS='Customer Quote PDF';
              obj.size=100;
              obj.saveQuoteAsPDF();
              obj.saveQuoteAsPDF2();
              obj.SBSTemplateDispay();

          Test.stopTest();
      }
      
       @isTest static void test_method_four() {
            Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
            Quote quot=[Select Id,Name,Division__c, Status,Distributor__c,Pricebook_Name__c,Is_Approved__c from Quote where Status='Approved' and Division__c='IT' limit 1];
          Test.startTest();
                apexPages.standardcontroller sc = new apexPages.standardcontroller(quot);
              QuotePDFGeneratorController obj = new QuotePDFGeneratorController(sc);
              obj.templateId=null;
            
              obj.SBSTemplateDispay();
          Test.stopTest();
      }
         @isTest static void test_method_five() {
            Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
           Quote quot=[Select Id,Name,Division__c, Status,Distributor__c,Pricebook_Name__c,Is_Approved__c from Quote where Status='Approved' and Division__c='Mobile' limit 1];
          Test.startTest();
                apexPages.standardcontroller sc = new apexPages.standardcontroller(quot);
              QuotePDFGeneratorController obj = new QuotePDFGeneratorController(sc);
              obj.templateId='0EH36000002S0tj';
              obj.size=100;
              obj.saveQuoteAsPDF();
              obj.saveQuoteAsPDF2();
              obj.saveQuoteAsPDFandEmail();

          Test.stopTest();
      }
          @isTest static void test_method_six() {
            Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
            Quote quot=[Select Id,Name,Division__c, Status,Distributor__c,Pricebook_Name__c,Is_Approved__c from Quote where Status='Approved' and Division__c='SBS' limit 1];
          Test.startTest();
                apexPages.standardcontroller sc = new apexPages.standardcontroller(quot);
              QuotePDFGeneratorController obj = new QuotePDFGeneratorController(sc);
              obj.templateId='0EH36000001k0aB';
                obj.selectedTemplateSBS='SBS Indirect Template';
              obj.size=100;
              obj.SBSTemplateDispay();
              
          
          Test.stopTest();
      }
}