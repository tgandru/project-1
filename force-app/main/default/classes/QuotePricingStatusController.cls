/** 
** SFDC_IF_008 :- Extension class for QuotePricingStatus VF page to switch the status after 15 seconds.
**/

public with sharing class QuotePricingStatusController {
    
    public boolean showlateMesg {get;set;}
    public String integrationStatus {get;set;}
    public String QuoteStatus {get;set;}
    public Quote q{get;set;}
    public boolean showWarnMesg {get;set;}
    public boolean reloadparent {get;set;}
    public String errorMessage {get;set;}
    
    public QuotePricingStatusController(ApexPages.StandardController controller) {
        showlateMesg = FALSE; 
        q=(Quote)controller.getRecord();
        q=[select id,Integration_Status__c,Status, Quote_Status_Boolean__c from Quote where id=:q.id];
        integrationStatus = q.integration_status__c;
        System.debug('lclc in cont integrationstatus '+integrationstatus);
        quotestatus = q.status;
        reloadparent = false;
        if(integrationstatus !=null)
            integrationstatus = integrationstatus.tolowercase();
        if(QuoteStatus !=null)
            QuoteStatus = QuoteStatus.tolowercase();  
        if (q.Quote_Status_Boolean__c) {
            showWarnMessage();
        }  
        List<message_queue__c> lstMq = new List<message_queue__c>();
        errorMessage = 'Information Not Available';
        lstMq = [select id,ERRORTEXT__c,Status__c,Identification_Text__c,Integration_Flow_Type__c from message_queue__c where Identification_Text__c =:q.id and (Status__c='failed' or Status__c='failed-retry')];
        if(lstMq.size() > 0)
            errorMessage = lstMq[0].ERRORTEXT__c;
    }
    public pageReference switchMessage(){
        system.debug('inside switch message...');
        q=[select id,Integration_Status__c,Status from Quote where id=:q.id];
        integrationStatus = q.integration_status__c!=null ? q.integration_status__c.tolowercase() :'';
        System.debug('lclc in cont integrationstatus in switch message '+integrationstatus);
        
        if(integrationstatus!='' && integrationstatus.contains('pending')){
            showlateMesg = TRUE;
            //reloadparent = true;
        }else if(integrationstatus!='' && integrationstatus.contains('success')){
            system.debug('inside success....');
            reloadparent = true;
            //start approval process
        }
       
        return null;
    }
    
    public void showWarnMessage() {
        //System.debug( 'q.Quote_Status_Boolean__c true' );
        showWarnMesg = true;
        //q.Quote_Status_Boolean__c = false;
        //update q;
        
        System.debug(' showWarnMesg '+showWarnMesg);
    }

}