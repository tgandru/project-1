//added alternative ability 7/21
public with sharing class QuoteProductEditExtension {

    //original vars
    public Opportunity theOpp{get;set;}
    public String theQuoteId{get;set;}
    public Quote theQuote{get;set;}
    public List<QuoteLineItem> theQLIs{get;set;} 
    public List<OpportunityLineItem> theOLIs{get;set;} //includes all oli's on oppty with same product group AND product division/family as theQuote
    public List<String> quoteProdGroups=new List<String>();

    //oli to qli mapping
    public Map<Id, QuoteLineItem> idToQLI=new Map<Id, QuoteLineItem>();
    public Set<String> oliIdInQLI{get;set;} //collection of oliid__c's in qli
    public Map<String,Boolean> oliInQLI= new Map<String,Boolean>(); //all oli's will be in this map, values are either 'true'-present in qli OR 'false'-not present in qli
    //public Map<OpportunityLineItem,QuoteLineItem> oliToQLI=new Map<OpportunityLineItem,QuoteLineItem>();
    
    //child mapping
    public Map<String, Integer> parentToBundleCount=new Map<String,Integer>();
    public Map<Id,Set<Id>> parentToChildProdMap=new Map<Id,Set<Id>>(); 

    //wrapper
    public List<shoppingCartWrapper> shoppingCartLst {get;set;}

    //page variables for columns
    public Boolean isAMVPProdGroup{get;set;}
    public Boolean isBundleFlagGroup{get;set;}
    public Boolean opptyHasChildProducts {get;set;}
    public Boolean hasAddlOLIs{get;set;}
    public Boolean isOppClosed{get;set;}
    public List<SelectOption> bundleFlagLst{get;set;}

    //page variables for page reference actions
    public String toUnselect{get;set;}
    //public String toAdd{get;set;} //may not be used.
    public List<shoppingCartWrapper> selectedOLIs=new List<shoppingCartWrapper>();
    public Set<shoppingCartWrapper> toRemove=new Set<shoppingCartWrapper>();

    //for upsert
    List<QuoteLineItem> toAddQLI=new List<QuoteLineItem>();
    set<OpportunityLineItem> maptoUpdateOLI=new set<OpportunityLineItem>();//vijay
    List<OpportunityLineItem> toUpdateOLI=new List<OpportunityLineItem>();
    List<OpportunityLineItem> finaltoUpdateOLI=new List<OpportunityLineItem>();//vijay
    List<QuoteLineItem> toRemoveQLI=new List<QuoteLineItem>();

    //map for comparing quantity or price changes
    public Map<String, Integer> oliToQuantMap{get;set;} //oli id to quantity integer
    public Map<String, Decimal> oliToPriceMap{get;set;} //oli id to requested price integer
    public Boolean confirmChangeNeeded{get;set;}
    
    public boolean isquotelocked {get;set;}
    public QuoteProductEditExtension(ApexPages.StandardController controller) {

        hasAddlOLIs=false;
        shoppingCartLst=new List<shoppingCartWrapper>();
        oliToQuantMap=new Map<String, Integer>();
        oliToPriceMap=new Map<String, Decimal>();
        //confirmChangeNeeded=false;

        if(!test.isRunningTest()){
            controller.addFields(new List<String> {'Name','Pricebook2Id','ProductGroupTest__c','Division__c','IsClosed'});
        }
        
        theOpp=(Opportunity)controller.getRecord();

        if(theOpp.isClosed){
            isOppClosed=true;
        }else{
            isOppClosed=false;
        }
        //if(!test.isRunningTest){
            theQuoteId=ApexPages.currentPage().getParameters().get('quoteId');

            theQuote=[select Id, Name, ProductGroupMSP__c, Division__c,Status,Valid_From_Date__c,ExpirationDate,RequestType__c from quote where id=:theQuoteId];
        //}
        
        
        
            System.debug('lclc quote '+theQuote);
            quoteProdGroups=theQuote.ProductGroupMSP__c.split(';');
    
            if(theQuote.ProductGroupMSP__c!=null && (theQuote.ProductGroupMSP__c.contains('PRINTER')||theQuote.ProductGroupMSP__c.contains('FAX/MFP')||theQuote.ProductGroupMSP__c.contains('A3 COPIER'))){
                isAMVPProdGroup=true;
            }
            if(theQuote.ProductGroupMSP__c!=null && (theQuote.ProductGroupMSP__c.contains('PRINTER')||theQuote.ProductGroupMSP__c.contains('FAX/MFP')||theQuote.ProductGroupMSP__c.contains('A3 COPIER'))){
                isBundleFlagGroup=true;
            }
    
            theQLIs=[select Id, Product2Id,OLIID__c, Alternative__c, Description from QuoteLineItem where QuoteId=:theQuoteId order by CreatedDate asc];
            System.debug('lclc qlis '+theQLIs.size());
            oliIdInQLI=new Set<String>();
            for(QuoteLineItem qli:theQLIs){
                oliIdInQLI.add(qli.OLIID__c);
                idToQLI.put(qli.OLIID__c,qli);
            }
    
            theOLIs=[select Id, Parent_Product__c, ProductId__c, Product_Group__c, Product_Family__c,Unit_Cost__c,
                            Quantity, TotalPrice, ListPrice, UnitPrice, Discount__c, PriceBookEntry.Product2.Description,
                            Product_Type__c, PriceBookEntry.Product2.Name, PriceBookEntryId, SAP_Material_Id__c,
                            AMPV_Mono__c, AMPV_Color__c,Bundle_Flag__c,Requested_Price__c,Closed_List_Price__c,
                            Total_Discount__c, Opportunity.IsClosed, Product_Name__c, PriceBookEntry.Product2Id, Alternative__c,
                            Description,Opportunity.recordtype.developername
                            from OpportunityLineItem
                            where Product_Group__c IN :quoteProdGroups AND OpportunityId=:theOpp.Id order by CreatedDate asc];//AND Alternative__c=false 
            System.debug('lclc olis '+theOLIs.size());
            getbundleflaglst();
            System.debug('lclc quoteProdGroups '+quoteProdGroups);
    
            //determine olis that are in quote or not
            //set up children stuff
            Integer bundleCount=0; //@TODO if searchable make this public variable.
            for(OpportunityLineItem oli:theOLIs){
                //find in quote or not
                if(oliIdInQLI.contains(oli.Id)){
                    oliInQLI.put(oli.Id,true);
                    //oliToQLI.put(oli, idToQLI.get(oli.Id));
                }else{
                    if(!hasAddlOLIs)
                        hasAddlOLIs=true;
                    oliInQLI.put(oli.Id,false);
                }
    
                //children stuff
                if(oli.Parent_Product__c!=null && oli.Parent_Product__c==oli.PriceBookEntry.Product2Id){
                    System.debug('lclc in this ');
                    parentToBundleCount.put(oli.PriceBookEntry.Product2Id,bundleCount);
                    bundleCount++;
                }
    
    
                //compile price and quant maps
                oliToPriceMap.put(oli.Id,oli.Requested_Price__c);
                oliToQuantMap.put(oli.Id, oli.Quantity.intValue());
            }
            System.debug('lclc parentToBundleCount '+parentToBundleCount);
            if(bundleCount!=0){
                opptyHasChildProducts=true;
            }else{
                opptyHasChildProducts=false;
            }
    
            //compile parenttochildprodmap
            for(OpportunityLineItem oli:theOLIs){
                System.debug('lclc in oli for loop '+oli.Parent_Product__c+' lookup-> '+oli.PriceBookEntry.Product2Id);
                if(oli.Parent_Product__c!=null && oli.Parent_Product__c!=oli.PriceBookEntry.Product2Id){
                    Integer bc=parentToBundleCount.get(oli.Parent_Product__c);
                    if(parentToChildProdMap.containsKey(oli.Parent_Product__c)){
                        Set<Id> cIds=parentToChildProdMap.get(oli.Parent_Product__c);
                        cIds.add(oli.PriceBookEntry.Product2Id);
                        parentToChildProdMap.put(oli.Parent_Product__c,cIds);
                    }else{
                        parentToChildProdMap.put(oli.Parent_Product__c, new Set<Id>{oli.PriceBookEntry.Product2Id});
                    }
                }
            }
    
            
            wrapInitial(theOLIs);
            System.debug('lclc 8');
            //delete
            for(shoppingCartWrapper scw:shoppingCartLst){
                system.debug('lclc scw before '+scw);
            }
            sequenceShoppingCart();
            System.debug('lclc 9');
             for(shoppingCartWrapper scw:shoppingCartLst){
                system.debug('lclc scw after '+scw);
            }
        
        //Added by Jagan to check if a record is locked
        if(Approval.isLocked(theQuote.id)){    
            isquotelocked = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Quote is locked for editing.'));
        }
             
    }

    /******************************PAGE REFERENCES*********************************/
    
    public PageReference checkValuesSave(){
        confirmChangeNeeded=false;
         if((theQuote.Status=='Presented' || theQuote.Status=='Approved')&&theQuote.Division__c !='Mobile' && theQuote.Division__c !='W/E'){
            for(shoppingCartWrapper scw:shoppingCartLst){
                Integer quantOrig=oliToQuantMap.get(scw.oppliId);
                Decimal priceOrig=oliToPriceMap.get(scw.oppliId);
                System.debug('lclc scw quantity '+scw.quantity+' quant orig '+quantOrig);
                if( (quantOrig > scw.quantity || priceOrig > scw.requestedPrice) && (scw.selected==true || (scw.isOnQuote==true && scw.removed==false) ) ){
                    System.debug('lclc changing? ');
                    confirmChangeNeeded=true;
                }
            }
        }else if((theQuote.Status=='Presented' || theQuote.Status=='Approved')&&theQuote.Division__c =='Mobile'){
            for(shoppingCartWrapper scw:shoppingCartLst){
                Integer quantOrig=oliToQuantMap.get(scw.oppliId);
                Decimal priceOrig=oliToPriceMap.get(scw.oppliId);
                System.debug('lclc scw quantity '+scw.quantity+' quant orig '+quantOrig);
                if( (quantOrig != scw.quantity || priceOrig != scw.requestedPrice) && (scw.selected==true || (scw.isOnQuote==true && scw.removed==false) ) ){
                    System.debug('lclc changing? ');
                    confirmChangeNeeded=true;
                }
            }
            
        }
        //Added by Thiru - 12/13/2017
        else if((theQuote.Status=='Presented' || theQuote.Status=='Approved') &&theQuote.Division__c =='W/E'){
            for(shoppingCartWrapper scw:shoppingCartLst){
                Integer quantOrig=oliToQuantMap.get(scw.oppliId);
                Decimal priceOrig=oliToPriceMap.get(scw.oppliId);
                System.debug('lclc scw quantity '+scw.quantity+' quant orig '+quantOrig);
                if( priceOrig != scw.requestedPrice && (scw.selected==true || (scw.isOnQuote==true && scw.removed==false) ) ){
                    System.debug('lclc changing? ');
                    confirmChangeNeeded=true;
                }
            }
            
        }
            
        System.debug('lclc afterr checkValuesSave '+confirmChangeNeeded);

        if(!confirmChangeNeeded){
            save();
            return new PageReference('/apex/CustomRefreshPage?Id='+theQuoteId);
        }
        return null;
    }

    public PageReference nada(){
        confirmChangeNeeded=false;
        return null;
    }
    public PageReference checkValuesSaveAndCont(){
        confirmChangeNeeded=false;
        if((theQuote.Status=='Presented' || theQuote.Status=='Approved') && theQuote.Division__c !='Mobile' && theQuote.Division__c !='W/E'){
            for(shoppingCartWrapper scw:shoppingCartLst){
                Integer quantOrig=oliToQuantMap.get(scw.oppliId);
                Decimal priceOrig=oliToPriceMap.get(scw.oppliId);
                if( (quantOrig > scw.quantity || priceOrig > scw.requestedPrice) && (scw.selected==true || (scw.isOnQuote==true && scw.removed==false) )){
                    confirmChangeNeeded=true;
                }
            }
        }else if((theQuote.Status=='Presented' || theQuote.Status=='Approved')&&theQuote.Division__c =='Mobile'){
            for(shoppingCartWrapper scw:shoppingCartLst){
                Integer quantOrig=oliToQuantMap.get(scw.oppliId);
                Decimal priceOrig=oliToPriceMap.get(scw.oppliId);
                if( (quantOrig != scw.quantity || priceOrig != scw.requestedPrice) && (scw.selected==true || (scw.isOnQuote==true && scw.removed==false) )){
                    confirmChangeNeeded=true;
                }
            }
        }
        else if((theQuote.Status=='Presented' || theQuote.Status=='Approved') &&theQuote.Division__c =='W/E'){
            for(shoppingCartWrapper scw:shoppingCartLst){
                Integer quantOrig=oliToQuantMap.get(scw.oppliId);
                Decimal priceOrig=oliToPriceMap.get(scw.oppliId);
                if( priceOrig != scw.requestedPrice && (scw.selected==true || (scw.isOnQuote==true && scw.removed==false) )){
                    confirmChangeNeeded=true;
                }
            }
        }
            

        System.debug('lclc afterr checkValuesSaveAndCont '+confirmChangeNeeded);

        if(!confirmChangeNeeded){
            saveAndContinue();
        }
        System.debug('outside save and cont ');
        return null;
    }

    //save
    public PageReference save(){

        unWrapInitial(shoppingCartLst);
        try{
            if(!toRemoveQLI.isEmpty()){
                delete toRemoveQLI;
                
            }

            if(!toUpdateOLI.isEmpty()){
                
                update toUpdateOLI;
                
            }

            if(!toAddQLI.isEmpty()){
                upsert toAddQLI;
                
            }

        }catch(exception e){
            System.debug('LCLC ERROR IN SAVE AND CONTINUE '+e);
            ApexPages.addMessages(e);
            return null;
        }

        toRemoveQLI.clear();
        toUpdateOLI.clear();
        toAddQLI.clear();

        return new PageReference('/apex/CustomRefreshPage?Id='+theQuoteId);
    }

    //cancel
    public PageReference cancel(){
        return new PageReference('/'+theQuoteId);
    }

    //save and continue??
    public PageReference saveAndContinue(){
        System.debug('lclc shoppingCartLst size 1 '+shoppingCartLst.size());

        confirmChangeNeeded=false;
        System.debug('lclc shoppingCartLst size 2 '+shoppingCartLst.size());
        System.debug('lclc  oliInQLI '+oliInQLI.size()+' parentToBundleCount '+parentToBundleCount.size());
        System.debug('lclc oliInQLI '+oliInQLI);
        unWrapInitial(shoppingCartLst);
        Boolean complexUpdateNeeded=false;
        try{
            if(!toRemoveQLI.isEmpty()){
                delete toRemoveQLI;
                toRemoveQLI.clear();
                complexUpdateNeeded=true;
            }

            if(!toUpdateOLI.isEmpty()){
                system.debug('******'+toUpdateOLI.size());
                for(OpportunityLineItem ol:toUpdateOLI){
                   maptoUpdateOLI.add(ol);
                }
                for(OpportunityLineItem mo:maptoUpdateOLI){
                    finaltoUpdateOLI.add(mo);
                }
                update finaltoUpdateOLI;
                toUpdateOLI.clear();
            }

            if(!toAddQLI.isEmpty()){
                upsert toAddQLI;
                toAddQLI.clear();
                complexUpdateNeeded=true;
            }

        }catch(exception e){
            System.debug('LCLC ERROR IN SAVE AND CONTINUE '+e);
            ApexPages.addMessages(e);
        }

        if(complexUpdateNeeded){
            resetPageComplex();
        }else{
            resetPageSimple();
        }
        
        

        System.debug('lclc at end '+shoppingCartLst.size());



        return null;
    }

    //remove 1
    public PageReference removeFromQLI(){
        for(shoppingCartWrapper d:shoppingCartLst){
            if(d.oppli.Id==toUnselect){
                toRemove.add(d);
                d.removed=true; 
                d.selected=false;
            }
        }
        return null;
    }

    //add all and make all checked
    public PageReference doSelect(){
        selectedOLIs.clear();
        for(shoppingCartWrapper scw:shoppingCartLst){
            System.debug('lclc in doSelect onq '+scw.isOnQuote+' selected '+scw.selected+' removed '+scw.removed);
            if(scw.isOnQuote==false && scw.selected==true){
                selectedOLIs.add(scw);
                if(scw.removed==true){
                    scw.removed=false;
                }
                System.debug('lclc added '+scw);
            }else if(scw.isOnQuote==true && scw.selected==true && scw.removed==true){
                scw.removed=false;
            }
        }
        System.debug('lclc selectedOLIs '+selectedOLIs.size());
        return null;
    }

    //go to oppty prod entry 
    public PageReference addOpptyProds(){
        
        if(UserInfo.getUiTheme() =='Theme4d' || UserInfo.getUiTheme()=='Theme4t' || UserInfo.getUiThemeDisplayed() =='Theme4d' || UserInfo.getUiThemeDisplayed()=='Theme4t' ){
            System.debug('****LTNG****');
            return new PageReference('/apex/OpportunityProductEntry_Ltng?id=' +theOpp.Id+'&quoteId='+theQuoteId+'&fromQuoteEntry=true');
        }
        
        return new PageReference('/apex/OpportunityProductEntry?id=' + theOpp.Id+'&quoteId='+theQuoteId+'&fromQuoteEntry=true');
    }

    /******************************HELPER METHODS*********************************/

    private void resetPageSimple(){
        shoppingCartLst.clear();
        theOLIs.clear();
        theOLIs=[select Id, Parent_Product__c, ProductId__c, Product_Group__c, Product_Family__c,Unit_Cost__c,
                        Quantity, TotalPrice, ListPrice, UnitPrice, Discount__c, PriceBookEntry.Product2.Description,
                        Product_Type__c, PriceBookEntry.Product2.Name, PriceBookEntryId, SAP_Material_Id__c,
                        AMPV_Mono__c, AMPV_Color__c,Bundle_Flag__c,Requested_Price__c,Closed_List_Price__c,
                        Total_Discount__c, Opportunity.IsClosed, Product_Name__c, PriceBookEntry.Product2Id, Alternative__c,
                        description,Opportunity.RecordType.developername
                        from OpportunityLineItem
                        where Product_Group__c IN :quoteProdGroups AND OpportunityId=:theOpp.Id]; //AND Alternative__c=false

        //lclc del
        for(OpportunityLineItem oli:theOLIs){
            system.debuG('lclc in olis just queried '+oli.quantity+' '+oli);
        }

        wrapInitial(theOLIs);
    }

    private void resetPageComplex(){
        shoppingCartLst.clear();
        theOLIs.clear();
        idToQLI.clear();
        oliInQLI.clear();
        theQLIs.clear();
        oliToPriceMap.clear();
        oliToQuantMap.clear();

        theQLIs=[select Id, Product2Id,OLIID__c,Alternative__c from QuoteLineItem where QuoteId=:theQuoteId];
        System.debug('lclc qlis 2 '+theQLIs.size());
        oliIdInQLI=new Set<String>();
        for(QuoteLineItem qli:theQLIs){
            oliIdInQLI.add(qli.OLIID__c);
            idToQLI.put(qli.OLIID__c,qli);
        }

        theOLIs=[select Id, Parent_Product__c, ProductId__c, Product_Group__c, Product_Family__c,Unit_Cost__c,
                        Quantity, TotalPrice, ListPrice, UnitPrice, Discount__c, PriceBookEntry.Product2.Description,
                        Product_Type__c, PriceBookEntry.Product2.Name, PriceBookEntryId, SAP_Material_Id__c,
                        AMPV_Mono__c, AMPV_Color__c,Bundle_Flag__c,Requested_Price__c,Closed_List_Price__c,
                        Total_Discount__c, Opportunity.IsClosed, Product_Name__c,PriceBookEntry.Product2Id, Alternative__c,
                        description,Opportunity.RecordType.developername
                        from OpportunityLineItem
                        where Product_Group__c IN :quoteProdGroups AND OpportunityId=:theOpp.Id];//AND Alternative__c=false 
        System.debug('lclc olis '+theOLIs.size());

        //determine olis that are in quote or not
        for(OpportunityLineItem oli:theOLIs){
            System.debug('lclc jsut queried olis '+oli.quantity+' '+oli);
            //find in quote or not
            if(oliIdInQLI.contains(oli.Id)){
                oliInQLI.put(oli.Id,true);
                //oliToQLI.put(oli, idToQLI.get(oli.Id));
            }else{
                if(!hasAddlOLIs)
                    hasAddlOLIs=true;
                oliInQLI.put(oli.Id,false);
            }

            //compile price and quant maps
            oliToPriceMap.put(oli.Id,oli.Requested_Price__c);
            oliToQuantMap.put(oli.Id, oli.Quantity.intValue());
        }

        wrapInitial(theOLIs);

    }

    private void wrapInitial(List<OpportunityLineItem> initCart){
        System.debug('lclc in wrap');
        for(OpportunityLineItem oli:initCart){
            System.debug('lclc in for oli '+oli.PriceBookEntry.Product2.Name+' '+oliInQLI.get(oli.Id) +' '+parentToBundleCount.get(oli.Parent_Product__c)+' and parent prod '+oli.Parent_Product__c);
            if(opptyHasChildProducts){
                if(oli.Parent_Product__c==oli.PriceBookEntry.Product2Id){
                    shoppingCartLst.add(new shoppingCartWrapper(oli,1,null,parentToBundleCount.get(oli.Parent_Product__c),oliInQLI.get(oli.Id),idToQLI.get(oli.Id)));
                }else if(oli.Parent_Product__c!=null){
                    shoppingCartLst.add(new shoppingCartWrapper(oli,2,oli.Parent_Product__c,parentToBundleCount.get(oli.Parent_Product__c),oliInQLI.get(oli.Id),idToQLI.get(oli.Id)));
                }else{
                    shoppingCartLst.add(new shoppingCartWrapper(oli,3,null,null,oliInQLI.get(oli.Id),idToQLI.get(oli.Id)));
                }
            }else{
                System.debug('lclc 1 '+oliInQLI.get(oli.Id));
                System.debug('lclc 2 '+idToQLI.get(oli.Id));
                shoppingCartLst.add(new shoppingCartWrapper(oli,4,null,null,oliInQLI.get(oli.Id),idToQLI.get(oli.Id)));
            }
        }
    }

    private void unWrapInitial(List<shoppingCartWrapper> unwrapLst){
        System.debug('lclc unwrapLst size '+unwrapLst.size());
        for(shoppingCartWrapper scw:unwrapLst){
            System.debug('lclc scw '+scw);
            if(scw.isOnQuote && scw.removed && !scw.selected){ //see if its removal 
                System.debug('lclc in remove qli '+scw);
                toRemoveQLI.add(scw.qli);
            }else if(scw.isOnQuote && !scw.removed){
                System.debug('lclc in update oli '+scw);
                toUpdateOLI.add(new OpportunityLineItem(Id=scw.oppli.Id, Quantity=scw.quantity, 
                                                        Bundle_Flag__c=scw.selectedBundleFlag, 
                                                        AMPV_Mono__c=scw.ampvMono, AMPV_Color__c=scw.ampvColor,
                                                        Requested_Price__c=scw.requestedPrice, UnitPrice=scw.requestedPrice));
                                                        
               
                
            }else if(!scw.isOnQuote && scw.selected){
                System.debug('lclc in add qli '+scw);
                System.debug('lclc scw.quantity '+scw.quantity+' '+scw);
                toAddQLI.add(new QuoteLineItem(OLIID__c=scw.oppli.Id, Quantity=scw.quantity, Quote=theQuote, QuoteId=theQuoteId,
                                                Requested_Price__c=scw.requestedPrice, Bundle_Flag__c=scw.selectedBundleFlag, 
                                                AMPV_Mono__c=scw.ampvMono, AMPV_Color__c=scw.ampvColor, UnitPrice=scw.oppli.UnitPrice,
                                                PricebookEntryId=scw.oppli.PricebookEntryId, Product2Id=scw.oppli.PriceBookEntry.Product2Id,
                                                Unit_Cost__c = scw.oppli.Unit_Cost__c,Discount__c=scw.discount,
                                                SAP_Material_ID__c=scw.oppli.SAP_Material_ID__c,
                                                Parent_Product__c=scw.oppli.Parent_Product__c, Alternative__c=scw.oppLi.Alternative__c,
                                                Description=scw.oppli.description
                                                ));
                toUpdateOLI.add(new OpportunityLineItem(Id=scw.oppli.Id, Quantity=scw.quantity, 
                                                        Bundle_Flag__c=scw.selectedBundleFlag, 
                                                        AMPV_Mono__c=scw.ampvMono, AMPV_Color__c=scw.ampvColor,
                                                        Requested_Price__c=scw.requestedPrice, UnitPrice=scw.requestedPrice));
                                                        
                                                        
                                                        
                                                       
            }
        }

    }

    public void getbundleFlagLst(){
        bundleFlagLst=new List<SelectOption>();
        Schema.DescribeFieldResult res=OpportunityLineItem.Bundle_Flag__c.getDescribe();
        List<Schema.PicklistEntry>ple=res.getPicklistValues();
        bundleFlagLst.add(new SelectOption('',''));
        for(Schema.PicklistEntry f:ple){
            bundleFlagLst.add(new SelectOption(f.getLabel(),f.getValue()));
        }
    }

    private void sequenceShoppingCart(){
        if(!parentToChildProdMap.isEmpty() && !parentToBundleCount.isEmpty()){
            
            Map<Integer,List<shoppingCartWrapper>> bundleCountToChildrenSCW=new Map<Integer,List<shoppingCartWrapper>>();
            //Map<Integer, shoppingCartWrapper> bundleCountToParentSCW=new Map<Integer,shoppingCartWrapper>();
            
            //go thru scw, separate out bundle vars.. so map<integer, list<scw>>
            for(shoppingCartWrapper scw:shoppingCartLst){
                System.debug('lclc in 1 '+scw.bundleCount+' '+scw.parentProdId+' '+scw.productEntry);
                if(scw.bundleCount!=null && scw.parentProdId!=scw.productEntry && scw.parentProdId!=null){
                    //set up child map
                    if(bundleCountToChildrenSCW.containsKey(scw.bundleCount)){
                        List<shoppingCartWrapper> existing = new List<shoppingCartWrapper>();
                        existing=bundleCountToChildrenSCW.get(scw.bundleCount);
                        existing.add(scw);
                        bundleCountToChildrenSCW.put(scw.bundleCount,existing);
                    }else{
                        bundleCountToChildrenSCW.put(scw.bundleCount,new List<shoppingCartWrapper>{scw});
                    }
                }
            }

            //go through and put them on top
            //Integer countParent=0;

            //lclc delete
        integer c=0;
        String s1='';
        for(shoppingCartWrapper scw:shoppingCartLst){
            system.debug('lclc 1 '+c+' scw '+scw.productName+' bc '+scw.bundleCount+' pti '+scw.productTypeInt+' '+scw.parentProdId);
            c++;
            s1+=scw.productName+',';
        }
        System.debug('lclc after 1 '+s1);

            for(shoppingCartWrapper scw1:shoppingCartLst){
                //find parent, count will be location of that parent. 
                if(scw1.productTypeInt==1){
                    shoppingCartWrapper parentSCW=scw1;

                    integer h=0;
                    String s15='';
                    for(shoppingCartWrapper scw:shoppingCartLst){
                        system.debug('lclc 1.5 '+h+' scw '+scw.productName+' bc '+scw.bundleCount+' pti '+scw.productTypeInt+' '+scw.parentProdId);
                        h++;
                        s15+=scw.productName+',';
                    }
                    System.debug('lclc after 1.5 '+s15);

                    //find children of parent and put them in place
                    Integer bundleNum=scw1.bundleCount;
                    List<shoppingCartWrapper> childrenSCWs = new List<shoppingCartWrapper>();
                    childrenSCWs=bundleCountToChildrenSCW.get(bundleNum);
                    System.debug('lclc childrenSCWs '+(childrenSCWs!=null?childrenSCWs.size():0)+' '+scw1.productName);
                    Integer countChild=0;
                    
                    for(shoppingCartWrapper scw2:shoppingCartLst){
                        if(childrenSCWs!=null){
                            for(Integer z=0; z<childrenSCWs.size();z++){
                                System.debug('lclc count'+countChild+' scw2bc '+scw2.bundleCount+' '+scw2.productEntry+' '+scw2.productName);
                                System.debug('lclc z info z '+z+' bc '+childrenSCWs[z].bundleCount+' '+childrenSCWs[z].productEntry+' '+childrenSCWs[z].productName);
                                if(childrenSCWs[z].bundleCount==scw2.bundleCount && childrenSCWs[z].productEntry==scw2.productEntry){
                                    Integer y=countChild;
                                    /*System.debug('lclc in it, y '+y); -->neeed to do this later or else it will be inaccurate in parent plaecement
                                    if(countchild>findLatestChild){
                                        findLatestChild=countChild;
                                        System.debug('lclc assign findLatestChild '+findLatestChild);
                                    }*/
                                    do{
                                        if((y-1)>=0){
                                            shoppingCartLst[y]=shoppingCartLst[y-1];
                                        }
                                        y--;
                                    }while((y-1)>=0);
                                    shoppingCartLst[0]=scw2;
                                }
                            }
                            countChild++;
                        }
                    }

                    //set position of latest child here AND the position of the parent. 
                    Integer countChild2=0;
                    Integer findLatestChild=0;
                    Integer countParent=0;
                    for(shoppingCartWrapper scwFindLatest:shoppingCartLst){
                        System.debug('lclc childrenSCWs '+childrenSCWs );
                        if(childrenSCWs != null){
                            for(Integer z=0; z<childrenSCWs.size();z++){
                                System.debug('lclc second count'+countChild2+' scw2bc '+scwFindLatest.bundleCount+' '+scwFindLatest.productEntry+' '+scwFindLatest.productName);
                                System.debug('lclc second z info z '+z+' bc '+childrenSCWs[z].bundleCount+' '+childrenSCWs[z].productEntry+' '+childrenSCWs[z].productName);
                                if(childrenSCWs[z].bundleCount==scwFindLatest.bundleCount && childrenSCWs[z].productEntry==scwFindLatest.productEntry){
                                    Integer y=countChild2;
                                    System.debug('lclc second in it, y '+y); 
                                    if(countchild2>findLatestChild){
                                        findLatestChild=countChild2;
                                        System.debug('lclc second assign findLatestChild '+findLatestChild);
                                    }
                                }
                            }
    
                            if(scwFindLatest==parentSCW){
                                countParent=countChild2;
                            }
    
                            countChild2++;
                        }
                    }



                    //delete
                    integer x=0;
                    String s2='';
                    for(shoppingCartWrapper scw:shoppingCartLst){
                        system.debug('lclc 2 '+x+' scw '+scw.productName+' bc '+scw.bundleCount+' pti '+scw.productTypeInt+' '+scw.parentProdId);
                        x++;
                        s2+=scw.productName+',';
                    }
                    System.debug('lclc after 2 '+s2);


                    //now put parent inplace
                    System.debug('lclc countParent '+countParent+' childsize '+(childrenSCWs!=null?childrenSCWs.size():0)+' whole size '+shoppingCartLst.size()+' findLatestChild '+findLatestChild);
                    Integer count2=0;
                    if( findLatestChild>=countParent && childrenSCWs!=null && (countParent+childrenSCWs.size()) <= (shoppingCartLst.size()-1)){
                        count2=countParent+childrenSCWs.size();
                        System.debug('lclc if 1');
                    }else if( findLatestChild<countParent && countParent <= (shoppingCartLst.size()-1)){
                        System.debug('lclc if2 ');
                        count2=countParent;
                    }else{
                        System.debug('lclc if 3');
                        count2=shoppingCartLst.size()-1;
                    }
                    system.debug('lclc count2 '+count2);

                    do{
                        if((count2-1)>=0){
                            System.debug('lclc count2 '+count2+' being replaced '+shoppingCartLst[count2].productName+ ' '+shoppingCartLst[count2].bundleCount+' by '+shoppingCartLst[count2-1].productName+ ' '+shoppingCartLst[count2-1].bundleCount);
                            shoppingCartLst[count2]=shoppingCartLst[count2-1];
                        }
                        count2--;
                    }while((count2-1)>=0);
                    shoppingCartLst[0]=scw1;

                     //delete
                    integer i=0;
                    String s3='';
                    for(shoppingCartWrapper scw:shoppingCartLst){
                        system.debug('lclc 3 '+i+' scw '+scw.productName+' bc '+scw.bundleCount+' pti '+scw.productTypeInt+' '+scw.parentProdId);
                        i++;
                        s3+=scw.productName+',';
                    }
                    System.debug('lclc after 3 '+s3);

                }
                 //delete
                    integer j=0;
                    String s4='';
                    for(shoppingCartWrapper scw:shoppingCartLst){
                        system.debug('lclc 4 '+j+' scw '+scw.productName+' bc '+scw.bundleCount+' pti '+scw.productTypeInt+' '+scw.parentProdId);
                        j++;
                        s4+=scw.productName+',';
                    }
                    System.debug('lclc after 4 '+s4);
                //countParent++;
            }

        }

    }

    /******************************WRAPPER*********************************/

    public class shoppingCartWrapper{
        public OpportunityLineItem oppli{get;set;}
        public String oppliId{get;set;}
        public Boolean isOnQuote {get;set;}
        public Boolean isAmpvPT{get;set;}
        public Boolean isBundlePT{get;set;}
        public String productGroup{get;set;}
        public String sapMaterialId{get;set;}
        public Decimal ampvColor{get;set;}
        public Decimal ampvMono{get;set;}
        public Decimal discount{get;set;}
        public Decimal invoicePrice{get;set;}
        public String description{get;set;}
        public String productType{get;set;}
        public String productName{get;set;}
        public Boolean alternative{get;set;}
        public Decimal requestedPrice{get;set;}
        public Decimal totalDiscount{get;set;}
        public Decimal totalPrice{get;set;}
        public Integer quantity{get;set;}
        public QuoteLineItem qli{get;set;} //add
        public String productEntry{get;set;}

        public String selectedBundleFlag{get;set;}
        public Integer bundleCount{get;set;}
        public String parentProdId{get;set;}
        public Integer productTypeInt{get;set;}

        //these variables control the bold features of the shopping cart based on parent-child relationships.
        public Boolean boldAndBlack{get;set;}
        public Boolean normalAndBlack{get;set;}

        //if selected
        public Boolean selected{get;set;} //selected only true if oli not on quote is selected to become qli
        //if removed
        public Boolean removed{get;set;}

        public shoppingCartWrapper(OpportunityLineItem oli, Integer productTypeIntegerVar, String parentProdIdVar, Integer bundleCountVar, Boolean isOnQuoteVar, QuoteLineItem qliVar){
            oppli=oli;
            oppliId=oli.Id;
            qli=qliVar;
            productEntry=oppli.PriceBookEntry.Product2Id;
            productType=oppli.Product_Type__c;
            isOnQuote=isOnQuoteVar;

            productGroup=oppli.Product_Group__c;
            sapMaterialId=oppli.SAP_Material_Id__c;
            ampvMono=oppLi.AMPV_Mono__c;
            ampvColor=oppLi.AMPV_Color__c;
            quantity=oppLi.Quantity.intValue();
            totalDiscount=oppLi.Total_Discount__c;
            discount=oppLi.Discount__c;
            totalPrice=oppLi.TotalPrice;
            requestedPrice=oppLi.Requested_Price__c;
            
            if(oppLi.Opportunity.RecordType.developername=='Services'){
                Description=oppLi.Description;
            }else{
                Description=oppli.PriceBookEntry.Product2.Description;
            }
            //description=oppli.PriceBookEntry.Product2.Description;
            selectedBundleFlag=oppLi.Bundle_Flag__c;
            productTypeInt=productTypeIntegerVar;
            productName=oppli.Product_Name__c;
            alternative=oppli.Alternative__c;

            System.debug('lclc in wrap '+oppli.Product_Name__c+' '+isOnQuoteVar);

            if(oli.opportunity.IsClosed){
                invoicePrice=oli.Closed_List_Price__c;
            }else{
                invoicePrice=oli.ListPrice;
            }

            if(parentProdIdVar!=null){
                parentProdId=parentProdIdVar;
            }
            if(bundleCountVar!=null){
                bundleCount=(bundleCountVar);
            }

            if(productTypeInt==2 || productTypeInt==4){
                boldAndBlack=false;
                normalAndBlack=true;
            }else{
                boldAndBlack=true;
                normalAndBlack=false;
            }

            isAmpvPT=false;
            isBundlePT=false;
            if(productGroup!=null && (productGroup.toUpperCase().contains('PRINTER')||productGroup.toUpperCase().contains('FAX/MFP')||productGroup.toUpperCase().contains('A3 COPIER'))){
                if(( productType.toUpperCase().contains('SET') || productType.toUpperCase().contains('H/W')) && (!productType.toUpperCase().contains('CON') && !productType.toUpperCase().contains('CONSUMABLE'))){
                    isAmpvPT=true;
                }else if((productType.toUpperCase().contains('CON') || productType.toUpperCase().contains('CONSUMABLE')) && (!productType.toUpperCase().contains('SET') && !productType.toUpperCase().contains('H/W'))){
                    isBundlePT=true;
                }
            }

            selected=false; 
            removed=false;

        }
    }
}