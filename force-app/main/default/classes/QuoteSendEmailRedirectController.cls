public with sharing class QuoteSendEmailRedirectController {
   Public String QuoteID {get; set;}
   Public String QuotePdfID {get; set;}
    public QuoteSendEmailRedirectController(ApexPages.StandardController controller) {
         string QtId = apexPages.currentPage().getParameters().get('id');
         if(Qtid !=null){
             QuoteID = Qtid;
             
             List<QuoteDocument> qpdfs=new List<QuoteDocument>([Select ID from QuoteDocument  where QuoteId=:qtid order by CreatedDate desc limit 1]);
             
             if(qpdfs.size()>0){
                 QuotePdfID =qpdfs[0].Id;
             }
         }
    }

}