public with sharing class QuoteSubmitApprovalButtonCustomExtension {

    public Quote quoteObj{get;set;}
    //public List<QuoteLineItem> qliLst{get;set;}
    public Boolean needCallout{get;set;}
    public Boolean qliToCallout {get;set;}
    //Added by Jagan 
    public boolean displayapproval{get;set;}
    //mobile--all mobile added by lauren
    public boolean isInMobile{get;set;}
    public String selectedISRRep{get;set;}
    public List<SelectOption> ISRReps{get;set;}
    public Boolean isComplete{get;set;}
    public Boolean isKnoxQuote = false;
    //public Boolean primaryDistExist {get;set;}
    public QuoteSubmitApprovalButtonCustomExtension(ApexPages.StandardController controller) {
      
        //Decide whether the users device is mobile or desktop
     /*  if(String.isNotBlank(ApexPages.currentPage().getParameters().get('sfdcIFrameHost')) ||
            String.isNotBlank(ApexPages.currentPage().getParameters().get('sfdcIFrameOrigin')) ||
            ApexPages.currentPage().getParameters().get('isdtp') == 'p1' ||
            (ApexPages.currentPage().getParameters().get('retURL') != null && ApexPages.currentPage().getParameters().get('retURL').contains('projectone') )
        ){
            isInMobile= true;
        }else{
            isInMobile= false;
        } */
        
        if(UserInfo.getUiThemeDisplayed() =='Theme4t'){
             isInMobile= true;
        }else{
             isInMobile= false;
        }
          
        //Added by Jagan:- To display separate approval step for manually chosen approval processes 
        displayapproval= false; 
        
        quoteObj=(Quote)controller.getRecord();
        quoteObj=[select id, Name, OpportunityId,ProductGroupMSP__c,BEP__c,MPS__c,Supplies_Buying_Location__c,Status, Division__c,
                    Approval_Requestor_Id__c, Last_Update_from_CastIron__c,Integration_Status__c,ISR_Rep__c,Quote_Approval__c,Set_Loss__c,Set_Marginal_Profit_Rate__c,
                    CON_Amount__c,SET_Amount__c,recordtype.name,ISR_Rep__r.profile.name,opportunity.Deal_Registration_Approval__c,(select id from QuoteLineItems where Discount__c>50)
                    from Quote where Id=:quoteObj.Id];//Successful_Update_Number__c, Failed_Update_Number__c, Total_Lines_For_Update__c, 
    }
    
    public pageReference submitApproval(){
         
        try{
            if(quoteObj.Division__c == 'IT'|| quoteObj.Division__c == 'Mobile'){
                if(quoteObj.ProductGroupMSP__c != null){
                    for(string s :quoteObj.ProductGroupMSP__c.split(';')){
                        if(s == 'KNOX'){
                            isKnoxQuote = true;
                            //submitApproval2();
                        }
                    }
                  
                }
            }
        }catch(exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'No Applicable Process found, please contact your system administrator'));
            return null;
        }
        
        
        if(quoteObj.Division__c == 'W/E' && quoteObj.QuoteLineItems.size()>0 && quoteObj.opportunity.Deal_Registration_Approval__c!='Approved'){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'One or More Products have Discount More than 50%. Deal Registration must be Approved for Approving this Quote'));     
            return null;
        }
    //Added by Thiru --- starts here... For SBS quote is submitted for approval directly.
       if(quoteObj.Division__c == 'SBS'||quoteObj.Division__c == 'W/E')
       {
          try{
                ISRReps=new List<SelectOption>();
                if(isInMobile)
                {
                    List<User> ISRRepsUsers=[select Id, 
                    
                    Name from User where (Profile.Name='B2B ISR Sales User - IT' or Profile.Name='B2B ISR Sales User - Mobile'  or Profile.Name='B2B Sales Leader - Mobile' or Profile.Name='B2B Sales Leader - IT') AND IsActive=true];
                    System.debug('lclc ISRRepsUsers '+ISRRepsUsers.size());
                    if(!ISRRepsUsers.isEmpty())
                    {
                        ISRReps.add(new SelectOption('--Select Approver--','--Select Approver--'));
                        for(User u:ISRRepsUsers)
                        {
                            ISRReps.add(new SelectOption(u.Id,u.Name));
                        } 
                    }else
                    {
                         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'No approvers found. Please contact your administrator'));
                         return null;
                    }
                }
            
         
                if(!isInMobile){
                    quoteObj.Quote_Approval__c=true;
                    update quoteObj;
                }  
                
                Approval.ProcessSubmitRequest req1=new Approval.ProcessSubmitRequest();
                req1.setObjectId(quoteObj.Id);
                req1.setSubmitterId(UserInfo.getUserId());
                Approval.ProcessResult result=Approval.process(req1);
                    
                return new PageReference('/apex/CustomRefreshPage?Id='+quoteObj.id);
          }catch(Exception e){
            //Added Exception by Thiru 04/16/2018 --- For SBS Division, Added ISR Selection Step.
            system.debug('EXCEPTION OCCURED...'+e);
            if(e.getMessage().contains('REQUIRED_FIELD_MISSING, missing required field: [nextApproverIds]')){
                displayapproval= true;
            }
            else
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,''+e));
            return null;
          }
            
        }
        // Added by Thiru-----Ends Here
        try{
            //if((!quoteObj.ProductGroupMSP__c.containsIgnoreCase('KNOX') && quoteObj.Integration_Status__c == null) ||(quoteObj.Integration_Status__c!=null &&  !quoteObj.Integration_Status__c.equalsIgnoreCase('success'))){               
            if((!quoteObj.ProductGroupMSP__c.containsIgnoreCase('KNOX') && quoteObj.recordtype.name!='Mobile' && quoteObj.Integration_Status__c == null) ||(quoteObj.Integration_Status__c!=null &&  !quoteObj.Integration_Status__c.equalsIgnoreCase('success'))){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Cannot submit for Approval as the invoice price is not updated.'));
                return null; 
            }
            else{
                //if(!isInMobile)
                //    submitApproval(); 
                ISRReps=new List<SelectOption>();
                if(isInMobile){
                    List<User> ISRRepsUsers=[select Id, 
                    
                    Name from User where (Profile.Name='B2B ISR Sales User - IT' or Profile.Name='B2B ISR Sales User - Mobile'  or Profile.Name='B2B Sales Leader - Mobile' or Profile.Name='B2B Sales Leader - IT') AND IsActive=true];
                    System.debug('lclc ISRRepsUsers '+ISRRepsUsers.size());
                    if(!ISRRepsUsers.isEmpty()){
                        ISRReps.add(new SelectOption('--Select Approver--','--Select Approver--'));
                        for(User u:ISRRepsUsers){
                            ISRReps.add(new SelectOption(u.Id,u.Name));
                        }
                    }else{
                         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'No approvers found. Please contact your administrator'));
                         return null;
                    }
                }
            }
        
            if(!isInMobile){
                quoteObj.Quote_Approval__c=true;
                update quoteObj;
            }  
            //if(!isKnoxQuote){
                Approval.ProcessSubmitRequest req1=new Approval.ProcessSubmitRequest();
                req1.setObjectId(quoteObj.Id);
                req1.setSubmitterId(UserInfo.getUserId());
                Approval.ProcessResult result=Approval.process(req1);
            //}   
            return new PageReference('/apex/CustomRefreshPage?Id='+quoteObj.id);
            
        }catch(Exception e){
            //Updated by Jagan
            system.debug('EXCEPTION OCCURED...'+e);
            if(e.getMessage().contains('REQUIRED_FIELD_MISSING, missing required field: [nextApproverIds]')){
                displayapproval= true;
            }else if(e.getMessage().contains('NO_APPLICABLE_PROCESS, No applicable approval process was found')){
                 displayapproval= true;
            }
            else
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,''+e));
            return null;
        }
            
    }
    
    //Added by Jagan :- Method called from VF page--
    public pageReference submitApproval2(){
    
        try{
            system.debug('Approval method from Visualforce page');
            system.debug('..ISR REP...'+quoteObj.ISR_Rep__c );
            if(!isInMobile){
                quoteObj.Quote_Approval__c=true;
                update quoteObj;
            }
            Approval.ProcessSubmitRequest req1=new Approval.ProcessSubmitRequest();
            req1.setObjectId(quoteObj.Id);
            req1.setSubmitterId(UserInfo.getUserId());
            req1.setNextApproverIds(new Id[] {quoteObj.ISR_Rep__c});
            Approval.ProcessResult result=Approval.process(req1);
            
            //return returnToQuote();
            return new PageReference ('/apex/CustomRefreshPage?Id='+quoteObj.Id);
        }
        catch(Exception e){
            
            system.debug('EXCEPTION OCCURED...'+e);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,''+e));
            return null;
        }
            
    }
    //added by lauren for salesforce1
    public void submitApprovalMobile(){
        if(selectedISRRep!=null && selectedISRRep!='--Select Approver--'){
            try{
                system.debug('lclc Approval method from Visualforce page salesforce1');
                system.debug('lclc..ISR REP...'+selectedISRRep);
                
                quoteObj.Quote_Approval__c=true;
                update quoteObj;
                system.debug('lclc now in approval ');

                Approval.ProcessSubmitRequest req1=new Approval.ProcessSubmitRequest();
                req1.setObjectId(quoteObj.Id);
                req1.setSubmitterId(UserInfo.getUserId());
                req1.setNextApproverIds(new Id[] {selectedISRRep});
                Approval.ProcessResult result=Approval.process(req1);
                isComplete=true;
                System.debug('lclc isComplete '+isComplete);
                
            }
            catch(Exception e){
                
                system.debug('EXCEPTION OCCURED...'+e);
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,''+e));
                isComplete=false;
            }
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please make a selection on the next approver before submitting for approval.'));
            isComplete=false;
        }
        
        return;

    }
    
    public PageReference returnToQuote(){
        return new PageReference ('/'+quoteObj.Id);
    }

    public void postToChatterPositive(Quote quoteVar){
        System.debug('lclc in positive ');

        quoteVar.Integration_Status__c='success';
        quoteVar.Last_Update_from_CastIron__c=DateTime.now();
        update quoteVar;

    }

    public void postToChatterNegative(Quote quoteVar){
        System.debug('lclc in negative');

        quoteVar.Integration_Status__c='error';
        update quoteVar;

    }
       
}