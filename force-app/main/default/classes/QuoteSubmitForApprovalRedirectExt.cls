public class QuoteSubmitForApprovalRedirectExt {
    
    Public Quote qt{get;set;}
    Public string qtid;
    Public id[] approverids;
    Public Set<id> GroupMemIds;
    
    public QuoteSubmitForApprovalRedirectExt(ApexPages.StandardController controller) {

    }
    
    Public pagereference Approvalredirect()
    {
        qtid= Apexpages.currentpage().getparameters().get('id');
        qt = [select id, Name, OpportunityId,ProductGroupMSP__c,BEP__c,MPS__c,Supplies_Buying_Location__c,Status, Division__c,
                    Approval_Requestor_Id__c, Last_Update_from_CastIron__c,Integration_Status__c,ISR_Rep__c,Quote_Approval__c,Set_Loss__c,Set_Marginal_Profit_Rate__c,
                    CON_Amount__c,SET_Amount__c,recordtype.name,ISR_Rep__r.profile.name,opportunity.Deal_Registration_Approval__c,(select id,Discount__c,unit_cost__c from QuoteLineItems)
                    from Quote where id=:qtid];
        Set<id> QLIWithoutUnitCost = new Set<id>();
        Set<id> QLICheckforWE = new Set<id>();
        for(QuoteLineItem qli : qt.QuoteLineItems){
            if(qli.unit_cost__c==null || qli.unit_cost__c==0){
                QLIWithoutUnitCost.add(qli.id);
            }
            if(qli.Discount__c>50){
                QLICheckforWE.add(qli.id);
            }
        }
        
        if(qt.Division__c == 'W/E' && QLICheckforWE.size()>0 && qt.opportunity.Deal_Registration_Approval__c!='Approved'){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'One or More Products have Discount More than 50%. Deal Registration must be Approved for Approving this Quote'));     
            return null;
        }
        //Added by Thiru 04/16/2018. Unit Cost Exception for SBS Dvision.
        if(qt.Division__c == 'SBS' && QLIWithoutUnitCost.size()>0){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Unit Cost is missing for one or more products in this Quote. Please contact related ISR for Unit Cost Input'));     
            return null;
        }
        //ISR step exception for user in this public group---05/29/2018
        GroupMemIds = new Set<id>();
        for(GroupMember mem : [SELECT GroupId,Id,UserOrGroupId FROM GroupMember where group.name='Bypass ISR Selection on Quote']){
            GroupMemIds.add(mem.UserOrGroupId);
        }
        
        id userid = UserInfo.getUserId();
        User u = [select id,profile.name,userrole.name from user where id=:userid];
        string rolename = u.userrole.name;
        
        if(rolename=='T2 CD Reps' || rolename=='T2 Inside Partner Support' || rolename=='Inside Sales Reps' || GroupMemIds.contains(userid)){
            if((!qt.ProductGroupMSP__c.containsIgnoreCase('KNOX') && qt.Division__c!='Mobile' && qt.Division__c!='W/E' && qt.Division__c!='SBS' && qt.Integration_Status__c == null) ||(qt.Integration_Status__c!=null &&  !qt.Integration_Status__c.equalsIgnoreCase('success'))){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Cannot submit for Approval as the invoice price is not updated.'));
                return null; 
            }else if(qt.status=='Draft' || qt.status=='Rejected' || qt.status=='Cancelled'){
                try{
                    if(!GroupMemIds.contains(userid)){
                        qt.ISR_Rep__c = userid;
                    }
                    //qt.ISR_Rep__c = userid;
                    qt.Quote_Approval__c=true;
                    update qt;
                    
                    Approval.ProcessSubmitRequest req1=new Approval.ProcessSubmitRequest();
                    req1.setObjectId(qt.Id);
                    req1.setSubmitterId(UserInfo.getUserId());
                    if(GroupMemIds.contains(userid)){
                        req1.setNextApproverIds(new Id[] {userid});
                    }else{
                        req1.setNextApproverIds(new Id[] {qt.ISR_Rep__c});
                    }
                    Approval.ProcessResult result=Approval.process(req1);
                    
                    Set<Id> pIds = (new Map<Id, ProcessInstance>([SELECT Id,Status,TargetObjectId FROM ProcessInstance where Status='Pending' and TargetObjectId =:qt.id])).keySet();
                    Set<Id> pInstanceWorkitems = (new Map<Id, ProcessInstanceWorkitem>([SELECT Id,Actorid,ProcessInstanceId FROM ProcessInstanceWorkitem WHERE ProcessInstanceId in :pIds])).keySet();
                    approverids = new list<id>();
                    for(ProcessInstanceWorkitem p : [SELECT Id,Actorid,ProcessInstanceId,OriginalActorId FROM ProcessInstanceWorkitem WHERE ProcessInstanceId in :pIds]){
                        approverids.add(p.OriginalActorId);
                    }
                    
                    system.debug('Approver ids---------->'+approverids);
                    if(approverids.size()>0){
                        id approverid = approverids[0];
                        if(approverid==userid){
                            Approval.ProcessWorkitemRequest[] allReq = New Approval.ProcessWorkitemRequest[]{}; 
                            for (Id pInstanceWorkitemsId:pInstanceWorkitems){
                                system.debug(pInstanceWorkitemsId);
                                    
                                    Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
                                    req2.setComments('');
                                    req2.setAction('Approve'); //to approve use 'Approve'
                                    req2.setNextApproverIds(new Id[] {UserInfo.getUserId()});
                                    // Use the ID from the newly created item to specify the item to be worked
                                    req2.setWorkitemId(pInstanceWorkitemsId);
                                    // Add the request for approval
                                    allReq.add(req2);
                            }
                            Approval.ProcessResult[] result2 =  Approval.process(allReq);
                        }
                    }
                    return new PageReference('/'+qt.id);
                }
                catch(Exception e){
                    system.debug('EXCEPTION OCCURED...'+e);
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,''+e));
                    return null;
                }
            }
            else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,' ALREADY_IN_PROCESS, This record is currently in an approval process. A record can be in only one approval process at a time.'));
                return null; 
            }
        }else{
            return new PageReference('/apex/QuoteSubmitApprovalButtonCustom?scontrolCaching=1&id='+qt.id);
        }
        
        return null;
    }

}