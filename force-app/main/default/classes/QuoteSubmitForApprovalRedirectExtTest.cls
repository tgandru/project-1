@istest(seealldata=true)
public class QuoteSubmitForApprovalRedirectExtTest{
    public static testmethod void test1(){
    test.startTest();
        User u = [select id,name,profile.name,userrole.name from user where profile.name='B2B ISR Sales User - Mobile' and userrole.name='Inside Sales Reps' and isactive=true limit 1];
        system.debug('USER Name---------->'+u.name);
        System.runAs(u){
        quote q = [select id, Name, OpportunityId,ProductGroupMSP__c,BEP__c,MPS__c,Supplies_Buying_Location__c,Status, Division__c,
                    Approval_Requestor_Id__c, Last_Update_from_CastIron__c,Integration_Status__c,ISR_Rep__c,Quote_Approval__c,Set_Loss__c,Set_Marginal_Profit_Rate__c,
                    CON_Amount__c,SET_Amount__c,recordtype.name
                    from Quote where createdby.profile.name='B2B ISR Sales User - Mobile' and status='Draft' and Integration_Status__c='Success' limit 1];
        Pagereference pg = page.QuoteSubmitForApprovalRedirect; 
        test.setcurrentpage(pg);
        ApexPages.currentPage().getparameters().put('id',q.id);    
        QuoteSubmitForApprovalRedirectExt cont = new QuoteSubmitForApprovalRedirectExt(new ApexPages.StandardController(q));
        cont.Approvalredirect();
        }
    test.stopTest();    
    }
    
     Public Static testmethod void test2(){
         test.startTest();
         
        quote quot = [select id, Name, OpportunityId,ProductGroupMSP__c,BEP__c,MPS__c,Supplies_Buying_Location__c,Status, Division__c,
                    Approval_Requestor_Id__c, Last_Update_from_CastIron__c,Integration_Status__c,ISR_Rep__c,Quote_Approval__c,Set_Loss__c,Set_Marginal_Profit_Rate__c,
                    CON_Amount__c,SET_Amount__c,recordtype.name
                    from Quote where createdby.profile.name='B2B ISR Sales User - Mobile' and status='Draft' and Integration_Status__c='Success' limit 1];
         
            PageReference pageRef = Page.QuoteSubmitApprovalButtonCustom;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('id',quot.Id);
            QuoteSubmitApprovalButtonCustomExtension cont = new QuoteSubmitApprovalButtonCustomExtension(new ApexPages.StandardController(quot));
            List<SelectOption> ISRReps=new List<SelectOption>();
            List<User> ISRRepsUsers=[select Id,Name from User where Profile.Name='B2B ISR Sales User - Mobile' AND IsActive=true limit 1];
            user us=[select Id,Name from User where Profile.Name='B2B ISR Sales User - Mobile' AND IsActive=true limit 1];
             for(User ur:ISRRepsUsers)
                        {
                            ISRReps.add(new SelectOption(ur.Id,ur.Name));
                        }
               cont.ISRReps= ISRReps;
             String selectedISRRep=cont.selectedISRRep;
             cont.selectedISRRep=String.valueOf(us.id);
             cont.isInMobile=false;
              cont.isComplete=false;
            cont.submitApproval();
            cont.submitApproval2();
            cont.submitApprovalMobile();
            cont.returnToQuote();
            cont.postToChatterNegative(quot);
            cont.postToChatterPositive(quot);
          
      test.StopTest();
         
         
     }
     
      Public Static testmethod void test3(){
         test.startTest();
         
        quote quot = [select id, Name, OpportunityId,ProductGroupMSP__c,BEP__c,MPS__c,Supplies_Buying_Location__c,Status, Division__c,
                    Approval_Requestor_Id__c, Last_Update_from_CastIron__c,Integration_Status__c,ISR_Rep__c,Quote_Approval__c,Set_Loss__c,Set_Marginal_Profit_Rate__c,
                    CON_Amount__c,SET_Amount__c,recordtype.name
                    from Quote where createdby.profile.name='B2B ISR Sales User - Mobile' and Division__c='SBS'  limit 1];
         
            PageReference pageRef = Page.QuoteSubmitApprovalButtonCustom;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('id',quot.Id);
            QuoteSubmitApprovalButtonCustomExtension cont = new QuoteSubmitApprovalButtonCustomExtension(new ApexPages.StandardController(quot));
            List<SelectOption> ISRReps=new List<SelectOption>();
            List<User> ISRRepsUsers=[select Id,Name from User where Profile.Name='B2B ISR Sales User - Mobile' AND IsActive=true limit 1];
            user us=[select Id,Name from User where Profile.Name='B2B ISR Sales User - Mobile' AND IsActive=true limit 1];
             for(User ur:ISRRepsUsers)
                        {
                            ISRReps.add(new SelectOption(ur.Id,ur.Name));
                        }
               cont.ISRReps= ISRReps;
             String selectedISRRep=cont.selectedISRRep;
             cont.selectedISRRep=String.valueOf(us.id);
             cont.isInMobile=True;
              cont.isComplete=false;
            cont.submitApproval();
            cont.submitApproval2();
            cont.submitApprovalMobile();
            cont.returnToQuote();
            cont.postToChatterNegative(quot);
            cont.postToChatterPositive(quot);
          
      test.StopTest();
         
         
     }
     
         Public Static testmethod void test4(){
         test.startTest();
         
        quote quot = [select id, Name, OpportunityId,ProductGroupMSP__c,BEP__c,MPS__c,Supplies_Buying_Location__c,Status, Division__c,
                    Approval_Requestor_Id__c, Last_Update_from_CastIron__c,Integration_Status__c,ISR_Rep__c,Quote_Approval__c,Set_Loss__c,Set_Marginal_Profit_Rate__c,
                    CON_Amount__c,SET_Amount__c,recordtype.name
                    from Quote where createdby.profile.name='B2B ISR Sales User - Mobile' and Division__c='IT' and Integration_Status__c=null limit 1];
         
            PageReference pageRef = Page.QuoteSubmitApprovalButtonCustom;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('id',quot.Id);
            QuoteSubmitApprovalButtonCustomExtension cont = new QuoteSubmitApprovalButtonCustomExtension(new ApexPages.StandardController(quot));
            List<SelectOption> ISRReps=new List<SelectOption>();
            List<User> ISRRepsUsers=[select Id,Name from User where Profile.Name='B2B ISR Sales User - Mobile' AND IsActive=true limit 1];
            user us=[select Id,Name from User where Profile.Name='B2B ISR Sales User - Mobile' AND IsActive=true limit 1];
             for(User ur:ISRRepsUsers)
                        {
                            ISRReps.add(new SelectOption(ur.Id,ur.Name));
                        }
               cont.ISRReps= ISRReps;
             String selectedISRRep=cont.selectedISRRep;
             cont.selectedISRRep=String.valueOf(us.id);
             cont.isInMobile=True;
              cont.isComplete=false;
            cont.submitApproval();
            cont.submitApproval2();
            cont.submitApprovalMobile();
            cont.returnToQuote();
            cont.postToChatterNegative(quot);
            cont.postToChatterPositive(quot);
          
      test.StopTest();
         
         
     }
     
    
}