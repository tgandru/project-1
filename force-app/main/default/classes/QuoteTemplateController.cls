public class QuoteTemplateController {
    //Attributes
    public Quote currentQuote;
    public Id quoId {get;set;}
    public List<Reseller__c> resellers;
    public List<QuoteLineItem> productLineItems;
    public Document samsungLogo;
    public Document SamsungBlueLogo{get;set;}
    public String orgId;
    public String domainId;
    public List<ProcessInstance> approvalProcessItems;
    public String approvalProcessStepsComments;
    
    //Constructor
    public QuoteTemplateController() { }
    
    //Getters
    public Document getSamsungLogo() {
        if(samsungLogo == null) {
            if(!Test.isRunningTest()) {
                try{
                    samsungLogo = [Select Id, Url from Document where DeveloperName = 'SamsungPDFLogo' Limit 1];   
                } catch(Exception e) { }
            } else
                samsungLogo = new Document();   
        }       
        return samsungLogo;
    }
    
    public Document getSamsungBlueLogo() {
        if(samsungBlueLogo == null) {
            if(!Test.isRunningTest()) {
                try{
                    samsungBlueLogo = [Select Id, Url from Document where DeveloperName = 'SamsungPDFBlueLogo' Limit 1];   
                } catch(Exception e) { }
            } else
                samsungBlueLogo = new Document();   
        }       
        return samsungBlueLogo;
    }
    
    public String getOrgId() {
        if(orgId == null) {
            orgId = UserInfo.getOrganizationId();
        }
        return orgId;
    }
    
    public String getDomainId() {
        if(domainId == null) {
            domainId = System.URL.getSalesforceBaseURL().getHost();
        }
        return domainId;
    }
    
    public List<ProcessInstance> getapprovalProcessItems() {
    	if (approvalProcessItems == null) {
    		approvalProcessItems = [Select p.TargetObjectId, p.SubmittedBy.Name, p.SubmittedById, p.Status, 
    								p.ProcessDefinition.State, p.ProcessDefinition.DeveloperName, p.ProcessDefinition.Name, 
    								p.ProcessDefinitionId, p.LastActor.Name, p.LastActorId, p.CompletedDate, 
    								(Select StepStatus, Comments From Steps) 
    								From ProcessInstance p Where targetObjectId =: quoId];
    	}
    	return approvalProcessItems;
    }
    
    public String getapprovalProcessStepsComments() {
    	if (approvalProcessStepsComments == null) {
    		approvalProcessStepsComments = '';
    		List<ProcessInstance>  tempProcess = [Select p.TargetObjectId, p.SubmittedBy.Name, p.SubmittedById, p.Status, 
    								p.ProcessDefinition.State, p.ProcessDefinition.DeveloperName, p.ProcessDefinition.Name, 
    								p.ProcessDefinitionId, p.LastActor.Name, p.LastActorId, p.CompletedDate, 
    								(Select StepStatus, Comments From Steps) 
    								From ProcessInstance p Where targetObjectId =: quoId];
    		if (!tempProcess.isEmpty()) {
    			for (ProcessInstance pi : tempProcess) {
    				for (ProcessInstanceStep pis: pi.Steps) {
    					if(pis.Comments != null) {
    						approvalProcessStepsComments = pis.Comments+', '+approvalProcessStepsComments;
    					}
    				}
    			}
    		}
    	}
    	return approvalProcessStepsComments;
    }
    
    public Quote getCurrentQuote() {
        if(currentQuote == null) {
            try {
                currentQuote = [Select Id, Name, QuoteNumber, Account.Name, Distributor__r.Name, DistributorAddress__c, RequestType__c, PricingNotes__c,               
                            Terms__c, Valid_From_Date__c, ExpirationDate, CreatedBy.Name, CreatedBy.Email, External_SPID__c, End_Customer_Address__c, 
                            Opportunity.Opportunity_Number__c, Opportunity.Owner.Name, Opportunity.Owner.Email, Distributor_Contact_Details__c, TotalCost__c, 
                            TotalDiscount__c, Opportunity.Name, Opportunity.StageName, Opportunity.CloseDate, Opportunity.Amount, Status, Division__c,
                            Product_Division__c, TotalPrice, ProductGroupMSP__c, MIN_GM__c, MAX_DISCOUNT__c, TotalGM__c, TotalDiscountPercent__c, BEP__c,
                            MPS__c, Supplies_Buying_Location__c, Set_Marginal_Profit_Rate__c, InternalNotes__c, Opportunity.Channel__c, additional_conditions__c,
                            VersionNumber__c,contactid,Custom_Deliverable_Details__c,Samsung_Contact__c,subtotal,total_device_quantity__c,
                            Sold_To__c,Ship_To__c,Primary_Reseller__c,
                            Additional_Terms__c
                            from Quote where Id = :quoId];   
            } catch(Exception e) { }
        }
        return currentQuote;
    }
    
    public List<Reseller__c> getResellers() {
        if(currentQuote == null) {
            getCurrentQuote();
        }
        if(currentQuote == null) {
            return null;
        }
        if(resellers == null) {
            resellers = [Select Reseller_Name__c, Reseller_Contact_Info__c, Partner_Reseller__c, BillingAddress__c
                         from Reseller__c where Quote__c = :currentQuote.Id];
        }
        return resellers;
    }
    
    public List<QuoteLineItem> getProductLineItems() {
        if(currentQuote == null) {
            getCurrentQuote();
        }
        if(currentQuote == null) {
            return null;
        }
        if(productLineItems == null) {
            productLineItems = [Select Product2.Name,Product2.SAP_Material_ID__c, Description, Quantity, ListPrice, UnitPrice, TotalPrice, Discount__c, 
                                Model_Name_Nickname__c, Requested_Price__c, TotalDiscount__c, Total_List_Price__c, Total_Requested_Price__c 
                                from QuoteLineItem where QuoteId = :currentQuote.Id];
            
            for(QuoteLineItem qli : productLineItems) {
                if(qli.UnitPrice == null) qli.UnitPrice = 0;
                if(qli.Total_List_Price__c == null) qli.Total_List_Price__c = 0;
                if(qli.Total_Requested_Price__c == null) qli.Total_Requested_Price__c = 0;
                if(qli.Discount__c == null) qli.Discount__c = 0;
            }
        }
        return productLineItems;
    }
}