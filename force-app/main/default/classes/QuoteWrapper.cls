/*
 * Model for Quote Wrapper used in CreditMemoAddQuoteExtension.cls
 * Author: Eric Vennaro
 * 
*/

public class QuoteWrapper {
    public Boolean isSelected {get;set;}
    public Quote quote {get;set;}
    
    public QuoteWrapper (Quote quote){
        this.quote = quote;
        this.isSelected = false;
    }
}