/*
 * Model for Quote Wrapper Data Access Object used in CreditMemoAddQuoteExtension.cls
 * Author: Eric Vennaro
 * 
*/
public class QuoteWrapperDAO {
	public String divisionValue;
    public String DivisionInfoMsg;
	public QuoteWrapperDAO(String cmDivision) {
        divisionValue = cmDivision;
	}

    public List<QuoteWrapper> getAvailableQuotes(String queryString) {
    	List<QuoteWrapper> availableQuotesWrapper = new List<QuoteWrapper>();
        try {
            List<Quote> availableQuotes = Database.query(queryString);
            if(availableQuotes != null) {
                for(Quote quote : availableQuotes) {
                    if(quote.Division__c != divisionValue){
                        if(String.isBlank(DivisionInfoMsg)){
                            DivisionInfoMsg = 'Division of Credit Memo does not match the Division for the Quote(s) :'+ quote.External_SPID__c+' - '+quote.Division__c+ ' ';
                        } 
                        else {
                            DivisionInfoMsg = ','+quote.External_SPID__c+' - '+quote.Division__c+' ';
                        }                        
                    }
                    availableQuotesWrapper.add(new QuoteWrapper(quote));
                }
            }
            return availableQuotesWrapper;
        } catch(Exception e) {
            throw new QuoteDAOException(e);
        }
    }
}