/*
 * Facade layer for Quote Wrapper used in CreditMemoAddQuoteExtension.cls, could be extended to include different queries.
 * Author: Eric Vennaro
 * 
*/
public class QuoteWrapperFacade {
    
    private QuoteWrapperDAO quoteDAO;

    public QuoteWrapperFacade(QuoteWrapperDAO quoteDAO) {
        this.quoteDAO = quoteDAO;
    }

    public List<QuoteWrapper> getAvailableQuotes(List<CreditMemoQuoteWrapper> initialCart, List<String> spas) {
        String queryString = 'SELECT Id, Quote.Account.Name, External_SPID__c, Quote_Number__c, ExpirationDate, Valid_From_Date__c,Product_Division__c,ProductGroupMSP__c,Division__c, Name ' + 
                             'FROM Quote ' + 
                             'WHERE (Status = '+'\'Approved\' OR Status = '+'\'Presented\')';

        if(initialCart.size() > 0) {
            String queryClause = ' and Id NOT IN (';
            for(CreditMemoQuoteWrapper shoppingCart : initialCart){
                queryClause += '\'' + (String) shoppingCart.quoteId + '\',';
            }
            String trimmedQueryPiece = queryClause.substring(0, queryClause.length()-1) + ')';
            queryString += trimmedQueryPiece;
        }

        String queryClauseTwo = ' and External_SPID__c IN (';
        for(String spa : spas){
        	queryClauseTwo += '\'' + spa.trim() + '\',';
        }
        String trimmedQueryPieceTwo = queryClauseTwo.substring(0, queryClauseTwo.length() - 1) + ')';
        queryString += trimmedQueryPieceTwo;
        
        queryString += ' ORDER BY External_SPID__c LIMIT 100';
        try {
        	List<QuoteWrapper> quoteWrappers = quoteDAO.getAvailableQuotes(queryString);
        	return quoteWrappers;
    	} catch(Exception e) {
    		throw new QuoteDAOException(e);
    	}
    }
}