/**
 * Created by ms on 2017-07-18.
 * author : sungil.kim, I2MAX
 */

public with sharing class RESTTool {
    public String requestBody {get;set;}
    public String responseBody {get;set;}
    public String responseToolAPI {get;set;}
    public String svcId {get;set;}
    public string guid {get; set;}
    public RESTTool() {}

    public void init() {
        svcId = 'SVC-01';
        requestBody=label.SVC_01;
        Blob b = Crypto.GenerateAESKey(128);
        String h = EncodingUtil.ConvertTohex(b);
        String s = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
         guid=s;
    }

    public void doPost() {
        responseBody = webCallOut(requestBody);
    }
    
    Public void populatesample(){
      if(svcId=='SVC-01'){
           requestBody=label.SVC_01;
      }else if(svcId=='SVC-03'){
           requestBody=label.SVC_03;
      }else if(svcId=='SVC-04'){
           requestBody=label.SVC_04;
      }else if(svcId=='SVC-05'){
           requestBody=label.SVC_05;
      }else if(svcId=='SVC-06'){
            requestBody=label.SVC_06+'\n'+label.SVC_06_IMEI;
      }else if(svcId=='SVC-07'){
           requestBody=label.SVC_07;
      }else if(svcId=='SVC-09'){
           requestBody=label.SVC_09;
      }
      
        
    }
    
    public void generateguid(){
         Blob b = Crypto.GenerateAESKey(128);
         String h = EncodingUtil.ConvertTohex(b);
        String s = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
         guid=s;
    }
    
    

    private String webCallOut(String body) {
        string partialEndpoint = Integration_EndPoints__c.getInstance(svcId).partial_endpoint__c;
        system.debug('=================================> Start Request');

        Httprequest httpReq = new Httprequest();
        HttpResponse httpRes = new HttpResponse();
        Http http = new Http();
        String resBody = null;

        System.debug('requestBody : '+ body);

        httpReq.setMethod('POST');
        httpReq.setBody(body);
        httpReq.setEndpoint('callout:X012_013_ROI' + partialEndpoint);
        httpReq.setTimeout(120000);
        httpReq.setHeader('Content-Type', 'application/json');
        httpReq.setHeader('Accept', 'application/json');

        httpRes = http.send(httpReq);

        resBody = httpRes.getBody();
        if (resBody.length() > 1900000) {
            System.debug('responseBody : null');
        } else {
            System.debug('responseBody : '+ resBody);
        }

        return resBody;
    }
}