/**
 * Created by ms on 2017-08-10.
 */

@IsTest
private class RESTTool_Test {
    @testSetup static void setup() {
        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='SVC-01';
        iep2.endPointURL__c = 'https://www.google.com';
        iep2.layout_id__c = 'LAY024197';
        iep2.partial_endpoint__c = '/SFDC_IF_012';
        
        insert iep2;
    }

    static testMethod void testBehavior() {
        Test.startTest();
        // for error
        String body = '{"aa":"bb"}';

        TestMockSingleCallout singleMock = new TestMockSingleCallout(body);

        Test.setMock(HttpCalloutMock.class, singleMock);

        RESTTool a = new RESTTool();

        a.init();
        a.requestBody = 'xxx';
        a.doPost();
        a.generateguid();
        a.populatesample();
        
        Test.stopTest();
    }
    
    static testMethod void testBehavior2() {
        Test.startTest();
        // for error
        String body = '{"aa":"bb"}';

        TestMockSingleCallout singleMock = new TestMockSingleCallout(body);

        Test.setMock(HttpCalloutMock.class, singleMock);

        RESTTool a = new RESTTool();
         
        a.init();
        a.requestBody = 'xxx';
        a.doPost();
        a.generateguid();
         a.svcId='SVC-06';
        a.populatesample();
        
        Test.stopTest();
    }
}