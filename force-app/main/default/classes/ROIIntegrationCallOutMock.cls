/*
This is the Utility class for REST Http mock ups

Author : Aarthi R C

Created on : 4/11/2016

*/
@isTest
public class ROIIntegrationCallOutMock{


public class ROIIntegrationSuccessTest implements HttpCalloutMock {

public HTTPResponse respond(HTTPRequest request) {
    System.debug('**** inside my mock');
     HttpResponse response = new HttpResponse();
    //String response = '{"input":{"MSGGUID": "b336d2a9-f8cb-a399-8eea-dc11782453c9","IFID": "LAY024197","IFDate": "1603161220013","MSGSTATUS": "S","ERRORTEXT": null,"ERRORCODE": null}}';
    String res ='{"inputHeaders": {"MSGGUID": "b336d2a9-f8cb-a399-8eea-dc11782453c9","IFID": "LAY024197","IFDate": "1603161220013","MSGSTATUS": "S","ERRORTEXT": null,"ERRORCODE": null}}';
    response.setHeader('Content-Type', 'application/json');
    response.setBody(res);
    response.setStatusCode(200);
    return response;
    
}

}

public class ROIIntegrationFailureTestCase implements HttpCalloutMock{

public HTTPResponse respond(HTTPRequest req)
{
    HttpResponse resp = new HttpResponse();
    String response = '{"input":{"MSGGUID": "b336d2a9-f8cb-a399-8eea-dc11782453c9","IFID": "LAY024197","IFDate": "1603161220013","MSGSTATUS": "failed","ERRORTEXT":"E1234","ERRORCODE": "Field validation failed"}}';
    resp.setHeader('Content-Type', 'application/json');
    resp.setBody(response);
    resp.setStatusCode(200);
    return resp;
}
}

public class ROIIntegrationSystemError implements HttpCalloutMock{
public HTTPResponse respond(HTTPRequest req)
{
    HttpResponse res = new HttpResponse();
    String response = '{"input":{"MSGGUID": "b336d2a9-f8cb-a399-8eea-dc11782453c9","IFID": "LAY024197","IFDate": "1603161220013","MSGSTATUS": "failed","ERRORTEXT": "F2345","ERRORCODE": "System Down"}}';
    res.setHeader('Content-Type', 'application/json');
    res.setBody(response);
    res.setStatusCode(400);
    return res;
}
}
}