@istest(seealldata=true)
private class ROIIntegrationHelperTest {

    @isTest static void ROITest(){
        Quote quoteVar = [select id, Name, ROI_Requestor_Id__c, BEP__c, Status from Quote where ProductGroupMSP__c includes('PRINTER','A3 COPIER','FAX/MFP','SOLUTION S/W') Limit 1];
        System.debug('quoteVar' + quoteVar.Id);
        message_queue__c mq = new message_queue__c(Integration_Flow_Type__c = '012', retry_counter__c = 0, Identification_Text__c = quotevar.id, Object_Name__c='Quote', Status__c='Not Started');
        insert mq;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HttpMockUps());
        RoiIntegrationHelper.callRoiEndpoint(quoteVar.Id);
        RoiIntegrationHelper.handleError(mq,null,'fields','error message');
        Test.stopTest();

    }
    public class HttpMockUps implements HttpCalloutMock {
    public HTTPResponse respond(HTTPRequest req) {
        // Create a fake response.
        // Set response values, and 
        // return response.
        HttpResponse response = new HttpResponse();
            String res ='{"inputHeaders": {"MSGGUID": "b336d2a9-f8cb-a399-8eea-dc11782453c9","IFID": "LAY024197","IFDate": "1603161220013","MSGSTATUS": "S","ERRORTEXT": null,"ERRORCODE": null}}';
            response.setHeader('Content-Type', 'application/json');
            response.setBody(res);
            response.setStatusCode(200);
            return response;
    }
    }
}