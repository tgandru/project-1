public with sharing class ROIQuoteButtonExtension {
    
    public static Quote quoteObj{get;set;}
    public List<QuoteLineItem> qliLst{get;set;}
    public Boolean qliToCallout {get;set;}
    //public set<QuoteLineItem> qliset{get;set;}


    public ROIQuoteButtonExtension(ApexPages.StandardController controller) {
        
        quoteObj=(Quote)controller.getRecord();
        quoteObj=[select id, Name, RecordType.Name,ROI_Requestor_Id__c, BEP__c, Status, Integration_Status__c, ProductGroupMSP__c, Last_Update_from_CastIron__c,
                        MPS__c,Supplies_Buying_Location__c
                    from Quote where Id=:quoteObj.Id];
        System.debug('quote' + quoteObj);
        //Added by Thiru --- Starts Here-----8/29/2016
        qlilst = [select id,Quoteid,SAP_material_id__c,ListPrice,Requested_Price__c from quotelineitem where Quoteid =:quoteObj.id];
        System.debug('quote' + qlilst);
        
        //added in the following to see if 8 needs to be called as well as 12 or just 12
        qliToCallout=false;
        
        //quoteObj.BEP__c==null && 
        if(quoteObj.Status=='Draft' && (quoteObj.ProductGroupMSP__c.contains('A3 COPIER') || quoteObj.ProductGroupMSP__c.contains('PRINTER') ||quoteObj.ProductGroupMSP__c.contains('FAX/MFP'))){
            qliToCallout = TRUE;
        }
    }

    
    public PageReference passQuoteId(){
        //@TODO are we sure that it's still status=needs review, this is not an option in the picklist
        System.debug('*****************quote' + quoteObj);
        //Added by Thiru----Starts Here----09/01/2016.
        for(quotelineitem QO : qlilst)
        {
            if(QO.Requested_Price__c > QO.ListPrice){
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'There is a line that has Requested Price is higher than Invoice Price, Please correct it and try again'));
               return null;
             }
            for(quotelineitem QI : qlilst)
            {
                If(QO.Id != QI.ID && QO.SAP_material_id__c == QI.SAP_material_id__c && QO.Requested_Price__c != QI.Requested_Price__c)
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'There are same model with different requested price, please check it out'));
                    return Null;
                }
            }
        }
        //Added by Thiru---Ends Here----09/01/2016.           
        if(quoteObj.Integration_Status__c != null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Cannot update Invoice price as it is already executed once.'));
            return null;
        }
        else{
            if(quoteObj.BEP__c == null && 
                (quoteObj.MPS__c  == null || quoteObj.Supplies_Buying_Location__c ==null ) && 
                quoteObj.Status == 'Draft' &&
                (quoteObj.ProductGroupMSP__c == 'A3 COPIER' ||  quoteObj.ProductGroupMSP__c == 'PRINTER' ||   quoteObj.ProductGroupMSP__c == 'FAX/MFP')
                ){
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.severity.Error,'MPS and Supplies Buying Location fields cannot be blank when BEP field is blank, Quote status is Draft and Product Group is either A3 COPIER, PRINTER or FAX/MFP.'));
                return null;
            }
            if(qliToCallout){
                //8 needs to be called, 
                //qlitocallout will only be true if the bep=null, the status is draft, and the product group is correct. .: no need to do if statement again
                system.debug('lclc in 8 to 12');
                if([select id from message_queue__c where Integration_Flow_Type__c = '008 to 012' and Identification_Text__c = :quoteObj.Id and Status__c='not started'].isEmpty()){
                    //RoiIntegrationHelper.callRoiEndpoint(quoteObj.Id);
                    //CastIronIntegrationHelper.sendMQrealTime008to012(quoteObj.Id);
                    CastIronIntegrationHelper.sendMQrealTime(quoteObj.Id); //------Added on 08/23/2017 to send the callout only to 008.
                    quoteObj.Approval_Requestor_Id__c=UserInfo.getUserId();
                    quoteObj.Integration_Status__c='pending';
                    quoteObj.ROI_Requestor_Id__c=UserInfo.getUserId();
                    update quoteObj;
                }
            }
            else{
                //8 does not need to be called, so just call 12. 
               if( quoteObj.Status=='Draft' && (quoteObj.ProductGroupMSP__c.contains('A3 COPIER') || quoteObj.ProductGroupMSP__c.contains('PRINTER') ||quoteObj.ProductGroupMSP__c.contains('FAX/MFP'))){
                    System.debug('lclc in it');
                    if([select id from message_queue__c where Integration_Flow_Type__c = '012' and Identification_Text__c = :quoteObj.Id and Status__c='not started'].isEmpty()){
                        RoiIntegrationHelper.callRoiEndpoint(quoteObj.Id);
                        quoteObj.Integration_Status__c='pending';
                        quoteObj.ROI_Requestor_Id__c=UserInfo.getUserId();
                        update quoteObj;
                    }
                }else{
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Quote Status should be Draft to process.'));   
                    return null;   
                }
            }
        }

        return new PageReference ('/'+quoteObj.Id);

    }

    public void postToChatterPositive(Quote quoteVar){
        System.debug('lclc in positive ');

        quoteVar.Integration_Status__c='success';
        update quoteVar;

    }

    public void postToChatterNegative(Quote quoteVar){
        System.debug('lclc in negative');

        quoteVar.Integration_Status__c='error';
        update quoteVar;
    }

}