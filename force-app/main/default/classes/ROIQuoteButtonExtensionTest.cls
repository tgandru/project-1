@isTest
private class ROIQuoteButtonExtensionTest {
    
    //Since the traditional way of testing i.e creating test datas and call the callout using mockup is throwing 'Uncommitted or pending work exception', have tried the seealldata to get the quote id from the org.

    @testSetUp static void testDatas()
    {

        
        Id endUserRT = TestDataUtility.retrieveRecordTypeId('End_User', 'Account'); 
        Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
        //Jagan: Test data for fiscalCalendar__c
        Test.loadData(fiscalCalendar__c.sObjectType, 'FiscalCalendarTestData');

        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
    
        List<Roll_Out_Schedule_Constants__c> roscs=new List<Roll_Out_Schedule_Constants__c>();
        Roll_Out_Schedule_Constants__c rosc1=new Roll_Out_Schedule_Constants__c(Name='Roll Out Schedule Parameters',Average_Week_Count__c=4.33,DML_Row_Limit__c=3200);
        roscs.add(rosc1);
        insert roscs;
            
        // Create Account
        Test.startTest();
        Account acc = TestDataUtility.createAccount(TestDataUtility.generateRandomString(5));
        //acc.recordTypeId = endUserRT;
        insert acc;
        //Test.stopTest();

    
        // Create a partner Account
        //Test.startTest();
        Account paAcct = TestDataUtility.createAccount(TestDataUtility.generateRandomString(6));
        paAcct.SAP_Company_Code__c='Testing';
        //paAcct.recordTypeId =  directRT;
        insert paAcct;
        //Test.stopTest();
    
        // Create custom Price Book
        //Test.startTest();
        PriceBook2 pb = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(6));
        insert pb;
        //Test.stopTest();
    
        // create Opportunity
        //Test.startTest();
        Opportunity opp = TestDataUtility.createOppty('Opp created from test', acc, pb, 'Tender', 'IT', 'FAX/MFP', 'Identified');
        insert opp;
        //Test.stopTest();
    
        // Create Partner for this Opportunity
        //Test.startTest();
        Partner__c partner = TestDataUtility.createPartner(opp,acc,pAacct,'Distributor');
        partner.Is_Primary__c = true;
        insert partner;
        //Test.stopTest();
    
        // Create Products
        //Test.startTest();
        List<Product2> prods=new List<Product2>();
        Product2 standaloneProd=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Standalone Prod1', 'Printer[C2]', 'FAX/MFP', 'H/W');
        prods.add(standaloneProd);
        Product2 parentProd1=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Parent Prod1', 'Printer[C2]', 'FAX/MFP', 'H/W');
        prods.add(parentProd1);
        Product2 childProd1=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod1', 'Printer[C2]', 'PRINTER', 'Service pack');
        prods.add(childProd1);
        Product2 childProd2=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod2', 'Printer[C2]', 'PRINTER', 'Service pack');
        prods.add(childProd2);
        Product2 parentProd2=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Parent Prod2', 'Printer[C2]', 'FAX/MFP', 'H/W');
        prods.add(parentProd2);
        Product2 childProd3=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod3', 'Printer[C2]', 'PRINTER', 'Service pack');
        prods.add(childProd3);
        Product2 childProd4=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod4', 'Printer[C2]', 'PRINTER', 'Service pack');
        prods.add(childProd4);
        insert prods;
        //Test.stopTest();
    
        // Create Product Relationship
        //Test.startTest();
        List<Product_Relationship__c> prs=new List<Product_Relationship__c>();
        Product_Relationship__c pr1=TestDataUtility.createProductRelationships(childProd1, parentProd1);
        prs.add(pr1);
        Product_Relationship__c pr2=TestDataUtility.createProductRelationships(childProd2, parentProd1);
        prs.add(pr2);
        Product_Relationship__c pr3=TestDataUtility.createProductRelationships(childProd3, parentProd2);
        prs.add(pr3);
        Product_Relationship__c pr4=TestDataUtility.createProductRelationships(childProd4, parentProd2);
        prs.add(pr4);
        insert prs;
        //Test.stopTest();
    
        // Create PriceBookEntry with standard pricebook
    
        //List<PriceBookEntry> pbes1=new List<PriceBookEntry>();
        List<PriceBookEntry> pbes=new List<PriceBookEntry>();
        /*Id pricebookId = Test.getStandardPricebookId();
    
    
        PriceBookEntry standaloneProdPBE1=TestDataUtility.createPriceBookEntry(standaloneProd.Id, pricebookId);
        standaloneProdPBE1.UseStandardPrice = true;
        pbes1.add(standaloneProdPBE1);
        PriceBookEntry parentProd1PBE1=TestDataUtility.createPriceBookEntry(parentProd1.Id,pricebookId);
        parentProd1PBE1.UseStandardPrice = true;
        pbes1.add(parentProd1PBE1);
        PriceBookEntry childProd1PBE1=TestDataUtility.createPriceBookEntry(childProd1.Id, pricebookId);
        childProd1PBE1.UseStandardPrice = true;
        pbes1.add(childProd1PBE1);
        PriceBookEntry childProd2PBE1=TestDataUtility.createPriceBookEntry(childProd2.Id, pricebookId);
        childProd2PBE1.UseStandardPrice = true;
        pbes1.add(childProd2PBE1);
        PriceBookEntry parentProd2PBE1=TestDataUtility.createPriceBookEntry(parentProd2.Id, pricebookId);
        parentProd2PBE1.UseStandardPrice = true;
        pbes1.add(parentProd2PBE1);
        insert pbes1;*/
        // create PriceBookentry for custom pricebook
        //Test.startTest();
        PriceBookEntry standaloneProdPBE=TestDataUtility.createPriceBookEntry(standaloneProd.Id, pb.Id);
        pbes.add(standaloneProdPBE);
        PriceBookEntry parentProd1PBE=TestDataUtility.createPriceBookEntry(parentProd1.Id,pb.Id);
        pbes.add(parentProd1PBE);
        PriceBookEntry childProd1PBE=TestDataUtility.createPriceBookEntry(childProd1.Id, pb.Id);
        pbes.add(childProd1PBE);
        PriceBookEntry childProd2PBE=TestDataUtility.createPriceBookEntry(childProd2.Id, pb.Id);
        pbes.add(childProd2PBE);
        PriceBookEntry parentProd2PBE=TestDataUtility.createPriceBookEntry(parentProd2.Id, pb.Id);
        pbes.add(parentProd2PBE);
        insert pbes;
        //Test.stopTest();
    
    
    
        // Create Opportunity Line item
        //Test.startTest();
        List<OpportunityLineItem> olis=new List<OpportunityLineItem>();
        OpportunityLineItem standaloneProdOLI=TestDataUtility.createOpptyLineItem(standaloneProd, standaloneProdPBE, opp);
        standaloneProdOLI.Quantity = 10;
        standaloneProdOLI.TotalPrice = standaloneProdOLI.Quantity * standaloneProdPBE.UnitPrice;
        olis.add(standaloneProdOLI);
        OpportunityLineItem parentProd1OLI=TestDataUtility.createOpptyLineItem(parentProd1, parentProd1PBE, opp);
        parentProd1OLI.Quantity = 10;
        parentProd1OLI.TotalPrice = parentProd1OLI.Quantity * parentProd1PBE.UnitPrice;
        olis.add(parentProd1OLI);
        OpportunityLineItem childProd1OLI=TestDataUtility.createOpptyLineItem(childProd1, childProd1PBE, opp);
        childProd1OLI.Quantity = 10;
        childProd1OLI.TotalPrice = childProd1OLI.Quantity * childProd1PBE.UnitPrice;
        olis.add(childProd1OLI);
        OpportunityLineItem childProd2OLI=TestDataUtility.createOpptyLineItem(childProd2, childProd2PBE, opp);
        childProd2OLI.Quantity = 10;
        childProd2OLI.TotalPrice = childProd2OLI.Quantity * childProd2PBE.UnitPrice;
        olis.add(childProd2OLI);
        OpportunityLineItem parentProd2OLI=TestDataUtility.createOpptyLineItem(parentProd2, parentProd2PBE, opp);
        parentProd2OLI.Quantity = 10;
        parentProd2OLI.TotalPrice = parentProd2OLI.Quantity * parentProd2PBE.UnitPrice;
        olis.add(parentProd2OLI);
        insert olis;
        //Test.stopTest();

        // Create Quote
        //Test.startTest();
        Quote quote = TestDataUtility.createQuote(TestDataUtility.generateRandomString(6), opp, pb);
        quote.BEP__c='';
        quote.Status ='Draft';
        quote.ROI_Requestor_Id__c=UserInfo.getUserId();
        insert quote;
        //Test.stopTest();

        // Create QuoteLineItem
        // Have to give OLIID__c as it is a reference to look for the QLI   
        //Have to give the Requested_Price__c as Quote Line Item Managementv1 flow needs this
        //Test.startTest();
        List<QuoteLineItem> qlis = new List<QuoteLineItem>();
        QuoteLineItem standardQLI = TestDataUtility.createQuoteLineItem(quote.Id, standaloneProd, standaloneProdPBE, standaloneProdOLI );
        standardQLI.Quantity = 25;
        standardQLI.UnitPrice = 100;
        standardQLI.Requested_Price__c=100;
        qlis.add(standardQLI);
        QuoteLineItem parentProd1QLI = TestDataUtility.createQuoteLineItem(quote.Id, parentProd1, parentProd1PBE, parentProd1OLI);
        parentProd1QLI.Quantity = 25;
        parentProd1QLI.UnitPrice = 100;
        parentProd1QLI.Requested_Price__c=100;
        qlis.add(parentProd1QLI);
        QuoteLineItem childProd1QLI = TestDataUtility.createQuoteLineItem(quote.Id, childProd1, childProd1PBE, childProd1OLI);
        childProd1QLI.Quantity = 25;
        childProd1QLI.UnitPrice = 100;
        childProd1QLI.Requested_Price__c=100;
        qlis.add(childProd1QLI);
        QuoteLineItem childProd2QLI = TestDataUtility.createQuoteLineItem(quote.Id, childProd2, childProd2PBE, childProd2OLI);
        childProd2QLI.Quantity = 25;
        childProd2QLI.UnitPrice = 100;
        childProd2QLI.Requested_Price__c=100;
        qlis.add(childProd2QLI);
        QuoteLineItem parentProd2QLI = TestDataUtility.createQuoteLineItem( quote.Id, parentProd2, parentProd2PBE, parentProd2OLI);
        parentProd2QLI.Quantity = 25;
        parentProd2QLI.UnitPrice = 100;
        parentProd2QLI.Requested_Price__c=100;
        qlis.add(parentProd2QLI);
        insert qlis;

        Integration_EndPoints__c iep = new Integration_EndPoints__c();
        iep.name ='Flow-012';
        iep.endPointURL__c = 'https://www.google.com';
        iep.layout_id__c = 'LAY024197';
        iep.partial_endpoint__c = '/SFDC_IF_012';
        insert iep;
        
        Test.stopTest();
        
        
    }

    //@isTest(seeAllData=true)
    //@isTest
    static void test_method_one() {
        // Implement test code
        Quote quoteVar = [select id, Name, ROI_Requestor_Id__c, BEP__c, Status from Quote Limit 1];
        System.debug('quoteVar' + quoteVar.Id);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HttpMockUps());
        //RoiIntegrationHelper.callRoiEndpoint('0Q0630000004nEACAY');
        //RoiIntegrationHelper.callRoiEndpoint('0Q0630000004oRY');//This quoteId doesnot have the necessary fields to go trhough the call out method.
        RoiIntegrationHelper.callRoiEndpoint(quoteVar.Id);
        Test.stopTest();

    }
    
    
    public class HttpMockUps implements HttpCalloutMock {
    public HTTPResponse respond(HTTPRequest req) {
        // Create a fake response.
        // Set response values, and 
        // return response.
        HttpResponse response = new HttpResponse();
            String res ='{"inputHeaders": {"MSGGUID": "b336d2a9-f8cb-a399-8eea-dc11782453c9","IFID": "LAY024197","IFDate": "1603161220013","MSGSTATUS": "S","ERRORTEXT": null,"ERRORCODE": null}}';
            response.setHeader('Content-Type', 'application/json');
            response.setBody(res);
            response.setStatusCode(200);
            return response;
    }
    }
    //@isTest(seeAllData = true)
    @isTest
    static void test_method_two() {
        // Implement test code
        Quote quoteVar = [select id, Name, ROI_Requestor_Id__c, BEP__c, Status from Quote Limit 1];
        System.debug('quoteVar' + quoteVar.Id);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HttpMockUps());
        //RoiIntegrationHelper.callRoiEndpoint('0Q0630000004nEACAY');
        RoiIntegrationHelper.callRoiEndpoint(quoteVar.Id);
        Test.stopTest();
    }

     
    
}