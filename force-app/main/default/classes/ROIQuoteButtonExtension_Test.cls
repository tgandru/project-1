/*
This is the test class for ROIQuoteButton Extension and ROIIntegrationHelper

Author : Aarthi R C

Created on : 4/11/2016

Modified on : 5/13/2016

*/
@isTest
public class ROIQuoteButtonExtension_Test {
  
    static Id endUserRT = TestDataUtility.retrieveRecordTypeId('End_User', 'Account'); 
    static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
    //@testSetup 
    static testMethod void testdatas()
    {
        //Test.startTest();
        //Channelinsight configuration
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
    
        // Create Account
        Account acc = TestDataUtility.createAccount(TestDataUtility.generateRandomString(5));
        //acc.recordTypeId = endUserRT;
        insert acc;
    
        // Create a partner Account
        Account paAcct = TestDataUtility.createAccount(TestDataUtility.generateRandomString(6));
        paAcct.SAP_Company_Code__c='Testing';
        //paAcct.recordTypeId =  directRT;
        insert paAcct;
    
        // Create custom Price Book
        PriceBook2 pb = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(6));
        insert pb;
    
        // create Opportunity
        Opportunity opp = TestDataUtility.createOppty('Opp created from test', acc, pb, 'Tender', 'IT', 'FAX/MFP', 'Identified');
        insert opp;
    
        // Create Partner for this Opportunity
        Partner__c partner = TestDataUtility.createPartner(opp,acc,pAacct,'Distributor');
        partner.Is_Primary__c = true;
        insert partner;
    
        // Create Products
        List<Product2> prods=new List<Product2>();
        Product2 standaloneProd=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Standalone Prod1', 'Printer[C2]', 'FAX/MFP', 'H/W');
        standaloneProd.Unit_Cost_Last_Update__c=Datetime.now().addHours(-2);
        prods.add(standaloneProd);
        Product2 parentProd1=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Parent Prod1', 'Printer[C2]', 'FAX/MFP', 'H/W');
        parentProd1.Unit_Cost_Last_Update__c=System.now().addDays(-3);
        prods.add(parentProd1);
        Product2 childProd1=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod1', 'Printer[C2]', 'PRINTER', 'Service pack');
        prods.add(childProd1);
        Product2 childProd2=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod2', 'Printer[C2]', 'PRINTER', 'Service pack');
        prods.add(childProd2);
        Product2 parentProd2=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Parent Prod2', 'Printer[C2]', 'FAX/MFP', 'H/W');
        prods.add(parentProd2);
        Product2 childProd3=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod3', 'Printer[C2]', 'PRINTER', 'Service pack');
        prods.add(childProd3);
        Product2 childProd4=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod4', 'Printer[C2]', 'PRINTER', 'Service pack');
        prods.add(childProd4);
        insert prods;
    
        // Create Product Relationship
        List<Product_Relationship__c> prs=new List<Product_Relationship__c>();
        Product_Relationship__c pr1=TestDataUtility.createProductRelationships(childProd1, parentProd1);
        prs.add(pr1);
        Product_Relationship__c pr2=TestDataUtility.createProductRelationships(childProd2, parentProd1);
        prs.add(pr2);
        Product_Relationship__c pr3=TestDataUtility.createProductRelationships(childProd3, parentProd2);
        prs.add(pr3);
        Product_Relationship__c pr4=TestDataUtility.createProductRelationships(childProd4, parentProd2);
        prs.add(pr4);
        insert prs;
    
        // Create PriceBookEntry with standard pricebook
    
        List<PriceBookEntry> pbes=new List<PriceBookEntry>();
    
        /*Id pricebookId = Test.getStandardPricebookId();
    
    
        PriceBookEntry standaloneProdPBE1=TestDataUtility.createPriceBookEntry(standaloneProd.Id, pricebookId);
        pbes.add(standaloneProdPBE1);
        PriceBookEntry parentProd1PBE1=TestDataUtility.createPriceBookEntry(parentProd1.Id,pricebookId);
        pbes.add(parentProd1PBE1);
        PriceBookEntry childProd1PBE1=TestDataUtility.createPriceBookEntry(childProd1.Id, pricebookId);
        pbes.add(childProd1PBE1);
        PriceBookEntry childProd2PBE1=TestDataUtility.createPriceBookEntry(childProd2.Id, pricebookId);
        pbes.add(childProd2PBE1);
        PriceBookEntry parentProd2PBE1=TestDataUtility.createPriceBookEntry(parentProd2.Id, pricebookId);
        pbes.add(parentProd2PBE1);*/
    
        // create PriceBookentry for custom pricebook
        PriceBookEntry standaloneProdPBE=TestDataUtility.createPriceBookEntry(standaloneProd.Id, pb.Id);
        pbes.add(standaloneProdPBE);
        PriceBookEntry parentProd1PBE=TestDataUtility.createPriceBookEntry(parentProd1.Id,pb.Id);
        pbes.add(parentProd1PBE);
        PriceBookEntry childProd1PBE=TestDataUtility.createPriceBookEntry(childProd1.Id, pb.Id);
        pbes.add(childProd1PBE);
        PriceBookEntry childProd2PBE=TestDataUtility.createPriceBookEntry(childProd2.Id, pb.Id);
        pbes.add(childProd2PBE);
        PriceBookEntry parentProd2PBE=TestDataUtility.createPriceBookEntry(parentProd2.Id, pb.Id);
        pbes.add(parentProd2PBE);
        insert pbes;
    
    
    
        // Create Opportunity Line item
        List<OpportunityLineItem> olis=new List<OpportunityLineItem>();
        OpportunityLineItem standaloneProdOLI=TestDataUtility.createOpptyLineItem(standaloneProd, standaloneProdPBE, opp);
        standaloneProdOLI.Quantity = 10;
        standaloneProdOLI.TotalPrice = standaloneProdOLI.Quantity * standaloneProdPBE.UnitPrice;
        olis.add(standaloneProdOLI);
        OpportunityLineItem parentProd1OLI=TestDataUtility.createOpptyLineItem(parentProd1, parentProd1PBE, opp);
        parentProd1OLI.Quantity = 10;
        parentProd1OLI.TotalPrice = parentProd1OLI.Quantity * parentProd1PBE.UnitPrice;
        olis.add(parentProd1OLI);
        OpportunityLineItem childProd1OLI=TestDataUtility.createOpptyLineItem(childProd1, childProd1PBE, opp);
        childProd1OLI.Quantity = 10;
        childProd1OLI.TotalPrice = childProd1OLI.Quantity * childProd1PBE.UnitPrice;
        olis.add(childProd1OLI);
        OpportunityLineItem childProd2OLI=TestDataUtility.createOpptyLineItem(childProd2, childProd2PBE, opp);
        childProd2OLI.Quantity = 10;
        childProd2OLI.TotalPrice = childProd2OLI.Quantity * childProd2PBE.UnitPrice;
        olis.add(childProd2OLI);
        OpportunityLineItem parentProd2OLI=TestDataUtility.createOpptyLineItem(parentProd2, parentProd2PBE, opp);
        parentProd2OLI.Quantity = 10;
        parentProd2OLI.TotalPrice = parentProd2OLI.Quantity * parentProd2PBE.UnitPrice;
        olis.add(parentProd2OLI);
        insert olis;
    
        // Create Quote
        List<Quote> quotes=new List<Quote>();
        Quote quote = TestDataUtility.createQuote(TestDataUtility.generateRandomString(6), opp, pb);
        quote.BEP__c='';
        quote.Status ='Draft';
        quote.ROI_Requestor_Id__c=UserInfo.getUserId();
        quote.MPS__c='Direct';
        quote.Supplies_Buying_Location__c='Central';
        quote.Last_Update_from_CastIron__c=DateTime.now().addHours(-30);
        quotes.add(quote);
        Quote quote2 = TestDataUtility.createQuote('LCLC TEST CLASS QUOTE1', opp, pb);
        quote2.BEP__c=null;
        quote2.Status ='Draft';
        quote2.ROI_Requestor_Id__c=UserInfo.getUserId();
        quote2.MPS__c='Direct';
        quote2.Supplies_Buying_Location__c='Central';
        //quote2.Last_Update_from_CastIron__c=System.now().addHours(-30);
        quotes.add(quote2);

        Quote quoteNullTest = TestDataUtility.createQuote(TestDataUtility.generateRandomString(6), opp, pb);
        quoteNullTest.BEP__c=null;
        quoteNullTest.Status ='Draft';
        quoteNullTest.ROI_Requestor_Id__c=UserInfo.getUserId();
        quoteNullTest.MPS__c=null;
        quoteNullTest.Supplies_Buying_Location__c=null;
        quotes.add(quoteNullTest);
        insert quotes;

    
        // Create QuoteLineItem
        // Have to give OLIID__c as it is a reference to look for the QLI   
        //Have to give the Requested_Price__c as Quote Line Item Managementv1 flow needs this
        List<QuoteLineItem> qlis = new List<QuoteLineItem>();
        QuoteLineItem standardQLI = TestDataUtility.createQuoteLineItem(quote.Id, standaloneProd, standaloneProdPBE, standaloneProdOLI );
        standardQLI.Quantity = 25;
        standardQLI.UnitPrice = 100;
        standardQLI.UnitPrice = 101;
        standardQLI.Requested_Price__c=100;
        qlis.add(standardQLI);
        QuoteLineItem parentProd1QLI = TestDataUtility.createQuoteLineItem(quote.Id, parentProd1, parentProd1PBE, parentProd1OLI);
        parentProd1QLI.Quantity = 25;
        parentProd1QLI.UnitPrice = 100;
        parentProd1QLI.Requested_Price__c=100;
        qlis.add(parentProd1QLI);
        QuoteLineItem childProd1QLI = TestDataUtility.createQuoteLineItem(quote.Id, childProd1, childProd1PBE, childProd1OLI);
        childProd1QLI.Quantity = 25;
        childProd1QLI.UnitPrice = 100;
        childProd1QLI.Requested_Price__c=100;
        qlis.add(childProd1QLI);
        QuoteLineItem childProd2QLI = TestDataUtility.createQuoteLineItem(quote.Id, childProd1, childProd1PBE, childProd2OLI);
        childProd2QLI.Quantity = 25;
        childProd2QLI.UnitPrice = 100;
        childProd2QLI.Requested_Price__c=99;
        qlis.add(childProd2QLI);
        QuoteLineItem parentProd2QLI = TestDataUtility.createQuoteLineItem( quote.Id, parentProd2, parentProd2PBE, parentProd2OLI);
        parentProd2QLI.Quantity = 25;
        parentProd2QLI.UnitPrice = 100;
        parentProd2QLI.Requested_Price__c=100;
        qlis.add(parentProd2QLI);

        QuoteLineItem parentProd1QLI2 = TestDataUtility.createQuoteLineItem(quote2.Id, parentProd1, parentProd1PBE, parentProd1OLI);
        parentProd1QLI2.Quantity = 25;
        parentProd1QLI2.UnitPrice = 100;
        parentProd1QLI2.Requested_Price__c=100;
        qlis.add(parentProd1QLI2);
        QuoteLineItem childProd1QLI2 = TestDataUtility.createQuoteLineItem(quote2.Id, childProd1, childProd1PBE, childProd1OLI);
        childProd1QLI2.Quantity = 25;
        childProd1QLI2.UnitPrice = 100;
        childProd1QLI2.Requested_Price__c=100;
        qlis.add(childProd1QLI2);
        QuoteLineItem childProd2QLI2 = TestDataUtility.createQuoteLineItem(quote2.Id, childProd2, childProd2PBE, childProd2OLI);
        childProd2QLI2.Quantity = 25;
        childProd2QLI2.UnitPrice = 100;
        childProd2QLI2.Requested_Price__c=100;
        qlis.add(childProd2QLI2);
        QuoteLineItem parentProd2QLI2 = TestDataUtility.createQuoteLineItem( quote2.Id, parentProd2, parentProd2PBE, parentProd2OLI);
        parentProd2QLI2.Quantity = 25;
        parentProd2QLI2.UnitPrice = 100;
        parentProd2QLI2.Requested_Price__c=100;
        qlis.add(parentProd2QLI2);
        insert qlis;
        
        List<Integration_EndPoints__c> ieps = new List<Integration_EndPoints__c>();
        Integration_EndPoints__c iep = new Integration_EndPoints__c();
        iep.name ='Flow-012';
        iep.endPointURL__c = 'https://www.google.com';
        iep.layout_id__c = 'LAY024197';
        iep.partial_endpoint__c = '/SFDC_IF_012';
        ieps.add(iep);
        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='Flow-008';
        iep2.endPointURL__c = 'https://www.google.com';
        iep2.layout_id__c = 'LAY024197';
        iep2.partial_endpoint__c = '/SFDC_IF_012';
        ieps.add(iep2);
        insert ieps;
        //Test.stopTest();
        
        //Quote quoteVar = [select id, Name, ROI_Requestor_Id__c, BEP__c, Status from Quote Limit 1];
        Test.startTest();
        ROIQuoteButtonExtension quoteExtController = new ROIQuoteButtonExtension(new ApexPages.StandardController(quote));
        Test.setMock(HttpCalloutMock.Class, new ROIIntegrationSuccessTest());
        quoteExtController.passQuoteId();

        parentProd1.Unit_Cost_Last_Update__c=System.now().addDays(-3);
        update parentProd1;

        ROIQuoteButtonExtension quoteExtController2 = new ROIQuoteButtonExtension(new ApexPages.StandardController(quote2));
        Test.setMock(HttpCalloutMock.Class, new ROIIntegrationSuccessTest());
        quoteExtController2.passQuoteId();

        ROIQuoteButtonExtension quoteExtControllerNull = new ROIQuoteButtonExtension(new ApexPages.StandardController(quoteNullTest));
        Test.setMock(HttpCalloutMock.Class, new ROIIntegrationSuccessTest());
        quoteExtControllerNull.passQuoteId();
        Test.stopTest();
        quoteExtController.postToChatterPositive(quote);
        quoteExtController.postToChatterNegative(quote);
    }

    // Test Methods
    //Scenario: where the quote nad QLI doesn't have all the variables it requires so it should throw a message.
   /* @isTest static void test_method_one()
    {    
        testdatas();
        Quote quoteVar = [select id, Name, ROI_Requestor_Id__c, BEP__c, Status from Quote Limit 1];
        
        
        Test.startTest();
        
        
        ROIQuoteButtonExtension quoteExtController = new ROIQuoteButtonExtension(new ApexPages.StandardController(quoteVar));
        Test.setMock(HttpCalloutMock.Class, new ROIIntegrationSuccessTest());
        ROIQuoteButtonExtension.passQuoteId();
        
        Test.stopTest();
        
        quoteExtController.postToChatterPositive(quoteVar);
        quoteExtController.postToChatterNegative(quoteVar);
    }
    */
    private class ROIIntegrationSuccessTest implements HttpCalloutMock {

        public HTTPResponse respond(HTTPRequest req) {
            System.debug('**** inside my mock');
            HttpResponse res = new HttpResponse();
            
            //String response = '{"MSGGUID": "b336d2a9-f8cb-a399-8eea-dc11782453c9","IFID": "LAY024197","IFDate": "1603161220013","MSGSTATUS": "S","ERRORTEXT": null,"ERRORCODE": null}';
            String response ='{"inputHeaders": {"MSGGUID": "b336d2a9-f8cb-a399-8eea-dc11782453c9","IFID": "LAY024197","IFDate": "1603161220013","MSGSTATUS": "S","ERRORTEXT": null,"ERRORCODE": null}}';
            res.setHeader('Content-Type', 'application/json');
            res.setBody(response);
            res.setStatusCode(200);
            return res;
        
        }
    }
}
    /*// Test fail on Rest call
    @isTest static void test_method_two()
    {
    Test.startTest();
    Quote quoteVar = [select id, Name, ROI_Requestor_Id__c, BEP__c, Status from Quote Limit 1];
    system.assert(quoteVar!=null);
    Test.setMock(HttpCalloutMock.class, new ROIIntegrationCallOutMock.ROIIntegrationFailureTestCase());
    ROIQuoteButtonExtension quoteExtController = new ROIQuoteButtonExtension(new ApexPages.StandardController(quoteVar));           
    ROIQuoteButtonExtension.passQuoteId();
    RoiIntegrationHelper.callRoiEndpoint(quoteVar.Id);
    Test.stopTest();
    }

    // Test error 400 on Rest call response
    @isTest static void test_method_three()
    {
    Test.startTest();
    Quote quoteVar = [select id, Name, ROI_Requestor_Id__c, BEP__c, Status from Quote Limit 1];
    system.assert(quoteVar!=null);
    Test.setMock(HttpCalloutMock.class, new ROIIntegrationCallOutMock.ROIIntegrationSystemError());
    ROIQuoteButtonExtension quoteExtController = new ROIQuoteButtonExtension(new ApexPages.StandardController(quoteVar));           
    ROIQuoteButtonExtension.passQuoteId();
    RoiIntegrationHelper.callRoiEndpoint(quoteVar.Id);
    Test.stopTest();
    }

    // Scenario where it doesnot meet the criteria for the rest call
    @isTest static void test_method_four()
    {
    Test.startTest();
    Quote quoteVar = [select id, Name, ROI_Requestor_Id__c, BEP__c, Status from Quote Limit 1];
    system.assert(quoteVar!=null);
    quoteVar.BEP__c = String.ValueOf(13.3);
    update quoteVar;
    ROIQuoteButtonExtension quoteExtController = new ROIQuoteButtonExtension(new ApexPages.StandardController(quoteVar));           
    ROIQuoteButtonExtension.passQuoteId();
    Test.stopTest();
    }
*/

    /*//Scenario : Passing the Quote record with BEP__C value as null
    @isTest static void calling_future_method_returnErrorCode() {
    // Implement test code

    //setting up the mock data
    Map<String,String> responseHeaders = new Map<String,String>();
    responseHeaders.put('Content-Type', 'application/json');
    responseHeaders.put('Accept', 'application/json');

    //Generating Response
    cihStub.roiRes response = new cihStub.roiRes();
    cihStub.roiResponse resbody = new cihStub.roiResponse();
    cihstub.msgHeadersResponse header = new cihstub.msgHeadersResponse();

    header.ERRORCODE='E12305';
    header.ERRORTEXT='Didnot get the variables right';
    header.IFID='asdf';
    header.IFDate=system.now().format('yyMMddHHmmss');
    header.MSGGUID=TestDataUtility.generateRandomString(8);
    header.MSGSTATUS='Fail';
    response.input = header;

    resbody.inputHeaders=header;
    response.body = resbody;
    system.debug('********************************************* response in cihstub format' + response);
    String responseBody = json.serialize(response);
    system.debug('*******************************  my response '+ responseBody);


    //setting up the response to the mock template
    ROIIntegrationCallOutMock mockCallOut = new ROIIntegrationCallOutMock(200, 'success', responseBody,responseHeaders);

        
    Test.startTest();
    Test.setMock(HttpCalloutMock.class, mockCallOut);
    system.debug('************************** mock up should have been used by this time');
    Quote quoteVar = [select id, Name, ROI_Requestor_Id__c, BEP__c, Status from Quote Limit 1];
    system.assert(quoteVar!=null);
    ROIQuoteButtonExtension quoteExtController = new ROIQuoteButtonExtension(new ApexPages.StandardController(quoteVar));           
    //calling future method 
    ROIQuoteButtonExtension.passQuoteId();
    Test.stopTest();
    }

    //Scenario : Passing the Quote object record with BEP__C value
    @isTest static void calling_future_method_two() {
    // Implement test code
    Quote quoteVar = [select id, Name, ROI_Requestor_Id__c, BEP__c, Status from Quote Limit 1];
    system.assert(quoteVar!=null);
    quoteVar.BEP__c =13.3;
    update quoteVar;

    //setting up the mock data
    Map<String,String> responseHeaders = new Map<String,String>();
    responseHeaders.put('Content-Type', 'application/json');
    responseHeaders.put('Accept', 'application/json');

    //Generating Response
    cihStub.roiRes response = new cihStub.roiRes();
    cihStub.roiResponse resbody = new cihStub.roiResponse();
    cihstub.msgHeadersResponse header = new cihstub.msgHeadersResponse();

    header.ERRORCODE='E13452y';
    header.ERRORTEXT='Didnot get the variables right';
    header.IFID='asdf';
    header.IFDate=system.now().format('yyMMddHHmmss');
    header.MSGGUID=TestDataUtility.generateRandomString(8);
    header.MSGSTATUS='Fail';
    response.input = header;

    resbody.inputHeaders=header;
    response.body = resbody;
    system.debug('********************************************* response in cihstub format' + response);
    String responseBody = json.serialize(response);
    system.debug('*******************************  my response '+ responseBody);
    ROIIntegrationCallOutMock mockCallOut = new ROIIntegrationCallOutMock(200, 'success', responseBody,responseHeaders);

    //calling future method 
    Test.startTest();
    Test.setMock(HttpCalloutMock.class, mockCallOut);
    ROIQuoteButtonExtension quoteExtController = new ROIQuoteButtonExtension(new ApexPages.StandardController(quoteVar));
    ROIQuoteButtonExtension.passQuoteId();
    Test.stopTest();
    }

    //Scenario: calling the future method with success as a response
    @isTest static void calling_future_method_three() {
    // Implement test code
    Quote quoteVar = [select id, Name, ROI_Requestor_Id__c, BEP__c, Status from Quote Limit 1];
    system.assert(quoteVar!=null);

    //setting up the mock data
    Map<String,String> responseHeaders = new Map<String,String>();
    responseHeaders.put('Content-Type', 'application/json');
    responseHeaders.put('Accept', 'application/json');

    //Generating Response
    cihStub.roiRes response = new cihStub.roiRes();
    cihStub.roiResponse resbody = new cihStub.roiResponse();
    cihstub.msgHeadersResponse header = new cihstub.msgHeadersResponse();

    header.ERRORCODE='';
    header.ERRORTEXT='';
    header.IFID='asdf';
    header.IFDate=system.now().format('yyMMddHHmmss');
    header.MSGGUID=TestDataUtility.generateRandomString(8);
    header.MSGSTATUS='S';
    response.input = header;

    resbody.inputHeaders=header;
    response.body = resbody;
    system.debug('********************************************* response in cihstub format' + response);
    String responseBody = json.serialize(response);
    system.debug('*******************************  my response '+ responseBody);
    ROIIntegrationCallOutMock mockCallOut = new ROIIntegrationCallOutMock(200, 'success', responseBody,responseHeaders);

    //calling future method 
    Test.startTest();
    Test.setMock(HttpCalloutMock.class, mockCallOut);
    ROIQuoteButtonExtension quoteExtController = new ROIQuoteButtonExtension(new ApexPages.StandardController(quoteVar));
    ROIQuoteButtonExtension.passQuoteId();
    Test.stopTest();
    }*/