public class RecordTypeUtil {

    public static Id recordTypeId(String objectName, String recordName){
        Id recordTypeId;
        
        if(objectName != null && recordName != null){
            recordTypeId= Schema.getGlobalDescribe().get(objectName).getDescribe().getRecordTypeInfosByName().get(recordName).getRecordTypeId();
        }  
        
        return recordTypeId;  
     }
    
    public static Map<String,Id> recordTypeMap(String objectName){
        Map<String,Id> recordTypeNameWithIdMap = new Map<String,Id>();
        if(objectName != null){
            for(Schema.RecordTypeInfo recordTypeInfo : Schema.getGlobalDescribe().get(objectName).getDescribe().getRecordTypeInfosByName().values()){
                recordTypeNameWithIdMap.put(recordTypeInfo.getName(), recordTypeInfo.getRecordTypeId());
            }
        }
        return recordTypeNameWithIdMap;
    }     
}