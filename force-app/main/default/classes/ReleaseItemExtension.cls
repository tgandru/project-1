public class ReleaseItemExtension {
   	
   	private ReleaseItem__c item{get;set;}
   	 
	public ReleaseItemExtension(ApexPages.StandardController controller) {
		this.item = (ReleaseItem__c)controller.getRecord();
        String releaseNoteId = ApexPages.currentPage().getParameters().get('ReleaseNoteId');
        if(this.item.Id==null) {
            this.item.ReleaseNote__c = releaseNoteId;
        }
	}
	
	public PageReference save() {
		try {
			insert item;
			String retUrl = ApexPages.currentPage().getParameters().get('retURL');
			PageReference reRend = new PageReference(retUrl);
			return reRend;
		} catch(DMLException e) {
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,'Error - '+e.getMessage());
			ApexPages.addMessage(myMsg);
			return null;
		}
	}
    
    public PageReference updateItem() {
		try {
			update item;
			String retUrl = ApexPages.currentPage().getParameters().get('retURL');
			PageReference reRend = new PageReference(retUrl);
			return reRend;
		} catch(DMLException e) {
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,'Error - '+e.getMessage());
			ApexPages.addMessage(myMsg);
			return null;
		}
	}
}