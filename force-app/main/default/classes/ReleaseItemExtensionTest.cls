@isTest
public class ReleaseItemExtensionTest {
    
    static testMethod void save() {
    	PageReference pr = new PageReference('/apex/ReleaseNoteDetail');
   		Test.setCurrentPage(pr);
   		ApexPages.currentPage().getParameters().put('ReleaseNoteId','a10110000020l6u');
   		ApexPages.currentPage().getParameters().put('retUrl','/apex/ReleaseNoteDetail');
   		
    	ReleaseItem__c item = new ReleaseItem__c();
   		item.Name='test item';
   		item.ComponentType__c = 'Apex Class';
   		item.ReleaseType__c = 'New';
   		item.Description__c = 'test item description';
   		
    	ReleaseItemExtension riext = new ReleaseItemExtension(new ApexPages.StandardController(item));
    	PageReference ref = riext.save();
    	//System.assertNotEquals(ref, null);
    }
    
    static testMethod void saveDMLException() {
    	PageReference pr = new PageReference('/apex/ReleaseNoteDetail');
   		Test.setCurrentPage(pr);
   		ApexPages.currentPage().getParameters().put('ReleaseNoteId','a10110000020l6u');
   		ApexPages.currentPage().getParameters().put('retUrl','/apex/ReleaseNoteDetail');
   		
    	ReleaseItem__c item = new ReleaseItem__c();
   		item.Name='test item';
   		item.ComponentType__c = 'Apex Class';
   		item.ReleaseType__c = 'New';
   		item.Description__c = 'test item description';
   		
    	ReleaseItemExtension riext = new ReleaseItemExtension(new ApexPages.StandardController(item));
    	riext.save();
    	
    	PageReference ref = new ReleaseItemExtension(new ApexPages.StandardController(item)).save();
    	System.assertEquals(ref, null);
    }
    
    static testMethod void updateItem() {
    	PageReference pr = new PageReference('/apex/ReleaseNoteDetail');
   		Test.setCurrentPage(pr);
   		ApexPages.currentPage().getParameters().put('retUrl','/apex/ReleaseNoteDetail');
   		
   		ReleaseNote__c rnote = new ReleaseNote__c();
    	rnote.Name='ReleaseNote1';
    	rnote.Description__c = 'Test Release Note';
    	rnote.HowToTest__c = 'It is for testing method';
    	rnote.PostAction__c = 'It is for post actions';
    	rnote.Status__c = 'Draft';
    	rnote.Type__c = 'Bug Fix';
    	rnote.Module__c='Sales';
   		insert rnote;
   		
   		ReleaseItem__c item = new ReleaseItem__c();
   		item.ReleaseNote__c = rnote.Id;
   		item.Name='test item';
   		item.ComponentType__c = 'Apex Class';
   		item.ReleaseType__c = 'New';
   		item.Description__c = 'test item description';
   		
   		insert item;
   		
    	ReleaseItemExtension riext = new ReleaseItemExtension(new ApexPages.StandardController(item));
    	PageReference ref = riext.updateItem();
    	System.assertNotEquals(ref, null);
    }
    
    static testMethod void updateItemDMLException() {
    	PageReference pr = new PageReference('/apex/ReleaseNoteDetail');
   		Test.setCurrentPage(pr);
   		ApexPages.currentPage().getParameters().put('retUrl','/apex/ReleaseNoteDetail');
   		
   		ReleaseItem__c item = new ReleaseItem__c();
   		item.ComponentType__c = 'Apex Class';
   		item.ReleaseType__c = 'New';
   		item.Description__c = 'test item description';
   		
    	ReleaseItemExtension riext = new ReleaseItemExtension(new ApexPages.StandardController(item));
    	PageReference ref = riext.updateItem();
    	System.assertEquals(ref, null);
    }
}