@isTest
public class ReleaseItemTriggerTest {
    
    static testMethod void testReleaseItem() {
    	ReleaseNote__c rnote = new ReleaseNote__c();
    	rnote.Name='ReleaseNote1';
    	rnote.Description__c = 'Test Release Note';
    	rnote.HowToTest__c = 'It is for testing method';
    	rnote.PostAction__c = 'It is for post actions';
    	rnote.Status__c = 'Draft';
    	rnote.Type__c = 'Bug Fix';
    	rnote.Module__c='Sales';
   		insert rnote;
   		
   		ReleaseItem__c item = new ReleaseItem__c();
   		item.ReleaseNote__c = rnote.Id;
   		item.Name='test item';
   		item.ComponentType__c = 'Apex Class';
   		item.ReleaseType__c = 'New';
   		item.Description__c = 'test item description';
   		
   		insert item;
   		
   		delete item;
    }
}