public class ReleaseNoteExtension {
        
    private ReleaseNote__c note {get;set;}
    private List<ReleaseItem__c> items {get;set;}
    public String ItemId {get;set;}
    
    public ReleaseNoteExtension(ApexPages.StandardController controller) {
        this.note = (ReleaseNote__c)controller.getRecord();
        this.items = [select Id, Name, ComponentType__c , Description__c, ReleaseType__c from ReleaseItem__c where ReleaseNote__c=:this.note.Id];
    }
    
    public List<ReleaseItem__c> getItems() {
        return this.items;
    }
    
    public PageReference submitForApproval() {
        if(this.note.itemCount__c==0) {
            this.note.addError('Please, add release items.');
            return null;
        } 
        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        System.debug(this.note.Id + ' - ' + UserInfo.getUserId());
		req.setComments('ReleaseNote Approval Process - '+this.note.Name);
        req.setObjectId(this.note.Id);
        req.setSubmitterId(UserInfo.getUserId());
        
        // Submit the approval request for the account
        Approval.ProcessResult result = Approval.process(req);
		System.assert(result.isSuccess());
        System.assertEquals('Pending', result.getInstanceStatus(),'Instance Status'+result.getInstanceStatus());
        
        PageReference pageRef = new PageReference(ApexPages.currentPage().getUrl());
        pageRef.setRedirect(true);

        return pageRef;
    }
    
    public PageReference addReleaseItem() {
        PageReference page = ApexPages.currentPage();
        String retUrl = page.getURL();
        System.debug('@RetUrl -> '+retUrl);
        
        PageReference rerend = new PageReference('/apex/ReleaseItemNew');
        rerend.getParameters().put('retURL', retUrl);
        rerend.getParameters().put('ReleaseNoteId',this.note.Id);
        return rerend;
    }
    
    public PageReference deploy() {
        if(this.note.Status__c!='Approved') {
            this.note.addError('Only approved release note should be deployed.');
            return null;
        }
        
        PageReference page = ApexPages.currentPage();
        String retUrl = page.getURL();
        System.debug('@RetUrl -> '+retUrl);
        
        PageReference rerend = new PageReference('/apex/ReleaseNoteDeploy');
        rerend.getParameters().put('retURL', retUrl);
        rerend.getParameters().put('ReleaseNoteId',this.note.Id);
        return rerend;
    }
    
    public PageReference deployed() {
        if(this.note.Status__c!='Approved') {
            this.note.addError('Only approved release note should be deployed.');
            return null;
        }
        
        this.note.Status__c='Deployed';
        update this.note;
        
        String retUrl = ApexPages.currentPage().getParameters().get('retURL');
		PageReference reRend = new PageReference(retUrl);
		return reRend;
    }
    
    public void completed() {
        if(this.note.Status__c!='Deployed') {
            this.note.addError('Only deployed release note should be completed.');
            return;
        }
        
        this.note.Status__c='Completed';
        update this.note;
    }
    
    public PageReference editItem() {
        PageReference page = ApexPages.currentPage();
        String retUrl = page.getURL();
        System.debug('@RetUrl -> '+retUrl);
        
        PageReference rerend = new PageReference('/apex/ReleaseItemEdit');
        rerend.getParameters().put('retURL', retUrl);
        rerend.getParameters().put('Id',this.ItemId);
        rerend.getParameters().put('ReleaseNoteId',this.note.Id);
        
        return rerend;
    }
    
    public PageReference deleteItem() {
        System.debug('########### '+ItemId);
        ReleaseItem__c item = [SELECT ID FROM ReleaseItem__c WHERE Id=:ItemId];
        delete item;
        
        PageReference pageRef = new PageReference(ApexPages.currentPage().getUrl());
        pageRef.setRedirect(true);

        return pageRef;
    }
}