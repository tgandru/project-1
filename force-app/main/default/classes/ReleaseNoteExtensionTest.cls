@isTest
public class ReleaseNoteExtensionTest {
    
    public static ReleaseNote__c createReleaseNote(String status, boolean hasItem) {
    	ReleaseNote__c rnote = new ReleaseNote__c();
    	rnote.Name='ReleaseNote1';
    	rnote.Description__c = 'Test Release Note';
    	rnote.HowToTest__c = 'It is for testing method';
    	rnote.PostAction__c = 'It is for post actions';
    	rnote.Status__c = status==null? 'Draft': status;
    	rnote.Type__c = 'Bug Fix';
    	rnote.Module__c='Sales';
   		insert rnote;
   		
        if(!hasItem) {
            return rnote;
        }
   		ReleaseItem__c item = new ReleaseItem__c();
   		item.ReleaseNote__c = rnote.Id;
   		item.Name='test item';
   		item.ComponentType__c = 'Apex Class';
   		item.ReleaseType__c = 'New';
   		item.Description__c = 'test item description';
   		
   		insert item;
   		
   		return rnote;
    }
    
    static testMethod void save() {
    	ReleaseNote__c rnote = ReleaseNoteExtensionTest.createReleaseNote('Draft', true);
    }
    
    
    static testMethod void getItems() {
    	ReleaseNote__c rnote = new ReleaseNote__c(Name='ReleaseNote1');
    	ReleaseNoteExtension rnext = new ReleaseNoteExtension(new ApexPages.StandardController(rnote));
    	List<ReleaseItem__c> items = rnext.getItems();
    	for(ReleaseItem__c item : items) {
    		System.debug(item.Name);
    	}
    }
   	
   	static testMethod void addReleaseItem() {
   		ReleaseNote__c rnote = createReleaseNote('Draft', true);
    	ReleaseNoteExtension rnext = new ReleaseNoteExtension(new ApexPages.StandardController(rnote));
    	PageReference pageRef = rnext.addReleaseItem();
    	System.debug('addReleaseItem ==> '+pageRef);
   	} 
   	
    
    static testMethod void submitForApproval() {
    	PageReference pr = new PageReference('/apex/ReleaseNoteDetail');
   		Test.setCurrentPage(pr);
   		
   		ReleaseNote__c rnote = createReleaseNote('Draft', true);
    	ReleaseNoteExtension rnext = new ReleaseNoteExtension(new ApexPages.StandardController(rnote));
    	PageReference pageRef = rnext.submitForApproval();
    	System.debug('submitForApproval ==> '+pageRef);
    }
    
    
   	static testMethod void deployNotApproved() {
   		ReleaseNote__c rnote = createReleaseNote('Draft', true);
    	ReleaseNoteExtension rnext = new ReleaseNoteExtension(new ApexPages.StandardController(rnote));
    	PageReference pageRef = rnext.deploy();
    	System.debug('deploy ==> '+pageRef);
   	} 
   	
   	static testMethod void deployApproved() {
   		PageReference pr = new PageReference('/apex/ReleaseNoteDetail');
   		Test.setCurrentPage(pr);
        pr.getParameters().put('retURL','/apex/ReleaseNoteDetail');
        
        ReleaseNote__c rnote = createReleaseNote('Approved', true);
    	ReleaseNoteExtension rnext = new ReleaseNoteExtension(new ApexPages.StandardController(rnote));
    	PageReference pageRef = rnext.deploy();
    	System.debug('deploy ==> '+pageRef);
   	} 
   	
   	static testMethod void deployedNotApproved() {
   		ReleaseNote__c rnote = createReleaseNote('Draft', true);
    	ReleaseNoteExtension rnext = new ReleaseNoteExtension(new ApexPages.StandardController(rnote));
    	PageReference pageRef = rnext.deployed();
    	System.debug('deployed ==> '+pageRef);
   	} 
   	
   	static testMethod void deployedApproved() {
   		PageReference pr = new PageReference('/apex/ReleaseNoteDetail');
   		Test.setCurrentPage(pr);
        pr.getParameters().put('retURL','/apex/ReleaseNoteDetail');
        
        ReleaseNote__c rnote = createReleaseNote('Approved', true);
    	ReleaseNoteExtension rnext = new ReleaseNoteExtension(new ApexPages.StandardController(rnote));
    	PageReference pageRef = rnext.deployed();
    	System.debug('deployed ==> '+pageRef);
   	} 
   	
   	static testMethod void completedNotDeployed() {
   		ReleaseNote__c rnote = createReleaseNote('Draft',true);
    	ReleaseNoteExtension rnext = new ReleaseNoteExtension(new ApexPages.StandardController(rnote));
    	rnext.completed();
   	}
   	
   	static testMethod void completedDeployed() {
   		ReleaseNote__c rnote = createReleaseNote('Deployed', true);
   		
    	ReleaseNoteExtension rnext = new ReleaseNoteExtension(new ApexPages.StandardController(rnote));
    	rnext.completed();
   	}
   	
   	static testMethod void editItem() {
   		ReleaseNote__c rnote = createReleaseNote('Draft', true);
    	ReleaseNoteExtension rnext = new ReleaseNoteExtension(new ApexPages.StandardController(rnote));
    	PageReference pageRef = rnext.editItem();
    	System.debug('editItem ==> '+pageRef);
   	}
   	
   	static testMethod void deleteItem() {
   		PageReference pr = new PageReference('/apex/ReleaseNoteDetail');
   		Test.setCurrentPage(pr);
   		
   		ReleaseNote__c rnote = createReleaseNote('Draft', true);
   		ReleaseNoteExtension rnext = new ReleaseNoteExtension(new ApexPages.StandardController(rnote));
   		List<ReleaseItem__c> items = rnext.getItems();
   		if(items.size()>0) {
	    	rnext.ItemId=items.get(0).Id;
	    	PageReference pageRef = rnext.deleteItem();
	    	System.debug('deleteItem ==> '+pageRef);
   		}
   	}
}