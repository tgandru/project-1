public class ReleaseNoteTemplate {
    public ReleaseNote__c currentRN;
    public Id rnId {get;set;}
    public List<ReleaseItem__c> releaseItems;
    public Document samsungLogo;
    public String orgId;
    public String domainId;
    public ReleaseNoteTemplate(){ }
   public Document getSamsungLogo() {
        if(samsungLogo == null) {
            if(!Test.isRunningTest()) {
                try{
                    samsungLogo = [Select Id, Url from Document where DeveloperName = 'SamsungPDFLogo' Limit 1];   
                } catch(Exception e) { }
            } else
                samsungLogo = new Document();   
        }       
        return samsungLogo;
    }
    
    public String getOrgId() {
        if(orgId == null) {
            orgId = UserInfo.getOrganizationId();
        }
        return orgId;
    }
    
    public String getDomainId() {
        if(domainId == null) {
            domainId = System.URL.getSalesforceBaseURL().getHost();
        }
        return domainId;
    }
   public ReleaseNote__c getcurrentRN() {
        if(currentRN == null) {
            try {
                currentRN = [SELECT ChangeSet__c,CreatedById,CreatedDate,DeployComment__c,DeployResult__c,Description__c,
                               HowToTest__c,Id,IsDeleted,ItemCount__c,LastActivityDate,LastModifiedById,LastModifiedDate,
                               LastReferencedDate,LastViewedDate,Name,OwnerId,PostAction__c,RelatedCase__c,Status__c,SystemModstamp,
                               TargetDate__c,Type__c,UrgentRelease__c FROM ReleaseNote__c where Id = :rnId];   
            } catch(Exception e) { }
        }
        return currentRN;
    }
    
    public List<ReleaseItem__c> getreleaseItems() {
        if(currentRN == null) {
            getcurrentRN();
        }
        if(currentRN == null) {
            return null;
        }
        
            if(releaseItems == null) {
                releaseItems = [SELECT ComponentType__c,CreatedById,CreatedDate,Description__c,Id,IsDeleted,LastActivityDate,LastModifiedById,LastModifiedDate,Name,OwnerId,ReleaseNote__c,
                                  ReleaseType__c,SystemModstamp FROM ReleaseItem__c WHERE ReleaseNote__c =  :currentRN.Id];
                
               
            }
       
        return releaseItems;
    }
}