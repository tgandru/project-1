@isTest
private class ReleaseNoteTemplateTest {

	private static testMethod void test1() {
	    Test.startTest();
          ReleaseNote__c rn =new ReleaseNote__c(Name='Test Release123',	Description__c='Testing the Template ',HowToTest__c='Make Status Complete ',PostAction__c='Check email',
                                                Status__c='Draft',TargetDate__c=System.Today(),Type__c='Enhancement',Module__c='Common' );
          insert rn;
          list<ReleaseItem__c> ri=new list<ReleaseItem__c>();
         ReleaseItem__c ri1= new ReleaseItem__c(name='Testclass',ComponentType__c='Apex Class',Description__c='Test Class for VF Component',ReleaseNote__c=rn.id,ReleaseType__c='New');
         ri.add(ri1);
         insert ri;
         Approval.ProcessSubmitRequest req1 = 
            new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(rn.id);
        req1.setSubmitterId(rn.ownerId); 
        Approval.ProcessResult result = Approval.process(req1);
        
        ReleaseNoteTemplate rnt=new ReleaseNoteTemplate();
        rnt.getSamsungLogo();
        rnt.getOrgId();
        rnt.getDomainId();
        //t.currentRN=[select id from ReleaseNote__c limit 1];
        rnt.rnId=[select id from ReleaseNote__c limit 1].id;
        rnt.getcurrentRN();
        rnt.getreleaseItems();
    Test.stopTest();
	}

}