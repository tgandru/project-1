public class ResellerAfterTriggerHandler{
    public static void updateQuote(List<Reseller__c> triggerNew){
        Set<string> quotids = new Set<string>();
        for(reseller__c r :  triggerNew){
            quotids.add(r.Quote__c);
        }
        
        List<quote> QuoteList = new List<Quote>();
        for(quote q : [select id,division__c,Reseller_Name__c,(select Partner_Reseller__r.partner__c,BillingAddress__c from Resellers__r) from Quote where id=:quotids]){
            //In below Condition q.division__c=='SBS' is added by Thiru on 05/19/2018 for Updating the selected reseller to SBS Quote.
            if((q.division__c=='W/E' || q.division__c=='SBS') && q.resellers__r.size()>0){
                q.reseller_Name__c=q.resellers__r[0].Partner_Reseller__r.partner__c;
                QuoteList.add(q);
            }
        }
        if(QuoteList.size()>0){
            update quoteList;
        }
    }
}