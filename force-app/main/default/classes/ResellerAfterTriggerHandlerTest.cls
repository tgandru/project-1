@isTest
private class ResellerAfterTriggerHandlerTest {

    @isTest static void test_method_one() {
        //Channelinsight configuration
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        
        Account acc = TestDataUtility.createAccount('Test Reseller');
        insert acc;

        PriceBook2 pb = TestDataUtility.createPriceBook('Test pbe1');       
        insert pb;
            
        Opportunity opp = TestDataUtility.createOppty('New', acc, pb, 'Managed', 'IT', 'product group','Identified');
        insert opp; 
        
        Quote q = new Quote(name='Test Quote', OpportunityId = opp.Id, Pricebook2Id = pb.Id, division__c='W/E');
        insert q;
        
        Partner__c OppPartner= new Partner__c(Opportunity__c=opp.id,Partner__c=Acc.id,Is_Primary__c=True,Role__c='Reseller');
        insert OppPartner;
        
        List<Reseller__c> rList = new List<Reseller__c>(); 
        Reseller__c r = new Reseller__c(Partner_Reseller__c=OppPartner.id,Quote__c=q.id);
        rlist.add(r);
        insert rList ;
        
        //ResellerAfterTriggerHandler cont = new ResellerAfterTriggerHandler();
        ResellerAfterTriggerHandler.updateQuote(rlist);
    }
    
}