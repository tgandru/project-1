public class RoiIntegrationHelper {

    public static List<String> missingFields=new List<String>();

    // call this from future method
    @future(callout=true)
    public static void callRoiEndpoint(string quoteId){ 
        message_queue__c mq = new message_queue__c(Integration_Flow_Type__c = '012', retry_counter__c = 0, Identification_Text__c = quoteId, Object_Name__c='Quote');
        callRoiEndpoint(mq);

    }

    public static void callRoiEndpointNoFuture(string quoteId){ 
        System.debug('lclc in callRoiEndpoint '+quoteId);
        message_queue__c mq = new message_queue__c(Integration_Flow_Type__c = '012', retry_counter__c = 0, Identification_Text__c = quoteId, Object_Name__c='Quote', Status__c='not started');
        //callRoiEndpoint(mq);
        insert mq;

    }

    // call this from batch Job, failed-retry stuff is also retried here
    public static void callRoiEndpoint(message_queue__c mq){
        
        //COMMENTED by Jagan as the end point need not be changed for test method
        //string partialEndpoint = Test.isRunningTest() ? 'asdf' : Integration_EndPoints__c.getInstance('Flow-012').partial_endpoint__c;
        //string layoutId = Test.isRunningTest() ? 'asdf' : Integration_EndPoints__c.getInstance('Flow-012').layout_id__c;
        
        string partialEndpoint = Integration_EndPoints__c.getInstance('Flow-012').partial_endpoint__c;
        string layoutId = Integration_EndPoints__c.getInstance('Flow-012').layout_id__c;
    
        Quote q = [select id, opportunity.Opportunity_Number__c, Integration_Status__c, opportunity.External_Opportunity_ID__c,
                    External_SPId__c, Opportunity.Roll_Out_Start__c, Opportunity.Roll_Out_End_Formula__c, Supplies_Buying_Location__c, MPS__c,
                    (select id, Product2.SAP_Material_ID__c, product2.Product_Type__c, quantity, Product2.Name,
                    ListPrice, Requested_Price__c, Reset_Approval__c, Bundle_Flag__c, AMPV_Color__c,PricebookEntryId,PricebookEntry.Pricebook2Id,
                    AMPV_Mono__c, Product_Type__c, Parent_Product__c
                    from QuoteLineItems) 
                    from Quote where id = :mq.Identification_Text__c];
    
        cihStub.roiAmpvReq reqStub = new cihStub.roiAmpvReq();
        cihStub.msgHeadersRequest headers = new cihStub.msgHeadersRequest(layoutId);
        cihStub.roiAmpvRequest body = new cihStub.roiAmpvRequest();
        boolean hasErrors=RoiIntegrationHelper.mapfields(body, q);
        
        if(hasErrors){
            String err='';
            if(!missingFields.isEmpty()){
                err=String.join(missingFields,', ');
            }
            //q.addError('There are fields missing from the call out, listed below '+err+'. Please fix this and try again.',true);
            handleError(mq, null, err, null);
            return;
        }
        reqStub.body = body;
        reqStub.inputHeaders = headers;
        string requestBody = json.serialize(reqStub);
        string response = '';
        string errorMessage = '';
        System.debug('lclc reqStub '+reqStub);
        System.debug('lclc serialized '+requestBody);
    
        try{
            response = webcallout(requestBody, partialEndpoint); 
            system.debug(' lclc response' + response);
            //MK 7/5/2016 Adding response size check to log
            // responses > 6MB (equal to 6000000 characters)
            // and avoid running into size limit errors
            if(response.length() > 6000000) {
                errorMessage = 'Response limit exceeded maximum allowed limit of 6000000 characters';
                handleError(mq, null, null, errorMessage);
            }
        }   catch(exception ex){
            System.debug('LCLC ERROR IN RESPONSE ');
            system.debug(ex.getmessage()); 
            handleError(mq, null, null, null);
        }
    
        if(string.isNotBlank(response)){ 
            cihStub.roiRes res = (cihStub.roiRes) json.deserialize(response, cihStub.roiRes.class);
            // process response. response from 012 is only an acknowledge that PAMS recevied request for ROI calculation
            // just update the message queue record to be success...
            system.debug('lclc stubresponse ' + res);
            if(res.inputHeaders!=null){
                if(res.inputHeaders.MSGSTATUS == 'S'){
                    handleSuccess(mq, res);
                } else {
                    System.debug('LCLC ERROR IN OTHER 1');
                    handleError(mq, null, null, null);
                }
            }else{
                System.debug('LCLC error in other 2 ');
                handleError(mq, res, null, null);
            }
        } else {
            System.debug('LCLC ERROR IN OTHER OTHER 3');
            handleError(mq, null, null, null);
        }
    }
    
    // MK 7/5/2016 -- Changed method signature to include errorMessage for logging 
    // large JSON responses 
    public static void handleError(message_queue__c mq, cihStub.roiRes res, String fields, String errorMessage){
        mq.retry_counter__c += 1;
        mq.status__c = mq.retry_counter__c > Integer.valueof(Label.IntegrationRetryCounter) ? 'failed' : 'failed-retry';
        if(res!= null && res.inputHeaders!=null){
            mq.ERRORTEXT__c=res.inputHeaders.ERRORTEXT;
            mq.ERRORCODE__c=res.inputHeaders.ERRORCODE;
        }
        if(fields!=null){
            if(mq.errortext__c!=null){
                mq.ERRORTEXT__c+='There are fields missing from the call out, listed below '+fields+'. Please fix this and try again.';
            }else{
                mq.ERRORTEXT__c='There are fields missing from the call out, listed below '+fields+'. Please fix this and try again.';
            }
        }
        // MK 7/5/2016 -- Added errorMessage check
        if(errorMessage != null) {
            mq.ERRORTEXT__c = mq.ERRORTEXT__c != null ? mq.ERRORTEXT__c += errorMessage : errorMessage;
        }
        upsert mq;      
        if(mq.Status__c=='failed'){
            Quote q=[select id, Name, Approval_Requestor_Id__c, Integration_Status__c from quote where id=:mq.Identification_Text__c limit 1];
            ROIQuoteButtonExtension cont=new ROIQuoteButtonExtension(new ApexPages.StandardController(q));
            cont.postToChatterNegative(q);
        }
    }

    public static void handleSuccess(message_queue__c mq, cihStub.roiRes res){
        mq.status__c = 'success';
        mq.MSGSTATUS__c = res.inputHeaders.MSGSTATUS;
        mq.MSGUID__c = res.inputHeaders.msgGUID;
        mq.IFID__c = res.inputHeaders.ifID;
        mq.IFDate__c = res.inputHeaders.ifDate;      
        mq.ERRORCODE__c=null;
        mq.ERRORTEXT__c=null;      
        upsert mq;
        if(!Test.isRunningTest())
        {
        Quote q=[select id, Name, Approval_Requestor_Id__c,ROI_Requestor_Id__c, Integration_Status__c from quote where id=:mq.Identification_Text__c limit 1];
        ROIQuoteButtonExtension cont=new ROIQuoteButtonExtension(new ApexPages.StandardController(q));
        cont.postToChatterPositive(q);
        }
    }

    
    public static string webcallout(string body, string partialEndpoint){ 

        System.Httprequest req = new System.Httprequest();
        system.debug('************************************** INSIDE CALLOUT METHOD');
        HttpResponse res = new HttpResponse();
        req.setMethod('POST');
        req.setBody(body);
        system.debug(body);
       
        req.setEndpoint('callout:X012_013_ROI'+partialEndpoint); //+namedEndpoint); todo, make naming consistent on picklist values and endpoints and use that here
        
        req.setTimeout(120000);
            
        //    String username = 'SFDC_USER';
        //    String password = 'samsung_cih_temp';
        //    Blob headerValue = Blob.valueOf(username + ':' + password);
        //    String authorizationHeader = 'BASIC ' +
        //    EncodingUtil.base64Encode(headerValue);
        //    req.setHeader('Authorization', authorizationHeader);
    
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Accept', 'application/json');
    
        Http http = new Http();  
        res = http.send(req);
        System.debug('******** my resonse' + res);
        system.debug('*****Response body' + res.getBody());
        System.debug('***** Response Body Size ' + res.getBody().length());
        return res.getBody();       
    }

public static string webcallout2(string body, string partialEndpoint){ 

        System.Httprequest req = new System.Httprequest();
        system.debug('************************************** INSIDE CALLOUT METHOD2 for Flow 009');
        HttpResponse res = new HttpResponse();
        req.setMethod('POST');
        req.setBody(body);
        system.debug(body);
       
        req.setEndpoint('callout:CIH_US'+partialEndpoint); 
        
        req.setTimeout(120000);
            
           
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Accept', 'application/json');
    
        Http http = new Http();  
        res = http.send(req);
        System.debug('******** my resonse' + res);
        if(res != null) {
            System.debug(' *********************** Response Received ************************');
            System.debug('Response Substring -- ' + res.getBody().subString(0, 50));
            System.debug(' *********************** Response Received ************************');
        }
        system.debug('*****Response body' + res.getBody());
        return res.getBody();       
    }


    public static boolean mapFields(cihStub.roiAmpvRequest body, Quote q){
        Boolean hasErrors=false;

        if(q.opportunity.External_Opportunity_ID__c==null || q.External_SPID__c==null || 
            q.Opportunity.Roll_Out_Start__c ==null|| q.MPS__c==null||
            q.Opportunity.Roll_Out_End_Formula__c==null || q.Supplies_Buying_Location__c==null){
            hasErrors=true;
            if(q.opportunity.External_Opportunity_ID__c==null){
                missingFields.add('Opportunity, External_Opportunity_ID__c');
            }
            if(q.External_SPID__c==null){
                missingFields.add('Quote, External_SPID__c');
            }if(q.Opportunity.Roll_Out_Start__c==null){
                missingFields.add('Opportunity, Roll_Out_Start__c');
            }if(q.MPS__c==null){
                missingFields.add('Quote, MPS__c');
            }if(q.Opportunity.Roll_Out_End_Formula__c==null){
                missingFields.add('Opportunity, Roll_Out_End_Formula__c');
            }if(q.Supplies_Buying_Location__c==null){
                missingFields.add('Quote, Supplies_Buying_Location__c');
            }

            return hasErrors;
        }

        body.busOppId = q.opportunity.External_Opportunity_ID__c;
        body.specialPricingId = q.External_SPID__c;
        Datetime st=datetime.newInstance(q.Opportunity.Roll_Out_Start__c.year(), q.Opportunity.Roll_Out_Start__c.month(),q.Opportunity.Roll_Out_Start__c.day());
        Datetime en=datetime.newInstance(q.Opportunity.Roll_Out_End_Formula__c.year(), q.Opportunity.Roll_Out_End_Formula__c.month(),q.Opportunity.Roll_Out_End_Formula__c.day());
        body.rolloutStartDate=st.formatGMT('yyyyMMdd');
        body.rolloutEndDate= en.formatGMT('yyyyMMdd');
        body.initialFlag='0';
        body.supplierBuyLocation=q.Supplies_Buying_Location__c;
        body.salesSubType=q.MPS__c;
        if(q.quotelineitems != null && q.quotelineitems.size()!=0){
            list<cihStub.oppLine> oppLines = new list<cihStub.oppLine>();
            //Map<Id,Set<Id>> parents=createParentMap(q.quotelineitems);
            hasErrors=mapLineFields(oppLines, q.quotelineitems);
            body.lines = oppLines;
        } else{
            hasErrors=true;
        }

        return hasErrors;      
    }

    public static boolean mapLineFields(list<cihStub.oppLine> oppLines, list<QuoteLineItem> qliList){
        Boolean hasErrors=false;
        system.debug('qliList size------>'+qliList.size());
        //figure out if there are errors
        for(QuoteLineItem qli:qliList){
            if(qli.Reset_Approval__c==null || qli.Product2.SAP_Material_ID__c==null ||qli.product2.Product_Type__c==null ){
                hasErrors=true;
                if(qli.Reset_Approval__c==null){
                    missingFields.add('Quote Line Item, Reset_Approval__c');
                }
                if(qli.Product2.SAP_Material_ID__c ==null ){
                    missingFields.add('Quote Line Item, Product SAP_Material_ID__c');
                }
                if(qli.Product2.Product_Type__c == null){
                    missingFields.add('Quote Line Item, Produt Product_Type__c');
                }
                return hasErrors;
            }
        }

        integer count=1;
        //integer parentCount=1;
        //Added by Thiru----Start----
        Map<String,Set<String>> MAP_QliSetWChild=new Map<String,Set<String>>();
        Map<String,Set<String>> MAP_ProductRelation=new Map<String,Set<String>>();
        String Picebookid = Null;
        Set<String> Set_Parent = new Set<String>();
        
        for(QuoteLineItem qli:qliList)
        {
            if(qli.Product2Id==qli.Parent_Product__c)
            {
                Set_Parent.add(qli.Parent_Product__c);
            }
            Picebookid = qli.PricebookEntry.Pricebook2Id;
            if(qli.Parent_Product__c!=null && qli.Parent_Product__c!=qli.Product2Id)
            {
                If(qli.Product2.Product_Type__c.toUpperCase()=='CONSUMABLE')
                {
                    if(MAP_QliSetWChild.Containskey(qli.Parent_product__c))
                    {
                        
                        MAP_QliSetWChild.get(qli.Parent_product__c).Add(qli.Product2Id);
                    }Else
                    {                        
                        Set<String> temp = new Set<String>();
                        //temp.addAll(MAP_QliSetWChild.get(qli.Parent_product__c));
                        temp.add(qli.Product2Id);  
                        MAP_QliSetWChild.put(qli.Parent_product__c,temp);                                          
                    }
                }                               
            }            
        }
        system.debug('the parent ids :::' + Set_Parent);
        system.debug('the parent and child ids :::' +  MAP_QliSetWChild);
        List<String> Missing = new List<string>();
        Set<String> temp;
        For(Product_Relationship__c Pr: [Select id,Parent_Product__c,Child_Product__c,Child_Product__r.SAP_Material_ID__c,Child_Product__r.Product_Type__c from Product_Relationship__c where Parent_Product__c IN:Set_Parent and Consumable_category__c = 'main' and Is_Active__c=true])
        {
            Missing.add(Pr.Child_Product__c);
            Missing.add(Pr.Parent_product__c);
            If(Pr.Child_Product__r.Product_Type__c.toUpperCase()=='CONSUMABLE')
            {
                if(MAP_ProductRelation.Containskey(Pr.Parent_product__c))
                {
                    
                    MAP_ProductRelation.get(Pr.Parent_product__c).Add(Pr.Child_Product__c);
                }Else
                {
                    temp = new Set<String>();
                    //temp = MAP_QliSetWChild.get(Pr.Parent_product__c);
                    temp.add(Pr.Child_Product__c);  
                    MAP_ProductRelation.put(Pr.Parent_product__c,temp); 
                                     
                }
            }
            
        }
        system.debug('the actual childs of parent :::' + MAP_ProductRelation);
        system.debug('the products in missing list :::' + missing);
        //Finding missing childs
        Map<String,Set<String>> Map_MissingProd = new Map<String,Set<String>>();
        Set<String> QliProd;
        
        For(string msp: Set_Parent)
        {
            If(!MAP_QliSetWChild.Containskey(msp))
            {
                 If(MAP_ProductRelation.Containskey(msp))
                 {
                     Map_MissingProd.put(msp,MAP_ProductRelation.Get(msp));                   
                 }
            }
        }
        system.debug('finding the missing products :::' + Map_MissingProd);
        
        //Getting unit price for missing childs
        Map<String,PricebookEntry> Map_PriceBookEntry = new Map<String,PricebookEntry>();
        If(Missing.Size()>0)
        For(PricebookEntry Price:[Select id,IsActive,Product2Id,UnitPrice,Pricebook2Id,product2.SAP_Material_ID__c from PricebookEntry where Product2Id IN : Missing and Pricebook2Id =:Picebookid ])
        {
            Map_PriceBookEntry.put(Price.Product2Id,Price);
        }
        //Add mising to quote line 
        For(String parent: Map_MissingProd.Keyset())
        {
            QuoteLineItem ql;
            For(String child: Map_MissingProd.get(parent))
            {
                ql= new QuoteLineItem();
                ql.Parent_product__c = parent;
                ql.Product2id = Child;
                ql.SAP_Material_ID__c = Map_PriceBookEntry.get(child).product2.SAP_Material_ID__c;
                //ql.listprice = Map_PriceBookEntry.get(child).UnitPrice;
                ql.Requested_Price__c = Map_PriceBookEntry.get(child).UnitPrice;
                ql.quantity = 0;
                ql.AMPV_Color__c = 0;
                ql.AMPV_Mono__c = 0;
                qliList.add(ql);
            }
        }
        system.debug('the missed product list :::' + qlilist);
        //Added By Thiru-----End-----
        Map<Id,Integer> parentToPItem=new Map<Id,Integer>();
        //set parent and standalone
        for(QuoteLineItem qli:qliList){
            cihStub.oppLine o = new cihStub.oppLine();
            //parent
            if(qli.Product2Id==qli.Parent_Product__c){
                parentToPItem.put(qli.Product2Id,count);
               
                mapSpecific(o,qli);

                o.pItemNo=string.valueOf(count).leftPad(6).replace(' ','0');
                o.itemNo=string.valueOf(count).leftPad(6).replace(' ','0');
                
                oppLines.add(o);

                count++;
                
            }

            //standalone
            if(qli.Parent_Product__c==null){

                mapSpecific(o,qli);

                o.pItemNo=string.valueOf(count).leftPad(6).replace(' ','0');
                o.itemNo=string.valueOf(count).leftPad(6).replace(' ','0');

                oppLines.add(o);

                count++;

            }            
        }
        
        //new for loop for children.
        for(QuoteLineItem qli : qliList){
            if(qli.Parent_Product__c!=null && qli.Parent_Product__c!=qli.Product2Id){
                cihStub.oppLine o = new cihStub.oppLine();
                
                mapSpecific(o,qli);

                o.itemNo=string.valueOf(count).leftPad(6).replace(' ','0');
                o.pItemNo=parentToPItem.get(qli.Parent_Product__c)!=null ? string.valueOf(parentToPItem.get(qli.Parent_Product__c)).leftPad(6).replace(' ','0') : string.valueOf(count).leftPad(6).replace(' ','0');

                count++;

                oppLines.add(o);
                
            }
                
        }
        return hasErrors;
    }

    public static cihStub.oppLine mapSpecific(cihStub.oppLine o, quotelineitem qli){
        if(qli.Reset_Approval__c){
            o.approveFlag='Y';
        }else{
            o.approveFlag='N';
        }
        //o.materialCode = qli.Product2.SAP_Material_ID__c;
        //o.materialType = qli.Product2.Product_Type__c.toUpperCase();
        if(qli.product2.Product_Type__c=='SET' ||  qli.Product2.Product_Type__c=='H/W'){
            o.setType='SET';
            o.materialType='SET';
        }
        else if((qli.id==NULL) || (qli.Product2.Product_Type__c!=NULL && (qli.Product2.Product_Type__c=='CON' || qli.Product2.Product_Type__c.toUpperCase()=='CONSUMABLE'))){
            o.setType='CON';
            o.materialType='CONSUMABLE';
        }else if(qli.Product2.Product_Type__c!=NULL && (qli.Product2.Product_Type__c=='OPT' ||qli.Product2.Product_Type__c.toUpperCase()=='OPTION')){
            o.setType='OPT';
            o.materialType='OPTION';
        }else if(qli.Product2.Product_Type__c!=NULL && (qli.Product2.Product_Type__c=='WAR' ||qli.Product2.Product_Type__c.toUpperCase()=='WARRANTY')){//Added by Thiru on 07/13/2017. For WAR producttype
            o.setType='WAR';
            o.materialType='WARRANTY';
        }
        if(qli.Product2.Product_Type__c!=NULL && qli.Bundle_Flag__c=='Bundle'){
            o.bundle='Y';
        }else{
            o.bundle='N';
        }
        if(qli.id==NULL){
            o.materialCode = string.valueof(qli.SAP_Material_ID__c);
            o.guidePrice = string.valueof(qli.Requested_Price__c);
            o.distyPrice = string.valueof(qli.Requested_Price__c);
            o.quantity = string.valueof(qli.quantity);
            o.ampvMono=string.valueof(qli.AMPV_Color__c);
            o.ampvColor=string.valueof(qli.AMPV_Mono__c);
        }
        else{
            o.materialCode = qli.Product2.SAP_Material_ID__c;
            o.guidePrice = string.valueof(qli.listprice);
            o.distyPrice = string.valueof(qli.Requested_Price__c);
            o.quantity = string.valueof(qli.quantity);
            o.ampvMono=string.valueof(qli.AMPV_Color__c);
            o.ampvColor=string.valueof(qli.AMPV_Mono__c);
        }

        return o;
    }
  /*  public static Map<Id,Set<Id>> createParentMap(list<quotelineitem> qliLst){
        Map<Id,Set<Id>> parentIds=new Map<Id,Set<Id>>();
        Set<Id> prodIds=new Set<Id>();
        
        //create qliids set
        for(quotelineitem qli:qliLst){
            prodIds.add(qli.product2Id);
        }
        System.debug('lclc prodIds '+prodIds);

        //find product relationships
        List<Product_Relationship__c> productRelationshipsInit=[select Parent_Product__c, Child_Product__c from Product_Relationship__c 
                                                                    where Parent_Product__c in :prodIds and Is_Active__c=true];

        System.debug('lclc productRelationshipsInit '+productRelationshipsInit);                                                           
        if(!productRelationshipsInit.isEmpty()){
            for(Product_Relationship__c prs1:productRelationshipsInit){
                if(prodIds.contains(prs1.Parent_Product__c)){
                    if(!parentIds.keySet().contains(prs1.Parent_Product__c) && prodIds.contains(prs1.Child_Product__c)){
                        parentIds.put(prs1.Parent_Product__c, new Set<Id>{prs1.Child_Product__c});
                    }else if(parentIds.keySet().contains(prs1.Parent_Product__c) && prodIds.contains(prs1.Child_Product__c)){
                        Set<Id> cids=parentIds.get(prs1.Parent_Product__c);
                        cids.add(prs1.Child_Product__c);
                        System.debug('lclc new cids '+cids);
                        parentIds.put(prs1.Parent_Product__c,cids);
                    }
                }
            }
        }
       
        return parentIds;

    }*/

}