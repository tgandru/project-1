/**
 * Created by ms on 2017-11-12.
 *
 * author : JeongHo.Lee, I2MAX
 */
public without sharing class RollOutNavigate {

	public Roll_Out__c rolloutObj{get;set;}
	public RollOutNavigate(ApexPages.StandardController controller) {
		rollOutObj = (Roll_Out__c)controller.getRecord();
    }
    public PageReference moveToPage(){

    	Roll_Out__c ro = [SELECT Id, Opportunity__c, Opportunity__r.RecordType.Name FROM Roll_Out__c WHERE Id =: rollOutObj.Id];
    	system.debug('ro : ' + ro.Opportunity__r.RecordType.Name);
    	
    	String retURL = '';

    	if(ro.Opportunity__r.RecordType.Name == 'HA Builder'){
    		retURL = '/apex/HA_RollOutPlan?id=' + ro.Id;
			PageReference goPage = new PageReference(retURL);
			goPage.setRedirect(true);
			return goPage;			
		}
		else{
			retURL = '/apex/RollOutPLan?id=' + ro.Id;
			PageReference goPage = new PageReference(retURL);
			goPage.setRedirect(true);
			return goPage;
		}
		return null;
    }
}