public class RollOutPlanTriggerHandler {
    
    //Logic to Create Roll Out Products for the Rollout Plan's Opportunity's OLI
    //Logic to populate Roll out Start and End dates
    
    //Jagan :- Added to prevent recursive updates
    public static boolean createRolloutProductCheck = FALSE;
    public static boolean updateRolloutProductCheck = FALSE;
    
    
    public static void createRolloutProduct(List<Roll_Out__c> insertedPlans) {
    
     if(!createRolloutProductCheck){ 
         createRolloutProductCheck = TRUE;
         System.debug('## In Rollout Product create block');
            
         List<Roll_Out_Product__c> insertRolloutProducts = new List<Roll_Out_Product__c>();
         Map<Id,Roll_out__c> oppIdToPlanMap = new Map<Id,Roll_out__c>();
         Map<Id, Id> planIdToProductIdMap = new Map<Id, Id>();   
            
         
         for(Roll_Out__c ro: insertedPlans){
            oppIdToPlanMap.put(ro.Opportunity__c, ro);
         }
         //Confirm the mapping
         for(Opportunity opp: [Select Id,Name,Recordtype.Name, 
         (Select Id, Alternative__c, Quantity, Shipped_Quantity__c, PricebookEntry.Product2Id,PricebookEntry.UnitPrice, TotalPrice, UnitPrice From OpportunityLineItems where Alternative__c=false) 
         From Opportunity where Id IN:oppIdToPlanMap.keySet()]) {
         for(OpportunityLineItem oli: opp.OpportunityLineItems){
         insertRolloutProducts.add(new Roll_Out_Product__c(
                                    Opportunity_Line_Item__c = oli.Id,
                                    Product__c = oli.PricebookEntry.Product2Id,
                                    Invoice_Price__c=oli.PricebookEntry.UnitPrice,
                                    Quote_Quantity__c = oli.Quantity,
                                    Quote_Revenue__c = oli.TotalPrice,
                                    SalesPrice__c = (oli.TotalPrice)/(oli.Quantity),
                                    Roll_Out_Plan__c = oppIdToPlanMap.get(opp.Id).Id, 
                                    Roll_Out_Start__c = oppIdToPlanMap.get(opp.Id).Roll_Out_Start__c, 
                                    Rollout_Duration__c = oppIdToPlanMap.get(opp.Id).Rollout_Duration__c,
                                    Opportunity_RecordType_Name__c=opp.Recordtype.name));
                                    
                                 
                                 
         
          }
         }
         
         try{
             if(insertRolloutProducts.size()>0)
             insert insertRolloutProducts;
         }
         catch(Exception e){
            system.debug('## insertRolloutProducts'+e);
         }
     
     } 
    }
    
    //Logic to update Roll Out Products when the respective Rollout Plan's Opportunity's OLI gets updated - by the Trigger on OLI
    //Logic to update Roll out Start and End dates
    
    public static void updateRolloutProduct(List<Roll_Out__c> updatedPlans, Map<Id, Roll_Out__c> oldMap) {
     
     if(!updateRolloutProductCheck){
         updateRolloutProductCheck = TRUE;
         System.debug('## In Rollout Product update block');
            
         List<Roll_Out_Product__c> updateRolloutProducts = new List<Roll_Out_Product__c>();
         Map<Id,Roll_out__c> roIdToPlanMap = new Map<Id,Roll_out__c>();
         Map<Id, Id> planIdToProductIdMap = new Map<Id, Id>();
        // Set<Roll_out__c> updatedPlans = new Set<Roll_out__c>();
         
         for(Roll_Out__c ro:updatedPlans) {
            
            if(ro.Roll_Out_Start__c !=oldMap.get(ro.Id).Roll_Out_Start__c || ro.Rollout_Duration__c !=oldMap.get(ro.Id).Rollout_Duration__c)
                roIdToPlanMap.put(ro.Id, ro);
    
         }   
         
         
    
         for(Roll_Out_Product__c rop: [select Id, Roll_Out_Plan__c, Roll_Out_Start__c, Roll_Out_End__c,Rollout_Duration__c, Do_Not_Edit__c from Roll_Out_Product__c where Roll_Out_Plan__c IN: roIdToPlanMap.keySet()]){
            
            if(rop.Do_Not_Edit__c){
                rop.Rollout_Duration__c = roIdToPlanMap.get(rop.Roll_Out_Plan__c).Rollout_Duration__c;
            
            }
            else {
                rop.Roll_Out_Start__c = roIdToPlanMap.get(rop.Roll_Out_Plan__c).Roll_Out_Start__c;
                rop.Rollout_Duration__c = roIdToPlanMap.get(rop.Roll_Out_Plan__c).Rollout_Duration__c;
            }
            updateRolloutProducts.add(rop);
         }
         try {
             if(updateRolloutProducts.size()>0)
             update updateRolloutProducts;
         }
         catch(Exception e){
            system.debug('## Inside updateRolloutProducts'+ e);
         }
         
        }
    }
}