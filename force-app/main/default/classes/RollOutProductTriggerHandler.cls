/** Apex Class related to  RollOutProduct Trigger
 ------------------------------------------------------------------
 Author          Date        Description
 ------------------------------------------------------------------
 Kavitha M      03/04/2016    Created
 Kavitha M      03/14/2016    updated - Do_Not_Allocate condition
 Jagan:         05/25/2016    changed all references of MiscUtilities to RosMiscUtilities
 
**/
public with sharing class RollOutProductTriggerHandler {

    //Logic to Create Roll Out Schedules for the Rollout Product's 
    public static Integer currentYear = System.Today().year();
    public static Boolean batchJobInitiated = false;
    public static String userMessage = 'Request to re-calculate RollOutSchedule is queued. Please click <a> here </a> to review your request';
    public static void createRolloutSchedule(List<Roll_Out_Product__c> insertedProducts, Map<Id, Roll_Out_Product__c> newROPsMap) {
        System.debug('## In Rollout Schedule create block');
        
        List<Roll_Out_Schedule__c> insertRollOutSchedules = new List<Roll_Out_Schedule__c>();
        
        //call uility method to create ROS: Jagan: changed MiscUtilities to RosMiscUtilities
        for(Roll_Out_Schedule__c s: RosMiscUtilities.createROS(newROPsMap)){
            insertRollOutSchedules.add(s) ;
        }
        try {
            if(insertRollOutSchedules.size()>0)
            insert insertRollOutSchedules;   
        }
        catch(Exception ex){
            System.debug('##ex inside New ROS Insert ::'+ex);
        }
        
    }
    
    public static void updateRolloutSchedule(List<Roll_Out_Product__c> updatedProducts, Map<Id, Roll_Out_Product__c> oldROPMap) {
        System.debug('## In Rollout Schedule update block');
        
        //Jagan:- This needs to be uncommented when it is decided to go with batch class
        Integer dmlRowLimit = 0;
        Integer durationForAllProducts = 0;
        
        for(Roll_Out_Product__c rop : updatedProducts) {
                durationForAllProducts += (Integer)rop.Rollout_Duration__c;
        }
        
        System.debug('*****************************************************');
        System.debug('Product Durations -- '   + durationForAllProducts);
        System.debug('Product List Size -- '   + updatedProducts.size());
        System.debug('Estimated Row Count -- ' + updatedProducts.size() * durationForAllProducts * Roll_Out_Schedule_Constants__c.getInstance('Roll Out Schedule Parameters').Average_Week_Count__c); 
        System.debug('*****************************************************');
        
    /** 
     *  MK 7/1/2016 Added logic to support batch processing
     *  for creating higher volumes of Roll Out Schedules
     */
    // No need to run through the logic if the collections are empty 
    if(updatedProducts.size() > 0 && !oldROPMap.isEmpty()) {
        // Validate record count by combining No. of products * Duration of products * Avg. weeks in a month and check to see if they are less than 3200.
        // 3200 was picked instead of 5000 since there are additional DML operations that get triggered and may run into Governor limits.  
         if((updatedProducts.size() * durationForAllProducts * Roll_Out_Schedule_Constants__c.getInstance('Roll Out Schedule Parameters').Average_Week_Count__c) > Roll_Out_Schedule_Constants__c.getInstance('Roll Out Schedule Parameters').DML_Row_Limit__c) {
                // Check to avoid recursive updates
                if(!batchJobInitiated && !System.isFuture()) {
                    String ropIdList='';
                    // Query for Batch job
                    String ropQuery = 'Select Quote_Quantity__c,Roll_Out_Plan__c,Roll_Out_Start__c,Roll_Out_End__c,Rollout_Duration__c, Do_Not_Allocate__c from Roll_Out_Product__c where Id <> null';
                    Set<Id> roplan = new Set<Id>();
                    RollOutSchedulesBatch rosBatch = new RollOutSchedulesBatch();
                    rosBatch.rolloutplanidset = new Set<Id>();
                        
                    // Build comma seperated list of RollOutProduct Ids
                    for(Roll_Out_Product__c rop: updatedProducts){
                        ropIdList +=',\''+rop.id+'\'';
                        roplan.add(rop.Roll_Out_Plan__c);
                    }
                    
                    // This could be used as an indicator on the RollOutPlan page
                    // to notify the user that the RollOutSchedule request is queued.
                    List<Roll_Out__c> updRoPlanlst = new list<Roll_Out__c>();
                    for(Id rid: roplan){
                        Roll_Out__c temp = new Roll_Out__c(id=rid);
                        temp.Batch_In_Progress__c = true;
                        rosBatch.rolloutplanidset.add(rid);
                        updRoPlanlst.add(temp);
                    }
                    if(updRoPlanlst.size()>0){ 
                        database.update(updRoPlanlst,false);
                        ropIdList = ropIdList.substring(1);
                        String batchQuery = ropQuery + ' AND Id IN (' + ropIdList + ')'; 
                        rosBatch.oldROPMap = oldROPMap;
                        rosBatch.query = batchQuery;
                        try {
                            Database.executeBatch(rosBatch);
                            ID batchprocessid = Database.executeBatch(rosBatch);
                            batchJobInitiated = true;
                      }catch(System.AsyncException ex) {
                           /* Could really add some good retry logic or routing emails to Samsung admin support distro
                           Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                           String[] toAddresses = new String[] {'REPLACE_WITH_EMAIL'};
                           mail.setToAddresses(toAddresses);
                           mail.setSubject('Batch Apex Exception');
                           mail.setPlainTextBody('An exception occured while processing the ROS batch. Please review the logs to determine the cause ' + ex.getMessage());
                           Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                           */
                      }
                    }
                }
         }
         // Process request real time to generate RollOutSchedules 
         else {
            
                //Variables for Qty update
                List<Roll_Out_Schedule__c> roswithQtyUpdatesToUpdate = new List<Roll_Out_Schedule__c>();    
                Set<Id> ropIdsWithUpdatedQty = new Set<Id>();      
                
                //Variables for Start/End date updates with no Actuals posted on ROS
                Set<Id> ropsWithUpdatedDates = new Set<Id>();
                Map<Id,Roll_Out_Product__c> ropIdToROPMap = new Map<Id,Roll_Out_Product__c>(); //dupe variable
                Map<Id, List<Roll_Out_Schedule__c>> ropIdToROSMap = new Map<Id, List<Roll_Out_Schedule__c>>();
                Set<Id> ropWithROSActuals = new Set<Id>(); 
                Set<Id> ropWithOutROSActuals  = new Set<Id>(); 
                List<Roll_Out_Schedule__c> rosToBeDeleted = new List<Roll_Out_Schedule__c>();
                List<Roll_Out_Schedule__c> rosToInsert = new List<Roll_Out_Schedule__c>();
                
                //variables for Start/End date updates with ROS having Actuals
                Set<Id> ROPSwithNoFWOnlyQtyUpdate = new Set<Id>();
                Map<Roll_out_product__c, Integer> ROPToMoreFW = new Map<Roll_out_product__c, Integer>();
                Map<Roll_out_product__c, Integer> ROPToLessFW = new Map<Roll_out_product__c, Integer>();
                List<Roll_Out_Schedule__c> ROSwithNoFWOnlyQtyUpdateList = new List<Roll_Out_Schedule__c>();
                List<Roll_Out_Schedule__c> insertROSrecs = new List<Roll_Out_Schedule__c>();
                Set<Id> ropSetWithMoreFW = new Set<Id>();
                List<Roll_Out_Schedule__c> ROSwithmoreFW = new List<Roll_Out_Schedule__c>();
                List<Roll_Out_Schedule__c> deleteROSrecs = new List<Roll_Out_Schedule__c>();
                Set<Id> ropSetWithLessFW = new Set<Id>();
                List<Roll_Out_Schedule__c> ROSwithfewFW = new List<Roll_Out_Schedule__c>();
                
                //Qty update logic begins
                
                for(Roll_Out_Product__c rop: updatedProducts){    
                    
                    //Do not auto-allocate if Do_Not_Allocate__c = True
                    if(!rop.Do_Not_Allocate__c){
                        //Check if Quantity has changed - If Yes, then reallocate the quantity across all ROS
                        if(rop.Quote_Quantity__c != oldROPMap.get(rop.Id).Quote_Quantity__c &&
                                rop.Roll_Out_Start__c == oldROPMap.get(rop.Id).Roll_Out_Start__c && 
                                rop.Roll_Out_End__c == oldROPMap.get(rop.Id).Roll_Out_End__c){
                            ropIdsWithUpdatedQty.add(rop.Id);                               
                        }
                        else if(rop.Roll_Out_Start__c != oldROPMap.get(rop.Id).Roll_Out_Start__c ||
                                rop.Rollout_Duration__c != oldROPMap.get(rop.Id).Rollout_Duration__c){
                            ropsWithUpdatedDates.add(rop.Id);
                        }
                    }
                }
                
                //Call utility method to get the latest quantities per ROS to update 
        
                //Jagan: changed MiscUtilities to RosMiscUtilities
                for(Roll_out_Schedule__c schedule: RosMiscUtilities.updateROS(ropIdsWithUpdatedQty)){
                    roswithQtyUpdatesToUpdate.add(schedule);
                    
                }
                
                try {
                    if(roswithQtyUpdatesToUpdate.size()>0){
                        System.debug('## roswithQtyUpdatesToUpdate ::'+roswithQtyUpdatesToUpdate.size());
                        update roswithQtyUpdatesToUpdate;
                    }
                }
                Catch(Exception ex){
                    system.debug('##ex Inside Update Qty ::'+ex);
                }
                
                //Start/End date update logic begins
                System.debug('## ropsWithUpdatedDates ::'+ropsWithUpdatedDates);
                for(Roll_Out_Schedule__c ros: [select Id, Do_Not_Edit__c, plan_Quantity__c, Actual_Quantity__c, Roll_Out_Product__c 
                from Roll_Out_Schedule__c where Roll_Out_Product__c IN :ropsWithUpdatedDates]){
                    
                    System.debug('## updated ROPs ROS records :: '+ros);
                    //Map holds Schedules with both Actuals and Non Actuals
                    if(!ropIdToROSMap.containsKey(ros.Roll_Out_Product__c)) {
                        ropIdToROSMap.put(ros.Roll_Out_Product__c, new List<Roll_Out_Schedule__c>());
                    }
                    ropIdToROSMap.get(ros.Roll_Out_Product__c).add(ros);
                    
                    //Set holds ROP which has schedules with Actuals posted
                    if(ros.Do_Not_Edit__c)
                    ropWithROSActuals.add(ros.Roll_Out_Product__c);                                                                                                 
                }
                System.debug('## All Rop with ROS map: ropIdToROSMap ::'+ropIdToROSMap);
                
                for(Id checkROP:ropsWithUpdatedDates){
                    //Set holds ROP which doesnt have schedules with Actuals posted
                    if(!ropWithROSActuals.contains(checkROP)){
                        system.debug('## ROPId with no actuals :: checkROP::'+checkROP);
                        ropWithOutROSActuals.add(checkROP);
                    }
                }
                //Logic to identify and delete ROS with no Actuals posted 
                for(Id productId:ropWithOutROSActuals){
                    If(ropIdToROSMap.containsKey(productId)){
                        for(Roll_Out_Schedule__c sched: ropIdToROSMap.get(productId))
                        rosToBeDeleted.add(sched);
                    }
                }
                //Jagan:- commented to code to put the delete and insert in same try/catch block
                /*try{
                    if(rosToBeDeleted.size()>0)
                        delete rosToBeDeleted;
                }
                
                catch(Exception ex){
                    system.debug('##Exception in ROS deletion ::'+ex);
                }*/
                //end uncommented
        
                System.debug('##ROS to be deleted : rosToBeDeleted.size::'+rosToBeDeleted.size());
                // Re-create ROS for ROP's (with latest info) in ropWithOutROSActuals 
                for(Roll_Out_Product__c product :[select Id, Quote_Quantity__c, Roll_Out_Start__c, Roll_Out_End__c, Roll_Out_End_Formula__c, Rollout_Duration__c, Number_of_Roll_Out_Schedules__c  from Roll_Out_Product__c where Id IN:ropWithOutROSActuals]){
                    ropIdToROPMap.put(product.Id, Product); 
                }
                //Jagan: Changed MiscUtilities to RosMiscUtilities
                for( Roll_Out_Schedule__c rosNew:RosMiscUtilities.createROS(ropIdToROPMap)) {          
                    rosToInsert.add(rosNew);
                }
                
                try{
                    System.debug('Before delete/insert. Current DML rows count: ' + Limits.getDMLRows() + ' of total: ' + Limits.getLimitDMLRows() ); 
                    if(Limits.getDMLRows()+rosToBeDeleted.size() + rosToInsert.size() < Limits.getLimitDMLRows()){
                        
                        if(rosToBeDeleted.size()>0)
                            delete rosToBeDeleted;
                            
                        system.debug('##rosToInsert.size::'+rosToInsert.size());
                        
                        if(rosToInsert.size()>0)
                            insert rosToInsert;
                    }
                    
                    else {
                        // Run a Batch Job and notify the user in the end
                        
                    }
                }
                catch(Exception ex){
                    system.debug('##Exception in ROS Insertion (in Update block) ::'+ex);
                }
                
                //Logic to handle ROP records where ROP's start and End date has changed and has actuals posted
                
                if(ropWithROSActuals.size()>0) {
                    for(Roll_Out_Product__c ropRec:[Select Id,Roll_Out_Start__c, Roll_Out_End__c, Roll_Out_End_Formula__c, Rollout_Duration__c, Number_of_Roll_Out_Schedules__c, Quote_Quantity__c from Roll_Out_Product__c where Id IN:ropWithROSActuals]) {
                        //Jagan: changed MiscUtilities to RosMiscUtilities
                        Integer startFW = RosMiscUtilities.giveFiscalWeek(ropRec.Roll_Out_Start__c);
                        Date EndDateCalc = (ropRec.Roll_Out_Start__c).addMonths((Integer)ropRec.Rollout_Duration__c);
                        //Jagan: changed MiscUtilities to RosMiscUtilities
                        Integer EndFW = RosMiscUtilities.giveFiscalWeek(EndDateCalc);
                        Integer tempFWDiff = (EndFW - startFW)+1;
                        
                        if((tempFWDiff == ropRec.Number_of_Roll_Out_Schedules__c) && ropRec.Quote_Quantity__c !=oldROPMap.get(ropRec.Id).Quote_Quantity__c){
                            ROPSwithNoFWOnlyQtyUpdate.add(ropRec.Id); 
                        }
                        
                        else if((tempFWDiff != ropRec.Number_of_Roll_Out_Schedules__c) && (tempFWDiff > ropRec.Number_of_Roll_Out_Schedules__c)) {
                            ROPToMoreFW.put(ropRec,EndFW); 
                        }
                        
                        else if ((tempFWDiff != ropRec.Number_of_Roll_Out_Schedules__c) && (tempFWDiff < ropRec.Number_of_Roll_Out_Schedules__c)) {
                            ROPToLessFW.put(ropRec,tempFWDiff); 
                        }
                        
                    }
                }
                
                //Scenario 1: when the new FW difference is same as Old and if Quantity is updated  
                if(ROPSwithNoFWOnlyQtyUpdate.size()> 0) {
                    //Jagan: changed MiscUtilities to RosMiscUtilities
                    for(Roll_Out_Schedule__c qtyUpdatedROS: RosMiscUtilities.updateROS(ROPSwithNoFWOnlyQtyUpdate)){
                        ROSwithNoFWOnlyQtyUpdateList.add(qtyUpdatedROS); 
                    }
                }
                
                try {
                    if(ROSwithNoFWOnlyQtyUpdateList.size()> 0) {
                        //Added by Jagan to check the dml limits
                        if(Limits.getDMLRows()+ROSwithNoFWOnlyQtyUpdateList.size() < Limits.getLimitDMLRows())
                            update ROSwithNoFWOnlyQtyUpdateList;
                    }
                }
                catch(Exception ex){
                    System.debug('## Exception inside scenario 1 ROSwithNoFWOnlyQtyUpdateList :: '+ex);
                }
                
                //Scenario 2: When the new FW difference number is greater than before
                if(ROPToMoreFW.size()> 0) {
                    for(Roll_Out_Product__c rop:ROPToMoreFW.keySet()){
                        Integer tempDiff = ROPToMoreFW.get(rop) - (Integer)rop.Number_of_Roll_Out_Schedules__c;
                        //Jagan: changed MiscUtilities to RosMiscUtilities
                        for(Integer i=((Integer)RosMiscUtilities.giveFiscalWeek(oldROPMap.get(rop.Id).Roll_Out_End_Formula__c))+1; i<= ROPToMoreFW.get(rop); i++) { 
                            insertROSrecs.add(new Roll_Out_Schedule__c(
                            Roll_Out_Product__c = rop.Id,  
                            //Jagan: changed MiscUtilities to RosMiscUtilities
                            Plan_Date__c = RosMiscUtilities.giveDateforFiscalWeek(''+i+currentYear), 
                            fiscal_week__c =  String.valueOf(i),
                            //Jagan: changed MiscUtilities to RosMiscUtilities
                            fiscal_month__c = String.valueOf(RosMiscUtilities.giveFiscalMonth (''+i+currentYear)),
                            year__c = String.valueOf(currentYear),
                            Plan_Quantity__c = 1)); //temporarily assigning qty to 1
                        }
                        ropSetWithMoreFW.add(rop.Id);
                    }
                }
                
                try {
                    if(insertROSrecs.size()> 0){
                        //Added by Jagan to check the dml limits
                        if(Limits.getDMLRows()+insertROSrecs.size() < Limits.getLimitDMLRows())
                            insert insertROSrecs;
                    }
                }
                catch(Exception ex) {
                    System.debug('## Exception inside scenario 2 insertROSrecs :: '+ex);
                }
                
                if(ropSetWithMoreFW.size()> 0) {
                    //Jagan: changed MiscUtilities to RosMiscUtilities
                    for(Roll_Out_Schedule__c qtyUpdatedROS: RosMiscUtilities.updateROS(ropSetWithMoreFW)){
                        ROSwithmoreFW.add(qtyUpdatedROS); 
                    }
                }
                
                try {
                    if(ROSwithmoreFW.size()> 0) {
                        //Added by Jagan to check the dml limits
                        if(Limits.getDMLRows()+ROSwithmoreFW.size() < Limits.getLimitDMLRows())
                            update ROSwithmoreFW;
                    }
                }
                catch(Exception ex){
                    System.debug('## Exception inside scenario 2 ROSwithmoreFW :: '+ex);
                }
                //Scenario 3: when the new FW difference number is less than before
                if(ROPToLessFW.size() > 0) {
                    for(Roll_Out_Product__c rop:ROPToLessFW.keySet()){
                        Integer tempDiff = ((Integer)rop.Number_of_Roll_Out_Schedules__c - ROPToLessFW.get(rop));
                        for(Roll_Out_Schedule__c ros: [Select Id,Do_Not_Edit__c, fiscal_week__c, Roll_Out_Product__c from Roll_Out_Schedule__c where Roll_Out_Product__c =:rop.Id Order By fiscal_week__c Desc limit :tempDiff]) { 
                            if(ros.Do_Not_Edit__c == false) { 
                                deleteROSrecs.add(ros);
                            }
                        }
                        ropSetWithLessFW.add(rop.Id);
                    }
                }
                
                try {
                    if(deleteROSrecs.size()> 0) {
                        //Added by Jagan to check the dml limits
                        if(Limits.getDMLRows()+deleteROSrecs.size() < Limits.getLimitDMLRows())
                            delete deleteROSrecs;
                    }
                }
                Catch(Exception ex) {
                    System.debug('## Exception inside scenario 3 deleteROSrecs :: '+ex);
                }
                
                if(ropSetWithLessFW.size()> 0) {
                    //Jagan: changed MiscUtilities to RosMiscUtilities
                    for(Roll_Out_Schedule__c qtyUpdatedSchedules: RosMiscUtilities.updateROS(ropSetWithLessFW)){
                        ROSwithfewFW.add(qtyUpdatedSchedules); 
                    }
                }
                
                if(ROSwithfewFW.size()> 0) {
                    //Added by Jagan to check the dml limits
                    if(Limits.getDMLRows()+ROSwithfewFW.size() < Limits.getLimitDMLRows())
                        update ROSwithfewFW;
                }
                
                //***************//
           }
        }
    }
}