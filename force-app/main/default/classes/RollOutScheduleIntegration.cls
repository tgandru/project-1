/*
 * TODO: Validate JSON as containing the correct parameters.
 * TODO: Update current/future week  
 * TODO: Add proper integration endpoint
 * TODO: Add test class
 * Recieves results of chunked batch query from RollOutScheduleBatch.cls, 
 * formats the result as JSON, and sends request through a webcallout. 
 * Author: Eric Vennaro
**/

public class RollOutScheduleIntegration {
	private CihStub.RollOutScheduleRequest wrapper;
	private Boolean hasMore;
	private CiHStub.RollOutResponse response;
	private List<Roll_Out_Schedule__c> rollOutSchedulesFromQuery;
	private Message_Queue__c mq;

	public RollOutScheduleIntegration(List<Roll_Out_Schedule__c> rollOutSchedulesFromQuery, Boolean hasMore) {
		this.rollOutSchedulesFromQuery = rollOutSchedulesFromQuery;
		wrapper = new CihStub.RollOutScheduleRequest();
		wrapper.inputHeaders = new CihStub.MsgHeadersRequest();
		wrapper.body = new CihStub.RollOutScheduleRequestBody();
		hasMore = this.hasMore;
	}

	public void callRollOutScheduleEndpoint() {
		Message_Queue__c mq = new Message_Queue__c(Integration_Flow_Type__c = '012-013-Rollout', Retry_Counter__c = 0);
		callRolloutScheduleEndpoint(mq);
	}

	public void callRollOutScheduleEndpoint(Message_Queue__c mq) {
		this.mq = mq;
		configureRollOutRequestBody();
		completeHttpRequest();
	}

	private void completeHttpRequest(){
		HttpClient httpClient = new HttpClient('callout:X012_013_Rollout');
		String requestBody = JSON.serialize(wrapper);
		try {
			String responseString = httpClient.performCallout(requestBody, mq.Integration_Flow_Type__c);
			if(String.isNotBlank(responseString)) {
				response = (CihStub.RollOutResponse) JSON.deserialize(responseString, CiHStub.RollOutResponse.Class);
			} else {
				httpClient.handleError(mq);
			}
		} catch(Exception e){
			System.debug(e.getMessage());
			httpClient.handleError(mq);
		}
	}

	private void configureRollOutRequestBody() {
		Map<String, CiHStub.RollOutPlanStub> result = new Map<String, CiHStub.RollOutPlanStub>();
	
		for(Roll_Out_Schedule__c schedule : rollOutSchedulesFromQuery){
			RollOutSchedule rollOutSchedule = new RollOutSchedule(schedule);
			String planId = rollOutSchedule.planId;
			
			if(result.containsKey(planId)){
				CihStub.RollOutPlanStub plan = result.get(planId);
				plan.rollOutSchedule.add(newRollOutSchedule(rollOutSchedule));
				result.put(planId, plan);
			} else {
				result.put(planId, newRollOutPlan(rollOutSchedule));
			}
		}

		wrapper.body.rollOutPlan = result.values();
	}

	private CihStub.RollOutPlanStub newRollOutPlan(RollOutSchedule schedule) {
		CiHStub.RollOutPlanStub rollOutPlanStub = new CiHStub.RollOutPlanStub();
		rollOutPlanStub.opportunityId = schedule.opportunityId;
		rollOutPlanStub.rollOutPlanId = schedule.planId;
		rollOutPlanStub.rollOutStart = schedule.rollOutStart;
		rollOutPlanStub.rollOutEnd = schedule.rollOutEnd;
		// MK Added 6/29/2016 The rollOutScheduleStub list
	    // will always be null the first time and needs to be initialized
		if(rollOutPlanStub.rollOutSchedule == null) {
			System.debug('rollout schedule stub list is null');
			rollOutPlanStub.rollOutSchedule = new List<CihStub.RollOutScheduleStub>();
		}
		rollOutPlanStub.rollOutSchedule.add(newRollOutSchedule(schedule));

		return rollOutPlanStub;
	}

	private CihStub.RollOutScheduleStub newRollOutSchedule(RollOutSchedule schedule) {
		CihStub.RollOutScheduleStub rollOutScheduleStub = new CihStub.RollOutScheduleStub();
		rollOutScheduleStub.materialCode = schedule.sapMaterialCode;
		rollOutScheduleStub.productType = schedule.productType;
		rollOutScheduleStub.currentWeek = schedule.currentWeek;
		rollOutScheduleStub.futureWeek = schedule.fiscalWeek;
		rollOutScheduleStub.planQuantity = schedule.planQuantity;
		rollOutScheduleStub.planRevenue = schedule.planRevenue;

		return rollOutScheduleStub;
	}

	// --- Model

	public class RollOutSchedule {
		private Roll_Out_Schedule__c rollOutSchedule;

		public RollOutSchedule(Roll_Out_Schedule__c rollOutSchedule){
			this.rollOutSchedule = rollOutSchedule;
		}
		
		public String planQuantity {
			get {
				return rollOutSchedule.Plan_Quantity__c != null ? String.valueOf(rollOutSchedule.Plan_Quantity__c) : ' ';
			}
		}

		public String planRevenue {
			get {
				return rollOutSchedule.Plan_Revenue__c != null ? String.valueOf(rollOutSchedule.Plan_Revenue__c) : ' ';
			}
		}

		public String fiscalWeek {
			get {
				return rollOutSchedule.Fiscal_Week__c != null ? String.valueOf(rollOutSchedule.Fiscal_Week__c) : ' ';
			}
		}

		public String currentWeek {
			get {
				return 'TODO: Configure current week through date utility';
			}
		}

		public String planId {
			get {
				return rollOutSchedule.Roll_Out_Product__r.Roll_Out_Plan__r.Id != null ? String.valueOf(rollOutSchedule.Roll_Out_Product__r.Roll_Out_Plan__r.Id) : ' ';
			}
		}

		public String opportunityId {
			get {
				return rollOutSchedule.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Id != null ? String.valueOf(rollOutSchedule.Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Id) : ' ';
			}
		}

		public String rollOutStart {
			get {
				return rollOutSchedule.Roll_Out_Product__r.Roll_Out_Plan__r.Roll_Out_Start__c != null ? String.valueOf(rollOutSchedule.Roll_Out_Product__r.Roll_Out_Plan__r.Roll_Out_Start__c) : ' ';
			}
		}

		public String rollOutEnd {
			get {
				return rollOutSchedule.Roll_Out_Product__r.Roll_Out_Plan__r.Roll_Out_End__c != null ? String.valueOf(rollOutSchedule.Roll_Out_Product__r.Roll_Out_Plan__r.Roll_Out_End__c) : ' ';
			}
		}

		public String sapMaterialCode {
			get {
				return rollOutSchedule.Roll_Out_Product__r.SAP_Material_CODE__c != null ? String.valueOf(rollOutSchedule.Roll_Out_Product__r.SAP_Material_CODE__c) : ' ';
			}
		}

		public String productType {
			get {
				return rollOutSchedule.Roll_Out_Product__r.Product__r.Product_Type__c != null ? String.valueOf(rollOutSchedule.Roll_Out_Product__r.Product__r.Product_Type__c) : ' ';
			}
		}
	}
}