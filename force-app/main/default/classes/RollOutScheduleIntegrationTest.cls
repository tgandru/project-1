/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 
/**
 * @Author: Mir Khan
 * @Info: Test class for RollOutScheduleIntegration
 *
 */
@isTest(SeeAllData=true) 
private class RollOutScheduleIntegrationTest {
    
    private static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');

    static testMethod void testRollOutScheduleCallOut() {
        
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Set<Id> oliSetIds = new Set<Id>();
        Map<Id, Id> ropToOLIMap = new Map<Id,Id>();
        Account account = TestDataUtility.createAccount('Marshal Mathers');
        insert account;
        Account partnerAcc = TestDataUtility.createAccount('Distributor account');
        partnerAcc.RecordTypeId = directRT;
        insert partnerAcc;
        
         //Creating custom Pricebook
        PriceBook2 pb = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(4)); 
        insert pb;
        Opportunity opportunity = TestDataUtility.createOppty('New', account, pb, 'Managed', 'IT', 'product group',
                                    'Identified');
        insert opportunity;
        Partner__c  partner= TestDataUtility.createPartner(opportunity, account, partnerAcc, 'Distributor');
        partner.Is_Primary__c=true;
        insert partner; 
        Product2 product = TestDataUtility.createProduct2('SL-SCF5300/SEG1', 'Standalone Printer', 'Printer[C2]', 'H/W', 'FAX/MFP');
        insert product;
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry pricebookEntry = TestDataUtility.createPriceBookEntry(product.Id, pb.Id);
        pricebookEntry.unitprice=110;
        insert pricebookEntry;
        
        List<OpportunityLineItem> opportunityLineItems = new List<OpportunityLineItem>();
        OpportunityLineItem op1 = TestDataUtility.createOpptyLineItem(product, pricebookEntry, opportunity);
        op1.Quantity = 100;
        op1.TotalPrice = 1000;
        op1.Requested_Price__c = 100;
        op1.Claimed_Quantity__c = 45;     
        opportunityLineItems.add(op1);
        insert opportunityLineItems;
        
        opportunity.StageName = 'Qualified';
        opportunity.Roll_Out_Start__c=System.today().addDays(61);
        opportunity.Rollout_Duration__c=5;
        
        update opportunity;
        
        Opportunity updatedOpp= [select Id, Roll_Out_Plan__c from Opportunity where id =: opportunity.Id limit 1];
        System.assert(updatedOpp.Roll_Out_Plan__c!=null);

        //Assuming we are inserting one test OLI
        List<Roll_Out_product__c> ropList = [select Id, Quote_Quantity__c, Rollout_Duration__c from Roll_Out_product__c where Roll_Out_Plan__c=: updatedOpp.Roll_Out_Plan__c limit 1];
        System.debug(ropList[0].Rollout_Duration__c);
        System.assert(ropList[0].Rollout_Duration__c==5);
        
        //(End Date - StartDate) = # of FWs = # of Schedules
        List<Roll_Out_Schedule__c> ros = [select Id, plan_Quantity__c, Actual_Quantity__c, Do_Not_Edit__c, Fiscal_week__c from Roll_Out_Schedule__c where Roll_Out_product__c =: ropList[0].Id];
        //System.assert(ros.size()>=1);
        
        for(OpportunityLineItem oli : opportunityLineItems) {
            oliSetIds.add(oli.Id);
        }
        //Query on RollOut Products
        for(Roll_Out_Product__c rop: [Select Id,Opportunity_Line_Item__c from Roll_Out_Product__c where Opportunity_Line_Item__c IN: oliSetIds]){         
            ropToOLIMap.put(rop.Id,rop.Opportunity_Line_Item__c);
        }
        
        List<Roll_Out_Schedule__c> rollOutScheduleList = [SELECT Id, Plan_Quantity__c, 
                                                            Plan_Revenue__c, 
                                                            Fiscal_Week__c, 
                                                            Roll_Out_Product__r.Roll_Out_Plan__r.Id, 
                                                            Roll_Out_Product__r.Roll_Out_Plan__r.Opportunity__r.Id, 
                                                            Roll_Out_Product__r.Roll_Out_Plan__r.Roll_Out_Start__c, 
                                                            Roll_Out_Product__r.Roll_Out_Plan__r.Roll_Out_End__c, 
                                                            Roll_Out_Product__r.SAP_Material_CODE__c,
                                                            Roll_Out_Product__r.Product__r.Product_Type__c from Roll_Out_Schedule__c where Roll_Out_Product__c IN: ropToOLIMap.keyset()];
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockResponseGenerator('"S"'));
        RollOutScheduleIntegration rolloutScheduleIntegration = new RollOutScheduleIntegration(rollOutScheduleList, true);
        rolloutScheduleIntegration.callRollOutScheduleEndpoint();
        Test.stopTest();
        
    }
    
    /**
  * Inner MockSuccessResponse class
  */   
 class MockResponseGenerator implements HttpCalloutMock {
    String responseStatus;
    MockResponseGenerator(String responseStatus) {
        this.responseStatus = responseStatus;
    }
    // Implement this interface method
    public HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        System.assertEquals('callout:X012_013_Rollout', req.getEndpoint());
        System.assertEquals('POST', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{' +
                        '"inputHeaders": {' +
                        '"MSGGUID": "b336d2a9-f8cb-a399-8eea-dc11782453c9",' +
                        '"IFID": "LAY024197",' +
                        '"IFDate": "1603161220013",' +
                        '"MSGSTATUS": "S",' +
                        '"ERRORTEXT": null,' +
                        '"ERRORCODE": null' +
                      '}' +
                    '}');
        res.setStatusCode(200);
        return res;
    }
  }
}