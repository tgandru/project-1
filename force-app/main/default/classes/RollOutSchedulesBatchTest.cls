/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 
 /**
  * @Author: Mir Khan
  * @Info: Test class for RollOutSchedulesBatch
  */
@isTest(SeeAllData=true)
private class RollOutSchedulesBatchTest {
	static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
	
    static testMethod void updateLargeRollOutScheduleTest() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Set<Id> oliSetIds = new Set<Id>();
        String ropIds = '';
        Map<Id, Roll_Out_Product__c> oldROPMap = new Map<Id, Roll_Out_Product__c>();
        Map<Id, Id> ropToOLIMap = new Map<Id,Id>();
        String ropQuery = 'Select Quote_Quantity__c,Roll_Out_Plan__c,Roll_Out_Start__c,Roll_Out_End__c,Rollout_Duration__c, Do_Not_Allocate__c from Roll_Out_Product__c where Id <> null';
        RollOutSchedulesBatch rosBatch = new RollOutSchedulesBatch();
	    rosBatch.rolloutplanidset = new Set<Id>();
        Roll_Out_Schedule_Constants__c rosCustomSetting = Roll_Out_Schedule_Constants__c.getValues('Roll Out Schedule Parameters');
        if(rosCustomSetting == null) {
        	rosCustomSetting = new Roll_Out_Schedule_Constants__c(Name='Roll Out Schedule Parameters');
        	rosCustomSetting.Average_Week_Count__c = 4.33;
        	rosCustomSetting.DML_Row_Limit__c = 3200;
        	insert rosCustomSetting;
        }
        
        Account account = TestDataUtility.createAccount('Marshal Mathers');
        insert account;
        Account partnerAcc = TestDataUtility.createAccount('Distributor account');
        partnerAcc.RecordTypeId = directRT;
        insert partnerAcc;
        
         //Creating custom Pricebook
        PriceBook2 pb = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(4)); 
        insert pb;
        Opportunity opportunity = TestDataUtility.createOppty('New', account, pb, 'Managed', 'IT', 'product group',
                                    'Identified');
        insert opportunity;
        Partner__c  partner= TestDataUtility.createPartner(opportunity, account, partnerAcc, 'Distributor');
        partner.Is_Primary__c=true;
        insert partner;
        
        Product2 product = TestDataUtility.createProduct2('SL-SCF5300/SEG1', 'Standalone Printer', 'Printer[C2]', 'H/W', 'FAX/MFP');
        insert product;
        
        
        PricebookEntry pricebookEntry = TestDataUtility.createPriceBookEntry(product.Id, pb.Id);
	    pricebookEntry.unitprice=110;
        insert pricebookEntry;
        
        
        
        List<OpportunityLineItem> opportunityLineItems = new List<OpportunityLineItem>();
        
        for(Integer i=0; i<36; i++) {
	        OpportunityLineItem op1 = TestDataUtility.createOpptyLineItem(product, pricebookEntry, opportunity);
	        op1.Quantity = 1000;
	        op1.TotalPrice = 1000;
	        op1.Requested_Price__c = 100;
	        op1.Claimed_Quantity__c = 45;     
	        opportunityLineItems.add(op1);
        }
        insert opportunityLineItems;
        
        opportunity.StageName = 'Qualified';
        opportunity.Roll_Out_Start__c=System.today().addDays(61);
        opportunity.Rollout_Duration__c=5;
        
        update opportunity;
        
        Opportunity updatedOpp= [select Id, Roll_Out_Plan__c from Opportunity where id =: opportunity.Id limit 1];
        System.assert(updatedOpp.Roll_Out_Plan__c!=null);

        //Assuming we are inserting one test OLI
        List<Roll_Out_product__c> ropList = [select Id, Quote_Quantity__c, Rollout_Duration__c from Roll_Out_product__c where Roll_Out_Plan__c=: updatedOpp.Roll_Out_Plan__c];
        System.debug(ropList[0].Rollout_Duration__c);
        System.assert(ropList[0].Rollout_Duration__c==5);
        for(Roll_Out_Product__c rp : ropList) {
        	rp.Rollout_Duration__c = 10;
        	oldROPMap.put(rp.Id, rp);
        }
        
        //(End Date - StartDate) = # of FWs = # of Schedules
        List<Roll_Out_Schedule__c> ros = [select Id, plan_Quantity__c, Actual_Quantity__c, Actual_Revenue__c, Do_Not_Edit__c, Fiscal_week__c from Roll_Out_Schedule__c where Roll_Out_product__c =: ropList[0].Id];
        //ros.get(0).Actual_Quantity__c = 2;
        //ros.get(0).Actual_Revenue__c = 100;
        //update ros;
        //System.assert(ros.size()>=1);
        
        Roll_Out__c rollOutPlan = new Roll_Out__c(Id=updatedOpp.Roll_Out_Plan__c);
        rollOutPlan.Rollout_Duration__c=60;
        
        update rollOutPlan;
        
        for(Roll_Out_Product__c rp : ropList) {
        	ropIds +=',\''+rp.id+'\'';
        }
        
        ropIds = ropIds.substring(1);
			            String batchQuery = ropQuery + ' AND Id IN (' + ropIds + ')'; 
			            rosBatch.oldROPMap = oldROPMap;
			            rosBatch.query = batchQuery;
			            try {
				            Database.executeBatch(rosBatch);
			          }catch(System.AsyncException ex) {
			          	
			          }
        
    }
}