global class RolloutDataDeleteBatch implements Database.batchable<sObject>,database.stateful {
    global String OppStagename;
    global Integer CloseDateYear;
    global String SuccessRecords;
    global String FailedRecords;
    global integer TotalRolloutRecords;

    global RolloutDataDeleteBatch(String OppStagename, integer CloseDateYear){
        this.OppStagename = OppStagename;
        this.CloseDateYear = CloseDateYear;
        SuccessRecords = 'Rollout ID, DeleteStatus\n';
        FailedRecords = 'Rollout ID, Error\n';
        TotalRolloutRecords = 0;
    }
    
    global database.QueryLocator start(Database.batchablecontext bc){
        Database.QueryLocator ql = Database.getQueryLocator([Select id from Roll_Out__c where Opportunity__r.stagename=:OppStagename and calendar_year(Opportunity__r.closedate)<=:CloseDateYear]);
        return ql;
    }
    
    global void execute(Database.batchablecontext bc, List<Roll_Out__c> RolloutList){
        system.debug('Rollout List Size:------->'+RolloutList.size());
        TotalRolloutRecords = TotalRolloutRecords + RolloutList.size();
        Database.DeleteResult[] drList = Database.delete(RolloutList, false);
        for(Database.DeleteResult dr : drList) {
            if (dr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully deleted Rollout with ID: ' + dr.getId());
                SuccessRecords = SuccessRecords +dr.getId()+',Success\n';
            }
            else {
                // Operation failed, so get all errors  
                String errMsg = dr.getErrors()[0].getMessage();
                FailedRecords = FailedRecords +dr.getId()+','+errMsg+'\n';
            }
        }
    }
    global void finish(database.batchablecontext bc){
        String subject = 'Rollout Data Delete Batch Result';
        String body = 'Your batch job '+ '"RolloutDataDeleteBatch" '+ 'for Opportunities with Satgename:'+OppStagename+' and Closedate on or before Year:'+CloseDateYear+' has been finished. \n' + 'Total Records Processed:'+TotalRolloutRecords+' records. \nPlease find the Success & Error list attached';
        
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage(); 
     
        Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
        efa.setFileName('Rollout_Data_Delete_Failed.csv');
        efa.setBody(Blob.valueOf(FailedRecords));
        
        Messaging.EmailFileAttachment efa2 = new Messaging.EmailFileAttachment();
        efa2.setFileName('Rollout_Data_Delete_Success.csv');
        efa2.setBody(Blob.valueOf(SuccessRecords));
        
        email.setSubject( subject );
        email.setToAddresses( new String[] {'t.gandru@partner.samsung.com'} );
        email.setPlainTextBody( body );
        email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa,efa2});

        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email}); 
    }
}