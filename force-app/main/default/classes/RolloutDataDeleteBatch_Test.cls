@istest
public class RolloutDataDeleteBatch_Test {
    @istest static void OppsToProcessInBatch(){
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = rtMap.get('Account' + 'HA_Builder'),
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );
        insert testAccount1;

        Opportunity testOpp1 = new Opportunity (
            Name = 'TestOpp1',
            RecordTypeId = rtMap.get('Opportunity' + 'HA_Builder'),
            Project_Type__c = 'SFNC',
            Project_Name__c = 'TestOpp1',
            AccountId = testAccount1.Id,
            StageName = 'Identification',
            CloseDate = Date.today(),
            Roll_Out_Start__c = Date.today(),
            Rollout_Duration__c = 5

        );
        insert testOpp1;

        Roll_Out__c testRo = new Roll_Out__c (
            Opportunity__c = testOpp1.Id,
            Opportunity_RecordType_Name__c = 'HA Builder',
            Roll_Out_Start__c = Date.today()
        );
        insert testRo;
        integer yr = System.Today().year();
        RolloutDataDeleteBatch ba = new RolloutDataDeleteBatch('Identification',yr);
        database.executeBatch(ba);
    }
}