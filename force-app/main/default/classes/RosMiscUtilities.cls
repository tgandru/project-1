/*
* New Class to create Roll out schedules as per the Roll out plan start date and end date
* All the core data of fiscal weeks for the years 2007 to 2040 is stored in custom settings "FiscalCalendar"
* Author :- Jagan
*/

public class RosMiscUtilities{
    
    //Get the whole calendar at once
    public static List<FiscalCalendar__c> lstFiscCal = FiscalCalendar__c.getall().values();
    
    //This method is called from RollOutProductTriggerHandler class
    //This will return the fiscal week number for a given date
    public static integer giveFiscalWeek(date d){
        
        Date weekbegindate = getWeekBeginDate(d);
        Map<Date,FiscalCalendar__c> fiscalWeekMap = new Map<Date,FiscalCalendar__c>();
        //List<FiscalCalendar__c> lstFiscCal = FiscalCalendar__c.getall().values();
        System.debug('Total fiscal week calendar size...'+lstFiscCal.size());
        for(FiscalCalendar__c fc: lstFiscCal)
        fiscalWeekMap.put(fc.WeekStartDate__c,fc);
        
        if(fiscalWeekMap.containsKey(weekbegindate))
            return integer.valueof(fiscalWeekMap.get(weekbegindate).FiscalWeek__c);
        else
            return null;
    }
    
    //This method is called from RollOutProductTriggerHandler class
    //This will return the week start date for a given week number in a given year
    public static Date giveDateForFiscalWeek(String weeknoplusYear){
        
        Map<String,FiscalCalendar__c> fiscalWeekMap = new Map<String,FiscalCalendar__c>();
        //List<FiscalCalendar__c> lstFiscCal = FiscalCalendar__c.getall().values();
        
        for(FiscalCalendar__c fc: lstFiscCal)
        fiscalWeekMap.put(fc.FiscalWeek__c+fc.FiscalYear__c,fc);
        
        if(fiscalWeekMap.containsKey(String.valueof(weeknoplusYear)))
            return fiscalWeekMap.get(String.valueof(weeknoplusYear)).WeekStartDate__c;
        else
            return null;
    }
    
    //This method is called from RollOutProductTriggerHandler class
    //This will return the fiscal month for a given fiscal week in a given year
    public static String giveFiscalMonth(String weeknoplusYear){
        
        Map<String,FiscalCalendar__c> fiscalWeekMap = new Map<String,FiscalCalendar__c>();
        
        for(FiscalCalendar__c fc: lstFiscCal)
        fiscalWeekMap.put(fc.FiscalWeek__c+fc.FiscalYear__c,fc);
        
        if(fiscalWeekMap.containsKey(String.valueof(weeknoplusYear)))
            return fiscalWeekMap.get(String.valueof(weeknoplusYear)).FiscalMonth__c;
        else
            return null;
    }
    
    /** Calculation to get the start of the week **/
    public static Date getWeekBeginDate(Date startorEndDate){
        
        //Standard date for calculation
        Date stndDate = date.newInstance(1900, 1, 7);
        
        //Calculate "begin date" of the week for the given date
        Integer stdf = stndDate.daysBetween(startorEndDate);
        Integer stdfmod = Math.MOD( stdf, 7);   
        //This will give start date as sunday
        Date startDate = startorEndDate-(stdfmod);
        //so if the start date is same as given date, we should take the previous
        //monday as start date, else, next day as start date
        if(startDate == startorEndDate)
            return (startorEndDate - 6);
        else
            return (startDate + 1);
    }
    
    //This method is called from RollOutProductTriggerHandler class
    public static list<Roll_Out_Schedule__c> createROS(Map<Id, Roll_Out_Product__c> newROPMap) {
        
        List<Roll_Out_Schedule__c> insertRollOutSchedules = new List<Roll_Out_Schedule__c>();
        
        //All fiscal dates are stored in the FiscalCalendar__c custom settings 
        //List<FiscalCalendar__c> lstFiscCal = FiscalCalendar__c.getall().values();
        
        Map<Date,FiscalCalendar__c> fiscalWeekMap = new Map<Date,FiscalCalendar__c>();
        Map<Date,FiscalCalendar__c> OrderedfiscalWeekMap = new Map<Date,FiscalCalendar__c>();
        Map<Id,Integer> ropIdToFiscalWeekMap = new Map<Id,Integer>();
        
        //List to sort the dates by fiscal week begin date
        List<Date> sortLst = new List<Date>();
        //Loop through all the fiscal weeks (2007 to 2040 and store it in map
        for(FiscalCalendar__c fc: lstFiscCal)
        fiscalWeekMap.put(fc.WeekStartDate__c,fc);
        
        //Add all the keys (begin dates) to the list
        sortLst.addAll(fiscalWeekMap.keyset());
        //Sort the list
        sortLst.sort();
        
        //Loop through the sorted list and put the records ordered by begin date
        for(Date d:sortLst){
            OrderedfiscalWeekMap.put(d,fiscalWeekMap.get(d));
        }
        
        //Calculate the total number of weeks for each roll out product
        for(Roll_Out_Product__c rop: newROPMap.values()){ 
            
            Date startDate = rop.Roll_Out_Start__c;
            Date EndDateCalculated = (rop.Roll_Out_Start__c).addMonths((Integer)rop.Rollout_Duration__c);
            
            Date weekBeginForStartDate = getWeekBeginDate(startDate);
            Date weekBeginforEndDate = getWeekBeginDate(EndDateCalculated);
            
            //Set to prevent duplicates being added to list
            Set<Date> duplicatePrev = new Set<Date>();
            
            Boolean fiscalPeriodStart = FALSE;
            Boolean fiscalPeriodEnd = FALSE;
            
            Integer totalCount = 0;
            //loop over the sorted fiscal week map
            for(Date d : OrderedfiscalWeekMap.keyset()){
                
                //start counting at start of week of the roll out plan start date
                if(d == weekBeginForStartDate){
                    fiscalPeriodStart = TRUE;
                    if(!duplicatePrev.contains(d)){
                        duplicatePrev.add(d);      
                        totalCount++;
                    }               
                }
                //If the date is within in the fiscal period -> keep counting
                if(fiscalPeriodStart == TRUE && fiscalPeriodEnd == FALSE ){
                    if(!duplicatePrev.contains(d)){
                        duplicatePrev.add(d);
                        totalCount++;
                    }
                }
                //Break it at the end
                if(d == weekBeginforEndDate){
                    fiscalPeriodEnd = TRUE;
                    if(!duplicatePrev.contains(d)){
                        duplicatePrev.add(d);
                        totalCount++;
                    }
                    break;
                }
            }
            
            //Total number of fiscal weeks from start to end date for the roll out
            ropIdToFiscalWeekMap.put(rop.Id, totalCount);
        }
        
        for(Roll_Out_Product__c rop: newROPMap.values()){ 
            
            Date startDate = rop.Roll_Out_Start__c;
            Date EndDateCalculated = (rop.Roll_Out_Start__c).addMonths((Integer)rop.Rollout_Duration__c);
            //Set to prevent duplicates being added to list
            Set<Date> duplicatePrev = new Set<Date>();
             
            Integer TotalQty = 0; 
            Integer Totalweeks = 0;
            Integer planQtyCntr = 0;
            Integer equalPlanQnty = 0;
            Integer remainingVal = 0;
            Date weekBeginForStartDate = getWeekBeginDate(startDate);
            Date weekBeginforEndDate = getWeekBeginDate(EndDateCalculated);
            
            
            //if(rop.Quote_Quantity__c != null && ropIdToFiscalWeekMap.get(rop.Id) != 0)
            //planQty = (Integer)rop.Quote_Quantity__c/ropIdToFiscalWeekMap.get(rop.Id);
            
            TotalQty = (Integer)rop.Quote_Quantity__c;
            Totalweeks = ropIdToFiscalWeekMap.get(rop.Id);
            Boolean TwgtQt = FALSE;
            Boolean eqPlnQt = FALSE;
            if(Totalweeks >= TotalQty){
                //totalweeks greater than product quantity
                TwgtQt = TRUE;
            }
            else{
                if(math.mod(TotalQty,Totalweeks) == 0){
                    eqPlnQt = TRUE;
                    equalPlanQnty = TotalQty/Totalweeks;
                }
                else{
                    equalPlanQnty = TotalQty/Totalweeks;
                    remainingVal = math.mod(TotalQty,Totalweeks);
                } 
            }
            system.debug('Start Date........................'+startDate);
            system.debug('End date calculated...............'+EndDateCalculated);
            system.debug('Week Begin for Start Date.........'+weekBeginForStartDate);
            system.debug('Week Begin for End Date...........'+weekBeginforEndDate);
            system.debug('Rop qunatity.......'+rop.Quote_Quantity__c+'......and #of fiscal weeks for this ROP....'+ropIdToFiscalWeekMap.get(rop.Id));
            
            Boolean fiscalPeriodStart = FALSE;
            Boolean fiscalPeriodEnd = FALSE;
            //loop over the sorted fiscal week map
            for(Date d : OrderedfiscalWeekMap.keyset()){
               
                //System.debug('Fiscal week start date..............'+d);
                Integer finalPlanQnt = 0;
                //start week of the roll out plan start date
                if(d == weekBeginForStartDate){
                    System.debug('*****************START OF THE FISCAL WEEK...............'+d);
                    system.debug('First date of the week of Roll out plan start date.......'+weekBeginForStartDate);
                    
                    fiscalPeriodStart = TRUE;
                    if(!duplicatePrev.contains(d)) {
                        planQtyCntr++;
                        if(TwgtQt)
                            finalPlanQnt = 1;
                        else
                            finalPlanQnt = equalPlanQnty;
                            
                        insertRollOutSchedules.add(new Roll_Out_Schedule__c(
                        Roll_Out_Product__c = rop.Id,  
                        Plan_Date__c = d, 
                        fiscal_week__c =  OrderedfiscalWeekMap.get(d).FiscalWeek__c,
                        fiscal_month__c = OrderedfiscalWeekMap.get(d).FiscalMonth__c,
                        year__c= OrderedfiscalWeekMap.get(d).FiscalYear__c,
                        Plan_Quantity__c = finalPlanQnt));
                        duplicatePrev.add(d);
                    }                     
                }
                //if the date is within in the fiscal period then create a new roll out schedule to insert
                if(fiscalPeriodStart == TRUE && fiscalPeriodEnd == FALSE ){
                    System.debug('******INSIDE FISCAL PERIOD..............'+d);
                    if(!duplicatePrev.contains(d)){
                        planQtyCntr++;
                        if(TwgtQt && planQtyCntr < = TotalQty)//if total weeks is greater than quantity
                            finalPlanQnt = 1;//put plan quantity as one until the counter is equal to total quantity
                        else if(TwgtQt)
                            finalPlanQnt = 0;//if the counter runs out, then just keep 0 for all the remaining weeks
                        else{
                            //if quantity is greater than total weeks then slice the total quantity and distribute the slice
                            //between all the weeks, if slices are equal (ex:20%5 = 0) distribute equal slice for each week else
                            //Get the remaining slice (ex: 24%5 = 4) and distribute it equally for the weeks at the end of duration.
                            if(remainingVal != 0){
                                //distribute the mod value equally among the weeks
                                if(math.mod(((TotalWeeks - planQtyCntr)+1),remainingVal) == 0){
                                    finalPlanQnt = equalPlanQnty+1;
                                    remainingVal--;
                                 }
                                 else{
                                     finalPlanQnt = equalPlanQnty;
                                 }
                            }
                            else
                                finalPlanQnt = equalPlanQnty;
                                
                            /*if(planQtyCntr == Totalweeks)
                                finalPlanQnt = equalPlanQnty+remainingVal ;
                            else
                                finalPlanQnt = equalPlanQnty;*/
                        }    
                        insertRollOutSchedules.add(new Roll_Out_Schedule__c(
                        Roll_Out_Product__c = rop.Id,  
                        Plan_Date__c = d, 
                        fiscal_week__c = OrderedfiscalWeekMap.get(d).FiscalWeek__c,
                        fiscal_month__c = OrderedfiscalWeekMap.get(d).FiscalMonth__c,
                        year__c= OrderedfiscalWeekMap.get(d).FiscalYear__c,
                        Plan_Quantity__c = finalPlanQnt));
                        duplicatePrev.add(d);
                    }
                }
                
                if(d == weekBeginforEndDate){
                    System.debug('*****************END OF THE FISCAL WEEK...............'+d);
                    system.debug('First date of the week of Roll out plan end date.......'+weekBeginForEndDate);
                    fiscalPeriodEnd = TRUE;
                    break;
                }
            }
        }
        
        system.debug('Total number of roll out schedules to be inserted.........'+insertRollOutSchedules.size());
        return insertRollOutSchedules;
    }
    
    // Utility Method to update quantity on Roll Out Schedule records (Jagan:- This method is copied as it is from old class ( MiscUtilities )
    public static list<Roll_Out_Schedule__c> updateROS (set<Id> ropsWithUpdatedQty) {
        
        //Method variables
        Map<Roll_Out_Product__c,List<Roll_Out_Schedule__c>> ropToROS = new Map<Roll_Out_Product__c,List<Roll_Out_Schedule__c>>();  
        Map<Id,Integer> ropsIdToPlanQty = new Map<Id,Integer>();
        Map<Id,Integer> ropsToChildROSCount = new Map<Id,Integer>(); 
        List<Roll_Out_Schedule__c> rosToUpdate = new List<Roll_Out_Schedule__c>();  
        
        Integer tempQty = 0; 
        Integer tempMod = 0;
        Integer childCount = 0;
        Integer tempROPSPlanQty = 0;
        
        
        for(Roll_Out_Product__c rops: [select Id, Quote_Quantity__c,Number_of_Roll_Out_Schedules__c , (select Id, Do_Not_Edit__c, plan_Quantity__c, Actual_Quantity__c from Roll_Out_Schedules__r) 
        from Roll_Out_Product__c where Id IN:ropsWithUpdatedQty]){
            childCount = 0;
            for(Roll_Out_Schedule__c ros: rops.Roll_Out_Schedules__r){
                
                if(ros.Do_Not_Edit__c){
                    System.debug('## Entered Do no Edit check :: ChildCount ::'+childCount);
                    childCount += 1; 
                    tempROPSPlanQty = (Integer)rops.Quote_Quantity__c -(Integer)ros.Actual_Quantity__c;
                    System.debug('Plan quantity = quoted - actual' + tempROPSPlanQty );
                    ropsIdToPlanQty.put(rops.Id,tempROPSPlanQty);
                    ropsToChildROSCount.put(rops.Id,childCount); 
                }
                else {
                    System.debug('## Entered Do no Edit check FALSE :: ChildCount ::'+childCount);
                    if(ropsIdToPlanQty.get(rops.Id)==null) {//check null
                        ropsIdToPlanQty.put(rops.Id,(Integer)rops.Quote_Quantity__c);
                    }
                    if(!ropToROS.containsKey(rops)){
                        ropToROS.put(rops,new List<Roll_Out_Schedule__c>());
                    }
                    ropToROS.get(rops).add(ros);
                    ropsToChildROSCount.put(rops.Id,childCount);
                    System.debug('## Get ropsToChildROSCount'+ropsToChildROSCount);
                }
                
            }
            
        }   
        
        if(ropToROS.size()>0) { 
            for(Roll_Out_Product__c p: ropToROS.keySet()){
                System.debug('No of roll out schedule '+ p + p.Number_of_Roll_Out_Schedules__c );
                if(Math.mod(ropsIdToPlanQty.get(p.Id),(Integer)(p.Number_of_Roll_Out_Schedules__c - ropsToChildROSCount.get(p.Id))) == 0){  
                    tempMod = 0;        
                    tempQty = (Integer)(ropsIdToPlanQty.get(p.Id)/(p.Number_of_Roll_Out_Schedules__c - ropsToChildROSCount.get(p.Id))); 
                    System.debug('## Inside If'+tempQty );        
                }

                else {
                    tempQty = (Integer)(ropsIdToPlanQty.get(p.Id)/(p.Number_of_Roll_Out_Schedules__c - ropsToChildROSCount.get(p.Id)));
                    tempMod = Math.mod(ropsIdToPlanQty.get(p.Id),(Integer)(p.Number_of_Roll_Out_Schedules__c - ropsToChildROSCount.get(p.Id)));
                    System.debug('## Inside else ::tempQty ::tempMod '+tempQty +'::'+tempMod ); 
                }
                
                Integer i = 0; 
                for(Roll_Out_Schedule__c r: ropToROS.get(p)) { 
                    System.debug('Child count' + ropsToChildROSCount.get(p.Id));
                    //Consider ONLY ROS records with no actuals posted ie.subtract ropsToChildROSCount.get(p.Id) count
                    // Commented and Updated below qty distribution on if and else block on 05/18/16
                    if(i< tempMod) {
                        r.Plan_Quantity__c = tempQty +1; 
                        System.debug('## IF block tempqty'+i);     
                    }
                    else {
                        r.Plan_Quantity__c = tempQty;
                        System.debug('## else block tempqty'+i);               
                    }                   
                    rosToUpdate.add(r);
                    i++;
                }       
            }
        }
        return rosToUpdate;
    }

}