public class SBQQ_QuoteCreationController {
/*
Custom Controller to Fill Opportunity Information on New Quote Creation Page From Opportunity Related List

Standard CPQ new Screen is not Supporting in Lightning. 


*/
     @AuraEnabled
    public static Opportunity getOpportunityInfo(String oppId){
        List<Opportunity> oppList = [Select id,Name,AccountId from Opportunity where id=:oppId];
        Opportunity opp;
        if(oppList.size()>0){
            opp = oppList[0];
        }else{
            opp = new Opportunity();
        }
        return opp;
    }


}