public class SBQQ_QuoteLineTriggerHandler {
    
    public static boolean createRolloutProductCheck = FALSE;
    public static boolean updateRolloutProductCheck = FALSE;
    
    public static void createRolloutProduct(List<SBQQ__QuoteLine__c> newList) {
        if(!createRolloutProductCheck){ 
            createRolloutProductCheck = TRUE;
            System.debug('## In HA_Rollout Product create block');
            
            List<Roll_Out_Product__c> insertRolloutProducts = new List<Roll_Out_Product__c>();
            Map<Id,SBQQ__Quote__c> quoteMap = new Map<Id,SBQQ__Quote__c>();
            Map<String, Integer> cntMap    = new Map<String, Integer>();
            Map<String, Integer> remainCntMap = new Map<String, Integer>();
            Map<String, String>  familyBaseMap = new Map<String, String>();
            
            Set<String> quoteIds = new Set<String>();
            for(SBQQ__QuoteLine__c ql : newList){
                quoteIds.add(ql.SBQQ__Quote__c);
            }
            for(SBQQ__Quote__c qt: [Select id, 
                                          SBQQ__Opportunity2__c,
                                          SBQQ__Opportunity2__r.Roll_Out_Plan__c,
                                          SBQQ__Opportunity2__r.Roll_Out_Start__c,
                                          SBQQ__Opportunity2__r.Rollout_Duration__c,
                                          SBQQ__Opportunity2__r.Recordtype.Name,
                                          SBQQ__Opportunity2__r.Project_Type__c,
                                          SBQQ__Opportunity2__r.Number_of_Living_Units__c,
                                          SBQQ__Opportunity2__r.Roll_Out_Plan__r.Quote__c 
                                          from SBQQ__Quote__c 
                                          where id=:quoteIds and SBQQ__Primary__c=true]){
                quoteMap.put(qt.Id, qt);
            }
            
            Map<String, Decimal> prType = projectTypeDataMap();
            
            //Confirm the mapping
            for(SBQQ__QuoteLine__c sb : newList){
                if(sb.type__c!='Dacor' && quoteMap.containsKey(sb.SBQQ__Quote__c)){
                    SBQQ__Quote__c qt = quoteMap.get(sb.SBQQ__Quote__c);
                    if(qt.SBQQ__Opportunity2__r.Roll_Out_Plan__c!=null && sb.SBQQ__Quote__c==qt.SBQQ__Opportunity2__r.Roll_Out_Plan__r.Quote__c){
                        Roll_Out_Product__c rop = new Roll_Out_Product__c();
                        
                        rop.Product__c = sb.SBQQ__Product__c;
                        rop.Roll_Out_Plan__c = qt.SBQQ__Opportunity2__r.Roll_Out_Plan__c;
                        rop.Roll_Out_Start__c = qt.SBQQ__Opportunity2__r.Roll_Out_Start__c;
                        rop.Rollout_Duration__c = qt.SBQQ__Opportunity2__r.Rollout_Duration__c;
                        rop.Opportunity_RecordType_Name__c = qt.SBQQ__Opportunity2__r.Recordtype.Name;
                        rop.Product_Family__c = sb.SBQQ__ProductFamily__c;
                        rop.Package_Option__c = sb.Package_Option__c;
                        rop.Project_Type__c = qt.SBQQ__Opportunity2__r.Project_Type__c;
                        rop.SBQQ_QuoteLine__c = sb.Id;
                        rop.Quote_Quantity__c = sb.SBQQ__Quantity__c;
                        if(rop.Package_Option__c == 'Option' || rop.Package_Option__c == 'Upgrade') rop.Quote_Quantity__c =qt.SBQQ__Opportunity2__r.Number_of_Living_Units__c;
                        
                        if(prType.get(rop.Project_Type__c +':'+rop.Package_Option__c) != null && rop.Package_Option__c == 'Base'){
                        
                            //familyBaseMap.put(sb.SBQQ__ProductFamily__c, sb.SBQQ__ProductFamily__c);
                              
                            rop.Roll_Out_Percent__c = prType.get(rop.Project_Type__c +':'+rop.Package_Option__c);
                            rop.Quote_Quantity__c = Math.Round(sb.SBQQ__Quantity__c *(rop.Roll_Out_Percent__c/100.00));
                        }
                        if(rop.Package_Option__c == 'Option' || rop.Package_Option__c == 'Upgrade'){
                        
                            rop.Roll_Out_Percent__c = 0;
                            rop.Quote_Quantity__c = Math.Round(sb.SBQQ__Quantity__c *(rop.Roll_Out_Percent__c/100.00));
                            //if(rop.Quote_Quantity__c==0) rop.Quote_Quantity__c = 1;
                            
                            /*if(rop.Product_Family__c != null && cntMap.containsKey(rop.Product_Family__c)){
                            cntMap.put(rop.Product_Family__c, Integer.valueOf(cntMap.get(rop.Product_Family__c)+1));
                            }
                            if(rop.Product_Family__c != null && !cntMap.containsKey(rop.Product_Family__c)){
                            cntMap.put(rop.Product_Family__c, 1);
                            }*/
                        }
                        rop.SalesPrice__c = sb.SBQQ__NetPrice__c;
                        insertRolloutProducts.add(rop);
                    }
                }
            }
            
            /*for(Roll_Out_Product__c rop : insertRolloutProducts){
                if(rop.Product_Family__c != null && rop.Package_Option__c != null &&rop.Package_Option__c != 'Base' && cntMap.get(rop.Product_Family__c) != null){
                
                if(remainCntMap.containsKey(rop.Product_Family__c)){
                    remainCntMap.put(rop.Product_Family__c, Integer.valueOf(remainCntMap.get(rop.Product_Family__c)+1));
                }
                if(!remainCntMap.containsKey(rop.Product_Family__c)){
                    remainCntMap.put(rop.Product_Family__c, 1);
                }
                
                if(prType.get(rop.Project_Type__c+':Base') != null && familyBaseMap.get(rop.Product_Family__c) != null){
                    rop.Roll_Out_Percent__c = Math.floor((100.00 - prType.get(rop.Project_Type__c+':Base')) / cntMap.get(rop.Product_Family__c));
                    if(cntMap.get(rop.Product_Family__c) == remainCntMap.get(rop.Product_Family__c) && Math.mod((100 - Integer.valueOf(prType.get(rop.Project_Type__c+':Base'))), cntMap.get(rop.Product_Family__c)) != 0)
                        rop.Roll_Out_Percent__c = rop.Roll_Out_Percent__c + (Math.mod(100 - Integer.valueOf(prType.get(rop.Project_Type__c+':Base')), cntMap.get(rop.Product_Family__c)));
                }
                else{
                    rop.Roll_Out_Percent__c = Math.floor(100.00 / cntMap.get(rop.Product_Family__c));
                    if(cntMap.get(rop.Product_Family__c) == remainCntMap.get(rop.Product_Family__c) && Math.mod(100, cntMap.get(rop.Product_Family__c)) != 0)
                        rop.Roll_Out_Percent__c = rop.Roll_Out_Percent__c + Math.mod(100, cntMap.get(rop.Product_Family__c));
                }
                if(rop.Roll_Out_Percent__c == null) rop.Roll_Out_Percent__c = 100.00;
                    rop.Quote_Quantity__c = Math.Round(rop.Quote_Quantity__c *(rop.Roll_Out_Percent__c/100.00));
                }
                rop.Quote_Revenue__c = rop.Quote_Quantity__c * rop.SalesPrice__c;
            }*/
              
            try{
                if(insertRolloutProducts.size()>0) insert insertRolloutProducts;
            }
            catch(Exception e){
                  system.debug('## insertHA_RolloutProducts'+e);
            }
        }
  }
  
  public static void updateRolloutProduct(Map<id,SBQQ__QuoteLine__c> qliNewMap,Map<id,SBQQ__QuoteLine__c> qliOldMap) {
    if(!updateRolloutProductCheck){ 
        updateRolloutProductCheck = TRUE;
        System.debug('## In HA_Rollout Product update block');
        
        List<Roll_Out_Product__c> updateRolloutProducts = new List<Roll_Out_Product__c>();
        Map<String,Roll_Out_Product__c> qliRopMap = new Map<String,Roll_Out_Product__c>();
        Map<String, Integer> cntMap    = new Map<String, Integer>();
        Map<String, Integer> remainCntMap = new Map<String, Integer>();
        Map<String, String>  familyBaseMap = new Map<String, String>();
        Set<id> ropIds = new Set<id>();
        
        for(Roll_Out_Product__c rop : [Select id,
                                              Package_Option__c,
                                              Quote_Quantity__c,
                                              Project_Type__c,
                                              Roll_Out_Percent__c,
                                              SalesPrice__c,
                                              Product_Family__c,
                                              Quote_Revenue__c,
                                              SBQQ_QuoteLine__c, 
                                              Roll_Out_Plan__r.Opportunity__r.Number_of_Living_Units__c 
                                              From Roll_Out_Product__c
                                              where SBQQ_QuoteLine__c=:qliNewMap.keySet()]){
            qliRopMap.put(rop.SBQQ_QuoteLine__c,rop);
        }
        
        Map<String, Decimal> prType = projectTypeDataMap();
        
        for(SBQQ__QuoteLine__c sb : qliNewMap.values()){
          
            if(qliRopMap.containsKey(sb.id)){
                Roll_Out_Product__c rop = qliRopMap.get(sb.id);
                
                rop.Package_Option__c = sb.Package_Option__c;
                rop.Quote_Quantity__c = sb.SBQQ__Quantity__c;
                
                if(rop.Package_Option__c == 'Option' || rop.Package_Option__c == 'Upgrade') rop.Quote_Quantity__c =rop.Roll_Out_Plan__r.Opportunity__r.Number_of_Living_Units__c;
                
                if(prType.get(rop.Project_Type__c +':'+rop.Package_Option__c) != null && rop.Package_Option__c == 'Base'){
                                      
                    //rop.Roll_Out_Percent__c = prType.get(rop.Project_Type__c +':'+rop.Package_Option__c);
                    rop.Quote_Quantity__c = Math.Round(sb.SBQQ__Quantity__c *(rop.Roll_Out_Percent__c/100.00));
                }
                if(rop.Package_Option__c == 'Option' || rop.Package_Option__c == 'Upgrade'){
                    rop.Quote_Quantity__c = Math.Round(rop.Quote_Quantity__c *(rop.Roll_Out_Percent__c/100.00));
                }
                rop.SalesPrice__c = sb.SBQQ__NetPrice__c;
                updateRolloutProducts.add(rop);
                ropIds.add(rop.id);
            }
        }
        
        try{
            if(updateRolloutProducts.size()>0) {
                update updateRolloutProducts;
                
                List<Roll_Out_Schedule__c> rosList = [Select id from Roll_Out_Schedule__c where Roll_Out_Product__c=:ropIds];
                
                if(rosList.size()>0){
                    delete rosList;
                }
                
                HA_RollOutSchedulesBatch ba = new HA_RollOutSchedulesBatch(ropIds);
                Database.executeBatch(ba, 30);
            }
        }
        catch(Exception e){
              system.debug('## Update HA_RolloutProducts'+e);
        }
    }
  }
  
  public static void deleteRolloutProduct(Map<id,SBQQ__QuoteLine__c> qliMap) {
      system.debug('Trigger.oldMap---->'+qliMap);
    List<Roll_Out_Product__c> ROPlist = [Select id from Roll_Out_Product__c where SBQQ_QuoteLine__c=:qliMap.keySet()];
    if(ROPlist.size()>0){
        delete ROPlist;
    }
  }
  
  /*public static void processRolloutProducts(List<Roll_Out_Product__c> ropList,Map<String, Decimal> prType,Map<String, Integer> cntMap,Map<String, Integer> remainCntMap,Map<String, String>  familyBaseMap){
        for(Roll_Out_Product__c rop : ropList){
          if(rop.Product_Family__c != null && rop.Package_Option__c != null &&rop.Package_Option__c != 'Base' && cntMap.get(rop.Product_Family__c) != null){

            if(remainCntMap.containsKey(rop.Product_Family__c)){
            remainCntMap.put(rop.Product_Family__c, Integer.valueOf(remainCntMap.get(rop.Product_Family__c)+1));
        }
        if(!remainCntMap.containsKey(rop.Product_Family__c)){
          remainCntMap.put(rop.Product_Family__c, 1);
        }
            
            if(prType.get(rop.Project_Type__c+':Base') != null && familyBaseMap.get(rop.Product_Family__c) != null){
              rop.Roll_Out_Percent__c = Math.floor((100.00 - prType.get(rop.Project_Type__c+':Base')) / cntMap.get(rop.Product_Family__c));
              if(cntMap.get(rop.Product_Family__c) == remainCntMap.get(rop.Product_Family__c) && Math.mod((100 - Integer.valueOf(prType.get(rop.Project_Type__c+':Base'))), cntMap.get(rop.Product_Family__c)) != 0)
              rop.Roll_Out_Percent__c = rop.Roll_Out_Percent__c + (Math.mod(100 - Integer.valueOf(prType.get(rop.Project_Type__c+':Base')), cntMap.get(rop.Product_Family__c)));
            }
            else{
              rop.Roll_Out_Percent__c = Math.floor(100.00 / cntMap.get(rop.Product_Family__c));
              if(cntMap.get(rop.Product_Family__c) == remainCntMap.get(rop.Product_Family__c) && Math.mod(100, cntMap.get(rop.Product_Family__c)) != 0)
                rop.Roll_Out_Percent__c = rop.Roll_Out_Percent__c + Math.mod(100, cntMap.get(rop.Product_Family__c));
            }
            if(rop.Roll_Out_Percent__c == null) rop.Roll_Out_Percent__c = 100.00;
            rop.Quote_Quantity__c = Math.Round(rop.Quote_Quantity__c *(rop.Roll_Out_Percent__c/100.00));
          }
          rop.Quote_Revenue__c = rop.Quote_Quantity__c * rop.SalesPrice__c;
        }
    }*/
    
    public static Map<String, Decimal> projectTypeDataMap() {
        Map<String, Decimal> prTypeMap = new Map<String, Decimal>();
          List<ProjectType__c> prTypeData = ProjectType__c.getall().values();
        for(ProjectType__c pt : prTypeData){
          prTypeMap.put(pt.Name, pt.Percent__c);
        }
        system.debug('prType' + prTypeMap.size());
        return prTypeMap;
    }
    
    //06-26-2019..Thiru::Added below Method to prevent duplicate products adding to HA Quote
    public static void checkDuplicateSKUs(List<SBQQ__QuoteLine__c> triggerNew){
    	Set<id> QuoteIds = new Set<id>();
    	for(SBQQ__QuoteLine__c qli : triggerNew){
    		QuoteIds.add(qli.SBQQ__Quote__c);
    	}
    	
    	Map<String,Set<String>> duplicateSKUsMap = new Map<String,Set<String>>();
    	
    	for(SBQQ__Quote__c qt : [Select id, Name, (Select id,SBQQ__Quote__c,SBQQ__Product__c,SBQQ__Product__r.SAP_Material_Id__c from SBQQ__Lineitems__r) from SBQQ__Quote__c where id=:QuoteIds]){
    		Set<String> qtProds = new Set<String>();
    		for(SBQQ__QuoteLine__c qli : qt.SBQQ__Lineitems__r){
    			if(qtProds.contains(qli.SBQQ__Product__r.SAP_Material_Id__c)){
    				Set<String> dupProdSet = new Set<String>();
    				if(duplicateSKUsMap.containsKey(qli.SBQQ__Quote__c)){
    					dupProdSet = duplicateSKUsMap.get(qli.SBQQ__Quote__c);
    				}
    				dupProdSet.add(qli.SBQQ__Product__r.SAP_Material_Id__c);
    				duplicateSKUsMap.put(qli.SBQQ__Quote__c,dupProdSet);
    			}
    			qtProds.add(qli.SBQQ__Product__r.SAP_Material_Id__c);
    		}
    	}
    	
    	if(duplicateSKUsMap.size()>0){
    		for(SBQQ__QuoteLine__c line : triggerNew){
    		    if(duplicateSKUsMap.containsKey(line.SBQQ__Quote__c)){
    		        system.debug('duplicate SKUs----->'+line.SBQQ__Quote__c+' - '+duplicateSKUsMap.get(line.SBQQ__Quote__c));
    		        line.addError('This Quote has duplicates products. Remove the following duplicates to Save this Quote. '+duplicateSKUsMap.get(line.SBQQ__Quote__c));
    		    }
    		}
    	}
    }
}