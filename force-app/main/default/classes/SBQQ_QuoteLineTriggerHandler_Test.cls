@istest
public class SBQQ_QuoteLineTriggerHandler_Test {
    @isTest(SeeAllData=true)
    static void CreateTest() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        
        List<SBQQ__Quote__c> sbqtList = [Select id,SBQQ__Primary__c,SBQQ__Opportunity2__c,SBQQ__Account__c,SBQQ__Type__c,SBQQ__Status__c from SBQQ__Quote__c where SBQQ__Status__c='Approved' limit 1];
        SBQQ__Quote__c sbq1 = sbqtList[0];
        
        List<Product2> prodList = [SELECT Id FROM Product2 WHERE Family =: 'DISH' LIMIT 5];

        List<SBQQ__QuoteLine__c> sbqList = new List<SBQQ__QuoteLine__c>();

        for(integer i = 0; i < 5; i++){
            SBQQ__QuoteLine__c sbq = new SBQQ__QuoteLine__c();
            sbq.SBQQ__Product__c = prodList[i].Id;
            if(i ==0) sbq.Package_Option__c = 'Base';
            else sbq.Package_Option__c = 'Option';
            sbq.SBQQ__Quantity__c = 50;
            sbq.SBQQ__Quote__c = sbq1.Id;
            sbq.SBQQ__ListPrice__c = 5000;
            sbq.SBQQ__NetPrice__c = 4950;
            sbqList.add(sbq);
        }
        insert sbqList;
        
        SBQQ__QuoteLine__c qli = sbqList[0];
        qli.SBQQ__Quantity__c = 40;
        update qli;
    }
}