/**
 * Created by ms on 2017-10-25.
 *
 * author : JeongHo.Lee, I2MAX
 */
public class SBQQ_QuoteTriggerHandler {

	public static Boolean isFirstTime = true;

	public static void rolloutCreate(List<SBQQ__Quote__c> newList, Map<Id, SBQQ__Quote__c> newQMap, Map<Id, SBQQ__Quote__c> oldQMap) {

		Set<Id> 			approveSet			= new Set<Id>();
		list<Roll_Out__c> 	newRollOuts 		= new list<Roll_Out__c>();
		Set<Id> 			toUpdateOpps 		= new Set<Id>();
		Set<Id>				opptySet			= new Set<Id>();
		List<Opportunity>	opptyToUpdateList	= new List<Opportunity>();
		
		//Thiru: Added this logic to stop Rollout Reset for Re-Approving Quotes--Starts Here--04/10/2019
		Map<String,Integer> QtApprovalCountMap = new Map<String,Integer>();
		for(SBQQ__Quote__c qt : newList){
		    if(qt.SBQQ__Status__c == 'Approved' && oldQMap.get(qt.Id).SBQQ__Status__c != 'Approved'){
		        QtApprovalCountMap.put(qt.Id,0);
		    }
		}
		for(ProcessInstance pi : [SELECT Id,
		                                 Status,
		                                 TargetObjectId,
		                                 TargetObject.name 
		                                 FROM ProcessInstance 
		                                 where TargetObjectId=:newQMap.keySet() and status='Approved']){
		    integer count = 0;
		    if(QtApprovalCountMap.containsKey(pi.TargetObjectId)){
		        count = QtApprovalCountMap.get(pi.TargetObjectId);
		        count++;
		        QtApprovalCountMap.put(pi.TargetObjectId, count);
		    }else{
		        count++;
		        QtApprovalCountMap.put(pi.TargetObjectId, count);
		    }
		}
		//Thiru: Added this IF condition to stop Rollout Reset for Re-Approving Quotes--Ends Here
		
		for(SBQQ__Quote__c sb : newList){
		    
		    if(QtApprovalCountMap.containsKey(sb.id) && QtApprovalCountMap.get(sb.id)<=1){ //Thiru: Added this IF condition to stop Rollout Reset for Re-Approving Quotes
    			system.debug('******Create Rollout******');
    			if(sb.SBQQ__Status__c == 'Approved' && oldQMap.get(sb.Id).SBQQ__Status__c != 'Approved'){
    				approveSet.add(sb.Id);
    				opptySet.add(sb.SBQQ__Opportunity2__c);
    			}
		    }
		}
		if(!opptySet.isEmpty()){
		    
		    //07-02-2019..Added by Thiru--Starts..To fix new Rollouts Deletion issue
    	    Set<id> existingRollouts = new Set<id>();
    	    for(Roll_Out__c rollout : [Select Id from Roll_Out__c where Opportunity__c=:opptySet]){
    	        existingRollouts.add(rollout.Id);
    	    }
    		if(existingRollouts.size()>0){
    		    HA_RollOutDeleteBatch ba = new HA_RollOutDeleteBatch(existingRollouts);
        	    Database.executeBatch(ba, 1);
    		}
    		//07-02-2019..Added by Thiru--Ends
			
			//HA_RollOutDeleteBatch ba = new HA_RollOutDeleteBatch(opptySet);
        	//Database.executeBatch(ba, 1);
		}

		if(!approveSet.isEmpty()){
			for(SBQQ__Quote__c sb : [SELECT Id
										, Name
										, SBQQ__Opportunity2__c
										, SBQQ__Opportunity2__r.Opportunity_Number__c
										, SBQQ__Opportunity2__r.Roll_Out_Start__c
										, SBQQ__Opportunity2__r.Rollout_Duration__c
										, SBQQ__Opportunity2__r.Roll_Out_Status__c
										, SBQQ__Opportunity2__r.RecordType.Name
										FROM SBQQ__Quote__c
										WHERE SBQQ__Opportunity2__r.Roll_Out_Start__c != NULL
										AND SBQQ__Opportunity2__r.Rollout_Duration__c != NULL
										AND Id IN: approveSet]){ 
				newRollOuts.add(
					new Roll_Out__c(
							Name 							= sb.SBQQ__Opportunity2__r.Opportunity_Number__c+ '- Rollout',
							Opportunity__c 					= sb.SBQQ__Opportunity2__c,
						 	Roll_Out_Start__c 				= sb.SBQQ__Opportunity2__r.Roll_Out_Start__c,
						 	Rollout_Duration__c 			= sb.SBQQ__Opportunity2__r.Rollout_Duration__c, 
						 	Roll_Out_Status__c 				= sb.SBQQ__Opportunity2__r.Roll_Out_Status__c,
						 	Opportunity_RecordType_Name__c	= sb.SBQQ__Opportunity2__r.RecordType.Name,
						 	Quote__c 						= sb.Id ));
						 	    
	            toUpdateOpps.add(sb.SBQQ__Opportunity2__c);
			}

			if(!newRollOuts.isEmpty()){           
	            System.debug('##JeongHo Insert newRollOuts'+newRollOuts.size()+'::'+newRollOuts);
	            insert newRollOuts;
	        }

	        for(Opportunity o: [Select Id,Roll_Out_Plan__c from Opportunity where Id IN:toUpdateOpps]) {
	            for(Roll_Out__c r: newRollOuts){
	                if(o.Id == r.Opportunity__c){
	                	o.Roll_Out_Plan__c = r.Id;
	                	opptyToUpdateList.add(o);
	            	}
	            }
	         }
	         
	        if(!opptyToUpdateList.isEmpty()){
	            update opptyToUpdateList;
	        }
	    }
	}
	/*public static void rolloutDelete(Map<Id, SBQQ__Quote__c> oldQMap) {
		//RollOut Delete, OpportunityItem Delete
		List<Roll_Out__c> roDeleteList = new List<Roll_Out__c>(); 
		Set<Id> oppIds = new Set<Id>();

		for(Roll_Out__c ro : [SELECT Id, Opportunity__c FROM Roll_Out__c WHERE Quote__c IN: oldQMap.keySet()]){
			roDeleteList.add(ro);
			oppIds.add(ro.Opportunity__c);
		}

		List<OpportunityLineItem> oppDeleteList = [SELECT Id FROM OpportunityLineItem WHERE OpportunityId IN: oppIds];
		
		if(!roDeleteList.isEmpty()) delete roDeleteList;
		if(!oppDeleteList.isEmpty()) delete oppDeleteList;
	}*/
	
}