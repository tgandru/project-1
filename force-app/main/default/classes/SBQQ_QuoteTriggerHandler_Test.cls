/**
 * Created by ms on 2017-11-12.
 *
 * author : JeongHo.Lee, I2MAX
 */
 @isTest
private class SBQQ_QuoteTriggerHandler_Test {

    @isTest static void CreateTest() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = rtMap.get('Account' + 'HA_Builder'),
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );
        insert testAccount1;

        Opportunity testOpp1 = new Opportunity (
            Name = 'TestOpp1',
            RecordTypeId = rtMap.get('Opportunity' + 'HA_Builder'),
            Project_Type__c = 'SFNC',
            Project_Name__c = 'TestOpp1',
            AccountId = testAccount1.Id,
            StageName = 'Identified',
            CloseDate = Date.today(),
            Roll_Out_Start__c = Date.today(),
            Rollout_Duration__c = 5

        );
        insert testOpp1;

        SBQQ__Quote__c sbq1 = new SBQQ__Quote__c (
            SBQQ__Primary__c = TRUE,
            SBQQ__Opportunity2__c = testOpp1.Id,
            SBQQ__Account__c = testAccount1.Id,
            SBQQ__Type__c = 'Quote',
            SBQQ__Status__c = 'Draft'
        );
        insert sbq1;
        
        SBQQ__Quote__c sbq2 = sbq1.clone(true, false, false, false);
        sbq2.SBQQ__Status__c = 'Approved';

        Map<Id, SBQQ__Quote__c> sbOldMap = new Map<Id, SBQQ__Quote__c>();
        sbOldMap.put(sbq1.Id, sbq1);
        
        Map<Id, SBQQ__Quote__c> sbNewMap = new Map<Id, SBQQ__Quote__c>();
        sbNewMap.put(sbq1.Id, sbq2);
 
        List<SBQQ__Quote__c> sbList = new List<SBQQ__Quote__c>();
        sbList.add(sbq2);

        SBQQ_QuoteTriggerHandler.rolloutCreate(sbList, sbNewMap, sbOldMap);
    }
}