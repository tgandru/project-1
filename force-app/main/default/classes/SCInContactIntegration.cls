public with sharing class SCInContactIntegration {
        
    @future (callout=true)
    public static void sendCaseDatatoInContact(List<Id> CaseIDs){
            Boolean switchval =IncontactCalloutSwitch__c.getInstance('Make_Call').TurnOn__c;
            if(switchval==true){
              //String inContactToken = GetinContactToken();
        String inContactToken = '';
        SCinContact_Constant__c inConactSettings = SCinContact_Constant__c.getValues('Samsung incontact settings');
        String pointoOfContact = inConactSettings.Point_of_Contact_ID__c+'-'+inConactSettings.Point_of_Contact_Name__c;
        String apiEndpoint = inConactSettings.API_Endpoint__c;


        String vendorName = inConactSettings.Vendor_Name__c;
        String appName = inConactSettings.Application_Name__c;
        String businessUnit = inConactSettings.Business_Unit_Number__c;
        String apiUserName = inConactSettings.API_User_Name__c;
        String apiUserPassword = inConactSettings.API_User_Password__c;

        Blob authKeyStr = Blob.valueof(appName+'@'+vendorName+':'+businessUnit);
        String authKey = 'basic '+EncodingUtil.base64Encode(authKeyStr);

        HttpRequest tokenRequest = new HttpRequest();
        Http tokenHttp = new Http();
           
        tokenRequest.setMethod('POST');
        tokenRequest.setEndpoint('https://api.incontact.com/InContactAuthorizationServer/Token');
        tokenRequest.setHeader('Authorization', authKey);
        tokenRequest.setHeader('Content-Type', 'application/json');

        String body = '{"grant_type" : "password",';
        body += '"username":"'+apiUserName +'",';
        body += '"password":"'+apiUserPassword +'","scope" : ""}';
        tokenRequest.setBody(body);
        httpResponse tokenResponse = tokenHttp.send(tokenRequest);
        //system.debug(tokenResponse);
        if (tokenResponse.getStatusCode() != 200) {
            System.debug('Error from ' + tokenRequest.getEndpoint() + ' : ' +
                tokenResponse.getStatusCode() + ' ' + tokenResponse.getStatus());
        }else{
            JSONParser parser = JSON.createParser(tokenResponse.getBody());
            while (parser.nextToken() != null) {

                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'access_token')) {
    
                    parser.nextToken();
    
                    inContactToken = parser.getText();
    
                }

            }
   
        }
        system.debug(inContactToken);


        List<Case> cases = [select id,casenumber,contact.name,contact.named_caller__c,account.SC_Service_Type_Ranking__c,
            account.SC_Service_Type__c,contact.Business_Hours__c,account.service_id__c,origin 
            from case where ID IN: CaseIDs  Limit 99];
           // and contact.named_caller__c != false and account.SC_Service_Type_Ranking__c != null Limit 99];

        //system.debug(inContactToken); 

        for (Case c:Cases){

                HttpRequest rq = new HttpRequest();
                Http hp = new Http();
                
                //rq.setMethod('POST');
                //rq.setEndpoint(apiEndpoint);
                //rq.setHeader('Authorization', 'bearer '+inContactToken);
                //rq.setHeader('Content-Type', 'application/json');

                String businessHours =(c.contact.Business_Hours__c == '24x7')?'24x7':'12x5';

                string workItemPayload = '{\\"TicketNumber\\":\\"'+c.casenumber+'\\",';
                workItemPayload += '\\"serviceid\\":\\"'+c.account.service_id__c+'\\",';
                workItemPayload += '\\"servicetype\\":\\"'+c.account.SC_Service_Type__c+'\\",';
                workItemPayload += '\\"coverage\\":\\"'+businessHours+'\\",';
                workItemPayload += '\\"channel\\":\\"'+c.origin+'\\"}';

                        
                body = '{"pointOfContact" : "'+pointoOfContact+'",';
                body += '"workItemId" : "'+c.casenumber+'",';
                body += '"workItemPayload" : "'+workItemPayload+'",';
                body += '"workItemType":"",';
                body += '"from":""}';


                String calloutURL = 'pointOfContact='+pointoOfContact;
                //calloutURL += '&workItemPayload='+workItemPayload;
                //calloutURL = EncodingUtil.urlEncode(calloutURL, 'UTF-8');
                calloutURL = apiEndpoint+'?'+EncodingUtil.urlEncode(calloutURL, 'UTF-8');
                system.debug(calloutURL); 

                rq.setMethod('POST');
                rq.setEndpoint(apiEndpoint);
                rq.setHeader('Authorization', 'bearer '+inContactToken);
                rq.setHeader('Content-Type', 'application/json');
                rq.setHeader('Accept', 'application/json');
                
                //system.debug(body);
                
                rq.setBody(body);
                httpResponse inContactResponse = hp.send(rq);
                
                //system.debug(inContactResponse);
                //system.debug(inContactResponse.getBody());
                /*if(!Test.isRunningTest()){ 
                    Messaging.SingleEmailMessage mail1=new Messaging.SingleEmailMessage();
                    String[] toAddresses1 = new String[] {'david.wang@slalom.com','d.rampelli@sea.samsung.com','kwangseung.l@partner.samsung.com'};
                    mail1.setToAddresses(toAddresses1);
                    mail1.setReplyTo('david.wang@slalom.com');
                    mail1.setSenderDisplayName('InContact Integration');
                    mail1.setSubject('Sent to inContact From Org : ' + UserInfo.getOrganizationName());
                    mail1.setPlainTextBody(body +'\n'+inContactResponse.getBody() +'\n\n'+' Sent to: ' + rq.getEndpoint() + ' : ' +
                        inContactResponse.getStatusCode() + ' ' + inContactResponse.getStatus());
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail1 });
                }*/

                if (inContactResponse.getStatusCode() != 202) {
                    System.debug('Error from ' + rq.getEndpoint() + ' : ' +
                        inContactResponse.getStatusCode() + ' ' + inContactResponse.getStatus());
                    /*Messaging.SingleEmailMessage mail=new Messaging.SingleEmailMessage();
                    String[] toAddresses = new String[] {'david.wang@slalom.com'};
                    mail.setToAddresses(toAddresses);
                    mail.setReplyTo('david.wang@slalom.com');
                    mail.setSenderDisplayName('InContact ID');
                    mail.setSubject('Error From Org : ' + UserInfo.getOrganizationName());
                    mail.setPlainTextBody(body +'\n'+inContactResponse.getBody() +'\n\n'+'Error from ' + rq.getEndpoint() + ' : ' +
                        inContactResponse.getStatusCode() + ' ' + inContactResponse.getStatus());
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });*/
                }
                        
         }  
            }
        
    }
}