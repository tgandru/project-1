@RestResource(urlMapping='/SECASelloutAccount/*')
global with sharing class SECASelloutAccountIntegration {
    
    @HttpPost
    global static SelloutStubClass.msgHeadersResponse SendSECADatatoGERP() {
         string partialEndpoint = Test.isRunningTest() ? 'asdf' : Integration_EndPoints__c.getInstance('Flow-031').partial_endpoint__c;
         String refNum='';
         Datetime now=System.Now();
         refNum=String.valueof(now.Year())+String.valueof(now.Month())+String.valueof(now.day())+String.valueof(now.hour())+String.valueof(now.minute())+String.valueOf(now.second());
        RestRequest req = RestContext.request;
        String postBody = req.requestBody.toString(); 
        System.debug('********postBody::'+postBody);
        SelloutStubClass.msgHeadersResponse resp=new SelloutStubClass.msgHeadersResponse();
        SelloutStubClass.SECASelloutAccRequest request = null;
        if(postBody != null && postBody !=''){
            
            try{
                request=(SelloutStubClass.SECASelloutAccRequest)json.deserialize(postBody, SelloutStubClass.SECASelloutAccRequest.class);
              }catch(exception ex){
                System.debug('********JSON Invalid******');
                 createMQRecord(postBody,'Invalid JSON','','','');
                resp.MSGGUID = '';
    	 		resp.IFID 	= '';
    	 		resp.IFDate 	= '';
    	 		resp.MSGSTATUS = 'F';
    	 		resp.ERRORTEXT = 'Invalid Request Message. - '+postBody;
    	 		resp.ERRORCODE = '';
    	 		return resp;
            }
            
            resp.MSGGUID = request.inputHeaders.MSGGUID;
	 		resp.IFID 	= request.inputHeaders.IFID;
	 		resp.IFDate 	= request.inputHeaders.IFDate;
	 	
                try{
	                    List<SECA_Sellout_Data__c> insertlst=new List<SECA_Sellout_Data__c>();
                        String type;
                          for(SelloutStubClass.SelloutProductsAcc_seca s:request.body.lines){
                              SECA_Sellout_Data__c item=new SECA_Sellout_Data__c();
                              if(s.version=='0' || s.version =='000'){
                                  item.Type__c='Actual';
                                  type='Actual';
                              }else{
                                  item.Type__c='Plan';
                                  type='Plan';
        
                              }
                               item.Data_Type__c='Sellout Account Data';
                               item.Material_Code__c=s.modelCode;
                               item.Account_No__c=s.accountNo;
                               item.SAP_Company_Code__c=s.sapCompanyCode;
                               item.Version__c=s.version;
                               item.Apply_Month__c=s.applyMonth;
                               item.Bill_To_Code__c=s.billToCode;
                               item.Loading_Month__c=s.loadingMonth;
                               item.Reference_Number__c=refNum;
                             
                              insertlst.add(item);
                          }
                        
                        if(insertlst.size()>0){
                            
                            Delete([Select id from SECA_Sellout_Data__c where Data_Type__c='Sellout Account Data' and Type__c=:type ]);
                            insert insertlst;    
                        }
                     resp.MSGSTATUS = 'S';
	 	             resp.ERRORTEXT = '';
	 		         resp.ERRORCODE = '';
	 		          createMQRecord(postBody,'',request.inputHeaders.MSGGUID,request.inputHeaders.IFDate,refNum);
	 		         return resp;
	                
	             }catch(exception ex){
	                  createMQRecord(postBody,ex.getmessage(),request.inputHeaders.MSGGUID,request.inputHeaders.IFDate,'');
	                 System.debug('********exception in callout******'+ex.getmessage());
	                 resp.MSGSTATUS = 'F';
	 	             resp.ERRORTEXT = ex.getmessage();
	 		         resp.ERRORCODE = '';
	 		         return resp;
	             }
	          return resp;
            
            
        }else{
             
            createMQRecord('','Request Body is Null','','','');
             System.debug('********Request Body Is Null******');
            resp.MSGGUID = '';
	 		resp.IFID 	= '';
	 		resp.IFDate 	= '';
	 		resp.MSGSTATUS = 'F';
	 		resp.ERRORTEXT = 'Invalid Request Message. - '+postBody;
	 		resp.ERRORCODE = '';
	 		return resp;
        }
        
        
        
    }
    
    
   
  
    @future
  public static void createMQRecord(String body,String err,String guid,String ifdate,string refNum){
      
      
              String postbody=body;
	 		
	 		if(postbody.length()>119990){
	 		    postbody=postbody.mid(0, 119990);
	 		} 
	 		List<message_queue__c> insertlst=new List<message_queue__c>();
        message_queue__c mq = new message_queue__c(MSGUID__c = guid, 
	 												MSGSTATUS__c = 'S',  
	 												Integration_Flow_Type__c = 'Flow_031_SECA',
	 												IFID__c = '',
	 												IFDate__c = ifdate,
	 											    Status__c = 'success',
	 												Object_Name__c = 'SECA Sellout Account', 
	 												Request_Body__c=postbody,
	 												ERRORTEXT__c=err,
	 												retry_counter__c = 0);
	 		insertlst.add(mq);
	 												
      if(err=='' || err==null){
          message_queue__c mq2 = new message_queue__c(Identification_Text__c=refNum,
                                                                 Integration_Flow_Type__c='Flow-031',
                                                                 retry_counter__c=0,
                                                                 Object_Name__c='Sellout-SECA',
                                                                 Status__c='not started');
            insertlst.add(mq2);
          
          
      }
        
	 	insert insertlst;	
      
      
  }
  
  
    
}