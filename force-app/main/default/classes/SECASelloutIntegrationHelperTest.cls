@isTest
private class SECASelloutIntegrationHelperTest {

	private static testMethod void testmethodSelloutQTY() {
            String request='{"inputHeaders":{"IFDate":"20190219044503","MSGGUID":"e7b278ce-df15-4c72-85ed-a494974c15b4","IFID":"SFDC_IF_US_030"},"body":{"lines":[{"version":"0","sapCompanyCode":"C310","ERNAM":"SFDC (US)","applyMonth":"201812","accountNo":"7280435","rolloutMonth":"201811","opportunityNo":"SLO-000015","indexCode":null,"division":"G1","quantity":"16","modelCode":"SM-T567VZKAVZW","countryCode":"US"},{"version":"0","sapCompanyCode":"C310","ERNAM":"SFDC (US)","applyMonth":"201812","accountNo":"7280435","rolloutMonth":"201811","opportunityNo":"SLO-000015","indexCode":null,"division":"G1","quantity":"8515","modelCode":"SM-T597VZKAVZW","countryCode":"US"}]}}';
            
            Test.startTest();
        
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/SECASelloutQTY/xxxxxx';  //Request URL
        req1.httpMethod = 'Post';//HTTP Request Type
        req1.requestBody = Blob.valueof(request);
        RestContext.request = req1;
        RestContext.response= res1;
        
        SelloutStubClass.msgHeadersResponse resData1 = SECASelloutQTYIntegration.SendSECADatatoGERP();
        
        System.assertEquals('S', resData1.MSGSTATUS);
        
        Test.stopTest();
            
	}
	
		private static testMethod void testmethodSelloutQTY2() {
            String request='';
            
            Test.startTest();
        
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/SECASelloutQTY/xxxxxx';  //Request URL
        req1.httpMethod = 'Post';//HTTP Request Type
        req1.requestBody = Blob.valueof(request);
        RestContext.request = req1;
        RestContext.response= res1;
        
        SelloutStubClass.msgHeadersResponse resData1 = SECASelloutQTYIntegration.SendSECADatatoGERP();
        
        System.assertEquals('F', resData1.MSGSTATUS);
        
        Test.stopTest();
            
	}
		private static testMethod void testmethodSelloutQTY3() {
            String request='<abc>a</abc>';
            
            Test.startTest();
        
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/SECASelloutQTY/xxxxxx';  //Request URL
        req1.httpMethod = 'Post';//HTTP Request Type
        req1.requestBody = Blob.valueof(request);
        RestContext.request = req1;
        RestContext.response= res1;
        
        SelloutStubClass.msgHeadersResponse resData1 = SECASelloutQTYIntegration.SendSECADatatoGERP();
        
        System.assertEquals('F', resData1.MSGSTATUS);
        
        Test.stopTest();
            
	}
	
	
	private static testMethod void testmethodSelloutAccount() {
            String request='{"body":{"lines":[{"version":"0","modelCode":"SM-G965UZDASPR","sapCompanyCode":"C310","ERNAM":"SFDC (US)","billToCode":"4412945","applyMonth":"201901","accountNo":"7280434"},{"version":"0","modelCode":"SM-G965UZDASPR","sapCompanyCode":"C310","ERNAM":"SFDC (US)","billToCode":"4419601","applyMonth":"201901","accountNo":"7280434"},{"version":"0","modelCode":"SM-J327PZSASPR","sapCompanyCode":"C310","ERNAM":"SFDC (US)","billToCode":"1816117","applyMonth":"201901","accountNo":"7280434"},{"version":"0","modelCode":"SM-J327PZSASPR","sapCompanyCode":"C310","ERNAM":"SFDC (US)","billToCode":"4223794","applyMonth":"201901","accountNo":"7280434"},{"version":"0","modelCode":"SM-J327PZSASPR","sapCompanyCode":"C310","ERNAM":"SFDC (US)","billToCode":"4412945","applyMonth":"201901","accountNo":"7280434"},{"version":"0","modelCode":"SM-J327PZSASPR","sapCompanyCode":"C310","ERNAM":"SFDC (US)","billToCode":"4419601","applyMonth":"201901","accountNo":"7280434"}]},"inputHeaders":{"IFDate":"20190219030015","MSGGUID":"50602fe0-2327-4041-8b1f-286c651e4d22","IFID":"SFDC_IF_US_031"}}';
            
            Test.startTest();
        
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/SECASelloutAccount/xxxxxx';  //Request URL
        req1.httpMethod = 'Post';//HTTP Request Type
        req1.requestBody = Blob.valueof(request);
        RestContext.request = req1;
        RestContext.response= res1;
        
        SelloutStubClass.msgHeadersResponse resData1 = SECASelloutAccountIntegration.SendSECADatatoGERP();
        
        System.assertEquals('S', resData1.MSGSTATUS);
        
        Test.stopTest();
            
	}
	
		private static testMethod void testmethodSelloutAccount2() {
            String request='';
            
            Test.startTest();
        
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/SECASelloutAccount/xxxxxx';  //Request URL
        req1.httpMethod = 'Post';//HTTP Request Type
        req1.requestBody = Blob.valueof(request);
        RestContext.request = req1;
        RestContext.response= res1;
        
        SelloutStubClass.msgHeadersResponse resData1 = SECASelloutAccountIntegration.SendSECADatatoGERP();
        
        System.assertEquals('F', resData1.MSGSTATUS);
        
        Test.stopTest();
            
	}
		private static testMethod void testmethodSelloutAccount3() {
            String request='<abc>a</abc>';
            
            Test.startTest();
        
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/SECASelloutAccount/xxxxxx';  //Request URL
        req1.httpMethod = 'Post';//HTTP Request Type
        req1.requestBody = Blob.valueof(request);
        RestContext.request = req1;
        RestContext.response= res1;
        
        SelloutStubClass.msgHeadersResponse resData1 = SECASelloutAccountIntegration.SendSECADatatoGERP();
        
        System.assertEquals('F', resData1.MSGSTATUS);
        
        Test.stopTest();
            
	}

}