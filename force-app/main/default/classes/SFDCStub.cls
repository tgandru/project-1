global with sharing class SFDCStub {
    public static final Integer LIMIT_COUNT = 300;
    public static final String FLOW_080 = 'Flow-080';
    public static final String FLOW_081 = 'Flow-081';
    public static final String FLOW_082 = 'Flow-082';
    
    public static final String LAYOUTID_080 = 'LAY024657';
    public static final String LAYOUTID_081 = 'LAY024658';
    public static final String LAYOUTID_082 = 'LAY024673';
    
    public static String toString(Date d) {
        return d==null ? '' : d.month()+'/'+d.day()+'/'+d.year();
    }
    
    public static String toString(DateTime dt) {
        return dt==null ? '' : dt.month()+'/'+dt.day()+'/'+dt.year();
    }
    
    global with sharing class RequestHeader{
        public string MSGGUID {get;set;} // GUID for message
        public string IFID {get;set;} // interface id, created automatically, so need logic
        public string IFDate {get{return datetime.now().formatGMT('YYYYMMddHHmmss');}set;} // interface date
    } 

    global with sharing class ResponseHeader {
        public string MSGGUID {get;set;} // GUID for message
        public string IFID {get;set;} // interface id, created automatically, so need logic
        public string IFDate {get;set;} // interface date
        public string MSGSTATUS {get;set;} // start, success, failure S, F/E)
        public string ERRORCODE {get;set;} // error code
        public string ERRORTEXT {get;set;} // error message
    } 
    
    global with sharing class OpportunityWithProductRequest {
        public RequestHeader inputHeaders {get;set;}        
        public OpportunityWithProductRequestBody body {get;set;}
        
        public OpportunityWithProductRequest() {
            this.inputHeaders = new RequestHeader();
            this.body = new OpportunityWithProductRequestBody();
        }
    }
    
    global with sharing class OpportunityWithProductRequestBody {
        public String CONSUMER {get;set;}
        public Integer PAGENO {get;set;}
    }
    
    global with sharing class OpportunityWithProductResponse {
        public ResponseHeader inputHeaders {get;set;}        
        public OpportunityWithProductResponseBody body {get;set;}
        
        public OpportunityWithProductResponse() {
            this.inputHeaders = new ResponseHeader();
            this.body = new OpportunityWithProductResponseBody();
        }
    }
    
    global with sharing class OpportunityWithProductResponseBody {
        public Integer PAGENO {set; get;}
        public Integer ROWCNT {set; get;}
        public Integer COUNT  {set; get;}
        public List<OpportunityWithProduct> Opportuntity{set; get;}
        
        public OpportunityWithProductResponseBody() {
            this.Opportuntity = new List<OpportunityWithProduct>();
        }
    }
    
    global with sharing class OpportunityWithProduct {
        public String Id                {set; get{return instance==null? Id  : instance.Id;}}
        public String Name              {set; get{return instance==null? Name: instance.Name;}}
        public Double Amount            {set; get{return instance==null? Amount: instance.Amount;}}
        public String CloseDate         {set; get{return instance==null? CloseDate: SFDCStub.toString(instance.CloseDate);}}
        public String CreatedBy         {set; get{return instance==null? createdBy: instance.CreatedBy.Name;}}
        public Double ExpectedRevenue   {set; get{return instance==null? ExpectedRevenue: instance.ExpectedRevenue;}}
        public String Source            {set; get{return instance==null? Source: instance.LeadSource;}}
        public String NextStep          {set; get{return instance==null? NextStep: instance.NextStep;}}
        public Decimal Probability      {set; get{return instance==null? Probability: instance.Probability;}}
        public Decimal Quantity         {set; get{return instance==null? Quantity: instance.TotalOpportunityQuantity;}}
        public String Stage             {set; get{return instance==null? Stage: instance.StageName;}}
        public String Type              {set; get{return instance==null? Type: instance.Type;}}
        public String Channel           {set; get{return instance==null? Channel: instance.Channel__c;}}
        public String DropDate          {set; get{return instance==null? DropDate: SFDCStub.toString(instance.Drop_Date__c);}}
        public String DropReason        {set; get{return instance==null? DropReason: instance.Drop_Reason__c;}}
        public Double ForecastAmount    {set; get{return instance==null? ForecastAmount: instance.ForecastAmount__c;}}
        public String HistoricalComments{set; get{return instance==null? HistoricalComments: instance.Historical_Comments__c;}}
        public String LostDate          {set; get{return instance==null? LostDate: SFDCStub.toString(instance.Lost_Date__c);}}
        public String LostReason        {set; get{return instance==null? LostReason: instance.Lost_Reason__c;}}
        public Boolean MDM              {set; get{return instance==null? MDM: instance.MDM__c;}}
        public String PrimaryDistributor{set; get{return instance==null? PrimaryDistributor: instance.Primary_Distributor__c;}}
        public String PrimaryReseller   {set; get{return instance==null? PrimaryReseller: instance.Primary_Reseller__c;}}
        public String RolloutEnd        {set; get{return instance==null? RolloutEnd: SFDCStub.toString(instance.Roll_Out_End_Formula__c);}}
        public String RolloutStart      {set; get{return instance==null? RolloutStart: SFDCStub.toString(instance.Roll_Out_Start__c);}}
        public String SamsungOpptyID    {set; get{return instance==null? SamsungOpptyID: instance.External_Opportunity_ID__c;}}
        public String WonDate           {set; get{return instance==null? WonDate: SFDCStub.toString(instance.Won_Date__c);}}
        public String OwnerDepartment   {set; get{return instance==null? OwnerDepartment: instance.Owner.Department;}}
        public String OwnerSalesTeam    {set; get{return instance==null? OwnerSalesTeam: instance.Owner.Sales_Team__c;}}
        public String Owner             {set; get{return instance==null? Owner: instance.Owner.Name;}}
        public String CreatedDate       {set; get{return instance==null? CreatedDate: SFDCStub.toString(instance.CreatedDate);}}
        public String LastModifiedDate  {set; get{return instance==null? LastModifiedDate: SFDCStub.toString(instance.LastModifiedDate);}}
        public String AccountId         {set; get{return instance==null? AccountId: instance.Account.Id;}}
        public String RecordStatus      {set; get{return status==null? RecordStatus: status;}}
         public String IL_CL      {set; get{return instance==null? IL_CL: instance.IL_CL__c;}}
            
        transient public Opportunity instance;
        transient public String status;
        public List<ProductLineItem> Product{set;get;}
        /*
        public List<ProductLineItem> Product{set; 
            get{
                if(this.instance==null) {
                    return this.Product;
                }
                
                for(OpportunityLineItem item : this.instance.OpportunityLineItems) {
                    ProductLineItem pi = new ProductLineItem(item);
                    this.Product.add(pi);
                }
                system.debug('product------>'+this.Product);
                return this.Product;
            }
        }
        */
        public OpportunityWithProduct(Opportunity instance, String status) {
            this.instance = instance;
            this.status = status;
            this.Product = new List<ProductLineItem>();
            
            if(this.instance==null) {
                return;
            }
            for(OpportunityLineItem item : this.instance.OpportunityLineItems) {
                ProductLineItem pi = new ProductLineItem(item);
                this.Product.add(pi);
                system.debug('product------>'+this.Product);
            }
        }
    }
    
    global with sharing class ProductLineItem{
        public String SAPMaterialId {set; get{ return instance==null? SAPMaterialId : instance.SAP_Material_ID__c;}}
        public String ProductFamily {set; get{ return instance==null? ProductFamily : instance.Product_Family__c;}}
        public String ProductGroup  {set; get{ return instance==null? ProductGroup : instance.Product_Group__c;}}
        public Boolean Alternative  {set; get{ return instance==null? Alternative : instance.Alternative__c;}}
        public Decimal Quantity     {set; get{ return instance==null? Quantity : instance.Quantity;}}
        public Double Discount      {set; get{ return instance==null? Discount : instance.Discount;}}
        public Double TotalPrice    {set; get{ return instance==null? TotalPrice : instance.TotalPrice;}}
        public Double UnitPrice     {set; get{ return instance==null? UnitPrice : instance.UnitPrice;}}
        public String ATTRIB06      {set; get{ return instance==null? ATTRIB06 : instance.Product2.ATTRIB06__c;}}
        public String ATTRIB04      {set; get{ return instance==null? ATTRIB04 : instance.Product2.ATTRIB04__c;}}
        public String PETName       {set; get{ return instance==null? PETName : instance.Product2.PET_Name__c;}}
        public String Name          {set; get{ return instance==null? Name : instance.Product2.Name;}}
        
        transient private OpportunityLineItem instance;
        
        public ProductLineItem(OpportunityLineItem item) {
            this.instance = item;
        }
    }
    
    global with sharing class AccountWithContactRequest {
        public RequestHeader inputHeaders {get;set;}        
        public AccountWithContactRequestBody body {get;set;}
        
        public AccountWithContactRequest() {
            this.inputHeaders = new RequestHeader();
            this.body = new AccountWithContactRequestBody();
        }
    }
    
    global with sharing class AccountWithContactRequestBody {
        public String CONSUMER {get;set;}
        public Integer PAGENO {get;set;}
    }
    
    global with sharing class AccountWithContactResponse {
        public ResponseHeader inputHeaders {get;set;}        
        public AccountWithContactResponseBody body{get;set;}
        
        public AccountWithContactResponse() {
            this.inputHeaders = new ResponseHeader();
            this.body = new AccountWithContactResponseBody();
        }
    }
    
    global with sharing class AccountWithContactResponseBody {
        public Integer PAGENO {set; get;}
        public Integer ROWCNT {set; get;}
        public Integer COUNT  {set; get;}
        
        public List<AccountWithContact> Account {set; get;}
        
        public AccountWithContactResponseBody() {
            this.Account = new List<AccountWithContact>();
        }
    }
    
    global with sharing class AccountWithContact {
        public String Id                {set; get{return instance==null? Id : instance.Id;}}
        public String Name              {set; get{return instance==null? Name : instance.Name;}}
        public String Owner             {set; get{return instance==null? Owner: instance.Owner.Name;}}
        public String OwnerSalesTeam    {set; get{return instance==null? OwnerSalesTeam: instance.Owner.Sales_Team__c;}}
        public String OwnerRoleName     {set; get{return instance==null? OwnerRoleName: instance.Owner.UserRole.Name;}}
        public String AccountId         {set; get{return instance==null? AccountId : instance.SAP_Company_Code__c;}}
        public String Type              {set; get{return instance==null? Type : instance.Type;}}
        public Boolean KeyAccount       {set; get{return instance==null? KeyAccount : instance.Key_Account__c;}}
        public String LERecordType      {set; get{return instance==null? LERecordType : instance.LE_Record_Type__c;}}
        public String ParentAccountId   {set; get{return instance==null? ParentAccountId : (instance.Parent==null? '' : instance.Parent.Id);}}
        public String ParentAccount     {set; get{return instance==null? ParentAccount : (instance.Parent==null? '' : instance.Parent.Name);}}
        public String IndustryType      {set; get{return instance==null? IndustryType : instance.Industry_Type__c;}}
        public String Industry          {set; get{return instance==null? Industry : instance.Industry;}}
        public String ExternalIndustry  {set; get{return instance==null? ExternalIndustry : instance.External_Industry__c;}}
        public String LastActivityDate  {set; get{return instance==null? LastActivityDate : SFDCStub.toString(instance.LastActivityDate);}}
        public String BillingCountry    {set; get{return instance==null? BillingCountry : instance.BillingCountry;}}
        public String BillingState      {set; get{return instance==null? BillingState : instance.BillingState;}}
        public String BillingZipCode    {set; get{return instance==null? BillingZipCode : instance.BillingPostalCode;}}
        public String YearStarted       {set; get{return instance==null? YearStarted : instance.YearStarted;}}
        public Integer Employees        {set; get{return instance==null? Employees : instance.NumberOfEmployees;}}
        public String Description       {set; get{return instance==null? Description : instance.Description;}}
        public String RecordStatus      {set; get{return status==null? RecordStatus : status;}}

        public List<ContactItem> Contact {set; get;}
        public transient String status;
        public transient Account instance;
        public AccountWithContact(Account instance, String status) {
            this.instance = instance;
            this.status = status;
            this.Contact = new List<ContactItem>();
        }
    }
    
    global with sharing class ContactItem {
        public String Id                {set; get{return instance==null? Id : instance.Id;}}
        public String Email             {set; get{return instance==null? Email : instance.Email;}}
        public String Fax               {set; get{return instance==null? Fax : instance.Fax;}}
        public String MailingStreet     {set; get{return instance==null? MailingStreet : instance.MailingStreet;}}
        public String MailingCity       {set; get{return instance==null? MailingCity : instance.MailingCity;}}
        public String MailingState      {set; get{return instance==null? MailingState : instance.MailingState;}}
        public String MailingZipCode    {set; get{return instance==null? MailingZipCode : instance.MailingPostalCode;}}
        public String MailingCountry    {set; get{return instance==null? MailingCountry : instance.MailingCountry;}}
        public String FirstName         {set; get{return instance==null? FirstName : instance.FirstName;}}
        public String LastName          {set; get{return instance==null? LastName : instance.LastName;}}
        public String Phone             {set; get{return instance==null? Phone : instance.Phone;}}
        public String Title             {set; get{return instance==null? Title : instance.Title;}}
        public String RecordStatus      {set; get{return status==null? RecordStatus : status;}}

        private transient Contact instance;
        public transient String status;
        public ContactItem(Contact instance, String status) {
            this.instance = instance;
            this.status = status;
        }
    }
    
    global with sharing class OpportunityRolloutRequest {
        public RequestHeader inputHeaders {get;set;}        
        public OpportunityRolloutRequestBody body {get;set;}
        
        public OpportunityRolloutRequest() {
            this.inputHeaders = new RequestHeader();
            this.body = new OpportunityRolloutRequestBody();
        }
    }
    
    global with sharing class OpportunityRolloutRequestBody {
        public String CONSUMER {get;set;}
        public Integer PAGENO {get;set;}
    }
    
    global with sharing class OpportunityRolloutResponse {
        public ResponseHeader inputHeaders {get;set;}        
        public OpportunityRolloutResponseBody body{get;set;}
        
        public OpportunityRolloutResponse() {
            this.inputHeaders = new ResponseHeader();
            this.body = new OpportunityRolloutResponseBody();
        }
    }
    
    global with sharing class OpportunityRolloutResponseBody {
        public Integer PAGENO {set; get;}
        public Integer ROWCNT {set; get;}
        public Integer COUNT  {set; get;}
        
        public List<OpportunityRolloutProduct> RolloutProduct {set; get;}
        
        public OpportunityRolloutResponseBody() {
            this.RolloutProduct = new List<OpportunityRolloutProduct>();
        }
    }
    
    global with sharing class OpportunityRolloutProduct {
        public String OpporutnityId {set; get{return instance==null? OpporutnityId : instance.Roll_Out_Plan__r.Opportunity__c;}}
        public String Id {set; get{return instance==null? Id : instance.Id;}}
        public String SAPMaterialCode{set; get{return instance==null? SAPMaterialCode : instance.SAP_Material_Code__c;}}
        public String ProductGroup {set; get{return instance==null? ProductGroup : instance.Product__r.Product_Group__c;}}
        public String ProductName   {set; get{return instance==null? ProductName : instance.Product__r.Name;}}
        public String PETName {set; get{return instance==null? PETName : instance.Product__r.PET_Name__c;}}
        public String RecordStatus {set; get{return instance==null? RecordStatus : status;}}
       // public String IL_CL {set; get{return instance==null? OpporutnityId : instance.Roll_Out_Plan__r.Opportunity__r.IL_CL__c;}}
        public List<OpportunityRolloutSchedule> RolloutSchedule{set; get;}
        
        transient public Roll_Out_Product__c instance;
        transient public string status;
        public OpportunityRolloutProduct(Roll_Out_Product__c instance, string status) {
            this.instance = instance;
            this.Status = status;
            this.RolloutSchedule = new List<OpportunityRolloutSchedule>();
            if(instance==null) {
                return;
            }
            for(Roll_Out_Schedule__c ros:this.instance.Roll_Out_Schedules__r) {
                OpportunityRolloutSchedule schedule = new OpportunityRolloutSchedule(ros);
                this.RolloutSchedule.add(schedule);
            }
        }
    }
    
    global with sharing class OpportunityRolloutSchedule {
        public String Id {set; get{return instance==null? Id : instance.Id;}}
        public String PlanDate {set; get{return instance==null? PlanDate : SFDCStub.toString(instance.Plan_Date__c);}}
        public Double PlanRevenue {set; get{return instance==null? PlanRevenue : instance.Plan_Revenue__c;}}
        public String RolloutMonthYear {set; get{return instance==null? RolloutMonthYear : instance.MonthYear__c;}}
        public Double PlanQuantity {set; get{return instance==null? PlanQuantity : instance.Plan_Quantity__c;}}
        
        transient private Roll_Out_Schedule__c instance;
        public OpportunityRolloutSchedule(Roll_Out_Schedule__c instance) {
            this.instance = instance;
        }
    }
}