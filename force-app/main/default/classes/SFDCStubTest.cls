/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SFDCStubTest {
    
    static testMethod void testOpportunityWithProduct() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Account acc1 = new account(Name ='Test', Type='Customer',SAP_Company_Code__c = 'AAA');
        insert acc1;
        Id rtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('IT').getRecordTypeId();
        
        Opportunity temp = new Opportunity(accountId=acc1.id,recordtypeId=rtId,
        StageName='Identified',Name='TEst Opp',Type='Tender',Division__c='IT',ProductGroupTest__c='LCD_MONITOR',LeadSource='BTA Dealer',Closedate=system.today());
        insert temp;
        
        SFDCStub.OpportunityWithProductRequest request = new SFDCStub.OpportunityWithProductRequest();
        request.inputHeaders.MSGGUID = cihStub.giveGUID();
        request.inputHeaders.IFID    = SFDCStub.LAYOUTID_080;
        request.inputHeaders.IFDate  = datetime.now().format('YYMMddHHmmss', 'America/New_York');
        request.body.CONSUMER = 'GMBT';
        request.body.PAGENO = 1;
        
        String jsonMsg = JSON.serialize(request);
        
        SFDCStub.OpportunityWithProductResponse response = new SFDCStub.OpportunityWithProductResponse();
        response.inputHeaders.MSGGUID = cihStub.giveGUID();
        response.inputHeaders.IFID    = SFDCStub.LAYOUTID_080;
        response.inputHeaders.IFDate  = datetime.now().format('YYMMddHHmmss', 'America/New_York');
        response.body.PAGENO = 1;
        response.body.ROWCNT = 10;
        response.body.COUNT = 10;
        
        Opportunity opp = [SELECT Id, Amount, CloseDate, CreatedBy.Name, ExpectedRevenue, LeadSource, NextStep, Name, Probability, TotalOpportunityQuantity, 
                    StageName, Type, Channel__c, Drop_Date__c, Drop_Reason__c, ForecastAmount__c, Historical_Comments__c, Lost_Date__c, Lost_Reason__c, 
                    MDM__c, Primary_Distributor__c, Primary_Reseller__c, Roll_Out_End__c, Roll_Out_Start__c, External_Opportunity_ID__c, Won_Date__c, 
                    Owner.Department, Owner.Sales_Team__c, Owner.Name, Account.Name, CreatedDate, LastModifiedDate, Roll_Out_End_Formula__c, campaign.name, Isdeleted,
                    (Select Id, SAP_Material_ID__c, Product_Family__c, Product_Group__c, Alternative__c, Quantity, Discount, TotalPrice, UnitPrice,
                        Product2.Name, Product2.PET_Name__c, Product2.ATTRIB04__c, Product2.ATTRIB06__c FROM OpportunityLineItems) FROM Opportunity limit 1];
        SFDCStub.OpportunityWithProduct oppWithProduct = new SFDCStub.OpportunityWithProduct(opp,'null'); 
        System.debug(oppWithProduct.Id);
        System.debug(oppWithProduct.Name);
        System.debug(oppWithProduct.Amount);
        System.debug(oppWithProduct.CloseDate);
        System.debug(oppWithProduct.CreatedBy);
        System.debug(oppWithProduct.ExpectedRevenue);
        System.debug(oppWithProduct.Source);
        System.debug(oppWithProduct.NextStep);
        System.debug(oppWithProduct.Probability);
        System.debug(oppWithProduct.Quantity);
        System.debug(oppWithProduct.Stage);
        System.debug(oppWithProduct.Type);
        System.debug(oppWithProduct.Channel);
        System.debug(oppWithProduct.DropDate);
        System.debug(oppWithProduct.DropReason);
        System.debug(oppWithProduct.ForecastAmount);
        //System.debug(oppWithProduct.HistoricalComments);
        System.debug(oppWithProduct.LostDate);
        System.debug(oppWithProduct.LostReason);
        System.debug(oppWithProduct.MDM);
        System.debug(oppWithProduct.PrimaryDistributor);
        System.debug(oppWithProduct.PrimaryReseller);
        System.debug(oppWithProduct.RolloutEnd);
        System.debug(oppWithProduct.RolloutStart);
        System.debug(oppWithProduct.SamsungOpptyID);
        System.debug(oppWithProduct.WonDate);
        System.debug(oppWithProduct.OwnerDepartment);
        System.debug(oppWithProduct.OwnerSalesTeam);
        System.debug(oppWithProduct.Owner);
        System.debug(oppWithProduct.CreatedDate);
        System.debug(oppWithProduct.LastModifiedDate);
        System.debug(oppWithProduct.AccountId);
        
        SFDCStub.ProductLineItem item = new SFDCStub.ProductLineItem(null);
        item.SAPMaterialId = 'SAPMaterialId';
        item.ProductFamily = 'ProductFamily';
        item.ProductGroup = 'ProductGroup';
        item.Alternative = false;
        item.Quantity = 10;
        item.Discount = 10.0;
        item.TotalPrice = 10.0;
        item.UnitPrice = 10.0;
        item.ATTRIB06 = 'ATTRIB06';
        item.ATTRIB04 = 'ATTRIB04';
        item.PETName = 'PETName';
        item.Name = 'Name';
        
        oppWithProduct.Product.add(item);
        response.body.Opportuntity.add(oppWithProduct);
        
        //OpportunityWithProductSECPI
        SFDCStubSECPI.OpportunityWithProductSECPI oppWithProduct2 = new SFDCStubSECPI.OpportunityWithProductSECPI(opp,'null');
        System.debug(oppWithProduct2.Id);
        System.debug(oppWithProduct2.Name);
        System.debug(oppWithProduct2.Amount);
        System.debug(oppWithProduct2.CloseDate);
        System.debug(oppWithProduct2.CreatedBy);
        System.debug(oppWithProduct2.ExpectedRevenue);
        System.debug(oppWithProduct2.LeadSource);
        System.debug(oppWithProduct2.NextStep);
        System.debug(oppWithProduct2.Probability);
        System.debug(oppWithProduct2.OpportunityQuantity);
        System.debug(oppWithProduct2.Stage);
        System.debug(oppWithProduct2.Type);
        System.debug(oppWithProduct2.Channel);
        System.debug(oppWithProduct2.DropDate);
        System.debug(oppWithProduct2.DropReason);
        System.debug(oppWithProduct2.ForecastAmount);
        System.debug(oppWithProduct2.LostDate);
        System.debug(oppWithProduct2.LostReason);
        System.debug(oppWithProduct2.MDM);
        System.debug(oppWithProduct2.PrimaryDistributor);
        System.debug(oppWithProduct2.PrimaryReseller);
        System.debug(oppWithProduct2.RolloutEnd);
        System.debug(oppWithProduct2.RolloutStart);
        System.debug(oppWithProduct2.SamsungOpptyID);
        System.debug(oppWithProduct2.WonDate);
        System.debug(oppWithProduct2.OwnerDepartment);
        System.debug(oppWithProduct2.OwnerSalesTeam);
        System.debug(oppWithProduct2.Owner);
        System.debug(oppWithProduct2.CreatedDate);
        System.debug(oppWithProduct2.LastModifiedDate);
        System.debug(oppWithProduct2.AccountId);
        System.debug(oppWithProduct2.Opp_Isdeleted);
        oppWithProduct2.RecordStatus = 'Modified';
        System.debug(oppWithProduct2.PrimayCampaignSource);
        
        SFDCStubSECPI.ProductLineItemSECPI item2 = new SFDCStubSECPI.ProductLineItemSECPI(null);
        item2.SAPMaterialId = 'SAPMaterialId';
        item2.ProductFamily = 'ProductFamily';
        item2.ProductGroup = 'ProductGroup';
        item2.Alternative = false;
        item2.Quantity = 10;
        item2.Discount = 10.0;
        item2.TotalPrice = 10.0;
        item2.UnitPrice = 10.0;
        item2.ATTRIB06 = 'ATTRIB06';
        item2.ATTRIB04 = 'ATTRIB04';
        item2.PETName = 'PETName';
        item2.ProductName = 'Name';
        item2.InvoicePrice = 11;
        item2.Requested_Price = 11;
        item2.Product_IsDeleted = False;
        
    }
    
    static testMethod void testAccountWithContact() {
        SFDCStub.AccountWithContactRequest reqData = new SFDCStub.AccountWithContactRequest();
        
        reqData.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData.inputHeaders.IFID    = SFDCStub.LAYOUTID_081;
        reqData.inputHeaders.IFDate  = datetime.now().format('YYMMddHHmmss', 'America/New_York');
        reqData.body.CONSUMER = 'GMBT';
        reqData.body.PAGENO = 1;
        
        SFDCStub.AccountWithContactResponse response = new SFDCStub.AccountWithContactResponse();
        response.inputHeaders.MSGGUID = cihStub.giveGUID();
        response.inputHeaders.IFID = '';
        response.inputHeaders.IFDate = datetime.now().format('YYMMddHHmmss', 'America/New_York');
        response.inputHeaders.MSGSTATUS = 'S';
        response.inputHeaders.ERRORTEXT = '';
        response.inputHeaders.ERRORCODE = '';
        
        response.body.PAGENO = 1;
        response.body.ROWCNT = 500;
        response.body.COUNT = 1;
        
        SFDCStub.AccountWithContact accountWithContact = new SFDCStub.AccountWithContact(null,'null');
        accountWithContact.Id = 'Id';
        accountWithContact.Name = 'Name';
        accountWithContact.Owner = 'Owner';
        accountWithContact.OwnerSalesTeam = 'OwnerSalesTeam';
        accountWithContact.OwnerRoleName = 'OwnerRoleName';
        accountWithContact.AccountId = 'AccountId';
        accountWithContact.Type = 'Type';
        accountWithContact.KeyAccount = true;
        accountWithContact.LERecordType = 'LERecordType';
        accountWithContact.ParentAccountId = 'ParentAccountId';
        accountWithContact.ParentAccount = 'ParentAccount';
        accountWithContact.IndustryType = 'IndustryType';
        accountWithContact.Industry = 'Industry';
        accountWithContact.ExternalIndustry = 'ExternalIndustry';
        accountWithContact.LastActivityDate = 'LastActivityDate';
        accountWithContact.BillingCountry = 'BillingCountry';
        accountWithContact.BillingState = 'BillingState';
        accountWithContact.BillingZipCode = 'BillingZipCode';
        accountWithContact.YearStarted = 'YearStarted';
        accountWithContact.Employees = 100;
        //accountWithContact.Description = 'Description';
        
        SFDCStub.ContactItem item = new SFDCStub.ContactItem(null,'null');
        item.Id = 'Id';
        item.Email = 'Email';
        item.Fax = 'Fax';
        item.MailingStreet = 'MailingStreet';
        item.MailingCity = 'MailingCity';
        item.MailingState = 'MailingState';
        item.MailingZipCode = 'MailingZipCode';
        item.MailingCountry = 'MailingCountry';
        item.FirstName = 'FirstName';
        item.LastName = 'LastName';
        item.Phone = 'Phone';
        item.Title = 'Title';
        
        accountWithContact.Contact.add(item);
        response.body.Account.add(accountWithContact);  
        
        //SFDCStubSECPI
        SFDCStubSECPI.AccountWithContact accountWithContact2 = new SFDCStubSECPI.AccountWithContact(null,'null');
        accountWithContact2.AccountId = 'Id';
        accountWithContact2.Name = 'Name';
        accountWithContact2.Owner = 'Owner';
        accountWithContact2.OwnerSalesTeam = 'OwnerSalesTeam';
        accountWithContact2.OwnerRoleName = 'OwnerRoleName';
        accountWithContact2.AccountId = 'AccountId';
        accountWithContact2.Type = 'Type';
        accountWithContact2.KeyAccount = true;
        accountWithContact2.LERecordType = 'LERecordType';
        accountWithContact2.ParentAccountId = 'ParentAccountId';
        accountWithContact2.ParentAccount = 'ParentAccount';
        accountWithContact2.IndustryType = 'IndustryType';
        accountWithContact2.Industry = 'Industry';
        accountWithContact2.ExternalIndustry = 'ExternalIndustry';
        accountWithContact2.LastActivityDate = 'LastActivityDate';
        accountWithContact2.BillingCountry = 'BillingCountry';
        accountWithContact2.BillingState = 'BillingState';
        accountWithContact2.BillingZipCode = 'BillingZipCode';
        accountWithContact2.YearStarted = 'YearStarted';
        accountWithContact2.Employees = 100;
        //accountWithContact2.Description = 'Description';
        
        SFDCStubSECPI.ContactItem item2 = new SFDCStubSECPI.ContactItem(null,'null');
        item2.ContactId = 'Id';
        item2.Email = 'Email';
        item2.Fax = 'Fax';
        item2.MailingStreet = 'MailingStreet';
        item2.MailingCity = 'MailingCity';
        item2.MailingState = 'MailingState';
        item2.MailingZipCode = 'MailingZipCode';
        item2.MailingCountry = 'MailingCountry';
        item2.FirstName = 'FirstName';
        item2.LastName = 'LastName';
        item2.Phone = 'Phone';
        item2.Title = 'Title';
    }
    
    static testMethod void testOpportunityRollout() {
        SFDCStub.OpportunityRolloutRequest reqData = new SFDCStub.OpportunityRolloutRequest();
        
        reqData.inputHeaders.MSGGUID = cihStub.giveGUID();
        reqData.inputHeaders.IFID    = SFDCStub.LAYOUTID_082;
        reqData.inputHeaders.IFDate  = datetime.now().format('YYMMddHHmmss', 'America/New_York');
        reqData.body.CONSUMER = 'GMBT';
        reqData.body.PAGENO = 1;
        
        SFDCStub.OpportunityRolloutResponse response = new SFDCStub.OpportunityRolloutResponse();
        response.inputHeaders.MSGGUID = cihStub.giveGUID();
        response.inputHeaders.IFID = '';
        response.inputHeaders.IFDate = datetime.now().format('YYMMddHHmmss', 'America/New_York');
        response.inputHeaders.MSGSTATUS = 'S';
        response.inputHeaders.ERRORTEXT = '';
        response.inputHeaders.ERRORCODE = '';
        
        response.body.PAGENO = 1;
        response.body.ROWCNT = 500;
        response.body.COUNT = 1;
        
        SFDCStub.OpportunityRolloutProduct orp = new SFDCStub.OpportunityRolloutProduct(null,'null');
        orp.OpporutnityId = 'OpporutnityId';
        orp.Id = 'Id';
        orp.SAPMaterialCode = 'SAPMaterialCode';
        orp.ProductGroup = 'ProductGroup';
        orp.ProductName = 'ProductName';
        orp.PETName = 'PETName';
        
        SFDCStub.OpportunityRolloutSchedule ors = new SFDCStub.OpportunityRolloutSchedule(null);
        ors.Id = 'Id';
        ors.PlanDate = 'PlanDate';
        ors.PlanRevenue = 10.0;
        ors.RolloutMonthYear = 'RolloutMonthYear';
        ors.PlanQuantity = 10.0;
        orp.RolloutSchedule.add(ors);
        
        response.body.RolloutProduct.add(orp);
        
        SFDCStubSECPI.OpportunityRolloutProduct orp2 = new SFDCStubSECPI.OpportunityRolloutProduct(null,'null');
        orp2.OpporutnityId = 'OpporutnityId';
        orp2.Id = 'Id';
        orp2.SAPMaterialCode = 'SAPMaterialCode';
        orp2.ProductGroup = 'ProductGroup';
        orp2.ProductName = 'ProductName';
        orp2.PETName = 'PETName';
        
        SFDCStubSECPI.OpportunityRolloutSchedule ors2 = new SFDCStubSECPI.OpportunityRolloutSchedule(null);
        ors2.ScheduleId = 'Id';
        ors2.PlanDate = 'PlanDate';
        ors2.PlanRevenue = 10.0;
        ors2.RolloutMonthYear = 'RolloutMonthYear';
        ors2.PlanQuantity = 10.0;
    }
    
}