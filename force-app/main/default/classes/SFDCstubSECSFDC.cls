global class SFDCstubSECSFDC {

    global  class RequestHeader{
        public string MSGGUID {get;set;} // GUID for message
        public string IFID {get;set;} // interface id, created automatically, so need logic
        public string IFDate {get{return datetime.now().formatGMT('YYYYMMddHHmmss');}set;} // interface date
    } 

    global  class ResponseHeader {
        public string MSGGUID {get;set;} // GUID for message
        public string IFID {get;set;} // interface id, created automatically, so need logic
        public string IFDate {get;set;} // interface date
        public string MSGSTATUS {get;set;} // start, success, failure S, F/E)
        public string ERRORCODE {get;set;} // error code
        public string ERRORTEXT {get;set;} // error message
    } 



   global  class OpportunityInfoRequest {
        public RequestHeader inputHeaders {get;set;}        
        public OpportunityInfoRequestBody body {get;set;}
        
        public OpportunityInfoRequest() {
            this.inputHeaders = new RequestHeader();
            this.body = new OpportunityInfoRequestBody();
        }
    }
    
    global  class OpportunityInfoRequestBody {
        public String cisCode {get;set;}
        public String opportunityId {get;set;}
        
    }



  global  class CaseCreationRequest {
        public RequestHeader inputHeaders {get;set;}        
        public CaseCreationRequestbody body {get;set;}
        
        public CaseCreationRequest() {
            this.inputHeaders = new RequestHeader();
            this.body = new CaseCreationRequestbody();
        }
    }
    
    
    global  class CaseCreationResponse {
        public ResponseHeader inputHeaders {get;set;}        
        public string body {get;set;}
        
        public CaseCreationResponse() {
            this.inputHeaders = new ResponseHeader();
            this.body = '';
        }
    }
    
    
    global  class CaseCreationRequestbody {
        public String caseNumber {get;set;}
        public String caseSubject {get;set;}
        public String caseProductCategory {get;set;}
        public String caseStatus {get;set;}
        public String caseUrl {get;set;}
        public String extOpportunityId {get;set;}
        public String subRequestType {get;set;}
    }
    
    
    global with sharing class OpportunitylistResponseSEC {
        public ResponseHeader inputHeaders {get;set;}        
        public Opportunitylistjson body {get;set;}
        
        public OpportunitylistResponseSEC() {
            this.inputHeaders = new ResponseHeader();
            this.body = new Opportunitylistjson();
        }
    }
    
    global with sharing class OpportunitylistResponseSECbody {
        
        public List<Opportunitylist> Opportuntity{set; get;}
        
        public OpportunitylistResponseSECbody() {
            this.Opportuntity = new List<Opportunitylist>();
        }
    }
    
     global class Opportunitylistjson
    {
        public List<Opportunitylist> Opportunitylists;
    }
    
    
   global  class Opportunitylist {
        public String opportunityName   {set; get;}
        public String opportunityId  {set; get;}
        public String opportunityStage  {set; get;}
        public String salesType {set; get;}
        public String CISCode  {set; get;}
        public String opportunityOwnerName {set; get;}
        public String opportunityOwnerEmail {set; get;}
        public String dealSize {set; get;}
        public String rolloutPeriod {set; get;}
         public String closeDate {set; get;}
         public string MonetaryUnit {set; get;}
         public string opportunityNo {set; get;}
         public string accountName {set; get;}
         public string division {set; get;}
         
         
    }
    

}