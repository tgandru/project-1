public class SKUASPTriggerHandler {
    
    Public static void checkskuisavibility(List<SKU_ASP__c> sk){
        Set<string> skuid= new set<string>();
        map<string,string> pdmap=new  map<string,string>();
        string pid='';
        for(SKU_ASP__c s:sk){
            
           skuid.add(s.SAP_Material_ID__c); 
        }
        if(skuid.size()>0){
            for(Product2 p:[select id,SAP_Material_ID__c from Product2 where SAP_Material_ID__c  in :skuid]){
                pdmap.put(p.SAP_Material_ID__c,p.id);
            }
        }
        
        for(SKU_ASP__c s:sk){
            
           pid =pdmap.get(s.SAP_Material_ID__c);
            system.debug('====='+pid);
            if(pid==null || pid==''){
                
                s.Adderror(s.SAP_Material_ID__c+'   does not exist in Product Master ');
            }
            
            
        }
   }
   
   
   
   Public Static Void updaterespectiveschedules(List<SKU_ASP__c> sk){
       
       Set<string>  skuid=new set<string>();
       
       for(SKU_ASP__c s:sk){
            skuid.add(s.SAP_Material_ID__c); 
       }
       
       if(skuid.size()>0){
           
           Database.executeBatch(new ASPUpdateintoROSBatch(skuid));

       }
       
   }

}