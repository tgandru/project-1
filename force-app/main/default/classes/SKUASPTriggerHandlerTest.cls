@isTest
private class SKUASPTriggerHandlerTest {
	static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');

	private static testMethod void testmethod2() {
	     test.startTest();
	    Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
	     Account account = TestDataUtility.createAccount('Marshal Mathers');
        insert account;
        Account partnerAcc = TestDataUtility.createAccount('Distributor account');
        partnerAcc.RecordTypeId = directRT;
        insert partnerAcc;
        
         //Creating custom Pricebook
        PriceBook2 pb = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(4)); 
        insert pb;
        Opportunity opportunity = TestDataUtility.createOppty('New', account, pb, 'Managed', 'Mobile', 'product group',
                                    'Identified');
         opportunity.Roll_Out_Start__c=System.today().addDays(35);
        opportunity.Rollout_Duration__c=1;                         
        insert opportunity;
        Partner__c  partner= TestDataUtility.createPartner(opportunity, account, partnerAcc, 'Distributor');
        partner.Is_Primary__c=true;
        insert partner;
        
        Product2 product = TestDataUtility.createProduct2('SL-SKUASPTEST/SEG2', 'Smart Phone ', 'MOBILE PHONE [G1]', 'SMART_PHONE', 'SET');
        insert product;
       
        
        PricebookEntry pricebookEntry = TestDataUtility.createPriceBookEntry(product.Id, pb.Id);
	    pricebookEntry.unitprice=110;
        insert pricebookEntry;
        
        
        
        List<OpportunityLineItem> opportunityLineItems = new List<OpportunityLineItem>();
        
        for(Integer i=0; i<1; i++) {
	        OpportunityLineItem op1 = TestDataUtility.createOpptyLineItem(product, pricebookEntry, opportunity);
	        op1.Quantity = 20;
	        op1.TotalPrice = 1000;
	        op1.Requested_Price__c = 100;
	        op1.Claimed_Quantity__c = 45;     
	        opportunityLineItems.add(op1);
        }
        insert opportunityLineItems;
        list<Roll_Out_Schedule__c> roslist=new  list<Roll_Out_Schedule__c>();
        for(Roll_Out_Product__c r:[select id,name from Roll_Out_Product__c where Roll_Out_Plan__r.Opportunity__c=:opportunity.id]){
            for(Integer i=0; i<4; i++){
                Roll_Out_Schedule__c rs=new Roll_Out_Schedule__c(Roll_Out_Product__c=r.id,
                                                                  year__c='2018',
                                                                  fiscal_month__c='5',
                                                                 Plan_Quantity__c=5
                                             );
                    roslist.add(rs);                         
            }
        }
        
       insert roslist;
        List<SKU_ASP__c> skus=new list<SKU_ASP__c>();
         SKU_ASP__c sk=new SKU_ASP__c(SAP_Material_ID__c='SL-SKUASPTEST/SEG2',
                                       Year__c='2018',
                                       Month__c=5,
                                       ASP_Value__c=100);
         SKU_ASP__c sk2=new SKU_ASP__c(SAP_Material_ID__c='SL-SKUASPTEST/SEG2',
                                       Year__c='2018',
                                       Month__c=8,
                                       ASP_Value__c=100);
       skus.add(sk);
       skus.add(sk2);
       insert skus;
        
         database.executeBatch(new ASPUpdateintoROSBatchForMigration());
         test.stopTest();
	    
	}

}