/**
 * Created by ms on 2017-07-11.
 *
 * author : sungil.kim, I2MAX
 *
 ## Request
 {
  "MSGGUID": "9d53c815-aabd-2132-2de3-35d38603bf86",
  "IV_ZIP_CODE": "07607",
  "IV_WIS_FLAG": "X",
  "IV_STATE": "NJ",
  "IV_SERVICE_TYPE": "",
  "IV_REQUEST_DATE": "",
  "IV_OBJECT_ID": "",
  "IV_MODEL_CODE": "ET-G900VMKAVZW",
  "IV_IF_CHANEL": "",
  "IV_COUNTRY": "US",
  "IV_COMPANY": "C370",
  "IV_CITY": "MAYWOOD",
  "IV_CATEG": "",
  "IV_BP_NO": "",
  "IFID": "IF_ASC_Lookup_SVC_GCIC",
  "IFDate": "20170711183910"
}

## Response


## How to use
- limitation : A single Apex transaction can make a maximum of 100 callouts to an HTTP request or an API call.
SVC_ASCCenterLookupBatchClass batch = new SVC_ASCCenterLookupBatchClass();
batch.caseId = (Case Id from UI)
Database.executeBatch(batch, 50); // under 100 for limitation.

*/

global class SVC_ASCCenterLookupBatchClass implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts {
    public final static String SVC_ID = 'SVC-09';
    public String caseId;// for batch
    public Integer nT = 0;// Total
    public Integer nS = 0; // Succeed
    public Integer nSF = 0; // SFDC Failed
    public Integer nGF = 0; // GCIC Failed
    private String message = '';
//    private User integrationUser;

    // Controller
    webservice static string processASCBatch(Id caseId){
        Case c = [
            SELECT
                Id, RecordType.Name, Parent_Case__c, ParentId, Case_Device_Status__c, Service_Order_Number__c, Device_Type_f__c, Device__r.Status__c,
                    Shipping_Address_1__c, Shipping_City__c, Shipping_Country__c, Shipping_State__c, Shipping_Zip_Code__c,
                Model_Code__c, ASC_Code__c, ASC_Name__c, isClosed
            FROM Case WHERE Id = :caseId
        ];

        if (c.Parent_Case__c) {
            String q = 'Select count(Id) cnt  from Case where parentid = \'' + caseId + '\'';
            AggregateResult[] groupedResults = Database.query(q);
            Integer cnt = (Integer)groupedResults[0].get('cnt');
            if (cnt == 0) {
                return 'This parent case has no child.';
            }

            // Parent Case with child cases. > execute a batch to get the child cases.
            SVC_ASCCenterLookupBatchClass batch = new SVC_ASCCenterLookupBatchClass();

            batch.caseId = caseId;
            Database.executeBatch(batch, 50); // under 100 for limitation.

            return 'ASC Lookup batch have been started for Child cases ';
        }

        // 단건 처리.
        if (c.isClosed) return 'This case is already closed.';
        if (SVC_UtilityClass.isEmpty(c.Service_Order_Number__c) == false) return 'This case is already transfered.';
        if (SVC_UtilityClass.isEmpty(c.Model_Code__c)) return 'Model code on case is empty.';

        // Standalone
        if ('Verified'.equals(c.Case_Device_Status__c) == false && 'Registered'.equals(c.Device__r.Status__c) == false) return 'This case has no verified device.';
        if (SVC_UtilityClass.isEmpty(c.Shipping_Country__c)) return 'Country is empty.';
        if (c.Shipping_Country__c.length() != 2) return 'Country is must be 2 digit. i.e : United States -> US';
        if (SVC_UtilityClass.isEmpty(c.Shipping_City__c)) return 'City is empty.';
        if (SVC_UtilityClass.isEmpty(c.Shipping_State__c)) return 'State is empty.';
        if (SVC_UtilityClass.isEmpty(c.Shipping_Zip_Code__c)) return 'Zipcode is empty.';

        SVC_ASCCenterLookupBatchClass batch = new SVC_ASCCenterLookupBatchClass();
        DateTime requestTime = System.now();
        MessageLog messageLog = batch.webcallout(c);
        DateTime responseTime = System.now();
        if (messageLog.body.startsWith('ERROR')) { // http error.
            List<Message_Queue__c> mqList = new List<Message_Queue__c>();

            Message_Queue__c mq = batch.setMessageQueue(caseId, messageLog, requestTime, responseTime);
            mqList.add(mq);
            insert mqList;

            return messageLog.body;
        }

        Response response = (SVC_ASCCenterLookupBatchClass.Response)JSON.deserialize(messageLog.body, SVC_ASCCenterLookupBatchClass.Response.class);
        SVC_ASCCenterLookupBatchClass.ResponseBody body = response.body;

        List<Message_Queue__c> mqList = new List<Message_Queue__c>();

        Message_Queue__c mq = batch.setMessageQueue(caseId, response, requestTime, responseTime);
        mqList.add(mq);
        insert mqList;

        system.debug('response :: ' + JSON.serialize(response));
        String retCode = body.RET_CODE;
        if (retCode != '0') {
            String retMessage = body.RET_MESSAGE;
            return 'GCIC Error :\r\n' + retMessage;
        }

        if (body == null || body.EtBestAsc == null || body.EtBestAsc.item == null || body.EtBestAsc.item.isEmpty()) return 'No ASC from GCIC';

        List<Item> itemList = body.EtBestAsc.item;

        Item asCenter = itemList.get(0);// 다건이 나와도 최상위 ASC를 선택하여 입력한다.

        // ignore a previous ASC value. whenever user request a ASC from GCIC.(GCIC would change a ASC in hourly.)
        Boolean isUpdated = false;
        if (c.ASC_Code__c <> asCenter.AscCode) {
            c.ASC_Code__c = asCenter.AscCode;

            isUpdated = true;
        }
        if (c.ASC_Name__c <> asCenter.AscName) {
            c.ASC_Name__c = asCenter.AscName;

            isUpdated = true;
        }

        if (isUpdated) {// Update when it is only changed.
            update c;

            return 'updated'; // reload for UI
        } else {
            return 'ASC is same with current ASC';
        }
    }

    public SVC_ASCCenterLookupBatchClass(){
        // integrationUser
//        integrationUser = [Select Id, Username from User where Username like 'sfdc_if@samsung.com%' LIMIT 1];// Sandbox and Production.
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        if (caseId == null || caseId == '') return null;

        String query = '';
        query += 'SELECT  Id, RecordType.Name, CaseNumber, Parent_Case__c, ParentId, Service_Order_Number__c, Case_Device_Status__c, Device_Type_f__c, Device__r.Status__c, ';
        query += 'Shipping_Address_1__c, Shipping_City__c, Shipping_Country__c, Shipping_State__c, Shipping_Zip_Code__c, ';
        query += 'Model_Code__c, ASC_Code__c, ASC_Name__c, isClosed ';
        query += 'FROM Case WHERE ParentId = \'' + caseId + '\' ';
//        query += 'AND (Case_Device_Status__c = \'Verified\' OR Device__r.Status__c = \'Registered\')';

        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Case> scope) {
        // insert ASC
        List<Message_Queue__c> mqList = new List<Message_Queue__c>();
        Map<String, Item> dupMap = new Map<String, Item>();
        List<Case> caseList = new List<Case>();
        for (Case c : scope) {
            nT++;

            if (c.isClosed) {
                nSF++;
                if (nSF + nGF < 100) message += c.CaseNumber + ' : SFDC, This case is already closed.\r\n';
                continue;
            }

            if (SVC_UtilityClass.isEmpty(c.Service_Order_Number__c) == false) {
                nSF++;
                if (nSF + nGF < 100) message += c.CaseNumber + ' : SFDC, This case is already transfered.\r\n';
                continue;
            }

            if (SVC_UtilityClass.isEmpty(c.Model_Code__c)) {
                nSF++;
                if (nSF + nGF < 100) message += c.CaseNumber + ' : SFDC, Model code on case is empty.\r\n';
                continue;
            }

            // Standalone
            if ('Verified'.equals(c.Case_Device_Status__c) == false && 'Registered'.equals(c.Device__r.Status__c) == false) {
                nSF++;
                if (nSF + nGF < 100) message += c.CaseNumber + ' : SFDC, This case has no verified device.\r\n';
                continue;
            }
            if (SVC_UtilityClass.isEmpty(c.Shipping_Country__c)) {
                nSF++;
                if (nSF + nGF < 100) message += c.CaseNumber + ' : SFDC, Country is empty.\r\n';
                continue;
            }
            if (c.Shipping_Country__c.length() != 2) {
                nSF++;
                if (nSF + nGF < 100) message += c.CaseNumber + ' : SFDC, Country is must be 2 digit. i.e : United States -> US\r\n';
                continue;
            }
            if (SVC_UtilityClass.isEmpty(c.Shipping_City__c)) {
                nSF++;
                if (nSF + nGF < 100) message += c.CaseNumber + ' : SFDC, City is empty.\r\n';
                continue;
            }
            if (SVC_UtilityClass.isEmpty(c.Shipping_State__c)) {
                nSF++;
                if (nSF + nGF < 100) message += c.CaseNumber + ' : SFDC, State is empty.\r\n';
                continue;
            }
            if (SVC_UtilityClass.isEmpty(c.Shipping_Zip_Code__c)) {
                nSF++;
                if (nSF + nGF < 100) message += c.CaseNumber + ' : SFDC, Zipcode is empty.\r\n';
                continue;
            }

            String key = c.Shipping_Zip_Code__c + '|' + c.Device_Type_f__c;// 07607|Connected

            Boolean isUpdated = false;
            Item keyASC = dupMap.get(key);
            if (keyASC == null) {
                DateTime requestTime = System.now();
                MessageLog messageLog = webcallout(c);
                DateTime responseTime = System.now();

                if (messageLog.body.startsWith('ERROR')) { // http error.
                    Message_Queue__c mq = setMessageQueue(caseId, messageLog, requestTime, responseTime);
                    mqList.add(mq);

                    continue;
                }

                Response response = (SVC_ASCCenterLookupBatchClass.Response)JSON.deserialize(messageLog.body, SVC_ASCCenterLookupBatchClass.Response.class);

                Message_Queue__c mq = setMessageQueue(c.Id, response, requestTime, responseTime);
                mqList.add(mq);

                SVC_ASCCenterLookupBatchClass.ResponseBody body = response.body;
                system.debug('response :: ' + JSON.serialize(response));
                String retCode = body.RET_CODE;
                if (retCode == '0') { // success
                    if (body.EtBestAsc == null) continue;

                    List<Item> itemList = body.EtBestAsc.item;

                    if (itemList == null || itemList.isEmpty()) continue;

                    Item asCenter = itemList.get(0);// 다건이 나와도 최상위 ASC를 선택하여 입력한다.

                    c.ASC_Code__c = asCenter.AscCode;
                    c.ASC_Name__c = asCenter.AscName;

                    dupMap.put(key, asCenter);

                    nS++;
                } else { // error
                    String retMessage = body.RET_MESSAGE;

                    c.ASC_Code__c = '';
                    c.ASC_Name__c = '[GCIC Error : ' + retMessage + ']';

                    message += c.CaseNumber + ' : GCIC, ' + retMessage + '\r\n';

                    nGF++;
                }
            } else {
                // ignore a previous ASC value. whenever user request a ASC from GCIC.(GCIC would change a ASC in hourly.)
                c.ASC_Code__c = keyASC.AscCode;
                c.ASC_Name__c = keyASC.AscName;

                nS++;
            }

            caseList.add(c);
        }

        if (caseList.isEmpty() == false) {
            update caseList;
        }

        if (mqList.isEmpty() == false) {
            insert mqList;
        }
    }

    global void finish(Database.BatchableContext BC) {
        system.debug('finish Call.');

        String title = 'ASC Lookup Batch Result';

        String chatterMessage = title + '\r\n\r\nTotal : ' + nT + ', Succeed : ' + nS + ', SFDC Failed : ' + nSF + ', GCIC Failed : ' + nGF;
        if (nSF + nGF > 0) {
            chatterMessage += '\r\n\r\nPlease check your mail.';
        }

        SVC_UtilityClass.postToChatter(caseId, chatterMessage);

        if (nSF + nGF > 0) {
            String summary = title + '\r\n\r\nTotal : ' + nT + ', Succeed : ' + nS + ', SFDC Failed : ' + nSF + ', GCIC Failed : ' + nGF;
            String body = summary + '\r\n\r\n==Failed list==\r\n' + message;

            SVC_UtilityClass.sendEmail(title, title, body);
        }
    }

    private MessageLog webcallout(Case c) {
        string layoutId = Integration_EndPoints__c.getInstance(SVC_ID).layout_id__c;

        Request request = new Request();
        request.inputHeaders = new cihStub.msgHeadersRequest(layoutId);

        // pseudo
        SVC_GCICJSONRequestClass.ASCLookupJSON body = new SVC_GCICJSONRequestClass.ASCLookupJSON();

        String company;

        if ('Connected'.equals(c.Device_Type_f__c)) {
            company = 'C370';
        } else if ('WiFi'.equals(c.Device_Type_f__c)) {
            company = 'C310';
        }

        body.IV_COMPANY = company;//'C370';
        body.IV_COUNTRY = c.Shipping_Country__c;//'US';
        body.IV_BP_NO = '';
        body.IV_CITY = c.Shipping_City__c;//'MAYWOOD';
        body.IV_STATE = c.Shipping_State__c;//'NJ';
        body.IV_ZIP_CODE = c.Shipping_Zip_Code__c;//'07607';
        body.IV_MODEL_CODE = c.Model_Code__c;//'ET-G900VMKAVZW';
        body.IV_SERVICE_TYPE = 'PS';
        body.IV_REQUEST_DATE = '';
        body.IV_IF_CHANEL = '';
        body.IV_OBJECT_ID = '';
        body.IV_CATEG = '';

//        String wisFlag = '';
//        if ('C370'.equals(company)) {
//            wisFlag = 'X';
//        }
        body.IV_WIS_FLAG = '';

        request.body = body;
        String serialized = JSON.serialize(request);
        system.debug('request :: ' + serialized);

        string partialEndpoint = Integration_EndPoints__c.getInstance(SVC_ID).partial_endpoint__c;

        System.Httprequest req = new System.Httprequest();

        req.setMethod('POST');
        req.setBody(serialized);
        req.setEndpoint('callout:X012_013_ROI' + partialEndpoint);
        req.setTimeout(120000);

        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Accept', 'application/json');

        Http http = new Http();
        HttpResponse res = http.send(req);
        system.debug('res.getStatusCode() :: ' + res.getStatusCode());

        MessageLog messageLog = new MessageLog();

        Integer statusCode = res.getStatusCode();
        messageLog.status = statusCode;
        if (statusCode != 200) {
            messageLog.body = 'ERROR : http status code = ' +  statusCode;
            messageLog.MSGGUID = request.inputHeaders.MSGGUID;
            messageLog.IFID = request.inputHeaders.IFID;
            messageLog.IFDate = request.inputHeaders.IFDate;

            return messageLog;//'ERROR : http status code = ' +  statusCode;
        }

        messageLog.body = res.getBody();
        system.debug('response :: ' + messageLog.body);

        return messageLog;
    }

    // http error save.
    private Message_Queue__c setMessageQueue(Id caseId, MessageLog messageLog, Datetime requestTime, Datetime responseTime) {
        Message_Queue__c mq = new Message_Queue__c();

        mq.ERRORCODE__c = messageLog.body;
        mq.ERRORTEXT__c = messageLog.body;
        mq.IFDate__c = messageLog.IFDate;
        mq.IFID__c = messageLog.IFID;
        mq.Identification_Text__c = caseId; //Object Id(=CaseId)
        mq.Initalized_Request_Time__c = requestTime;//Datetime
        mq.Integration_Flow_Type__c = SVC_ASCCenterLookupBatchClass.SVC_ID;
        mq.Is_Created__c = true;
        mq.MSGGUID__c = messageLog.MSGGUID;
        mq.MSGUID__c = messageLog.MSGGUID;
//        mq.MSGSTATUS__c = messageLog.MSGSTATUS;
        mq.Object_Name__c = 'Case';//picklist
        mq.Request_To_CIH_Time__c = requestTime.getTime();//Number(15, 0)
        mq.Response_Processing_Time__c = responseTime.getTime();//Number(15, 0)
        mq.Status__c = 'failed'; // picklist

        return mq;
    }

    private Message_Queue__c setMessageQueue(Id caseId, Response response, Datetime requestTime, Datetime responseTime) {
        cihStub.msgHeadersResponse inputHeaders = response.inputHeaders;
        SVC_ASCCenterLookupBatchClass.ResponseBody body = response.body;

        Message_Queue__c mq = new Message_Queue__c();

        mq.ERRORCODE__c = 'CIH :' + nvl(inputHeaders.ERRORCODE) + ', GCIC :' + nvl(body.RET_CODE);
        mq.ERRORTEXT__c = 'CIH :' + nvl(inputHeaders.ERRORTEXT) + ', GCIC :' + nvl(body.RET_MESSAGE);
        mq.IFDate__c = inputHeaders.IFDate;
        mq.IFID__c = inputHeaders.IFID;
        mq.Identification_Text__c = caseId; //Object Id(=CaseId)
        mq.Initalized_Request_Time__c = requestTime;//Datetime
        mq.Integration_Flow_Type__c = SVC_ASCCenterLookupBatchClass.SVC_ID;
        mq.Is_Created__c = true;
        mq.MSGGUID__c = inputHeaders.MSGGUID;
        mq.MSGUID__c = inputHeaders.MSGGUID;
        mq.MSGSTATUS__c = inputHeaders.MSGSTATUS;
        mq.Object_Name__c = 'Case';//picklist
        mq.Request_To_CIH_Time__c = requestTime.getTime();//Number(15, 0)
        mq.Response_Processing_Time__c = responseTime.getTime();//Number(15, 0)
        mq.Status__c = (body.RET_CODE == '0') ? 'success' : 'failed';

        return mq;
    }

    private String nvl(String s) {
        return (s == null) ? '' : s;
    }

    private class MessageLog {
        public Integer status;
        public String body;
        public String MSGGUID;
        public String IFID;
        public String IFDate;
        public String MSGSTATUS;
        public String ERRORCODE;
        public String ERRORTEXT;
    }

    private class Request {
        public cihStub.msgHeadersRequest inputHeaders; // variable name is must be inputHeaders. Fixed.
        public SVC_GCICJSONRequestClass.ASCLookupJSON body;
    }

    private class Response {
        cihStub.msgHeadersResponse inputHeaders;
        ResponseBody body;
    }

    private class ResponseBody {
        public String RET_CODE;
        public String RET_MESSAGE;

        EtBestAsc EtBestAsc;
    }

    private class EtBestAsc {
        List<Item> item;
    }

    private class Item {
        public String Company;
        public String AscCode;
        public String AscName;
        public String AscType;
        public String Capacity;
        public String Sla;
        public String Express;
        public String Grade;
        public String Score;
        public String Distance;
        public String Duration;
        public String FromX;
        public String DateX;
    }

}