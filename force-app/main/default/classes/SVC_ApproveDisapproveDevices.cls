public without sharing class SVC_ApproveDisapproveDevices {
    public Integer cnt{get;set;}
    public Boolean singleAccount{get;set;}
    public Integer totalRecords{get; set;}
    Public Integer noOfSelected{get; set;}
    Public Map<ID,ID> selectedDeviceMap = new Map<ID,ID>();
    public string approvalOrDisapproval {get;set;} 

    public List<deviceWrapper> selectedDeviceWrapperList {
      get {
          selectedDeviceWrapperList = new List<deviceWrapper>();
            for (deviceWrapper d:devices){
                if (selectedDeviceMap.containsKey(d.device.id)){
                    selectedDeviceWrapperList.add(d);
                }
            }

            return selectedDeviceWrapperList;
        }
        set;
    }
    public List<Device__c> selectedDeviceList {
        get {
            selectedDeviceList = 
            [select id,imei__c from Device__c where id in:selectedDeviceMap.keySet()];
            /*selectedCaselist = new List<Case>();
            for (ChildCaseWrapper c:childCases){
                if (c.selected){
                    selectedCaselist.add(c.childCase);
                }
            }*/

            return selectedDeviceList;
        }
        set;
    }

    public List<Account> SamAccounts{
        get {

            if (SamAccounts == null){
                SamAccounts = new List<account>();
                SamAccounts = [select id,name from account where 
                Service_Account_Manager__c =: UserInfo.getUserId()];

            }
        return SamAccounts;
        }

    set;}

    public String selectedParAccountID {get;set;}

    public List<Entitlement> entitlementLst {
    get {
      if (entitlementLst == null) {

        entitlementLst = new List<Entitlement>();
        if (AccountId != null){
            entitlementLst = [select id,name from entitlement where accountid =:Accountid];
        }else if (selectedParAccountID != null){
            entitlementLst = [select id,name from entitlement where accountid =:selectedParAccountID];
        }

      }
      return entitlementLst;         
    }
    set;
  }


    public String SamAccountStr{get;set;}

    public string AccountId {get;set;}

    public String AccountName{get;set;}    
    private String soql {get;set;}
    private String soqlCommon {
        get {
            if (soqlCommon == null){
                soqlCommon = 'select id, name,account__r.name,account__c,Carrier__c,date_verified__c,Device_Type_f__c,';
                soqlCommon +='Entitlement__c,Entitlement__r.name,IMEI__c,Is_Active__c,IsBulkUpload__c,Model_Name__c,Purchase_Date__c,';
                soqlCommon +='Reference_Number__c,Serial_Number__c,Status__c from device__c ';
                soqlCommon +='where Status__c = \'Verified\' and Is_Active__c = \'Yes\'';
            }
        return soqlCommon;
        }

    set;}

    private String soqlWhere {get;set;}
    public List<deviceWrapper> devices {get;set;}
    public Boolean showConfirmationMsg {get; set;}

    public Boolean showRecordWarningMsg {get; set;}
    
    public String sortDir {
        get  { if (sortDir == null) {  sortDir = 'asc'; } return sortDir;  }
        set;
    }

  // the current field to sort by. defaults to last name
  public String sortField {
    get  { if (sortField == null) {sortField = 'name'; } return sortField;  }
    set;
  }

  // format the soql for display on the visualforce page
  public String debugSoql {
    get { return approvalOrDisapproval + ' ' +soql + ' order by ' + sortField + ' ' + sortDir + ' limit 1000'; }
    set;
  }


public SVC_ApproveDisapproveDevices(ApexPages.StandardController controller) {
        AccountId = ApexPages.currentPage().getParameters().get('accid');
        String entitlementId = ApexPages.currentPage().getParameters().get('entId');
        if (AccountId == null && (SamAccounts==null || SamAccounts.size()==0)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'You are not a SAM on any account. Please click Account tab and chose an account to manage the devices. '));
        }else if (AccountId != null){
            AccountName = [select name from Account where id=:AccountId].name;
            singleAccount = true;
            soqlCommon += 'and account__c = \''+AccountId+'\'';
            soql = soqlCommon;
            soqlWhere ='and account__c = \''+AccountId+'\' ';
            runQuery();
        }else{
            singleAccount = false;
            List<String> accIds = new List<String>();
            for (Account a:SamAccounts){
                accIds.add('\''+a.id+'\'');
            }

            SamAccountStr = string.join(accIds,',');
            soqlCommon += 'and account__c in ('+SamAccountStr+')';
            soql = soqlCommon;
            soqlWhere += 'and account__c in ('+SamAccountStr+')';
            runQuery();
        }


    }

    // runs the actual query
  public void runQuery() {
    selectedDeviceMap.clear();
    noOfSelected = 0;
    showRecordWarningMsg = false;
    devices = new List<deviceWrapper>();

    try {
     // system.debug(soql + ' order by ' + sortField + ' ' + sortDir + ' limit 20000');
      List<Device__c> deviceRS = Database.query(soql + ' order by ' + sortField + ' ' + sortDir + ' limit 1000');
      for (Device__c d:deviceRS){

        devices.add(new deviceWrapper(d,false,false));
      }
      totalRecords = devices.size();
      if (devices.size() >= 20000){
        showRecordWarningMsg = true;
      }

    } catch (Exception e) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
    }

  }

  public PageReference runSearchByAcct() {
    String parAccountId = Apexpages.currentPage().getParameters().get('parAccountId');
    String parEntitlementId = Apexpages.currentPage().getParameters().get('parEntitlementId');
    soql = soqlCommon;
    if (!parAccountId.equals('')&&!parAccountId.equals('--')){
        soql += ' and account__c = \''+parAccountId+'\'';
        selectedParAccountID = parAccountId;
        entitlementLst = new List<Entitlement>();
        
        entitlementLst = [select id,name from entitlement where accountid =:selectedParAccountID];
        
    }else{
        entitlementLst = new List<Entitlement>();
    }

    //if (!parEntitlementId.equals('')&&!parEntitlementId.equals('--'))
    //  soql += ' and entitlement__c = \''+parEntitlementId+'\'';

    runQuery();
    return null;
  }  

  public PageReference runSearchByEntitlement() {
    String parAccountId = Apexpages.currentPage().getParameters().get('parAccountId');
    String parEntitlementId = Apexpages.currentPage().getParameters().get('parEntitlementId');
    soql = soqlCommon;
    if (!parAccountId.equals('')&&!parAccountId.equals('--')){
        soql += ' and account__c = \''+parAccountId+'\'';
        selectedParAccountID = parAccountId;
        entitlementLst = new List<Entitlement>();
        
        entitlementLst = [select id,name from entitlement where accountid =:selectedParAccountID];
        
    }else{
        entitlementLst = new List<Entitlement>();
    }

    if (!parEntitlementId.equals('')&&!parEntitlementId.equals('--'))
      soql += ' and entitlement__c = \''+parEntitlementId+'\'';

    runQuery();
    return null;
  } 
  

  // use apex describe to build the picklist values
  

  public class deviceWrapper {
        public Device__c device {get; set;}
        public Boolean selected {get; set;}
        public Boolean currentPage{get;set;}


        public deviceWrapper(Device__c d,Boolean bool,Boolean bool2) {
            device = d;
            selected = bool;
            currentPage = bool2;
        }
    }

  public PageReference updateAll(){
    selectedDeviceMap.clear();
    for ( deviceWrapper dw:devices){
      if (dw.device.status__c == 'Verified')  
        selectedDeviceMap.put(dw.device.id,dw.device.id);  
    }
    PageReference pg = UpdateSelected();
    return pg;
  }  
  

  public PageReference UpdateSelected(){
    if (selectedDeviceMap.size() <=0){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'Please select at least one device.'));
        return null;
    }

    string status = '';
    if (approvalOrDisapproval == 'approval'){

        status = 'Registered';
    }
    else if (approvalOrDisapproval == 'disapproval'){
        status = 'Approval Failed';
    }
    else{
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Failed to set the approval/disapproval flag.'));
        return null;
    }

    List<Device__c> toUpdate = [select id,status__c from device__c where id 
                        in:selectedDeviceMap.keySet()];
    for (Device__c d:toUpdate){
        d.status__c = status;
    }
    try{
              update toUpdate;

              for ( deviceWrapper dw:devices){
                //system.debug('before casenumber: ' + c.childCase.casenumber + ' ' +c.selected);
                if (selectedDeviceMap.containsKey(dw.device.id)){
                    //system.debug('YES casenumber: ' + c.childCase.casenumber );
                    dw.selected = false;
                    dw.device.status__c = status;
                }

                //system.debug('After casenumber: ' + c.childCase.casenumber + ' ' +c.selected);
             }
             totalRecords = totalRecords-selectedDeviceMap.size();

             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, selectedDeviceMap.size() +' devices were updated to '+status));
             selectedDeviceMap.clear();
             noOfSelected  = 0;
             return null;

        } catch (DMLException e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
    }
    return null;
  }

  public void checkboxClicked(){
        String deviceID = Apexpages.currentPage().getParameters().get('clickedDeviceID');
        String selected = Apexpages.currentPage().getParameters().get('selected');
        if (selected == 'true'){
            if (!selectedDeviceMap.containsKey(deviceID)){
                selectedDeviceMap.put(deviceID, deviceID);
            }
        }else{
            if (selectedDeviceMap.containsKey(deviceID)){
                selectedDeviceMap.remove(deviceID);
            }

        }
        noOfSelected = selectedDeviceMap.size();
    }
   
    
}