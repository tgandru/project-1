global with sharing class SVC_CancelCaseClass {
    
    webservice static String cancelCases(String caseId)
    {

        System.debug(caseId);
        Map<Id, Case> caseMap  = new Map<Id, Case>([SELECT Status, Sub_Status__c, CaseNumber 
                               FROM Case 
                               WHERE (Id =: caseId 
                               OR ParentId =: caseId)
                               AND Status != 'Cancelled']);

        for(Case c: caseMap.values())
        {
            c.Status = 'Cancelled';
            c.Sub_Status__c = 'Cancelled by Agent';
        }

        try{
            Set<Id> updatedCases = new Set<Id>();

            List<Database.SaveResult> updateResults = Database.update(caseMap.values(), true);

            for(Integer i = 0; i < updateResults.size(); i++){
                if(updateResults.get(i).isSuccess()) updatedCases.add(updateResults.get(i).getId());
                else throw new DmlException('Error: ' + updateResults.get(i).getId());
            }

            if(updatedCases.size() > 0) SVC_CancelCasesBatchClass.processBatchClass(updatedCases);
            return 'Case has been cancelled';
        }catch(DmlException de)
        {
            return 'Error on Case ' + caseMap.get(de.getDmlId(0)).CaseNumber + ': ' + de.getDmlMessage(0);
        }
    }

    public static void sendStatus(String serviceOrderNumber, String caseNumber, String recordType){

        cihStub.msgHeadersRequest cih = new cihStub.msgHeadersRequest('IF_SVC_07');

        //create the object and its attributes to send to the endpoint
        request sts = new request();
        inputHeaders ih = new inputHeaders();
        requestBody bd = new requestBody();
        ih.MSGGUID = cih.MSGGUID;
        ih.IFID = cih.IFID;
        ih.IFDate = cih.IFDate;
        bd.Service_Ord_no = serviceOrderNumber;
        bd.Case_NO = caseNumber;
        bd.Ticket_status = recordType == 'Repair' ? 'ST051' : 'ES080';
        sts.inputHeaders = ih;
        sts.body = bd;

        //serialize the object to json format
        String jsonBody = JSON.serialize(sts);
        System.debug('@@@ JSON Body: ' + jsonBody);

        //make the rest callout
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();

        string partialEndpoint = Integration_EndPoints__c.getInstance('SVC-07').partial_endpoint__c;

        req.setHeader('Accept', 'application/json');
        req.setHeader('Content-Type', 'application/json');
        req.setTimeout(120000);
        req.setMethod('POST');
        req.setEndpoint('callout:X012_013_ROI'+partialEndpoint);
        req.setBody(jsonBody);

        String result = '';
        try{
            res = h.send(req);
            result = res.getBody();

            System.debug('@@@ Result: ' + result);
            if(res.getStatusCode() != 200) throw new CalloutException();
        }catch(CalloutException ex){
            System.debug('Result Status and Code: ' + res.toString() + '. System Exception: ' + ex.getMessage() + ' on line number ' + ex.getLineNumber());
            return;
        }

        response response = (SVC_CancelCaseClass.response)JSON.deserialize(result, SVC_CancelCaseClass.response.class);
        SVC_CancelCaseClass.responseHeaders resHead = response.inputHeaders;
        SVC_CancelCaseClass.responseBody resBody = response.body;

        //create the message queue record
        Message_Queue__c mq = new Message_Queue__c();
        mq.Status__c = resBody.Ret_code == '1' ? 'failed' : 'success';
        mq.IFID__c = cih.IFID;
        mq.MSGGUID__c = cih.MSGGUID;
        mq.IFDate__c = cih.IFDate;
        mq.Integration_Flow_Type__c = 'IF_SVC_07';
        mq.MSGSTATUS__c = resHead.MSGSTATUS;
        mq.Object_Name__c = 'Case';
        mq.Identification_Text__c = caseNumber;

        try{
            insert mq;
        }catch(DmlException e){
            System.debug('Error inserting message queue record: ' + e.getMessage() + e.getCause() + e.getDmlFields(0));
        }

    }

    class request {
        inputHeaders inputHeaders;
        requestBody body;
    }

    class inputHeaders {
        String MSGGUID;
        String IFID;
        String IFDate;
    }

    class requestBody {
        String Service_Ord_no;
        String Case_NO;
        String Ticket_status;
    }

    class response {
        responseHeaders inputHeaders;
        responseBody body;
    }

    class responseHeaders {
        String MSGGUID;
        String IFID;
        String IFDate;
        String MSGSTATUS;
        String ERRORTEXT;
    }

    class responseBody {
        String Ret_code;
        String Ret_message;
    }
}