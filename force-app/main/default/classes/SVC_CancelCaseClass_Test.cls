@isTest
private class SVC_CancelCaseClass_Test {
    
    @isTest static void CancelCaseClassTest() {
        
        //Select the case exchange record type
        RecordType CaseRT = [SELECT Name,SobjectType FROM RecordType WHERE SobjectType = 'Case' AND DeveloperName = 'Exchange'];
        
        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];

        Integration_EndPoints__c ie = new Integration_EndPoints__c(Name = 'SVC-07', endPointURL__c = 'www.samsung.com', layout_id__c = 'this', partial_endpoint__c = '/test');
        insert ie;

        Account testAccount = new Account (
            Name = 'TestAcc1',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        );

        insert testAccount;

        // Insert Contact 
        Contact contact = new Contact(); 
        contact.AccountId = testAccount.id; 
        contact.Email = 'svcTest@svctest.com'; 
        contact.FirstName = 'FirstName'; 
        contact.LastName = 'Named Caller'; 
        contact.Named_Caller__c = true;
        contact.MailingState = 'NJ';
        contact.MailingCountry = 'US';
        insert contact; 
        
        Case c1 = new Case(
            AccountId = testAccount.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com',
            //Service_Order_Number__c = '4100167843',
            Origin = 'Email',
            contactid=contact.id,
            RecordTypeId = caseRT.id,
            status='New',
            Parent_Case__c = true
            );
        insert c1;
        c1.Status = 'Cancelled';
        update c1;
        
        Test.startTest();

        Test.setMock(HttpCalloutMock.class, new cancelCase200());
        
        String res = SVC_CancelCaseClass.cancelcases(c1.id);
        System.debug('res :: ' + res);
        
        SVC_CancelCaseClass.sendStatus('1', '0000001', 'Repair');
        //System.assertEquals('New',c1.status);

        Test.stopTest();
        
    }

    class cancelCase200 implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            String jsonBody = '{"inputHeaders":{"MSGGUID":"70b7780f-b4bd-be38-ac69-470f9729ea84","IFID":"IF_SVC_07","IFDate":"20170804035610","MSGSTATUS":"S","ERRORTEXT":null},"body":{"ItComments":null,"Ret_code":"0","Ret_message":"[Status Update : Success]"}}';
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody(jsonBody);
            res.setStatusCode(200);
            return res;
        }
    }

}