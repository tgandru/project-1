/**
 * Created by ryan on 8/2/2017.
 */

global class SVC_CancelCasesBatchClass implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts {

    public Set<Id> caseIds;

    public static void processBatchClass(Set<Id> cases){
        if(cases.size() > 0){
            SVC_CancelCasesBatchClass batch = new SVC_CancelCasesBatchClass();

            batch.caseIds = new Set<Id>();
            batch.caseIds.addAll(cases);

            Database.executeBatch(batch, 50);
        }
    }

    global Database.QueryLocator start(Database.BatchableContext ctx){//get the cases with the cancelled status
        String query = 'SELECT Id, Service_Order_Number__c, CaseNumber, Status, RecordType.DeveloperName';
        query += ' FROM Case WHERE Id IN: caseIds';
        query += ' AND Service_Order_Number__c != null AND CaseNumber != null AND Status != null';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext ctx, List<sObject> scope){

        List<Case> caseList = (List<Case>)scope;//our case list

        for(Case c: caseList){
            if(c.RecordType.DeveloperName == 'Exchange' || c.RecordType.DeveloperName == 'Repair')
                SVC_CancelCaseClass.sendStatus(c.Service_Order_Number__c, c.CaseNumber, c.RecordType.DeveloperName);
        }
    }

    global void finish(Database.BatchableContext ctx){}

}