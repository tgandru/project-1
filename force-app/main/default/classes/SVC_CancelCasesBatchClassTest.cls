@isTest
private class SVC_CancelCasesBatchClassTest
{
    @isTest
    static void TestCancelCasesBatchClass()
    {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }
        Profile adminProfile = [SELECT Id FROM Profile Where Name ='B2B Service System Administrator'];

        User admin1 = new User (
            FirstName = 'admin1',
            LastName = 'admin1',
            Email = 'admin1seasde@example.com',
            Alias = 'admint1',
            Username = 'admin1seaa@example.com',
            ProfileId = adminProfile.Id,
            TimeZoneSidKey = 'America/New_York',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US'
        );
        
        insert admin1;

        BusinessHours bh24 = [SELECT Id FROM BusinessHours WHERE name = 'B2B Service Hours 24x7'];
        BusinessHours bh12 = [SELECT Id FROM BusinessHours WHERE name = 'B2B Service Hours 12x5'];
        
        RecordType caseRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Technical Support' 
            AND SobjectType = 'Case'];
        

        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );

        insert testAccount1;

        Account testAccount2 = new Account (
            Name = 'TestAcc2',
            RecordTypeId = accountRT.Id,
            BillingStreet = '411 E Brinkerhoff Ave, Palisades Park',
            BillingCity ='Palisades Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07650'
            
        );

        insert testAccount2;

        RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];
        Product2 prod1 = new Product2(
                name ='MI-test',
                SAP_Material_ID__c = 'MI-test',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = productRT.id
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Asset ast2 = new Asset(
            name ='Assert-MI-Test2',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast2;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            BusinessHoursId = bh12.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent;

        // Insert Contact 
        Contact contact = new Contact(); 
        contact.AccountId = testAccount1.id; 
        contact.Email = 'svcTest@svctest.com'; 
        contact.LastName = 'Named Caller'; 
        contact.Named_Caller__c = true;
        contact.FirstName = 'Named Caller';
        contact.MailingStreet = '1234 Main';
        contact.MailingCity = 'Chicago';
        contact.MailingState = 'IL';
        contact.MailingCountry = 'US';

        insert contact; 

        // Insert Contact non-named caller 
        Contact contact1 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest2@svctest.com', 
            LastName = 'Named Caller2',
            Named_Caller__c = true,
            MailingStreet = '1234 Main',
            MailingCity = 'Chicago',
            MailingState = 'IL',
            MailingCountry = 'US',
            FirstName = 'Named Caller2');
        insert contact1; 

        // Insert Contact no entitlement account 
        Contact contact2 = new Contact( 
            AccountId = testAccount2.id, 
            Email = 'svcTest3@svctest.com', 
            LastName = 'Non Entitlement',
            Named_Caller__c = true,
            MailingStreet = '1234 Main',
            MailingCity = 'Chicago',
            MailingState = 'IL',
            MailingCountry = 'US',
            FirstName = 'Named Caller2');
        insert contact2; 
        
        List<Case> cases = new List<Case>();
        Case c1 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact.id,
            RecordTypeId = caseRT.id,
            status='New'
            );
        insert c1;
        //cases.add(c1);
        //insert cases;
        Test.startTest();
        Case c2 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 non named caller', 
            reason = 'test2@test.com', 
            Origin = 'Phone',
            contactid=contact1.id,
            RecordTypeId = caseRT.id,
            status='New',
            parentid=c1.id,
            ownerid=admin1.id
            );
        insert c2;

        Group q= [select id from group where type='Queue' limit 1 ];

                
        Case c3 = new Case(
            AccountId = testAccount2.id,
            Subject = 'Case3 Spam', 
            reason = 'test3@test.com', 
            Origin = 'Web',
            contactid=contact2.id,
            RecordTypeId = caseRT.id,
            status='New',
            ownerid=q.id
            );
        insert c3;

        Case c4 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Repair'),
            status='New',
            Imei__c = '11111111111111'
            );
        insert c4;
        
        List<Case> cUpdate4 = [select Id, Service_Order_Number__c, CaseNumber, Status, RecordType.DeveloperName from case where id=:c3.id];
        
        Set<Id> cIds = new Set<Id>();
        for (Case ccc:cUpdate4){
            cIds.add(ccc.id);
        }

        SVC_CancelCasesBatchClass batch = new SVC_CancelCasesBatchClass();

        SVC_CancelCasesBatchClass.processBatchClass(cIds);

        batch.execute(null, cUpdate4);

        message_queue__c mq  = SVC_GCICIntegrationHelper.createMessageQueue();

        SVC_ReplacementDeviceWrapper dr = new SVC_ReplacementDeviceWrapper('123456789011111',null,'R-12345');

        List<Device__c> devs = new List<Device__c>();
        //15 digit imei
        device__c dev1 = new device__c(
            account__c = testAccount1.id,
            imei__c = '123456789012341',
            entitlement__c=ent.id
            
        );
       insert dev1;

       Map<id,SVC_ReplacementDeviceWrapper> rMap = new Map<id,SVC_ReplacementDeviceWrapper>();
       rMap.put(dev1.id,dr);
       SVC_ReplacementDeviceHelper.replaceDevices(rMap);

        Test.stopTest();

    }
}