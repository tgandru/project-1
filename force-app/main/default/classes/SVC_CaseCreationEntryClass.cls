//JeongHo Lee ChildCase Creation
public with sharing class SVC_CaseCreationEntryClass {

    public Id                       parentId            {get;set;}
    public String                   allfields           {get;set;}
    public Case                     caStd               {get;set;}
    public String                   rtName              {get;set;}
    public List<DataWrapper>        dataList            {get;set;}
    public Integer                  rowNo               {get;set;}
    public Map<String,Integer>      imeiMap             {get;set;}
    public Map<String,Integer>      serialMap           {get;set;}
    public Set<String>              imeiSet             {get;set;}
    public Set<String>              serialSet           {get;set;}
    public Map<String,String>       caseIMap            {get;set;}
    public Map<String,String>       caseSMap            {get;set;}
    //public Set<String>                devImeiSet          {get;set;}
    //public Set<String>                devSerialSet        {get;set;}
    public Map<String, Device__c>   devImeiMap          {get;set;}
    public Map<String, Device__c>   devSerialMap        {get;set;}
    public String                   caseNumber          {get;set;}
    public Boolean                  doDisplay           {get;set;}
    public Boolean                  insertError         {get;set;}
    public Boolean                  insertSuccess       {get;set;}

    public SVC_CaseCreationEntryClass() { 
        parentId    = Id.valueOf(ApexPages.currentPage().getParameters().get('id'));
        rtName      = String.valueOf(ApexPages.currentPage().getParameters().get('caseRecordType'));
        caseNumber  = String.valueOf(ApexPages.currentPage().getParameters().get('caseNumber'));

        rowNo = 0; 
        doDisplay = true;

        if(rtName == 'Exchange')
            doDisplay = false;

        Map<String, Schema.SObjectField> fields = Schema.getGlobalDescribe().get('Case').getDescribe().SObjectType.getDescribe().fields.getMap();
  
        List<String> accessiblefields = new List<String>();
  
        for(Schema.SObjectField field : fields.values()){
            if(field.getDescribe().isAccessible())
                accessiblefields.add(field.getDescribe().getName());
        }
  
        allfields='';
  
        for(String fieldname : accessiblefields)
            allfields += fieldname+',';
  
        allfields = allfields.subString(0,allfields.length()-1);

        dataList = new List<DataWrapper>();
        if(parentId != null){
            String soqlQuery = '';
            soqlQuery +=    ' SELECT '      ;
            soqlQuery +=    allfields       ;
            soqlQuery +=    ' ,Entitlement.Status, RecordType.Name ';
            soqlQuery +=    ' FROM Case '   ;
            soqlQuery +=    'WHERE Id = \'' + parentId + '\' ';
            caStd = DataBase.query(soqlQuery);

            for(Integer i = 0; i < 10; i++){
                DataWrapper dt      = new DataWrapper();
                Case ca             = new Case();
                ca                  = caStd.clone();
                ca.ParentId         = parentId;
                ca.IMEI__c          = null;
                ca.SerialNumber__c  = null;
                ca.Description = null;
                ca.Issue_Description__c  = null;//Added by Thiru
                ca.Shipping_Address_Controlled_By__c = 'Parent Case';
                if(caStd.RecordType.Name == 'Repair')
                    ca.Model_Code_Repair__c = null;
                dt.ca               = ca;
                dt.rowNo            = ++rowNo;
                dataList.add(dt);
            }
        }
    }
    public Pagereference doSave() {
        if(this.isValidation() == true){
            Boolean isCheck = true;
            List<Case> insertList = new List<Case>();
            Set<String> modelNames = new Set<String>();
            Set<String> existingModelNames = new Set<String>();

            if(doDisplay == true){
                for(DataWrapper dt : dataList){
                    if(String.isNotBlank(dt.ca.Model_Code_Repair__c))
                        modelNames.add(dt.ca.Model_Code_Repair__c);
                }

                if(!modelNames.isEmpty())
                {
                    List<Product2> productList = [SELECT SAP_Material_Id__c
                                                    FROM Product2
                                                    WHERE SAP_Material_Id__c IN: modelNames
                                                      AND SAP_Material_Id__c != null];

                    for(Product2 p2: productList)
                        existingModelNames.add(p2.SAP_Material_Id__c);
                }
            }

            for(DataWrapper dt : dataList){
                /*if(dt.ca.IMEI__c != null && dt.ca.SerialNumber__c != null){
                    dt.status = 'Serial Number and IMEI Number can not coexist.';
                }*/
                System.debug(dt.ca.Model_Code_Repair__c);
                system.debug(doDisplay);
                System.debug(string.isnotblank(dt.ca.Model_Code_Repair__c));
                if(doDisplay && String.isNotBlank(dt.ca.Model_Code_Repair__c) && !existingModelNames.contains(dt.ca.Model_Code_Repair__c))
                {

                    dt.status = 'Unable to validate Model Code ' + dt.ca.Model_Code_Repair__c + '. Please check and try again.';
                    isCheck = false;
                }
                else if(dt.ca.IMEI__c != null && dt.ca.IMEI__c !=''
                    && Integer.valueOf(imeiMap.get(dt.ca.IMEI__c)) < 1
                    && caseIMap.get(dt.ca.IMEI__c) == null
                    && rtName == 'Repair'){

                    dt.status = '';
                    dt.ca.Is_Child__c = true;
                    dt.ca.Parent_Case__c = false;
                    dt.ca.Issue_Description__c=dt.probSyms;//Added by Thiru
                    insertList.add(dt.ca);
                }
                else if(dt.ca.IMEI__c != null && dt.ca.IMEI__c !=''
                    && Integer.valueOf(imeiMap.get(dt.ca.IMEI__c)) < 1
                    && caseIMap.get(dt.ca.IMEI__c) == null
                    && rtName == 'Exchange'
                    && devImeiMap.get(dt.ca.IMEI__c) != null){

                    dt.ca.device__c = Id.valueOf(devImeiMap.get(dt.ca.IMEI__c).Id);
                    //dt.ca.IMEI__c = null;
                    //dt.ca.SerialNumber__c = null;
                    dt.status = '';
                    dt.ca.Is_Child__c = true;
                    dt.ca.Parent_Case__c = false;
                    dt.ca.Issue_Description__c=dt.probSyms;//Added by Thiru
                    insertList.add(dt.ca);
                }
                else if((dt.ca.IMEI__c != null && dt.ca.IMEI__c !='') && Integer.valueOf(imeiMap.get(dt.ca.IMEI__c)) >= 1){
                    dt.status = 'Please Check Duplicated [IMEI Number] : ' + dt.ca.IMEI__c;
                    isCheck = false;
                }
                else if((dt.ca.IMEI__c != null && dt.ca.IMEI__c !='') && caseIMap.get(dt.ca.IMEI__c) != null){
                    dt.status = 'Existing IMEI Number exists in Case. [IMEI Number] : ' + dt.ca.IMEI__c + ', [Case Number] : ' + caseIMap.get(dt.ca.IMEI__c);
                    isCheck = false;
                }
                else if(dt.ca.IMEI__c != null && dt.ca.IMEI__c != '' && devImeiMap.get(dt.ca.IMEI__c) == null && rtName == 'Exchange'){
                    dt.status =  'Not Existing IMEI Number in Device. [IMEI Number] : ' + dt.ca.IMEI__c;
                    isCheck = false;
                }
                else if(dt.ca.SerialNumber__c != null && dt.ca.SerialNumber__c !=''
                    && Integer.valueOf(serialMap.get(dt.ca.SerialNumber__c)) < 1
                    && caseSMap.get(dt.ca.SerialNumber__c) == null
                    && rtName == 'Repair'){

                    dt.status = '';
                    dt.ca.Parent_Case__c = false;
                    dt.ca.Is_Child__c = true;
                    dt.ca.Issue_Description__c=dt.probSyms;//Added by Thiru
                    insertList.add(dt.ca);
                }
                else if(dt.ca.SerialNumber__c != null && dt.ca.SerialNumber__c !=''
                    && Integer.valueOf(serialMap.get(dt.ca.SerialNumber__c)) < 1
                    && caseSMap.get(dt.ca.SerialNumber__c) == null
                    && rtName =='Exchange'
                    && devSerialMap.get(dt.ca.SerialNumber__c) != null){

                    dt.ca.device__c = Id.valueOf(devSerialMap.get(dt.ca.SerialNumber__c).Id);
                    dt.status = '';
                    dt.ca.Parent_Case__c = false;
                    dt.ca.Is_Child__c = true;
                    dt.ca.Issue_Description__c=dt.probSyms;//Added by Thiru
                    insertList.add(dt.ca);
                }
                else if((dt.ca.SerialNumber__c != null && dt.ca.SerialNumber__c !='') && Integer.valueOf(serialMap.get(dt.ca.SerialNumber__c)) >= 1){
                    dt.status = 'Please Check Duplicated [Serial Number] : ' + dt.ca.SerialNumber__c;
                    isCheck = false;
                }
                else if((dt.ca.SerialNumber__c != null && dt.ca.SerialNumber__c !='') && caseSMap.get(dt.ca.SerialNumber__c) != null){
                    dt.status = 'Existing Serial Number exists in Case. [Serial Number] : ' + dt.ca.SerialNumber__c + ', [Serial Number] : ' + caseSMap.get(dt.ca.SerialNumber__c);
                    isCheck = false;
                }
                else if(dt.ca.SerialNumber__c != null && dt.ca.SerialNumber__c != '' && devSerialMap.get(dt.ca.SerialNumber__c) == null && rtName == 'Exchange'){
                    dt.status = 'Not Existing Serial Number in Device. [Serial Number] : ' + dt.ca.SerialNumber__c;
                    isCheck = false;
                }

                else if(dt.ca.IMEI__c == null && dt.ca.SerialNumber__c == null){
                    dt.status = '';
                }
            }
            //system.debug(insertlist.size());
            //system.debug(imeiMap);
            if(!insertList.isEmpty() && isCheck == true){
                List<String> errList = new List<String>();
                Database.DMLOptions dlo = new Database.DMLOptions();
                dlo.EmailHeader.triggerAutoResponseEmail = true;
                dlo.optAllOrNone = false;
                try{
                    insertError = false;
                    insertSuccess = false;
                    List<Database.SaveResult> insertResults = Database.insert(insertList, dlo);
                    String errorMsg = '';
                    Integer errorCount = 0;
                    for(Integer i = 0; i < insertResults.size(); i++){
                        if(!insertResults.get(i).isSuccess()){
                            Database.Error error = insertResults.get(i).getErrors().get(0);
                            insertError = true;
                            errList.add(error.getMessage());
                            errorCount++;
                        }
                    }

                    if(insertList.size() > errorCount) insertSuccess = true;

                    if(errList.size() > 0){
                        for(Integer i = 0; i < errList.size(); i++){
                             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errList[i]));
                             return null;
                        }
                    }
                }catch(DmlException e){
                    System.debug('DML Exception: ' + e.getMessage() + e.getCause() + e.getDmlFields(0));
                }
                return new PageReference('/apex/SVC_ManageChildCases?Id='+parentId+'&showconfirmmsg=true');
            }
            //PageReference pr;
            //pr = new PageReference('/'+parentId);
            //pr.setRedirect(true);
            //return pr;
        }
        return null;
    }
    public void doAddCase(){
        for(Integer i = 0; i < 10; i++){

            DataWrapper dt      = new DataWrapper();
            Case ca             = new Case();
            ca                  = caStd.clone();
            ca.ParentId         = parentId;
            ca.IMEI__c          = null;
            ca.SerialNumber__c  = null;
            ca.Issue_Description__c  = null;//Added by Thiru.
            if(caStd.RecordType.Name == 'Repair')
                ca.Model_Code_Repair__c = null;
            dt.ca               = ca;
            dt.rowNo            = ++rowNo;
            dataList.add(dt);
        }
    }
    public PageReference doDone() {
        PageReference pr = new PageReference('/'+parentId);
        pr.setRedirect(true);
        return pr;
    }
    private Boolean isValidation() {
        Boolean isError = true;
        //system.debug(dataList.size());
        imeiMap         = new Map<String,Integer>();
        serialMap       = new Map<String,Integer>();
        imeiSet         = new Set<String>();
        serialSet       = new Set<String>();
        caseIMap        = new Map<String,String>();
        caseSMap        = new Map<String,String>();
        //devImeiSet        = new Set<String>();
        //devSerialSet  = new Set<String>();
        devImeiMap      = new Map<String, Device__c>();
        devSerialMap    = new Map<String, Device__c>();

        for(DataWrapper dt : dataList){
            if(dt.ca.IMEI__c != null && dt.ca.IMEI__c !='' && dt.ca.IMEI__c.length() < 14){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'The IMEI number must be at least 14 digits.'));
                isError = false;
            }
            //Imei
            if(dt.ca.IMEI__c != null && imeiMap.containsKey(dt.ca.IMEI__c)){
                imeiMap.put(dt.ca.IMEI__c, Integer.valueOf(imeiMap.get(dt.ca.IMEI__c)+1));
            }
            if(dt.ca.IMEI__c != null && !imeiMap.containsKey(dt.ca.IMEI__c)){
                imeiMap.put(dt.ca.IMEI__c, 0);
            }
            //Serial
            if(dt.ca.SerialNumber__c != null && serialMap.containsKey(dt.ca.SerialNumber__c)){
                serialMap.put(dt.ca.SerialNumber__c, Integer.valueOf(serialMap.get(dt.ca.SerialNumber__c)+1));
            }
            if(dt.ca.SerialNumber__c != null && !serialMap.containsKey(dt.ca.SerialNumber__c)){
                serialMap.put(dt.ca.SerialNumber__c, 0);
            }
            //system.debug('imeiMap' + imeiMap);
            if(rtName == 'Repair'){
                if(dt.ca.IMEI__c != null) dt.ca.Case_Device_Status__c = SVC_IMEICheckSumValidationClass.validation(dt.ca.IMEI__c);
            }
            if(dt.ca.IMEI__c != null && dt.ca.IMEI__c != '') imeiSet.add(dt.ca.IMEI__c);
            if(dt.ca.SerialNumber__c != null && dt.ca.SerialNumber__c != '') serialSet.add(dt.ca.SerialNumber__c);
        }

        if(rtName == 'Exchange'){
            for(Device__c d : [SELECT Id, IMEI__c, Serial_Number__c, Status__c, Is_Active__c FROM Device__c WHERE (IMEI__c in: imeiSet OR Serial_Number__c in: serialSet) AND Status__c = 'Registered' AND Account__c =: caStd.AccountId]){
                if(d.IMEI__c != null){
                    devImeiMap.put(d.IMEI__c, d);
                }
                else if(d.Serial_Number__c != null){
                    devSerialMap.put(d.Serial_Number__c, d);
                }
            }
        }
        //system.debug('devImeiMap : ' + devImeiMap);
        //system.debug('devSerialMap : ' + devSerialMap);

        //if(rtName == 'Repair'){
        for(Case ca : [SELECT Id, CaseNumber, IMEI__c, SerialNumber__c, IMEI_Number_Device__c, Serial_Number_Device__c
                        FROM Case
                        WHERE (IMEI__c in: imeiSet OR SerialNumber__c in: serialSet OR IMEI_Number_Device__c in: imeiSet OR Serial_Number_Device__c in: serialSet)
                        AND Status != 'Closed' 
                        AND Status != 'Cancelled'
                        AND (RecordType.Name = 'Repair' OR RecordType.Name = 'Exchange')]){

            if(ca.IMEI__c != null && ca.IMEI__c != '') caseIMap.put(ca.IMEI__c, ca.CaseNumber);
            if(ca.IMEI_Number_Device__c != null) caseIMap.put(ca.IMEI_Number_Device__c, ca.CaseNumber);
            if(ca.SerialNumber__c != null && ca.SerialNumber__c != '') caseSMap.put(ca.SerialNumber__c, ca.CaseNumber);
            if(ca.Serial_Number_Device__c != null) caseSMap.put(ca.Serial_Number_Device__c, ca.CaseNumber);
            //system.debug('ca : ' + ca);
        }
        //}
        /*if(rtName == 'Exchange'){
            for(Case ca : [SELECT Id, CaseNumber, IMEI__c, SerialNumber__c, IMEI_Number_Device__c, Serial_Number_Device__c FROM Case WHERE (IMEI_Number_Device__c in: devImeiSet OR Serial_Number_Device__c in: devSerialSet )AND Status != 'Resolved' AND Status != 'Closed' AND (RecordType.Name = 'Repair' OR RecordType.Name = 'Exchange')]){

                if(ca.IMEI_Number_Device__c != null && ca.IMEI_Number_Device__c != '') caseIMap.put(ca.IMEI_Number_Device__c, ca.CaseNumber);
                if(ca.SerialNumber__c != null && ca.SerialNumber__c != '') caseSMap.put(ca.SerialNumber__c, ca.CaseNumber);
                        //system.debug('ca : ' + ca);
            }
        }*/

        return isError;
    }
    public class DataWrapper {
        public Case     ca      {get;set;}
        public String   status  {get;set;}
        public Integer  rowNo   {get;set;}
        public string   probSyms{get;set;}
        public DataWrapper(){
            this.ca         = new Case();
            this.status     = null;
        }
    }
}