//JeongHo Lee csvfile FileUpload
public with sharing class SVC_CaseFileUploadClass {
    public String               fileName            {get;set;}
    public transient Blob       contentFile         {get;set;}
    public List<Case>           caseList            {get;set;}
    public Id                   parentId            {get;set;}
    public String               allfields           {get;set;}
    public Case                 caStd               {get;set;}
    public String               rtName              {get;set;}
    public String               caseNumber          {get;set;}
    public Boolean              insertError         {get;set;}
    public Boolean              insertSuccess       {get;set;}
    public String               cUrl                {get;set;}
    public String               exchDocId           {get;set;}
    public String               repairDocId         {get;set;}
    
    public SVC_CaseFileUploadClass() {
        cUrl = ApexPages.currentPage().getURL();
        rtName = String.valueOf(ApexPages.currentPage().getParameters().get('caseRecordType'));
        system.debug('rtName : ' + rtName);
        //added rt type query
        fileName = '';
        parentId = Id.valueOf(ApexPages.currentPage().getParameters().get('id'));
        caseNumber = String.valueOf(ApexPages.currentPage().getParameters().get('caseNumber'));
        //system.debug('parentId = ' + parentId);
        List<Document> docList = [SELECT Id, Name FROM Document WHERE Name='Exchange Case Upload' OR Name='Repair Case Upload'];
        
        for(Document doc: docList)
        {
            if(doc.Name == 'Exchange Case Upload')
                exchDocId = doc.Id;
            else if(doc.Name == 'Repair Case Upload')
                repairDocId = doc.Id;
        }
        
        Map<String, Schema.SObjectField> fields = Schema.getGlobalDescribe().get('Case').getDescribe().SObjectType.getDescribe().fields.getMap();
  
        List<String> accessiblefields = new List<String>();
  
        for(Schema.SObjectField field : fields.values()){
            if(field.getDescribe().isAccessible())
                accessiblefields.add(field.getDescribe().getName());
        }
  
        allfields='';
  
        for(String fieldname : accessiblefields)
            allfields += fieldname+',';
  
        allfields = allfields.subString(0,allfields.length()-1);
    }

    public Pagereference uploadFile() {
        String tempFileStr;
        Map<String,Integer> imeiMap         = new Map<String,Integer>();
        Map<String,Integer> serialMap       = new Map<String,Integer>();
        Set<String>         imeiSet         = new Set<String>();
        Set<String>         serialSet       = new Set<String>();
        Map<String,String>  caseIMap        = new Map<String, String>();
        Map<String,String>  caseSMap        = new Map<String, String>();
        Set<String>         devImeiSet      = new Set<String>();
        Set<String>         devSerialSet    = new Set<String>();

        if(parentId != null){
            String soqlQuery = '';

            soqlQuery +=    ' SELECT '      ;
            soqlQuery +=    allfields       ;
            soqlQuery +=    ' ,Entitlement.Status,RecordType.Name ';
            soqlQuery +=    ' FROM Case '   ;
            soqlQuery +=    'WHERE Id = \'' + parentId + '\' ';
                
            caStd = DataBase.query(soqlQuery);
        }

        if(this.isValidation() == true){

            tempFileStr = blobToString(contentFile, 'MS949');
            String[] filelines = tempFileStr.split('\n');
            system.debug(' =====================filelines: ' + filelines.size());
            integer ii=0;
            for(string s : filelines){
                system.debug(' =====================fileline: ' +s);
                ii++;
                system.debug(' =====================fileline: ' +ii);
            }
            
            caseList = new List<Case>();
            for(Integer i = 1; i < filelines.size(); i++){
                String[] inputvalues    = new String[]{};
                system.debug(' =====================inputvalues: ' +inputvalues);
                String tempStr= '';
                tempStr                 = filelines[i] + ',dump';
                inputvalues             = tempStr.split(',') ;
                Case ca                 = new Case();
                ca                      = caStd.clone();
                ca.ParentId             = caStd.Id;
                ca.Shipping_Address_Controlled_By__c = 'Parent Case';
                ca.IMEI__c              = inputvalues[0].trim();
                ca.SerialNumber__c      = inputvalues[1].trim();
                //if(rtName == caStd.RecordType.Name)
                if(rtName == 'Repair')
                {
                    ca.Model_Code_Repair__C = inputvalues[2].trim();
                    ca.Description = inputvalues[3].trim();
                    ca.Issue_Description__c = inputvalues[4].trim();
                }
                else
                {
                    ca.Description = inputvalues[2].trim();
                    ca.Issue_Description__c = inputvalues[3].trim();
                }


                caseList.add(ca);

                //Imei
                if(ca.IMEI__c != null && imeiMap.containsKey(ca.IMEI__c)){
                    imeiMap.put(ca.IMEI__c, Integer.valueOf(imeiMap.get(ca.IMEI__c)+1));
                }
                if(ca.IMEI__c != null && !imeiMap.containsKey(ca.IMEI__c)){
                    imeiMap.put(ca.IMEI__c, 0);
                }
                //Serial
                if(ca.SerialNumber__c != null && serialMap.containsKey(ca.SerialNumber__c)){
                    serialMap.put(ca.SerialNumber__c, Integer.valueOf(serialMap.get(ca.SerialNumber__c)+1));
                }
                if(ca.SerialNumber__c != null && !serialMap.containsKey(ca.SerialNumber__c)){
                    serialMap.put(ca.SerialNumber__c, 0);
                }
                //system.debug('imeiMap' + imeiMap);
                if(rtName == 'Repair'){
                    if(ca.IMEI__c != null && ca.IMEI__c != '') ca.Case_Device_Status__c = SVC_IMEICheckSumValidationClass.validation(ca.IMEI__c);
                }
                if(ca.IMEI__c != null && ca.IMEI__c != '') imeiSet.add(ca.IMEI__c);
                if(ca.SerialNumber__c != null && ca.SerialNumber__c != '') serialSet.add(ca.SerialNumber__c);
                //system.debug('ca : ' + ca);
            }

            Map<String, Device__c> devImeiMap = new Map<String, Device__c>();
            Map<String, Device__c> devSerialMap = new Map<String, Device__c>();

            System.debug('### Serial Set ' + serialSet.size());
            System.debug('### Account ' + caStd.AccountId);
            System.debug('### RT NAME ' + rtName);
            if(rtName == 'Exchange'){
                for(Device__c d : [SELECT Id, IMEI__c, Serial_Number__c, Status__c, Is_Active__c FROM Device__c WHERE (IMEI__c in: imeiSet OR Serial_Number__c in: serialSet) AND Status__c = 'Registered' AND Account__c =: caStd.AccountId]){
                    System.debug('###INSERIAL');
                    if(d.IMEI__c != null){
                        devImeiMap.put(d.IMEI__c, d);
                    }
                    else if(d.Serial_Number__c != null){
                        devSerialMap.put(d.Serial_Number__c, d);
                    }
                }
            }           

            //system.debug('devImeiMap : ' + devImeiMap);
            //system.debug('devSerialMap : ' + devImeiMap);
            //system.debug('devimeiSet : ' + devimeiSet);
            //system.debug('devserialSet : ' + devserialSet);
            try{
                //system.debug('===> CaseList size : ' + caseList.size());
                system.debug('===> CaseList : ' + caseList);
                List<Case> insertList = new List<Case>();
                for(Case ca : [SELECT Id, CaseNumber, IMEI__c, SerialNumber__c, IMEI_Number_Device__c, Serial_Number_Device__c 
                        FROM Case 
                        WHERE (IMEI__c in: imeiSet OR SerialNumber__c in: serialSet OR IMEI_Number_Device__c in: imeiSet OR Serial_Number_Device__c in: serialSet) 
                        AND Status != 'Closed' 
                        AND Status != 'Cancelled'
                        AND (RecordType.Name = 'Repair' OR RecordType.Name = 'Exchange')]){

                    if(ca.IMEI__c != null && ca.IMEI__c != '') caseIMap.put(ca.IMEI__c, ca.CaseNumber);
                    if(ca.IMEI_Number_Device__c != null) caseIMap.put(ca.IMEI_Number_Device__c, ca.CaseNumber);
                    if(ca.SerialNumber__c != null && ca.SerialNumber__c != '') caseSMap.put(ca.SerialNumber__c, ca.CaseNumber); 
                    if(ca.Serial_Number_Device__c != null) caseSMap.put(ca.Serial_Number_Device__c, ca.CaseNumber); 
                    //system.debug('ca : ' + ca);
                }
                //system.debug('caseIMap : ' + caseIMap);
                //system.debug('caseSMap : ' + caseSMap);
                Boolean isCheck = true;

                if(!caseList.isEmpty()){
                    for(Case ca : caseList){
                        /*if(ca.SerialNumber__c != null && ca.SerialNumber__c != '' && ca.IMEI__c != null && ca.IMEI__c != ''){
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Serial Number and IMEI Number can not coexist.'));
                            isCheck = false;
                        }*/
                        if(ca.IMEI__c != null && ca.IMEI__c != '' && Integer.valueOf(imeiMap.get(ca.IMEI__c)) >= 1){
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please Check Duplicated [IMEI Number] : ' + ca.IMEI__c));
                            isCheck = false;
                        }
                        else if(ca.IMEI__c != null && ca.IMEI__c != '' && caseIMap.get(ca.IMEI__c) != null){
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Existing IMEI Number exists in Case. [IMEI Number] : ' + ca.IMEI__c + ', [Case Number] : ' + caseIMap.get(ca.IMEI__c)));
                            isCheck = false;
                        }
                        else if(ca.SerialNumber__c != null && ca.SerialNumber__c != '' && Integer.valueOf(serialMap.get(ca.SerialNumber__c)) >= 1){
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please Check Duplicated [Serial Number] : ' + ca.SerialNumber__c));
                            isCheck = false;
                        }
                        else if(ca.SerialNumber__c != null && ca.SerialNumber__c != '' && caseSMap.get(ca.SerialNumber__c) != null){
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Existing Serial Number in Case. [Serial Number] : ' + ca.SerialNumber__c + ', [Case Number] : ' + caseSMap.get(ca.SerialNumber__c)));
                            isCheck = false;
                        }
                        else if(ca.IMEI__c != null && ca.IMEI__c != '' && devImeiMap.get(ca.IMEI__c) != null && rtName == 'Exchange'){
                            ca.device__c = Id.valueOf(devImeiMap.get(ca.IMEI__c).Id);
                            ca.IMEI__c = null;
                            ca.SerialNumber__c = null;
                            ca.Is_Child__c = true;
                            ca.Parent_Case__c = false;
                            insertList.add(ca);
                        }
                        else if(ca.IMEI__c != null && ca.IMEI__c != '' && caseIMap.get(ca.IMEI__c) == null && rtName == 'Repair'){
                            ca.Is_Child__c = true;
                            ca.Parent_Case__c = false;
                            //ca.Device_Type_f__c = 'Connected'; //Removed when Device Type was changed to formula field
                            insertList.add(ca);
                        }
                        else if(ca.IMEI__c != null && ca.IMEI__c != '' && devImeiMap.get(ca.IMEI__c) == null && rtName == 'Exchange'){
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Not Existing IMEI Number in Device. [IMEI Number] : ' + ca.IMEI__c));
                            isCheck = false;
                        }
                        else if(ca.SerialNumber__c != null && ca.SerialNumber__c != '' && devSerialMap.get(ca.SerialNumber__c) != null && rtName == 'Exchange'){
                            ca.device__c = Id.valueOf(devSerialMap.get(ca.SerialNumber__c).Id);
                            ca.IMEI__c = null;
                            ca.SerialNumber__c = null;
                            ca.Is_Child__c = true;
                            ca.Parent_Case__c = false;
                            insertList.add(ca);
                        }
                        else if(ca.SerialNumber__c != null && ca.SerialNumber__c != '' && caseSMap.get(ca.SerialNumber__c) == null && rtName == 'Repair'){
                            ca.Is_Child__c = true;
                            ca.Parent_Case__c = false;
                            //ca.Device_Type_f__c = 'WiFi'; //Removed because Device type is formula
                            insertList.add(ca);
                        }
                        else if(ca.SerialNumber__c != null && ca.SerialNumber__c != '' && devSerialMap.get(ca.SerialNumber__c) == null && rtName == 'Exchange'){
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Not Existing Serial Number in Device. [Serial Number] : ' + ca.SerialNumber__c));
                            isCheck = false;
                        }
                       /* else{
                            ca.Is_Child__c = true;
                            insertList.add(ca);
                        }*/
                    }
                }
                system.debug('insertList : ' + insertList.size());
                if(!insertList.isEmpty() && isCheck == true){
                    List<String> errList = new List<String>();
                    Database.DMLOptions dlo = new Database.DMLOptions();
                    dlo.EmailHeader.triggerAutoResponseEmail = true;
                    dlo.optAllOrNone = false;
                    try{
                        insertError = false;
                        insertSuccess = false;
                        List<Database.SaveResult> insertResults = Database.insert(insertList, dlo);
                        String errorMsg = '';
                        Integer errorCount = 0;
                        for(Integer i = 0; i < insertResults.size(); i++){
                            if(!insertResults.get(i).isSuccess()){
                                Database.Error error = insertResults.get(i).getErrors().get(0);
                                insertError = true;
                                errList.add(error.getMessage());
                                errorCount++;
                            }
                        }
                        if(insertList.size() > errorCount) insertSuccess = true;

                        if(errList.size() > 0){
                            for(Integer i = 0; i < errList.size(); i++){
                                 ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errList[i]));
                                 return null;
                            }
                        }
                    }catch(DMLexception e){
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
                        System.debug('DML Exception: ' + e.getMessage() + e.getCause() + e.getDmlFields(0));
                    }
                    //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Success : ' + fileName +', Success Count : ' + insertList.size()));
                    //pr.setRedirect(true);
                    return new PageReference('/apex/SVC_ManageChildCases?Id='+parentId+'&showconfirmmsg=true');
                }   
            }
            catch(Exception e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            }
        }

        return null;
    }
    public PageReference doDone() {
        PageReference pr = new PageReference('/'+parentId);
        pr.setRedirect(true);
        return pr;
    }

    /*
    Validation
    */
    private Boolean isValidation() {
        Boolean isError = true;
        if(parentId == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'It is not the exact path check your Parent Case Id'));
            isError = false;
        }
        /*if(caStd != null){
            if(caStd.Entitlement.Status == 'Expired'){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please check the status of Entitlement'));
                isError = false;        
            }
            if(caStd.EntitlementId == null ){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'If there is no entitlement in the case'));
                isError = false;            
            }
        }*/
        if(fileName == null || fileName == ''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please check the file'));
            isError = false;
        } 
        if(!fileName.contains('.csv')){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please upload a valid CSV file'));
            isError = false;
        }
        return isError;
    }

    public static String blobToString(Blob input, String inCharset) {
        String hex = EncodingUtil.convertToHex(input);
        system.debug('hex ::: ' + hex);
        System.assertEquals(0, hex.length() & 1);
        final Integer bytesCount = hex.length() / 2;
        String[] bytes = new String[bytesCount];
        for(Integer i = 0; i < bytesCount; ++i)
            bytes[i] =  hex.mid(i * 2, 2);
            
        string d = '%' + String.join(bytes, '%');
        //system.debug('encoded ::: ' + d);
        
        string e = EncodingUtil.urlDecode(d, inCharset);
        //system.debug('decoded ::: ' + e);
        return e;
    }
}