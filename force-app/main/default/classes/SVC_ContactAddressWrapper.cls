public class SVC_ContactAddressWrapper {
	public String mailingStreet {get; set;}
    public String mailingState {get; set;}
    public String mailingCity{get;set;}
    public String mailingCountry{get;set;}
    public String mailingZipCode{get;set;}


    public SVC_ContactAddressWrapper(string street,string city,string state,string ZipCode,string country) {
            mailingStreet = street;
            mailingState = state;
            mailingCity = city;
            mailingCountry = country;
            mailingZipCode = ZipCode;
    }
}