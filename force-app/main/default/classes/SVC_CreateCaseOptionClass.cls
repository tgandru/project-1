public with sharing class SVC_CreateCaseOptionClass {

	public List<SelectOption> createCaseOption {get; private set;}
	public String selectedOption {get; set;}
	public String caseNumber {get; private set;}
	private String caseId {private get; private set;}
	private String caseRecordType {private get; private set;} 

	public SVC_CreateCaseOptionClass() 
	{
		createCaseOption = new List<SelectOption>();

		createCaseOption.add(new SelectOption('Manual Entry', 'Manual Entry'));
		createCaseOption.add(new SelectOption('File Upload', 'File Upload'));	

		selectedOption = 'Manual Entry';

		caseId = ApexPages.currentPage().getParameters().get('id');
		caseRecordType = ApexPages.currentPage().getParameters().get('caseRecordType');
		caseNumber = ApexPages.currentPage().getParameters().get('caseNumber');
	}

	public PageReference createCase()
	{
		PageReference pr = new PageReference('');
		String urlParam = '?id='+caseId+'&caseRecordType='+caseRecordType+'&caseNumber='+caseNumber;
		if(String.isNotBlank(selectedOption) && selectedOption == 'Manual Entry')
			pr = new PageReference('/apex/SVC_CaseCreationEntryPage' + urlParam);
		else if(String.isNotBlank(selectedOption) && selectedOption == 'File Upload')
			pr = new PageReference('/apex/SVC_CaseFileUploadPage' + urlParam);

		pr.setRedirect(true);
		return pr;
	}

	public PageReference cancel()
	{
		PageReference pr = new PageReference('/'+caseId);
		pr.setRedirect(true);
		return pr;
	}
}