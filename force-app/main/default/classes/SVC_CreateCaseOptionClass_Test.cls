@isTest(SeeAllData = false)
public with sharing class SVC_CreateCaseOptionClass_Test {

	static testmethod void caseOptiontestClass()
	{
		SVC_CreateCaseOptionClass option = new SVC_CreateCaseOptionClass();
		System.assertEquals(option.createCaseOption.isEmpty(), false);
		System.assertEquals('Manual Entry', option.selectedOption);

		option.cancel();

		test.startTest();
		option.createCase();
		option.selectedOption = 'File Upload';
		option.createCase();
		test.stopTest();
	}
}