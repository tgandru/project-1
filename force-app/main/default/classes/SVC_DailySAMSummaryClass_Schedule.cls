global class SVC_DailySAMSummaryClass_Schedule implements Schedulable {
    global void execute(SchedulableContext sc) {
        sendDailySummaryEmail();
    }

    public static void sendDailySummaryEmail()
    {
        
        Set<String> entitlementIds = new Set<String>();
        Set<String> processedEntitlementIds = new Set<String>();
        Set<Id> userIds = new Set<Id>();
        List<Messaging.SingleEmailMessage> singleMsgList = new List<Messaging.SingleEmailMessage>();
        Map<String, Integer> deviceStatusMap = new Map<String, Integer>();
        //Map<String, Integer> entitlementCountMap = new Map<String, Integer>();
        Map<String, AggregateResult> aggregateResultMap = new Map<String, AggregateResult>();
        Map<Id, Device__c> deviceMap = new Map<Id, Device__c>();
        final String deviceURL = URL.getSalesforceBaseUrl().toExternalForm() + '/apex/SVC_ApproveDisapproveDevices?';
        List<String> supportEmailList = new List<String>();
        String supportEmailCCAddress = Label.CC_Email_address_for_SAM_Notification;
        List<String> supportEmailCCAddressList = new List<String>();

        if(String.isNotBlank(supportEmailCCAddress))
            supportEmailCCAddressList = supportEmailCCAddress.split(',');

        List<GroupMember> groupMemberList = [Select UserOrGroupId From GroupMember where Group.Name = 'Service Support Team'];

        for(GroupMember gm: groupMemberList)
            userIds.add(gm.UserOrGroupId);

        if(!userIds.isEmpty())
        {
            List<User> userList = [SELECT Email FROM User WHERE Id IN: userIds];

            for(User u: userList)
                supportEmailList.add(u.Email);
        }

        AggregateResult[] groupedResults = [SELECT count(Id) numofDevice, Account__r.Name acctName, Entitlement__c ent, Status__c status
                                             FROM Device__c
                                             WHERE Entitlement__c != NULL 
                                             AND (Status__c = 'Registered' OR Status__c = 'Verified' OR Status__c ='Invalid' OR Status__c ='Unverified')
                                             GROUP BY Account__r.Name, Entitlement__c, Status__c
                                             ORDER BY Account__r.Name, Entitlement__c];

        for(AggregateResult ar: groupedResults)
        {
            String key = String.valueOf(ar.get('ent')) + String.valueOf(ar.get('status'));
            deviceStatusMap.put(key, Integer.valueOf(ar.get('numofDevice')));
            aggregateResultMap.put(String.valueOf(ar.get('ent')), ar);

        }

        Map<Id, Entitlement> entitlementMap = new Map<Id, Entitlement>([SELECT Name, AccountId, Units_Allowed__c, Entitlement_Notes__c, 
                                                                        Account.Service_Account_Manager__r.Email, Account.Service_Account_Manager__c
                                                                        FROM Entitlement
                                                                        WHERE Id IN: aggregateResultMap.keyset()]);

        String acctName = '';
        String body = '';
        Messaging.SingleEmailMessage mailMessage;
        String entId = '';
        String oldEntId = '';

        for(AggregateResult ar: groupedResults)
        {
            entId = String.valueOf(ar.get('ent'));

            if(!deviceStatusMap.containskey(entId+'Verified'))
                continue;
            else if(processedEntitlementIds.contains(entId))
                continue;
            else
                processedEntitlementIds.add(entId);

            if(String.isBlank(acctName) || (String.isNotBlank(acctName) && acctName != String.valueOf(ar.get('acctName'))))
            {

                if(String.isNotBlank(acctName))
                {
                    body += 'Click link to approve Device records: ' + deviceURL + 'accId=' + entitlementMap.get(oldEntId).AccountId;
                    //mailMessage.setPlainTextBody(body);
                    mailMessage.setHTMLBody(body);
                    singleMsgList.add(mailMessage);
                }

                mailMessage = new Messaging.SingleEmailMessage();
                mailMessage.setSaveAsActivity(false);
                acctName = String.valueOf(ar.get('acctName'));
                oldEntId = entId;
                mailMessage.setSubject('Device Approval Summary for ' + acctName);
                body = 'Account: ' + acctName + '<br/>';
                
                if(entitlementMap.containsKey(entId) && entitlementMap.get(entId).Account.Service_Account_Manager__c != null && entitlementMap.get(entId).Account.Service_Account_Manager__r.Email != null) 
                    mailMessage.setToAddresses(new List<String>{entitlementMap.get(entId).Account.Service_Account_Manager__r.Email});
                else if(!supportEmailList.isEmpty())
                    mailMessage.setToAddresses(supportEmailList);

                if(!supportEmailCCAddressList.isEmpty())
                    mailMessage.setCcAddresses(supportEmailCCAddressList);

            }
            
            body += '<br/>';

            body += 'Entitlement Name: ' + entitlementMap.get(entId).Name + '<br/>';
            
            if(entitlementMap.get(entId).Units_Allowed__c != null)  body += 'Number of Devices Allowed: ' + entitlementMap.get(entId).Units_Allowed__c + '<br/>'; 
            else body += 'Number of Devices Allowed: 0' + '<br/>';
            
            if(entitlementMap.get(entId).Entitlement_Notes__c != null) body += 'Entitlement Notes: ' + entitlementMap.get(entId).Entitlement_Notes__c + '<br/>';
            else body += 'Entitlement Notes: <br/>';

            if(deviceStatusMap.get(entId+'Registered') != null) body += 'Number of Devices Registered: ' + deviceStatusMap.get(entId+'Registered') + '<br/>';
            else body += 'Number of Devices Registered: 0 <br/>';
            
            if(deviceStatusMap.get(entId+'Verified') != null) body += 'Number of Devices Verified: ' + deviceStatusMap.get(entId+'Verified') + '<br/>';
            else body += 'Number of Devices Verified: 0 <br/>';
            
            if(deviceStatusMap.get(entId+'Invalid') != null || deviceStatusMap.get(entId+'Unverified') != null)
                
                if(deviceStatusMap.get(entId+'Invalid') != null && deviceStatusMap.get(entId+'Unverified') != null) body += 'Number of Devices Invalid/Unverified: ' + deviceStatusMap.get(entId+'Invalid') + deviceStatusMap.get(entId+'Unverified') + '<br/>';
                else if(deviceStatusMap.get(entId+'Invalid') != null) body += 'Number of Devices Invalid/Unverified: ' + deviceStatusMap.get(entId+'Invalid') + '<br/>';
                else if(deviceStatusMap.get(entId+'Unverified') != null) body += 'Number of Devices Invalid/Unverified: ' + deviceStatusMap.get(entId+'Unverified') + '<br/>';
            else body += 'Number of Devices Invalid/Unverified: 0 <br/>';
        }

        if(!groupedResults.isEmpty() && mailMessage != null)
        {
            body += 'Click link to approve Device records: ' + deviceURL + 'accId=' + entitlementMap.get(oldEntId).AccountId;
            mailMessage.setHTMLBody(body);
            //mailMessage.setPlainTextBody(body);
            singleMsgList.add(mailMessage);
        }

        if(!singleMsgList.isEmpty())
            Messaging.sendEmail(singleMsgList);
    }
}