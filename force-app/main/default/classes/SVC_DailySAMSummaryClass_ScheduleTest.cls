/**
 * Created by ryan on 8/1/2017.
 */

@IsTest
private class SVC_DailySAMSummaryClass_ScheduleTest {
    static Device__c dev1;
    static Device__c dev2;
    static Device__c dev3;
    static Device__c dev4;

    @testSetup static void testData(){
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        //get record type ids for account
        Id deviceRT = Schema.SObjectType.Device__c.getRecordTypeInfosByName().get('Edit Device').getRecordTypeId();
        Id deviceIn = Schema.SObjectType.Device__c.getRecordTypeInfosByName().get('Invalid Device').getRecordTypeId();
        
        Id acctRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Direct').getRecordTypeId();
        Id caseRT = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Exchange').getRecordTypeId();
        Id prodRT = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('B2B').getRecordTypeId();
        BusinessHours bh12 = [SELECT Id FROM BusinessHours WHERE name = 'B2B Service Hours 12x5'];

        Account acc = new Account(Name = 'Test Account', RecordTypeId = acctRT, Type = 'Distributor');
        insert acc;
        Contact con = new Contact(LastName = 'Test Contact', Email = 'test@email.com', Account = acc, MailingCountry = 'US');
        insert con;
        RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];
        Product2 prod1 = new Product2(name ='MI-test', SAP_Material_ID__c = 'MI-test', sc_service_type__c = 'Incident', sc_service_type_ranking__c = '6', RecordTypeId = prodRT);
        insert prod1;
        Asset ast = new Asset(name ='Assert-MI-Test', AccountId = acc.id, Product2Id = prod1.id);
        insert ast;
        Slaprocess sla = [select id from SlaProcess where name like 'Enhanc%' and isActive = true limit 1 ];
        Entitlement ent = new Entitlement(Name ='Test Entitlement', AccountId=acc.id, assetid = ast.id, BusinessHoursId = bh12.id, StartDate = System.today().addDays(-14), EndDate = System.today().addDays(14), slaprocessid = sla.id, Units_Allowed__c = 10, Units_Exchanged__c = 0);
        insert ent;
        dev1 = new Device__c(RecordTypeId = deviceRT,Account__c = acc.Id, IMEI__c = '7259863232145', Status__c = 'Verified', Entitlement__c = ent.Id);
        insert dev1;
        dev2 = new Device__c(RecordTypeId = deviceRT,Account__c = acc.Id, IMEI__c = '7259863232146', Status__c = 'Verified', Entitlement__c = ent.Id);
        insert dev2;
        dev3 = new Device__c(RecordTypeId = deviceIn,Account__c = acc.Id, IMEI__c = '7259863232321', Status__c = 'Invalid', Entitlement__c = ent.Id);
        insert dev3;
        dev4 = new Device__c(RecordTypeId = deviceRT,Account__c = acc.Id, IMEI__c = '7259863232147', Status__c = 'Unverified', Entitlement__c = ent.Id);
        insert dev4;
    }

    static testMethod void testSendDailySummaryEmail(){
        Test.startTest();
            SVC_DailySAMSummaryClass_Schedule s = new SVC_DailySAMSummaryClass_Schedule();
            SchedulableContext ctx;
            s.execute(ctx);
        Test.stopTest();
    }
}