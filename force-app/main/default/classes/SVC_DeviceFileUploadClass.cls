public without sharing class SVC_DeviceFileUploadClass {

    public Device__c device {get; set;}
    public String accountName {get; private set;}
    public Boolean selectAcct {get; private set;}
    public Boolean isPortalUser {get; private set;}
    public String selectedEntitlement {get; set;}
    public Id accountId {get; private set;}
    public transient blob contentFile {get; set;}
    public String fileName {get; set;}
    public String documentId {get; set;}
    public String referenceNum {get; set;}

    public List<SelectOption> entitlementOptions {get; private set;}


    public SVC_DeviceFileUploadClass(Apexpages.StandardController stdCon) {
        
        device = new Device__c();
        accountName = '';
        selectAcct = true;
        entitlementOptions = new List<SelectOption>();
        isPortalUser = false;
        
        try{
            documentId = [SELECT Id FROM Document WHERE Name = 'Device Upload' LIMIT 1].Id;
        }catch(QueryException qe){
            documentId = '';
        }

        if(ApexPages.currentPage().getParameters().get('acctId') != null)
        {
            accountId = Id.valueof(ApexPages.currentPage().getParameters().get('acctId'));
            system.debug(accountId);
            setAcctValues();
            selectAcct = false;
        }
        else
        {
            User u = [SELECT ContactId, Contact.AccountId 
                                    FROM User 
                                    WHERE Id =: UserInfo.getUserId()];
            if(u.ContactId != null)
            {
                accountId = u.Contact.AccountId;
                setAcctValues();
                selectAcct = false;
                isPortalUser = true;
            }
        }

    }

    public void selectAccount()
    {   
        accountId = device.Account__c;
        setAcctValues();
    }

    public PageReference createDevices()
    {
        if(accountId == null)
        {
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please specify an Account before uploading'));
            return null;
        }

        if(String.isBlank(fileName))
        {
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please upload a file'));
            return null;
        }

        if(String.isNotBlank(fileName) && !fileName.contains('.csv'))
        {
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please upload a valid CSV file'));
            return null;
        }

        List<Device__c> newDeviceList = new List<Device__c>();
        //String contentFileStr = contentFile.toString();
        String contentFileStr = SVC_CaseFileUploadClass.blobToString(contentFile, 'MS949');
        List<String> deviceUploadStringList = contentFileStr.split('\n');
        List<String> imeiSerialNumList = new List<String>();
        Map<String, String> imeiSerialNumMap = new Map<String, String>();

        //Get row headers
        String fieldHeaders = deviceUploadStringList[0];
        final List<String> fieldHeadersList;
        if(fieldHeaders.contains(','))
            fieldHeadersList = fieldHeaders.split(',');
        else
            fieldHeadersList = new List<String>{fieldHeaders};

        for(Integer i=1; i<deviceUploadStringList.size(); i++)
        {
            List<String> strList;
            if(deviceUploadStringList[i] != null && deviceUploadStringList[i].contains(','))
                strList = deviceUploadStringList[i].split(',');
            else 
                strList = new List<String>{deviceUploadStringList[i]};

            for(String s: strList)
            {
                if(String.isNotBlank(s))
                    imeiSerialNumList.add(s);
            }
        }
    
        List<Device__c> unregDeviceList = [SELECT Id, Serial_Number__c, IMEI__c
                                           FROM Device__c 
                                           WHERE 
                                           (IMEI__c IN: imeiSerialNumList OR
                                            Serial_Number__c IN: imeiSerialNumList)
                                            AND (Status__c = 'Exchanged' OR Status__c = 'Unregistered')];
        
        for(Device__c d: unregDeviceList)
        {
            if(d.IMEI__c != null)
                imeiSerialNumMap.put(d.IMEI__c, d.Id);

            if(d.Serial_Number__c != null)
                imeiSerialNumMap.put(d.Serial_Number__c, d.Id);
        }
        DateTime pr=system.now();
        String refNum=String.valueOf(accountId).right(8)+String.valueof(pr.Year())+String.valueof(pr.Month())+String.valueof(pr.day())+String.valueof(pr.hour())+String.valueof(pr.minute());

        for(Integer i=1; i<deviceUploadStringList.size(); i++)
        {
            List<String> strList;
            if(deviceUploadStringList[i] != null && deviceUploadStringList[i].contains(','))
                strList = deviceUploadStringList[i].split(',');
            else 
                strList = new List<String>{deviceUploadStringList[i]};

            Boolean addDevice = false;

            Device__c d = new Device__c(Account__c = accountId, Entitlement__c = selectedEntitlement, isBulkUpload__c = true,ReferenceNum__c=refNum);

            for(Integer x=0; x<strList.size(); x++)
            {
                if(String.isNotBlank(strList[x]))
                {
                    if(imeiSerialNumMap.containsKey(strList[x]))
                        d.Id = imeiSerialNumMap.get(strList[x]);

                    String str = fieldHeadersList[x].trim();
                    d.put(str, strList[x]);
                    addDevice = true;
                }   
            }

            if(addDevice)
                newDeviceList.add(d);
        }

        if(!newDeviceList.isEmpty())
            try{    
                upsert newDeviceList;
            }catch(DmlException de){
                Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, de.getMessage()));
                return null;
            }

        

        String referenceNum = [SELECT Reference_Number__c FROM Device__c WHERE Id IN: newDeviceList AND Id NOT IN: unregDeviceList LIMIT 1].Reference_Number__c;

        return new PageReference('/apex/SVC_ManageDevicePage?ref='+referenceNum+'&disableRef=true&acctName='+accountName+'&acctId='+accountId +'&entId=' + selectedEntitlement +'&cnt=' +newDeviceList.size());
    }

    private void setAcctValues()
    {
        accountName = [SELECT Name FROM Account WHERE Id =: accountId].Name;
            
        List<Entitlement> entList = [SELECT Name, Id FROM Entitlement WHERE AccountId =: accountId AND Status = 'Active'];

        entitlementOptions = new List<SelectOption>();

        for(Entitlement ent: entList)
            entitlementOptions.add(new SelectOption(ent.Id, ent.Name));
    }
}