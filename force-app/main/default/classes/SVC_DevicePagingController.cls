public with sharing class SVC_DevicePagingController {
    public String size { get; set; }
    Public List<DeviceWrapper> deviceList;
    Public Integer noOfRecords{get; set;}
    private final Device__c des; 
    public list<Device__c> selectedList {get;set;}

    List<Entitlement> EntitlementList {get;set;}

    public Id deviceId {get;set;}

    Public Map<ID,Device__c> selectedDeviceMap = new Map<ID,Device__c>();

    public string selectedEntitlementId {
        get;
        //{
            //return '5500m00000001UYAAY';
            
        //}
        set;
    }

    public string selectedEntitlementName {
        get
        {
            selectedEntitlementName = null;
            if (selectedEntitlementId !=null){
                selectedEntitlementName = [select id,name from entitlement where id=:selectedEntitlementId].name;

            }
            return selectedEntitlementName;
        }
        set;
    }


    public string AccountId {
        get
        {
            if (AccountId == null) {
                AccountId = ApexPages.currentPage().getParameters().get('acc_id');
                EntitlementList =  [select id,name from entitlement where Accountid =: AccountId and status='Active' order by name];
             }
            return AccountId;
        }
        set;
    }

    public SVC_DevicePagingController(ApexPages.StandardSetController controller) {
        if (AccountId == null){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Missing Account ID'));
        }
        this.des = (Device__c)controller.getRecord();

        String updated_cnt = ApexPages.currentPage().getParameters().get('updated');
        if (updated_cnt !=null){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Number of devices updated:' + updated_cnt));
        }

    }
     
     public ApexPages.StandardSetController deviceRecords {
        get {
            if(deviceRecords == null) {
                  deviceRecords = new ApexPages.StandardSetController(
                    Database.getQueryLocator([Select Id, Name,status__c,IMEI__c,
                            Carrier__c,Model_Name__c,Purchase_Date__c,
                        entitlement__r.name,entitlement__c FROM Device__c 
                        where account__c =: AccountId and status__c NOT IN ('Invalid','Unregistered','Registered')
                        Order By Name]));
                // sets the number of records in each page set
                deviceRecords.setPageSize(100);
                noOfRecords = deviceRecords.getResultSize();
            }
            return deviceRecords;
        }
        set;
    }

    public List<DeviceWrapper> getDevice() {
        getSelectedDevice();
        deviceList = new List<DeviceWrapper>();
         for (Device__c devc : (List<Device__c>)deviceRecords.getRecords()){
 
             if (selectedDeviceMap.containsKey(devc.id)){
                deviceList.add(new DeviceWrapper(devc, true));
             }else{
                deviceList.add(new DeviceWrapper(devc, false));
             }  
        }    
        return deviceList;
    }

    public void getSelectedDevice(){ 
        if(deviceList != null){ 
            for(DeviceWrapper dr:deviceList) {
                if(dr.selected == true) { 
                    selectedDeviceMap.put(dr.devc.id,dr.devc); 
                } 
                else 
                { 
                    selectedDeviceMap.remove(dr.devc.id); 
                } 
            } 
        } 
    }


    public PageReference processSelected() {
        Boolean hasError = false;
        Integer cnt = 0;
        Id createRecordTypeId = Schema.SObjectType.Device__c.getRecordTypeInfosByName().get('Create Device').getRecordTypeId();
        getSelectedDevice(); 
        selectedList = selectedDeviceMap.values();
        if (selectedEntitlementId == null){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select an entitlement.'));
            hasError = true;
        }

        if (selectedList.size()<=0){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select at least one device.'));
            hasError = true;
        }
        if (!hasError){
            List<Device__c> devices = [select id,entitlement__c from device__c 
                where id in:selectedList];
            for (Device__c devc:devices){
                devc.entitlement__c = selectedEntitlementId;
                devc.Status__c = 'New';
                devc.RecordTypeId = createRecordTypeId;
                cnt++;
            }

            try {
                update devices;
                //return new PageReference('/apex/SVC_ManageDeviceEntitlement?acc_id='+AccountId);
                PageReference pageRef = new PageReference('/apex/SVC_ManageDeviceEntitlement?acc_id='+AccountId+'&updated='+cnt);
                pageRef.setRedirect(true);

                //PageReference pageRef = new PageReference('/'+AccountId);
                return pageRef;
                //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Number of devices updated: ' +cnt));
            } catch (DmlException e) {

                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,e.getMessage()));
            }
        }
        return null;
    }

    public class DeviceWrapper {
        public Device__c devc {get; set;}
        public Boolean selected {get; set;}

        public DeviceWrapper(Device__c d,Boolean bool) {
            devc = d;
            selected = bool;
        }
    }

     public Boolean hasNext {
        get {
            return deviceRecords.getHasNext();
        }
        set;
    }

    // indicates whether there are more records before the current page set.
    public Boolean hasPrevious {
        get {
            return deviceRecords.getHasPrevious();
        }
        set;
    }

    // returns the page number of the current page set
    public Integer pageNumber {
        get {
            return deviceRecords.getPageNumber();
        }
        set;
    }

    // returns the first page of records
     public void first() {
         deviceRecords.first();
     }

     // returns the last page of records
     public void last() {
         deviceRecords.last();
     }

     // returns the previous page of records
     public void previous() {
         deviceRecords.previous();
     }

     // returns the next page of records
     public void next() {
         deviceRecords.next();
     }

     // returns the PageReference of the original page, if known, or the home page.
     public PageReference cancel() {

        if (AccountId != null){
            PageReference pageRef = new PageReference('/'+AccountId);
            return pageRef;
        }
        else{
            deviceRecords.cancel();
            return null;
        }
     }

     public List<SelectOption> getEntitlementItems() {

        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','None'));
        for (Entitlement ent:EntitlementList){
            options.add(new SelectOption(ent.id,ent.name));
        }
         return options;

    }

    public PageReference setSelectedEntitlementId() {
        return null;
    }
    public void clearAll(){
        if (deviceList !=null){
            for (DeviceWrapper dr:deviceList){
                dr.selected = false;
            }
        }

    }
    public void selectAll(){
        if (deviceList !=null){
            for (DeviceWrapper dr:deviceList){
                dr.selected = true;
            }
        }
    }

}