/**
 * Created by ms on 2017-08-07.
 *
 * author : JeongHo.Lee, I2MAX
 */
public without sharing class SVC_FeedCommentTriggerHandler {
	public static void insertFeedComment(Set<Id> ids){
    system.debug('***InsertFeedComment');
    Set<Id> batchids = new Set<Id>(); 
    Set<Id> futureids = new Set<Id>(); 
    List<Data> dataList = new List<Data>();

    Set<Id> feedItems = new Set<Id>();
    for(FeedComment fc : [SELECT Id,FeedItemId,ParentId,InsertedById,InsertedBy.Name,CommentBody,CreatedDate FROM FeedComment WHERE id in: ids]){
      feedItems.add(fc.FeedItemId);
      Data dt = new Data();
      dt.com = fc;
      dt.feedItemid = fc.FeedItemId;
      dataList.add(dt);
    }

    Set<Id> ParentIds = new Set<Id>();
    for(FeedItem fi : [SELECT Id, ParentId, Parent.Type FROM FeedItem WHERE Id in: feedItems AND Parent.Type in('Case','Task','Event')]){
      ParentIds.add(fi.ParentId);
      for(Data dt : dataList){
        if(fi.Parent.Type == 'Case' && fi.Id == dt.feedItemid) dt.caseId = fi.ParentId;
        else if(fi.Parent.Type == 'Task' && fi.Id == dt.feedItemid) dt.taskId = fi.ParentId; 
        else if(fi.Parent.Type == 'Event' && fi.Id == dt.feedItemid) dt.eventId =  fi.ParentId;
      }
    }

    Set<Id> taCaseIds = new Set<Id>(); //whatid = caseId
    for(Task ta : [SELECT Id, WhatId FROM Task WHERE Id in: ParentIds AND What.Type = 'Case']){
      taCaseIds.add(ta.WhatId);
      for(Data dt: dataList){
        if(ta.id == dt.taskId) dt.caseId = ta.WhatId;
      }
    }

    Set<Id> evtCaseIds = new Set<Id>(); //whatid = caseId
    Id caseParentId = null;
    for(Event evt : [SELECT Id, WhatId FROM Event WHERE Id in: ParentIds AND What.Type = 'Case']){
      evtCaseIds.add(evt.WhatId);
      for(Data dt: dataList){
        if(evt.id == dt.eventId) dt.caseId = evt.WhatId;
      }
    }
    for(Case p : [SELECT Id, CaseNumber,Service_Order_Number__c, Status, GCIC_Status_Code__c FROM Case 
                WHERE (Id in: ParentIds OR Id in: taCaseIds OR Id in: evtCaseIds)
                AND RecordType.DeveloperName in('Exchange','Repair','Repair_To_Exchange')
                AND Status NOT in('Cancelled', 'Closed')
                AND Service_Order_Number__c = NULL
                AND Parent_Case__c = true]){
      for(Data dt : dataList){
        if(dt.caseId == p.Id){
          batchids.add(dt.com.id);
          caseParentId = p.id;
        } 
      }
    }

    for(Case c: [SELECT Id, CaseNumber,Service_Order_Number__c, Status, GCIC_Status_Code__c FROM Case 
                WHERE (Id in: ParentIds OR Id in: taCaseIds OR Id in: evtCaseIds)
                AND RecordType.DeveloperName in('Exchange','Repair','Repair_To_Exchange')
                AND Status NOT in('Cancelled', 'Closed')
                AND Service_Order_Number__c != NULL
                AND Parent_Case__c = false]){
      for(Data dt : dataList){
        if(dt.caseId == c.Id) futureids.add(dt.com.id);
      }
    }
    system.debug('batchids : ' + batchids);
    system.debug('futureids : ' + futureids);
    
    if(!batchids.isEmpty()){
      List<Id> childSet = new List<Id>();
      for(case ca : [SELECT Id FROM Case 
              WHERE ParentId =: caseParentId
              AND RecordType.DeveloperName in('Exchange','Repair','Repair_To_Exchange')
              AND Status NOT in('Cancelled', 'Closed')
              AND Service_Order_Number__c != NULL
              AND Parent_Case__c = false]){
        childSet.add(ca.Id);
      }
      system.debug('childSet' + childSet);
      if(!childSet.isEmpty() && !Test.isRunningTest()){
        SVC_GCICFeedCommentBatch ba = new SVC_GCICFeedCommentBatch(childSet, dataList[0].com);
            Database.executeBatch(ba, 50);
          }
    }
    if(!futureids.isEmpty() && !Test.isRunningTest()){
      SVC_FeedCommentTriggerHandler.afterInsert(futureids);
    }
  }

  @future(callout = true)
  public static void afterInsert(Set<Id> ids){
    system.debug('***feedComment afterInsert***');    
    List<Data> dataList = new List<Data>();

    Set<Id> feedItems = new Set<Id>();
    
    for(FeedComment fc : [SELECT Id,FeedItemId,ParentId,InsertedById,InsertedBy.Name,CommentBody,CreatedDate FROM FeedComment WHERE id in: ids]){
      feedItems.add(fc.FeedItemId);
      Data dt = new Data();
      dt.com = fc;
      dt.feedItemid = fc.FeedItemId;
      dataList.add(dt);
    }

    /*for(FeedComment fc : triggerNew){
      feedItems.add(fc.FeedItemId);
      Data dt = new Data();
      dt.com = fc;
      dt.feedItemid = fc.FeedItemId;
      dataList.add(dt);
    }*/

    Set<Id> ParentIds = new Set<Id>();
    for(FeedItem fi : [SELECT Id, ParentId, Parent.Type FROM FeedItem WHERE Id in: feedItems AND Parent.Type in('Case','Task','Event')]){
      ParentIds.add(fi.ParentId);
      for(Data dt : dataList){
        if(fi.Parent.Type == 'Case' && fi.Id == dt.feedItemid) dt.caseId = fi.ParentId;
        else if(fi.Parent.Type == 'Task' && fi.Id == dt.feedItemid) dt.taskId = fi.ParentId; 
        else if(fi.Parent.Type == 'Event' && fi.Id == dt.feedItemid) dt.eventId =  fi.ParentId;
      }
    }

    Set<Id> taCaseIds = new Set<Id>(); //whatid = caseId
    for(Task ta : [SELECT Id, WhatId FROM Task WHERE Id in: ParentIds AND What.Type = 'Case']){
      taCaseIds.add(ta.WhatId);
      for(Data dt: dataList){
        if(ta.id == dt.taskId) dt.caseId = ta.WhatId;
      }
    }

    Set<Id> evtCaseIds = new Set<Id>(); //whatid = caseId
    for(Event evt : [SELECT Id, WhatId FROM Event WHERE Id in: ParentIds AND What.Type = 'Case']){
      evtCaseIds.add(evt.WhatId);
      for(Data dt: dataList){
        if(evt.id == dt.eventId) dt.caseId = evt.WhatId;
      }
    }

    /*Set<Id> caseIds = new Set<Id>();
    for(Case ca : [SELECT Id, Service_Order_Number__c FROM Case 
            WHERE Id in: ParentIds AND Status NOT in('Cancelled', 'Closed')
            AND RecordType.DeveloperName in('Exchange','Repair','Repair_To_Exchange')]){
      caseIds.add(ca.Id);
    }*/

    List<Case> caseList = [SELECT Id, CaseNumber,Service_Order_Number__c, Status, GCIC_Status_Code__c, RecordType.DeveloperName FROM Case 
                WHERE (Id in: ParentIds OR Id in: taCaseIds OR Id in: evtCaseIds)
                AND RecordType.DeveloperName in('Exchange','Repair','Repair_To_Exchange')
                AND Status NOT in('Cancelled', 'Closed')
                AND Service_Order_Number__c != NULL
                AND Parent_Case__c = false];

    for(Case ca : caseList){
      for(Data dt : dataList){
        if(dt.caseId == ca.Id) dt.ca = ca;
      }
    }
    
    String layoutId = Integration_EndPoints__c.getInstance('SVC-07').layout_id__c;
      String partialEndpoint = Integration_EndPoints__c.getInstance('SVC-07').partial_endpoint__c;

      FeedCommentRequest feedReq = new FeedCommentRequest();
      SVC_GCICJSONRequestClass.HWTicketStatusJSON body = new SVC_GCICJSONRequestClass.HWTicketStatusJSON();
      cihStub.msgHeadersRequest headers = new cihStub.msgHeadersRequest(layoutId);
      body.CommentList = new List<SVC_GCICJSONRequestClass.Comment>();

      //system.debug('***dataList***' + dataList);
      if(!dataList.isEmpty() && dataList[0].ca != null){
        for(Integer i = 0; i < dataList.size(); i++){
          body.Service_Ord_no = dataList[i].ca.Service_Order_Number__c;
          body.Case_NO    = dataList[i].ca.CaseNumber;
          body.Ticket_status  = ' ';
          if(dataList[i].ca.Status =='Cancelled' && dataList[i].ca.RecordType.DeveloperName == 'Repair') body.Ticket_status  = 'ST051';
          if(dataList[i].ca.Status =='Cancelled' && dataList[i].ca.RecordType.DeveloperName == 'Exchange') body.Ticket_status  = 'ES080';
          //body.Ticket_status  = 'ST010';
          SVC_GCICJSONRequestClass.Comment ct = new SVC_GCICJSONRequestClass.Comment();
          ct.Comment = dataList[i].com.CommentBody;
          ct.Comment_Created_by = dataList[i].com.InsertedBy.Name;
          //ct.Comment_Created_by_email = dataList[i].com.InsertedBy.Email;
          ct.Comment_Created_Date = String.valueOf(dataList[i].com.CreatedDate);
          body.CommentList.add(ct);
        }
        feedReq.body = body;
          feedReq.inputHeaders = headers;
          string requestBody = json.serialize(feedReq);
          system.debug('requestbody' + requestBody);

          System.Httprequest req = new System.Httprequest();
          HttpResponse res = new HttpResponse();
          req.setMethod('POST');
          req.setBody(requestBody);
        
          req.setEndpoint('callout:X012_013_ROI'+partialEndpoint);
          
          req.setTimeout(120000);
              
          req.setHeader('Content-Type', 'application/json');
          req.setHeader('Accept', 'application/json');
      
          Http http = new Http();
          Datetime requestTime = system.now();   
          res = http.send(req);
          Datetime responseTime = system.now();
          //system.debug('***datalistsize : ' + dataList.size());
          if(res.getStatusCode() == 200){
            //success
            system.debug('res.getBody() : ' + res.getBody());
            Response response = (SVC_FeedCommentTriggerHandler.Response)JSON.deserialize(res.getBody(), SVC_FeedCommentTriggerHandler.Response.class);
            
            system.debug('response : ' + response);
          
            Message_Queue__c mq = setMessageQueue(dataList[0].ca.Id, response, requestTime, responseTime);
            //mqList.add(mq);
            //insert mqList;
            insert mq;

            SVC_FeedCommentTriggerHandler.ResponseBody resbody = response.body;
            system.debug('resbody : ' + resbody);

          }
          else{
            //Fail
              MessageLog messageLog = new MessageLog();
              messageLog.body = 'ERROR : http status code = ' +  res.getStatusCode();
              messageLog.MSGGUID = feedReq.inputHeaders.MSGGUID;
              messageLog.IFID = feedReq.inputHeaders.IFID;
              messageLog.IFDate = feedReq.inputHeaders.IFDate;

              Message_Queue__c mq = setHttpQueue(dataList[0].ca.Id, messageLog, requestTime, responseTime);
              //mqList.add(mq);
              //insert mqList;
              insert mq;       
          }
      }
      else{
        system.debug('There is no satisfying condition.');
      }  
  }
  public class Data {
    public FeedComment com;
    public String feedItemid;
    public String taskId;
    public String eventId;
    public String caseId;
    public Case ca;
  }
    public class FeedCommentRequest{
        public cihStub.msgHeadersRequest inputHeaders;        
        public SVC_GCICJSONRequestClass.HWTicketStatusJSON body;    
    }
    public class Response{
        cihStub.msgHeadersResponse inputHeaders;
        ResponseBody body;
    }
    public class ResponseBody{
        public String RET_CODE;
        public String RET_Message;
    }
    public static Message_Queue__c setMessageQueue(Id caseId, Response response, Datetime requestTime, Datetime responseTime) {
        cihStub.msgHeadersResponse inputHeaders = response.inputHeaders;
        SVC_FeedCommentTriggerHandler.ResponseBody body = response.body;

        Message_Queue__c mq = new Message_Queue__c();

        mq.ERRORCODE__c = 'CIH :' + SVC_UtilityClass.nvl(inputHeaders.ERRORCODE) + ', GCIC : ' + SVC_UtilityClass.nvl(body.RET_CODE);
        mq.ERRORTEXT__c = 'CIH :' + SVC_UtilityClass.nvl(inputHeaders.ERRORTEXT) + ', GCIC : ' + SVC_UtilityClass.nvl(body.RET_Message); 
        mq.IFDate__c = inputHeaders.IFDate;
        mq.IFID__c = inputHeaders.IFID;
        mq.Identification_Text__c = caseId;
        mq.Initalized_Request_Time__c = requestTime;//Datetime
        mq.Integration_Flow_Type__c = 'SVC-07'; // 
        mq.Is_Created__c = true;
        mq.MSGGUID__c = inputHeaders.MSGGUID;
        mq.MSGSTATUS__c = inputHeaders.MSGSTATUS;
        mq.MSGUID__c = inputHeaders.MSGGUID;
        mq.Object_Name__c = 'Case';
        mq.Request_To_CIH_Time__c = requestTime.getTime();//Number(15, 0)
        mq.Response_Processing_Time__c = responseTime.getTime();//Number(15, 0)
        mq.Status__c = (body.RET_CODE == '0') ? 'success' : 'failed';

        return mq;
    }
    //http error save.
        public static Message_Queue__c setHttpQueue(Id caseId, MessageLog messageLog, Datetime requestTime, Datetime responseTime) {
            Message_Queue__c mq       = new Message_Queue__c();
            mq.ERRORCODE__c         = messageLog.body;
            mq.ERRORTEXT__c         = messageLog.body;
            mq.IFDate__c           = messageLog.IFDate;
            mq.IFID__c             = messageLog.IFID;
            mq.Identification_Text__c     = caseId;
            mq.Initalized_Request_Time__c   = requestTime;//Datetime
            mq.Integration_Flow_Type__c   = 'SVC-07';
            mq.Is_Created__c         = true;
            mq.MSGGUID__c           = messageLog.MSGGUID;
            mq.MSGUID__c            = messageLog.MSGGUID;
        //mq.MSGSTATUS__c = messageLog.MSGSTATUS;
            mq.Object_Name__c         = 'Case';
            mq.Request_To_CIH_Time__c     = requestTime.getTime();//Number(15, 0)
            mq.Response_Processing_Time__c   = responseTime.getTime();//Number(15, 0)
            mq.Status__c           = 'failed'; // picklist

            return mq;
        }
    public class MessageLog {
        public Integer status;
        public String body;
        public String MSGGUID;
        public String IFID;
        public String IFDate;
        public String MSGSTATUS;
        public String ERRORCODE;
        public String ERRORTEXT;
    }
}