/**
 * Created by ms on 2017-08-07.
 *
 * author : JeongHo.Lee, I2MAX
 */
global without sharing class SVC_GCICAllFeedItemBatch implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts {
    
    //public String     caseId;// for batch
    public Set<Id> caseIds;
    //public Id
    public List<Data> dataList;
    public List<Comment> commentList;
    public List<Message_Queue__c> mqList;

    public static void processBatchClass(Set<Id> ids){
        SVC_GCICAllFeedItemBatch ba = new SVC_GCICAllFeedItemBatch();
        ba.caseIds = ids;
        Database.executeBatch(ba, 50);
    }

    public SVC_GCICAllFeedItemBatch(){
        //caseIds = ids;
        system.debug('batch start');
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {

        String query = '';
        query += ' SELECT  Id, RecordType.Name, CaseNumber, Parent_Case__c, ParentId, Service_Order_Number__c, Status, RecordType.DeveloperName ';
        query += ' FROM Case WHERE Id in: caseIds';
        query += ' AND (RecordType.DeveloperName = \'Repair\' OR RecordType.DeveloperName = \'Repair_To_Exchange\' OR RecordType.DeveloperName = \'Exchange\')';
        query += ' AND Service_Order_Number__c != NULL ';
        query += ' AND Parent_Case__c = FALSE ';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Case> scope) {

        system.debug('***scope.size : ' + scope.size());
        mqList = new List<Message_Queue__c>();
        Set<Id> caseParentIds = new Set<Id>();
        for(Case ca : scope){
            if(ca.ParentId != null) caseParentIds.add(ca.ParentId);
        }

        dataList = new List<Data>();

        Set<Id> commentIds = new Set<Id>();
        for(FeedItem fi : [SELECT Id, ParentId, Parent.Type, Body ,CreatedDate, CreatedBy.Profile.Name,CreatedBy.Name ,Type 
                                    FROM FeedItem 
                                    WHERE (ParentId in: caseIds OR ParentId in: caseParentIds)
                                    AND Parent.Type  =: 'Case'
                                    AND (Type =: 'TextPost' OR Type =: 'EmailMessageEvent')]){
                 
            if(fi.CreatedBy.Profile.Name != 'Integration User'){
                commentIds.add(fi.id);  
                Data dt             = new Data();
                dt.comment          = fi.Body;
                dt.createdBy        = fi.CreatedBy.Name;
                dt.createdDate      = String.valueOf(fi.CreatedDate);
                dt.caseId           = fi.ParentId;
                dt.feedId           = fi.Id;
                dt.Type             = fi.Type;
                dataList.add(dt);
            }
        }

        system.debug('feed dataList : ' + dataList);
        for(Task ta : [SELECT Id, WhatId, Description, CreatedDate, CreatedBy.Profile.Name ,CreatedBy.Name, Subject 
                                FROM Task 
                                WHERE (WhatId in: caseIds OR WhatId in: caseParentIds)
                                AND What.Type = 'Case']){
                
            if(ta.CreatedBy.Profile.Name != 'Integration User'){
                commentIds.add(ta.Id);  
                Data dt = new Data();
                if(ta.Subject == 'Call'){ 
                    dt.comment      = 'Log a Call - ';
                    dt.comment      += ta.Description;
                }
                else dt.comment     = ta.Description;
                dt.createdBy        = ta.CreatedBy.Name;
                dt.createdDate      = String.valueOf(ta.CreatedDate);
                dt.caseId           = ta.WhatId;
                dt.taskId           = ta.Id;
                dt.Type             = 'Task';
                dataList.add(dt);
            }
        }
        system.debug('Task dataList : ' + dataList);
        for(Event evt : [SELECT Id, WhatId, Description, CreatedDate, CreatedBy.Profile.Name ,CreatedBy.Name 
                            FROM Event 
                            WHERE (WhatId in: caseIds  OR WhatId in: caseParentIds)
                            AND What.Type = 'Case']){
                
            if(evt.CreatedBy.Profile.Name != 'Integration User'){
                commentIds.add(evt.Id); 
                Data dt = new Data();
                dt.comment          = evt.Description;
                dt.createdBy        = evt.CreatedBy.Name;
                dt.createdDate      = String.valueOf(evt.CreatedDate);
                dt.caseId           = evt.WhatId;
                dt.eventId          = evt.Id;
                dt.Type             = 'Event';
                dataList.add(dt);
            }
        }
        system.debug('Event dataList : ' + dataList);
        commentList = new List<Comment>();
        for(FeedComment fc : [SELECT Id,FeedItemId,ParentId,CreatedBy.Profile.Name,InsertedById,InsertedBy.Name,CommentBody,CreatedDate, CreatedBy.Name
                                FROM FeedComment 
                                WHERE (ParentId in: commentIds OR ParentId in: caseIds OR ParentId in: caseParentIds)]){
            if(fc.CreatedBy.Profile.Name != 'Integration User'){
                Comment com = new Comment();
                com.comment         = fc.CommentBody;
                com.createdBy       = fc.CreatedBy.Name;
                com.createdDate     = String.valueOf(fc.CreatedDate);
                com.commentId       = fc.ParentId;
                com.feedItemId      = fc.FeedItemId;
                commentList.add(com);
            }
        }
        system.debug('***dataList : ' + dataList);
        system.debug('***dataList.size : ' + dataList.size());
        system.debug('***comment.size' + commentList.size());
        system.debug('***commentList : ' + commentList);
        if(!dataList.isEmpty()){
            //Multi 5
            for(Case ca : scope){

                String layoutId = Integration_EndPoints__c.getInstance('SVC-07').layout_id__c;
                String partialEndpoint = Integration_EndPoints__c.getInstance('SVC-07').partial_endpoint__c;

                FeedItemRequest feedReq = new FeedItemRequest();
                SVC_GCICJSONRequestClass.HWTicketStatusJSON body = new SVC_GCICJSONRequestClass.HWTicketStatusJSON();
                cihStub.msgHeadersRequest headers = new cihStub.msgHeadersRequest(layoutId);
                body.CommentList = new List<SVC_GCICJSONRequestClass.Comment>();
                body.Service_Ord_no = ca.Service_Order_Number__c;
                body.Case_NO        = ca.CaseNumber;
                body.Ticket_status  = ' ';
                if (ca.Status == 'Cancelled' && ca.RecordType.DeveloperName == 'Repair') body.Ticket_status = 'ST051';
                if (ca.Status == 'Cancelled' && ca.RecordType.DeveloperName == 'Exchange') body.Ticket_status = 'ES080';

                for(Data dt : dataList){
                    
                    //feedItem, task, event
                    if((dt.caseId == ca.Id || dt.caseId == ca.ParentId) && dt.Type != 'EmailMessageEvent'){
                        SVC_GCICJSONRequestClass.Comment ct = new SVC_GCICJSONRequestClass.Comment();
                        ct.Comment = dt.Comment;
                        ct.Comment_Created_by = dt.createdBy;
                        ct.Comment_Created_Date = dt.createdDate;
                        body.CommentList.add(ct);
                    }
                    for(Comment com : commentList){
                        if(dt.caseId == com.commentId && dt.feedId == com.FeedItemId && (dt.Type == 'TextPost' || dt.Type == 'EmailMessageEvent')&& (dt.caseId == ca.Id || dt.caseId == ca.ParentId)){
                            SVC_GCICJSONRequestClass.Comment ct = new SVC_GCICJSONRequestClass.Comment();
                            ct.Comment = com.Comment;
                            ct.Comment_Created_by = com.createdBy;
                            ct.Comment_Created_Date = com.createdDate;
                            body.CommentList.add(ct);
                            //break;
                        }
                        else if(dt.taskId != null && dt.taskId == com.commentId && (dt.caseId == ca.Id || dt.caseId == ca.ParentId)){
                            SVC_GCICJSONRequestClass.Comment ct = new SVC_GCICJSONRequestClass.Comment();
                            ct.Comment = com.Comment;
                            ct.Comment_Created_by = com.createdBy;
                            ct.Comment_Created_Date = com.createdDate;
                            body.CommentList.add(ct);
                            //break;
                        }
                        else if(dt.eventId != null && dt.eventId == com.commentId && (dt.caseId == ca.Id || dt.caseId == ca.ParentId)){
                            SVC_GCICJSONRequestClass.Comment ct = new SVC_GCICJSONRequestClass.Comment();
                            ct.Comment = com.Comment;
                            ct.Comment_Created_by = com.createdBy;
                            ct.Comment_Created_Date = com.createdDate;
                            body.CommentList.add(ct);
                            //break;
                        }
                    }
                }
                
                feedReq.body = body;
                feedReq.inputHeaders = headers;
                string requestBody = json.serialize(feedReq);
                system.debug('requestbody' + requestBody);

                System.Httprequest req = new System.Httprequest();
                HttpResponse res = new HttpResponse();
                req.setMethod('POST');
                req.setBody(requestBody);
              
                req.setEndpoint('callout:X012_013_ROI'+partialEndpoint);
                
                req.setTimeout(120000);
                    
                req.setHeader('Content-Type', 'application/json');
                req.setHeader('Accept', 'application/json');
            
                Http http = new Http();
                Datetime requestTime = system.now();   
                res = http.send(req);
                Datetime responseTime = system.now();
                //system.debug('***datalistsize : ' + dataList.size());
                if(res.getStatusCode() == 200){
                    //success
                    system.debug('res.getBody() : ' + res.getBody());
                    Response response = (SVC_GCICAllFeedItemBatch.Response)JSON.deserialize(res.getBody(), SVC_GCICAllFeedItemBatch.Response.class);
                    
                    system.debug('response : ' + response);
                
                    Message_Queue__c mq = setMessageQueue(ca.Id, response, requestTime, responseTime);
                    mqList.add(mq);

                    SVC_GCICAllFeedItemBatch.ResponseBody resbody = response.body;
                    system.debug('resbody : ' + resbody);

                }
                else{
                    //Fail
                    MessageLog messageLog = new MessageLog();
                    messageLog.body = 'ERROR : http status code = ' +  res.getStatusCode();
                    messageLog.MSGGUID = feedReq.inputHeaders.MSGGUID;
                    messageLog.IFID = feedReq.inputHeaders.IFID;
                    messageLog.IFDate = feedReq.inputHeaders.IFDate;

                    Message_Queue__c mq = setHttpQueue(ca.Id, messageLog, requestTime, responseTime);
                    mqList.add(mq);     
                }
            }
        }
        if(!mqList.isEmpty()){
            insert mqList;
        }
    }

    global void finish(Database.BatchableContext BC) {
    }
    public class Data {
        public String comment;
        public String createdBy;
        public String createdDate;
        public String caseId;
        public String feedId;
        public String taskId;
        public String eventId;
        public String Type;
    }
    public class Comment {
        public String comment;
        public String createdBy;
        public String createdDate;
        public String commentId;
        public String feedItemId;
    }
    public class FeedItemRequest{
        public cihStub.msgHeadersRequest inputHeaders;        
        public SVC_GCICJSONRequestClass.HWTicketStatusJSON body;    
    }
    public class Response{
        @TestVisible cihStub.msgHeadersResponse inputHeaders;
        @TestVisible ResponseBody body;
    }
    public class ResponseBody{
        public String RET_CODE;
        public String RET_Message;
    }
    public static Message_Queue__c setMessageQueue(Id caseId, Response response, Datetime requestTime, Datetime responseTime) {
        cihStub.msgHeadersResponse inputHeaders = response.inputHeaders;
        SVC_GCICAllFeedItemBatch.ResponseBody body = response.body;

        Message_Queue__c mq = new Message_Queue__c();

        mq.ERRORCODE__c = 'CIH :' + SVC_UtilityClass.nvl(inputHeaders.ERRORCODE) + ', GCIC : ' + SVC_UtilityClass.nvl(body.RET_CODE);
        mq.ERRORTEXT__c = 'CIH :' + SVC_UtilityClass.nvl(inputHeaders.ERRORTEXT) + ', GCIC : ' + SVC_UtilityClass.nvl(body.RET_Message); 
        mq.IFDate__c = inputHeaders.IFDate;
        mq.IFID__c = inputHeaders.IFID;
        mq.Identification_Text__c = caseId;
        mq.Initalized_Request_Time__c = requestTime;//Datetime
        mq.Integration_Flow_Type__c = 'SVC-07'; // 
        mq.Is_Created__c = true;
        mq.MSGGUID__c = inputHeaders.MSGGUID;
        mq.MSGSTATUS__c = inputHeaders.MSGSTATUS;
        mq.MSGUID__c = inputHeaders.MSGGUID;
        mq.Object_Name__c = 'Case';
        mq.Request_To_CIH_Time__c = requestTime.getTime();//Number(15, 0)
        mq.Response_Processing_Time__c = responseTime.getTime();//Number(15, 0)
        mq.Status__c = (body.RET_CODE == '0') ? 'success' : 'failed';

        return mq;
    }
    //http error save.
    public static Message_Queue__c setHttpQueue(Id caseId, MessageLog messageLog, Datetime requestTime, Datetime responseTime) {
        Message_Queue__c mq             = new Message_Queue__c();
        mq.ERRORCODE__c                 = messageLog.body;
        mq.ERRORTEXT__c                 = messageLog.body;
        mq.IFDate__c                    = messageLog.IFDate;
        mq.IFID__c                      = messageLog.IFID;
        mq.Identification_Text__c       = caseId;
        mq.Initalized_Request_Time__c   = requestTime;//Datetime
        mq.Integration_Flow_Type__c     = 'SVC-07';
        mq.Is_Created__c                = true;
        mq.MSGGUID__c                   = messageLog.MSGGUID; 
        mq.MSGUID__c                    = messageLog.MSGGUID;
        //mq.MSGSTATUS__c = messageLog.MSGSTATUS;
        mq.Object_Name__c               = 'Case';
        mq.Request_To_CIH_Time__c       = requestTime.getTime();//Number(15, 0)
        mq.Response_Processing_Time__c  = responseTime.getTime();//Number(15, 0)
        mq.Status__c                    = 'failed'; // picklist

        return mq;
    }
    public class MessageLog {
        public Integer status;
        public String body;
        public String MSGGUID;
        public String IFID;
        public String IFDate;
        public String MSGSTATUS;
        public String ERRORCODE;
        public String ERRORTEXT;
    }
}