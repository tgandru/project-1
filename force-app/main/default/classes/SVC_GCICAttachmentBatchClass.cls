/**
 * Created by ryan on 7/20/2017.
 */

global class SVC_GCICAttachmentBatchClass implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts {

    public Set<Id> caseIds;

    public static void processBatchClass(Set<Id> cases){
        if(cases.size() > 0){
            SVC_GCICAttachmentBatchClass batch = new SVC_GCICAttachmentBatchClass();

            batch.caseIds = new Set<Id>();
            batch.caseIds.addAll(cases);

            Database.executeBatch(batch, 50);
        }
    }

    global Database.QueryLocator start(Database.BatchableContext ctx){//get the cases and most recent proof of purhcase for each case
        String query = 'SELECT Id, CreatedDate, ParentId, Service_Order_Number__c, CaseNumber, Parent_Case__c,';
        query += '(SELECT Id FROM Proof_Of_Purchases__r ORDER BY CreatedDate DESC LIMIT 1)';
        query += ' FROM Case WHERE Id IN: caseIds OR ParentId IN: caseIds';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext ctx, List<sObject> scope){
        
        List<Case> caseList = (List<Case>)scope;//our case list
        List<ContentVersion> contentVersionList;//list for storing all versions of documents for the Proof of Purchase record
        List<message_queue__c> mqList = new List<message_queue__c>();
        Set<Id> popIds = new Set<Id>();
        Map<Id, Id> proofofPurchaseIdToCaseIdMap = new Map<Id, Id>();
        Map<Id, Id> contentDocumentIdToCaseIdMap = new Map<Id, Id>();
        Map<Id, ContentVersion> caseIdtoContentVersionMap = new Map<Id, ContentVersion>();

        for(Case c: caseList){
            if(c.Proof_Of_Purchases__r != null && !c.Proof_Of_Purchases__r.isEmpty())
                proofofPurchaseIdToCaseIdMap.put(c.Proof_Of_Purchases__r.get(0).Id, c.Id);//add all proof of purchase ids into the map
        }

        if(proofofPurchaseIdToCaseIdMap.isEmpty()) return; //stop here if there are no proof of purchase records

        popIds = proofofPurchaseIdToCaseIdMap.keySet();

        //get all of the content document ids for files attached to the Proof of Purchase record
        for(ContentDocumentLink cdl : [SELECT ContentDocumentId, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId IN: popIds]){
            Id caseId = proofofPurchaseIdToCaseIdMap.get(cdl.LinkedEntityId);
            contentDocumentIdToCaseIdMap.put(cdl.ContentDocumentId, caseId); //FIX
        }

        //get a list of all Content Versions for the Content Document ids in the contentDocumentIdSet
        contentVersionList = [SELECT ContentDocumentId, ContentSize, ContentModifiedDate, FileType, Title, VersionData, VersionNumber
                                FROM ContentVersion 
                                WHERE IsDeleted = false 
                                AND ContentDocumentId IN: contentDocumentIdToCaseIdMap.keySet()
                                ORDER BY ContentModifiedDate DESC];

        for(ContentVersion cv: contentVersionList){
            Id caseId = contentDocumentIdToCaseIdMap.get(cv.ContentDocumentId);
            caseIdtoContentVersionMap.put(caseId, cv);
        }
        
        for(Case c: caseList){
            if(!c.Parent_Case__c){//only process child cases
                if(c.Proof_Of_Purchases__r != null && !c.Proof_Of_Purchases__r.isEmpty() && caseIdtoContentVersionMap.containsKey(c.Id)){//Check to see if child case has a PoP
                    SVC_GCICAttachmentClass.sendAttachment(caseIdtoContentVersionMap.get(c.Id), c.CaseNumber, c.Service_Order_Number__c);
                }else if(caseIdtoContentVersionMap.containsKey(c.ParentId)){//if no PoP, use the parent id
                    message_queue__c mq = SVC_GCICAttachmentClass.sendAttachment(caseIdtoContentVersionMap.get(c.ParentId), c.CaseNumber, c.Service_Order_Number__c);
                    
                    if(mq != null)
                        mqList.add(mq);
                }
            }
        }
        
        if(mqList.isEmpty())
        {
            insert mqList;
        }
    }

    global void finish(Database.BatchableContext ctx){

    }

}