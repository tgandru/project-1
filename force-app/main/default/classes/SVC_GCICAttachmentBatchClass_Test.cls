/**
 * Created by ryan on 7/21/2017.
 */

@IsTest
public class SVC_GCICAttachmentBatchClass_Test {

    static Proof_Of_Purchase__c proofOfPurchase;
    static Case cse;
    static ContentDocumentLink contentDocumentLink;

    @testSetup static void testData(){
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        //get record type ids for account
        Id acctRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Direct').getRecordTypeId();
        Id caseRT = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Exchange').getRecordTypeId();
        Id prodRT = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('B2B').getRecordTypeId();
        BusinessHours bh12 = [SELECT Id FROM BusinessHours WHERE name = 'B2B Service Hours 12x5'];

        Account acc = new Account(Name = 'Test Account', RecordTypeId = acctRT, Type = 'Distributor');
        insert acc;
        Contact con = new Contact(LastName = 'Test Contact', Email = 'test@email.com', Account = acc);
        insert con;
        RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];
        Product2 prod1 = new Product2(name ='MI-test', SAP_Material_ID__c = 'MI-test', sc_service_type__c = 'Incident', sc_service_type_ranking__c = '6', RecordTypeId = prodRT);
        insert prod1;
        Asset ast = new Asset(name ='Assert-MI-Test', AccountId = acc.id, Product2Id = prod1.id);
        insert ast;
        Slaprocess sla = [select id from SlaProcess where name like 'Enhanc%' and isActive = true limit 1 ];
        Entitlement ent = new Entitlement(name ='test Entitlement1', accountid=acc.id, assetid = ast.id, BusinessHoursId = bh12.id, startdate = system.today()-1, slaprocessid = sla.id, Units_Allowed__c = 100, Units_Exchanged__c = 0);
        insert ent;
        cse = new Case(AccountId = acc.Id, ContactId = con.Id, Subject = 'Test Case', Origin = 'Web', Severity__c = '4-Low', EntitlementId = ent.Id, Service_Order_Number__c = '4100167438');
        insert cse;
        proofOfPurchase = new Proof_Of_Purchase__c(Case__c = cse.Id, Status__c = 'Confirmed');
        insert proofOfPurchase;
        ContentVersion contentVersion = new ContentVersion(Title = 'TestAttachment', PathOnClient = 'TestAttachment.jpg', VersionData = Blob.valueOf('Test Content'), IsMajorVersion = true);
        insert contentVersion;
        ContentDocument document = [SELECT Id, Title FROM ContentDocument WHERE Title = 'TestAttachment' LIMIT 1];
        contentDocumentLink = new ContentDocumentLink(ContentDocumentId = document.Id, LinkedEntityId = proofOfPurchase.Id, ShareType = 'V');
        insert contentDocumentLink;
        External_Integration_EndPoints__c eie = new External_Integration_EndPoints__c(Name = 'GCIC Attachment', Username__c = 'TestUser', Password__c = 'TestPassword', EndPointURL__c = 'https://test.com', AccessKey__c = 'FIOSHOS');
        insert eie;
        Integration_EndPoints__c ie = new Integration_EndPoints__c(Name = 'SVC-04', endPointURL__c = 'www.samsung.com', layout_id__c = 'this', partial_endpoint__c = '/test');
        insert ie;
    }

    static testMethod void testBatchProcess() {
        cse = [SELECT Id, Subject FROM Case WHERE Subject = 'Test Case' LIMIT 1];
        System.assertNotEquals(cse, null);

        Set<Id> caseIds = new Set<Id>();
        caseIds.add(cse.Id);

        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new attachmentPOST401());
            SVC_GCICAttachmentBatchClass.processBatchClass(caseIds);
        Test.stopTest();
    }

    class attachmentPOST401 implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            String jsonBody = '{"inputHeaders":{"MSGGUID":"927c140c-6505-7296-64fc-1b0a9aad0806","IFID":"IF_SVC_04","IFDate":"20170722045053","MSGSTATUS":"S","ERRORTEXT":null},"body":{"Ret_code":"0","Ret_message":"Success."}}';
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody(jsonBody);
            res.setStatusCode(401);
            return res;
        }
    }
}