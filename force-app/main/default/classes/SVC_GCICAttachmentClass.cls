/**
 * Created by ryan on 7/17/2017.
 */

public with sharing class SVC_GCICAttachmentClass {

    public static Message_Queue__c sendAttachment(ContentVersion contentVersion, String caseNumber, String serviceOrderNumber){

        cihStub.msgHeadersRequest cih = new cihStub.msgHeadersRequest('IF_SVC_04');

        //create the object and its attributes to send to the endpoint
        request att = new request();
        SVC_GCICAttachmentClass.inputHeaders ih = new SVC_GCICAttachmentClass.inputHeaders();
        SVC_GCICAttachmentClass.requestBody bd = new SVC_GCICAttachmentClass.requestBody();
        ih.MSGGUID = cih.MSGGUID;
        ih.IFID = cih.IFID;
        ih.IFDate = cih.IFDate;
        bd.title_attchment = contentVersion.Title;
        bd.Attachments = contentVersion.VersionData;
        bd.Service_Ord_no = serviceOrderNumber;
        //bd.Service_Ord_no = '4100167438';
        bd.Case_NO = caseNumber;
        bd.category = 'P';
        bd.Size = String.valueOf(contentVersion.ContentSize);
        att.inputHeaders = ih;
        att.body = bd;

        //serialize the object to json format
        String jsonBody = JSON.serialize(att);
        System.debug('@@@ JSON Body: ' + jsonBody);

        //make the rest callout
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();

        string partialEndpoint = Integration_EndPoints__c.getInstance('SVC-04').partial_endpoint__c;

        req.setHeader('Accept', 'application/json');
        req.setHeader('Content-Type', 'application/json');
        req.setTimeout(120000);
        req.setMethod('POST');
        req.setEndpoint('callout:X012_013_ROI'+partialEndpoint);
        req.setBody(jsonBody);

        String result = '';
        try{
            res = h.send(req);
            result = res.getBody();

            System.debug('@@@ Result: ' + result);
            if(res.getStatusCode() != 200) throw new CalloutException();
        }catch(CalloutException ex){
            System.debug('Result Status and Code: ' + res.toString() + '. System Exception: ' + ex.getMessage() + ' on line number ' + ex.getLineNumber());
            return null;
        }

        response response = (SVC_GCICAttachmentClass.response)JSON.deserialize(result, SVC_GCICAttachmentClass.response.class);
        SVC_GCICAttachmentClass.responseBody resBody = response.body;

        //create the message queue record
        Message_Queue__c mq = new Message_Queue__c();
        mq.Status__c = resBody.Ret_code == '0' ? 'success' : 'failed';
        mq.IFID__c = cih.IFID;
        mq.MSGGUID__c = cih.MSGGUID;
        mq.MSGUID__c = cih.MSGGUID;
        mq.IFDate__c = cih.IFDate;
        mq.Integration_Flow_Type__c = 'IF_SVC_04';
        mq.MSGSTATUS__c = resBody.Ret_message;
        mq.Object_Name__c = 'Case';
        mq.Identification_Text__c = caseNumber;
        
        return mq;
        
        /*try{
            insert mq;
        }catch(DmlException e){
            System.debug('Error inserting message queue record: ' + e.getMessage() + e.getCause() + e.getDmlFields(0));
        }*/

    }

    class request {
        inputHeaders inputHeaders;
        requestBody body;
    }

    class inputHeaders {
        String MSGGUID;
        String IFID;
        String IFDate;
    }

    class requestBody {
        String Service_Ord_no;
        String Case_NO;
        String title_attchment;
        Blob Attachments;
        String category;
        String Size;
    }

    class response {
        responseBody body;
    }

    class responseBody {
        String Ret_code;
        String Ret_message;
    }

}