/**
 * Created by ms on 2017-08-07.
 *
 * author : JeongHo.Lee, I2MAX
 */
public with sharing class SVC_GCICBpCreation {
    @future(callout = true)
    public static void doCreation(Set<Id> caseIds){

        Case ca = [SELECT Id
                            , AccountId
                            , Account.BP_Number__c
                            , Account.Name
                            , Account.Fax
                            , Account.Phone
                            , Account.BillingStreet
                            , Account.BillingCity
                            , Account.BillingPostalCode
                            , Account.BillingState
                            , ContactId
                            , Contact.BP_Number__c
                            , Contact.FirstName
                            , Contact.LastName
                            , Contact.Fax
                            , Contact.MobilePhone
                            , Contact.Phone
                            , Contact.Email
                            , Contact.MailingStreet
                            , Contact.MailingCity
                            , Contact.MailingPostalCode
                            , Contact.MailingState
                    FROM Case 
                    WHERE id in: caseIds];

        String layoutId = Integration_EndPoints__c.getInstance('SVC-01').layout_id__c;
        String partialEndpoint = Integration_EndPoints__c.getInstance('SVC-01').partial_endpoint__c;

        BpInfoRequest BpInfoReq = new BpInfoRequest();
        SVC_GCICJSONRequestClass.BPCreationJSON body = new SVC_GCICJSONRequestClass.BPCreationJSON();
        cihStub.msgHeadersRequest headers = new cihStub.msgHeadersRequest(layoutId);

        if(ca.Account.BP_Number__c == null || ca.Contact.BP_Number__c == null){
            if(ca.Account.BP_Number__c == null){
                //Account
                body.Request_type       = 'A';
                String accName = ca.Account.Name;
                if(Account.Name != null && accName.length() > 40){
                    body.Account_Name2  = accName.substring(0,40);  
                    body.Account_Name1  = accName.substring(40);        
                }
                else if(Account.Name != null && accName.length() <= 40){
                    body.Account_Name2  = accName;
                }
                body.Account_FAX        = ca.Account.Fax;
                body.Account_TELEPHONE  = ca.Account.Phone;
                body.Account_ADDRESS    = ca.Account.BillingStreet;
                //body.Account_ADDRESS1 =
                //body.Account_ADDRESS2 =
                body.Account_CITY       = ca.Account.BillingCity;
                body.Account_ZIPCODE    = ca.Account.BillingPostalCode;
                body.Account_STATE      = ca.Account.BillingState;
                //body.Account_BP_Role  =

            }
            if(ca.Contact.BP_Number__c == null){
                //Contact
                body.Request_type       = 'C';
                body.Contact_FNAME      = ca.Contact.FirstName.left(20);
                body.Contact_LNAME      = ca.Contact.LastName.left(20);
                body.Contact_FAX        = ca.Contact.Fax;
                body.Contact_CELLPHONE  = ca.Contact.MobilePhone;
                body.Contact_TELEPHONE  = ca.Contact.Phone;
                body.Contact_EMAIL      = ca.Contact.Email;
                body.Contact_ADDRESS    = ca.Contact.MailingStreet;
                //body.Contact_ADDRESS1 =
                //body.Contact_ADDRESS2 =
                body.Contact_CITY       = ca.Contact.MailingCity;
                body.Contact_ZIPCODE    = ca.Contact.MailingPostalCode;
                body.Contact_STATE      = ca.Contact.MailingState;
                if(ca.Account.BP_Number__c != null) body.ACCOUNT_BP_NO = ca.Account.BP_Number__c;
                //body.Contact_COMM_TYPE    =
                //body.Contact_BP_ROLE  =
                
            }
            if(ca.Account.BP_Number__c == null && ca.Contact.BP_Number__c == null){
                //both
                body.Request_type = 'B';
            }

            BpInfoReq.body = body;
            BpInfoReq.inputHeaders = headers;
            string requestBody = json.serialize(BpInfoReq);
            system.debug(requestBody);

            System.Httprequest req = new System.Httprequest();
            HttpResponse res = new HttpResponse();
            req.setMethod('POST');
            req.setBody(requestBody);
          
            req.setEndpoint('callout:X012_013_ROI'+partialEndpoint);
            
            req.setTimeout(120000);
                
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('Accept', 'application/json');
            system.debug('req : ' + req);

            Http http = new Http();
            Datetime requestTime = system.now();   
            res = http.send(req);
            Datetime responseTime = system.now();

            if(res.getStatusCode() == 200){
                //success
                system.debug('res.getBody() : ' + res.getBody());
                Response response = (SVC_GCICBpCreation.Response)JSON.deserialize(res.getBody(), SVC_GCICBpCreation.Response.class);
            
                system.debug('response : ' + response);
        
                Message_Queue__c mq = setMessageQueue(ca.Id, response, requestTime, responseTime);
                insert mq;

                SVC_GCICBpCreation.ResponseBody resbody = response.body;
                system.debug('resbody : ' + resbody);
                if(resbody.RET_CODE == '0'){
                    if(body.Request_type == 'A'){
                        Account acc = [SELECT Id, BP_Number__c FROM Account WHERE Id =: ca.AccountId];
                        acc.BP_Number__c = resbody.Account_BP_no;
                        update acc;
                    }
                    else if(body.Request_type == 'B'){
                        Account acc = [SELECT Id, BP_Number__c FROM Account WHERE Id =: ca.AccountId];
                        Contact con = [SELECT Id, BP_Number__c FROM Contact WHERE Id =: ca.ContactId];

                        acc.BP_Number__c = resbody.Account_BP_no;
                        con.BP_Number__c = resbody.Contact_BP_no;
                        update acc;
                        update con; 

                    }
                    else if(body.Request_type == 'C'){
                        Contact con = [SELECT Id FROM Contact WHERE Id =: ca.ContactId];
                        con.BP_Number__c = resbody.Contact_BP_no;
                        update con;
                    }
                }
            }
            else{
                //Fail
                MessageLog messageLog = new MessageLog();
                messageLog.body = 'ERROR : http status code = ' +  res.getStatusCode();
                messageLog.MSGGUID = BpInfoReq.inputHeaders.MSGGUID;
                messageLog.IFID = BpInfoReq.inputHeaders.IFID;
                messageLog.IFDate = BpInfoReq.inputHeaders.IFDate;

                Message_Queue__c mq = setHttpQueue(ca.Id, messageLog, requestTime, responseTime);
                insert mq;
            }
        }
        else{
            system.debug('acc bp, con bp is not null');
        }
    }
    public class BpInfoRequest{
        public cihStub.msgHeadersRequest inputHeaders;        
        public SVC_GCICJSONRequestClass.BPCreationJSON body;    
    }
    public class Response{
        cihStub.msgHeadersResponse inputHeaders;
        ResponseBody body;
    }
    public class ResponseBody{
        public String RET_CODE;
        public String RET_Message;
        public String Contact_BP_no;
        public String Account_BP_no;
    }
    public static Message_Queue__c setMessageQueue(Id caseId, Response response, Datetime requestTime, Datetime responseTime) {
        cihStub.msgHeadersResponse inputHeaders = response.inputHeaders;
        SVC_GCICBpCreation.ResponseBody body = response.body;

        Message_Queue__c mq = new Message_Queue__c();

        mq.ERRORCODE__c = 'CIH :' + SVC_UtilityClass.nvl(inputHeaders.ERRORCODE) + ', GCIC : ' + SVC_UtilityClass.nvl(body.RET_CODE);
        mq.ERRORTEXT__c = 'CIH :' + SVC_UtilityClass.nvl(inputHeaders.ERRORTEXT) + ', GCIC : ' + SVC_UtilityClass.nvl(body.RET_Message); 
        mq.IFDate__c = inputHeaders.IFDate;
        mq.IFID__c = inputHeaders.IFID;
        mq.Identification_Text__c = caseId;
        mq.Initalized_Request_Time__c = requestTime;//Datetime
        mq.Integration_Flow_Type__c = 'SVC-01'; // 
        mq.Is_Created__c = true;
        mq.MSGGUID__c = inputHeaders.MSGGUID;
        mq.MSGSTATUS__c = inputHeaders.MSGSTATUS;
        mq.MSGUID__c = inputHeaders.MSGGUID;
        mq.Object_Name__c = 'Case';
        mq.Request_To_CIH_Time__c = requestTime.getTime();//Number(15, 0)
        mq.Response_Processing_Time__c = responseTime.getTime();//Number(15, 0)
        mq.Status__c = (body.RET_CODE == '0') ? 'success' : 'failed';

        return mq;
    }
    //http error save.
    public static Message_Queue__c setHttpQueue(Id caseId, MessageLog messageLog, Datetime requestTime, Datetime responseTime) {
        Message_Queue__c mq             = new Message_Queue__c();
        mq.ERRORCODE__c                 = messageLog.body;
        mq.ERRORTEXT__c                 = messageLog.body;
        mq.IFDate__c                    = messageLog.IFDate;
        mq.IFID__c                      = messageLog.IFID;
        mq.Identification_Text__c       = caseId;
        mq.Initalized_Request_Time__c   = requestTime;//Datetime
        mq.Integration_Flow_Type__c     = 'SVC-01';
        mq.Is_Created__c                = true;
        mq.MSGGUID__c                   = messageLog.MSGGUID;
        mq.MSGUID__c                    = messageLog.MSGGUID;
        //mq.MSGSTATUS__c = messageLog.MSGSTATUS;
        mq.Object_Name__c               = 'Case';
        mq.Request_To_CIH_Time__c       = requestTime.getTime();//Number(15, 0)
        mq.Response_Processing_Time__c  = responseTime.getTime();//Number(15, 0)
        mq.Status__c                    = 'failed'; // picklist

        return mq;
    }
    public class MessageLog {
        public Integer status;
        public String body;
        public String MSGGUID;
        public String IFID;
        public String IFDate;
        public String MSGSTATUS;
        public String ERRORCODE;
        public String ERRORTEXT;
    }
}