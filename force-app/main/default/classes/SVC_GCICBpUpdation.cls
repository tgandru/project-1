/**
 * Created by ms on 2017-08-29.
 *
 * author : JeongHo.Lee, I2MAX
 */

public without sharing class SVC_GCICBpUpdation {
    public static void accValidation(Account accObj){
        Account acc = accObj;
        
        List<Case> caseList = new List<Case>();
        caseList = [SELECT Id FROM Case 
                    WHERE AccountId =: acc.Id
                    AND Status != 'Cancelled' 
                    AND Status != 'Closed' 
                    AND (RecordType.DeveloperName ='Repair' OR RecordType.DeveloperName ='Exchange' OR RecordType.DeveloperName ='R2E') limit 1];
        if(caseList.size() > 0){
            //acc.addError('The Case is Still Open.');
        }
        else
            SVC_GCICBpUpdation.doUpdation(String.valueOf(acc.Id));
    }
    public static void conValidation(Contact conObj){
        Contact con = conObj;
        
        List<Case> caseList = new List<Case>();
        caseList = [SELECT Id FROM Case WHERE ContactId =: con.Id
                    AND Status != 'Cancelled' 
                    AND Status != 'Closed' 
                    AND (RecordType.DeveloperName ='Repair' OR RecordType.DeveloperName ='Exchange' OR RecordType.DeveloperName ='R2E') limit 1];
        if(caseList.size() > 0){
            //con.addError('The Case is Still Open.');
        }
        else
            SVC_GCICBpUpdation.doUpdation(String.valueOf(con.Id));
    }
    
    @future(callout = true)
    public static void doUpdation(String ids){
        String obj = '';
        Account acc;
        Contact con;

        String layoutId = Integration_EndPoints__c.getInstance('SVC-02').layout_id__c;
        String partialEndpoint = Integration_EndPoints__c.getInstance('SVC-02').partial_endpoint__c;
        BpInfoRequest BpInfoReq = new BpInfoRequest();
        SVC_GCICJSONRequestClass.BPUpdateJSON body = new SVC_GCICJSONRequestClass.BPUpdateJSON();
        cihStub.msgHeadersRequest headers = new cihStub.msgHeadersRequest(layoutId);

        //Account
        if(ids.startsWith('001')){
            obj = 'Account';
            acc = [SELECT Id
                    , Name
                    , Fax
                    , Phone
                    , BillingStreet
                    , BillingCity
                    , BillingPostalCode
                    , BillingState
                    , BP_Number__c
                    FROM Account
                    WHERE Id =: ids];

            //Account
            body.Request_type       = 'A';
            String accName = acc.Name;
            if(acc.Name != null && accName.length() > 40){
                body.ACCOUNT_Name2  = accName.substring(0,40);  
                body.ACCOUNT_Name1  = accName.substring(40);        
            }
            else if(acc.Name != null && accName.length() <= 40){
                body.ACCOUNT_Name2  = accName;
            }
            body.ACCOUNT_FAX        = acc.Fax;
            body.ACCOUNT_TELEPHONE  = acc.Phone;
            body.ACCOUNT_ADDRESS    = acc.BillingStreet;
            //body.Account_ADDRESS1 =
            //body.Account_ADDRESS2 =
            body.ACCOUNT_CITY       = acc.BillingCity;
            body.ACCOUNT_ZIPCODE    = acc.BillingPostalCode;
            body.ACCOUNT_STATE      = acc.BillingState;
            body.ACCOUNT_BP_NO      = acc.BP_Number__c;
        }
        //Contact
        else if(ids.startsWith('003')){
            obj = 'Contact';
            con = [SELECT Id
                    , FirstName
                    , LastName
                    , Fax
                    , MobilePhone
                    , Phone
                    , Email
                    , MailingStreet
                    , MailingCity
                    , MailingPostalCode
                    , MailingState
                    , Account.BP_Number__c
                    , Account.Name
                    , Account.Fax
                    , Account.Phone
                    , Account.BillingStreet
                    , Account.BillingCity
                    , Account.BillingPostalCode
                    , Account.BillingState
                    , BP_Number__c
                    FROM Contact
                    WHERE Id =: ids];

            body.REQUEST_TYPE       = 'C';
            body.CONTACT_FNAME      = con.FirstName.left(20);
            body.CONTACT_LNAME      = con.LastName.left(20);
            body.CONTACT_FAX        = con.Fax;
            body.CONTACT_CELLPHONE  = con.MobilePhone;
            body.CONTACT_TELEPHONE  = con.Phone;
            body.CONTACT_EMAIL      = con.Email;
            body.CONTACT_ADDRESS    = con.MailingStreet;
            //body.Contact_ADDRESS1 =
            //body.Contact_ADDRESS2 =
            body.CONTACT_CITY       = con.MailingCity;
            body.CONTACT_ZIPCODE    = con.MailingPostalCode;
            body.CONTACT_STATE      = con.MailingState;
            body.CONTACT_BP_NO      = con.BP_Number__c;
            //body.Contact_COMM_TYPE    =
            //body.Contact_BP_ROLE  =
        }

        BpInfoReq.body = body;
        BpInfoReq.inputHeaders = headers;
        string requestBody = json.serialize(BpInfoReq);
        system.debug(requestBody);

        System.Httprequest req = new System.Httprequest();
        HttpResponse res = new HttpResponse();
        req.setMethod('POST');
        req.setBody(requestBody);
      
        req.setEndpoint('callout:X012_013_ROI'+partialEndpoint);
        
        req.setTimeout(120000);
            
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Accept', 'application/json');
        system.debug('req : ' + req);

        Http http = new Http();
        Datetime requestTime = system.now();   
        res = http.send(req);
        Datetime responseTime = system.now();

        if(res.getStatusCode() == 200){
            //success
            system.debug('res.getBody() : ' + res.getBody());
            Response response = (SVC_GCICBpUpdation.Response)JSON.deserialize(res.getBody(), SVC_GCICBpUpdation.Response.class);
        
            system.debug('response : ' + response);
    
            Message_Queue__c mq = setMessageQueue(ids, response, requestTime, responseTime);
            insert mq;

            SVC_GCICBpUpdation.ResponseBody resbody = response.body;
            system.debug('resbody : ' + resbody);
            String str = (body.REQUEST_TYPE == 'A') ? 'Account' : 'Contact';
            if(resbody.EvRetCode == '0'){
                SVC_UtilityClass.postToChatter(ids, 'Succeed '+ str +' BP Information updating to GCIC');
            }
            else if(resbody.EvRetCode == '1') SVC_UtilityClass.postToChatter(ids, 'Failed' + str +' BP information updating to GCIC \n' + resbody.EvRetMsg);
                            
            else SVC_UtilityClass.postToChatter(ids, 'Failed' + str +' BP information updating to GCIC');

        }
        else{
            //Fail
            MessageLog messageLog = new MessageLog();
            messageLog.body = 'ERROR : http status code = ' +  res.getStatusCode();
            messageLog.MSGGUID = BpInfoReq.inputHeaders.MSGGUID;
            messageLog.IFID = BpInfoReq.inputHeaders.IFID;
            messageLog.IFDate = BpInfoReq.inputHeaders.IFDate;

            Message_Queue__c mq = setHttpQueue(ids, messageLog, requestTime, responseTime);
            insert mq;
        }
    }
    public class BpInfoRequest{
        public cihStub.msgHeadersRequest inputHeaders;        
        public SVC_GCICJSONRequestClass.BPUpdateJSON body;    
    }
    public class Response{
        cihStub.msgHeadersResponse inputHeaders;
        ResponseBody body;
    }
    public class ResponseBody{
        public String EvRetCode;
        public String EvRetMsg;
    }
    public static Message_Queue__c setMessageQueue(Id objId, Response response, Datetime requestTime, Datetime responseTime) {
        cihStub.msgHeadersResponse inputHeaders = response.inputHeaders;
        SVC_GCICBpUpdation.ResponseBody body = response.body;

        Message_Queue__c mq = new Message_Queue__c();

        mq.ERRORCODE__c = 'CIH :' + SVC_UtilityClass.nvl(inputHeaders.ERRORCODE) + ', GCIC : ' + SVC_UtilityClass.nvl(body.EvRetCode);
        mq.ERRORTEXT__c = 'CIH :' + SVC_UtilityClass.nvl(inputHeaders.ERRORTEXT) + ', GCIC : ' + SVC_UtilityClass.nvl(body.EvRetMsg); 
        mq.IFDate__c = inputHeaders.IFDate;
        mq.IFID__c = inputHeaders.IFID;
        mq.Identification_Text__c = objId;
        mq.Initalized_Request_Time__c = requestTime;//Datetime
        mq.Integration_Flow_Type__c = 'SVC-02'; // 
        mq.Is_Created__c = true;
        mq.MSGGUID__c = inputHeaders.MSGGUID;
        mq.MSGSTATUS__c = inputHeaders.MSGSTATUS;
        mq.MSGUID__c = inputHeaders.MSGGUID;
        mq.Object_Name__c = (String.valueOf(objId).startsWith('001')) ? 'Account' : 'Contact';
        mq.Request_To_CIH_Time__c = requestTime.getTime();//Number(15, 0)
        mq.Response_Processing_Time__c = responseTime.getTime();//Number(15, 0)
        mq.Status__c = (body.EvRetCode == '0') ? 'success' : 'failed';

        return mq;
    }
    //http error save.
    public static Message_Queue__c setHttpQueue(Id objId, MessageLog messageLog, Datetime requestTime, Datetime responseTime) {
        Message_Queue__c mq             = new Message_Queue__c();
        mq.ERRORCODE__c                 = messageLog.body;
        mq.ERRORTEXT__c                 = messageLog.body;
        mq.IFDate__c                    = messageLog.IFDate;
        mq.IFID__c                      = messageLog.IFID;
        mq.Identification_Text__c       = objId;
        mq.Initalized_Request_Time__c   = requestTime;//Datetime
        mq.Integration_Flow_Type__c     = 'SVC-02';
        mq.Is_Created__c                = true;
        mq.MSGGUID__c                   = messageLog.MSGGUID;
        mq.MSGUID__c                    = messageLog.MSGGUID;
        //mq.MSGSTATUS__c = messageLog.MSGSTATUS;
        mq.Object_Name__c               = (String.valueOf(objId).startsWith('001')) ? 'Account' : 'Contact';
        mq.Request_To_CIH_Time__c       = requestTime.getTime();//Number(15, 0)
        mq.Response_Processing_Time__c  = responseTime.getTime();//Number(15, 0)
        mq.Status__c                    = 'failed'; // picklist

        return mq;
    }
    public class MessageLog {
        public Integer status;
        public String body;
        public String MSGGUID;
        public String IFID;
        public String IFDate;
        public String MSGSTATUS;
        public String ERRORCODE;
        public String ERRORTEXT;
    }
}