/**
 * Created by ms on 2017-08-30.
 *
 * author : JeongHo.Lee, I2MAX
 */

@isTest
private class SVC_GCICBpUpdation_Test {

    @isTest static void AccountOpenTest() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        //Repair Insert,Update
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='SVC-02';
        iep2.endPointURL__c = 'https://www.google.com';
        iep2.layout_id__c = 'LAY025202';
        iep2.partial_endpoint__c = '/IF_SVC_02';
        insert iep2;

        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'BpCreationCheckTestJeongHoLeeAccountName40',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );

        insert testAccount1;

        Product2 prod1 = new Product2(
                name ='SM-G950UZKAVZW',
                SAP_Material_ID__c = 'SM-G950UZKAVZW',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = rtMap.get('Product2'+'B2B')
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent;

        // Insert Contact non-named caller 
        Contact contact1 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest2@svctest.com', 
            LastName = 'Named Caller2',
            BP_Number__c = '12345678',
            MailingCountry = 'US',
            MailingState = 'NJ',
            FirstName = 'test',
            Named_Caller__c = true);
        insert contact1; 

        //repair Valid imei
        Case c1 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Repair'),
            status='New',
            Imei__c = '35833008016052'
            );
        insert c1;

        Test.startTest();
        String body = '{"inputHeaders":{"MSGGUID":"28f20955-512a-90ca-cab5-2f18410ba02c","IFID":"IF_SVC_01","IFDate":"20170728231923","MSGSTATUS":"S","ERRORTEXT":null},"body":{"EvRetCode":"0","EvRetMsg":null}}';
        TestMockSingleCallout singleMock = new TestMockSingleCallout(body);
        Test.setMock(HttpCalloutMock.class, singleMock);
        SVC_GCICBpUpdation.accValidation(testAccount1);
        Test.stopTest();
    }
    @isTest static void AccountCloseTest() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        //Repair Insert,Update
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='SVC-02';
        iep2.endPointURL__c = 'https://www.google.com';
        iep2.layout_id__c = 'LAY025202';
        iep2.partial_endpoint__c = '/IF_SVC_02';
        insert iep2;

        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'BpCreationCheckTestJeongHoLeeAccountName40',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );

        insert testAccount1;

        Test.startTest();
        String body = '{"inputHeaders":{"MSGGUID":"28f20955-512a-90ca-cab5-2f18410ba02c","IFID":"IF_SVC_01","IFDate":"20170728231923","MSGSTATUS":"S","ERRORTEXT":null},"body":{"EvRetCode":"0","EvRetMsg":null}}';
        TestMockSingleCallout singleMock = new TestMockSingleCallout(body);
        Test.setMock(HttpCalloutMock.class, singleMock);
        SVC_GCICBpUpdation.accValidation(testAccount1);
        Test.stopTest();
    }
    @isTest static void ContactOpenTest() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        //Repair Insert,Update
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='SVC-02';
        iep2.endPointURL__c = 'https://www.google.com';
        iep2.layout_id__c = 'LAY025202';
        iep2.partial_endpoint__c = '/IF_SVC_02';
        insert iep2;

        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'BpCreationCheckTestJeongHoLeeAccountName40',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );

        insert testAccount1;

        Product2 prod1 = new Product2(
                name ='SM-G950UZKAVZW',
                SAP_Material_ID__c = 'SM-G950UZKAVZW',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = rtMap.get('Product2'+'B2B')
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent;

        // Insert Contact non-named caller 
        Contact contact1 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest2@svctest.com', 
            LastName = 'Named Caller2',
            BP_Number__c = '12345678',
            MailingCountry = 'US',
            MailingState = 'NJ',
            FirstName = 'test',
            Named_Caller__c = true);
        insert contact1; 

        //repair Valid imei
        Case c1 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Repair'),
            status='New',
            Imei__c = '35833008016052'
            );
        insert c1;

        Test.startTest();
        String body = '{"inputHeaders":{"MSGGUID":"28f20955-512a-90ca-cab5-2f18410ba02c","IFID":"IF_SVC_01","IFDate":"20170728231923","MSGSTATUS":"S","ERRORTEXT":null},"body":{"EvRetCode":"0","EvRetMsg":null}}';
        TestMockSingleCallout singleMock = new TestMockSingleCallout(body);
        Test.setMock(HttpCalloutMock.class, singleMock);
        SVC_GCICBpUpdation.conValidation(contact1);
        Test.stopTest();
    }
    @isTest static void ContactCloseTest() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        //Repair Insert,Update
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='SVC-02';
        iep2.endPointURL__c = 'https://www.google.com';
        iep2.layout_id__c = 'LAY025202';
        iep2.partial_endpoint__c = '/IF_SVC_02';
        insert iep2;

        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'BpCreationCheckTestJeongHoLeeAccountName40',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );

        insert testAccount1;

        Product2 prod1 = new Product2(
                name ='SM-G950UZKAVZW',
                SAP_Material_ID__c = 'SM-G950UZKAVZW',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = rtMap.get('Product2'+'B2B')
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent;

        // Insert Contact non-named caller 
        Contact contact1 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest2@svctest.com', 
            LastName = 'Named Caller2',
            BP_Number__c = '12345678',
            MailingCountry = 'US',
            MailingState = 'NJ',
            FirstName = 'test',
            Named_Caller__c = true);
        insert contact1; 

        Test.startTest();
        String body = '{"inputHeaders":{"MSGGUID":"28f20955-512a-90ca-cab5-2f18410ba02c","IFID":"IF_SVC_01","IFDate":"20170728231923","MSGSTATUS":"S","ERRORTEXT":null},"body":{"EvRetCode":"0","EvRetMsg":null}}';
        TestMockSingleCallout singleMock = new TestMockSingleCallout(body);
        Test.setMock(HttpCalloutMock.class, singleMock);
        SVC_GCICBpUpdation.conValidation(contact1);
        Test.stopTest();
    }
    @isTest static void BPUpdationErrorTest() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        //Repair Insert,Update
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='SVC-02';
        iep2.endPointURL__c = 'https://www.google.com';
        iep2.layout_id__c = 'LAY025202';
        iep2.partial_endpoint__c = '/IF_SVC_02';
        insert iep2;

        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660',
            BP_Number__c = '12345678'
        );

        insert testAccount1;

        Test.startTest();
        String body = '{"inputHeaders":{"MSGGUID":"28f20955-512a-90ca-cab5-2f18410ba02c","IFID":"IF_SVC_01","IFDate":"20170728231923","MSGSTATUS":"S","ERRORTEXT":null},"body":{"EvRetCode":"0","EvRetMsg":null}}';
        TestMockSingleCallout singleMock = new TestMockSingleCallout(body, 505);
        Test.setMock(HttpCalloutMock.class, singleMock);
        SVC_GCICBpUpdation.accValidation(testAccount1);
        Test.stopTest();
    }
}