global class SVC_GCICEchngRARequest {
    @AuraEnabled
     webservice static string GCICEchngRARequest(string csid){
         String rtn='';
       if(csid!=null&&csid!=''){
           Case cs=[SELECT AccountId,Account_Service_Type_Ranking__c,ASC_Code__c,ASC_Name__c,AssetId,Available_QTY__c,BusinessHoursId,Business_Priority__c,Bypass__c,Carrier__c,CaseNumber,Case_Classification__c,Case_Close_Comments__c,Case_Device_Status_Exchange__c,Case_Device_Status__c,Category_1__c,Category_2__c,Category__c,ClosedDate,Close_ETA__c,ContactEmail,ContactFax,ContactId,ContactMobile,ContactPhone,Contact_Business_Hours__c,Contact_Named_Caller__c,CreatedById,CreatedDate,Created_By_firstname__c,Created_By_Lastname__c,
                     Customer_Impact__c,Date_Assigned__c,Description,Developer_Assigned_To__c,Device_Type_Device__c,Device_Type_f__c,Device_Type__c,Device__c,Division__c,Due_Date__c,EAP__c,EntitlementId,Entitlement_Name__c,Entitlement__c,Error_Message__c,Exchange_Delivery_Type__c,Exchange_Device_Grade__c,Exchange_DO_Date__c,Exchange_DO_No__c,Exchange_GI_Date__c,Exchange_GI_No__c,Exchange_GI_Plant__c,Exchange_Model_Option__c,Exchange_Reason_GCIC__c,Exchange_Reason__c,Exchange_RMA_Date__c,Exchange_RMA_No__c,Exchange_Sales_Order__c,Exchange_Sales_Plant__c,Exchange_SO_Date__c,Exchange_Type_GCIC__c,Exchange_Type__c,
                     Extended_Warranty_Contract_Name__c,Extended_Warranty_Contract_No__c,Extended_Warranty_End_Date__c,Extended_Warranty_Information__c,Extended_Warranty_Start_Date__c,External_Escalation__c,External_Id__c,fm_Carrier__c,Follow_Up_Case__c,Frequency_CD__c,GCIC_Follow_SO_Number__c,GCIC_History_Tran_No__c,GCIC_Inbound_UPS_Status__c,GCIC_Inbound_UPS_URL_Link__c,GCIC_Inbound_UPS_URL__c,GCIC_Reason_Code__c,GCIC_Reason_Name__c,GCIC_Status_Code__c,GCIC_Status_Name__c,GCIC_Transferable__c,Generate_Shipping_Label__c,GS6_Plus_ADL__c,Id,IMEI_Number_Device__c,IMEI_Number__c,IMEI__c,Inbound_Shipping_Status__c,Inbound_Tracking_Number_Link__c,Inbound_Tracking_Number__c,Integration_Status__c,International_Device__c,IsClosed,IsDeleted,IsEscalated,IsStopped,Issue_Description__c,Is_Child__c,IS_SPAM__c,JIRA_Id__c,
                     Level2SymptomCode__c,Level_1__c,Level_3_Symptom_Code_del__c,Level_3_Symptom_Code__c,LIcense_Key__c,MDM_Type__c,MilestoneStatus,Model_Code_GCIC__c,Model_Code_Repair__c,Model_Code__c,Model__c,Note_5_ADL__c,Origin,Original_Case__c,Outbound_Shipping_Status__c,Outbound_Tracking_Number_Link__c,Outbound_Tracking_Number__c,OwnerId,ParentId,Parent_Case__c,POP_Source__c,Priority,Priority__c,ProductId,Product_Model__c,Product_Name__c,Purchase_Date__c,Reason,Reason_RMA_Status__c,RecordTypeId,Remaining_Units_for_Exchange__c,Remedy__c,Remote_Support__c,Repair_Start_Date__c,Replacement_Device_IMEI__c,Replacement_Device_Model_Code__c,Replacement_Device_Serial_No__c,Resolution_Category__c,Resolution__c,Selected_Model_Code__c,Selected_Model_Grade__c,Selected_Storage_Location__c,SerialNumber__c,Serial_Number_Device__c,Service_ID__c,Service_Order_Number__c,
                     Service_Provider_Account__c,Service_Provider_Contact__c,Severity__c,Shipping_Address_1__c,Shipping_Address_2__c,Shipping_Address_3__c,Shipping_Address_Controlled_By__c,Shipping_Address__c,Shipping_City__c,Shipping_Company__c,Shipping_Country__c,Shipping_Incoterms__c,Shipping_Instruction1__c,Shipping_Instruction__c,Shipping_State__c,Shipping_Zip_Code__c,SlaExitDate,SlaStartDate,Solution_Version__c,SourceId,SparkAFW__c,Status,Steps_to_reproduce__c,StopStartDate,Subject,Sub_Status__c,SuppliedCompany,SuppliedEmail,SuppliedName,SuppliedPhone,Support_SKU_Type__c,SVCisWithinBusinessHours__c,Symptom_Type__c,SystemModstamp,Temp_Case_Comment__c,Test_Case__c,Total_Allowed_Units_for_Exchange__c,Type,Unit_Received_Date_to_ship__c,Unit_Send_Date_to_ASC__c,Unit_Send_Date_to_Customer__c,UPS_Auto_Publish__c,URL__c,Warranty_End_Date__c,Warranty_Start_Date__c,
                     Warranty_Status__c,Zendesk_ID__c,Contact.BP_Number__c,contact.Name,contact.firstName,contact.LastName,RA_Request_Number__c FROM Case where id=:csid];
           
              msgHeadersRequest headers= new msgHeadersRequest();
             //
             if(cs.RA_Request_Number__c !=null){
                 return 'For this case RA Request already Submitted.';
             }else if(cs.Service_Order_Number__c == Null || cs.Service_Order_Number__c == '' ){
                 return 'Service Order Number is Missing';
             }else if(cs.Exchange_Reason__c ==Null){
                 return 'Please Select Exchage Reason value.';
             }else if(cs.Selected_Model_Grade__c == Null || cs.Selected_Model_Code__c ==null){
                  return 'Inventory Check data is Missing';
             }else if(cs.Contact.BP_Number__c == null){
                  return 'Contact BP Number is Missing';
             }else if(cs.ASC_Code__c==null){
                 return 'ASC code is Missing. Please Check ASC Lookup.';
             }else{
             
                IS_ORDERADM_H p2=new IS_ORDERADM_H();
                p2.OBJECT_ID=cs.Service_Order_Number__c;
              
                IS_ZTSVC104 p1=new IS_ZTSVC104();
                
                 String company;
                 String org;
                 String account;
                 String invlocation;
                    if ('Connected'.equals(cs.Device_Type_f__c)) {
                        company = 'C370';
                        org='3107';
                        account='0001901224';
                        invlocation='WS40';
                         p1.S_STORAGE_LOC='WS40';
                         p1.STORAGE_LOC='WS70';
                    } else if ('WiFi'.equals(cs.Device_Type_f__c)) {
                        company = 'C310';
                        org='3101';
                         account='TBD';
                         invlocation='TBD';
                         p1.S_STORAGE_LOC='TBD';
                         p1.STORAGE_LOC='TBD';
                    }
              
              
             
              p1.OBJECT_ID=cs.Service_Order_Number__c;
              p1.COMPANY=company;
              p1.EXCH_TYPE='ET010';
              p1.EXCH_REASON=cs.Exchange_Reason__c;
              p1.EXCH_GRADE=cs.Selected_Model_Grade__c;
              p1.NEW_MODEL=cs.Selected_Model_Code__c;
              p1.EXCH_SUB_REASON='';
             // p1.STORAGE_LOC=cs.Selected_Storage_Location__c;
              p1.SALES_ORG=org;
              p1.INV_LOCATION=invlocation; 
              p1.ORDER_TYPE='YRF0';
             // p1.S_STORAGE_LOC='TBD';
              p1.ACCOUNT=account;
              p1.BILLING_PT=account;
              p2.ZZCOLLECT_POINT=account;
              
              String fulladd='';
              if(cs.Shipping_Address_1__c !=null){
                  fulladd=cs.Shipping_Address_1__c;
              }
              if(cs.Shipping_Address_2__c !=null){
                  fulladd=fulladd+','+cs.Shipping_Address_2__c;
              }
              if(cs.Shipping_Address_3__c !=null){
                  fulladd=fulladd+','+cs.Shipping_Address_3__c;
              }
              
              CustmerAddress custadd=new CustmerAddress();
              custadd.BP_NO=cs.Contact.BP_Number__c;
              custadd.FULL_NAME=cs.contact.Name;
              custadd.STREET=cs.Shipping_Address_1__c;
              custadd.STR_SUPPL1=cs.Shipping_Address_2__c;
              custadd.STR_SUPPL2=cs.Shipping_Address_3__c;
              custadd.CITY=cs.Shipping_City__c;
              custadd.REGION=cs.Shipping_State__c;
              custadd.COUNTRY=cs.Shipping_Country__c;
              custadd.ZIP=cs.Shipping_Zip_Code__c;
              custadd.E_MAIL=cs.ContactEmail;
              custadd.TELEPHONE_M=cs.ContactMobile;
              custadd.TELEPHONE_H=cs.ContactPhone;
              custadd.FULL_STREET=fulladd;
              custadd.FIRSTNAME=cs.contact.firstName;
              custadd.LASTNAME=cs.contact.LastName;
              
              
              AscAdderss ascad=new AscAdderss();
              ascad.BP_NO=cs.ASC_Code__c;
              ascad.FULL_NAME=cs.ASC_Name__c;
              
              RequestBody body=new RequestBody();
                body.IS_ZTSVC104=p1;
                body.IS_CUSTOMER_ADDR=custadd;
                body.IS_SALES_ADDR=ascad;
                body.IS_ORDERADM_H=p2;
             
                 
               RARequest req=new RARequest();
               req.inputHeaders=headers;
               req.body=body;
               
               String jsonbody= JSON.serialize(req);
               system.debug('request :: ' + jsonbody);
           
               String response=webcallout(jsonbody);
              // String response='{"inputHeaders":{"MSGGUID":"2e697552-3d84-e23b-deaa-97be77249f85","IFID":"IF_SVC_11","IFDate":"20181212001845","MSGSTATUS":"S","ERRORTEXT":null},"body":{"EvEmployee":null,"EvErrCd":null,"EvErrMsg":null,"EvExchReqno":"0000059261","EvRaNo":null,"EvReason":"RM006","EvSoNo":null,"EvStatus":"ES010"}}';
                   if(response !='' && response !=null){
                        RAResponse res = (RAResponse) json.deserialize(response,RAResponse.class);
                            system.debug('##  Response ::'+res);
                            if(res.inputHeaders.MSGSTATUS == 'S'){
                                 
                                 rtn=handleSuccess(res,cs,jsonbody);
                                 String bypass = cs.Bypass__c;
                                 cs.RA_Request_Number__c=res.body.EvExchReqno;
                                 cs.Bypass__c=(bypass == 'N') ? 'Y' : 'N'; 
                                 update cs;
                             }else{
                                 
                                 rtn= handleError(res,cs,jsonbody);
                             }
                   }
          }    
           
       }else{
           
       }       
       return rtn;
    }
    
    
        public static string webcallout(string body){ 
         system.debug('************************************** INSIDE CALLOUT METHOD');
         System.debug('##requestBody ::'+body);
          string partialEndpoint = Test.isRunningTest() ? 'asdf' : Integration_EndPoints__c.getInstance('SVC-11').partial_endpoint__c;
        System.Httprequest req = new System.Httprequest();
        
        HttpResponse res = new HttpResponse();
        req.setMethod('POST');
        req.setBody(body);
        req.setEndpoint('callout:X012_013_ROI'+partialEndpoint); 
        
        req.setTimeout(120000);
         req.setHeader('Content-Type', 'application/json');
        req.setHeader('Accept', 'application/json');
    
        Http http = new Http(); 
        
          if(!Test.isRunningTest()){
                          res = http.send(req);
                        }else{
                           res = Testrespond(req);
                        }
        
        
        
        
        System.debug('******** my resonse' + res);
        system.debug('*****Response body::' + res.getBody());
       
        return res.getBody();     
    }
    
    
    
    public class RARequest {
        public msgHeadersRequest inputHeaders {get;set;}        
        public RequestBody body {get;set;}
    }
     public class RAResponse {
        public msgHeadersResponse inputHeaders {get;set;}        
        public ResponseBody body {get;set;}
    }
    
    public class ResponseBody{
        public string EvEmployee {get;set;}
        public string EvErrCd {get;set;}
        public string EvErrMsg {get;set;}
        public string EvExchReqno {get;set;}
        public string EvRaNo {get;set;}
        public string EvReason {get;set;}
        public string EvSoNo {get;set;}
        public string EvStatus {get;set;}
        
    }
       public class msgHeadersResponse {
        public string MSGGUID {get;set;} // GUID for message
        public string IFID {get;set;} // interface id, created automatically, so need logic
        public string IFDate {get;set;} // interface date
        public string MSGSTATUS {get;set;} // start, success, failure (ST , S, F)
        public string ERRORCODE {get;set;} // error code
        public string ERRORTEXT {get;set;} // error message
    }  
    
     public class msgHeadersRequest{
        public string MSGGUID {get{string s = giveGUID(); return s;}set;} // GUID for message
        public string IFID {get{return 'IF_SVC_11';}set;} // interface id, created automatically, so need logic
        public string IFDate {get{return datetime.now().formatGMT('YYYYMMddHHmmss');}set;} // interface date
        
        public msgHeadersRequest(){
            
        }
        
       
    } 
    
    public class RequestBody {
         public IS_ZTSVC104 IS_ZTSVC104 {get;set;}
         public IS_ORDERADM_H IS_ORDERADM_H {get;set;}
         public AscAdderss IS_SALES_ADDR {get;set;}
         public  CustmerAddress IS_CUSTOMER_ADDR {get;set;}
         public string IV_BTN {get{return '1';}set;}
        
    }
    public class IS_ORDERADM_H{
        public string OBJECT_ID {get; set;}
         public string ZZCOLLECT_POINT {get;set;}
    }
    
    public class IS_ZTSVC104{
        public string ACCOUNT {get;set;}
        public string BILLING_PT {get;set;}
        public string OBJECT_ID {get;set;}
        public string COMPANY {get;set;}
        public string EXCH_TYPE {get;set;}
        public string EXCH_REASON {get;set;}
        public string EXCH_SUB_REASON {get;set;}
        public string EXCH_GRADE {get;set;}
        public string NEW_ORI_PLANT {get{return 'V319';}set;}
        public string NEW_SHIP_PLANT {get{return 'V319';}set;}
        public string NEW_MODEL {get;set;}
        public string CUKY {get{return 'USD';}set;}
        public string SALES_ORG {get;set;}
        public string STORAGE_LOC {get;set;}
        public string S_STORAGE_LOC {get{return 'WS40';}set;}
         public string DELIVERY_TYPE {get{return 'DT005';}set;}
        public string ORDER_TYPE {get;set;}
        public string INV_LOCATION {get;set;}
        
     }
     Public class CustmerAddress{
        public string ADDR_TYPE {get{return '';}set;}
        public string REGION {get{return '';}set;}
        public string BP_NO {get;set;}
        public string FULL_NAME {get;set;}
        public string STREET {get;set;}
        public string STR_SUPPL1 {get;set;}
        public string STR_SUPPL2 {get;set;}
        public string CITY {get;set;}
        public string ZIP {get;set;}
        public string COUNTRY {get;set;}
        public string E_MAIL {get;set;}
        public string TELEPHONE_M {get;set;}
        public string TELEPHONE_H {get;set;}
        public string FIRSTNAME {get;set;}
        public string LASTNAME {get;set;}
        public string NAME2 {get{return '';}set;}
        public string ADDR_GUID {get{return '';}set;}
        public string ADDR_SHORT {get{return '';}set;}
        public string DISTRICT {get{return '';}set;}
        public string FAX_NUMBER {get{return '';}set;}
        public string FULL_STREET {get; set;}
         
     }
     Public class AscAdderss{
        public string ADDR_TYPE {get{return '';}set;}
        public string REGION  {get{return '';}set;}
        public string BP_NO  {get; set;}
        public string FULL_NAME {get;set;}
        public string STREET  {get{return '';}set;}
        public string STR_SUPPL1  {get{return '';}set;}
        public string STR_SUPPL2  {get{return '';}set;}
        public string CITY  {get{return '';}set;}
        public string ZIP  {get{return '';}set;}
        public string COUNTRY  {get{return '';}set;}
        public string E_MAIL  {get{return '';}set;}
        public string TELEPHONE_M  {get{return '';}set;}
        public string TELEPHONE_H  {get{return '';}set;}
        public string FIRSTNAME  {get{return '';}set;}
        public string LASTNAME  {get{return '';}set;}
        public string NAME2 {get{return '';}set;}
        public string ADDR_GUID {get{return '';}set;}
        public string ADDR_SHORT {get{return '';}set;}
        public string DISTRICT {get{return '';}set;}
        public string FAX_NUMBER {get{return '';}set;}
        public string FULL_STREET  {get{return '';}set;}
         
     }
     
    
    public static string giveGUID(){
        Blob b = Crypto.GenerateAESKey(128);
        String h = EncodingUtil.ConvertTohex(b);
        String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
        return guid;
    }
    
     public static string handleSuccess(RAResponse res,case c,String req){
         String reqNo=res.body.EvExchReqno;
         String retunval='';
          if(reqNo !='' && reqNo !=null){
             
             
             message_queue__c mq=new message_queue__c();
             mq.Object_Name__c='Case';
             mq.Integration_Flow_Type__c='SVC-11';
             mq.Status__c='sucess';
             mq.MSGUID__c=res.inputHeaders.MSGGUID;
             mq.Response_Body__c=String.valueOf(res);
             mq.Request_Body__c=req;
              mq.Identification_Text__c=c.id;
             insert mq;
             
             
             
             retunval='Successfully Submitted the RA Request. Here is Reference Number:'+reqNo;
         }else{
            retunval='Something went wrong. Please contact System Admin'; 
              retunval=retunval+'  '+ handleError(res,c,req);
         }
         
         
         return retunval;
     }
     
       public static string handleError(RAResponse res,case c,String req){
            message_queue__c mq=new message_queue__c();
             mq.Object_Name__c='Case';
             mq.Integration_Flow_Type__c='SVC-11';
             mq.Status__c='failed';
             mq.MSGUID__c=res.inputHeaders.MSGGUID;
             mq.Response_Body__c=String.valueOf(res);
             mq.Request_Body__c=req;
              mq.Identification_Text__c=c.id;
             insert mq;
             string err='Error :'+res.inputHeaders.ERRORTEXT;
            return err;
          
       }
       
       
       
      public Static HTTPResponse Testrespond(HTTPRequest req) {          
          String outputBodyString = ' { "inputHeaders": {    "MSGGUID": "780d4162-28bb-1fb8-fadc-bea064c0133b",    "IFID": "IF_SVC_11",    "IFDate": "20181211080441",    "MSGSTATUS": "S",    "ERRORTEXT": null  },  "body": {  "EvEmployee": null,    "EvErrCd": null,    "EvErrMsg": null,    "EvExchReqno": "0000059205",    "EvRaNo": null,    "EvReason": "RM006",    "EvSoNo": null,    "EvStatus": "ES010"  }}';    
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatus('2');
            res.setStatusCode(200);
            res.setBody(outputBodyString);
            
            return res;                
      } 

}