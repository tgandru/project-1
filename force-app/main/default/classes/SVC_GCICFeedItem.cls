/**
 * Created by ms on 2017-08-07.
 *
 * author : JeongHo.Lee, I2MAX
 */
public without sharing class SVC_GCICFeedItem {

	public static void insertFeedItem(Set<Id> ids, String obj){
		system.debug('***InsertFeedItem');
		Set<Id> batchids = new Set<Id>(); 
		Set<Id> futureids = new Set<Id>(); 
		List<Data> dataList = new List<Data>();

		Set<Id> srchItems = ids;

		Set<Id> ParentIds = new Set<Id>();
		if(obj == 'FeedItem'){
			for(FeedItem fi : [SELECT Id, ParentId, Parent.Type, Body ,CreatedDate, CreatedBy.Profile.Name, CreatedBy.Name FROM FeedItem WHERE Id in: srchItems AND Parent.Type  =: 'Case'
								AND Type =: 'TextPost']){
				 
				if(fi.CreatedBy.Profile.Name != 'Integration User'){
					ParentIds.add(fi.ParentId);	
					Data dt = new Data();
					dt.feedItem = fi;
					dt.caseId = fi.ParentId;
					dataList.add(dt);
				}
			}
		}
		Set<Id> taCaseIds = new Set<Id>(); //whatid = caseId
		if(obj == 'Task'){
			for(Task ta : [SELECT Id, WhatId, Description, CreatedDate, CreatedBy.Profile.Name, CreatedBy.Name FROM Task WHERE Id in: srchItems AND What.Type = 'Case']){
				
				if(ta.CreatedBy.Profile.Name != 'Integration User'){
					taCaseIds.add(ta.WhatId);	
					Data dt = new Data();
					dt.task = ta;
					dt.caseId = ta.WhatId;
					dataList.add(dt);
				}
			}
		}
		Set<Id> evtCaseIds = new Set<Id>(); //whatid = caseId
		Id caseParentId = null;
		if(obj == 'Event'){
			for(Event evt : [SELECT Id, WhatId, Description, CreatedDate, CreatedBy.Profile.Name, CreatedBy.Name FROM Event WHERE Id in: srchItems AND What.Type = 'Case']){
				
				if(evt.CreatedBy.Profile.Name != 'Integration User'){
					evtCaseIds.add(evt.WhatId);	
					Data dt = new Data();
					dt.event = evt;
					dt.caseId = evt.WhatId;
					dataList.add(dt);
				}
			}
		}

		system.debug('***dataList' + dataList);
		String objType ='';
		//Parent
		for(Case p : [SELECT Id, CaseNumber,Service_Order_Number__c, Status, GCIC_Status_Code__c FROM Case 
								WHERE (Id in: ParentIds OR Id in: taCaseIds OR Id in: evtCaseIds)
								AND RecordType.DeveloperName in('Exchange','Repair','Repair_To_Exchange')
								AND Status NOT in('Cancelled', 'Closed')
								AND Service_Order_Number__c = NULL
								AND Parent_Case__c = true]){
			for(Data dt : dataList){
				if(dt.caseId == p.Id){
					if(dt.feedItem != null) batchids.add(dt.feedItem.id);
					if(dt.task != null) batchids.add(dt.task.id);
					if(dt.event != null) batchids.add(dt.event.id);
					caseParentId = p.id;
				} 
			}
		}
		//Child
		for(Case c: [SELECT Id, CaseNumber,Service_Order_Number__c, Status, GCIC_Status_Code__c FROM Case 
								WHERE (Id in: ParentIds OR Id in: taCaseIds OR Id in: evtCaseIds)
								AND RecordType.DeveloperName in('Exchange','Repair','Repair_To_Exchange')
								AND Status NOT in('Cancelled', 'Closed')
								AND Service_Order_Number__c != NULL
								AND Parent_Case__c = false]){
			for(Data dt : dataList){
				if(dt.caseId == c.Id){
					if(dt.feedItem != null){
						futureids.add(dt.feedItem.id);
						objType = 'feedItem';
					}
					if(dt.task != null){
						futureids.add(dt.task.id);
						objType = 'task';
					} 
					if(dt.event != null){
						futureids.add(dt.event.id);
						objType = 'event';
					} 
				} 
			}
		}
		system.debug('batchids : ' + batchids);
		system.debug('futureids : ' + futureids);
		
		if(!batchids.isEmpty()){
			List<Id> childSet = new List<Id>();
			for(case ca : [SELECT Id FROM Case 
							WHERE ParentId =: caseParentId
							AND RecordType.DeveloperName in('Exchange','Repair','Repair_To_Exchange')
							AND Status NOT in('Cancelled', 'Closed')
							AND Service_Order_Number__c != NULL
							AND Parent_Case__c = false]){
				childSet.add(ca.Id);
			}
			system.debug('childSet' + childSet);
			if(!childSet.isEmpty() && !Test.isRunningTest()){
				SVC_GCICFeedItemBatch ba;
				if(dataList[0].feedItem != null) ba = new SVC_GCICFeedItemBatch(childSet, dataList[0].feedItem, NULL, NULL);
				else if(dataList[0].task != null) ba = new SVC_GCICFeedItemBatch(childSet, NULL, dataList[0].task, NULL);
				else if(dataList[0].event != null) ba = new SVC_GCICFeedItemBatch(childSet, NULL, NULL, dataList[0].event);
	        	Database.executeBatch(ba, 50);
	        }
		}
		if(!futureids.isEmpty() && !Test.isRunningTest()){
			SVC_GCICFeedItem.futureFeedItem(futureids , objType);
		}
	}
	@future(callout = true)
	public static void futureFeedItem(Set<Id> ids, String obj){
		system.debug('*** future method : ' + ids + 'obj : ' + obj);
		List<Data> dataList = new List<Data>();
		Set<Id> srchItems = ids;

		Set<Id> ParentIds = new Set<Id>();
		if(obj == 'FeedItem'){
			for(FeedItem fi : [SELECT Id, ParentId, Parent.Type, Body ,CreatedDate, CreatedBy.Profile.Name,CreatedBy.Name  FROM FeedItem WHERE Id in: srchItems AND Parent.Type  =: 'Case'
								AND Type =: 'TextPost']){
				 
				if(fi.CreatedBy.Profile.Name != 'Integration User'){
					ParentIds.add(fi.ParentId);	
					Data dt = new Data();
					dt.feedItem = fi;
					dt.caseId = fi.ParentId;
					dataList.add(dt);
				}
			}
		}
		Set<Id> taCaseIds = new Set<Id>(); //whatid = caseId
		if(obj == 'Task'){
			for(Task ta : [SELECT Id, WhatId, Description, CreatedDate, CreatedBy.Profile.Name ,CreatedBy.Name FROM Task WHERE Id in: srchItems AND What.Type = 'Case']){
				
				if(ta.CreatedBy.Profile.Name != 'Integration User'){
					taCaseIds.add(ta.WhatId);	
					Data dt = new Data();
					dt.task = ta;
					dt.caseId = ta.WhatId;
					dataList.add(dt);
				}
			}
		}
		Set<Id> evtCaseIds = new Set<Id>(); //whatid = caseId
		Id caseParentId = null;
		if(obj == 'Event'){
			for(Event evt : [SELECT Id, WhatId, Description, CreatedDate, CreatedBy.Profile.Name ,CreatedBy.Name FROM Event WHERE Id in: srchItems AND What.Type = 'Case']){
				
				if(evt.CreatedBy.Profile.Name != 'Integration User'){
					evtCaseIds.add(evt.WhatId);	
					Data dt = new Data();
					dt.event = evt;
					dt.caseId = evt.WhatId;
					dataList.add(dt);
				}
			}
		}

		List<Case> caseList = [SELECT Id, CaseNumber,Service_Order_Number__c, Status, GCIC_Status_Code__c,RecordType.DeveloperName FROM Case 
								WHERE (Id in: ParentIds OR Id in: taCaseIds OR Id in: evtCaseIds)
								AND RecordType.DeveloperName in('Exchange','Repair','Repair_To_Exchange')
								AND Status NOT in('Cancelled', 'Closed')
								AND Service_Order_Number__c != NULL
								AND Parent_Case__c = false];

		for(Case ca : caseList){
			for(Data dt : dataList){
				if(dt.caseId == ca.Id) dt.ca = ca;
			}
		}
		system.debug('*** future method : ' + '***dataList' + dataList);

		String layoutId = Integration_EndPoints__c.getInstance('SVC-07').layout_id__c;
	    String partialEndpoint = Integration_EndPoints__c.getInstance('SVC-07').partial_endpoint__c;

	    FeedItemRequest feedReq = new FeedItemRequest();
	    SVC_GCICJSONRequestClass.HWTicketStatusJSON body = new SVC_GCICJSONRequestClass.HWTicketStatusJSON();
	    cihStub.msgHeadersRequest headers = new cihStub.msgHeadersRequest(layoutId);
	    body.CommentList = new List<SVC_GCICJSONRequestClass.Comment>();

		if(!dataList.isEmpty() && dataList[0].ca != null){
		    for(Integer i = 0; i < dataList.size(); i++){
		    	body.Service_Ord_no = dataList[i].ca.Service_Order_Number__c;
		    	body.Case_NO		= dataList[i].ca.CaseNumber;
		    	body.Ticket_status	= ' ';
		    	if(dataList[i].ca.Status == 'Cancelled' && dataList[i].ca.RecordType.DeveloperName == 'Repair') body.Ticket_status	= 'ST051';
		    	if(dataList[i].ca.Status == 'Cancelled' && dataList[i].ca.RecordType.DeveloperName == 'Exchange') body.Ticket_status	= 'ES080';
		    	//body.Ticket_status	= 'ST010';
		    	SVC_GCICJSONRequestClass.Comment ct = new SVC_GCICJSONRequestClass.Comment();
		    	if(obj == 'feedItem'){
		    		ct.Comment = dataList[i].feedItem.Body;
		    		ct.Comment_Created_by = dataList[i].feedItem.CreatedBy.Name;
		    		//ct.Comment_Created_by_email = dataList[i].com.InsertedBy.Email;
		    		ct.Comment_Created_Date = String.valueOf(dataList[i].feedItem.CreatedDate);
		    	}
		    	if(obj == 'Task'){
		    		ct.Comment = dataList[i].task.Description;
		    		ct.Comment_Created_by = dataList[i].task.CreatedBy.Name;
		    		//ct.Comment_Created_by_email = dataList[i].com.InsertedBy.Email;
		    		ct.Comment_Created_Date = String.valueOf(dataList[i].task.CreatedDate);
		    	}
		    	if(obj == 'Event'){
		    		ct.Comment = dataList[i].event.Description;
		    		ct.Comment_Created_by = dataList[i].event.CreatedBy.Name;
		    		//ct.Comment_Created_by_email = dataList[i].com.InsertedBy.Email;
		    		ct.Comment_Created_Date = String.valueOf(dataList[i].event.CreatedDate);
		    	}
		    	body.CommentList.add(ct);
		    }
		    feedReq.body = body;
        	feedReq.inputHeaders = headers;
        	string requestBody = json.serialize(feedReq);
        	system.debug('requestbody' + requestBody);

        	System.Httprequest req = new System.Httprequest();
	        HttpResponse res = new HttpResponse();
	        req.setMethod('POST');
	        req.setBody(requestBody);
	      
	        req.setEndpoint('callout:X012_013_ROI'+partialEndpoint);
	        
	        req.setTimeout(120000);
	            
	        req.setHeader('Content-Type', 'application/json');
	        req.setHeader('Accept', 'application/json');
	    
	        Http http = new Http();
	        Datetime requestTime = system.now();   
	        res = http.send(req);
	        Datetime responseTime = system.now();
	        //system.debug('***datalistsize : ' + dataList.size());
        	if(res.getStatusCode() == 200){
	        	//success
	        	system.debug('res.getBody() : ' + res.getBody());
	        	Response response = (SVC_GCICFeedItem.Response)JSON.deserialize(res.getBody(), SVC_GCICFeedItem.Response.class);
	        	
	        	system.debug('response : ' + response);
	        
	        	Message_Queue__c mq = setMessageQueue(dataList[0].ca.Id, response, requestTime, responseTime);
	        	//mqList.add(mq);
	        	//insert mqList;
	        	insert mq;

	        	SVC_GCICFeedItem.ResponseBody resbody = response.body;
	        	system.debug('resbody : ' + resbody);

	        }
	        else{
	        	//Fail
	            MessageLog messageLog = new MessageLog();
	            messageLog.body = 'ERROR : http status code = ' +  res.getStatusCode();
	            messageLog.MSGGUID = feedReq.inputHeaders.MSGGUID;
	            messageLog.IFID = feedReq.inputHeaders.IFID;
	            messageLog.IFDate = feedReq.inputHeaders.IFDate;

	            Message_Queue__c mq = setHttpQueue(dataList[0].ca.Id, messageLog, requestTime, responseTime);
	            //mqList.add(mq);
	            //insert mqList;
	            insert mq;     	
	        }
	    }
	    else{
	    	system.debug('There is no satisfying condition.');
	    }
	}
	public class Data {
		public FeedItem feedItem;
		public Task	task;
		public Event event;
		public Case ca;
		public String caseId;
	}
    public class FeedItemRequest{
        public cihStub.msgHeadersRequest inputHeaders;        
        public SVC_GCICJSONRequestClass.HWTicketStatusJSON body;    
    }
    public class Response{
        cihStub.msgHeadersResponse inputHeaders;
        ResponseBody body;
    }
    public class ResponseBody{
        public String RET_CODE;
        public String RET_Message;
    }
    public static Message_Queue__c setMessageQueue(Id caseId, Response response, Datetime requestTime, Datetime responseTime) {
        cihStub.msgHeadersResponse inputHeaders = response.inputHeaders;
        SVC_GCICFeedItem.ResponseBody body = response.body;

        Message_Queue__c mq = new Message_Queue__c();

        mq.ERRORCODE__c = 'CIH :' + SVC_UtilityClass.nvl(inputHeaders.ERRORCODE) + ', GCIC : ' + SVC_UtilityClass.nvl(body.RET_CODE);
        mq.ERRORTEXT__c = 'CIH :' + SVC_UtilityClass.nvl(inputHeaders.ERRORTEXT) + ', GCIC : ' + SVC_UtilityClass.nvl(body.RET_Message); 
        mq.IFDate__c = inputHeaders.IFDate;
        mq.IFID__c = inputHeaders.IFID;
        mq.Identification_Text__c = caseId;
        mq.Initalized_Request_Time__c = requestTime;//Datetime
        mq.Integration_Flow_Type__c = 'SVC-07'; // 
        mq.Is_Created__c = true;
        mq.MSGGUID__c = inputHeaders.MSGGUID;
        mq.MSGSTATUS__c = inputHeaders.MSGSTATUS;
        mq.MSGUID__c = inputHeaders.MSGGUID;
        mq.Object_Name__c = 'Case';
        mq.Request_To_CIH_Time__c = requestTime.getTime();//Number(15, 0)
        mq.Response_Processing_Time__c = responseTime.getTime();//Number(15, 0)
        mq.Status__c = (body.RET_CODE == '0') ? 'success' : 'failed';

        return mq;
    }
    //http error save.
    public static Message_Queue__c setHttpQueue(Id caseId, MessageLog messageLog, Datetime requestTime, Datetime responseTime) {
        Message_Queue__c mq 			= new Message_Queue__c();
        mq.ERRORCODE__c 				= messageLog.body;
        mq.ERRORTEXT__c 				= messageLog.body;
        mq.IFDate__c 					= messageLog.IFDate;
        mq.IFID__c 						= messageLog.IFID;
        mq.Identification_Text__c 		= caseId;
        mq.Initalized_Request_Time__c 	= requestTime;//Datetime
        mq.Integration_Flow_Type__c 	= 'SVC-07';
        mq.Is_Created__c 				= true;
        mq.MSGGUID__c 					= messageLog.MSGGUID;
        mq.MSGUID__c					= messageLog.MSGGUID;
		//mq.MSGSTATUS__c = messageLog.MSGSTATUS;
        mq.Object_Name__c 				= 'Case';
        mq.Request_To_CIH_Time__c 		= requestTime.getTime();//Number(15, 0)
        mq.Response_Processing_Time__c 	= responseTime.getTime();//Number(15, 0)
        mq.Status__c 					= 'failed'; // picklist

        return mq;
    }
    public class MessageLog {
        public Integer status;
        public String body;
        public String MSGGUID;
        public String IFID;
        public String IFDate;
        public String MSGSTATUS;
        public String ERRORCODE;
        public String ERRORTEXT;
    }
}