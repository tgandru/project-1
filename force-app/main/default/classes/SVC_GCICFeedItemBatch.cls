/**
 * Created by ms on 2017-08-07.
 *
 * author : JeongHo.Lee, I2MAX
 */
global without sharing class SVC_GCICFeedItemBatch implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts {
    
    public List<Id> caseIds;
    public FeedItem fiba;
    public Task 	taba;
    public Event 	evtba;
    public List<Message_Queue__c> mqList;

    public SVC_GCICFeedItemBatch(List<Id> ids, FeedItem fi, Task ta, Event evt){
    	caseIds = ids;
    	fiba = fi;
    	taba = ta;
    	evtba = evt;
        
		system.debug('batch start');
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {

        String query = '';
        query += ' SELECT  Id, RecordType.Name, CaseNumber, Parent_Case__c, ParentId, Service_Order_Number__c, Status ';
        query += ' FROM Case WHERE Id in: caseIds ';
        query += ' AND (RecordType.DeveloperName = \'Repair\' OR RecordType.DeveloperName = \'Repair_To_Exchange\' OR RecordType.DeveloperName = \'Exchange\')';
        query += ' AND Service_Order_Number__c != NULL ';
        query += ' AND Parent_Case__c = FALSE ';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Case> scope) {

        mqList = new List<Message_Queue__c>();
        for(Case ca : scope){

            String layoutId = Integration_EndPoints__c.getInstance('SVC-07').layout_id__c;
            String partialEndpoint = Integration_EndPoints__c.getInstance('SVC-07').partial_endpoint__c;

            FeedItemRequest feedReq = new FeedItemRequest();
            SVC_GCICJSONRequestClass.HWTicketStatusJSON body = new SVC_GCICJSONRequestClass.HWTicketStatusJSON();
            cihStub.msgHeadersRequest headers = new cihStub.msgHeadersRequest(layoutId);
            body.CommentList = new List<SVC_GCICJSONRequestClass.Comment>();

        	body.Service_Ord_no = ca.Service_Order_Number__c;
	    	body.Case_NO		= ca.CaseNumber;
	    	body.Ticket_status	= ' ';
            if(ca.Status == 'Cancelled' && ca.RecordType.DeveloperName == 'Repair') body.Ticket_status  = 'ST051';
            if(ca.Status == 'Cancelled' && ca.RecordType.DeveloperName == 'Exchange') body.Ticket_status  = 'ES080';
	    	//body.Ticket_status	= 'ST010';
	    	SVC_GCICJSONRequestClass.Comment ct = new SVC_GCICJSONRequestClass.Comment();
	    	if(fiba != null){
	    		ct.Comment = fiba.Body;
	    		ct.Comment_Created_by = fiba.CreatedBy.Name;
	    		ct.Comment_Created_Date = String.valueOf(fiba.CreatedDate);
	    	}
	    	else if(taba != null){
	    		ct.Comment = taba.Description;
	    		ct.Comment_Created_by = taba.CreatedBy.Name;
	    		ct.Comment_Created_Date = String.valueOf(taba.CreatedDate);

	    	}
	    	else if(evtba != null){
	    		ct.Comment = evtba.Description;
	    		ct.Comment_Created_by = evtba.CreatedBy.Name;
	    		ct.Comment_Created_Date = String.valueOf(evtba.CreatedDate);
	    	}
	    	body.CommentList.add(ct);
        
	    	feedReq.body = body;
	    	feedReq.inputHeaders = headers;
	    	string requestBody = json.serialize(feedReq);
	    	system.debug('requestbody' + requestBody);

	    	System.Httprequest req = new System.Httprequest();
	        HttpResponse res = new HttpResponse();
	        req.setMethod('POST');
	        req.setBody(requestBody);
	      
	        req.setEndpoint('callout:X012_013_ROI'+partialEndpoint);
	        
	        req.setTimeout(120000);
	            
	        req.setHeader('Content-Type', 'application/json');
	        req.setHeader('Accept', 'application/json');
	    
	        Http http = new Http();
	        Datetime requestTime = system.now();   
	        res = http.send(req);
	        Datetime responseTime = system.now();
	        //system.debug('***datalistsize : ' + dataList.size());
	    	if(res.getStatusCode() == 200){
	        	//success
	        	system.debug('res.getBody() : ' + res.getBody());
	        	Response response = (SVC_GCICFeedItemBatch.Response)JSON.deserialize(res.getBody(), SVC_GCICFeedItemBatch.Response.class);
	        	
	        	system.debug('response : ' + response);
	        
	        	Message_Queue__c mq = setMessageQueue(ca.Id, response, requestTime, responseTime);
	        	mqList.add(mq);

	        	SVC_GCICFeedItemBatch.ResponseBody resbody = response.body;
	        	system.debug('resbody : ' + resbody);

	        }
	        else{
	        	//Fail
	            MessageLog messageLog = new MessageLog();
	            messageLog.body = 'ERROR : http status code = ' +  res.getStatusCode();
	            messageLog.MSGGUID = feedReq.inputHeaders.MSGGUID;
	            messageLog.IFID = feedReq.inputHeaders.IFID;
	            messageLog.IFDate = feedReq.inputHeaders.IFDate;

	            Message_Queue__c mq = setHttpQueue(ca.Id, messageLog, requestTime, responseTime);
	            mqList.add(mq);
	        }
	    }
        if(!mqList.isEmpty()){
            insert mqList;
        }
    }

    global void finish(Database.BatchableContext BC) {
    }
    public class FeedItemRequest{
        public cihStub.msgHeadersRequest inputHeaders;        
        public SVC_GCICJSONRequestClass.HWTicketStatusJSON body;    
    }
    public class Response{
        cihStub.msgHeadersResponse inputHeaders;
        ResponseBody body;
    }
    public class ResponseBody{
        public String RET_CODE;
        public String RET_Message;
    }
    public static Message_Queue__c setMessageQueue(Id caseId, Response response, Datetime requestTime, Datetime responseTime) {
        cihStub.msgHeadersResponse inputHeaders = response.inputHeaders;
        SVC_GCICFeedItemBatch.ResponseBody body = response.body;

        Message_Queue__c mq = new Message_Queue__c();

        mq.ERRORCODE__c = 'CIH :' + SVC_UtilityClass.nvl(inputHeaders.ERRORCODE) + ', GCIC : ' + SVC_UtilityClass.nvl(body.RET_CODE);
        mq.ERRORTEXT__c = 'CIH :' + SVC_UtilityClass.nvl(inputHeaders.ERRORTEXT) + ', GCIC : ' + SVC_UtilityClass.nvl(body.RET_Message); 
        mq.IFDate__c = inputHeaders.IFDate;
        mq.IFID__c = inputHeaders.IFID;
        mq.Identification_Text__c = caseId;
        mq.Initalized_Request_Time__c = requestTime;//Datetime
        mq.Integration_Flow_Type__c = 'SVC-07'; // 
        mq.Is_Created__c = true;
        mq.MSGGUID__c = inputHeaders.MSGGUID;
        mq.MSGSTATUS__c = inputHeaders.MSGSTATUS;
        mq.MSGUID__c = inputHeaders.MSGGUID;
        mq.Object_Name__c = 'Case';
        mq.Request_To_CIH_Time__c = requestTime.getTime();//Number(15, 0)
        mq.Response_Processing_Time__c = responseTime.getTime();//Number(15, 0)
        mq.Status__c = (body.RET_CODE == '0') ? 'success' : 'failed';

        return mq;
    }
    //http error save.
    public static Message_Queue__c setHttpQueue(Id caseId, MessageLog messageLog, Datetime requestTime, Datetime responseTime) {
        Message_Queue__c mq 			= new Message_Queue__c();
        mq.ERRORCODE__c 				= messageLog.body;
        mq.ERRORTEXT__c 				= messageLog.body;
        mq.IFDate__c 					= messageLog.IFDate;
        mq.IFID__c 						= messageLog.IFID;
        mq.Identification_Text__c 		= caseId;
        mq.Initalized_Request_Time__c 	= requestTime;//Datetime
        mq.Integration_Flow_Type__c 	= 'SVC-07';
        mq.Is_Created__c 				= true;
        mq.MSGGUID__c 					= messageLog.MSGGUID;
        mq.MSGUID__c                    = messageLog.MSGGUID;
		//mq.MSGSTATUS__c = messageLog.MSGSTATUS;
        mq.Object_Name__c 				= 'Case';
        mq.Request_To_CIH_Time__c 		= requestTime.getTime();//Number(15, 0)
        mq.Response_Processing_Time__c 	= responseTime.getTime();//Number(15, 0)
        mq.Status__c 					= 'failed'; // picklist

        return mq;
    }
    public class MessageLog {
        public Integer status;
        public String body;
        public String MSGGUID;
        public String IFID;
        public String IFDate;
        public String MSGSTATUS;
        public String ERRORCODE;
        public String ERRORTEXT;
    }
}