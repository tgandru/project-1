public class SVC_GCICIntegrationHelper {
	
	public static message_queue__c createMessageQueue()
	{
		message_queue__c mq = new message_queue__c();
		insert mq;
		mq.MSGGUID__c = mq.Id;
		mq.IFID__c = mq.Id;

		return mq;
	}
	/* BP creation 
	public class BPRequest{
		public cihStub.msgHeadersRequest inputHeaders;        
        public BPCreationReqBody body;    
    }
    public class BPCreationReqBody{
    	public string accountName;
    	public string contact_Fname;
    	public string contact_Lname;
    	//...
    	//...
    }

    public class BPCreationResponse{
    	Public String Ret_code;
    	Public String RET_Message;
		Public String Contact_BP_no;
		Public String Account_BP_no;
     }
	 
	 /* Warranty check 
	 public class WtyCheckRequest{
		public cihStub.msgHeadersRequest inputHeaders;        
        public WtyCreationReqBody body;    
    }
    public class WtyCheckReqBody{
    	public string I_IMEI;
    	public string I_Purchase_Date;
    	public string I_Posting_Date;
    	//...
    	//...
    }

    public class WtyCheckResponse{
    	Public String IMEI;
    	Public String ModelCode;
		Public String GIDate;
		Public String Act_Date;
		//Public Wty_End_Date;
		//...
		//...
     }
	 
	 
	@future(callout=true)
    public static void WtyCheck(ID CaseID){
    	WtyCheck(CaseID);
    }    
    
	/*public static void WtyCheck(ID CaseID){
		//Case caseToCheck = [select Device__r.imei__c, etc from case where id=:CaseID];
		
		if (caseToCheck.device__r.imei__c == null){
			
			Message_queue__c mq = new message_queue__c(TBD here);
       
			string partialEndpoint = Integration_EndPoints__c.getInstance('IF_Wty_Check_SVC_GCIC').partial_endpoint__c;
	        string layoutId = 'IF_Wty_Check_SVC_GCIC';

	        WtyCheckRequest WtyCheckReq = new WtyCheckRequest();

	        WtyCheckReqBody body = new WtyCheckReqBody();

	        cihStub.msgHeadersRequest headers = new cihStub.msgHeadersRequest(layoutId);

	        boolean hasErrors = setWtyCheckFields(body,caseToCheck);

		    if(hasErrors){
		    	String err = 'TBD';
	            handleError(mq, err);
	            return;
	        }
	        WtyCheckReq.body = body;
	        WtyCheckReq.inputHeaders = headers;
	        string requestBody = json.serialize(WtyCheckReq);
	        string response = '';
	        string errorMessage = '';
	        
	        try{
	            response = httpCallout(requestBody, partialEndpoint); 
	            if(response error) {
	                errorMessage = 'TBD';
	                handleError(mq, errorMessage);
	            }
	        } catch(exception e){
	        	errorMessage = 'TBD';
	            handleError(mq, errorMessage);
	        }
	    
	        if(string.isNotBlank(response)){ 
	            WtyCheckResponse res = (WtyCheckResponse) json.deserialize(response, WtyCheckResponse.class);
	            //process response...update warranty info 
	            //Update message queue mq
				
				mq.status__c = 'success';
				mq.MSGSTATUS__c = res.inputHeaders.MSGSTATUS;
				mq.MSGUID__c = res.inputHeaders.msgGUID;
				mq.IFID__c = res.inputHeaders.ifID;
				mq.IFDate__c = res.inputHeaders.ifDate;      
				mq.ERRORCODE__c=null;
				mq.ERRORTEXT__c=null;    
				upsert mq;	
	            
	        } else {
	            handleError(mq);
	        }


		}

	}
	 
    @future(callout=true)
    public static void BPCreation(ID CaseID){
    	BPCreation(CaseID);
    }    
    
	/*public static void BPCreation(ID CaseID){
		Case c = [select accountid, contactid from case where id=:CaseID];
		Account acct = [select BP_number__c,name,fax,phone,billingstreet,billingCity, etc from Account where id =: c.accountID ];
		Contact cont  = [select BP_number__C,firstname, lastname, mailingStreet, mailingCity, etc from Contact where id=:c.ContactID];
		if (acct.BP_number__c == null || cont.BP_number__c == null){
			
			Message_queue__c mq = new message_queue__c(TBD here);
       
			string partialEndpoint = Integration_EndPoints__c.getInstance('IF_BP_Creation_SVC_GCIC').partial_endpoint__c;
	        string layoutId = 'IF_BP_Creation_SVC_GCIC';

	        BPRequest bpReq = new BPRequest();

	        BPCreationReqBody body = new BPCreationReqBody();

	        cihStub.msgHeadersRequest headers = new cihStub.msgHeadersRequest(layoutId);

	        boolean hasErrors = setBPCreationFields(body,acct,cont);

		    if(hasErrors){
		    	String err = 'TBD';
	            handleError(mq, err);
	            return;
	        }
	        bpReq.body = body;
	        bpReq.inputHeaders = headers;
	        string requestBody = json.serialize(bpReq);
	        string response = '';
	        string errorMessage = '';
	        
	        try{
	            response = httpCallout(requestBody, partialEndpoint); 
	            if(response error) {
	                errorMessage = 'TBD';
	                handleError(mq, errorMessage);
	            }
	        } catch(exception e){
	        	errorMessage = 'TBD';
	            handleError(mq, errorMessage);
	        }
	    
	        if(string.isNotBlank(response)){ 
	            BPCreationResponse res = (BPCreationResponse) json.deserialize(response, BPCreationResponse.class);
	            //process response...update account / contact BP#
	            //Update message queue mq
				cont.BP_number__C = res.contact_bp_no;
				update cont;
				acct.BP_number__C = res.account_bp_no;
				update acct;
				
				mq.status__c = 'success';
				mq.MSGSTATUS__c = res.inputHeaders.MSGSTATUS;
				mq.MSGUID__c = res.inputHeaders.msgGUID;
				mq.IFID__c = res.inputHeaders.ifID;
				mq.IFDate__c = res.inputHeaders.ifDate;      
				mq.ERRORCODE__c=null;
				mq.ERRORTEXT__c=null;    
				upsert mq;	
	            
	        } else {
	            handleError(mq);
	        }


		}

	}*/

	/*public static boolean setBPCreationFields(BPCreationReqBody body, Account acct, Contact cont){
		if (missing fields or error){
			return true;
		}
		body.accountName = acct.name;
		body.contact_Fname = cont.firstName;
		body.contact_Lname = cont.lastName;
		//...
		//...
	}

	 public static string httpCallout(string body, string partialEndpoint){ 

        System.Httprequest req = new System.Httprequest();
        HttpResponse res = new HttpResponse();
        req.setMethod('POST');
        req.setBody(body);
      
        req.setEndpoint('callout:SVC_GCIC_Integration'+partialEndpoint);
        
        req.setTimeout(120000);
            
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Accept', 'application/json');
    
        Http http = new Http();  
        res = http.send(req);
        return res.getBody();       
    }*/
	

}