/****************************************
         FOR ALL GCIC REQUEST 
*****************************************/

public class SVC_GCICJSONRequestClass {
    
    public class BPCreationJSON
    {
        //public String MSGGUID;
        //public String IFID;
        //public String IFDate;
        private final String COUNTRY = 'US';
        private final String COMPANY = 'C370';
        public String IIDUSER = Userinfo.getUserId();
        public String REQUEST_TYPE;
        public String CONTACT_FNAME;
        public String CONTACT_LNAME;
        public String CONTACT_FAX;
        public String CONTACT_CELLPHONE;
        public String CONTACT_TELEPHONE;
        public String CONTACT_EMAIL;
        public String CONTACT_ADDRESS;
        public String CONTACT_ADDRESS1;
        public String CONTACT_ADDRESS2;
        public String CONTACT_CITY;
        public String CONTACT_ZIPCODE;
        public String CONTACT_STATE;
        public String CONTACT_COMM_TYPE;
        private final String SOURCE = 'SEA';   
        public String CONTACT_BP_ROLE;
        public String ACCOUNT_BP_NO;
        public String ACCOUNT_Name1;
        public String ACCOUNT_Name2;
        public String ACCOUNT_FAX;
        public String ACCOUNT_TELEPHONE;
        public String ACCOUNT_ADDRESS;
        public String ACCOUNT_ADDRESS1;
        public String ACCOUNT_ADDRESS2;
        public String ACCOUNT_CITY;
        public String ACCOUNT_ZIPCODE;
        public String ACCOUNT_STATE;
        public String ACCOUNT_BP_ROLE;
    }

    public class BPUpdateJSON
    {
        //public String MSGGUID;
        //public String IFID;
        //public String IFDate;
        private final String COUNTRY = 'US';
        private final String COMPANY = 'C370';
        public String IIDUSER = Userinfo.getUserId();
        public String CONTACT_BP_NO;
        public String CONTACT_FNAME;
        public String CONTACT_LNAME;
        public String CONTACT_FAX;
        public String CONTACT_CELLPHONE;
        public String CONTACT_TELEPHONE;
        public String CONTACT_EMAIL;
        public String CONTACT_ADDRESS;
        public String CONTACT_ADDRESS1;
        public String CONTACT_ADDRESS2;
        public String CONTACT_CITY;
        public String CONTACT_ZIPCODE;
        public String CONTACT_STATE;
        public String ACCOUNT_BP_NO;
        public String ACCOUNT_Name2;
        public String ACCOUNT_Name1;
        public String ACCOUNT_FAX;
        public String ACCOUNT_TELEPHONE;
        public String ACCOUNT_ADDRESS;
        public String ACCOUNT_ADDRESS1;
        public String ACCOUNT_ADDRESS2;
        public String ACCOUNT_CITY;
        public String ACCOUNT_ZIPCODE;
        public String ACCOUNT_STATE;
        public String REQUEST_TYPE;
    }

    public class HWTicketCreationJSON
    {
//        public String MSGGUID;
//        public String IFID;
//        public String IFDate;
        public String COMP;
        public String TICKET_FROM;
        public String Case_NO;
        public String TICKET_TYPE;
        public String ASC_ACCNO;
        public String ASC_CODE;
        public String CONTACT_BP;
        public String MODEL;
        public String SERIAL_NO;
        public String IMEI;
        public String SYMPTOM_TYPE;
        public String SYMPTOM_CAT1;
        public String SYMPTOM_CAT2;
        public String SYMPTOM_CAT3;
        public String SERVICE_TYPE;
        public String STATUS;
        public String PURCHASED_DATE;
        public String COMMENT;
        public String Shipping_Name;
        public String Shipping_FAX;
        public String Shipping_TELEPHONE;
        public String Shipping_ADDRESS;
        public String Shipping_ADDRESS1;
        public String Shipping_ADDRESS2;
        public String Shipping_CITY;
        public String Shipping_ZIPCODE;
        public String Shipping_STATE;
        public String EXCHANGE_TYPE;
        public String IV_ASC_JOB_NO;// Added By Vijay on 4/2/2019 to Send parent Case Number to Group all child cases in GCIC
        public String IV_CONTRACT_NO;// Added By Vijay on 4/2/2019 to Send Repair Contract Number to Group all child cases in GCIC
        public String EXCHANGE_REASON;
        public String DELIVERY_TYPE;
//        public String Service_level; N/A
        public String ADRESS_CH_FLAG;
        public String UPS_AUTO_PUBLISH;
    }

    public class TicketAttachmentJSON
    {
        public String MSGGUID;
        public String IFID;
        public String IFDate;
        public String Service_Ord_no;
        public String Case_NO;
        public TicketAttachment attach;
    }

    public class TicketAttachment
    {
        public String attachmentName;
        public Attachment attach;
    }

    // IF_HW_Ticket_Status_SVC_GCIC
    public class HWTicketStatusJSON
    {
        //public String MSGGUID;
        //public String IFID;
        //public String IFDate;
        public String Service_Ord_no;
        public String Case_NO;
        public String Ticket_status;
        public List<Comment> CommentList;
    }

    public class Comment
    {
        public String Comment;
        public String Comment_Created_by;
        //public String Comment_Created_by_email;
        public String Comment_Created_by_email = Userinfo.getUserEmail();
        public String Comment_Created_Date;
    }

    // IF_HW_Ticket_Status_GCIC_SVC
    public class HWTicketStatusRefreshJSON
    {
        public String MSGGUID;
        public String IFID;
        public String IFDate;
        public List<HWTicketStatusRefreshItem> serviceOrderList;
    }

    public class HWTicketStatusRefreshItem {
        public String Service_Ord_no;
        public String Case_NO;
    }

    public class WarrantyCheckJSON
    {
        public List<wtyDevice> wtyDeviceList;
    }
    public class wtyDevice{
        //public String MSGGUID;
        //public String IFID;
        //public String IFDate;
        public String I_IMEI;
        public String I_sn;
        public String I_model_code;
        public String I_device_type;
        public String I_so_no;
        public String I_purchased_date;
        public String I_POSTING_DATE;
        public String I_REDO_FLAG;
        public String I_DELIVERY_DATE;
        public String I_ext_wt_contract_no;// Added By Vijay on 4/2/2019 to Send Repair Contract Number
    }

    public class ShippingLabelJSON
    {
        public String MSGGUID;
        public String IFID;
        public String IFDate;
        public String IV_COMPANY;
        public String IV_ASC_CODE;
        public String IV_OBJECT_ID; 
    }

    public class ASCLookupJSON {
        //public String MSGGUID;
        //public String IFID;
        //public String IFDate;
        public String IV_COMPANY;
        public String IV_COUNTRY;
        public String IV_BP_NO;
        public String IV_CITY;
        public String IV_STATE;
        public String IV_ZIP_CODE;
        public String IV_MODEL_CODE;
        public String IV_SERVICE_TYPE;
        public String IV_REQUEST_DATE;
        public String IV_IF_CHANEL;
        public String IV_OBJECT_ID;
        public String IV_CATEG;
        public String IV_WIS_FLAG;
    }


}