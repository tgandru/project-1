/**
 * Created by ms on 2017-07-14.
 * author : sungil.kim, I2MAX
 */

global class SVC_GCICTransferBatchClass implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts {
    public final static String SVC_ID = 'SVC-03';
    public String caseId;// for batch, ParentId
    Integer nS = 0;
    Integer nF = 0;
    private String message = '';
    Set<Id> caseIds = new Set<Id>();
    // Controller
    webservice static String processBatch(Id caseId){
        // Repair which doesn't have a Device
        // Exchange which does have a Device.

        /*
        Fields used in Repair
        Device_Type_f__c : Connected, WiFi
        Case_Device_Status__c : Valid, Invalid, Verified, Unverified
        IMEI__c
        SerialNumber__c
        Purchase_Date__c
        */

        /*
        Fields used in Exchage.(Data from Device)
        Device_Type_f__c : TEXT(Device__r.Device_Type_f__c), Connected, WiFi
        Case_Device_Status_Exchange__c : TEXT(Device__r.Status__c), New, Valid, Invalid ...
        IMEI_Number_Device__c : Device__r.IMEI__c
        Serial_Number_Device__c : Device__r.Serial_Number__c
        Device__r.Purchase_Date__c
         */

        /*
        Model_Code__c : CASE(RecordType.Name,
"Exchange", Device__r.Model_Name__c,
"Repair", Product.SAP_Material_ID__c,
"Repair To Exchange", Product.SAP_Material_ID__c,"")
         */
        Case c = [
                SELECT
                        Id,
                        Parent_Case__c,
                        RecordType.Name,
                        CaseNumber,
                        Level_1__c,
                        Level2SymptomCode__c,
                        Level_3_Symptom_Code__r.Name,
                        Symptom_Type__c,
                        Description,
                        Exchange_Type__c,
                        Exchange_Reason__c,
                        UPS_Auto_Publish__c,
                        ASC_Code__c,
                        Service_Order_Number__c,
                        Model_Code__c,
                        Device_Type_f__c,
                        Case_Device_Status__c,
                        IMEI__c,
                        SerialNumber__c,
                        Parent.CaseNumber,
                        Purchase_Date__c,
                        Case_Device_Status_Exchange__c,
                        IMEI_Number_Device__c,
                        Serial_Number_Device__c,
                        Extended_Warranty_Contract_No__c,
                        Device__r.Purchase_Date__c,
                        Device__r.IMEI__C,
                        Device__r.Serial_Number__c,
                        Device__r.Status__c,
                        Device__r.Model_Name__c,

                        Contact.BP_Number__c,
                        Contact.FirstName,
                        Contact.LastName,
                        Contact.Fax,
                        Contact.Phone,

                        Contact.MailingStreet,
                        Contact.MailingCity,
                        Contact.MailingState,
                        Contact.MailingPostalCode,
                        Contact.MailingCountry,

                        Shipping_Address_1__c,
                        Shipping_Address_2__c,
                        Shipping_Address_3__c,
                        Shipping_City__c,
                        Shipping_Country__c,
                        Shipping_State__c,
                        Shipping_Zip_Code__c,

                        GCIC_Inbound_UPS_Status__c,
                        GCIC_Inbound_UPS_URL__c,

                        isClosed
                FROM Case WHERE Id = :caseId
        ];

        if (c.Parent_Case__c) {
            String q = 'Select count(Id) cnt  from Case where ParentId = \'' + caseId + '\'';
            AggregateResult[] groupedResults = Database.query(q);
            Integer cnt = (Integer)groupedResults[0].get('cnt');
            if (cnt == 0) {
                return 'This parent case has no child.';
            }

            // Parent Case with child cases. > execute a batch to get the child cases.
            SVC_GCICTransferBatchClass batch = new SVC_GCICTransferBatchClass();

            batch.caseId = caseId;
            //Database.executeBatch(batch, 20); // under 100 for limitation. ////08-02-2019::Commented by Thiru to changed Batch Size to 1.
            Database.executeBatch(batch, 1); //08-02-2019::Added by Thiru - changed Batch Size to 1.
            
            return 'GCIC Transfer batch has been started for Child cases ';
        }

        // 단건 처리.

        // isClosed
        if (c.isClosed) return 'This case is already closed.';
        if (SVC_UtilityClass.isEmpty(c.Service_Order_Number__c) == false) return 'This case is already transfered.';
        if ('Exchange'.equals(c.RecordType.Name) == false && 'Repair'.equals(c.RecordType.Name) == false) return 'Transfer is avaliable only for Exchange and Repair case.';

        // Standalone
        if ('Verified'.equals(c.Case_Device_Status__c) == false && 'Registered'.equals(c.Device__r.Status__c) == false) return 'This case has no valid device.(Repair : Verified, Exchange : Registered)';

        if (SVC_UtilityClass.isEmpty(c.Contact.BP_Number__c)) return 'BP Number on contact is empty.';
        if (SVC_UtilityClass.isEmpty(c.Model_Code__c)) return 'Model code on case is empty.';
        if (SVC_UtilityClass.isEmpty(c.Device_Type_f__c)) return 'Device type on case is empty.';

        if (c.RecordType.Name == 'Repair') { // Case
            if (c.Device_Type_f__c == 'Connected') { // IMEI
                if (SVC_UtilityClass.isEmpty(c.IMEI__c)) return 'IMEI on case is empty.';
            } else if (c.Device_Type_f__c == 'WiFi') { // SN
                if (SVC_UtilityClass.isEmpty(c.SerialNumber__c)) return 'Serial Number on case is empty.';
            }
        } else if (c.RecordType.Name == 'Exchange') { // Device
            if (c.Device_Type_f__c == 'Connected') { // IMEI
                if (SVC_UtilityClass.isEmpty(c.IMEI_Number_Device__c)) return 'IMEI on device is empty.';
            } else if (c.Device_Type_f__c == 'WiFi') { // SN
                if (SVC_UtilityClass.isEmpty(c.Serial_Number_Device__c)) return 'Serial Number on device is empty.';
            }
                    if (SVC_UtilityClass.isEmpty(c.Exchange_Reason__c)) return 'Exchange Reason on case is empty.';

        }

        if (SVC_UtilityClass.isEmpty(c.ASC_Code__c)) return 'ASC Lookup on case is empty.';
        if (SVC_UtilityClass.isEmpty(c.Symptom_Type__c)) return 'Symptom Type on case is empty.';
        if (SVC_UtilityClass.isEmpty(c.Level_1__c)) return 'Level 1 symptom code on case is empty.';
        if (SVC_UtilityClass.isEmpty(c.Level2SymptomCode__c)) return 'Level 2 symptom code on case is empty.';
        if (SVC_UtilityClass.isEmpty(c.Level_3_Symptom_Code__r.Name)) return 'Level 3 symptom code on case is empty.';
        if (SVC_UtilityClass.isEmpty(c.Contact.FirstName)) return 'FirstName on Contact is empty.';
        if (SVC_UtilityClass.isEmpty(c.Shipping_Address_1__c)) return 'Shipping Address is empty.';
        if (SVC_UtilityClass.isEmpty(c.Shipping_City__c)) return 'Shipping City on Case is empty.';
        if (SVC_UtilityClass.isEmpty(c.Shipping_Zip_Code__c)) return 'Shipping ZipCode on Case is empty.';
        if (SVC_UtilityClass.isEmpty(c.Shipping_State__c)) return 'Shipping State on Case is empty.';

        SVC_GCICTransferBatchClass batch = new SVC_GCICTransferBatchClass();
        DateTime requestTime = System.now();
        MessageLog messageLog = batch.webcallout(c);
        DateTime responseTime = System.now();
        if (messageLog.body.startsWith('ERROR')) { // http error.
            List<Message_Queue__c> mqList = new List<Message_Queue__c>();

            Message_Queue__c mq = batch.setMessageQueue(caseId, messageLog, requestTime, responseTime);
            mqList.add(mq);
            insert mqList;

            return messageLog.body;
        }

        // succeed.
        List<Message_Queue__c> mqList = new List<Message_Queue__c>();

        Response response = (SVC_GCICTransferBatchClass.Response)JSON.deserialize(messageLog.body, SVC_GCICTransferBatchClass.Response.class);

        Message_Queue__c mq = batch.setMessageQueue(caseId, response, requestTime, responseTime);
        mqList.add(mq);
        insert mqList;

        SVC_GCICTransferBatchClass.ResponseBody body = response.body;

        system.debug('response :: ' + JSON.serialize(response));
        String retCode = body.EV_RET_CODE;
        if (retCode != '0') {
            String retMessage = body.EV_RET_MSG;
            return 'GCIC Error :\r\n' + retMessage;
        }

        c.Service_Order_Number__c = body.EV_OBJECT_ID;
        c.Inbound_Tracking_Number__c = body.EV_UPS_TR_NO; // inbound tracking number.
        c.GCIC_Inbound_UPS_URL__c = body.EV_UPS_URL;
        c.GCIC_Inbound_UPS_Status__c = body.EV_UPS_MESSAGE;

        update c;

        Set<Id> caseIds = new Set<Id>();
        caseIds.add(caseId);// Not ParentId, Child CaseId Or Standalone(=Not Parent and Not Child)

        if (caseIds.isEmpty() == false) {
            // POP attachment
            SVC_GCICAttachmentBatchClass.processBatchClass(caseIds);

            // send Feed item & comment to GCIC
            SVC_GCICAllFeedItemBatch.processBatchClass(caseIds);
        }

        return 'updated'; // reload for UI
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = '';
        query += 'SELECT ';
        query += '      Id, ';
        query += '      Parent_Case__c, ';
        query += '      RecordType.Name, ';
        query += '      CaseNumber, ';
        query += '      Level_1__c, ';
        query += '      Level2SymptomCode__c, ';
        query += '      Level_3_Symptom_Code__r.Name, ';
        query += '      Symptom_Type__c, ';
        query += '      Description, ';
        query += '      Exchange_Type__c, ';
        query += '      Exchange_Reason__c, ';
        query += '      UPS_Auto_Publish__c, ';
        query += '      ASC_Code__c, ';
        query += '      Service_Order_Number__c, ';
        query += ' ';
        query += '      Model_Code__c, ';
        query += ' ';
        query += '      Parent.CaseNumber, ';
        query += ' ';
        query += '      Device_Type_f__c, ';
        query += '      Case_Device_Status__c, ';
        query += '      IMEI__c, ';
        query += '      SerialNumber__c, ';
        query += ' ';
        query += '      Purchase_Date__c, ';
        query += '      Case_Device_Status_Exchange__c, ';
        query += '      IMEI_Number_Device__c, ';
        query += '      Serial_Number_Device__c, ';
        query += ' ';
        query += '      Device__r.Purchase_Date__c, ';
        query += '      Device__r.IMEI__C, ';
        query += '      Device__r.Serial_Number__c, ';
        query += '      Device__r.Status__c, ';
        query += '      Device__r.Model_Name__c, ';
        query += '      Extended_Warranty_Contract_No__c, ';
        query += ' ';
        query += '      Contact.BP_Number__c, ';
        query += '      Contact.FirstName, ';
        query += '      Contact.LastName, ';
        query += '      Contact.Fax, ';
        query += '      Contact.Phone, ';

        query += '      Contact.MailingStreet, ';
        query += '      Contact.MailingCity, ';
        query += '      Contact.MailingState, ';
        query += '      Contact.MailingPostalCode, ';
        query += '      Contact.MailingCountry, ';

        query += '      Shipping_Address_1__c, ';
        query += '      Shipping_Address_2__c, ';
        query += '      Shipping_Address_3__c, ';
        query += '      Shipping_City__c, ';
        query += '      Shipping_Country__c, ';
        query += '      Shipping_State__c, ';
        query += '      Shipping_Zip_Code__c, ';

        query += '      GCIC_Inbound_UPS_Status__c, ';
        query += '      GCIC_Inbound_UPS_URL__c, ';

        query += '      isClosed ';
        query += 'FROM Case WHERE ParentId = \'' + caseId + '\' ';

        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Case> scope) {
        // insert ASC
        List<Message_Queue__c> mqList = new List<Message_Queue__c>();
        List<Case> caseList = new List<Case>();

        for (Case c : scope) {
            if (c.isClosed) continue;
            if (SVC_UtilityClass.isEmpty(c.Service_Order_Number__c) == false) continue;
            if ('Exchange'.equals(c.RecordType.Name) == false && 'Repair'.equals(c.RecordType.Name) == false) continue;

            // Standalone
            if ('Verified'.equals(c.Case_Device_Status__c) == false && 'Registered'.equals(c.Device__r.Status__c) == false) continue;

            if (SVC_UtilityClass.isEmpty(c.Contact.BP_Number__c)) continue;
            if (SVC_UtilityClass.isEmpty(c.Model_Code__c)) continue;
            if (SVC_UtilityClass.isEmpty(c.Device_Type_f__c)) continue;

            if (c.RecordType.Name == 'Repair') { // Case
                if (c.Device_Type_f__c == 'Connected') { // IMEI
                    if (SVC_UtilityClass.isEmpty(c.IMEI__c)) continue;
                } else if (c.Device_Type_f__c == 'WiFi') { // SN
                    if (SVC_UtilityClass.isEmpty(c.SerialNumber__c)) continue;
                }
            } else if (c.RecordType.Name == 'Exchange') { // Device
                if (c.Device_Type_f__c == 'Connected') { // IMEI
                    if (SVC_UtilityClass.isEmpty(c.IMEI_Number_Device__c)) continue;
                } else if (c.Device_Type_f__c == 'WiFi') { // SN
                    if (SVC_UtilityClass.isEmpty(c.Serial_Number_Device__c)) continue;
                }
            }

            if (SVC_UtilityClass.isEmpty(c.ASC_Code__c)) continue;
            if (SVC_UtilityClass.isEmpty(c.Symptom_Type__c)) continue;
            if (SVC_UtilityClass.isEmpty(c.Level_1__c)) continue;
            if (SVC_UtilityClass.isEmpty(c.Level2SymptomCode__c)) continue;
            if (SVC_UtilityClass.isEmpty(c.Level_3_Symptom_Code__r.Name)) continue;

            if (SVC_UtilityClass.isEmpty(c.Contact.FirstName)) continue;

            if (SVC_UtilityClass.isEmpty(c.Shipping_Address_1__c)) continue;
            if (SVC_UtilityClass.isEmpty(c.Shipping_City__c)) continue;
            if (SVC_UtilityClass.isEmpty(c.Shipping_Zip_Code__c)) continue;
            if (SVC_UtilityClass.isEmpty(c.Shipping_State__c)) continue;

            DateTime requestTime = System.now();
            MessageLog messageLog = webcallout(c);
            DateTime responseTime = System.now();
            if (messageLog.body.startsWith('ERROR')) { // http error.
                Message_Queue__c mq = setMessageQueue(caseId, messageLog, requestTime, responseTime);
                mqList.add(mq);

                continue;
            }
            // succeed
            Response response = (SVC_GCICTransferBatchClass.Response)JSON.deserialize(messageLog.body, SVC_GCICTransferBatchClass.Response.class);

            Message_Queue__c mq = setMessageQueue(c.Id, response, requestTime, responseTime);
            mqList.add(mq);

            SVC_GCICTransferBatchClass.ResponseBody body = response.body;

            system.debug('response :: ' + JSON.serialize(response));
            String retCode = body.EV_RET_CODE;
            if (retCode == '0') { // success
                c.Service_Order_Number__c = body.EV_OBJECT_ID;
                c.Inbound_Tracking_Number__c = body.EV_UPS_TR_NO; // inbound tracking number.
                c.GCIC_Inbound_UPS_URL__c = body.EV_UPS_URL;
                c.GCIC_Inbound_UPS_Status__c = body.EV_UPS_MESSAGE;

                caseList.add(c);
                caseIds.add(c.Id);// Not ParentId, Child CaseId Or Standalone(=Not Parent and Not Child)
                nS++;
            } else { // error
                message += c.CaseNumber + ' : ' + body.EV_RET_MSG + '\r\n';
                nF++;
            }
        }

        if (caseList.isEmpty() == false) {
            update caseList;
        }

        if (mqList.isEmpty() == false) {
            insert mqList;
        }
    }

    global void finish(Database.BatchableContext BC) {
        system.debug('finish Call.');
        postToChatter(caseId);

        if (nF > 0) {
            sendEmail();
        }

//        // POP attachment for standalone.
//        Set<Id> caseIds = new Set<Id>();
//        caseIds.add(caseId);// ParentId

        if (caseIds.isEmpty() == false) {
            // POP attachment
            SVC_GCICAttachmentBatchClass.processBatchClass(caseIds);

            // send Feed item & comment to GCIC
            SVC_GCICAllFeedItemBatch.processBatchClass(caseIds);
        }
    }

    private MessageLog webcallout(Case c) {
        string layoutId = Integration_EndPoints__c.getInstance(SVC_GCICTransferBatchClass.SVC_ID).layout_id__c;

        Request request = new Request();
        request.inputHeaders = new cihStub.msgHeadersRequest(layoutId);

        // pseudo
        SVC_GCICJSONRequestClass.HWTicketCreationJSON body = new SVC_GCICJSONRequestClass.HWTicketCreationJSON();

        String company;
        if ('Connected'.equals(c.Device_Type_f__c)) {
            company = 'C370';
        } else if ('WiFi'.equals(c.Device_Type_f__c)) {
            company = 'C310';
        }

        body.COMP = company;
        body.TICKET_FROM = 'P'; // hard-code: "P"
        body.Case_NO = c.CaseNumber;//'';

        String ticketType = (c.RecordType.Name == 'Repair') ? 'DET001' : 'DET002';
        body.TICKET_TYPE = ticketType;//''; // "Repair: DET001 Exchange: DET002"
        body.ASC_ACCNO = SVC_UtilityClass.lpad(c.ASC_Code__c, 10); // Same to ASC_CODE
        body.ASC_CODE = SVC_UtilityClass.lpad(c.ASC_Code__c, 10);//
        body.Contact_BP = c.Contact.BP_Number__c; // Missing Contact.BP#
        body.MODEL = c.Model_Code__c;
        body.IV_CONTRACT_NO = c.Extended_Warranty_Contract_No__c;
        body.SERIAL_NO = (c.RecordType.Name == 'Repair') ? c.SerialNumber__c : c.Serial_Number_Device__c;
        body.IMEI = (c.RecordType.Name == 'Repair') ? c.IMEI__c : c.IMEI_Number_Device__c;

        String symptomType = c.Symptom_Type__c.left(c.Symptom_Type__c.indexOf(' ')).trim();
        String code1 = c.Level_1__c.left(c.Level_1__c.indexOf(' ')).trim();
        String code2 = c.Level2SymptomCode__c.left(c.Level2SymptomCode__c.indexOf(' ')).trim();
        String code3 = c.Level_3_Symptom_Code__r.Name.left(c.Level_3_Symptom_Code__r.Name.indexOf(' ')).trim();

        body.SYMPTOM_TYPE = symptomType;//c.Level_1__c;//'';
        body.SYMPTOM_CAT1 = code1;//c.Level_1__c;//'';
        body.SYMPTOM_CAT2 = code2;//'';
        body.SYMPTOM_CAT3 = code3;//'';

        String serviceType = (c.RecordType.Name == 'Repair') ? 'PS' : 'PS';
        body.SERVICE_TYPE = serviceType;//Service Type (Static Value : Repair PS, Exchange PS )

        String status = (c.RecordType.Name == 'Repair') ? 'ST010' : 'ES005';
        body.STATUS = status;//''; // Ticket Status (Static Value : Repair ST010, Exchange ES005 )

        Date purchaseDate = c.Purchase_Date__c;//(c.RecordType.Name == 'Repair') ? c.Purchase_Date__c : c.Device__r.Purchase_Date__c;

        body.PURCHASED_DATE = SVC_UtilityClass.formatDate(purchaseDate);//'';
        body.COMMENT = c.Description;//'';
        if(c.Parent.CaseNumber != null){
            body.IV_ASC_JOB_NO = c.Parent.CaseNumber;// Added By Vijay on 4/2/2019 to send Parent case Number as grouping for all child cases 
        }
        
        body.Shipping_Name = c.Contact.FirstName + ' ' + c.Contact.LastName;
        body.Shipping_FAX = c.Contact.Fax;
        body.Shipping_TELEPHONE = c.Contact.Phone;
        body.Shipping_ADDRESS = c.Shipping_Address_1__c;
        body.Shipping_ADDRESS1 = c.Shipping_Address_2__c;
        body.Shipping_ADDRESS2 = c.Shipping_Address_3__c;
        body.Shipping_CITY = c.Shipping_City__c;//

//        String zipcode = c.Shipping_Zip_Code__c;
//        if (zipcode.indexOf('-') > -1) {
//            zipcode = zipcode.replace('-', '');// zipcode 9 digit
//        }
        body.Shipping_ZIPCODE = c.Shipping_Zip_Code__c.left(5);
        body.Shipping_STATE = c.Shipping_State__c;
        body.EXCHANGE_TYPE = c.Exchange_Type__c;//''; // Exchange Type(Static Value : Advanced_exchange, ET010)

        String reason = '';
        if (SVC_UtilityClass.isEmpty(c.Exchange_Reason__c) == false) {// picklist > Program Exchange
            reason = c.Exchange_Reason__c.left(10);
        }
        body.EXCHANGE_REASON = reason;//
        body.DELIVERY_TYPE = ''; // No mapping info.
        body.UPS_AUTO_PUBLISH = (c.UPS_Auto_Publish__c) ? 'X' : '';
//        body.Service_level = ''; // No exist in 21 version

        String addressChangeFlag = '';

        if (
                c.Contact.MailingStreet != c.Shipping_Address_1__c
                        || c.Contact.MailingCity != c.Shipping_City__c
                        || c.Contact.MailingState != c.Shipping_State__c
                        || c.Contact.MailingPostalCode != c.Shipping_Zip_Code__c
                ) {// ==  is case insensitive, .equals is case sensitive
            addressChangeFlag = 'Y';
        }
        system.debug('addressChangeFlag :: ' + addressChangeFlag);
        body.ADRESS_CH_FLAG = addressChangeFlag;//'Y'; // No exist in 21 version

        request.body = body;

        String serialized = JSON.serialize(request);
        system.debug('request :: ' + serialized);

        string partialEndpoint = Integration_EndPoints__c.getInstance(SVC_GCICTransferBatchClass.SVC_ID).partial_endpoint__c;

        System.Httprequest req = new System.Httprequest();

        req.setMethod('POST');
        req.setBody(serialized);
        req.setEndpoint('callout:X012_013_ROI' + partialEndpoint);
        req.setTimeout(120000);

        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Accept', 'application/json');

        Http http = new Http();
        HttpResponse res = http.send(req);
        system.debug('res.getStatusCode() :: ' + res.getStatusCode());

        MessageLog messageLog = new MessageLog();

        Integer statusCode = res.getStatusCode();
        messageLog.status = statusCode;
        if (statusCode != 200) {
            messageLog.body = 'ERROR : http status code = ' +  statusCode;
            messageLog.MSGGUID = request.inputHeaders.MSGGUID;
            messageLog.IFID = request.inputHeaders.IFID;
            messageLog.IFDate = request.inputHeaders.IFDate;

            return messageLog;//'ERROR : http status code = ' +  statusCode;
        }

        messageLog.body = res.getBody();
        system.debug('response :: ' + messageLog.body);

        return messageLog;
    }

    // http error save.
    private Message_Queue__c setMessageQueue(Id caseId, MessageLog messageLog, Datetime requestTime, Datetime responseTime) {
        Message_Queue__c mq = new Message_Queue__c();

        mq.ERRORCODE__c = messageLog.body;
        mq.ERRORTEXT__c = messageLog.body;
        mq.IFDate__c = messageLog.IFDate;
        mq.IFID__c = messageLog.IFID;
        mq.Identification_Text__c = caseId; //Object Id(=CaseId)
        mq.Initalized_Request_Time__c = requestTime;//Datetime
        mq.Integration_Flow_Type__c = SVC_ID;
        mq.Is_Created__c = true;
        mq.MSGGUID__c = messageLog.MSGGUID;
        mq.MSGUID__c = messageLog.MSGGUID;
//        mq.MSGSTATUS__c = messageLog.MSGSTATUS;
        mq.Object_Name__c = 'Case';//picklist
        mq.Request_To_CIH_Time__c = requestTime.getTime();//Number(15, 0)
        mq.Response_Processing_Time__c = responseTime.getTime();//Number(15, 0)
        mq.Status__c = 'failed'; // picklist

        return mq;
    }

    private Message_Queue__c setMessageQueue(Id caseId, Response response, Datetime requestTime, Datetime responseTime) {
        cihStub.msgHeadersResponse inputHeaders = response.inputHeaders;
        SVC_GCICTransferBatchClass.ResponseBody body = response.body;

        Message_Queue__c mq = new Message_Queue__c();

        mq.ERRORCODE__c = 'CIH :' + SVC_UtilityClass.nvl(inputHeaders.ERRORCODE) + ', GCIC :' + SVC_UtilityClass.nvl(body.EV_RET_CODE);
        mq.ERRORTEXT__c = 'CIH :' + SVC_UtilityClass.nvl(inputHeaders.ERRORTEXT) + ', GCIC :' + SVC_UtilityClass.nvl(body.EV_RET_MSG);
        mq.IFDate__c = inputHeaders.IFDate;
        mq.IFID__c = inputHeaders.IFID;
        mq.Identification_Text__c = caseId; //Object Id(=CaseId)
        mq.Initalized_Request_Time__c = requestTime;//Datetime
        mq.Integration_Flow_Type__c = SVC_ID; // 003 Need to check code domain. Dev5 has 003 at all.
        mq.Is_Created__c = true;
        mq.MSGGUID__c = inputHeaders.MSGGUID;
        mq.MSGUID__c = inputHeaders.MSGGUID;
        mq.MSGSTATUS__c = inputHeaders.MSGSTATUS;
        mq.Object_Name__c = 'Case';//picklist
        mq.Request_To_CIH_Time__c = requestTime.getTime();//Number(15, 0)
        mq.Response_Processing_Time__c = responseTime.getTime();//Number(15, 0)
        mq.Status__c = (body.EV_RET_CODE == '0') ? 'success' : 'failed'; // picklist

        return mq;
    }

    private void postToChatter(String caseId) {
        User u = [Select Id, Username from User where Username like 'sfdc_if@samsung.com%' LIMIT 1];// Sandbox and Production.
        String chatterMessage = 'Transfer Batch Result\r\n\r\nSucceed : ' + nS + ', Failed :' + nF;
        if (nF > 0) {
            chatterMessage += '\r\n\r\nPlease check your mail.';
        }
        // Parent Id
        FeedItem post = new FeedItem();
        post.ParentId = caseId;//
        post.Body = chatterMessage;//'Batch is completed successfully.';
        post.Type = 'ContentPost';
        post.CreatedById = u.Id;

        insert post;
    }

    private class MessageLog {
        public Integer status;
        public String body;
        public String MSGGUID;
        public String IFID;
        public String IFDate;
        public String MSGSTATUS;
        public String ERRORCODE;
        public String ERRORTEXT;
    }

    private class Request {
        public cihStub.msgHeadersRequest inputHeaders; // variable name is must be inputHeaders. Fixed.
        public SVC_GCICJSONRequestClass.HWTicketCreationJSON body;
    }

    private class Response {
        cihStub.msgHeadersResponse inputHeaders;
        ResponseBody body;
    }

    private class ResponseBody {
        public String EV_RET_CODE;
        public String EV_RET_MSG;
        public String EV_OBJECT_ID; // Service_Order_Number__c
        public String EV_CONSUMER;
        public String EV_WTY_STATUS;
        public String EV_WA_MSG;
        public String EV_UPS_TR_NO;
        // 2017.09.06 add
        public String EV_UPS_URL;
        public String EV_UPS_MESSAGE;
    }

    private void sendEmail() {
        List<Messaging.SingleEmailMessage> mails =  new List<Messaging.SingleEmailMessage>();

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        List<String> to = new List<String>();
        to.add(UserInfo.getUserEmail());

        mail.setToAddresses(to); // List<String> param1

        String supportEmailCCAddress = Label.CC_Email_address_for_SAM_Notification;
        List<String> supportEmailCCAddressList = new List<String>();

        if(String.isNotBlank(supportEmailCCAddress))
            supportEmailCCAddressList = supportEmailCCAddress.split(',');

        if (supportEmailCCAddressList.isEmpty() == false) {
            mail.setCcAddresses(supportEmailCCAddressList);
        }

        mail.setReplyTo(UserInfo.getUserEmail());
        mail.setSenderDisplayName('Transfer Batch Result');
        mail.setSubject('Transfer Batch Result'); // chatterMessage

        String chatterMessage = 'Transfer Batch Result\r\n\r\nSucceed : ' + nS + ', Failed :' + nF;
        mail.setPlainTextBody(chatterMessage + '\r\n\r\n==Failed list==\r\n' + message);

        mails.add(mail);

        Messaging.sendEmail(mails);
    }
}