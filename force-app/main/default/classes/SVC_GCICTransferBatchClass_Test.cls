/**
 * Created by ms on 2017-07-14.
 * author : sungil.kim, I2MAX
 */

@IsTest
private class SVC_GCICTransferBatchClass_Test {
    @testVisible private static User admin1;
    @testVisible private static User admin2;    
    @testVisible private static Zipcode_Lookup__c zl;
    @testVisible private static Account account;
    @testVisible private static Contact contact;
    @testVisible private static Product2 prod1;
    @testVisible private static Product2 prod2;
    @testVisible private static Asset ast;
    @testVisible private static Entitlement ent;
    @testVisible private static Device__c device;
    @testVisible private static Level_3_Symptom_Code__c level3;
    @testVisible private static Map<String, Id> rtMap;

    @testSetup static void methodName() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='SVC-03';
        iep2.endPointURL__c = 'https://www.google.com';
        iep2.layout_id__c = 'LAY024197';
        iep2.partial_endpoint__c = '/SFDC_IF_012';
        insert iep2;

        External_Integration_EndPoints__c a = new External_Integration_EndPoints__c();
        
        a.Name = 'UPS-001';
        a.Username__c = 'a';
        a.Password__c = 'a';
        a.AccessKey__c = 'a';
        a.EndPointURL__c = 'https://wwwcie.ups.com/ups.app/xml/Track';
        insert a;

        // GCIC_To_SFDC_Code_Map__c "Status":"ST030","Reason":"HP010"
        GCIC_To_SFDC_Code_Map__c codeMap = new GCIC_To_SFDC_Code_Map__c();
        codeMap.Name = 'STS-001';
        codeMap.GCIC_Ticket_Name__c = '';
        codeMap.GCIC_Ticket_Type__c = '';
        codeMap.GCIC_Main_Code__c = '';
        codeMap.GCIC_Main_Name__c = '';
        codeMap.GCIC_Status_Code__c = 'ST030';
        codeMap.GCIC_Status_Name__c = 'xxxx';
        codeMap.GCIC_Reason_Code__c = 'HP060';
        codeMap.GCIC_Reason_Name__c = 'xxxx';
        codeMap.SFDC_Status__c = 'Repair In Progress';
        codeMap.SFDC_Sub_Status__c = 'Repair In Progress';
        codeMap.SFDC_Status_Group__c = 'Pending Tech Support';

        insert codeMap;

        Profile adminProfile = [SELECT Id FROM Profile Where Name ='B2B Service System Administrator'];

        admin1 = new User (
            FirstName = 'admin1',
            LastName = 'admin1',
            Email = 'admin1seasde@example.com',
            Alias = 'admint1',
            Username = 'serviceclouduser9142017@ss.com',
            ProfileId = adminProfile.Id,
            TimeZoneSidKey = 'America/New_York',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US'
        );
        
        insert admin1;

        User admin2 = new User (
            FirstName = 'admin2',
            LastName = 'admin2',
            Email = 'admin2@example.com',
            Alias = 'admin2',
            Username = 'serviceclouduser914@samsung.com',
            ProfileId = adminProfile.Id,
            TimeZoneSidKey = 'America/New_York',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US'
        );
        insert admin2;

        zl = new Zipcode_Lookup__c();
        zl.Name = '07650';
        zl.City_Name__c = 'Palisades Park';
        zl.Country_Code__c = 'US';
        zl.State_Code__c = 'NJ';
        zl.State_Name__c = 'New Jersey';
        insert zl;

        account = new Account (
            Name = 'TestAcc1',
            RecordTypeId = rtMap.get('Account' + 'End_User'),//accountRT.Id,
            Type = 'Customer',
            BillingStreet = '411 E Brinkerhoff Ave, Palisades Park',
            BillingCity ='PALISADES PARK',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07650'
        );
        insert account;

        // Insert Contact 
        contact = new Contact(); 
        contact.AccountId = account.id; 
        contact.BP_Number__c = '5000094347';
        contact.MailingState = 'NJ';
        contact.MailingCountry = 'US';
        contact.Email = 'svcTest@svctest.com'; 
        contact.FirstName = 'FirstName'; 
        contact.LastName = 'Named Caller'; 
        contact.Named_Caller__c = true;

        insert contact; 

        prod1 = new Product2(
                Name ='MI-OVCPAC',
                SAP_Material_ID__c = 'SM-G900TZKATMB',
                PET_Name__c = 'Elite Plus Advanced Exchange',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = rtMap.get('Product2' + 'B2B')
            );
        insert prod1;

        //prod2 = new Product2(
        //        Name ='EF-GS6LIFEP',
        //        SAP_Material_ID__c = 'EF-GS6LIFEPOTT',
        //        PET_Name__c = '',
        //        //Family = 'MOBILE PHONE [G1]',
        //        SAP_SW_Product_Code__c = 'EBH2A',
        //        Product_Group__c = 'SOLUTION [MOBILE]',
        //        //sc_service_type__c = 'Incident',
        //        //sc_service_type_ranking__c = '6',
        //        RecordTypeId = rtMap.get('Product2' + 'B2B')
        //    );
        //insert prod2;

        ast = new Asset(
            Name ='MI-OVCPAB',
            Accountid = account.id,
            //RecordTypeId = assetRT.Id,
            Product2Id = prod1.id
            );
        insert ast;

        //Slaprocess sla = [select id from SlaProcess where name LIKE 'Enhanced%'
            //and isActive = true limit 1 ];

        BusinessHours bh12 = [SELECT Id FROM BusinessHours WHERE name = 'B2B Service Hours 12x5'];

        ent = new Entitlement(
            name ='test Entitlement',
            accountid = account.id,
            assetid = ast.id,
            Units_Allowed__c = 100,
            Units_Exchanged__c = 0,
            BusinessHoursId = bh12.id,
            //slaprocessid = sla.id,
            StartDate = System.today().addDays(-14),
            EndDate = System.today().addDays(14)
            );
        insert ent;

        // Device
        device = new Device__c();
        device.Account__c = account.Id;
        //device.Device_Type_f__c = 'Connected'; //Removed because Device Type was changed to formula field
        device.Entitlement__c = ent.Id;
        device.IMEI__c = '84561506582433';
        device.Model_Name__c = 'SM-G900TZKATMB';
        device.Serial_Number__c = 'R52J20VMLSM';
        device.Status__c = 'New';

        insert device;

        device.Status__c = 'Registered';

        update device;

        level3 = new Level_3_Symptom_Code__c();
        
        level3.Symptom_Type__c = 'HHP_HHP (Cell Phone)';
        level3.Level_1_Symptom_Name__c = 'L1 (Accessories)';
        level3.Level_2_Symptom_Name__c = '01 (3rd Party (Non OEM))';
        level3.Name = '01 (3rd Party Battery)';

        insert level3;
    }

     //failed.
    @isTest
    static void itShould()
    {
        account = [SELECT Id FROM Account LIMIT 1];
        admin1 = [SELECT Id FROM User WHERE Username = 'serviceclouduser9142017@ss.com' LIMIT 1];
        device = [SELECT Id FROM Device__c LIMIT 1];
        ent = [SELECT Id FROM Entitlement LIMIT 1];
        contact = [SELECT Id FROM Contact LIMIT 1];
        level3 = [SELECT Id, Name, Symptom_Type__c, Level_1_Symptom_Name__c, Level_2_Symptom_Name__c FROM Level_3_Symptom_Code__c LIMIT 1];
        prod1 = [SELECT Id, Name, Family FROM Product2 LIMIT 1];
        //prod2 = [SELECT Id, Name, Family, SAP_Material_ID__c FROM Product2 WHERE Name ='EF-GS6LIFEP' LIMIT 1];

        rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }
        
        //Test.startTest();

        // case insert
        Case c1 = new Case (
            AccountId = account.id,
            Subject = 'Case1 named caller', 
            Reason = 'test1@test.com', 
            Origin = 'Web',
            Severity__c = '4-Low',
            Ownerid = admin1.id,
            //Device__c = device.Id,
            //ProductId = prod2.Id,
            Purchase_Date__c = Date.today(),
            Parent_Case__c = false,
            RecordTypeId = rtMap.get('Case' + 'Repair'),
            Model_Code_Repair__c = 'SM-G900TZKATMB',
            IMEI__c = '84561506582433',
            EntitlementId = ent.Id,
            ContactId = contact.id,
            Inbound_Shipping_Status__c = 'Shipped',
            Inbound_Tracking_Number__c = '1234567890',
            Outbound_Tracking_Number__c = '1234567890',
            Symptom_Type__c = Level3.Symptom_Type__c,
            Level_1__c = level3.Level_1_Symptom_Name__c,
            Level2SymptomCode__c = level3.Level_2_Symptom_Name__c,
            Level_3_Symptom_Code__c = level3.Id,
            Case_Device_Status__c = 'Verified',
            ASC_Code__c = 'xxxxxxx',
            // address
            Shipping_Address_1__c = '411 E Brinkerhoff Ave, Palisades Park',
            Shipping_City__c = 'PALISADES PARK',
            Shipping_State__c = 'NJ',
            Shipping_Zip_Code__c = '07650',
            Shipping_Country__c = 'US',
            Status = 'New'
        );
        insert c1;

        c1.Case_Device_Status__c = 'Verified';// Valid on Insert then Update is with 'Verified'
        update c1;
        
        Test.startTest();
        
        // for error
        String body = '{"inputHeaders":{"MSGSTATUS":"S","MSGGUID":"ada747b2-dda9-1931-acb2-14a2872d4358","IFID":"IF_ASC_Lookup_SVC_GCIC JSON","IFDate":"160317","ERRORTEXT":null,"ERRORCODE":null},"body":{"EV_WTY_STATUS":null,"EV_WA_MSG":null,"EV_RET_MSG":"xxxx","EV_RET_CODE":"-1","EV_OBJECT_ID":"X-0001","EV_CONSUMER":null}}';

        TestMockSingleCallout singleMock = new TestMockSingleCallout(body);

        Test.setMock(HttpCalloutMock.class, singleMock);

        SVC_GCICTransferBatchClass.processBatch(c1.Id);

        Test.stopTest();
    }

    // success.
    @isTest
    static void itShould2()
    {
        account = [SELECT Id FROM Account LIMIT 1];
        admin1 = [SELECT Id FROM User WHERE Username = 'serviceclouduser9142017@ss.com' LIMIT 1];
        device = [SELECT Id FROM Device__c LIMIT 1];
        ent = [SELECT Id FROM Entitlement LIMIT 1];
        contact = [SELECT Id FROM Contact LIMIT 1];
        level3 = [SELECT Id, Name, Symptom_Type__c, Level_1_Symptom_Name__c, Level_2_Symptom_Name__c FROM Level_3_Symptom_Code__c LIMIT 1];
        prod1 = [SELECT Id, Name, Family FROM Product2 LIMIT 1];
        //prod2 = [SELECT Id, Name, Family, SAP_Material_ID__c FROM Product2 WHERE Name ='EF-GS6LIFEP' LIMIT 1];

        rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }
        
        //Test.startTest();

        // case insert
        Case c1 = new Case (
            AccountId = account.id,
            Subject = 'Case1 named caller', 
            Reason = 'test1@test.com', 
            Origin = 'Web',
            Severity__c = '4-Low',
            Ownerid = admin1.id,
            //Device__c = device.Id,
            //ProductId = prod2.Id,
            Purchase_Date__c = Date.today(),
            Parent_Case__c = false,
            RecordTypeId = rtMap.get('Case' + 'Repair'),
            Model_Code_Repair__c = 'SM-G900TZKATMB',
            IMEI__c = '84561506582433',
            EntitlementId = ent.Id,
            ContactId = contact.id,
            Inbound_Shipping_Status__c = 'Shipped',
            Inbound_Tracking_Number__c = '1234567890',
            Outbound_Tracking_Number__c = '1234567890',
            Symptom_Type__c = Level3.Symptom_Type__c,
            Level_1__c = level3.Level_1_Symptom_Name__c,
            Level2SymptomCode__c = level3.Level_2_Symptom_Name__c,
            Level_3_Symptom_Code__c = level3.Id,
            Case_Device_Status__c = 'Verified',
            ASC_Code__c = 'xxxxxxx',
            // address
            Shipping_Address_1__c = '411 E Brinkerhoff Ave, Palisades Park',
            Shipping_City__c = 'PALISADES PARK',
            Shipping_State__c = 'NJ',
            Shipping_Zip_Code__c = '07650',
            Shipping_Country__c = 'US',
            Status = 'New'
        );
        insert c1;

        c1.Case_Device_Status__c = 'Verified';// Valid on Insert then Update is with 'Verified'
        update c1;
        
        Test.startTest();
        
        // for error
        String body = '{"inputHeaders":{"MSGSTATUS":"S","MSGGUID":"ada747b2-dda9-1931-acb2-14a2872d4358","IFID":"IF_ASC_Lookup_SVC_GCIC JSON","IFDate":"160317","ERRORTEXT":null,"ERRORCODE":null},"body":{"EV_WTY_STATUS":null,"EV_WA_MSG":null,"EV_RET_MSG":"xxxx","EV_RET_CODE":"0","EV_OBJECT_ID":"X-0001","EV_CONSUMER":null}}';

        TestMockSingleCallout singleMock = new TestMockSingleCallout(body);

        Test.setMock(HttpCalloutMock.class, singleMock);

        SVC_GCICTransferBatchClass.processBatch(c1.Id);

        Test.stopTest();
    }

    // on demand with no child as Parent Case
    @isTest
    static void itShould3()
    {
        account = [SELECT Id FROM Account LIMIT 1];
        admin1 = [SELECT Id FROM User WHERE Username = 'serviceclouduser9142017@ss.com' LIMIT 1];
        device = [SELECT Id FROM Device__c LIMIT 1];
        ent = [SELECT Id FROM Entitlement LIMIT 1];
        contact = [SELECT Id FROM Contact LIMIT 1];
        level3 = [SELECT Id, Name, Symptom_Type__c, Level_1_Symptom_Name__c, Level_2_Symptom_Name__c FROM Level_3_Symptom_Code__c LIMIT 1];
        prod1 = [SELECT Id, Name, Family FROM Product2 LIMIT 1];
        //prod2 = [SELECT Id, Name, Family, SAP_Material_ID__c FROM Product2 WHERE Name ='EF-GS6LIFEP' LIMIT 1];

        rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }
        
        //Test.startTest();

        // case insert
        Case c1 = new Case (
            AccountId = account.id,
            Subject = 'Case1 named caller', 
            Reason = 'test1@test.com', 
            Origin = 'Web',
            Severity__c = '4-Low',
            Ownerid = admin1.id,
            //Device__c = device.Id,
            //ProductId = prod2.Id,
            Purchase_Date__c = Date.today(),
            Parent_Case__c = true,
            RecordTypeId = rtMap.get('Case' + 'Repair'),
            //Model_Code_Repair__c = 'SM-G900TZKATMB',
            //IMEI__c = '84561506582433',
            //EntitlementId = ent.Id,
            ContactId = contact.id,
            Inbound_Shipping_Status__c = 'Shipped',
            Inbound_Tracking_Number__c = '1234567890',
            Outbound_Tracking_Number__c = '1234567890',
            Symptom_Type__c = Level3.Symptom_Type__c,
            Level_1__c = level3.Level_1_Symptom_Name__c,
            Level2SymptomCode__c = level3.Level_2_Symptom_Name__c,
            Level_3_Symptom_Code__c = level3.Id,
            Case_Device_Status__c = 'Verified',
            ASC_Code__c = 'xxxxxxx',
            // address
            Shipping_Address_1__c = '411 E Brinkerhoff Ave, Palisades Park',
            Shipping_City__c = 'PALISADES PARK',
            Shipping_State__c = 'NJ',
            Shipping_Zip_Code__c = '07650',
            Shipping_Country__c = 'US',
            Status = 'New'
        );
        insert c1;

        c1.Case_Device_Status__c = 'Verified';// Valid on Insert then Update is with 'Verified'
        update c1;
        
        Test.startTest();
        
        // for error
        String body = '{"inputHeaders":{"MSGSTATUS":"S","MSGGUID":"ada747b2-dda9-1931-acb2-14a2872d4358","IFID":"IF_ASC_Lookup_SVC_GCIC JSON","IFDate":"160317","ERRORTEXT":null,"ERRORCODE":null},"body":{"EV_WTY_STATUS":null,"EV_WA_MSG":null,"EV_RET_MSG":"xxxx","EV_RET_CODE":"0","EV_OBJECT_ID":"X-0001","EV_CONSUMER":null}}';

        TestMockSingleCallout singleMock = new TestMockSingleCallout(body);

        Test.setMock(HttpCalloutMock.class, singleMock);

        SVC_GCICTransferBatchClass.processBatch(c1.Id);

        Test.stopTest();
    }

    // batch for on demand with no child as Parent Case
    @isTest
    static void itShould4()
    {
        account = [SELECT Id FROM Account LIMIT 1];
        admin1 = [SELECT Id FROM User WHERE Username = 'serviceclouduser9142017@ss.com' LIMIT 1];
        device = [SELECT Id FROM Device__c LIMIT 1];
        ent = [SELECT Id FROM Entitlement LIMIT 1];
        contact = [SELECT Id FROM Contact LIMIT 1];
        level3 = [SELECT Id, Name, Symptom_Type__c, Level_1_Symptom_Name__c, Level_2_Symptom_Name__c FROM Level_3_Symptom_Code__c LIMIT 1];
        prod1 = [SELECT Id, Name, Family FROM Product2 LIMIT 1];
        //prod2 = [SELECT Id, Name, Family, SAP_Material_ID__c FROM Product2 WHERE Name ='EF-GS6LIFEP' LIMIT 1];

        rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }
        
        //Test.startTest();

        // case insert
        Case c1 = new Case (
            AccountId = account.id,
            Subject = 'Case1 named caller', 
            Reason = 'test1@test.com', 
            Origin = 'Web',
            Severity__c = '4-Low',
            Ownerid = admin1.id,
            //Device__c = device.Id,
            //ProductId = prod2.Id,
            Purchase_Date__c = Date.today(),
            Parent_Case__c = true,
            RecordTypeId = rtMap.get('Case' + 'Repair'),
            //Model_Code_Repair__c = 'SM-G900TZKATMB',
            //IMEI__c = '84561506582433',
            //EntitlementId = ent.Id,
            ContactId = contact.id,
            Inbound_Shipping_Status__c = 'Shipped',
            Inbound_Tracking_Number__c = '1234567890',
            Outbound_Tracking_Number__c = '1234567890',
            Symptom_Type__c = Level3.Symptom_Type__c,
            Level_1__c = level3.Level_1_Symptom_Name__c,
            Level2SymptomCode__c = level3.Level_2_Symptom_Name__c,
            Level_3_Symptom_Code__c = level3.Id,
            Case_Device_Status__c = 'Verified',
            ASC_Code__c = 'xxxxxxx',
            // address
            Shipping_Address_1__c = '411 E Brinkerhoff Ave, Palisades Park',
            Shipping_City__c = 'PALISADES PARK',
            Shipping_State__c = 'NJ',
            Shipping_Zip_Code__c = '07650',
            Shipping_Country__c = 'US',
            Status = 'New'
        );
        insert c1;

        c1.Case_Device_Status__c = 'Verified';// Valid on Insert then Update is with 'Verified'
        update c1;
        
        Test.startTest();
        
        // for error
        String body = '{"inputHeaders":{"MSGSTATUS":"S","MSGGUID":"ada747b2-dda9-1931-acb2-14a2872d4358","IFID":"IF_ASC_Lookup_SVC_GCIC JSON","IFDate":"160317","ERRORTEXT":null,"ERRORCODE":null},"body":{"EV_WTY_STATUS":null,"EV_WA_MSG":null,"EV_RET_MSG":"xxxx","EV_RET_CODE":"0","EV_OBJECT_ID":"X-0001","EV_CONSUMER":null}}';

        TestMockSingleCallout singleMock = new TestMockSingleCallout(body);

        Test.setMock(HttpCalloutMock.class, singleMock);

        //SVC_GCICTransferBatchClass.processBatch(c1.Id);

        SVC_GCICTransferBatchClass batch = new SVC_GCICTransferBatchClass();

        batch.caseId = c1.Id;
        Database.executeBatch(batch, 1); // under 100 for limitation.

        Test.stopTest();
    }

    // batch for on demand with child as Parent Case
    @isTest
    static void itShould5()
    {
        account = [SELECT Id FROM Account LIMIT 1];
        admin1 = [SELECT Id FROM User WHERE Username = 'serviceclouduser9142017@ss.com' LIMIT 1];
        device = [SELECT Id FROM Device__c LIMIT 1];
        ent = [SELECT Id FROM Entitlement LIMIT 1];
        contact = [SELECT Id FROM Contact LIMIT 1];
        level3 = [SELECT Id, Name, Symptom_Type__c, Level_1_Symptom_Name__c, Level_2_Symptom_Name__c FROM Level_3_Symptom_Code__c LIMIT 1];
        prod1 = [SELECT Id, Name, Family FROM Product2 LIMIT 1];
        //prod2 = [SELECT Id, Name, Family, SAP_Material_ID__c FROM Product2 WHERE Name ='EF-GS6LIFEP' LIMIT 1];
        //Case c1 = [SELECT Id, Ownerid, RecordTypeId, AccountId, ContactId FROM Case LIMIT 1];

        rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }
        
        //Test.startTest();
        // case insert
        Case c1 = new Case (
            AccountId = account.id,
            Subject = 'Case1 named caller', 
            Reason = 'test1@test.com', 
            Origin = 'Web',
            Severity__c = '4-Low',
            Ownerid = admin1.id,
            //Device__c = device.Id,
            //ProductId = prod2.Id,
            Purchase_Date__c = Date.today(),
            Parent_Case__c = true,
            RecordTypeId = rtMap.get('Case' + 'Repair'),
            //Model_Code_Repair__c = 'SM-G900TZKATMB',
            //IMEI__c = '84561506582433',
            //EntitlementId = ent.Id,
            ContactId = contact.id,
            Inbound_Shipping_Status__c = 'Shipped',
            Inbound_Tracking_Number__c = '1234567890',
            Outbound_Tracking_Number__c = '1234567890',
            Symptom_Type__c = Level3.Symptom_Type__c,
            Level_1__c = level3.Level_1_Symptom_Name__c,
            Level2SymptomCode__c = level3.Level_2_Symptom_Name__c,
            Level_3_Symptom_Code__c = level3.Id,
            Case_Device_Status__c = 'Verified',
            ASC_Code__c = 'xxxxxxx',
            // address
            Shipping_Address_1__c = '411 E Brinkerhoff Ave, Palisades Park',
            Shipping_City__c = 'PALISADES PARK',
            Shipping_State__c = 'NJ',
            Shipping_Zip_Code__c = '07650',
            Shipping_Country__c = 'US',
            Status = 'New'
        );
        //insert c1;

        Case c2 = new Case (
            AccountId = account.id,
            Subject = 'Case1 named caller', 
            Reason = 'test1@test.com', 
            Origin = 'Web',
            Severity__c = '4-Low',
            Ownerid = admin1.id,
            //Device__c = device.Id,
            //ProductId = prod2.Id,
            //ParentId = c1.Id,
            Purchase_Date__c = Date.today(),
            Parent_Case__c = false,
            RecordTypeId = rtMap.get('Case' + 'Repair'),
            Model_Code_Repair__c = 'SM-G900TZKATMB',
            IMEI__c = '84561506582433',
            EntitlementId = ent.Id,
            ContactId = contact.id,
            Inbound_Shipping_Status__c = 'Shipped',
            Inbound_Tracking_Number__c = '1234567890',
            Outbound_Tracking_Number__c = '1234567890',
            Symptom_Type__c = Level3.Symptom_Type__c,
            Level_1__c = level3.Level_1_Symptom_Name__c,
            Level2SymptomCode__c = level3.Level_2_Symptom_Name__c,
            Level_3_Symptom_Code__c = level3.Id,
            Case_Device_Status__c = 'Verified',
            ASC_Code__c = 'xxxxxxx',
            // address
            Shipping_Address_1__c = '411 E Brinkerhoff Ave, Palisades Park',
            Shipping_City__c = 'PALISADES PARK',
            Shipping_State__c = 'NJ',
            Shipping_Zip_Code__c = '07650',
            Shipping_Country__c = 'US',
            Status = 'New'
        );
        //insert c2;

        List<Case> caseList = new List<Case>();
        caseList.add(c1);
        caseList.add(c2);

        insert caseList;
        
        c1.Case_Device_Status__c = 'Verified';// Valid on Insert then Update is with 'Verified'
//        c2.Case_Device_Status__c = 'Verified';// Valid on Insert then Update is with 'Verified'
//        c2.ParentId = c1.Id;

        //update caseList;

        Test.startTest();

        // for success
        String body = '{"inputHeaders":{"MSGSTATUS":"S","MSGGUID":"ada747b2-dda9-1931-acb2-14a2872d4358","IFID":"IF_ASC_Lookup_SVC_GCIC JSON","IFDate":"160317","ERRORTEXT":null,"ERRORCODE":null},"body":{"EV_WTY_STATUS":null,"EV_WA_MSG":null,"EV_RET_MSG":"xxxx","EV_RET_CODE":"-1","EV_OBJECT_ID":"X-0001","EV_CONSUMER":null}}';

        TestMockSingleCallout singleMock = new TestMockSingleCallout(body);

        Test.setMock(HttpCalloutMock.class, singleMock);

        c2 = [
            Select 
                Id,
                Parent_Case__c,
                RecordType.Name,
                CaseNumber,
                Level_1__c,
                Level2SymptomCode__c,
                Level_3_Symptom_Code__r.Name,
                Symptom_Type__c,
                Description,
                Exchange_Type__c,
                Exchange_Reason__c,
                UPS_Auto_Publish__c,
                ASC_Code__c,
                Service_Order_Number__c,
                Model_Code__c,
                Device_Type_f__c,
                Case_Device_Status__c,
                IMEI__c,
                SerialNumber__c,

                Purchase_Date__c,
                Case_Device_Status_Exchange__c,
                IMEI_Number_Device__c,
                Serial_Number_Device__c,

                Device__r.Purchase_Date__c,
                Device__r.IMEI__C,
                Device__r.Serial_Number__c,
                Device__r.Status__c,
                Device__r.Model_Name__c,

                Contact.BP_Number__c,
                Contact.FirstName,
                Contact.LastName,
                Contact.Fax,
                Contact.Phone,

                Contact.MailingStreet,
                Contact.MailingCity,
                Contact.MailingState,
                Contact.MailingPostalCode,
                Contact.MailingCountry,

                Shipping_Address_1__c,
                Shipping_Address_2__c,
                Shipping_Address_3__c,
                Shipping_City__c,
                Shipping_Country__c,
                Shipping_State__c,
                Shipping_Zip_Code__c,

                GCIC_Inbound_UPS_Status__c,
                GCIC_Inbound_UPS_URL__c,

                isClosed
            from Case WHERE Id = :c2.Id
        ];
        c2.Case_Device_Status__c = 'Verified';// Valid on Insert then Update is with 'Verified'
        c2.ParentId = c1.Id;

        SVC_GCICTransferBatchClass batch = new SVC_GCICTransferBatchClass();

        batch.caseId = c1.Id;
        batch.execute(null, new List<Case>{c2});
        batch.finish(null);

        Test.stopTest();
    }

    // http error
    @isTest
    static void itShould6()
    {
        account = [SELECT Id FROM Account LIMIT 1];
        admin1 = [SELECT Id FROM User WHERE Username = 'serviceclouduser9142017@ss.com' LIMIT 1];
        device = [SELECT Id FROM Device__c LIMIT 1];
        ent = [SELECT Id FROM Entitlement LIMIT 1];
        contact = [SELECT Id FROM Contact LIMIT 1];
        level3 = [SELECT Id, Name, Symptom_Type__c, Level_1_Symptom_Name__c, Level_2_Symptom_Name__c FROM Level_3_Symptom_Code__c LIMIT 1];
        prod1 = [SELECT Id, Name, Family FROM Product2 LIMIT 1];
        //prod2 = [SELECT Id, Name, Family, SAP_Material_ID__c FROM Product2 WHERE Name ='EF-GS6LIFEP' LIMIT 1];
        //Case c1 = [SELECT Id, Ownerid, RecordTypeId, AccountId, ContactId FROM Case LIMIT 1];

        rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }
        
        //Test.startTest();
        // case insert
        Case c1 = new Case (
            AccountId = account.id,
            Subject = 'Case1 named caller', 
            Reason = 'test1@test.com', 
            Origin = 'Web',
            Severity__c = '4-Low',
            Ownerid = admin1.id,
            //Device__c = device.Id,
            //ProductId = prod2.Id,
            Purchase_Date__c = Date.today(),
            Parent_Case__c = true,
            RecordTypeId = rtMap.get('Case' + 'Repair'),
            //Model_Code_Repair__c = 'SM-G900TZKATMB',
            //IMEI__c = '84561506582433',
            //EntitlementId = ent.Id,
            ContactId = contact.id,
            Inbound_Shipping_Status__c = 'Shipped',
            Inbound_Tracking_Number__c = '1234567890',
            Outbound_Tracking_Number__c = '1234567890',
            Symptom_Type__c = Level3.Symptom_Type__c,
            Level_1__c = level3.Level_1_Symptom_Name__c,
            Level2SymptomCode__c = level3.Level_2_Symptom_Name__c,
            Level_3_Symptom_Code__c = level3.Id,
            Case_Device_Status__c = 'Verified',
            ASC_Code__c = 'xxxxxxx',
            // address
            Shipping_Address_1__c = '411 E Brinkerhoff Ave, Palisades Park',
            Shipping_City__c = 'PALISADES PARK',
            Shipping_State__c = 'NJ',
            Shipping_Zip_Code__c = '07650',
            Shipping_Country__c = 'US',
            Status = 'New'
        );
        //insert c1;

        Case c2 = new Case (
            AccountId = account.id,
            Subject = 'Case1 named caller', 
            Reason = 'test1@test.com', 
            Origin = 'Web',
            Severity__c = '4-Low',
            Ownerid = admin1.id,
            //Device__c = device.Id,
            //ProductId = prod2.Id,
            //ParentId = c1.Id,
            Purchase_Date__c = Date.today(),
            Parent_Case__c = false,
            RecordTypeId = rtMap.get('Case' + 'Repair'),
            Model_Code_Repair__c = 'SM-G900TZKATMB',
            IMEI__c = '84561506582433',
            EntitlementId = ent.Id,
            ContactId = contact.id,
            Inbound_Shipping_Status__c = 'Shipped',
            Inbound_Tracking_Number__c = '1234567890',
            Outbound_Tracking_Number__c = '1234567890',
            Symptom_Type__c = Level3.Symptom_Type__c,
            Level_1__c = level3.Level_1_Symptom_Name__c,
            Level2SymptomCode__c = level3.Level_2_Symptom_Name__c,
            Level_3_Symptom_Code__c = level3.Id,
            Case_Device_Status__c = 'Verified',
            ASC_Code__c = 'xxxxxxx',
            // address
            Shipping_Address_1__c = '411 E Brinkerhoff Ave, Palisades Park',
            Shipping_City__c = 'PALISADES PARK',
            Shipping_State__c = 'NJ',
            Shipping_Zip_Code__c = '07650',
            Shipping_Country__c = 'US',
            Status = 'New'
        );
        //insert c2;

        List<Case> caseList = new List<Case>();
        caseList.add(c1);
        caseList.add(c2);

        insert caseList;
        
        c1.Case_Device_Status__c = 'Verified';// Valid on Insert then Update is with 'Verified'
        c2.Case_Device_Status__c = 'Verified';// Valid on Insert then Update is with 'Verified'
        c2.ParentId = c1.Id;

        //update caseList;

        Test.startTest();

        // for success
        String body = '{"inputHeaders":{"MSGSTATUS":"S","MSGGUID":"ada747b2-dda9-1931-acb2-14a2872d4358","IFID":"IF_ASC_Lookup_SVC_GCIC JSON","IFDate":"160317","ERRORTEXT":null,"ERRORCODE":null},"body":{"EV_WTY_STATUS":null,"EV_WA_MSG":null,"EV_RET_MSG":"xxxx","EV_RET_CODE":"0","EV_OBJECT_ID":"X-0001","EV_CONSUMER":null}}';

        TestMockSingleCallout singleMock = new TestMockSingleCallout(body, 500);

        Test.setMock(HttpCalloutMock.class, singleMock);

        SVC_GCICTransferBatchClass batch = new SVC_GCICTransferBatchClass();

        //batch.caseId = c1.Id;
        //Database.executeBatch(batch, 1); // under 100 for limitation.

        batch.caseId = c1.Id;

        c2 = [
            Select 
                Id,
                Parent_Case__c,
                RecordType.Name,
                CaseNumber,
                Level_1__c,
                Level2SymptomCode__c,
                Level_3_Symptom_Code__r.Name,
                Symptom_Type__c,
                Description,
                Exchange_Type__c,
                Exchange_Reason__c,
                UPS_Auto_Publish__c,
                ASC_Code__c,
                Service_Order_Number__c,
                Model_Code__c,
                Device_Type_f__c,
                Case_Device_Status__c,
                IMEI__c,
                SerialNumber__c,

                Purchase_Date__c,
                Case_Device_Status_Exchange__c,
                IMEI_Number_Device__c,
                Serial_Number_Device__c,

                Device__r.Purchase_Date__c,
                Device__r.IMEI__C,
                Device__r.Serial_Number__c,
                Device__r.Status__c,
                Device__r.Model_Name__c,

                Contact.BP_Number__c,
                Contact.FirstName,
                Contact.LastName,
                Contact.Fax,
                Contact.Phone,

                Contact.MailingStreet,
                Contact.MailingCity,
                Contact.MailingState,
                Contact.MailingPostalCode,
                Contact.MailingCountry,

                Shipping_Address_1__c,
                Shipping_Address_2__c,
                Shipping_Address_3__c,
                Shipping_City__c,
                Shipping_Country__c,
                Shipping_State__c,
                Shipping_Zip_Code__c,

                GCIC_Inbound_UPS_Status__c,
                GCIC_Inbound_UPS_URL__c,

                isClosed
            from Case WHERE Id = :c2.Id
        ];
        c2.Case_Device_Status__c = 'Verified';// Valid on Insert then Update is with 'Verified'
        c2.ParentId = c1.Id;

        batch.execute(null, new List<Case>{c2});
        batch.finish(null);

        Test.stopTest();
    }


}