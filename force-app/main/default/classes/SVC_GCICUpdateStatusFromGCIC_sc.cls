/**
 * Created by ms on 2017-08-01.
 * author : sungil.kim, I2MAX

String CRON_EXP = '0 0 0,4,8,12,16,20 ? * *';// every four hours
System.schedule('Ticket Update from GCIC in every four hours', CRON_EXP, new SVC_GCICUpdateStatusFromGCIC_sc());
 */

public with sharing class SVC_GCICUpdateStatusFromGCIC_sc implements Schedulable {
    public void execute(SchedulableContext sc) {
        SVC_GCICUpdateStatusFromGCIC batch = new SVC_GCICUpdateStatusFromGCIC();
        Integer batchsize=50;
        try{
            Batch_Size_Settings__c obj= Batch_Size_Settings__c.getvalues('SVC_GCICUpdateStatusFromGCIC'); 
            batchsize= Integer.valueOf(obj.Batch_Size__c);
        }catch(exception ex){
            
        }
        Database.executeBatch(batch, batchsize); // Because of webcallout limitation(100 per transaction), maximum 50 is limitation.
    }
}