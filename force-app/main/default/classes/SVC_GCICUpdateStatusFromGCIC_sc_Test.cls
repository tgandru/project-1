/**
 * Created by ms on 2017-08-02.
 * author : sungil.kim, I2MAX
 */

@IsTest
private class SVC_GCICUpdateStatusFromGCIC_sc_Test {
    @isTest
    static void itShould()
    {
        String CRON_EXP = '0 0 0 4 10 ? '  + (System.now().year() + 1) ; // Next Year
        Test.startTest();
        System.schedule('Test UPS delivery status update per one hour', CRON_EXP, new SVC_GCICUpdateStatusFromGCIC_sc());
        Test.stopTest();
    }
}