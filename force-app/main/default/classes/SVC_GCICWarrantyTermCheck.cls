/**
 * Created by ms on 2017-07-24.
 *
 * author : JeongHo.Lee, I2MAX
 */
global class SVC_GCICWarrantyTermCheck {
    
    webservice static string warrantyCheck(id caseId){
        String result = '';
        Map<String, Case> caseMap = new Map<String, Case>();
        //Stand Alone, A Child
        Case ca  = [SELECT Id 
                            , IMEI__c
                            , SerialNumber__c
                            , Device_Type_f__c                      //formula
                            , IMEI_Number_Device__c                 //formula
                            , Serial_Number_Device__c               //formula
                            , RecordType.DeveloperName
                            , Device__c
                            , Device__r.Model_Name__c
                            , Case_Device_Status__c
                            , Parent_Case__c
                            , ParentId
                            , Warranty_Status__c
                            , Model_Code__c
                            , Model_Code_Repair__c
                            , Extended_Warranty_Contract_No__c
                            , Extended_Warranty_Start_Date__c
                            , Extended_Warranty_Contract_Name__c
                            , Extended_Warranty_End_Date__c
                            , Extended_Warranty_Information__c 
                    FROM Case 
                    WHERE id =: caseId];
         //Stand Alone, A Child
        if(ca.Parent_Case__c == false){
            //SVC_GCICWarrantyTermCheck.toSend(ca.Id);

            //Request Header
            String layoutId = Integration_EndPoints__c.getInstance('SVC-06').layout_id__c;
            String partialEndpoint = Integration_EndPoints__c.getInstance('SVC-06').partial_endpoint__c;

            WtyCheckRequest WtyCheckReq = new WtyCheckRequest();
            SVC_GCICJSONRequestClass.WarrantyCheckJSON body = new SVC_GCICJSONRequestClass.WarrantyCheckJSON();
            cihStub.msgHeadersRequest headers = new cihStub.msgHeadersRequest(layoutId);
            body.wtyDeviceList = new List<SVC_GCICJSONRequestClass.wtyDevice>();

            SVC_GCICJSONRequestClass.wtyDevice wd = new SVC_GCICJSONRequestClass.wtyDevice();
            //wd.MSGGUID            = cihStub.giveGUID();
            //wd.IFID               = layoutId;
            //wd.IFDate         = String.valueOf(datetime.now().formatGMT('YYYYMMddHHmmss'));
            if(ca.RecordType.DeveloperName == 'Repair'){
                wd.I_IMEI           = ca.IMEI__c;
                wd.I_sn             = ca.SerialNumber__c;
                wd.I_model_code     = ca.Model_Code__c;
                wd.I_ext_wt_contract_no     = ca.Extended_Warranty_Contract_No__c;// Added By Vijay on 4/2/2019 to send Contract Number to GCIC
                if(wd.I_IMEI != null && wd.I_sn != null) wd.I_sn = null;
                if(ca.Device_Type_f__c == 'Connected') wd.I_device_type = 'IMEI';
                if(ca.Device_Type_f__c == 'Wifi') wd.I_device_type    = 'SN';
                if(ca.Extended_Warranty_Contract_No__c !=null) wd.I_device_type    = 'CNTR';
                //if(wd.I_IMEI != null) caseMap.put(ca.IMEI__c , ca);
                if(wd.I_IMEI != null) caseMap.put(ca.IMEI__c.left(14) , ca);
                if(wd.I_sn != null) caseMap.put(ca.SerialNumber__c , ca);
                if(wd.I_ext_wt_contract_no != null) caseMap.put(ca.Extended_Warranty_Contract_No__c , ca);
            }
            else if(ca.RecordType.DeveloperName == 'Exchange'){
                wd.I_IMEI           = ca.IMEI_Number_Device__c;
                wd.I_sn             = ca.Serial_Number_Device__c;
                wd.I_model_code     = ca.Model_Code__c;
                if(wd.I_IMEI != null && wd.I_sn != null) wd.I_sn = null;
                if(ca.Device_Type_f__c == 'Connected') wd.I_device_type = 'IMEI';
                if(ca.Device_Type_f__c == 'Wifi') wd.I_device_type = 'SN';
                //if(wd.I_IMEI != null) caseMap.put(ca.IMEI_Number_Device__c , ca);
                if(wd.I_IMEI != null) caseMap.put(ca.IMEI_Number_Device__c.left(14) , ca);
                if(wd.I_sn != null) caseMap.put(ca.Serial_Number_Device__c , ca);
            }
            body.wtyDeviceList.add(wd);

            List<wtyDevice> wtyDeviceList = new List<wtyDevice>();
            if(!body.wtyDeviceList.isEmpty()){
                WtyCheckReq.body = body;
                WtyCheckReq.inputHeaders = headers;
                string requestBody = json.serialize(WtyCheckReq);
                system.debug(requestBody);

                System.Httprequest req = new System.Httprequest();
                HttpResponse res = new HttpResponse();
                req.setMethod('POST');
                req.setBody(requestBody);
              
                req.setEndpoint('callout:X012_013_ROI'+partialEndpoint);
                //req.setEndpoint('callout:CIH_Production'+partialEndpoint);
                req.setTimeout(120000);
                    
                req.setHeader('Content-Type', 'application/json');
                req.setHeader('Accept', 'application/json');
            
                Http http = new Http();
                Datetime requestTime = system.now();   
                res = http.send(req);
                Datetime responseTime = system.now();

                
                if(res.getStatusCode() == 200){
                    //success
                    system.debug('res.getBody() : ' + res.getBody());
                    Response response = (SVC_GCICWarrantyTermCheck.Response)JSON.deserialize(res.getBody(), SVC_GCICWarrantyTermCheck.Response.class);
                    
                    system.debug('response : ' + response);

                    Message_Queue__c mq = setMessageQueue('Case', response, requestTime, responseTime);
                    //mqList.add(mq);
                    //insert mqList;
                    insert mq;

                    SVC_GCICWarrantyTermCheck.ResponseBody resbody = response.body;
                    system.debug('resbody : ' + resbody);


                    wtyDeviceList = resbody.EtOutput.item;
                    system.debug('wtyDeviceList size : ' + wtyDeviceList.size());
                    system.debug('wtyDeviceList : ' + wtyDeviceList);
                }
                else{
                    //Fail
                    MessageLog messageLog = new MessageLog();
                    messageLog.body = 'ERROR : http status code = ' +  res.getStatusCode();
                    messageLog.MSGGUID = WtyCheckReq.inputHeaders.MSGGUID;
                    messageLog.IFID = WtyCheckReq.inputHeaders.IFID;
                    messageLog.IFDate = WtyCheckReq.inputHeaders.IFDate;

                    Message_Queue__c mq = setHttpQueue('Case', messageLog, requestTime, responseTime);
                    //mqList.add(mq);
                    //insert mqList;
                    insert mq;
                }
            }
            if(!wtyDeviceList.isEmpty()){
                for(Integer i = 0;  i< wtyDeviceList.size(); i++){
                    if(wtyDeviceList[i].Imei != null && caseMap.get(wtyDeviceList[i].Imei.left(14)) != null){
                        //RET == 0 or  RET == 1 ?
                        if(wtyDeviceList[i].Ret == '0'){
                            caseMap.get(wtyDeviceList[i].Imei.left(14)).Extended_Warranty_Contract_Name__c   = wtyDeviceList[i].ExtWtyContractName;
                            caseMap.get(wtyDeviceList[i].Imei.left(14)).Extended_Warranty_Contract_No__c     = wtyDeviceList[i].ExtWtyContractNo;                       
                            caseMap.get(wtyDeviceList[i].Imei.left(14)).Extended_Warranty_Information__c     = wtyDeviceList[i].ExtWtyInformation;

                            if(wtyDeviceList[i].ExtWtyStartDate != '- -' && wtyDeviceList[i].ExtWtyStartDate != '0000-00-00') caseMap.get(wtyDeviceList[i].Imei.left(14)).Extended_Warranty_Start_Date__c = Date.valueOf(wtyDeviceList[i].ExtWtyStartDate);
                            if(wtyDeviceList[i].ExtWtyEndDate != '- -' && wtyDeviceList[i].ExtWtyEndDate != '0000-00-00') caseMap.get(wtyDeviceList[i].Imei.left(14)).Extended_Warranty_End_Date__c = Date.valueOf(wtyDeviceList[i].ExtWtyEndDate);
                            
                            //ACT_DATE(1) -> GI_DATEM(2) -> PROD DATE(3)
                            if(wtyDeviceList[i].ProdDate != '- -' && wtyDeviceList[i].ProdDate != '0000-00-00') caseMap.get(wtyDeviceList[i].Imei.left(14)).Warranty_Start_Date__c = Date.valueOf(wtyDeviceList[i].ProdDate);
                            if(wtyDeviceList[i].GidateM != '- -' && wtyDeviceList[i].GidateM != '0000-00-00') caseMap.get(wtyDeviceList[i].Imei.left(14)).Warranty_Start_Date__c = Date.valueOf(wtyDeviceList[i].GidateM);
                            if(wtyDeviceList[i].ActDate != '- -' && wtyDeviceList[i].ActDate != '0000-00-00') caseMap.get(wtyDeviceList[i].Imei.left(14)).Warranty_Start_Date__c = Date.valueOf(wtyDeviceList[i].ActDate);

                            if(wtyDeviceList[i].WtyEndDate != '- -' && wtyDeviceList[i].WtyEndDate != '0000-00-00') caseMap.get(wtyDeviceList[i].Imei.left(14)).Warranty_End_Date__c = Date.valueOf(wtyDeviceList[i].WtyEndDate);
                            
                            if(wtyDeviceList[i].WtyFlag == 'OW') caseMap.get(wtyDeviceList[i].Imei.left(14)).Warranty_Status__c = 'Out of Warranty';
                            if(wtyDeviceList[i].WtyFlag == 'IW') caseMap.get(wtyDeviceList[i].Imei.left(14)).Warranty_Status__c = 'In Warranty';

                            if(caseMap.get(wtyDeviceList[i].Imei.left(14)).RecordType.DeveloperName =='Repair'){
                                caseMap.get(wtyDeviceList[i].Imei.left(14)).Model_Code_Repair__c = wtyDeviceList[i].Model; 
                                caseMap.get(wtyDeviceList[i].Imei.left(14)).Case_Device_Status__c = 'Verified';
                            }
                        }
            
                    }
                    else if(wtyDeviceList[i].Imei == null && wtyDeviceList[i].SerialNo != null && caseMap.get(wtyDeviceList[i].SerialNo) != null){
                        if(wtyDeviceList[i].Ret == '0'){

                            caseMap.get(wtyDeviceList[i].SerialNo).Extended_Warranty_Contract_Name__c   = wtyDeviceList[i].ExtWtyContractName;
                            caseMap.get(wtyDeviceList[i].SerialNo).Extended_Warranty_Contract_No__c     = wtyDeviceList[i].ExtWtyContractNo;                       
                            caseMap.get(wtyDeviceList[i].SerialNo).Extended_Warranty_Information__c     = wtyDeviceList[i].ExtWtyInformation;

                            if(wtyDeviceList[i].ExtWtyStartDate != '- -' && wtyDeviceList[i].ExtWtyStartDate != '0000-00-00') caseMap.get(wtyDeviceList[i].SerialNo).Extended_Warranty_Start_Date__c = Date.valueOf(wtyDeviceList[i].ExtWtyStartDate);
                            if(wtyDeviceList[i].ExtWtyEndDate != '- -' && wtyDeviceList[i].ExtWtyEndDate != '0000-00-00') caseMap.get(wtyDeviceList[i].SerialNo).Extended_Warranty_End_Date__c = Date.valueOf(wtyDeviceList[i].ExtWtyEndDate);
                            
                            //ACT_DATE(1) -> GI_DATEM(2) -> PROD DATE(3)
                            if(wtyDeviceList[i].ProdDate != '- -' && wtyDeviceList[i].ProdDate != '0000-00-00') caseMap.get(wtyDeviceList[i].SerialNo).Warranty_Start_Date__c = Date.valueOf(wtyDeviceList[i].ProdDate);
                            if(wtyDeviceList[i].GidateM != '- -' && wtyDeviceList[i].GidateM != '0000-00-00') caseMap.get(wtyDeviceList[i].SerialNo).Warranty_Start_Date__c = Date.valueOf(wtyDeviceList[i].GidateM);
                            if(wtyDeviceList[i].ActDate != '- -' && wtyDeviceList[i].ActDate != '0000-00-00') caseMap.get(wtyDeviceList[i].SerialNo).Warranty_Start_Date__c = Date.valueOf(wtyDeviceList[i].ActDate);

                            if(wtyDeviceList[i].WtyEndDate != '- -' && wtyDeviceList[i].WtyEndDate != '0000-00-00') caseMap.get(wtyDeviceList[i].SerialNo).Warranty_End_Date__c = Date.valueOf(wtyDeviceList[i].WtyEndDate);
                            
                            if(wtyDeviceList[i].WtyFlag == 'OW') caseMap.get(wtyDeviceList[i].SerialNo).Warranty_Status__c = 'Out of Warranty';
                            if(wtyDeviceList[i].WtyFlag == 'IW') caseMap.get(wtyDeviceList[i].SerialNo).Warranty_Status__c = 'In Warranty';

                            if(caseMap.get(wtyDeviceList[i].SerialNo).RecordType.DeveloperName =='Repair'){
                                caseMap.get(wtyDeviceList[i].SerialNo).Case_Device_Status__c = 'Verified';  
                            }
                        }
                    }else if(wtyDeviceList[i].Imei == null && wtyDeviceList[i].SerialNo == null && caseMap.get(wtyDeviceList[i].ExtWtyContractNo) != null){
                        
                            caseMap.get(wtyDeviceList[i].ExtWtyContractNo).Extended_Warranty_Contract_Name__c   = wtyDeviceList[i].ExtWtyContractName;
                            caseMap.get(wtyDeviceList[i].ExtWtyContractNo).Extended_Warranty_Contract_No__c     = wtyDeviceList[i].ExtWtyContractNo;                       
                            caseMap.get(wtyDeviceList[i].ExtWtyContractNo).Extended_Warranty_Information__c     = wtyDeviceList[i].ExtWtyInformation;

                            if(wtyDeviceList[i].ExtWtyStartDate != '- -' && wtyDeviceList[i].ExtWtyStartDate != '0000-00-00') caseMap.get(wtyDeviceList[i].ExtWtyContractNo).Extended_Warranty_Start_Date__c = Date.valueOf(wtyDeviceList[i].ExtWtyStartDate);
                            if(wtyDeviceList[i].ExtWtyEndDate != '- -' && wtyDeviceList[i].ExtWtyEndDate != '0000-00-00') caseMap.get(wtyDeviceList[i].ExtWtyContractNo).Extended_Warranty_End_Date__c = Date.valueOf(wtyDeviceList[i].ExtWtyEndDate);
                            
                            //ACT_DATE(1) -> GI_DATEM(2) -> PROD DATE(3)
                            if(wtyDeviceList[i].ProdDate != '- -' && wtyDeviceList[i].ProdDate != '0000-00-00') caseMap.get(wtyDeviceList[i].ExtWtyContractNo).Warranty_Start_Date__c = Date.valueOf(wtyDeviceList[i].ProdDate);
                            if(wtyDeviceList[i].GidateM != '- -' && wtyDeviceList[i].GidateM != '0000-00-00') caseMap.get(wtyDeviceList[i].ExtWtyContractNo).Warranty_Start_Date__c = Date.valueOf(wtyDeviceList[i].GidateM);
                            if(wtyDeviceList[i].ActDate != '- -' && wtyDeviceList[i].ActDate != '0000-00-00') caseMap.get(wtyDeviceList[i].ExtWtyContractNo).Warranty_Start_Date__c = Date.valueOf(wtyDeviceList[i].ActDate);

                            if(wtyDeviceList[i].WtyEndDate != '- -' && wtyDeviceList[i].WtyEndDate != '0000-00-00') caseMap.get(wtyDeviceList[i].ExtWtyContractNo).Warranty_End_Date__c = Date.valueOf(wtyDeviceList[i].WtyEndDate);
                            
                            if(wtyDeviceList[i].WtyFlag == 'OW') caseMap.get(wtyDeviceList[i].ExtWtyContractNo).Warranty_Status__c = 'Out of Warranty';
                            if(wtyDeviceList[i].WtyFlag == 'IW') caseMap.get(wtyDeviceList[i].ExtWtyContractNo).Warranty_Status__c = 'In Warranty';

                            if(caseMap.get(wtyDeviceList[i].ExtWtyContractNo).RecordType.DeveloperName =='Repair'){
                                caseMap.get(wtyDeviceList[i].ExtWtyContractNo).Case_Device_Status__c = 'Verified';  
                            }
                        
                    }
                }
                for(Case cs : caseMap.values()){
                    if(cs.Case_Device_Status__c != 'Verified' && cs.RecordType.DeveloperName =='Repair'){
                        cs.Case_Device_Status__c                = 'Unverified';
                        cs.Extended_Warranty_Contract_Name__c   = null;
                        cs.Extended_Warranty_Contract_No__c     = null;
                        cs.Extended_Warranty_End_Date__c        = null;
                        cs.Extended_Warranty_Information__c     = null;
                        cs.Extended_Warranty_Start_Date__c      = null;
                        cs.Warranty_Start_Date__c               = null;
                        cs.Warranty_End_Date__c                 = null;
                        cs.Warranty_Status__c                   = null;
                        if(cs.IMEI__c != null)
                        cs.Model_Code_Repair__c                 = null;

                    }
                }
                if(caseMap.size() > 0){
                    //system.debug('caseMap' + caseMap);
                    //update caseMap.values();
                    
                    Set<Case> unqset = new Set<Case>();
                    List<Case> unqlist = new List<Case>();
                    unqset.addAll(caseMap.values());
                    unqlist.addAll(unqset);
                    update unqlist;
                    result = 'Success';
                }
            }
            //result = 'Success';
        }
        //Child List Check
        else if(ca.Parent_Case__c == true && ca.ParentId == null){
            SVC_GCICWarrantyTermCheckCaseBatch ba = new SVC_GCICWarrantyTermCheckCaseBatch(ca.Id,null,true);
            Database.executeBatch(ba, 1);
            //Database.executeBatch(ba, 100);
            result = 'Warranty Term Check batch have been started for Child cases';
        }
        else{
            result = 'IMEI or Serial No is not valid.';
        }
        return result;
      
    }
    @future(callout = true)
    public static void toSend(Id caseId){
        Case ca  = [SELECT Id 
                            , IMEI__c
                            , SerialNumber__c
                            , Device_Type_f__c                      //formula
                            , IMEI_Number_Device__c                 //formula
                            , Serial_Number_Device__c               //formula
                            , RecordType.DeveloperName
                            , Device__c
                            , Device__r.Model_Name__c
                            , Case_Device_Status__c
                            , Parent_Case__c
                            , ParentId
                            , Warranty_Status__c
                            , Model_Code__c
                            , Model_Code_Repair__c
                            , Extended_Warranty_Contract_No__c
                            , Extended_Warranty_Start_Date__c
                            , Extended_Warranty_Contract_Name__c
                            , Extended_Warranty_End_Date__c
                            , Extended_Warranty_Information__c 
                    FROM Case 
                    WHERE id =: caseId];
        Map<String, Case> caseMap = new Map<String, Case>();

        //Request Header
        String layoutId = Integration_EndPoints__c.getInstance('SVC-06').layout_id__c;
        String partialEndpoint = Integration_EndPoints__c.getInstance('SVC-06').partial_endpoint__c;

        WtyCheckRequest WtyCheckReq = new WtyCheckRequest();
        SVC_GCICJSONRequestClass.WarrantyCheckJSON body = new SVC_GCICJSONRequestClass.WarrantyCheckJSON();
        cihStub.msgHeadersRequest headers = new cihStub.msgHeadersRequest(layoutId);
        body.wtyDeviceList = new List<SVC_GCICJSONRequestClass.wtyDevice>();

        SVC_GCICJSONRequestClass.wtyDevice wd = new SVC_GCICJSONRequestClass.wtyDevice();
        //wd.MSGGUID            = cihStub.giveGUID();
        //wd.IFID               = layoutId;
        //wd.IFDate         = String.valueOf(datetime.now().formatGMT('YYYYMMddHHmmss'));
        if(ca.RecordType.DeveloperName == 'Repair'){
            wd.I_IMEI           = ca.IMEI__c;
            wd.I_sn             = ca.SerialNumber__c;
            wd.I_model_code     = ca.Model_Code__c;
               wd.I_ext_wt_contract_no     = ca.Extended_Warranty_Contract_No__c;// Added By Vijay on 4/2/2019 to send Contract Number to GCIC
            if(wd.I_IMEI != null && wd.I_sn != null) wd.I_sn = null;
            if(ca.Device_Type_f__c == 'Connected') wd.I_device_type = 'IMEI';
            if(ca.Device_Type_f__c == 'Wifi') wd.I_device_type    = 'SN';
            if(ca.Extended_Warranty_Contract_No__c !=null) wd.I_device_type    = 'CNTR';
            //if(wd.I_IMEI != null) caseMap.put(ca.IMEI__c , ca);
            if(wd.I_IMEI != null) caseMap.put(ca.IMEI__c.left(14) , ca);
            if(wd.I_sn != null) caseMap.put(ca.SerialNumber__c , ca);
            if(wd.I_ext_wt_contract_no != null) caseMap.put(ca.Extended_Warranty_Contract_No__c , ca);
            
        }
        else if(ca.RecordType.DeveloperName == 'Exchange'){
            wd.I_IMEI           = ca.IMEI_Number_Device__c;
            wd.I_sn             = ca.Serial_Number_Device__c;
            wd.I_model_code     = ca.Model_Code__c;
            if(wd.I_IMEI != null && wd.I_sn != null) wd.I_sn = null;
            if(ca.Device_Type_f__c == 'Connected') wd.I_device_type = 'IMEI';
            if(ca.Device_Type_f__c == 'Wifi') wd.I_device_type = 'SN';
            //if(wd.I_IMEI != null) caseMap.put(ca.IMEI_Number_Device__c , ca);
            if(wd.I_IMEI != null) caseMap.put(ca.IMEI_Number_Device__c.left(14) , ca);
            if(wd.I_sn != null) caseMap.put(ca.Serial_Number_Device__c , ca);
        }
        body.wtyDeviceList.add(wd);

        List<wtyDevice> wtyDeviceList = new List<wtyDevice>();
        if(!body.wtyDeviceList.isEmpty()){
            WtyCheckReq.body = body;
            WtyCheckReq.inputHeaders = headers;
            string requestBody = json.serialize(WtyCheckReq);
            system.debug(requestBody);

            System.Httprequest req = new System.Httprequest();
            HttpResponse res = new HttpResponse();
            req.setMethod('POST');
            req.setBody(requestBody);
          
            req.setEndpoint('callout:X012_013_ROI'+partialEndpoint);
            //req.setEndpoint('callout:CIH_Production'+partialEndpoint);
            req.setTimeout(120000);
                
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('Accept', 'application/json');
        
            Http http = new Http();
            Datetime requestTime = system.now();   
            res = http.send(req);
            Datetime responseTime = system.now();

            
            if(res.getStatusCode() == 200){
                //success
                system.debug('res.getBody() : ' + res.getBody());
                Response response = (SVC_GCICWarrantyTermCheck.Response)JSON.deserialize(res.getBody(), SVC_GCICWarrantyTermCheck.Response.class);
                
                system.debug('response : ' + response);

                Message_Queue__c mq = setMessageQueue('Case', response, requestTime, responseTime);
                //mqList.add(mq);
                //insert mqList;
                insert mq;

                SVC_GCICWarrantyTermCheck.ResponseBody resbody = response.body;
                system.debug('resbody : ' + resbody);


                wtyDeviceList = resbody.EtOutput.item;
                system.debug('wtyDeviceList size : ' + wtyDeviceList.size());
                system.debug('wtyDeviceList : ' + wtyDeviceList);
            }
            else{
                //Fail
                MessageLog messageLog = new MessageLog();
                messageLog.body = 'ERROR : http status code = ' +  res.getStatusCode();
                messageLog.MSGGUID = WtyCheckReq.inputHeaders.MSGGUID;
                messageLog.IFID = WtyCheckReq.inputHeaders.IFID;
                messageLog.IFDate = WtyCheckReq.inputHeaders.IFDate;

                Message_Queue__c mq = setHttpQueue('Case', messageLog, requestTime, responseTime);
                //mqList.add(mq);
                //insert mqList;
                insert mq;
            }
        }
        if(!wtyDeviceList.isEmpty()){
            for(Integer i = 0;  i< wtyDeviceList.size(); i++){
                if(wtyDeviceList[i].Imei != null && caseMap.get(wtyDeviceList[i].Imei.left(14)) != null){
                    //RET == 0 or  RET == 1 ?
                    if(wtyDeviceList[i].Ret == '0'){
                        caseMap.get(wtyDeviceList[i].Imei.left(14)).Extended_Warranty_Contract_Name__c   = wtyDeviceList[i].ExtWtyContractName;
                        caseMap.get(wtyDeviceList[i].Imei.left(14)).Extended_Warranty_Contract_No__c     = wtyDeviceList[i].ExtWtyContractNo;                       
                        caseMap.get(wtyDeviceList[i].Imei.left(14)).Extended_Warranty_Information__c     = wtyDeviceList[i].ExtWtyInformation;

                        if(wtyDeviceList[i].ExtWtyStartDate != '- -' && wtyDeviceList[i].ExtWtyStartDate != '0000-00-00') caseMap.get(wtyDeviceList[i].Imei.left(14)).Extended_Warranty_Start_Date__c = Date.valueOf(wtyDeviceList[i].ExtWtyStartDate);
                        if(wtyDeviceList[i].ExtWtyEndDate != '- -' && wtyDeviceList[i].ExtWtyEndDate != '0000-00-00') caseMap.get(wtyDeviceList[i].Imei.left(14)).Extended_Warranty_End_Date__c = Date.valueOf(wtyDeviceList[i].ExtWtyEndDate);
                        
                        //ACT_DATE(1) -> GI_DATEM(2) -> PROD DATE(3)
                        if(wtyDeviceList[i].ProdDate != '- -' && wtyDeviceList[i].ProdDate != '0000-00-00') caseMap.get(wtyDeviceList[i].Imei.left(14)).Warranty_Start_Date__c = Date.valueOf(wtyDeviceList[i].ProdDate);
                        if(wtyDeviceList[i].GidateM != '- -' && wtyDeviceList[i].GidateM != '0000-00-00') caseMap.get(wtyDeviceList[i].Imei.left(14)).Warranty_Start_Date__c = Date.valueOf(wtyDeviceList[i].GidateM);
                        if(wtyDeviceList[i].ActDate != '- -' && wtyDeviceList[i].ActDate != '0000-00-00') caseMap.get(wtyDeviceList[i].Imei.left(14)).Warranty_Start_Date__c = Date.valueOf(wtyDeviceList[i].ActDate);

                        if(wtyDeviceList[i].WtyEndDate != '- -' && wtyDeviceList[i].WtyEndDate != '0000-00-00') caseMap.get(wtyDeviceList[i].Imei.left(14)).Warranty_End_Date__c = Date.valueOf(wtyDeviceList[i].WtyEndDate);
                        
                        if(wtyDeviceList[i].WtyFlag == 'OW') caseMap.get(wtyDeviceList[i].Imei.left(14)).Warranty_Status__c = 'Out of Warranty';
                        if(wtyDeviceList[i].WtyFlag == 'IW') caseMap.get(wtyDeviceList[i].Imei.left(14)).Warranty_Status__c = 'In Warranty';

                        if(caseMap.get(wtyDeviceList[i].Imei.left(14)).RecordType.DeveloperName =='Repair'){
                            caseMap.get(wtyDeviceList[i].Imei.left(14)).Model_Code_Repair__c = wtyDeviceList[i].Model; 
                            caseMap.get(wtyDeviceList[i].Imei.left(14)).Case_Device_Status__c = 'Verified';
                        }
                    }
        
                }
                else if(wtyDeviceList[i].Imei == null && wtyDeviceList[i].SerialNo != null && caseMap.get(wtyDeviceList[i].SerialNo) != null){
                    if(wtyDeviceList[i].Ret == '0'){

                        caseMap.get(wtyDeviceList[i].SerialNo).Extended_Warranty_Contract_Name__c   = wtyDeviceList[i].ExtWtyContractName;
                        caseMap.get(wtyDeviceList[i].SerialNo).Extended_Warranty_Contract_No__c     = wtyDeviceList[i].ExtWtyContractNo;                       
                        caseMap.get(wtyDeviceList[i].SerialNo).Extended_Warranty_Information__c     = wtyDeviceList[i].ExtWtyInformation;

                        if(wtyDeviceList[i].ExtWtyStartDate != '- -' && wtyDeviceList[i].ExtWtyStartDate != '0000-00-00') caseMap.get(wtyDeviceList[i].SerialNo).Extended_Warranty_Start_Date__c = Date.valueOf(wtyDeviceList[i].ExtWtyStartDate);
                        if(wtyDeviceList[i].ExtWtyEndDate != '- -' && wtyDeviceList[i].ExtWtyEndDate != '0000-00-00') caseMap.get(wtyDeviceList[i].SerialNo).Extended_Warranty_End_Date__c = Date.valueOf(wtyDeviceList[i].ExtWtyEndDate);
                        
                        //ACT_DATE(1) -> GI_DATEM(2) -> PROD DATE(3)
                        if(wtyDeviceList[i].ProdDate != '- -' && wtyDeviceList[i].ProdDate != '0000-00-00') caseMap.get(wtyDeviceList[i].SerialNo).Warranty_Start_Date__c = Date.valueOf(wtyDeviceList[i].ProdDate);
                        if(wtyDeviceList[i].GidateM != '- -' && wtyDeviceList[i].GidateM != '0000-00-00') caseMap.get(wtyDeviceList[i].SerialNo).Warranty_Start_Date__c = Date.valueOf(wtyDeviceList[i].GidateM);
                        if(wtyDeviceList[i].ActDate != '- -' && wtyDeviceList[i].ActDate != '0000-00-00') caseMap.get(wtyDeviceList[i].SerialNo).Warranty_Start_Date__c = Date.valueOf(wtyDeviceList[i].ActDate);

                        if(wtyDeviceList[i].WtyEndDate != '- -' && wtyDeviceList[i].WtyEndDate != '0000-00-00') caseMap.get(wtyDeviceList[i].SerialNo).Warranty_End_Date__c = Date.valueOf(wtyDeviceList[i].WtyEndDate);
                        
                        if(wtyDeviceList[i].WtyFlag == 'OW') caseMap.get(wtyDeviceList[i].SerialNo).Warranty_Status__c = 'Out of Warranty';
                        if(wtyDeviceList[i].WtyFlag == 'IW') caseMap.get(wtyDeviceList[i].SerialNo).Warranty_Status__c = 'In Warranty';

                        if(caseMap.get(wtyDeviceList[i].SerialNo).RecordType.DeveloperName =='Repair'){
                            caseMap.get(wtyDeviceList[i].SerialNo).Case_Device_Status__c = 'Verified';  
                        }
                    }
                }else if(wtyDeviceList[i].Imei == null && wtyDeviceList[i].SerialNo == null && caseMap.get(wtyDeviceList[i].ExtWtyContractNo) != null){
                        
                            caseMap.get(wtyDeviceList[i].ExtWtyContractNo).Extended_Warranty_Contract_Name__c   = wtyDeviceList[i].ExtWtyContractName;
                            caseMap.get(wtyDeviceList[i].ExtWtyContractNo).Extended_Warranty_Contract_No__c     = wtyDeviceList[i].ExtWtyContractNo;                       
                            caseMap.get(wtyDeviceList[i].ExtWtyContractNo).Extended_Warranty_Information__c     = wtyDeviceList[i].ExtWtyInformation;

                            if(wtyDeviceList[i].ExtWtyStartDate != '- -' && wtyDeviceList[i].ExtWtyStartDate != '0000-00-00') caseMap.get(wtyDeviceList[i].ExtWtyContractNo).Extended_Warranty_Start_Date__c = Date.valueOf(wtyDeviceList[i].ExtWtyStartDate);
                            if(wtyDeviceList[i].ExtWtyEndDate != '- -' && wtyDeviceList[i].ExtWtyEndDate != '0000-00-00') caseMap.get(wtyDeviceList[i].ExtWtyContractNo).Extended_Warranty_End_Date__c = Date.valueOf(wtyDeviceList[i].ExtWtyEndDate);
                            
                            //ACT_DATE(1) -> GI_DATEM(2) -> PROD DATE(3)
                            if(wtyDeviceList[i].ProdDate != '- -' && wtyDeviceList[i].ProdDate != '0000-00-00') caseMap.get(wtyDeviceList[i].ExtWtyContractNo).Warranty_Start_Date__c = Date.valueOf(wtyDeviceList[i].ProdDate);
                            if(wtyDeviceList[i].GidateM != '- -' && wtyDeviceList[i].GidateM != '0000-00-00') caseMap.get(wtyDeviceList[i].ExtWtyContractNo).Warranty_Start_Date__c = Date.valueOf(wtyDeviceList[i].GidateM);
                            if(wtyDeviceList[i].ActDate != '- -' && wtyDeviceList[i].ActDate != '0000-00-00') caseMap.get(wtyDeviceList[i].ExtWtyContractNo).Warranty_Start_Date__c = Date.valueOf(wtyDeviceList[i].ActDate);

                            if(wtyDeviceList[i].WtyEndDate != '- -' && wtyDeviceList[i].WtyEndDate != '0000-00-00') caseMap.get(wtyDeviceList[i].ExtWtyContractNo).Warranty_End_Date__c = Date.valueOf(wtyDeviceList[i].WtyEndDate);
                            
                            if(wtyDeviceList[i].WtyFlag == 'OW') caseMap.get(wtyDeviceList[i].ExtWtyContractNo).Warranty_Status__c = 'Out of Warranty';
                            if(wtyDeviceList[i].WtyFlag == 'IW') caseMap.get(wtyDeviceList[i].ExtWtyContractNo).Warranty_Status__c = 'In Warranty';

                            if(caseMap.get(wtyDeviceList[i].ExtWtyContractNo).RecordType.DeveloperName =='Repair'){
                                caseMap.get(wtyDeviceList[i].ExtWtyContractNo).Case_Device_Status__c = 'Verified';  
                            }
                        
                    }
            }
            for(Case cs : caseMap.values()){
                if(cs.Case_Device_Status__c != 'Verified' && cs.RecordType.DeveloperName =='Repair'){
                    cs.Case_Device_Status__c                = 'Unverified';
                    cs.Extended_Warranty_Contract_Name__c   = null;
                    cs.Extended_Warranty_Contract_No__c     = null;
                    cs.Extended_Warranty_End_Date__c        = null;
                    cs.Extended_Warranty_Information__c     = null;
                    cs.Extended_Warranty_Start_Date__c      = null;
                    cs.Warranty_Start_Date__c               = null;
                    cs.Warranty_End_Date__c                 = null;
                    cs.Warranty_Status__c                   = null;
                    if(cs.IMEI__c != null)
                    cs.Model_Code_Repair__c                 = null;

                }
            }
            if(caseMap.size() > 0){
                //system.debug('caseMap' + caseMap);
               // update caseMap.values();
                //result = 'Success';
                
                    
                    Set<Case> unqset = new Set<Case>();
                    List<Case> unqlist = new List<Case>();
                    unqset.addAll(caseMap.values());
                    unqlist.addAll(unqset);
                    update unqlist;
                   
     
            }
        }
        else{
            //result = 'There is no Case to update.';
            system.debug('Response NULL');
        }
        
    }
    public class WtyCheckRequest{
        public cihStub.msgHeadersRequest inputHeaders;        
        public SVC_GCICJSONRequestClass.WarrantyCheckJSON body;    
    }
    public class Response{
        cihStub.msgHeadersResponse inputHeaders;
        ResponseBody body;
    }
    public class ResponseBody{
        //List<item> ItInput;
        item EtOutput;
    }
    public class item{
        List<wtyDevice> item;
    }
    public class wtyDevice{
        public String Imei;
        public String SerialNo;
        public String Model;
        public String Gidate;
        public String SwVer;
        public String HwVer;
        public String PrlVer;
        public String Kunnr;
        public String KunnrDesc;
        public String Lfdate;
        public String GidateM;
        public String ProdDate;
        public String Ber;
        public String ActDate;
        public String WtyEndDate;
        public String WtyFlag;
        public String WtyChangeFlag;
        public String Ret;
        public String RetMsg;
        public String RetMat;
        public String ExtWtyContractNo;
        public String ExtWtyContractName;
        public String ExtWtyInformation;
        public String ExtWtyStartDate;
        public String ExtWtyEndDate;
    }
    public static Message_Queue__c setMessageQueue(String obj, Response response, Datetime requestTime, Datetime responseTime) {
        cihStub.msgHeadersResponse inputHeaders = response.inputHeaders;
        //.ResponseBody body = response.body;

        Message_Queue__c mq = new Message_Queue__c();

        mq.ERRORCODE__c = 'CIH :' + SVC_UtilityClass.nvl(inputHeaders.ERRORCODE) + ', GCIC : Multiple';
        mq.ERRORTEXT__c = 'CIH :' + SVC_UtilityClass.nvl(inputHeaders.ERRORTEXT) + ', GCIC : Multiple'; 
        mq.IFDate__c = inputHeaders.IFDate;
        mq.IFID__c = inputHeaders.IFID;
        mq.Identification_Text__c = 'Multiple';
        mq.Initalized_Request_Time__c = requestTime;//Datetime
        mq.Integration_Flow_Type__c = 'SVC-06'; // 003 Need to check code domain. Dev5 has 003 at all.
        mq.Is_Created__c = true;
        mq.MSGGUID__c = inputHeaders.MSGGUID;
        mq.MSGSTATUS__c = inputHeaders.MSGSTATUS;
        mq.MSGUID__c = inputHeaders.MSGGUID;
        mq.Object_Name__c = (obj == 'Case') ? 'Case' : 'Device';
        mq.Request_To_CIH_Time__c = requestTime.getTime();//Number(15, 0)
        mq.Response_Processing_Time__c = responseTime.getTime();//Number(15, 0)
        //mq.Status__c = (body.RET_CODE == '0') ? 'success' : 'failed';

        return mq;
    }
    // http error save.
    public static Message_Queue__c setHttpQueue(String obj, MessageLog messageLog, Datetime requestTime, Datetime responseTime) {
        Message_Queue__c mq             = new Message_Queue__c();
        mq.ERRORCODE__c                 = messageLog.body;
        mq.ERRORTEXT__c                 = messageLog.body;
        mq.IFDate__c                    = messageLog.IFDate;
        mq.IFID__c                      = messageLog.IFID;
        mq.Identification_Text__c       = 'Multiple';
        mq.Initalized_Request_Time__c   = requestTime;//Datetime
        mq.Integration_Flow_Type__c     = 'SVC-06';
        mq.Is_Created__c                = true;
        mq.MSGGUID__c                   = messageLog.MSGGUID;
        mq.MSGUID__c                    = messageLog.MSGGUID;
        //mq.MSGSTATUS__c = messageLog.MSGSTATUS;
        mq.Object_Name__c               = (obj == 'Case') ? 'Case' : 'Device';
        mq.Request_To_CIH_Time__c       = requestTime.getTime();//Number(15, 0)
        mq.Response_Processing_Time__c  = responseTime.getTime();//Number(15, 0)
        mq.Status__c                    = 'failed'; // picklist

        return mq;
    }

    public class MessageLog {
        public Integer status;
        public String body;
        public String MSGGUID;
        public String IFID;
        public String IFDate;
        public String MSGSTATUS;
        public String ERRORCODE;
        public String ERRORTEXT;
    }
}