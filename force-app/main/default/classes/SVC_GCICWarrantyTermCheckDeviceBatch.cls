/**
 * Created by ms on 2017-08-07.
 *
 * author : JeongHo.Lee, I2MAX
 */
//SVC_GCICWarrantyTermCheckDeviceBatch batch = new SVC_GCICWarrantyTermCheckDeviceBatch('IMEI' OR 'SN');
//Database.executeBatch(batch, 500); IMEI : 500, SN : 150
global without sharing class SVC_GCICWarrantyTermCheckDeviceBatch implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts {
    public Integer nT = 0;// Total
    public Integer nS = 0; // Succeed
    public Integer nSF = 0; // SFDC Failed
    public Integer nGF = 0; // GCIC Failed
    public String message ='';
    public List<Message_Queue__c> mqList;
    public Map<String, Device__c> devMap;
    public String dType;

    public SVC_GCICWarrantyTermCheckDeviceBatch(String str){
        dType = str;
    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        //system.debug('Warranty Term Check batch start');
        String query = '';
        if(dType == 'IMEI'){
            query += ' SELECT  Id, Name, IMEI__c, Serial_Number__c, Status__c, Model_Name__c, Device_Type_f__c, Purchase_Date__c ';
            query += ' FROM Device__c WHERE Status__c =' + '\'' + 'Valid' + '\' ';
            query += ' AND Device_Type_f__c=' + '\'' + 'Connected' + '\' ';
            query += ' AND IMEI__c != NULL';
        }
        else if(dType == 'SN'){
            query += ' SELECT  Id, Name, IMEI__c, Serial_Number__c, Status__c, Model_Name__c, Device_Type_f__c, Purchase_Date__c ';
            query += ' FROM Device__c WHERE Status__c =' + '\'' + 'Valid' + '\' ';
            query += ' AND Device_Type_f__c=' + '\'' + 'Wifi' + '\' ';
            query += ' AND Serial_Number__c != NULL '; 
            query += ' AND Model_Name__c != NULL ';  
        }
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Device__c> scope) {
        mqList = new List<Message_Queue__c>();
        devMap = new Map<String, Device__c>();

        //Request Header
        String layoutId = Integration_EndPoints__c.getInstance('SVC-06').layout_id__c;
        String partialEndpoint = Integration_EndPoints__c.getInstance('SVC-06').partial_endpoint__c;

        WtyCheckRequest WtyCheckReq = new WtyCheckRequest();
        SVC_GCICJSONRequestClass.WarrantyCheckJSON body = new SVC_GCICJSONRequestClass.WarrantyCheckJSON();
        cihStub.msgHeadersRequest headers = new cihStub.msgHeadersRequest(layoutId);
        body.wtyDeviceList = new List<SVC_GCICJSONRequestClass.wtyDevice>();

        for(Device__c dev : scope){
            nT++;
            SVC_GCICJSONRequestClass.wtyDevice wd = new SVC_GCICJSONRequestClass.wtyDevice();

            wd.I_IMEI           = dev.IMEI__c;
            wd.I_sn             = dev.Serial_Number__c;
            wd.I_model_code     = dev.Model_Name__c;
            if(wd.I_IMEI != null && wd.I_sn != null) wd.I_sn = null;
            if(dev.Device_Type_f__c == 'Connected') wd.I_device_type = 'IMEI';
            if(dev.Device_Type_f__c == 'Wifi') wd.I_device_type    = 'SN';
            
            //wd.I_so_no      = null;
            //wd.I_purchased_date = null;
            //wd.I_POSTING_DATE   = null;
            //wd.I_REDO_FLAG      = null;
            //wd.I_DELIVERY_DATE  = null;
            body.wtyDeviceList.add(wd);
            if(wd.I_IMEI != null) devMap.put(dev.IMEI__c.left(14) , dev);
            if(wd.I_sn != null) devMap.put(dev.Serial_Number__c , dev);
        }
        //Request body
        WtyCheckReq.body = body;
        WtyCheckReq.inputHeaders = headers;
        string requestBody = json.serialize(WtyCheckReq);
        system.debug(requestBody);

        System.Httprequest req = new System.Httprequest();
        HttpResponse res = new HttpResponse();
        req.setMethod('POST');
        req.setBody(requestBody);
      
        req.setEndpoint('callout:X012_013_ROI'+partialEndpoint);
        //req.setEndpoint('callout:SVC_CIH_Eendpoint'+partialEndpoint);
        
        req.setTimeout(120000);
            
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Accept', 'application/json');
    
        Http http = new Http();
        Datetime requestTime = system.now();   
        res = http.send(req);
        Datetime responseTime = system.now();

        List<wtyDevice> wtyDeviceList = new List<wtyDevice>();
        if(res.getStatusCode() == 200){
            //success
            system.debug('res.getBody() : ' + res.getBody());
            Response response = (SVC_GCICWarrantyTermCheckDeviceBatch.Response)JSON.deserialize(res.getBody(), SVC_GCICWarrantyTermCheckDeviceBatch.Response.class);
            
            system.debug('response : ' + response);
        
            Message_Queue__c mq = setMessageQueue(response, requestTime, responseTime);
            mqList.add(mq);

            SVC_GCICWarrantyTermCheckDeviceBatch.ResponseBody resbody = response.body;
            system.debug('resbody : ' + resbody);

            wtyDeviceList = resbody.EtOutput.item;
            //system.debug('wtyDeviceList size : ' + wtyDeviceList.size());
            system.debug('wtyDeviceList : ' + wtyDeviceList);
        }
        else{
            //Fail
            MessageLog messageLog = new MessageLog();
            messageLog.body = 'ERROR : http status code = ' +  res.getStatusCode();
            messageLog.MSGGUID = WtyCheckReq.inputHeaders.MSGGUID;
            messageLog.IFID = WtyCheckReq.inputHeaders.IFID;
            messageLog.IFDate = WtyCheckReq.inputHeaders.IFDate;

            Message_Queue__c mq = setHttpQueue(messageLog, requestTime, responseTime);
            mqList.add(mq);         
        }
        //system.debug('devMap0 : ' + devMap);
        //system.debug('wtyList size : ' + wtyDeviceList.size());
        for(Integer i = 0;  i< wtyDeviceList.size(); i++){
            system.debug('IMEI : ' + wtyDeviceList[i].Imei);
            if(wtyDeviceList[i].Imei != null && devMap.get(wtyDeviceList[i].Imei.left(14)) != null){
                //RET == 0 or  RET == 1 ?
                if(wtyDeviceList[i].Ret == '0'){
                    devMap.get(wtyDeviceList[i].Imei.left(14)).Status__c = 'Verified';
                    devMap.get(wtyDeviceList[i].Imei.left(14)).Model_Name__c = wtyDeviceList[i].Model;
                    nS++;
                    //system.debug('IMEI RET : 0');
                }
            }
            else if(wtyDeviceList[i].Imei == null && wtyDeviceList[i].SerialNo != null && devMap.get(wtyDeviceList[i].SerialNo) != null){
                if(wtyDeviceList[i].Ret == '0'){
                    devMap.get(wtyDeviceList[i].SerialNo).Status__c = 'Verified';
                    nS++;
                    //system.debug('SN RET : 0');
                }
            }
        }
        system.debug('devMap1 : ' + devMap);
        for(Device__c de : devMap.values()){
            if(de.Status__c != 'Verified'){
                de.Status__c = 'Unverified';
                if(de.IMEI__c != null) de.Model_Name__c = null;
                nGF++;
                if(de.Device_Type_f__c == 'Connected' && nGF < 100 ) message += de.Name + ' : Invalid ESN (IMEI) : '+de.IMEI__c+'\r\n';
                else if(de.Device_Type_f__c == 'Wifi' && nGF < 100)  message += de.Name + ' : Invalid serial no : '+de.Serial_Number__c+'\r\n';
            }
        }
        //system.debug('devMap2 : ' + devMap);
        if(devMap.size() > 0){
            update devMap.values();
        }
        //system.debug('devMap3 : ' + devMap);
        if(!mqList.isEmpty()){
            insert mqList;
        }
    }
    global void finish(Database.BatchableContext BC) {
        system.debug('finish Call.');

        String title = 'Device Warranty term Check Batch Result';
        if (nSF + nGF > 0) {
            String summary = title + '\r\n\r\nTotal : ' + nT + ', Succeed : ' + nS + ', SFDC Failed : ' + nSF + ', GCIC Failed : ' + nGF;
            String body = summary + '\r\n\r\n==Failed list==\r\n' + message;
            SVC_UtilityClass.sendEmail(title, title, body);
        }
    }
    public class WtyCheckRequest{
        public cihStub.msgHeadersRequest inputHeaders;        
        public SVC_GCICJSONRequestClass.WarrantyCheckJSON body;    
    }

    public class Response{
        cihStub.msgHeadersResponse inputHeaders;
        ResponseBody body;
    }
    public class ResponseBody{
        //List<item> ItInput;
        item EtOutput;
    }
    public class item{
        List<wtyDevice> item;
    }
    public class wtyDevice{
        public String Imei;
        public String SerialNo;
        public String Model;
        public String Gidate;
        public String SwVer;
        public String HwVer;
        public String PrlVer;
        public String Kunnr;
        public String KunnrDesc;
        public String Lfdate;
        public String GidateM;
        public String ProdDate;
        public String Ber;
        public String ActDate;
        public String WtyEndDate;
        public String WtyFlag;
        public String WtyChangeFlag;
        public String Ret;
        public String RetMsg;
        public String RetMat;
        public String ExtWtyContractNo;
        public String ExtWtyContractName;
        public String ExtWtyInformation;
        public String ExtWtyStartDate;
        public String ExtWtyEndDate;
     }
     public static Message_Queue__c setMessageQueue(Response response, Datetime requestTime, Datetime responseTime) {
        cihStub.msgHeadersResponse inputHeaders = response.inputHeaders;
        //SVC_GCICWarrantyTermCheckDeviceBatch.ResponseBody body = response.body;

        Message_Queue__c mq = new Message_Queue__c();

        mq.ERRORCODE__c = 'CIH :' + SVC_UtilityClass.nvl(inputHeaders.ERRORCODE) + ', GCIC : Multiple';
        mq.ERRORTEXT__c = 'CIH :' + SVC_UtilityClass.nvl(inputHeaders.ERRORTEXT) + ', GCIC : Multiple';
        mq.IFDate__c = inputHeaders.IFDate;
        mq.IFID__c = inputHeaders.IFID;
        mq.Identification_Text__c = 'Multiple';
        mq.Initalized_Request_Time__c = requestTime;//Datetime
        mq.Integration_Flow_Type__c = 'SVC-06';
        mq.Is_Created__c = true;
        mq.MSGGUID__c = inputHeaders.MSGGUID;
        mq.MSGSTATUS__c = inputHeaders.MSGSTATUS;
        mq.MSGUID__c = inputHeaders.MSGGUID;
        mq.Object_Name__c = 'Device';
        mq.Request_To_CIH_Time__c = requestTime.getTime();//Number(15, 0)
        mq.Response_Processing_Time__c = responseTime.getTime();//Number(15, 0)
        //mq.Status__c = (body.item[0].RET_CODE == '0') ? 'success' : 'failed';

        return mq;
    }

    // http error save.
    public static Message_Queue__c setHttpQueue(MessageLog messageLog, Datetime requestTime, Datetime responseTime) {
        Message_Queue__c mq             = new Message_Queue__c();
        mq.ERRORCODE__c                 = messageLog.body;
        mq.ERRORTEXT__c                 = messageLog.body;
        mq.IFDate__c                    = messageLog.IFDate;
        mq.IFID__c                      = messageLog.IFID;
        mq.Identification_Text__c       = 'Multiple';
        mq.Initalized_Request_Time__c   = requestTime;//Datetime
        mq.Integration_Flow_Type__c     = 'SVC-06';
        mq.Is_Created__c                = true;
        mq.MSGGUID__c                   = messageLog.MSGGUID;
        mq.MSGUID__c                    = messageLog.MSGGUID;
        //mq.MSGSTATUS__c = messageLog.MSGSTATUS;
        mq.Object_Name__c               = 'Device';
        mq.Request_To_CIH_Time__c       = requestTime.getTime();//Number(15, 0)
        mq.Response_Processing_Time__c  = responseTime.getTime();//Number(15, 0)
        mq.Status__c                    = 'failed'; // picklist

        return mq;
    }

    public class MessageLog {
        public Integer status;
        public String body;
        public String MSGGUID;
        public String IFID;
        public String IFDate;
        public String MSGSTATUS;
        public String ERRORCODE;
        public String ERRORTEXT;
    }
}