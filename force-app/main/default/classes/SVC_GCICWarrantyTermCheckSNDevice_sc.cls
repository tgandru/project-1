/**
 * Created by ms on 2017-08-10.
 * author : JeongHo.Lee, I2MAX
String CRON_EXP = '0 0 0,4,8,12,16,20 ? * *';// not decision
*/
public with sharing class SVC_GCICWarrantyTermCheckSNDevice_sc implements Schedulable {
	public void execute(SchedulableContext sc) {
        SVC_GCICWarrantyTermCheckDeviceBatch batch = new SVC_GCICWarrantyTermCheckDeviceBatch('SN');
        //Database.executeBatch(batch, 150);
        Database.executeBatch(batch, 20);
    }
}