public with sharing class SVC_GenerateShippingLabelController {

	private Case c;
    private Boolean err;

    public SVC_GenerateShippingLabelController(ApexPages.StandardController stdController) {
        this.c = (Case)stdController.getRecord();
        this.c = [
                SELECT
                        Id,
                        status,
                        Parent_Case__c,
                        RecordType.Name,
                        RecordType.DeveloperName,
                        ParentId,
                        Case_Device_Status__c,
                        Device_Type_f__c,
                        CaseNumber,
                        Level_1__c,
                        Level2SymptomCode__c,
                        Level_3_Symptom_Code__r.Name,
                        Symptom_Type__c,
                        Description,
                        Exchange_Type__c,
                        Exchange_Reason__c,
                        Device__r.Purchase_Date__c,
                        Device__r.IMEI__C,
                        Device__r.Serial_Number__c,
                        Device__r.Status__c,
                        Device__r.Model_Name__c,
                        Contact.BP_Number__c,
                        Contact.FirstName,
                        Contact.LastName,
                        Contact.Fax,
                        Contact.Phone,
                        Contact.MailingStreet,
                        Contact.MailingCity,
                        Contact.MailingPostalCode,
                        Contact.MailingState,
                        Service_Order_Number__c,
                        ASC_Code__c,
                        Inbound_Tracking_Number__c,
                        Outbound_Tracking_Number__c,
                        isClosed
                FROM Case WHERE Id = :c.id
        ];

        err = false;
    }

    public PageReference GenerateShippingLabel() {

        String errMsg = '';

        if(c.RecordType.DeveloperName != 'Exchange' && c.RecordType.DeveloperName != 'Repair') errMsg = 'Shipping label can only be generated for Exchange and Repair cases.';
        else if (c.RecordType.DeveloperName == 'Exchange' && c.Outbound_Tracking_Number__c == null) errMsg = 'Shipping label cannot be generated for this case. Exchange cases must have an Outbound Tracking Number in order to generate a shipping label.';
        else if (c.RecordType.DeveloperName == 'Repair' && c.Inbound_Tracking_Number__c == null && c.Outbound_Tracking_Number__c == null) errMsg = 'Shipping label cannot be generated for this case. Repair cases must have an Inbound Tracking Number or Outbound Tracking Number in order to generate a shipping label.';

        if(errMsg != ''){
            err = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errMsg));
            return null;
        }

        Message_queue__c mq = new message_queue__c(Integration_Flow_Type__c = 'IF_SVC_08', retry_counter__c = 0, Identification_Text__c = c.id, Object_Name__c='Case');
        callShippingLabelEndpoint(mq,c);
        if (mq.status__c =='success' ){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Shipping Label request was successfully sent.'));
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Shipping Label request ' + mq.status__c + ' Error: ' + mq.ERRORTEXT__c + ' Error Code: '+mq.ERRORCODE__c));
        }

        return null;
    }

    private static void callShippingLabelEndpoint(message_queue__c mq, Case c){

        cihStub.msgHeadersRequest cih = new cihStub.msgHeadersRequest('IF_SVC_08');

        request shp = new request();
        inputHeaders ih = new inputHeaders();
        requestBody bd = new requestBody();
        ih.MSGGUID = cih.MSGGUID;
        ih.IFID = cih.IFID;
        ih.IFDate = cih.IFDate;
        bd.IV_OBJECT_ID = c.Service_Order_Number__c == null ? '' : c.Service_Order_Number__c;
        bd.IV_COMPANY = c.Device_Type_f__c == 'WiFi' ? 'C310' : 'C370';
        bd.IV_ASC_CODE = c.ASC_Code__c == null ? '' : c.ASC_Code__c;
        shp.inputHeaders = ih;
        shp.body = bd;

        String reqBody = JSON.serialize(shp);
        System.debug('@@@ JSON Body: ' + reqBody);

        string response = '';
        string errorMessage = '';
         try{
            response = webcallout(reqBody);
            system.debug(' Shipping label response' + response);
            if(response.length() > 6000000) {
                errorMessage = 'Response limit exceeded maximum allowed limit of 6000000 characters';
                handleError(mq, null, null, errorMessage);
            }
        }   catch(exception ex){
            System.debug('Shipping label  ERROR IN RESPONSE ');
            system.debug(ex.getmessage()); 
            handleError(mq, null, null, null);
        }

        if(string.isNotBlank(response)){ 
            shippingLabelRes res = (shippingLabelRes) json.deserialize(response, shippingLabelRes.class);
            
            system.debug('Shipping label  stubresponse ' + res);
            if(res.inputHeaders!=null){
                if(res.inputHeaders.MSGSTATUS == 'S'){
                    handleSuccess(mq, res);
                } else {
                    handleError(mq, res, null, null);
                }
            }else{
                handleError(mq, res, null, null);
            }
        } else {
            handleError(mq, null, null, null);
        }

    }

    public class shippingLabelRes{
        public responseHeader inputHeaders {get;set;}      
        public responseBody body {get;set;}      
    }

    public class responseHeader {
        public string MSGGUID {get;set;} // GUID for message
        public string IFID {get;set;} // interface id, created automatically, so need logic
        public string IFDate {get;set;} // interface date
        public string MSGSTATUS {get;set;} // start, success, failure (ST , S, F)
        public string ERRORCODE {get;set;} // error code
        public string ERRORTEXT {get;set;} // error message
    }

    public class responseBody {
        public string EV_RECEIPT_URL {get;set;}
        public string EV_RET_CODE {get;set;}
        public string EV_RET_MSG {get;set;}
        public string EV_TR_NO {get;set;}
        public string EV_UPS_URL {get;set;}
    }

    public class request {
        inputHeaders inputHeaders;
        requestBody body;
    }

    public class inputHeaders {
        String MSGGUID;
        String IFID;
        String IFDate;
    }

    public class requestBody {
        String IV_OBJECT_ID;
        String IV_COMPANY;
        String IV_ASC_CODE;
    }

    public static String webcallout(String body){

        String partialEndpoint = Integration_EndPoints__c.getInstance('SVC-08').partial_endpoint__c;
        //make the rest callout
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();

        req.setHeader('Accept', 'application/json');
        req.setHeader('Content-Type', 'application/json');
        req.setTimeout(120000);
        req.setMethod('POST');
        req.setBody(body);
        req.setEndpoint('callout:X012_013_ROI'+partialEndpoint);

        String result = '';
        try{
            res = h.send(req);
            result = res.getBody();

            if(res.getStatusCode() != 200) throw new CalloutException();
        }catch(CalloutException ex){
            System.debug('Result Status and Code: ' + res.toString() + '. System Exception: ' + ex.getMessage() + ' on line number ' + ex.getLineNumber());
        }

        System.debug('@@@ Response' + res);
        system.debug('@@@ Response body' + res.getBody());
        System.debug('@@@ Response Body Size ' + res.getBody().length());

        return result;
    }

    public static void handleSuccess(message_queue__c mq, shippingLabelRes res){
        mq.status__c = 'success';
        mq.MSGSTATUS__c = res.inputHeaders.MSGSTATUS;
        mq.MSGUID__c = res.inputHeaders.msgGUID;
        mq.IFID__c = res.inputHeaders.ifID;
        mq.IFDate__c = res.inputHeaders.ifDate;      
        mq.ERRORCODE__c=null;
        mq.ERRORTEXT__c=null;      
        upsert mq;

        System.debug('Receipt URL: ' + res.body.EV_RECEIPT_URL + ' Return Code: ' + res.body.EV_RET_CODE + ' Return Message: ' + res.body.EV_RET_MSG + ' Return Tracking Number: ' + res.body.EV_TR_NO + ' UPS URL: ' + res.body.EV_UPS_URL);

    }

    public static void handleError(message_queue__c mq, shippingLabelRes res, String fields, String errorMessage){
        mq.status__c = 'failed';
        if(res!= null && res.inputHeaders!=null){
            mq.ERRORTEXT__c=res.inputHeaders.ERRORTEXT;
            mq.ERRORCODE__c=res.inputHeaders.ERRORCODE;
        }
        if(fields!=null){
            if(mq.errortext__c!=null){
                mq.ERRORTEXT__c+='There are fields missing from the call out, listed below '+fields+'. Please fix this and try again.';
            }else{
                mq.ERRORTEXT__c='There are fields missing from the call out, listed below '+fields+'. Please fix this and try again.';
            }
        }

        if(errorMessage != null) {
            mq.ERRORTEXT__c = mq.ERRORTEXT__c != null ? mq.ERRORTEXT__c += errorMessage : errorMessage;
        }
        upsert mq;      
        
    }
}