/**
 * Created by ryan on 8/1/2017.
 */

@IsTest
private class SVC_GenerateShippingLabelController_Test {

    static Case cse;

    @testSetup static void testData(){
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        //get record type ids for account
        
        Id deviceRT = Schema.SObjectType.Device__c.getRecordTypeInfosByName().get('Edit Device').getRecordTypeId();
      
        Id acctRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Direct').getRecordTypeId();
        Id caseRT = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Exchange').getRecordTypeId();
        Id prodRT = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('B2B').getRecordTypeId();
        BusinessHours bh12 = [SELECT Id FROM BusinessHours WHERE name = 'B2B Service Hours 12x5'];

        Account acc = new Account(Name = 'Test Account', RecordTypeId = acctRT, Type = 'Distributor');
        insert acc;
        Contact con = new Contact(LastName = 'Test Contact', Email = 'test@email.com', Account = acc, Named_Caller__c = true, firstName='Test Con',MailingState = 'NJ',MailingCountry = 'US');
        insert con;
        RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];
        Product2 prod1 = new Product2(name ='MI-test', SAP_Material_ID__c = 'MI-test', sc_service_type__c = 'Incident', sc_service_type_ranking__c = '6', RecordTypeId = prodRT);
        insert prod1;
        Asset ast = new Asset(name ='Assert-MI-Test', AccountId = acc.id, Product2Id = prod1.id);
        insert ast;
        Slaprocess sla = [select id from SlaProcess where name like 'Enhanc%' and isActive = true limit 1 ];
        Entitlement ent = new Entitlement(Name ='Test Entitlement', AccountId=acc.id, assetid = ast.id, BusinessHoursId = bh12.id, StartDate = System.today().addDays(-14), EndDate = System.today().addDays(14), slaprocessid = sla.id, Units_Allowed__c = 10, Units_Exchanged__c = 0);
        insert ent;
        Device__c dev1 = new Device__c(RecordTypeId=deviceRT,Account__c = acc.Id, IMEI__c = '7259863232145', Status__c = 'Verified', Entitlement__c = ent.Id);
        insert dev1;
        cse = new Case(AccountId = acc.Id, ContactId = con.Id, Subject = 'Test Case', Origin = 'Web', Severity__c = '4-Low', EntitlementId = ent.Id, Service_Order_Number__c = '4100167438', Status = 'In Progress', RecordTypeId = caseRT, Parent_Case__c = true, Inbound_Tracking_Number__c = '9999 9999 9999', Outbound_Tracking_Number__c = '1111 1111 1111');
        insert cse;
        Integration_EndPoints__c ie = new Integration_EndPoints__c(Name = 'SVC-08', endPointURL__c = 'www.samsung.com', layout_id__c = 'this', partial_endpoint__c = '/test');
        insert ie;
    }

    static testMethod void testShippingLabelControllerSuccess(){
        cse = [SELECT Id, Status FROM Case WHERE Subject = 'Test Case' LIMIT 1];
        System.assertEquals(cse.Status, 'In Progress');
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new shippinglabelSUCCESS200());
            ApexPages.StandardController sc = new ApexPages.StandardController(cse);
            SVC_GenerateShippingLabelController pageCont = new SVC_GenerateShippingLabelController(sc);
            PageReference pageRef = Page.svc_generateShippingLabel;
            pageRef.getParameters().put('id',cse.Id);
            Test.setCurrentPage(pageRef);

            pageCont.GenerateShippingLabel();

        Test.stopTest();
    }

    static testMethod void testShippingLabelControllerError(){
        Id caseRT = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Repair').getRecordTypeId();
        cse = [SELECT Id, Status, RecordTypeId FROM Case WHERE Subject = 'Test Case' LIMIT 1];
        cse.RecordTypeId = caseRT;
        try{
            update cse;
        }catch(DmlException e){
            System.debug(e.getMessage());
        }

        System.assertEquals(cse.Status, 'In Progress');
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new shippinglabelERROR200());
            ApexPages.StandardController sc = new ApexPages.StandardController(cse);
            SVC_GenerateShippingLabelController pageCont = new SVC_GenerateShippingLabelController(sc);
            PageReference pageRef = Page.svc_generateShippingLabel;
            pageRef.getParameters().put('id',cse.Id);
        Test.setCurrentPage(pageRef);

        pageCont.GenerateShippingLabel();

        Test.stopTest();
    }

    class shippinglabelSUCCESS200 implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            String jsonBody = '{"inputHeaders":{"MSGGUID":"b75b4e43-d054-f84b-edd8-3d77c321b02d","IFID":"IF_SVC_08","IFDate":"20170803000647","MSGSTATUS":"S","ERRORTEXT":""},"body":{"EV_RECEIPT_URL":"www.testreceipt.com","EV_RET_CODE":"0","EV_RET_MSG":"OK","EV_TR_NO":"001","EV_UPS_URL":"www.test.com"}}';
            //String jsonBody = '{"inputHeaders":{"MSGGUID":"ada747b2-dda9-1931-acb2-14a2872d4358","IFID":"IF_ASC_Lookup_SVC_GCIC JSON","IFDate":"160317","MSGSTATUS":"S","ERRORTEXT":"Something wrong","ERRORCODE":"Err 001"},"body":{"ZifSfdcTicketCreateResponse":{"EV_CONSUMER":"","EV_OBJECT_ID":"SO-00001","EV_RET_CODE":"0","EV_RET_MSG":"","EV_WA_MSG":"","EV_WTY_STATUS":""}}}';
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody(jsonBody);
            res.setStatusCode(200);
            return res;
        }
    }

    class shippinglabelERROR200 implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            String jsonBody = '{"inputHeaders":{"MSGGUID":"b75b4e43-d054-f84b-edd8-3d77c321b02d","IFID":"IF_SVC_08","IFDate":"20170803000647","MSGSTATUS":"E","ERRORTEXT":"Something wrong", "ERRORCODE":"Err 001"},"body":{"EV_RECEIPT_URL":"www.testreceipt.com","EV_RET_CODE":"0","EV_RET_MSG":"OK","EV_TR_NO":"001","EV_UPS_URL":"www.test.com"}}';
            //String jsonBody = '{"inputHeaders":{"MSGGUID":"ada747b2-dda9-1931-acb2-14a2872d4358","IFID":"IF_ASC_Lookup_SVC_GCIC JSON","IFDate":"160317","MSGSTATUS":"S","ERRORTEXT":"Something wrong","ERRORCODE":"Err 001"},"body":{"ZifSfdcTicketCreateResponse":{"EV_CONSUMER":"","EV_OBJECT_ID":"SO-00001","EV_RET_CODE":"0","EV_RET_MSG":"","EV_WA_MSG":"","EV_WTY_STATUS":""}}}';
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody(jsonBody);
            res.setStatusCode(200);
            return res;
        }
    }
}