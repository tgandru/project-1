public class SVC_IMEICheckSumValidationClass {

	public static String validation(String str){
		String result = '';
		String subResult = '';
		//string imei_str =string.valueof(a.IMEI_Number__c);
		string imei_str = str;
		
		if(imei_str.length() < 14 || imei_str.length() > 15 || !imei_str.isNumeric()){
			result = 'Invalid';
			subResult = 'IMEI validation failed';
		}
		else if (imei_str.length() != 15){
			//a.Status = 'Validated';
			result = 'Valid';
			subResult ='';
		} 
		else {    //checksum

			Integer sum = 0;
			for (Integer i = 0;i < imei_str.length()-1; i++) {
				//System.debug(Math.mod(i,2));
				//System.debug(imei_str.substring(i,i+1));
				if (Math.mod(i,2) == 1){
					String s = String.valueof(integer.valueof(imei_str.substring(i,i+1))*2);
					//system.debug(s);
					if (s.length()==2){
						sum += integer.valueof(s.substring(0,1)) + integer.valueof(s.substring(1,2));
					}
					else{
						sum += integer.valueof(s);
					}
				}
				else{
					sum += integer.valueof(imei_str.substring(i,i+1));
				}
			}
			//system.debug(Math.Mod(sum,10));
			if ((10 - Math.Mod(sum,10) == integer.valueof(imei_str.substring(14,15))) 
			||
			(10 - Math.Mod(sum,10)==10 && integer.valueof(imei_str.substring(14,15))==0 )
			){ //checksum passed

				//Integer imeiExists = [Select count() From Device__c Where ID !=:a.id and IMEI__c =: a.IMEI__c and Is_Active__c = 'Yes'];


			//if (imeiExists == 0 && a.Entitlement__r.id != null){
			//  imeiExists = [Select count() From Device__c where IMEI__c =: a.IMEI__c and Entitlement__r.id =: a.Entitlement__r.id ];
			//  system.debug('ContractLineItem ' + imeiExists);
			//}

				/*if (imeiExists > 0 ){
					//a.Status = 'Invalid';
					result = 'Invalid';
					a.Verification_Failure_Reason__c = 'Asset duplication check failed';
				}*/
				//else{

					result = 'Valid';
					subResult='';

					//account match check

					/*if (a.entitlement__r.accountid != null){

						if (a.entitlement__r.accountid != a.account__r.id ){
							newStatus = 'Invalid';
							a.Verification_Failure_Reason__c=' Account on device & Entitlement Account match failed';
						}
					}*/
				//}
			}
			else{
				//a.Status = 'Invalid';
				result = 'Invalid';
				subResult = 'IMEI validation failed';
			}
		}

		/*if (a.Verification_Failure_Reason__c !=''){
			a.Status__c = newStatus;
			a.RecordTypeid = RecTypeInvalid;
		}
		else{
			if (a.Status__c == 'Invalid'){
				a.Status__c = 'Validated';
			}
			a.RecordTypeid = RecTypeValidated;
		}*/
	return result;
	}
}