public with sharing class SVC_ManageChildCases {
    public Boolean showMassEditForm{get;set;}
    public Boolean showResultForm{get;set;}
    
    Public Integer noOfSelected{get; set;}
    Public Map<ID,ID> selectedCaseMap = new Map<ID,ID>();

    public List<ChildCaseWrapper> selectedCaseWrapperList {
      get {
          selectedCaseWrapperList = new List<ChildCaseWrapper>();
            for (ChildCaseWrapper c:childCases){
                if (selectedCaseMap.containsKey(c.childCase.id)){
                    selectedCaseWrapperList.add(c);
                }
            }

            return selectedCaseWrapperList;
        }
        set;
    }

    public string caseId {
        get{
            if (caseId == null){
                caseId = ApexPages.currentPage().getParameters().get('caseId');
            }
        return caseId;

        }

        set;}

    public List<Case> selectedCaselist {
        get {
            selectedCaselist = 
            [select id,casenumber from case where id in:selectedCaseMap.keySet()];
            /*selectedCaselist = new List<Case>();
            for (ChildCaseWrapper c:childCases){
                if (c.selected){
                    selectedCaselist.add(c.childCase);
                }
            }*/

            return selectedCaselist;
        }
        set;
    }

    private String parentStatus {get;set;}
     private final Case parentCase;
    private String soql {get;set;}
    private String soqlCommon {
        get {
            if (soqlCommon == null){
                soqlCommon = 'select ID, casenumber, subject, status,Integration_Status__c,Device_Type_f__c,Model_Code_Repair__c,Model_Code__c,';
                soqlCommon +='Outbound_Shipping_Status__c, Inbound_Shipping_Status__c,device__r.Status__c,';
                soqlCommon +='device__c,device__r.name,device__r.IMEI__c,IMEI__c,SerialNumber__c, Case_Device_Status__c,';
                soqlCommon += 'device__r.Serial_Number__c,Model_Code_GCIC__c,recordtype.name ';
                soqlCommon += 'from case ';
            }
        return soqlCommon;
        }

    set;}

    private String exchangeRecTypeId{
      get{
        if (exchangeRecTypeId == null){
           exchangeRecTypeId = [select id,sobjecttype from recordtype where sobjecttype='Case' and name='Exchange' limit 1].id;
     
        }
        return exchangeRecTypeId;
      }
      set;
    }

    private Boolean IsparentExchange {get;set;}
    public List<ChildCaseWrapper> childCases {get;set;}
    public Boolean showConfirmationMsg {get; set;}

    public Boolean showRecordWarningMsg {get; set;}
    

    private transient Integer validCaseCnt = 0;
    private transient Integer invalidCaseCnt = 0;

    /*private String soqlExchange {
        get{
            if (soqlExchange == null){
                soqlExchange = 'select ID, casenumber, subject, status,Integration_Status__c,Device_Type_f__c,Model_Code_Repair__c,Model_Code__c,';
                soqlExchange +='Outbound_Shipping_Status__c, Inbound_Shipping_Status__c,device__r.Status__c,';
                soqlExchange +='device__c,device__r.name,IMEI_Number_Device__c,Serial_Number_Device__c, ';
                soqlExchange += 'Model_Code_GCIC__c from case ';
            }
            return soqlExchange;
        }

        set;
    }

    private String soqlRepair {
        get{
            if (soqlRepair == null){
                soqlRepair = 'select ID, casenumber, subject, status,Integration_Status__c,Device_Type_f__c,Model_Code_Repair__c,Model_Code__c,';
                soqlRepair +='Outbound_Shipping_Status__c, Inbound_Shipping_Status__c,device__r.Status__c,';
                soqlRepair +='IMEI__c,SerialNumber__c, Case_Device_Status__c, ';
                soqlRepair += 'Model_Code_GCIC__c from case ';
            }
            return soqlRepair;
        }

        set;
    }*/
    

    public String sortDir {
        get  { if (sortDir == null) {  sortDir = 'asc'; } return sortDir;  }
        set;
    }

  // the current field to sort by. defaults to last name
  public String sortField {
    get  { if (sortField == null) {sortField = 'casenumber'; } return sortField;  }
    set;
  }

  // format the soql for display on the visualforce page
  public String debugSoql {
    get { return soql + ' order by ' + sortField + ' ' + sortDir + ' limit 20'; }
    set;
  }
  

  // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public SVC_ManageChildCases(ApexPages.StandardController stdController) {
        noOfSelected = 0;
        showMassEditForm = false;
        showResultForm = true;
        
        if (caseId != null){
          this.parentCase = [select id,recordtypeid,caseNumber from case where id =:caseID];  
        }  
        else{
          this.parentCase = (Case)stdController.getRecord();
        }
        if ((String)parentCase.get('recordtypeid') == exchangeRecTypeId){
          IsparentExchange = true;
        }else{
          IsparentExchange = false;
        }
        
        parentStatus = [select status from case where id=:(Id)parentCase.get('Id')].status;

        /*soql = 'select ID, casenumber, subject, status,Integration_Status__c,';
        soql +='Outbound_Shipping_Status__c, Inbound_Shipping_Status__c,device__r.Status__c,';
        soql +='device__c,device__r.name,device__r.IMEI__c,IMEI__c,SerialNumber__c, Case_Device_Status__c,';
        soql += 'device__r.Serial_Number__c,Model_Code_GCIC__c,IMEI__c,recordtype.name ';
        soql += 'from case where parentid = \''+(Id)parentCase.get('Id')+'\''; */
        soql = soqlCommon + 'where parentid = \''+(Id)parentCase.get('Id')+'\'';
        runQuery();

        showConfirmationMsg = false;

        if(IsparentExchange)
        {
            final Integer childCaseCount = [SELECT Id FROM Case WHERE ParentId =: parentCase.Id].size();
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Total Number of Child Cases: ' + childCaseCount));
        }
        else 
        {
            validCaseCnt = [SELECT Id FROM Case WHERE Case_Device_Status__c = 'Valid' AND ParentId =: parentCase.Id].size();
            invalidCaseCnt = [SELECT Id FROM Case WHERE Case_Device_Status__c = 'Invalid' AND ParentId =: parentCase.Id].size();
            final Integer totalCasecnt = validCaseCnt + invalidCaseCnt;
        
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Total Number of Child Cases: ' + totalCasecnt + ' Number of Valid Device: ' + validCaseCnt + ' Number of Invalid Device '+ invalidCaseCnt));
        }
    }

    public String getRecordName() {
        return 'Hello ' + (String)parentCase.get('name') + ' (' + (Id)parentCase.get('Id') + ')';
    }

  // toggles the sorting of query from asc<-->desc
  public void toggleSort() {
    // simply toggle the direction
    sortDir = sortDir.equals('asc') ? 'desc' : 'asc';
    // run the query again
    runQuery();
  }

  // runs the actual query
  public void runQuery() {
    selectedCaseMap.clear();
    noOfSelected = 0;
    childCases = new List<childCaseWrapper>();
    showRecordWarningMsg = false;
    try {
      List<Case> cases = Database.query(soql + ' order by ' + sortField + ' ' + sortDir + ' limit 20000');
      for (Case c:cases){
        childCases.add(new ChildCaseWrapper(c,false,false));
      }
      if (childCases.size() >= 20000){
        showRecordWarningMsg = true;
      }

    } catch (Exception e) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
    }

  }

  // runs the search with parameters passed via Javascript
  public PageReference runSearch() {

    String caseStatus = Apexpages.currentPage().getParameters().get('caseStatus');
    String deviceStatus = Apexpages.currentPage().getParameters().get('deviceStatus');
    String integrationStatus = Apexpages.currentPage().getParameters().get('integrationStatus');
    String inboundShippingStatus = Apexpages.currentPage().getParameters().get('inboundShippingStatus');
    String outboundShippingStatus = Apexpages.currentPage().getParameters().get('outboundShippingStatus');
    //soql = 'select ID, casenumber, subject, status,Integration_Status__c,';
    //soql +='Outbound_Shipping_Status__c, Inbound_Shipping_Status__c,device__r.Status__c,device__c,device__r.name ';
    //soql += 'from case where parentid = \''+(Id)parentCase.get('Id')+'\'';
    soql = soqlCommon + 'where parentid = \''+(Id)parentCase.get('Id')+'\'';
    //soql = 'select firstname, lastname, account.name, interested_technologies__c from contact where account.name != null';
    if (!caseStatus.equals('')&&!caseStatus.equals('--'))
      soql += ' and status = \''+String.escapeSingleQuotes(caseStatus)+'\'';
    if (!deviceStatus.equals('')&&!deviceStatus.equals('--')){
      if (IsparentExchange){
        soql += ' and device__r.status__c = \''+String.escapeSingleQuotes(deviceStatus)+'\'';
      }else{
        soql += ' and Case_Device_Status__c = \''+String.escapeSingleQuotes(deviceStatus)+'\'';
      }
    }
    if (!integrationStatus.equals('')&&!integrationStatus.equals('--'))
      soql += ' and Integration_Status__c = \''+String.escapeSingleQuotes(integrationStatus)+'\'';
    if (!inboundShippingStatus.equals('')&&!inboundShippingStatus.equals('--'))
      soql += ' and Inbound_Shipping_Status__c = \''+String.escapeSingleQuotes(inboundShippingStatus)+'\'';
    if (!outboundShippingStatus.equals('')&&!outboundShippingStatus.equals('--'))
      soql += ' and Outbound_Shipping_Status__c = \''+String.escapeSingleQuotes(outboundShippingStatus)+'\'';
    
    runQuery();
    
    return null;
  }

  // use apex describe to build the picklist values
  public List<String> caseStatus {
    get {
      if (caseStatus == null) {

        caseStatus = new List<String>();
        Schema.DescribeFieldResult field = Case.status.getDescribe();

        for (Schema.PicklistEntry f : field.getPicklistValues())
          caseStatus.add(f.getLabel());

      }
      return caseStatus;          
    }
    set;
  }

  public List<String> caseDeviceStatus {
    get {
      if (caseDeviceStatus == null) {

        caseDeviceStatus = new List<String>();
        
        if (IsparentExchange){
            Schema.DescribeFieldResult field = Device__c.status__c.getDescribe();
            for (Schema.PicklistEntry f : field.getPicklistValues())
                caseDeviceStatus.add(f.getLabel());
        }else{
            Schema.DescribeFieldResult field = Case.Case_Device_Status__c.getDescribe();
            for (Schema.PicklistEntry f : field.getPicklistValues())
              caseDeviceStatus.add(f.getLabel());
        }
      }
      return caseDeviceStatus;          
    }
    set;
  }
    
  public List<String> integrationStatus {
    get {
      if (integrationStatus == null) {

        integrationStatus = new List<String>();
        Schema.DescribeFieldResult field = Case.integration_status__c.getDescribe();

        for (Schema.PicklistEntry f : field.getPicklistValues())
          integrationStatus.add(f.getLabel());

      }
      return integrationStatus;          
    }
    set;
  }

  public List<String> inboundShippingStatus {
    get {
      if (inboundShippingStatus == null) {

        inboundShippingStatus = new List<String>();
        Schema.DescribeFieldResult field = Case.Inbound_Shipping_Status__c.getDescribe();

        for (Schema.PicklistEntry f : field.getPicklistValues())
          inboundShippingStatus.add(f.getLabel());

      }
      return inboundShippingStatus;          
    }
    set;
  }

  public List<String> outboundShippingStatus {
    get {
      if (outboundShippingStatus == null) {

        outboundShippingStatus = new List<String>();
        Schema.DescribeFieldResult field = Case.Outbound_Shipping_Status__c.getDescribe();

        for (Schema.PicklistEntry f : field.getPicklistValues())
          outboundShippingStatus.add(f.getLabel());

      }
      return outboundShippingStatus;          
    }
    set;
  }

  public class ChildCaseWrapper {
        public Case childCase {get; set;}
        public Boolean selected {get; set;}
        public Boolean currentPage{get;set;}
        public String imei {get; set;}
        public String serialNumber {get; set;} 
        public String deviceType{get;set;} 
        public String deviceStatus {get; set;}
        public String deviceLnk {get; set;}
        public String modelCode {get; set;}


        public ChildCaseWrapper(Case c,Boolean bool,Boolean bool2) {
            childCase = c;
            selected = bool;
            currentPage = bool2;
            if (c.recordtype.name == 'Exchange'){
                imei = c.device__r.IMEI__c;
                deviceStatus = c.device__r.Status__c;
                serialNumber = c.device__r.Serial_Number__c;
                deviceLnk = c.device__r.id;
                deviceType = c.Device_Type_f__c;
                modelCode = c.Model_Code__c;
            }
            else{
                imei = c.IMEI__c;
                deviceStatus = c.Case_Device_Status__c;
                serialNumber = c.SerialNumber__c;
                deviceLnk = c.id;
                deviceType = c.Device_Type_f__c;
                modelCode = c.Model_Code_Repair__c;
            }
        }
    }

    public void checkboxClicked(){
        String caseID = Apexpages.currentPage().getParameters().get('clickedcaseID');
        String selected = Apexpages.currentPage().getParameters().get('selected');
        if (selected == 'true'){
            if (!selectedCaseMap.containsKey(caseID)){
                selectedCaseMap.put(caseID, caseID);
            }
        }else{
            if (selectedCaseMap.containsKey(caseID)){
                selectedCaseMap.remove(caseID);
            }

        }
        noOfSelected = selectedCaseMap.size();
    }

    public void cancelSelectedCases(){
      if (parentStatus == 'Closed'){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Closed case cannot be updated.'));
      
      }else if (selectedCaseMap.size() <=0){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Please select at least one case.'));
        }else{
            List<Case> toCancel = [select id,status from case where id in:selectedCaseMap.keySet()];
            for (Case c:toCancel){
              c.status = 'Cancelled';
            }
            try{
              update toCancel;

              for ( ChildCaseWrapper c:childCases){
                //system.debug('before casenumber: ' + c.childCase.casenumber + ' ' +c.selected);
                if (selectedCaseMap.containsKey(c.childCase.id)){
                    //system.debug('YES casenumber: ' + c.childCase.casenumber );
                    c.selected = true;
                    c.childCase.status = 'cancelled';
                }
                //system.debug('After casenumber: ' + c.childCase.casenumber + ' ' +c.selected);
            }

            } catch (DMLException e) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            }
        }
    }

    public void toggleSelect(){

      for ( ChildCaseWrapper c:childCases){
        if (c.currentPage){
          if (c.selected){
            if (!selectedCaseMap.containsKey(c.childCase.id)){
                selectedCaseMap.put(c.childCase.id,c.childCase.id);
            }

          }else{
            if (selectedCaseMap.containsKey(c.childCase.id)){
                selectedCaseMap.remove(c.childCase.id);
            }
          }
        }
      }

      noOfSelected = selectedCaseMap.size();

    }

    public PageReference massEdit(){
       
       if (parentStatus == 'Closed'){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Closed case cannot be updated.'));
      
      }else if (selectedCaseMap.size() <=0){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Please select at least one case to edit.'));
        }else{
          showMassEditForm = true;
          showResultForm = false;
        }

        return null;
    }

    public PageReference backToList(){
      PageReference pageRef = new PageReference('/apex/SVC_ManageChildCases?id='+parentCase.id);
      pageRef.setRedirect(true);
      return pageRef;
    }

    public PageReference save(){
      Map<ID,ImeiSerialWrapper> deviceMap = new Map<ID,ImeiSerialWrapper>();
      Map<ID,ImeiSerialWrapper> caseMap = new Map<ID,ImeiSerialWrapper>();
      try{
        for (ChildCaseWrapper s : selectedCaseWrapperList){
            if (s.childCase.recordtype.name == 'Exchange'){
              deviceMap.put(s.childCase.device__c,new ImeiSerialWrapper(s.imei,s.serialNumber));
           }
           else{
              caseMap.put(s.childCase.id,new ImeiSerialWrapper(s.imei,s.serialNumber));
           }
        }

        if (deviceMap.size()>0){
            List<Device__c> decs = [select id, imei__c,Serial_Number__c from device__c 
              where id in:deviceMap.keySet()];
            for (Device__c d:decs){
              d.imei__c = deviceMap.get(d.id).imei;
              d.Serial_Number__c = deviceMap.get(d.id).serialNumber;
            }
            update decs;
        }

        if (caseMap.size()>0){
            List<Case> cases = [select id, imei__c,SerialNumber__c from case 
              where id in:caseMap.keySet()];
            for (Case c:cases){
              c.imei__c = caseMap.get(c.id).imei;
              c.SerialNumber__c = caseMap.get(c.id).serialNumber;
            }
            update cases;
        }

      } catch (Exception e) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            }

      PageReference pageRef = new PageReference('/apex/SVC_ManageChildCases?id='+parentCase.id);
      pageRef.setRedirect(true);
      return pageRef;
    }

    private class ImeiSerialWrapper{
      public String imei {get; set;}
      public String serialNumber {get; set;}
      public imeiSerialWrapper(String imei_i,String serialNumber_i){
        imei = imei_i;
        serialNumber = serialNumber_i;
      }


    }

}