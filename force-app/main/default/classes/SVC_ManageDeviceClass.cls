public with sharing class SVC_ManageDeviceClass {

    public final String accountName             {get; private set;}
    public Boolean disableReferencePicklist     {get; private set;}
    public String selectedRefNum                {get; set;}
    public List<SelectOption> refNumOption      {get; private set;}
    public List<Device__c> deviceList           {get; private set;}
    public final String acctId                  {get; private set;}
    public String entitlementId                 {get;set;}
    public String totalCnt                      {get;set;}
    public Integer validDevice                  {get;set;}
    public Integer invalidDevice                {get;set;}

    public SVC_ManageDeviceClass() {
        validDevice                 = 0;
        invalidDevice               = 0;
        disableReferencePicklist    = false;
        accountName                 = ApexPages.currentPage().getParameters().get('acctName');
        selectedRefNum              = ApexPages.currentPage().getParameters().get('ref');
        acctId                      = ApexPages.currentPage().getParameters().get('acctId');
        entitlementId               = ApexPages.currentPage().getParameters().get('entId');
        totalCnt                    = ApexPages.currentPage().getParameters().get('cnt');

        if(ApexPages.currentPage().getParameters().get('disableRef') != null)   
            disableReferencePicklist = Boolean.valueOf(ApexPages.currentPage().getParameters().get('disableRef'));
        


        refNumOption = new List<SelectOption>();

        if(disableReferencePicklist)
        {
            refNumOption.add(new SelectOption(selectedRefNum, selectedRefNum));
        }
        /*else
        {
            //get all reference number
        }*/

        deviceList = [SELECT IMEI__c, Serial_Number__c, Entitlement__r.Name, Status__c, Model_Name__c 
                      FROM Device__c 
                      WHERE Reference_Number__c =: selectedRefNum LIMIT 1000];

        validDevice = [SELECT Id 
                       FROM Device__c 
                       WHERE Reference_Number__c =: selectedRefNum
                       AND Status__c = 'Valid'].size();


        invalidDevice = [SELECT Id 
                         FROM Device__c 
                         WHERE Reference_Number__c =: selectedRefNum
                         AND Status__c = 'Invalid'].size();

        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'Number of Valid Device: ' + validDevice + '<br>'+'Number of Invalid Device '+ invalidDevice));
    }
    public void init(){
        sendEmail(validDevice, invalidDevice);
    }
    public void sendEmail(Integer validCnt, Integer invalidCnt){
        List<Messaging.SingleEmailMessage> mails =  new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        List<String> to = new List<String>();
        for(Contact c : [SELECT Id, Email FROM Contact WHERE AccountId =: acctId AND Named_Caller__c = true]){
            to.add(c.Email);    
        }

        Id usId = UserInfo.getUserId();
        String entName = [SELECT Name FROM Entitlement WHERE Id =: entitlementId].Name;
        EmailTemplate et = [SELECT Id, Subject, HtmlValue, Body, BrandTemplateId FROM EmailTemplate WHERE Name = 'Device On-boarding Result'];
        Domain dm = [SELECT Domain,DomainType FROM Domain];
        Document dc;
        if(!Test.isRunningTest()) dc = [SELECT Id, Name FROM Document WHERE Name = 'samsung_login_logo.png'];
        OrgWideEmailAddress owda = [SELECT Id, Address FROM OrgWideEmailAddress WHERE Displayname = 'SBS Support'];
        ListView lv = [SELECT Id From ListView WHERE Name ='Registered Devices'];

        mail.setToAddresses(to); 
        mail.setReplyTo(owda.Address);

        String customlink = '';
        String imgstr = '';
        String header = '';
        String footer = '';

        if(!Test.isRunningTest()){
            customlink = 'https://'+dm.Domain + '/samsungservices/s/recordlist/Device__c/' + lv.Id;
            imgstr = '<img src="'+url.getSalesforceBaseUrl().toExternalForm()+'/servlet/servlet.ImageServer?id='+dc.id+'&oid='+UserInfo.getOrganizationId()+'" height="44" width="250"/><br/>';
            header = '<tr valign = "top"><td style="background-color:#AAAAFF; bEditID:r2st1; bLabel:accent1; height:5;"></td></tr><tr valign="top" height="400">';
            footer = '<tr valign = "top"><td style="background-color:#AAAAFF; bEditID:r2st1; bLabel:accent1; height:5;"></td></tr></table>';
        }
        else{
            customlink = 'testData';
            imgstr = 'testData';
            header = 'testData';
            footer = 'testData';
        }
        String body = '';
        body = imgstr;
        body += et.HtmlValue;
        //system.debug('before body : ' + body);
        //system.debug('et============>' + et.HtmlValue);
        body = body.remove('<![CDATA[');
        body = body.remove(']]>');
        body = body.replace('<tr valign="top" height="400" >', header);
        body = body.replace('</table>', footer);
        body = body.replace('{AccountName}', accountName);
        body = body.replace('{EntitlementName}', entName);
        body = body.replace('{CustomLink}', customlink);
        body = body.replace('{Count}', totalCnt);
        body = body.replace('{validCount}', String.valueOf(validCnt));
        body = body.replace('{invalidCount}', String.valueOf(invalidCnt));

        //system.debug('after body : ' + body);

        String subject = et.Subject;
        subject = subject.replace('{AccountName}', accountName);
        
        mail.setSubject(subject);
        mail.setTargetObjectId(usId);
        mail.setSaveAsActivity(false);
        mail.setHtmlBody(body);
        mail.setTreatBodiesAsTemplate(false);
        mail.setOrgWideEmailAddressId(owda.Id);
        mail.setTemplateId(et.Id);
        mails.add(mail);

        if(!to.isEmpty()) Messaging.sendEmail(mails);
    }
}