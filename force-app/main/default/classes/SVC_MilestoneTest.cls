@isTest
private class SVC_MilestoneTest {
  
  static testMethod void TestCompleteMilestoneCase(){
    Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
    id userid = userinfo.getUserId();
    //id MigrationUserid = [select id from user where name='Migration User' limit 1].id;
    User u = [select id,name from user where name = 'Integration User' and profile.name='Integration User' and isactive=true limit 1];
    system.debug('USER Name---------->'+u.name);
    System.runAs(u){
    /*Profile adminProfile = [SELECT Id FROM Profile Where Name ='B2B Service System Administrator'];

    User admin1 = new User (
      FirstName = 'admin1',
      LastName = 'admin1',
      Email = 'admin1seasde@example.com',
      Alias = 'admint1',
      Username = 'admin1seaa@example.com',
      ProfileId = adminProfile.Id,
      TimeZoneSidKey = 'America/New_York',
      LocaleSidKey = 'en_US',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US'
     );
    
    insert admin1;*/
    
    //Address insert
    List<Zipcode_Lookup__c> zipList = new List<Zipcode_Lookup__c>(); 
    Zipcode_Lookup__c  zip1 = new Zipcode_Lookup__c(Name='07660',City_Name__c='RIDGEFIELD PARK', Country_Code__c='US', State_Code__c='NJ',State_Name__c='New Jersey');
    zipList.add(zip1);
    
    insert zipList;

    //BusinessHours bh24 = [SELECT Id FROM BusinessHours WHERE name = 'B2B Service Hours 24x7'];
    BusinessHours bh12 = [SELECT Id FROM BusinessHours WHERE name = 'B2B Service Hours 12x5'];
  
    //RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
    List<Account> acclist = new List<Account>();
    Id accountRTid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Temporary').getRecordTypeId();
    Account testAccount1 = new Account (
      Name = 'TestAcc1-milestone',
      RecordTypeId = accountRTid,
      //ownerid = MigrationUserid,
      BillingStreet = '85 Challenger Rd',
      BillingCity ='Ridgefield Park',
      BillingState ='NJ',
      BillingCountry='US',
      BillingPostalCode='07660'
    );
    acclist.add(testAccount1);
    //insert testAccount1;

    Account testAccount2 = new Account (
      Name = 'TestAcc2-milestone',
      RecordTypeId = accountRTid,
      //ownerid = MigrationUserid,
      BillingStreet = '85 Challenger Rd',
      BillingCity ='Ridgefield Park',
      BillingState ='NJ',
      BillingCountry='US',
      BillingPostalCode='07660'
      
    );
    acclist.add(testAccount2);
    //insert testAccount2;
    insert acclist;

    //RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];
    id productRTid = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('B2B').getRecordTypeId();
    Product2 prod1 = new Product2(
        name ='MI-test',
        SAP_Material_ID__c = 'MI-test',
        sc_service_type__c = 'Incident',
        sc_service_type_ranking__c = '6',
        RecordTypeId = productRTid
      );

    insert prod1;
    List<Asset> assList = new List<Asset>();
    Asset ast = new Asset(
      name ='Assert-MI-Test',
      Accountid = testAccount1.id,
      Product2Id = prod1.id
      );
    assList.add(ast);
    //insert ast;

    Asset ast2 = new Asset(
      name ='Assert-MI-Test2',
      Accountid = testAccount2.id,
      Product2Id = prod1.id
      );
    assList.add(ast2);
    //insert ast2;
    insert assList;
    
    Slaprocess sla = [select id from SlaProcess where name like 'Enhanc%' 
      and isActive = true limit 1 ];
    
    List<Entitlement> entList = new List<Entitlement>();
    Entitlement ent = new Entitlement(
      name ='test Entitlement1',
      accountid=testAccount1.id,
      assetid = ast.id,
      BusinessHoursId = bh12.id,
      startdate = system.today()-1,
      slaprocessid = sla.id
      );
    //insert ent;
    entList.add(ent);

    Entitlement ent2 = new Entitlement(
      name ='test Entitlement2',
      accountid=testAccount2.id,
      assetid = ast2.id,
      BusinessHoursId = bh12.id,
      startdate = system.today()-1
      );
    //insert ent2;
    entList.add(ent2);
    insert entList;
    
    Contact contact = new Contact(); 
    contact.AccountId = testAccount1.id; 
    contact.Email = 'svcTest@svctest.com'; 
    contact.LastName = 'Named Caller'; 
    contact.FirstName = 'FirstName';
    contact.Named_Caller__c = true;
    contact.MailingCity     = testAccount1.BillingCity;
    contact.MailingCountry  = testAccount1.BillingCountry;
    contact.MailingPostalCode= testAccount1.BillingPostalCode;
    contact.MailingState    = testAccount1.BillingState;
    contact.MailingStreet   = testAccount1.BillingStreet;
    insert contact; 

    String entlId;
    if (ent != null)
    entlId = ent.Id; 

    List<Case> cases = new List<Case>{};
    //if (entlId != null){
      Case c = new Case(Subject = 'Test Case with Entitlement ', 
      origin = 'Web',
      Severity__c = '4-Low',
      ownerid = userid,
      EntitlementId = entlId, ContactId = contact.id,Shipping_Address_1__c='100 challenger rd',
            Shipping_City__c='RIDGEFIELD PARK',
            Shipping_Country__c='US',
            Shipping_State__c='NJ',
            Shipping_Zip_Code__c= '07660');
      cases.add(c);
      
      Case c2 = new Case(Subject = 'Test Case with Entitlement ', 
      EntitlementId = entlId, ContactId = contact.id,Shipping_Address_1__c='100 challenger rd',
            Shipping_City__c='RIDGEFIELD PARK',
            Shipping_Country__c='US',
            Shipping_State__c='NJ',
            Shipping_Zip_Code__c= '07660');
      cases.add(c2);
    
      insert cases;
      List<Id> caseIds = new List<Id>();
      caseIds.add(c.Id);
      //update cases;

      SVC_milestoneUtils.completeMilestone(caseIds, 'First Response Time', System.now());
      SVC_milestoneUtils.completeMilestone(caseIds, 'Status Update', System.now());
      
      List<Id> caseIds2 = new List<Id>();
      caseIds2.add(c2.id);
      SVC_milestoneUtils.completeMilestoneFuture(caseIds, 'First Response Time', System.now());

    /*List<Case> cases2 = new List<Case>{};
    if (entlId != null){
      Case c2 = new Case(Subject = 'Test Case with Entitlement ', 
      EntitlementId = entlId, ContactId = contact.id);
      cases2.add(c2);
    }
    if (cases2.isEmpty()==false){
      insert cases2;
      List<Id> caseIds = new List<Id>();
      for (Case cL : cases2){
        caseIds.add(cL.Id);
      }
      SVC_milestoneUtils.completeMilestoneFuture(caseIds, 'First Response Time', System.now());
    }*/

  }
  }
  
}