@isTest
public class SVC_MockinContactHttpToken implements HttpCalloutMock {
    public HTTPResponse respond(HTTPRequest req) {
         HttpResponse res = new HttpResponse();

        if ( req.getEndpoint().contains('Token')){
            res.setStatusCode(200);
            res.setStatus('OK Token');
            res.setBody('{"access_token":"234234234"}');
        }else{
            res.setStatusCode(202);
            res.setStatus('Accepted');
            res.setBody('');
        }
        return res;
    }

}