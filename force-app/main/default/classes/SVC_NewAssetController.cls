public with sharing class SVC_NewAssetController {
	private ApexPages.StandardController controller {get; set;}
	public SVC_NewAssetController(ApexPages.StandardController controller) {
		
	}

	public PageReference pageRedirect() {
		String RecordType = ApexPages.currentPage().getParameters().get('RecordType');
		String acc_id = ApexPages.currentPage().getParameters().get('acc_id');
		String acc_id_str='';
		String prd_id= ApexPages.currentPage().getParameters().get('prd_id');
		String prd_id_str='';

		String name= ApexPages.currentPage().getParameters().get('Name');
		String name_str='DO NOT CHANGE';

		String cfid = ApexPages.currentPage().getParameters().get('cfid');
		String cfidvalue = ApexPages.currentPage().getParameters().get('cfidvalue');
		String cflkid = ApexPages.currentPage().getParameters().get('cflkid');
		String cflkidvalue = ApexPages.currentPage().getParameters().get('cflkidvalue');
		string cf_str='';

		if (cfid !=null && cfidvalue !=null && cflkid !=null && cflkidvalue != null){
			cf_str = '&'+cfid+'='+cfidvalue +'&'+cflkid+'='+cflkidvalue;
		}


		if (acc_id !=null){
			acc_id_str =  '&acc_id='+acc_id;
		}

		if (prd_id !=null){
			prd_id_str =  '&prd_id='+prd_id;
		}

		if (name !=null){
			name_str = name;
		}

		return new PageReference('/02i/e?RecordType='+RecordType+'&nooverride=1&Name='+name_str+'&retURL=' + ApexPages.currentPage().getParameters().get('retURL')
			+acc_id_str	+ prd_id_str+cf_str	);
	}
}