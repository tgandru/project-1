public  class SVC_NewPop {
	public Proof_Of_Purchase__c pop {get; set;}
	public String casenumber {get; private set;}
	public Id caseId {get; private set;}
	public String caseStatus {get; private set;}
	public transient blob contentFile {get; set;}
	public String fileName {get; set;}
	public Integer fileSize {get; set;}
	public Boolean popCreated {get; set;}
	public Boolean hasError{get;set;}


	public SVC_NewPop(Apexpages.StandardController stdCon) {

		pop = new Proof_Of_Purchase__c();
		casenumber = '';
		popCreated = false;
		hasError = false;

		if(ApexPages.currentPage().getParameters().get('caseId') != null)
		{
			caseId = Id.valueof(ApexPages.currentPage().getParameters().get('caseId'));
			setCaseValues();
		}else if(ApexPages.currentPage().getParameters().get('Id') != null)
		{
			caseId = Id.valueof(ApexPages.currentPage().getParameters().get('Id'));
			setCaseValues();
		}
		else{
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Missing Case ID'));
			hasError = true;
		}
		if (caseStatus == 'Resolved' || caseStatus=='Closed'){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'New proof of purchase cannot be added to '+caseStatus +' case.'));
			hasError = true;
		}
		
	}
	public void createPop(){

		
		if (contentFile == null){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a proof of purchase file.'));
		}
		else if (fileSize > 3774874){ //file size check. must <3.6MB (3774874 bytes))
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'File size cannot be more than 3.6MB. Please chose a different file.'));
		}
		else{

			try{
				pop.status__c = 'Pending Confirmation';
			
				insert pop;
				ContentVersion v = new ContentVersion();
	        	v.versionData = contentFile;
	        	v.title = fileName;
	        	v.pathOnClient =fileName;
	        	insert v;
	        	//return new PageReference('/' + v.id);
				
				ContentVersion cv = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :v.Id];

				ContentDocumentLink cd = new ContentDocumentLink();
				cd.LinkedEntityId = pop.id;
				cd.ShareType = 'V';
				cd.Visibility='AllUsers';
				cd.ContentDocumentId = cv.ContentDocumentId;
				insert cd;

			}
			catch (DMLException e) {
	                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
	        }

	        //if created by internal users, populate purchase date on case, child cases, GCIC, etc
	        
	        if (UserInfo.getUserType() == 'Standard'){
	        	updateCases();
	        }

	        popCreated = true;
    	}
	}
	private void setCaseValues(){
		casenumber = [select Id,casenumber from Case where id=:caseId].casenumber;
		caseStatus = [select Id,status from Case where id=:caseId].status;
		pop.case__c = caseid;
	}

	public PageReference backToCase(){
	  pop.purchase_date__c = system.today();
      PageReference pageRef = new PageReference('/'+caseid);
      pageRef.setRedirect(true);
      return pageRef;
    }

    private void updateCases(){
    	Case c = [select id, purchase_date__c from case where id=:caseId];
    	c.purchase_date__c = pop.purchase_date__c;
    	update c;
    	//child cases

    	List<case> childCases = [select id,purchase_date__c,POP_Source__c from case 
    			where parentid =: caseId and status NOT IN ('Closed','Resolved')];
    	for (Case cc:childCases){
    		if (cc.purchase_date__c ==null){
    			cc.purchase_date__c = pop.purchase_date__c;
    			cc.POP_Source__c = 'Parent';
    		}else if (cc.POP_Source__c != 'Self' ){
    			cc.purchase_date__c = pop.purchase_date__c;
    			cc.POP_Source__c = 'Parent';
    		}
    	}
    	update childCases;

    	pop.status__c = 'Confirmed';
    	update pop;
    	//GCIC?

    }

}