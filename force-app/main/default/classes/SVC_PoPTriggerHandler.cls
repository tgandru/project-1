public class SVC_PoPTriggerHandler 
{
    public Static Void AfterUpdate(List<Proof_Of_Purchase__c> triggerNew, List<Proof_Of_Purchase__c> triggerOld) {
        Map<ID,Date> parentCaseMap = new Map<ID,Date>();

        for (Proof_Of_Purchase__c p:triggerNew){
            if (p.status__c == 'Confirmed'){
                parentCaseMap.put(p.case__c,p.purchase_date__c);
            }
        }

        //child cases
        List<Case> childCases = [select id, purchase_date__c,parentid,POP_Source__c from case 
            where parentid in: parentCaseMap.keySet() and (POP_Source__c != 'Self' OR purchase_date__c = null) ];
        for (Case cc:childCases){
            cc.POP_Source__c = 'Parent';
            cc.purchase_date__c = parentCaseMap.get(cc.parentid);
        }
        update childCases;

        //parent       
        List<Case> parentCases = [select id, purchase_date__c,POP_Source__c from case 
            where id in: parentCaseMap.keySet()];
        for (Case pc:parentCases){
            pc.POP_Source__c = 'Self';
            pc.purchase_date__c = parentCaseMap.get(pc.id);
        }
        update parentCases;

    }    
}