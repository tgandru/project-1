public with sharing class SVC_ReplacementDeviceHelper {
	public SVC_ReplacementDeviceHelper() {
		
	}

	public static string replaceDevices(Map<ID,SVC_ReplacementDeviceWrapper> deviceMap){
		List<Device__c> newDevices = new List<Device__c>();
		List<Device__C> oldDevices = [select id,entitlement__c,status__c,account__c from Device__c
			where ID IN:deviceMap.keySet()]; 

		try{
			for (device__c oldD:oldDevices){
				oldD.status__c = 'Unregistered';
				Device__c newD = new Device__c();
				newD.entitlement__c = oldD.entitlement__c;
				newD.imei__c = deviceMap.get(oldD.ID).imei;
				newD.Serial_Number__c = deviceMap.get(oldD.ID).serial_no;
				newD.Model_Name__c = deviceMap.get(oldD.ID).model_no;
				newD.account__c = oldD.account__c;
				newD.status__c = 'Registered';
				newDevices.add(newD);
			}
			
			insert newDevices;
			update oldDevices;
			return null;
		
		}catch(DmlException e) {	
    		System.debug('The following exception has occurred: ' + e.getMessage());
    		return e.getMessage();
		}

	}
}