public class SVC_ReplacementDeviceWrapper {
	public string imei {get; set;}
	public string serial_no {get; set;}
	public string model_no {get;set;}

	public SVC_ReplacementDeviceWrapper(String i,string s,string m) {
		imei = i;
		serial_no = s;
		model_no = m;
	}
}