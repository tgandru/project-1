@isTest
private class SVC_TestApproveDevicesController
{
    @isTest
    static void TestApproveDevicesController()
    {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Profile adminProfile = [SELECT Id FROM Profile Where Name ='B2B Service System Administrator'];

        User admin1 = new User (
            FirstName = 'admin1',
            LastName = 'admin1',
            Email = 'admin1seasde@example.com',
            Alias = 'admint1',
            Username = 'admin1seaa@example.com',
            ProfileId = adminProfile.Id,
            TimeZoneSidKey = 'America/New_York',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US'
        );
        
        insert admin1;

        BusinessHours bh24 = [SELECT Id FROM BusinessHours WHERE name = 'B2B Service Hours 24x7'];
        BusinessHours bh12 = [SELECT Id FROM BusinessHours WHERE name = 'B2B Service Hours 12x5'];

        RecordType caseRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Repair' 
            AND SobjectType = 'Case'];
        

        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660',
            Service_Account_Manager__c = admin1.id
            
        );

        insert testAccount1;

        
        RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];
        Product2 prod1 = new Product2(
                name ='MI-test',
                SAP_Material_ID__c = 'MI-test',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = productRT.id
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Asset ast2 = new Asset(
            name ='Assert-MI-Test2',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast2;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            BusinessHoursId = bh12.id,
            startdate = system.today()-1
            );
        insert ent;

        List<Device__c> devs = new List<Device__c>();
        //valid imei
        device__c dev4 = new device__c(
            account__c = testAccount1.id,
            imei__c = '490154203237518',
            entitlement__c=ent.id
            
        );
        devs.add(dev4);

        device__c dev5 = new device__c(
            account__c = testAccount1.id,
            Serial_Number__c = '123456789012341',
            Model_Name__c = 'MI-test',
            entitlement__c=ent.id
            
        );
        devs.add(dev5); 
        
        Test.startTest();
        insert devs;

        List<Device__c> devices = [select id,status__c from device__c where id in:devs];
        for (Device__c c:devices){
            c.status__c = ' Verified';
        }
        update devices;

        Device__c d = [select id from device__c where id in:devs limit 1];
        

        PageReference pageRef = Page.SVC_ApproveDisapproveDevices;

        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('accid', testAccount1.id);
        ApexPages.StandardController stdCtr = new ApexPages.StandardController(d);
        SVC_ApproveDisapproveDevices devCtr = new SVC_ApproveDisapproveDevices(stdCtr);
        


        System.runAs(admin1) {
            PageReference pageRef1 = Page.SVC_ApproveDisapproveDevices;

            Test.setCurrentPage(pageRef1);
            ApexPages.StandardController stdCtr1 = new ApexPages.StandardController(d);
            SVC_ApproveDisapproveDevices devCtr1 = new SVC_ApproveDisapproveDevices(stdCtr1);
            
            ApexPages.currentPage().getParameters().put('clickedDeviceID', d.id);
            ApexPages.currentPage().getParameters().put('selected', 'false');
            devCtr1.checkboxClicked();

            ApexPages.currentPage().getParameters().put('clickedDeviceID', d.id);
            ApexPages.currentPage().getParameters().put('selected', 'true');
            devCtr1.approvalOrDisapproval = 'approval';
            devCtr1.checkboxClicked();
            PageReference p1 = devCtr1.UpdateSelected();
            PageReference p2 = devCtr1.updateAll();
            devCtr1.approvalOrDisapproval = 'disapproval';
            PageReference p3 = devCtr1.updateAll();

            List<SVC_ApproveDisapproveDevices.deviceWrapper> dwps= devCtr1.selectedDeviceWrapperList;
            List<Device__c> ds = devCtr1.selectedDeviceList;
            List<Entitlement> ents = devCtr1.entitlementLst;
            
            ApexPages.currentPage().getParameters().put('parAccountId', testAccount1.id);
            ApexPages.currentPage().getParameters().put('parEntitlementId', ent.id);
            PageReference p4 = devCtr1.runSearchByEntitlement();
            PageReference p5 = devCtr1.runSearchByAcct();
        }

        Test.stopTest();
    }
}