@isTest
private class SVC_TestAssetTeamTrigger
{
    @isTest
    static void testAssetTeamTrigger()
    {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Profile adminProfile = [SELECT Id FROM Profile Where Name ='B2B Service System Administrator'];

        User admin1 = new User (
            FirstName = 'admin1',
            LastName = 'admin1',
            Email = 'admin1seasde@example.com',
            Alias = 'admint1',
            Username = 'admin1seaa@example.com',
            ProfileId = adminProfile.Id,
            TimeZoneSidKey = 'America/New_York',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US'
        );
        
        insert admin1;

        BusinessHours bh24 = [SELECT Id FROM BusinessHours WHERE name = 'B2B Service Hours 24x7'];
        BusinessHours bh12 = [SELECT Id FROM BusinessHours WHERE name = 'B2B Service Hours 12x5'];

        RecordType caseRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Repair' 
            AND SobjectType = 'Case'];
        

        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660',
            ownerid = admin1.id
            
        );

        insert testAccount1;

        //Account testAccount1 = [select id from account where id='0013600000SaVYk'];

        
        RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];
        Product2 prod1 = new Product2(
                name ='MI-test',
                SAP_Material_ID__c = 'MI-test',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = productRT.id
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Asset ast2 = new Asset(
            name ='Assert-MI-Test2',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast2;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            BusinessHoursId = bh12.id,
            startdate = system.today()-1
            );
        insert ent;

        // Insert Contact 
        Contact contact = new Contact(); 
        contact.AccountId = testAccount1.id; 
        contact.Email = 'svcTest@svctest.com'; 
        contact.FirstName = 'Named Caller'; 
        contact.LastName = 'Named Caller'; 
        contact.Named_Caller__c = true;
        contact.MailingStreet = '1234 Main';
        contact.MailingCity = 'Chicago';
        contact.MailingState = 'IL';
        contact.MailingCountry = 'US';

        insert contact; 

        Asset_Team__c assetTeam1 = new Asset_Team__c(
            asset__c = ast.id,
            User__c = admin1.id,
            Asset_Team_Role__c ='Account Owner'
            );
        insert assetTeam1;

        Asset_Team__c assetTeam2 = new Asset_Team__c(
            asset__c = ast.id,
            User__c = admin1.id,
            Asset_Team_Role__c ='PM'
            );
        insert assetTeam2;

        List<Asset_Team__c> assetTeam3 = [select id from Asset_Team__c where id =: assetTeam1.id];
        delete assetTeam3;

        List<Asset_Team__c> assetTeam4 = [select id from Asset_Team__c where id =: assetTeam2.id];
        delete assetTeam4;

    }
}