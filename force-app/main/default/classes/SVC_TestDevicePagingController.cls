@isTest
private class SVC_TestDevicePagingController
{
    @isTest
    static void testDevicePagingController()
    {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Profile adminProfile = [SELECT Id FROM Profile Where Name ='B2B Service System Administrator'];

        User admin1 = new User (
            FirstName = 'admin1',
            LastName = 'admin1',
            Email = 'admin1seasde@example.com',
            Alias = 'admint1',
            Username = 'admin1seaa@example.com',
            ProfileId = adminProfile.Id,
            TimeZoneSidKey = 'America/New_York',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US'
        );
        
        insert admin1;

        BusinessHours bh24 = [SELECT Id FROM BusinessHours WHERE name = 'B2B Service Hours 24x7'];
        BusinessHours bh12 = [SELECT Id FROM BusinessHours WHERE name = 'B2B Service Hours 12x5'];

        RecordType caseRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Repair' 
            AND SobjectType = 'Case'];
        

        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );

        insert testAccount1;

        
        RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];
        Product2 prod1 = new Product2(
                name ='MI-test',
                SAP_Material_ID__c = 'MI-test',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = productRT.id
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Asset ast2 = new Asset(
            name ='Assert-MI-Test2',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast2;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            BusinessHoursId = bh12.id,
            startdate = system.today()-1
            );
        insert ent;

        List<Device__c> devs = new List<Device__c>();
        //valid imei
        device__c dev4 = new device__c(
            account__c = testAccount1.id,
            imei__c = '490154203237518',
            entitlement__c=ent.id
            
        );
        devs.add(dev4);

        device__c dev5 = new device__c(
            account__c = testAccount1.id,
            Serial_Number__c = '123456789012341',
            Model_Name__c = 'MI-test',
            entitlement__c=ent.id
            
        );
        devs.add(dev5); 

        Test.startTest();
        insert devs;

        List<Device__c> devices = [select id from device__c where id in:devs];

        PageReference pageRef = Page.SVC_ManageDeviceEntitlement;

        Test.setCurrentPage(pageRef);
        
        ApexPages.currentPage().getParameters().put('acc_id', testAccount1.id);
        ApexPages.currentPage().getParameters().put('updated', '10');
        ApexPages.StandardSetController setCon = new ApexPages.StandardSetController(devices);
        setCon.setSelected(devices);
        SVC_DevicePagingController dpCtr = new SVC_DevicePagingController(setCon);
        dpCtr.selectedEntitlementId = ent.id;
         //Sytem.assertNotEqual(dpCtr.deviceRecords,null);

         List<SVC_DevicePagingController.DeviceWrapper> devWlist = new List<SVC_DevicePagingController.DeviceWrapper>();
        devWlist = dpCtr.getDevice();
        dpCtr.selectAll();
        PageReference pref = dpCtr.processSelected();
        //dpCtr.hasNext = false;
        system.assertEquals(false, dpCtr.hasNext, 'No next page');
        system.assertEquals(false, dpCtr.hasPrevious, 'No previous page');
        system.assertEquals(1, dpCtr.pageNumber, 'Should be page 1');
        system.assertEquals(null, dpCtr.size, 'Should be null');
        system.assertEquals(null, dpCtr.deviceId, 'Should be null');
        system.assertEquals('test Entitlement', dpCtr.selectedEntitlementName, 'Should be test Entitlement');

        dpCtr.first();
        dpCtr.last();
        dpCtr.previous();
        dpCtr.next();
        PageReference pref2 = dpCtr.cancel();
        dpCtr.clearAll();
        List<SelectOption> entList = dpCtr.getEntitlementItems();
        Test.stopTest();

    }
}