@isTest
private class SVC_TestManageChildCasesController
{
	@isTest
	static void TestManageChildCasesController()
	{
		id userid = UserInfo.getUserId();
		User u = [select id,name from user where name = 'Integration User' and profile.name='Integration User' and isactive=true limit 1];
        system.debug('USER Name---------->'+u.name);
        System.runAs(u)
        {
        //Address insert
		List<Zipcode_Lookup__c> zipList = new List<Zipcode_Lookup__c>(); 
		Zipcode_Lookup__c  zip1 = new Zipcode_Lookup__c(Name='07660',City_Name__c='RIDGEFIELD PARK', Country_Code__c='US', State_Code__c='NJ',State_Name__c='New Jersey');
		zipList.add(zip1);
		
        insert zipList;

        //BusinessHours bh24 = [SELECT Id FROM BusinessHours WHERE name = 'B2B Service Hours 24x7'];
        BusinessHours bh12 = [SELECT Id FROM BusinessHours WHERE name = 'B2B Service Hours 12x5'];

        id caseRTid = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Repair').getRecordTypeId();
        id caseEXRTid = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Exchange').getRecordTypeId();

        Id accountRTid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Temporary').getRecordTypeId();
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = accountRTid,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );

        insert testAccount1;

        id productRTid = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('B2B').getRecordTypeId();
        Product2 prod1 = new Product2(
                name ='MI-test',
                SAP_Material_ID__c = 'MI-test',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = productRTid
            );

        insert prod1;
        
        List<Asset> asstList = new List<Asset>();
        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        //insert ast;
        asstList.add(ast);

        Asset ast2 = new Asset(
            name ='Assert-MI-Test2',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        //insert ast2;
        asstList.add(ast2);
        insert asstList;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            BusinessHoursId = bh12.id,
            Units_Allowed__c = 100,
            startdate = system.today()-1
            );
        insert ent;

        List<Contact> conList = new List<Contact>();
        // Insert Contact 
        Contact contact = new Contact(); 
        contact.AccountId = testAccount1.id; 
        contact.Email = 'svcTest@svctest.com'; 
        contact.LastName = 'Named Caller'; 
        contact.Named_Caller__c = true;
        contact.MailingCity = testAccount1.BillingCity;
		contact.MailingCountry	= testAccount1.BillingCountry;
		contact.MailingPostalCode = testAccount1.BillingPostalCode;
		contact.MailingState = testAccount1.BillingState;
		contact.MailingStreet = testAccount1.BillingStreet;
        contact.FirstName = 'Named Caller'; 
        //insert contact;
        conList.add(contact);

        // Insert Contact non-named caller 
        Contact contact1 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest2@svctest.com', 
            LastName = 'Named Caller2',
            Named_Caller__c = true,
            FirstName = 'Named Caller2',
            MailingCity = testAccount1.BillingCity,
		    MailingCountry = testAccount1.BillingCountry,
		    MailingPostalCode = testAccount1.BillingPostalCode,
		    MailingState = testAccount1.BillingState,
		    MailingStreet = testAccount1.BillingStreet);
        //insert contact1; 
        conList.add(contact1);
        
        // Insert Contact no entitlement account 
        Contact contact2 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest3@svctest.com', 
            LastName = 'Non Entitlement',
            Named_Caller__c = true,
            FirstName = 'Named Caller2',
            MailingCity = testAccount1.BillingCity,
		    MailingCountry = testAccount1.BillingCountry,
		    MailingPostalCode = testAccount1.BillingPostalCode,
		    MailingState = testAccount1.BillingState,
		    MailingStreet = testAccount1.BillingStreet);
        //insert contact2;
        conList.add(contact2);
        insert conList;
        
        Test.startTest();

        device__c dev4 = new device__c(
			account__c = testAccount1.id,
			imei__c = '490154203237518',
			entitlement__c=ent.id
			
		);
		insert dev4;

        List<Case> cases = new List<Case>();
        Case c1 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact.id,
            RecordTypeId = caseRTid,
            status='New',
            Parent_Case__c = true,
            Shipping_Address_1__c='100 challenger rd',
			Shipping_City__c='RIDGEFIELD PARK',
			Shipping_Country__c='US',
			Shipping_State__c='NJ',
            Shipping_Zip_Code__c= '07660'
            );
        insert c1;
       
        Case child1 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact.id,
            RecordTypeId = caseRTid,
            status='New',
            parentid = c1.id,
            SerialNumber__c = 'S-12345222',
            Shipping_Address_1__c='100 challenger rd',
			Shipping_City__c='RIDGEFIELD PARK',
			Shipping_Country__c='US',
			Shipping_State__c='NJ',
            Shipping_Zip_Code__c= '07660'
            );
        //insert child1;

         cases.add(child1);

        Case child2 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact.id,
            RecordTypeId = caseEXRTid,
            status='New',
            parentid = c1.id,
            SerialNumber__c = 'S-22',
            POP_Source__c = 'Parent',
            purchase_date__c = system.today(),
            device__c = dev4.id,
            Shipping_Address_1__c='100 challenger rd',
			Shipping_City__c='RIDGEFIELD PARK',
			Shipping_Country__c='US',
			Shipping_State__c='NJ',
            Shipping_Zip_Code__c= '07660'
            );
        //insert child2;
        cases.add(child2);
        
        insert cases;

        PageReference pageRef = Page.SVC_ManageChildCases;

        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('caseId', c1.id);
        ApexPages.StandardController stdCase = new ApexPages.StandardController(c1);
        SVC_ManageChildCases caseCtr = new SVC_ManageChildCases(stdCase);

        system.assertNotEquals(null, caseCtr.caseStatus, 'Should not be null');
        system.assertNotEquals(null, caseCtr.caseDeviceStatus, 'Should not be null');
        system.assertNotEquals(null, caseCtr.integrationStatus, 'Should not be null');
        system.assertNotEquals(null, caseCtr.inboundShippingStatus, 'Should not be null');
        system.assertNotEquals(null, caseCtr.outboundShippingStatus, 'Should not be null');
        //system.assertNotEquals(null, caseCtr.soqlExchange, 'Should not be null');
        //system.assertNotEquals(null, caseCtr.soqlRepair, 'Should not be null');
        system.assertNotEquals(null, caseCtr.sortDir, 'Should not be null');
        system.assertNotEquals(null, caseCtr.sortField, 'Should not be null');
        system.assertNotEquals(null, caseCtr.debugSoql, 'Should not be null');
        
        ApexPages.currentPage().getParameters().put('caseStatus', 'New');
        ApexPages.currentPage().getParameters().put('deviceStatus', 'Valid');
        ApexPages.currentPage().getParameters().put('integrationStatus', 'OK');
        ApexPages.currentPage().getParameters().put('inboundShippingStatus', 'Shipped');
        ApexPages.currentPage().getParameters().put('outboundShippingStatus', 'Shipped');
        PageReference pageRef1 = caseCtr.runSearch();
        caseCtr.toggleSort();

        List<Case> childCases = [select id from case where parentid =:c1.id];

        system.assertNotEquals(null, childCases, 'Should not be null');

        ApexPages.currentPage().getParameters().put('caseStatus', '');
        ApexPages.currentPage().getParameters().put('deviceStatus', '');
        ApexPages.currentPage().getParameters().put('integrationStatus', '');
        ApexPages.currentPage().getParameters().put('inboundShippingStatus', '');
        ApexPages.currentPage().getParameters().put('outboundShippingStatus', '');
        pageRef1 = caseCtr.runSearch();

        List<SVC_ManageChildCases.ChildCaseWrapper> cWraps = caseCtr.childCases;
        system.assertEquals(2, cWraps.size(), 'Should be 2');
        for (SVC_ManageChildCases.ChildCaseWrapper cw:cWraps){
        	cw.currentPage = true;
        }

        ApexPages.currentPage().getParameters().put('clickedcaseID', childCases[0].id);
        ApexPages.currentPage().getParameters().put('selected', 'true');

        caseCtr.checkboxClicked();
        
        caseCtr.toggleSelect();
        caseCtr.toggleSelect();

        system.assertNotEquals(null, caseCtr.selectedCaselist, 'Should not be null');
        

        ApexPages.currentPage().getParameters().put('clickedcaseID', childCases[0].id);
        ApexPages.currentPage().getParameters().put('selected', 'true');

        caseCtr.checkboxClicked();

        ApexPages.currentPage().getParameters().put('clickedcaseID', childCases[1].id);
        ApexPages.currentPage().getParameters().put('selected', 'true');

        caseCtr.checkboxClicked();
        caseCtr.massEdit();

        
        PageReference page1 = caseCtr.save();

        ApexPages.currentPage().getParameters().put('clickedcaseID', childCases[0].id);
        ApexPages.currentPage().getParameters().put('selected', 'true');

        caseCtr.checkboxClicked();

        ApexPages.currentPage().getParameters().put('clickedcaseID', childCases[1].id);
        ApexPages.currentPage().getParameters().put('selected', 'true');

        caseCtr.checkboxClicked();

        caseCtr.cancelSelectedCases();
        page1 = caseCtr.backToList();

	}
	}
}