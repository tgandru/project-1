@isTest
private class SVC_TestNewAssetController
{
    @isTest
    static void itShould()
    {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        RecordType assetRT = [SELECT Id, Name,SobjectType FROM RecordType WHERE Name = 'VAS Asset' 
            AND SobjectType = 'Asset'];
        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        );

        insert testAccount1;

        RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];
        Product2 prod1 = new Product2(
                name ='MI-test',
                SAP_Material_ID__c = 'MI-test',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = productRT.id
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        PageReference pageRef = Page.SVC_NewAsset;
        Test.setCurrentPage(pageRef);

        ApexPages.StandardController sc = new ApexPages.StandardController(ast);

        pageRef.getParameters().put('RecordType',assetRT.id);
        pageRef.getParameters().put('acc_id',testAccount1.id);
        pageRef.getParameters().put('prd_id',prod1.id);
        pageRef.getParameters().put('Name','Test');
        SVC_NewAssetController newAssetoController = new SVC_NewAssetController(sc);
        System.assertNotEquals(null,newAssetoController.pageRedirect());
        
    }
}