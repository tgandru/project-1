@isTest
public class SVC_TestNewPopController
{
    @isTest
    static void testNewPopController()
    {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        User u = [select id,name from user where name = 'Integration User' and profile.name='Integration User' and isactive=true limit 1];
        system.debug('USER Name---------->'+u.name);
        /*User admin1 = new User (
            FirstName = 'Migration',
            LastName = 'User',
            Email = 'admin1seasde@example.com',
            Alias = 'admint1',
            Username = 'admin1seaa@example.com',
            ProfileId = '00e36000000OfjcAAC',
            TimeZoneSidKey = 'America/New_York',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US'
        );
        insert admin1;*/
        System.runAs(u){
        
        //Address insert
        List<Zipcode_Lookup__c> zipList = new List<Zipcode_Lookup__c>(); 
        Zipcode_Lookup__c  zip1 = new Zipcode_Lookup__c(Name='07660',City_Name__c='RIDGEFIELD PARK', Country_Code__c='US', State_Code__c='NJ',State_Name__c='New Jersey');
        zipList.add(zip1);
        
        insert zipList;

        //BusinessHours bh24 = [SELECT Id FROM BusinessHours WHERE name = 'B2B Service Hours 24x7'];
        BusinessHours bh12 = [SELECT Id FROM BusinessHours WHERE name = 'B2B Service Hours 12x5'];

        id caseRTid = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Repair').getRecordTypeId();

        Id accountRTid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Temporary').getRecordTypeId();
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = accountRTid,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );

        insert testAccount1;

        id productRTid = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('B2B').getRecordTypeId();
        Product2 prod1 = new Product2(
                name ='MI-test',
                SAP_Material_ID__c = 'MI-test',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = productRTid
            );

        insert prod1;

        List<Asset> assetList = new List<Asset>();
        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        //insert ast;
        assetList.add(ast);

        Asset ast2 = new Asset(
            name ='Assert-MI-Test2',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        //insert ast2;
        assetList.add(ast2);
        insert assetList;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            BusinessHoursId = bh12.id,
            startdate = system.today()-1
            );
        insert ent;

        // Insert Contact 
        List<contact> conlist = new List<contact>();
        Contact contact = new Contact(); 
        contact.AccountId = testAccount1.id; 
        contact.Email = 'svcTest@svctest.com'; 
        contact.LastName = 'Named Caller'; 
        contact.Named_Caller__c = true;
        contact.MailingCity = testAccount1.BillingCity;
        contact.MailingCountry  = testAccount1.BillingCountry;
        contact.MailingPostalCode = testAccount1.BillingPostalCode;
        contact.MailingState = testAccount1.BillingState;
        contact.MailingStreet = testAccount1.BillingStreet;
        contact.FirstName = 'Named Caller'; 
        //insert contact; 
        conlist.add(contact);
        
        // Insert Contact non-named caller 
        Contact contact1 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest2@svctest.com', 
            LastName = 'Named Caller2',
            Named_Caller__c = true,
            FirstName = 'Named Caller2',
            MailingCity = testAccount1.BillingCity,
            MailingCountry = testAccount1.BillingCountry,
            MailingPostalCode = testAccount1.BillingPostalCode,
            MailingState = testAccount1.BillingState,
            MailingStreet = testAccount1.BillingStreet);
        //insert contact1; 
        conlist.add(contact1);
        
        // Insert Contact no entitlement account 
        Contact contact2 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest3@svctest.com', 
            LastName = 'Non Entitlement',
            Named_Caller__c = true,
            FirstName = 'Named Caller2',
            MailingCity = testAccount1.BillingCity,
            MailingCountry = testAccount1.BillingCountry,
            MailingPostalCode = testAccount1.BillingPostalCode,
            MailingState = testAccount1.BillingState,
            MailingStreet = testAccount1.BillingStreet);
        //insert contact2;
        conlist.add(contact2);
        insert conlist;
        
        Test.startTest();
        List<Case> cases = new List<Case>();
        Case c1 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact.id,
            RecordTypeId = caseRTid,
            status='New',
            Parent_Case__c = true,
            Shipping_Address_1__c='100 challenger rd',
            Shipping_City__c='RIDGEFIELD PARK',
            Shipping_Country__c='US',
            Shipping_State__c='NJ',
            Shipping_Zip_Code__c= '07660'
            );
        insert c1;
       
        /*Case child1 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact.id,
            RecordTypeId = caseRTid,
            status='New',
            parentid = c1.id,
            SerialNumber__c = 'S-12345222',
            Shipping_Address_1__c='100 challenger rd',
            Shipping_City__c='RIDGEFIELD PARK',
            Shipping_Country__c='US',
            Shipping_State__c='NJ',
            Shipping_Zip_Code__c= '07660'
            );
        //insert child1;

         cases.add(child1);

        Case child2 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact.id,
            RecordTypeId = caseRTid,
            status='New',
            parentid = c1.id,
            SerialNumber__c = 'S-22',
            POP_Source__c = 'Parent',
            purchase_date__c = system.today(),
            Shipping_Address_1__c='100 challenger rd',
            Shipping_City__c='RIDGEFIELD PARK',
            Shipping_Country__c='US',
            Shipping_State__c='NJ',
            Shipping_Zip_Code__c= '07660'
            );
        //insert child2;
        cases.add(child2);
        
        insert cases;*/

        Proof_of_purchase__c pop = new Proof_of_purchase__c(case__c=c1.id,purchase_date__c=system.today());
        insert pop;

        PageReference pageRef = Page.SVC_NewPop;

        Test.setCurrentPage(pageRef);
        ApexPages.StandardController stdPop = new ApexPages.StandardController(pop);
        SVC_NewPop popCtr = new SVC_NewPop(stdPop);


        ApexPages.currentPage().getParameters().put('caseid', c1.id);

        stdPop = new ApexPages.StandardController(pop);


        popCtr = new SVC_NewPop(stdPop);


        PageReference pageRef2 = Page.SVC_NewPopCommunity;

        Test.setCurrentPage(pageRef2);
        ApexPages.currentPage().getParameters().put('id', c1.id);

        stdPop = new ApexPages.StandardController(pop);

        

        popCtr = new SVC_NewPop(stdPop);

        popCtr.createPop();
        string cv = 'document';
        Blob cvBlob = Blob.valueOf(cv);
        popCtr.contentFile = cvBlob;
        popCtr.fileName = 'document';
        popCtr.createPop();
        popCtr.backToCase();

        Test.stopTest();

        }
    }
}