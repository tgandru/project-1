/*
author : sungil.kim, I2MAX

Description : for all web-service related logic. For example, building the request, parsing the response, doing the callout.

// Update a Case
CompletionDate 을 Delivered Time으로 Update

// Date
1. URL : http://www.thephani.com/week-date-numbers-apex-salesforce/
2. Day of Week in Month => system.now().format('F')

// GMT
1. 배송일자를 US의 Daylight Time 기간인 경우, GMT 기준에서 +1하고 기간이 아닌 경우 GMT 기준으로 가감한다.

// zipcode download
https://boutell.com/zipcodes/

// 3월 2주째 일요일부터 11월 첫번째 일요일까지

// Daylight Saving Time.

Code for status type of activity.
I = In Transit
D = Delivered
X = Exception
P = Pickup
M = Manifest Pickup


String CRON_EXP = '0 0 * * * ?';// every one hours
System.schedule('UPS delivery status update per one hour', CRON_EXP, new SVC_UPSIntegrationScheduledJob_sc());
*/

global class SVC_UPSIntegrationClass {
	/*
	From the second Sunday in March until the first Sunday in November
	2017-03-12 Sun ~ 2017-11-04 Sat
	2018-03-11 Sun ~ 2018-11-03 Sat
	2019-03-10 Sun ~ 2019-11-02 Sat
	2020-03-08 Sun ~ 2020-10-31 Sat
	*/
	public static Map<String, Integer> dayMap = new Map<String, Integer>();
	public static Map<String, String> statusMap = new Map<String, String>();
	static {
		dayMap.put('Sun', 0);
		dayMap.put('Mon', 1);
		dayMap.put('Tue', 2);
		dayMap.put('Wed', 3);
		dayMap.put('Thu', 4);
		dayMap.put('Fri', 5);
		dayMap.put('Sat', 6);
		//
		statusMap.put('I', 'In Transit');
		statusMap.put('D', 'Delivered');
		statusMap.put('X', 'Exception');
		statusMap.put('P', 'Pickup');
		statusMap.put('M', 'Manifest Pickup');
	}

	public String webcallout(String trackingNumber) {
		External_Integration_EndPoints__c endpoint = External_Integration_EndPoints__c.getInstance('UPS-001');

		String xml = '';
		xml += '<?xml version="1.0" encoding="UTF-8"?>';
		xml += '<AccessRequest xml:lang="en-US">';
		xml += '   <AccessLicenseNumber>' + endpoint.AccessKey__c + '</AccessLicenseNumber>';
		xml += '   <UserId>' + endpoint.Username__c + '</UserId>';
		xml += '   <Password>' + endpoint.Password__c + '</Password>';
		xml += '</AccessRequest>';
		xml += '<TrackRequest xml:lang="en-US">';
		xml += '   <Request>';
		xml += '      <TransactionReference>';
		xml += '         <CustomerContext>sea</CustomerContext>';
		xml += '         <XpciVersion>1.0001</XpciVersion>';
		xml += '      </TransactionReference>';
		xml += '      <RequestAction>Track</RequestAction>';
		xml += '      <RequestOption>activity</RequestOption>';
		xml += '   </Request>';
		xml += '   <TrackingNumber>' + trackingNumber + '</TrackingNumber>';
		xml += '</TrackRequest>';

		system.debug(xml);
	    System.Httprequest req = new System.Httprequest();
	  	//String body = JSON.serialize(upsRequest);

	    req.setMethod('POST');
	    req.setBody(xml);
	    req.setEndpoint(endpoint.EndPointURL__c);
	    req.setTimeout(120000);
	    req.setHeader('Content-Type', 'application/xml');
	    req.setHeader('Accept', 'application/xml');

	    Http http = new Http();
	    HttpResponse res = http.send(req);

	    return (res == null) ? '' : res.getBody();
	}

	/*
	Case.Outbound_Tracking_Number__c : 
	Device.Outbound_Tracking_Number__c :
	*/
	webservice static string updateShipping(id caseId){
		if (caseId == null || ''.equals(caseId)) {
			return 'Case Id is required';
		}

		Case c = [SELECT 
					Id, RecordType.Name,
					Status, Sub_Status__c,
					Inbound_Shipping_Status__c, Inbound_Tracking_Number__c,
					Outbound_Shipping_Status__c, Outbound_Tracking_Number__c 
				FROM Case WHERE Id = :caseId LIMIT 1];
		if (c == null) { // Defence code
			return 'No record with Case Id : ' + caseId;
		}

		List<Tracking> trackingList = new List<Tracking>();

		if (
			'Delivered'.equals(c.Inbound_Shipping_Status__c) == false
			&& c.Inbound_Tracking_Number__c != null
			) {
			Tracking tracking = new Tracking();

			tracking.trackingNumber = c.Inbound_Tracking_Number__c;
			tracking.inout = 'in';
			tracking.isExchageOutbound = false;

			trackingList.add(tracking);
		}

		if (
			'Delivered'.equals(c.Outbound_Shipping_Status__c) == false
			&& c.Outbound_Tracking_Number__c != null
			) {
			Tracking tracking = new Tracking();

			tracking.trackingNumber = c.Outbound_Tracking_Number__c;
			tracking.inout = 'out';
			tracking.isExchageOutbound = ('Exchange'.equals(c.RecordType.Name)) ? true : false;

			trackingList.add(tracking);
		}

		if (trackingList.isEmpty()) {
			return 'Tracking Number is empty or Delivery has already done.';
		}
		system.debug('SVC_UPSIntegrationClass updateShipping call.');

		Boolean isUpdated = false;// for UI fresh.
		Tracking exchangeOutboundDeliveryTracking = null;
		String errorMessage = '';
		for (Tracking t : trackingList) {
			SVC_UPSIntegrationClass controller = new SVC_UPSIntegrationClass();
			String body = controller.webcallout(t.trackingNumber);
			system.debug('response :: ' + body);

			Dom.Document doc = new Dom.Document();
			doc.load(body);

			Dom.XMLNode root = doc.getRootElement(); // TrackResponse
			//String statusCode = root.getChildElement('/Shipment/Package/Activity[1]/Status/StatusType/Code', null).getText();
			Dom.XMLNode errorNode = root.getChildElement('Response', null).getChildElement('Error', null);
			if (errorNode == null) {
				Dom.XMLNode activities = root.getChildElement('Shipment', null).getChildElement('Package', null).getChildElement('Activity', null);
				String statusCode = activities.getChildElement('Status', null).getChildElement('StatusType', null).getChildElement('Code', null).getText();
				system.debug('statusCode :: ' + statusCode);
				String statusVal = statusMap.get(statusCode);

				if ('in'.equals(t.inout)) {
					if (statusVal.equals(c.Inbound_Shipping_Status__c) == false) isUpdated = true;
					c.Inbound_Shipping_Status__c = statusVal;
				} else if ('out'.equals(t.inout)) {
					if (statusVal.equals(c.Outbound_Shipping_Status__c) == false) isUpdated = true;
					c.Outbound_Shipping_Status__c = statusVal;
				}

				if (t.isExchageOutbound && 'D'.equals(statusCode)) {
					String postalCode = activities.getChildElement('ActivityLocation', null).getChildElement('Address', null).getChildElement('PostalCode', null).getText();
					String strDate = activities.getChildElement('Date', null).getText();
					String strTime = activities.getChildElement('Time', null).getText();
					String strDateTime = strDate + strTime;

					t.postalCode = postalCode;
					t.strDateTime = strDateTime;

					exchangeOutboundDeliveryTracking = t;
				}
			} else {
				errorMessage += '[Tracking Number : ' + t.trackingNumber + '] ' + errorNode.getChildElement('ErrorDescription', null).getText() + '\r\n';
			}
		}

		if (SVC_UtilityClass.isEmpty(errorMessage) == false) return 'UPS Error :\r\n ' + errorMessage;

		// When isExchangeOutboundDelivered is true, Update case mile stone.
		List<CaseMilestone> caseMilestoneList = [
			SELECT Id, CompletionDate FROM CaseMilestone WHERE CaseId = :c.Id
			AND MilestoneType.Name = 'Exchange SLA' AND IsCompleted = false
		];
		
		if (exchangeOutboundDeliveryTracking != null && caseMilestoneList != null && caseMilestoneList.isEmpty() == false) {
			String postalCode = exchangeOutboundDeliveryTracking.postalCode;// activities.getChildElement('ActivityLocation', null).getChildElement('Address', null).getChildElement('PostalCode', null).getText();
			//system.debug('postalCode :: ' + postalCode);
			Zipcode_Timezone__c zt = [
				SELECT Id, Zipcode__c, Timezone__c, DST__c 
				FROM Zipcode_Timezone__c
				WHERE Zipcode__c = :postalCode
			];

			String strDateTime = exchangeOutboundDeliveryTracking.strDateTime;
			
			//String dtme = '20170619113700';
			Datetime plainGMT = Datetime.newInstanceGmt(
				Integer.valueOf(strDateTime.substring(0,4)), 
				Integer.valueOf(strDateTime.substring(4,6)),
				Integer.valueOf(strDateTime.substring(6,8)),
				Integer.valueOf(strDateTime.substring(8,10)),
				Integer.valueOf(strDateTime.substring(10,12)),
				Integer.valueOf(strDateTime.substring(12,14))
			);// plain Datetime. -> 2017-06-19 11:37:00

			Decimal timezone = zt.Timezone__c;
			Decimal dst = zt.DST__c;
			Decimal upsOffset = 0;// ups 값과 GMT 값의 차이.

			/*
				우편번호로 검색한 지역이 DST 적용대상고 해당 지역 시간으로 DST 기간인지 확인한다.
				예를 들어 도착지 우편번호가 07072(NJ)고 도착시간이 2017-06-19 11:37:00인 경우 해당 시간이 DST시간인 지 확인한다.
			*/
			if (dst > 0 && isDST(plainGMT)) {
				upsOffset = timezone + dst;
			} else {
				upsOffset = timezone;
			}

			Datetime upsToGMT = plainGMT.addHours((Integer)upsOffset * -1);
			system.debug('upsToGMT :: ' + upsToGMT);

			for (CaseMilestone cm : caseMilestoneList) {
				cm.CompletionDate = upsToGMT;
			}

			update caseMilestoneList;
		}

		system.debug('c.RecordType.Name :: ' + c.RecordType.Name);
		system.debug('c.Status:: ' + c.Status);
		system.debug('c.Inbound_Shipping_Status__c :: ' + c.Inbound_Shipping_Status__c);
		system.debug('c.Outbound_Shipping_Status__c :: ' + c.Outbound_Shipping_Status__c);

		if ('Repair'.equals(c.RecordType.Name)) { //
			if ('Repair Completed'.equals(c.Status) && 'Repair Completed'.equals(c.Sub_Status__c)) {
				if ('Delivered'.equals(c.Outbound_Shipping_Status__c)) {
					c.Status = 'Closed';
					c.Sub_Status__c = null;
				}
			}
		} else if ('Repair To Exchange'.equals(c.RecordType.Name)) {
			if ('Order Shipped'.equals(c.Status) && 'Exchange Completed'.equals(c.Sub_Status__c)) {
				if ('Delivered'.equals(c.Outbound_Shipping_Status__c)) {
					c.Status = 'Closed';
					c.Sub_Status__c = null;
				}
			}
		} else if ('Exchange'.equals(c.RecordType.Name)) {
			if ('Order Shipped'.equals(c.Status) && 'Exchange Completed'.equals(c.Sub_Status__c)) {
				if ('Delivered'.equals(c.Inbound_Shipping_Status__c)) {
					c.Status = 'Closed';
					c.Sub_Status__c = null;
				}
			}
		}

		update c; // closed가 있어 milestone 뒤에 업데이트를 한다.

		return isUpdated ? 'updated' : 'not updated';
	}

	public static boolean isDST(Datetime plainGMT) {
		Integer month = plainGMT.month();
		if (month < 3 || month > 11) { return false; }// -> 1,2 and 12
		if (month > 3 && month < 11) { return true; } // 4,5,6,7,8,9,10
		
		Integer day = plainGMT.day();
		String w = plainGMT.format('E'); // Sun,Mon,Tue,Wed,Thu,Fri,Sat
		Integer dayOfWeek = dayMap.get(w);
		Integer previousSunday = day - dayOfWeek;
		if (month == 3) { return previousSunday >= 8; }

		return previousSunday <= 0;
	}

	private class Tracking {
		public String trackingNumber;
		public String inout;
		public Boolean isExchageOutbound = false;
        public String postalCode;
        public String strDateTime;
	}
}