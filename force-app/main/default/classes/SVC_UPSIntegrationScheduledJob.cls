/*
author : sungil.kim, I2MAX
How to use :
SVC_UPSIntegrationScheduledJob batch = new SVC_UPSIntegrationScheduledJob();
Database.executeBatch(batch, 50);
*/

global class SVC_UPSIntegrationScheduledJob implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts {
    
    global SVC_UPSIntegrationScheduledJob() {}
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = '';

        query += 'Select Id, RecordType.Name, Status, Sub_Status__c, Inbound_Tracking_Number__c, Inbound_Shipping_Status__c, ';
        query += 'Outbound_Tracking_Number__c, Outbound_Shipping_Status__c from Case ';
        query += 'WHERE Status != \'Closed\' ';
        query += 'AND ( ';
        query += '(Inbound_Tracking_Number__c != NULL AND Inbound_Shipping_Status__c != \'Delivered\' ) ';
        query += 'OR (Outbound_Tracking_Number__c != NULL AND Outbound_Shipping_Status__c != \'Delivered\' ) ';
        query += ') ';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Case> scope) {
        system.debug('scope.size() :: ' + scope.size());
        List<CaseWrapper> exchageOutboundDeliveredList = new List<CaseWrapper>();// for outbound.
        List<Case> cList = new List<Case>();
        for (Case c : scope) {
            CaseWrapper cw = updateShipping(c);// update a IN/OUT bound status code.
            if (cw == null) continue;

            Case c1 = cw.c;
            cList.add(c1);

            if (cw.exchangeOutboundDeliveryTracking != null) {
                exchageOutboundDeliveredList.add(cw);
            }
        }

        if (cList.isEmpty()) return;// nothing to be updated.

//        update cList;// update a IN/OUT bound status code.

//        if (exchageOutboundDeliveredList.isEmpty()) return;

        // update case mile stone when outbound is delivered.
        Set<String> postalCodeSet = new Set<String>();
        Map<String, CaseWrapper> exchageOutboundDeliveredMap = new Map<String, CaseWrapper>();

        for (CaseWrapper cw : exchageOutboundDeliveredList) {
            Case c = cw.c;
            postalCodeSet.add(cw.exchangeOutboundDeliveryTracking.postalCode);
            
            exchageOutboundDeliveredMap.put(c.Id, cw);
        }

//        if (exchageOutboundDeliveredMap.isEmpty()) return;// outbound is empty.
        system.debug('exchageOutboundDeliveredMap :: ' + JSON.serialize(exchageOutboundDeliveredMap));

        List<CaseMilestone> cmsList = [
            SELECT Id, CaseId, CompletionDate FROM CaseMilestone WHERE CaseId IN :exchageOutboundDeliveredMap.keySet() 
            AND MilestoneType.Name = 'Exchange SLA' AND IsCompleted = false
        ];

//        if (cmsList.isEmpty()) return;// case mile stone is empty.

        List<Zipcode_Timezone__c> ztList = [
            SELECT Id, Zipcode__c, Timezone__c, DST__c 
            FROM Zipcode_Timezone__c
            WHERE Zipcode__c IN :postalCodeSet
        ];

        Map<String, Zipcode_Timezone__c> zipCodeMap = new Map<String, Zipcode_Timezone__c>();
        for (Zipcode_Timezone__c zt : ztList) {
            zipCodeMap.put(zt.Zipcode__c, zt);
        }

        for (CaseMilestone cms : cmsList) {
            CaseWrapper cw = exchageOutboundDeliveredMap.get(cms.CaseId);
            String postalCode = cw.exchangeOutboundDeliveryTracking.postalCode;
            String strDateTime = cw.exchangeOutboundDeliveryTracking.strDateTime;
            Zipcode_Timezone__c zt = zipCodeMap.get(postalCode);
            system.debug('postalCode :: ' + postalCode);

            //String dtme = '20170619113700';
            Datetime plainGMT = Datetime.newInstanceGmt(
                Integer.valueOf(strDateTime.substring(0,4)), 
                Integer.valueOf(strDateTime.substring(4,6)),
                Integer.valueOf(strDateTime.substring(6,8)),
                Integer.valueOf(strDateTime.substring(8,10)),
                Integer.valueOf(strDateTime.substring(10,12)),
                Integer.valueOf(strDateTime.substring(12,14))
            );// plain Datetime. -> 2017-06-19 11:37:00

            Decimal timezone = zt.Timezone__c;
            Decimal dst = zt.DST__c;
            Decimal upsOffset = 0;// ups 값과 GMT 값의 차이.

            if (dst > 0 && SVC_UPSIntegrationClass.isDST(plainGMT)) {
                upsOffset = timezone + dst;
            } else {
                upsOffset = timezone;
            }

            Datetime upsToGMT = plainGMT.addHours((Integer)upsOffset * -1);
            system.debug('upsToGMT :: ' + upsToGMT);

            cms.CompletionDate = upsToGMT;
        }

        update cmsList;

        for (Case c : cList) {
            if ('Repair'.equals(c.RecordType.Name)) { //
                if ('Repair Completed'.equals(c.Status) && 'Repair Completed'.equals(c.Sub_Status__c)) {
                    if ('Delivered'.equals(c.Outbound_Shipping_Status__c)) {
                        c.Status = 'Closed';
                        c.Sub_Status__c = null;
                    }
                }
            } else if ('Repair To Exchange'.equals(c.RecordType.Name)) {
                if ('Order Shipped'.equals(c.Status) && 'Exchange Completed'.equals(c.Sub_Status__c)) {
                    if ('Delivered'.equals(c.Outbound_Shipping_Status__c)) {
                        c.Status = 'Closed';
                        c.Sub_Status__c = null;
                    }
                }
            } else if ('Exchange'.equals(c.RecordType.Name)) {
                if ('Order Shipped'.equals(c.Status) && 'Exchange Completed'.equals(c.Sub_Status__c)) {
                    if ('Delivered'.equals(c.Inbound_Shipping_Status__c)) {
                        c.Status = 'Closed';
                        c.Sub_Status__c = null;
                    }
                }
            }
        }

        update cList;// closed가 있어 milestone 뒤에 업데이트를 한다.
    }
    
    private CaseWrapper updateShipping(Case c) {
		List<Tracking> trackingList = new List<Tracking>();

		if (
			'Delivered'.equals(c.Inbound_Shipping_Status__c) == false
			&& c.Inbound_Tracking_Number__c != null
			) {
			Tracking tracking = new Tracking();

			tracking.trackingNumber = c.Inbound_Tracking_Number__c;
			tracking.inout = 'in';
			tracking.isExchageOutbound = false;

			trackingList.add(tracking);
		}

		if (
			'Delivered'.equals(c.Outbound_Shipping_Status__c) == false
			&& c.Outbound_Tracking_Number__c != null
			) {
			Tracking tracking = new Tracking();

			tracking.trackingNumber = c.Outbound_Tracking_Number__c;
			tracking.inout = 'out';
			tracking.isExchageOutbound = ('Exchange'.equals(c.RecordType.Name)) ? true : false;

			trackingList.add(tracking);
		}

		if (trackingList.isEmpty()) return null;

		//Tracking exchangeOutboundDeliveryTracking = null;
		
		CaseWrapper cw = new CaseWrapper();

		for (Tracking t : trackingList) {
			SVC_UPSIntegrationClass controller = new SVC_UPSIntegrationClass();
			String body = controller.webcallout(t.trackingNumber);
			system.debug('response :: ' + body);

	        Dom.Document doc = new Dom.Document();
	        doc.load(body);

	        Dom.XMLNode root = doc.getRootElement(); // TrackResponse
	        //String statusCode = root.getChildElement('/Shipment/Package/Activity[1]/Status/StatusType/Code', null).getText();
	        Dom.XMLNode errorNode = root.getChildElement('Response', null).getChildElement('Error', null);

            if (errorNode == null) {
                Dom.XMLNode activities = root.getChildElement('Shipment', null).getChildElement('Package', null).getChildElement('Activity', null);
                String statusCode = activities.getChildElement('Status', null).getChildElement('StatusType', null).getChildElement('Code', null).getText();
                system.debug('statusCode :: ' + statusCode);
                String statusVal = SVC_UPSIntegrationClass.statusMap.get(statusCode);

                if ('in'.equals(t.inout)) {
                    c.Inbound_Shipping_Status__c = statusVal;
                } else if ('out'.equals(t.inout)) {
                    c.Outbound_Shipping_Status__c = statusVal;
                }

                if (t.isExchageOutbound && 'D'.equals(statusCode)) {
                    String postalCode = activities.getChildElement('ActivityLocation', null).getChildElement('Address', null).getChildElement('PostalCode', null).getText();
                    String strDate = activities.getChildElement('Date', null).getText();
                    String strTime = activities.getChildElement('Time', null).getText();
                    String strDateTime = strDate + strTime;

                    t.postalCode = postalCode;
                    t.strDateTime = strDateTime;

                    cw.exchangeOutboundDeliveryTracking = t;
                }
            }
		}

		cw.c = c;

		return cw;
    }

    global void finish(Database.BatchableContext BC) {
        system.debug('finish Call.');
    }

    private class CaseWrapper {
        //public String postalCode;
        //public String strDateTime;
        //public Boolean isOutbound = false;
        public Case c;
        public Tracking exchangeOutboundDeliveryTracking;
    }

   	private class Tracking {
		public String trackingNumber;
		public String inout;
		public Boolean isExchageOutbound = false;
        public String postalCode;
        public String strDateTime;
	}
}