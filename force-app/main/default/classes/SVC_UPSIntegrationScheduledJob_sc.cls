/*
author : sungil.kim, I2MAX
Usage :
String CRON_EXP = '0 0 * * * ?';// every one hours
System.schedule('UPS delivery status update per one hour', CRON_EXP, new SVC_UPSIntegrationScheduledJob_sc());
*/
public with sharing class SVC_UPSIntegrationScheduledJob_sc implements Schedulable {
	public void execute(SchedulableContext sc) {
		SVC_UPSIntegrationScheduledJob batch = new SVC_UPSIntegrationScheduledJob();
		Database.executeBatch(batch, 50); // Because of webcallout limitation(100 per transaction), maximum 50 is limitation.
	}
}