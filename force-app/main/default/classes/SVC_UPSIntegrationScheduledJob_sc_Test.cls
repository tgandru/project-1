/*
author : sungil.kim, I2MAX
 */
@isTest
private class SVC_UPSIntegrationScheduledJob_sc_Test
{
	@isTest
	static void itShould()
	{
        String CRON_EXP = '0 0 0 4 10 ? '  + (System.now().year() + 1) ; // Next Year
        Test.startTest();
        System.schedule('Test UPS delivery status update per one hour', CRON_EXP, new SVC_UPSIntegrationScheduledJob_sc());
		Test.stopTest();
	}
}