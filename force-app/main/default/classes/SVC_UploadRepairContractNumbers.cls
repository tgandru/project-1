public class SVC_UploadRepairContractNumbers {

    public void selectAccount() {
        accountId = contrct .Account__c;
        setAcctValues();
    }

    public Repair_Contracts__c contrct {get; set;}
    public String accountName {get; private set;}
    public Boolean selectAcct {get; private set;}
    public Boolean isPortalUser {get; private set;}
    public String selectedEntitlement {get; set;}
    public Id accountId {get;  set;}
    public transient blob contentFile {get; set;}
    public String fileName {get; set;}
   
    
    public list<ErrorRecord> errorlists {get;set;}
    
     public class ErrorRecord {
        
        public string cnumber {get;set;}
        public string error{get;set;}
        
        public ErrorRecord (string cnum,string err){
           cnumber=cnum;
           error=err;
        }
        
    }

    public List<SelectOption> entitlementOptions {get; private set;}
    public SVC_UploadRepairContractNumbers(){
         contrct =new Repair_Contracts__c();
         accountName = '';
        selectAcct = true;
        entitlementOptions = new List<SelectOption>();
        isPortalUser = false;
         errorlists=new list<ErrorRecord>();
    } 
    
    public void setAcctValues()
    {
        accountName = [SELECT Name FROM Account WHERE Id =: accountId].Name;
            
        List<Entitlement> entList = [SELECT Name, Id FROM Entitlement WHERE AccountId =: accountId AND Status = 'Active'];

        entitlementOptions = new List<SelectOption>();

        for(Entitlement ent: entList)
            entitlementOptions.add(new SelectOption(ent.Id, ent.Name));
    }
    
      public PageReference createData(){
      if(accountId == null)
        {
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please specify an Account before uploading'));
            return null;
        }

        if(String.isBlank(fileName))
        {
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please upload a file'));
            return null;
        }

        if(String.isNotBlank(fileName) && !fileName.contains('.csv'))
        {
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please upload a valid CSV file'));
            return null;
        }
         
         List<Repair_Contracts__c> insertlist=new List<Repair_Contracts__c>();
         String contentFileStr = SVC_CaseFileUploadClass.blobToString(contentFile, 'MS949');
         List<String> UploadStringList = contentFileStr.split('\n');
         
         for(Integer i=1; i<UploadStringList.size(); i++)
        {
           string[] csvRecordData = UploadStringList[i].split(',');
           Repair_Contracts__c rec = new Repair_Contracts__c(Account__c = accountId, Entitlement__c = selectedEntitlement);
           rec.Name=csvRecordData[0];
          insertlist.add(rec);
        }
         
         if(insertlist.size()>0){
           Database.SaveResult[] SR = Database.insert(insertlist, False);
            for(Integer i=0;i<SR.size();i++){
                if(!SR[i].isSuccess()){//only look at failures, or NOT Successes
                     String err= String.Valueof(SR[i].getErrors());
                     if(err.Contains('DUPLICATE_VALUE')){
                     err='Dupliacte Contract Number(Contract Number Already Exists)';
                     }
                     String Cnum=insertlist[i].Name;
                     
                     errorlists.add(new ErrorRecord(Cnum,err));
                }
            }
         
         }
     return null;
      }
  
}