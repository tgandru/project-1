@isTest
private class SVC_UploadRepairContractNumbersTest {

 @testSetup static void setData() {
    Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
    //RecordType
    Map<String, Id> rtMap = new Map<String, Id>();
    for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
      rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
    }
    //Product 
    List<Product2> prodList = new List<Product2>();
    for(Integer i = 0; i < 3; i ++){
      Product2 prod               = new Product2();
      prod.RecordTypeId             = rtMap.get('Product2'+'B2B');
      prod.ProductCode            = 'testProduct' + i;
      prod.Name                = 'testProduct' + i;
      if(i == 0) prod.SC_Service_Type__c     = 'Elite';         //Signature
      if(i == 1) prod.SC_Service_Type__c     = 'Named_caller_exp';   //Other
      if(i == 2) prod.SC_Service_Type__c     = 'Premier';       //Premier
      prod.SAP_Material_ID__c          = 'test' + i;

      prodList.add(prod);
    }
    insert prodList;
    //ZipCode Lookup
    Zipcode_Lookup__c zl    = new Zipcode_Lookup__c();
    zl.Name            = '07650';
    zl.City_Name__c        = 'Palisades Park';
    zl.Country_Code__c      = 'US';
    zl.State_Code__c      = 'NJ';
    zl.State_Name__c      = 'New Jersey';
    insert zl;
    //Account
    List<Account> accList = new List<Account>();
    for(Integer i = 0; i< 1; i++){
      Account acc       = new Account();
      acc.Name         = 'testAcc' + i;
      acc.RecordTypeId     = rtMap.get('Account'+'End_User');
      acc.BillingCity     = 'PALISADES PARK';
      acc.BillingCountry     = 'US';
      acc.BillingPostalCode   = '07650';
      acc.BillingState     = 'NJ';
      acc.BillingStreet     = '411 E Brinkerhoff Ave, Palisades Park';

      accList.add(acc);
    }
    insert accList;
    //Contact
    List<Contact> conList = new List<Contact>();
    for(Integer i = 0; i<3; i++){
      Contact con       = new Contact();
      con.RecordTypeId    = rtMap.get('Contact'+'SEA_Service_Contact');
      con.LastName      = 'conTest' + i;
      con.AccountId      = accList[0].Id;
      con.Email        = i+'test@aa.com';
      con.Named_Caller__c    = true;
      con.FirstName = 'Named Caller'+i;
          con.MailingStreet = '1234 Main';
          con.MailingCity = 'Chicago';
          con.MailingState = 'IL';
          con.MailingCountry = 'US';

      conList.add(con);    

    }
    insert conList;
    //Asset
    List<Asset> assList = new List<Asset>();
    for(Integer i = 0; i<3; i++){
      Asset ass         = new Asset();
      ass.Name         = 'testAsset' + i;
      ass.AccountId       = accList[0].Id;
      ass.Product2Id       = prodList[i].Id;
      ass.Quantity      = 1;

      assList.add(ass);

    }
    insert assList;

    //Entitlement
    List<Entitlement> entList = new List<Entitlement>();
    for(Integer i = 0; i<1; i++){
      Entitlement ent   = new Entitlement();
      ent.Name      = 'testEntName' + i;
      ent.AssetId      = assList[i].Id;
      ent.AccountId    = accList[0].Id;
      ent.Startdate     = system.today()-1;
      //ent.EndDate      = Date.today()+1;
      ent.Helper_Status__c = 'Active';
      ent.Units_Allowed__c = 500;

      entList.add(ent);
    }
    insert entList;
    
  }
  static testmethod void UploadTest() {
    Account acc = [SELECT Id, Name FROM Account LIMIT 1];
    Entitlement ent  = [SELECT Id FROM Entitlement LIMIT 1];

    SVC_UploadRepairContractNumbers svc = new SVC_UploadRepairContractNumbers();

    svc.createData();
   
    SVC_UploadRepairContractNumbers svc2 = new SVC_UploadRepairContractNumbers();
    svc2.accountId=acc.Id;
    svc2.selectedEntitlement=ent.id;
    svc2.fileName = 'test.xls';
    svc2.createData();

    svc2.selectedEntitlement = ent.Id;
    String blobCreator = '';
    blobCreator += 'Contract Number \n';
    blobCreator += '352328081365687\n';
    blobCreator += '352328081365685\n';
    blobCreator += '352328081365685\n';
    blobCreator += '352328081365682\n';
    svc2.contentFile = blob.valueOf(blobCreator);
    svc2.fileName = 'test.csv';
    svc2.setAcctValues();
    svc2.createData();

  }
  
  
    static testmethod void UploadTest2() {
    Account acc = [SELECT Id, Name FROM Account LIMIT 1];
    Entitlement ent  = [SELECT Id FROM Entitlement LIMIT 1];

    SVC_UploadRepairContractNumbers svc = new SVC_UploadRepairContractNumbers();

    svc.createData();
   
    SVC_UploadRepairContractNumbers svc2 = new SVC_UploadRepairContractNumbers();
    svc2.accountId=acc.Id;
    svc2.selectedEntitlement=ent.id;
  
    svc2.createData();

  }
}