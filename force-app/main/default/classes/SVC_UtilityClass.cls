/*
author : sungil.kim, I2MAX
 */
global without sharing class SVC_UtilityClass {
//  public static boolean runCaseShare;
    public static User integrationUser;
    static {
        integrationUser = [Select Id, Username from User where Username like 'sfdc_if@samsung.com%' LIMIT 1];// Sandbox and Production.
    }
    /**
     * e.g.) Util.formatDate('20160816') --> '2016-08-16'
     **/
    global static String formatDate(String d) {
        return formatDate(d, '-');
    }

    /**
     * Date format
     * e.g.) 20160708 -> 2016/07/08, 2016-07-08, 2016.07.08
     **/
    global static String formatDate(String d, String delimiter) {
        if (d == null || d.length() != 8) return d; // 8 자리일 경우에만 해당

        String yyyy = d.substring(0, 4);
        String mm = d.substring(4, 6);
        String dd = d.substring(6, 8);
        return yyyy + delimiter + mm + delimiter + dd;
    }

    global static String formatDate(Date d) {
        return formatDate(d, '-');
    }

    /**
     * e.g.) Util.dateFormat(Date.today(), '-') --> '2015-04-21'
     **/
    global static String formatDate(Date d, String delimiter) {
        if (d == null) return null;

        return String.valueOf(d.year()) + delimiter + lpad(String.valueOf(d.month()), '0', 2) + delimiter + lpad(String.valueOf(d.day()), '0', 2);
    }

    /**
     * e.g.) lpad('123', '0', 5) --> '00123'
     **/
    global static String lpad(String s, Integer l) {
        if (s == null) return null;

        String p = '0';
        while(s.length() < l) {
            s = p + s;
        }
        return s;
    }

    global static String lpad(String s, String p, Integer l) {
        if (s == null) return null;

        while(s.length() < l) {
            s = p + s;
        }
        return s;
    }

    global static Boolean isEmpty(String s) {
        if (s == null || s == '') return true;
        if ('0000-00-00'.equals(s)) return true;
        if ('- -'.equals(s)) return true;

        return false;
    }

//  global static Boolean isEmptyDate(String s) {
//      if (s == null || ''.equals(s.trim())) return true;
//
//      if ('0000-00-00'.equals(s)) return true;
//      if ('- -'.equals(s)) return true;
//
//      return false;
//  }

    global static String substring(String s, Integer startIndex, Integer endIndex) {
        if (isEmpty(s)) return null;

        if (startIndex > s.length()) return '';

        if (endIndex > s.length()) {
            endIndex = s.length();
        }
        return s.substring(startIndex, endIndex);
    }

    global static String nvl(String s) {
        return (s == null) ? '' : s;
    }

    /**
     * e.g.) Util.str2Dt('20150825') -->
     * e.g.) Util.str2Dt('2015-08-25') -->
     **/
    global static Date str2Date(String str) {
        Date d;
        if(str!=null) {
            if ('0000-00-00'.equals(str)) return null;
            if ('- -'.equals(str)) return null;
            try{
                d = Date.parse(str);
            }catch(System.TypeException te) {
                if(str.length()==8) {
                    d = Date.newInstance(Integer.valueOf(str.left(4)), Integer.valueOf(str.substring(4,6)), Integer.valueOf(str.right(2)));
                } else if(str.length()==10) {
                    d = Date.newInstance(Integer.valueOf(str.left(4)), Integer.valueOf(str.substring(5,7)), Integer.valueOf(str.right(2)));
                }
            }
        }
        return d;
    }

    /**
     * e.g.) Util.str2Dt('201508251137') -->
     * e.g.) Util.str2Dt('20150825113723') -->
     **/
    global static DateTime str2Datetime(String str) {
        DateTime dt;
        if(str!=null) {
            if(str.length()==12) {
                dt = DateTime.newInstance(Integer.valueOf(str.left(4)), Integer.valueOf(str.substring(4,6)), Integer.valueOf(str.substring(6,8))
                        , Integer.valueOf(str.substring(8,10)), Integer.valueOf(str.right(2)), 0);
            } else if(str.length()==14) {
                dt = DateTime.newInstance(Integer.valueOf(str.left(4)), Integer.valueOf(str.substring(4,6)), Integer.valueOf(str.substring(6,8))
                        , Integer.valueOf(str.substring(8,10)), Integer.valueOf(str.substring(10,12)), Integer.valueOf(str.right(2)));
            }
        }
        return dt;
    }

//  global static void resolveCase(Case c) {
////        c.isClosed = true;
////        c.ClosedDate = Datetime.now();
//      c.Status = 'Resolved';

//      update c;
//  }

//  global static void closeCase(Case c) {
////        c.isClosed = true;
////        c.ClosedDate = Datetime.now();
//      c.Status = 'Closed';

//      update c;
//  }

    global static void postToChatter(String caseId, String chatterMessage) {
        // Parent Id
        FeedItem post = new FeedItem();
        
        post.ParentId = caseId;//
        post.Body = chatterMessage;//'Batch is completed successfully.';
        post.Type = 'ContentPost';
        post.CreatedById = integrationUser.Id;

        insert post;
    }

    global static void sendEmail(String displayName, String subject, String body) {
        List<Messaging.SingleEmailMessage> mails =  new List<Messaging.SingleEmailMessage>();

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        List<String> to = new List<String>();
        to.add(UserInfo.getUserEmail());

        mail.setToAddresses(to); // List<String> param1

        String supportEmailCCAddress = Label.CC_Email_address_for_SAM_Notification;
        List<String> supportEmailCCAddressList = new List<String>();

        if(String.isNotBlank(supportEmailCCAddress))
            supportEmailCCAddressList = supportEmailCCAddress.split(',');

        if (supportEmailCCAddressList.isEmpty() == false) {
            mail.setCcAddresses(supportEmailCCAddressList);
        }

        mail.setReplyTo(UserInfo.getUserEmail());
        mail.setSenderDisplayName(displayName);
        mail.setSubject(subject);

        mail.setPlainTextBody(body);

        mails.add(mail);

        Messaging.sendEmail(mails);
    }
}