/**
 * Created by ms on 2017-07-25.
 * author : sungil.kim, I2MAX
 */

@IsTest
private class SVC_UtilityClass_Test {
    @testVisible private static User admin1;
    @testVisible private static User admin2;    
    @testVisible private static Zipcode_Lookup__c zl;
    @testVisible private static Account account;
    @testVisible private static Contact contact;
    @testVisible private static Product2 prod1;
    @testVisible private static Product2 prod2;
    @testVisible private static Asset ast;
    @testVisible private static Entitlement ent;
    @testVisible private static Device__c device;
    @testVisible private static Level_3_Symptom_Code__c level3;
    @testVisible private static Map<String, Id> rtMap;

    @testSetup static void methodName() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='SVC-09';
        iep2.endPointURL__c = 'https://www.google.com';
        iep2.layout_id__c = 'LAY024197';
        iep2.partial_endpoint__c = '/SFDC_IF_012';
        insert iep2;

        External_Integration_EndPoints__c a = new External_Integration_EndPoints__c();
        
        a.Name = 'UPS-001';
        a.Username__c = 'a';
        a.Password__c = 'a';
        a.AccessKey__c = 'a';
        a.EndPointURL__c = 'https://wwwcie.ups.com/ups.app/xml/Track';
        insert a;

        Profile adminProfile = [SELECT Id FROM Profile Where Name ='B2B Service System Administrator'];

        admin1 = new User (
            FirstName = 'admin1',
            LastName = 'admin1',
            Email = 'admin1seasde@example.com',
            Alias = 'admint1',
            Username = 'SvcAdmin914@example.ss.com',
            ProfileId = adminProfile.Id,
            TimeZoneSidKey = 'America/New_York',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US'
        );
        
        insert admin1;

        User admin2 = new User (
            FirstName = 'admin2',
            LastName = 'admin2',
            Email = 'admin2@example.com',
            Alias = 'admin2',
            Username = '914adminsvc@ss.samsung.com',
            ProfileId = adminProfile.Id,
            TimeZoneSidKey = 'America/New_York',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US'
        );
        insert admin2;

        zl = new Zipcode_Lookup__c();
        zl.Name = '07650';
        zl.City_Name__c = 'Palisades Park';
        zl.Country_Code__c = 'US';
        zl.State_Code__c = 'NJ';
        zl.State_Name__c = 'New Jersey';
        insert zl;

        // for timezone and DST
        Zipcode_Timezone__c zt = new Zipcode_Timezone__c();
        
        zt.Zipcode__c = '15147'; // sync with XML response
        zt.Timezone__c = -5;
        zt.DST__c = 1;
        
        insert zt;
        
        account = new Account (
            Name = 'TestAcc1',
            RecordTypeId = rtMap.get('Account' + 'End_User'),//accountRT.Id,
            Type = 'Customer',
            BillingStreet = '411 E Brinkerhoff Ave, Palisades Park',
            BillingCity ='PALISADES PARK',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07650'
        );
        insert account;

        // Insert Contact 
        contact = new Contact(); 
        contact.AccountId = account.id; 
        contact.Email = 'svcTest@svctest.com'; 
        contact.LastName = 'Named Caller'; 
        contact.Named_Caller__c = true;
        contact.FirstName = 'Named Caller';
        contact.MailingStreet = 'Palisades Park';
        contact.MailingCity = 'New Jersey';
        contact.MailingState = 'NJ';
        contact.MailingCountry = 'US';
        insert contact; 

        prod1 = new Product2(
                Name ='MI-OVCPAC',
                SAP_Material_ID__c = 'SM-G900TZKATMB',
                PET_Name__c = 'Elite Plus Advanced Exchange',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = rtMap.get('Product2' + 'B2B')
            );
        insert prod1;

        ast = new Asset(
            Name ='MI-OVCPAB',
            Accountid = account.id,
            //RecordTypeId = assetRT.Id,
            Product2Id = prod1.id
            );
        insert ast;

        Slaprocess sla = [select id from SlaProcess where name LIKE 'Exchange SLA%'
            and isActive = true limit 1 ];

        BusinessHours bh12 = [SELECT Id FROM BusinessHours WHERE name = 'B2B Service Hours 12x5'];

        ent = new Entitlement(
            name ='test Entitlement',
            accountid = account.id,
            assetid = ast.id,
            Units_Allowed__c = 100,
            Units_Exchanged__c = 0,
            BusinessHoursId = bh12.id,
            slaprocessid = sla.id,
            startdate = system.today()-1
            );
        insert ent;

        // Device
        device = new Device__c();
        device.Account__c = account.Id;
        //device.Device_Type_f__c = 'Connected'; //Removed because Device Type was changed to formula field
        device.Entitlement__c = ent.Id;
        device.IMEI__c = '84561506582433';
        device.Model_Name__c = 'SM-G900TZKATMB';
        device.Serial_Number__c = 'R52J20VMLSM';
        device.Status__c = 'New';

        insert device;

        device.Status__c = 'Registered';

        update device;

        level3 = new Level_3_Symptom_Code__c();
        
        level3.Symptom_Type__c = 'HHP_HHP (Cell Phone)';
        level3.Level_1_Symptom_Name__c = 'L1 (Accessories)';
        level3.Level_2_Symptom_Name__c = '01 (3rd Party (Non OEM))';
        level3.Name = '01 (3rd Party Battery)';

        insert level3;

        Case c1 = new Case(
            AccountId = account.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Web',
            Severity__c = '4-Low',
            ownerid = admin1.id,
            Device__c = device.Id,
            Parent_Case__c = false,
            RecordTypeId = rtMap.get('Case' + 'Exchange'),
            //Device_Type_f__c = 'Connected', //Removed because Device Type was changed to formula field
            IMEI__c = '351955080300080',
            EntitlementId = ent.Id,
            ContactId = contact.id,
            Inbound_Shipping_Status__c = 'Shipped',
            Inbound_Tracking_Number__c = '1234567890',
            Outbound_Tracking_Number__c = '1234567890',
            // address
            Shipping_Address_1__c = '411 E Brinkerhoff Ave, Palisades Park', 
            Shipping_City__c = 'PALISADES PARK', 
            Shipping_Country__c = 'US', 
            Shipping_State__c = 'NJ', 
            Shipping_Zip_Code__c = '07650',
            status='New'
            );
        insert c1;
        //update c1;// for milestone

    }

    static testMethod void testBehavior() {
        SVC_UtilityClass.formatDate('20170725');
        SVC_UtilityClass.formatDate('20170725', '-');
        SVC_UtilityClass.formatDate(Date.newInstance(2017, 7, 25));
        SVC_UtilityClass.formatDate(Date.newInstance(2017, 7, 25), '-');
        SVC_UtilityClass.lpad('X', 2);
        SVC_UtilityClass.lpad('X', '0', 2);
        SVC_UtilityClass.isEmpty('20170725');
        SVC_UtilityClass.substring('20170725', 1, 2);
        SVC_UtilityClass.nvl('20170725');
        SVC_UtilityClass.str2Date('20170725');
        SVC_UtilityClass.str2Datetime('20150825113723');

        Case c1 = [SELECT Id, Ownerid, RecordTypeId, AccountId, ContactId FROM Case LIMIT 1];
        SVC_UtilityClass.postToChatter(c1.Id, 'body');
        
        SVC_UtilityClass.sendEmail('xxx', 'xxx', 'xxx');
    }
}