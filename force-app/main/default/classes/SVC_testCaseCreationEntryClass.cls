@isTest
private class SVC_testCaseCreationEntryClass {
    @isTest static void repairCreationTest() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        //RecordType
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = rtMap.get('Account' + 'Temporary'),
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        );
        insert testAccount1;

        Product2 prod1 = new Product2(
                name ='MI-test',
                SAP_Material_ID__c = 'MI-test',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = rtMap.get('Product2'+'B2B')
            );
        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent;

        // Insert Contact non-named caller 
        Contact contact1 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest2@svctest.com', 
            LastName = 'Named Caller2',
            Named_Caller__c = true,
            FirstName = 'Named Caller',
            MailingStreet = '1234 Main',
            MailingCity = 'Chicago',
            MailingState = 'IL',
            MailingCountry = 'US');
        insert contact1;

        //repair serial
        Case c1 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Repair'),
            status='New',
            Parent_Case__c = true
            );
        insert c1;

        Test.startTest();

        ApexPages.currentPage().getParameters().put('id', c1.id);
        ApexPages.currentPage().getParameters().put('caseRecordType', 'Repair');
        SVC_CaseCreationEntryClass svc = new SVC_CaseCreationEntryClass();

        svc.parentId = null;
        svc.rtName = 'Repair';
        svc.doSave();
        svc.parentId = c1.id;
        svc.doAddCase();
        svc.dataList[0].ca.IMEI__c = '12345678910234';
        svc.doSave();
        svc.dataList[0].ca.SerialNumber__c = '1234567891023t';
        svc.doSave();
        svc.dataList[1].ca.IMEI__c = '123456789101112';
        svc.doSave();
        svc.dataList[0].ca.SerialNumber__c = '1234567891023t';
        svc.dataList[1].ca.SerialNumber__c = '1234567891023t';
        svc.doSave();
        svc.dataList[0].ca.IMEI__c = '12345678910235';
        svc.dataList[1].ca.IMEI__c = '12345678910235';
        svc.doSave();
        svc.doDone();

        ApexPages.currentPage().getParameters().put('id', c1.id);
        ApexPages.currentPage().getParameters().put('caseRecordType', 'Repair');
        SVC_CaseCreationEntryClass svc2 = new SVC_CaseCreationEntryClass();
        svc2.parentId = c1.id;
        svc2.dataList[0].ca.IMEI__c = '12345678910234';
        svc2.doSave();

        ApexPages.currentPage().getParameters().put('id', c1.id);
        ApexPages.currentPage().getParameters().put('caseRecordType', 'Repair');
        SVC_CaseCreationEntryClass svc3 = new SVC_CaseCreationEntryClass();
        svc3.parentId = c1.id;
        svc3.dataList[0].ca.SerialNumber__c = 'SB345678910234';
        svc3.doSave();

        ApexPages.currentPage().getParameters().put('id', c1.id);
        ApexPages.currentPage().getParameters().put('caseRecordType', 'Repair');
        SVC_CaseCreationEntryClass svc4 = new SVC_CaseCreationEntryClass();
        svc4.parentId = c1.id;
        svc4.dataList[0].ca.SerialNumber__c = 'SB345678910234';
        svc4.dataList[0].ca.Model_Code_Repair__c = 'test';
        svc4.doSave();

        Test.stopTest();
    }

    @isTest static void exchangeCreationTest() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        //RecordType
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = rtMap.get('Account' + 'Temporary'),
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        );
        insert testAccount1;

        Product2 prod1 = new Product2(
                name ='MI-test',
                SAP_Material_ID__c = 'MI-test',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = rtMap.get('Product2'+'B2B')
            );
        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent;

        // Insert Contact non-named caller 
        Contact contact1 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest2@svctest.com', 
            LastName = 'Named Caller2',
            Named_Caller__c = true,
            FirstName = 'Named Caller',
            MailingStreet = '1234 Main',
            MailingCity = 'Chicago',
            MailingState = 'IL',
            MailingCountry = 'US');
        insert contact1;
        
        Device__c dev1 = new Device__c(
            Account__c = testAccount1.id,
            Entitlement__c = ent.id,
            Status__c = 'Registered',
            Imei__c = '351955080300551'
            );
        insert dev1;

        Device__c dev2 = new Device__c(
            Account__c = testAccount1.id,
            Entitlement__c = ent.id,
            Status__c = 'Registered',
            Serial_Number__c = 'SB1955080300551',
            Model_Name__c = 'MI-test'
            );
        insert dev2;        
        Test.startTest();
        //repair serial
        Case c1 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Exchange'),
            status='New',
            Parent_Case__c = true
            );
        insert c1;

        
        ApexPages.currentPage().getParameters().put('id', c1.id);
        ApexPages.currentPage().getParameters().put('caseRecordType', 'Exchange');
        SVC_CaseCreationEntryClass svc = new SVC_CaseCreationEntryClass();
        svc.parentId = c1.id;
        svc.rtName = 'Exchange';
        svc.dataList[0].ca.IMEI__c = '351955080300551';
        svc.dataList[0].ca.SerialNumber__c = '';
        svc.doSave();

        ApexPages.currentPage().getParameters().put('id', c1.id);
        ApexPages.currentPage().getParameters().put('caseRecordType', 'Exchange');
        SVC_CaseCreationEntryClass svc2 = new SVC_CaseCreationEntryClass();
        
        svc2.parentId = null;
        svc2.rtName = 'Exchange';
        svc2.doSave();
        svc2.parentId = c1.id;
        svc2.doAddCase();
        svc2.dataList[0].ca.IMEI__c = '12345678910234';
        svc2.doSave();
        svc2.dataList[0].ca.SerialNumber__c = '1234567891023t';
        svc2.doSave();
        svc2.dataList[1].ca.IMEI__c = '351955080300551';
        svc2.doSave();
        svc.dataList[0].ca.IMEI__c = '12345678910234';
        svc2.dataList[0].ca.SerialNumber__c = '';
        svc2.doSave();

        ApexPages.currentPage().getParameters().put('id', c1.id);
        ApexPages.currentPage().getParameters().put('caseRecordType', 'Exchange');
        SVC_CaseCreationEntryClass svc3 = new SVC_CaseCreationEntryClass();
        svc3.parentId = c1.id;
        svc3.rtName = 'Exchange';
        svc3.dataList[0].ca.IMEI__c = '1';
        svc3.dataList[0].ca.SerialNumber__c = '';
        svc3.doSave();

        ApexPages.currentPage().getParameters().put('id', c1.id);
        ApexPages.currentPage().getParameters().put('caseRecordType', 'Exchange');
        SVC_CaseCreationEntryClass svc4 = new SVC_CaseCreationEntryClass();
        svc4.parentId = c1.id;
        svc4.rtName = 'Exchange';
        svc4.dataList[0].ca.SerialNumber__c = 'SB1955080300551';
        svc4.doSave();

        Test.stopTest();

    }
}