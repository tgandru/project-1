@isTest
private class SVC_testCaseEmailActionHandler {
    
    @isTest static void testCaseEmailActionHandler() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );

        insert testAccount1;

        // Insert Contact 
        Contact contact = new Contact(); 
        contact.AccountId = testAccount1.id; 
        contact.Email = 'Test@test.com'; 
        contact.LastName = 'TestLastName';
        contact.FirstName = 'FirstName';
        contact.Named_Caller__c = true;
        contact.MailingCity         = testAccount1.BillingCity;
        contact.MailingCountry      = testAccount1.BillingCountry;
        contact.MailingPostalCode   = testAccount1.BillingPostalCode;
        contact.MailingState        = testAccount1.BillingState;
        contact.MailingStreet       = testAccount1.BillingStreet;
        insert contact; 


        Case case1 = new Case(); 
        case1.AccountId = testAccount1.id; 
        case1.Subject = 'test case'; 
        case1.reason = 'test@test.com'; 
        case1.Origin = 'Web';
        insert case1; 

        EmailMessage msg1 = new EmailMessage(
            subject = 'reply',
            ParentId = case1.id,
            FromAddress = 'customer@customer.com',
            toaddress = 'test@sfdc.com',
            incoming = true);
        insert msg1;

        Case case2 = new Case(); 
        case1.AccountId = testAccount1.id; 
        case1.Subject = 'test case2'; 
        case1.reason = 'test2@test.com'; 
        case1.Origin = 'Phone';
        insert case2; 

        EmailMessage msg2 = new EmailMessage(
            subject = 'reply',
            ParentId = case2.id,
            FromAddress = 'customer@customer.com',
            toaddress = 'test2@sfdc.com',
            incoming = true);
        insert msg2;


        List<Map<String, Object>> defaultSettingAsObject = new List<Map<String, Object>>
{
  new Map<String, Object>
  {
        'targetSObject' => new EmailMessage(),
        'contextId' => case1.id,
        'actionType' => 'Email',
        'actionName' => 'Case.Email',
        'fromAddressList' => new List<String> { 'salesforce@test.com' }
  }
};
    Exception failureDuringExecution = null;
    List<QuickAction.SendEmailQuickActionDefaults> defaultsSettings = 
    (List<QuickAction.SendEmailQuickActionDefaults>)JSON.deserialize(JSON.serialize(defaultSettingAsObject), List<QuickAction.SendEmailQuickActionDefaults>.class);
    Test.startTest();
    try { (new CaseEmailActionHandler()).onInitDefaults(defaultsSettings); }
    catch(Exception failure) { failureDuringExecution = failure; }

   // Test.stopTest();

        QuickAction.SendEmailQuickActionDefaults sendEmailDefaults = 
        TEST.newSendEmailQuickActionDefaults(case1.Id, msg1.id);
        List<QuickAction.SendEmailQuickActionDefaults> defaultsSettings1 = 
        new List<QuickAction.SendEmailQuickActionDefaults>();
        defaultsSettings1.add(sendEmailDefaults);


    //  Test.startTest();
        //CaseEmailTemplateSelector cntl = new CaseEmailTemplateSelector();
        CaseEmailActionHandler emailPublisher = new CaseEmailActionHandler();
        emailPublisher.onInitDefaults(defaultsSettings1);

        QuickAction.SendEmailQuickActionDefaults sendEmailDefaults2 = 
        TEST.newSendEmailQuickActionDefaults(case2.Id, msg2.id);
        List<QuickAction.SendEmailQuickActionDefaults> defaultsSettings2 = 
        new List<QuickAction.SendEmailQuickActionDefaults>();
        defaultsSettings2.add(sendEmailDefaults2);


    //  Test.startTest();
        //CaseEmailTemplateSelector cntl = new CaseEmailTemplateSelector();
        CaseEmailActionHandler emailPublisher2 = new CaseEmailActionHandler();
        emailPublisher2.onInitDefaults(defaultsSettings2);


        Test.stopTest();
        EmailMessage emailMessage = (EmailMessage) sendEmailDefaults.getTargetSObject();
        EmailMessage emailMessage2 = (EmailMessage) sendEmailDefaults2.getTargetSObject();
    
    }
}