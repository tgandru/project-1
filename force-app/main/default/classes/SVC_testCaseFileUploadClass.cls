@isTest
private class SVC_testCaseFileUploadClass {

    @isTest static void repairUploadTest() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        //RecordType
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = rtMap.get('Account' + 'Temporary'),
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        );
        insert testAccount1;

        Product2 prod1 = new Product2(
                name ='MI-test',
                SAP_Material_ID__c = 'MI-test',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = rtMap.get('Product2'+'B2B')
            );
        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent;

        // Insert Contact non-named caller 
        Contact contact1 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest2@svctest.com', 
            LastName = 'Named Caller2',
            Named_Caller__c = true,
            FirstName = 'Named Caller',
            MailingStreet = '1234 Main',
            MailingCity = 'Chicago',
            MailingState = 'IL',
            MailingCountry = 'US');
        insert contact1;

        //repair serial
        Case c1 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Repair'),
            status='New',
            Parent_Case__c = true
            );
        insert c1;

        Test.startTest();
        ApexPages.currentPage().getParameters().put('id', c1.id);
        ApexPages.currentPage().getParameters().put('caseRecordType', 'Repair');
        SVC_CaseFileUploadClass svc = new SVC_CaseFileUploadClass();
        svc.parentId = null;
        svc.fileName = '';
        svc.uploadFile();
        svc.rtName = 'Repair';

        svc.parentId = c1.id;
        svc.fileName = '';
        svc.uploadFile();
        svc.fileName = 'test.xls';
        svc.uploadFile();

        String blobCreator = '';
        blobCreator += 'IMEI,SerialNumber,ModelName,Desc\n';
        blobCreator += '352328081365687,,,\n';
        blobCreator += '352328081365685,,,\n';
        blobCreator += '352328081365685,,,\n';
        blobCreator += '352328081365682,,,\n';
        blobCreator += ',testserial_1,,\n';
        blobCreator += ',testserial_2,,\n';
        blobCreator += ',testserial_2,,\n';
        svc.contentFile = blob.valueOf(blobCreator);
        svc.fileName = 'test.csv';
        svc.uploadFile();
        svc.doDone();
        Test.stopTest();
    }
    @isTest static void exchangeUploadTest() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        //RecordType
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = rtMap.get('Account' + 'Temporary'),
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        );
        insert testAccount1;

        Product2 prod1 = new Product2(
                name ='MI-test',
                SAP_Material_ID__c = 'MI-test',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = rtMap.get('Product2'+'B2B')
            );
        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent;

        // Insert Contact non-named caller 
        Contact contact1 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest2@svctest.com', 
            LastName = 'Named Caller2',
            Named_Caller__c = true,
            FirstName = 'Named Caller',
            MailingStreet = '1234 Main',
            MailingCity = 'Chicago',
            MailingState = 'IL',
            MailingCountry = 'US');
        insert contact1;
        
        Device__c dev1 = new Device__c(
            Account__c = testAccount1.id,
            Entitlement__c = ent.id,
            Status__c = 'Registered',
            Imei__c = '351955080300551'
            );
        insert dev1;
        
        

        Device__c dev2 = new Device__c(
            Account__c = testAccount1.id,
            Entitlement__c = ent.id,
            Status__c = 'Registered',
            Serial_Number__c = 'SB1955080300551',
            Model_Name__c = 'MI-test'
            );
        insert dev2;        
        Test.startTest();
        //repair serial
        list<Device__c> d=new list<Device__c>([select id,name,Status__c,Imei__c from Device__c ]);
        for(Device__c dv:d){
        dv.Status__c='Registered';}
        update d;
        Case c1 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Exchange'),
            status='New',
            Parent_Case__c = true
            );
        insert c1;

        
        //Exchange
        ApexPages.currentPage().getParameters().put('id', c1.id);
        ApexPages.currentPage().getParameters().put('caseRecordType', 'Exchange');
        SVC_CaseFileUploadClass svc2 = new SVC_CaseFileUploadClass();
        
        svc2.parentId = c1.id;
        svc2.rtName = 'Exchange';
        String blobCreator = '';
        blobCreator += 'IMEI,SerialNumber,ModelName,Desc\n';
        blobCreator += '351955080300550,,,\n';
        blobCreator += '351955080300551,,,\n';
        blobCreator += ',testserial_2,,\n';
        blobCreator += ',testserial_3,,\n';
        svc2.contentFile = blob.valueOf(blobCreator);
        svc2.fileName = 'test.csv';
        svc2.uploadFile();

        blobCreator = '';
        blobCreator += 'IMEI,SerialNumber,ModelName,Desc\n';
        blobCreator += '351955080300555,,,\n';
        blobCreator += ',SB1955080300551,MI-test,\n';
        svc2.contentFile = blob.valueOf(blobCreator);
        svc2.fileName = 'test.csv';
        svc2.uploadFile();

        Test.stopTest();
    }
}