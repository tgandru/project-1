@isTest
private class SVC_testCaseFollowUpCaseCloneController {

    static User user1;
    static Account acct;
    static Contact contact1;

    @testSetup static void testDataSetup(){
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();   
        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
    
        // setup an account for the case
        acct = new Account(
                Name = 'Stratix',
                Type = 'Customer',
                RecordTypeId=accountRT.id,
                BillingStreet = '85 Challenger Rd',
                BillingCity ='Ridgefield Park',
                BillingState ='NJ',
                BillingCountry='US',
                BillingPostalCode='07660');

        insert acct;

        contact1 = new Contact(
                AccountId = acct.id,
                Email = 'svcTest2@svctest.com',
                LastName = 'Named Caller2',
                Named_Caller__c = true,
                FirstName = 'Named Caller2',
                MailingState = 'NJ',
                MailingCountry = 'US'
                );
        insert contact1;

        SVC_testCaseFollowUpCaseCloneController.getUser(contact1.Id);
    }
    
    static testMethod void CaseFollowUpCaseCloneControllerTest() {
        Profile adminProfile = [SELECT Id FROM Profile Where Name ='B2B Service System Administrator'];

        User admin1 = new User (
            FirstName = 'admin1',
            LastName = 'admin1',
            Email = 'admin1seasde@example.com',
            Alias = 'adm199',
            Username = 'admin1seaa199@example.com',
            ProfileId = adminProfile.Id,
            TimeZoneSidKey = 'America/New_York',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US'
        );
        
        insert admin1;

        BusinessHours bh24 = [SELECT Id FROM BusinessHours WHERE name = 'B2B Service Hours 24x7'];
        BusinessHours bh12 = [SELECT Id FROM BusinessHours WHERE name = 'B2B Service Hours 12x5'];
        
        RecordType caseRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Technical Support' 
            AND SobjectType = 'Case'];
        

        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );

        insert testAccount1;

        Account testAccount2 = new Account (
            Name = 'TestAcc2',
            RecordTypeId = accountRT.Id,
            BillingStreet = '411 E Brinkerhoff Ave, Palisades Park',
            BillingCity ='Palisades Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07650'
            
        );

        insert testAccount2;

        RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];
        Product2 prod1 = new Product2(
                name ='MI-test',
                SAP_Material_ID__c = 'MI-test',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = productRT.id
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Asset ast2 = new Asset(
            name ='Assert-MI-Test2',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast2;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            BusinessHoursId = bh12.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent;

        // Insert Contact 
        Contact contact = new Contact(); 
        contact.AccountId = testAccount1.id; 
        contact.Email = 'svcTest@svctest.com'; 
        contact.LastName = 'Named Caller'; 
        contact.Named_Caller__c = true;
        contact.FirstName = 'Named Caller';
        contact.MailingStreet = '1234 Main';
        contact.MailingCity = 'Chicago';
        contact.MailingState = 'IL';
        contact.MailingCountry = 'US';

        insert contact; 

        // Insert Contact non-named caller 
        Contact contact1 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svc199t2@svctest.com', 
            LastName = 'Named 2',
            Named_Caller__c = true,
            MailingStreet = '1234 Main',
            MailingCity = 'Chicago',
            MailingState = 'IL',
            MailingCountry = 'US',
            FirstName = 'Named 2');
        insert contact1; 

        // Insert Contact no entitlement account 
        Contact contact2 = new Contact( 
            AccountId = testAccount2.id, 
            Email = 'svc199Test3@svctest.com', 
            LastName = 'Non Entitlement',
            Named_Caller__c = true,
            MailingStreet = '1234 Main',
            MailingCity = 'Chicago',
            MailingState = 'IL',
            MailingCountry = 'US',
            FirstName = 'Named 3');
        insert contact2; 
         Test.startTest();
        List<Case> cases = new List<Case>();
        Case c1 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact.id,
            RecordTypeId = caseRT.id,
            status='New'
            );
        insert c1;
        //cases.add(c1);
        //insert cases;
       
        Case c2 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 non named caller', 
            reason = 'test2@test.com', 
            Origin = 'Phone',
            contactid=contact1.id,
            RecordTypeId = caseRT.id,
            status='New',
            parentid=c1.id,
            ownerid=admin1.id
            );
        //insert c2;

        // create new case record
       // Case c = new Case();
       // c.subject = 'test';
        //c.status = 'New';
      //  insert c;

        c1.status = 'Resolved';
        c1.Resolution_Category__c = 'Password Reset';
        c1.Resolution__c = 'done';
        update c1;

        c1.status = 'Closed';
        c1.Resolution_Category__c = 'Password Reset';
        c1.Resolution__c = 'done';
        update c1;

        // Construct the standard controller
        ApexPages.StandardController con = new ApexPages.StandardController(c1);

        // create the controller
        CaseFollowUpCaseCloneController ext = new CaseFollowUpCaseCloneController (con);

        // Switch to test context
       
        // call the CaseFollowUpCaseClone method
        PageReference ref = ext.CaseFollowUpCaseClone();

        // create the matching page reference
        PageReference redir = new PageReference('/'+ext.newRecordId+'/e?retURL=%2F'+ext.newRecordId);

        // make sure the user is sent to the correct url
        //System.assertEquals(ref.getUrl(),redir.getUrl());

        // check that the new case was created successfully
       // Case newCase = [select id from Case where id = :ext.newRecordId];
      //  System.assertNotEquals(newCase, null);
        
        //System.runAs(user1) {
             PageReference ref2 = ext.CaseFollowUpCaseClone();

       // }
        // Switch back to runtime context
        Test.stopTest();

    }

    @future
    public static void getUser(String contactId){
        Profile communityProfile = [SELECT Id FROM Profile Where Name like'%Community User%' limit 1];

        User user = new User();
        user.UserRoleId = null;
        user.LastName = 'svcUser1';
        user.Alias = 'user1';
        user.Email = 'user1@example.com';
        user.UserName = 'svcuser1@svcexample.com';
        user.ProfileId = communityProfile.Id;
        user.TimeZoneSidKey = 'America/New_York';
        user.LocaleSidKey = 'en_US';
        user.EmailEncodingKey = 'UTF-8';
        user.LanguageLocaleKey = 'en_US';
        user.IsActive = true;
        user.ContactId = contactId;

        try{
            insert user;
        }catch(DmlException e){
            System.debug(e.getCause() + e.getMessage() + e.getDmlFieldNames(0));
        }
    }
}