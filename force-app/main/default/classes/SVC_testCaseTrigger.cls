@isTest
private class SVC_testCaseTrigger {
    
    @isTest static void testCaseTrigger() {
        
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Profile adminProfile = [SELECT Id FROM Profile Where Name ='B2B Service System Administrator'];

        User admin1 = new User (
            FirstName = 'admin1',
            LastName = 'admin1',
            Email = 'admin1seasde@example.com',
            Alias = 'admint1',
            Username = 'admin1seaa@example.com',
            ProfileId = adminProfile.Id,
            TimeZoneSidKey = 'America/New_York',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US'
        );
        
        insert admin1;

        BusinessHours bh24 = [SELECT Id FROM BusinessHours WHERE name = 'B2B Service Hours 24x7'];
        BusinessHours bh12 = [SELECT Id FROM BusinessHours WHERE name = 'B2B Service Hours 12x5'];
        
        RecordType caseRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Technical Support' 
            AND SobjectType = 'Case'];
        

        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );

        insert testAccount1;

        Account testAccount2 = new Account (
            Name = 'TestAcc2',
            RecordTypeId = accountRT.Id,
            BillingStreet = '411 E Brinkerhoff Ave, Palisades Park',
            BillingCity ='Palisades Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07650'
            
        );

        insert testAccount2;

        RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];
        Product2 prod1 = new Product2(
                name ='MI-test',
                SAP_Material_ID__c = 'MI-test',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = productRT.id
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Asset ast2 = new Asset(
            name ='Assert-MI-Test2',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast2;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            BusinessHoursId = bh12.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent;

        // Insert Contact 
        Contact contact = new Contact(); 
        contact.AccountId = testAccount1.id; 
        contact.Email = 'svcTest@svctest.com'; 
        contact.LastName = 'Named Caller'; 
        contact.Named_Caller__c = true;
        contact.FirstName = 'Named Caller';
        contact.MailingStreet = '1234 Main';
        contact.MailingCity = 'Chicago';
        contact.MailingState = 'IL';
        contact.MailingCountry = 'US';

        insert contact; 

        // Insert Contact non-named caller 
        Contact contact1 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest2@svctest.com', 
            LastName = 'Named Caller2',
            Named_Caller__c = true,
            MailingStreet = '1234 Main',
            MailingCity = 'Chicago',
            MailingState = 'IL',
            MailingCountry = 'US',
            FirstName = 'Named Caller2');
        insert contact1; 

        // Insert Contact no entitlement account 
        Contact contact2 = new Contact( 
            AccountId = testAccount2.id, 
            Email = 'svcTest3@svctest.com', 
            LastName = 'Non Entitlement',
            Named_Caller__c = true,
            MailingStreet = '1234 Main',
            MailingCity = 'Chicago',
            MailingState = 'IL',
            MailingCountry = 'US',
            FirstName = 'Named Caller2');
        insert contact2; 
        
        List<Case> cases = new List<Case>();
        Case c1 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact.id,
            RecordTypeId = caseRT.id,
            status='New'
            );
        insert c1;
        //cases.add(c1);
        //insert cases;
        Test.startTest();
        Case c2 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 non named caller', 
            reason = 'test2@test.com', 
            Origin = 'Phone',
            contactid=contact1.id,
            RecordTypeId = caseRT.id,
            status='New',
            parentid=c1.id,
            ownerid=admin1.id
            );
        insert c2;

        Group q= [select id from group where type='Queue' limit 1 ];

        
        Case cUpdate = [select id,status from case where id=:c1.id];
        //cUpdate.status = 'Resolved';
        cUpdate.ownerid = admin1.id;
        update cUpdate;

        Case cUpdateNew = [select id,status,Entitlement.name from case where id=:c1.id];
        system.assertEquals('Assigned',cUpdateNew.Status);
        
        cUpdate.ownerid = q.id;
        update cUpdate;

        Case cUpdate2 = [select id,status from case where id=:c2.id];
        cUpdate2.status = 'Resolved';
        cUpdate2.Resolution_Category__c = 'Password Reset';
        cUpdate2.Resolution__c = 'done';
        update cUpdate2;
        
        Case cUpdate3 = [select id,status from case where id=:c1.id];
        cUpdate3.status = 'Resolved';
        cUpdate3.Resolution_Category__c = 'Password Reset';
        cUpdate3.Resolution__c = 'done';
        update cUpdate3;
        
        
        Case c3 = new Case(
            AccountId = testAccount2.id,
            Subject = 'Case3 Spam', 
            reason = 'test3@test.com', 
            Origin = 'Web',
            contactid=contact2.id,
            RecordTypeId = caseRT.id,
            status='New',
            ownerid=q.id
            );
        insert c3;
        
        Case cUpdate4 = [select id,status from case where id=:c3.id];
        cUpdate4.IS_SPAM__c = true;
        update cUpdate4;
        Set<Id> cIds = new Set<Id>();
        CIds.add(cUpdate4.id);
        SVC_CancelCasesBatchClass.processBatchClass(cIds);

        Test.stopTest();
    }
    @isTest static void repairInsertTest() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        //Repair Insert,Update
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );

        insert testAccount1;

        Product2 prod1 = new Product2(
                name ='MI-test',
                SAP_Material_ID__c = 'MI-test',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = rtMap.get('Product2'+'B2B')
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent;

        // Insert Contact non-named caller 
        Contact contact1 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest2@svctest.com', 
            LastName = 'Named Caller2',
            Named_Caller__c = true,
            MailingStreet = '1234 Main',
            MailingCity = 'Chicago',
            MailingState = 'IL',
            MailingCountry = 'US',
            FirstName = 'Named Caller2');
        insert contact1; 
        //repair imei
        Case c1 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Repair'),
            status='New',
            Imei__c = '11111111111111'
            );
        insert c1;

        Test.startTest();

        //repair imei
        try{
            Case c2 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Repair'),
            status='New',
            Imei__c = '11111111111111'
            );
            insert c2;
        }
        catch(Exception e){}

        //repair imei
        Case c3 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Repair'),
            status='New',
            Imei__c = '11111111111112'
            );
        insert c3;

        try{
            c3.Imei__c = '11111111111111';
            update c3;  
        }
        catch(Exception e){}
    

        Test.stopTest();

    }
    @isTest static void repairUpdateTest() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        //Repair Insert,Update
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );

        insert testAccount1;

        Product2 prod1 = new Product2(
                name ='MI-test',
                SAP_Material_ID__c = 'MI-test',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = rtMap.get('Product2'+'B2B')
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent;

        // Insert Contact non-named caller 
        Contact contact1 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest2@svctest.com', 
            LastName = 'Named Caller2',
            Named_Caller__c = true,
            MailingStreet = '1234 Main',
            MailingCity = 'Chicago',
            MailingState = 'IL',
            MailingCountry = 'US',
            FirstName = 'Named Caller2');
        insert contact1; 
        //repair serial
        Case c1 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Repair'),
            status='New',
            SerialNumber__c = '1b2b3b4b5b6b7b', 
            Imei__c = ''
            );
        insert c1;
        system.debug('@@@@@@@ - > Test : ' + c1.SerialNumber__c + ':::' + c1.Imei__c);

        Test.startTest();

        //repair serial
        try{
            Case c2 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Repair'),
            status='New',
            SerialNumber__c = '1b2b3b4b5b6b7b',
            Imei__c = ''
            );
            insert c2;
        }
        catch(Exception e){}

        //repair serial
        Case c3 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Repair'),
            status='New',
            SerialNumber__c = '1b2b3b4b5b6b7c', 
            Imei__c = ''
            );
        insert c3;

        try{
            c3.SerialNumber__c = '1b2b3b4b5b6b7b';
            update c3;  
        }
        catch(Exception e){}
    
        Test.stopTest();
    }
    @isTest static void ExchangeInsertTest() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        //Repair Insert,Update
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );

        insert testAccount1;

        Product2 prod1 = new Product2(
                name ='MI-test',
                SAP_Material_ID__c = 'MI-test',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = rtMap.get('Product2'+'B2B')
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent;

        Contact contact1 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest2@svctest.com', 
            LastName = 'Named Caller2',
            Named_Caller__c = true,
            MailingStreet = '1234 Main',
            MailingCity = 'Chicago',
            MailingState = 'IL',
            MailingCountry = 'US',
            FirstName = 'Named Caller2');
        insert contact1;
       List<Device__c> dev=new list<Device__c>();
        Device__c dev1 = new Device__c(
            Account__c = testAccount1.id,
            Entitlement__c = ent.id,
            Status__c = 'Registered',
            Imei__c = '111111111111111'
            );
        //insert dev1;
         dev.add(dev1);
        Device__c dev2 = new Device__c(
            Account__c = testAccount1.id,
            Entitlement__c = ent.id,
            Status__c = 'Registered',
            Imei__c = '111111111111112'
            );
        //insert dev2;
        dev.add(dev2);
        insert dev;
        //repair serial
        Case c1 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Exchange'),
            status='New',
            Device__c = dev1.id
            );
        insert c1;

        Test.startTest();

        //repair serial
        try{
            Case c2 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Exchange'),
            status='New',
            Device__c = dev1.id
            );
            insert c2;
        }
        catch(Exception e){}

        //repair serial
        Case c3 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Exchange'),
            status='New',
            Device__c = dev2.id
            );
        insert c3;

        try{
            c3.Device__c = dev1.id;
            update c3;  
        }
        catch(Exception e){}
    
        Test.stopTest();
    }
    @isTest static void ExchangeUpdateTest() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        //Repair Insert,Update
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        Zipcode_Lookup__c zip  = new Zipcode_Lookup__c(
            Name = '07650',
            City_Name__c = 'Palisades Park',
            Country_Code__c = 'US',
            State_Code__c = 'NJ',
            State_Name__c = 'New Jersey'
        );
        insert zip;

        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07650'
            
        );

        insert testAccount1;

        Product2 prod1 = new Product2(
                name ='MI-test',
                SAP_Material_ID__c = 'MI-test',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = rtMap.get('Product2'+'B2B')
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent;

        Contact contact1 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest2@svctest.com', 
            LastName = 'Named Caller2',
            Named_Caller__c = true,
            MailingStreet = '1234 Main',
            MailingCity = 'Chicago',
            MailingState = 'IL',
            MailingCountry = 'US',
            FirstName = 'Named Caller2');
        insert contact1;

        Device__c dev1 = new Device__c(
            Account__c = testAccount1.id,
            Entitlement__c = ent.id,
            Status__c = 'Registered',
            Serial_Number__c = '111111111111111',
            Model_Name__c = 'MI-test'
            );
        insert dev1;

        Device__c dev2 = new Device__c(
            Account__c = testAccount1.id,
            Entitlement__c = ent.id,
            Status__c = 'Registered',
            Serial_Number__c = '111111111111112',
            Model_Name__c = 'MI-test'
            );
        insert dev2;

        //Exchange
        Test.startTest();
        Case c1 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Exchange'),
            Shipping_Address_1__c = '411 E Brinkerhoff Ave, Palisades Park', 
            Shipping_City__c = 'PALISADES PARK', 
            Shipping_Country__c = 'US', 
            Shipping_State__c = 'NJ', 
            Shipping_Zip_Code__c = '07650',
            status='New',
            Device__c = dev1.id
            );
        insert c1;

        

        //Exchange
        try{
            Case c2 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Exchange'),
            status='New',
            Device__c = dev1.id
            );
            insert c2;
        }
        catch(Exception e){}

        //Exchange
        Case c3 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Exchange'),
            status='New',
            Device__c = dev2.id
            );
        insert c3;

        try{
            c3.Device__c = dev1.id;
            update c3;  
        }
        catch(Exception e){}

        //ProductTest
        try{
            Case c4 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Repair'),
            SerialNumber__c = 'test12345678',
            Model_Code_Repair__c = 'testProduct',
            status='New'
            );
            insert c4;
        }
        catch(Exception e){}

        Test.stopTest();
    }
    @isTest static void RepairInvalidUpdateTest() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        //Repair Insert,Update
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );
        insert testAccount1;

        Product2 prod1 = new Product2(
                name ='MI-test',
                SAP_Material_ID__c = 'MI-test',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = rtMap.get('Product2'+'B2B')
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent;

        Contact contact1 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest2@svctest.com', 
            LastName = 'Named Caller2',
            Named_Caller__c = true,
            MailingStreet = '1234 Main',
            MailingCity = 'Chicago',
            MailingState = 'IL',
            MailingCountry = 'US',
            FirstName = 'Named Caller2');
        insert contact1;

        //repair Parent
        Case c1 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Repair'),
            Imei__c = '900000000000000',
            Case_Device_Status__c  = 'Verified',
            status='New'
            );
        insert c1;

        Test.startTest();
        c1.Imei__c = '900000000000011'; 
        c1.Case_Device_Status__c = 'Invalid';
        update c1;
        Test.stopTest();
    }
    @isTest static void orderShipTest() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        //Repair Insert,Update
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        Zipcode_Lookup__c zip  = new Zipcode_Lookup__c(
            Name = '07650',
            City_Name__c = 'Palisades Park',
            Country_Code__c = 'US',
            State_Code__c = 'NJ',
            State_Name__c = 'New Jersey'
        );
        insert zip;

        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07650'
            
        );

        insert testAccount1;

        Product2 prod1 = new Product2(
                name ='MI-test',
                SAP_Material_ID__c = 'MI-test',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = rtMap.get('Product2'+'B2B')
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent;

        Contact contact1 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest2@svctest.com', 
            LastName = 'Named Caller2',
            Named_Caller__c = true,
            MailingStreet = '1234 Main',
            MailingCity = 'Chicago',
            MailingState = 'IL',
            MailingCountry = 'US',
            FirstName = 'Named Caller2');
        insert contact1;
        List<Device__c> dev=new List<Device__c>();
        Device__c dev1 = new Device__c(
            Account__c = testAccount1.id,
            Entitlement__c = ent.id,
            Status__c = 'Registered',
            Serial_Number__c = '111111111111111',
            Model_Name__c = 'MI-test'
            );
       // insert dev1;
        dev.add(dev1);
        Device__c dev2 = new Device__c(
            Account__c = testAccount1.id,
            Entitlement__c = ent.id,
            Status__c = 'Registered',
            Serial_Number__c = '111111111111112',
            Model_Name__c = 'MI-test'
            );
             dev.add(dev2);
        insert dev;

        //Exchange Parent
        Test.startTest();
        Case c1 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Exchange'),
            Shipping_Address_1__c = '411 E Brinkerhoff Ave, Palisades Park', 
            Shipping_City__c = 'PALISADES PARK', 
            Shipping_Country__c = 'US', 
            Shipping_State__c = 'NJ', 
            Shipping_Zip_Code__c = '07650',
            status='New',
            Parent_Case__c = TRUE
            );
       // insert c1;

        

        //Exchange
        Case c2 = new Case(
        AccountId = testAccount1.id,
        Subject = 'Case1 named caller', 
        reason = 'test1@test.com', 
        Origin = 'Email',
        contactid=contact1.id,
        RecordTypeId = rtMap.get('Case'+'Exchange'),
        status='New',
        Device__c = dev1.id,
        Shipping_Address_1__c = '411 E Brinkerhoff Ave, Palisades Park', 
            Shipping_City__c = 'PALISADES PARK', 
            Shipping_Country__c = 'US', 
            Shipping_State__c = 'NJ', 
            Shipping_Zip_Code__c = '07650'
        );
        insert c2;

        //ordership
        c2.Replacement_Device_IMEI__c = '111111111111113';
        c2.Replacement_Device_Serial_No__c = '11111111111';
        c2.Replacement_Device_Model_Code__c = 'MI-test';
        c2.Status = 'Order Shipped';
        update c2;
        Test.stopTest();

    }  
    @isTest static void shippingAddressTest() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        //Repair Insert,Update
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        Zipcode_Lookup__c zip  = new Zipcode_Lookup__c(
            Name = '07650',
            City_Name__c = 'Palisades Park',
            Country_Code__c = 'US',
            State_Code__c = 'NJ',
            State_Name__c = 'New Jersey'
        );
        insert zip;

        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07650'
            
        );

        insert testAccount1;

        Product2 prod1 = new Product2(
                name ='MI-test',
                SAP_Material_ID__c = 'MI-test',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = rtMap.get('Product2'+'B2B')
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent;

        Contact contact1 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest2@svctest.com', 
            LastName = 'Named Caller2',
            Named_Caller__c = true,
            MailingStreet = '1234 Main',
            MailingCity = 'Chicago',
            MailingState = 'IL',
            MailingCountry = 'US',
            FirstName = 'Named Caller2');
        insert contact1;

        Device__c dev1 = new Device__c(
            Account__c = testAccount1.id,
            Entitlement__c = ent.id,
            Status__c = 'Registered',
            Serial_Number__c = '111111111111111',
            Model_Name__c = 'MI-test'
            );
        insert dev1;

        Device__c dev2 = new Device__c(
            Account__c = testAccount1.id,
            Entitlement__c = ent.id,
            Status__c = 'Registered',
            Serial_Number__c = '111111111111112',
            Model_Name__c = 'MI-test'
            );
        insert dev2;

        //Exchange Parent
        Test.startTest();
        Case c1 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Exchange'),
            Shipping_Address_1__c = '411 E Brinkerhoff Ave, Palisades Park', 
            Shipping_City__c = 'PALISADES PARK', 
            Shipping_Country__c = 'US', 
            Shipping_State__c = 'NJ', 
            Shipping_Zip_Code__c = '07650',
            status='New',
            Parent_Case__c = TRUE
            );
        insert c1;

        

        //Exchange
        Case c2 = new Case(
        AccountId = testAccount1.id,
        Subject = 'Case1 named caller', 
        reason = 'test1@test.com', 
        Origin = 'Email',
        contactid=contact1.id,
        RecordTypeId = rtMap.get('Case'+'Exchange'),
        status='New',
        Device__c = dev1.id,
        ParentId = c1.Id
        );
        insert c2;

        //shipping address
        c1.Shipping_Address_1__c = '412 E Brinkerhoff Ave, Palisades Park';
        update c1;
        Test.stopTest();
    }

    @isTest static void caseParentCloseTest() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        //Repair Insert,Update
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );

        insert testAccount1;

        Product2 prod1 = new Product2(
                name ='MI-test',
                SAP_Material_ID__c = 'MI-test',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = rtMap.get('Product2'+'B2B')
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent;

        // Insert Contact non-named caller 
        Contact contact1 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest2@svctest.com', 
            LastName = 'Named Caller2',
            Named_Caller__c = true,
            MailingStreet = '1234 Main',
            MailingCity = 'Chicago',
            MailingState = 'IL',
            MailingCountry = 'US',
            FirstName = 'Named Caller2');
        insert contact1; 
        //repair serial
        Case c1 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Repair'),
            status='New',
            SerialNumber__c = '1b2b3b4b5b6b7b', 
            Imei__c = ''
            );
        insert c1;
        system.debug('@@@@@@@ - > Test : ' + c1.SerialNumber__c + ':::' + c1.Imei__c);

        Test.startTest();

        //repair serial
        Case c2 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Repair'),
            ParentId = c1.id,
            status='New',
            SerialNumber__c = '1b2b3b4b5b6b7c', 
            Imei__c = ''
            );
        insert c2;

        Case c3 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Repair'),
            ParentId = c1.id,
            status='New',
            SerialNumber__c = '1b2b3b4b5b6b73', 
            Imei__c = ''
            );
      //  insert c3;

        try{
            c2.Status = 'Closed';
            update c2;  
        }
        catch(Exception e){}

        try{
            c3.Status = 'Closed';
            //update c3;  
        }
        catch(Exception e){}
    
        Test.stopTest();
    }  
}