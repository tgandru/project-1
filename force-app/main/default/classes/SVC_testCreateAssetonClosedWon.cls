@isTest(SeeAllData=true)
private class SVC_testCreateAssetonClosedWon {
    
    @isTest static void testCreateAssetonClosedWon() {
        Profile adminProfile = [SELECT Id FROM Profile Where Name ='B2B Service System Administrator'];

        User admin1 = new User (
            FirstName = 'admin1',
            LastName = 'admin1',
            Email = 'admin1@example.com',
            Alias = 'admint1',
            Username = 'oppadmin1@example.com',
            ProfileId = adminProfile.Id,
            TimeZoneSidKey = 'America/New_York',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US'
        );
        
        insert admin1;
        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = accountRT.Id,
            OwnerID = admin1.id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        );

        insert testAccount1;

        Account testAccount2 = new Account (
            Name = 'Partner1',
            RecordTypeId = accountRT.Id,
            OwnerID = admin1.id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        );

        insert testAccount2;
        
        Id pricebookId = Test.getStandardPricebookId();  

        //Pricebook2 standardBook = [SELECT Id FROM Pricebook2 WHERE IsStandard = true limit 1];

        RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];
        Product2 prod1 = new Product2(
                name ='MI-testsvc',
                SAP_Material_ID__c = 'MI-testsvc',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = productRT.id,
                Product_Category__c='Service' 
            );

        insert prod1;
      
      /*ID entryid = null; 
      PricebookEntry oldEntry =  [select id from PricebookEntry where Pricebook2Id =:pricebookId and Product2id =: prod1.id limit 1];
      if (oldEntry.id != null){
          entryid = oldEntry.id;
      }else{
        PricebookEntry entry = new PricebookEntry(
            Pricebook2Id =pricebookId,
            Product2id = prod1.id,
            UnitPrice = 1723,
            isActive = true
            );
        insert entry;
        entryid = entry.id;
       }*/

        Opportunity oppt = new Opportunity(
            name ='test oppt',
            closedate = system.today()+10,
            StageName = 'Identified',
            accountid = testAccount1.id,
            Division__c = 'SBS',
            LeadSource = 'Others(Internal)',
            OwnerID = admin1.id,
            ProductGroupTest__c = 'ACCESSORY',
            Type = 'Tender',
            Pricebook2Id = pricebookId
            );
        insert oppt;
       
       pricebookentry pe=[SELECT id, product2id,product2.name,product2.Product_Category__c from pricebookentry where pricebook2id=:pricebookId and product2.Product_Category__c = 'Service' limit 1];
        
        OpportunityLineItem oi = new OpportunityLineItem(
                PricebookEntryId=pe.id, 
                OpportunityId=oppt.Id, 
                UnitPrice=1723, 
                Quantity=1);
        insert oi;

        oppt.Primary_Distributor__c = testAccount2.id;
        oppt.StageName = 'Qualified';
        oppt.Has_Partner__c = true;
        oppt.Rollout_Duration__c = 12;
        oppt.Roll_Out_Start__c = system.today()+20;

        update oppt;

        oppt.StageName = 'Commit';
        
        update oppt;

        oppt.StageName = 'Win';
        
        update oppt;

        Integer ast = [select count() from asset where AccountId =: testAccount1.id and Product2Id =: pe.product2id];
        System.assertEquals(0, ast);

    }
    
}