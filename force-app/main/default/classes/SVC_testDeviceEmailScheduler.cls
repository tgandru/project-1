@isTest
private class SVC_testDeviceEmailScheduler {
	
	@isTest static void testEmailJobScheduler() {
		Test.StartTest();
		ScheduleSendNewDeviceEmail eee = new ScheduleSendNewDeviceEmail();
		// Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
		String sch = '0 0 * * * ?';
		String jobID = System.schedule('Daily Device Email Test', sch, eee);
		Test.stopTest();	
	}
}