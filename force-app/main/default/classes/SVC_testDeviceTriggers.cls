@isTest
private class SVC_testDeviceTriggers {
    
    @isTest static void testDeviceTriggerEntitlementUpdate() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        // Setup Test Account
        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );

        insert testAccount1;

        RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];
        Product2 prod1 = new Product2(
                name ='MI-test',
                SAP_Material_ID__c = 'MI-test',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = productRT.id
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Asset ast2 = new Asset(
            name ='Assert-MI-Test2',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast2;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent;

        Entitlement ent2 = new Entitlement(
            name ='test Entitlement2',
            accountid=testAccount1.id,
            assetid = ast2.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent2;

        Test.startTest();
        List<Device__c> devs = new List<Device__c>();
        //15 digit imei
        device__c dev1 = new device__c(
            account__c = testAccount1.id,
            imei__c = '123456789012341',
            entitlement__c=ent.id
            
        );
        devs.add(dev1);

        
        //14 digit imei
        device__c dev2 = new device__c(
            account__c = testAccount1.id,
            imei__c = '12345678901234',
            entitlement__c=ent.id
        );
        devs.add(dev2);
    
        device__c dev3 = new device__c(
            account__c = testAccount1.id,
            imei__c = '123456789012',
            entitlement__c=ent.id
            
        );
        devs.add(dev3);

        //valid imei
        device__c dev4 = new device__c(
            account__c = testAccount1.id,
            imei__c = '490154203237518',
            entitlement__c=ent.id
            
        );
        devs.add(dev4);

        device__c dev5 = new device__c(
            account__c = testAccount1.id,
            Serial_Number__c = '123456789012341',
            Model_Name__c = 'MI-test',
            entitlement__c=ent.id
            
        );
        devs.add(dev5); 


        device__c dev15 = new device__c(
            account__c = testAccount1.id,
            Serial_Number__c = '111111111112341',
            Model_Name__c = 'MI-test',
            entitlement__c=ent.id
            
        );
        devs.add(dev15);    
    
        insert devs;



        device__c d1 =[select status__c from device__c where imei__c = '123456789012341'];
        System.assertEquals('Invalid',d1.status__c, 'Should be Invalid Status');
    
        List<device__c> devs2 =[select id,imei__c,Model_Name__c,entitlement__c from device__c where 
            imei__c IN ('123456789012341','123456789012341','490154203237518','123456789012')];
        for (Device__c d : devs2){
            d.Model_Name__c = 'MI-test';
        }
        update devs2;
        
        try{
            device__c dev6 = new device__c(
            account__c = testAccount1.id,
            imei__c = '123456789012341',
            entitlement__c=ent.id
            
            );
            insert dev6;
        }
        catch(Exception e){}
        
        try{
            device__c dev7 = new device__c(
            account__c = testAccount1.id,
            Serial_Number__c = '123456789012341',
            Model_Name__c = 'MI-test',
            entitlement__c=ent.id
            
            );
            insert dev7;

        }
        catch(Exception e){}

        checkRecursive.resetRunOnceFlag();
        /*Device__c dev7_u = [select id,Serial_Number__c from device__c where 
            Serial_Number__c = '111111111112341' limit 1];
        dev7_u.Serial_Number__c = '123456789012341';
        update dev7_u;*/

        //checkRecursive.resetRunOnceFlag();
        Device__c dev8_u = [select id,Serial_Number__c from device__c where 
            Serial_Number__c = '123456789012341' limit 1];
        dev8_u.Serial_Number__c = '1234';
        update dev8_u;

        checkRecursive.resetRunOnceFlag();
        device__c dev8 = new device__c(
            account__c = testAccount1.id,
            imei__c = '490154203237518',
            entitlement__c=ent.id
            
        );
        insert dev8;
        checkRecursive.resetRunOnceFlag();
        Device__c dev9 = [select id,imei__c from device__c where 
            imei__c = '123456789012' limit 1];
        dev9.imei__c = '490154203237518';
        update dev9;
        
        Test.stopTest();
        
    }
}