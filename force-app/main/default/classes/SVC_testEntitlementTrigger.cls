@isTest
private class SVC_testEntitlementTrigger {
    
    @isTest static void testEntitlementTrigger() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        BusinessHours bh24 = [SELECT Id FROM BusinessHours WHERE name = 'B2B Service Hours 24x7'];
        BusinessHours bh12 = [SELECT Id FROM BusinessHours WHERE name = 'B2B Service Hours 12x5'];
    
        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        );

        insert testAccount1;

        Account testAccount2 = new Account (
            Name = 'TestAcc2',
            RecordTypeId = accountRT.Id,
            BillingStreet = '123 State St',
            BillingCity ='Chicago',
            BillingState ='IL',
            BillingCountry='US',
            BillingPostalCode='60606'
            
        );

        insert testAccount2;

        RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];
        Product2 prod1 = new Product2(
                name ='MI-test',
                SAP_Material_ID__c = 'MI-test',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = productRT.id
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Asset ast2 = new Asset(
            name ='Assert-MI-Test2',
            Accountid = testAccount2.id,
            Product2Id = prod1.id
            );
        insert ast2;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            BusinessHoursId = bh12.id,
            startdate = system.today()-1
            );
        insert ent;

        Entitlement ent2 = new Entitlement(
            name ='test Entitlement2',
            accountid=testAccount2.id,
            assetid = ast2.id,
            BusinessHoursId = bh12.id,
            startdate = system.today()-1
            );
        insert ent2;
        
        Contact contact = new Contact(); 
        contact.AccountId = testAccount1.id; 
        contact.Email = 'svcTest@svctest.com'; 
        contact.LastName = 'Named Caller'; 
        contact.Named_Caller__c = true;
        contact.FirstName = 'Named Caller';
        contact.MailingStreet = '1234 Main';
        contact.MailingCity = 'Chicago';
        contact.MailingState = 'IL';
        contact.MailingCountry = 'US';
        insert contact; 

        EntitlementContact enc = new EntitlementContact(
            Entitlementid = ent.id,
            contactid = contact.id);
        insert enc;
    }
    
}