@isTest
private class SVC_testFeedCommentTrigger {
    @isTest static void feedCommentCreationTest() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        //RecordType
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = rtMap.get('Account' + 'Temporary'),
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        );
        insert testAccount1;

        Product2 prod1 = new Product2(
                name ='MI-test',
                SAP_Material_ID__c = 'MI-test',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = rtMap.get('Product2'+'B2B')
            );
        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent;

        // Insert Contact non-named caller 
        Contact contact1 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest2@svctest.com', 
            LastName = 'Named Caller2',
            Named_Caller__c = true,
            FirstName = 'Named Caller',
            MailingStreet = '1234 Main',
            MailingCity = 'Chicago',
            MailingState = 'IL',
            MailingCountry = 'US');
        insert contact1;

        //repair serial
        Case c1 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Repair'),
            status='New',
            Parent_Case__c = false,
            Imei__c = '123456789123456',
            Service_Order_Number__c = '12345678'
            );
        insert c1;

        FeedItem fi = new FeedItem(
            ParentId = c1.Id,
            body = 'test' ,
            Type = 'TextPost'
            );
        insert fi;

        FeedComment fc = new FeedComment(
            FeedItemId = fi.Id,
            CommentBody = 'TEST'
            );
        insert fc;
    }
}