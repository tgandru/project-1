/**
 * Created by ms on 2017-08-07.
 *
 * author : JeongHo.Lee, I2MAX
 */
@isTest
private class SVC_testGCICFeedCommentBatch {
    @isTest static void feedCommentCreationTest() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        //RecordType
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='SVC-07';
        iep2.endPointURL__c = 'https://www.google.com';
        iep2.layout_id__c = 'LAY025152';
        iep2.partial_endpoint__c = '/IF_SVC_07';
        insert iep2;

        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = rtMap.get('Account' + 'Temporary'),
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        );
        insert testAccount1;

        Product2 prod1 = new Product2(
                name ='MI-test',
                SAP_Material_ID__c = 'MI-test',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = rtMap.get('Product2'+'B2B')
            );
        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent;

        // Insert Contact non-named caller 
        Contact contact1 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest2@svctest.com', 
            LastName = 'Named Caller2',
            Named_Caller__c = true,
            FirstName = 'Named Caller',
            MailingStreet = '1234 Main',
            MailingCity = 'Chicago',
            MailingState = 'IL',
            MailingCountry = 'US');
        insert contact1;
        //Test.startTest();
        //repair serial
        List<Case> caseList = new List<Case>();
        Case c1 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case2 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Repair'),
            status='New',
            Parent_Case__c = true
            );
        //insert c1;
        caseList.add(c1);
        
        Case c2 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case2 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Repair'),
            status='New',
            ParentId = c1.Id,
            Parent_Case__c = false,
            Imei__c = '123456789123456',
            Service_Order_Number__c = '12345678'
            );
        //insert c2;
        caseList.add(c2);
        insert caseList;
        
        FeedItem fi = new FeedItem(
            ParentId = c1.Id,
            body = 'test' ,
            Type = 'TextPost'
            );
        insert fi;

        FeedComment fc = new FeedComment(
            FeedItemId = fi.Id,
            CommentBody = 'TEST'
            );
        insert fc;

        
        Set<Id> fiSet = new Set<Id>();
        fiSet.add(fi.Id);
        String body = '{"inputHeaders":{"MSGGUID":"28f20955-512a-90ca-cab5-2f18410ba02c","IFID":"IF_SVC_07","IFDate":"20170728231923","MSGSTATUS":"S","ERRORTEXT":null},"body":{"RET_CODE":"0","RET_Message":null}}';
        TestMockSingleCallout singleMock = new TestMockSingleCallout(body);
        Test.setMock(HttpCalloutMock.class, singleMock);
        List<Id> ids = new List<Id>();
        ids.add(c1.Id);
        test.startTest();
        SVC_GCICFeedCommentBatch batch = new SVC_GCICFeedCommentBatch(ids,fc);
        batch.execute(null, new List<Case>{c1});
        batch.finish(null);
        Test.stopTest();
    }
    @isTest static void httpErrorTest() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        //RecordType
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='SVC-07';
        iep2.endPointURL__c = 'https://www.google.com';
        iep2.layout_id__c = 'LAY025152';
        iep2.partial_endpoint__c = '/IF_SVC_07';
        insert iep2;

        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = rtMap.get('Account' + 'Temporary'),
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        );
        insert testAccount1;

        Product2 prod1 = new Product2(
                name ='MI-test',
                SAP_Material_ID__c = 'MI-test',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = rtMap.get('Product2'+'B2B')
            );
        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent;

        // Insert Contact non-named caller 
        Contact contact1 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest2@svctest.com', 
            LastName = 'Named Caller2',
            Named_Caller__c = true,
            FirstName = 'Named Caller',
            MailingStreet = '1234 Main',
            MailingCity = 'Chicago',
            MailingState = 'IL',
            MailingCountry = 'US');
        insert contact1;
        //Test.startTest();
        List<Case> caselist = new List<Case>();
        //repair serial
        Case c1 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case2 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Repair'),
            status='New',
            Parent_Case__c = true
            );
        //insert c1;
        caselist.add(c1);   
            
        Case c2 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case2 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Repair'),
            status='New',
            ParentId = c1.Id,
            Parent_Case__c = false,
            Imei__c = '123456789123456',
            Service_Order_Number__c = '12345678'
            );
        //insert c2;
        caselist.add(c2);
        insert caseList;

        FeedItem fi = new FeedItem(
            ParentId = c1.Id,
            body = 'test' ,
            Type = 'TextPost'
            );
        insert fi;

        FeedComment fc = new FeedComment(
            FeedItemId = fi.Id,
            CommentBody = 'TEST'
            );
        insert fc;

        
        Set<Id> fiSet = new Set<Id>();
        fiSet.add(fi.Id);
        String body = '{"inputHeaders":{"MSGGUID":"28f20955-512a-90ca-cab5-2f18410ba02c","IFID":"IF_SVC_07","IFDate":"20170728231923","MSGSTATUS":"S","ERRORTEXT":null},"body":{"RET_CODE":"0","RET_Message":null}}';
        TestMockSingleCallout singleMock = new TestMockSingleCallout(body, 505);
        Test.setMock(HttpCalloutMock.class, singleMock);
        List<Id> ids = new List<Id>();
        ids.add(c1.Id);
        test.startTest();
        SVC_GCICFeedCommentBatch batch = new SVC_GCICFeedCommentBatch(ids,fc);
        batch.execute(null, new List<Case>{c1});
        batch.finish(null);
        Test.stopTest();
    }

}