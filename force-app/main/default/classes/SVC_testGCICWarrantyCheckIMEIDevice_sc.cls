/*
author : JeongHo.Lee, I2MAX
 */
@isTest
private class SVC_testGCICWarrantyCheckIMEIDevice_sc
{
  @isTest
  static void itShould()
  {
        String CRON_EXP = '0 0 0 4 10 ? '  + (System.now().year() + 1) ; // Next Year
        Test.startTest();
        System.schedule('Test IMEI Device Warranty Term Check', CRON_EXP, new SVC_GCICWarrantyTermCheckIMEIDevice_sc());
    Test.stopTest();
  }
}