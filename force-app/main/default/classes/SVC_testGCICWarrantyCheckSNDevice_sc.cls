/*
author : JeongHo.Lee, I2MAX
 */
@isTest
private class SVC_testGCICWarrantyCheckSNDevice_sc
{
  @isTest
  static void itShould()
  {
        String CRON_EXP = '0 0 0 4 10 ? '  + (System.now().year() + 1) ; // Next Year
        Test.startTest();
        System.schedule('Test SN Device Warranty Term Check', CRON_EXP, new SVC_GCICWarrantyTermCheckSNDevice_sc());
    Test.stopTest();
  }
}