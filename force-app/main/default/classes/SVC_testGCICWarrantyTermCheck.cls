@isTest
private class SVC_testGCICWarrantyTermCheck {

    @isTest static void caseRepairChildLinkCheck() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        //Repair Insert,Update
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='SVC-06';
        iep2.endPointURL__c = 'https://www.google.com';
        iep2.layout_id__c = 'LAY025151';
        iep2.partial_endpoint__c = '/IF_SVC_06';
        insert iep2;

        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );

        insert testAccount1;

        Product2 prod1 = new Product2(
                name ='SM-G950UZKAVZW',
                SAP_Material_ID__c = 'SM-G950UZKAVZW',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = rtMap.get('Product2'+'B2B')
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent;

        // Insert Contact non-named caller 
        Contact contact1 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest2@svctest.com', 
            LastName = 'Named Caller2',
            Named_Caller__c = true,
            FirstName = 'Named Caller',
            MailingStreet = '1234 Main',
            MailingCity = 'Chicago',
            MailingState = 'IL',
            MailingCountry = 'US');
        insert contact1; 

        //repair Valid imei
        Case c1 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Repair'),
            status='New',
            Imei__c = '35833008016052'
            );
        insert c1;

        Test.startTest();
        String body = '{"inputHeaders":{"MSGGUID":"101003b0-f928-7728-94b4-5e1edb0293ad","IFID":"IF_SVC_06","IFDate":"20170722041034","MSGSTATUS":"S","ERRORTEXT":null},"body":{"EtOutput":{"item":[{"Imei":"35833008016052","SerialNo":null,"Model":"SM-G950UZKAVZW","Gidate":"2017-04-02","SwVer":"QC8","HwVer":null,"PrlVer":null,"Kunnr":"1770349","KunnrDesc":null,"Lfdate":"2017-04-08","GidateM":"2017-04-08","ProdDate":"2017-03-30","Ber":"N","ActDate":"- -","WtyEndDate":"2018-07-30","WtyFlag":"IW","WtyChangeFlag":null,"Ret":"0","RetMsg":null,"RetMat":"REC-G950UZKAVZWPHN","ExtWtyContractNo":null,"ExtWtyContractName":null,"ExtWtyInformation":null,"ExtWtyStartDate":"0000-00-00","ExtWtyEndDate":"0000-00-00"}]},"ItInput":{"item":{"Imei":"35833008016052","SerialNo":null,"Model":null,"DeviceType":"IMEI","ObjectId":null,"PurchaseDate":"0000-00-00","PostingDate":"0000-00-00","RedoFlag":null,"DeliveryDate":"0000-00-00"}}}}';
        TestMockSingleCallout singleMock = new TestMockSingleCallout(body);
        Test.setMock(HttpCalloutMock.class, singleMock);
        SVC_GCICWarrantyTermCheck.warrantyCheck(c1.Id);
        Test.stopTest();
    }
     @isTest static void caseRepairParentLinkCheck() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        //Repair Insert,Update
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='SVC-06';
        iep2.endPointURL__c = 'https://www.google.com';
        iep2.layout_id__c = 'LAY025151';
        iep2.partial_endpoint__c = '/IF_SVC_06';
        insert iep2;

        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );
        insert testAccount1;

        Product2 prod1 = new Product2(
                name ='SM-G950UZKAVZW',
                SAP_Material_ID__c = 'SM-G950UZKAVZW',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = rtMap.get('Product2'+'B2B')
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent;

        // Insert Contact non-named caller 
        Contact contact1 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest2@svctest.com', 
            LastName = 'Named Caller2',
            Named_Caller__c = true,
            FirstName = 'Named Caller',
            MailingStreet = '1234 Main',
            MailingCity = 'Chicago',
            MailingState = 'IL',
            MailingCountry = 'US');
        insert contact1; 

        //repair Valid imei
        Case c1 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Repair'),
            status='New',
            Parent_Case__c = true
            );
        insert c1;

        Case c2 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Repair'),
            status='New',
            ParentId = c1.Id,
            Imei__c = '35833008016052'
            );
        insert c2;

        Test.startTest();
        String body = '{"inputHeaders":{"MSGGUID":"101003b0-f928-7728-94b4-5e1edb0293ad","IFID":"IF_SVC_06","IFDate":"20170722041034","MSGSTATUS":"S","ERRORTEXT":null},"body":{"EtOutput":{"item":[{"Imei":"35833008016052","SerialNo":null,"Model":"SM-G950UZKAVZW","Gidate":"2017-04-02","SwVer":"QC8","HwVer":null,"PrlVer":null,"Kunnr":"1770349","KunnrDesc":null,"Lfdate":"2017-04-08","GidateM":"2017-04-08","ProdDate":"2017-03-30","Ber":"N","ActDate":"- -","WtyEndDate":"2018-07-30","WtyFlag":"IW","WtyChangeFlag":null,"Ret":"0","RetMsg":null,"RetMat":"REC-G950UZKAVZWPHN","ExtWtyContractNo":null,"ExtWtyContractName":null,"ExtWtyInformation":null,"ExtWtyStartDate":"0000-00-00","ExtWtyEndDate":"0000-00-00"}]},"ItInput":{"item":{"Imei":"35833008016052","SerialNo":null,"Model":null,"DeviceType":"IMEI","ObjectId":null,"PurchaseDate":"0000-00-00","PostingDate":"0000-00-00","RedoFlag":null,"DeliveryDate":"0000-00-00"}}}}';
        TestMockSingleCallout singleMock = new TestMockSingleCallout(body);
        Test.setMock(HttpCalloutMock.class, singleMock);
        SVC_GCICWarrantyTermCheck.warrantyCheck(c1.Id);
        Test.stopTest();
    }
    @isTest static void caseRepairChildSerialCheck() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        //Repair Insert,Update
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='SVC-06';
        iep2.endPointURL__c = 'https://www.google.com';
        iep2.layout_id__c = 'LAY025151';
        iep2.partial_endpoint__c = '/IF_SVC_06';
        insert iep2;

        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );

        insert testAccount1;

        Product2 prod1 = new Product2(
                name ='SM-T580NZKAXAR',
                SAP_Material_ID__c = 'SM-T580NZKAXAR',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = rtMap.get('Product2'+'B2B')
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent;

        // Insert Contact non-named caller 
        Contact contact1 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest2@svctest.com', 
            LastName = 'Named Caller2',
            Named_Caller__c = true,
            FirstName = 'Named Caller',
            MailingStreet = '1234 Main',
            MailingCity = 'Chicago',
            MailingState = 'IL',
            MailingCountry = 'US');
        insert contact1; 

        //repair Valid imei
        Case c1 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Repair'),
            status='New',
            SerialNumber__c = 'R52J40B2YFV',
            Model_Code_Repair__c = 'SM-T580NZKAXAR'
            );
        insert c1;

        Test.startTest();
        String body = '{"inputHeaders":{"MSGGUID":"101003b0-f928-7728-94b4-5e1edb0293ad","IFID":"IF_SVC_06","IFDate":"20170722043913","MSGSTATUS":"S","ERRORTEXT":null},"body":{"EtOutput":{"item":[{"Imei":null,"SerialNo":"R52J40B2YFV","Model":"SM-T580NZKAXAR","Gidate":"0000-00-00","SwVer":null,"HwVer":null,"PrlVer":null,"Kunnr":null,"KunnrDesc":null,"Lfdate":"0000-00-00","GidateM":"0000-00-00","ProdDate":"0000-00-00","Ber":null,"ActDate":"0000-00-00","WtyEndDate":"2017-06-30","WtyFlag":"OW","WtyChangeFlag":null,"Ret":"0","RetMsg":null,"RetMat":null,"ExtWtyContractNo":null,"ExtWtyContractName":null,"ExtWtyInformation":null,"ExtWtyStartDate":"0000-00-00","ExtWtyEndDate":"0000-00-00"}]},"ItInput":{"item":{"Imei":null,"SerialNo":"R52J40B2YFV","Model":"SM-T580NZKAXAR","DeviceType":"SN","ObjectId":null,"PurchaseDate":"0000-00-00","PostingDate":"0000-00-00","RedoFlag":null,"DeliveryDate":"0000-00-00"}}}}';
        TestMockSingleCallout singleMock = new TestMockSingleCallout(body);
        Test.setMock(HttpCalloutMock.class, singleMock);
        SVC_GCICWarrantyTermCheck.warrantyCheck(c1.Id);
        Test.stopTest();

    }
    @isTest static void caseErrorCheck2() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        //Repair Insert,Update
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='SVC-06';
        iep2.endPointURL__c = 'https://www.google.com';
        iep2.layout_id__c = 'LAY025151';
        iep2.partial_endpoint__c = '/IF_SVC_06';
        insert iep2;

        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );

        insert testAccount1;

        Product2 prod1 = new Product2(
                name ='SM-G950UZKAVZW',
                SAP_Material_ID__c = 'SM-G950UZKAVZW',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = rtMap.get('Product2'+'B2B')
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent;

        // Insert Contact non-named caller 
        Contact contact1 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest2@svctest.com', 
            LastName = 'Named Caller2',
            Named_Caller__c = true,
            FirstName = 'Named Caller',
            MailingStreet = '1234 Main',
            MailingCity = 'Chicago',
            MailingState = 'IL',
            MailingCountry = 'US');
        insert contact1; 

        //valid
        Device__c dev1 = new Device__c(
            Account__c = testAccount1.id,
            Entitlement__c = ent.id,
            Status__c = 'Registered',
            Imei__c = '35833008016052'
            );
        insert dev1;

        //Exchange Valid imei
        Case c1 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Exchange'),
            status='New',
            Device__c = dev1.id
            );
        insert c1;

        Test.startTest();
        String body = '{"inputHeaders":{"MSGGUID":"101003b0-f928-7728-94b4-5e1edb0293ad","IFID":"IF_SVC_06","IFDate":"20170722041034","MSGSTATUS":"S","ERRORTEXT":null},"body":{"EtOutput":{"item":[{"Imei":"35833008016052","SerialNo":null,"Model":"SM-G950UZKAVZW","Gidate":"2017-04-02","SwVer":"QC8","HwVer":null,"PrlVer":null,"Kunnr":"1770349","KunnrDesc":null,"Lfdate":"2017-04-08","GidateM":"2017-04-08","ProdDate":"2017-03-30","Ber":"N","ActDate":"- -","WtyEndDate":"2018-07-30","WtyFlag":"IW","WtyChangeFlag":null,"Ret":"0","RetMsg":null,"RetMat":"REC-G950UZKAVZWPHN","ExtWtyContractNo":null,"ExtWtyContractName":null,"ExtWtyInformation":null,"ExtWtyStartDate":"0000-00-00","ExtWtyEndDate":"0000-00-00"}]},"ItInput":{"item":{"Imei":"35833008016052","SerialNo":null,"Model":null,"DeviceType":"IMEI","ObjectId":null,"PurchaseDate":"0000-00-00","PostingDate":"0000-00-00","RedoFlag":null,"DeliveryDate":"0000-00-00"}}}}';
        TestMockSingleCallout singleMock = new TestMockSingleCallout(body, 505);
        Test.setMock(HttpCalloutMock.class, singleMock);
        SVC_GCICWarrantyTermCheck.warrantyCheck(c1.Id);
        Test.stopTest();
    }
    @isTest static void caseRepairInvalidLinkCheck() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        //Repair Insert,Update
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='SVC-06';
        iep2.endPointURL__c = 'https://www.google.com';
        iep2.layout_id__c = 'LAY025151';
        iep2.partial_endpoint__c = '/IF_SVC_06';
        insert iep2;

        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );

        insert testAccount1;

        Product2 prod1 = new Product2(
                name ='SM-G950UZKAVZW',
                SAP_Material_ID__c = 'SM-G950UZKAVZW',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = rtMap.get('Product2'+'B2B')
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent;

        // Insert Contact non-named caller 
        Contact contact1 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest2@svctest.com', 
            LastName = 'Named Caller2',
            Named_Caller__c = true,
            FirstName = 'Named Caller',
            MailingStreet = '1234 Main',
            MailingCity = 'Chicago',
            MailingState = 'IL',
            MailingCountry = 'US');
        insert contact1; 

        //repair Valid imei
        Case c1 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Repair'),
            status='New',
            Imei__c = '35833008016053'
            );
        insert c1;

        Test.startTest();
        String body = '{"inputHeaders":{"MSGGUID":"101003b0-f928-7728-94b4-5e1edb0293ad","IFID":"IF_SVC_06","IFDate":"20170722041034","MSGSTATUS":"S","ERRORTEXT":null},"body":{"EtOutput":{"item":[{"Imei":"35833008016052","SerialNo":null,"Model":"SM-G950UZKAVZW","Gidate":"2017-04-02","SwVer":"QC8","HwVer":null,"PrlVer":null,"Kunnr":"1770349","KunnrDesc":null,"Lfdate":"2017-04-08","GidateM":"2017-04-08","ProdDate":"2017-03-30","Ber":"N","ActDate":"- -","WtyEndDate":"2018-07-30","WtyFlag":"IW","WtyChangeFlag":null,"Ret":"0","RetMsg":null,"RetMat":"REC-G950UZKAVZWPHN","ExtWtyContractNo":null,"ExtWtyContractName":null,"ExtWtyInformation":null,"ExtWtyStartDate":"0000-00-00","ExtWtyEndDate":"0000-00-00"}]},"ItInput":{"item":{"Imei":"35833008016052","SerialNo":null,"Model":null,"DeviceType":"IMEI","ObjectId":null,"PurchaseDate":"0000-00-00","PostingDate":"0000-00-00","RedoFlag":null,"DeliveryDate":"0000-00-00"}}}}';
        TestMockSingleCallout singleMock = new TestMockSingleCallout(body);
        Test.setMock(HttpCalloutMock.class, singleMock);
        SVC_GCICWarrantyTermCheck.warrantyCheck(c1.Id);
        Test.stopTest();

    }
    @isTest static void caseRepairChildIMEITriggerCheck() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        //Repair Insert,Update
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='SVC-06';
        iep2.endPointURL__c = 'https://www.google.com';
        iep2.layout_id__c = 'LAY025151';
        iep2.partial_endpoint__c = '/IF_SVC_06';
        insert iep2;

        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );

        insert testAccount1;

        Product2 prod1 = new Product2(
                name ='SM-G950UZKAVZW',
                SAP_Material_ID__c = 'SM-G950UZKAVZW',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = rtMap.get('Product2'+'B2B')
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent;

        // Insert Contact non-named caller 
        Contact contact1 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest2@svctest.com', 
            LastName = 'Named Caller2',
            Named_Caller__c = true,
            FirstName = 'Named Caller',
            MailingStreet = '1234 Main',
            MailingCity = 'Chicago',
            MailingState = 'IL',
            MailingCountry = 'US');
        insert contact1; 

        //repair Valid imei
        Case c1 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Repair'),
            status='New',
            Imei__c = '35833008016052'
            );
        insert c1;

        Test.startTest();
        String body = '{"inputHeaders":{"MSGGUID":"101003b0-f928-7728-94b4-5e1edb0293ad","IFID":"IF_SVC_06","IFDate":"20170722041034","MSGSTATUS":"S","ERRORTEXT":null},"body":{"EtOutput":{"item":[{"Imei":"35833008016052","SerialNo":null,"Model":"SM-G950UZKAVZW","Gidate":"2017-04-02","SwVer":"QC8","HwVer":null,"PrlVer":null,"Kunnr":"1770349","KunnrDesc":null,"Lfdate":"2017-04-08","GidateM":"2017-04-08","ProdDate":"2017-03-30","Ber":"N","ActDate":"- -","WtyEndDate":"2018-07-30","WtyFlag":"IW","WtyChangeFlag":null,"Ret":"0","RetMsg":null,"RetMat":"REC-G950UZKAVZWPHN","ExtWtyContractNo":null,"ExtWtyContractName":null,"ExtWtyInformation":null,"ExtWtyStartDate":"0000-00-00","ExtWtyEndDate":"0000-00-00"}]},"ItInput":{"item":{"Imei":"35833008016052","SerialNo":null,"Model":null,"DeviceType":"IMEI","ObjectId":null,"PurchaseDate":"0000-00-00","PostingDate":"0000-00-00","RedoFlag":null,"DeliveryDate":"0000-00-00"}}}}';
        TestMockSingleCallout singleMock = new TestMockSingleCallout(body);
        Test.setMock(HttpCalloutMock.class, singleMock);
        SVC_GCICWarrantyTermCheck.toSend(c1.Id);
        Test.stopTest();
    }
    @isTest static void caseRepairChildSerialTriggerCheck() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        //Repair Insert,Update
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='SVC-06';
        iep2.endPointURL__c = 'https://www.google.com';
        iep2.layout_id__c = 'LAY025151';
        iep2.partial_endpoint__c = '/IF_SVC_06';
        insert iep2;

        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );

        insert testAccount1;

        Product2 prod1 = new Product2(
                name ='SM-T580NZKAXAR',
                SAP_Material_ID__c = 'SM-T580NZKAXAR',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = rtMap.get('Product2'+'B2B')
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent;

        // Insert Contact non-named caller 
        Contact contact1 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest2@svctest.com', 
            LastName = 'Named Caller2',
            Named_Caller__c = true,
            FirstName = 'Named Caller',
            MailingStreet = '1234 Main',
            MailingCity = 'Chicago',
            MailingState = 'IL',
            MailingCountry = 'US');
        insert contact1; 

        //repair Valid imei
        Case c1 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Repair'),
            status='New',
            SerialNumber__c = 'R52J40B2YFV',
            Model_Code_Repair__c = 'SM-T580NZKAXAR'
            );
        insert c1;

        Test.startTest();
        String body = '{"inputHeaders":{"MSGGUID":"101003b0-f928-7728-94b4-5e1edb0293ad","IFID":"IF_SVC_06","IFDate":"20170722043913","MSGSTATUS":"S","ERRORTEXT":null},"body":{"EtOutput":{"item":[{"Imei":null,"SerialNo":"R52J40B2YFV","Model":"SM-T580NZKAXAR","Gidate":"0000-00-00","SwVer":null,"HwVer":null,"PrlVer":null,"Kunnr":null,"KunnrDesc":null,"Lfdate":"0000-00-00","GidateM":"0000-00-00","ProdDate":"0000-00-00","Ber":null,"ActDate":"0000-00-00","WtyEndDate":"2017-06-30","WtyFlag":"OW","WtyChangeFlag":null,"Ret":"0","RetMsg":null,"RetMat":null,"ExtWtyContractNo":null,"ExtWtyContractName":null,"ExtWtyInformation":null,"ExtWtyStartDate":"0000-00-00","ExtWtyEndDate":"0000-00-00"}]},"ItInput":{"item":{"Imei":null,"SerialNo":"R52J40B2YFV","Model":"SM-T580NZKAXAR","DeviceType":"SN","ObjectId":null,"PurchaseDate":"0000-00-00","PostingDate":"0000-00-00","RedoFlag":null,"DeliveryDate":"0000-00-00"}}}}';
        TestMockSingleCallout singleMock = new TestMockSingleCallout(body);
        Test.setMock(HttpCalloutMock.class, singleMock);
        SVC_GCICWarrantyTermCheck.toSend(c1.Id);
        Test.stopTest();

    }
    @isTest static void httpError() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        //Repair Insert,Update
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='SVC-06';
        iep2.endPointURL__c = 'https://www.google.com';
        iep2.layout_id__c = 'LAY025151';
        iep2.partial_endpoint__c = '/IF_SVC_06';
        insert iep2;

        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );

        insert testAccount1;

        Product2 prod1 = new Product2(
                name ='SM-G950UZKAVZW',
                SAP_Material_ID__c = 'SM-G950UZKAVZW',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = rtMap.get('Product2'+'B2B')
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent;

        // Insert Contact non-named caller 
        Contact contact1 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest2@svctest.com', 
            LastName = 'Named Caller2',
            Named_Caller__c = true,
            FirstName = 'Named Caller',
            MailingStreet = '1234 Main',
            MailingCity = 'Chicago',
            MailingState = 'IL',
            MailingCountry = 'US');
        insert contact1; 

        //repair Valid imei
        Case c1 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Repair'),
            status='New',
            Imei__c = '35833008016052'
            );
        insert c1;

        Test.startTest();
        String body = '{"inputHeaders":{"MSGGUID":"101003b0-f928-7728-94b4-5e1edb0293ad","IFID":"IF_SVC_06","IFDate":"20170722041034","MSGSTATUS":"S","ERRORTEXT":null},"body":{"EtOutput":{"item":[{"Imei":"35833008016052","SerialNo":null,"Model":"SM-G950UZKAVZW","Gidate":"2017-04-02","SwVer":"QC8","HwVer":null,"PrlVer":null,"Kunnr":"1770349","KunnrDesc":null,"Lfdate":"2017-04-08","GidateM":"2017-04-08","ProdDate":"2017-03-30","Ber":"N","ActDate":"- -","WtyEndDate":"2018-07-30","WtyFlag":"IW","WtyChangeFlag":null,"Ret":"0","RetMsg":null,"RetMat":"REC-G950UZKAVZWPHN","ExtWtyContractNo":null,"ExtWtyContractName":null,"ExtWtyInformation":null,"ExtWtyStartDate":"0000-00-00","ExtWtyEndDate":"0000-00-00"}]},"ItInput":{"item":{"Imei":"35833008016052","SerialNo":null,"Model":null,"DeviceType":"IMEI","ObjectId":null,"PurchaseDate":"0000-00-00","PostingDate":"0000-00-00","RedoFlag":null,"DeliveryDate":"0000-00-00"}}}}';
        TestMockSingleCallout singleMock = new TestMockSingleCallout(body, 505);
        Test.setMock(HttpCalloutMock.class, singleMock);
        SVC_GCICWarrantyTermCheck.warrantyCheck(c1.Id);
        Test.stopTest();
    }
    @isTest static void httpError2() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        //Repair Insert,Update
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='SVC-06';
        iep2.endPointURL__c = 'https://www.google.com';
        iep2.layout_id__c = 'LAY025151';
        iep2.partial_endpoint__c = '/IF_SVC_06';
        insert iep2;

        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );

        insert testAccount1;

        Product2 prod1 = new Product2(
                name ='SM-G950UZKAVZW',
                SAP_Material_ID__c = 'SM-G950UZKAVZW',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = rtMap.get('Product2'+'B2B')
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent;

        // Insert Contact non-named caller 
        Contact contact1 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest2@svctest.com', 
            LastName = 'Named Caller2',
            Named_Caller__c = true,
            FirstName = 'Named Caller',
            MailingStreet = '1234 Main',
            MailingCity = 'Chicago',
            MailingState = 'IL',
            MailingCountry = 'US');
        insert contact1; 

        //repair Valid imei
        Case c1 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Repair'),
            status='New',
            Imei__c = '35833008016052'
            );
        insert c1;

        Test.startTest();
        String body = '{"inputHeaders":{"MSGGUID":"101003b0-f928-7728-94b4-5e1edb0293ad","IFID":"IF_SVC_06","IFDate":"20170722041034","MSGSTATUS":"S","ERRORTEXT":null},"body":{"EtOutput":{"item":[{"Imei":"35833008016052","SerialNo":null,"Model":"SM-G950UZKAVZW","Gidate":"2017-04-02","SwVer":"QC8","HwVer":null,"PrlVer":null,"Kunnr":"1770349","KunnrDesc":null,"Lfdate":"2017-04-08","GidateM":"2017-04-08","ProdDate":"2017-03-30","Ber":"N","ActDate":"- -","WtyEndDate":"2018-07-30","WtyFlag":"IW","WtyChangeFlag":null,"Ret":"0","RetMsg":null,"RetMat":"REC-G950UZKAVZWPHN","ExtWtyContractNo":null,"ExtWtyContractName":null,"ExtWtyInformation":null,"ExtWtyStartDate":"0000-00-00","ExtWtyEndDate":"0000-00-00"}]},"ItInput":{"item":{"Imei":"35833008016052","SerialNo":null,"Model":null,"DeviceType":"IMEI","ObjectId":null,"PurchaseDate":"0000-00-00","PostingDate":"0000-00-00","RedoFlag":null,"DeliveryDate":"0000-00-00"}}}}';
        TestMockSingleCallout singleMock = new TestMockSingleCallout(body, 505);
        Test.setMock(HttpCalloutMock.class, singleMock);
        SVC_GCICWarrantyTermCheck.toSend(c1.Id);
        Test.stopTest();
    }
}