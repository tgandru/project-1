/**
 * Created by ms on 2017-08-10.
 *
 * author : JeongHo.Lee, I2MAX
 */
@isTest
private class SVC_testGCICWarrantyTermCheckCaseBatch {
    @testSetup
    static void setData(){
                    Channelinsight.CI_TEST_TestUtils.createBaseInstallData();

        id AccountRTid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Temporary').getRecordTypeId();
        Account testAccount = new Account (
            Name = 'TestAcc1',
            RecordTypeId = AccountRTid,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        );
        insert testAccount;
        //Address insert
        List<Zipcode_Lookup__c> zipList = new List<Zipcode_Lookup__c>(); 
        Zipcode_Lookup__c  zip1 = new Zipcode_Lookup__c(Name='07660',City_Name__c='RIDGEFIELD PARK', Country_Code__c='US', State_Code__c='NJ',State_Name__c='New Jersey');
        zipList.add(zip1);
        insert zipList;
    }
    @isTest static void caseRepairIMEICheck() {
        //Repair Insert,Update
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }
        system.debug('***rtType***' + rtMap.get('Case' + 'Repair'));
        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='SVC-06';
        iep2.endPointURL__c = 'https://www.google.com';
        iep2.layout_id__c = 'LAY025151';
        iep2.partial_endpoint__c = '/IF_SVC_06';
        insert iep2;

        /*Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = rtMap.get('Account' + 'Temporary'),
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );
        insert testAccount1;*/
        account testAccount1 = [select id from Account limit 1];
        

        Product2 prod1 = new Product2(
                name ='SM-G950UZKAVZW',
                SAP_Material_ID__c = 'SM-G950UZKAVZW',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = rtMap.get('Product2'+'B2B')
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent;

        // Insert Contact non-named caller 
        Contact contact1 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest2@svctest.com', 
            LastName = 'Named Caller2',
            Named_Caller__c = true,
            FirstName = 'Named Caller',
            MailingStreet = '1234 Main',
            MailingCity = 'Chicago',
            MailingState = 'IL',
            MailingCountry = 'US');
        insert contact1; 

        //repair Valid imei
        Case c1 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Repair'),
            status='New',
            Parent_Case__c = true
            );
        insert c1;

        Case c2 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Repair'),
            status='New',
            ParentId = c1.Id,
            Imei__c = '35833008016052',
            Parent_Case__c = false
            );
        insert c2;
        system.debug('***c2 : ' + c2);

        Test.startTest();
        Case ca3  = [SELECT Id 
                            , IMEI__c
                            , SerialNumber__c
                            , Device_Type_f__c                      //formula
                            , IMEI_Number_Device__c                 //formula
                            , Serial_Number_Device__c               //formula
                            , RecordType.DeveloperName
                            , Device__c
                            , Device__r.Model_Name__c
                            , Case_Device_Status__c
                            , Parent_Case__c
                            , ParentId
                            , Warranty_Status__c
                            , Model_Code__c
                            , Model_Code_Repair__c
                            , Extended_Warranty_Contract_No__c
                            , Extended_Warranty_Start_Date__c
                            , Extended_Warranty_Contract_Name__c
                            , Extended_Warranty_End_Date__c
                            , Extended_Warranty_Information__c 
                    FROM Case 
                    WHERE id =: c2.Id];
        String body = '{"inputHeaders":{"MSGGUID":"101003b0-f928-7728-94b4-5e1edb0293ad","IFID":"IF_SVC_06","IFDate":"20170722041034","MSGSTATUS":"S","ERRORTEXT":null},"body":{"EtOutput":{"item":[{"Imei":"35833008016052","SerialNo":null,"Model":"SM-G950UZKAVZW","Gidate":"2017-04-02","SwVer":"QC8","HwVer":null,"PrlVer":null,"Kunnr":"1770349","KunnrDesc":null,"Lfdate":"2017-04-08","GidateM":"2017-04-08","ProdDate":"2017-03-30","Ber":"N","ActDate":"- -","WtyEndDate":"2018-07-30","WtyFlag":"IW","WtyChangeFlag":null,"Ret":"0","RetMsg":null,"RetMat":"REC-G950UZKAVZWPHN","ExtWtyContractNo":null,"ExtWtyContractName":null,"ExtWtyInformation":null,"ExtWtyStartDate":"0000-00-00","ExtWtyEndDate":"0000-00-00"}]},"ItInput":{"item":{"Imei":"35833008016052","SerialNo":null,"Model":null,"DeviceType":"IMEI","ObjectId":null,"PurchaseDate":"0000-00-00","PostingDate":"0000-00-00","RedoFlag":null,"DeliveryDate":"0000-00-00"}}}}';
        TestMockSingleCallout singleMock = new TestMockSingleCallout(body);
        Test.setMock(HttpCalloutMock.class, singleMock);
        SeT<Id> caseIds = new Set<Id>();
        caseIds.add(c2.id);
        SVC_GCICWarrantyTermCheckCaseBatch batch = new SVC_GCICWarrantyTermCheckCaseBatch(null,caseIds,false);
        batch.execute(null, new List<case>{ca3});
        batch.finish(null);
        Test.stopTest();
    }
    @isTest static void caseExchangeIMEICheck() {
        //Repair Insert,Update
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='SVC-06';
        iep2.endPointURL__c = 'https://www.google.com';
        iep2.layout_id__c = 'LAY025151';
        iep2.partial_endpoint__c = '/IF_SVC_06';
        insert iep2;

        /*Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = rtMap.get('Account' + 'Temporary'),
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );
        insert testAccount1;*/
        account testAccount1 = [select id from Account limit 1];

        Product2 prod1 = new Product2(
                name ='SM-G950UZKAVZW',
                SAP_Material_ID__c = 'SM-G950UZKAVZW',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = rtMap.get('Product2'+'B2B')
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent;

        // Insert Contact non-named caller 
        Contact contact1 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest2@svctest.com', 
            LastName = 'Named Caller2',
            Named_Caller__c = true,
            FirstName = 'Named Caller',
            MailingStreet = '1234 Main',
            MailingCity = 'Chicago',
            MailingState = 'IL',
            MailingCountry = 'US');
        insert contact1; 

         //valid
        Device__c dev1 = new Device__c(
            Account__c = testAccount1.id,
            Entitlement__c = ent.id,
            Status__c = 'Valid',
            Imei__c = '35833008016052'
            );
        insert dev1;

        //repair Valid imei
        Case c1 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Repair'),
            status='New',
            Parent_Case__c = true,
            Shipping_Address_1__c='100 challenger rd',
            Shipping_City__c='RIDGEFIELD PARK',
            Shipping_Country__c='US',
            Shipping_State__c='NJ',
            Shipping_Zip_Code__c= '07660'
            );
        insert c1;

        Case c2 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Exchange'),
            status='New',
            ParentId = c1.Id,
            Device__c = dev1.Id,
            Shipping_Address_1__c='100 challenger rd',
            Shipping_City__c='RIDGEFIELD PARK',
            Shipping_Country__c='US',
            Shipping_State__c='NJ',
            Shipping_Zip_Code__c= '07660'
            );
        insert c2;

        Test.startTest();
        Case ca3  = [SELECT Id 
                            , IMEI__c
                            , SerialNumber__c
                            , Device_Type_f__c                      //formula
                            , IMEI_Number_Device__c                 //formula
                            , Serial_Number_Device__c               //formula
                            , RecordType.DeveloperName
                            , Device__c
                            , Device__r.Model_Name__c
                            , Case_Device_Status__c
                            , Parent_Case__c
                            , ParentId
                            , Warranty_Status__c
                            , Model_Code__c
                            , Model_Code_Repair__c
                            , Extended_Warranty_Contract_No__c
                            , Extended_Warranty_Start_Date__c
                            , Extended_Warranty_Contract_Name__c
                            , Extended_Warranty_End_Date__c
                            , Extended_Warranty_Information__c 
                    FROM Case 
                    WHERE id =: c2.Id];

        String body = '{"inputHeaders":{"MSGGUID":"101003b0-f928-7728-94b4-5e1edb0293ad","IFID":"IF_SVC_06","IFDate":"20170722041034","MSGSTATUS":"S","ERRORTEXT":null},"body":{"EtOutput":{"item":[{"Imei":"35833008016052","SerialNo":null,"Model":"SM-G950UZKAVZW","Gidate":"2017-04-02","SwVer":"QC8","HwVer":null,"PrlVer":null,"Kunnr":"1770349","KunnrDesc":null,"Lfdate":"2017-04-08","GidateM":"2017-04-08","ProdDate":"2017-03-30","Ber":"N","ActDate":"- -","WtyEndDate":"2018-07-30","WtyFlag":"IW","WtyChangeFlag":null,"Ret":"0","RetMsg":null,"RetMat":"REC-G950UZKAVZWPHN","ExtWtyContractNo":null,"ExtWtyContractName":null,"ExtWtyInformation":null,"ExtWtyStartDate":"0000-00-00","ExtWtyEndDate":"0000-00-00"}]},"ItInput":{"item":{"Imei":"35833008016052","SerialNo":null,"Model":null,"DeviceType":"IMEI","ObjectId":null,"PurchaseDate":"0000-00-00","PostingDate":"0000-00-00","RedoFlag":null,"DeliveryDate":"0000-00-00"}}}}';
        TestMockSingleCallout singleMock = new TestMockSingleCallout(body);
        Test.setMock(HttpCalloutMock.class, singleMock);
        SVC_GCICWarrantyTermCheckCaseBatch batch = new SVC_GCICWarrantyTermCheckCaseBatch(c2.Id,null,false);
        batch.execute(null, new List<Case>{ca3});
        batch.finish(null);
        Test.stopTest();
    }
    @isTest static void caseSerialCheck() {
        //Repair Insert,Update
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='SVC-06';
        iep2.endPointURL__c = 'https://www.google.com';
        iep2.layout_id__c = 'LAY025151';
        iep2.partial_endpoint__c = '/IF_SVC_06';
        insert iep2;

        /*Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = rtMap.get('Account' + 'Temporary'),
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );
        insert testAccount1;*/
        account testAccount1 = [select id from Account limit 1];

        Product2 prod1 = new Product2(
                name ='SM-G950UZKAVZW',
                SAP_Material_ID__c = 'SM-G950UZKAVZW',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = rtMap.get('Product2'+'B2B')
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent;

        // Insert Contact non-named caller 
        Contact contact1 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest2@svctest.com', 
            LastName = 'Named Caller2',
            Named_Caller__c = true,
            FirstName = 'Named Caller',
            MailingStreet = '1234 Main',
            MailingCity = 'Chicago',
            MailingState = 'IL',
            MailingCountry = 'US');
        insert contact1; 

        //repair Valid imei
        Case c1 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Repair'),
            status='New',
            Parent_Case__c = true
            );
        insert c1;

        Case c2 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Repair'),
            status='New',
            ParentId = c1.Id,
            SerialNumber__c = '35833008016052',
            Model_Code_Repair__c = 'SM-G950UZKAVZW'
            );
        insert c2;

        Test.startTest();
        Case ca3  = [SELECT Id 
                            , IMEI__c
                            , SerialNumber__c
                            , Device_Type_f__c                      //formula
                            , IMEI_Number_Device__c                 //formula
                            , Serial_Number_Device__c               //formula
                            , RecordType.DeveloperName
                            , Device__c
                            , Device__r.Model_Name__c
                            , Case_Device_Status__c
                            , Parent_Case__c
                            , ParentId
                            , Warranty_Status__c
                            , Model_Code__c
                            , Model_Code_Repair__c
                            , Extended_Warranty_Contract_No__c
                            , Extended_Warranty_Start_Date__c
                            , Extended_Warranty_Contract_Name__c
                            , Extended_Warranty_End_Date__c
                            , Extended_Warranty_Information__c 
                    FROM Case 
                    WHERE id =: c2.Id];
        String body = '{"inputHeaders":{"MSGGUID":"101003b0-f928-7728-94b4-5e1edb0293ad","IFID":"IF_SVC_06","IFDate":"20170722041034","MSGSTATUS":"S","ERRORTEXT":null},"body":{"EtOutput":{"item":[{"Imei":null,"SerialNo":"35833008016052","Model":"SM-G950UZKAVZW","Gidate":"2017-04-02","SwVer":"QC8","HwVer":null,"PrlVer":null,"Kunnr":"1770349","KunnrDesc":null,"Lfdate":"2017-04-08","GidateM":"2017-04-08","ProdDate":"2017-03-30","Ber":"N","ActDate":"- -","WtyEndDate":"2018-07-30","WtyFlag":"IW","WtyChangeFlag":null,"Ret":"0","RetMsg":null,"RetMat":"REC-G950UZKAVZWPHN","ExtWtyContractNo":null,"ExtWtyContractName":null,"ExtWtyInformation":null,"ExtWtyStartDate":"0000-00-00","ExtWtyEndDate":"0000-00-00"}]},"ItInput":{"item":{"Imei":"35833008016052","SerialNo":null,"Model":null,"DeviceType":"IMEI","ObjectId":null,"PurchaseDate":"0000-00-00","PostingDate":"0000-00-00","RedoFlag":null,"DeliveryDate":"0000-00-00"}}}}';
        TestMockSingleCallout singleMock = new TestMockSingleCallout(body);
        Test.setMock(HttpCalloutMock.class, singleMock);
        SVC_GCICWarrantyTermCheckCaseBatch batch = new SVC_GCICWarrantyTermCheckCaseBatch(c2.Id,null,false);
        batch.execute(null, new List<Case>{ca3});
        batch.finish(null);
        Test.stopTest();
    }
    @isTest static void caseSerialInvalidCheck() {
        //Repair Insert,Update
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='SVC-06';
        iep2.endPointURL__c = 'https://www.google.com';
        iep2.layout_id__c = 'LAY025151';
        iep2.partial_endpoint__c = '/IF_SVC_06';
        insert iep2;

        /*Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = rtMap.get('Account' + 'Temporary'),
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );
        insert testAccount1;*/
        account testAccount1 = [select id from Account limit 1];

        Product2 prod1 = new Product2(
                name ='SM-G950UZKAVZW',
                SAP_Material_ID__c = 'SM-G950UZKAVZW',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = rtMap.get('Product2'+'B2B')
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent;

        // Insert Contact non-named caller 
        Contact contact1 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest2@svctest.com', 
            LastName = 'Named Caller2',
            Named_Caller__c = true,
            FirstName = 'Named Caller',
            MailingStreet = '1234 Main',
            MailingCity = 'Chicago',
            MailingState = 'IL',
            MailingCountry = 'US');
        insert contact1; 

        //repair Valid imei
        Case c1 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Repair'),
            status='New',
            Parent_Case__c = true
            );
        insert c1;

        Case c2 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Repair'),
            status='New',
            ParentId = c1.Id,
            SerialNumber__c = '35833008016052',
            Model_Code_Repair__c = 'SM-G950UZKAVZW'
            );
        insert c2;

        Test.startTest();
        Case ca3  = [SELECT Id 
                            , IMEI__c
                            , SerialNumber__c
                            , Device_Type_f__c                      //formula
                            , IMEI_Number_Device__c                 //formula
                            , Serial_Number_Device__c               //formula
                            , RecordType.DeveloperName
                            , Device__c
                            , Device__r.Model_Name__c
                            , Case_Device_Status__c
                            , Parent_Case__c
                            , ParentId
                            , Warranty_Status__c
                            , Model_Code__c
                            , Model_Code_Repair__c
                            , Extended_Warranty_Contract_No__c
                            , Extended_Warranty_Start_Date__c
                            , Extended_Warranty_Contract_Name__c
                            , Extended_Warranty_End_Date__c
                            , Extended_Warranty_Information__c 
                    FROM Case 
                    WHERE id =: c2.Id];
        String body = '{"inputHeaders":{"MSGGUID":"101003b0-f928-7728-94b4-5e1edb0293ad","IFID":"IF_SVC_06","IFDate":"20170722041034","MSGSTATUS":"S","ERRORTEXT":null},"body":{"EtOutput":{"item":[{"Imei":null,"SerialNo":null,"Model":"SM-G950UZKAVZW","Gidate":"2017-04-02","SwVer":"QC8","HwVer":null,"PrlVer":null,"Kunnr":"1770349","KunnrDesc":null,"Lfdate":"2017-04-08","GidateM":"2017-04-08","ProdDate":"2017-03-30","Ber":"N","ActDate":"- -","WtyEndDate":"2018-07-30","WtyFlag":"IW","WtyChangeFlag":null,"Ret":"0","RetMsg":null,"RetMat":"REC-G950UZKAVZWPHN","ExtWtyContractNo":null,"ExtWtyContractName":null,"ExtWtyInformation":null,"ExtWtyStartDate":"0000-00-00","ExtWtyEndDate":"0000-00-00"}]},"ItInput":{"item":{"Imei":"35833008016052","SerialNo":null,"Model":null,"DeviceType":"IMEI","ObjectId":null,"PurchaseDate":"0000-00-00","PostingDate":"0000-00-00","RedoFlag":null,"DeliveryDate":"0000-00-00"}}}}';
        TestMockSingleCallout singleMock = new TestMockSingleCallout(body);
        Test.setMock(HttpCalloutMock.class, singleMock);
        SVC_GCICWarrantyTermCheckCaseBatch batch = new SVC_GCICWarrantyTermCheckCaseBatch(c2.Id,null,false);
        batch.execute(null, new List<Case>{ca3});
        batch.finish(null);
        Test.stopTest();
    }
    @isTest static void httpError() {
        //Repair Insert,Update
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='SVC-06';
        iep2.endPointURL__c = 'https://www.google.com';
        iep2.layout_id__c = 'LAY025151';
        iep2.partial_endpoint__c = '/IF_SVC_06';
        insert iep2;

        /*Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = rtMap.get('Account' + 'Temporary'),
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );
        insert testAccount1;*/
        account testAccount1 = [select id from Account limit 1];

        Product2 prod1 = new Product2(
                name ='SM-G950UZKAVZW',
                SAP_Material_ID__c = 'SM-G950UZKAVZW',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = rtMap.get('Product2'+'B2B')
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent;

        // Insert Contact non-named caller 
        Contact contact1 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest2@svctest.com', 
            LastName = 'Named Caller2',
            Named_Caller__c = true,
            FirstName = 'Named Caller',
            MailingStreet = '1234 Main',
            MailingCity = 'Chicago',
            MailingState = 'IL',
            MailingCountry = 'US');
        insert contact1; 

        //repair Valid imei
        Case c1 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Repair'),
            status='New',
            Parent_Case__c = true
            );
        insert c1;

        Case c2 = new Case(
            AccountId = testAccount1.id,
            Subject = 'Case1 named caller', 
            reason = 'test1@test.com', 
            Origin = 'Email',
            contactid=contact1.id,
            RecordTypeId = rtMap.get('Case'+'Repair'),
            status='Valid',
            ParentId = c1.Id,
            SerialNumber__c = '35833008016052',
            Model_Code_Repair__c = 'SM-G950UZKAVZW'
            );
        insert c2;

        Test.startTest();
        String body = '{"inputHeaders":{"MSGGUID":"101003b0-f928-7728-94b4-5e1edb0293ad","IFID":"IF_SVC_06","IFDate":"20170722041034","MSGSTATUS":"S","ERRORTEXT":null},"body":{"EtOutput":{"item":[{"Imei":,"SerialNo":"35833008016052","Model":"SM-G950UZKAVZW","Gidate":"2017-04-02","SwVer":"QC8","HwVer":null,"PrlVer":null,"Kunnr":"1770349","KunnrDesc":null,"Lfdate":"2017-04-08","GidateM":"2017-04-08","ProdDate":"2017-03-30","Ber":"N","ActDate":"- -","WtyEndDate":"2018-07-30","WtyFlag":"IW","WtyChangeFlag":null,"Ret":"0","RetMsg":null,"RetMat":"REC-G950UZKAVZWPHN","ExtWtyContractNo":null,"ExtWtyContractName":null,"ExtWtyInformation":null,"ExtWtyStartDate":"0000-00-00","ExtWtyEndDate":"0000-00-00"}]},"ItInput":{"item":{"Imei":"35833008016052","SerialNo":null,"Model":null,"DeviceType":"IMEI","ObjectId":null,"PurchaseDate":"0000-00-00","PostingDate":"0000-00-00","RedoFlag":null,"DeliveryDate":"0000-00-00"}}}}';
        TestMockSingleCallout singleMock = new TestMockSingleCallout(body, 505);
        Test.setMock(HttpCalloutMock.class, singleMock);
        SVC_GCICWarrantyTermCheckCaseBatch batch = new SVC_GCICWarrantyTermCheckCaseBatch(c2.Id,null,false);
        batch.execute(null, new List<Case>{c2});
        batch.finish(null);
        Test.stopTest();
    }
}