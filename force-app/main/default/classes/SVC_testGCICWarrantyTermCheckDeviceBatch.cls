/**
 * Created by ms on 2017-08-08.
 *
 * author : JeongHo.Lee, I2MAX
 */
@isTest
private class SVC_testGCICWarrantyTermCheckDeviceBatch {
    @isTest static void deviceIMEICheck() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        //Repair Insert,Update
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='SVC-06';
        iep2.endPointURL__c = 'https://www.google.com';
        iep2.layout_id__c = 'LAY025151';
        iep2.partial_endpoint__c = '/IF_SVC_06';
        insert iep2;

        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = rtMap.get('Account' + 'Temporary'),
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );

        insert testAccount1;

        Product2 prod1 = new Product2(
                name ='SM-G950UZKAVZW',
                SAP_Material_ID__c = 'SM-G950UZKAVZW',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = rtMap.get('Product2'+'B2B')
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent;

        // Insert Contact non-named caller 
        Contact contact1 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest2@svctest.com', 
            LastName = 'Named Caller2',
            Named_Caller__c = true,
            FirstName = 'Named Caller',
            MailingStreet = '1234 Main',
            MailingCity = 'Chicago',
            MailingState = 'IL',
            MailingCountry = 'US');
        insert contact1; 

         //valid
        Device__c dev1 = new Device__c(
            Account__c = testAccount1.id,
            Entitlement__c = ent.id,
            Status__c = 'Valid',
            Imei__c = '35833008016052'
            );
        insert dev1;

        Test.startTest();
        String body = '{"inputHeaders":{"MSGGUID":"101003b0-f928-7728-94b4-5e1edb0293ad","IFID":"IF_SVC_06","IFDate":"20170722041034","MSGSTATUS":"S","ERRORTEXT":null},"body":{"EtOutput":{"item":[{"Imei":"35833008016052","SerialNo":null,"Model":"SM-G950UZKAVZW","Gidate":"2017-04-02","SwVer":"QC8","HwVer":null,"PrlVer":null,"Kunnr":"1770349","KunnrDesc":null,"Lfdate":"2017-04-08","GidateM":"2017-04-08","ProdDate":"2017-03-30","Ber":"N","ActDate":"- -","WtyEndDate":"2018-07-30","WtyFlag":"IW","WtyChangeFlag":null,"Ret":"0","RetMsg":null,"RetMat":"REC-G950UZKAVZWPHN","ExtWtyContractNo":null,"ExtWtyContractName":null,"ExtWtyInformation":null,"ExtWtyStartDate":"0000-00-00","ExtWtyEndDate":"0000-00-00"}]},"ItInput":{"item":{"Imei":"35833008016052","SerialNo":null,"Model":null,"DeviceType":"IMEI","ObjectId":null,"PurchaseDate":"0000-00-00","PostingDate":"0000-00-00","RedoFlag":null,"DeliveryDate":"0000-00-00"}}}}';
        TestMockSingleCallout singleMock = new TestMockSingleCallout(body);
        Test.setMock(HttpCalloutMock.class, singleMock);
        SVC_GCICWarrantyTermCheckDeviceBatch batch = new SVC_GCICWarrantyTermCheckDeviceBatch('IMEI');
        batch.execute(null, new List<Device__c>{dev1});
        batch.finish(null);
        Test.stopTest();
    }
    @isTest static void deviceSerialCheck() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        //Repair Insert,Update
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='SVC-06';
        iep2.endPointURL__c = 'https://www.google.com';
        iep2.layout_id__c = 'LAY025151';
        iep2.partial_endpoint__c = '/IF_SVC_06';
        insert iep2;

        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = rtMap.get('Account' + 'Temporary'),
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );

        insert testAccount1;

        Product2 prod1 = new Product2(
                name ='SM-G950UZKAVZW',
                SAP_Material_ID__c = 'SM-G950UZKAVZW',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = rtMap.get('Product2'+'B2B')
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent;

        // Insert Contact non-named caller 
        Contact contact1 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest2@svctest.com', 
            LastName = 'Named Caller2',
            Named_Caller__c = true,
            FirstName = 'Named Caller',
            MailingStreet = '1234 Main',
            MailingCity = 'Chicago',
            MailingState = 'IL',
            MailingCountry = 'US');
        insert contact1; 

         //valid
        Device__c dev1 = new Device__c(
            Account__c = testAccount1.id,
            Entitlement__c = ent.id,
            Status__c = 'Valid',
            Serial_Number__c = '35833008016052',
            Model_Name__c = 'SM-G950UZKAVZW'
            );
        insert dev1;

        Test.startTest();
        String body = '{"inputHeaders":{"MSGGUID":"101003b0-f928-7728-94b4-5e1edb0293ad","IFID":"IF_SVC_06","IFDate":"20170722041034","MSGSTATUS":"S","ERRORTEXT":null},"body":{"EtOutput":{"item":[{"Imei":null,"SerialNo":"35833008016052","Model":"SM-G950UZKAVZW","Gidate":"2017-04-02","SwVer":"QC8","HwVer":null,"PrlVer":null,"Kunnr":"1770349","KunnrDesc":null,"Lfdate":"2017-04-08","GidateM":"2017-04-08","ProdDate":"2017-03-30","Ber":"N","ActDate":"- -","WtyEndDate":"2018-07-30","WtyFlag":"IW","WtyChangeFlag":null,"Ret":"0","RetMsg":null,"RetMat":"REC-G950UZKAVZWPHN","ExtWtyContractNo":null,"ExtWtyContractName":null,"ExtWtyInformation":null,"ExtWtyStartDate":"0000-00-00","ExtWtyEndDate":"0000-00-00"}]},"ItInput":{"item":{"Imei":"35833008016052","SerialNo":null,"Model":null,"DeviceType":"IMEI","ObjectId":null,"PurchaseDate":"0000-00-00","PostingDate":"0000-00-00","RedoFlag":null,"DeliveryDate":"0000-00-00"}}}}';
        TestMockSingleCallout singleMock = new TestMockSingleCallout(body);
        Test.setMock(HttpCalloutMock.class, singleMock);
        SVC_GCICWarrantyTermCheckDeviceBatch batch = new SVC_GCICWarrantyTermCheckDeviceBatch('IMEI');
        batch.execute(null, new List<Device__c>{dev1});
        batch.finish(null);
        Test.stopTest();
    }
    @isTest static void httpError() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        //Repair Insert,Update
        Map<String, Id> rtMap = new Map<String, Id>();
        for(RecordType rt : [SELECT id, DeveloperName, SobjectType FROM RecordType ]){
            rtMap.put(rt.SobjectType+rt.DeveloperName, rt.Id);
        }

        Integration_EndPoints__c iep2 = new Integration_EndPoints__c();
        iep2.name ='SVC-06';
        iep2.endPointURL__c = 'https://www.google.com';
        iep2.layout_id__c = 'LAY025151';
        iep2.partial_endpoint__c = '/IF_SVC_06';
        insert iep2;

        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = rtMap.get('Account' + 'Temporary'),
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );

        insert testAccount1;

        Product2 prod1 = new Product2(
                name ='SM-G950UZKAVZW',
                SAP_Material_ID__c = 'SM-G950UZKAVZW',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = rtMap.get('Product2'+'B2B')
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            Units_Allowed__c = 5000,
            startdate = system.today()-1
            );
        insert ent;

        // Insert Contact non-named caller 
        Contact contact1 = new Contact( 
            AccountId = testAccount1.id, 
            Email = 'svcTest2@svctest.com', 
            LastName = 'Named Caller2',
            Named_Caller__c = true,
            FirstName = 'Named Caller',
            MailingStreet = '1234 Main',
            MailingCity = 'Chicago',
            MailingState = 'IL',
            MailingCountry = 'US');
        insert contact1; 

         //valid
        Device__c dev1 = new Device__c(
            Account__c = testAccount1.id,
            Entitlement__c = ent.id,
            Status__c = 'Valid',
            Imei__c = '35833008016052'
            );
        insert dev1;

        Test.startTest();
        String body = '{"inputHeaders":{"MSGGUID":"101003b0-f928-7728-94b4-5e1edb0293ad","IFID":"IF_SVC_06","IFDate":"20170722041034","MSGSTATUS":"S","ERRORTEXT":null},"body":{"EtOutput":{"item":[{"Imei":"35833008016052","SerialNo":null,"Model":"SM-G950UZKAVZW","Gidate":"2017-04-02","SwVer":"QC8","HwVer":null,"PrlVer":null,"Kunnr":"1770349","KunnrDesc":null,"Lfdate":"2017-04-08","GidateM":"2017-04-08","ProdDate":"2017-03-30","Ber":"N","ActDate":"- -","WtyEndDate":"2018-07-30","WtyFlag":"IW","WtyChangeFlag":null,"Ret":"0","RetMsg":null,"RetMat":"REC-G950UZKAVZWPHN","ExtWtyContractNo":null,"ExtWtyContractName":null,"ExtWtyInformation":null,"ExtWtyStartDate":"0000-00-00","ExtWtyEndDate":"0000-00-00"}]},"ItInput":{"item":{"Imei":"35833008016052","SerialNo":null,"Model":null,"DeviceType":"IMEI","ObjectId":null,"PurchaseDate":"0000-00-00","PostingDate":"0000-00-00","RedoFlag":null,"DeliveryDate":"0000-00-00"}}}}';
        TestMockSingleCallout singleMock = new TestMockSingleCallout(body, 505);
        Test.setMock(HttpCalloutMock.class, singleMock);
        SVC_GCICWarrantyTermCheckDeviceBatch batch = new SVC_GCICWarrantyTermCheckDeviceBatch('IMEI');
        batch.execute(null, new List<Device__c>{dev1});
        batch.finish(null);
        Test.stopTest();
    }
}