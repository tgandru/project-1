@isTest
private class SVC_testSendNewDeviceEmail {
    
    @isTest static void testSendNewDeviceEmail() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        // Implement test code
        Profile adminProfile = [SELECT Id FROM Profile Where Name ='B2B Service System Administrator'];

        User admin1 = new User (
            FirstName = 'admin1',
            LastName = 'admin1',
            Email = 'admin1@example.com',
            Alias = 'admint1',
            Username = 'sdsasea1admin1@example.com',
            ProfileId = adminProfile.Id,
            TimeZoneSidKey = 'America/New_York',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US'
        );
        
        insert admin1;
        
        User admin2 = new User (
            FirstName = 'admin2',
            LastName = 'admin2',
            Email = 'admin2@example.com',
            Alias = 'admint2',
            Username = 'myservice@example.com',
            ProfileId = adminProfile.Id,
            TimeZoneSidKey = 'America/New_York',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US'
        );
        
        insert admin2;
        
        // Setup Test Account
        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = accountRT.Id,
            Service_Account_Manager__c = admin1.id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );

        insert testAccount1;
        
        Account testAccount2 = new Account (
            Name = 'TestAcc2',
            RecordTypeId = accountRT.Id,
            Service_Account_Manager__c = admin2.id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );

        insert testAccount2;

        Account testAccount3 = new Account (
            Name = 'TestAcc3',
            RecordTypeId = accountRT.Id,
            Service_Account_Manager__c = admin2.id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );

        insert testAccount3;

        // Setup Test device
        
        device__c dev1 = new device__c(
            account__c = testAccount1.id,
            imei__c = '123467899012234',
            status__c = 'Invalid'
            
        );
        
        insert dev1;
        
        
        device__c dev2 = new device__c(
            imei__c = '223467899012234',
            account__c = testAccount2.id,
            status__c = 'Invalid'
        );
        
        insert dev2;

        device__c dev3 = new device__c(
            imei__c = '423467899012234',
            account__c = testAccount3.id,
            status__c = 'Invalid'
        );
        
        insert dev3;
        
        test.startTest();
        SendNewDeviceEmail  emailtest = new SendNewDeviceEmail ();
        emailtest.EmailNewDeviceList();
        test.stopTest();
    }
    
    
}