@isTest
private class SVC_testinContactAPICallout {
    @testSetup static void setupTestData() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        
        List<Zipcode_Lookup__c> zipList = new List<Zipcode_Lookup__c>(); 
        Zipcode_Lookup__c  zip1 = new Zipcode_Lookup__c(Name='07660',City_Name__c='RIDGEFIELD PARK', Country_Code__c='US', State_Code__c='NJ',State_Name__c='New Jersey');
        zipList.add(zip1);
        Zipcode_Lookup__c  zip2 = new Zipcode_Lookup__c(Name='07650',City_Name__c='PALISADES PARK', Country_Code__c='US', State_Code__c='NJ',State_Name__c='New Jersey');
        zipList.add(zip2);
        insert zipList;
        
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        );

        insert testAccount1;

        // Insert Contact 
        Contact contact = new Contact(); 
        contact.AccountId = testAccount1.id; 
        contact.Email = 'svcTest@svctest.com'; 
        contact.LastName = 'svcTestLastName'; 
        contact.Named_Caller__c = true;
        contact.FirstName = 'FirstName';
        contact.MailingCity         = testAccount1.BillingCity;
        contact.MailingCountry      = testAccount1.BillingCountry;
        contact.MailingPostalCode   = testAccount1.BillingPostalCode;
        contact.MailingState        = testAccount1.BillingState;
        contact.MailingStreet       = testAccount1.BillingStreet;
        insert contact; 

        RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];
        Product2 prod1 = new Product2(
                name ='MI-test',
                SAP_Material_ID__c = 'MI-test',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = productRT.id
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        BusinessHours bh24 = [SELECT Id FROM BusinessHours WHERE name = 'B2B Service Hours 24x7'];
    

        Entitlement ent = new Entitlement(
            name ='test Entitlement',
            accountid=testAccount1.id,
            assetid = ast.id,
            BusinessHoursId = bh24.id
            );
        insert ent;


        Case case1 = new Case(); 
        case1.AccountId = testAccount1.id; 
        case1.Subject = 'svc_test case1'; 
        case1.reason = 'test@test.com'; 
        case1.Origin = 'Web';
        case1.contactid=contact.id;
        insert case1; 
        Case case2 = new Case(); 
        case2.AccountId = testAccount1.id; 
        case2.Subject = 'svc_test case2'; 
        case2.reason = 'test2@test.com'; 
        case2.Origin = 'Phone';
        case2.contactid=contact.id;
        insert case2; 

        SCinContact_Constant__c setting = new SCinContact_Constant__c();
        setting.Name = 'Samsung incontact settings';
        setting.API_Endpoint__c = 'https://api-C8.incontact.com/inContactAPI/services/v8.0/interactions/work-items';
        setting.API_User_Name__c ='APIuser@Samsung.com';
        setting.API_User_Password__c ='jD@NhlAJ2B3sl928cy';
        setting.Application_Name__c ='SamsungAPI';
        setting.Business_Unit_Number__c ='4594422';
        setting.Point_of_Contact_ID__c ='1278509';
        setting.Point_of_Contact_Name__c ='SamsungCase';
        setting.Vendor_Name__c  ='SamsungSTA';
        insert setting;
        IncontactCalloutSwitch__c sw=new IncontactCalloutSwitch__c();
        sw.Name='Make_Call';
        sw.TurnOn__c=true;
        insert sw;

    }
    
    @isTest static void testCalloutToInContact() {
         Test.setMock(HttpCalloutMock.class, new SVC_MockinContactHttpToken());

        List<id> caseIDs = new List<id>();
        List<case> cases = [select id from case where subject in ('svc_test case1','svc_test case2')];
        for (case c:cases){
            caseIds.add(c.id);
        }
     SCInContactIntegration.sendCaseDatatoInContact(caseIDs);
    }

    
}