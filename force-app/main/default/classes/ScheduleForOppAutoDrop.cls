global class ScheduleForOppAutoDrop implements Schedulable{

  global void execute(SchedulableContext sc) {
      BatchForOpptyAutoDrop bo = new BatchForOpptyAutoDrop();
      database.executebatch(bo);
  }

}