@isTest
public class ScheduleForOppAutoDropTest {  
    public Static String CRON_EXP = '0 0 12 15 2 ?'; //To execute schedulable 
    
    static testMethod void testScheduledJob() {
        Test.startTest();
        String jobid = System.schedule('TestScheduleBatchJobName',CRON_EXP, new ScheduleForOppAutoDrop());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id=:jobid];
        System.assertEquals(CRON_EXP, ct.CronExpression);
        
        System.assertEquals(0, ct.TimesTriggered);
        Test.stopTest();
    }

}