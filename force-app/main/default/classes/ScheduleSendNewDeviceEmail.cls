global class ScheduleSendNewDeviceEmail implements Schedulable {
	global void execute(SchedulableContext sc) {
		SendNewDeviceEmail newEmails = new SendNewDeviceEmail();
		newEmails.EmailNewDeviceList();
	}
}