global class SchedulerHAPriceBookIntegrationHelper implements Schedulable{

  global void execute(SchedulableContext sc) {
    BatchforHAPriceBookIntegrationHelper ba = new BatchforHAPriceBookIntegrationHelper();
    database.executebatch(ba,1);
    /*
    integer scheduleInterval = 1;
    
    try{
            
      //Call batchJob
      if(BatchJobHelper.canThisBatchRun(BatchforHAPriceBookIntegrationHelper.class.getName())  && !Test.isRunningTest()){
        Database.executeBatch(new BatchforHAPriceBookIntegrationHelper(), 100);        
      }
    }
    catch(Exception ex){
      System.debug('Scheduled Job failed : ' + ex.getMessage());
      //Next part of the scheduler needs to run regardless of what happens in the logic above.      
    }


    Integer rqmi = scheduleInterval;

    DateTime timenow = DateTime.now().addDays(rqmi);

    SchedulerHAPriceBookIntegrationHelper rqm = new SchedulerHAPriceBookIntegrationHelper();
    
    String seconds = '0';
    String minutes = String.valueOf(timenow.minute());
    String hours = String.valueOf(timenow.hour());
    
    //Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
    String sch = seconds + ' ' + minutes + ' ' + hours + ' * * ?';
    String jobName = 'SchedulerHAPriceBookIntegrationHelper - ' + timenow;
    system.schedule(jobName, sch, rqm);
    
    if (sc != null)  {  
      system.abortJob(sc.getTriggerId());      
    }
    */
  }
}