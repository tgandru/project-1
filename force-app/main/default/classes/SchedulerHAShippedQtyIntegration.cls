global class SchedulerHAShippedQtyIntegration implements Schedulable{

  global void execute(SchedulableContext sc) {
    integer scheduleInterval = 1;
    
    try{
            
      //Call batchJob
      if(BatchJobHelper.canThisBatchRun(HAShippedQuantityBatch.class.getName())  && !Test.isRunningTest()){
        Database.executeBatch(new HAShippedQuantityBatch(), 100);        
      }
    }
    catch(Exception ex){
      System.debug('Scheduled Job failed : ' + ex.getMessage());
      //Next part of the scheduler needs to run regardless of what happens in the logic above.      
    }


    Integer rqmi = scheduleInterval;

    DateTime timenow = DateTime.now().addDays(rqmi);

    SchedulerHAShippedQtyIntegration rqm = new SchedulerHAShippedQtyIntegration();
    
    String seconds = '0';
    String minutes = String.valueOf(timenow.minute());
    String hours = String.valueOf(timenow.hour());
    
    //Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
    String sch = seconds + ' ' + minutes + ' ' + hours + ' * * ?';
    String jobName = 'SchedulerHAShippedQtyIntegration - ' + timenow;
    system.schedule(jobName, sch, rqm);
    
    if (sc != null)  {  
      system.abortJob(sc.getTriggerId());      
    }
  }
}