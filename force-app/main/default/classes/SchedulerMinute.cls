global class SchedulerMinute implements Schedulable{

	global void execute(SchedulableContext sc) {
		integer scheduleInterval = 5;
		
		try{
            
            //Call Minute batchJob
			//if(BatchJobHelper.canThisBatchRun(BatchJobHelper.webserviceBatch)  && !Test.isRunningTest()){
			if(BatchJobHelper.canThisBatchRun(BatchForMinuteScheduler.class.getName())  && !Test.isRunningTest()){
				Database.executeBatch(new BatchForMinuteScheduler(), 1);				
			}
		}
		catch(Exception ex){
			System.debug('Scheduled Job failed : ' + ex.getMessage());
			//Next part of the scheduler needs to run regardless of what happens in the logic above.			
		}


		Integer rqmi = scheduleInterval;

		DateTime timenow = DateTime.now().addMinutes(rqmi);

		SchedulerMinute rqm = new SchedulerMinute();
		
		String seconds = '0';
		String minutes = String.valueOf(timenow.minute());
		String hours = String.valueOf(timenow.hour());
		
		//Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
		String sch = seconds + ' ' + minutes + ' ' + hours + ' * * ?';
		String jobName = '5 minute scheduler - ' + hours + ':' + minutes;
		system.schedule(jobName, sch, rqm);
		
		if (sc != null)	{	
			system.abortJob(sc.getTriggerId());			
		}
	}
}