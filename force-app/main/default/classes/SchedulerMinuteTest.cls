/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 
 /**
  *   SchedulerMinuteTest
  *   @author: Mir Khan
 **/
@isTest
private class SchedulerMinuteTest {
	
	public Static String CRON_EXP = '0 0 12 15 2 ?'; //To execute schedulable 

    private static String messageQueueId  = '007';
    private static String messageStatus  = 'Test';
    private static String messageQueueStatus = 'Not Started';
    private static Decimal messageQueueRetryCounter = 1;
    //Specifically testing PRMLeadStatusHelper callout
    private static String messageQueueIntegrationFlowType = 'Flow-016';
    private static String messageQueueIdentificationText = 'Test Message Queue';
    
	
	@testSetup static void createMessageQueue() {
	 	message_queue__c mq = new message_queue__c(Identification_Text__c = messageQueueIdentificationText, MSGUID__c = messageQueueId, 
	 												MSGSTATUS__c = messageStatus,  
	 												Status__c = messageQueueStatus, 
	 												retry_counter__c = messageQueueRetryCounter, 
	 												Integration_Flow_Type__c = messageQueueIntegrationFlowType);
	 	insert mq;
	}
    
    
    static testMethod void testScheduledJob() {
        
        Test.startTest();
        //Executing Schedulable Class   
        String jobId = System.schedule('TestScheduledBatch',CRON_EXP, new SchedulerMinute());
        
        // Get the information from the CronTrigger API object 
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        
        // Verify the expressions are the same     
        System.assertEquals(CRON_EXP, ct.CronExpression);
        
        // Verify the job has not run 
        
        System.assertEquals(0, ct.TimesTriggered);    
        Test.stopTest();    
    }
    
    static testMethod void HAtestScheduledJob() {
        
        Test.startTest();
        //Executing Schedulable Class   
        String jobId = System.schedule('TestScheduledBatch',CRON_EXP, new SchedulerHAPriceBookIntegrationHelper ());
        
        // Get the information from the CronTrigger API object 
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        
        // Verify the expressions are the same     
        System.assertEquals(CRON_EXP, ct.CronExpression);
        
        // Verify the job has not run 
        
        System.assertEquals(0, ct.TimesTriggered);    
        Test.stopTest();    
    }
    static testMethod void HPtestScheduledJob() {
        
        Test.startTest();
        //Executing Schedulable Class   
        String jobId = System.schedule('TestScheduledBatch',CRON_EXP, new SchedulerBatchCSV2 ());
        
        // Get the information from the CronTrigger API object 
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        
        // Verify the expressions are the same     
        System.assertEquals(CRON_EXP, ct.CronExpression);
        
        // Verify the job has not run 
        
        System.assertEquals(0, ct.TimesTriggered);    
        Test.stopTest();    
    }
}