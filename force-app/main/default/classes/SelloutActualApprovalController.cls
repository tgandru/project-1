public class SelloutActualApprovalController {
public string commnts{get; set;}
public string knoxemil{get; set;}
public string knoxepid {get; set;}
Public boolean showerr {get; set;}
 Public string  err {get; set;}
 Public Sellout__c so {get; set;}
 Public List<Sellout_Products__c>  slprd {get; set;}
 public Set<id> ids{get; set;}
 public String fileName{get;set;}
    public Blob fileBody {get;set;}
public Attachment attachment {
  get ;
  set;
  }
    public SelloutActualApprovalController(ApexPages.StandardController controller) {
          showerr=False;
          attachment = new Attachment();
          err='';
          so=(Sellout__c) controller.getRecord();
          slprd=new list<Sellout_Products__c>();
          ids=new set<id>();

          if(so.Id !=null){
         Sellout__c sel=[select id,Comments__c,Has_Attachement__c,RecordType.Name,Approval_Status__c,Request_Approval__c,Pre_check_Blank__c,Total_CL_Qty__c,Total_IL_Qty__c,Closing_Month_Year1__c,Subsidiary__c,Description__c,KNOX_Approval_ID__c  from sellout__c where id=:so.id];
         slprd =[Select id,Status__c from Sellout_Products__c where sellout__c=:so.id and Status__c !='OK'];
         so=sel;
         
        
         if(sel.Total_CL_Qty__c <=0 || sel.Total_IL_Qty__c <=0){
             showerr=True;
             err='There are NO Sellout Products to Process. ';  
            return;
         }else if(sel.Approval_Status__c =='Pending Approval'){
              showerr=True;
              err='This Record is alredy In Approval Process (In Pending). ';  
            return;
          }else if(sel.Pre_check_Blank__c >=1){
              showerr=True;
              err='Please Run Pre-check before going to Approval.';  
            return;
          }
        if(slprd.size()>0){
               showerr=True;
               err='There are '+slprd.size() +'   records with Not OK Status, please fix the data before going to Approval.';  
               return;
              
          }
          List<Sellout_Approval_Order__c> selappr = Sellout_Approval_Order__c.getall().values();
          String usrid=UserInfo.getUserId();
          Boolean epidpresent=False;
          for(Sellout_Approval_Order__c s:selappr){
              if(s.Salesforce_UserId__c==usrid && s.Status__c=='Submitter'){
                  knoxepid =s.KNOX_EPID__c;
                  knoxemil=s.KNOX_E_mail__c;
                  epidpresent=true;
              }
          }
          
        
          
       /*   if(sel.Approval_Status__c =='Approved'){
              showerr=True;
              err='This Record is alredy Approved.';  
            return;
          } */
          
          if(sel.Has_Attachement__c==false){
            showerr=True;
            err='Attachement is Missing Please add Attachement';  
            return;
          }
          
          if(epidpresent==false && !Test.isRunningTest()){
              showerr=True;
             err='You are not allowed to Submit for Approval. Please contact System Admin to Setup KNOX ID.';  
            return;
          }
          
     AggregateResult[] ar=[select sum(Quantity__c) qty,Carrier__c cr,IL_CL__c ilcl from Sellout_Products__c where Sellout__c=:so.id and Status__C='OK' and (Carrier__c  !='EPP' and Carrier__c  !='USC') group by Carrier__c,IL_CL__c order by Carrier__c  ];
        Map<string,list<AggregateResult>> aggregateMap=new Map<string,list<AggregateResult>>(); 
      for(AggregateResult a:ar){
          if(aggregateMap.containsKey((string)a.get('cr'))){
             List<AggregateResult> arl = aggregateMap.get((string)a.get('cr'));
                            arl.add(a);
                           aggregateMap.put((string)a.get('cr'), arl);  
          }else{
              aggregateMap.put((string)a.get('cr'), new List<AggregateResult> { a });  
          }
      }
      for(string s:aggregateMap.keyset()){
          String crr=s;
           List<AggregateResult> arl=aggregateMap.get(s);
           if(arl.size()<2){
                for(AggregateResult a:aggregateMap.get(s)){
                    showerr=True;
                    err='Only'+'\t'+(string)a.get('ilcl')+' \t'+'data is avilable for'+'\t'+s+'\t'+'.Please fix(Add both IL and CL) the Data Before going to Approval ';
                }
           }
          
      }
       
      
    }   
    }
    
    Public void uploadfile(){
    
          attachment.OwnerId = UserInfo.getUserId();
          attachment.ParentId = so.id;
          attachment.Description='KNOX Approval';
         if(attachment.name !=null){
            
              insert attachment;
           }
    
    
    }
   public PageReference InsertRecords() {
    System.debug('**Attachment Name**'+fileName);
      try{
       if(Commnts ==Null || Commnts ==''){
         ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Comments are required.'));
         return null;
       }
       if(attachment.name ==Null || attachment.name==''){
         ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Attachement is required.'));
          return null;
       }
      
       
          uploadfile();
            List<Sellout_Approval__c> soaprlst=new list<Sellout_Approval__c>();
               List<Sellout_Approval_Order__c> aprvr=new list<Sellout_Approval_Order__c>([select id,name,Status__c,KNOX_EPID__c,Approval_Type__c,Sequence__c,Approval_Step_User_ID__c,KNOX_E_mail__c from Sellout_Approval_Order__c where Approval_Type__c !='99' and  Sequence__c !=null order by Sequence__c asc ]);
                for(Sellout_Approval_Order__c ord :aprvr){
                    Sellout_Approval__c soapr=new Sellout_Approval__c();
                     soapr.Step_No__c=ord.Sequence__c;
                     soapr.Sellout__c=so.id;
                    if(ord.Sequence__c==0){
                        soapr.KNOX_E_mail__c=knoxemil;
                        soapr.KNOX_EPID__c=knoxepid;
                        soapr.Status__c='Submitted';
                        soapr.Date__c=System.now();
                        soapr.Approver__c=UserInfo.getName();
                        soapr.Comments__c=Commnts;
                        soapr.Approval_Type__c='0';
                    }else{
                       soapr.KNOX_E_mail__c=ord.KNOX_E_mail__c; 
                       soapr.Status__c='Pending';
                       soapr.Comments__c='';
                       soapr.Approver__c=ord.Name;
                       soapr.Approval_Type__c=ord.Approval_Type__c;
                    }
                    
                    soaprlst.add(soapr);
                }
               
               
               if(soaprlst.size()>0){
                  Database.SaveResult[] srList = Database.insert(soaprlst, false);
                 
                  for (Database.SaveResult sr : srList) {
                      if (sr.isSuccess()) {
                          ids.add(sr.getid());
                      }
                      
                  }
                  
                 if(ids.size()>0){
                    KNOXApprovalRequestCreate.callknoxportaltocreateRequest(so.id,ids);
                 } 
                  pageReference pf= new pagereference('/'+so.id);
                 return pf;
                 
            }
          
          
          
      }catch(exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()));
            return null;
      }
      return null;
   } 
   
  
   
   
    

}