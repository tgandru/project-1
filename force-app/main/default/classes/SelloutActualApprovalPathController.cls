public class SelloutActualApprovalPathController {
 Public Sellout__c so {get; set;}
  Public String rtn {get; set;}
 Public List<Sellout_Approval__c>  slapprv {get; set;}
 Public boolean  showmsg {get; set;}
 Public boolean  refreshpg {get; set;}
  Public boolean  showcancel {get; set;}
    public SelloutActualApprovalPathController(ApexPages.StandardController controller) {
       rtn='';
       so=(Sellout__c) controller.getRecord();
       showcancel =false;
       showmsg =false;
       refreshpg =false;
       if(so.Id !=null){
       so=[select RecordTypeId,id,name,Approval_Status__c,Closing_Month_Year1__c,Comments__c,KNOX_Approval_ID__c,Description__c,Has_Attachement__c,OwnerId ,Integration_Status__c ,Total_CL_Qty__c,Total_IL_Qty__c,Subsidiary__c from Sellout__c where id=:so.id limit 1];
        slapprv =new list<Sellout_Approval__c>([select id,Step_No__c,Type__c,Approval_Type__c,Approver__c,Status__c,KNOX_Approval_ID__c,KNOX_E_mail__c,Date__c,Comments__c,createddate from Sellout_Approval__c where Sellout__c=:so.id  order by createddate desc,Step_No__c desc ]);
         if(so.Approval_Status__c =='Pending Approval' && So.KNOX_Approval_ID__c !=null){
           showcancel =true;
         }
       }
    }
    
    public void recallapprovalprocess(){
       if(so.Approval_Status__c =='Pending Approval' && So.KNOX_Approval_ID__c !=null){
         System.debug('Call Cancel Method');
          update so;
       }else{
          rtn ='Only Pending Approvals Can Cancel/Recall';
          showmsg =true;
       }
    
    }
    
     public void CallIntegrationMethod(){
       if(showmsg ==false && So.KNOX_Approval_ID__c !=null ){
           rtn= KNOXApprovalCancelRequest.cancelrequest(so.id);
           if(rtn !='Sucessfully Canceled the Request'){
                showmsg =True;
           }
           
       }
       
     }
     
     public void CallStatusUpdateMethod(){
     refreshpg =false;
       if(showmsg ==false && So.KNOX_Approval_ID__c !=null){
          rtn =  KNOXApprovalStatus.getApprovalStatus(so.id);
          refreshpg =true;
       }else{
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.info,rtn));
       }
     }

}