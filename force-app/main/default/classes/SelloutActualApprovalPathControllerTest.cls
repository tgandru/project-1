@isTest
private class SelloutActualApprovalPathControllerTest {

	private static testMethod void test() {
          Test.startTest();
          Sellout__C so=new Sellout__C(Closing_Month_Year1__c='10/2018',Description__c='From Test Class',Subsidiary__c='SEA',KNOX_Approval_ID__c='TEST132616161561',Approval_Status__c='Pending Approval',Integration_Status__c='Draft',Has_Attachement__c=true,Request_Approval__c=true);
         insert so;
         Date d=System.today();
                  RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];

          Product2 prod1 = new Product2(
                name ='SM-TEST123',
                SAP_Material_ID__c = 'SM-TEST123',
                RecordTypeId = productRT.id
            );

        insert prod1;
          List<Sellout_Products__c> slp=new list<Sellout_Products__c>();
          Sellout_Products__c sl0=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST123',Quantity__c=100,Carrier__c='ATT',Carrier_SKU__c='Samsung Mobile',IL_CL__c='CL',Pre_Check_Result__c='OK');
          Sellout_Products__c sl1=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST123',Quantity__c=100,Carrier__c='ATT',Carrier_SKU__c='Samsung Mobile',IL_CL__c='IL',Pre_Check_Result__c='OK');
          slp.add(sl0);
          slp.add(sl1);
          insert slp;
             KNOX_Credentials__c kn=new KNOX_Credentials__c(Name='Sandbox',Base_URL__c='www.google.com',Token__c='abc12346590249',Company_ID__c='Sandbox');
            insert kn;
            KNOX_Credentials__c kn1=new KNOX_Credentials__c(Name='Production',Base_URL__c='www.google.com',Token__c='abc12346590249',Company_ID__c='Sandbox');
            insert kn1;
        
    	
    	Pagereference pf= Page.SelloutActualApprovalPath;
           pf.getParameters().put('id',so.id);
           test.setCurrentPage(pf);
        
        apexPages.standardcontroller sc = new apexPages.standardcontroller(so);
          SelloutActualApprovalPathController cls=new SelloutActualApprovalPathController(sc);
         cls.recallapprovalprocess();
         cls.CallStatusUpdateMethod();
         cls.CallIntegrationMethod();
    	
    	
    	Test.stopTest();
    	
	}

}