public class SelloutApprovalPageController {
 Public boolean showerr {get; set;}
 Public string  err {get; set;}
 Public Sellout__c so {get; set;}
 Public List<Sellout_Products__c>  slprd {get; set;}
  public Attachment attachment {
  get {
      if (attachment == null)
        attachment = new Attachment();
      return attachment;
    }
  set;
  }
    public SelloutApprovalPageController(ApexPages.StandardController controller) {
          showerr=False;
          err='';
          so=(Sellout__c) controller.getRecord();
          slprd=new list<Sellout_Products__c>();
          if(so.Id !=null){
         Sellout__c sel=[select id,Comments__c,Has_Attachement__c,Approval_Status__c,Request_Approval__c,Pre_check_Blank__c,Total_CL_Qty__c,Total_IL_Qty__c  from sellout__c where id=:so.id];
         slprd =[Select id,Status__c from Sellout_Products__c where sellout__c=:so.id and Status__c !='OK'];
         
         
         List<ProcessInstance> AppRecs=new  List<ProcessInstance>([SELECT Status, TargetObjectId FROM ProcessInstance WHERE TargetObject.Type = 'Sellout__C' and (Status='Pending')]);
        
         if(sel.Total_CL_Qty__c <=0 || sel.Total_IL_Qty__c <=0){
             showerr=True;
             err='There are NO Sellout Products to Process. ';  
            return;
         }else if(sel.Approval_Status__c =='Pending Approval'){
              showerr=True;
              err='This Record is alredy In Approval Process (In Pending). ';  
            return;
          }else if(sel.Pre_check_Blank__c >=1){
              showerr=True;
              err='Please Run Pre-check before going to Approval.';  
            return;
          }
        if(slprd.size()>0){
               showerr=True;
               err='There are '+slprd.size() +'   records with Not OK Status, please fix the data before going to Approval.';  
               return;
              
          }
          
          
          
          if(AppRecs.size()>0){
              showerr=True;
              err=' '+AppRecs.size()+'  record already in Approval Process. Please wait until those record get Approved/Rejected.';  
            return;
          } 
          
          if(sel.Approval_Status__c =='Approved'){
              showerr=True;
              err='This Record is alredy Approved.';  
            return;
          }
          
          if(sel.Has_Attachement__c==false){
            showerr=True;
            err='Attachement is Missing Please add Attachement';  
            return;
          }
          
     AggregateResult[] ar=[select sum(Quantity__c) qty,Carrier__c cr,IL_CL__c ilcl from Sellout_Products__c where Sellout__c=:so.id and Status__C='OK' and (Carrier__c  !='EPP' and Carrier__c  !='USC') group by Carrier__c,IL_CL__c order by Carrier__c  ];
        Map<string,list<AggregateResult>> aggregateMap=new Map<string,list<AggregateResult>>(); 
      for(AggregateResult a:ar){
          if(aggregateMap.containsKey((string)a.get('cr'))){
             List<AggregateResult> arl = aggregateMap.get((string)a.get('cr'));
                            arl.add(a);
                           aggregateMap.put((string)a.get('cr'), arl);  
          }else{
              aggregateMap.put((string)a.get('cr'), new List<AggregateResult> { a });  
          }
      }
      for(string s:aggregateMap.keyset()){
          String crr=s;
           List<AggregateResult> arl=aggregateMap.get(s);
           if(arl.size()<2){
                for(AggregateResult a:aggregateMap.get(s)){
                    showerr=True;
                    err='Only'+'\t'+(string)a.get('ilcl')+' \t'+'data is avilable for'+'\t'+s+'\t'+'.Please fix(Add both IL and CL) the Data Before going to Approval ';
                }
           }
          
      }
       
      
    }    
    }
    
    Public void uploadfile(){
         String parentid=label.Sellout_Template_ID;
         attachment.OwnerId = UserInfo.getUserId();
         attachment.ParentId = parentid;
         
         if(attachment.name !=null){
            
              insert attachment;
           }
    }
    public pagereference saveandsubmitforApproval(){
       try{
           uploadfile();
           so.Request_Approval__c=true; 
          Update so;
          
          
          
          
          Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                                req.setComments(so.Comments__c);
                                req.setObjectId(so.Id);
                                // submit the approval request for processing
                                Approval.ProcessResult result = Approval.process(req);
                                // display if the reqeust was successful
                                System.debug('Submitted for approval successfully: '+result.isSuccess());
          
       
       pageReference pf= new pagereference('/'+so.id);
            return pf;
       }catch(exception ex){
             
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()));
            return null;
       
       }
    
                               
    
    }

}