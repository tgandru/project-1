@isTest
private class SelloutApprovalPageControllerTest {

	private static testMethod void testMethod1() {
           Test.startTest();
          Sellout__C so=new Sellout__C(Closing_Month_Year1__c='10/2018',Description__c='From Test Class',Subsidiary__c='SEA',Approval_Status__c='Draft',Integration_Status__c='Draft',Has_Attachement__c=true,Request_Approval__c=true);
         insert so;
         Date d=System.today();
                  RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];

          Product2 prod1 = new Product2(
                name ='SM-TEST123',
                SAP_Material_ID__c = 'SM-TEST123',
                RecordTypeId = productRT.id
            );

        insert prod1;
          List<Sellout_Products__c> slp=new list<Sellout_Products__c>();
          Sellout_Products__c sl0=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST123',Quantity__c=100,Carrier__c='ATT',Carrier_SKU__c='Samsung Mobile',IL_CL__c='CL',Pre_Check_Result__c='OK');
          Sellout_Products__c sl1=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST123',Quantity__c=100,Carrier__c='ATT',Carrier_SKU__c='Samsung Mobile',IL_CL__c='IL',Pre_Check_Result__c='OK');
          slp.add(sl0);
          slp.add(sl1);
          insert slp;
         
         Pagereference pf= Page.SelloutApprovalPage;
           pf.getParameters().put('id',so.id);
           test.setCurrentPage(pf);
        
        apexPages.standardcontroller sc = new apexPages.standardcontroller(so);
          SelloutApprovalPageController cls=new SelloutApprovalPageController(sc);
          Attachment attach=new Attachment();   	
    	attach.Name='Unit Test Attachment';
    	Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
    	attach.body=bodyBlob;
    	cls.attachment =attach;
         
            cls.saveandsubmitforApproval();
         Test.StopTest(); 
	}
   
   	private static testMethod void testMethod2() {
           Test.startTest();
            Id pln = TestDataUtility.retrieveRecordTypeId('Plan', 'Sellout__C');
          Sellout__C so=new Sellout__C(Recordtypeid=pln,Closing_Month_Year1__c='10/2018',Description__c='From Test Class',Subsidiary__c='SEA',Approval_Status__c='Pending Approval',Integration_Status__c='Draft',Request_Approval__c=true);
         insert so;
                  RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];
          Product2 prod1 = new Product2(
                name ='SM-TEST123',
                SAP_Material_ID__c = 'SM-TEST123',
                RecordTypeId = productRT.id
            );

        insert prod1;
         Date d=System.today();
          List<Sellout_Products__c> slp=new list<Sellout_Products__c>();
          Sellout_Products__c sl0=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST123',Quantity__c=100,Carrier__c='ATT',Carrier_SKU__c='Samsung Mobile',IL_CL__c='CL',Pre_Check_Result__c='OK');
          Sellout_Products__c sl1=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST123',Quantity__c=100,Carrier__c='ATT',Carrier_SKU__c='Samsung Mobile',IL_CL__c='IL',Pre_Check_Result__c='OK');
          slp.add(sl0);
          slp.add(sl1);
          insert slp;
         
         Pagereference pf= Page.SelloutApprovalPage;
           pf.getParameters().put('id',so.id);
           test.setCurrentPage(pf);
        
        apexPages.standardcontroller sc = new apexPages.standardcontroller(so);
          SelloutApprovalPageController cls=new SelloutApprovalPageController(sc);
          Attachment attach=new Attachment();   	
    	attach.Name='Unit Test Attachment';
    	Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
    	attach.body=bodyBlob;
    	cls.attachment =attach;
         
            cls.saveandsubmitforApproval();
         Test.StopTest(); 
	}


    	private static testMethod void testMethod3() {
           Test.startTest();
            Id pln = TestDataUtility.retrieveRecordTypeId('Plan', 'Sellout__C');
          Sellout__C so=new Sellout__C(Recordtypeid=pln,Closing_Month_Year1__c='10/2018',Description__c='From Test Class',Subsidiary__c='SEA',Approval_Status__c='Approved',Integration_Status__c='Draft',Request_Approval__c=true,Has_Attachement__c=true);
         insert so;
         Date d=System.today();
               RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];

          Product2 prod1 = new Product2(
                name ='SM-TEST123',
                SAP_Material_ID__c = 'SM-TEST123',
                RecordTypeId = productRT.id
            );

        insert prod1;
          List<Sellout_Products__c> slp=new list<Sellout_Products__c>();
          Sellout_Products__c sl0=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST123',Quantity__c=100,Carrier__c='ATT',Carrier_SKU__c='Samsung Mobile',IL_CL__c='CL');
          Sellout_Products__c sl1=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST123',Quantity__c=100,Carrier__c='ATT',Carrier_SKU__c='Samsung Mobile',IL_CL__c='IL');
          slp.add(sl0);
          slp.add(sl1);
          insert slp;
         
         Pagereference pf= Page.SelloutApprovalPage;
           pf.getParameters().put('id',so.id);
           test.setCurrentPage(pf);
        
        apexPages.standardcontroller sc = new apexPages.standardcontroller(so);
          SelloutApprovalPageController cls=new SelloutApprovalPageController(sc);
          Attachment attach=new Attachment();   	
    	attach.Name='Unit Test Attachment';
    	Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
    	attach.body=bodyBlob;
    	cls.attachment =attach;
         
            cls.saveandsubmitforApproval();
         Test.StopTest(); 
	}
private static testMethod void testMethod4() {
           Test.startTest();
            Id pln = TestDataUtility.retrieveRecordTypeId('Plan', 'Sellout__C');
          Sellout__C so=new Sellout__C(Recordtypeid=pln,Closing_Month_Year1__c='10/2018',Description__c='From Test Class',Subsidiary__c='SEA',Approval_Status__c='Draft',Integration_Status__c='Draft',Request_Approval__c=true,Has_Attachement__c=false);
         insert so;
         Date d=System.today();
               RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];

          Product2 prod1 = new Product2(
                name ='SM-TEST123',
                SAP_Material_ID__c = 'SM-TEST123',
                RecordTypeId = productRT.id
            );

        insert prod1;
          List<Sellout_Products__c> slp=new list<Sellout_Products__c>();
          Sellout_Products__c sl0=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST123',Quantity__c=100,Carrier__c='ATT',Carrier_SKU__c='Samsung Mobile',IL_CL__c='IL');
          Sellout_Products__c sl1=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST123',Quantity__c=100,Carrier__c='ATT',Carrier_SKU__c='Samsung Mobile',IL_CL__c='IL');
          slp.add(sl0);
          slp.add(sl1);
          insert slp;
         
         Pagereference pf= Page.SelloutApprovalPage;
           pf.getParameters().put('id',so.id);
           test.setCurrentPage(pf);
        
        apexPages.standardcontroller sc = new apexPages.standardcontroller(so);
          SelloutApprovalPageController cls=new SelloutApprovalPageController(sc);
         
         
            cls.saveandsubmitforApproval();
         Test.StopTest(); 
	}
	
	
	private static testMethod void testMethod5() {
           Test.startTest();
            Id pln = TestDataUtility.retrieveRecordTypeId('Plan', 'Sellout__C');
          Sellout__C so=new Sellout__C(Recordtypeid=pln,Closing_Month_Year1__c='10/2018',Description__c='From Test Class',Subsidiary__c='SEA',Approval_Status__c='Draft',Integration_Status__c='Draft',Request_Approval__c=true,Has_Attachement__c=false);
         insert so;
         Date d=System.today();
               RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];

          Product2 prod1 = new Product2(
                name ='SM-TEST123',
                SAP_Material_ID__c = 'SM-TEST123',
                RecordTypeId = productRT.id
            );

        insert prod1;
         apexPages.standardcontroller sc = new apexPages.standardcontroller(so);
          SelloutApprovalPageController cls=new SelloutApprovalPageController(sc);
          Attachment attach=new Attachment();   	
    	attach.Name='Unit Test Attachment';
    	Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
    	attach.body=bodyBlob;
    	cls.attachment =attach;
         
            cls.saveandsubmitforApproval();
	     Test.StopTest();
	}

}