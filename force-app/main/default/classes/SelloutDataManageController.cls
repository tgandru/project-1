public class SelloutDataManageController {
public list<Sellout_Products__c> prdcts{get; set;}
public string selectedsoldto {get; set;}
public string selectedstatus {get; set;}
Public Sellout__c so {get; set;}
public boolean showerror {get; set;}
public boolean isPlan {get; set;}
public string  errormsg {get; set;}
Public string strID{get;set;}
public List<listWrapp> listWrappdel {get; set;}

//public Map<string,string>  crrMap {get; set;}
    public SelloutDataManageController(ApexPages.StandardController controller) {
       //crrMap=new Map<string,string>();
       isplan=false;
       so=[select RecordTypeId,id,name,Approval_Status__c,Closing_Month_Year1__c,Description__c,Has_Attachement__c,Total_CL_Qty__c,Total_IL_Qty__c,Subsidiary__c from Sellout__c where id=:controller.getRecord().Id limit 1];
       prdcts=new list<Sellout_Products__c>();
       listWrappdel = new List<listWrapp>();
       String planRT = Sellout__c.sObjectType.getDescribe().getRecordTypeInfosByName().get('Plan').getRecordTypeId();
       if(so.RecordTypeId==planRT){
            isplan=true;
       }
        if(so.Approval_Status__c=='Approved' ||so.Approval_Status__c=='Pending Approval' ){
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'This Sellout record is already Approved or In Pending Approval. Edit will avilable on Draft and Rejected records. '));
            }
      // prdcts=new list<Sellout_Products__c>([select id,name,Amount__c,Carrier__c,Carrier_SKU__c,Invoice_Price__c,Quantity__c,Samsung_SKU__c,Sellout_Date__c,Sold_To_Code__c,Status__c,sellout__r.RecordType.Name from Sellout_Products__c where sellout__c=:so.id]);
      /* soldtoMap= Carrier_Sold_to_Mapping__c.getall().values();
      for(Carrier_Sold_to_Mapping__c c:soldtoMap){
         crrMap.put(c.Sold_To_Code__c,c.Carrier_Code__c);
      }*/
       
    }
public List<SelectOption> getselectsoldtos(){
    List<SelectOption> crOptions = new List<SelectOption>();
    crOptions.add(new SelectOption('--All--','--All--'));
     for(Schema.PicklistEntry pe : Sellout_Products__c.Carrier__c.getDescribe().getPicklistValues()){
         crOptions.add(new SelectOption(pe.getValue(),pe.getValue()));
     }
     return crOptions;
   }
   
public SelectOption[] getselectstatus(){
   //     New SelectOption('Missing Price','Missing Price'),
   //      New SelectOption('Duplicated Record Exist','Duplicate Records')
      return New SelectOption[]{
            New SelectOption('--All--','--All--'),
            New SelectOption('OK','OK'),
            New SelectOption('Missing SKU Map','Missing SKU Map')
      
            
      };
   }   
   public void intialloadingpage(){
       prdcts=new list<Sellout_Products__c>([select id,name,IL_CL__c,Carrier__c,Carrier_SKU__c,Quantity__c,Samsung_SKU__c,Sellout_Date__c,Sold_To_Code__c,Status__c,sellout__r.RecordType.Name,Pre_Check_Result__c from Sellout_Products__c where sellout__c=:so.id order by Name asc]);
            if(so.Approval_Status__c=='Approved' ||so.Approval_Status__c=='Pending Approval' ){
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'This Sellout record is already Approved or In Pending Approval. Edit will avilable on Draft and Rejected records. '));
            }
        listWrappdel = new List<listWrapp>();
        for(Sellout_Products__c p:prdcts){
             listWrappdel.add(new listWrapp(p));
    
        }    
            
            
   }
   
 public void searchbyfilter(){
     if(so.Approval_Status__c=='Approved' ||so.Approval_Status__c=='Pending Approval' ){
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'This Sellout record is already Approved or In Pending Approval. Edit will avilable on Draft and Rejected records. '));
            }
     
   If(selectedsoldto !='--All--'&&selectedstatus =='--All--'){
       
      prdcts=new list<Sellout_Products__c>([select id,name,IL_CL__c,Carrier__c,Carrier_SKU__c,Quantity__c,Samsung_SKU__c,Sellout_Date__c,Sold_To_Code__c,Status__c,sellout__r.RecordType.Name,Pre_Check_Result__c from Sellout_Products__c where sellout__c=:so.id and Carrier__c=:selectedsoldto order by Name asc]);
    }else if(selectedstatus !='--All--'&&selectedsoldto =='--All--'){
      
       prdcts=new list<Sellout_Products__c>([select id,name,IL_CL__c,Carrier__c,Carrier_SKU__c,Quantity__c,Samsung_SKU__c,Sellout_Date__c,Sold_To_Code__c,Status__c,sellout__r.RecordType.Name,Pre_Check_Result__c from Sellout_Products__c where sellout__c=:so.id and Status__c=:selectedstatus order by Name asc]); 
    }else if(selectedsoldto !='--All--'&&selectedstatus !='--All--'){
       prdcts=new list<Sellout_Products__c>([select id,name,IL_CL__c,Carrier__c,Carrier_SKU__c,Quantity__c,Samsung_SKU__c,Sellout_Date__c,Sold_To_Code__c,Status__c,sellout__r.RecordType.Name,Pre_Check_Result__c from Sellout_Products__c where sellout__c=:so.id and Status__c=:selectedstatus and Carrier__c=:selectedsoldto order by Name asc]); 
 
    }else if(selectedsoldto =='--All--'&&selectedstatus =='--All--'){
        
        prdcts=new list<Sellout_Products__c>([select id,name,IL_CL__c,Carrier__c,Carrier_SKU__c,Quantity__c,Samsung_SKU__c,Sellout_Date__c,Sold_To_Code__c,Status__c,sellout__r.RecordType.Name,Pre_Check_Result__c from Sellout_Products__c where sellout__c=:so.id order by Name asc]);
    }
    
    
    listWrappdel = new List<listWrapp>();
    for(Sellout_Products__c p:prdcts){
         listWrappdel.add(new listWrapp(p));

    }
     
 } 
 
 
 public void deleterec(){
   Sellout_Products__c tmp = new Sellout_Products__c();
    for(Sellout_Products__c sp :prdcts){
        if(sp.id == strID){
            tmp = sp;
        }
    }
    delete tmp;
 }
 public void UpdateMapping(){
    //strID= ApexPages.currentpage().getParameters().get('strIDs');
    system.debug('Row ID::'+strID);
   try{ 
    Sellout_Products__c tmp = new Sellout_Products__c();
    for(Sellout_Products__c sp :prdcts){
        if(sp.id == strID){
            tmp = sp;
        }
    }
     
    List<Sellout_SKU_Mapping__c> skumap=new list<Sellout_SKU_Mapping__c>([select id,name,Carrier__c,Carrier_SKU__c,Samsung_SKU__c from Sellout_SKU_Mapping__c where Samsung_SKU__c =:tmp.Samsung_SKU__c]);
    if(skumap.size()>0){
     showerror=true;
     errormsg='Currently this SKU  '+tmp.Samsung_SKU__c+'  '+'is Mapped With '+skumap[0].Carrier_SKU__c+'  '+skumap[0].Carrier__c+'   '+' Carrier SKU';
     system.debug('errormsg :: '+errormsg +'---'+showerror);
   
    }else{
         Sellout_SKU_Mapping__c sk=new Sellout_SKU_Mapping__c();
         sk.Carrier_SKU__c=tmp.Carrier_SKU__c;
         sk.Samsung_SKU__c=tmp.Samsung_SKU__c;
         sk.Carrier__c=tmp.Carrier__c;
         insert sk;
         Update tmp;
    }
    
  }catch(exception ex){
     ApexPages.addMessages(ex) ; 
  }
 } 
 
 
 public pageReference savemethod(){
     try{
       update prdcts; 
       return  new pageReference('/'+so.id);
     }catch(exception ex){
        ApexPages.addMessages(ex) ; 
            return null;  
     }
     
 }
 
 
 public PageReference FetchExcelReport() {
        PageReference nextpage = new PageReference('/apex/ExportSelloutLines?id='+so.id);
        nextpage.setRedirect(true);
         return nextpage;
    }
 public class listWrapp{
   public Sellout_Products__c sp {get; set;}
   public Boolean selected {get; set;}
      public listWrapp(Sellout_Products__c s){
         sp=s;
         selected =false;
      }

 }
 
 public void processSelected() {
      System.debug('****1****');
       List<Sellout_Products__c> lstToDelete = new List<Sellout_Products__c>();
        for(listWrapp d: listWrappdel) 
        {
            if(d.selected  == true)
            {
                System.debug('****11****');
                lstToDelete.add(d.sp);
            }
        }
         if(lstToDelete.size() > 0 )
        {
            Delete lstToDelete;
        }
    }
}