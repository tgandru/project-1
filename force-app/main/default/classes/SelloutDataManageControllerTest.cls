@isTest
private class SelloutDataManageControllerTest {

	private static testMethod void testmethod1() {
        Test.startTest();
        
          Product2 prod1 = new Product2(
                name ='SM-TEST123',
                SAP_Material_ID__c = 'SM-TEST123'
                
            );
         insert prod1;
          Sellout__C so=new Sellout__C(Closing_Month_Year1__c='10/2018',Description__c='From Test Class',Subsidiary__c='SEA',Approval_Status__c='Draft',Integration_Status__c='Draft',Has_Attachement__c=true,Request_Approval__c=true);
         insert so;
         Date d=System.today();
          List<Sellout_Products__c> slp=new list<Sellout_Products__c>();
          Sellout_Products__c sl0=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST123',Quantity__c=100,Carrier__c='ATT',Carrier_SKU__c='Samsung Mobile',IL_CL__c='CL');
          Sellout_Products__c sl1=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST123',Quantity__c=100,Carrier__c='ATT',Carrier_SKU__c='Samsung Mobile',IL_CL__c='IL');
          slp.add(sl0);
          slp.add(sl1);
          insert slp;
         
         Pagereference pf= Page.SellOutSKUEdits;
           pf.getParameters().put('id',so.id);
           test.setCurrentPage(pf);
        
        apexPages.standardcontroller sc = new apexPages.standardcontroller(so);
          SelloutDataManageController cls=new SelloutDataManageController(sc);
          cls.selectedsoldto='ATT';
          cls.selectedstatus='OK';
          cls.getselectsoldtos();
          cls.getselectstatus();
          cls.intialloadingpage();
          cls.searchbyfilter();
            cls.UpdateMapping();
            cls.processSelected();
            cls.savemethod();
         Test.StopTest(); 
	}
    
    
    	private static testMethod void testmethod2() {
        Test.startTest();
        Product2 prod1 = new Product2(
                name ='SM-TEST123',
                SAP_Material_ID__c = 'SM-TEST123'
                
            );
         insert prod1;
          Sellout__C so=new Sellout__C(Closing_Month_Year1__c='10/2018',Description__c='From Test Class',Subsidiary__c='SEA',Approval_Status__c='Draft',Integration_Status__c='Draft',Has_Attachement__c=true,Request_Approval__c=true);
         insert so;
         Date d=System.today();
          List<Sellout_Products__c> slp=new list<Sellout_Products__c>();
          Sellout_Products__c sl0=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST123',Quantity__c=100,Carrier__c='ATT',Carrier_SKU__c='Samsung Mobile',IL_CL__c='CL');
          Sellout_Products__c sl1=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST123',Quantity__c=100,Carrier__c='ATT',Carrier_SKU__c='Samsung Mobile',IL_CL__c='IL');
          slp.add(sl0);
          slp.add(sl1);
          insert slp;
         
         Pagereference pf= Page.SellOutSKUEdits;
           pf.getParameters().put('id',so.id);
           test.setCurrentPage(pf);
        
        apexPages.standardcontroller sc = new apexPages.standardcontroller(so);
          SelloutDataManageController cls=new SelloutDataManageController(sc);
          cls.selectedsoldto='ATT';
          cls.selectedstatus='--All--';
          cls.getselectsoldtos();
          cls.getselectstatus();
          cls.intialloadingpage();
          cls.searchbyfilter();
            cls.UpdateMapping();
           cls.selectedsoldto='--All--';
           cls.searchbyfilter();
            cls.FetchExcelReport();
         Test.StopTest(); 
	}
    
}