@isTest
private class SelloutIntegrationHelperTest {

	private static testMethod void testmethod1() {// This Method to Cover Integration for Sending Account Info to GERP
          Sellout__C so=new Sellout__C(Closing_Month_Year1__c='10/2018',Description__c='From Test Class',Subsidiary__c='SEA',Approval_Status__c='Draft',Integration_Status__c='Draft');
         insert so;
         Date d=System.today();
            RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];
          Product2 prod1 = new Product2(
                name ='SM-TEST123',
                SAP_Material_ID__c = 'SM-TEST123',
                RecordTypeId = productRT.id
            );
             insert prod1;
             List<Sellout_Products__c> slp=new list<Sellout_Products__c>();
          Sellout_Products__c sl0=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST123',Quantity__c=100,Carrier__c='ATT',Carrier_SKU__c='Samsung Mobile',IL_CL__c='CL');
           Sellout_Products__c sl1=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST123',Quantity__c=100,Carrier__c='SPR',Carrier_SKU__c='Samsung Mobile',IL_CL__c='CL');
           slp.add(sl0);
           slp.add(sl1);
          insert slp;
          
          List<SECA_Sellout_Data__c> secalst=new List<SECA_Sellout_Data__c>();
          
          SECA_Sellout_Data__c seca1=new SECA_Sellout_Data__c(Version__c='T04',Type__c='Plan',SAP_Company_Code__c='C330',Rollout_Month__c='201903',Quantity__c=20,Opportunity_No__c='TEST-OPP-1',Material_Code__c='SECA-0001',Loading_Month__c='201904',Index_Code__c='D',Data_Type__c='Sellout Account Data');
          SECA_Sellout_Data__c seca2=new SECA_Sellout_Data__c(Version__c='000',Type__c='Actual',SAP_Company_Code__c='C330',Rollout_Month__c='201903',Quantity__c=20,Opportunity_No__c='TEST-OPP-1',Material_Code__c='SECA-0001',Loading_Month__c='201904',Index_Code__c='D',Data_Type__c='Sellout Account Data');
          SECA_Sellout_Data__c seca3=new SECA_Sellout_Data__c(Version__c='T04',Type__c='Plan',SAP_Company_Code__c='C330',Rollout_Month__c='201903',Quantity__c=20,Opportunity_No__c='TEST-OPP-1',Material_Code__c='SECA-0001',Loading_Month__c='201904',Index_Code__c='D',Data_Type__c='Sellout QTY Data');
          SECA_Sellout_Data__c seca4=new SECA_Sellout_Data__c(Version__c='000',Type__c='Actual',SAP_Company_Code__c='C330',Rollout_Month__c='201903',Quantity__c=20,Opportunity_No__c='TEST-OPP-1',Material_Code__c='SECA-0001',Loading_Month__c='201904',Index_Code__c='D',Data_Type__c='Sellout QTY Data');
            secalst.add(seca1);
            secalst.add(seca2);
            secalst.add(seca3);
            secalst.add(seca4);
            insert secalst;
           message_queue__c mq = new message_queue__c(MSGUID__c = '', 
                                                    MSGSTATUS__c = '',  
                                                    Status__c = 'Not Started', 
                                                    retry_counter__c = 0,
                                                    Integration_Flow_Type__c='Flow-031',
                                                    Identification_Text__c=so.id,
                                                    IFDate__c='',
                                                    IFID__c='',
                                                    ERRORTEXT__c='',
                                                    Object_Name__c='Sellout'
                                                    );
                                                    
        insert mq;
        List<Carrier_Sold_to_Mapping__c> crr=new list<Carrier_Sold_to_Mapping__c>();
          Carrier_Sold_to_Mapping__c cr1=new Carrier_Sold_to_Mapping__c(Name='AT&T',Carrier_Code__c='ATT',Sold_To_Code__c='00002222',Sold_To_Code_Pre_Check__c='00003333,00004444');
          Carrier_Sold_to_Mapping__c cr2=new Carrier_Sold_to_Mapping__c(Name='Sprint',Carrier_Code__c='SPR',Sold_To_Code__c='000022229');
          crr.add(cr1);
          crr.add(cr2);
       
        insert crr;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockResponseGenerator('"S"'));
        SelloutAccountIntegration.SendSelloutAccountData(mq);  
        Test.Stoptest();
        
        
          
	}
		private static testMethod void testmethod2() {//This Method to cover Integration for Sending Sellout QTY
          Sellout__C so=new Sellout__C(Closing_Month_Year1__c='10/2018',Description__c='From Test Class',Subsidiary__c='SEA',Approval_Status__c='Draft',Integration_Status__c='Draft');
         insert so;
         Date d=System.today();
            RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];
          Product2 prod1 = new Product2(
                name ='SM-TEST123',
                SAP_Material_ID__c = 'SM-TEST123',
                RecordTypeId = productRT.id
            );
           insert prod1;
          Sellout_Products__c sl0=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST123',Quantity__c=100,Carrier__c='ATT',Carrier_SKU__c='Samsung Mobile',IL_CL__c='CL');
          insert sl0;
           message_queue__c mq = new message_queue__c(MSGUID__c = '', 
                                                    MSGSTATUS__c = '',  
                                                    Status__c = 'Not Started', 
                                                    retry_counter__c = 0,
                                                    Integration_Flow_Type__c='Flow-030',
                                                    Identification_Text__c=so.id,
                                                    IFDate__c='',
                                                    IFID__c='',
                                                    ERRORTEXT__c='',
                                                    Object_Name__c='Sellout'
                                                    );
                                                    
        insert mq;
        List<SECA_Sellout_Data__c> secalst=new List<SECA_Sellout_Data__c>();
          
          SECA_Sellout_Data__c seca1=new SECA_Sellout_Data__c(Version__c='T04',Type__c='Plan',SAP_Company_Code__c='C330',Rollout_Month__c='201903',Quantity__c=20,Opportunity_No__c='TEST-OPP-1',Material_Code__c='SECA-0001',Loading_Month__c='201904',Index_Code__c='D',Data_Type__c='Sellout Account Data');
          SECA_Sellout_Data__c seca2=new SECA_Sellout_Data__c(Version__c='000',Type__c='Actual',SAP_Company_Code__c='C330',Rollout_Month__c='201903',Quantity__c=20,Opportunity_No__c='TEST-OPP-1',Material_Code__c='SECA-0001',Loading_Month__c='201904',Index_Code__c='D',Data_Type__c='Sellout Account Data');
          SECA_Sellout_Data__c seca3=new SECA_Sellout_Data__c(Version__c='T04',Type__c='Plan',SAP_Company_Code__c='C330',Rollout_Month__c='201903',Quantity__c=20,Opportunity_No__c='TEST-OPP-1',Material_Code__c='SECA-0001',Loading_Month__c='201904',Index_Code__c='D',Data_Type__c='Sellout QTY Data');
          SECA_Sellout_Data__c seca4=new SECA_Sellout_Data__c(Version__c='000',Type__c='Actual',SAP_Company_Code__c='C330',Rollout_Month__c='201903',Quantity__c=20,Opportunity_No__c='TEST-OPP-1',Material_Code__c='SECA-0001',Loading_Month__c='201904',Index_Code__c='D',Data_Type__c='Sellout QTY Data');
            secalst.add(seca1);
            secalst.add(seca2);
            secalst.add(seca3);
            secalst.add(seca4);
            insert secalst;
        Carrier_Sold_to_Mapping__c cr=new Carrier_Sold_to_Mapping__c(Name='AT&T',Carrier_Code__c='ATT',Sold_To_Code__c='00002222',Sold_To_Code_Pre_Check__c='00003333,00004444');
        insert cr;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockResponseGenerator('"S"'));
        SelloutQuantityIntegration.SendSelloutQTYData(mq);  
        Test.Stoptest();
        
        
          
	}
	
	private static testMethod void testmethod3() {// This Method to Cover Integration for Sending Account Info to GERP
          Sellout__C so=new Sellout__C(Closing_Month_Year1__c='10/2018',Description__c='From Test Class',Subsidiary__c='SEA',Approval_Status__c='Draft',Integration_Status__c='Draft');
         insert so;
         Date d=System.today();
            RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];
          Product2 prod1 = new Product2(
                name ='SM-TEST123',
                SAP_Material_ID__c = 'SM-TEST123',
                RecordTypeId = productRT.id
            );
             insert prod1;
             List<Sellout_Products__c> slp=new list<Sellout_Products__c>();
          Sellout_Products__c sl0=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST123',Quantity__c=100,Carrier__c='ATT',Carrier_SKU__c='Samsung Mobile',IL_CL__c='CL');
           Sellout_Products__c sl1=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST123',Quantity__c=100,Carrier__c='SPR',Carrier_SKU__c='Samsung Mobile',IL_CL__c='CL');
           slp.add(sl0);
           slp.add(sl1);
          insert slp;
          
          List<SECA_Sellout_Data__c> secalst=new List<SECA_Sellout_Data__c>();
          
          SECA_Sellout_Data__c seca1=new SECA_Sellout_Data__c(Version__c='T04',Type__c='Plan',SAP_Company_Code__c='C330',Rollout_Month__c='201903',Quantity__c=20,Opportunity_No__c='TEST-OPP-1',Material_Code__c='SECA-0001',Loading_Month__c='201904',Index_Code__c='D',Data_Type__c='Sellout Account Data', Reference_Number__c='20190418093359');
          SECA_Sellout_Data__c seca2=new SECA_Sellout_Data__c(Version__c='000',Type__c='Actual',SAP_Company_Code__c='C330',Rollout_Month__c='201903',Quantity__c=20,Opportunity_No__c='TEST-OPP-1',Material_Code__c='SECA-0001',Loading_Month__c='201904',Index_Code__c='D',Data_Type__c='Sellout Account Data',Reference_Number__c='20190418093359');
          SECA_Sellout_Data__c seca3=new SECA_Sellout_Data__c(Version__c='T04',Type__c='Plan',SAP_Company_Code__c='C330',Rollout_Month__c='201903',Quantity__c=20,Opportunity_No__c='TEST-OPP-1',Material_Code__c='SECA-0001',Loading_Month__c='201904',Index_Code__c='D',Data_Type__c='Sellout QTY Data',Reference_Number__c='20190418093359');
          SECA_Sellout_Data__c seca4=new SECA_Sellout_Data__c(Version__c='000',Type__c='Actual',SAP_Company_Code__c='C330',Rollout_Month__c='201903',Quantity__c=20,Opportunity_No__c='TEST-OPP-1',Material_Code__c='SECA-0001',Loading_Month__c='201904',Index_Code__c='D',Data_Type__c='Sellout QTY Data',Reference_Number__c='20190418093359');
            secalst.add(seca1);
            secalst.add(seca2);
            secalst.add(seca3);
            secalst.add(seca4);
            insert secalst;
           message_queue__c mq = new message_queue__c(MSGUID__c = '', 
                                                    MSGSTATUS__c = '',  
                                                    Status__c = 'Not Started', 
                                                    retry_counter__c = 0,
                                                    Integration_Flow_Type__c='Flow-031',
                                                    Identification_Text__c='20190418093359',
                                                    IFDate__c='',
                                                    IFID__c='',
                                                    ERRORTEXT__c='',
                                                    Object_Name__c='Sellout- SECA'
                                                    );
                                                    
        insert mq;
        List<Carrier_Sold_to_Mapping__c> crr=new list<Carrier_Sold_to_Mapping__c>();
          Carrier_Sold_to_Mapping__c cr1=new Carrier_Sold_to_Mapping__c(Name='AT&T',Carrier_Code__c='ATT',Sold_To_Code__c='00002222',Sold_To_Code_Pre_Check__c='00003333,00004444');
          Carrier_Sold_to_Mapping__c cr2=new Carrier_Sold_to_Mapping__c(Name='Sprint',Carrier_Code__c='SPR',Sold_To_Code__c='000022229');
          crr.add(cr1);
          crr.add(cr2);
       
        insert crr;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockResponseGenerator('"S"'));
        SelloutAccountIntegration.SendSelloutAccountData(mq);  
        Test.Stoptest();
        
        
          
	}
		private static testMethod void testmethod4() {//This Method to cover Integration for Sending Sellout QTY
          Sellout__C so=new Sellout__C(Closing_Month_Year1__c='10/2018',Description__c='From Test Class',Subsidiary__c='SEA',Approval_Status__c='Draft',Integration_Status__c='Draft');
         insert so;
         Date d=System.today();
            RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];
          Product2 prod1 = new Product2(
                name ='SM-TEST123',
                SAP_Material_ID__c = 'SM-TEST123',
                RecordTypeId = productRT.id
            );
           insert prod1;
          Sellout_Products__c sl0=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST123',Quantity__c=100,Carrier__c='ATT',Carrier_SKU__c='Samsung Mobile',IL_CL__c='CL');
          insert sl0;
           message_queue__c mq = new message_queue__c(MSGUID__c = '', 
                                                    MSGSTATUS__c = '',  
                                                    Status__c = 'Not Started', 
                                                    retry_counter__c = 0,
                                                    Integration_Flow_Type__c='Flow-030',
                                                    Identification_Text__c='20190418093359',
                                                    IFDate__c='',
                                                    IFID__c='',
                                                    ERRORTEXT__c='',
                                                    Object_Name__c='Sellout - SECA'
                                                    );
                                                    
        insert mq;
        List<SECA_Sellout_Data__c> secalst=new List<SECA_Sellout_Data__c>();
          
          SECA_Sellout_Data__c seca1=new SECA_Sellout_Data__c(Version__c='T04',Type__c='Plan',SAP_Company_Code__c='C330',Rollout_Month__c='201903',Quantity__c=20,Opportunity_No__c='TEST-OPP-1',Material_Code__c='SECA-0001',Loading_Month__c='201904',Index_Code__c='D',Data_Type__c='Sellout Account Data',Reference_Number__c='20190418093359');
          SECA_Sellout_Data__c seca2=new SECA_Sellout_Data__c(Version__c='000',Type__c='Actual',SAP_Company_Code__c='C330',Rollout_Month__c='201903',Quantity__c=20,Opportunity_No__c='TEST-OPP-1',Material_Code__c='SECA-0001',Loading_Month__c='201904',Index_Code__c='D',Data_Type__c='Sellout Account Data',Reference_Number__c='20190418093359');
          SECA_Sellout_Data__c seca3=new SECA_Sellout_Data__c(Version__c='T04',Type__c='Plan',SAP_Company_Code__c='C330',Rollout_Month__c='201903',Quantity__c=20,Opportunity_No__c='TEST-OPP-1',Material_Code__c='SECA-0001',Loading_Month__c='201904',Index_Code__c='D',Data_Type__c='Sellout QTY Data',Reference_Number__c='20190418093359');
          SECA_Sellout_Data__c seca4=new SECA_Sellout_Data__c(Version__c='000',Type__c='Actual',SAP_Company_Code__c='C330',Rollout_Month__c='201903',Quantity__c=20,Opportunity_No__c='TEST-OPP-1',Material_Code__c='SECA-0001',Loading_Month__c='201904',Index_Code__c='D',Data_Type__c='Sellout QTY Data',Reference_Number__c='20190418093359');
            secalst.add(seca1);
            secalst.add(seca2);
            secalst.add(seca3);
            secalst.add(seca4);
            insert secalst;
        Carrier_Sold_to_Mapping__c cr=new Carrier_Sold_to_Mapping__c(Name='AT&T',Carrier_Code__c='ATT',Sold_To_Code__c='00002222',Sold_To_Code_Pre_Check__c='00003333,00004444');
        insert cr;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockResponseGenerator('"S"'));
        SelloutQuantityIntegration.SendSelloutQTYData(mq);  
        Test.Stoptest();
        
        
          
	}
	
	
	
	 class MockResponseGenerator implements HttpCalloutMock {
    String responseStatus;
    MockResponseGenerator(String responseStatus) {
        this.responseStatus = responseStatus;
    }
    // Implement this interface method
    public HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        System.assertEquals('callout:X012_013_ROIasdf', req.getEndpoint());
        System.assertEquals('POST', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{' +
                        '"inputHeaders": {'+
                            '"MSGGUID": "a6bba321-2183-5fb4-ff75-4d024e39a98e",' +
                            '"IFID": "TBD",' +
                            '"IFDate": "160317",' +
                            '"MSGSTATUS": "S",' +
                            '"ERRORTEXT": null,' +
                            '"ERRORCODE": null' +
                        '}'+
                     '}');
        res.setStatusCode(200);
        return res;
    }
	 }
}