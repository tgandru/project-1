public class SelloutNewnEditController {
public string val {get; set;}
public Sellout__c so{get; set;}
    public SelloutNewnEditController(ApexPages.StandardController controller) {
        so=new Sellout__c();
        if(Apexpages.currentPage().getParameters().get('RecordType') !=null){
            so.RecordTypeId=Apexpages.currentPage().getParameters().get('RecordType');
          }
        
        
      if(controller.getRecord().Id !=null){
          so=[select RecordTypeId,id,name,Approval_Status__c,Closing_Month_Year1__c,Description__c,Has_Attachement__c,OwnerId ,Integration_Status__c ,Total_CL_Qty__c,Total_IL_Qty__c,Subsidiary__c from Sellout__c where id=:controller.getRecord().Id limit 1];
          if(Apexpages.currentPage().getParameters().get('RecordType') !=null){
              so.RecordTypeId=Apexpages.currentPage().getParameters().get('RecordType');
        }
         try{
         String s=so.Closing_Month_Year1__c.right(4)+'-'+so.Closing_Month_Year1__c.left(2);
          system.debug('val1::'+s);
          val=s;
         }catch(exception ex){
         
         }
          
       }
    }
    Public pagereference saverec(){
   system.debug('val::'+val);
   string s= val.right(2)+'/'+val.left(4);
    so.Closing_Month_Year1__c=s;
  try{
   upsert so;
   return new pageReference('/'+so.id);
      
  }catch(Exception ex){
        ApexPages.addMessages (ex); 
        return null;
  }
  }  

}