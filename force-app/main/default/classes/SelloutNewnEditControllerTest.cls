@isTest
private class SelloutNewnEditControllerTest {

	private static testMethod void testmethod1() {
         Test.startTest();
          Sellout__C so=new Sellout__C(Closing_Month_Year1__c='10/2018',Description__c='From Test Class',Subsidiary__c='SEA',Approval_Status__c='Draft',Integration_Status__c='Draft',Has_Attachement__c=true,Request_Approval__c=true);
         insert so;
       
         Id pln = TestDataUtility.retrieveRecordTypeId('Plan', 'Sellout__C');
         Pagereference pf= Page.SelloutNewnEdit;
           pf.getParameters().put('id',so.id);
             pf.getParameters().put('RecordType',pln);
           test.setCurrentPage(pf);
        
        apexPages.standardcontroller sc = new apexPages.standardcontroller(so);
          SelloutNewnEditController cls=new SelloutNewnEditController(sc);
          cls.saverec();
         Test.StopTest();    
	}
	
		private static testMethod void testmethod2() {
         Test.startTest();
          Sellout__C so=new Sellout__C(Closing_Month_Year1__c='10/2018',Description__c='From Test Class',Subsidiary__c='SEA',Approval_Status__c='Draft',Integration_Status__c='Draft',Has_Attachement__c=true,Request_Approval__c=true);
         insert so;
       
         
         Pagereference pf= Page.SelloutNewnEdit;
           pf.getParameters().put('id',so.id);
          
           test.setCurrentPage(pf);
        
        apexPages.standardcontroller sc = new apexPages.standardcontroller(so);
          SelloutNewnEditController cls=new SelloutNewnEditController(sc);
          cls.Val='TEST12';
          cls.saverec();
         Test.StopTest();    
	}

}