global  class SelloutPrecheckingDatas implements Database.Batchable<sObject>,Schedulable,database.stateful{
 global string soids;
     global SelloutPrecheckingDatas(string soids){
         system.debug('**********'+soids);
         this.soids=soids;
     }
     
      global void execute(SchedulableContext SC) {
         database.executeBatch(new SelloutPrecheckingDatas(soids)) ;
     }
       global Database.QueryLocator start(Database.BatchableContext BC){
           
           return Database.getQueryLocator([select id,name,Carrier__c,Quantity__c,Samsung_SKU__c,Sellout_Date__c,Status__c,sellout__r.RecordType.Name,sellout__r.Subsidiary_Code__c,sellout__r.Version__c,Pre_Check_Result__c from Sellout_Products__c where sellout__c =:soids order by Sellout_Date__c asc]);
       }
       
        global void execute(Database.BatchableContext BC, List<Sellout_Products__c> scope){
            Set<String> ExtIDset= new set<String>();
             Map<String,String> codemap=new map<string,string>();
                  List<Carrier_Sold_to_Mapping__c> crr=Carrier_Sold_to_Mapping__c.getall().values();
                  for(Carrier_Sold_to_Mapping__c c:crr){
                    codemap.put(c.Carrier_Code__c,c.Sold_To_Code_Pre_Check__c);  
                  }
                  Set<String> versions = new Set<String>();
                  Set<String> subsCodes = new Set<String>();
                  Set<String> selloutMonths = new Set<String>();
                  Set<String> selloutModels = new Set<String>();
                  
                  Map<String,String> VersionMap=new Map<String,String>();
                      VersionMap.put('01','T01');
                      VersionMap.put('02','T02');
                      VersionMap.put('03','T03');
                      VersionMap.put('04','T04');
                      VersionMap.put('05','T05');
                      VersionMap.put('06','T06');
                      VersionMap.put('07','T07');
                      VersionMap.put('08','T08');
                      VersionMap.put('09','T09');
                      VersionMap.put('10','T0A');
                      VersionMap.put('11','T0B');
                      VersionMap.put('12','T0C');
                      
                      Integer yr=0;
                      String Vers='';
                  
                  for(Sellout_Products__c s:scope){
                           subsCodes.add(s.sellout__r.Subsidiary_Code__c);
                           //versions.add(s.sellout__r.Version__c);
                           selloutModels.add(s.Samsung_SKU__c);
                           if(s.sellout__r.RecordType.Name=='Plan'){
                               
                               date d=s.Sellout_Date__c;
                               String sMonth=String.valueof(d.month());
                                     if(sMonth.length()==1){
                                          sMonth = '0' + sMonth;
                                        }
                                        
                                        if(d.year()>yr){
                                              yr=d.year();
                                              vers=VersionMap.get(sMonth);
                                              versions.add(vers);
                                            }
                                       String sodt=String.valueOf(d.year())+sMonth;
                                      selloutMonths.add(sodt);  
                              /* for(Date d = dt.addMonths(-1) ; d <= dt ; d = d.addMonths(1)){
                                      
                               } */
                               
                              
                           }else{
                               versions.add('000');
                                date dt=s.Sellout_Date__c;
                               for(Date d = dt.addMonths(-11) ; d <= dt ; d = d.addMonths(1)){
                                    String sMonth=String.valueof(d.month());
                                     if(sMonth.length()==1){
                                          sMonth = '0' + sMonth;
                                        }
                                     String sodt=String.valueOf(d.year())+sMonth;
                                      selloutMonths.add(sodt);   
                               }
                               
                           }
                  }
                  
                  for(Sellout_Pre_Check_Data__c pre:[select id,External_ID__c from Sellout_Pre_Check_Data__c where Version__c in:versions and Samsung_SKU__c in:selloutModels ]){
                      ExtIDset.add(pre.External_ID__c);
                  }
                  
                  for(Sellout_Products__c s:scope){
                      if(s.sellout__r.RecordType.Name=='Plan'){
                          String res = 'Fail';// Default Value is Fail
                          for(String v : versions){
                              for(String sm:selloutMonths){
                                 for(String sc:subsCodes){
                                     String key=sm+'-'+v+'-'+sc+'-'+s.Samsung_SKU__c;
                                    if(ExtIDset.contains(key)){
                                            res = 'OK';
	                                         break;
                                     }
                                     
                                 } 
                                if(res == 'OK') break;
                              }
                               if(res == 'OK') break;
                               
                          }
                          s.Pre_Check_Result__c = res; 
                      }else{
                            String res = 'Fail';// Default Value is Fail
                          for(String v : versions){
                              for(String sm:selloutMonths){
                                 for(String sc:subsCodes){
                                     //
                                     List<string> soldtos=codemap.get(s.Carrier__c).split(',');
                                       for(String sol: soldtos){
                                           string soldto=sol.trim();
                                            String key=sm+'-'+v+'-'+sc+'-'+s.Samsung_SKU__c+'-'+soldto;
                                                if(ExtIDset.contains(key)){
                                                        res = 'OK';
            	                                         break;
                                                 }
                                       }
                                    if(res == 'OK') break;
                                    // 
                                 } 
                                if(res == 'OK') break;
                              }
                               if(res == 'OK') break;
                               
                          }
                          s.Pre_Check_Result__c = res; 
                      }
                  }
                  
         
             update scope; 
              
        }
        global void finish(Database.BatchableContext BC){
            Try{
                FeedItem post = new FeedItem();
                post.ParentId = soids;
                post.Body = 'Pre-check is Done. Please Refresh the Screen to see Pre-check Result.';
                insert post;
            }catch(exception ex){
                
            }
            
            
        }
  
}