@isTest
private class SelloutPrecheckingDatasTest {

	private static testMethod void testmethod1() {
         Test.startTest();
          Id pln = TestDataUtility.retrieveRecordTypeId('Plan', 'Sellout__C');
          Sellout__C so=new Sellout__C(RecordTypeid=pln,Closing_Month_Year1__c='09/2018',Description__c='From Test Class',Subsidiary__c='SEA',Approval_Status__c='Draft',Integration_Status__c='Draft',Has_Attachement__c=true,Request_Approval__c=true);
         insert so;
         Date d=System.today();
         RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];
          Product2 prod1 = new Product2(
                name ='SM-TEST123',
                SAP_Material_ID__c = 'SM-TEST123',
                RecordTypeId = productRT.id
            );
             insert prod1;
          List<Sellout_Products__c> slp=new list<Sellout_Products__c>();
          Sellout_Products__c sl0=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST123',Quantity__c=100,Carrier__c='ATT',Carrier_SKU__c='Samsung Mobile',IL_CL__c='CL');
          Sellout_Products__c sl1=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST123',Quantity__c=100,Carrier__c='ATT',Carrier_SKU__c='Samsung Mobile',IL_CL__c='IL');
          slp.add(sl0);
          slp.add(sl1);
          insert slp;
          Carrier_Sold_to_Mapping__c crr=new Carrier_Sold_to_Mapping__c(Name='ATT',Carrier_Code__c='ATT',Sold_To_Code_Pre_Check__c='0001773705, 0001773706');
          insert crr;
          String sMonth=String.valueof(d.month());
                                     if(sMonth.length()==1){
                                          sMonth = '0' + sMonth;
                                        }
                                     String sodt=String.valueOf(d.year())+sMonth;
         Sellout_Pre_Check_Data__c spre=new Sellout_Pre_Check_Data__c(Samsung_SKU__c='SM-TEST123',SAP_Company_Code__c='C310',Sold_To_Code__c='0001773705',Version__c='T09',Sellout_Month__c=sodt);   
             
             ExecuteprecheckBatch.ExecuteprecheckBatchMethod(so.id);
        Test.StopTest();
	}


    	private static testMethod void testmethod2() {
         Test.startTest();
          Id Act = TestDataUtility.retrieveRecordTypeId('Actual', 'Sellout__C');
          Sellout__C so=new Sellout__C(RecordTypeid=act,Closing_Month_Year1__c='09/2018',Description__c='From Test Class',Subsidiary__c='SEA',Approval_Status__c='Draft',Integration_Status__c='Draft',Has_Attachement__c=true,Request_Approval__c=true);
         insert so;
         Date d=System.today();
         RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];
          Product2 prod1 = new Product2(
                name ='SM-TEST123',
                SAP_Material_ID__c = 'SM-TEST123',
                RecordTypeId = productRT.id
            );
             insert prod1;
          List<Sellout_Products__c> slp=new list<Sellout_Products__c>();
          Sellout_Products__c sl0=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST123',Quantity__c=100,Carrier__c='ATT',Carrier_SKU__c='Samsung Mobile',IL_CL__c='CL');
          Sellout_Products__c sl1=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST123',Quantity__c=100,Carrier__c='ATT',Carrier_SKU__c='Samsung Mobile',IL_CL__c='IL');
          slp.add(sl0);
          slp.add(sl1);
          insert slp;
          Carrier_Sold_to_Mapping__c crr=new Carrier_Sold_to_Mapping__c(Name='ATT',Carrier_Code__c='ATT',Sold_To_Code_Pre_Check__c='0001773705, 0001773706');
          insert crr;
          String sMonth=String.valueof(d.month());
                                     if(sMonth.length()==1){
                                          sMonth = '0' + sMonth;
                                        }
                                     String sodt=String.valueOf(d.year())+sMonth;
         Sellout_Pre_Check_Data__c spre=new Sellout_Pre_Check_Data__c(Samsung_SKU__c='SM-TEST123',SAP_Company_Code__c='C310',Sold_To_Code__c='0001773705',Version__c='T09',Sellout_Month__c=sodt);   
             
             ExecuteprecheckBatch.ExecuteprecheckBatchMethod(so.id);
        Test.StopTest();
	}

  
}