public class SelloutProductTriggerHandler {

   /* public static void updateInvoicePrice(List<Sellout_Products__c> prds){
        Set<string> skus= new set<string>();
        map<string,decimal> skuprice=new map<string,decimal>();
        for(Sellout_Products__c p:prds){
            skus.add(p.Samsung_SKU__c);
        }
        if(skus.size()>0){
            for(PricebookEntry pbe:[select id,product2id,pricebook2id,pricebook2.Name,product2.SAP_Material_ID__c,UnitPrice,Pricebook2.SAP_Price_Account_Code__c,pricebook2.SAP_Price_Group_Code__c from PriceBookEntry where product2.SAP_Material_ID__c in:skus and isActive=true and Pricebook2.Name !='Standard Price Book']){
                
                if(pbe.pricebook2.Name=='Mobile'){
                    String key=pbe.product2.SAP_Material_ID__c+'-'+'EPP';
                    skuprice.put(key,pbe.UnitPrice);
                     System.debug('PBE Key'+key);
                }else{
                    if(pbe.pricebook2.SAP_Price_Group_Code__c=='VZN'){
                        String key=pbe.product2.SAP_Material_ID__c+'-'+'VZW'; 
                         skuprice.put(key,pbe.UnitPrice);
                    }else{
                    String key=pbe.product2.SAP_Material_ID__c+'-'+pbe.pricebook2.SAP_Price_Group_Code__c;
                    skuprice.put(key,pbe.UnitPrice);
                        
                    }
                   
                }
            }
        }
        
        for(Sellout_Products__c p:prds){
            try{
                String key=p.Samsung_SKU__c+'-'+p.Carrier__c;
                 System.debug('SP Key'+key);
                p.Invoice_Price__c=skuprice.get(key);
            }catch(exception ex){
                system.debug('Exception ::'+ex);
            }
        }
        
    } */
    
    Public Static Void identifyDuplicaterecords(List<Sellout_Products__c> prds){
        Set<string> key=new set<String>();
        
        for(Sellout_Products__c p:prds){
            String sMonth = String.valueof(p.Sellout_Date__c.month());
            if(sMonth.length()==1){
                  sMonth = '0' + sMonth;
                }
            string s=p.Sellout__c+'-'+p.Carrier__c+'-'+p.Samsung_SKU__c+'-'+p.Sellout_Date__c.year()+sMonth;
            
            key.add(s);
        }
        Map<string,List<Sellout_Products__c>> keymap= new  Map<string,List<Sellout_Products__c>>();
        for(Sellout_Products__c p:[select id,name,Sellout__r.RecordType.Name,ExternalId__c from Sellout_Products__c where ExternalId__c in:key]){
            if(keymap.containsKey(p.ExternalId__c)){
                                List<Sellout_Products__c> ol = keymap.get(p.ExternalId__c);
                                        ol.add(p);
                                      keymap.put(p.ExternalId__c,ol);
                            }else{
                               keymap.put(p.ExternalId__c, new List<Sellout_Products__c> { p }); 
                            }
        }
          Set<string> keyset=new set<String>();
         for(Sellout_Products__c p:prds){
             String sMonth = String.valueof(p.Sellout_Date__c.month());
            if(sMonth.length()==1){
                  sMonth = '0' + sMonth;
                }
            string s=p.Sellout__c+'-'+p.Carrier__c+'-'+p.Samsung_SKU__c+'-'+p.Sellout_Date__c.year()+sMonth;
            System.debug('******Key*******::'+s);
            if(keyset.contains(s)){
                 p.Dup_Record_Found__c=true;
            }else{
            if(keymap.containsKey(s)){
                List<Sellout_Products__c> ol=keymap.get(s);
                if(ol.size()>0){
                    System.debug('******Duplicate Record Found*******');
                    p.Dup_Record_Found__c=true;
                }
                try{
                    if(ol.size()==1 && p.id==ol[0].id){
                         p.Dup_Record_Found__c=false;
                    }
                }catch(exception ex){
                    
                }
               
            }else{
                 p.Dup_Record_Found__c=false;
            }
         }
         keyset.add(s);
         }
        
    }
    
    
    
     Public static void checkskuisavibility(list<Sellout_Products__c> skus){
       Set<string> skuid= new set<string>();
        map<string,string> pdmap=new  map<string,string>();
      
        for(Sellout_Products__c s:skus){
            
           skuid.add(s.Samsung_SKU__c); 
        }
        if(skuid.size()>0){
            for(Product2 p:[select id,SAP_Material_ID__c from Product2 where SAP_Material_ID__c  in :skuid]){
                pdmap.put(p.SAP_Material_ID__c,p.id);
            }
        }
        
        for(Sellout_Products__c s:skus){
            
          String pid =pdmap.get(s.Samsung_SKU__c);
            system.debug('====='+pid);
            if(pid==null || pid==''){
                
                s.Adderror(s.Samsung_SKU__c+'   does not exist in Product Master ');
            }
            
            
        }
       
   }
   
   

}