public  class SelloutQtyIntegrationHelper {
  
  public static void callSelloutQtyIntegration (message_queue__c mq){
       System.debug('## Starting Callout Method');
       //Sellout__c so=[Select id,name,Closing_Month_Year__c,Version__c,Description__c,Month__c,Subsidiary__c,Year__C from Sellout__c where id=:mq.Identification_Text__c];
      
       List<Sellout_Products__c> sols=new list<Sellout_Products__c>([Select id,name,Carrier__c,BO_Number__c,Sold_To_Code__c,Sellout__r.Owner.email,Carrier_SKU__c,IL_CL__c,Quantity__c,Samsung_SKU__c,Sellout_Date__c,Sellout__r.Subsidiary__c,Sellout__r.Version__c,Sellout__r.Closing_Month_Year1__c,Sellout__r.Description__c,Sellout__r.Name,Sellout__r.RecordType.Name from Sellout_Products__c where Status__c ='OK' and Sellout__c=:mq.Identification_Text__c order by Sellout_Date__c asc]);
       
       string partialEndpoint = Test.isRunningTest() ? 'asdf' : Integration_EndPoints__c.getInstance('Flow-030').partial_endpoint__c;
        string layoutId = Test.isRunningTest() ? 'asdf' : Integration_EndPoints__c.getInstance('Flow-030').layout_id__c;
       
        SelloutStubClass.SelloutQTYRequest reqStub = new SelloutStubClass.SelloutQTYRequest();
        SelloutStubClass.msgHeadersRequest headers = new SelloutStubClass.msgHeadersRequest(layoutId);
        SelloutStubClass.SelloutQTYRequestBody body = new SelloutStubClass.SelloutQTYRequestBody();
         mapbodyFields(body, sols);
        reqStub.body = body;
        reqStub.inputHeaders = headers;
        string requestBody = json.serialize(reqStub);
        string response = '';
        
        try{
            response = webcallout(requestBody,partialEndpoint);
            //response='{"inputHeaders":{"MSGSTATUS":"S","MSGGUID":"2cf7acec-9744-36e5-6790-5e50e85676cd","IFID":"SFDC_IF_US_031","IFDate":"20190109074957","ERRORTEXT":"","ERRORCODE":""}}';
            System.debug('response ::'+response);
        }catch(exception ex){
            system.debug('Error ::'+ex.getmessage()); 
             handleError(mq,ex.getmessage());
        }
        
         if(string.isNotBlank(response)){ 
                SelloutStubClass.UpdateQtyFrmERPResponse res = (SelloutStubClass.UpdateQtyFrmERPResponse) json.deserialize(response, SelloutStubClass.UpdateQtyFrmERPResponse.class);
                system.debug('##  Response ::'+res);
                if(res.inputHeaders.MSGSTATUS == 'S'){
                    handleSuccess(mq, res);
                    
                    //String emailaddrs=sols[0].Sellout__r.Owner.email;
                    String emailaddrs=mq.CreatedBy.Email;
                    SendRequestfileasEmail(requestBody,emailaddrs);
                 }else{
                     String er=res.inputHeaders.ERRORTEXT;
                     handleError(mq,er);
                 }
         }
      
  }
  
  public static string webcallout(string body, string partialEndpoint){ 
       System.debug('##requestBody ::'+body);
        System.Httprequest req = new System.Httprequest();
        system.debug('************************************** INSIDE CALLOUT METHOD');
        HttpResponse res = new HttpResponse();
        req.setMethod('POST');
        req.setBody(body);
        system.debug(body);
       
        req.setEndpoint('callout:X012_013_ROI'+partialEndpoint); //+namedEndpoint); todo, make naming consistent on picklist values and endpoints and use that here
        
        req.setTimeout(120000);
         req.setHeader('Content-Type', 'application/json');
        req.setHeader('Accept', 'application/json');
    
        Http http = new Http();  
        res = http.send(req);
        System.debug('******** my resonse' + res);
         System.debug('******** my request' + req);
        system.debug('*****Response body::' + res.getBody());
        System.debug('***** Response Body Size ' + res.getBody().length());
        return res.getBody();     
      
  }
   public static void mapbodyFields(SelloutStubClass.SelloutQTYRequestBody body, List<Sellout_Products__c> sols){

     // if(so.Sellout_Products__r.size()!=0){
          list<SelloutStubClass.SelloutProducts> SelloutProducts = new list<SelloutStubClass.SelloutProducts>();
            mapProductFields(SelloutProducts, sols);
            body.lines = SelloutProducts;
      //}
   }
   
   public static void mapProductFields(list<SelloutStubClass.SelloutProducts> SelloutProducts,  List<Sellout_Products__c> sols){ 
       Map<String,String> codemap=new map<string,string>();
      List<Carrier_Sold_to_Mapping__c> crr=Carrier_Sold_to_Mapping__c.getall().values();
      for(Carrier_Sold_to_Mapping__c c:crr){
        codemap.put(c.Carrier_Code__c,c.Sold_To_Code__c);  
      }
      Map<string,Sellout_Products__c> ilMap=new Map<string,Sellout_Products__c>();
      Map<string,Sellout_Products__c> clMap=new Map<string,Sellout_Products__c>();
      Map<String,String> VersionMap=new Map<String,String>();
      VersionMap.put('01','T01');
      VersionMap.put('02','T02');
      VersionMap.put('03','T03');
      VersionMap.put('04','T04');
      VersionMap.put('05','T05');
      VersionMap.put('06','T06');
      VersionMap.put('07','T07');
      VersionMap.put('08','T08');
      VersionMap.put('09','T09');
      VersionMap.put('10','T0A');
      VersionMap.put('11','T0B');
      VersionMap.put('12','T0C');
      
      //   
         for(Sellout_Products__c sp :sols){
             if(sp.IL_CL__c=='IL'){
                 String key=String.valueOf(sp.Carrier__c)+'-'+sp.Samsung_SKU__c+'-'+String.valueOf(sp.Sellout_Date__c);
                 ilMap.put(key,sp);
             }else{
                 String key=String.valueOf(sp.Carrier__c)+'-'+sp.Samsung_SKU__c+'-'+String.valueOf(sp.Sellout_Date__c);
                 clMap.put(key,sp);
             }
         }
      
      //
      Integer yr=0;
      String Vers='';
      for(Sellout_Products__c sp :sols){
         
          SelloutStubClass.SelloutProducts sol = new SelloutStubClass.SelloutProducts();
               sol.modelCode=sp.Samsung_SKU__c;
               sol.opportunityNo=sp.BO_Number__c;
               sol.accountNo=codemap.get(sp.Carrier__c);
                Date d=sp.Sellout_Date__c;
                String sMonth = String.valueof(d.month());
                String sDay = String.valueof(d.day());
                if(sMonth.length()==1){
                  sMonth = '0' + sMonth;
                }
                if(sDay.length()==1){
                  sDay = '0' + sDay;
                }
                string closemnth=sp.Sellout__r.Closing_Month_Year1__c;
                sol.rolloutMonth=string.valueOf(d.year())+sMonth;
               
                if(sp.Sellout__r.Subsidiary__c=='SEA'){
                      sol.countryCode='US';
                     }else if(sp.Sellout__r.Subsidiary__c=='SECA'){
                        sol.countryCode='CA'; 
                     }
               
                 if(sp.Sellout__r.RecordType.Name=='Plan'){
                    sol.applyMonth=string.valueOf(d.year())+sMonth;
                    
                       //
                        if(d.year()>yr){
                          yr=d.year();
                          vers=VersionMap.get(sMonth);
                        }
                         sol.version=vers;
                     //   
                    
                 }else{
                    sol.applyMonth=string.valueOf(closemnth.right(4)+closemnth.left(2)); 
                     sol.version='000';
                 }
               //  sol.indexCode ='';
                // sol.quantity=String.valueOf(sp.Quantity__c); 
                 String key=sp.Carrier__c+'-'+sp.Samsung_SKU__c+'-'+String.valueOf(sp.Sellout_Date__c);
                if(sp.IL_CL__c=='IL'){
                     sol.indexCode ='';
                      if(clMap.containsKey(key)){
                          Sellout_Products__c clsp=clMap.get(key);
                          decimal qty=sp.Quantity__c+clsp.Quantity__c;
                          sol.quantity=String.valueOf(qty);
                      }else{
                          sol.quantity=String.valueOf(sp.Quantity__c);  
                      }
                 }else{
                      sol.indexCode ='D';
                      sol.quantity=String.valueOf(sp.Quantity__c);  
                      
                   
                 }
                 
                   SelloutProducts.add(sol); 
                  if(sp.IL_CL__c=='CL'){ 
                      if(!ilMap.containsKey(key)){
                              SelloutStubClass.SelloutProducts sol2 = sol.clone();
                                sol2.indexCode ='';
                               SelloutProducts.add(sol2);
                          } 
                  }
                
             
               
          /*   if(sp.IL_CL__c=='CL'){
                   
                    SelloutStubClass.SelloutProducts sol1 = new SelloutStubClass.SelloutProducts();
                       sol1.modelCode=sp.Samsung_SKU__c;
                       sol1.opportunityNo=sp.Sellout__r.Name;
                       sol1.accountNo=codemap.get(sp.Carrier__c);
                        
                       sol1.version=sp.Sellout__r.Version__c;
                       sol1.rolloutMonth=string.valueOf(d.year())+sMonth;
                       //sol1.applyMonth=string.valueOf(closemnth.right(4)+closemnth.left(2));
                        if(sp.Sellout__r.Subsidiary__c=='SEA'){
                              sol1.countryCode='US';
                             }else if(sp.Sellout__r.Subsidiary__c=='SECA'){
                                sol1.countryCode='CA'; 
                             }
                        if(sp.Sellout__r.RecordType.Name=='Plan'){
                            sol1.applyMonth=string.valueOf(d.year())+sMonth;
                            //
                            if(d.year()>yr){
                                  yr=d.year();
                                  vers=VersionMap.get(sMonth);
                                }
                            sol1.version=vers;
                            //
                         }else{
                            sol1.applyMonth=string.valueOf(closemnth.right(4)+closemnth.left(2)); 
                              sol1.version='000';
                         }     
                        sol1.indexCode ='D';
                        String key=String.valueOf(sp.Carrier__c)+'-'+sp.Samsung_SKU__c+'-'+String.valueOf(sp.Sellout_Date__c);
                          if(ilMap.containsKey(key)){
                              Sellout_Products__c ilsp=ilMap.get(key);
                              decimal qty=sp.Quantity__c+ilsp.Quantity__c;
                              sol1.quantity=String.valueOf(qty);
                          }else{
                              sol1.quantity=String.valueOf(sp.Quantity__c);  
                          }
                        
                        
                        
                        
                        
                       SelloutProducts.add(sol1);
                   
               } */
               
       
        
      }
      
     
   }
   
    public static void handleSuccess(message_queue__c mq, SelloutStubClass.UpdateQtyFrmERPResponse res){
            mq.status__c = 'success';
            mq.MSGSTATUS__c = res.inputHeaders.MSGSTATUS;
            mq.msguid__c = res.inputHeaders.msgGUID;
            mq.IFID__c = res.inputHeaders.ifID;
            mq.IFDate__c = res.inputHeaders.ifDate;
            upsert mq; 
            
            Sellout__c so=[select id,name,Integration_Status__c from Sellout__c where id=: mq.Identification_Text__c];
            so.Integration_Status__c='Success';
            update so;
            
            
    }
    
    
    
    public static void handleError(message_queue__c mq,string err){
            mq.retry_counter__c += 1;
            mq.status__c ='failed';
            mq.ERRORTEXT__c =err;
            upsert mq; 
            
            
            Sellout__c so=[select id,name,Integration_Status__c from Sellout__c where id=: mq.Identification_Text__c];
            so.Integration_Status__c='Error';
            update so;
    }
    
    public Static void SendRequestfileasEmail (String jsondata,string toaddress){
       
       Map<String, Object> meta = (Map<String, Object>) JSON.deserializeUntyped(jsondata);
       Map<String, Object>  carMap = (Map<String, Object>) meta.get('body');
        list<Object> lines=(list<Object>)carMap.get('lines'); 
        
        List<Map<String, Object>> mapList = new List<Map<String, Object>>();
        Set<String> keySet = new Set<String>();
        
        for (Object entry : lines) {
            Map<String, Object> m = (Map<String, Object>)(entry);
            keySet.addAll(m.keySet());
            mapList.add(m);
        }
        
        List<String> keys = new List<String>(keySet);
        keys.sort();
        
        List<List<String>> csvLines = new List<List<String>>();
        
        for (Integer i = 0; i <= mapList.size(); i++) {
            csvLines.add(new List<String>());
        }
        
        for (String key : keys) {
            csvLines.get(0).add('"' + key + '"');
            
            for (Integer i = 1; i <= mapList.size(); i++) {
                csvLines.get(i).add('"' + (String)(mapList.get(i - 1).get(key)) + '"');
            }
        }
        
        String csvFile = '';
        for (List<String> csvLine : csvLines) {
            String line = '';
            for (Integer i = 0; i < csvLine.size() - 1; i++) {
            	line += csvLine.get(i) + ',';
            }
            line += csvLine.get(csvLine.size() - 1);
            csvFile += line + '\n';
        }
        
          if(csvFile !='' && csvFile !=null){
              Blob csv = Blob.valueof(csvFile);
              Messaging.EmailFileAttachment attch = new Messaging.EmailFileAttachment();
              attch.setFileName('Sellout_QTY_Data.CSV');
              attch.setBody(csv); 
            
             Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
               email.setSubject( 'Sellout Quantity Data Sent to GERP' );
               string body ='Hello,'+'</br></br>'+'Please Check The Attached file that we sent to GERP Sucessfully. '+'</br></br>'+'Thanks,SFDC Admin Team';
               email.setHTMLBody(body );
               list<string> emailaddresses = new list<string>{toaddress};
               email.setToAddresses( emailaddresses );
                String supportEmailCCAddress = Label.CC_Email_address_for_SAM_Notification;
               List<String> supportEmailCCAddressList = new List<String>();

            if(String.isNotBlank(supportEmailCCAddress))
                supportEmailCCAddressList = supportEmailCCAddress.split(',');

            if (supportEmailCCAddressList.isEmpty() == false) {
                email.setCcAddresses(supportEmailCCAddressList);
            }
               email.setFileAttachments(new Messaging.EmailFileAttachment[] {attch});
              if(!Test.isRunningTest()){
                  Messaging.SendEmailResult [] result = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
              }
          }
            
        
        
    }
   
   
}