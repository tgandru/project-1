public class SelloutQuantityIntegration {
   Public static String soid;
   Public static Void SendSelloutQTYData(message_queue__c mq){
        System.debug('## Starting Callout Method');
        string partialEndpoint = Test.isRunningTest() ? 'asdf' : Integration_EndPoints__c.getInstance('Flow-030').partial_endpoint__c;
             string layoutId = Test.isRunningTest() ? 'asdf' : Integration_EndPoints__c.getInstance('Flow-030').layout_id__c;
        if(mq.Object_Name__c=='Sellout'){
            //  Callout Intiated from SEA Data
            List<Sellout_Products__c> sols=new list<Sellout_Products__c>([Select id,name,Carrier__c,BO_Number__c,Sold_To_Code__c,Sellout__r.Upload_Month__c,Sellout__r.Owner.email,Carrier_SKU__c,IL_CL__c,Quantity__c,Samsung_SKU__c,Sellout_Date__c,Sellout__r.Subsidiary__c,Sellout__r.Version__c,Sellout__r.Closing_Month_Year1__c,Sellout__r.Description__c,Sellout__r.Name,Sellout__r.RecordType.Name from Sellout_Products__c where Status__c ='OK' and Sellout__c=:mq.Identification_Text__c order by Sellout_Date__c asc]);
             
             SelloutStubClass.SelloutQTYRequest reqStub = new SelloutStubClass.SelloutQTYRequest();
                SelloutStubClass.msgHeadersRequest headers = new SelloutStubClass.msgHeadersRequest(layoutId);
                SelloutStubClass.SelloutQTYRequestBody body = new SelloutStubClass.SelloutQTYRequestBody();
                 mapbodyFields(body, sols);
                reqStub.body = body;
                reqStub.inputHeaders = headers;
                string requestBody = json.serialize(reqStub);
             System.debug('requestBody::'+requestBody);
                string response = '';
                try{
                      response = webcallout(requestBody,partialEndpoint);
                     //   response='{"inputHeaders":{"MSGSTATUS":"S","MSGGUID":"2cf7acec-9744-36e5-6790-5e50e85676cd","IFID":"SFDC_IF_US_031","IFDate":"20190109074957","ERRORTEXT":"","ERRORCODE":""}}';
                        System.debug('response ::'+response);
                    }catch(exception ex){
                        system.debug('Error ::'+ex.getmessage()); 
                         handleError(mq,ex.getmessage(),mq.Identification_Text__c);
                    }
        
         if(string.isNotBlank(response)){ 
                SelloutStubClass.UpdateQtyFrmERPResponse res = (SelloutStubClass.UpdateQtyFrmERPResponse) json.deserialize(response, SelloutStubClass.UpdateQtyFrmERPResponse.class);
                system.debug('##  Response ::'+res);
                if(res.inputHeaders.MSGSTATUS == 'S'){
                    handleSuccess(mq, res,mq.Identification_Text__c);
                    
                    //String emailaddrs=sols[0].Sellout__r.Owner.email;
                    String emailaddrs=mq.CreatedBy.Email;
                    SendRequestfileasEmail(requestBody);
                 }else{
                     String er=res.inputHeaders.ERRORTEXT;
                     handleError(mq,er,mq.Identification_Text__c);
                 }
         }
            
               
        }else{
            // Callout Intiated from SECA Data
          List<SECA_Sellout_Data__c>  sols=new List<SECA_Sellout_Data__c>([Select id,Name,Account_No__c,Apply_Month__c,Country_Code__c,Loading_Month__c,Index_Code__c,Material_Code__c,Opportunity_No__c,Quantity__c,Rollout_Month__c,SAP_Company_Code__c,Version__c,Type__c from SECA_Sellout_Data__c where Data_Type__c='Sellout QTY Data' and Reference_Number__c =:mq.Identification_Text__c]);
             SelloutStubClass.SelloutQTYRequest reqStub = new SelloutStubClass.SelloutQTYRequest();
                SelloutStubClass.msgHeadersRequest headers = new SelloutStubClass.msgHeadersRequest(layoutId);
                SelloutStubClass.SelloutQTYRequestBody body = new SelloutStubClass.SelloutQTYRequestBody();
                mapbodyFields_SECA(body, sols);
                reqStub.body = body;
                reqStub.inputHeaders = headers;
                string requestBody = json.serialize(reqStub);
                string response = '';
                         try{
                      response = webcallout(requestBody,partialEndpoint);
                     //   response='{"inputHeaders":{"MSGSTATUS":"S","MSGGUID":"2cf7acec-9744-36e5-6790-5e50e85676cd","IFID":"SFDC_IF_US_031","IFDate":"20190109074957","ERRORTEXT":"","ERRORCODE":""}}';
                        System.debug('response ::'+response);
                    }catch(exception ex){
                        system.debug('Error ::'+ex.getmessage()); 
                         handleError(mq,ex.getmessage(),soid);
                    }
        
         if(string.isNotBlank(response)){ 
                SelloutStubClass.UpdateQtyFrmERPResponse res = (SelloutStubClass.UpdateQtyFrmERPResponse) json.deserialize(response, SelloutStubClass.UpdateQtyFrmERPResponse.class);
                system.debug('##  Response ::'+res);
                if(res.inputHeaders.MSGSTATUS == 'S'){
                    handleSuccess(mq, res,soid);
                    
                    //String emailaddrs=sols[0].Sellout__r.Owner.email;
                    String emailaddrs=mq.CreatedBy.Email;
                    SendRequestfileasEmail(requestBody);
                 }else{
                     String er=res.inputHeaders.ERRORTEXT;
                     handleError(mq,er,soid);
                 }
         }
            
            
            
            
        }
   }
   
   
   public static void mapbodyFields(SelloutStubClass.SelloutQTYRequestBody body, List<Sellout_Products__c> sols){

          list<SelloutStubClass.SelloutProducts> SelloutProducts = new list<SelloutStubClass.SelloutProducts>();
            mapProductFields(SelloutProducts, sols);
            body.lines = SelloutProducts;
    
   }
   
   public static void mapProductFields(list<SelloutStubClass.SelloutProducts> SelloutProducts,  List<Sellout_Products__c> sols){ 
       Map<String,String> codemap=new map<string,string>();
      List<Carrier_Sold_to_Mapping__c> crr=Carrier_Sold_to_Mapping__c.getall().values();
      for(Carrier_Sold_to_Mapping__c c:crr){
        codemap.put(c.Carrier_Code__c,c.Sold_To_Code__c);  
      }
      Map<string,Sellout_Products__c> ilMap=new Map<string,Sellout_Products__c>();
      Map<string,Sellout_Products__c> clMap=new Map<string,Sellout_Products__c>();
      Map<String,String> VersionMap=new Map<String,String>();
      VersionMap.put('01','T01');
      VersionMap.put('02','T02');
      VersionMap.put('03','T03');
      VersionMap.put('04','T04');
      VersionMap.put('05','T05');
      VersionMap.put('06','T06');
      VersionMap.put('07','T07');
      VersionMap.put('08','T08');
      VersionMap.put('09','T09');
      VersionMap.put('10','T0A');
      VersionMap.put('11','T0B');
      VersionMap.put('12','T0C');
    
         for(Sellout_Products__c sp :sols){
             if(sp.IL_CL__c=='IL'){
                 String key=String.valueOf(sp.Carrier__c)+'-'+sp.Samsung_SKU__c+'-'+String.valueOf(sp.Sellout_Date__c);
                 ilMap.put(key,sp);
             }else{
                 String key=String.valueOf(sp.Carrier__c)+'-'+sp.Samsung_SKU__c+'-'+String.valueOf(sp.Sellout_Date__c);
                 clMap.put(key,sp);
             }
         }
      
  
      Integer yr=0;
      String Vers='';
      String type='';
      String uploadmnth='';// File Upload Month field same as Sellout Created Date 
      for(Sellout_Products__c sp :sols){
         
          SelloutStubClass.SelloutProducts sol = new SelloutStubClass.SelloutProducts();
               sol.modelCode=sp.Samsung_SKU__c;
               sol.opportunityNo=sp.BO_Number__c;
               sol.accountNo=codemap.get(sp.Carrier__c);
               sol.sapCompanyCode='C310';
               uploadmnth=sp.Sellout__r.Upload_Month__c;
                Date d=sp.Sellout_Date__c;
                String sMonth = String.valueof(d.month());
                String sDay = String.valueof(d.day());
                if(sMonth.length()==1){
                  sMonth = '0' + sMonth;
                }
                if(sDay.length()==1){
                  sDay = '0' + sDay;
                }
                string closemnth=sp.Sellout__r.Closing_Month_Year1__c;
                sol.rolloutMonth=string.valueOf(d.year())+sMonth;
               
                if(sp.Sellout__r.Subsidiary__c=='SEA'){
                      sol.countryCode='US';
                     }else if(sp.Sellout__r.Subsidiary__c=='SECA'){
                        sol.countryCode='CA'; 
                     }
               
                 if(sp.Sellout__r.RecordType.Name=='Plan'){
                    sol.applyMonth=string.valueOf(d.year())+sMonth;
                    
                       //
                        if(d.year()>yr){
                          yr=d.year();
                          vers=VersionMap.get(sMonth);
                        }
                         sol.version=vers;
                     //   
                    type='Plan';
                 }else{
                    sol.applyMonth=string.valueOf(closemnth.right(4)+closemnth.left(2)); 
                     sol.version='000';
                     type='Actual';
                 }
                String key=sp.Carrier__c+'-'+sp.Samsung_SKU__c+'-'+String.valueOf(sp.Sellout_Date__c);
                if(sp.IL_CL__c=='IL'){
                     sol.indexCode ='';
                      if(clMap.containsKey(key)){
                          Sellout_Products__c clsp=clMap.get(key);
                          decimal qty=sp.Quantity__c+clsp.Quantity__c;
                          sol.quantity=String.valueOf(qty);
                      }else{
                          sol.quantity=String.valueOf(sp.Quantity__c);  
                      }
                 }else{
                      sol.indexCode ='D';
                      sol.quantity=String.valueOf(sp.Quantity__c);  
                      
                   
                 }
                 
                   SelloutProducts.add(sol); 
                  if(sp.IL_CL__c=='CL'){ 
                      if(!ilMap.containsKey(key)){
                              SelloutStubClass.SelloutProducts sol2 = sol.clone();
                                sol2.indexCode ='';
                               SelloutProducts.add(sol2);
                          } 
                  }
                
             
      
        
      }
      
      /*
        Adding the SECA Sellout Data to JSON Body
           
      */
         for(SECA_Sellout_Data__c s:[Select id,Name,Account_No__c,Apply_Month__c,Country_Code__c,Index_Code__c,Loading_Month__c,Material_Code__c,Opportunity_No__c,Quantity__c,Rollout_Month__c,SAP_Company_Code__c,Version__c,Type__c from SECA_Sellout_Data__c where Data_Type__c='Sellout QTY Data' and Type__c=:type and Loading_Month__c=:uploadmnth]){
              SelloutStubClass.SelloutProducts sol = new SelloutStubClass.SelloutProducts();
               sol.modelCode=s.Material_Code__c;
               sol.opportunityNo=s.Opportunity_No__c;
               sol.accountNo=s.Account_No__c;
               sol.rolloutMonth=s.Rollout_Month__c;
               sol.countryCode='CA'; 
               sol.applyMonth=s.Apply_Month__c;
               sol.quantity=String.valueOf(s.Quantity__c);
               sol.indexCode =s.Index_Code__c;
               sol.sapCompanyCode='C330';
               sol.version=s.Version__c;
               SelloutProducts.add(sol);
               
               
         }
    
    
   }
   
   /*
      This Method to Prepare the JSON Body from SECA data and Combining the SEA Data
      Tigger From - SECA Data 
   */
   
   
   public static void mapbodyFields_SECA (SelloutStubClass.SelloutQTYRequestBody body, List<SECA_Sellout_Data__c> sols){
        list<SelloutStubClass.SelloutProducts> SelloutProducts = new list<SelloutStubClass.SelloutProducts>();
            mapProductFields_SECA(SelloutProducts, sols);
            body.lines = SelloutProducts;
   }
   public static void mapProductFields_SECA (list<SelloutStubClass.SelloutProducts> SelloutProducts,  List<SECA_Sellout_Data__c> secsol){ 
       String type='';
       String Version='';
       String uploadmnth='';
         Soid='';
       for(SECA_Sellout_Data__c s:secsol){
           SelloutStubClass.SelloutProducts sol = new SelloutStubClass.SelloutProducts();
               sol.modelCode=s.Material_Code__c;
               sol.opportunityNo=s.Opportunity_No__c;
               sol.accountNo=s.Account_No__c;
               sol.rolloutMonth=s.Rollout_Month__c;
               sol.countryCode='CA'; 
               sol.applyMonth=s.Apply_Month__c;
               sol.quantity=String.valueOf(s.Quantity__c);
               sol.indexCode =s.Index_Code__c;
               sol.sapCompanyCode='C330';
               sol.version=s.Version__c;
               SelloutProducts.add(sol);
               uploadmnth= s.Loading_Month__c;
               if(s.Version__c=='0' || s.Version__c=='000'){
                   type='Actual';
               }else{
                   type='Plan';
               }
               
       }
       
       /* Add Logic to Add SEA DATA */
        List<Sellout_Products__c> sols=new list<Sellout_Products__c>([Select id,name,Carrier__c,BO_Number__c,Sold_To_Code__c,Sellout__r.Upload_Month__c,Sellout__r.Owner.email,Carrier_SKU__c,IL_CL__c,Quantity__c,Samsung_SKU__c,Sellout_Date__c,Sellout__r.Subsidiary__c,Sellout__r.Version__c,Sellout__r.Closing_Month_Year1__c,Sellout__r.Description__c,Sellout__r.Name,Sellout__r.RecordType.Name from Sellout_Products__c where Status__c ='OK' and Sellout__r.Upload_Month__c=:uploadmnth and Sellout__r.RecordType.Name=:Type  order by Sellout_Date__c asc]);
        Integer yr=0;
      String Vers='';
        Map<String,String> codemap=new map<string,string>();
      List<Carrier_Sold_to_Mapping__c> crr=Carrier_Sold_to_Mapping__c.getall().values();
      for(Carrier_Sold_to_Mapping__c c:crr){
        codemap.put(c.Carrier_Code__c,c.Sold_To_Code__c);  
      }
      Map<string,Sellout_Products__c> ilMap=new Map<string,Sellout_Products__c>();
      Map<string,Sellout_Products__c> clMap=new Map<string,Sellout_Products__c>();
      Map<String,String> VersionMap=new Map<String,String>();
      VersionMap.put('01','T01');
      VersionMap.put('02','T02');
      VersionMap.put('03','T03');
      VersionMap.put('04','T04');
      VersionMap.put('05','T05');
      VersionMap.put('06','T06');
      VersionMap.put('07','T07');
      VersionMap.put('08','T08');
      VersionMap.put('09','T09');
      VersionMap.put('10','T0A');
      VersionMap.put('11','T0B');
      VersionMap.put('12','T0C');
    
         for(Sellout_Products__c sp :sols){
             if(sp.IL_CL__c=='IL'){
                 String key=String.valueOf(sp.Carrier__c)+'-'+sp.Samsung_SKU__c+'-'+String.valueOf(sp.Sellout_Date__c);
                 ilMap.put(key,sp);
             }else{
                 String key=String.valueOf(sp.Carrier__c)+'-'+sp.Samsung_SKU__c+'-'+String.valueOf(sp.Sellout_Date__c);
                 clMap.put(key,sp);
             }
             soid=sp.Sellout__C;
         }
      
  
      
      for(Sellout_Products__c sp :sols){
         
          SelloutStubClass.SelloutProducts sol = new SelloutStubClass.SelloutProducts();
               sol.modelCode=sp.Samsung_SKU__c;
               sol.opportunityNo=sp.BO_Number__c;
               sol.accountNo=codemap.get(sp.Carrier__c);
               sol.sapCompanyCode='C310';
               uploadmnth=sp.Sellout__r.Upload_Month__c;
                Date d=sp.Sellout_Date__c;
                String sMonth = String.valueof(d.month());
                String sDay = String.valueof(d.day());
                if(sMonth.length()==1){
                  sMonth = '0' + sMonth;
                }
                if(sDay.length()==1){
                  sDay = '0' + sDay;
                }
                string closemnth=sp.Sellout__r.Closing_Month_Year1__c;
                sol.rolloutMonth=string.valueOf(d.year())+sMonth;
               
                if(sp.Sellout__r.Subsidiary__c=='SEA'){
                      sol.countryCode='US';
                     }else if(sp.Sellout__r.Subsidiary__c=='SECA'){
                        sol.countryCode='CA'; 
                     }
               
                 if(sp.Sellout__r.RecordType.Name=='Plan'){
                    sol.applyMonth=string.valueOf(d.year())+sMonth;
                    
                       //
                        if(d.year()>yr){
                          yr=d.year();
                          vers=VersionMap.get(sMonth);
                        }
                         sol.version=vers;
                     //   
                    type='Plan';
                 }else{
                    sol.applyMonth=string.valueOf(closemnth.right(4)+closemnth.left(2)); 
                     sol.version='000';
                     type='Plan';
                 }
                String key=sp.Carrier__c+'-'+sp.Samsung_SKU__c+'-'+String.valueOf(sp.Sellout_Date__c);
                if(sp.IL_CL__c=='IL'){
                     sol.indexCode ='';
                      if(clMap.containsKey(key)){
                          Sellout_Products__c clsp=clMap.get(key);
                          decimal qty=sp.Quantity__c+clsp.Quantity__c;
                          sol.quantity=String.valueOf(qty);
                      }else{
                          sol.quantity=String.valueOf(sp.Quantity__c);  
                      }
                 }else{
                      sol.indexCode ='D';
                      sol.quantity=String.valueOf(sp.Quantity__c);  
                      
                   
                 }
                 
                   SelloutProducts.add(sol); 
                  if(sp.IL_CL__c=='CL'){ 
                      if(!ilMap.containsKey(key)){
                              SelloutStubClass.SelloutProducts sol2 = sol.clone();
                                sol2.indexCode ='';
                               SelloutProducts.add(sol2);
                          } 
                  }
                
             
      
        
      }
      
       
       
       
   }
   
   // Sending the Email Method
      public Static void SendRequestfileasEmail (String jsondata){
       
       Map<String, Object> meta = (Map<String, Object>) JSON.deserializeUntyped(jsondata);
       Map<String, Object>  carMap = (Map<String, Object>) meta.get('body');
        list<Object> lines=(list<Object>)carMap.get('lines'); 
        
        List<Map<String, Object>> mapList = new List<Map<String, Object>>();
        Set<String> keySet = new Set<String>();
        
        for (Object entry : lines) {
            Map<String, Object> m = (Map<String, Object>)(entry);
            keySet.addAll(m.keySet());
            mapList.add(m);
        }
        
        List<String> keys = new List<String>(keySet);
        keys.sort();
        
        List<List<String>> csvLines = new List<List<String>>();
        
        for (Integer i = 0; i <= mapList.size(); i++) {
            csvLines.add(new List<String>());
        }
        
        for (String key : keys) {
            csvLines.get(0).add('"' + key + '"');
            
            for (Integer i = 1; i <= mapList.size(); i++) {
                csvLines.get(i).add('"' + (String)(mapList.get(i - 1).get(key)) + '"');
            }
        }
        
       List<String> csvstring=new List<String>();
        for (List<String> csvLine : csvLines) {
            String line = '';
            for (Integer i = 0; i < csvLine.size() - 1; i++) {
              line += csvLine.get(i) + ',';
            }
            line += csvLine.get(csvLine.size() - 1);
           csvstring.add(line);
        }
        
            String seacsv='';
            string secacsv='';
            Integer count=0;
            Integer sealines=0;
            Integer secalines=0;
            for(String s:csvstring){
                if(count==0){
                    seacsv +=s+'\n';
                    secacsv +=s+'\n';
                }else{
                    if(s.contains('C310')){
                        seacsv +=s+'\n';
                        sealines++;
                    }else{
                        secacsv +=s+'\n';
                        secalines++;
                    } 
                }
                
                Count++;
            }
                     
          if(seacsv !='' && seacsv !=null && sealines >=1 ){
              Blob csv = Blob.valueof(seacsv);
              Messaging.EmailFileAttachment attch = new Messaging.EmailFileAttachment();
              attch.setFileName('SEA_Sellout_QTY_Data.CSV');
              attch.setBody(csv); 
            
             Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
               email.setSubject( 'Sellout Quantity Data Sent to GERP' );
               string body ='Hello, \n'+'</br></br>'+'Please Check The Attached file that we sent to GERP Sucessfully. '+'</br></br>'+'Thanks,SFDC Admin Team';
               email.setHTMLBody(body );
               list<string> emailaddresses = new list<string>();
                String ToAddress = Label.SEA_Sellout_Notification;
                if(String.isNotBlank(ToAddress))
                emailaddresses = ToAddress.split(',');
                
                if(emailaddresses.isEmpty() == false){
                      email.setToAddresses( emailaddresses );
                        String supportEmailCCAddress = Label.CC_Email_address_for_SAM_Notification;
                       
                       List<String> supportEmailCCAddressList = new List<String>();
        
                    if(String.isNotBlank(supportEmailCCAddress))
                        supportEmailCCAddressList = supportEmailCCAddress.split(',');
        
                    if (supportEmailCCAddressList.isEmpty() == false) {
                        email.setCcAddresses(supportEmailCCAddressList);
                    }
                       email.setFileAttachments(new Messaging.EmailFileAttachment[] {attch});
                      if(!Test.isRunningTest()){
                          Messaging.SendEmailResult [] result = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
                      }
                            
                }
                
             
          }
          
                if(secacsv !='' && secacsv !=null && secalines >=1 ){
              Blob csv = Blob.valueof(secacsv);
              Messaging.EmailFileAttachment attch = new Messaging.EmailFileAttachment();
              attch.setFileName('SECA_Sellout_QTY_Data.CSV');
              attch.setBody(csv); 
            
             Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
               email.setSubject( 'Sellout Quantity Data Sent to GERP' );
               string body ='Hello,\n'+'</br></br>'+'Please Check The Attached file that we sent to GERP Sucessfully. '+'</br></br>'+'Thanks,SFDC Admin Team';
                       email.setHTMLBody(body );
               list<string> emailaddresses = new list<string>();
                String ToAddress = Label.SECA_Sellout_Notification;
                if(String.isNotBlank(ToAddress))
                emailaddresses = ToAddress.split(',');
                
                if(emailaddresses.isEmpty() == false){
                      email.setToAddresses( emailaddresses );
                        String supportEmailCCAddress = Label.CC_Email_address_for_SAM_Notification;
                       
                       List<String> supportEmailCCAddressList = new List<String>();
        
                    if(String.isNotBlank(supportEmailCCAddress))
                        supportEmailCCAddressList = supportEmailCCAddress.split(',');
        
                    if (supportEmailCCAddressList.isEmpty() == false) {
                        email.setCcAddresses(supportEmailCCAddressList);
                    }
                       email.setFileAttachments(new Messaging.EmailFileAttachment[] {attch});
                      if(!Test.isRunningTest()){
                          Messaging.SendEmailResult [] result = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
                      }
                            
                }
          }
            
            
        
        
    }
    
        public static void handleError(message_queue__c mq,string err, String soid){
            mq.retry_counter__c += 1;
            mq.status__c ='failed';
            mq.ERRORTEXT__c =err;
            upsert mq; 
            
             if(mq.Identification_Text__c.startsWith('a34')){
                Sellout__c so=[select id,name,Integration_Status__c from Sellout__c where id=: soid];
            so.Integration_Status__c='Success';
            update so;
            }else if(soid !='' && soid != null){
                Sellout__c so=[select id,name,Integration_Status__c from Sellout__c where id=: soid];
                 so.Integration_Status__c='Success';
                 so.Last_Data_Sent__c=System.now();
            update so;
            } 
           
    }
    
        public static void handleSuccess(message_queue__c mq, SelloutStubClass.UpdateQtyFrmERPResponse res, string soid){
            mq.status__c = 'success';
            mq.MSGSTATUS__c = res.inputHeaders.MSGSTATUS;
            mq.msguid__c = res.inputHeaders.msgGUID;
            mq.IFID__c = res.inputHeaders.ifID;
            mq.IFDate__c = res.inputHeaders.ifDate;
            upsert mq; 
            if(mq.Identification_Text__c.startsWith('a34')){
                Sellout__c so=[select id,name,Integration_Status__c from Sellout__c where id=: soid];
            so.Integration_Status__c='Success';
            update so;
            }else if(soid !='' && soid != null){
                Sellout__c so=[select id,name,Integration_Status__c from Sellout__c where id=: soid];
            so.Integration_Status__c='Success';
            update so;
            }
            
            
            
    }
   
      public static string webcallout(string body, string partialEndpoint){ 
       System.debug('##requestBody ::'+body);
        System.Httprequest req = new System.Httprequest();
        system.debug('************************************** INSIDE CALLOUT METHOD');
        HttpResponse res = new HttpResponse();
        req.setMethod('POST');
        req.setBody(body);
        system.debug(body);
       
        req.setEndpoint('callout:X012_013_ROI'+partialEndpoint); //+namedEndpoint); todo, make naming consistent on picklist values and endpoints and use that here
        
        req.setTimeout(120000);
         req.setHeader('Content-Type', 'application/json');
        req.setHeader('Accept', 'application/json');
    
        Http http = new Http();  
        res = http.send(req);
        System.debug('******** my resonse' + res);
         System.debug('******** my request' + req);
        system.debug('*****Response body::' + res.getBody());
        System.debug('***** Response Body Size ' + res.getBody().length());
        return res.getBody();     
      
  }
   
}