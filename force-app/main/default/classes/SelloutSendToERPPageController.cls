public class SelloutSendToERPPageController {

 Public Sellout__c so {get; set;}
 public SelloutSendToERPPageController(ApexPages.StandardController controller) {
     if(controller.getRecord().Id !=null){
         so=[select id from Sellout__c where id=:controller.getRecord().Id limit 1];
     }
 }
 
  public pagereference send(){
      if(so.id!=null){
       List<message_queue__c> mqs=new list<message_queue__c>();
         message_queue__c mq1=new message_queue__c(Identification_Text__c=so.id,
                                                 Integration_Flow_Type__c='Flow-030',
                                                 retry_counter__c=0,
                                                 Object_Name__c='Sellout',
                                                 Status__c='not started'
                                                  );
         message_queue__c mq2=new message_queue__c(Identification_Text__c=so.id,
                                                 Integration_Flow_Type__c='Flow-031',
                                                 retry_counter__c=0,
                                                 Object_Name__c='Sellout',
                                                 Status__c='not started'
                                                  );        
          mqs.add(mq1);
          mqs.add(mq2);
          Insert mqs;
      }
           return new pageReference('/'+so.id);
        
  }

}