@isTest
private class SelloutSendToERPPageControllerTEST {

	private static testMethod void testmethod1() {
              Test.startTest();
          Sellout__C so=new Sellout__C(Closing_Month_Year1__c='10/2018',Description__c='From Test Class',Subsidiary__c='SEA',Approval_Status__c='Draft',Integration_Status__c='Draft',Has_Attachement__c=true,Request_Approval__c=true);
         insert so;
         Date d=System.today();
         RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];
          Product2 prod1 = new Product2(
                name ='SM-TEST123',
                SAP_Material_ID__c = 'SM-TEST123',
                RecordTypeId = productRT.id
            );
             insert prod1;
          Sellout_Products__c sl0=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST123',Quantity__c=100,Carrier__c='ATT',Carrier_SKU__c='Samsung Mobile',IL_CL__c='CL');
          insert sl0;
         
         Pagereference pf= Page.SelloutSendToERPPage;
           pf.getParameters().put('id',so.id);
           test.setCurrentPage(pf);
        
        apexPages.standardcontroller sc = new apexPages.standardcontroller(so);
          SelloutSendToERPPageController cls=new SelloutSendToERPPageController(sc);
          
            cls.send();
         Test.StopTest(); 
	}

}