global class SelloutStubClass {
    global class SelloutQTYRequest {
        public msgHeadersRequest inputHeaders {get;set;}        
        public SelloutQTYRequestBody body {get;set;}
    }
    
     global class SECASelloutQTYRequest {
        public msgHeadersResponse inputHeaders {get;set;}        
        public SelloutQTYRequestBody_seca body {get;set;}
    }
     global class SelloutQTYRequestBody_seca {
        
        public list<SelloutProducts_seca> lines {get;set;}
    }
    
    global class SelloutQTYRequestBody {
        
        public list<SelloutProducts> lines {get;set;}
    }
    
    global class SelloutProducts {
        public string division {get{return 'G1';}set;}
        public string modelCode {get;set;}
        public string accountNo {get;set;}
        public string quantity {get;set;}
        public string sapCompanyCode {get; set;}
        public string ERNAM {get{return 'SFDC (US)';} set;}
        public string version {get;set;}
        public string applyMonth {get;set;}
        public string countryCode {get;set;}
         public string opportunityNo {get;set;}
         public string rolloutMonth {get;set;}
         public string indexCode {get; set;}
       // public string comment {get;set;}
    }
    
       global class SelloutProducts_seca {
        public string division {get{return 'G1';}set;}
        public string modelCode {get;set;}
        public string accountNo {get;set;}
        public string quantity {get;set;}
        public string sapCompanyCode {get; set;}
        public string ERNAM {get{return 'SFDC (US)';} set;}
        public string version {get;set;}
        public string applyMonth {get;set;}
        public string countryCode {get;set;}
         public string opportunityNo {get;set;}
         public string rolloutMonth {get;set;}
         public string indexCode {get; set;}
         public string loadingMonth {get;set;}
    }
     global class SelloutAccRequest {
        public msgHeadersRequest inputHeaders {get;set;}        
        public SelloutAccRequestBody body {get;set;}
    }
    
    global class SECASelloutAccRequest {
        public msgHeadersResponse inputHeaders {get;set;}        
        public SelloutAccRequestBody_seca body {get;set;}
    }
     global class SelloutAccRequestBody_seca {
        
        public list<SelloutProductsAcc_seca> lines {get;set;}
    }
    
    
    global class SelloutAccRequestBody {
        
        public list<SelloutProductsAcc> lines {get;set;}
    }
    
    global class SelloutProductsAcc {
       
        public string modelCode {get;set;}
        public string accountNo {get;set;}
      
        public string sapCompanyCode {get; set;}
        public string ERNAM {get{return 'SFDC (US)';} set;}
        public string version {get;set;}
        public string applyMonth {get;set;}
        public string billToCode {get;set;}
       // public string comment {get;set;}
    }
    
     global class SelloutProductsAcc_seca {
       
        public string modelCode {get;set;}
        public string accountNo {get;set;}
      
        public string sapCompanyCode {get; set;}
        public string ERNAM {get{return 'SFDC (US)';} set;}
        public string version {get;set;}
        public string applyMonth {get;set;}
        public string billToCode {get;set;}
        public string loadingMonth {get;set;}
    }
 /*    global class SelloutQTYRequestFrmERP {
        public msgHeadersResponse inputHeaders {get;set;}        
        public SelloutRequestBodyFrmERP body {get;set;}
    }
    global class SelloutRequestBodyFrmERP {
       public list<SelloutqtyfrmERP> lines {get;set;}
    }
    
    global class SelloutqtyfrmERP {
       public string selloutMonth {get;set;}
       public string ver {get;set;}
       public string sapCompanyCode {get{return 'C310';} set;}
       public string profileCenterCode {get;set;}
       public string modelCode {get;set;}
       public string billToCode {get;set;}
       public string applyYn {get;set;}
     
    } */
    
  global  class UpdateQtyFrmERPResponse {
        public msgHeadersResponse inputHeaders {get;set;}        
        public string body {get;set;}
        
        public UpdateQtyFrmERPResponse() {
            this.inputHeaders = new msgHeadersResponse();
            this.body = '';
        }
    }
    
   global class msgHeadersRequest{
        public string MSGGUID {get{string s = giveGUID(); return s;}set;} // GUID for message
        public string IFID {get;set;} // interface id, created automatically, so need logic
        public string IFDate {get{return datetime.now().formatGMT('YYYYMMddHHmmss');}set;} // interface date
        
        public msgHeadersRequest(){
            
        }
        
        public msgHeadersRequest(string layoutId){
            IFID = layoutId;
        }
    } 

    global class msgHeadersResponse {
        public string MSGGUID {get;set;} // GUID for message
        public string IFID {get;set;} // interface id, created automatically, so need logic
        public string IFDate {get;set;} // interface date
        public string MSGSTATUS {get;set;} // start, success, failure (ST , S, F)
        public string ERRORCODE {get;set;} // error code
        public string ERRORTEXT {get;set;} // error message
    } 
 
    global static string giveGUID(){
        Blob b = Crypto.GenerateAESKey(128);
        String h = EncodingUtil.ConvertTohex(b);
        String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
        return guid;
    }
    
    
     global class returnResponse {
        global string fileName {get;set;}
        global string fileType {get;set;}
        global blob body {get;set;} 
     }

}