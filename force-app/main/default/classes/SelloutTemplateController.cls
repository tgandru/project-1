public class SelloutTemplateController {
 public Id soId {get;set;}
 public integer cltotal {get;set;}
 public integer iltotal {get;set;}
 public integer grndtotal {get;set;}
 public list<AggrigateWrapper> wrp{get; set;}
  public list<ApprovalWrapper> Apprvlwrp{get; set;}
   public List<ProcessInstanceStep> approvalProcessItems2;
   public String recordlnk {get; set;}
    public Sellout__c so {get; set;}
    
    public  List<ProcessInstanceStep>  getapprovalProcessItems2() {
    	if (approvalProcessItems2 == null && soId !=null) {
    	    approvalProcessItems2=new List<ProcessInstanceStep>();
    		List<ProcessInstance>  tempProcess = [Select p.TargetObjectId, p.SubmittedBy.Name, p.SubmittedById, p.Status, 
    								p.ProcessDefinition.State, p.ProcessDefinition.DeveloperName, p.ProcessDefinition.Name, 
    								p.ProcessDefinitionId, p.LastActor.Name, p.LastActorId, p.CompletedDate, 
    								(Select StepStatus, Comments,Actor.Name, OriginalActor.Name,StepNodeid,CreatedDate From Steps order by CreatedDate desc) 
    								From ProcessInstance p Where targetObjectId =: soId order by CreatedDate desc limit 1];
    		if (!tempProcess.isEmpty()) {
    			for (ProcessInstance pi : tempProcess) {
    				for (ProcessInstanceStep pis: pi.Steps) {
    				    approvalProcessItems2.add(pis);
    					
    				}
    			}
    		}
    	}
    	return approvalProcessItems2;
    } 
    
     public List<ApprovalWrapper> getApprovalWrappers(){
         
         Apprvlwrp= new list<ApprovalWrapper>();
         if(soId !=null){
         List<ProcessInstance>  tempProcess = [Select p.TargetObjectId, p.SubmittedBy.Name, p.SubmittedById, p.Status, 
    								p.ProcessDefinition.State, p.ProcessDefinition.DeveloperName, p.ProcessDefinition.Name, 
    								p.ProcessDefinitionId, p.LastActor.Name, p.LastActorId, p.CompletedDate, 
    								(Select StepStatus, Comments,Actor.Name, OriginalActor.Name,StepNodeid,CreatedDate,ActorID,OriginalActorID From Steps order by CreatedDate asc) 
    								From ProcessInstance p Where targetObjectId =: soId order by CreatedDate desc limit 1];
    			    			List<Sellout_Approval_Order__c> aprvr=new list<Sellout_Approval_Order__c>([select id,name,Status__c,Sequence__c,Approval_Step_User_ID__c from Sellout_Approval_Order__c order by Sequence__c asc ]);    
    		  decimal seqn=0;
    			if (!tempProcess.isEmpty()) {
    			for (ProcessInstance pi : tempProcess) {
    			     
        			        
        			       for (ProcessInstanceStep pis: pi.Steps) {
        			           
        			            for(Sellout_Approval_Order__c s:aprvr){
                    			           String Comment=pis.Comments;
                    				      if(s.Sequence__c==0 && pis.StepStatus=='Started'){
                    				          Apprvlwrp.add(new ApprovalWrapper(s.Sequence__c,'Submitted',pis.OriginalActor.Name,pis.CreatedDate,Comment));
                    				          	seqn=s.Sequence__c;
                    				      }else if(s.Sequence__c!=0 && pis.StepStatus!='Started' && s.Sequence__c> seqn && s.Approval_Step_User_ID__c !=null ){// s.Approval_Step_User_ID__c==pis.OriginalActorID &&
                    				         Apprvlwrp.add(new ApprovalWrapper(s.Sequence__c,s.Status__c,pis.Actor.Name,pis.CreatedDate,Comment)); 
                    				         	seqn=s.Sequence__c;
                    				         	break;
                    				      }
        				
        				} 
        			        
    			    }
    			    
    			    if(pi.Status=='Approved'){
    			        
    			         for(Sellout_Approval_Order__c sor:aprvr){
    			              if(sor.Status__c =='Notification' && sor.Sequence__c >seqn ){
    			                   Apprvlwrp.add(new ApprovalWrapper(sor.Sequence__c,'Notification',sor.Name,pi.CompletedDate,' ')); 
    			                    seqn=sor.Sequence__c;
    			              }
    			             
    			         }
    			    }
    			    
    		 
    				
    			}
    		}
    		
    		 if(aprvr.size()>Apprvlwrp.size()){
    			        integer cnt=Apprvlwrp.size()-1;
    			        for(Sellout_Approval_Order__c s:aprvr){
    			            if(s.Sequence__c >cnt){
    			                if(s.Status__c !='Notification'){
    			                     Apprvlwrp.add(new ApprovalWrapper(s.Sequence__c,'Pending',s.Name,null,' ')); 
    			                }else{
    			                     Apprvlwrp.add(new ApprovalWrapper(s.Sequence__c,'Notification',s.Name,null,' ')); 
    			                }
    			              
    			            }
    			        }
    			    }
    		System.debug('Apprvlwrp::'+Apprvlwrp);						
         }
         return Apprvlwrp;
     }
    
    
    
 public List<AggrigateWrapper> getAggrigateWrappers(){
     list<AggrigateWrapper> wrp = new list<AggrigateWrapper>();
     recordlnk = System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+soid;
     Map<String,String> codemap=new map<string,string>();
      List<Carrier_Sold_to_Mapping__c> crr=Carrier_Sold_to_Mapping__c.getall().values();
      for(Carrier_Sold_to_Mapping__c c:crr){
        codemap.put(c.Carrier_Code__c,c.Name);  
      }
      cltotal=0;
      iltotal=0;
      grndtotal=0;
      if(soId != null){
     so=[select id,name,Description__c,Subsidiary__c,Closing_Month_Year1__c from Sellout__c where id=:soid];
     AggregateResult[] ar=[select sum(Quantity__c) qty,sum(Amount__c) amt,Carrier__c cr,IL_CL__c ilcl from Sellout_Products__c where Sellout__c=:soid and Status__C='OK' group by Carrier__c,IL_CL__c  ];
       
      Map<string,list<AggregateResult>> aggregateMap=new Map<string,list<AggregateResult>>(); 
      for(AggregateResult a:ar){
          if(aggregateMap.containsKey((string)a.get('cr'))){
             List<AggregateResult> arl = aggregateMap.get((string)a.get('cr'));
                            arl.add(a);
                           aggregateMap.put((string)a.get('cr'), arl);  
          }else{
              aggregateMap.put((string)a.get('cr'), new List<AggregateResult> { a });  
          }
      }
      for(string s:aggregateMap.keyset()){
          decimal clq=0;
          decimal ilq=0;
          decimal qt=0;
            for(AggregateResult a:aggregateMap.get(s)){
               if((string)a.get('ilcl')=='IL'){
                   ilq=ilq+(decimal)a.get('qty');
                   iltotal=iltotal+integer.valueOf(ilq);
                 //  ilamt=ilamt+(decimal)a.get('amt');
               }else if((string)a.get('ilcl')=='CL'){
                  clq=clq+(decimal)a.get('qty');
                  cltotal=cltotal+integer.valueOf(clq);
               }
               qt=qt+(decimal)a.get('qty');
               
           }
           String crName=codemap.get(s);
           wrp.add(new AggrigateWrapper(crName,clq,ilq,qt));
           
      }
     grndtotal=cltotal+iltotal;
      }
     return wrp;
 }
 Public SelloutTemplateController(){
        if(soid !=null){
            so=[select id,name,Description__c,Subsidiary__c,Closing_Month_Year1__c from Sellout__c where id=:soid];
        }
     }
     
     
public class ApprovalWrapper{
    Public integer  seq {get; set;}
    Public string  Status {get; set;}
    Public string  UserName {get; set;}
    Public datetime  Approvaldate {get; set;}
    Public string  comments {get; set;}
    
    
    public ApprovalWrapper(decimal sq,string sta,string urname,datetime dt,string cmnts){
        seq=integer.valueOf(sq);
        Status=sta;
        UserName=urname;
        //Approvaldate=dt;
        comments=cmnts;
       if(dt !=null){
                Integer offset = UserInfo.getTimezone().getOffset(dt);
                Approvaldate = dt.addSeconds(offset/1000);
       }
    }
}     
     
 public class AggrigateWrapper{
      public String carrier {get;set;}
      public integer clqty {get;set;}
      public decimal clamnt {get; set;}
      public decimal ilqty {get;set;}
      public decimal ilamnt {get; set;}
      public decimal qty {get; set;}
      public decimal amnt {get; set;}
      public AggrigateWrapper(string cr,decimal clq,decimal ilq,decimal qt){
         carrier=cr;
         clqty=integer.valueOf(clq);
         //clamnt=clamt;
         ilqty=integer.valueOf(ilq);
        // ilamnt=ilamt;
         qty=integer.valueOf(qt);
         //amnt=amt;
         
     }
 }
}