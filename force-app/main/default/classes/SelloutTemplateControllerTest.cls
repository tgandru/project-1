@isTest
private class SelloutTemplateControllerTest {

	private static testMethod void testmethod1() {
	    Test.startTest();
          Sellout__C so=new Sellout__C(Closing_Month_Year1__c='10/2018',Description__c='From Test Class',Subsidiary__c='SEA',Approval_Status__c='Draft',Integration_Status__c='Draft',Has_Attachement__c=true,Request_Approval__c=true);
         insert so;
         Date d=System.today();
          RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];

          Product2 prod1 = new Product2(
                name ='SM-TEST123',
                SAP_Material_ID__c = 'SM-TEST123',
                RecordTypeId = productRT.id
            );

        insert prod1;
         List<Sellout_Products__c> slp=new list<Sellout_Products__c>();
          Sellout_Products__c sl0=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST123',Quantity__c=100,Carrier__c='ATT',Carrier_SKU__c='Samsung Mobile',IL_CL__c='CL');
          Sellout_Products__c sl1=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST123',Quantity__c=100,Carrier__c='ATT',Carrier_SKU__c='Samsung Mobile',IL_CL__c='IL');
          slp.add(sl0);
          slp.add(sl1);
          insert slp;
          Sellout_Approval_Order__c sapp=new Sellout_Approval_Order__c(name='TEST',Sequence__c=0,Status__c='Submitted');
          insert sapp;
           Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitrequest();
             app.setObjectId(so.Id);
    
            Approval.ProcessResult result = Approval.process(app);
    
          SelloutTemplateController cls=new SelloutTemplateController();
          cls.soId=so.id;
          cls.so=so;
          cls.getapprovalProcessItems2();
          cls.getApprovalWrappers();
          cls.getAggrigateWrappers();
            
       Test.StopTest();      
	}

}