public class SelloutTriggerHandler {

     Public Static void deleteAttachementfromTemplate(List<Sellout__C> so,map<id, Sellout__c> oldMap){
     String parentid=label.Sellout_Template_ID;
     List<id> ids=new list<id>();
       for(Sellout__C s:so){
           Sellout__C oldso=oldMap.get(s.id);
           if(s.Approval_Status__c =='Draft' && oldso.Approval_Status__c =='Pending Approval'){
               ids.add(s.id);
           }
           
       }
        if(ids.size()>0){
           if(parentid !=null && parentid !=''){
                List<Attachment> att=new list<Attachment>([select id,name from Attachment  where parentid=:parentid and Description='KNOX Approval'  order by createddate desc limit 1 ]); 
                if(att.size()>0){
                   delete att; 
                }
             }  
        }
            
   
 }
     
     Public Static void UpdateComments(List<Sellout__C> so,map<id, Sellout__c> oldMap){
         
         for(Sellout__c s:so){
              Sellout__C oldso=oldMap.get(s.id);
              if(s.Integration_Comments__c != oldso.Integration_Comments__c){
                  String newcomm=s.Integration_Comments__c;
                  String oldcomm=oldso.Integration_Comments__c;
                  String curtime=Datetime.now().format('MM/dd/yyyy hh:mm a');
                  if(oldcomm !=null&& oldcomm !=''){
                     // s.Integration_Comments__c=newcomm+'\t'+'-' +UserInfo.getName()+'\t'+curtime+'\n'+oldcomm;
                  }else{
                     // s.Integration_Comments__c=newcomm+'\t'+'-'+ UserInfo.getName()+'\t'+curtime;
                  }
              }
         }
         
     }
 

}