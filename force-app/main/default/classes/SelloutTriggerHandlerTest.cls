@isTest
private class SelloutTriggerHandlerTest {

	private static testMethod void testmethod1() {
	    Test.starttest();
         Sellout__C so=new Sellout__C(Closing_Month_Year1__c='10/2018',Description__c='From Test Class',Subsidiary__c='SEA',Approval_Status__c='Draft',Integration_Status__c='Draft');
         insert so;
         Sellout__C s=[select id,Approval_Status__c from Sellout__C where id=:so.id];
         s.Approval_Status__c='Approved';
         s.Integration_Comments__c ='TEST';
         update s;
         
         Test.StopTest();
         
	}

}