public class SelloutUploadfileController {

/*
Created By- Vijay 7/30
Purpose - Provide Option to User to Load CSV file into Sellout obejct which includes actual sellout SKU records;

Template:
Actual ::
Column1   Column2        Column3        Column4         Column5      Column6
Carrier  Carrier SKU    Samsung SKU    Quantity       CL or IL     SellOut Date(MMDDYYYY)

Template:
Plan ::
Column1           Column2        Column3         Column4      Column5
Carrier          Samsung SKU    Quantity        CL or IL     SellOut Date(MMDDYYYY)


*/   
    
    
    public transient blob contentFile {get; set;}
    public String fileName {get; set;}
     public String errorlist {get; set;}
    public String crrError {get; set;}
    public boolean showselectoptn {get; set;}
    public String RTName {get; set;}
    public String selectedoption {get; set;}
    Public Sellout__c so {get; set;}
    public Blob csvFileBody{get;set;}
    public string csvAsString{get;set;}
    Public list<Sellout_Products__c> sellpr {get; set;}
    //public Map<string,string>  crrMap {get; set;}
    
    public list<ErrorRecord> errorlists {get;set;}
    
     public class ErrorRecord {
        
        public string crr {get;set;}
        public string sku {get;set;}
        public string qty {get;set;}
        public string ilcl {get; set;}
        public string sodate {get;set;}
        public string error {get;set;}
        public ErrorRecord (string cr,string skus,string qt,string icl,string dt,string err){
            crr=cr;
            sku=skus;
            qty=qt;
            ilcl=icl;
            sodate=dt;
            error=err;
        }
        
    }
     public boolean isExport {get;set;}
    public SelloutUploadfileController (ApexPages.StandardController controller) {
        isExport=false;
        errorlists=new list<ErrorRecord>();
       so=(Sellout__c) controller.getRecord();
       errorlist='Carrier,Samsung SKU,Quantity,IL/CL,Sellout Date, Error Message \n';
       showselectoptn =false;
       if(so.id !=null){
       
          sellpr=[select id,name from Sellout_Products__c where Sellout__c=:so.id];
          if(sellpr.size()>0){
            showselectoptn =true;
          }
       
          RTName =[select RecordType.Name from Sellout__c where id=:so.id limit 1][0].RecordType.Name;
          system.debug('RecTy::'+RTName);
       }
    }
   public List<SelectOption> getselectOptions(){
     List<SelectOption> options = new List<SelectOption>();
        options.add(New SelectOption('Override','Override'));
         options.add(New SelectOption('Append','Append'));
   
       return options;
   } 
   
    
    Public PageReference createdata(){
     boolean retval=True;
    if(selectedoption=='Override'){
      delete sellpr;
    }
    
      if(String.isBlank(fileName))
        {
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please upload a file'));
            return null;
        }

        if(String.isNotBlank(fileName) && (!fileName.contains('.csv') && !fileName.contains('.CSV')))
        {
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please upload a valid CSV file'));
            return null;
        }
        
        List<Sellout_Products__c> sop=new List<Sellout_Products__c>();
        String contentFileStr = contentFile.toString();//blobToString(contentFile, 'MS949')//contentFile.toString()
        //system.debug('contentFileStr ::'+contentFileStr );
        List<String> UploadStringList = contentFileStr.split('\n');
        String actMnth='';
        String actyr='';
        set<string> plnMnth=new set<string>();
         for(Integer i=1;i<UploadStringList.size();i++){
            Sellout_Products__c soprec=new Sellout_Products__c();
            string[] csvRecordData = UploadStringList[i].split(',');
            soprec.Sellout__c=so.id;
            String crr=csvRecordData[0].replace('"','');
            soprec.Carrier__c=csvRecordData[0].replace('"','').trim();//First Column is Carrier
             //if(RTName=='Plan'){
                    string sku=csvRecordData[1].replace('"','');
                    String qty=csvRecordData[2].replace('"','');//#rd Column is QTY
                     
                    soprec.IL_CL__c=csvRecordData[3].replace('"','');//3rd Column is IL/CL
                    string dt=string.valueof(csvRecordData[4].replace('\\s+', '')).trim();//4th cloumn is Sellout Date 
                      if(dt!=null&&dt!=''){
                       
                           if(dt.length()==7){
                               String d=dt.left(1)+'/'+dt.mid(1,2)+'/'+dt.right(4);
                               soprec.Sellout_Date__c=Date.parse(d);
                           }else{
                              String d=dt.left(2)+'/'+dt.mid(2,2)+'/'+dt.right(4);
                              soprec.Sellout_Date__c=Date.parse(d); 
                           }
                       }
                      soprec.Samsung_SKU__c=sku;
                      soprec.Carrier_SKU__c=null;
                      if(qty !=null && qty !=''){
                         qty=qty.replace(',','');
                        soprec.Quantity__c=Decimal.valueOf(qty);
                    }else{
                         errorlists.add(new ErrorRecord(crr,sku,qty,csvRecordData[3].replace('"',''),dt,'Quantity is Blank'));
                         retval=false;
                    }
            sop.add(soprec);
         }
         
         
        try{
          //database.insert(sop,false);
          
          boolean valid=validatedata(sop,retval);
          if(valid==true){
              insert sop;
          
              so.File_Upload_Date__c=system.now();
              update so;
              return new PageReference('/apex/SellOutSKUEdits?id='+so.id);
          }else{
             
              ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'There is Some Data need fix before Uploading into System. Please Take a look Error List');
              ApexPages.addMessage(errorMessage);
              return null;
          }
          
          
          
        }catch(exception ex){
           ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured while importin data Please make sure input csv file is correct'+ex);
            ApexPages.addMessage(errorMessage);
        }
       return null; 
       
    }
    
    
    public boolean validatedata(List<Sellout_Products__c> sop,boolean retval){
        boolean ret=retval;
        set<String> crOptions=new set<string>();
        set<String> skus=new set<string>();
     for(Schema.PicklistEntry pe : Sellout_Products__c.Carrier__c.getDescribe().getPicklistValues()){
         crOptions.add(pe.getValue());
        } 
        
        Set<string> skuid= new set<string>();
        map<string,string> pdmap=new  map<string,string>();
      
        for(Sellout_Products__c s:sop){
            skuid.add(s.Samsung_SKU__c); 
        }
        if(skuid.size()>0){
            for(Product2 p:[select id,SAP_Material_ID__c from Product2 where SAP_Material_ID__c  in :skuid]){
                pdmap.put(p.SAP_Material_ID__c,p.id);
            }
        }
        
        set<String> uniqrecord=new set<String>();
        
        
        for(Sellout_Products__c s:sop){
            
           
            
          String pid =pdmap.get(s.Samsung_SKU__c);
             Date d=s.Sellout_Date__c;
                String sMonth = String.valueof(d.month());
                String sDay = String.valueof(d.day());
                if(sMonth.length()==1){
                  sMonth = '0' + sMonth;
                }
                if(sDay.length()==1){
                  sDay = '0' + sDay;
                }
                String dt=sMonth+sDay+string.valueOf(d.year());
                
                 String key= s.Carrier__c+'-'+s.Samsung_SKU__c+'-'+s.IL_CL__c+'-'+dt;
                 if(uniqrecord.contains(key)){
                     ret=false;
                    errorlists.add(new ErrorRecord(s.Carrier__c,s.Samsung_SKU__c,string.valueOf(s.Quantity__c),s.IL_CL__c,dt,'Dupliacate Line'));
                 }else{
                     uniqrecord.add(key);
                 }
                 
            if(pid==null || pid==''){
                ret=false;
                errorlists.add(new ErrorRecord(s.Carrier__c,s.Samsung_SKU__c,string.valueOf(s.Quantity__c),s.IL_CL__c,dt,'SKU Not Avilable in Product Master'));
               
            }
            if(!crOptions.contains(s.Carrier__c)){
                  ret=false;
                  errorlists.add(new ErrorRecord(s.Carrier__c,s.Samsung_SKU__c,string.valueOf(s.Quantity__c),s.IL_CL__c,dt,'Carrier Code is Wrong'));
              }
            
            
        }
        
        System.debug('**Error List::**'+errorlist);
       return ret; 
        
        
    }
    
    
    public PageReference export(){
    isExport = true;
      return null;
    }
    
 
}