@isTest
private class SelloutUploadfileControllerTest {

	private static testMethod void testmethod1() {
         Test.startTest();
          Product2 prod1 = new Product2(
                name ='MI-test',
                SAP_Material_ID__c = 'MI-test'
                
            );
         insert prod1;
          Sellout__C so=new Sellout__C(Closing_Month_Year1__c='10/2018',Description__c='From Test Class',Subsidiary__c='SEA',Approval_Status__c='Draft',Integration_Status__c='Draft',Has_Attachement__c=true,Request_Approval__c=true);
         insert so;
         Date d=System.today();
         
        Sellout_SKU_Mapping__c skumap1=new Sellout_SKU_Mapping__c(Samsung_SKU__c='MI-test',Carrier_SKU__c='Test Product',Carrier__c='ATT');
        insert skumap1;
         
         Pagereference pf= Page.SelloutUploadfile;
           pf.getParameters().put('id',so.id);
           test.setCurrentPage(pf);
        
        apexPages.standardcontroller sc = new apexPages.standardcontroller(so);
          SelloutUploadfileController cls=new SelloutUploadfileController(sc);
          cls.fileName='Test.CSV';
          String blobCreator = '';
            blobCreator += 'Carrier,Samsung_SKU,Quantity,IL,SellOut_Date\n';
            blobCreator += 'ATT,SM-TEST1234,100,IL,10252018\n';
          
          cls.csvFileBody = blob.valueOf(blobCreator);
          cls.contentFile = blob.valueOf(blobCreator);
           cls.getselectOptions();
          cls.createdata();
         
          
          
          
          Test.StopTest();
	}
  private static testMethod void testmethod2() {
         Test.startTest();
          Product2 prod1 = new Product2(
                name ='MI-test',
                SAP_Material_ID__c = 'MI-test'
                
            );
         insert prod1;
          Id pln = TestDataUtility.retrieveRecordTypeId('Plan', 'Sellout__C');
          Sellout__C so=new Sellout__C(RecordtypeId=pln,Closing_Month_Year1__c='10/2018',Description__c='From Test Class',Subsidiary__c='SEA',Approval_Status__c='Draft',Integration_Status__c='Draft',Has_Attachement__c=true,Request_Approval__c=true);
         insert so;
         Date d=System.today();
         
        Sellout_SKU_Mapping__c skumap1=new Sellout_SKU_Mapping__c(Samsung_SKU__c='MI-test',Carrier_SKU__c='Test Product',Carrier__c='ATT');
        insert skumap1;
         
         Pagereference pf= Page.SelloutUploadfile;
           pf.getParameters().put('id',so.id);
           test.setCurrentPage(pf);
        
        apexPages.standardcontroller sc = new apexPages.standardcontroller(so);
          SelloutUploadfileController cls=new SelloutUploadfileController(sc);
          cls.fileName='Test.CSV';
          String blobCreator = '';
            blobCreator += 'Carrier,Samsung_SKU,Quantity,IL,SellOut_Date\n';
            blobCreator += 'ATT,SM-TEST1234,100,IL,10252018\n';
          
          cls.csvFileBody = blob.valueOf(blobCreator);
          cls.contentFile = blob.valueOf(blobCreator);
           cls.getselectOptions();
          cls.createdata();
         
          
          
          
          Test.StopTest();
	}
}