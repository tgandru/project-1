public class SendBulkEmailExt{
    public list<Account> Accinfo;
    public list<Contact> coninfo;
    public string ToEmails{get;set;}
    public string Subject{get;set;}
    public string body{get;set;}
    public string cc{get;set;}
    public string bcc{get;set;}
    public string AdditionalTo{get;set;}
    public string AdditionalCc{get;set;}
    public Document document {
    get {
      if (document == null)
        document = new Document();
        return document;
    }
    set;}
    public String EmailTempateId { public get; public set; }
    public OrgWideEmailAddress FromEmail{ public get; public set; }
    public EmailTemplate ET {get;set;}
    public id selectedFromEmail { public get; public set; }
    public List<SelectOption> FromEmails{get;set;}
    public String docId  {get; set;}
    public String htmlBody {get; set;}
    public string imageid;
    public string orgid;
    public Set<id> contactids;
    public Set<id> accids;

    public SendBulkEmailExt(){
        
        orgid=UserInfo.getOrganizationId();
        imageid = label.SVN_Email_DocumentId;
        
        accids = new Set<id>();
        contactids = new Set<id>();
        
        //for(AccountContactRole acr : [select id,contactid,accountid from AccountContactRole where role='Security Notification' and contact.Active__c=true]){
        for(AccountContactRelation acr : [select id,contactid,accountid from AccountContactRelation where roles='Security Notification' and contact.Active__c=true]){
            accids.add(acr.accountid);
            contactids.add(acr.contactid);
        }
        
        set<id> ToAccountIDs = new set<id>();
        for(Account acc : [select id,owner.email,Service_Account_Manager__c,Service_Account_Manager__r.email,(select id from Entitlements where asset.product2.SVN_Opt_Out__c=false),(select teammemberrole,user.email from Accountteammembers where TeamMemberRole='Technical Pre-sales') from Account where id=:accids]){
            //if(acc.entitlements.size()>0){//Commented--10/22/2018--As Biz want to add CCs irrespective of Account has Entitlements 
                ToAccountIDs.add(acc.id);
                if(cc==null){
                    cc = acc.owner.email;
                }else if(!cc.contains(acc.owner.email)){
                    cc = cc +';'+acc.owner.email;
                }
                if(acc.Service_Account_Manager__c!=null){
                    if(cc==null){
                        cc = acc.Service_Account_Manager__r.email;
                    }else if(!cc.contains(acc.Service_Account_Manager__r.email)){
                        cc = cc +';'+acc.Service_Account_Manager__r.email;
                    }
                }
                for(AccountTeamMember member : acc.AccountTeamMembers){
                    if(member.user.email!=null){
                        if(cc==null){
                            cc = member.user.email;
                        }else if(!cc.contains(member.user.email)){
                            cc = cc +';'+member.user.email;
                        }
                    }
                }
            //}
        }
        
        coninfo = [select id,email,accountid from contact where id=:contactids];
        for(Contact c : coninfo){
            if(c.email!=null){
                if(ToEmails==null){
                    ToEmails = c.email;
                }else if(!ToEmails.contains(c.email)){ 
                    ToEmails = ToEmails +';'+c.email;
                }
            }
        }
        
        FromEmails= new List<SelectOption>();
        for (OrgWideEmailAddress e : [select Id,address from OrgWideEmailAddress]) {
            FromEmails.add(new SelectOption(e.Id,e.address));
        }
        //FromEmails.add(UserInfo.getUserId(),UserInfo.getUserEmail());
        if(label.SVN_Email_Template!=null){
            ET = [Select Id,Subject,Body,HtmlValue,brandtemplateid from EmailTemplate where Id =:label.SVN_Email_Template];
            body = ET.body;
            htmlBody = ET.HtmlValue;
            Subject = ET.Subject;
        }
        String OrgURL = URL.getSalesforceBaseUrl().toExternalForm();
        system.debug(OrgURL);
        htmlBody = htmlBody.remove('![CDATA[<');
        htmlBody = htmlBody.remove(']]>');
        if(imageid.length()==15 || imageid.length()==18){
            htmlBody = '<img src="'+OrgURL+'/servlet/servlet.ImageServer?id='+imageid+'&oid='+orgid+'" alt="Samsung" height="100" width="543"/>'+htmlbody;
        }
    }
    
    public PageReference upload() {
        
        system.debug('Selected From Email------->'+selectedFromEmail);
        system.debug('Subject------>'+Subject);
        if(ToEmails==null && ToEmails==''){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'The Email is not sent. To address is blank.'));
            return null;
        }    
        try{
            BatchforSVNEmail ba = new BatchforSVNEmail(document,AdditionalTo,AdditionalCc,Subject,selectedFromEmail,htmlbody,accids);
            database.executeBatch(ba,1);
            
            /*Messaging.Email[] emailList = new Messaging.Email[0];
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            if(document.body!= null){
                efa.setFileName(document.name);
                efa.setBody(document.body);
            }
            for(Account acc : [select id,ownerid,owner.email,Service_Account_Manager__c,Service_Account_Manager__r.email,(select id from Entitlements where asset.product2.SVN_Opt_Out__c=false),(select teammemberrole,user.email,userid from Accountteammembers where TeamMemberRole='Technical Pre-sales'),(select id from contacts where id=:contactids) from Account where id=:accids]){
                set<string> ccids = new set<string>();
                if(AdditionalCc!=null && AdditionalCc!=''){
                    List<String> AddCC = AdditionalCc.split(';');
                    for(string s : addCC){
                        ccids.add(s);
                    }
                }
                if(acc.entitlements.size()>0){
                    ccids.add(acc.ownerid);
                    if(acc.Service_Account_Manager__c!=null){
                        ccids.add(acc.Service_Account_Manager__c);
                    }
                    for(AccountTeamMember member : acc.AccountTeamMembers){
                        ccids.add(member.userid);
                    }
                }
                set<string> ToContactids = new set<string>();
                if(AdditionalTo!=null && AdditionalTo!=''){
                    List<String> AddTo = AdditionalTo.split(';');
                    for(string s : addTo){
                        ToContactids.add(s);
                    }
                }
                for(contact con : acc.contacts){
                    ToContactids.add(con.id);
                }
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setOrgWideEmailAddressId(selectedFromEmail);
                //Adding contact ids and additionalto to ToAddress.
                List<string> ToAddresses = new List<string>();
                ToAddresses.addAll(ToContactids);
                mail.setToAddresses(ToAddresses);
                //Adding cc and additionalCc to ccAddress.
                if(ccids.size()>0){
                    List<String> ccAddresses = new list<string>();
                    ccAddresses.addAll(ccids);
                    mail.setCcAddresses(ccAddresses);
                }
                mail.setSaveAsActivity(true);
                mail.setSubject(Subject);
                mail.setHtmlBody(htmlBody);
                //mail.setPlainTextBody(body);
                mail.setUseSignature(false);
                if(document.body!= null){
                    mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
                }
                emailList.add(mail);
                //Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
                if(emailList.size()==100){
                    Messaging.sendEmail(emailList);
                    emailList.clear();
                }
            }
            system.debug('emailList size-------->'+emailList.size());
            if(emailList.size()>0){
                Messaging.sendEmail(emailList,false);
            }*/
            
        } catch (DMLException e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading file'));
            return null;
        } finally {
            document.body = null; // clears the viewstate
            document = new Document();
        }
        PageReference pageRef = new PageReference('/001/o');
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    Public Pagereference cancel(){
        PageReference pageRef = new PageReference('/001/o');
        pageRef.setRedirect(true);
        return pageRef;
    }
}