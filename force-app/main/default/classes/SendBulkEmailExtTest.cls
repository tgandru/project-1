@istest
private class SendBulkEmailExtTest {
    static Id AccRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('End Customer').getRecordTypeId();
    static Id ConRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('B2B Sales').getRecordTypeId();
    static Id AssetRecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('VAS Asset').getRecordTypeId();
    static Id ProdRecordTypeId = Schema.SObjectType.product2.getRecordTypeInfosByName().get('B2B').getRecordTypeId();
    @testSetup static void setup() {
        User u = [select id from user where name='Integration User'];
        system.runAs(u){
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Account acct=new Account(name='Test SVN',recordtypeid=AccRecordTypeId,type='Customer',ownerid=userinfo.getuserid(),Service_Account_Manager__c=userinfo.getuserid());
        insert acct;
        Account acct2=new Account(name='Test SVN2',recordtypeid=AccRecordTypeId,type='Customer',ownerid=userinfo.getuserid(),Service_Account_Manager__c=userinfo.getuserid());
        insert acct2;
        AccountTeamMember atm = new AccountTeamMember(AccountId=acct.id,UserId=userinfo.getuserid(),teammemberrole='Technical Pre-sales',AccountAccessLevel='Read');
        insert atm;
        Contact con = new contact(firstname='test',lastname='LNtest',email='test@gmail.example.com.test',accountid=acct2.id,recordtypeid=ConRecordTypeId,MailingCountry='US');
        insert con;
        //AccountContactRole acr = new AccountContactRole(accountid=acct.id,contactid=con.id,role='Security Notification');
        //insert acr;
        AccountContactRelation accConRelation = new AccountContactRelation(accountid=acct.id,contactid=con.id,roles='Security Notification');
        insert accConRelation;
        Product2 Prod=new product2(name='TestProduct',SAP_Material_ID__c='SV-SCDFER',SVN_Opt_Out__c=false,isactive=true);
        insert Prod;
        Asset ass = new Asset(name='Test Asset',recordtypeid=AssetRecordTypeId,accountid=acct.id,product2id=prod.id);
        insert ass;
        Entitlement Ent = new Entitlement(name='Test SVN Entitlement',accountid=acct.id,assetid=ass.id);
        insert Ent;
        }
    }
    @isTest static void test_method_One() {
        User u = [select id from user where name='Integration User'];
        system.runAs(u){
        SendBulkEmailExt send = new SendBulkEmailExt();
        send.AdditionalTo='test@test.com;test2@test.com';
        send.upload();
        send.cancel();
        }
    }
}