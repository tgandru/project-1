global class SendDatatoGERP {
    @AuraEnabled
     webservice static string CreateMQRecords(id soId){
         try{
             Sellout__c so=New Sellout__C();
             List<Sellout_Products__c> slo=new list<Sellout_Products__c>();
             if(soId != null){
                so=[Select id,name,Pre_check_Blank__c,Total_CL_Qty__c,Total_IL_Qty__c,Integration_Status__c from Sellout__c where id=:soId limit 1];
                slo=[Select id,Status__c from Sellout_Products__c where sellout__c=:soId and Status__c !='OK'];
                 if(so.Total_CL_Qty__c <=0 || so.Total_IL_Qty__c <=0){
                      return 'There are no Sellout Products to Send to GERP .';
                 }else if(slo.size()>0){
                     String err='There are '+slo.size() +'   records with Not OK Status, please fix the data before Sending to GERP.';
                      return err;
                 }else if(so.Pre_check_Blank__c >=1){
                      return 'Please Run Pre-check before Sending Data to GERP.';
                 }else{
                      List<message_queue__c> mqs=new list<message_queue__c>();
                         message_queue__c mq1=new message_queue__c(Identification_Text__c=so.id,
                                                                 Integration_Flow_Type__c='Flow-030',
                                                                 retry_counter__c=0,
                                                                 Object_Name__c='Sellout',
                                                                 Status__c='not started'
                                                                  );
                         message_queue__c mq2=new message_queue__c(Identification_Text__c=so.id,
                                                                 Integration_Flow_Type__c='Flow-031',
                                                                 retry_counter__c=0,
                                                                 Object_Name__c='Sellout',
                                                                 Status__c='not started'
                                                                  );        
                          mqs.add(mq1);
                          mqs.add(mq2);
                          Insert mqs;
                          
                          
                          so.Integration_Status__c='Pending';
                          Update so;
                          
                          
                          
                          return 'Sucess/ submitted the request to send data';
                          //return 'We are working on this.';
                     
                 }
                 
             }
             return'';
             
         }catch(exception ex){
             return 'Error occurred '+ex.getMessage();
         }
     }
}