@isTest
private class SendDatatoGERPTest {

	private static testMethod void testmethod1() {
          Test.startTest();
          Id pln = TestDataUtility.retrieveRecordTypeId('Plan', 'Sellout__C');
          Sellout__C so=new Sellout__C(RecordTypeid=pln,Closing_Month_Year1__c='09/2018',Description__c='From Test Class',Subsidiary__c='SEA',Approval_Status__c='Draft',Integration_Status__c='Draft',Has_Attachement__c=true,Request_Approval__c=true);
         insert so;
         Date d=System.today();
         RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];
        Product2 prod1 = new Product2(
                name ='SM-TEST1234',
                SAP_Material_ID__c = 'SM-TEST1234',
                RecordTypeId = productRT.id
            );

        insert prod1;
          List<Sellout_Products__c> slp=new list<Sellout_Products__c>();
          Sellout_Products__c sl0=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST1234',Quantity__c=100,Carrier__c='ATT',Carrier_SKU__c='Samsung Mobile',IL_CL__c='CL',Pre_Check_Result__c='OK');
          Sellout_Products__c sl1=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST1234',Quantity__c=100,Carrier__c='ATT',Carrier_SKU__c='Samsung Mobiles',IL_CL__c='IL',Pre_Check_Result__c='OK');
          slp.add(sl0);
          slp.add(sl1);
          insert slp;
         SendDatatoGERP.CreateMQRecords(so.id);
        Test.StopTest();
	}

}