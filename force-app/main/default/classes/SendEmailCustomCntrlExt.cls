public class SendEmailCustomCntrlExt{

    public string lookUp{get;set;}
    public list<conContact> contactList{get;set;}
    public boolean allbool{get;set;}
    //public string inputValue{get;set;}
    public boolean bool{get;set;}
    public set<id> contactids{get;set;}
    public id recordId{get;set;}
    public String searchString {get;set;}
    public List<Contact> AvailableContacts{get;set;}
    public List<conContact> SelectedContacts{get;set;}
    public string bcc{get;set;}
    public string cc{get;set;}
    public string AdditionalTo{get;set;}
    public string Subject{get;set;}
    public String htmlBody {get; set;}
    public string body{get;set;}
    public EmailTemplate ET {get;set;}
    public string TemplateId{get;set;}
    public Boolean hasTemplateId{get;set;}
    public string SelectedTemplateId{get;set;}
    public string RelatedToName{get;set;}
    public boolean ShowRemovePanel{get;set;}
    public boolean ShowAttachmentPanel{get;set;}
    public boolean NoSearchString{get;set;}
    public boolean NoRecordsFound{get;set;}
    public List<SelectOption> FromEmails{get;set;}
    public id selectedFromEmail { public get; public set; }
    public Document document {
    get {
      if (document == null)
        document = new Document();
        return document;
    }
    set;}
    
    //Code for Select Template
    private Map<Id, Folder> folderMap{get;set;}
    //List of string to store Alphabets character for Alphabetically searching
    public List<EmailingTemplate> emailingTemplates {get; set;} 
    //This is map to fetch all ids of email folders.
    private Set<Id> folderListIds = new Set<Id>(); 
    //List of all folders's ids
    private Set<Id> folderOtherIds = new Set<Id>();
    //To get Template's value        
    public String folderId {get; set;}
    //Flag to show popup in page
    public boolean ShowTemplatePopup{get;set;}
    
    public SendEmailCustomCntrlExt(){
        recordId = ApexPages.CurrentPage().getparameters().get('RelatedToID');
        TemplateId = ApexPages.CurrentPage().getparameters().get('TemplateId');
        if(recordId!=null){
            RelatedToName = recordId.getSObjectType().getDescribe().getName();
        }
        contactList = new list<conContact>();
        SelectedContacts = new list<conContact>();
        bool = false;
        contactids = new Set<Id>();
        lookUp ='';
        bcc = '';
        cc = '';
        AdditionalTo = '';
        Subject = '';
        hasTemplateId=False;
        ShowRemovePanel=False;
        ShowAttachmentPanel=False;
        NoSearchString = False;
        NoRecordsFound = False;
        /*if(TemplateId!=null&&TemplateId!=''){
            SelectedTemplateId = TemplateId;
            hasTemplateId=True;
            ET = [Select Id,Subject,Body,HtmlValue,brandtemplateid from EmailTemplate where Id =:SelectedTemplateId];
            body = ET.body;
            htmlBody = ET.HtmlValue;
            htmlBody = htmlBody.remove('![CDATA[<');
            htmlBody = htmlBody.remove(']]>');
            Subject = ET.Subject;
        }*/
        if(TemplateId!=null&&TemplateId!=''){
            SelectedTemplateId = TemplateId;
        }
        system.debug('Constructor Selected TemplateID----------->'+SelectedTemplateId);
        if(SelectedTemplateId!=null&&SelectedTemplateId!=''){ 
            hasTemplateId=True;
            ET = [Select Id,Subject,Body,HtmlValue,brandtemplateid from EmailTemplate where Id =:SelectedTemplateId];
            body = ET.body;
            htmlBody = ET.HtmlValue;
            if(htmlBody!=null && htmlBody!='' && htmlBody.contains('![CDATA[<')){
                htmlBody = htmlBody.remove('![CDATA[<');
            }
            if(htmlBody!=null && htmlBody!='' && htmlBody.contains(']]>')){
                htmlBody = htmlBody.remove('![CDATA[<');
            }
            //htmlBody = htmlBody.remove(']]>');
            Subject = ET.Subject;
        }
        
        //Code for Select Template
        emailingTemplates = new List<EmailingTemplate>();
        folderMap = new Map<Id, Folder>( [Select id , Name from Folder where Type = 'Email']);
        this.ETData();
        ShowTemplatePopup=False;
        
        FromEmails= new List<SelectOption>();
        for (OrgWideEmailAddress e : [select Id,address from OrgWideEmailAddress where address='b2bcxo@sea.samsung.com']) {
            FromEmails.add(new SelectOption(e.Id,e.address));
        }
        FromEmails.add(new SelectOption(UserInfo.getUserId(),UserInfo.getUserEmail()));
    }
    
    //Contact Wrapper
    public class conContact{
        public contact con{get;set;}
        public boolean check{get;set;}
        
        public conContact(contact c, boolean boo){
            con = c;
            check = boo;
        }
    }
    
    //Method to add selected Contacts
    public void inIt(){
        list<Contact> selectedContact = new list<Contact>();
        for(conContact conObj : contactList){
            if(conObj.check != false){
                system.debug('conObj.con:'+conObj.con);
                selectedContact.add(conObj.con);
                lookUp += conObj.con.name + '; ';
                system.debug('lookUp::'+lookUp);
                contactids.add(conObj.con.id);
                SelectedContacts.add(new conContact(conObj.con,conObj.check));
            }
        }
        bool = false;
    }
    
    //Method to display list of Contacts using Search String in lookup Popup
    public List<conContact> getSearchContacts(){
        contactList.clear();
        String qString = 'select id,name,email,accountid from contact';
        if(searchString!=null && searchString!=''){
            qString+= ' where Name like \'%' + searchString + '%\'';
            if(contactids.size()>0){
                String tempFilter = ' and Id not in (';
                for(Id i : contactids){
                    tempFilter+= '\'' + (String)i + '\',';
                }
                String extraFilter = tempFilter.substring(0,tempFilter.length()-1);
                extraFilter+= ')';
                qString+= extraFilter;
            }
            qString+= ' order by Name asc';
            qString+= ' limit 101';
            AvailableContacts = database.query(qString);
            for(Contact c : AvailableContacts){
                contactList.add(new conContact(c,allbool));
            }
            if(contactList.size()<=0){
                NoRecordsFound = True;
            }else{
                NoSearchString = false;
                NoRecordsFound = false;
            }
            return contactList;
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Enter any search string'));
            NoSearchString = True;
            return null;
        }
    }
    
    //Method to close Popup
    public void closePopup(){
        bool = false;
        ShowRemovePanel = false;
        ShowTemplatePopup = false;
        NoSearchString = false;
        NoRecordsFound = false;
    }
    
    //Method to show list of Contacts Popup for Lookup
    public void add(){
        bool = true;
    }
    
    //Method to show Contacts Remove Popup
    public void ContactRemovePanel(){
        ShowRemovePanel=True;
        for(conContact conObj : SelectedContacts){
            conObj.check=false;
        }
    }
    
    //Methos to Remove Contacts
    public void RemoveContacts(){
        List<conContact> ContactsToKeep = new List<conContact>();
        for(conContact conObj : SelectedContacts){
            if(conObj.check == False){
                ContactsToKeep.add(conObj);
            }
        }
        if(ContactsToKeep.size()>0){
            SelectedContacts.clear();
            contactids.clear();
            lookUp=null;
            for(conContact c : ContactsToKeep){
                c.check=true;
                SelectedContacts.add(c);
                contactids.add(c.con.id);
                if(lookUp==null)
                    lookUp = c.con.name + '; ';
                else
                    lookUp += c.con.name + '; ';
                system.debug('lookUp::'+lookUp);
            }
        }else{
            SelectedContacts.clear();
            contactids.clear();
            lookUp='';
        }
        ShowRemovePanel = False;
    }
    
    //Method to show list of Contacts Popup for Lookup
    public void ShowattPanel(){
        ShowAttachmentPanel = true;
    }
    
    public void CloseattPanel(){
        ShowAttachmentPanel = false;
    }
    
    public PageReference SendEmail(){
        if(SelectedContacts.size()<=0){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'The Email is not sent. To address is blank.'));
            return null;
        }
        try{
            List<String> AddToEmails = new List<String>();
            List<String> ccEmails = new List<String>();
            List<String> bccEmails = new List<String>();
            if(AdditionalTo!=null&&AdditionalTo!=''){
                AddToEmails = AdditionalTo.split(';');
            }
            if(AddToEmails.size()>100){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'The Email is not sent. AdditionalTo address limit is 100 emails'));
                return null;
            }
            if(cc!=null&&cc!=''){
                ccEmails = cc.split(';');
            }
            if(bcc!=null&&bcc!=''){
                bccEmails = bcc.split(';');
            }
            BatchforSendEmailCustom ba = new BatchforSendEmailCustom(contactids,AddToEmails,ccEmails,bccEmails,Subject,selectedFromEmail,htmlBody,recordId,SelectedTemplateId);
            Database.executeBatch(ba,1);
        } catch (DMLException e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading file'));
            return null;
        } finally {
            document.body = null; // clears the viewstate
            document = new Document();
        }
        PageReference pageRef = new PageReference('/'+recordId);
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    public PageReference Cancel(){
        PageReference pageRef = new PageReference('/'+recordId);
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    //Code for Select Template
    public void ETData() //Method to get Name TemplateType Description FolderId from EmailTemplate
    {
       for(EmailTemplate e : [select Name, TemplateType, Description, FolderId from EmailTemplate where IsActive=true Order by Name])
            {
                if(!folderListIds.contains(e.FolderId))
                folderListIds.add(e.FolderId);
                EmailingTemplate tempE = new EmailingTemplate();
                tempE.templateId = e.Id;
                tempE.Name = e.Name;
                tempE.isSelected = false;
                tempE.TemplateType = e.TemplateType;
                tempE.Description = e.Description;            
                emailingTemplates.add(tempE);
            }
            for(Folder f : folderMap.values())
                if(!folderListIds.contains(f.id))
                    folderListIds.add(f.Id);
    }
    public void createTempList() //Method to get all folder and email template in eamil setting
    {
        emailingTemplates = new List<EmailingTemplate>();
        List<EmailTemplate> etList = new List<EmailTemplate>();
        if('All' == folderId)
                etList = [select Name, TemplateType, Description, FolderId from EmailTemplate where FolderId IN :folderListIds AND IsActive=true Order by Name];
        else if('Others' == folderId)
                etList = [select Name, TemplateType, Description, FolderId from EmailTemplate where FolderId IN :folderOtherIds AND IsActive=true Order by Name];
        else
                etList = [select Name, TemplateType, Description, FolderId from EmailTemplate where FolderId = :folderId AND IsActive=true Order by Name];
        for(EmailTemplate e : etList)
        {
            EmailingTemplate tempE = new EmailingTemplate();
            tempE.templateId = e.Id;
            tempE.Name = e.Name;
            tempE.isSelected = false;
            tempE.TemplateType = e.TemplateType;
            tempE.Description = e.Description;
            emailingTemplates.add(tempE);
        }
    }
    
    public List<SelectOption> getItems() // show email template according to selaeted email template folder
    {
        List<SelectOption> options = new List<SelectOption>();
        folderOtherIds = new Set<Id>();
        options.add(new SelectOption('All', 'All'));
        for(String f : folderListIds)
        {
            if(folderMap.get(f) == null)
            {
                folderOtherIds.add(f);
            }
            else
                options.add(new SelectOption(folderMap.get(f).id, folderMap.get(f).Name));
        }
        if(folderMap.size() > 0)
        options.add(new SelectOption('Others', 'Others'));
        return options;
    }                                
                                
    class EmailingTemplate // inner class to get emailTemplates Properties
    {
        public Boolean isSelected {get; set;}
        public String templateId {get; set;}
        public String Name {get; set;}
        public String TemplateType {get; set;}
        public String Description {get; set;}
    }
    //Method to show Select Template Popup
    public void TemplatePopup(){
        ShowTemplatePopup = true;
    }
    public void TemplatePopupClose(){
        SelectedTemplateId = TemplateID;
        system.debug('Selected TemplateID---->'+SelectedTemplateId);
        hasTemplateId = True;
        ET = [Select Id,Subject,Body,HtmlValue,brandtemplateid from EmailTemplate where Id =:SelectedTemplateId];
        body = ET.body;
        htmlBody = ET.HtmlValue;
        //htmlBody = htmlBody.remove('![CDATA[<');
        //htmlBody = htmlBody.remove(']]>');
        if(htmlBody!=null && htmlBody!='' && htmlBody.contains('![CDATA[<')){
            htmlBody = htmlBody.remove('![CDATA[<');
        }
        if(htmlBody!=null && htmlBody!='' && htmlBody.contains(']]>')){
            htmlBody = htmlBody.remove('![CDATA[<');
        }
        Subject = ET.Subject;
        ShowTemplatePopup = False;
    }
}