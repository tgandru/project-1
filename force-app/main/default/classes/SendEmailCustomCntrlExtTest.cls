@istest(seealldata=true)
public class SendEmailCustomCntrlExtTest {
    public static testmethod void test1(){
        EmailTemplate ET = [Select Id from EmailTemplate where templatetype='html' limit 1];
        Asset ast = [select id from Asset limit 1];
        Pagereference pg = page.SendEmailCustom;
        ApexPages.currentPage().getparameters().put('RelatedToID',ast.id);
        ApexPages.currentPage().getparameters().put('TemplateId',ET.id);
        SendEmailCustomCntrlExt sndEmail = new SendEmailCustomCntrlExt();
        sndEmail.searchString = 'Testing SVN';
        sndEmail.add();
        sndEmail.getSearchContacts();
        sndEmail.inIt();
        sndEmail.getSearchContacts();
        sndEmail.closePopup();
        sndEmail.ContactRemovePanel();
        sndEmail.RemoveContacts();
        sndEmail.Cancel();
        sndEmail.TemplatePopup();
        sndEmail.folderId='All';
        sndEmail.createTempList();
        sndEmail.getItems();
        sndEmail.TemplatePopupClose();
        sndEmail.ShowattPanel();
        sndEmail.CloseattPanel();
        sndEmail.AdditionalTo='Test@test.com;Test1@test1.com';
        sndEmail.cc='Test3@test.com;Test13@test1.com';
        sndEmail.bcc='Test4@test.com;Test14@test1.com';
        sndEmail.SendEmail();
    }
}