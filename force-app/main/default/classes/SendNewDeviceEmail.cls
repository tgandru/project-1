public with sharing class SendNewDeviceEmail {
	public void EmailNewDeviceList() {

		 List<Messaging.SingleEmailMessage> lstMails = new List<Messaging.SingleEmailMessage>();

		String device_str = null;
		String to_email = null;
		String accountName = null;
		String samName = null;
		Integer total = 0;
		Integer device_cnt = 0;
		String deviceURL = System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+
			Device__c.sobjecttype.getDescribe().getKeyPrefix()+'/o';
		


		AggregateResult[] groupedResults = [select  
			account__r.Service_Account_Manager__r.name sam_name,
			account__r.Service_Account_Manager__r.email sam_email,
			account__r.name acct_name, status__c, count(id) device_cnt	
			from device__c  where status__c IN ('Invalid','Validated') 
			and createddate = today 
			and account__r.Service_Account_Manager__r.email !=null 
			group by account__r.Service_Account_Manager__r.name,	account__r.Service_Account_Manager__r.email,
			account__r.name , status__c order by account__r.Service_Account_Manager__r.email,account__r.name,status__c];

		for (AggregateResult ar : groupedResults)  {
			object emailObj = ar.get('sam_email');
			if (to_email != (String)emailObj){
				if (to_email != null){
					device_str +=total + ' - Total Uploaded\n\n';
					device_str +='Please click the link below to view the devices for your review & approval.\n';
					device_str += deviceURL;
					total = 0;

					//send email
					system.debug(to_email +'----'+ device_str);
					Messaging.SingleEmailMessage email = getEmail();       
					email.setSubject('New Devices Added Today');
					String[] toAddresses = new list<string> {to_email};
					email.setToAddresses(toAddresses);
					email.setHtmlBody(device_str);
				    lstMails.add(email);
				}
				
				to_email = (String)emailObj;
				accountName = (String)ar.get('acct_name');
				samName = (String)ar.get('sam_name');
				device_cnt = (Integer)ar.get('device_cnt');
				device_str = 'Dear ' + samName +',\n\nNew devices have been uploaded to your account today.\n\n';
				device_str +='Account: ' + accountName+'\n';
				device_str +=device_cnt + ' - ' +(String)ar.get('status__c') +'\n';
				total = total + device_cnt;
				
			}else{
				if (accountName != (String)ar.get('acct_name') ){
					device_str +=total + ' - Total Uploaded\n\n';
					total = 0;
					accountName = (String)ar.get('acct_name');
					device_cnt = (Integer)ar.get('device_cnt');
					device_str +='Account: ' + accountName+'\n';
					device_str +=device_cnt + ' - ' +(String)ar.get('status__c') +'\n';
					total = total + device_cnt;
				}
				else{
					device_cnt = (Integer)ar.get('device_cnt');
					device_str +=device_cnt + ' - ' +(String)ar.get('status__c') +'\n';
					total = total + device_cnt;
				}
			}
		}
		if (to_email != null){
			device_str +=total + ' - Total Uploaded\n\n';
			device_str +='Please click the link below to view the devices for your review & approval.\n';
			device_str += deviceURL;
			system.debug(to_email +'----'+ device_str);
			Messaging.SingleEmailMessage email = getEmail();       
			email.setSubject('New Devices Added Today');
			String[] toAddresses = new list<string> {to_email};
			email.setToAddresses(toAddresses);
			email.setPlainTextBody(device_str);
			lstMails.add(email);
		}
		if (lstMails.size()>0){
			Messaging.sendEmail(lstMails);
		}	
	}

	public static Messaging.SingleEmailMessage getEmail() {

    	Messaging.SingleEmailMessage mailMessage = new Messaging.SingleEmailMessage();

    	mailMessage.setSaveAsActivity(false);

    	return mailMessage;

  }
 
}