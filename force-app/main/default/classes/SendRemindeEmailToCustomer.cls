public class SendRemindeEmailToCustomer {
 @InvocableMethod
    public static void SendEmails(List<Id> Entids) 
    {
        List<Entitlement> Entlst=new list<Entitlement>([Select id,Name,EndDate,Account.Id,Account.Service_Account_Manager__c,Services_Sales_Technician__c,Services_Sales_Technician__r.Name,Services_Sales_Technician__r.Email,Account.Service_Account_Manager__r.email,Account.Service_Account_Manager__r.name,Contact__c,Contact__r.Email,Contact__r.Name from Entitlement where id in:Entids]);
        if(Entlst.size()>0){
            Set<string> accids=new set<string>();
            for(Entitlement e:Entlst){
               accids.add(e.Account.ID);
            }
           /* List<String> teamroles=label.Account_Team_Roles_Service.split(',');
            List<AccountTeamMember> ateam =new list<AccountTeamMember>([SELECT AccountId,TeamMemberRole,UserId,User.Name,user.Email FROM AccountTeamMember where AccountID in:accids and TeamMemberRole in:teamroles]);
            Map<string,string> accteammap=new Map<string,string>();
            for(AccountTeamMember at:ateam){
                accteammap.put(at.Accountid,at.user.Name);
            }*/
               List<Messaging.SingleEmailMessage> emails=new List<Messaging.SingleEmailMessage>();
               OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where DisplayName = 'SBS Support'];
            for(Entitlement e:Entlst){
                if(e.Contact__c != null){
                    String sam=e.Account.Service_Account_Manager__r.name;
                    String tech=e.Services_Sales_Technician__r.Name;
                    
                    Date d=e.enddate;
                    String dt=String.valueOf(d.month())+'/'+String.valueOf(d.day())+'/'+String.valueOf(d.year());
                   
                    String contct=e.Contact__r.Name;
                    
                    String body=emailbody(contct,sam,tech,dt,e.Name);
                    String sub=  'Entitlement  '+e.Name+'  will Expire on  '+dt;
                    
                      String[] toAddresses = new String[] {e.Contact__r.Email};
                      list<String> ccaddress=new list<string>();
                      ccaddress.add(e.Account.Service_Account_Manager__r.email);
                      ccaddress.add(e.Services_Sales_Technician__r.email);
                      
                       Messaging.SingleEmailMessage  email = new Messaging.SingleEmailMessage();
                        email.setToAddresses(toAddresses);
                        email.setCCAddresses(ccaddress);
                       if ( owea.size() > 0 ) {
                                email.setOrgWideEmailAddressId(owea.get(0).Id);
                            }
                       email.setTargetObjectId(e.Contact__c);
                       email.setSubject(sub);
                       email.setHtmlBody(body);
                       email.setSaveAsActivity(true);
                       emails.add(email);    
                    
                }
            }
            
            
            if(emails.size()>0){
                 Messaging.SendEmail(emails,false); 
                
            }
            
        }
        
    }
    
    
    
    public Static String emailbody(String cont,String sam, String techn,string dt,String EntName){
         string bdy = 'Dear  '+cont+',';
          bdy = bdy + '\n \n <br/>';
          
          if(EntName.contains('Advanced Exchange')){
              bdy = bdy + '\n \n <br/>'+'This is an automatic reminder from Samsung ProCare that your Advanced Support contract will expire on '+'<b>'+dt+'</b>';
              if(techn !=null && techn !=''){
               bdy=bdy+'\t'+'. Please contact your Services Sales Technician '+'<b>'+techn+'</b>';
               bdy=bdy+'\t  if you have questions about your renewal status.';
              }
            
          }else{
             bdy = bdy + '\n \n <br/>'+'This is an automatic reminder from Samsung ProCare that your Elite Support contract will expire on '+'<b>'+dt+'</b>';
             bdy = bdy + '\t'+'.Please contact your Support Account Manager '+'<b>'+sam+'</b>';
             
                 if(techn !=null && techn !=''){
                   bdy=bdy+'\t'+', or Services Sales Technician '+'<b>'+techn+'</b>';
                  }
            bdy=bdy+'\t  if you have questions about your renewal status.';
          }
          
           
            
            bdy = bdy + '\n \n <br/>';
            bdy = bdy + '\n \n <br/>';
            
         bdy = bdy + '\n \n'+ '<br/><br/><br/><b>Thank you!</b><br/>';
         bdy = bdy + '<b>Samsung Business Service</b>';
         
         return bdy;  
    }
}