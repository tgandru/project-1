public class SiteSurveyOpportunityExt {
    public Site_Survey_Opportunity__c ssopp{get;set;}
    public id ssid;
    public Site_Survey__c ss{get;set;}

    public SiteSurveyOpportunityExt(ApexPages.StandardController controller) {
        ssid = apexpages.currentpage().getparameters().get('CF00N3600000RRq3z_lkid');
        system.debug('id------>'+ssid);
        ssopp = new Site_Survey_Opportunity__c();
        ssopp.Site_Survey__c = ssid;
        ss = [select id,name from Site_Survey__c  where id =:ssid];
    }
    public pagereference OnSave(){
        /*if(ssopp.Opportunity__c==null || ssopp.Opportunity__c==''){
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'You must select the Opportunity'));
        }*/
        //Else{
            insert ssopp;
            pagereference pg = new pagereference('/'+ssid);
            pg.setredirect(true);
            return pg;    
        //}
        //return null;
    }
}