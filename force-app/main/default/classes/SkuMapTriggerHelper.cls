public class SkuMapTriggerHelper {
   
   Public static void checkskuisavibility(list<Sellout_SKU_Mapping__c> skus){
       Set<string> skuid= new set<string>();
        map<string,string> pdmap=new  map<string,string>();
        string pid='';
        for(Sellout_SKU_Mapping__c s:skus){
            
           skuid.add(s.Samsung_SKU__c); 
        }
        if(skuid.size()>0){
            for(Product2 p:[select id,SAP_Material_ID__c from Product2 where SAP_Material_ID__c  in :skuid]){
                pdmap.put(p.SAP_Material_ID__c,p.id);
            }
        }
        
        for(Sellout_SKU_Mapping__c s:skus){
            
           pid =pdmap.get(s.Samsung_SKU__c);
            system.debug('====='+pid);
            if(pid==null || pid==''){
                
                s.Adderror(s.Samsung_SKU__c+'   does not exist in Product Master ');
            }
            
            
        }
       
   }
   
}