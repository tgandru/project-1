@RestResource(urlMapping='/SEASFDCStatus/*')
global with sharing class SystemFailuerMonitoring {
   
   @Httpget
    global static response getStatusResponse() {
         response rtnresp=new response();
         rtnresp.retcode='0';
         rtnresp.message='System Status is Active';
         String endpoint=Label.SFDC_Trust_Status;
         System.Httprequest req = new System.Httprequest(); 
        HttpResponse res = new HttpResponse();
        req.setMethod('GET');
        req.setEndpoint(endpoint);// 'https://api.status.salesforce.com/v1/instances/NA81/status'
        
        req.setTimeout(120000);
         req.setHeader('Content-Type', 'application/json');
        req.setHeader('Accept', 'application/json');
    
        Http http = new Http();  
        String response;
        if(!test.isRunningTest()){
             res = http.send(req);
              response=res.getBody();
        }else{
            response='{"key":"NA81","location":"NA","environment":"production","releaseVersion":"Summer 19 Patch 20","releaseNumber":"220.20","status":"OK","isActive":true}';
        }
         
            if(string.isNotBlank(response)){ 
                SFDCStatusResponse re= (SFDCStatusResponse) json.deserialize(response, SFDCStatusResponse.class);
                if(re.isActive){
                    rtnresp.retcode='0';
                    rtnresp.message='System Status is Active'; 
                }else{
                    rtnresp.retcode='1';
                    rtnresp.message='Please Contact SFDC Admin'; 
                }
            }
         
         
         
         
         return rtnresp;
           
    }
    
    global class response{
        Public String retcode;
        Public String message;
    }
    
    
    global class SFDCStatusResponse{
        public String status;
        public boolean isActive;
    }
}