@isTest
private class SystemFailuerMonitoring_Test {

	private static testMethod void test() {
         Test.startTest();
        
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();
             
        req1.requestURI = '/services/apexrest/SEASFDCStatus/xxxxxx';  //Request URL
        req1.httpMethod = 'GET';//HTTP Request Type
       
        RestContext.request = req1;
        RestContext.response= res1;
        
        SystemFailuerMonitoring.response resData1 = SystemFailuerMonitoring.getStatusResponse();
        
        System.assertEquals('0', resData1.retcode);
        
        Test.stopTest();
	}

}