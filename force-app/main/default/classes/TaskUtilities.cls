/*
 * Description: This Apex Class is used to set the Quote Status
 * From Approved to Presented when a Quote is E-mailed.
 * 
 * Created By: Mayank S
 *
 * Created Date: Mar 08, 2016
 * 
 * Revisions: NIL
*/

public class TaskUtilities {
   public static Boolean isFirstTime = true;
    public static void changeQuoteStatusOnSendEmail(List<Task> newEmails) {
        List<String> newEmailIds=new List<String>();
          List<String> OppIds=new List<String>();
        for(Task t:newEmails){
            if(t.WhatID!=null ){
                       String s=String.valueOf(t.WhatID);
                    if(s.startsWith('0Q0')){
                        newEmailIds.add(t.Id);
                    }
                    
            }
           
        }
           
           if(newEmailIds.size()>0){
                 changeQuoteStatusOnSendEmailFut(newEmailIds); 
           }
          
        
    }
    
    
    public static void UpdateOpportunityUpdateDate(List<Task> newEmails){
                  List<String> OppIds=new List<String>();

         for(Task t:newEmails){
            if(t.WhatID!=null ){
                       String s=String.valueOf(t.WhatID);
                
                    if(s.startsWith('006') && (t.Activity_Type__c=='Meeting' || t.Activity_Type__c =='Call')){
                        OppIds.add(t.WhatID);
                    }
              
            }
           
        }
           
           
           if(OppIds.size()>0){
                 UpdateOppLastUpdateValue(OppIds); 
           }
    }
    
    
     @future 
    Public Static void UpdateOppLastUpdateValue(List<string> oppids){
        List<Opportunity> Opp=new List<Opportunity>([Select id,Name,Last_Modified_Date_Time__c,last_updated_Date__c from Opportunity where id in:oppids]);
        for(Opportunity o:opp){
            o.last_updated_Date__c=System.now();
        }
        
        if(Opp.size()>0){
            update opp;
        }
    }
    

    @future 
    public static void changeQuoteStatusOnSendEmailFut(List<String> newEmailIds){

        List<Task> newEmails=[select Id, LastModifiedDate,TaskSubtype, Status, WhatId from Task where Id in :newEmailIds];

        List<Quote> quotesToBeUpdated = new List<Quote>();
        Set<Id> quoteIds = new Set<Id>();
        for(Task t : newEmails) {
            if(t.TaskSubtype == 'Email' && t.Status == 'Completed') {
                quoteIds.add(t.WhatId);
            }
        }
        
        quotesToBeUpdated = [Select Id, Status,Quote_Email_Sent_Date__c from Quote where Id in :quoteIds ];
        
        for(Quote q : quotesToBeUpdated) {
            if(q.Status == 'Approved'){
                q.Status = 'Presented';
            }
            if(q.Quote_Email_Sent_Date__c==null){
            q.Quote_Email_Sent_Date__c=system.now();}

        }
        
        update quotesToBeUpdated;
    }

    public static void svc_CompleteMileStones(List<Task> newTasks) {
        DateTime completionDate = System.now();
        Map<ID, ID> CaseWhoMap = new Map<ID,ID>();

        for (Task t:newTasks){
            string s=string.valueOf(t.whatid);
            if(s !=null && s !=''){
               if (t.TaskSubtype != 'Email' && s.startsWith('500') ){ //email is handled in EmailMessage trigger
               
                CaseWhoMap.put(t.whatid, t.whoid);
            } 
            }
            
        }
        if (CaseWhoMap.size()>0){
            Set <Id> CaseIds = new Set<Id>();
            CaseIds = CaseWhoMap.keySet();
            List<Case> caseList = [Select c.Id, c.ContactId, c.Contact.Email,
                                  c.OwnerId, c.Status,
                                  c.EntitlementId,
                                  c.SlaStartDate, c.SlaExitDate
                               From Case c where c.Id IN:CaseIds];
                if (caseList.isEmpty()==false){
                    List<Id> updateCases = new List<Id>();
                    for (Case caseObj:caseList) {
                        // consider an outbound email to the contact on the case a valid first response
                        if ((CaseWhoMap.get(caseObj.Id)==caseObj.ContactId)&&
                           (caseObj.EntitlementId!= null)&&
                           (caseObj.SlaStartDate <= completionDate)&&
                           (caseObj.SlaStartDate!= null)&&
                           (caseObj.SlaExitDate == null))
                            updateCases.add(caseObj.Id);
                    }
                    if(updateCases.isEmpty() == false)
                        SVC_milestoneUtils.completeMilestone(updateCases, 'First Response Time', completionDate);
                        SVC_milestoneUtils.completeMilestone(updateCases, 'Status Update', completionDate);
                }

        }
    }
    
    
    //Added by Vijay for M* Inquiry Process- Sharing the Activities to Inquiry records -4/17/2018
    public static void copyactivitytoinquiry(list<task> tk){
        map<string,Inquiry__c> leadmap= new map<string,Inquiry__c>();
        map<string,Inquiry__c> contactmap= new map<string,Inquiry__c>();
         map<string,Inquiry__c> inqmap= new map<string,Inquiry__c>();
         set<id> leadid=new set<id>();
         set<id> contactid=new set<id>();
         set<id> WhoIDs=new set<id>();
        list<Inquiry__c> inq=new list<Inquiry__c>();
        for(task t:tk){
            String s=String.valueOf(t.WhoID);
            if(s !=null && s !=''){
                if(s.startsWith('00Q')){//Adding Task List to Lead Map
                    WhoIDs.add(t.whoID);
                }else if(s.startsWith('003') && t.WhatID==null){//Adding Task list to Contact Map
                    WhoIDs.add(t.whoID);
                }
            }
        }
        
        if(WhoIDs.size()>0){
            for(Inquiry__c i:[select id,name,lead__C,contact__c from inquiry__c where (contact__c in :WhoIDs OR lead__c in :WhoIDs ) and IsActive__c = true limit 1]){//Getting Active Inquiry records relates to contact 
                 if(i.lead__c !=null && i.Contact__c==null){
                     inqmap.put(i.lead__c,i);
                 }else{
                     inqmap.put(i.contact__c,i);
                 }
            }
        }
        if(inqmap.size()>0){
           for(task t:tk){
              //Share Task Record with inquiry , filling the Inquiry__c value
              String s=String.valueOf(t.WhoID);
                if(s !=null && s !=''){
                      try{
                           t.Inquiry__c=inqmap.get(t.WhoID).id;
                      }catch(exception ex){
                          system.debug('No Active Inqurires Present');
                      }
                  }
              
          } 
        }
     }
    
    public static void CopyTypeFromActivityType(List<Task> nwtsk,Map<id,Task> oldMap){
      for(Task t:nwtsk){
            If(t.Type==null && t.Activity_Type__c !=null){
                t.Type=t.Activity_Type__c;
            }else if(t.Type!=null && t.Activity_Type__c ==null){
                t.Activity_Type__c=t.Type;
            }
            
            try{
                Task oldtsk=oldMap.get(t.Id);
                If(t.Type==oldtsk.Type && t.Activity_Type__c !=oldtsk.Activity_Type__c){
                    t.type=t.Activity_Type__c;
                }else if(t.Type!=oldtsk.Type && t.Activity_Type__c ==oldtsk.Activity_Type__c){
                     t.Activity_Type__c=t.Type;
                }
                
            }catch(Exception ex){
                
            }
            
        }
    }
}