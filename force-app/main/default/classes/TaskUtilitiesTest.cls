@isTest
private class TaskUtilitiesTest {
    static Account testMainAccount;
    static Account testCustomerAccount;
    static List<Opportunity> testOpportunityList;
    static Opportunity oppOpen;
    static Pricebook2 pb;
    static PricebookEntry standaloneProdPBE;
    static Quote quot;
    static Contact contactRecord;

    static Id endUserRT = TestDataUtility.retrieveRecordTypeId('End_User', 'Account'); 
    static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
    static Id demoAcctRtId = TestDataUtility.retrieveRecordTypeId('ADL','Order');
    static Id demoOppRtId1 = TestDataUtility.retrieveRecordTypeId('Evaluation','Order');
    
    private static void setup() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Test.loadData(fiscalCalendar__c.sObjectType, 'FiscalCalendarTestData');
        
        //Channelinsight configuration
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();

        Zipcode_Lookup__c zip=TestDataUtility.createZipcode();
        insert zip;
        
        //Creating test Account
        testMainAccount = TestDataUtility.createAccount(TestDataUtility.generateRandomString(5));
        testMainAccount.RecordTypeId = endUserRT;
        insert testMainAccount;
        
        testCustomerAccount = TestDataUtility.createAccount(TestDataUtility.generateRandomString(5));
        testCustomerAccount.RecordTypeId = directRT;
        insert testCustomerAccount;
        
        //Creating custom Pricebook
        pb = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(4)); 
        insert pb;
        
        testOpportunityList = new List<Opportunity>();
            oppOpen=TestDataUtility.createOppty('Open Oppty',testMainAccount, pb, 'RFP', 'IT', 'FAX/MFP', 'Identified');
            testOpportunityList.add(oppOpen);
            Opportunity noPB = TestDataUtility.createOppty('NoPriceBook', testCustomerAccount, null, 'RFP', 'IT', 'FAX/MFP', 'Identified');
            testOpportunityList.add(noPB);
        insert testOpportunityList;
        
        //Creating Products
        List<Product2> prods=new List<Product2>();
            Product2 standaloneProd=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Standalone Prod1', 'Printer[C2]', 'OFFICE', 'H/W');
            prods.add(standaloneProd);
            Product2 parentProd1=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Parent Prod1', 'Printer[C2]', 'OFFICE', 'H/W');
            prods.add(parentProd1);
            Product2 childProd1=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod1', 'Printer[C2]', 'SMART_PHONE', 'Service pack');
            prods.add(childProd1);
            Product2 childProd2=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod2', 'Printer[C2]', 'SMART_PHONE', 'Service pack');
            prods.add(childProd2);
            Product2 parentProd2=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Parent Prod2', 'Printer[C2]', 'FAX/MFP', 'H/W');
            prods.add(parentProd2);
            Product2 childProd3=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod3', 'Printer[C2]', 'PRINTER', 'Service pack');
            prods.add(childProd3);
            Product2 childProd4=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod4', 'Printer[C2]', 'ACCESSARY', 'Service pack');
            prods.add(childProd4);
        insert prods;
        
        List<PriceBookEntry> pbes=new List<PriceBookEntry>();
            standaloneProdPBE=TestDataUtility.createPriceBookEntry(standaloneProd.Id, pb.Id);
            standaloneProdPBE.Product2 = standaloneProd;
            standaloneProdPBE.UnitPrice = 100;
            pbes.add(standaloneProdPBE);
            PriceBookEntry parentProd1PBE=TestDataUtility.createPriceBookEntry(parentProd1.Id,pb.Id);
            parentProd1PBE.Product2 = parentProd1;
            parentProd1PBE.UnitPrice = 120;
            pbes.add(parentProd1PBE);
            PriceBookEntry childProd1PBE=TestDataUtility.createPriceBookEntry(childProd1.Id, pb.Id);
            childProd1PBE.Product2 = childProd1;
            pbes.add(childProd1PBE);
            PriceBookEntry childProd2PBE=TestDataUtility.createPriceBookEntry(childProd2.Id, pb.Id);
            childProd2PBE.Product2 = childProd2;
            pbes.add(childProd2PBE);
            PriceBookEntry parentProd2PBE=TestDataUtility.createPriceBookEntry(parentProd2.Id, pb.Id);
            parentProd2PBE.Product2 = parentProd2;
            pbes.add(parentProd2PBE);
        insert pbes;

        List<OpportunityLineItem> opportunityLineItems = new List<OpportunityLineItem>();
        OpportunityLineItem op1 = TestDataUtility.createOpptyLineItem(standaloneProd, standaloneProdPBE, oppOpen);
        op1.Quantity = 100;
        op1.TotalPrice = 1000;
        op1.Requested_Price__c = 100;
        op1.Claimed_Quantity__c = 45;     
        opportunityLineItems.add(op1);
        insert opportunityLineItems;
        
        contactRecord = TestDataUtility.createContact(testMainAccount);
        insert contactRecord;
        
        quot = TestDataUtility.createQuote('Test Quote', oppOpen, pb);
        quot.ROI_Requestor_Id__c=UserInfo.getUserId();
        quot.Status = 'Approved';
        insert quot;

    }
        @isTest static void test_method_one() {
            Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
            setup();
            Task t = new Task();
            t.Subject = quot.Name + ' - Task';
            t.WhatId = quot.Id;
            t.WhoId = contactRecord.Id;//accContacts.get(quo.AccountId).Id;
            t.TaskSubtype = 'Email';
            t.Status = 'Completed';
            t.Type='Other';
            
            Test.startTest();
                insert t;
            Test.stopTest();
            Quote quotRec = [Select Id, Status from Quote where Id =:quot.Id limit 1];
                System.assertEquals('Presented', quotRec.Status);
        }

        @isTest static void svc_testTaskUtilities() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Profile adminProfile = [SELECT Id FROM Profile Where Name ='B2B Service System Administrator'];

        User admin1 = new User (
            FirstName = 'admin1',
            LastName = 'admin1',
            Email = 'admin1seasde@example.com',
            Alias = 'admint1',
            Username = 'admin1seaa@example.com',
            ProfileId = adminProfile.Id,
            TimeZoneSidKey = 'America/New_York',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US'
        );
        
        insert admin1;

        BusinessHours bh24 = [SELECT Id FROM BusinessHours WHERE name = 'B2B Service Hours 24x7'];
        BusinessHours bh12 = [SELECT Id FROM BusinessHours WHERE name = 'B2B Service Hours 12x5'];
    
        RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
        Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
        );

        insert testAccount1;

        Account testAccount2 = new Account (
            Name = 'TestAcc2',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            
        );

        insert testAccount2;

        RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];
        Product2 prod1 = new Product2(
                name ='MI-test',
                SAP_Material_ID__c = 'MI-test',
                sc_service_type__c = 'Incident',
                sc_service_type_ranking__c = '6',
                RecordTypeId = productRT.id
            );

        insert prod1;

        Asset ast = new Asset(
            name ='Assert-MI-Test',
            Accountid = testAccount1.id,
            Product2Id = prod1.id
            );
        insert ast;

        Asset ast2 = new Asset(
            name ='Assert-MI-Test2',
            Accountid = testAccount2.id,
            Product2Id = prod1.id
            );
        insert ast2;

        Slaprocess sla = [select id from SlaProcess where name like 'Enhanc%' 
            and isActive = true limit 1 ];

        Entitlement ent = new Entitlement(
            name ='test Entitlement1',
            accountid=testAccount1.id,
            assetid = ast.id,
            BusinessHoursId = bh12.id,
            startdate = system.today()-1,
            slaprocessid = sla.id
            );
        insert ent;

        Entitlement ent2 = new Entitlement(
            name ='test Entitlement2',
            accountid=testAccount2.id,
            assetid = ast2.id,
            BusinessHoursId = bh12.id,
            startdate = system.today()-1
            );
        //insert ent2;
        
        Contact contact = new Contact(); 
        contact.AccountId = testAccount1.id; 
        contact.Email = 'svcTest@svctest.com'; 
        contact.LastName = 'Named Caller'; 
        contact.FirstName = 'Caller';
        contact.Named_Caller__c = true;
        contact.MailingState='NJ';
        contact.MailingCountry='US';
        insert contact; 

        String entlId;
        if (ent != null)
        entlId = ent.Id; 

        Test.startTest();
        Group q= [select id from group where type='Queue' limit 1 ];
        List<Case> cases = new List<Case>{};
        if (entlId != null){
            Case c1 = new Case(Subject = 'Test Case with Entitlement ', 
            EntitlementId = entlId, ContactId = contact.id,
            origin = 'Phone',
            ownerid=admin1.id);
            
            insert c1;

            Case c2 = new Case(Subject = 'Test Case with Entitlement ', 
            EntitlementId = entlId, ContactId = contact.id,
            origin = 'Web',
            ownerid=q.id);
            
            insert c2;

            Task tsk = new Task(
                Subject = 'Hello World',
                Status = 'Completed',
                Priority = 'Normal',
                WhatId = c2.Id,
                whoid = contact.id);
            insert tsk;

            Task tsk2 = new Task(
                Subject = 'Hello World',
                Status = 'Completed',
                Priority = 'Normal'
                //whoid = c2.Id
                );
            insert tsk2;
            tsk2.whoid=contact.id;
            update tsk2;

            
        }
        Test.stopTest();
        
    }
 }