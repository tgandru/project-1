public class TempSelloutSendtoGERPController {
  Public string QtyRolloutMonth {get; set;}
  Public string QtyApplyMonth {get; set;}
  Public string AccountApplyMonth {get; set;}
  Public string QtyRolloutMonthplan {get; set;}
  Public string QtyApplyMonthplan {get; set;}
  Public string AccountApplyMonthplan {get; set;}
  Public Boolean senddata {get; set;}
  Public List<Sellout_Products__c> sols {get; set;}
  Public Sellout__c so {get; set;}
    public TempSelloutSendtoGERPController(ApexPages.StandardController controller) {
       sols=new list<Sellout_Products__c>();
       so=(Sellout__c) controller.getRecord();
       if(so.id !=null){
                   so=[select RecordTypeId,RecordType.Name,id,name,Approval_Status__c,Closing_Month_Year1__c,Description__c,Has_Attachement__c,OwnerId ,Integration_Status__c ,Total_CL_Qty__c,Total_IL_Qty__c,Subsidiary__c from Sellout__c where id=:controller.getRecord().Id limit 1];

         sols =[Select id,name,Carrier__c,Sold_To_Code__c,Carrier_SKU__c,IL_CL__c,Quantity__c,Samsung_SKU__c,Sellout_Date__c,Sellout__r.Subsidiary__c,Sellout__r.Version__c,Sellout__r.Closing_Month_Year1__c,Sellout__r.Description__c,Sellout__r.Name,Sellout__r.RecordType.Name from Sellout_Products__c where Status__c ='OK' and Sellout__c=:so.id order by Sellout_Date__c asc];
       
       }
    }
    
    Public PageReference checkvalues(){
    if(so.Recordtype.Name=='Actual'){
       QtyRolloutMonth =QtyRolloutMonth.replace('-','');
     QtyApplyMonth =QtyApplyMonth.replace('-','');
     AccountApplyMonth  =AccountApplyMonth.replace('-','');
     System.debug('QtyRolloutMonth ::'+ QtyRolloutMonth );
    System.debug('QtyApplyMonth ::'+ QtyApplyMonth );
    System.debug('AccountApplyMonth ::'+AccountApplyMonth );
    
    if(QtyRolloutMonth !=null && QtyApplyMonth !=null && QtyApplyMonth !='' && QtyRolloutMonth  !='' &&AccountApplyMonth !=null&&AccountApplyMonth  !='' ){
    }else{
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Please fill All Date Values'));
          return null;
    }
    
     if(QtyRolloutMonth !=null && QtyApplyMonth !=null && QtyApplyMonth !='' && QtyRolloutMonth  !='' ){
         sendSelloutQTYData();
     }else{
       ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Please fill Apply Month and Rollout Month for Sellout QTY Integration'));
          return null;
     }
     
      if(AccountApplyMonth !=null&&AccountApplyMonth  !=''){
        sendSelloutAccountData();
      }else{
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Please fill Apply Month for Sellout Account Integration'));
          return null;
      }
    }
     
      
      return null;
    }
    
    
    Public void sendSelloutQTYData(){
       string partialEndpoint = Test.isRunningTest() ? 'asdf' : Integration_EndPoints__c.getInstance('Flow-030').partial_endpoint__c;
       string layoutId = Test.isRunningTest() ? 'asdf' : Integration_EndPoints__c.getInstance('Flow-030').layout_id__c;
        
        SelloutStubClass.SelloutQTYRequest reqStub = new SelloutStubClass.SelloutQTYRequest();
        SelloutStubClass.msgHeadersRequest headers = new SelloutStubClass.msgHeadersRequest(layoutId);
        SelloutStubClass.SelloutQTYRequestBody body = new SelloutStubClass.SelloutQTYRequestBody();
        mapbodyFields(body, sols);
        reqStub.body = body;
        reqStub.inputHeaders = headers;
        string requestBody = json.serialize(reqStub);
         system.debug('QTY Data requestBody ::'+requestBody );
         webcallout(requestBody,partialEndpoint);
    }
    
    
    
    public void sendSelloutAccountData(){
        string partialEndpoint = Test.isRunningTest() ? 'asdf' : Integration_EndPoints__c.getInstance('Flow-031').partial_endpoint__c;
        string layoutId = Test.isRunningTest() ? 'asdf' : Integration_EndPoints__c.getInstance('Flow-031').layout_id__c;
       
        SelloutStubClass.SelloutAccRequest reqStub = new SelloutStubClass.SelloutAccRequest();
        SelloutStubClass.msgHeadersRequest headers = new SelloutStubClass.msgHeadersRequest(layoutId);
        SelloutStubClass.SelloutAccRequestBody body = new SelloutStubClass.SelloutAccRequestBody();
        mapbodyFieldsAccount(body, sols);
        reqStub.body = body;
        reqStub.inputHeaders = headers;
        string requestBody = json.serialize(reqStub);
        system.debug('Account Data requestBody ::'+requestBody );
        webcallout(requestBody,partialEndpoint);
    }
    
    
    public void webcallout(string body, string partialEndpoint){ 
       System.debug('##requestBody ::'+body);
        System.Httprequest req = new System.Httprequest();
        system.debug('************************************** INSIDE CALLOUT METHOD');
        HttpResponse resp = new HttpResponse();
        req.setMethod('POST');
        req.setBody(body);
        system.debug(body);
       
        req.setEndpoint('callout:X012_013_ROI'+partialEndpoint); //+namedEndpoint); todo, make naming consistent on picklist values and endpoints and use that here
        
        req.setTimeout(120000);
         req.setHeader('Content-Type', 'application/json');
        req.setHeader('Accept', 'application/json');
    
        Http http = new Http();  
      
                     if(!Test.isRunningTest()){
                          if(senddata ==true){
                              System.debug('Sending');
                              resp = http.send(req);
                          }else{
                             System.debug('Not Sending');
                          }
                         
                        }else{
                           resp = Testrespond(req);
                        }
       
        System.debug('******** my resonse' + resp);
        system.debug('*****Response body::' + resp.getBody());
        System.debug('***** Response Body Size ' + resp.getBody().length());
        String response= resp.getBody();
        if(string.isNotBlank(response)){
           SelloutStubClass.UpdateQtyFrmERPResponse res = (SelloutStubClass.UpdateQtyFrmERPResponse) json.deserialize(response, SelloutStubClass.UpdateQtyFrmERPResponse.class);
                system.debug('##  Response ::'+res);
                if(res.inputHeaders.MSGSTATUS == 'S'){
                    handleSuccess(res);
                 }else{
                     String er=res.inputHeaders.ERRORTEXT;
                    handleError(res);
                 }
        
        }
         
        
           
      
  }
    
    
     public  void mapbodyFields(SelloutStubClass.SelloutQTYRequestBody body, List<Sellout_Products__c> sols){
           list<SelloutStubClass.SelloutProducts> SelloutProducts = new list<SelloutStubClass.SelloutProducts>();
            mapProductFields(SelloutProducts, sols);
            body.lines = SelloutProducts;
     
   }
   
   public  void mapProductFields(list<SelloutStubClass.SelloutProducts> SelloutProducts,  List<Sellout_Products__c> sols){ 
       Map<String,String> codemap=new map<string,string>();
      List<Carrier_Sold_to_Mapping__c> crr=Carrier_Sold_to_Mapping__c.getall().values();
      for(Carrier_Sold_to_Mapping__c c:crr){
        codemap.put(c.Carrier_Code__c,c.Sold_To_Code__c);  
      }
      Map<string,Sellout_Products__c> ilMap=new Map<string,Sellout_Products__c>();
      Map<string,Sellout_Products__c> clMap=new Map<string,Sellout_Products__c>();
      Map<String,String> VersionMap=new Map<String,String>();
      VersionMap.put('01','T01');
      VersionMap.put('02','T02');
      VersionMap.put('03','T03');
      VersionMap.put('04','T04');
      VersionMap.put('05','T05');
      VersionMap.put('06','T06');
      VersionMap.put('07','T07');
      VersionMap.put('08','T08');
      VersionMap.put('09','T09');
      VersionMap.put('10','T0A');
      VersionMap.put('11','T0B');
      VersionMap.put('12','T0C');
      
      //   
         for(Sellout_Products__c sp :sols){
             if(sp.IL_CL__c=='IL'){
                 String key=String.valueOf(sp.Carrier__c)+'-'+sp.Samsung_SKU__c+'-'+String.valueOf(sp.Sellout_Date__c);
                 ilMap.put(key,sp);
             }else{
                 String key=String.valueOf(sp.Carrier__c)+'-'+sp.Samsung_SKU__c+'-'+String.valueOf(sp.Sellout_Date__c);
                 clMap.put(key,sp);
             }
         }
      
      //
      Integer yr=0;
      String Vers='';
      for(Sellout_Products__c sp :sols){
         
          SelloutStubClass.SelloutProducts sol = new SelloutStubClass.SelloutProducts();
               sol.modelCode=sp.Samsung_SKU__c;
               sol.opportunityNo=sp.Sellout__r.Name;
               sol.accountNo=codemap.get(sp.Carrier__c);
                Date d=sp.Sellout_Date__c;
                String sMonth = String.valueof(d.month());
                String sDay = String.valueof(d.day());
                if(sMonth.length()==1){
                  sMonth = '0' + sMonth;
                }
                if(sDay.length()==1){
                  sDay = '0' + sDay;
                }
                string closemnth=sp.Sellout__r.Closing_Month_Year1__c;
             
               
                if(sp.Sellout__r.Subsidiary__c=='SEA'){
                      sol.countryCode='US';
                     }else if(sp.Sellout__r.Subsidiary__c=='SECA'){
                        sol.countryCode='CA'; 
                     }
               
                 if(sp.Sellout__r.RecordType.Name=='Plan'){
                    sol.applyMonth  =QtyApplyMonthplan;//string.valueOf(d.year())+sMonth;
                    sol.rolloutMonth=QtyRolloutMonthplan;//string.valueOf(d.year())+sMonth;
                    
                       //
                        if(d.year()>yr){
                          yr=d.year();
                          vers=VersionMap.get(sMonth);
                        }
                         sol.version=vers;
                     //   
                    
                 }else{
                     sol.rolloutMonth=QtyRolloutMonth;//string.valueOf(d.year())+sMonth;
                     sol.applyMonth=QtyApplyMonth;//string.valueOf(closemnth.right(4)+closemnth.left(2)); 
                     sol.version='000';
                 }
              
                 String key=sp.Carrier__c+'-'+sp.Samsung_SKU__c+'-'+String.valueOf(sp.Sellout_Date__c);
                if(sp.IL_CL__c=='IL'){
                     sol.indexCode ='';
                      if(clMap.containsKey(key)){
                          Sellout_Products__c clsp=clMap.get(key);
                          decimal qty=sp.Quantity__c+clsp.Quantity__c;
                          sol.quantity=String.valueOf(qty);
                      }else{
                          sol.quantity=String.valueOf(sp.Quantity__c);  
                      }
                 }else{
                      sol.indexCode ='D';
                      sol.quantity=String.valueOf(sp.Quantity__c);  
                      
                   
                 }
                 
                   SelloutProducts.add(sol); 
                  if(sp.IL_CL__c=='CL'){ 
                      if(!ilMap.containsKey(key)){
                              SelloutStubClass.SelloutProducts sol2 = sol.clone();
                                sol2.indexCode ='';
                               SelloutProducts.add(sol2);
                          } 
                  }
                
             
         
      }
      
     
   }
   
   
   public  void mapbodyFieldsAccount(SelloutStubClass.SelloutAccRequestBody body, List<Sellout_Products__c> sols){
             list<SelloutStubClass.SelloutProductsAcc> SelloutProducts = new list<SelloutStubClass.SelloutProductsAcc>();
            mapProductFieldsAccount(SelloutProducts, sols);
            body.lines = SelloutProducts;
   
   }
   
  
   public void mapProductFieldsAccount(list<SelloutStubClass.SelloutProductsAcc> SelloutProducts,  List<Sellout_Products__c> sols){ 
       Map<String,String> codemap=new map<string,string>();
       Map<String,String> billtocodemap=new map<string,string>();
       Set<string> unqrec=new set<String>();
      List<Carrier_Sold_to_Mapping__c> crr=Carrier_Sold_to_Mapping__c.getall().values();
      for(Carrier_Sold_to_Mapping__c c:crr){
         codemap.put(c.Carrier_Code__c,c.Sold_To_Code__c);
         billtocodemap.put(c.Carrier_Code__c,c.Sold_To_Code_Pre_Check__c); 
      }
      Map<String,String> VersionMap=new Map<String,String>();
      VersionMap.put('01','T01');
      VersionMap.put('02','T02');
      VersionMap.put('03','T03');
      VersionMap.put('04','T04');
      VersionMap.put('05','T05');
      VersionMap.put('06','T06');
      VersionMap.put('07','T07');
      VersionMap.put('08','T08');
      VersionMap.put('09','T09');
      VersionMap.put('10','T0A');
      VersionMap.put('11','T0B');
      VersionMap.put('12','T0C');
      
      Integer yr=0;
      String Vers='';
       //
        for(Sellout_Products__c sp :sols){
             List<string> billtos=new  List<string>();
             try{
             billtos=billtocodemap.get(sp.Carrier__c).split(',');
             }catch(exception ex){
                 
             }
             if(billtos.size()>0){
                  for(String bt:billtos){
                      SelloutStubClass.SelloutProductsAcc sol = new SelloutStubClass.SelloutProductsAcc();
                         sol.modelCode=sp.Samsung_SKU__c;
                         sol.accountNo=codemap.get(sp.Carrier__c);
                         sol.billToCode=bt;
                         string closemnth=sp.Sellout__r.Closing_Month_Year1__c;
                         Date d=sp.Sellout_Date__c;
                            String sMonth = String.valueof(d.month());
                            String sDay = String.valueof(d.day());
                            if(sMonth.length()==1){
                              sMonth = '0' + sMonth;
                            }
                            if(sDay.length()==1){
                              sDay = '0' + sDay;
                            }
                     
                          if(sp.Sellout__r.RecordType.Name=='Plan'){
                             sol.applyMonth=AccountApplyMonthplan;
            
                                    if(d.year()>yr){
                                      yr=d.year();
                                      vers=VersionMap.get(sMonth);
                                    }
                                     sol.version=vers;
                                 
                             
                             
                        }else{
                            sol.version='000';
                            sol.applyMonth=AccountApplyMonth;
                           
                        }
                        String key=sol.version+'-'+sol.applyMonth+'-'+sol.modelCode+'-'+sol.billToCode;
                        if(!unqrec.contains(key)){
                            unqrec.add(key);
                            SelloutProducts.add(sol);
                        }
                                  
                  } 
             }else{
                    SelloutStubClass.SelloutProductsAcc sol = new SelloutStubClass.SelloutProductsAcc();
                         sol.modelCode=sp.Samsung_SKU__c;
                         sol.accountNo=codemap.get(sp.Carrier__c);
                         sol.billToCode=codemap.get(sp.Carrier__c);
                         string closemnth=sp.Sellout__r.Closing_Month_Year1__c;
                         Date d=sp.Sellout_Date__c;
                            String sMonth = String.valueof(d.month());
                            String sDay = String.valueof(d.day());
                            if(sMonth.length()==1){
                              sMonth = '0' + sMonth;
                            }
                            if(sDay.length()==1){
                              sDay = '0' + sDay;
                            }
                     
                          if(sp.Sellout__r.RecordType.Name=='Plan'){
                             sol.applyMonth=AccountApplyMonthplan;
            
                                    if(d.year()>yr){
                                      yr=d.year();
                                      vers=VersionMap.get(sMonth);
                                    }
                                     sol.version=vers;
                                 
                             
                             
                        }else{
                            sol.version='000';
                            sol.applyMonth=AccountApplyMonthplan;
                           
                        }
                        String key=sol.version+'-'+sol.applyMonth+'-'+sol.modelCode+'-'+sol.billToCode;
                        if(!unqrec.contains(key)){
                            unqrec.add(key);
                            SelloutProducts.add(sol);
                        } 
             }
         
            
       }
       
   }
   
   
   
   public void handleSuccess (SelloutStubClass.UpdateQtyFrmERPResponse res){
           message_queue__c mq=new message_queue__c();
            mq.Identification_Text__c=so.id;
            mq.Object_Name__c='Sellout';
            mq.status__c = 'success';
            mq.MSGSTATUS__c = res.inputHeaders.MSGSTATUS;
            mq.msguid__c = res.inputHeaders.msgGUID;
            mq.IFID__c = res.inputHeaders.ifID;
            mq.IFDate__c = res.inputHeaders.ifDate;
            mq.retry_counter__c=0;
            insert mq;
       
   }
   
    public void handleError (SelloutStubClass.UpdateQtyFrmERPResponse res){
           message_queue__c mq=new message_queue__c();
            mq.Identification_Text__c=so.id;
            mq.Object_Name__c='Sellout';
            mq.status__c = 'failed';
            mq.MSGSTATUS__c = res.inputHeaders.MSGSTATUS;
            mq.msguid__c = res.inputHeaders.msgGUID;
            mq.IFID__c = res.inputHeaders.ifID;
            mq.IFDate__c = res.inputHeaders.ifDate;
            mq.retry_counter__c=1;
            String er=res.inputHeaders.ERRORTEXT;
            mq.ERRORTEXT__c =er;
         insert mq;
   }
   
   
        public Static HTTPResponse Testrespond(HTTPRequest req) {          
               String outputBodyString = '{"inputHeaders":{"MSGSTATUS":"S","MSGGUID":"a57f6e1a-318d-7891-2707-335f850d92b8","IFID":"SFDC_IF_US_031","IFDate":"20190108093101","ERRORTEXT":"","ERRORCODE":""}}';
         HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatus('2');
            res.setStatusCode(200);
            res.setBody(outputBodyString);
            
            return res;                
      } 
   
}