@isTest
private class TempSelloutSendtoGERPControllerTest {

	
	
		private static testMethod void test2() {
        Test.startTest();
          Carrier_Sold_to_Mapping__c csm=new Carrier_Sold_to_Mapping__c();
          csm.name='ATT';
          csm.Carrier_Code__c='ATT';
          csm.Sold_To_Code__c='0002';
          csm.Sold_To_Code_Pre_Check__c='0001,0002';
          insert csm;
          
          Carrier_Sold_to_Mapping__c csm1=new Carrier_Sold_to_Mapping__c();
          csm1.name='SPR';
          csm1.Carrier_Code__c='SPR';
          csm1.Sold_To_Code__c='0003';
          insert csm1;
        
          Sellout__C so=new Sellout__C(Closing_Month_Year1__c='10/2018',Description__c='From Test Class',Subsidiary__c='SEA',Approval_Status__c='Draft',Integration_Status__c='Draft',Has_Attachement__c=true,Request_Approval__c=true);
         insert so;
         Date d=System.today();
                  RecordType productRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'B2B' AND SobjectType = 'Product2'];

          Product2 prod1 = new Product2(
                name ='SM-TEST123',
                SAP_Material_ID__c = 'SM-TEST123',
                RecordTypeId = productRT.id
            );

        insert prod1;
          List<Sellout_Products__c> slp=new list<Sellout_Products__c>();
          Sellout_Products__c sl0=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST123',Quantity__c=100,Carrier__c='ATT',Carrier_SKU__c='Samsung Mobile',IL_CL__c='CL',Pre_Check_Result__c='OK');
          Sellout_Products__c sl1=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST123',Quantity__c=100,Carrier__c='ATT',Carrier_SKU__c='Samsung Mobile',IL_CL__c='IL',Pre_Check_Result__c='OK');
           Sellout_Products__c sl2=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST123',Quantity__c=100,Carrier__c='SPR',Carrier_SKU__c='Samsung Mobile',IL_CL__c='CL',Pre_Check_Result__c='OK');
          Sellout_Products__c sl3=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST123',Quantity__c=100,Carrier__c='SPR',Carrier_SKU__c='Samsung Mobile',IL_CL__c='IL',Pre_Check_Result__c='OK');
          slp.add(sl0);
          slp.add(sl1);
          slp.add(sl2);
          slp.add(sl3);
          insert slp;
          Pagereference pf= Page.TempSelloutSendtoGERP;
           pf.getParameters().put('id',so.id);
           test.setCurrentPage(pf);
        
        apexPages.standardcontroller sc = new apexPages.standardcontroller(so);
          TempSelloutSendtoGERPController cls=new TempSelloutSendtoGERPController(sc);
          cls.QtyRolloutMonth='2018-10';
          cls.QtyApplyMonth='2018-10';
          cls.AccountApplyMonth='2018-10';
          
          cls.checkvalues();
          Test.stopTest();
	}

}