/*This utility class is used by other test classes to set up some test data
Modified by : Aarthi Chidambaram */
@isTest
public with sharing class TestDataUtility {

    //This method will accept the developerName and sObject and returns the ID back for that particular developerName of the sObject
    public static Id retrieveRecordTypeId(String DeveloperName, String sObjectName){
        Map<String,Id> recMaps = retrieveRecordTypes(sObjectName);
        return recMaps.get(DeveloperName);
    } 

    //This method is to retrieve the RecordType Id and Developername for the given Sobject
    public static Map<String,Id> retrieveRecordTypes (String objName){
        Map<String,Id> recMaps = new Map<String,Id>();  
        List<RecordType> recordTypes = [select id, DeveloperName from RecordType where sObjectType =: objName];
        for(RecordType rt : recordTypes){
            recMaps.put(rt.DeveloperName, rt.Id);
        }
        return recMaps;
    }
    
  //This method is to retrieve the Pricebooks by its name
    public static Id retrievePricebook (String pbName){
 
        Pricebook2 pbRec = [select Id, Name from Pricebook2 where isactive=true and Name=:pbName limit 1];
        System.debug('## pbRec'+pbRec);
        return pbRec.Id;
    }

    public static Zipcode_Lookup__c createZipcode(){
        Zipcode_Lookup__c z=new Zipcode_Lookup__c(City_Name__c='San Francisco', Country_Code__c='US', State_Code__c='CA', State_Name__c='California', Name='94102');
        return z;
    }

    //create Account
    // MK 6/23/2016
    // Updated method to incorporate creating a zipcode lookup entity
    // before creating an account
    public static Account createAccount(String name){
      Zipcode_Lookup__c zipCode = createZipcode();
      insert zipCode;
        Account a=new Account(Name=name, Type='Customer', BillingStreet='1 Market St',
                        BillingCity='San Francisco', BillingState='CA', BillingCountry='US', BillingPostalCode=zipCode.Name);
        return a;

    }

    public static PriceBook2 createPriceBook(String name){
        PriceBook2 pb=new PriceBook2(isActive=true,Name=name);
        return pb;
    }

    //As per the recent validation rule('Valid_Identification_Stage') on Opportunity, have to include ProductGroupTest__c
    //Included the Legacy_Id__c as per new validation rules
    public static Opportunity createOppty(String name, Account acct,PriceBook2 priceBook, String type, String division, String productGroup,
                                    String stage){
        Opportunity o = new Opportunity(Name=name, Account=acct, AccountId = acct.Id,Pricebook2=priceBook,Type=type,Division__c=division,
                             ProductGroupTest__c=productGroup, StageName=stage, CloseDate=System.today().addDays(5),
                            Amount=0, Description = 'Test', Roll_Out_Start__c = System.today().addDays(30),
                                    Rollout_Duration__c = 5, Legacy_Id__c=TestDataUtility.generateRandomString(10));
        //As per validation rule for the stage name as Drop, Drop reason should be specified
        if(stage=='Drop'){
            system.debug('Im inside the drop condition');
            o.Drop_Reason__c = 'No Sales Activity';
        } else {
            o.Drop_Reason__c = null;
        }
        return o;
    }


    //SAP_Material_Id__C should be unique and case sensitive ID
    public static Product2 createProduct2(String SAPMaterialID, String name, String family, String prodGroup, String prodType){
        Product2 prod=new Product2(SAP_Material_ID__c=SAPMaterialID,isActive=true, Name=name, ProductCode='1234',Description='test description',
                        Family=family,Product_Type__c=prodType,Product_Group__c=prodGroup, Unit_Cost__c=22);
        return prod;
    }


    /* Insert a price book entry for the standard price book.
    Standard price book entries require the standard price book ID*/

    /*
    createPricebookEntry method signature is modified from (Product, PriceBook) to accept(Id, Id) as 
    product and pricebook are the lookup fields and it should be connected with the id's

    API name for the fields pricebook2 and product2 was Pricebook2Id,Product2Id
    */

    public static PriceBookEntry createPriceBookEntry(Id product, Id priceBook){
        PriceBookEntry pbe=new PriceBookEntry(isActive=true,UnitPrice=111.11, Pricebook2Id=priceBook,
                                        Product2Id=product);
    return pbe;

    }

    //Creating opportunity Line Item
    /* The system is looking for the totla price so included that as a parameter */
    public static OpportunityLineItem createOpptyLineItem(Product2 product, PriceBookEntry pbEntry, Opportunity oppty){
        OpportunityLineItem oli=new OpportunityLineItem(OpportunityId=oppty.Id, PriceBookEntry=pbEntry, PriceBookEntryId=pbEntry.Id,
                                                    Requested_Price__c=pbEntry.UnitPrice, Description=product.Description,
                                                    Product_Group__c=product.Product_Group__c,Product_Type__c=product.Product_Type__c,
                                                    Quantity=1,Alternative__c=false,TotalPrice=250);
        return oli;

    }

    public static Product_Relationship__c createProductRelationships(Product2 childProduct, Product2 parentProduct){
        Product_Relationship__c pr=new Product_Relationship__c(Child_Product__c=childProduct.Id, Parent_Product__c=parentProduct.Id);
        return pr;
    }

    public static Partner__c createPartner(Opportunity oppty, Account acc, Account partnerAcct, String role){
        Partner__c  partner = new Partner__c(Opportunity__c = oppty.Id, Customer__c = acc.Id, Partner__c= partnerAcct.Id, Role__c = role, Registered__c = false);
        return partner;
    }

    //Create Rollouts
    //Roll out start date must be later than opp close date
    public static Roll_Out__c createRollOuts(String rolloutname, Opportunity opp){
        Roll_Out__c rollout = new Roll_Out__c(name=rolloutname, Opportunity__c = opp.Id, Rollout_Duration__c = opp.Rollout_Duration__c, 
        Roll_Out_Start__c = opp.Roll_Out_Start__c, Roll_Out_End__c = System.today().addDays(365));
        return rollout;
    }

    //Create Contact
    public static Contact createContact(Account acc){
        String email = TestDataUtility.generateRandomString(7) + '@gmail.com';
        Contact c = new Contact(LastName = TestDataUtility.generateRandomString(8), AccountId = acc.Id, Email=email);
        return c;
    }


    //To generate Random Strings for SAP_Material_Ids, Account name, Price book name..etc
    public static String generateRandomString(Integer len) {
        final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String randStr = '';
        while (randStr.length() < len) {
            Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
            randStr += chars.substring(idx, idx+1);
        }
        return randStr; 
    }

    //Create Quote
    public static Quote createQuote(String name,Opportunity opp, Pricebook2 pb){
        if(null != pb){
        Quote quote = new Quote(Name=name, OpportunityId = opp.Id, Pricebook2Id = pb.Id, BEP__c=null, ProductGroupMSP__c='FAX/MFP');
        return quote;
        } else {
            Quote quote = new Quote(Name=name, OpportunityId = opp.Id, BEP__c=null, ProductGroupMSP__c='FAX/MFP');   
            return quote;
        }
    }

    //Create QuoteLineItem
    public static QuoteLineItem createQuoteLineItem(Id quoteId, Product2 product2, PriceBookEntry customPBEID, OpportunityLineItem oli) {
        QuoteLineItem QLI = new QuoteLineItem( QuoteId = quoteId,  Product2Id = product2.Id, PriceBookEntryId = customPBEID.Id, OLIID__c = oli.Id, UnitPrice=customPBEID.UnitPrice, Requested_Price__c=customPBEID.UnitPrice, Quantity=5,PriceBookEntry=customPBEID);
        return QLI;
    }

    // create creditMemo

    public static Credit_Memo__c createCreditMemo()
    {
        Credit_Memo__c cm = new Credit_Memo__c(Assignment_Number__c= TestDataUtility.generateRandomString(6), Status__c ='Open');
        return cm;
    }

    // create Credit memo product
    public static Credit_Memo_Product__c createCreditMemoProduct(Credit_Memo__c cm, OpportunityLineItem oli)
    {
        Credit_Memo_Product__c cmProd = new Credit_Memo_Product__c(Credit_Memo__c = cm.Id, OLI_ID__c = oli.Id );
        return cmProd;
    }

    //Create the PickListTranslation
    public static List<PicklistTranslation__c> createPickListTranslation(Boolean isUpdate)
    {
        List<PicklistTranslation__c> pickVals = new List<PicklistTranslation__c>();
        if(!isUpdate)
        {
            PicklistTranslation__c pl1 = new PicklistTranslation__c(Integration_Source__c='CIS',SourceAPIName__c='External_Industry__c',SourceValue__c='CO',TargetAPIFieldName__c='Industry',TargetValue__c='Communications');
            pickVals.add(pl1);
            PicklistTranslation__c pl2 = new PicklistTranslation__c(Integration_Source__c='CIS',SourceAPIName__c='external_industry_type__c',SourceValue__c='COM',TargetAPIFieldName__c='Industry_type__c',TargetValue__c='Dot com');
            pickVals.add(pl2);
            PicklistTranslation__c pl3 = new PicklistTranslation__c(Integration_Source__c='CIS',SourceAPIName__c='External_Top_Tier__c',SourceValue__c='PT',TargetAPIFieldName__c='Partner_top_tier__c',TargetValue__c='Platinum');
            pickVals.add(pl3);
            PicklistTranslation__c pl4 = new PicklistTranslation__c(Integration_Source__c='CIS',SourceAPIName__c='external_business_partner_type__c',SourceValue__c='CO2',TargetAPIFieldName__c='business_partner_type__c',TargetValue__c='Danger');
            pickVals.add(pl4);
            PicklistTranslation__c pl5 = new PicklistTranslation__c(Integration_Source__c='CIS',SourceAPIName__c='External_Type__c',SourceValue__c='EX',TargetAPIFieldName__c='Type',TargetValue__c='Customer');
            pickVals.add(pl5);
            
        }
        else
        {
            PicklistTranslation__c pl1 = new PicklistTranslation__c(Integration_Source__c='CIS',SourceAPIName__c='External_Industry__c',SourceValue__c='CO1',TargetAPIFieldName__c='Industry',TargetValue__c='Communications1');
            pickVals.add(pl1);
            PicklistTranslation__c pl2 = new PicklistTranslation__c(Integration_Source__c='CIS',SourceAPIName__c='external_industry_type__c',SourceValue__c='COM1',TargetAPIFieldName__c='Industry_type__c',TargetValue__c='Dot com1');
            pickVals.add(pl2);
            PicklistTranslation__c pl3 = new PicklistTranslation__c(Integration_Source__c='CIS',SourceAPIName__c='External_Top_Tier__c',SourceValue__c='PT1',TargetAPIFieldName__c='Partner_top_tier__c',TargetValue__c='Platinum1');
            pickVals.add(pl3);
            PicklistTranslation__c pl4 = new PicklistTranslation__c(Integration_Source__c='CIS',SourceAPIName__c='external_business_partner_type__c',SourceValue__c='CO21',TargetAPIFieldName__c='business_partner_type__c',TargetValue__c='Danger1');
            pickVals.add(pl4);
            PicklistTranslation__c pl5 = new PicklistTranslation__c(Integration_Source__c='CIS',SourceAPIName__c='External_Type__c',SourceValue__c='EX1',TargetAPIFieldName__c='Type',TargetValue__c='Customer1');
            pickVals.add(pl5);
        }
        return pickVals;
    }
        //Create Channel Relationship
        public static Channel__c createChannel (Opportunity opp , Account distriAcct, Account resellerAcct, Partner__c distriParter, Partner__c resellPartner )
        {
            Channel__c channel = new Channel__c(Partner_Distributor__c= distriParter.Id, Partner_Reseller__c = resellPartner.Id, Reseller__c = resellerAcct.Id, Distributor__c = distriAcct.Id, Opportunity__c = opp.Id);
            return channel;
        }

        //Create Account_Profile__c

        public static Account_Profile__c createAccountProfile(Account acc)
        {
            Account_Profile__c accProfile = new Account_Profile__c(FiscalYear__c = '2019', Account__c = acc.Id);
            return accProfile;

        }

        //create AccountTeamMember

        public static AccountTeamMember createAccountTeamMember(User user, Account account)
        {
            AccountTeamMember accTeamMem = new AccountTeamMember(TeamMemberRole='Enterprise',AccountId=account.Id,UserId= user.Id);
            return accTeamMem;
        }
   
        // create Account Contact role
        public static AccountContactRole createAcccontactRole(Account acc, Contact con)
        {
            AccountContactRole accConRole = new AccountContactRole(AccountId=acc.Id, ContactId=con.Id, IsPrimary=true, Role='Distributor');
            return accConRole;
        }

        // Create an Event
        public static Event createEvent(Contact con, Account acc)
        {
            Event event = new Event(ActivityDate=System.today().addDays(-45),SamsungExec__c='TestUser',Subject='Testsub',Type='ETP',WhatId=acc.Id,WhoId=con.Id);
            return event;
        }

        // create Marketing_Investment__c
        public static Marketing_Investment__c createMarketingInvesment(Account_Profile__c accProfile)
        {
            Marketing_Investment__c marInvesment = new Marketing_Investment__c(Account_Profile__c=accProfile.Id,Amount__c=100.00,FiscalYear__c='2019',Focus__c='Partner',Type__c='Training');
            return marInvesment;
        }

        //create CompetitiveProductShare__c
        public static CompetitiveProductShare__c createCompetitiveProductShare(Account_Profile__c accProfile )
        {
            CompetitiveProductShare__c compShare = new CompetitiveProductShare__c(CompetitorName__c='Apple',Account_Profile__c= accProfile.Id,CompetitorProduct__c='iPhone6S', Product__c='HHP',Share_Percent__c=25.0);
            return compShare;
        }
        
        //Create Creadit Memo Quote
        public static Credit_Memo_Quote__c createCreditMemoQuote (Credit_Memo__c cm, Quote quot, String name)
        {
            Credit_Memo_Quote__c creditMemoQuote = new Credit_Memo_Quote__c(Name = name, Credit_Memo__c= cm.Id, Quote__c = quot.Id );
            return creditMemoQuote;
        }

        // Create Attachment
        public static Attachment createAttachment(Quote quote){
            Attachment attach = new Attachment(Body=blob.toPDF('From the test class'), ContentType='pdf', Name='My test pdf' + '.pdf', ParentId= quote.Id);
            return attach;

        }

        // create Quote Document
        public static QuoteDocument createQuoteDoc (Quote qot, Attachment attach)
        {
            QuoteDocument qotDoc = new QuoteDocument(Document= attach.body, QuoteId = qot.id );
            return qotDoc;
        }

        // create quote pdf history
        public static QuotePDFHistory__c createQuotePDFHistory(Quote qot, String pdfName, Account distributor)
        {
            QuotePDFHistory__c history = new QuotePDFHistory__c(Quote__c= qot.id, Distributor_Record_ID__c=distributor.Id, Distributor__c='Test Distributor', Quote_PDF_Name__c = pdfName, Resellers__c = 'Test Resellers');
            return history;
        }

        // create an order
        public static Order createOrder(Account acc, Opportunity opp)
        {
            Order order = new Order(AccountId = acc.Id, OpportunityId = opp.Id, EffectiveDate= System.today().addDays(-45), Status='Approved' ); 
            return order;
        }
        
        // create an OrderItem
        public static OrderItem createOrderItem (PriceBookEntry pbEntry, Order ord, Decimal unitPrice)
        {
          OrderItem orderItem = new OrderItem (OrderId = ord.Id, PriceBookEntry=pbEntry, PriceBookEntryId=pbEntry.Id, Quantity=1.0, UnitPrice = unitPrice);
          return orderItem;
        }

        public static DirectPriceBookAssign__c createDPBA(Account acct, PriceBook2 pb){
            DirectPriceBookAssign__c d=new DirectPriceBookAssign__c(Direct_Account__c=acct.Id, Price_book__c=pb.Id);
            return d;
        }

		public static User createPortalUser() {
		Id profileId = [select Id from Profile where Name ='Samsung Customer Community User' limit 1].Id;
		Account acct = new Account();
		User portalAccountOwner = TestDataUtility.createPortalAccountOwner();
		System.runAs ( portalAccountOwner) { 
			acct = TestDataUtility.createAccount('TestAccount');

			Contact customer = TestDataUtility.createContact(acct);
			User portalUser = new User(
				email='test-user@fakeemail.com', 
				contactid = customer.id, 
				profileid = profileid, 
				UserName=System.now().millisecond()+'test-user123@fakeemail123.com'+System.now().millisecond()+Math.random(), 
				alias='tuser1', 
				CommunityNickName='tuser1',
				TimeZoneSidKey='America/New_York', 
				LocaleSidKey='en_US', 
				EmailEncodingKey='ISO-8859-1',
				LanguageLocaleKey='en_US', 
				FirstName = 'Test', 
				LastName = 'User',
                isActive = true ); 
			insert portalUser;
        
			Id userId = Site.createPortalUser(portalUser, customer.Id, '12345');
			return portalUser;
		}

		return null;
		
	}

	public static User createPortalAccountOwner(){
		UserRole portalRole = [Select Id From UserRole Where DeveloperName = 'Contracts_and_Proposals' Limit 1];

        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
			User portalAccountOwner1 = new User(
            UserRoleId = portalRole.Id,
            ProfileId = profile1.Id,
            Username = System.now().millisecond() + 'test12@test1234.com'+System.now().millisecond()+Math.random(),
            Alias = 'buzzy',
            Email='Buzz@AndysRoom.com',
            EmailEncodingKey='UTF-8',
            Firstname='Buzz',
            Lastname='Lightyear',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago'
        );
        insert portalAccountOwner1; 

		return portalAccountOwner1;
	}
}