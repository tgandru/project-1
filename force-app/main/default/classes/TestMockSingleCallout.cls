/*
author : sungil.kim, I2MAX
*/
@isTest
public with sharing class TestMockSingleCallout implements HttpCalloutMock {
	public String resultBody {get;set;}
	public Integer status {get;set;}
	public TestMockSingleCallout(String body){
		this.resultBody = body;
		this.status = 200;

	}
	public TestMockSingleCallout(String body, Integer status){
		this.resultBody = body;
		this.status = status;
	}
	public HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = newResponse(this.status, resultBody);
		
		return res;
	}
	
	// HTTPResponse 객체 생성
	private static HTTPResponse newResponse(Integer statusCode, String resultBody) {
		System.debug('newResponse resultBody ==> ' + resultBody);
		HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'application/json');
		res.setStatusCode(statusCode);
		res.setBody(resultBody);
		
		return res;
	}
}