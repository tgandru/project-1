public class TrackerView {
   public list<Tracker__c> lst{get; set;}
  public list<selectOption> own {get; set;}
   public string selectedow {get; set;}
 
    public boolean disableFilters{get; set;}
    public TrackerView(ApexPages.StandardController controller) {
       own = new list<selectoption>();
       own.add(new selectOption('','-None-'));
        for(Schema.PicklistEntry pe : Tracker__c.Owner__c.getDescribe().getPicklistValues()){
            own.add(new selectOption(pe.getValue(),pe.getValue()));
        }
       
    }
    public pageReference prepareRecords(){
     
            lst =[  select id,Date__c,Description__c,   Hours_Spent2__c, Module__c,Owner__c,  Requester__c,   Type__c from Tracker__c where Owner__c =:selectedow order by Date__c DESC];
       
    
    return null;
    }
     public pageReference saving(){
      
       try{
           upsert lst;
       return new  Pagereference('/apex/TrackerView');
      }catch(exception e){
            ApexPages.addMessages(e);
            return null;
        }
    }
    public pageReference EditRecords(){
       
       return new  Pagereference('/apex/TrackerViewEdit');
    }
     public pageReference NewRecords(){
       
       return new  Pagereference('/apex/AddTracker');
    }
     public pageReference ExportRecords(){
       
       return new  Pagereference('/apex/TrackerExport');
    }
}