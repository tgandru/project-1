public class Trackernew {

    public Trackernew(ApexPages.StandardController controller) {
      Tracker__c exp=new Tracker__c();
      lst =new list<Tracker__c>();
      lst.add(exp);
    }

 public Integer rowNum{get;set;}
  public list<Tracker__c> lst{get; set;}
    
  public void adding(){
    Tracker__c e=new Tracker__c();
    lst.add(e);
  }
  public pagereference saving(){
      try{
           for(integer i=0;i<lst.size();i++){
      upsert lst;
    } 
      }catch(exception e){
            ApexPages.addMessages(e);
            return null;
        }
  
    return new Pagereference('/apex/TrackerView');
    
  }
   public pagereference Cancelon(){
    return new Pagereference('/apex/TrackerView');
    
  }
    public void RemoveItems(){       

       Integer rowNum = Integer.valueof(system.currentpagereference().getparameters().get('index'));
       lst.remove(rowNum - 1);     
    }          
  
}