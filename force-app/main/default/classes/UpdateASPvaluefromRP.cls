/*
This Class is to update the ASP value on ROS from ASP custom object

Logic for this class is, in ASP present in Custom Object update ASP on ROS with that value other wise update with Salesprice
All future months needs to update with recent ASP from custom object.

We need to call this class from ROP Trigger After insert only for MObile Record type 



*/
public class UpdateASPvaluefromRP implements Queueable {
   list<Id> recordids;
  public UpdateASPvaluefromRP (list<Id> recordIds) {
        this.recordIds = recordIds;
    }
    public void execute(QueueableContext context){
        
         List<Roll_Out_Schedule__c> updatelist =new list<Roll_Out_Schedule__c>();
        set<string> skus =new set<string>();
        set<string> ids =new set<string>();
        Map<string,List<SKU_ASP__c>> skumap=new  Map<string,List<SKU_ASP__c>>();
        for(Roll_Out_Product__c rp:[select id,name,SAP_Material_Code__c,SalesPrice__c from Roll_Out_Product__c where id in :recordids]){
            skus.add(rp.SAP_Material_Code__c);
            ids.add(rp.id);
        }
        
        
        if(skus.size()>0){
                List<SKU_ASP__c> asps=new list<SKU_ASP__c>([select id,name,month__c,year__C,ASP_Value__c,SAP_Material_ID__c from SKU_ASP__c where  SAP_Material_ID__c in :skus  order by Year__c,Month__c asc]);
                
                if(asps.size()>0){
                    
                     for(SKU_ASP__c s:asps){
                      if(skumap.containsKey(s.SAP_Material_ID__c)){
                                List<SKU_ASP__c> ol = skumap.get(s.SAP_Material_ID__c);
                                        ol.add(s);
                                       skumap.put(s.SAP_Material_ID__c, ol);
                            }else{
                               skumap.put(s.SAP_Material_ID__c, new List<SKU_ASP__c> { s }); 
                            }
                                
                       
                       
                   }
    
                    
                } 
                    
                    
                    
                    
                     for(Roll_Out_Product__c rp:[select id,name,SAP_Material_Code__c,SalesPrice__c,(select id,ASP__c,year__c,fiscal_month__c from Roll_Out_Schedules__r	) 
                                       from Roll_Out_Product__c where id in :ids]){
                                  
                               
                                  
                                  for(Roll_Out_Schedule__c ros: rp.Roll_Out_Schedules__r){
                                      List<SKU_ASP__c> asp=new List<SKU_ASP__c>();
                                      asp=skumap.get(rp.SAP_Material_Code__c);
                                       if(asp!=null && !asp.isEmpty()){
                                           for(SKU_ASP__c a :asp){  
                                                 Integer rosyear = integer.valueOf(ros.year__c);
                                                 Integer rosmnth = integer.valueOf(ros.fiscal_month__c);
                                                 Integer aspyear = integer.valueOf(a.year__c);

                                                     if(rosyear==aspyear&&rosmnth ==a.month__C){
                                                             System.debug('Same Month and Year ');
                                                             ros.ASP__c=a.ASP_Value__c;
                                                         }else if(rosyear==aspyear&&rosmnth > a.month__C){
                                                              System.debug('Same Year and ROS month is more than ASP. ');
                                                              ros.ASP__c=a.ASP_Value__c;
                                                         }else if(rosyear>aspyear){
                                                             ros.ASP__c=a.ASP_Value__c;
                                                         }
                                                         
                                               
                                         }
                                       }else{
                                            ros.ASP__c=rp.SalesPrice__c;
                                       }
                                          
                                          
                                          if(ros.ASP__c==null){
                                              
                                               ros.ASP__c=rp.SalesPrice__c;
                                          }
                                          
                                         
                                           updatelist.add(ros); 
                                           
                                           
                                       
                                  }
                                  
                   } 
         
                    
                
                
            
        }
        
        if(updatelist.size()>0){
            update updatelist;
            
        }
        
    }

}