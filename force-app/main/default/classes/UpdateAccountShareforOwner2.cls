global class UpdateAccountShareforOwner2 implements Database.Batchable<sObject>,database.stateful{
  global UpdateAccountShareforOwner2(){}
  
  global Database.QueryLocator start(Database.BatchableContext BC){
    return Database.getQueryLocator([Select id,Name,Owner2__c from Account where Owner2__c!=null ]);
  }
  
  global void execute(Database.BatchableContext BC, List<Account> scope){
      map<string,string> accmap=new map<string,string>();
      map<string,List<AccountShare>> acsharemap=new map<string,List<AccountShare>>();
      list<AccountShare> inseretShare=new list<AccountShare>();
      for(Account a:scope){
          accmap.put(a.id,a.Owner2__c);
      }
      
      if(accmap.size()>0){
          List<AccountShare> accshare=new list<AccountShare>([select id,UserOrGroupId,accountId,accountAccessLevel,opportunityAccessLevel,caseAccessLevel from AccountShare where userOrgroupId in :accmap.values() and Accountid=:accmap.keyset()]);
          for(AccountShare ah: accshare){
              if(acsharemap.containsKey(ah.accountID)){
                  List<AccountShare> ashr=acsharemap.get(ah.accountID);
                   ashr.add(ah);
                   acsharemap.put(ah.accountID,ashr);
              }else{
                  acsharemap.put(ah.accountID,new list<AccountShare> {ah});
              }
              
          }
      }
      
      for(Account a:scope){
          if(acsharemap.containsKey(a.id)){
               List<AccountShare> ashr=acsharemap.get(a.id);
               boolean recordexist=false;
               for(AccountShare asr: ashr){
                  if(asr.accountAccessLevel=='Edit'&&asr.opportunityAccessLevel=='Edit' && asr.UserOrGroupId ==a.owner2__c){
                      recordexist=true;
                  }   
               }
              if(recordexist=false){
                  inseretShare.add(new AccountShare(accountId=a.id, userOrgroupId=a.owner2__c, accountAccessLevel = 'Edit',opportunityAccessLevel = 'Edit', caseAccessLevel='None'));
              } 
               
          }else{
            inseretShare.add(new AccountShare(accountId=a.id, userOrgroupId=a.owner2__c, accountAccessLevel = 'Edit',opportunityAccessLevel = 'Edit', caseAccessLevel='None'));
          }
      }
      
      if(inseretShare.size()>0){
          database.insert(inseretShare, false);
          
      }
      
  }
  
  global void finish(Database.BatchableContext BC){
   }
   
}