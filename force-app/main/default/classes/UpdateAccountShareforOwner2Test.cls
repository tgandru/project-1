@isTest
private class UpdateAccountShareforOwner2Test {

	private static testMethod void testmethod1() {
       Channelinsight.CI_TEST_TestUtils.createBaseInstallData();

        Account a = TestDataUtility.createAccount('Account 1');


         Profile p1 = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
         Profile p2 = [SELECT Id FROM Profile WHERE Name='Integration User']; 
        list<user> ur=new list<User>();
        User u1 = new User(Alias = 'standt', Email='sysAdminuser@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p1.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName=TestDataUtility.generateRandomString(7)+'@testorg.com');
       User u2 = new User(Alias = 'standt', Email='sysAdminuser21@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p2.Id, Sales_Team__c='Finance',
      TimeZoneSidKey='America/Los_Angeles', UserName=TestDataUtility.generateRandomString(7)+'@testorg.com');
        ur.add(u1);
         ur.add(u2);
         insert ur;
         
         
         a.Owner2__c=u2.id;
         insert a;
         
         Test.startTest();
         database.executeBatch(new UpdateAccountShareforOwner2());
         Test.stopTest();
	}

}