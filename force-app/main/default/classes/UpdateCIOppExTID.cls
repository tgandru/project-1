/*
Created By - Vijay 7/5/2018
Purpose - To Updtae the External ID field in CI_Opportunity object with Quote SPA ID(Old Quote)
CI_OPP --Opportunity -- Quote(Approved or Presented Quotes)

*/

global class UpdateCIOppExTID implements Database.Batchable<sObject>{//,Database.Stateful 
     global UpdateCIOppExTID(){
         
     }
      global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([select id,Channelinsight__OPP_External_Id__c,Channelinsight__OPP_Opportunity__c from Channelinsight__CI_Opportunity__c where Channelinsight__OPP_External_Id__c=null ]); // 
       } 
       global void execute(Database.BatchableContext BC, List<Channelinsight__CI_Opportunity__c> scope){
           Map<String,List<Quote>> qtMap=New  Map<String,List<Quote>>();
           List<string> oppid=new list<string>();
           for(Channelinsight__CI_Opportunity__c c:scope){
               oppid.add(c.Channelinsight__OPP_Opportunity__c);
           }
           if(oppid.size()>0){
               for(Quote q:[select id,name,OpportunityID,External_SPID__c,CreatedDate,status,QuoteNumber,Legacy_id__c from Quote where OpportunityID in :oppid and ((status='Approved' or status='Presented') or(VersionNumber__c>0))order By CreatedDate Asc]){
                   if(qtMap.containsKey(q.OpportunityID)){
                       List<Quote> qt=qtMap.get(q.OpportunityID);
                       qt.Add(q);
                       qtMap.Put(q.OpportunityID,qt);
                   }else{
                       qtMap.Put(q.OpportunityID,new list<Quote>{q});
                   }
                   
               }
           }
           List<Channelinsight__CI_Opportunity__c> updatelist=new List<Channelinsight__CI_Opportunity__c>();
           for(Channelinsight__CI_Opportunity__c c:scope){
               try{
                   List<Quote> qts=qtMap.get(c.Channelinsight__OPP_Opportunity__c);
                   if(!qts.isEmpty()){
                       //c.Channelinsight__OPP_External_Id__c=qts[0].External_SPID__c;
                       if(qts[0].Legacy_id__c ==null){
                               c.Channelinsight__OPP_External_Id__c='SP-'+qts[0].QuoteNumber ;
                           }else{
                           c.Channelinsight__OPP_External_Id__c=qts[0].External_SPID__c;
                               
                           }
                       
                       updatelist.add(c);
                   }
                  
                   
                   
               }catch(exception ex){
                   
               }
               
           }
           
          update updatelist; 
       }
       
        global void finish(Database.BatchableContext BC){}
    
}