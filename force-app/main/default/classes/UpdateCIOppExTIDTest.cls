@isTest
private class UpdateCIOppExTIDTest {
   static Account testMainAccount;
    static Account testCustomerAccount;
    static List<Opportunity> testOpportunityList;
    static Opportunity oppOpen;
    static Order orderRecord1;
    static Order orderRecord2;
    static Pricebook2 pb;
    static PricebookEntry standaloneProdPBE;
    static Quote quot;
    static QuoteLineItem standardQLI;
    static QuoteLineItem standardQLI2;
    static QuoteLineItem CustomQLI2;
    static Id endUserRT = TestDataUtility.retrieveRecordTypeId('End_User', 'Account'); 
    static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
    static Id demoAcctRtId = TestDataUtility.retrieveRecordTypeId('ADL','Order');
    static Id demoOppRtId1 = TestDataUtility.retrieveRecordTypeId('Evaluation','Order');
    
    private static void setup() {
        //Jagan: Test data for fiscalCalendar__c
        Test.loadData(fiscalCalendar__c.sObjectType, 'FiscalCalendarTestData');
        
        //Channelinsight configuration
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();

        Zipcode_Lookup__c zip=TestDataUtility.createZipcode();
        insert zip;
        
        //Creating test Account
        testMainAccount = TestDataUtility.createAccount(TestDataUtility.generateRandomString(5));
        testMainAccount.RecordTypeId = endUserRT;
        insert testMainAccount;
        
        testCustomerAccount = TestDataUtility.createAccount(TestDataUtility.generateRandomString(5));
        testCustomerAccount.RecordTypeId = directRT;
        insert testCustomerAccount;
        
        //Creating custom Pricebook
        pb = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(4)); 
        insert pb;
        
       
          Opportunity  oppOpen=TestDataUtility.createOppty('Open Oppty',testMainAccount, pb, 'RFP', 'IT', 'FAX/MFP', 'Identified');
           
        insert oppOpen;
        
        //Creating Products
        List<Product2> prods=new List<Product2>();
            Product2 standaloneProd=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Standalone Prod1', 'Printer[C2]', 'OFFICE', 'H/W');
            prods.add(standaloneProd);
            Product2 parentProd1=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Parent Prod1', 'Printer[C2]', 'OFFICE', 'H/W');
            prods.add(parentProd1);
            Product2 childProd1=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod1', 'Printer[C2]', 'SMART_PHONE', 'Service pack');
            prods.add(childProd1);
            Product2 childProd2=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod2', 'Printer[C2]', 'SMART_PHONE', 'Service pack');
            prods.add(childProd2);
            Product2 parentProd2=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Parent Prod2', 'Printer[C2]', 'FAX/MFP', 'H/W');
            prods.add(parentProd2);
            Product2 childProd3=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod3', 'Printer[C2]', 'PRINTER', 'Service pack');
            prods.add(childProd3);
            Product2 childProd4=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod4', 'Printer[C2]', 'ACCESSARY', 'Service pack');
            prods.add(childProd4);

        insert prods;
        
        List<PriceBookEntry> pbes=new List<PriceBookEntry>();
            standaloneProdPBE=TestDataUtility.createPriceBookEntry(standaloneProd.Id, pb.Id);
            standaloneProdPBE.Product2 = standaloneProd;
            standaloneProdPBE.UnitPrice = 100;
            pbes.add(standaloneProdPBE);
            PriceBookEntry parentProd1PBE=TestDataUtility.createPriceBookEntry(parentProd1.Id,pb.Id);
            parentProd1PBE.Product2 = parentProd1;
            parentProd1PBE.UnitPrice = 120;
            pbes.add(parentProd1PBE);
            PriceBookEntry childProd1PBE=TestDataUtility.createPriceBookEntry(childProd1.Id, pb.Id);
            childProd1PBE.Product2 = childProd1;
            pbes.add(childProd1PBE);
            PriceBookEntry childProd2PBE=TestDataUtility.createPriceBookEntry(childProd2.Id, pb.Id);
            childProd2PBE.Product2 = childProd2;
            pbes.add(childProd2PBE);
            PriceBookEntry parentProd2PBE=TestDataUtility.createPriceBookEntry(parentProd2.Id, pb.Id);
            parentProd2PBE.Product2 = parentProd2;
            pbes.add(parentProd2PBE);

        insert pbes;

        List<OpportunityLineItem> opportunityLineItems = new List<OpportunityLineItem>();
        OpportunityLineItem op1 = TestDataUtility.createOpptyLineItem(standaloneProd, standaloneProdPBE, oppOpen);
        op1.Quantity = 100;
        op1.TotalPrice = 1000;
        op1.Requested_Price__c = 100;
        op1.Claimed_Quantity__c = 45;     
        opportunityLineItems.add(op1);
        
        insert opportunityLineItems;
        
       
        
        quot = TestDataUtility.createQuote('Test Quote', oppOpen, pb);
        quot.ROI_Requestor_Id__c=UserInfo.getUserId();
        quot.Status = 'Approved';
        insert quot;
        
        List<QuoteLineItem> qlis = new List<QuoteLineItem>();
        standardQLI = TestDataUtility.createQuoteLineItem(quot.Id, standaloneProd, standaloneProdPBE, op1);
        standardQLI.Quantity = 25;
        standardQLI.UnitPrice = 100;
        standardQLI.Requested_Price__c=100;
        standardQLI.OLIID__c = op1.id;
        qlis.add(standardQLI);
        standardQLI2 = TestDataUtility.createQuoteLineItem(quot.Id, parentProd1, parentProd1PBE, op1);
        standardQLI2.Quantity = 25;
        standardQLI2.UnitPrice = 100;
        standardQLI2.Requested_Price__c=106;
        standardQLI2.OLIID__c = op1.id;
        qlis.add(standardQLI2);
        
        insert qlis;
    }




	private static testMethod void testmethod1() {
         setup();
         test.startTest();
         Opportunity op=[select id from Opportunity ];
         
         Quote q=[select id, opportunityId, status from quote];
         	q.Status='Approved';
         	q.VersionNumber__c=1;
		update q;
		Channelinsight__CI_Opportunity__c cop=[select id,Channelinsight__OPP_External_Id__c,Channelinsight__OPP_Opportunity__c from Channelinsight__CI_Opportunity__c where Channelinsight__OPP_Opportunity__c=:op.id];
         cop.Channelinsight__OPP_External_Id__c=null;
         update cop;
         Database.executeBatch(new UpdateCIOppExTID());
         test.stopTest();
	}

}