public class UpdateCMinvoiceNumController {
//This class is to update the Invoice Number and Create MQ 
 Public Credit_Memo__c cm{get; set;}
 public string inv{get; set;}
 public string err{get; set;}
 public Boolean showpg {get;set;}
    public UpdateCMinvoiceNumController(ApexPages.StandardController controller) {
      cm=(Credit_Memo__c) controller.getRecord();
      inv=cm.InvoiceNumber__c;
      showpg=false;
      Boolean grpMem=false;
      boolean Historypr=false;
      for (GroupMember gm : [SELECT Id, group.id, group.name, group.type
						FROM GroupMember
						WHERE (UserOrGroupId = :UserInfo.getUserId() AND group.Name='CM Invoice Num Manage')]){
						   grpMem=true;
						}
						
      for(Credit_Memo__History c:[SELECT Field,Id,IsDeleted,NewValue,OldValue,ParentId FROM Credit_Memo__History WHERE ParentId =:cm.id and Field='SAP_Billing_Number__c']){
          Historypr=true;
      } 
      if(grpMem==true&&Historypr==false){
        showpg=true;
        err='';
      }else if(grpMem==false){
          showpg=false;
          err='You are not allowed to update Invoice Number';
      }else if(Historypr==true){
          showpg=false;
          err='This record already processed to GERP once';
      }
      
    }
    
  Public Pagereference saverec(){
  try{
    cm.Integration_Status__c=null;
    Update cm;
    
    string layoutId = (Test.isRunningTest() || Integration_EndPoints__c.getInstance('Flow-010').layout_id__c==null )? 'asdf' : Integration_EndPoints__c.getInstance('Flow-010').layout_id__c;
    Message_Queue__c mq = new Message_Queue__c(Integration_Flow_Type__c = 'Flow-010', retry_counter__c = 0, Identification_Text__c = cm.id, Status__c='not started',Object_Name__c='Credit Memo', IFID__c=layoutId);
    insert mq;
    
     return  new pageReference('/'+cm.id);
    }catch(exception ex){
     System.debug('Exception ::'+ex);
     string err=String.valueof(ex);
     if(err.Contains('DUPLICATE_VALUE')){
     
       ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Assignment Number is Duplicate , Please give correct Invoice Number')); 
           }else{
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,err));
          }

      return null;
    
    }
  }  
   
}