@isTest
private class UpdateCMinvoiceNumControllerTest {
private static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
	 static testMethod void testmethod1() {
         Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Account account = TestDataUtility.createAccount('Marshal Mathers');
        insert account;
        Account partnerAcc = TestDataUtility.createAccount('Distributor account');
        partnerAcc.RecordTypeId = directRT;
        partnerAcc.SAP_Company_Code__c = '0000121233';
        insert partnerAcc;
       
        Credit_Memo__c c = TestDataUtility.createCreditMemo();
        //cm.Credit_Memo_Number__c = TestDataUtility.generateRandomString(5);
        c.InvoiceNumber__c = 'TEST_001';
        c.Direct_Partner__c = partnerAcc.id;
        c.Division__c = 'Mobile';
        c.SAP_Billing_Number__c='00113322';
        insert c;
         Credit_Memo__c cmm=c;
         cmm.SAP_Billing_Number__c='0011332200';
         update cmm;
         test.startTest();
        
        Pagereference pf= Page.UpdateCMinvoiceNum;
        pf.getParameters().put('id',c.id);
        test.setCurrentPage(pf);
        
        apexPages.standardcontroller sc = new apexPages.standardcontroller(c);
                UpdateCMinvoiceNumController obj = new UpdateCMinvoiceNumController(sc);
                obj.cm=c;
                obj.cm.Direct_Partner__c=null;
                         obj.saverec();
        test.stopTest();
      
	}
	static testMethod void testmethod2() {
	            Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Account account = TestDataUtility.createAccount('Marshal Mathers');
        insert account;
        Account partnerAcc = TestDataUtility.createAccount('Distributor account');
        partnerAcc.RecordTypeId = directRT;
        partnerAcc.SAP_Company_Code__c = '0000121233';
        insert partnerAcc;
       
        Credit_Memo__c c = TestDataUtility.createCreditMemo();
        //cm.Credit_Memo_Number__c = TestDataUtility.generateRandomString(5);
        c.InvoiceNumber__c = 'TEST_002';
        c.Direct_Partner__c = partnerAcc.id;
        c.Division__c = 'Mobile';
        c.SAP_Billing_Number__c='00113322';
        insert c;
         Credit_Memo__c cmm=c;
         cmm.SAP_Billing_Number__c='0011332200';
         update cmm;
         test.startTest();
        
        Pagereference pf= Page.UpdateCMinvoiceNum;
        pf.getParameters().put('id',c.id);
        test.setCurrentPage(pf);
        
        apexPages.standardcontroller sc = new apexPages.standardcontroller(c);
                UpdateCMinvoiceNumController obj = new UpdateCMinvoiceNumController(sc);
               
                         obj.saverec();
        test.stopTest(); 
	}
	
	
	

}