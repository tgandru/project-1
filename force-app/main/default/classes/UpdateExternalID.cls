global class UpdateExternalID implements Database.Batchable<sObject>{


   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator([SELECT id,Name,Pricebook2Id,Product2Id,SAP_Material_ID__c,UnitPrice FROM PricebookEntry WHERE External_Id__c = null and CreatedDate=Today]);// CreatedDate=Today and External_Id__c = null
   }

   global void execute(Database.BatchableContext BC, List<PricebookEntry> scope){
           for(PricebookEntry p:Scope ){
              string exid=string.valueof(p.Product2Id) +string.valueof(p.Pricebook2Id);
              system.debug('*************'+exid);
             p.External_Id__c=string.valueof(exid);
           }
            update scope;
    }

   global void finish(Database.BatchableContext BC){
   }
   
   
}