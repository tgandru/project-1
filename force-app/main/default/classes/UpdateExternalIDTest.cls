@isTest
private class UpdateExternalIDTest {

	private static testMethod void test() {
	     Product2 prod = new Product2(Name = 'ABC Test Prod ', 
            Family = 'ACCESSORY',SAP_Material_ID__c='987123abc');
        insert prod;
          Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
          PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = customPB.id, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
         test.startTest();
            database.executeBatch(new UpdateExternalID());
        test.stopTest();
        
	}

}