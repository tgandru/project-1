/*
*Description: Extension for UpdateInvoicePrice VF page. This does a real time call out to get the latest invoice prices.
*
*/
public with sharing class UpdateInvoicePriceExtension {
    
    Quote quoteObj;
    Public boolean showcancel {get;set;}
    public UpdateInvoicePriceExtension(ApexPages.StandardController controller) {
        quoteObj = (Quote) controller.getRecord();
        quoteObj = [select id, Name, OpportunityId,ProductGroupMSP__c,BEP__c,MPS__c,Supplies_Buying_Location__c,Status, Division__c,
                    Approval_Requestor_Id__c, Last_Update_from_CastIron__c,Integration_Status__c,ISR_Rep__c,Quote_Approval__c
                    from Quote where Id=:quoteObj.Id];
        showcancel = false;            
        
    }
    
    public PageReference doRealtimeIntegration(){
        try{
            //Proceed only if the integration status is null and(Added by Thiru.. Division not equal to SBS..)
            
            
            if(quoteObj.Division__c == 'IT' || quoteObj.Division__c == 'Mobile'){
                
                    if(quoteObj.ProductGroupMSP__c != null){
                        for(string s :quoteObj.ProductGroupMSP__c.split(';')){
                            if(s == 'KNOX'){
                                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'There is no need to update Invoice Price for KNOX Product Group.'));
                                showcancel = true;  
                                return null;
                            }
                        }
                    }
             
            }
             
            if(quoteObj.Integration_Status__c != null || quoteObj.Division__c == 'SBS' || quoteObj.Division__c == 'W/E' ){
                if(quoteObj.Division__c == 'SBS'){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'For SBS Division no need to update Invoice price'));
                }
                if(quoteObj.Division__c == 'W/E'){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'For W/E Division no need to update Invoice price'));
                }
                showcancel = true;
                if(quoteObj.Integration_Status__c != null){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Cannot update Invoice price as it is already executed once'));
                }
                return null;
            }
            else{
                if(quoteObj.OpportunityId != null){
                    //Primary distributor check. Proceed only if there are any primary distributors
                    List<Partner__c> lstpartnr = new List<Partner__c>();
                    lstpartnr = [Select Id,Role__c,Is_Primary__c,Opportunity__c,Partner__r.SAP_Company_Code__c from Partner__c where Opportunity__c = :quoteObj.OpportunityId AND Role__c='Distributor' and Is_Primary__c = True ];
                   if(lstpartnr.size()>0){
                     for(Partner__c p:lstpartnr){
                         if(p.Partner__r.SAP_Company_Code__c == null){// Added By Vijay on 11/20 to block Sending request to GERP if Primary DIST sold to code in Null
                             showcancel = true;
                             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'The Sold to Code is Blank for primary distributor related Opportunity. Please Contact System Admin.'));     
                             return null;
                         }
                     }
                   }
                    if(lstpartnr.size() == 0){
                        showcancel = true;
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'There is no primary distributor for the related Opportunity. Please add a primary distributor before updating the Invoice Price.'));     
                        return null;
                    }
                    else{
                        if(quoteObj.Division__c=='IT' ||quoteObj.Division__c=='Mobile'){
                            quoteObj.Approval_Requestor_Id__c=UserInfo.getUserId();
                            quoteObj.Integration_Status__c='pending';
                            update quoteObj;
                            //Real time integration call
                            if(!Test.isRunningTest())
                                CastIronIntegrationHelper.sendMQrealTime(quoteObj.Id);    
                            return new PageReference ('/'+quoteObj.Id);
                        }
                    }
                }
                return null;
            }
            
        }catch(exception e){  showcancel = true;    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Failed to update quote due to the error '+e.getMessage()));       return null;  }
    }
    
    public pageReference cancel(){
        return new PageReference ('/'+quoteObj.Id);
    }
    
    
    

}