@isTest
private class UpdateInvoicePriceExtension_Test {

    private static testMethod void testmethod1() {
         Id endUserRT = TestDataUtility.retrieveRecordTypeId('End_User', 'Account'); 
        Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
    
        //Jagan: Test data for fiscalCalendar__c
        Test.loadData(fiscalCalendar__c.sObjectType, 'FiscalCalendarTestData');

        //Channelinsight configuration
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();

        Zipcode_Lookup__c zip=TestDataUtility.createZipcode();
        insert zip;
        
        //Creating test Account
        Account acct=TestDataUtility.createAccount(TestDataUtility.generateRandomString(5));
        acct.RecordTypeId = endUserRT;
        insert acct;
        
        
        //Creating the Parent Account for Partners
        Account paacct=TestDataUtility.createAccount(TestDataUtility.generateRandomString(7));
        acct.RecordTypeId = directRT;
        insert paacct;
        
        
        //Creating custom Pricebook
        PriceBook2 pb = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(4)); 
        insert pb;
        
        
        //Creating list of Opportunities
        List<Opportunity> opps=new List<Opportunity>();
        Opportunity oppOpen=TestDataUtility.createOppty('New Opp from Test setup',acct, pb, 'RFP', 'IT', 'FAX/MFPz', 'Identified');
        opps.add(oppOpen);
        /*Opportunity oppClosed=TestDataUtility.createOppty('Closed Oppty',acct, pb, 'RFP', 'IT', 'FAX/MFP', 'Drop');
        opps.add(oppClosed);*/
        
        insert opps;
        

        //Creating the partner for that opportunity
        Partner__c partner = TestDataUtility.createPartner(oppOpen,acct,paacct,'Distributor');
        partner.is_primary__c = true;
        insert partner;


        /* Creating Products: SAP_Material_Id__C should be unique and case sensitive ID
        Used the utility method to generate random string for the SAP_Material_ID as it should be unique 
        when every time you passed on
        */
        
        //Creating Products
        List<Product2> prods=new List<Product2>();
        Product2 standaloneProd=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Standalone Prod1', 'Printer[C2]', 'FAX/MFP', 'H/W');
        prods.add(standaloneProd);
        Product2 parentProd1=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Parent Prod1', 'Printer[C2]', 'FAX/MFP', 'H/W');
        prods.add(parentProd1);
        Product2 childProd1=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod1', 'Printer[C2]', 'FAX/MFP', 'Service pack');
        prods.add(childProd1);
        Product2 childProd2=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod2', 'Printer[C2]', 'FAX/MFP', 'Service pack');
        prods.add(childProd2);
        Product2 parentProd2=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Parent Prod2', 'Printer[C2]', 'FAX/MFP', 'H/W');
        prods.add(parentProd2);
        Product2 childProd3=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod3', 'Printer[C2]', 'FAX/MFP', 'Service pack');
        prods.add(childProd3);
        Product2 childProd4=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod4', 'Printer[C2]', 'FAX/MFP', 'Service pack');
        prods.add(childProd4);
        Product2 childProd5Standalone=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12), 'child AND standalone prod5', 'Printer[C2]', 'FAX/MFP', 'Service pack');
        prods.add(childProd5Standalone);
        insert prods;

        Id pricebookId = Test.getStandardPricebookId();

        //CreatePBEs
        //create the pricebook entries for the custom pricebook
        List<PriceBookEntry> custompbes=new List<PriceBookEntry>();
        PriceBookEntry standaloneProdPBE=TestDataUtility.createPriceBookEntry(standaloneProd.Id, pb.Id);
        custompbes.add(standaloneProdPBE);
        PriceBookEntry parentProd1PBE=TestDataUtility.createPriceBookEntry(parentProd1.Id, pb.Id);
        custompbes.add(parentProd1PBE);
        PriceBookEntry parentProd2PBE=TestDataUtility.createPriceBookEntry(parentProd2.Id, pb.Id);
        custompbes.add(parentProd2PBE);
        PriceBookEntry childProd1PBE=TestDataUtility.createPriceBookEntry(childProd1.Id, pb.Id);
        custompbes.add(childProd1PBE);
        PriceBookEntry childProd2PBE=TestDataUtility.createPriceBookEntry(childProd2.Id, pb.Id);
        custompbes.add(childProd2PBE);
        PriceBookEntry childProd3PBE=TestDataUtility.createPriceBookEntry(childProd3.Id, pb.Id);
        custompbes.add(childProd3PBE);
        PriceBookEntry childProd4PBE=TestDataUtility.createPriceBookEntry(childProd4.Id, pb.Id);
        custompbes.add(childProd4PBE);
        PriceBookEntry childProd5StandalonePBE=TestDataUtility.createPriceBookEntry(childProd5Standalone.Id, pb.Id);
        custompbes.add(childProd5StandalonePBE);
        insert custompbes;

        //insert product relationship
        List<Product_Relationship__c> prs=new List<Product_Relationship__c>();
        Product_Relationship__c pr1=TestDataUtility.createProductRelationships(childProd1, parentProd1);
        prs.add(pr1);
        Product_Relationship__c pr2=TestDataUtility.createProductRelationships(childProd2, parentProd1);
        prs.add(pr2);
        Product_Relationship__c pr3=TestDataUtility.createProductRelationships(childProd3, parentProd2);
        prs.add(pr3);
        Product_Relationship__c pr4=TestDataUtility.createProductRelationships(childProd4, parentProd2);
        prs.add(pr4);
        Product_Relationship__c pr5=TestDataUtility.createProductRelationships(childProd5Standalone, parentProd2);
        prs.add(pr5);
        insert prs;

        // Creating opportunity Line item
        List<OpportunityLineItem> olis=new List<OpportunityLineItem>();
        OpportunityLineItem standaloneProdOLI=TestDataUtility.createOpptyLineItem(standaloneProd, standaloneProdPBE, oppOpen);
        olis.add(standaloneProdOLI);
        OpportunityLineItem parentProd1OLI=TestDataUtility.createOpptyLineItem(parentProd1, parentProd1PBE, oppOpen);
        parentProd1OLI.Parent_Product__c=parentProd1.Id;
        olis.add(parentProd1OLI);
        OpportunityLineItem childProd1OLI=TestDataUtility.createOpptyLineItem(childProd1, childProd1PBE, oppOpen);
        childProd1OLI.Parent_Product__c=parentProd1.Id;
        olis.add(childProd1OLI);
        OpportunityLineItem childProd2OLI=TestDataUtility.createOpptyLineItem(childProd2, childProd2PBE, oppOpen);
        childProd2OLI.Parent_Product__c=parentProd1.Id;
        olis.add(childProd2OLI);
        OpportunityLineItem parentProd2OLI=TestDataUtility.createOpptyLineItem(parentProd2, parentProd2PBE, oppOpen);
        parentProd2OLI.Parent_Product__c=parentProd2.Id;
        olis.add(parentProd2OLI);
        OpportunityLineItem childProd3OLI=TestDataUtility.createOpptyLineItem(childProd3, childProd3PBE, oppOpen);
        childProd3OLI.Parent_Product__c=parentProd2.Id;
        olis.add(childProd3OLI);
        OpportunityLineItem childProd4OLI=TestDataUtility.createOpptyLineItem(childProd4, childProd4PBE, oppOpen);
        childProd4OLI.Parent_Product__c=parentProd2.Id;
        olis.add(childProd4OLI);
        OpportunityLineItem childProd5OLI=TestDataUtility.createOpptyLineItem(childProd5Standalone, childProd5StandalonePBE, oppOpen);
        childProd5OLI.Parent_Product__c=parentProd2.Id;
        olis.add(childProd5OLI);
        
        insert olis;


        OpportunityLineItem childProd5StandaloneOLI=TestDataUtility.createOpptyLineItem(childProd5Standalone, childProd5StandalonePBE, oppOpen);
        insert childProd5StandaloneOLI;

         System.debug('lclc delete later '+[select id from opportunitylineitem].size());
         System.debug('lclc 2 delete later '+[select id,Product_Group__c from opportunitylineitem]);

        //create quote
        Quote quot = TestDataUtility.createQuote('Test Quote', oppOpen, pb);
        quot.ROI_Requestor_Id__c=UserInfo.getUserId();
        quot.Division__c = 'IT';
        quot.Status='Draft';
        insert quot;
        System.debug('lclc quote delete '+quot.ProductGroupMSP__c);


        //create qlis
        List<QuoteLineItem> qlis=new List<QuoteLineItem>();
        QuoteLineItem standaloneProdQLI=TestDataUtility.createQuoteLineItem(quot.Id, standaloneProd, standaloneProdPBE, standaloneProdOLI);
        qlis.add(standaloneProdQLI);
        QuoteLineItem parentProd2QLI=TestDataUtility.createQuoteLineItem(quot.Id, parentProd2, parentProd2PBE, parentProd2OLI);
        parentProd2QLI.Parent_Product__c=parentProd2.Id;
        qlis.add(parentProd2QLI);
        QuoteLineItem childProd3QLI=TestDataUtility.createQuoteLineItem(quot.Id, childProd3, childProd3PBE, childProd3OLI);
        childProd3QLI.Parent_Product__c=parentProd2.Id;
        qlis.add(childProd3QLI);
        QuoteLineItem childProd4QLI=TestDataUtility.createQuoteLineItem(quot.Id, childProd4, childProd4PBE, childProd4OLI);
        childProd4QLI.Parent_Product__c=parentProd2.Id;
        qlis.add(childProd4QLI);
        QuoteLineItem childProd5QLI=TestDataUtility.createQuoteLineItem(quot.Id, childProd5Standalone, childProd5StandalonePBE, childProd5OLI);
        childProd5QLI.Parent_Product__c=parentProd2.Id;
        qlis.add(childProd5QLI);
        QuoteLineItem childProd5StandaloneQLI=TestDataUtility.createQuoteLineItem(quot.Id, childProd5Standalone, childProd5StandalonePBE, childProd5StandaloneOLI);
        qlis.add(childProd5StandaloneQLI);
        System.debug('lclc for childProd5StandaloneQLI whats pbe  '+childProd5StandaloneQLI.pricebookentryId);
        insert qlis;

        //quot.Status='Approved';
        //update quot;

        ApexPages.StandardController c = new ApexPages.StandardController(quot);
        UpdateInvoicePriceExtension upe = new UpdateInvoicePriceExtension(c);
        upe.doRealtimeIntegration();
        quot.Integration_Status__c = 'Success';
        //update quot;
        
        ApexPages.StandardController c2 = new ApexPages.StandardController(quot);
        UpdateInvoicePriceExtension upe2 = new UpdateInvoicePriceExtension(c2);
        upe2.doRealtimeIntegration();
        upe2.cancel();
        
        delete partner;
        ApexPages.StandardController c3 = new ApexPages.StandardController(quot);
        UpdateInvoicePriceExtension upe3 = new UpdateInvoicePriceExtension(c3);
        upe3.doRealtimeIntegration();
        
       /* PageReference pageRef = Page.QuoteSubmitApprovalButtonCustom;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('id',quot.Id);
            QuoteSubmitApprovalButtonCustomExtension cont = new QuoteSubmitApprovalButtonCustomExtension(new ApexPages.StandardController(quot));
            cont.submitApproval();
            cont.submitApproval2();
            cont.submitApprovalMobile();
            cont.returnToQuote();
            cont.postToChatterNegative(quot);
            cont.postToChatterPositive(quot); */
    }
    
        
    private static testMethod void testmethod2() {
        
           Id endUserRT = TestDataUtility.retrieveRecordTypeId('End_User', 'Account'); 
        Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
    
        //Jagan: Test data for fiscalCalendar__c
        Test.loadData(fiscalCalendar__c.sObjectType, 'FiscalCalendarTestData');

        //Channelinsight configuration
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();

        Zipcode_Lookup__c zip=TestDataUtility.createZipcode();
        insert zip;
        
        //Creating test Account
        Account acct=TestDataUtility.createAccount(TestDataUtility.generateRandomString(5));
        acct.RecordTypeId = endUserRT;
        insert acct;
        
        
        //Creating the Parent Account for Partners
        Account paacct=TestDataUtility.createAccount(TestDataUtility.generateRandomString(7));
        acct.RecordTypeId = directRT;
         acct.SAP_Company_Code__c='9969695949';
        insert paacct;
        
        
        //Creating custom Pricebook
        PriceBook2 pb = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(4)); 
        insert pb;
        
        
        //Creating list of Opportunities
        List<Opportunity> opps=new List<Opportunity>();
        Opportunity oppOpen=TestDataUtility.createOppty('New Opp from Test setup',acct, pb, 'RFP', 'IT', 'KNOX', 'Identified');
        opps.add(oppOpen);
        /*Opportunity oppClosed=TestDataUtility.createOppty('Closed Oppty',acct, pb, 'RFP', 'IT', 'FAX/MFP', 'Drop');
        opps.add(oppClosed);*/
        
        insert opps;
        

        //Creating the partner for that opportunity
        Partner__c partner = TestDataUtility.createPartner(oppOpen,acct,paacct,'Distributor');
        partner.is_primary__c = true;
        insert partner;
         List<Product2> prods=new List<Product2>();
        Product2 standaloneProd=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Standalone Prod1', 'MOBILE PHONE [G1]', 'KNOX', 'H/W');
        prods.add(standaloneProd);
        Product2 parentProd1=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Parent Prod1', 'MOBILE PHONE [G1]', 'KNOX', 'H/W');
        prods.add(parentProd1);
        insert prods;
        Id pricebookId = Test.getStandardPricebookId();

        List<PriceBookEntry> custompbes=new List<PriceBookEntry>();
        PriceBookEntry standaloneProdPBE=TestDataUtility.createPriceBookEntry(standaloneProd.Id, pb.Id);
        custompbes.add(standaloneProdPBE);
        PriceBookEntry parentProd1PBE=TestDataUtility.createPriceBookEntry(parentProd1.Id, pb.Id);
        custompbes.add(parentProd1PBE);
        insert custompbes;
        List<OpportunityLineItem> olis=new List<OpportunityLineItem>();
        OpportunityLineItem standaloneProdOLI=TestDataUtility.createOpptyLineItem(standaloneProd, standaloneProdPBE, oppOpen);
        olis.add(standaloneProdOLI);
        OpportunityLineItem parentProd1OLI=TestDataUtility.createOpptyLineItem(parentProd1, parentProd1PBE, oppOpen);
        parentProd1OLI.Parent_Product__c=parentProd1.Id;
        olis.add(parentProd1OLI);
        insert olis;
        Quote quot = TestDataUtility.createQuote('Test Quote', oppOpen, pb);
        quot.ROI_Requestor_Id__c=UserInfo.getUserId();
        quot.Division__c = 'IT';
        quot.ProductGroupMSP__c = 'KNOX';
        quot.Status='Draft';
       
        insert quot;
      
        List<QuoteLineItem> qlis=new List<QuoteLineItem>();
        QuoteLineItem standaloneProdQLI=TestDataUtility.createQuoteLineItem(quot.Id, standaloneProd, standaloneProdPBE, standaloneProdOLI);
        qlis.add(standaloneProdQLI);
        QuoteLineItem parentProd1QLI=TestDataUtility.createQuoteLineItem(quot.Id, parentProd1, parentProd1PBE, parentProd1OLI);
        parentProd1QLI.Parent_Product__c=parentProd1.Id;
        qlis.add(parentProd1QLI);
        insert qlis;
        ApexPages.StandardController c = new ApexPages.StandardController(quot);
        UpdateInvoicePriceExtension upe = new UpdateInvoicePriceExtension(c);
        upe.doRealtimeIntegration();
    }
   private static testMethod void testmethod3() {
       
        Id endUserRT = TestDataUtility.retrieveRecordTypeId('End_User', 'Account'); 
        Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
    
        //Jagan: Test data for fiscalCalendar__c
        Test.loadData(fiscalCalendar__c.sObjectType, 'FiscalCalendarTestData');

        //Channelinsight configuration
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();

        Zipcode_Lookup__c zip=TestDataUtility.createZipcode();
        insert zip;
        
        //Creating test Account
        Account acct=TestDataUtility.createAccount(TestDataUtility.generateRandomString(5));
        acct.RecordTypeId = endUserRT;
        insert acct;
        
        
        //Creating the Parent Account for Partners
        Account paacct=TestDataUtility.createAccount(TestDataUtility.generateRandomString(7));
        acct.RecordTypeId = directRT;
        insert paacct;
        
        
        //Creating custom Pricebook
        PriceBook2 pb = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(4)); 
        insert pb;
        
        
        //Creating list of Opportunities
        List<Opportunity> opps=new List<Opportunity>();
        Opportunity oppOpen=TestDataUtility.createOppty('New Opp from Test setup',acct, pb, 'RFP', 'SBS', 'SOLUTION [MOBILE]', 'Identified');
        opps.add(oppOpen);
        /*Opportunity oppClosed=TestDataUtility.createOppty('Closed Oppty',acct, pb, 'RFP', 'IT', 'FAX/MFP', 'Drop');
        opps.add(oppClosed);*/
        
        insert opps;
        

        //Creating the partner for that opportunity
        Partner__c partner = TestDataUtility.createPartner(oppOpen,acct,paacct,'Distributor');
        partner.is_primary__c = true;
        insert partner;
         List<Product2> prods=new List<Product2>();
        Product2 standaloneProd=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Standalone Prod1', 'MOBILE PHONE [G1]', 'ACCESSORY', 'H/W');
        prods.add(standaloneProd);
        Product2 parentProd1=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Parent Prod1', 'MOBILE PHONE [G1]', 'ACCESSORY', 'H/W');
        prods.add(parentProd1);
        insert prods;
        Id pricebookId = Test.getStandardPricebookId();

        List<PriceBookEntry> custompbes=new List<PriceBookEntry>();
        PriceBookEntry standaloneProdPBE=TestDataUtility.createPriceBookEntry(standaloneProd.Id, pb.Id);
        custompbes.add(standaloneProdPBE);
        PriceBookEntry parentProd1PBE=TestDataUtility.createPriceBookEntry(parentProd1.Id, pb.Id);
        custompbes.add(parentProd1PBE);
        insert custompbes;
        List<OpportunityLineItem> olis=new List<OpportunityLineItem>();
        OpportunityLineItem standaloneProdOLI=TestDataUtility.createOpptyLineItem(standaloneProd, standaloneProdPBE, oppOpen);
        olis.add(standaloneProdOLI);
        OpportunityLineItem parentProd1OLI=TestDataUtility.createOpptyLineItem(parentProd1, parentProd1PBE, oppOpen);
        parentProd1OLI.Parent_Product__c=parentProd1.Id;
        olis.add(parentProd1OLI);
        insert olis;
        Quote quot = TestDataUtility.createQuote('Test Quote', oppOpen, pb);
        quot.ROI_Requestor_Id__c=UserInfo.getUserId();
        quot.Division__c = 'SBS';
        quot.ProductGroupMSP__c = 'ACCESSORY';
        quot.Status='Draft';
        quot.Integration_Status__c='success';
        insert quot;
      
        List<QuoteLineItem> qlis=new List<QuoteLineItem>();
        QuoteLineItem standaloneProdQLI=TestDataUtility.createQuoteLineItem(quot.Id, standaloneProd, standaloneProdPBE, standaloneProdOLI);
        qlis.add(standaloneProdQLI);
        QuoteLineItem parentProd1QLI=TestDataUtility.createQuoteLineItem(quot.Id, parentProd1, parentProd1PBE, parentProd1OLI);
        parentProd1QLI.Parent_Product__c=parentProd1.Id;
        qlis.add(parentProd1QLI);
        insert qlis;
        ApexPages.StandardController c = new ApexPages.StandardController(quot);
        UpdateInvoicePriceExtension upe = new UpdateInvoicePriceExtension(c);
        upe.doRealtimeIntegration();
         
         
     }

}