public with sharing class UpdatePicklistValuesHandler {

	public static List<PicklistTranslation__c> picklistVals{get;set;}
    public static Map<String, String> picklistMapT2S{get;set;}
    public static Map<String, String> picklistMapS2T{get;set;}

	public void getUpdatedAccts(List<Account> newLst, Map<Id, Account> oldMap){

		if (picklistVals == null){
	        picklistVals=[select TargetAPIFieldName__c, TargetValue__c, SourceValue__c, SourceAPIName__c from PicklistTranslation__c];
	        picklistMapT2S=new Map<String,String>();
	        picklistMapS2T=new Map<String,String>();
	        for(PicklistTranslation__c pt:picklistVals){
	            picklistMapT2S.put(pt.TargetAPIFieldName__c+','+pt.TargetValue__c,pt.SourceAPIName__c+','+pt.SourceValue__c);
	            picklistMapS2T.put(pt.SourceAPIName__c+','+pt.SourceValue__c,pt.TargetAPIFieldName__c+','+pt.TargetValue__c);
	        }
	    }

		List<Account> acctsToUpdate=new List<Account>();

		for(Account newAcc:newLst){
			Account oldAcc=oldMap.get(newAcc.Id);

			if(oldAcc.external_business_partner_type__c!=newAcc.external_business_partner_type__c){
				String val=picklistMapS2T.get('external_business_partner_type__c,'+newAcc.external_business_partner_type__c);
				
				if(val!=null){
					String[] valParse=val.split(',');
					newAcc.business_partner_type__c=valParse[1];
					
					acctsToUpdate.add(newAcc);
				}
			}else if(oldAcc.business_partner_type__c!=newAcc.business_partner_type__c){
				String val=picklistMapT2S.get('business_partner_type__c,'+newAcc.business_partner_type__c);
				if(val!=null){
					String[] valParse=val.split(',');
					newAcc.external_business_partner_type__c=valParse[1];
					
					acctsToUpdate.add(newAcc);
				}
			}

			if(oldAcc.External_Industry__c!=newAcc.External_Industry__c){
				String val=picklistMapS2T.get('External_Industry__c,'+newAcc.External_Industry__c);
				if(val!=null){
					String[] valParse=val.split(',');
					newAcc.Industry=valParse[1];
					acctsToUpdate.add(newAcc);
				}
			}else if(oldAcc.Industry!=newAcc.Industry){
				String val=picklistMapT2S.get('Industry,'+newAcc.Industry);
				if(val!=null){
					String[] valParse=val.split(',');
					newAcc.External_Industry__c=valParse[1];
					acctsToUpdate.add(newAcc);
				}
			}

			if(oldAcc.external_industry_type__c!=newAcc.external_industry_type__c){
				String val=picklistMapS2T.get('external_industry_type__c,'+newAcc.external_industry_type__c);
				if(val!=null){
					String[] valParse=val.split(',');
					newAcc.Industry_type__c=valParse[1];
					acctsToUpdate.add(newAcc);
				}
			}else if(oldAcc.Industry_type__c!=newAcc.Industry_type__c){
				String val=picklistMapT2S.get('Industry_type__c,'+newAcc.Industry_type__c);
				if(val!=null){
					String[] valParse=val.split(',');
					newAcc.external_industry_type__c=valParse[1];
					acctsToUpdate.add(newAcc);
				}
			}

			if(oldAcc.External_Top_Tier__c!=newAcc.External_Top_Tier__c){
				String val=picklistMapS2T.get('External_Top_Tier__c,'+newAcc.External_Top_Tier__c);
				if(val!=null){
					String[] valParse=val.split(',');
					newAcc.Partner_top_tier__c=valParse[1];
					acctsToUpdate.add(newAcc);
				}
			}else if(oldAcc.Partner_top_tier__c!=newAcc.Partner_top_tier__c){
				String val=picklistMapT2S.get('Partner_top_tier__c,'+newAcc.Partner_top_tier__c);
				if(val!=null){
					String[] valParse=val.split(',');
					newAcc.External_Top_Tier__c=valParse[1];
					acctsToUpdate.add(newAcc);
				}
			}

			if(oldAcc.External_Type__c!=newAcc.External_Type__c){
				String val=picklistMapS2T.get('External_Type__c,'+newAcc.External_Type__c);
				if(val!=null){
					String[] valParse=val.split(',');
					newAcc.Type=valParse[1];
					acctsToUpdate.add(newAcc);
				}
			}else if(oldAcc.Type!=newAcc.Type){
				String val=picklistMapT2S.get('Type,'+newAcc.Type);
				if(val!=null){
					String[] valParse=val.split(',');
					newAcc.External_Type__c=valParse[1];
					acctsToUpdate.add(newAcc);
				}
			}
		}

		/*if(!acctsToUpdate.isEmpty()){
			update acctsToUpdate;
		}*/
	}

	public void getInsertAccts(List<Account> newLst){

		if (picklistVals == null){
	        picklistVals=[select TargetAPIFieldName__c, TargetValue__c, SourceValue__c, SourceAPIName__c from PicklistTranslation__c];
	        picklistMapT2S=new Map<String,String>();
	        picklistMapS2T=new Map<String,String>();
	        
	        for(PicklistTranslation__c pt:picklistVals){
	        	
	        	picklistMapT2S.put(pt.TargetAPIFieldName__c+','+pt.TargetValue__c,pt.SourceAPIName__c+','+pt.SourceValue__c);
	            picklistMapS2T.put(pt.SourceAPIName__c+','+pt.SourceValue__c,pt.TargetAPIFieldName__c+','+pt.TargetValue__c);
	        }
	         
	    }

		List<Account> acctsToUpdate=new List<Account>();
		
		for(Account newAcc:newLst){

			if(newAcc.external_business_partner_type__c!=null){
				
				String val=picklistMapS2T.get('external_business_partner_type__c,'+newAcc.external_business_partner_type__c);
				if(val!=null){
					String[] valParse=val.split(',');
					newAcc.business_partner_type__c=valParse[1];
					acctsToUpdate.add(newAcc);
				}
			}else if(newAcc.business_partner_type__c!=null){
				
				String val=picklistMapT2S.get('business_partner_type__c,'+newAcc.business_partner_type__c);
				if(val!=null){
					String[] valParse=val.split(',');
					newAcc.external_business_partner_type__c=valParse[1];
					acctsToUpdate.add(newAcc);
				}
			}

			if(newAcc.External_Industry__c!=null){
				
				String val=picklistMapS2T.get('External_Industry__c,'+newAcc.External_Industry__c);
				if(val!=null){
					String[] valParse=val.split(',');
					newAcc.Industry=valParse[1];
					acctsToUpdate.add(newAcc);
				}
			}else if(newAcc.Industry!=null){
				String val=picklistMapT2S.get('Industry,'+newAcc.Industry);
				if(val!=null){
					String[] valParse=val.split(',');
					newAcc.External_Industry__c=valParse[1];
					acctsToUpdate.add(newAcc);
				}
			}

			if(newAcc.external_industry_type__c!=null){
				String val=picklistMapS2T.get('external_industry_type__c,'+newAcc.external_industry_type__c);
				if(val!=null){
					String[] valParse=val.split(',');
					newAcc.Industry_type__c=valParse[1];
					acctsToUpdate.add(newAcc);
				}
			}else if(newAcc.Industry_type__c!=null){
				String val=picklistMapT2S.get('Industry_type__c,'+newAcc.Industry_type__c);
				if(val!=null){
					String[] valParse=val.split(',');
					newAcc.external_industry_type__c=valParse[1];
					acctsToUpdate.add(newAcc);
				}
			}

			if(newAcc.External_Top_Tier__c!=null){
				String val=picklistMapS2T.get('External_Top_Tier__c,'+newAcc.External_Top_Tier__c);
				if(val!=null){
					String[] valParse=val.split(',');
					newAcc.Partner_top_tier__c=valParse[1];
					acctsToUpdate.add(newAcc);
				}
			}else if(newAcc.Partner_top_tier__c!=null){
				String val=picklistMapT2S.get('Partner_top_tier__c,'+newAcc.Partner_top_tier__c);
				if(val!=null){
					String[] valParse=val.split(',');
					newAcc.External_Top_Tier__c=valParse[1];
					acctsToUpdate.add(newAcc);
				}
			}

			if(newAcc.External_Type__c!=null){
				String val=picklistMapS2T.get('External_Type__c,'+newAcc.External_Type__c);
				if(val!=null){
					String[] valParse=val.split(',');
					newAcc.Type=valParse[1];
					acctsToUpdate.add(newAcc);
				}
			}else if(newAcc.Type!=null){
				String val=picklistMapT2S.get('Type,'+newAcc.Type);
				if(val!=null){
					String[] valParse=val.split(',');
					newAcc.External_Type__c=valParse[1];
					acctsToUpdate.add(newAcc);
				}
			}
		}

		/*if(!acctsToUpdate.isEmpty()){
			update acctsToUpdate;
		}*/
	}
		

}