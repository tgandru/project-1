global class UpdateRequestPriceOppLine implements Database.Batchable<sObject>,Schedulable,Database.Stateful{
      
   global void execute(SchedulableContext SC) {
       Integer rqmi = 24;
    DateTime timenow = DateTime.now().addHours(rqmi);
    UpdateRequestPriceOppLine rqm = new UpdateRequestPriceOppLine();
    String seconds = '0';
    String minutes = String.valueOf(timenow.minute());
    String hours = String.valueOf(timenow.hour());
    String sch = seconds + ' ' + minutes + ' ' + hours + ' * * ?';
    String jobName = 'UpdateRequestPriceOppLine scheduler - ' + timenow;
    system.schedule(jobName, sch, rqm);
          database.executeBatch(new UpdateRequestPriceOppLine()) ;
          if (sc != null)  {  
      system.abortJob(sc.getTriggerId());      
    }
          
   }
 Public  List<OpportunityLineItem> listRecords = new List<OpportunityLineItem>();
 Public  map<string,list<OpportunityLineItem>> oppoli=new map<string,list<OpportunityLineItem>>();
 Public  map<id,Decimal> oldpri =new map<id,Decimal>();
 Public map<string,list<Opportunity>> usropp= new map<string,list<Opportunity>>();

  Public set<id> oppid=new set<id>();
   global Database.QueryLocator start(Database.BatchableContext BC){
    
      return Database.getQueryLocator([SELECT Opportunity.Name,Opportunity.Opportunity_Number__c,ListPrice,Name,Product2.Name,SAP_Material_Id__c,From_Batch__c, Product_Group__c,   TotalPrice,Quantity,Requested_Price__c,OpportunityId ,Opportunity.stagename,Opportunity.Ownerid,Opportunity.Owner.email,Opportunity.RecordType.Name  FROM OpportunityLineItem  
                                         where Opportunity.RecordType.Name ='Mobile' AND ( Opportunity.stagename='Commit' OR Opportunity.stagename='Proposal' OR opportunity.stagename='Identified' OR opportunity.stagename='Qualified' )and Product_Group__c='SMART_PHONE' ]);
   }

   global void execute(Database.BatchableContext BC, List<OpportunityLineItem> scope){
      
      
       for(OpportunityLineItem o :Scope){
           if((o.Requested_Price__c>o.ListPrice)&&o.Product_Group__c=='SMART_PHONE'){
               oldpri.put(o.id,o.Requested_Price__c);
             o.Requested_Price__c=o.ListPrice;
             o.From_Batch__c=true;
             System.debug('id'+o.OpportunityId );
             System.debug('Req'+o.Requested_Price__c);
             o.TotalPrice=o.Requested_Price__c * o.Quantity;
             listRecords.add(o);

             if(oppoli.containsKey(o.OpportunityId)){
                    List<OpportunityLineItem> ol = oppoli.get(o.OpportunityId);
                            ol.add(o);
                           oppoli.put(o.OpportunityId, ol);
                }else{
                   oppoli.put(o.OpportunityId, new List<OpportunityLineItem> { o }); 
                }
             oppid.add(o.OpportunityId);
           }
       }
       if(listRecords.size()>0){
           database.update(listRecords,false);
       }
      
   
   }
   global void finish(Database.BatchableContext BC){
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        system.debug('Oppid Size ' + oppid.size());
        system.debug('oppoli ' + oppoli.size());
        system.debug('usropp Size ' + usropp.size());
       if(oppid.size()>0){
         for(Opportunity opp :[select id,name,Ownerid,Owner.Name,Owner.Email from Opportunity where id in:oppid]){
                if(usropp.containsKey(opp.Ownerid)){
                    List<Opportunity> oppl = usropp.get(opp.Ownerid);
                            oppl.add(opp);
                           usropp.put(opp.Ownerid, oppl);
                }else{
                   usropp.put(opp.Ownerid, new List<Opportunity> { opp }); 
                }

          }
          
          for(user ur:[select id,email,name from user where id in:usropp.keyset()]){
              Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
              list<string> toAddresses = new list<string>();
                toAddresses.add(ur.email);
                mail.setToAddresses(toAddresses);
                mail.setSubject('Request Price Update Reminder for following Opportunity Due to Invoice Price Drop ');                
                String username = ur.name;
                String htmlBody = '';
                htmlBody = '<table width="100%" border="0" cellspacing="0" cellpadding="8" align="center" bgcolor="#F7F7F7">'+
                            +'<tr>'+
                              +'<td style="font-size: 14px; font-weight: normal; font-family:Calibri;line-height: 18px; color: #333;"><br />'+
                                   +'<br />'+
                                    +'Dear '+username+',</td>'+
                            +'</tr>'+
                             +'<tr>'+
                              +'<td style="font-size: 14px; font-weight: normal; font-family:Calibri;line-height: 18px; color: #333;"><br />'+
                                   +'<br />'+
                                    +'The Following List of Opportunity LineItems were updated due to Price Drop in GERP '+ +'.</td>'+
                            +'</tr>'+
                             
                        +'</table>';
                        
               htmlBody +=  '<table border="1" style="border-collapse: collapse"><tr><th> Opportunity No    </th><th> Opportunity Name    </th><th> Product Name    </th><th> SAP Material ID   </th><th> Invoice Price   </th><th> New Request Price  </th><th> Old Request Price</th></tr>'; 
                for(Opportunity op : usropp.get(ur.id)){
                   
                        for(OpportunityLineItem ol : oppoli.get(op.id)){
                             string Url = System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+ol.OpportunityId;
                              id id= ol.OpportunityId;
                             String opName = ol.Opportunity.Name;
                            String pName = ol.Product2.Name;
                            String  mid = ol.SAP_Material_Id__c;
                            decimal inpr = ol.ListPrice;
                            decimal rqpr = ol.Requested_Price__c;
                            decimal oldrqpr = oldpri.get(ol.id);
                            string opno = ol.Opportunity.Opportunity_Number__c;
                            htmlBody += '<tr><td>'+opno+'</td><td><a href="'+Url+'">'+opName+'</a></td><td>'+ pName + '</td><td>' + mid + '</td><td align="right">' + inpr + '</td><td align="right">' +rqpr + '</td><td align="right">' + oldrqpr +'</td></tr>';                    
                        }    
                }
                 htmlBody += '</table><br>';
                 mail.sethtmlBody(htmlBody);
                 mails.add(mail);                
          }
        if(mails.size()>0){
             Messaging.sendEmail(mails);
    }
    }
   }
   
   
}