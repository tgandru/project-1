global class UpdateRequestPriceOppLineBugFIx implements Database.Batchable<sObject>,Database.Stateful{
      
  
 Public  List<OpportunityLineItem> listRecords = new List<OpportunityLineItem>();
 Public  map<string,list<OpportunityLineItem>> oppoli=new map<string,list<OpportunityLineItem>>();
 Public  map<id,Decimal> oldpri =new map<id,Decimal>();
 Public map<string,list<Opportunity>> usropp= new map<string,list<Opportunity>>();

  Public set<id> oppid=new set<id>();
   global Database.QueryLocator start(Database.BatchableContext BC){
    
      return Database.getQueryLocator([SELECT Opportunity.Name,Opportunity.Opportunity_Number__c,ListPrice,Name,Product2.Name,SAP_Material_Id__c,From_Batch__c,	Product_Group__c,	TotalPrice,Quantity,Requested_Price__c,OpportunityId ,Opportunity.stagename,Opportunity.Ownerid,Opportunity.Owner.email,Opportunity.RecordType.Name  FROM OpportunityLineItem  
                                         where Opportunity.RecordType.Name ='Mobile' AND ( Opportunity.stagename='Commit' OR Opportunity.stagename='Proposal' OR opportunity.stagename='Identified' OR opportunity.stagename='Qualified' )and Product_Group__c='SMART_PHONE' and LastModifiedById='00536000002rYc2' and LastModifiedDate >= 2017-07-29T00:00:00Z]);
   }

   global void execute(Database.BatchableContext BC, List<OpportunityLineItem> scope){
      
      
       for(OpportunityLineItem o :Scope){
           
             listRecords.add(o);

             if(oppoli.containsKey(o.OpportunityId)){
                    List<OpportunityLineItem> ol = oppoli.get(o.OpportunityId);
		                    ol.add(o);
		                   oppoli.put(o.OpportunityId, ol);
                }else{
                   oppoli.put(o.OpportunityId, new List<OpportunityLineItem> { o }); 
                }
             oppid.add(o.OpportunityId);
        
       }
      
   
   }
   global void finish(Database.BatchableContext BC){
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        system.debug('Oppid Size ' + oppid.size());
        system.debug('oppoli ' + oppoli.size());
        system.debug('usropp Size ' + usropp.size());
       if(oppid.size()>0){
         for(Opportunity opp :[select id,name,Ownerid,Owner.Name,Owner.Email from Opportunity where id in:oppid]){
                if(usropp.containsKey(opp.Ownerid)){
                    List<Opportunity> oppl = usropp.get(opp.Ownerid);
		                    oppl.add(opp);
		                   usropp.put(opp.Ownerid, oppl);
                }else{
                   usropp.put(opp.Ownerid, new List<Opportunity> { opp }); 
                }

          }
          
          for(user ur:[select id,email,name from user where id in:usropp.keyset()]){
              Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
              list<string> toAddresses = new list<string>();
                 toAddresses.add(ur.email);
                mail.setToAddresses(toAddresses);
                mail.setSubject('Request Price Update Reminder for following Opportunity Due to Invoice Price Drop-LINK UPDATED ');                
                String username = ur.name;
                String htmlBody = '';
                htmlBody = '<table width="100%" border="0" cellspacing="0" cellpadding="8" align="center" bgcolor="#F7F7F7">'+
                            +'<tr>'+
                              +'<td style="font-size: 14px; font-weight: normal; font-family:Calibri;line-height: 18px; color: #333;"><br />'+
                                   +'<br />'+
                                    +'Hi '+username+',</td>'+
                            +'</tr>'+
                             +'<tr>'+
                              +'<td style="font-size: 14px; font-weight: normal; font-family:Calibri;line-height: 18px; color: #333;"><br />'+
                                   +'<br />'+
                                    +'Please disregard the links provided in previous email notifications for Requested Price updates on open opportunities due to SKU price changes. Below are the lists of opportunities with the correct links '+ +'.</td>'+
                            +'</tr>'+
                             
                        +'</table>';
                        
               htmlBody +=  '<table border="1" style="border-collapse: collapse"><tr><th> Opportunity No  </th><th> Opportunity Name    </th><th> Product Name    </th><th> SAP Material ID   </th></tr>'; 
                for(Opportunity op : usropp.get(ur.id)){
                   
                        for(OpportunityLineItem ol : oppoli.get(op.id)){
                             
                              string Url = System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+ol.OpportunityId;
                              id id= ol.OpportunityId;
                             String opName = ol.Opportunity.Name;
                            String pName = ol.Product2.Name;
                            String  mid = ol.SAP_Material_Id__c;
                            string opno = ol.Opportunity.Opportunity_Number__c;
                           
                            htmlBody += '<tr><td>'+opno+'</td><td><a href="'+Url+'">'+opName+'</a></td><td>' + pName + '</td><td>' + mid + '</td></tr>';                    
                        }    
                }
                 htmlBody += '</table><br>';
                 mail.sethtmlBody(htmlBody);
                 mails.add(mail);                
          }
        if(mails.size()>0){
             Messaging.sendEmail(mails);
    }
    }
   }
   
   
}