@isTest
private class UpdateRequestPriceOppLineBugFixtest {
    static Id endUserRT = TestDataUtility.retrieveRecordTypeId('End_User', 'Account'); 
    static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');
    static Id mbl = TestDataUtility.retrieveRecordTypeId('Mobile','Opportunity');

   
    @isTest static void testmethodone() {
        
        
        Test.loadData(fiscalCalendar__c.sObjectType, 'FiscalCalendarTestData');

        
        //Channelinsight configuration
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        
        User u = [select id from user where id='00536000002rYc2'];
        system.runAs(u){
        Zipcode_Lookup__c zip=TestDataUtility.createZipcode();
        insert zip;
        
        List<Roll_Out_Schedule_Constants__c> roscs=new List<Roll_Out_Schedule_Constants__c>();
        Roll_Out_Schedule_Constants__c rosc1=new Roll_Out_Schedule_Constants__c(Name='Roll Out Schedule Parameters',Average_Week_Count__c=4.33,DML_Row_Limit__c=3200);
        roscs.add(rosc1);
        insert roscs;
        
        //Creating test Account
        Account acct=TestDataUtility.createAccount(TestDataUtility.generateRandomString(5));
        acct.RecordTypeId = endUserRT;
        insert acct;
        
        
        //Creating the Parent Account for Partners
        Account paacct=TestDataUtility.createAccount(TestDataUtility.generateRandomString(7));
        acct.RecordTypeId = directRT;
        insert paacct;
        
        
        //Creating custom Pricebook
        PriceBook2 pb = TestDataUtility.createPriceBook(TestDataUtility.generateRandomString(4)); 
        insert pb;
        
        
        //Creating list of Opportunities
        
            List<Opportunity> opps=new List<Opportunity>();
            Opportunity oppOpen=TestDataUtility.createOppty('New Opp from Test setup',acct, pb, 'RFP', 'Mobile', 'SMART_PHONE', 'Identified');
            oppOpen.RecordTypeId=mbl;
            opps.add(oppOpen);
            insert opps;
        
        
        //Creating the partner for that opportunity
        Partner__c partner = TestDataUtility.createPartner(oppOpen,acct,paacct,'Distributor');
        insert partner;

        //Creating Products
        List<Product2> prods=new List<Product2>();
        Product2 standaloneProd=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Standal 12', 'MOBILE PHONE', 'SMART_PHONE', 'SET');
        prods.add(standaloneProd);
        Product2 parentProd1=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Parent Prod1', 'MOBILE PHONE', 'SMART_PHONE', 'SET');
        prods.add(parentProd1);
        Product2 childProd1=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod1', 'MOBILE PHONE', 'SMART_PHONE', 'SET');
        prods.add(childProd1);
        Product2 childProd2=TestDataUtility.createProduct2(TestDataUtility.generateRandomString(12),'Child Prod2', 'MOBILE PHONE', 'SMART_PHONE', 'SET');
        prods.add(childProd2);
        insert prods;
        
       
        List<PriceBookEntry> pbes=new List<PriceBookEntry>();
        

       
        PriceBookEntry standaloneProdPBE=TestDataUtility.createPriceBookEntry(standaloneProd.Id, pb.Id);
        pbes.add(standaloneProdPBE);
        PriceBookEntry parentProd1PBE=TestDataUtility.createPriceBookEntry(parentProd1.Id,pb.Id);
        pbes.add(parentProd1PBE);
        PriceBookEntry childProd1PBE=TestDataUtility.createPriceBookEntry(childProd1.Id, pb.Id);
        pbes.add(childProd1PBE);
        PriceBookEntry childProd2PBE=TestDataUtility.createPriceBookEntry(childProd2.Id, pb.Id);
        pbes.add(childProd2PBE);
       
        insert pbes;
        
        
        
        //Creating opportunity Line Item
        
        List<OpportunityLineItem> olis=new List<OpportunityLineItem>();
        OpportunityLineItem standaloneProdOLI=TestDataUtility.createOpptyLineItem(standaloneProd, standaloneProdPBE, oppOpen);
        standaloneProdOLI.Quantity = 10;
        standaloneProdOLI.Requested_Price__c = 120;
        standaloneProdOLI.TotalPrice = standaloneProdOLI.Quantity * standaloneProdPBE.UnitPrice;
        olis.add(standaloneProdOLI);
        OpportunityLineItem parentProd1OLI=TestDataUtility.createOpptyLineItem(parentProd1, parentProd1PBE, oppOpen);
        parentProd1OLI.Quantity = 10;
        parentProd1OLI.Requested_Price__c = 120;
        parentProd1OLI.TotalPrice = parentProd1OLI.Quantity * parentProd1PBE.UnitPrice;
        olis.add(parentProd1OLI);
        OpportunityLineItem childProd1OLI=TestDataUtility.createOpptyLineItem(childProd1, childProd1PBE, oppOpen);
        childProd1OLI.Quantity = 10;
        childProd1OLI.Requested_Price__c = 120;
        childProd1OLI.TotalPrice = childProd1OLI.Quantity * childProd1PBE.UnitPrice;
        childProd1OLI.parent_product__c=parentProd1.Id;
        olis.add(childProd1OLI);
        OpportunityLineItem childProd2OLI=TestDataUtility.createOpptyLineItem(childProd2, childProd2PBE, oppOpen);
        childProd2OLI.Quantity = 700;
        childProd2OLI.Requested_Price__c = 120;
        childProd2OLI.TotalPrice = childProd2OLI.Quantity * childProd2PBE.UnitPrice;
        childProd1OLI.parent_product__c=parentProd1.Id;
        olis.add(childProd2OLI);
        
        insert olis;
        test.startTest();
         
         database.executeBatch(new UpdateRequestPriceOppLineBugFIx()) ;
        test.stopTest();
        }
    }

}