public class UpdateSPAApprovalToInquiry {

 @InvocableMethod(label='Update PRM SPA Approval Status' description='Update PRM SPA Status, Approve/Reject Comments to Inquiry')
    Public Static void UpdateInquiryWithSPAApproval(List<id> oppIds){
        if(oppIds.size()>0){
            Map<id,Quote> spamap=new Map<id,Quote>();
            for(Quote qt:[Select id,External_SPID__c, Status,Opportunity.Inquiry_ID__c from Quote where OpportunityID in :oppIDs]){
                spamap.put(qt.ID,qt);
            }
            map<id,List<ProcessInstance>> approvalmap=new map<id,List<ProcessInstance>>();
             for(ProcessInstance p:[SELECT Id,TargetObjectId,(SELECT Id, StepStatus, Comments,ActorId,Actor.Name FROM Steps order by createddate desc) FROM ProcessInstance Where  TargetObjectId in:spamap.keyset() order by createddate desc ]){
                  if(approvalmap.containsKey(p.TargetObjectId)){
                        List<ProcessInstance> existed=approvalmap.get(p.TargetObjectId);
                        existed.add(p);
                        approvalmap.put(p.TargetObjectId,existed);
                    }else{
                        approvalmap.put(p.TargetObjectId,new List<ProcessInstance>{p});
                    }
             }
          Map<id,Inquiry__c> inquirymap=new Map<id,Inquiry__c>();   
             for(Quote qt:spamap.Values()){
                 if(approvalmap.containsKey(qt.Id)){
                      String comments=qt.External_SPID__c +'\n';
                      String Status=qt.Status;
                      List<ProcessInstance> apvlist=approvalmap.get(qt.ID);
                       for(ProcessInstance pi:apvlist){
                       for (ProcessInstanceStep step : pi.Steps) {
                           if(step.comments != null){
                                  comments=comments+'\n'+ step.Actor.Name+' - '+Step.StepStatus+' - '+step.Comments;

                           }else{
                                 comments=comments+'\n'+ step.Actor.Name+' - '+Step.StepStatus+' - ';

                           }
                       }
                      break;  
                    }
                    
                    if(inquirymap.containsKey(qt.Opportunity.Inquiry_ID__c)){
                        Inquiry__c iq=inquirymap.get(qt.Opportunity.Inquiry_ID__c);
                        Comments=Comments+'\n'+iq.PRM_PR_Approve_Reject_Comments__c;
                        Comments=Comments.replaceAll('null',' '); 
                        if(Status==iq.PRM_Project_Registration_Status__c){
                            inquirymap.put(qt.Opportunity.Inquiry_ID__c,new Inquiry__c(id=qt.Opportunity.Inquiry_ID__c,PRM_Project_Registration_Status__c=status,  PRM_Resubmit_Flag__c=false));//PRM_PR_Approve_Reject_Comments__c=Comments,
                        }else {//if(Status=='Approved' || (iq.PRM_SPA_Status__c=='Approved' ||iq.PRM_SPA_Status__c=='Presented'))
                          inquirymap.put(qt.Opportunity.Inquiry_ID__c,new Inquiry__c(id=qt.Opportunity.Inquiry_ID__c,PRM_Project_Registration_Status__c='Partial Approved', PRM_Resubmit_Flag__c=false));  // PRM_PR_Approve_Reject_Comments__c=Comments,
                        }
                    }else{
                         inquirymap.put(qt.Opportunity.Inquiry_ID__c,new Inquiry__c(id=qt.Opportunity.Inquiry_ID__c,PRM_Project_Registration_Status__c=status,  PRM_Resubmit_Flag__c=false));//PRM_PR_Approve_Reject_Comments__c=Comments,
                    }
                      
                 }
             }
             
             if(inquirymap.values().size()>0){
                 
                 database.update(inquirymap.values(),false); 
             }
             
        }
    }
    
    Public static void testclassMethod(){
        Integer x=1;
        x=1;
        x=1;
        x=x+1;
         x=x+1;
          x=x+1;
           x=x+1;
            x=x+1;
             x=x+1;
              x=x+1;
               x=x+1;
                x=x+1;
                 x=x+1;
    }
    

}