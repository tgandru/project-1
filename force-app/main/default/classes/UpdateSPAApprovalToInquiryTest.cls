@isTest()
private class UpdateSPAApprovalToInquiryTest {
    Static Id inquiryRecordType= TestDataUtility.retrieveRecordTypeId('PRM_SPA_Request','Inquiry__c');
     Static Id ITRecordType= TestDataUtility.retrieveRecordTypeId('IT','Opportunity');
    @isTest(SeeAllData=true)
    static void InquiryStatusUpdate() {
        Test.startTest();
       List<Quote> opp=new List<Quote>([Select  Id,OpportunityID from Quote where Opportunity.Inquiry_ID__c !=null and Status !='Draft' and  Opportunity.Inquiry_ID__r.RecordTypeId=:inquiryRecordType limit  1]);
       List<id> oppids=new List<id>();
       For(Quote o:opp){
           oppids.add(o.OpportunityID);
       }
       
       UpdateSPAApprovalToInquiry.UpdateInquiryWithSPAApproval(oppids);
       
       Test.stopTest();
       
    }
    
      @isTest(SeeAllData=true)
    static void InquiryStatusUpdate2() {
         UpdateSPAApprovalToInquiry.testclassMethod();
        
    }
   
}