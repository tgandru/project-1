global class UpdatingtheInvoicePriceOnRollouts implements Database.Batchable<sObject>{
//one time batch 

   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator([SELECT Invoice_Price__c,Name,OpportunityId__c,Opportunity_Line_Item__c,Roll_Out_Plan__c FROM Roll_Out_Product__c where Invoice_Price__c =null and Roll_Out_Plan__r.Opportunity__r.RecordType.Name !='HA Builder' ]);
   }

   global void execute(Database.BatchableContext BC, List<Roll_Out_Product__c> scope){
      set<string> oliid = new set<string>();
	  for(Roll_Out_Product__c rp:scope){
	       string ol=rp.Opportunity_Line_Item__c;
		   oliid.add(ol);
	   }
	   map<id,OpportunityLineItem> olmap=new map<id,OpportunityLineItem>([SELECT Id,ListPrice,Name,OpportunityId,PricebookEntryId FROM OpportunityLineItem where id in:oliid]);
         for(Roll_Out_Product__c rp:scope){
	      rp.Invoice_Price__c=olmap.get(rp.Opportunity_Line_Item__c).ListPrice;
		   
	     }  
		 update scope;
    }

   global void finish(Database.BatchableContext BC){
       
   }
   
   
}