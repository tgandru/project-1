global class UploadHARolloutDataIntoTempBatch implements Database.Batchable<string>, Database.Stateful
{

   global final blob dataDocuments;
   global integer count;
   global string refNum;
   global List<String> headers;
   global UploadHARolloutDataIntoTempBatch (blob data)
   {
             this.dataDocuments=data;
            count=0;
            headers = new List<String>();
             DateTime pr=system.now();
           refNum=UserInfo.getUserId()+String.valueof(pr.Year())+String.valueof(pr.Month())+String.valueof(pr.day())+String.valueof(pr.hour())+String.valueof(pr.minute());
   }

   global Iterable<string>  start(Database.BatchableContext BC)
   {
       
      return new CSVIterator(this.dataDocuments.toString(), '\n');
   }

   global void execute(Database.BatchableContext BC,List<String> scope)
   {
        List<HA_Rollout_Temp__c> templist=new List<HA_Rollout_Temp__c>();
      for(String row : scope)
       {
            List<String> fields = row .split(',');
            if(count==0){
               headers=fields; 
            }else{
                 for(integer l=28;l<fields.size();l++){
                    HA_Rollout_Temp__c temprec=new HA_Rollout_Temp__c();
                     temprec.Opportunity_Number__c=fields[0].replaceAll('"', '');
                     temprec.SAP_Material_Code__c=fields[15].replaceAll('"', '');
                     temprec.Product_Option__c=fields[14].replaceAll('"', '');
                     temprec.Reference_Number__c=refNum;
                     String qty=fields[l].trim();
                     qty=qty.replaceAll('"', '');
                     if(qty !='N/A' && qty !=null && qty !=''){
                         System.debug('OPP::'+fields[0]);
                         System.debug('QTY::'+qty);
                        temprec.Quantity__c=integer.valueof(qty);
                        temprec.Rollout_Month__c=headers[l].replaceAll('"', '');
                        templist.add(temprec);
                     }
                 }   
            
            }
          count++; 
       }
       
       if(templist.size()>0){
          insert templist;
       }
   }

   global void finish(Database.BatchableContext BC){
     database.executeBatch(new HARolloutDataLoadingBatch(refNum),30);
     system.debug(' Batch finish bankStatment new created ');
   }

  
  
}