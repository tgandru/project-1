public class UserTriggerHandler {
    static final String closedStatus = 'Closed';
    static final String resolvedStatus = 'Resolved';
    
    public void onAfter(List<User> newUserList) {
        syncContactUserStatus(newUserList);
    }

    public void onBeforeUpdate(Map<ID, User> newUserList, Map<ID, User> oldUserList) {
        userHasOpenCases(newUserList, oldUserList);
    }
    
    public void userHasOpenCases(Map<ID, User> userList, Map<ID, User> oldUserList){
        Map<ID, User> contactUsers = mapRelatedContacts(userList.values()); //KEY => Contact.ID   VALUE => User record
                        
        List<Contact> contactsWithOpenCases = [SELECT ID, NAME 
                                                FROM Contact 
                                                WHERE ID IN :contactUsers.keyset()
                                                AND
                                                ID IN (SELECT ContactID FROM Case WHERE Status NOT IN (:closedStatus, :resolvedStatus))];        
        //Loop through users with open cases
        for(Contact c : contactsWithOpenCases){
             User openCases = contactUsers.get(c.ID);
             //User being deactivated? add error mesage
             if(oldUserList.get(openCases.ID).IsActive &&  !userList.get(openCases.ID).IsActive){
                openCases.addError('This User has Open Cases.');
             }
        }
    }
    
    public void syncContactUserStatus(List<User> userList){
        //filter out which users have related Contacts
        Map<ID, User> contactUsers = mapRelatedContacts(userList); //KEY => Contact.ID   VALUE => User record         
        //map out just the IDs to be passed into a @future method
        Map<ID, ID> userContactIDs = new Map<ID, ID>();  //KEY => User.ID   VALUE => Contact.ID
        
        for(ID i : contactUsers.keyset()){
            userContactIDs.put(contactUsers.get(i).ID, i);
        }        
        UserTriggerHandler.syncPortalUserStatus(userContactIDs);
    }
    
    @future
    public static void syncPortalUserStatus(Map<ID, ID> userContactIDs){
System.debug('Inside syncPortalUserStatus()');       
        List<Contact> updateContacts = new List<Contact>();
        
        Map<ID, User> userList = new Map<ID, User>([SELECT ID, IsActive FROM User WHERE ID IN :userContactIDs.keyset()]);        
        
        for(String uID : userContactIDs.keyset()){
           String portalUserStatus = ' ';
            if(userList.get(uID).IsActive){
                portalUserStatus = 'Active';
            }
            else{
                portalUserStatus = 'Inactive';
            }
            updateContacts.add(new Contact(
                                Id = userContactIDs.get(uID),
                                Portal_User_Status__c = portalUserStatus
                                )
                              ); 
            if(updateContacts.size() > 0){
                update updateContacts;
            }
        }
        
        
    }
    
    private Map<ID, User> mapRelatedContacts(List<User> userList){
System.debug('inside mapRelatedContacts');  
        Map<ID, User> contactUsers = new  Map<ID, User>(); //KEY => Contact.ID   VALUE => User record            
        for(User u: userList){  
            if(u.ContactID != null){
                contactUsers.put(u.ContactID, u);
            }
        }        
        return contactUsers;
    }

}