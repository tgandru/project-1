@isTest
public class UserTriggerHandler_Test {
    @isTest
    static void activateContactUser(){
 
        Id profileId = [select Id from Profile where Name = 'Samsung Customer Community User' limit 1].Id;
        Account acct = new Account();
        User portalAccountOwner = TestDataUtility.createPortalAccountOwner();
        system.debug(portalAccountOwner.UserRoleId);
        System.runAs ( portalAccountOwner) { 
            Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
            acct = TestDataUtility.createAccount('TestAccount');

            RecordType accountRT = [SELECT Name,SobjectType FROM RecordType WHERE Name = 'Temporary' AND SobjectType = 'Account'];
            Account testAccount1 = new Account (
            Name = 'TestAcc1',
            RecordTypeId = accountRT.Id,
            BillingStreet = '85 Challenger Rd',
            BillingCity ='Ridgefield Park',
            BillingState ='NJ',
            BillingCountry='US',
            BillingPostalCode='07660'
            );

        insert testAccount1;

            Contact customer = TestDataUtility.createContact(testAccount1);
            customer.Named_Caller__c = true;
            customer.FirstName = 'FirstName';
            customer.MailingCity        = testAccount1.BillingCity;
            customer.MailingCountry         = testAccount1.BillingCountry;
            customer.MailingPostalCode  = testAccount1.BillingPostalCode;
            customer.MailingState       = testAccount1.BillingState;
            customer.MailingStreet      = testAccount1.BillingStreet;
            insert customer;
            
        test.startTest();
            User portalUser = new User(
                email='test-user@fakeemail.com', 
                contactid = customer.id, 
                profileid = profileid, 
                UserName=System.now().millisecond()+'test-user123@fakeemail123.com'+System.now().millisecond()+Math.random(), 
                alias='tuser1', 
                CommunityNickName='tuser1',
                TimeZoneSidKey='America/New_York', 
                LocaleSidKey='en_US', 
                EmailEncodingKey='ISO-8859-1',
                LanguageLocaleKey='en_US', 
                FirstName = 'Test', 
                LastName = 'User' ); 
            insert portalUser;
        Test.stopTest();
            Id userId = Site.createPortalUser(portalUser, customer.Id, '12345');
            system.debug(customer.Portal_User_Status__c);
            UserTriggerHandler handler = new UserTriggerHandler();
            List<user> uList = new List<user>();
            uList.add(portalUser);
            handler.syncContactUserStatus(uList);
            portalUser.isActive = false;
            update portalUser;
            //System.assert(customer.Portal_User_Status__c == 'Active');
        }
        
    }
    
    static void setupTestData(Account acct, Contact person){
        Case ca = new Case(
            RecordTypeId = Case.SObjectType.getDescribe().getRecordTypeInfosByName().get('Technical Support').getRecordTypeId(),
            Subject = 'Test Case',
            AccountID = acct.Id,
            Type = 'Incident',
            Status = 'New',
        Origin = 'Phone',
        Category_1__c = 'Applications',
        Category_2__c = 'Applications - Other');
        
        insert ca;
    }
}