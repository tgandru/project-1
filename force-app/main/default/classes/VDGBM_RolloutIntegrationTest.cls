@isTest(seeAllData=true)
private class VDGBM_RolloutIntegrationTest {
   
	private static testMethod void testMethod1() {
      Test.startTest();
        Id batchJobId = Database.executeBatch(new ProcessVD_GMBDataCalculations());
      Test.stopTest();
	}
    	private static testMethod void testMethod2() {
      Test.startTest();
       
         List<VD_GMB_Opportunity_List__c> insertlist=new List<VD_GMB_Opportunity_List__c>();
        List<AggregateResult> arlist=new List<AggregateResult>([Select sum(Number_of_Roll_Out_Schedules__c) cnt,Roll_Out_Plan__r.Opportunity__c opp,Roll_Out_Plan__r.Opportunity__r.Primary_Distributor__c pd from Roll_Out_Product__c where (Product__r.Product_Group__c='SMART_SIGNAGE' OR Product__r.Product_Group__c='LCD_MONITOR' OR Product__r.Product_Group__c='HOSPITALITY_DISPLAY')  Group By Roll_Out_Plan__r.Opportunity__r.Primary_Distributor__c,Roll_Out_Plan__r.Opportunity__c limit 3]);
             for(AggregateResult a:arlist){
                 VD_GMB_Opportunity_List__c rec=new VD_GMB_Opportunity_List__c();
                 rec.Name=String.valueOf(a.get('opp'));
                 rec.No_Of_Schedules__c=Integer.valueOf(a.get('cnt'));
                 rec.Primary_Distributor__c=String.valueOf(a.get('pd'));
                 insertlist.add(rec);
             }
             
           List<Opportunity> opplist=new List<Opportunity>([Select id,Name from Opportunity where Division__c='IT' and Roll_Out_Plan__c=null and Amount>0 limit 3]);   
             for(Opportunity o:opplist){
                 VD_GMB_Opportunity_List__c rec=new VD_GMB_Opportunity_List__c();
                 rec.Name=String.valueOf(o.id);
                 rec.No_Of_Schedules__c=0;
               
                 insertlist.add(rec);
             }
        insert insertlist;
        
         Id batchJobId = Database.executeBatch(new VD_GMBIntegrationBatch('Flow-025-GSCM'));
      Test.stopTest();
	}
	
		private static testMethod void testMethod3() {
      Test.startTest();
        Id batchJobId = Database.executeBatch(new IntialDataVD_GMBDataCalculations(2017));
      Test.stopTest();
	}

}