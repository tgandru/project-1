/*
Created by - Vijay Kamani 04/16/2019
Division - SEA B2B IT

Purpose - To Send VDGB Rollout Information to GSBN/GSCM  HQ System 

Note: - This Is helper class used in VD_GMBIntegrationBatch batch class

Conditons :
               Division - IT
               Product Group - Smart Signage and LCD Monitor
*/

public class VDGB_RolloutIntegrationHelper {
    
    
/*----------------------------------Callout Method------------------------------------------------------*/

 public static string webcallout(string body, string partialEndpoint){ 
        system.debug('************************************** INSIDE CALLOUT METHOD');
        System.debug('##requestBody Size ::'+body.length());
       System.debug('##requestBody ::'+body);
        System.Httprequest req = new System.Httprequest();
     
        HttpResponse res = new HttpResponse();
        req.setMethod('POST');
        req.setBody(body);
        system.debug(body);
       
        req.setEndpoint('callout:X012_013_ROI'+partialEndpoint); //+namedEndpoint); todo, make naming consistent on picklist values and endpoints and use that here
        
        req.setTimeout(120000);
         req.setHeader('Content-Type', 'application/json');
        req.setHeader('Accept', 'application/json');
    
        Http http = new Http();  
      
         if(!Test.isRunningTest()){
           res = http.send(req);
         }else{
          res = Testrespond(req);
          }
        
        System.debug('******** my resonse' + res);
        system.debug('##responseBody :::' + res.getBody());
        System.debug('***** Response Body Size ' + res.getBody().length());
        return res.getBody();     
      
  }
/*-----------------------------------Handle Sucess-------------------------------------------------------*/


/*-----------------------------------Handle Error -------------------------------------------------------*/
public static void handleError(message_queue__c mq){
           
    
}
    
 /*-----------------------------------JSON Stub Classes-------------------------------------------------*/    
  Public class Requestbody{
        public msgHeadersRequest inputHeaders {get;set;}        
        Public List<line> body {get; set;}
  }
  
   Public class Responsebody{
        public msgHeadersResponse inputHeaders {get;set;}        
       
  }
  
  Public Class line{
        Public String   yearMon   {get; set;}
        Public String   yearWeek   {get; set;}
        Public String   hq   {get; set;}
        Public String   subsidiary   {get; set;}
        Public String   opportunityNo   {get; set;}
        Public String   opportunityName   {get; set;}
        Public String   opportunityOwner   {get; set;}
        Public String   type   {get; set;}
        Public String   stageName   {get; set;}
        Public String   commitment   {get; set;}
        Public Integer   probability   {get; set;}
        Public String   productName   {get; set;}
        Public String   modelCode   {get; set;}
        Public Integer   itemNo   {get; set;}
        Public String   frstChannel   {get; set;}
        Public String   frstChannelNo   {get; set;}
        Public String   scndChannel   {get; set;}
        Public String   endCustomer   {get; set;}
        Public String   bizFocus   {get; set;}
        Public String   bizFocusDetail   {get; set;}
        Public String   creationMonth   {get; set;}
        Public String   creationDate   {get; set;}
        Public String   closeDate   {get; set;}
        Public String   winDate   {get; set;}
        Public String   lastModifiedDate   {get; set;}
        Public String   rolloutStartMonth   {get; set;}
        Public String   rolloutEndMonth   {get; set;}
        Public String   rolloutStartDate   {get; set;}
        Public String   rolloutEndDate   {get; set;}
        Public Decimal   opportunityQty   {get; set;}
        Public Decimal   opportunityAmt   {get; set;}
        Public Decimal   opportunityUSDAmt   {get; set;}
        Public Decimal   rolloutPlanQty   {get; set;}
        Public Decimal   rolloutAmt   {get; set;}
        Public Decimal   rolloutUSDAmt   {get; set;}
        Public Decimal   rolloutResultQty   {get; set;} 
      
  }
  
  public class msgHeadersRequest{
        public string MSGGUID {get{string s = giveGUID(); return s;}set;} // GUID for message
        public string IFID {get;set;} // interface id, created automatically, so need logic
        public string IFDate {get{return datetime.now().formatGMT('YYYYMMddHHmmss');}set;} // interface date
        
        public msgHeadersRequest(){
            
        }
        
        public msgHeadersRequest(string layoutId){
            IFID = layoutId;
        }
    } 

    public class msgHeadersResponse {
        public string MSGGUID {get;set;} // GUID for message
        public string IFID {get;set;} // interface id, created automatically, so need logic
        public string IFDate {get;set;} // interface date
        public string MSGSTATUS {get;set;} // start, success, failure (ST , S, F)
        public string ERRORCODE {get;set;} // error code
        public string ERRORTEXT {get;set;} // error message
    } 
 
    public static string giveGUID(){
        Blob b = Crypto.GenerateAESKey(128);
        String h = EncodingUtil.ConvertTohex(b);
        String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
        return guid;
    }
    
    
    public static string DatetoString(Date d,String format){
        String yr=String.valueof(d.Year());
        String mnth=String.valueof(d.Month());
        String day=String.valueof(d.Day());
              if(mnth.length()==1){
                  mnth = '0' + mnth;
                }
                if(day.length()==1){
                  day = '0' + day;
                }
                
        String returnVal='';
        if(format=='YYYYMMdd'){
            returnVal=yr+'-'+mnth+'-'+day;
        }else if(format=='YYYYMM'){
           returnVal=yr+mnth; 
        }else if(format=='YYYYdd'){
            returnVal=yr+day;
        }else if(format=='MMdd'){
            returnVal=mnth+day;
        }
        
        return returnVal;
    }
    
     
   public Static HTTPResponse Testrespond(HTTPRequest req) {          
        string response='';// Testing Purpose 
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatus('OK');
            res.setStatusCode(200);
            res.setBody(response);
            
            return res;                
      } 
}