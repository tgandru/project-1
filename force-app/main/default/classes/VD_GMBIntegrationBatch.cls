/*
Created by - Vijay Kamani 04/16/2019
Division - SEA B2B IT

Purpose - To Send VDGB Rollout Information to GSBN/GSCM  HQ System 

Note: - Daily Batch to Send Rollout Information created/changed rollout data in last 3 days was insterded from Other Batch "ProcessVD_GMBDataCalculations" into custom setting "VD_GMB_Opportunity_List__c"

        AND in finish method we are going to wipe out the Custom setting Data.
  

Conditons :
               Division - IT
               Product Group - Smart Signage and LCD Monitor and HTV 
*/




global class VD_GMBIntegrationBatch implements Database.Batchable<sObject>,database.stateful,Schedulable, Database.AllowsCallouts{
    global Set<String> processedOPPs;// set to Store the Processed OPP's from Each batch
    global void execute(SchedulableContext SC) {}
    global string previousPD;
    global string flowName;
     global string Summary;
     global integer batchNo;
    global VD_GMBIntegrationBatch(string flowName){
       processedOPPs=new set<String>();
        this.flowName=flowName;
        Summary='';
        batchNo=0;
       }
   
    global Database.QueryLocator start(Database.BatchableContext BC) {
           return Database.getQueryLocator([Select id,Name,Primary_Distributor__c,No_Of_Schedules__c from VD_GMB_Opportunity_List__c order by Primary_Distributor__c ASC NULLS LAST]);
    }
     
    global void execute(Database.BatchableContext BC, List<VD_GMB_Opportunity_List__c> opp) {
        
       Set<String> oppids=new Set<String>();// Set to Store OPP ID , to Process All Schedules at a time under one OPP.
       Set<String> NoRolloutoppids=new Set<String>();
       integer Schedules=0;
       List<VD_GMB_Opportunity_List__c> allrecords = VD_GMB_Opportunity_List__c.getall().values();
       for(VD_GMB_Opportunity_List__c r:allrecords){
            
              if(!processedOPPs.contains(r.Name)){
                      if(r.No_Of_Schedules__c==0){
                          Schedules=Schedules+1;
                           if(Schedules <=5900){
                              NoRolloutoppids.add(r.Name);
                              processedOPPs.add(r.Name);
                           }else{
                               break;
                           }
                          
                    }else{
                        
                        if(r.No_Of_Schedules__c >=5900){
                             processedOPPs.removeAll(oppids);
                              oppids.clear();
                              oppids.add(r.Name);
                            processedOPPs.add(r.Name);
                               break;
                            
                        }else{
                           Schedules=Schedules+Integer.valueOf(r.No_Of_Schedules__c);
                           if(Schedules <=5900){
                              oppids.add(r.Name);
                              processedOPPs.add(r.Name);
                           }else{
                               break;
                           }
                            
                        }
                        

                    }
              }
            
          
       }
       
        Integer count=1;

        List<VDGB_RolloutIntegrationHelper.line> body=new List<VDGB_RolloutIntegrationHelper.line>();
      
        if(NoRolloutoppids.size()>0){
             for(Opportunity o:[Select id,Name,StageName,CloseDate,Won_Date__c,Account.Name,Account.NaicsDesc,Opportunity_Number__c,Owner.Name,Type,Probability,Primary_Distributor__r.Name,Primary_Distributor__r.SAP_Company_Code__c,Primary_Reseller__r.Name,Account.Industry,CreatedDate,LastModifiedDate,Roll_Out_Start__c,Roll_Out_End_Formula__c from Opportunity where id in: NoRolloutoppids]){
                      VDGB_RolloutIntegrationHelper.line line=new VDGB_RolloutIntegrationHelper.line();
                       
                            line.yearMon='';
                            line.yearWeek='';
                            line.hq='NAHQ';
                            line.subsidiary='SEA';
                            line.opportunityNo=o.Opportunity_Number__c;
                            line.opportunityName=String.Valueof(o.Name);
                            line.opportunityOwner=o.Owner.Name;
                            line.type=o.Type;
                            line.stageName=o.StageName;
                            line.commitment='';
                            line.probability=Integer.valueOf(o.Probability);
                            line.productName='';
                            line.modelCode='';
                            String c=string.valueOf(count).leftPad(10).replace(' ','0');
                            line.itemNo=Integer.valueOf(c);
                            line.frstChannel=o.Primary_Distributor__r.Name;
                            line.frstChannelNo=o.Primary_Distributor__r.SAP_Company_Code__c;
                            line.scndChannel=o.Primary_Reseller__r.Name;
                            line.endCustomer=o.Account.Name;
                            line.bizFocus=o.Account.Industry;
                            line.bizFocusDetail=o.Account.NaicsDesc;
                            line.creationMonth=VDGB_RolloutIntegrationHelper.DatetoString(o.CreatedDate.Date(),'YYYYMM');
                            line.creationDate=VDGB_RolloutIntegrationHelper.DatetoString(o.CreatedDate.Date(),'YYYYMMdd');
                            line.closeDate=VDGB_RolloutIntegrationHelper.DatetoString(o.CloseDate,'YYYYMMdd');
                            if(o.Won_Date__c !=null){
                            line.winDate=VDGB_RolloutIntegrationHelper.DatetoString(o.Won_Date__c,'YYYYMMdd');
                                
                            }else{
                                line.winDate='';
                            }
                            line.lastModifiedDate=VDGB_RolloutIntegrationHelper.DatetoString(o.LastModifiedDate.Date(),'YYYYMMdd');
                            If(o.Roll_Out_Start__c !=null)line.rolloutStartMonth=VDGB_RolloutIntegrationHelper.DatetoString(o.Roll_Out_Start__c,'YYYYMM');
                            If(o.Roll_Out_End_Formula__c !=null)line.rolloutEndMonth=VDGB_RolloutIntegrationHelper.DatetoString(o.Roll_Out_End_Formula__c,'YYYYMM');
                            If(o.Roll_Out_Start__c !=null)line.rolloutStartDate=VDGB_RolloutIntegrationHelper.DatetoString(o.Roll_Out_Start__c,'YYYYMMdd');
                            If(o.Roll_Out_End_Formula__c !=null)line.rolloutEndDate=VDGB_RolloutIntegrationHelper.DatetoString(o.Roll_Out_End_Formula__c,'YYYYMMdd');
                            line.opportunityQty=0;
                            line.opportunityAmt=0;
                            line.opportunityUSDAmt=0;
                            line.rolloutPlanQty=0;
                            line.rolloutAmt=0;
                            line.rolloutUSDAmt=0;
                            line.rolloutResultQty=0;
                            body.add(line);
                            count++;
             }
            
        }
        
        
       if(oppids.size()>0){
           // Process all Rollout  Data under One OPP at a time
            
           
             for(Roll_Out_Product__c r:[Select id,Name,Roll_Out_Plan__c,Roll_Out_Plan__r.Opportunity__c,Roll_Out_Plan__r.Opportunity__r.Account.Name,Product__c,Product__r.Product_Group__c,Roll_Out_Start__c,Roll_Out_End_Formula__c,Quote_Quantity__c,SalesPrice__c,Roll_Out_Plan__r.Opportunity__r.Opportunity_Number__c
                                            ,Roll_Out_Plan__r.Opportunity__r.Name,Roll_Out_Plan__r.Opportunity__r.Owner.Name,Roll_Out_Plan__r.Opportunity__r.Type,Roll_Out_Plan__r.Opportunity__r.StageName,Roll_Out_Plan__r.Opportunity__r.Probability,Roll_Out_Plan__r.Opportunity__r.Primary_Distributor__r.Name
                                            ,Roll_Out_Plan__r.Opportunity__r.Primary_Distributor__r.SAP_Company_Code__c,Roll_Out_Plan__r.Opportunity__r.Primary_Reseller__r.Name,Roll_Out_Plan__r.Opportunity__r.Primary_Reseller__r.SAP_Company_Code__c,Roll_Out_Plan__r.Opportunity__r.Account.Industry,Roll_Out_Plan__r.Opportunity__r.Account.NaicsDesc
                                            ,Roll_Out_Plan__r.Opportunity__r.CreatedDate,Roll_Out_Plan__r.Opportunity__r.Won_Date__c,Roll_Out_Plan__r.Opportunity__r.CloseDate,Roll_Out_Plan__r.Opportunity__r.LastModifiedDate
                                            ,(Select id,name,year__c,SAP_Material_Code__c,fiscal_week__c,MonthYear__c,Plan_Date__c,Plan_Quantity__c,Plan_Revenue__c from Roll_Out_Schedules__r)
                                            from Roll_Out_Product__c where (Product__r.Product_Group__c='SMART_SIGNAGE' OR Product__r.Product_Group__c='LCD_MONITOR' OR Product__r.Product_Group__c='HOSPITALITY_DISPLAY') and Roll_Out_Plan__r.Opportunity__c in:oppids
                                          ]){
                 
                 
                 for(Roll_Out_Schedule__c ros:r.Roll_Out_Schedules__r){
                     VDGB_RolloutIntegrationHelper.line line=new VDGB_RolloutIntegrationHelper.line();
                            String yrMonth= ros.MonthYear__c;
                            line.yearMon=yrMonth.replaceAll( '\\s+', '');
                            String week=string.valueOf(ros.fiscal_week__c).leftPad(2).replace(' ','0');
                            line.yearWeek=ros.year__c+week;
                            line.hq='NAHQ';
                            line.subsidiary='SEA';
                            line.opportunityNo=r.Roll_Out_Plan__r.Opportunity__r.Opportunity_Number__c;
                            line.opportunityName=r.Roll_Out_Plan__r.Opportunity__r.Name;
                            line.opportunityOwner=r.Roll_Out_Plan__r.Opportunity__r.Owner.Name;
                            line.type=r.Roll_Out_Plan__r.Opportunity__r.Type;
                            line.stageName=r.Roll_Out_Plan__r.Opportunity__r.StageName;
                            line.commitment='';
                            line.probability=Integer.valueOf(r.Roll_Out_Plan__r.Opportunity__r.Probability);
                            line.productName=r.Product__r.Product_Group__c;
                            line.modelCode=ros.SAP_Material_Code__c;
                            String c=string.valueOf(count).leftPad(10).replace(' ','0');
                            line.itemNo=Integer.valueOf(c);
                            line.frstChannel=r.Roll_Out_Plan__r.Opportunity__r.Primary_Distributor__r.Name;
                            line.frstChannelNo=r.Roll_Out_Plan__r.Opportunity__r.Primary_Distributor__r.SAP_Company_Code__c;
                            line.scndChannel=r.Roll_Out_Plan__r.Opportunity__r.Primary_Reseller__r.Name;
                            line.endCustomer=r.Roll_Out_Plan__r.Opportunity__r.Account.Name;
                            line.bizFocus=r.Roll_Out_Plan__r.Opportunity__r.Account.Industry;
                            line.bizFocusDetail=r.Roll_Out_Plan__r.Opportunity__r.Account.NaicsDesc;
                            line.creationMonth=VDGB_RolloutIntegrationHelper.DatetoString(r.Roll_Out_Plan__r.Opportunity__r.CreatedDate.Date(),'YYYYMM');
                            line.creationDate=VDGB_RolloutIntegrationHelper.DatetoString(r.Roll_Out_Plan__r.Opportunity__r.CreatedDate.Date(),'YYYYMMdd');
                            line.closeDate=VDGB_RolloutIntegrationHelper.DatetoString(r.Roll_Out_Plan__r.Opportunity__r.CloseDate,'YYYYMMdd');
                            if(r.Roll_Out_Plan__r.Opportunity__r.Won_Date__c !=null){
                            line.winDate=VDGB_RolloutIntegrationHelper.DatetoString(r.Roll_Out_Plan__r.Opportunity__r.Won_Date__c,'YYYYMMdd');
                                
                            }else{
                                line.winDate='';
                            }
                            line.lastModifiedDate=VDGB_RolloutIntegrationHelper.DatetoString(r.Roll_Out_Plan__r.Opportunity__r.LastModifiedDate.Date(),'YYYYMMdd');
                            line.rolloutStartMonth=VDGB_RolloutIntegrationHelper.DatetoString(r.Roll_Out_Start__c,'YYYYMM');
                            If(r.Roll_Out_End_Formula__c !=null)line.rolloutEndMonth=VDGB_RolloutIntegrationHelper.DatetoString(r.Roll_Out_End_Formula__c,'YYYYMM');
                            line.rolloutStartDate=VDGB_RolloutIntegrationHelper.DatetoString(r.Roll_Out_Start__c,'YYYYMMdd');
                              If(r.Roll_Out_End_Formula__c !=null)line.rolloutEndDate=VDGB_RolloutIntegrationHelper.DatetoString(r.Roll_Out_End_Formula__c,'YYYYMMdd');
                            line.opportunityQty=r.Quote_Quantity__c;
                            line.opportunityAmt=r.Quote_Quantity__c*r.SalesPrice__c;
                            line.opportunityUSDAmt=(r.Quote_Quantity__c*r.SalesPrice__c);
                            line.rolloutPlanQty=ros.Plan_Quantity__c;
                            line.rolloutAmt=ros.Plan_Revenue__c;
                            line.rolloutUSDAmt=ros.Plan_Revenue__c;
                            line.rolloutResultQty=null;
                            body.add(line);
                            count++;
                 }
             } 
       }
       
       
       if(body.size()>0){
           System.debug('Body Prensent');
           System.debug('processedOPPs count::'+processedOPPs.size());
            string partialEndpoint = Test.isRunningTest() ? 'asdf' : Integration_EndPoints__c.getInstance(flowName).partial_endpoint__c;
          //  string partialEndpoint_GSBN = Test.isRunningTest() ? 'asdf' : Integration_EndPoints__c.getInstance('Flow-025-GSBN').partial_endpoint__c;
            string layoutId = Test.isRunningTest() ? 'asdf' : Integration_EndPoints__c.getInstance(flowName).layout_id__c;
                VDGB_RolloutIntegrationHelper.Requestbody request=new VDGB_RolloutIntegrationHelper.Requestbody();
                VDGB_RolloutIntegrationHelper.msgHeadersRequest headers=new VDGB_RolloutIntegrationHelper.msgHeadersRequest(layoutId);
                request.inputHeaders=headers;
                request.body=body;
                final string requestBody = json.serialize(request);
              
              // Send request
                 try{
                     String GSCMResponse= VDGB_RolloutIntegrationHelper.webcallout(requestBody,partialEndpoint);
                    
                          batchNo=batchNo+1;
                          Summary=summary+'\n'+'Batch -'+batchNo+': Records Count -'+body.size();
                   
                 }catch(exception ex){
                     System.debug('Exception ::'+ex.getmessage());
                         // Remove Current transaction IDs from Processed List.
                     processedOPPs.removeAll(oppids);
                     processedOPPs.removeAll(NoRolloutoppids);
                 }
           
       }
       
         
         
    }   
     
    global void finish(Database.BatchableContext BC) {
        // execute any post-processing operations
         
         If(flowName=='Flow-025-GSCM' && !Test.isRunningTest()){// Process 2nd Batch to Send Data GSBN executing the same batch class.
            Database.executeBatch(new VD_GMBIntegrationBatch('Flow-025-GSBN'),3);
         }
          String processedOPPsString = String.join(new List<String>(processedOPPs), ',');
         message_queue__c mq = new message_queue__c(MSGUID__c = '', 
	 												MSGSTATUS__c = 'S',  
	 												Integration_Flow_Type__c = 'Flow-025',
	 												IFID__c = '',
	 											    Status__c = 'success',
	 												Object_Name__c = 'VD GMD Rollout Data', 
	 												Request_Body__c=summary,
                                                 
	 												retry_counter__c = 0);//'Processed Opportunities ::'+processedOPPsString
        Insert mq;
        If(flowName=='Flow-025-GSBN'){// Delete Custom setting once 2nd (Sending Data to GSBN) batch Completes
           if(processedOPPs.size()<9999){// If Opp's Less than 10000 Delete at once
              Delete([Select id from VD_GMB_Opportunity_List__c where name in:processedOPPs]);
           }else{
               // Call batch to delete Custom Setting records.
           } 
        }
        
        
  }
}