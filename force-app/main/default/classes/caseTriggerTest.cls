@isTest
public class caseTriggerTest {

    public static testMethod void testSendingEmail(){
        profile p = [select id, Name from profile where name= 'System Administrator' limit 1];
        
        list<user> ulst = new list<user>();
        
        
        User u1 = new User(Alias = 'standt', Email='mycaseTriggerTestwww@ss.com', 
                  EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                  LocaleSidKey='en_US', ProfileId = p.Id, 
                  TimeZoneSidKey='America/Los_Angeles', UserName='abc11234@abc.com');
        ulst.add(u1);
        User u2 = new User(Alias = 'stand1', Email='abc@abc.com', 
                  EmailEncodingKey='UTF-8', LastName='Testi1ng', LanguageLocaleKey='en_US', 
                  LocaleSidKey='en_US', ProfileId = p.Id, 
                  TimeZoneSidKey='America/Los_Angeles', UserName='mycaseTriggerTestwwqqw@samss.com');
        ulst.add(u2);
        insert ulst;
        
       
        Case cs1 = new case(status='New',origin='Salesforce',ownerId=u1.id);
        insert cs1;
        cs1.ownerId=u1.id;
        update cs1;
        
        
        system.runAs(u1){
            Case cs2 = new case(status='New',origin='Salesforce',parentId = cs1.id,ownerId=u2.id);
            insert cs2;
            cs2.ownerId=u2.id;
            update cs2;
            list<caseComment> caseCmnt = new list<caseComment>();
            caseCmnt.add(new caseComment(parentId=cs2.id, CommentBody='comment 1'));
            caseCmnt.add(new caseComment(parentId=cs2.id, CommentBody='comment 1'));
            caseCmnt.add(new caseComment(parentId=cs2.id, CommentBody='comment 1'));
            insert caseCmnt;
        }
        
        system.runAs(u2){
            Case cs2 = new case(status='New',origin='Salesforce',parentId = cs1.id,ownerId=u2.id);
            insert cs2;
            cs2.ownerId=u2.id;
            update cs2;
            list<caseComment> caseCmnt = new list<caseComment>();
            caseCmnt.add(new caseComment(parentId=cs2.id, CommentBody='comment 1'));
            caseCmnt.add(new caseComment(parentId=cs2.id, CommentBody='comment 1'));
            caseCmnt.add(new caseComment(parentId=cs2.id, CommentBody='comment 1'));
            insert caseCmnt;
        }
        
        
    }
}