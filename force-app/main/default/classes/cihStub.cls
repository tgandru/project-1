public class cihStub {
    
// --- start of PRM  Leads stubs Flow_016

    public class prmLeadStatus{
        public msgHeadersRequest inputHeaders {get;set;}        
        public prmLeadStatusRequest body {get;set;}         
    }

    public class PrmLeadStatusRequest{
        public String prmLeadId {get;set;}       
        public String extLeadId {get;set;} 
        public String status {get;set;} 
        public String ownerEmail {get;set;} 
        public String lastTransferDate {get;set;} 
        public String ifType{get;set;} 
        public String statusReason {get;set;}           
    }



    public class PrmLeadStatusResponse{
       public msgHeadersResponse inputHeaders {get;set;}            
    }

// ---  end of PRM Lead Stubs

//-- Start PRM Opp --- Flow_016_1
    public class PrmOppRequest {
        public msgHeadersRequest inputHeaders {get;set;}        
        public PrmOppBodyRequest body {get;set;}
    }
    
public class PrmOppBodyRequest{
	public String prmLeadId {get;set;} 
	public String sea {get{return 'SEA';} set;}
	public String accountCompanyCode {get;set;} 
	public String accountName {get;set;} 
	public String oppCreatedByName {get;set;} 
	public String oppCreatedByID {get;set;} 
	public String extOppID {get;set;} 
	public String oppName {get;set;} 
	public String oppOwner {get;set;} 
	public String leadExtID {get;set;} 
	public String oppHistoricalComments {get;set;} 
	public String oppStageName {get;set;} 
	public String type {get{return 'Pre-BO';} set;} 
	public String leadDealRegistrationFlag {get;set;} 
	public String oppDealRegistrationApproval {get;set;} 
	public String oppDropReason {get;set;} 
	public String oppLastModifiedDate {get;set;} 
	public String oppLastModifiedByName {get;set;} 
	public String accountIndustryType {get;set;} 
	public String rollOutStartDate {get;set;} 
	public String rollOutEndDate {get;set;} 
	public String ownerEmail {get;set;} 
	public String ownerName {get;set;} 
	public String createdDate {get;set;} 
	public String pyDistSapCompanyCode {get;set;} 
	public String pyDistName {get;set;} 
	public String pyContactName {get;set;} 
	public String resellerSapCompanyCode {get;set;} 
	public String resellerName {get;set;} 
	public String resellerContactName {get;set;} 
    public List<OppurtunityLineItemRequest> lines {get;set;} 
	}
	
public class OppurtunityLineItemRequest {
	public String prmLeadId {get;set;} 
	public String sapMaterialId {get;set;} 
	public String quantity {get;set;} 
	public String productGroup {get;set;} 
	public String extOppId {get;set;} 
	public String extSPID {get;set;} 
	public String quoteStatus {get;set;} 
	public String requestedPrice {get;set;} 
	public String seqNum {get;set;} 	
}
	
    public class PrmOppResponse{
       public msgHeadersResponse inputHeaders {get;set;}            
    }	
//-- End PRM Opp

  
// --- start of ROI and AMPV stubs

    public class roiAmpvReq{
        public msgHeadersRequest inputHeaders {get;set;}        
        public roiAmpvRequest body {get;set;}       
    }

    public class roiRes{
        public msgHeadersResponse inputHeaders {get;set;}      
        //public roiResponse body {get;set;}      
    }

    public class ampvRes{
        public msgHeadersResponse inputHeaders {get{return new msgHeadersResponse();} set;}     
        public ampvResponse body {get;set;}     
    }

    /*public class roiResponse{
        public msgHeadersResponse inputHeaders {get;set;}
        
        //public string busOppId {get;set;}
        //public string specialPricingId {get;set;}
        //public string intialProfit {get;set;}
        //public string totalProfit {get;set;}
        //public string bep {get;set;} 
    }*/

    public class ampvResponse{
        public string subId {get;set;}
        public string modeCode {get;set;}
        public string color {get;set;}
        public string ampv {get;set;}
    }

    public class roiAmpvRequest{
        public string busOppId {get;set;}
        public string specialPricingId {get;set;}
        public string site {get{return 'SEA';} set;}
        public string currencyCode {get{return 'USD';} set;}
        public string rolloutStartDate {get;set;}
        public string rolloutEndDate {get;set;}
        public string initialFlag {get;set;}
        public string supplierBuyLocation {get;set;}
        public string salesSubType {get;set;}
        public list<oppLine> lines {get;set;}
    }

    public class oppLine {
        public string approveFlag {get;set;}
        public string pItemNo {get;set;}
        public string itemNo{get;set;}
        public string materialCode {get;set;}
        public string materialType {get;set;}
        public string bundle{get;set;}
        public string guidePrice {get;set;}
        public string distyPrice {get;set;}
        public string quantity {get;set;}
        public string ampvMono {get;set;}
        public string ampvColor {get;set;}
        public string setType {get;set;}
    }

    
    
// --- end of ROI and AMPV stubs

// --- Start of RollOut Stubs

    public class RollOutScheduleRequest {
        public msgHeadersRequest inputHeaders {get;set;}        
        public RollOutScheduleRequestBody body {get;set;}
    }

    public class RollOutResponse {
        public String MSGGUID {get;set;}
        public String IFID {get;set;}
        public String IFDate {get;set;}
        public String MSGSTATUS {get;set;}
        public String ERRORTEXT {get;set;}
        public String ERRORCODE {get;set;}
    }

    public class RollOutScheduleRequestBody {
        public List<RollOutPlanStub> rolloutPlan {get;set;}
        public Boolean hasMore {get;set;}
    }

    public class RollOutPlanStub {
        public String opportunityId {get;set;}
        public String rollOutPlanId {get;set;}
        public String rollOutStart {get;set;}
        public String rollOutEnd {get;set;}
        public List<RollOutScheduleStub> rollOutSchedule {get;set;}
    }

    public class RollOutScheduleStub {
        public String materialCode {get;set;}
        public String productType {get;set;}
        public String currentWeek {get;set;}
        public String futureWeek {get;set;}
        public String planQuantity {get;set;}
        public String planRevenue {get;set;}
    }

// --- End of Rollout stubs

// --- Start of Lead Opportunity stubs
    public class LeadOpportunityRequest {
        public msgHeadersRequest inputHeaders {get;set;}
        public List<ConvertedOpportunity> body {get;set;}
    }

    public class ConvertedOpportunity {
        public String id;
        public String description;
        public String carrierOpportunityNumber;
    }
// --- End of Lead Opportunity stubs

// --- Start of Credit Memo Stubs - Added by KR on 04/12/16

    public class CreditMemoRequest {
        public msgHeadersRequest inputHeaders {get;set;}        
        public CreditMemoRequestBody body {get;set;}
    }

    public class CreditMemoRequestBody{
        public string companyCode {get{return 'C310';} set;}
        public string creditMemoNum {get;set;}
        public string documentType {get{return 'YCD1';} set;}
        public string salesOrg {get;set;} //{get{return 'SEA';} set;}
        public string distributionChannel {get;set;}
        public string salesDivision {get;set;}
        public string soldToParty {get;set;}
        public string shipToParty {get;set;}
        public string assignmentNumber {get; set;}
        public string orderReason {get{return '2A9';} set;}
        public string comment {get;set;}
        public list<creditMemoLine> lines {get;set;}
    }

    public class creditMemoLine {
        public string itemNumber {get;set;}
        public string material {get;set;}
        public string quantity {get;set;}
        public string condition {get{return '2500';}set;}
        public string amount {get;set;}
    }
    
    public class creditMemRes{
        public msgHeadersResponse inputHeaders {get;set;}      
        public creditMemoResponse body {get;set;}       
    }
    
    public class creditMemoResponse {
        public list<creditMemoResults> results {get;set;}
    }
    
    
    public class creditMemoResults{
        public string type {get;set;}
        public string id {get;set;}
        public string message {get;set;}
        public string messageV1 {get; set;}
        public string messageV2 {get;set;}
    }   
// --- End of Credit Memo stubs

// --- Start of Credit Memo Update Invoice Price Stubs

    public class UpdateInvoicePriceRequest {
        public msgHeadersRequest inputHeaders {get;set;}
        public InvoiceBodyRequest body {get;set;}
    }

    public class InvoiceBodyRequest {
        public String salesOrg {get;set;}
        public String distributionChannel {get;set;}
        public String division {get;set;}
        public String soldToParty {get;set;}
        public String shipToParty {get;set;}
        public String salesDocType {get;set;}
        public String orderReason {get;set;}
        public String currencyKey {get;set;}
        public String validFromDate {get;set;}
        public List<CreditMemoLineItemsRequest> lines {get;set;} 
    }

    public class CreditMemoLineItemsRequest {
        public String materialCode {get;set;}
        public String quantity {get;set;}
        public String itemNumberSdDoc {get;set;}
        public String werks{get;set;}
    }

    public class UpdateInvoicePriceResponse {
        public msgHeadersResponse inputHeaders {get;set;}
        public InvoiceBodyResponse body {get;set;}
    }

    public class InvoiceBodyResponse {
        public List<CreditMemoLineItemsResponse> lines {get;set;}
    }

    public class CreditMemoLineItemsResponse {
        public String unitCost {get;set;}
        public String materialCode {get;set;}
        public String quantity {get;set;}
        public String invoicePrice {get;set;}
        public String currencyKey {get;set;}
        public String itemNumberSdDoc {get;set;}
    }

// --- End of Credit Memo Update Invoice Price Stubs


// --- Start of Account Insert/Update Stubs

    public class UpdateAccountRequest {
        public msgHeadersRequest inputHeaders {get;set;}
        public UpdateAccountBodyRequest body {get;set;}
    }

    public class UpdateAccountBodyRequest {
        public String sapCompanyCode {get;set;}
        public String systemId {get;set;}
        public String requestNumber {get;set;}
        public String companyCode {get;set;}
        public String accountName {get;set;}
        public String billingStreet {get;set;}
        public String billingCity {get;set;}
        public String billingCountry {get;set;}
        public String billingState {get;set;}
        public String billingPostalCode {get;set;}
        public String phone {get;set;}
        public String fax {get;set;}
        public String website {get;set;}
        public String email {get;set;}
        public String directFlag {get;set;}
        public String indirectFlag {get;set;}
        public String endCustomerFlag {get;set;}
        public String topTier {get;set;}
        public String businessPartnerTier {get;set;}
        public String servicePartnerTier {get;set;}
        public String solutionPartnerTier {get;set;}
        public String industryType {get;set;}
        public String businessPartnerType {get;set;}
        public String industry {get;set;}
        public String keyAccount {get;set;}
        public String level5 {get;set;}
        public String level4 {get;set;}
        public String level3 {get;set;}
        public String level2 {get;set;}
        public String level1 {get;set;}
        public String parentSapID {get;set;}
        public String parentLevel {get;set;}
        public String DUP_AFLAG {get;set;}
        public String DUP_REMARK {get;set;}
        public string DUP_ADATE {get;set;}
        public String DUP_AUSER {get;set;}
    }

    public class UpdateAccountResponse {
        public msgHeadersResponse inputHeaders {get;set;}   
        public UpdateAccountBodyResponse body{get;set;}
    }

    public class UpdateAccountBodyResponse{
        public String sapCompanyCode {get;set;}
    }

// --- End of Account Insert/Update STubs




    public class realTimePriceReq{
        public msgHeadersRequest inputHeaders {get;set;}        
        public realTimeReq body {get; set;}
    }  

    public class realTimePriceRes{
        public msgHeadersResponse inputHeaders {get;set;}       
        public realTimeRes body {get;set;}
    }  

    public class realTimeRes{
        public string unitCost {get;set;}
        public string invoicePrice {get;set;}
        public string materialNumber {get;set;}
        public string currencyKey {get {return '1';} set;}
    }

    public class realTimeReq{
        public string soldToParty {get;set;}
        public string shipToParty {get;set;}
        public string salesOrg {get{return '3101';} set;}
        public string distributionChannel {get {return '10';} set;}
        public string division {get;set;}
        public string salesDocType {get {return 'YN01';} set;}
        public string currencyKey {get {return 'USD';} set;}
        public string materialCode {get;set;}
    }







    public class timeWindowReq{
        public string fromTime {get;set;} // time in GMT
        public string toTime {get;set;} // time in GMT
    //    public string jobId {get;set;}
        public msgHeadersRequest inputHeaders {get;set;}        
    }  
 
 
    public class msgHeadersRequest{
        public string MSGGUID {get{string s = giveGUID(); return s;}set;} // GUID for message
        public string IFID {get;set;} // interface id, created automatically, so need logic
        public string IFDate {get{return datetime.now().formatGMT('YYYYMMddHHmmss');}set;} // interface date
        
        public msgHeadersRequest(){
            
        }
        
        public msgHeadersRequest(string layoutId){
            IFID = layoutId;
        }
    } 

    public class msgHeadersResponse {
        public string MSGGUID {get;set;} // GUID for message
        public string IFID {get;set;} // interface id, created automatically, so need logic
        public string IFDate {get;set;} // interface date
        public string MSGSTATUS {get;set;} // start, success, failure (ST , S, F)
        public string ERRORCODE {get;set;} // error code
        public string ERRORTEXT {get;set;} // error message
    } 
 
    public static string giveGUID(){
        Blob b = Crypto.GenerateAESKey(128);
        String h = EncodingUtil.ConvertTohex(b);
        String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
        return guid;
    }

    
}