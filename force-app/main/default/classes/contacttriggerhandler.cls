public class contacttriggerhandler {
    public static void beforedelete(list<contact> conts){
        set<id> accIds = new set<id>();
        for(Contact con : conts){
            if(con.accountId != null){
                accIds.add(con.accountId);
            }
        } 
        if(accIds.size() > 0){
            map<id, Account> accMap = new map<id, Account>([select id, ownerId from Account where id IN :accIds]);
            for(Contact con : conts){
                if(con.accountId != null && accMap.get(con.accountId).ownerId != userInfo.getUserId()){
                    con.addError('Only Account owner can delete contact');
                }
            }
        } 
        
    }
 /*   Public static void afterupdate(list<contact> ncon,map<id,contact> oldmap){
        set<id> conid =new set<id>();
        list<task> upt=new list<task>();
        for(contact con:ncon){
          contact oldcon=oldmap.get(con.id);
          if(con.Title!= oldcon.Title){
              conid.add(con.Id);
          }
        }
        if(conid.size()>0){
            list<task> tl=new list<task>([select id from task where WhoId in:conid]);
            upt.addAll(tl);
        }
        if(upt.size()>0){
            System.debug('***********'+upt);
            Database.Update(upt,false);
            //Update upt;
        }
        
    } */
   
}