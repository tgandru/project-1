@isTest
private class contacttriggerhandlertest {
    
    private static testMethod void beforedelete() {
        
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Zipcode_Lookup__c zp = new Zipcode_Lookup__c(City_Name__c ='Edison', Country_Code__c='US', State_Code__c='NJ',State_Name__c='New Jersey',Name ='08820');
        insert zp;
        
        profile p1 = [select id, Name from profile where name= 'B2B Sales User - Mobile' limit 1];
        User u1 = new User(Alias = 'standt', Email='mycontactTriggerTestwww@ss.com', 
              EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
              LocaleSidKey='en_US', ProfileId = p1.Id, 
              TimeZoneSidKey='America/Los_Angeles', UserName='abc11234555@abc.com');
        
        User loggedInUser = [select id from User where id=:userInfo.getUserId()];     
        Id endCustomer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('End Customer').getRecordTypeId();
        Account acc1 = new account(recordtypeId=endCustomer,Name ='testdelete', Type='Customer',BillingStreet ='673 wood Ave',BillingCity='Edison',BillingState='NJ',BillingCountry='US',BillingPostalCode='08820');
        
        system.runAs(loggedInUser){
            insert new list<user>{u1};
            
        }
        
        system.runAs(u1){
            insert acc1;
            contact con = new contact( LastName='mytestdelete',Accountid=acc1.id,Email='mytestdelete@abc.com');
            insert con;
            
            test.startTest();
            
            delete con;
            
            test.stopTest();
        }
    }

    private static testMethod void afterupdate() {
        
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Zipcode_Lookup__c zp = new Zipcode_Lookup__c(City_Name__c ='Edison', Country_Code__c='US', State_Code__c='NJ',State_Name__c='New Jersey',Name ='08820');
        insert zp;
        
        profile p1 = [select id, Name from profile where name= 'B2B Sales User - Mobile' limit 1];
        User u1 = new User(Alias = 'standt', Email='mycontactTriggerTestwww@ss.com', 
              EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
              LocaleSidKey='en_US', ProfileId = p1.Id, 
              TimeZoneSidKey='America/Los_Angeles', UserName='abc11234555@abc.com');
        
        User loggedInUser = [select id from User where id=:userInfo.getUserId()];     
        Id endCustomer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('End Customer').getRecordTypeId();
        Account acc1 = new account(recordtypeId=endCustomer,Name ='testdelete', Type='Customer',BillingStreet ='673 wood Ave',BillingCity='Edison',BillingState='NJ',BillingCountry='US',BillingPostalCode='08820');
        
        system.runAs(loggedInUser){
            insert new list<user>{u1};
            
        }
        
        system.runAs(u1){
            insert acc1;
            contact con = new contact( LastName='mytestupdate',Accountid=acc1.id,Email='mytestupdate@abc.com', BP_Number__c ='1234567890');
            insert con;
            
            con.LastName = 'mytestupdate_1';
            update con;
        }
        
        
            
    }
}