public with sharing class creditMemoIntegrationHelper {
    
    // call this from batch Job, failed-retry stuff is also retried here
    public static void callCreditMemoEndpoint(message_queue__c mq){
        System.debug('## In Batch - Credit Memo');
        Map<String, String> statusMap = new Map<String,String>();
        statusMap.put('E', 'Error');
        statusMap.put('S','Success');
        statusMap.put('W','Warning');
        string partialEndpoint = Test.isRunningTest() ? 'asdf' : Integration_EndPoints__c.getInstance('Flow-010').partial_endpoint__c;
        string layoutId = Test.isRunningTest() ? 'asdf' : Integration_EndPoints__c.getInstance('Flow-010').layout_id__c;
        
        Credit_Memo__c cm = [select Id, Assignment_Number__c, Comments__c, InvoiceNumber__c, Credit_Memo_Number__c, Direct_Partner__c,SoldToAccount__c, SoldToAccount__r.Account__c,                            
                    SoldToAccount__r.Account__r.SAP_Company_Code__c, Integration_Status__c, SAP_Billing_Number__c, SAP_I_F_Result__c, SAP_Request_Number__c, 
                    SAP_Response__c, Status__c, document_type__c, Name, (Select Id, Name, Approved_Price__c, Credit_Memo__c, Credit_Price__c,
                     Invoice_Price__c, OLI_ID__c, Product_Name__c, Quote__c,  QLI_ID__c, TotalClaimAmount__c,
                     Request_Quantity__c, SAP_Condition_Type__c, SAP_Material_ID__c, Shipped_Quantity__c, Total_Claim_Amount__c   
                     from Credit_Memo_Products__r where TotalClaimAmount__c!=0 and TotalClaimAmount__c !=null) from Credit_Memo__c where Id=:mq.Identification_Text__c];
        
        Account_Sold_To__c soldRec = [Select Id, Sales_Organization__c,Account__c, distribution_channel_id__c, sales_division_id__c from Account_Sold_To__c 
                                        where id=:cm.SoldToAccount__c  limit 1];
        system.debug('## soldRec'+soldRec);         
        cihStub.CreditMemoRequest reqStub = new cihStub.CreditMemoRequest();
        cihStub.msgHeadersRequest headers = new cihStub.msgHeadersRequest(layoutId);
        cihStub.CreditMemoRequestBody body = new cihStub.CreditMemoRequestBody();
        mapCreditMemoFields(body, cm, soldRec);
        reqStub.body = body;
        reqStub.inputHeaders = headers;
        string requestBody = json.serialize(reqStub);
        string response = '';
        System.debug('##requestBody ::'+requestBody);
        try{
            response = RoiIntegrationHelper.webcallout(requestBody, partialEndpoint); 
            System.debug('##response ::'+response);
        } 
        catch(exception ex){
            System.debug('## Error in Credit Memo Integration RESPONSE ');
            system.debug(ex.getmessage()); 
            handleError(mq);
        }
        
        if(string.isNotBlank(response)){ 
            Boolean flag = false;
            cihStub.creditMemRes res = (cihStub.creditMemRes) json.deserialize(response, cihStub.creditMemRes.class);
            system.debug('## Credit Memo Response ::'+res);
            // process response. response from 010 is to update Billing Number, Request Number Response, Integration Status, Error Message 
              mq.Response_Body__c=String.valueOf(response);//Added By Vijay to store the response from integration - 8/8/2018
              mq.Request_Body__c=String.valueOf(requestBody);//Added By Vijay to store the Request body - 8/8/2018
            if(res.inputHeaders.MSGSTATUS == 'S'){
                handleSuccess(mq, res);
            } 
            else {     
                   string tempMsg = '';
                    //process body from creditMemoResponse  
                    for(cihStub.creditMemoResults cmResult: res.body.results){
                        //Added by KR: on 06/04/16
                        if(statusMap.containsKey(cmResult.type)){
                        	tempMsg = statusMap.get(cmResult.type);
                        }else{
                            tempMsg = cmResult.type;
                        }
                        flag = true;
                        /*
                        if(cmResult.ID == 'V1'){
                            cm.SAP_Response__c +='\n'+ cmResult.message;
                            cm.SAP_Request_Number__c = cmResult.messageV2;
                            cm.Integration_Status__c =tempMsg;//cmResult.type;
                            mq.ERRORTEXT__c +=' '+ cmResult.message;
                        } 
                        else if(cmResult.ID == 'VF'){
                            cm.SAP_Response__c +='\n'+ cmResult.message;
                            cm.SAP_Billing_number__c = cmResult.messageV1;
                            cm.Integration_Status__c = tempMsg;//cmResult.type;
                            mq.ERRORTEXT__c +=' '+ cmResult.message;
                        }  
                        else {
                        	cm.SAP_Response__c +='\n'+ cmResult.message;
                            cm.Integration_Status__c = tempMsg;//cmResult.type;
                            mq.ERRORTEXT__c +=' ' + cmResult.message;
                        }*/
                        if(cm.SAP_Response__c==null && cm.Integration_Status__c==null) {
                        	cm.SAP_Response__c = cmResult.message;
                        	cm.Integration_Status__c = tempMsg;//cmResult.type;
                        }
                        
                        if(mq.ERRORTEXT__c!=''&&mq.ERRORTEXT__c!=null){// Added By Vijay on 8/7/2018 - To avoid Adding Null in Error Text-- Starts Here
                             mq.ERRORTEXT__c +=' ' + cmResult.message;
                        }else{
                               mq.ERRORTEXT__c =' ' + cmResult.message;
                        } //-- Ends Here 
                        
                    }// End of For Loop
                    // Update Message Queue with Error info
	                if(res.inputHeaders!=null) {
	                    mq.ERRORCODE__c = res.inputHeaders.errorCode; 
	                    If(!flag) mq.ERRORTEXT__c = res.inputHeaders.errorText;
	                    mq.MSGSTATUS__c = res.inputHeaders.MSGSTATUS;
	                    mq.msguid__c = res.inputHeaders.msgGUID;
	                    mq.IFID__c = res.inputHeaders.ifID;
	                    mq.IFDate__c = res.inputHeaders.ifDate;
	                }
                    handleError(mq);
		            try {
		                upsert cm;
		            } catch(Exception ex){
		                System.debug('## Exception in Credit Memo upsert:: else block'+ex);
		            }
            } 
  
            
            // Duplicated Error Handling
            //handleError(mq);    
        }
        else {
                cm.Integration_Status__c = 'Error'; 
                upsert cm;
                System.debug('## Credit Memo Error :: Blank Response');
                handleError(mq);
            }
        
    }
    
    public static void handleError(message_queue__c mq){
            mq.retry_counter__c += 1;
            mq.status__c = mq.retry_counter__c > 5 ? 'failed' : 'failed-retry';
            upsert mq;      
    }
    
    public static void handleSuccess(message_queue__c mq, cihStub.creditMemRes res){
            mq.status__c = 'success';
            mq.MSGSTATUS__c = res.inputHeaders.MSGSTATUS;
            mq.msguid__c = res.inputHeaders.msgGUID;
            mq.IFID__c = res.inputHeaders.ifID;
            mq.IFDate__c = res.inputHeaders.ifDate;
           
            upsert mq;   
            
            //Update Last Succesful Run DateTime
            Integration_EndPoints__c cmIntegSettings = [SELECT id, last_successful_run__c,endPointURL__c, Name  FROM Integration_EndPoints__c where Name = 'Flow-010' limit 1]; 
            cmIntegSettings.last_successful_run__c = system.now();
            
            update cmIntegSettings; 
            system.debug('## cmIntegSettings'+cmIntegSettings); 
             
           Credit_Memo__c cmRecord =[select Id, Assignment_Number__c, Comments__c,  Credit_Memo_Number__c,  
                    Integration_Status__c, SAP_Billing_Number__c, SAP_I_F_Result__c, SAP_Request_Number__c, InvoiceNumber__c,
                    SAP_Response__c, Status__c from Credit_Memo__c where id=:mq.Identification_Text__c limit 1];
            
            if(cmRecord.SAP_Response__c==null) {
            	cmRecord.SAP_Response__c='';
            }
            String oldval=cmRecord.SAP_Response__c;//Added By Vijay on 8/9/2018 - to Update SAP Response as New Val + Old Val
            // Process CMResponse body and update Credit Memo   
            for(cihStub.creditMemoResults cmResult: res.body.results){
                
                if(cmResult.ID == 'V1'){
                    cmRecord.SAP_Response__c =cmResult.message ;//+' with invoice number '+cmRecord.InvoiceNumber__c+'.'+'\n'+oldval 
                    cmRecord.SAP_Request_Number__c = cmResult.messageV2;
                    cmRecord.Integration_Status__c = 'Success';
                }
                else if(cmResult.ID == 'VF'){
                    cmRecord.SAP_Response__c +='\n'+cmResult.message+' with invoice number '+cmRecord.InvoiceNumber__c+'.';
                    cmRecord.SAP_Billing_number__c = cmResult.messageV1;
                    cmRecord.Integration_Status__c = 'Success';
                } 
               
            }   
           cmRecord.SAP_Response__c +=oldval;
         upsert cmRecord;
         System.debug('## Upserted Credit Memo Response on ::'+cmRecord);
    }
    
    
   public static void mapCreditMemoFields(cihStub.CreditMemoRequestBody body, Credit_Memo__c c, Account_Sold_To__c asRec){
        CreditMemoIntegration__c distValue = CreditMemoIntegration__c.getValues('Distribution Channel Value');
        
        body.creditMemoNum = c.Credit_Memo_Number__c;
        body.documentType = c.document_type__c;
        body.salesOrg = asRec.Sales_Organization__c; 
        body.distributionChannel = distValue.Distribution_Channel__c;
       // body.distributionChannel = asRec.distribution_channel_id__c;
        body.salesDivision = asRec.sales_division_id__c;
        body.soldToParty = c.SoldToAccount__r.Account__r.SAP_Company_Code__c;
        body.shipToParty = c.SoldToAccount__r.Account__r.SAP_Company_Code__c;
        body.assignmentNumber = c.InvoiceNumber__c;
        body.comment = c.Comments__c;
        if(c.Credit_Memo_Products__r != null && c.Credit_Memo_Products__r.size()!=0){
            list<cihStub.creditMemoLine> creditMemoLines = new list<cihStub.creditMemoLine>();
            mapCreditMemoProductFields(creditMemoLines, c.Credit_Memo_Products__r);
            body.lines = creditMemoLines;
        }       
    }
    
    public static void mapCreditMemoProductFields(list<cihStub.creditMemoLine> creditMemoLines, list<Credit_Memo_Product__c> cmProductList){     

        integer i=0;
        for(Credit_Memo_Product__c cmp : cmProductList){
            cihStub.creditMemoLine cml = new cihStub.creditMemoLine();
            i+=10;
            cml.itemNumber = string.valueOf(i); 
            cml.material = cmp.SAP_Material_ID__c; 
            cml.quantity = string.valueOf(cmp.Request_Quantity__c);
            cml.condition = cmp.SAP_Condition_Type__c;
            cml.amount = string.valueOf(cmp.TotalClaimAmount__c);
            //cml.amount = string.valueOf(cmp.Total_Claim_Amount__c);

            creditMemoLines.add(cml);
        }
    }
    

}