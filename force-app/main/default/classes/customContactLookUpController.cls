public class customContactLookUpController {
    
  public Contact Contact {get;set;} // new Contact to create
  public List<Contact> results{get;set;} // search results
  public string searchString{get;set;} // search keyword
  public Id accid {get;set;}
  public boolean displaySuccess{get;set;}
    
  public CustomContactLookupController() {
    accid = system.currentPageReference().getParameters().get('accid');  
    Contact = new Contact();
    // get the current search string
    searchString = System.currentPageReference().getParameters().get('lksrch');
    runSearch();  
  }

  // performs the keyword search
  public PageReference search() {
    runSearch();
    return null;
  }

  // prepare the query and issue the search command
  private void runSearch() {
    // TODO prepare query string for complex serarches & prevent injections
    results = performSearch(searchString);               
  } 

  // run the search and return the records found. 
  private List<Contact> performSearch(string searchString) {

    String soql = 'select id, Name, Account.Name, title, Email, Phone, MailingState, Job_Position__c, Job_Role__c, Owner.Name from Contact';
    if(searchString != '' && searchString != null && accid != null){
      soql = soql +  ' where name LIKE \'%' + searchString +'%\'';
      soql = soql + ' and accountid = \''+accid+'\'';
    }
    else if(accid != null){
        soql = soql + ' where accountid = \''+accid+'\'';
    }
    else{
        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error,'Account id is not associated'));    
        return null;
    }
    soql = soql + ' limit 20';
    System.debug(soql);
    return database.query(soql); 

  }
    
    
    public String newConId {get; set;}
    public String newConName {get; set;}
    // save the new Contact record
    public PageReference saveContact() {
        if(Contact.Email != null && Contact.LastName != null) {
            Contact.AccountId = accid;
            insert Contact;
            newConId = Contact.id;
            newConName = Contact.FirstName + ' ' + Contact.LastName;
            displaySuccess = true;
        }
        else{
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error,'Last Name and Email are required'));    
        }
        // reset the Contact
        Contact = new Contact();
        return null;
    }

  // used by the visualforce page to send the link to the right dom element
  public string getFormTag() {
    return System.currentPageReference().getParameters().get('frm');
  }

  // used by the visualforce page to send the link to the right dom element for the text box
  public string getTextBox() {
    return System.currentPageReference().getParameters().get('txt');
  }


}