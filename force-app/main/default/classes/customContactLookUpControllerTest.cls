@isTest
private class customContactLookUpControllerTest {
    static Account testMainAccount;
    
    private static void setup() {
    	//Jagan: Test data for fiscalCalendar__c
        Test.loadData(fiscalCalendar__c.sObjectType, 'FiscalCalendarTestData');
        
        //Channelinsight configuration
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();

        Zipcode_Lookup__c zip=TestDataUtility.createZipcode();
        insert zip;
        
        testMainAccount = TestDataUtility.createAccount('TestAccount');
        insert testMainAccount;
        
        Contact contactRecord = TestDataUtility.createContact(testMainAccount);
        	contactRecord.Email = 'test@test.com';
        	contactRecord.LastName = 'TestLastName';
        insert contactRecord;
    }
    
    @isTest static void test_method_one() {
    	setup();
    	PageReference pageRef = Page.customcontactlookup;
        Test.setCurrentPage(pageRef);
		ApexPages.currentPage().getParameters().put('accid', testMainAccount.Id);
		ApexPages.currentPage().getParameters().put('lksrch', 'TestSearch');
		ApexPages.currentPage().getParameters().put('frm', 'FRM');
		ApexPages.currentPage().getParameters().put('txt', 'TXT');
		CustomContactLookupController con = new CustomContactLookupController();
		con.search();
		con.Contact = new Contact(Email = 'TestEmail@test.com', LastName = 'TestLastName');
		con.saveContact();
		con.getFormTag();
		con.getTextBox();
		System.assertEquals(true, con.displaySuccess);
    }
    
    @isTest static void test_method_two() {
    	setup();
    	PageReference pageRef = Page.customcontactlookup;
        Test.setCurrentPage(pageRef);
		ApexPages.currentPage().getParameters().put('accid', testMainAccount.Id);
		ApexPages.currentPage().getParameters().put('lksrch', '');
		ApexPages.currentPage().getParameters().put('frm', 'FRM');
		ApexPages.currentPage().getParameters().put('txt', 'TXT');
		CustomContactLookupController con = new CustomContactLookupController();
		con.search();
		con.Contact = new Contact();
		con.saveContact();
		con.getFormTag();
		con.getTextBox();
		System.assertEquals(null, con.displaySuccess);
    }
    
    @isTest static void test_method_three() {
    	setup();
    	PageReference pageRef = Page.customcontactlookup;
        Test.setCurrentPage(pageRef);
		ApexPages.currentPage().getParameters().put('accid', null);
		ApexPages.currentPage().getParameters().put('lksrch', '');
		ApexPages.currentPage().getParameters().put('frm', 'FRM');
		ApexPages.currentPage().getParameters().put('txt', 'TXT');
		CustomContactLookupController con = new CustomContactLookupController();
		con.search();
		con.Contact = new Contact();
		con.saveContact();
		con.getFormTag();
		con.getTextBox();
		System.assertEquals(null, con.displaySuccess);
    }
}