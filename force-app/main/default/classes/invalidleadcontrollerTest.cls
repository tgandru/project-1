@isTest
private class invalidleadcontrollerTest {

	private static testMethod void testmethod1() {
        Test.startTest();
         string   CampaignTypeId = [Select Id From RecordType Where SobjectType = 'Campaign' and Name = 'Sales'  limit 1].Id;
	    string   LeadTypeId = [Select Id From RecordType Where SobjectType = 'Lead' and Name = 'End User'  limit 1].Id;
       Campaign cmp= new Campaign(Name='Test Campaign for test class ',	IsActive=true,type='Email',StartDate=system.Today(),recordtypeid=CampaignTypeId);
       insert cmp;
       List<Lead> leadLst = new list<Lead>();
       
       lead le1=new lead(lastname='test lead 123',Company='My Lead Company',Email='mylead@sds.com',	MQL_Score__c='T1',Status='New',LeadSource='Others',recordtypeid=LeadTypeId);
	  
	    insert le1;
	    

	        Inquiry__c iq=new Inquiry__c(Lead__c = le1.id,Product_Solution_of_Interest__c='Monitors',	 campaign__c = cmp.id);
    		 insert iq; 
    		PageReference VFPage = Page.Invalid_Lead;
         Test.setCurrentPageReference(VFPage); 
         ApexPages.currentPage().getParameters().put('id',le1.id);
         ApexPages.currentPage().getParameters().put('mqlid', iq.Id);
         invalidleadcontroller contr = new invalidleadcontroller();
         contr.mqlid = iq.Id; 
         contr.ld.Status ='invalid'; 
         contr.ld.Reason__c = 'SPAM'; 
         contr.savelead();
         contr.canl();

	     
	}

}