public class leStub {
    
    //This class doesn't seemed to be referenced anywhere else 
    // can be deleted upon confirmation from the team
    
	/*public class contactInfo{
		public string Name {get {return 'Gold Belt Hotel Juneau';}set;}
		public string address1 {get{return '12 main st';} set;}
		public string address2 {get{return 'unit 123';} set;}
		public string city {get {return 'buffalo';} set;}
		public string state {get {return 'NY';} set;}
		public string postalCode {get{return '14214';} set;}
		public string country {get{return 'US';} set;}		
		public string Phone {get {return '1231231234';}set;}
		public string Fax {get {return '1231231234';}set;}
	}
	
	public class leRequest{
		public string hotel_id {get {return 'H581033';}set;}
		public string RecordType {get {return 'Census Conversion';}set;}
		public string TargetAccountDescription {get {return '';}set;}
		public string branch_office {get {return '';}set;}
	//	public contactInfo hotelInfo {get {return new contactInfo();}set;}

		public string GeneralManagerName {get {return 'abc';}set;}
		public string WorldRegion1 {get {return 'North America';}set;}
		public string WorldRegion2 {get {return 'United States';}set;}
		public string ProjectStage {get {return 'Early Planning';}set;}
		public string DateOpened {get {return datetime.now().formatGMT('YY/MM/DD');}set;}
		public string YearBuilt {get {return '1975';}set;}
		public string BrandAffiliationDate {get {return '';}set;}
		public string LastRenovationYear {get {return '';}set;}
		public string DateOfSale {get {return '';}set;}
		public string SellingPrice {get {return '';}set;}
		public string NewFranchiseName {get {return 'Four Points Hotel';}set;}
		public string NewParentCompany {get {return 'Starwood';}set;}
		public string NewChainScale {get {return 'upscale';}set;}
		public string NewIndChainScale {get {return '';}set;}
		public string ConstructionStartDate {get {return datetime.now().formatGMT('YY/MM/DD');}set;}
		public string Rooms {get {return '104';}set;}
		public string ChainScale {get {return 'unbranded';}set;}
		public string IndChainScale {get {return 'upscale';}set;}
		public string FranchiseName {get {return 'unbranded hotel';}set;}
		public string ParentCompany {get {return 'unbranded hotel';}set;}
		public string Market {get {return 'Alaska';}set;}
		public string Tract {get {return 'Alaska';}set;}
		public string County {get {return 'Juneau';}set;}
		public string ThirdPartyManaged {get {return 'owner/operator';}set;}
		public string TypeOfOwnership {get {return 'unbranded';}set;}
		public string RecordStatus {get {return 'This is a new Renovation/Conversion project in December 2015.';}set;}
		public string Commentary {get {return 'The Gold Belt Hotel Juneau may convert to a four Points hotel.';}set;}

	//	public contactInfo DevOwnCompanyContact {get {return new contactInfo();}set;}
	//	public contactInfo ManagementCompanyContact {get {return new contactInfo();}set;}
	//	public contactInfo ArchitectCompanyContact {get {return new contactInfo();}set;}
	//	public contactInfo InteriorDesignCompanyContact {get {return new contactInfo();}set;}
	//	public contactInfo ContractorCompanyContact {get {return new contactInfo();}set;}
	//	public contactInfo PurchasingCompanyContact {get {return new contactInfo();}set;}


		public string HotelName {get {return 'Gold Belt Hotel Juneau';}set;}
		public string HotelAddress {get{return '12 main st';} set;}
		public string HotelAddress2 {get{return 'unit 123';} set;}
		public string city {get {return 'buffalo';} set;}
		public string state {get {return 'NY';} set;}
		public string postalCode {get{return '14214';} set;}
		public string HotelCountry {get{return 'US';} set;}		
		public string HotelPhone {get {return '1231231234';}set;}
		public string HotelFax {get {return '1231231234';}set;}
	
		public string DevOwnCompanyName {get {return 'Gold Belt Hotel Juneau';}set;}
		public string DevOwnCompanyAddress {get{return '12 main st';} set;}
		public string DevOwnCompanyCity {get {return 'buffalo';} set;}
		public string DevOwnCompanyState {get {return 'NY';} set;}
		public string DevOwnCompanyZip {get{return '14214';} set;}
		public string DevOwnCompanyCountry {get{return 'US';} set;}		
		public string DevOwnCompanyPhone {get {return '1231231234';}set;}
		public string DevOwnCompanyFax {get {return '1231231234';}set;}
	
		public string ManagementCompanyName {get {return 'Gold Belt Hotel Juneau';}set;}
		public string ManagementCompanyAddress {get{return '12 main st';} set;}
		public string ManagementCompanyCity {get {return 'buffalo';} set;}
		public string ManagementCompanyState {get {return 'NY';} set;}
		public string ManagementCompanyZip {get{return '14214';} set;}
		public string ManagementCompanyCountry {get{return 'US';} set;}		
		public string ManagementCompanyPhone {get {return '1231231234';}set;}
		public string ManagementCompanyFax {get {return '1231231234';}set;}
	
		public string ArchitectCompanyName {get {return 'Gold Belt Hotel Juneau';}set;}
		public string ArchitectCompanyAddress {get{return '12 main st';} set;}
		public string ArchitectCompanyCity {get {return 'buffalo';} set;}
		public string ArchitectCompanyState {get {return 'NY';} set;}
		public string ArchitectCompanyZip {get{return '14214';} set;}
		public string ArchitectCompanyCountry {get{return 'US';} set;}		
		public string ArchitectCompanyPhone {get {return '1231231234';}set;}
		public string ArchitectCompanyFax {get {return '1231231234';}set;}
	
		public string InteriorDesignCompanyName {get {return 'Gold Belt Hotel Juneau';}set;}
		public string InteriorDesignCompanyAddress {get{return '12 main st';} set;}
		public string InteriorDesignCompanyCity {get {return 'buffalo';} set;}
		public string InteriorDesignCompanyState {get {return 'NY';} set;}
		public string InteriorDesignCompanyZip {get{return '14214';} set;}
		public string InteriorDesignCompanyCountry {get{return 'US';} set;}		
		public string InteriorDesignCompanyPhone {get {return '1231231234';}set;}
		public string InteriorDesignCompanyFax {get {return '1231231234';}set;}
	
		public string ContractorCompanyName {get {return 'Gold Belt Hotel Juneau';}set;}
		public string ContractorCompanyAddress {get{return '12 main st';} set;}
		public string ContractorCompanyCity {get {return 'buffalo';} set;}
		public string ContractorCompanyState {get {return 'NY';} set;}
		public string ContractorCompanyZip {get{return '14214';} set;}
		public string ContractorCompanyCountry {get{return 'US';} set;}		
		public string ContractorCompanyPhone {get {return '1231231234';}set;}
		public string ContractorCompanyFax {get {return '1231231234';}set;}
	
		public string PurchasingCompanyName {get {return 'Gold Belt Hotel Juneau';}set;}
		public string PurchasingCompanyAddress {get{return '12 main st';} set;}
		public string PurchasingCompanyCity {get {return 'buffalo';} set;}
		public string PurchasingCompanyState {get {return 'NY';} set;}
		public string PurchasingCompanyZip {get{return '14214';} set;}
		public string PurchasingCompanyCountry {get{return 'US';} set;}		
		public string PurchasingCompanyPhone {get {return '1231231234';}set;}
		public string PurchasingCompanyFax {get {return '1231231234';}set;}
	}*/
    
}