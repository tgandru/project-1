// Apex Class related to  PBEIntegration class
// ---------------------------------------------------------------------------------------------------------------------------------
//  Author          Date         Description                    Purpose
// ---------------------------------------------------------------------------------------------------------------------------------
// Kavitha M      03/18/2016      Created         To process Pricebook Entries based on Http Response Body 
//                                                + create child Message Queues
// Kavitha M      04/26/2016      Updated         Updated JSON Response format + included DirectPriceBookAssign__c 
//                                                processing logic + upsert dpaListToUpsert + Recurssive Enqueue jobs
// Kavitha M      04/29/2016      Updated         Category parameter
// Kavitha M      05/03/2016      Updated         Added logic to upsert to Standard PriceBook + Mobile Pricebook
// MK             07/06/2016      Updated         Changing class type to not queue PBE responses but process them real time  
// ---------------------------------------------------------------------------------------------------------------------------------

public class managePriceBookEntry { //implements Queueable{

private List<PBEIntegration.pricingRecords> pbeList;
private List<Account> accList;
private map<String, PBEIntegration.pricingRecords> productCodeMap;
private map<String, PBEIntegration.pricingRecords> priceBookCodeMap;
private map<String, PBEIntegration.pricingRecords> priceBookAccountMap;
private map<String, Id> extIDToProductIDMap;
private map<String, Id> extIDToPriceBookIDMap;
private map<String, Id> extIDToAccountCodeIdMap;
private map<String, Id> extIDToAccountIDMap;
private Map<String, PricebookEntry> pbeMAP; 
private Map<String, PricebookEntry> pbeStandardMap;
private Map<String, PricebookEntry> pbeMobileMap;
private Map<String, DirectPriceBookAssign__c> dpaMap;
private Map<Integer, list<PBEIntegration.pricingRecords>> pbeMapDetails;
//private set<String> mobilePriceBooks;
private List<PricebookEntry> pbeListToUpsert;
private List<PricebookEntry> pbeStandardListToUpsert;
private List<PricebookEntry> pbeMobileListToUpsert;
private Map<string,PricebookEntry> mobileMap; 
private List<DirectPriceBookAssign__c> dpaListToUpsert;
private List<message_queue__c> mqRecordsToInsert;
private Id recurssivejobId;
private Id mqId;
private Integer i;
private String StandardPBId;
private string MobilePBId;
private String tempstring;
private String tempDPAExtID;
private String tempExtIDstring;
private String tempExtIdForStandardPB;
private String tempExtIdForMobilePB;
private Boolean processRecursive;
    // Changing constructor to process PBE lists directly received from the response
    //public ManagePriceBookEntry(Map<Integer, list<PBEIntegration.pricingRecords>> pbeMapVal, Integer j, Id messageQueueId){  
    public ManagePriceBookEntry(List<PBEIntegration.pricingRecords> orginialPbeList, Id messageQueueId) {      
        //pbeMapDetails = new Map<Integer, list<PBEIntegration.pricingRecords>>();
        //pbeMapDetails = pbeMapVal;
        //System.debug('## pbeMapVal'+pbeMapVal.keyset()+'::'+pbeMapVal);
        //System.debug('## pbeMapDetails'+pbeMapDetails.keySet()+'::'+pbeMapDetails);
        //this.pbeList = pbeMapDetails.get(j);
        System.debug('## originalPbeList -- '+ orginialPbeList);
        this.pbeList = orginialPbeList;
        //System.debug('## jvalue'+j);
        //i=j;
        //i++;
        processRecursive = false;
        productCodeMap = new map<String, PBEIntegration.pricingRecords>();
        priceBookCodeMap = new map<String, PBEIntegration.pricingRecords>();
        priceBookAccountMap = new map<String, PBEIntegration.pricingRecords>();
        pbeStandardMap = new Map<String, PricebookEntry>();
        pbeMobileMap = new Map<String,PricebookEntry>();
        pbeListToUpsert = new List<PricebookEntry>();
        pbeStandardListToUpsert = new List<PricebookEntry>();
        pbeMobileListToUpsert = new List<PricebookEntry>();
        mobileMap = new Map<string,PricebookEntry>();
        dpaListToUpsert = new List<DirectPriceBookAssign__c>();
        pbeMAP = new Map<String, PricebookEntry>();
        dpaMap = new Map<String, DirectPriceBookAssign__c>();
        mqRecordsToInsert = new List<message_queue__c>();
        extIDToProductIDMap = new map<String, Id>();
        extIDToPriceBookIDMap = new map<String, Id>();
        extIDToAccountCodeIdMap = new map<String, Id>();
        extIDToAccountIDMap = new map<String, Id>();
        //mobilePriceBooks = new Set<String>{'ATT','Verizon','Sprint','T-Mobile'};
        mqId = messageQueueId;
        tempExtIdForStandardPB = '';
        tempExtIdForMobilePB = '';
        tempExtIDstring = '';
        tempDPAExtID = '';
        tempstring='';
        executePriceBookEntry();       
    }
    
    //public void execute(QueueableContext context){
    public void executePriceBookEntry(){
        
        //Id tempPricebookId='';
        String sdsit=label.SDSA_IT_Code;
        String sdsmbl=label.SDSA_MBL_Code;
        //Assuming SAP_Price_Account_Code__c is populated only for mobile (ATT, T-Mobile, Sprint, Verizon) 
        for(pricebook2 pbList:[select Id, SAP_Price_Account_Code__c from PriceBook2 where SAP_Price_Account_Code__c !='' ]){          
          extIDToAccountCodeIdMap.put(pbList.SAP_Price_Account_Code__c, pbList.Id);  
        }
        
        //get Standard PriceBook, Mobile PriceBook Ids
        for(pricebook2 pb:[select Id,Name from PriceBook2 where Name IN ('Standard Price Book','Mobile') ]){
          if(pb.Name == 'Standard Price Book')
            StandardPBId = pb.Id;
          else if(pb.Name == 'Mobile')
            MobilePBId = pb.Id;       
        }
        
        //Look for null values
        for(PBEIntegration.pricingRecords obj: pbeList){
            if(string.isNotBlank(obj.materialNumber)) {
              System.debug('## Product Record -- ' + obj.MaterialNumber); 
                productCodeMap.put(obj.materialNumber, obj);
            }
            
            if(string.isNotBlank(obj.productPriceGroup)) {
              System.debug('## Price Book Record -- ' + obj.productPriceGroup);   
                priceBookCodeMap.put(obj.productPriceGroup, obj); 
          }
          
            if((obj.category == 'MOBILE') && string.isNotBlank(obj.accountCode) && extIDToAccountCodeIdMap.containsKey(obj.accountCode)){
                System.debug('## Removing PriceBook Record -- ' + obj.productPriceGroup);   
                priceBookCodeMap.remove(obj.productPriceGroup);
            }

            if((obj.category == 'MOBILE') && string.isNotBlank(obj.accountCode))  {
              System.debug('## PriceBook Account Record -- ' + obj.accountCode);   
                priceBookAccountMap.put(obj.accountCode, obj);  
            }
        }
        
        System.debug('##productCodeMap'+productCodeMap);
        System.debug('##priceBookCodeMap'+priceBookCodeMap);
        System.debug('##priceBookAccountMap'+priceBookAccountMap);
        
        //get Account SFDC Ids
        if(priceBookAccountMap != null && priceBookAccountMap.size()>0){
          for(Account acc:[select Id, SAP_Company_Code__c from Account where SAP_Company_Code__c IN:priceBookAccountMap.keySet()]){
            extIDToAccountIDMap.put(acc.SAP_Company_Code__c, acc.Id);  
          }
        }
        System.debug('##extIDToAccountIDMap'+extIDToAccountIDMap);
        
        //get Product SFDC Ids
        if(productCodeMap !=null && productCodeMap.size()>0){
            for(product2 product:[Select Id, SAP_Material_ID__c from Product2 where  SAP_Material_ID__c IN:productCodeMap.keySet() ]){
                extIDToProductIDMap.put(product.SAP_Material_ID__c, product.Id);
            }
        }
        System.debug('##extIDToProductIDMap'+extIDToProductIDMap);
        
        //get Pricebook SFDC Ids for the Category != mobile
        if(!priceBookCodeMap.isEmpty()){
            //T16522 - Merge Pricebook 86, 105 to Pricebook 84 - 06.21.16 - KR
            //This modification would allow any future pricebook merges as long as the collapsed pricebooks are populated on SAP_Price_Group_Code__c (comma separated)
            List<String> pricebookSplitStrings = new List<string>();
            for(PriceBook2 pb:[Select Id, SAP_Price_Group_Code__c from PriceBook2 where SAP_Price_Group_Code__c!=null ]){
                if(pb.SAP_Price_Group_Code__c.contains(',')){
                    pricebookSplitStrings = pb.SAP_Price_Group_Code__c.split(',');
                    for(string temp:pricebookSplitStrings){
                        extIDToPriceBookIDMap.put(temp,pb.Id);
                    }
                }
                else {
                    extIDToPriceBookIDMap.put(pb.SAP_Price_Group_Code__c,pb.Id);
                }
            }    
            //for(PriceBook2 pb:[Select Id, SAP_Price_Group_Code__c from PriceBook2 where SAP_Price_Group_Code__c IN:priceBookCodeMap.keySet() AND SAP_Price_Group_Code__c = '84' AND IsActive=true]) - commented on 06.22 by KR                      
        }
        System.debug('##extIDToPriceBookIDMap'+extIDToPriceBookIDMap);
         
        //Query existing Pricebook Entries
        for(PricebookEntry pbe:[Select Id,Name, Pricebook2Id,Product2Id,External_Id__c,UnitPrice from PricebookEntry where Product2Id IN: extIDToProductIDMap.values() and BY_Pass_Price_Integration__c !=true]){
            if(string.isNotBlank(pbe.External_Id__c)) {
              pbeMAP.put(pbe.External_Id__c,pbe);
            }
        }
        
       /* for(PricebookEntry p :[Select Id,Name, Pricebook2Id,Product2Id,External_Id__c,UnitPrice from PricebookEntry where Pricebook2Id =: StandardPBId]){ 
              pbeStandardMap.put(p.External_Id__c,p);
        }*/
        //Added New Filed into filter Condition to ByPass the priceUpdate for Mobile Pricebook -- Vijay 11/8/2017
        for(PricebookEntry pb :[Select Id,Name, Pricebook2Id,Product2Id,External_Id__c,UnitPrice,BY_Pass_Price_Integration__c from PricebookEntry where BY_Pass_Price_Integration__c !=true and  Pricebook2Id =: MobilePBId]){     
              pbeMobileMap.put(pb.External_Id__c,pb);
        }
     
         System.debug('##pbeMAP'+pbeMAP);
         System.debug('##pbeStandardMap'+pbeStandardMap);
         System.debug('##pbeMobileMap'+pbeMobileMap);
                          
        //Query existing DirectPriceBookAssign__c
        for(DirectPriceBookAssign__c dpa:[select Id, Direct_Account__c, Price_Book__c,External_Id__c from DirectPriceBookAssign__c where Direct_Account__c IN:extIDToAccountIDMap.values()]){
          dpaMAP.put(dpa.External_Id__c,dpa);
      }
      System.debug('##dpaMAP'+dpaMAP);
      
        //process inserts to Standard Pricebook, Mobile Pricebook, Pricebook Entries and DirectPriceBookAssign__c
        for(PBEIntegration.pricingRecords pbeObject : pbeList){
          System.debug('###--> '+pbeObject.materialNumber+','+pbeObject.accountCode+','+pbeObject.invoicePrice+','+pbeObject.category);
            string productExtId = extIdToProductIdMap.get(pbeObject.materialNumber);  //extIDToPriceBookIDMap          
            //string priceBookExtId = ((pbeObject.category == 'Mobile') && extIDToAccountCodeIdMap.ContainsKey(pbeObject.accountCode)) ? extIDToAccountCodeIdMap.get(pbeObject.accountCode) : extIDToPriceBookIDMap.get(pbeObject.productPriceGroup);
            // Chnaged from String to Id
            Id priceBookExtId;
            if((pbeObject.category == 'Mobile') && extIDToAccountCodeIdMap.containsKey(pbeObject.accountCode)){
                priceBookExtId = extIDToAccountCodeIdMap.get(pbeObject.accountCode);
            }
            else if(extIDToPriceBookIDMap.containsKey(pbeObject.productPriceGroup) &&(pbeObject.accountCode !=sdsit) ){
                priceBookExtId = extIDToPriceBookIDMap.get(pbeObject.productPriceGroup);
            }else if(pbeObject.accountCode ==sdsit){
                priceBookExtId =extIDToAccountCodeIdMap.get(pbeObject.accountCode);
            }
            System.debug('$$$--> priceBookExtId='+priceBookExtId+',productExtId='+productExtId);
            tempExtIDstring=''; // added this line by HongSeong Jeon(12/27/2016)
            If(string.IsNotBlank(priceBookExtId) && string.IsNotBlank(productExtId)){
                tempExtIDstring = productExtId + priceBookExtId;
            }
            System.debug('## External_Id__c :: If Block::'+tempExtIDstring);
            
            //Upsert records to Mobile Pricebook Entry
             if(pbeObject.category == 'Mobile'&&pbeObject.accountCode !=sdsmbl){
               tempExtIdForMobilePB = productExtId+MobilePBId;
               // Only Active Products will be considered
               if(extIdToProductIdMap.get(pbeObject.materialNumber)!=null){
                    //Fetch records to upserted to Mobile Pricebook
                   if(!pbeMobileMap.containsKey(tempExtIdForMobilePB)){                                
                                              
                         mobileMap.put(tempExtIdForMobilePB,new PricebookEntry(Product2Id = extIdToProductIdMap.get(pbeObject.materialNumber),
                                                                Pricebook2Id = MobilePBId,
                                                                UnitPrice = Decimal.valueOf(pbeObject.invoicePrice),
                                                                isActive = true,
                                                                External_Id__c = String.valueOf(extIdToProductIdMap.get(pbeObject.materialNumber))+MobilePBId)); 
                        
                   } 
                   else {
                           pbeMobileMap.get(tempExtIdForMobilePB).UnitPrice = Decimal.valueOf(pbeObject.invoicePrice);
                           mobileMap.put(tempExtIdForMobilePB,pbeMobileMap.get(tempExtIdForMobilePB));
                   } 
                                      
               }   
            }             
             
            
            //Upsert records to respective PriceBook Entry 
            if(tempExtIDstring!=null && tempExtIDstring!='') 
            {
                if(pbeMAP.containsKey(tempExtIDstring)){
                    pbeMAP.get(tempExtIDstring).UnitPrice = Decimal.valueOf(pbeObject.invoicePrice);
                    pbeListToUpsert.add(pbeMAP.get(tempExtIDstring));   
                    
                    System.debug('## pbeListToUpsert : If Block::'+pbeListToUpsert.size()+'::'+pbeListToUpsert);        
                }   
                else {      
                  if(extIdToProductIdMap.get(pbeObject.materialNumber)!=null && String.isNotBlank(priceBookExtId)){ //(extIDToPriceBookIDMap.get(pbeObject.productPriceGroup)!=null || extIDToAccountCodeIdMap.get(pbeObject.accountCode)!=null) - commented on 06.22 by KR
                      pbeListToUpsert.add(new PricebookEntry(Product2Id = extIdToProductIdMap.get(pbeObject.materialNumber),
                                                              Pricebook2Id = priceBookExtId, //extIDToPriceBookIDMap.get(pbeObject.productPriceGroup) - commented on 06.22 by KR
                                                              UnitPrice = Decimal.valueOf(pbeObject.invoicePrice),
                                                              isActive = true,
                                                              External_Id__c = String.valueOf(extIdToProductIdMap.get(pbeObject.materialNumber))+priceBookExtId));
                      //External_Id__c = String.valueOf(extIdToProductIdMap.get(pbeObject.materialNumber))+String.valueOf(extIDToPriceBookIDMap.get(pbeObject.productPriceGroup)) - KR Commented on 07/11/16
                      System.debug('## pbeListToUpsert : Else Block::'+pbeListToUpsert.size()+'::'+pbeListToUpsert);
                      System.debug('## External_Id__c :: Else block::'+String.valueOf(extIdToProductIdMap.get(pbeObject.materialNumber))+String.valueOf(extIDToPriceBookIDMap.get(pbeObject.productPriceGroup)));
                  }
                }   
           }                                        
        
          //create an Upsert list for every pricing record (getting in response) on DirectPriceBookAssign__c 
        
            tempDPAExtID = extIDToAccountIDMap.get(pbeObject.accountCode)+String.valueOf(priceBookExtId);
            System.debug('## DPA Pricebook'+priceBookExtId);
            System.debug('## DPA Account contains'+'::'+extIDToAccountIDMap.containsKey(pbeObject.accountCode)+'::'+extIDToAccountIDMap.get(pbeObject.accountCode));
            if(priceBookExtId !=null && extIDToAccountIDMap.containsKey(pbeObject.accountCode) && !dpaMAP.containsKey(tempDPAExtID)){
                dpaListToUpsert.add(new DirectPriceBookAssign__c(Direct_Account__c = extIDToAccountIDMap.get(pbeObject.accountCode),
                                        Price_Book__c = priceBookExtId,
                                        External_Id__c= tempDPAExtID));
         
            }   

        }
        
       // Upsert DPAList
       if(dpaListToUpsert.size()>0){
           System.debug('## dpaListToUpsert ::'+dpaListToUpsert.size()+'::'+dpaListToUpsert); 
           Schema.SObjectField extIdField = DirectPriceBookAssign__c.Fields.External_Id__c;
           //Removing the Duplicates from list
           set<DirectPriceBookAssign__c> uniqdpaSetInsert = new Set<DirectPriceBookAssign__c>(dpaListToUpsert);
           List<DirectPriceBookAssign__c> uniqdpaLstInsert = new List<DirectPriceBookAssign__c>();
           uniqdpaLstInsert.addAll(uniqdpaSetInsert);
           System.debug('## unique dpaListToUpsert ::'+uniqdpaLstInsert.size()+'::'+uniqdpaLstInsert); 
           Database.UpsertResult[] sr = Database.upsert(uniqdpaLstInsert,extIdField, false);
            
            for(Database.UpsertResult s: sr)
                if(!s.isSuccess()) system.debug('Failed inserting direct price book assign..'+s.getErrors());
            uniqdpaSetInsert.clear();
            dpaListToUpsert.clear();
            uniqdpaLstInsert.clear();
                
       }
       
      
       Schema.SObjectField idField = PriceBookEntry.Fields.External_Id__c;
       //Upsert pbeStandardListToUpsert
       if(pbeStandardListToUpsert.size()>0){
          System.debug('## pbeStandardListToUpsert ::'+pbeStandardListToUpsert.size()+'::'+pbeStandardListToUpsert);
          //Removing the Duplicates from list
          set<PricebookEntry> uniqstdpbeSetInsert = new Set<PricebookEntry>(pbeStandardListToUpsert);
          List<PricebookEntry> uniqstdpbeLstInsert = new List<PricebookEntry>();
          uniqstdpbeLstInsert.addAll(uniqstdpbeSetInsert);
          System.debug('## unique uniqstdpbeLstInsert ::'+uniqstdpbeLstInsert.size()+'::'+uniqstdpbeLstInsert); 
          Database.UpsertResult[] sr = Database.upsert(uniqstdpbeLstInsert,idField, false);    
           for(Database.UpsertResult s: sr)
                if(!s.isSuccess()) system.debug('Failed inserting std pricebookentry..'+s.getErrors());  
          
          pbeStandardListToUpsert.clear();
           uniqstdpbeSetInsert.clear();
           uniqstdpbeLstInsert.clear();
          
       }
       
        //Upsert pbeMobileListToUpsert
        if(mobileMap.size()>0){
          pbeMobileListToUpsert = mobileMap.values();   
          System.debug('## pbeMobileListToUpsert ::'+pbeMobileListToUpsert.size()+'::'+pbeMobileListToUpsert);
          //Removing the Duplicates from list
          set<PricebookEntry> uniqMobPbeSetInsert = new Set<PricebookEntry>(pbeMobileListToUpsert);
          List<PricebookEntry> uniqMobPbeLstInsert = new List<PricebookEntry>();
          uniqMobPbeLstInsert.addAll(uniqMobPbeSetInsert);
          System.debug('## unique dpaListToUpsert ::'+uniqMobPbeLstInsert.size()+'::'+uniqMobPbeLstInsert); 
           
         Database.UpsertResult[] sr = Database.upsert(uniqMobPbeLstInsert,idField, false);      
         for(Database.UpsertResult s: sr)
                if(!s.isSuccess()) system.debug('Failed inserting pbe mobile list..'+s.getErrors()); 
          
          pbeMobileListToUpsert.clear();
           uniqMobPbeSetInsert.clear();
           uniqMobPbeLstInsert.clear();  
        }
        
        try {
            System.debug('## Before Upsert :: pbeListToUpsert :: '+pbeListToUpsert);
            System.debug('## Before Upsert with duplicate entries :: pbeListToUpsert :: '+pbeListToUpsert.size());
            string tempString ='';
            map<string,PriceBookEntry> pbeins = new map<string,PricebookEntry>();
            List<PriceBookEntry> uniquePbeList1 = new List<PriceBookEntry>();
            for(PriceBookEntry p:pbeListToUpsert){
                pbeins.put(p.External_Id__c,p);
            }
            for(string s:pbeins.Keyset()){
                uniquePbeList1.add(pbeins.get(s));
            }
            Set<PricebookEntry> pbeSet = new Set<PriceBookEntry>(pbeListToUpsert);
            List<PriceBookEntry> uniquePbeList = new List<PriceBookEntry>();
            uniquePbeList.addAll(pbeSet);
            //System.debug('## Before Upsert :: uniquePbeList :: '+uniquePbeList.size());
            //Database.UpsertResult[] sr = Database.upsert(uniquePbeList,idField, false);
            System.debug('## Before Upsert :: uniquePbeList1 :: '+uniquePbeList1.size());
            Database.UpsertResult[] sr = Database.upsert(uniquePbeList1,idField, false);
            for (Integer i=0; i< sr.size(); i++) {
                if (sr[i].isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed             
                System.debug('Successfully inserted PriceBook Entrries ID: ' + sr[i].getId());
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr[i].getErrors()) {   
                        
                        tempString = tempString+'..Record #'+ i+' - Error Msg: '+string.valueOf(err.getMessage())+' - Record Details : Product2Id -:'+uniquePbeList1[i].Product2Id+' Pricebook2Id -:'+uniquePbeList1[i].Pricebook2Id+' UnitPrice -:'+uniquePbeList1[i].UnitPrice+' External Id -:'+uniquePbeList1[i].External_Id__c;
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Fields that affected this error: ' + err.getFields());
                        }
                    }
                }
               
                //Query MessageQueue - to Update success/Error details 
                Message_Queue__c updateMQ = [select Id,Status__c,ERRORTEXT__c from Message_Queue__c where id = :mqId];
                if(tempString == ''){  
                    
                    updateMQ.Status__c = 'success';
                    updateMQ.ERRORTEXT__c = string.valueof(uniquePbeList1.size())+' priceBook Entries were processed here';

                    System.debug('Updating new Success Message Queue -- MQ ID: ' + mqId);

                }   
                else {
                    System.debug('Inserting new Error Message Queue -- MQ ID: ' + mqId);
                    updateMQ.Status__c = 'Failed';
                    updateMQ.ERRORTEXT__c = tempString;           

                }   
                update updateMQ;
            
            
              /*  if(mqRecordsToInsert.size()>0){
                    insert  mqRecordsToInsert;
                }*/
                          
            }//Try block ends
            catch(Exception ex) {
                System.debug('## Exception in processing PBE :: Catch block ::'+ex);
            }
             

    }

}