public with sharing class messageQueueHelper {

        public void generalFail(Message_queue__c omq, Exception ex, Quote quoteVar, String otherErr, DateTime initializeTime){
            if(omq.retry_counter__c<=5){
                failedRetry(omq,ex,otherErr,initializeTime);
            }else{
                failed(omq,ex,quoteVar,otherErr,initializeTime);
            }
        }

        public void failedRetry(Message_queue__c omq, Exception ex, String otherErr, DateTime initializeTime){
            System.debug('lclc in failedRetry mqhelper');
            omq.MSGSTATUS__c = 'Queued - Retry';
            omq.retry_counter__c = omq.retry_counter__c + 1;
            omq.Status__c='failed-retry';
            if(ex!=null){
                omq.ERRORTEXT__c = string.valueof(ex); 
            }else if(otherErr!=null){
                omq.ERRORTEXT__c=otherErr;
            }
            if(omq.Initalized_Request_Time__c==null && initializeTime!=null){
                omq.Initalized_Request_Time__c=initializeTime;
            }
           
            try{
                upsert omq;
            }catch(Exception e){
                 System.debug('ERROR IN HELPER CLASS ---- '+e);
            }
        }

        public void failed(Message_queue__c omq, Exception ex, Quote quoteVar, String otherErr, DateTime initializeTime){
            System.debug('lclc in failed mqhelper '+quoteVar);
            omq.MSGSTATUS__c ='Failed';
            if(ex!=null){
                omq.ERRORTEXT__c = string.valueof(ex); 
            }else if(otherErr!=null){
                omq.ERRORTEXT__c=otherErr;
            }
            if(omq.Initalized_Request_Time__c==null && initializeTime!=null){
                omq.Initalized_Request_Time__c=initializeTime;
            }
            omq.Status__c ='failed';
            /*if(quoteVar!=null){
                    quoteVar.Failed_Update_Number__c+=1;
            }*/

            try{
                upsert omq;
                if(quoteVar!=null){
                    //update quoteVar;
                    //if(quoteVar.Failed_Update_Number__c>0){
                        quoteVar.Integration_Status__c='error';
                        System.debug('lclc going to post to chatter? ');
                        //QuoteSubmitApprovalButtonCustomExtension cont=new QuoteSubmitApprovalButtonCustomExtension(new ApexPages.StandardController(quoteVar));
                        //cont.postToChatterNegative(quoteVar);
                        update quoteVar;
                    //}
                }
            }catch(Exception e){
                System.debug('ERROR IN HELPER CLASS ---- '+e);
            }
        }

        public void success(Message_queue__c omq, Quote quoteVar, String errorTxt, Long requestTime, Long updateTime, DateTime initializeTime){
            System.debug('lclc in success mqhelper ');
            omq.MSGSTATUS__c = 'success';
            if(errorTxt!=null){
                omq.ERRORTEXT__c = errorTxt;
            }else{
                omq.ERRORTEXT__c = null;
            }
            omq.Status__c='success';
            if(omq.Initalized_Request_Time__c==null){
                omq.Initalized_Request_Time__c=initializeTime;
            }
            if(requestTime!=null){
                omq.Request_To_CIH_Time__c=Integer.valueOf(requestTime);
            }
            if(updateTime!=null){
                omq.Response_Processing_Time__c=Integer.valueOf(updateTime);
            }
           

            try{
                upsert omq;
                System.debug('lclc quoteVar '+quoteVar + ' Integration_Flow_Type__c '+omq.integration_flow_type__c);
                if(quoteVar!=null && omq.Integration_Flow_Type__c=='submit quote for approval'){
                    System.debug('lclc in submit quote for approval success');
                    //update quoteVar;
                    //if(quoteVar.Successful_Update_Number__c==quoteVar.Total_Lines_For_Update__c){
                        //QuoteSubmitApprovalButtonCustomExtension cont=new QuoteSubmitApprovalButtonCustomExtension(new ApexPages.StandardController(quoteVar));
                        //cont.postToChatterPositive(quoteVar);
                    //}
                    quoteVar.Last_Update_from_CastIron__c=DateTime.now();
                    quoteVar.Integration_Status__c='success';
                    update quoteVar;
                }else if(quoteVar!=null && omq.Integration_Flow_Type__c=='008 to 012'){
                    System.debug('lclc in 008 to 012 success');
                    quoteVar.Last_Update_from_CastIron__c=DateTime.now();
                    update quoteVar;
                    RoiIntegrationHelper.callRoiEndpointNoFuture(quoteVar.Id);
                }
            }catch(Exception e){
                System.debug('ERROR IN HELPER CLASS ---- '+e);
            }
        }
}