public with sharing class opportunityProductRedirectExtension {

    Id oppId;
    Id quoteId;
    string tempId;

    // we are extending the OpportunityLineItem controller, so we query to get the parent OpportunityId
    public opportunityProductRedirectExtension(ApexPages.StandardController controller) {
    	tempId = controller.getRecord().Id;
    	
    	if(tempId.startsWithIgnoreCase('00k')){
    		oppId = [select Id, OpportunityId from OpportunityLineItem where Id = :tempId limit 1].OpportunityId;
    	} else if(tempId.startsWithIgnoreCase('0QL')) {
    		QuoteLineItem qli = [select QuoteId, Quote.OpportunityId from QuoteLineItem where Id = :tempId limit 1];
    		oppId = qli.Quote.OpportunityId;
    		quoteId = qli.quoteId;
    	}
    	
    	
    	
        
    }
    
    // then we redirect to our desired page with the Opportunity Id in the URL
    public pageReference redirect(){
    	
    	if(tempId.startsWithIgnoreCase('00k')){
    		return new PageReference('/apex/opportunityProductEntry?id=' + oppId);
    	} else if(tempId.startsWithIgnoreCase('0QL')) {
    		return new PageReference('/apex/QuoteProductEdit?id=' + oppId +'&quoteId='+quoteId);
    	}
    	
        return null;
    }

}