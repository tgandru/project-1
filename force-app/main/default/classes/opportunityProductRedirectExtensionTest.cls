@isTest
private class opportunityProductRedirectExtensionTest {
	static Id endUserRT = TestDataUtility.retrieveRecordTypeId('End_User', 'Account');
	static Id inDirectRT = TestDataUtility.retrieveRecordTypeId('Indirect', 'Account');
	static Id directRT = TestDataUtility.retrieveRecordTypeId('Direct','Account');

	@isTest static void test_method_one() {
		// Implement test code
		//Channelinsight configuration
		Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
		
	    //Creating test Account
	    Account acct=TestDataUtility.createAccount('Main Account');
	    acct.SAP_Company_Code__c=TestDataUtility.generateRandomString(12);
	    acct.RecordTypeId = endUserRT;
		insert acct;
		
	    
	   //Creating the Parent Account for Distributor as Partners
		Account paDistAcct=TestDataUtility.createAccount('Distributor Account');
		paDistAcct.RecordTypeId = directRT;
		paDistAcct.Type= 'Distributor';
		insert paDistAcct;

		//Creating the Parent Account for Reseller as Partners
		Account paResellAcct=TestDataUtility.createAccount('Reseller Account');
		paResellAcct.RecordTypeId = inDirectRT;
		paResellAcct.Type = 'Corporate Reseller';
		insert paResellAcct;
		
		PriceBook2 priceBook = TestDataUtility.createPriceBook('Shady Records');
        insert pricebook;

        Opportunity opportunity = TestDataUtility.createOppty('New', acct, pricebook, 'Managed', 'IT', 'product group',
                                    'Identified');
        insert opportunity;

        //Creating the partners
		 Partner__c partner1 = TestDataUtility.createPartner(opportunity,acct,paDistAcct,'Distributor');
		 insert partner1 ;

		 Partner__c partner2 = TestDataUtility.createPartner(opportunity,acct,paResellAcct,'Corporate Reseller');
		 insert partner2 ;

		 // Create Channel
		 List<Channel__c> channels = new List<Channel__c>();
		 Channel__c channel = TestDataUtility.createChannel(opportunity, paDistAcct, paResellAcct, partner1, partner2);
		 channels.add(channel);
		 insert channels;

        Product2 product = TestDataUtility.createProduct2('SL-SCF5300/SEG', 'Standalone Printer', 'Printer[C2]', 'H/W', 'FAX/MFP');
        insert product;
        
        PricebookEntry pricebookEntry = TestDataUtility.createPriceBookEntry(product.Id, priceBook.Id);
        insert pricebookEntry;

        //List<OpportunityLineItem> opportunityLineItems = new List<OpportunityLineItem>();
        OpportunityLineItem op1 = TestDataUtility.createOpptyLineItem(product, pricebookEntry, opportunity);
        op1.Quantity = 100;
        op1.TotalPrice = 1000;
        op1.Requested_Price__c = 100;
        op1.Claimed_Quantity__c = 45;     
        //opportunityLineItems.add(op1);
        //insert opportunityLineItems;
        insert op1;

        opportunityProductRedirectExtension productExtension = new opportunityProductRedirectExtension(new ApexPages.StandardController(op1));
        productExtension.redirect();
	}
	
	@isTest static void test_method_two() {
		// Implement test code
		//Channelinsight configuration
		Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
		
	    //Creating test Account
	    Account acct=TestDataUtility.createAccount('Main Account');
	    acct.SAP_Company_Code__c=TestDataUtility.generateRandomString(12);
	    acct.RecordTypeId = endUserRT;
		insert acct;
		
	    
	   //Creating the Parent Account for Distributor as Partners
		Account paDistAcct=TestDataUtility.createAccount('Distributor Account');
		paDistAcct.RecordTypeId = directRT;
		paDistAcct.Type= 'Distributor';
		insert paDistAcct;

		//Creating the Parent Account for Reseller as Partners
		Account paResellAcct=TestDataUtility.createAccount('Reseller Account');
		paResellAcct.RecordTypeId = inDirectRT;
		paResellAcct.Type = 'Corporate Reseller';
		insert paResellAcct;
		
		PriceBook2 priceBook = TestDataUtility.createPriceBook('Shady Records');
        insert pricebook;

        Opportunity opportunity = TestDataUtility.createOppty('New', acct, pricebook, 'Managed', 'IT', 'product group',
                                    'Identified');
        insert opportunity;

        //Creating the partners
		 Partner__c partner1 = TestDataUtility.createPartner(opportunity,acct,paDistAcct,'Distributor');
		 insert partner1 ;

		 Partner__c partner2 = TestDataUtility.createPartner(opportunity,acct,paResellAcct,'Corporate Reseller');
		 insert partner2 ;

		 // Create Channel
		 List<Channel__c> channels = new List<Channel__c>();
		 Channel__c channel = TestDataUtility.createChannel(opportunity, paDistAcct, paResellAcct, partner1, partner2);
		 channels.add(channel);
		 insert channels;

        Product2 product = TestDataUtility.createProduct2('SL-SCF5300/SEG', 'Standalone Printer', 'Printer[C2]', 'H/W', 'FAX/MFP');
        insert product;
        
        PricebookEntry pricebookEntry = TestDataUtility.createPriceBookEntry(product.Id, priceBook.Id);
        insert pricebookEntry;

        List<OpportunityLineItem> opportunityLineItems = new List<OpportunityLineItem>();
        OpportunityLineItem op1 = TestDataUtility.createOpptyLineItem(product, pricebookEntry, opportunity);
        op1.Quantity = 100;
        op1.TotalPrice = 1000;
        op1.Requested_Price__c = 100;
        op1.Claimed_Quantity__c = 45;     
        opportunityLineItems.add(op1);
        insert opportunityLineItems;

        Quote quot = TestDataUtility.createQuote('Test Quote', opportunity, pricebook);
        quot.ROI_Requestor_Id__c=UserInfo.getUserId();
        quot.Status='Test is Approved';
        insert quot;

        Quote quot2 = TestDataUtility.createQuote('Test Quote2 ', opportunity, pricebook);
        quot2.ROI_Requestor_Id__c=UserInfo.getUserId();
        quot2.Status='Test is Approved';
        insert quot2;
        
        //List<QuoteLineItem> qlis = new List<QuoteLineItem>();
        QuoteLineItem standardQLI = TestDataUtility.createQuoteLineItem(quot.Id, product, pricebookEntry, op1);
        standardQLI.Quantity = 25;
        standardQLI.UnitPrice = 100;
        standardQLI.Requested_Price__c=100;
        standardQLI.OLIID__c = op1.id;
        //qlis.add(standardQLI);
        //insert qlis;
        insert standardQLI;

        opportunityProductRedirectExtension productExtension = new opportunityProductRedirectExtension(new ApexPages.StandardController(standardQLI));
        productExtension.redirect();

	}
	
}