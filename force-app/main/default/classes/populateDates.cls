global class populateDates implements Database.Batchable<sObject>{


   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator([SELECT Id,status,CompletedDate,createddate,targetObjectId FROM ProcessInstance]);
   }

   global void execute(Database.BatchableContext BC, List<ProcessInstance> scope){
       set<string> quoteIds = new set<string>();
       for(ProcessInstance p : scope){
           string s = p.targetObjectId;
           if(s.startswith('0Q0')){
               quoteIds.add(s);
           }
       }
       
       if(quoteIds.size() > 0){
           map<id,Quote> qt = new map<id,Quote>([select id,	Status,SPA_Submission_Date__c,SPA_Arppoval_Date__c from Quote where id in:quoteIds  and SPA_Submission_Date__c = null ]);//in:quoteIds
           system.debug('*********'+qt);
           for(ProcessInstance p : scope){
               string s = p.targetObjectId;
               if(s.startswith('0Q0')){
                   if(qt.get(s) != null){
                       
                       qt.get(s).SPA_Submission_Date__c = p.createddate;
                      
                        qt.get(s).SPA_Arppoval_Date__c = null;
                       
                       if(p.CompletedDate != null){
                            if(qt.get(s).Status =='Approved' ||qt.get(s).Status=='Presented'){
                           qt.get(s).SPA_Arppoval_Date__c = p.CompletedDate;
                       }
                       }
                       
                       
                   }
               }
           }
           if(qt.size() > 0){
               update qt.values();
           }
       }
    }

   global void finish(Database.BatchableContext BC){
   }
   
   
}