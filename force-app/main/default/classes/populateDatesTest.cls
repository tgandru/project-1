@isTest
private class populateDatesTest {

    static testmethod void testpopulatingDates(){
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Account acc1 = new account(Name ='Test', Type='Customer',SAP_Company_Code__c = 'AAA');
        insert acc1;
        Id rtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('IT').getRecordTypeId();
        
        Opportunity opp = new Opportunity(accountId=acc1.id,recordtypeId=rtId,
        StageName='Identified',Name='TEst Opp',Type='Tender',Division__c='IT',ProductGroupTest__c='LCD_MONITOR',LeadSource='BTA Dealer',Closedate=system.today());
        insert opp;
        
        Quote qt = new Quote(name='Test ', OpportunityId=opp.id,ProductGroupMSP__c='LCD_MONITOR',status='Draft',Division__c='IT',Quote_Approval__c=true);
        insert qt;
        
        Approval.ProcessSubmitRequest req1 =  new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(qt.id);
        req1.setSubmitterId(userInfo.getuserId()); 
        req1.setNextApproverIds(new list<string>{userInfo.getuserId()});
        Approval.ProcessResult result = Approval.process(req1);
        
        
        qt.SPA_Submission_Date__c = null;
        update qt;
        test.startTest();
            database.executeBatch(new populateDates());
        test.stopTest();
        
    }

}