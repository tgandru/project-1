global class populateHasAttachment implements Database.Batchable<sObject>{


   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator([SELECT Id,ParentId FROM Attachment]);
   }

   global void execute(Database.BatchableContext BC, List<Attachment> scope){
       set<string> OppIds = new set<string>();
       for(Attachment a : scope){
           string s = a.ParentId;
           if(s.startswith('006')){
               OppIds.add(s);
           }
       }
       
       if(OppIds.size() > 0){
           map<id,Opportunity > op = new map<id,Opportunity >([SELECT Has_Attachment__c,Id FROM Opportunity where id in:OppIds   ]);//in:quoteIds
           system.debug('*********'+op);
           for(Attachment a : scope){
               string s = a.ParentId;
               if(s.startswith('006')){
                   if(op.get(s) != null){
                      op.get(s).Has_Attachment__c = true;
                   }
               }
           }
           if(op.size() > 0){
               Database.Update(op.values(), false);
               //update op.values();
           }
       }
    }

   global void finish(Database.BatchableContext BC){
   }
   
   
}