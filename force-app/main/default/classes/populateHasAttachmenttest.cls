@isTest
private class populateHasAttachmenttest {

    private static testMethod void testmethod1() {
        Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Account acc1 = new account(Name ='Test', Type='Customer',SAP_Company_Code__c = 'AAA');
        insert acc1;
        Id rtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('IT').getRecordTypeId();
        
        Opportunity opp = new Opportunity(accountId=acc1.id,recordtypeId=rtId,
        StageName='Identified',Name='TEst Opp',Type='Tender',Division__c='IT',ProductGroupTest__c='LCD_MONITOR',LeadSource='BTA Dealer',Closedate=system.today());
        insert opp;
        
        Attachment attach=new Attachment();     
        attach.Name='Test';
        Blob bodyBlob=Blob.valueOf('Testing Body of Attachment');
        attach.body=bodyBlob;
        attach.parentId=opp.id;
        insert attach;
        test.startTest();
            database.executeBatch(new populateHasAttachment());
        test.stopTest();

    }

}