global class populateproductrecordtype implements Database.Batchable<sObject>{
//One Time Batch to Update the existed products to B2B type 

   global Database.QueryLocator start(Database.BatchableContext BC){
       if(test.isRunningTest()){
           return Database.getQueryLocator([select id,name,RecordTypeid,RecordType.Name from product2  ]);
       }else{
           return Database.getQueryLocator([select id,name,RecordTypeid,RecordType.Name from product2 Where RecordTypeid=null ]);
       }
      
   }

   global void execute(Database.BatchableContext BC, List<product2> scope){
            RecordType r = [SELECT Id,Name FROM RecordType WHERE SobjectType = 'Product2' AND Name = 'B2B' limit 1]; 
            for(Product2 p:scope){ 
                   if(p.RecordType==null){ 
                    p.RecordTypeID=r.id; 
                   }
                
            } 
            update scope;
    }

   global void finish(Database.BatchableContext BC){
   }
   
   
}