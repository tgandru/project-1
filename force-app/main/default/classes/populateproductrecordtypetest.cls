@isTest
private class populateproductrecordtypetest {

	private static testMethod void test() {
            integer i;
         	List<Product2> prods=new List<Product2>();
         	
         	for(i=0;i<10;i++){
         	  Product2 p=new Product2();
         	  p.name='My Test Record Prod '+i;
         	  p.SAP_Material_ID__c='SS1234SDSA'+i;
         	  prods.add(p); 
         	}
         	insert prods;
         	test.startTest();
            database.executeBatch(new populateproductrecordtype());
            test.stopTest();
         	
         	
	}

}