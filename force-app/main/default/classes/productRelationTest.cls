@isTest
public class productRelationTest{
    
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';

    static testMethod void ProductandProductRelationTest(){
        
        Pricebook2 pb = new Pricebook2(Name = 'Standard Price Book 2016', Description = 'Price Book 2016 Products', IsActive = true);
        insert pb;
        Product2 prod = new Product2(Name = 'Test Product', 
            Family = 'Hardware',SAP_Material_ID__c='Test Material',Unit_Cost__c=100.0);
        insert prod;
        Product2 prod2 = new Product2(Name = 'Test PRoduct 2', 
            Family = 'Hardware',SAP_Material_ID__c='Test Material2',Unit_Cost__c=10.0);
        insert prod2;
        message_queue__c  testm = new message_queue__c ( last_successful_run__c=system.now(), Integration_Flow_Type__c = '007');
        insert testm;
        
        Product_Relationship__c pr = new Product_Relationship__c();
        pr.Parent_Product__c = prod.id;
        pr.ParentChildSKU__c = 'testparentchildsku';
        pr.message_queue__c = testm.id;
        pr.Child_Product__c = prod2.id;
        insert pr;
        Test.startTest();
        // Schedule the test job
        String jobId = System.schedule('ProductRelationSchedularTest',
                        CRON_EXP, 
                        new ProductRelationSchedular());
        Database.executeBatch( new ProductRelationBatch(),1);
        Test.stopTest();
    }
    
    static testMethod void ProductandProductRelationTest2(){
        
        Pricebook2 pb = new Pricebook2(Name = 'Standard Price Book 2016', Description = 'Price Book 2016 Products', IsActive = true);
        insert pb;
        Product2 prod = new Product2(Name = 'Test Product', 
            Family = 'Hardware',SAP_Material_ID__c='Test Material',Unit_Cost__c=100.0);
        insert prod;
        Product2 prod2 = new Product2(Name = 'Test PRoduct 2', 
            Family = 'Hardware',SAP_Material_ID__c='Test Material2',Unit_Cost__c=10.0);
        insert prod2;
        message_queue__c  testm = new message_queue__c ( last_successful_run__c=system.now()+24, Integration_Flow_Type__c = '007');
        insert testm;
        
        Product_Relationship__c pr = new Product_Relationship__c();
        pr.Parent_Product__c = prod.id;
        pr.ParentChildSKU__c = 'testparentchildsku';
        pr.message_queue__c = testm.id;
        pr.Child_Product__c = prod2.id;
        insert pr;
        Test.startTest();
        // Schedule the test job
        String jobId = System.schedule('ProductRelationSchedularTest',
                        CRON_EXP, 
                        new ProductRelationSchedular());
        Database.executeBatch( new ProductRelationBatch(),1);
        Test.stopTest();
    }
}