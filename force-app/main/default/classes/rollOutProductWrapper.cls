/***********************************************WRAPPER***********************************************/
public class rollOutProductWrapper{

        public Roll_Out_Product__c rop{get;set;}
        public List<Roll_Out_Schedule__c> rosLst{get;set;}
        public OpportunityLineItem oli{get;set;}
        public Integer oliQty{get;set;}
        public Decimal oliAmt{get;set;}
        public Decimal percentPlanQty {get;set;}
        public Decimal percentActualQty {get;set;}
        public Decimal percentPlanRev {get;set;}
        public Decimal percentActualRev {get;set;}
        public Decimal sumPlanQty{get;set;}
        public Decimal sumActualQty{get;set;}
        public Decimal sumPlanRev{get;set;}
        public Decimal sumActualRev{get;set;}
        public Decimal variancePlanQty{get;set;}
        public Decimal varianceActualQty{get;set;}
        public Decimal variancePlanRev{get;set;}
        public Decimal varianceActualRev{get;set;}
        //public List<scheduleWrapper> scheduleWrapperLst {get;set;}
        //public Integer numberOfMonths {get;set;}
        public Boolean doNotAllocate{get;set;}
        public String parentProd {get;set;}
        
        public Map<String,Decimal> combineROSWeeksPlanQty{get;set;}
        public Map<String,Decimal> combineROSWeeksActualQty{get;set;}
        public Map<String,Decimal> combineROSWeeksPlanRev{get;set;}
        public Map<String,Decimal> combineROSWeeksActualRev{get;set;}
        public Map<String, Boolean> canEdit{get;set;}
        public Map<String, Boolean> canEditActuals{get;set;}
        
        public rollOutProductWrapper(Roll_Out_Product__c ropVar,List<Roll_Out_Schedule__c> rosLstVar, OpportunityLineItem oliVar){

            rop=ropVar;
            rosLst=rosLstVar;
            doNotAllocate=ropVar.Do_Not_Allocate__c;

            oli=oliVar;
            oliQty=oliVar.Quantity.round(System.RoundingMode.DOWN).intValue();
            oliAmt=oliVar.TotalPrice;
            parentProd=oliVar.Parent_Product__c;

            percentPlanQty= rop.Quote_Quantity__c!=0 ? (rop.Plan_Quantity__c / rop.Quote_Quantity__c) : 0;
            percentPlanRev= (rop.Quote_Revenue__c!=0 && rop.Quote_Revenue__c!=null && rop.Plan_Revenue__c!=null) ? (rop.Plan_Revenue__c/rop.Quote_Revenue__c) : 0;
            percentActualQty= rop.Quote_Quantity__c!=0 ? (rop.Actual_Quantity__c / rop.Quote_Quantity__c) : 0; 
            percentActualRev= rop.Quote_Revenue__c!=0 ? (rop.Actual_Revenue__c/rop.Quote_Revenue__c) : 0;

            sumPlanQty=rop.Plan_Quantity__c;
            sumPlanRev=rop.Plan_Revenue__c;
            sumActualQty=rop.Actual_Quantity__c;
            sumActualRev=rop.Actual_Revenue__c;
            //sumPlanQty=0;
            //sumActualQty=0;

            variancePlanQty= (rop.Plan_Quantity__c - rop.Quote_Quantity__c);
            variancePlanRev= (rop.Plan_Revenue__c - rop.Quote_Revenue__c);
            varianceActualQty=(rop.Actual_Quantity__c - rop.Quote_Quantity__c); //no actual quantity
            varianceActualRev= (rop.Actual_Revenue__c - rop.Quote_Revenue__c);

            //combine roll over schedule records per month
            combineROSWeeksPlanQty=new Map <String,Decimal>();
            combineROSWeeksActualQty=new Map <String,Decimal>();
            combineROSWeeksPlanRev=new Map <String,Decimal>();
            combineROSWeeksActualRev=new Map <String,Decimal>();

            canEdit=new Map<String, Boolean>();
            canEditActuals=new Map<String, Boolean>();

            Date today=system.today();
            Integer nowMon=today.month();
            Integer nowYear=today.year();

            for(Roll_Out_Schedule__c ross:rosLstVar){

                

                //figure out which months are editable or not.
                if(canEdit.containsKey(ross.Rollout_Month__c+','+ross.Calendar_Year__c)){
                    Boolean previous=canEdit.get(ross.Rollout_Month__c+','+ross.Calendar_Year__c);
                    if(ross.Do_Not_Edit__c==false && previous==false){ //if do not edit is false, but the previous boolean is true, then mkae the month editable
                        canEdit.put(ross.Rollout_Month__c+','+ross.Calendar_Year__c,true);
                    }
                }else{
                    canEdit.put(ross.Rollout_Month__c+','+ross.Calendar_Year__c, !(ross.Do_Not_Edit__c)); //if do_not_Edit=true, then we put false aka you cannot edit it. & vice versa
                }

                //figure out which months can have acutals edited or not. 

                if(!canEditActuals.containsKey(ross.Rollout_Month__c+','+ross.Calendar_Year__c)){
                    if(ross.Plan_Date__c < today){
                        canEditActuals.put(ross.Rollout_Month__c+','+ross.Calendar_Year__c,true);
                    }else{
                        canEditActuals.put(ross.Rollout_Month__c+','+ross.Calendar_Year__c,false);
                    }
                }else{
                    Boolean previous=canEditActuals.get(ross.Rollout_Month__c+','+ross.Calendar_Year__c);
                    if(ross.Plan_Date__c < today && previous==false){
                         canEditActuals.put(ross.Rollout_Month__c+','+ross.Calendar_Year__c,true);
                    }
                }


                //sum weeks into months
                if(combineROSWeeksPlanQty.containsKey(ross.Rollout_Month__c+','+ross.Calendar_Year__c)){
                    if(combineROSWeeksPlanQty.get(ross.Rollout_Month__c+','+ross.Calendar_Year__c)!=null && ross.Plan_Quantity__c!=null){
                        Decimal before=combineROSWeeksPlanQty.get(ross.Rollout_Month__c+','+ross.Calendar_Year__c);
                        combineROSWeeksPlanQty.put(ross.Rollout_Month__c+','+ross.Calendar_Year__c,(before+ross.Plan_Quantity__c));
                    }
                }else{
                    if(ross.Plan_Quantity__c!=null){
                        combineROSWeeksPlanQty.put(ross.Rollout_Month__c+','+ross.Calendar_Year__c,ross.Plan_Quantity__c);
                    }else{
                        combineROSWeeksPlanQty.put(ross.Rollout_Month__c+','+ross.Calendar_Year__c,0);
                    }
                }
                
                if(combineROSWeeksActualQty.containsKey(ross.Rollout_Month__c+','+ross.Calendar_Year__c)){
                    if(combineROSWeeksActualQty.get(ross.Rollout_Month__c+','+ross.Calendar_Year__c)!=null && ross.Actual_Quantity__c!=null){
                        Decimal before=combineROSWeeksActualQty.get(ross.Rollout_Month__c+','+ross.Calendar_Year__c);
                        combineROSWeeksActualQty.put(ross.Rollout_Month__c+','+ross.Calendar_Year__c,(before+ross.Actual_Quantity__c));
                    }
                }else{
                    if(ross.Actual_Quantity__c!=null){
                        combineROSWeeksActualQty.put(ross.Rollout_Month__c+','+ross.Calendar_Year__c,ross.Actual_Quantity__c);
                    }else{
                        combineROSWeeksActualQty.put(ross.Rollout_Month__c+','+ross.Calendar_Year__c,0);
                    }
                    
                }

                if(combineROSWeeksPlanRev.containsKey(ross.Rollout_Month__c+','+ross.Calendar_Year__c)){
                    if( combineROSWeeksPlanRev.get(ross.Rollout_Month__c+','+ross.Calendar_Year__c)!=null && ross.Plan_Revenue__c!=null){
                        Decimal before=combineROSWeeksPlanRev.get(ross.Rollout_Month__c+','+ross.Calendar_Year__c);
                        combineROSWeeksPlanRev.put(ross.Rollout_Month__c+','+ross.Calendar_Year__c,(before+ross.Plan_Revenue__c));
                    }
                }else{
                    if(ross.Plan_Revenue__c!=null){
                        combineROSWeeksPlanRev.put(ross.Rollout_Month__c+','+ross.Calendar_Year__c,ross.Plan_Revenue__c);
                    }else{
                        combineROSWeeksPlanRev.put(ross.Rollout_Month__c+','+ross.Calendar_Year__c,0);
                    }
                    
                }

                if(combineROSWeeksActualRev.containsKey(ross.Rollout_Month__c+','+ross.Calendar_Year__c)){
                    if(combineROSWeeksActualRev.get(ross.Rollout_Month__c+','+ross.Calendar_Year__c)!=null && ross.Actual_Revenue__c!=null){
                        Decimal before=combineROSWeeksActualRev.get(ross.Rollout_Month__c+','+ross.Calendar_Year__c);
                        combineROSWeeksActualRev.put(ross.Rollout_Month__c+','+ross.Calendar_Year__c,(before+ross.Actual_Revenue__c));
                    }
                }else{
                    if(ross.Actual_Revenue__c!=null){
                        combineROSWeeksActualRev.put(ross.Rollout_Month__c+','+ross.Calendar_Year__c,ross.Actual_Revenue__c);
                    }else{
                        combineROSWeeksActualRev.put(ross.Rollout_Month__c+','+ross.Calendar_Year__c,0);
                    }
                    
                }

            }


        }

    }