public class selloutproducteditredirectController {
 Public Sellout_Products__c sp{get; set;}
 public string soid {set; get;}
    public selloutproducteditredirectController(ApexPages.StandardController controller) {
      if(controller.getRecord().Id !=null){
         sp=[select id,name,Sellout__c from Sellout_Products__c  where id=:controller.getRecord().Id limit 1];
         soid =sp.Sellout__c;
         }
    }
    
    public pagereference redirecteditpage(){
      if(soid!= null){
        return new pageReference('/apex/SellOutSKUEdits?id='+soid);
      }
      return null;
    }

}