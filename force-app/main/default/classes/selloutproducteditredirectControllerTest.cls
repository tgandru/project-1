@isTest
private class selloutproducteditredirectControllerTest {

	private static testMethod void testmethod1() {
          Test.startTest();
          Sellout__C so=new Sellout__C(Closing_Month_Year1__c='10/2018',Description__c='From Test Class',Subsidiary__c='SEA',Approval_Status__c='Draft',Integration_Status__c='Draft',Has_Attachement__c=true,Request_Approval__c=true);
         insert so;
         Date d=System.today();
         
          Sellout_Products__c sl0=new Sellout_Products__c(Sellout__c=so.id,Sellout_Date__c=d,Samsung_SKU__c='SM-TEST123',Quantity__c=100,Carrier__c='ATT',Carrier_SKU__c='Samsung Mobile',IL_CL__c='CL');
          insert sl0;
         
         Pagereference pf= Page.SelloutProductEditRedirect;
           pf.getParameters().put('id',sl0.id);
           test.setCurrentPage(pf);
        
        apexPages.standardcontroller sc = new apexPages.standardcontroller(sl0);
          selloutproducteditredirectController cls=new selloutproducteditredirectController(sc);
          
            cls.redirecteditpage();
         Test.StopTest(); 
	}

}