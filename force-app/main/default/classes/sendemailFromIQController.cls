public with sharing class sendemailFromIQController {
public String selectedTemplateId { public get; public set; }
Public string body{get; set;}
Public string to{get; set;}
Public string addto{get; set;}
Public string cc{get; set;}
Public string bcc{get; set;}
Public string sub{get; set;}
public string frm{get; set;}
public string frmName{get; set;}
Public Inquiry__c iq{get; set;}
    public List<SelectOption> templist {get; set;}
    
    public List<Folder> allEmailTemplateFolders;
    public String selectedEmailTemplateFolder {get;set;}
    public String selectedEmailTemplate {get; set;}
    public EmailTemplate chosenEmailTemplate {public get; private set;}
Public String tempstring {get; set;}
Public String tempIQid {get; set;}
public Document document {
    get {
      if (document == null)
        document = new Document();
        return document;
    }
    set;}
    public sendemailFromIQController() {
       frm=UserInfo.getUserEmail();
       frmName=UserInfo.getUserName();
       templist = new list<selectOption>();
       if(ApexPages.currentPage().getParameters().get('ID') != null && ApexPages.currentPage().getParameters().get('ID') != 'null'){
                                tempIQid=ApexPages.currentPage().getParameters().get('ID');
                               iq=[select id,Name,Prospect_Email__c,Campaign__c,Product_Solution_of_Interest__c,Owner.Email,Lead__c,Contact__c,Lead__r.ID,Contact__r.ID from Inquiry__c where id=:tempIQid ];
                               to=iq.Prospect_Email__c;
                               bcc=UserInfo.getUserEmail()+';';
                            }
        allEmailTemplateFolders = [select Id, Name from Folder where Type = 'Email' order by Name asc];
                          if(ApexPages.currentPage().getParameters().get('temID') != null){
                                                  selectedEmailTemplate= ApexPages.currentPage().getParameters().get('temID');
                                                  system.debug('temID-->'+selectedEmailTemplate);
                                                 EmailTemplate t=[select Id, Name, Subject, Body, FolderId,htmlvalue from EmailTemplate where id=:selectedEmailTemplate limit 1];
                                                     
                                                     
                                                 createbodyfromtemp(selectedEmailTemplate,tempIQid);  

                                                }
                            
    }
 
    public void createbodyfromtemp(id tid,id rid){
    
       String retvalues='';
       string cid=label.Temp_Contact;
        Contact c=[select id from Contact where id=:cid limit 1];
       Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
        msg.setTemplateId(tid);
        msg.setWhatId(rid);
        msg.setTargetObjectId(c.id);
        msg.setToAddresses(new List<String>{'randomemiloppscontsabc@gmail.com'});
       if(!Test.isRunningTest()){
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] {msg});
        }
        
       // Database.rollback(sp);
        sub=msg.getSubject();
        String s=msg.getHTMLBody();
        if(s !=null){
            body=msg.getHTMLBody(); 
        }else{
             body=msg.getPlainTextBody();
        }
       
         system.debug('Sub'+msg.getSubject());
         system.debug('Sub2'+msg.getHTMLBody());
                 
    }
    

    public List<SelectOption> getEmailTemplateFolderOpts() {
        List<SelectOption> opts = new List<SelectOption>();
        opts.add(new SelectOption('null', ' - Select - '));
        for ( Folder f : allEmailTemplateFolders )
            opts.add(new SelectOption(f.Id, f.Name));
        // add an option for templates in the Unfiled Public folder
        opts.add(new SelectOption(UserInfo.getOrganizationId(), 'Unfiled Public'));
         opts.add(new SelectOption(UserInfo.getUserId(), 'My Personal Email Templates'));
        return opts;
    }
    public void refreshEmailTemplateSection() {
        system.debug('Action function block');
        system.debug('selectedEmailTemplateFolder--'+selectedEmailTemplateFolder);
        templist.clear();
         templist.add(new SelectOption('null', ' - Select - '));
            if(selectedEmailTemplateFolder !=null ){
                
                for ( EmailTemplate et : [select Id, Name, Subject, Body, FolderId from EmailTemplate where FolderId=:selectedEmailTemplateFolder  order by Name asc  ] ) {
                
                    templist.add(new SelectOption(et.Id, et.Name));
                } 
            }
  
        
    }  
    
    Public void emailbody(){
        if(selectedEmailTemplate!=null){
            tempstring=selectedEmailTemplate;
        }
        
    }
     public PageReference reloadthepage() {
           system.debug('selectedEmailTemplate--'+selectedEmailTemplate);
           string pf='/apex/sendemailFromIQ?ID='+tempIQid;
           pf=pf+'&temID=';
           pf=pf+tempstring;
         PageReference pageRef = new PageReference(pf);
                 pageRef.setRedirect(true);
                 system.debug('redirect page-->'+selectedEmailTemplate);
                return pageRef;
  }
  public PageReference gobackthepage() {
             PageReference pageRef = new PageReference('/apex/IQDetail?id='+tempIQid);
                 pageRef.setRedirect(true);
               
                return pageRef;
        
  }
  
  Public PageReference sendemail(){
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       List<String> toadd = to.split(';');
       list<String> to = new list<string>();
       for(String s:toadd){
         if(s !=null && s!=''){
             to.add(s);
         }  
       }
       if(cc!=null && cc !=''){
            list<String> ccadd = new list<string>();
           List<String> cca=cc.split(';');
            for(String s:cca){
                 if(s !=null && s!=''){
                     ccadd.add(s);
                 }  
           }
            mail.setCCAddresses(ccadd);
           
       }
       if(bcc!=null && bcc !=''){
            list<String> bccadd = new list<string>();
           List<String> bcca=bcc.split(';');
            for(String s:bcca){
                 if(s !=null && s!=''){
                     bccadd.add(s);
                 }  
           }
            mail.setBCCAddresses(bccadd);
           
       }
      
          mail.setToAddresses(to);
          mail.setSubject(sub);
          mail.setHtmlBody(body);
          mail.setSaveAsActivity(true);
          mail.setWhatId(iq.id);
          Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            if(document.body!= null){
                efa.setFileName(document.name);
                efa.setBody(document.body);
            }
            if(document.body!= null){
                    mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
                }
         
      
          Messaging.sendEmailResult[] sendEmailResults = Messaging.sendEmail(new Messaging.Email[] { mail }, false);
              if(sendEmailResults[0].isSuccess()){
              /*  EmailMessage newEmail = new EmailMessage();
                    newEmail.ToAddress=to[0];
                     newEmail.FromAddress=frm;
                    newEmail.Subject=sub;
                    newEmail.HtmlBody=body;
                    newEmail.RelatedTo.ID=iq.id;
                    newEmail.MessageDate = system.now();
                    newEmail.Status = '3';
                   */
                    //insert newEmail;
             }
         
        PageReference pageRef = new PageReference('/apex/IQDetail?id='+tempIQid);
                 pageRef.setRedirect(true);
               
                return pageRef;
         
         
   
      
  }

}