@isTest(SeeAllData=true)
private class sendemailFromIQControllerTest {

	private static testMethod void testmethod1() {
	     Test.startTest();
	    Account newAccount= TestDataUtility.createAccount(TestDataUtility.generateRandomString(5));
        newAccount.name ='Test Account';
        //acc.recordTypeId = endUserRT;
        newAccount.SAP_Company_Code__c='99999888';
        insert newAccount;
        Zipcode_Lookup__c z=new Zipcode_Lookup__c(City_Name__c='Ridgefield Park', Country_Code__c='US', State_Code__c='NJ', State_Name__c='New Jersey', Name='07660');
        insert z;
        
         string   CampaignTypeId = [Select Id From RecordType Where SobjectType = 'Campaign' and Name = 'Sales'  limit 1].Id;
	    string   LeadTypeId = [Select Id From RecordType Where SobjectType = 'Lead' and Name = 'End User'  limit 1].Id;
       Campaign cmp= new Campaign(Name='Test Campaign for test class ',	IsActive=true,type='Email',StartDate=system.Today(),recordtypeid=CampaignTypeId);
       insert cmp;
        lead le1=new Lead(
                        Company = 'Test Account', LastName= 'Test Lead',
                        LeadSource = 'Web', Email='test@testabc.com',Division__c='IT',ProductGroup__c='A3 COPIER',
                        Status = 'Account Only',street='123 sample st',city='Ridgefield Park',state='NJ', Country='US',PostalCode='07660',recordtypeid=LeadTypeId);
        insert le1;
         Inquiry__c iq=new Inquiry__c(Lead__c = le1.id,Product_Solution_of_Interest__c='Monitors',	 campaign__c = cmp.id);
         insert iq;
         PageReference VFPage = Page.sendemailFromIQ;
         Test.setCurrentPageReference(VFPage); 
         
         ApexPages.currentPage().getParameters().put('ID', iq.Id);
         
         sendemailFromIQController con=new sendemailFromIQController();
       
         con.body='Test Email Body';
         con.sub='Test Class ';
         con.cc='test@sdfc.com';
        
         con.getEmailTemplateFolderOpts();
         con.refreshEmailTemplateSection();
         con.sendemail();
         con.gobackthepage();
         con.reloadthepage();
         Test.stopTest(); 
	    

	}
    
    
    	private static testMethod void testmethod2() {
	     Test.startTest();
	    Account newAccount= TestDataUtility.createAccount(TestDataUtility.generateRandomString(5));
        newAccount.name ='Test Account';
        //acc.recordTypeId = endUserRT;
        newAccount.SAP_Company_Code__c='99999888';
        insert newAccount;
        Zipcode_Lookup__c z=new Zipcode_Lookup__c(City_Name__c='Ridgefield Park', Country_Code__c='US', State_Code__c='NJ', State_Name__c='New Jersey', Name='07660');
        insert z;
        
         string   CampaignTypeId = [Select Id From RecordType Where SobjectType = 'Campaign' and Name = 'Sales'  limit 1].Id;
	    string   LeadTypeId = [Select Id From RecordType Where SobjectType = 'Lead' and Name = 'End User'  limit 1].Id;
       Campaign cmp= new Campaign(Name='Test Campaign for test class ',	IsActive=true,type='Email',StartDate=system.Today(),recordtypeid=CampaignTypeId);
       insert cmp;
        lead le1=new Lead(
                        Company = 'Test Account', LastName= 'Test Lead',
                        LeadSource = 'Web', Email='test@testabc.com',Division__c='IT',ProductGroup__c='A3 COPIER',
                        Status = 'Account Only',street='123 sample st',city='Ridgefield Park',state='NJ', Country='US',PostalCode='07660',recordtypeid=LeadTypeId);
        insert le1;
         Inquiry__c iq=new Inquiry__c(Lead__c = le1.id,Product_Solution_of_Interest__c='Monitors',	 campaign__c = cmp.id);
         insert iq;
         PageReference VFPage = Page.sendemailFromIQ;
         Test.setCurrentPageReference(VFPage); 
         
         ApexPages.currentPage().getParameters().put('ID', iq.Id);
         
         sendemailFromIQController con=new sendemailFromIQController();
         EmailTemplate t=[select Id, Name, Subject, Body, FolderId,htmlvalue from EmailTemplate  limit 1];
         con.body='Test Email Body';
         con.sub='Test Class ';
         con.cc='test@sdfc.com';
         con.createbodyfromtemp(t.id,iq.id);
         Test.stopTest(); 
	    

	}
}