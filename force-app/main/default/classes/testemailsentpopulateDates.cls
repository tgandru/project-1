global class testemailsentpopulateDates implements Database.Batchable<sObject>{


   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator([SELECT Id, LastModifiedDate, Status, TaskSubtype, CreatedDate,WhatId, WhoId FROM Task WHERE TaskSubtype = 'Email' ]);
   }

   global void execute(Database.BatchableContext BC, List<Task> scope){
       set<string> quoteIds = new set<string>();
       for(Task p : scope){
           string s = p.WhatId;
           if(s!=null){
           if(s.startswith('0Q0')){
               quoteIds.add(s);
               system.debug('******'+quoteIds);
           }
           } 
       }
        system.debug('1213');
       if(quoteIds.size() > 0){
           system.debug('123');
           map<id,Quote> qt = new map<id,Quote>([select id,Quote_Email_Sent_Date__c from Quote where id in:quoteIds  and Quote_Email_Sent_Date__c = null ]);//in:quoteIds
           system.debug('*********'+qt);
           for(Task p : scope){
               string s = p.WhatId;
               if(s!=null){
               if(s.startswith('0Q0')){
                   if(qt.get(s) != null){
                       qt.get(s).Quote_Email_Sent_Date__c = p.CreatedDate;
                      
                   }
               }
           }
           }
           if(qt.size() > 0){
               update qt.values();
           }
       }
    }

   global void finish(Database.BatchableContext BC){
   }
   
   
}