@isTest
private class unsubscribeTest {

	private static testMethod void testmethod1() {
	    Test.startTest();
             Channelinsight.CI_TEST_TestUtils.createBaseInstallData();
        Zipcode_Lookup__c zp = new Zipcode_Lookup__c(City_Name__c ='Edison', Country_Code__c='US', State_Code__c='NJ',State_Name__c='New Jersey',Name ='08820');
        insert zp;
          Id endCustomer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('End Customer').getRecordTypeId();
        Account acc1 = new account(recordtypeId=endCustomer,Name ='testdelete', Type='Customer',BillingStreet ='673 wood Ave',BillingCity='Edison',BillingState='NJ',BillingCountry='US',BillingPostalCode='08820');
        insert acc1;
        contact con = new contact( LastName='mytestdelete',Accountid=acc1.id,Email='mytestdelete@abc.com',Send_Articles__c=true);
        insert con;
        
          Messaging.InboundEmail email = new Messaging.InboundEmail() ;
          Messaging.InboundEnvelope env   = new Messaging.InboundEnvelope();
           email.subject = 'unsubscribe';
           env.fromAddress = 'mytestdelete@abc.com';
            unsubscribe unsubscribeObj = new unsubscribe();
             unsubscribeObj.handleInboundEmail(email, env );
             
        Test.StopTest();     

	}

}