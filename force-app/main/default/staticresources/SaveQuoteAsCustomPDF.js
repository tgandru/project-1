function SaveQuoteAsCustomPDF(quoteId) {
    try {
        var res = sforce.apex.execute("QuoteCustomPdfGenerator","createQuotePdf",{Id : quoteId});
        if(res=='SUCCESS') {
            window.location.reload();
        }
        else {
            alert('Error in attaching file ----'+ res);
        }
    }
    catch(er) {
        alert(er);
    }
}