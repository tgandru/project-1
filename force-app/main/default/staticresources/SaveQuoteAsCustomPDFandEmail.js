function SaveQuoteAsCustomPDFandEmail(quoteId) {
    try {
        var res = sforce.apex.execute("QuoteCustomPdfGenerator","saveQuoteAsPDFandEmail",{QuotId : quoteId});
        if(res!=null&&res!='') {
			window.location=res;
        }
        else {
            alert('Error in attaching file ----'+ res);
        }
    }
    catch(er) {
        alert(er);
    }
}