trigger AccountTrigger on Account (before insert, after insert, before update,after Update ){
    if(!DataMigrationSwitch__c.getInstance(UserInfo.getProfileId()).DisableTrigger__c) {  
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            //addIntAttributes.addGUID(trigger.new, trigger.oldMap, false);
            //addIntAttributes.createMessageQueue(trigger.new, trigger.oldMap, false);
            AccountTriggerHandler accountTriggerHandler = new AccountTriggerHandler(Trigger.New);
            accountTriggerHandler.processNewAccounts();
            UpdatePicklistValuesHandler cont=new UpdatePicklistValuesHandler();
            cont.getInsertAccts(Trigger.new);
            IntegrationUpdateAccountTriggerHelper cont2=new IntegrationUpdateAccountTriggerHelper();
            cont2.onBeforeInsert(Trigger.new);
            AccountTriggerHandler.updateownerandcoowner(Trigger.new);
            //AccountTriggerHandler.checkowner2role(Trigger.new);
            //AccountTriggerHandler.insertkamkeyAccount(Trigger.new);
        }
        if(trigger.isUpdate){
            //addIntAttributes.addGUID(trigger.new, trigger.oldMap, true);
            //addIntAttributes.createMessageQueue(trigger.new,  trigger.oldMap, true);
            AccountTriggerHandler accountTriggerHandler = new AccountTriggerHandler(Trigger.New, Trigger.oldMap);
            accountTriggerHandler.syncAccountRecTypeWithOpptyType(Trigger.newMap, Trigger.oldMap);//Added by Thiru
            accountTriggerHandler.updateExistingAccounts();
            UpdatePicklistValuesHandler cont=new UpdatePicklistValuesHandler();
            cont.getUpdatedAccts(Trigger.new, Trigger.oldMap);
            IntegrationUpdateAccountTriggerHelper cont2=new IntegrationUpdateAccountTriggerHelper();
            cont2.onUpdate(Trigger.new, Trigger.oldMap);
            //AccountTriggerHandler.checkowner2role(Trigger.new);
            // AccountTriggerHandler.updatekamkeyAccount(Trigger.newMap,trigger.oldmap);
        }
        // To validate Billing Address on Before Insert, Before update
        AccountTriggerHandler.validateAddress(trigger.new);
    }
    if(Trigger.isAfter){
        if(Trigger.isInsert){
            IntegrationUpdateAccountTriggerHelper cont2=new IntegrationUpdateAccountTriggerHelper();
            cont2.OnInsert(Trigger.new);
             AccountTriggerHandler.shareaccountowner2(trigger.new );
             //  AccountTriggerHandler.AutoSubmmitforkeyaccountinsert(trigger.new );
        }
        
        if(trigger.isUpdate){
           // AccountTriggerHandler.shareaccountowner2(trigger.old);//To Share Account With Owner2 
          
            //AccountTriggerHandler.insertkamkeyAccountdelete(trigger.newMap, trigger.oldMap); 
           // AccountTriggerHandler.AutoSubmmitforkeyaccount(trigger.newMap, trigger.oldMap );
           /* set<id> acid = AccountTriggerHandler.updatemqstatusrecors(trigger.newMap);
                    if(acid.size()>0){
                        system.debug('Update the MQ Status'+acid);
                     AccountTriggerHandler.updatemqstatus(acid);
                  }
            */
            
            List<Account> accList = new List<Account>(); 
            
            for(Account acc : Trigger.new){
                Account olda = Trigger.oldMap.get(acc.Id); 
                if(acc.BP_Number__c != NULL){
                    accList.add(acc);
                }
            }
            if(!Test.isRunningTest() && accList.size() == 1){
                system.debug('Start Bp Update');
                SVC_GCICBpUpdation.accValidation(accList.get(0));
            }    
        }
    }
    
    
    }
    
    if(Trigger.isAfter&&trigger.isUpdate){
          AccountTriggerHandler.updateshareaccounts(trigger.newMap, trigger.oldMap);
    }
    
    
    
}