trigger AssetTeamTrigger on Asset_Team__c (after insert, after delete) {
	if(Trigger.isAfter && Trigger.isDelete) {
        AssetTeamTriggerHandler.AfterDelete(Trigger.Old);
    }

    if(Trigger.isAfter && Trigger.isInsert) {
        AssetTeamTriggerHandler.AfterInsert(Trigger.New);
    }


}