trigger AssetTrigger on Asset (before delete,before insert,before Update) {
	if(Trigger.isBefore && Trigger.isDelete) {
        AssetTriggerHandler.blockDelete(Trigger.OldMap);
        AssetTriggerHandler.BeforeDelete(Trigger.Old);
    }
    
    if(Trigger.isBefore && (Trigger.isInsert || Trigger.IsUpdate)){
        AssetTriggerHandler.CheckInStallationPartner(Trigger.new);
    }
}