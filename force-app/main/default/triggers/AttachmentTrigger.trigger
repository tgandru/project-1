trigger AttachmentTrigger on Attachment (after insert,after delete) {
//public static String prfName=[SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId()].Name;// Added by Vijay 5/29
    if(trigger.isAfter){
        map<id,Opportunity> oppToUpdate = new map<id,Opportunity>();
         map<id,Sellout__c> SoToUpdate = new map<id,Sellout__c>();
        if(trigger.isInsert || trigger.isUpdate ){
            for(Attachment att : trigger.new){
                string parentID = att.parentId;
                if(parentID.startsWith('006')){
                    oppToUpdate.put(parentId,new Opportunity(id=parentId, Has_Attachment__c = true));
                }
                 if(parentID.startsWith('a34')){
                    SoToUpdate.put(parentId,new Sellout__c(id=parentId, Has_Attachement__c = true));
                }
                    
            }
        }
        else if(trigger.isDelete){
          String  prfName=[SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId()].Name;// Added by Vijay 5/29
            for(Attachment att : trigger.Old){
                string parentID = att.parentId;
                if(parentID.startsWith('006') && prfName!='System Administrator' ){
                    att.adderror('You are Not Allowed to Delete the Attachment. Please Contact System Admin');//Added By Vijay on 5/29 to block the Attachment deletion - Request from Wlater
                    oppToUpdate.put(parentId,new Opportunity(id=parentId, Has_Attachment__c = false ));
                }else if(parentID.startsWith('006') && prfName=='System Administrator'){
                     oppToUpdate.put(parentId,new Opportunity(id=parentId, Has_Attachment__c = false ));
                }else if(parentID.startsWith('a34') && att.Description !='KNOX Approval'){
                     SoToUpdate.put(parentId,new Sellout__c(id=parentId, Has_Attachement__c = false));
                }
                    
            }
        }
        
        if(oppToUpdate.size() > 0){
            update oppToUpdate.values();
        }
        
         if(SoToUpdate.size() > 0){
            update SoToUpdate.values();
        }
    }
}