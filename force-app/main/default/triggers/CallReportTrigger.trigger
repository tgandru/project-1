trigger CallReportTrigger on Call_Report__c (After Insert, After Update) {
	if(!DataMigrationSwitch__c.getInstance(UserInfo.getProfileId()).DisableTrigger__c){
    	if(trigger.isInsert){
    
    	    List<Call_Report__c> CBDCallReports = new List<Call_Report__c>();
    	    List<Call_Report__c> B2BCallReports = new List<Call_Report__c>();
    	    for(Call_Report__c cr : trigger.new){
    	        if(cr.division__c=='CBD'){
    	            CBDCallReports.add(cr);
    	        }else if(cr.division__c=='B2B'){
    	            B2BCallReports.add(cr);
    	        }
    	    }
    	    if(CBDCallReports.size()>0) CallReportTriggerHandler.afterInsert(CBDCallReports);
    	    if(B2BCallReports.size()>0) CallReportTriggerHandler.CreateAccountTask(B2BCallReports);
    	    
    	}else{
    	    Map<id,Call_Report__c> CBDCallReports = new Map<id,Call_Report__c>();
    	    Map<id,Call_Report__c> B2BCallReports = new Map<id,Call_Report__c>();
    	    Map<id,Call_Report__c> CBDCallReportsOld = new Map<id,Call_Report__c>();
    	    Map<id,Call_Report__c> B2BCallReportsOld = new Map<id,Call_Report__c>();
    	    for(Call_Report__c cr : trigger.new){
    	        if(cr.division__c=='CBD'){
    	            CBDCallReports.put(cr.id,cr);
    	        }else if(cr.division__c=='B2B'){
    	            B2BCallReports.put(cr.id,cr);
    	        }
    	    }
    	    for(Call_Report__c cr : trigger.old){
    	        if(cr.division__c=='CBD'){
    	            CBDCallReportsOld.put(cr.id,cr);
    	        }else if(cr.division__c=='B2B'){
    	            B2BCallReportsOld.put(cr.id,cr);
    	        }
    	    }
    	    if(CBDCallReports.size()>0) CallReportTriggerHandler.NotifyCallReportSubmission(CBDCallReports, CBDCallReportsOld);
    	    if(B2BCallReports.size()>0) CallReportTriggerHandler.NotifyB2BCallReportSubmission(B2BCallReports, B2BCallReportsOld);
    	    //CallReportTriggerHandler.NotifyCallReportSubmission(trigger.newmap, trigger.oldmap);
    	}
	}
}