trigger CampaignMemberTrg on CampaignMember (after insert , after  update) {
    
    if(!DataMigrationSwitch__c.getInstance(UserInfo.getProfileId()).DisableTrigger__c){
       if(trigger.isAfter && (trigger.isInsert || trigger.isUpdate) ){
        CampaignMemberTrgHandler.updatedLeads(trigger.new, trigger.oldMap, trigger.isUpdate);
    } 
    }
    
}