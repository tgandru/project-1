trigger CaseTrigger on Case (Before Update,Before Insert,after insert, after Update, after delete, after undelete)
{

    if(caseTriggerHandler.caseRecordTypeMap.isEmpty())
    {
        Schema.DescribeSObjectResult sobjectResult = Case.SObjectType.getDescribe();
        List<Schema.RecordTypeInfo> rtMap = sobjectResult.getRecordTypeInfos();

        for(Schema.RecordTypeInfo r: rtMap)
        {
            caseTriggerHandler.caseRecordTypeMap.put(r.getName(), r.getRecordTypeId());
        }
    }

    if(trigger.isBefore)
    {
        if(trigger.isInsert)
        {
           caseTriggerHandler.validateModeCode(trigger.new, new Map<Id, Case>());
            caseTriggerHandler.setCaseEntitlement(trigger.new);
            caseTriggerHandler.checkBusinessHour(trigger.new);
            caseTriggerHandler.checkCaseDuplication(trigger.new, new Map<Id, Case>());
            caseTriggerHandler.checkDeviceValid(trigger.new, new Map<Id, Case>());
            caseTriggerHandler.calculateEntitlementCap(trigger.new, null);
            caseTriggerHandler.validateaddress(trigger.New); 
        }
        else if(trigger.isUpdate)
        {
            caseTriggerHandler.validateModeCode(trigger.new, trigger.oldMap);
            caseTriggerHandler.checkCaseDuplication(trigger.new, trigger.oldMap);
            caseTriggerHandler.checkCaseValidation(trigger.new, trigger.oldMap);
            caseTriggerHandler.checkDeviceValid(trigger.new, trigger.oldMap);
            caseTriggerHandler.calculateEntitlementCap(trigger.new, trigger.oldMap);
            caseTriggerHandler.checkResolvedStatus(trigger.new, trigger.oldMap);
            caseTriggerHandler.setCaseStatus(trigger.new, trigger.oldMap);
            caseTriggerHandler.validateaddress(trigger.New);
            caseTriggerHandler.updateExchangedDevice(trigger.new, trigger.oldMap);
            caseTriggerHandler.AttachContractNumber(trigger.New);
            Set<id> caseIds = new Set<id>();
            for(Case c : trigger.new){
                if((c.accountid!=trigger.oldMap.get(c.id).accountid) || (c.accountid!=null && c.EntitlementId==null)){
                    caseIds.add(c.id);
                }
            }
            if(caseIds.size()>0){
                caseTriggerHandler.setCaseEntitlementOnUpdate(caseIds,trigger.new);
            }
        }
    }
    else if(trigger.isAfter)
    {
        if(trigger.isInsert)
        {
            caseTriggerHandler.createCaseShare(trigger.new, new Map<Id, Case>());
            caseTriggerHandler.bpCreationWTYTermCheck(trigger.new, new Map<Id, Case>());
            //caseTriggerHandler.sendContactdataAndCompleteMilestone(trigger.new, new Map<Id, Case>());
        }
        else if(trigger.isUpdate)
        {
            caseTriggerHandler.updateChildCaseShippingAddress(trigger.new, trigger.oldMap);
            caseTriggerHandler.createCaseShare(trigger.new, trigger.oldMap);
            caseTriggerHandler.bpCreationWTYTermCheck(trigger.new, trigger.oldMap);
            caseTriggerHandler.createCaseCommentforSpam(trigger.new, trigger.oldMap);
          
            caseTriggerHandler.parentStatusUpdate(trigger.new, trigger.oldMap);
            caseTriggerHandler.MarkContractAsClosed(trigger.new,trigger.oldMap);
            caseTriggerHandler.sendContactdataAndCompleteMilestone(trigger.new, new Map<Id, Case>());
           
        }
        else if(trigger.isDelete)
        {
            caseTriggerHandler.calculateEntitlementCap(trigger.old, trigger.OldMap);

        }
        else if(trigger.isUndelete)
        {
            caseTriggerHandler.calculateEntitlementCap(trigger.new, trigger.newMap);
        }
    }
}