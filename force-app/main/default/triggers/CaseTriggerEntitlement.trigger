/**
 * Created by ryan on 7/31/2017.
 */

trigger CaseTriggerEntitlement on Case (before insert, before update, after delete, after undelete) {

    if(trigger.isInsert && trigger.isBefore){
        //caseTriggerHandler.calculateEntitlementCap(trigger.new, null);
        caseTriggerHandler.checkSum(trigger.new, null);
    }

    if(trigger.isUpdate && trigger.isBefore){
       // caseTriggerHandler.calculateEntitlementCap(trigger.new, trigger.oldMap);
        caseTriggerHandler.checkSum(trigger.new, trigger.oldMap);
    }

    if(trigger.isDelete && trigger.isAfter){
        //caseTriggerHandler.calculateEntitlementCap(trigger.old, trigger.OldMap);
    }
    if(trigger.isUndelete && trigger.isAfter){
       // caseTriggerHandler.calculateEntitlementCap(trigger.new, trigger.newMap);
    }

}