trigger ContentDocumentLinkTrigger on ContentDocumentLink (before insert, before delete) {
    if(Trigger.isBefore && Trigger.isInsert) {
        System.debug('ContentDocumentLinkTrigger Trigger.isBefore && Trigger.isInsert is called');
        ContentDocumentLinkTriggerHandler.insertBeforeContentDocumentLink(Trigger.New);
    }
    if(Trigger.isBefore && Trigger.isDelete) {
        System.debug('ContentDocumentLinkTrigger Trigger.isBefore && Trigger.isDelete is called');
        ContentDocumentLinkTriggerHandler.deleteBeforeContentDocumentLink(Trigger.OldMap);
    }
}