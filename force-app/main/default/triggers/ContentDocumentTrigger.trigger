trigger ContentDocumentTrigger on ContentDocument (before update, before delete) {
    if(Trigger.isBefore && Trigger.isUpdate) {
        System.debug('ContentDocumentTrigger Trigger.isBefore && Trigger.isUpdate is Called');
        ContentDocumentTriggerHandler.updateBeforeContentDocument(Trigger.newMap);
    }
	if(Trigger.isBefore && Trigger.isDelete) {
        System.debug('This ContentDocumentTrigger isBefore isDelete is called');
        ContentDocumentTriggerHandler.deleteBeforeContentDocument(Trigger.OldMap);
    }
}