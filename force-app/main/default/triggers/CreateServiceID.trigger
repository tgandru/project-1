trigger CreateServiceID on entitlement (before insert, after update ) {

    if(trigger.isBefore && trigger.isInsert){
        Set<ID> accountIDs = new Set<ID>();
        for(Entitlement e : trigger.New){
            accountIDs.add(e.accountid);
        }
        Decimal maxID = 100000;
        Decimal maxID2= 100000;
        aggregateResult results = [select max(Service_ID_Numeric__c) m from account where Service_ID_Numeric__c!=null ];
        
        if (results.get('m') != null){

            maxID = (Decimal)results.get('m');
            maxID2 = (Decimal)results.get('m');
            if (maxID < 100000){
                maxID = 100000;
                maxID2= 100000;
            }
        }

        List<account> accounts = [select ID,Service_ID__c from account where ID in: accountIDs];
        
        Map<ID,String> accServiceIDs = new Map<ID, String>();

        for (account a: accounts){
            if (a.Service_ID__c == null){
                maxID += 11;
                a.Service_ID__c = String.valueOf(maxID);
                accServiceIDs.put(a.id,a.Service_ID__c);
            }else{
                accServiceIDs.put(a.id,a.Service_ID__c);
            }
        }
    update accounts;
    
    /*for(Entitlement e : trigger.New){
        if (e.SC_Service_Type__c == 'Trial'){
            e.IS_Trial__c = true;
            e.endDate = null;
        }
    }*/
    }
    if(trigger.isAfter && trigger.isUpdate){
        if(!Test.isRunningTest()) EntitlementTriggerHandler.capLimitSendEmail(trigger.new, trigger.oldMap);
    }
}