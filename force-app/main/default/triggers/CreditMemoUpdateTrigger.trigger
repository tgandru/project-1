trigger CreditMemoUpdateTrigger on Credit_Memo__c (after update){
    if(!DataMigrationSwitch__c.getInstance(UserInfo.getProfileId()).DisableTrigger__c) { 
	System.debug('EV: Triggered Credit Memo Update');
	CreditMemoUpdateTriggerFacade facade = new CreditMemoUpdateTriggerFacade(Trigger.new, Trigger.oldMap);
	facade.updateOpportunityProductClaimQuantity();
    }
}