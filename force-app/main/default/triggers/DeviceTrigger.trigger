trigger DeviceTrigger on Device__c (before insert, before update, before delete, after insert,after update) {


    if (Trigger.isBefore) 
    {
        if(trigger.isInsert || trigger.isUpdate)
        {
            if(trigger.oldMap != null)
            {
                DeviceTriggerHandler.validateModeCode(trigger.new, trigger.oldMap);
                DeviceTriggerHandler.calculateTotalNumberofDevice(trigger.new, trigger.oldMap, trigger.isInsert, trigger.isUpdate, trigger.isDelete);
                DeviceTriggerHandler.checkDevice(trigger.new, trigger.oldMap);
            }
            else
            {
                DeviceTriggerHandler.validateModeCode(trigger.new, new Map<Id, Device__c>());
                DeviceTriggerHandler.calculateTotalNumberofDevice(trigger.new, new Map<Id, Device__c>(), trigger.isInsert, trigger.isUpdate, trigger.isDelete);
                DeviceTriggerHandler.checkDevice(trigger.new, new Map<Id, Device__c>());
            }
        }

        if(trigger.isDelete)
        {
            DeviceTriggerHandler.calculateTotalNumberofDevice(trigger.new, trigger.oldMap, trigger.isInsert, trigger.isUpdate, trigger.isDelete);
        }
    }
}