trigger EmailMessageTrigger on EmailMessage (after insert) {
	if (Trigger.isInsert && Trigger.isAfter){
		EmailMessageTriggerHandler.afterInsert(Trigger.new);
	}
}