trigger EntitlementContactAccountCheck on EntitlementContact (before insert) {
	List<ID> entitlementIDs  = new List<ID>();
	List<ID> contactIDs  = new List<ID>();

	for (EntitlementContact enc:trigger.new){
		entitlementIDs.add(enc.EntitlementID);
		contactIDs.add(enc.contactId);
	}
	
	Map<ID, Entitlement> entitlementAccounts = new Map<ID, Entitlement>([SELECT Id, account.id FROM entitlement where id in: entitlementIDs]);
	Map<ID, Contact> contactAccounts = new Map<ID, Contact>([SELECT Id, account.id,Named_Caller__c FROM contact where id in: contactIDs]);

	for (EntitlementContact enc:trigger.new){
		if ( (entitlementAccounts.get(enc.EntitlementID)).account.id != (contactAccounts.get(enc.contactID)).account.id ){
			enc.addError('Contact account msut match entitlement account.');
		}else if( (contactAccounts.get(enc.contactID)).Named_Caller__c != TRUE){

			enc.addError('Contact msut be a named caller.');
		}
 	
	}
}