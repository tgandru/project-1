trigger EventTrigger on Event (after insert,after update,before insert,before update) {
  if(!DataMigrationSwitch__c.getInstance(UserInfo.getProfileId()).DisableTrigger__c) { 
     if(trigger.isBefore && (trigger.isInsert || trigger.isUpdate)){
	        EventTriggerHandler.CopyTypeFromActivityType(trigger.New,trigger.oldMap);// Added By Vijay on 10/25/2018 - To Make In Sync of Type and Activity Type fields
	    }
    
	If (Trigger.isAfter){
		EventTriggerHandler.svc_CompleteMileStones(Trigger.New);
		if(Trigger.isInsert) {
			Set<Id> ids = new Set<Id>();
			for(Event evt : Trigger.new){
				ids.add(evt.Id);
			}
			SVC_GCICFeedItem.insertFeedItem(ids, 'Event');
		}
		String currentuserprfid=UserInfo.getProfileId();
	         String integrationids=Label.Integration_ProfileIDs;
		
		if(EventTriggerHandler.isFirstTime && (!integrationids.contains(currentuserprfid))){
	           EventTriggerHandler.isFirstTime = false;
	           EventTriggerHandler.UpdateOpportunityUpdateDate(Trigger.New); 
	        }
		
	}
  }
}