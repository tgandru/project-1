/**
 * Created by ms on 2017-08-07.
 *
 * author : JeongHo.Lee, I2MAX
 */
trigger FeedCommentTrigger on FeedComment (after insert) {
	if(Trigger.isAfter && Trigger.isInsert) {
		Set<Id> ids = new Set<Id>();
		for(FeedComment fc : Trigger.new){
			ids.add(fc.Id);
		}
		SVC_FeedCommentTriggerHandler.insertFeedComment(ids);
	}
}