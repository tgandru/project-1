trigger FeedItemTrigger on FeedItem (before insert, before update, before delete, after insert, after update, after delete, after undelete) {

    if(Trigger.isAfter && Trigger.isInsert) {
        FeedItemTriggerHandler.afterInsert(Trigger.New);
        Set<Id> ids = new Set<Id>();
		for(FeedItem fi : Trigger.new){
		    string s=string.valueOf(fi.parentid);
		     if(s !=null && s !=''){
                if (s.startsWith('500') ){ 
                   system.debug('Parent Record ID'+s);
               	   ids.add(fi.Id);
                }  
             }
		
		}
		if(ids.size()>0){
		   	SVC_GCICFeedItem.insertFeedItem(ids, 'FeedItem'); 
		}
	
    }

    if(Trigger.isAfter && Trigger.isUpdate) {
        FeedItemTriggerHandler.afterUpdate(Trigger.New,trigger.oldMap);
    }
}