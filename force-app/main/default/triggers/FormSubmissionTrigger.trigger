trigger FormSubmissionTrigger on Form_Submission__c (after Insert, after update, after delete) {

    if(trigger.IsAfter && (trigger.isInsert || trigger.isUpdate)){
       FormSubmissionTriggerHandler.afterInsertupdate(trigger.new); 
    }
    
    if(trigger.IsAfter && trigger.isDelete){
       FormSubmissionTriggerHandler.afterDelete(trigger.old); 
    }

}