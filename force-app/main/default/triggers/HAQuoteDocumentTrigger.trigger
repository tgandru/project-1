trigger HAQuoteDocumentTrigger on SBQQ__QuoteDocument__c (After insert,After Update) {
    if(Trigger.isAfter){
        if(Trigger.isInsert){
            HAQuoteDocumentTriggerHandler QuoteDocumentAfter = new HAQuoteDocumentTriggerHandler();
            QuoteDocumentAfter.ProcessDocument(Trigger.new);
        }
        
        if(Trigger.isUpdate){
            HAQuoteDocumentTriggerHandler QuoteDocumentAfter = new HAQuoteDocumentTriggerHandler();
            QuoteDocumentAfter.ProcessDocument(Trigger.new);
        }
        
    }
}