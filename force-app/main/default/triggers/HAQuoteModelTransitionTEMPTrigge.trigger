trigger HAQuoteModelTransitionTEMPTrigge on HA_Quote_Model_Transition_TEMP__c (before Insert) {
  if(trigger.IsBefore && trigger.IsInsert){
      HAQuoteModelTransitionTEMPTriggerHandler.GetNewSKUInformation(trigger.new);
  }
}