trigger InquiryTrigger on Inquiry__c (Before insert, After insert , After Update,Before Update ) {
  if(!DataMigrationSwitch__c.getInstance(UserInfo.getProfileId()).DisableTrigger__c) {
      Id HtvRecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('HTV Inquiry').getRecordTypeId();
      Id MobileITRecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Mobile_IT').getRecordTypeId();
      
        if(trigger.IsBefore){
       if(Trigger.isInsert){
          List<Inquiry__c> trggernew=new List<Inquiry__c>();
          for(Inquiry__c i:trigger.new){
             if(i.RecordTypeId == HtvRecordTypeId || i.RecordTypeId == MobileITRecordTypeId){
                 trggernew.add(i);
             } 
          }
          InquiryTriggerHandler.beforeinsert(trggernew); 
          InquiryTriggerHandler.checkduplicaterecords(trggernew,'Insert'); 
        }
        if(Trigger.isUpdate){
              List<Inquiry__c> trggernew=new List<Inquiry__c>();
          for(Inquiry__c i:trigger.new){
             if(i.RecordTypeId == HtvRecordTypeId || i.RecordTypeId == MobileITRecordTypeId){
                 trggernew.add(i);
             } 
          }
          
            InquiryTriggerHandler.updatethestatus(trigger.new,trigger.oldMap);
            InquiryTriggerHandler.CalculatetheSLATimes(trggernew,trigger.oldMap);
            InquiryTriggerHandler.updatetimestamps(trggernew,trigger.oldMap);
            InquiryTriggerHandler.UpdateProductFamily(trggernew);
            InquiryTriggerHandler.checkduplicaterecords(trggernew,'Update'); 
        }
   }
   
   if(trigger.IsAfter){
       if(Trigger.isInsert){
            List<Inquiry__c> trggernew=new List<Inquiry__c>();
          for(Inquiry__c i:trigger.new){
             if(i.RecordTypeId == HtvRecordTypeId || i.RecordTypeId == MobileITRecordTypeId){
                 trggernew.add(i);
             } 
          }
          InquiryTriggerHandler.afterinsert(trggernew); 
        }
        if(Trigger.isUpdate){
            List<Inquiry__c> trggernew=new List<Inquiry__c>();
          for(Inquiry__c i:trigger.new){
             if(i.RecordTypeId == HtvRecordTypeId || i.RecordTypeId == MobileITRecordTypeId){
                 trggernew.add(i);
             } 
          }
           InquiryTriggerHandler.afterupdate(trggernew,trigger.oldMap); 
        }
   }
  }
  
   
}