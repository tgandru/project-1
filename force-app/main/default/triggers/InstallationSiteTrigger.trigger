/*
 * Created By: Joe Jung
 * Date: 2019.09.06
 * Description: Trigger for the Installation Site Custom Object.
 */
trigger InstallationSiteTrigger on Installation_site__c (before insert, before update, after insert, after update, before delete) {
    if(Trigger.isBefore && Trigger.isInsert) {
        InstallationSiteTriggerHandler.createNewInstallationSite(Trigger.new);
        InstallationSiteTriggerHandler.AddressValidation(Trigger.New);
    }
    if(Trigger.isBefore && Trigger.isUpdate){
        InstallationSiteTriggerHandler.updateInstallationSite(Trigger.newMap, Trigger.oldMap);
        InstallationSiteTriggerHandler.AddressValidation(Trigger.New);
    }
   	if(Trigger.isAfter && Trigger.isInSert) {
        InstallationSiteTriggerHandler.sendEmail(Trigger.newMap);
    }
    if(Trigger.isAfter && Trigger.isUpdate){
        
    }
    if(Trigger.isBefore && Trigger.isDelete) {
        InstallationSiteTriggerHandler.blockInstallationSiteDelete(Trigger.OldMap);
    }
}