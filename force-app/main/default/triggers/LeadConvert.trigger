trigger LeadConvert on Lead (before Insert, before update,after update) { 
    public static String HA_Builder_Recordtype =Schema.SObjectType.Lead.getRecordTypeInfosByName().get('HA').getRecordTypeId();
    if(!DataMigrationSwitch__c.getInstance(UserInfo.getProfileId()).DisableTrigger__c) { 
        if(Trigger.isBefore){
            LeadTriggerHandler verifyLead = new LeadTriggerHandler(Trigger.new,Trigger.oldMap);
            verifyLead.validateAddress();
            LeadTriggerHandler.trinzipcode(trigger.new);
        
        }
        if(Trigger.isAfter){
            if(Trigger.new != null && Trigger.oldMap != null) {
                LeadTriggerHandler opportunityLead = new LeadTriggerHandler(Trigger.new, Trigger.oldMap);
                opportunityLead.mapOpportunities();
            
            }
              if(Trigger.ISupdate){
                    if(LeadTriggerHandler.isFirstTime){
                            LeadTriggerHandler.isFirstTime = false;

                  
                        LeadTriggerHandler.linkconvertdetailstomql(trigger.new,trigger.oldMap); 
                    }
          } 
        }       
    }
    
       if(Trigger.isBefore){ // Added By Vijay for HA Lead Assignment Based on Zip Code - 6/7/2019
           List<Lead> triggernew=new List<Lead>();
            for(Lead l:trigger.new){
                if(l.RecordTypeId==HA_Builder_Recordtype){
                    triggernew.add(l);
                }
            }
            if(triggernew.size()>0 && Trigger.isUpdate){
               LeadTriggerHandler.HALeadAssignment(triggernew);
            }
           
       }
    
    
        // if Lead is converted 
        //PRMLeadStatusHelper.createMessageQueueForPRMIntegration(Trigger.new, Trigger.oldMap);
}