/* 
Description: Trigger for Lead product
Author: Jagan
1) Populate Lead__c field whenever Lead product is created with PRM Lead Id
2) Update Product_Information__c in lead to concatenate  leadProduct.Model_Code__c + ', ' + leadProduct.Quantity__c + ', ' + leadProduct.Requested_Price__c 
*/
trigger LeadProductTrigger on Lead_Product__c (before insert,after insert,after delete) {
    if(!DataMigrationSwitch__c.getInstance(UserInfo.getProfileId()).DisableTrigger__c) { 
        if(trigger.isBefore){
            LeadProductTriggerHandler.beforeInsert(trigger.New);
        }
        else{
            if(trigger.isInsert){
                LeadProductTriggerHandler.afterInsertDelete(trigger.New);
            }
            else if(trigger.isDelete){
                LeadProductTriggerHandler.afterInsertDelete(trigger.Old);
            }
        }
    }
}