trigger OpportunityTrigger on Opportunity (before insert, after insert,before update,after Update) {
    
    public static String HA_Builder_Recordtype =Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('HA Builder').getRecordTypeId();//Added by  vijay  to bypass  the HA record validation 
    
    if(!DataMigrationSwitch__c.getInstance(UserInfo.getProfileId()).DisableTrigger__c) {     
        if(Trigger.isBefore && Trigger.isInsert) {
            OpportunityTriggerHandler.createRollout(trigger.New, Trigger.oldMap);
            OpportunityTriggerHandler.CheckNextStepFieldUpdate(trigger.new, Trigger.oldMap,'Insert');// Added By Vijay on 8/26 to Update Next Step Value from custom to Standard

            /*
            list<opportunity> triggerNew = OpportunityTriggerHandler.OpportunityRecordsNeedToProcessInTrigger(Trigger.new);
            OpportunityTriggerHandler.createRollout(triggerNew, Trigger.oldMap);
            if(triggerNew.size() > 0){
                OpportunityTriggerHandler.createRollout(triggerNew, Trigger.oldMap);
            }
            //Added by Thiru - For HA Oppotunities
            list<opportunity> HAtriggerNew = OpportunityTriggerHandler.HAOpportunityRecordsNeedToProcessInTrigger(Trigger.new);
            if(HAtriggerNew.size()>0){
                OpportunityTriggerHandler.HAopportunitiesToProcess(HAtriggerNew);
            }
            //Added by Thiru ---Ends---
            */
            
            //Added By Joe Jung on 2019.08.08 -- Start
            HA_OpportunityTriggerHandler.insertHAShippingPlant(Trigger.New);
            //Added By Joe Jung on 2019.08.08 -- End
        }
        
        if(Trigger.isBefore && Trigger.isUpdate) {
            	System.debug('Testing Trigger.isBefore && Trigger.isUpdate');
                OpportunityTriggerHandler.CheckNextStepFieldUpdate(trigger.new, Trigger.oldMap,'Update');// Added By Vijay on 8/26 to Update Next Step Value from custom to Standard
                //Added By Joe Jung on 2019.07.15 -- start
                //If an opportunity StageName changes to 'Win', increment HA_Won_Count__c by 1 for the validation rule HA_Block_Edit_Based_On_Won_Count to be in effect.
				HA_OpportunityTriggerHandler.incrementHAWonCount(Trigger.New, Trigger.OldMap);
                //Added By Joe Jung on 2019.07.15 -- end 
                
                //Added By Joe Jung on 2019.07.30 -- start
                //To Correspondingly Update HA Shipping Plant based on the value of Shipment State field. 
                HA_OpportunityTriggerHandler.updateHAShippingPlant(Trigger.New, Trigger.OldMap);
                //Added By Joe Jung on 2019.07.30 -- end
                
                list<opportunity> triggerNew = OpportunityTriggerHandler.OpportunityRecordsNeedToProcessInTrigger(Trigger.new);
                if(triggerNew.size()>0){
                   OpportunityTriggerHandler.updateRollout(triggerNew, Trigger.oldMap);
                   PRMOpportunityStatusFacade cont=new PRMOpportunityStatusFacade();
                //Added by vijay starts 
        
                for (Opportunity opp : trigger.New){
                    if(opp.StageName =='Closed Lost'||opp.StageName =='Drop'){
                       opp.ForecastCategoryName = 'Omitted';
                    }else If(opp.StageName =='Identified'||opp.StageName =='Qualified'||opp.StageName =='Needs Analysis'||opp.StageName =='Commit'){
                             opp.ForecastCategoryName = 'Upside';
                    }else if(opp.StageName =='Negotiation'){
                             opp.ForecastCategoryName = 'Commit';
                    }else if ( opp.StageName =='Proposal'){
                             opp.ForecastCategoryName = 'Probable';
                    } else if(opp.StageName =='Closed Won'||opp.StageName =='Win'){
                             opp.ForecastCategoryName = 'Won';
                    }
                 }
   
                cont.gatherOppsFromTrigger(triggerNew, Trigger.oldMap);
            }
            //OpportunityTriggerHandler.UpdateHAquotes(trigger.newMap,trigger.oldMap);
            
        }
        
        if(trigger.isAfter && trigger.isUpdate){
            list<opportunity> triggerNew = OpportunityTriggerHandler.OpportunityRecordsNeedToProcessInTrigger(Trigger.new);
            OpportunityTriggerHandler.CheckCampaignInfluenceOnPrimaryCamp(trigger.New,trigger.oldMap);
            if(triggerNew.size()>0){
                OpportunityTriggerHandler.afterUpdate(triggerNew,trigger.oldMap);
                OpportunityTriggerHandler.UpdatePRMProjectRegistarationStation(triggerNew,trigger.oldMap);
                //OpportunityTriggerHandler.ITinstallationSiteCreateAvailabilityCheck(triggerNew,trigger.oldMap); //Added By Joe Jung 2019.09.19 for IT Installation.
            }
            //Edit Start JeongHo Lee 2017-11-03 only HA_Builder
            List<Opportunity>           haNewList   = new List<Opportunity>();
            Map<Id, Opportunity>        haOldMap    = new Map<Id, Opportunity>();
            //HA_OpportunityTriggerHandler.isFirstTime = FALSE;

            for(Opportunity opp : Trigger.new){
                system.debug('## HA Builder rollout create');
                if(opp.RecordTypeId == HA_Builder_Recordtype) haNewList.add(opp);
            }
            for(Opportunity opp : Trigger.old){
                system.debug('## HA Builder rollout create');
                if(opp.RecordTypeId == HA_Builder_Recordtype) haOldMap.put(opp.Id,opp);   
            }
            system.debug('haNewList---->'+haNewList);
            system.debug('haOldMap---->'+haOldMap);
            HA_OpportunityTriggerHandler.rolloutCreate(haNewList, haOldMap);
            //Edit End JeongHo Lee
            OpportunityTriggerHandler.UpdateHAquotes(trigger.newMap,trigger.oldMap);//Added by Thiru
            OpportunityTriggerHandler.CreateMQforSalesPortal(trigger.newMap.keySet(),trigger.oldMap,trigger.New);//Added by Thiru---for Sales portal MQ creation
            //OpportunityTriggerHandler.DeleteOppRollout(trigger.New);//Added by Thiru---09/25/2018
        }

        if (trigger.isAfter){ //SVC - after, create asset if closed won
            OpportunityTriggerHandler.After_SC_CreateAssetonClosedWon(trigger.New);
            /*if(OpportunityTriggerHandler.CheckDupOpps && !System.isFuture() && !System.isBatch()){
                 OpportunityTriggerHandler.CheckDupOpportunities(trigger.New);
            }*/
        }
    }
    if(trigger.isBefore && trigger.isUpdate){
       //OpportunityTriggerHandler.populateWonDateForExternalOpp(trigger.newMap, trigger.oldMap);//added by  vijay
    }
    
}