/*
 * Description: This Master trigger is for the Partners object. 
 * All Apex classes for the partner object must be invoked from
 * this trigger.
 *
 * Created By: Mayank S
 *
 * Created Date: Mar 02, 2016
 * 
 * Revisions: NIL
*/


trigger PartnerTrigger on Partner__c (after insert, after update, after delete) {
    
    if(!DataMigrationSwitch__c.getInstance(UserInfo.getProfileId()).DisableTrigger__c) { 
        
    if(Trigger.isAfter) {
        if (Trigger.isInsert) {
            PartnerUtilities.afterInsert(Trigger.New);
        } 
        if (Trigger.isUpdate) {
            PartnerUtilities.afterUpdate(Trigger.New, Trigger.oldMap);
        }
        if (Trigger.isDelete) {
            PartnerUtilities.afterDelete(Trigger.old);
        }
    }
    }
}