/* 
Product trigger to 
    1) To update Unit_Cost_Last_Update__c;
    2) To update Product Family and Product group;
    3) To create a pricebook entry with standard price book

Author :- Lauren
Modified by :- Jagan
*/

trigger ProductTrigger on Product2 (before insert,before update,after Insert,after update) {
    if(!DataMigrationSwitch__c.getInstance(UserInfo.getProfileId()).DisableTrigger__c) { 
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            
                ProductTriggerHandler cont=new ProductTriggerHandler();
                cont.isInsert(Trigger.new);
                //Jagan :- New method to update product family/product group
                ProductTriggerHandler.productBeforeInsertUpdate(Trigger.new);
          
        }
        if(Trigger.isUpdate){
            
                ProductTriggerHandler cont=new ProductTriggerHandler();
                cont.isUpdate(Trigger.new, Trigger.oldMap);
                //Jagan :- New method to update product family/product group
                ProductTriggerHandler.productBeforeInsertUpdate(Trigger.new);
            
        }
    }
    else{
        if(trigger.isInsert){
            
            //Insert pricebook entries after insert of product
            ProductTriggerHandler.productAfterInsert(trigger.new);
        }
        else{   //DW -SVC after update trigger
            ProductTriggerHandler.productAfterUpdate(Trigger.new,Trigger.old);
        }
    }
    }
}