trigger ProofOfPurchaseTrigger on Proof_Of_Purchase__c (after update) {
	if(Trigger.isAfter && Trigger.isUpdate) {
            SVC_PoPTriggerHandler.afterUpdate(trigger.New, Trigger.Old);
     }
}