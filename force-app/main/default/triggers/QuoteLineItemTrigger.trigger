/* 
Trigger for varios actions on QuoteLineItem
1) Update list price
Author: Jagan
*/

trigger QuoteLineItemTrigger on QuoteLineItem (before insert,before Update,After insert,After Update,After Delete) {
    if(!DataMigrationSwitch__c.getInstance(UserInfo.getProfileId()).DisableTrigger__c) { 
        if(trigger.isBefore){
            if(trigger.isInsert)
                QuoteLineItemTriggerHelper.beforeInsertUpdate(trigger.new,null);
            else
                QuoteLineItemTriggerHelper.beforeInsertUpdate(trigger.new,trigger.oldmap);
        }
        
        //Added below method for "Service Min GM" update and "Wearable Count" update on Quotes--05/09/2018
        if(trigger.isAfter){
            /*if(trigger.isInsert){
                QuoteLineItemTriggerHelper.QuoteServiceMinGMUpdate(trigger.newMap);
                QuoteLineItemTriggerHelper.QuoteWearableCountUpdate(trigger.newMap);
            }else{
                QuoteLineItemTriggerHelper.QuoteServiceMinGMUpdate(trigger.newMap);
                QuoteLineItemTriggerHelper.QuoteWearableCountUpdate(trigger.newMap);
            }*/
            if(trigger.isDelete){
                QuoteLineItemTriggerHelper.QuoteServiceMinGMUpdate(trigger.oldMap);
                QuoteLineItemTriggerHelper.QuoteWearableCountUpdate(trigger.oldMap);
            }else{
                QuoteLineItemTriggerHelper.QuoteServiceMinGMUpdate(trigger.newMap);
                QuoteLineItemTriggerHelper.QuoteWearableCountUpdate(trigger.newMap);
            }
        }
    }
    
}