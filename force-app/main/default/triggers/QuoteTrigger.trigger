trigger QuoteTrigger on Quote (before insert, before update,after update) {
    if(!DataMigrationSwitch__c.getInstance(UserInfo.getProfileId()).DisableTrigger__c) { 
    	if(Trigger.isBefore && Trigger.isInsert){
    
    		PRMOpportunityStatusFacade cont=new PRMOpportunityStatusFacade();
    		cont.gatherInsertQuotesFromTrigger(Trigger.new);
    
    	}else if(Trigger.isBefore && Trigger.isUpdate){
    		
    		PRMOpportunityStatusFacade cont2=new PRMOpportunityStatusFacade();
    		cont2.gatherUpdateQuotesFromTrigger(Trigger.new, Trigger.oldMap);
            
            //For populating the Manager_Email__c field in Quote with Quote's submitter's Manager
            User u = [select id,Managerid,Manager.email from user where id=:userinfo.getUserId()];
    		for(Quote qt : Trigger.new){
    		    Quote q=Trigger.oldmap.get(qt.id);
    		  if(qt.status=='Pending Approval'&& q.status!=qt.Status){
                if(u.Managerid!=null){
                  qt.Manager_Email__c = u.Manager.email;
                }else{
                  qt.Manager_Email__c = null;
                }
    		  }
    		}
    	}
    	
    	 if(Trigger.isAfter && Trigger.isUpdate){// Added By Vijay on 7/5 to Update CI_Opp External Id with SPA ID
    	    Set<id> oppids=new set<id>();
    	     for(Quote qt:trigger.new){
    	       Quote q=Trigger.oldmap.get(qt.id);
    	           if(q.External_SPID__c!=qt.External_SPID__c){//qt.status=='Approved'&& q.status!=qt.Status
    	               oppids.add(q.OpportunityID);
    	           }
    	      }
    	      if(!oppids.isEmpty()){
    	      PRMOpportunityStatusFacade.UpdateCIOppExternalID(oppids);
    	      }
    	 }
    	
    }
}