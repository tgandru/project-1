trigger ReleaseItemTrigger on ReleaseItem__c (after insert,after delete) {
	set<Id> ids = new Set<Id>();
    
    if(Trigger.isInsert && Trigger.isAfter){
        for(ReleaseItem__c item : Trigger.new) {
            ids.add(item.ReleaseNote__c);
        }
    } else if(Trigger.isDelete && Trigger.isAfter) {
        for(ReleaseItem__c item : Trigger.old) {
            ids.add(item.ReleaseNote__c);
        }
    }  
    
    List<ReleaseNote__c> notes = [select id, 	itemCount__c from ReleaseNote__c where id in:ids];
    for(ReleaseNote__c note : notes) {
        List<ReleaseItem__c> items = [select id from ReleaseItem__c where ReleaseNote__c=:note.Id];
        note.itemCount__c = items.size();
        update note;
    }
}