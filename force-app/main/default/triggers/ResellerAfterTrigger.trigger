trigger ResellerAfterTrigger on Reseller__c (after insert, after update) {
    if(Trigger.isAfter && Trigger.isInsert) {
        //ResellerAfterTriggerHandler cont = new ResellerAfterTriggerHandler();
        //cont.updateQuote(trigger.New);
        ResellerAfterTriggerHandler.updateQuote(trigger.New);
    }
    if(Trigger.isAfter && Trigger.isUpdate) {
        //ResellerAfterTriggerHandler cont1 = new ResellerAfterTriggerHandler();
        //cont1.updateQuote(trigger.New);
        ResellerAfterTriggerHandler.updateQuote(trigger.New);
    }
}