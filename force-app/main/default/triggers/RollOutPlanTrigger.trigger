trigger RollOutPlanTrigger on Roll_Out__c (after insert, after update) {
    
    if(!DataMigrationSwitch__c.getInstance(UserInfo.getProfileId()).DisableTrigger__c) { 
			
	    if(Trigger.isAfter && Trigger.isInsert) {
	    	//2017-10-27 Seperate HA Builder , B2B Sales
	    	List<Roll_Out__c> b2bNewList 	= new List<Roll_Out__c>();
	    	List<Roll_Out__c> hanewList 	= new List<Roll_Out__c>();
	    	
	    	for(Roll_Out__c ro : Trigger.new){
	    		if(ro.Opportunity_RecordType_Name__c == 'HA Builder') haNewList.add(ro);
	    		else b2bNewList.add(ro);
	    	}
	    	//B2B Sales
	    	if(!b2bNewList.isEmpty()) RollOutPlanTriggerHandler.createRolloutProduct(b2bNewList);
	    	//HA Sales
	    	else if(!haNewList.isEmpty()) HA_RollOutPlanTriggerHandler.createRolloutProduct(haNewList);
            
        }
        
        if(Trigger.isAfter && Trigger.isUpdate) {
        	//2017-10-27 Seperate HA Builder , B2B Sales
        	List<Roll_Out__c> 		b2bNewList 	= new List<Roll_Out__c>();
        	Map<Id, Roll_Out__c> 	b2bOldMap 	= new Map<Id, Roll_Out__c>();
	    	List<Roll_Out__c> 		haNewList 	= new List<Roll_Out__c>();
	    	Map<Id, Roll_Out__c> 	haOldMap 	= new Map<Id, Roll_Out__c>();

	    	for(Roll_Out__c ro : Trigger.new){
	    		if(ro.Opportunity_RecordType_Name__c == 'HA Builder') haNewList.add(ro);
	    		else b2bNewList.add(ro);
	    	}
	    	for(Roll_Out__c ro : Trigger.old){
	    		if(ro.Opportunity_RecordType_Name__c == 'HA Builder') haOldMap.put(ro.Id, ro);
	    		else b2bOldMap.put(ro.Id, ro);
	    	}
	    	//B2B Sales
	    	if(!b2bNewList.isEmpty() && !b2bOldMap.isEmpty()) RollOutPlanTriggerHandler.updateRolloutProduct(b2bNewList, b2bOldMap);
	    	//HA Sales
	    	//else if(!haNewList.isEmpty() && !haOldMap.isEmpty()) HA_RollOutPlanTriggerHandler.updateRolloutProduct(haNewList, haOldMap);
        }
    }

}