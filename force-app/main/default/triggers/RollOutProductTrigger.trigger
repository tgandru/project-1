trigger RollOutProductTrigger on Roll_Out_Product__c (After insert, After update) {
    public static Boolean batchJobInitiated = false;
    if(!DataMigrationSwitch__c.getInstance(UserInfo.getProfileId()).DisableTrigger__c) { 
		if(Trigger.isAfter && Trigger.isInsert) {
			//2017-10-29 Seperate HA Builder , B2B Sales
        	List<Roll_Out_Product__c> 		b2bNewList 	= new List<Roll_Out_Product__c>();
        	Map<Id, Roll_Out_Product__c> 	b2bNewMap 	= new Map<Id, Roll_Out_Product__c>();
	    	List<Roll_Out_Product__c> 		haNewList 	= new List<Roll_Out_Product__c>();
	    	Map<Id, Roll_Out_Product__c> 	haNewMap 	= new Map<Id, Roll_Out_Product__c>();

	    	for(Roll_Out_Product__c rop : Trigger.new){
	    	    	    		 

	    		if(rop.Opportunity_RecordType_Name__c == 'HA Builder'){
	    			haNewList.add(rop);
	    			haNewMap.put(rop.Id, rop);
	    		}
	    		else{
	    			b2bNewList.add(rop);
	    			b2bNewMap.put(rop.Id,rop);
	    		}
	    		
	    		
	    	
	    		
	    	}
	    	//B2b Sales
	    	if(!b2bNewList.isEmpty() && !b2bNewMap.isEmpty() && !Test.isRunningTest()) RollOutProductTriggerHandler.createRolloutSchedule(b2bNewList, b2bNewMap);
	    	//HA Sales
	    	else if(!haNewList.isEmpty() && !haNewMap.isEmpty() && !Test.isRunningTest()) HA_RollOutProductTriggerHandler.createRolloutSchedule(haNewList, haNewMap);
	    	
	    	
	    	
        }
        
        if(Trigger.isAfter && Trigger.isUpdate) {

        	//2017-10-29 Seperate HA Builder , B2B Sales
        	List<Roll_Out_Product__c> 		b2bNewList 	= new List<Roll_Out_Product__c>();
        	Map<Id, Roll_Out_Product__c> 	b2bOldMap 	= new Map<Id, Roll_Out_Product__c>();
	    	for(Roll_Out_Product__c rop : Trigger.new){
	    		if(rop.Opportunity_RecordType_Name__c != 'HA Builder') b2bNewList.add(rop);
	    		
	    		
	    	}
	    	for(Roll_Out_Product__c rop : Trigger.old){
	    		if(rop.Opportunity_RecordType_Name__c != 'HA Builder') b2bOldMap.put(rop.Id, rop);
	    	}
	    	//B2B Sales
	    	system.debug('ROP old map---------------->'+b2bNewList.size());
	    	system.debug('ROP old map---------------->'+b2bOldMap.size());
	    	if(!b2bNewList.isEmpty() && !b2bOldMap.isEmpty() && !Test.isRunningTest()) RollOutProductTriggerHandler.updateRolloutSchedule(b2bNewList, b2bOldMap);
	    	//HA Sales
	    	//else if(!haNewList.isEmpty() && !haOldMap.isEmpty() && !Test.isRunningTest()) HA_RollOutProductTriggerHandler.updateRolloutSchedule(haNewList, haOldMap);
	    	
        }   
        
        if(Trigger.isAfter && Trigger.isUpdate){// Added by Vijay 4/2/2018 - To Populate the ASP value into ROS and Opp header based on Custom object data SKU ASP
            	List<id> b2bMblLst 	= new List<id>();
            	for(Roll_Out_Product__c rop : Trigger.new){
	    	     	if(rop.Opportunity_RecordType_Name__c == 'Mobile'){
	    		    system.debug('Record Type Name =='+rop.Opportunity_RecordType_Name__c );
	    		    b2bMblLst.add(rop.id);
	    	     	}
	         	}
	    	
	    	if(b2bMblLst.size()>0 && !system.isBatch() && !system.isFuture()&& Limits.getQueueableJobs() == 0){
	    	    system.debug('Job in Process ::::'+batchJobInitiated);
	    	      batchJobInitiated = true;
	    	     System.enqueueJob(new UpdateASPvaluefromRP(b2bMblLst));//Queunig the Future Method to Update the ASP Data 
	    	   
	    	}
	    }
        
        
    }
}