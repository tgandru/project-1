/*Created by Thiru on 04-08-2019.*/
trigger SBQQ_QuoteLineTrigger on SBQQ__QuoteLine__c (after insert, after update, before delete) {
    if(Trigger.isInsert){
        SBQQ_QuoteLineTriggerHandler.createRolloutProduct(trigger.new);
        SBQQ_QuoteLineTriggerHandler.checkDuplicateSKUs(trigger.new);//06-26-2019..Thiru::Added this Method to prevent duplicate products adding to HA Quote
    }
    
    if(Trigger.isUpdate){
        
        SBQQ_QuoteLineTriggerHandler.checkDuplicateSKUs(trigger.new);//06-26-2019..Thiru::Added this Method to prevent duplicate products adding to HA Quote
        
        Map<id,SBQQ__QuoteLine__c> newMap = new Map<id,SBQQ__QuoteLine__c>();
        Map<id,SBQQ__QuoteLine__c> oldMap = trigger.oldMap;
        
        for(SBQQ__QuoteLine__c qli : trigger.new){
            if((qli.Package_Option__c=='Base' && qli.SBQQ__Quantity__c!=oldMap.get(qli.id).SBQQ__Quantity__c) || qli.Package_Option__c!=oldMap.get(qli.id).Package_Option__c){
                newMap.put(qli.id, qli);
            }
        }
        if(newMap.size()>0){
            SBQQ_QuoteLineTriggerHandler.updateRolloutProduct(newMap,oldMap);
        }
    }
    
    if(Trigger.isBefore && Trigger.isDelete){
        SBQQ_QuoteLineTriggerHandler.deleteRolloutProduct(trigger.oldMap);
    }
}