/**
 * Created by ms on 2017-10-25.
 *
 * author : JeongHo.Lee, I2MAX
 */
trigger SBQQ_QuoteTrigger on SBQQ__Quote__c (after update, before delete) {
	if(!DataMigrationSwitch__c.getInstance(UserInfo.getProfileId()).DisableTrigger__c && SBQQ_QuoteTriggerHandler.isFirstTime ==true) { 

		SBQQ_QuoteTriggerHandler.isFirstTime = false;
        
        if(Trigger.isAfter && Trigger.isUpdate) {
            SBQQ_QuoteTriggerHandler.rolloutCreate(Trigger.new,Trigger.newMap,Trigger.oldMap);
        }
        /*if(Trigger.isBefore && Trigger.isDelete){
        	Map<Id, SBQQ__Quote__c> qOldMap = new Map<Id, SBQQ__Quote__c>();
        	for(SBQQ__Quote__c sb : Trigger.old){
        		if(sb.SBQQ__Primary__c == TRUE) qOldMap.put(sb.Id,sb);
        	}
        	if(!qOldMap.isEmpty()) SBQQ_QuoteTriggerHandler.rolloutDelete(qOldMap);
        }*/
    }
}