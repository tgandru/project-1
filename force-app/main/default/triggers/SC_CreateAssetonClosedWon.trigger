trigger SC_CreateAssetonClosedWon on Opportunity (after insert, after update) {
    
  List<Asset> ast = new List<Asset>();
    List<Opportunity> opp = new List<Opportunity>();
    
  Set<Id> opptyIds = new Set<Id>();
    for(Opportunity o: trigger.new){ 
        if(o.isWon == true && o.HasOpportunityLineItem == true){
            String opptyId = o.Id;
            opptyIds.add(o.id);
            opp.add(o);
        }
    }

  list<OpportunityLineItem> OLI = [Select UnitPrice, Quantity, PricebookEntry.Product2Id, 
  PricebookEntry.Product2.Name, Description,opportunity.id,  opportunity.accountId,opportunity.closeDate
                                  From OpportunityLineItem 
                                  where OpportunityId = :opptyIds and PricebookEntry.Product2.Product_Category__c='SBS' ];
     
     for(OpportunityLineItem ol: OLI){
        Asset a = new Asset();
        a.AccountId = ol.opportunity.AccountId;
        a.Product2Id = ol.PricebookEntry.Product2Id;
        a.Quantity = ol.Quantity;
        a.Price =  ol.UnitPrice;
        a.PurchaseDate = ol.opportunity.CloseDate;
        a.Status = 'Purchased';
        a.Description = ol.Description;
        a.Name = ol.PricebookEntry.Product2.Name;
        ast.add(a);
      }

    if(ast.size()>0){     
        insert ast;
    }  
}