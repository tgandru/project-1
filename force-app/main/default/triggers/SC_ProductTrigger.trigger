trigger SC_ProductTrigger on Product2 (after update) {

    Map<id,string> changedProductMap = new Map<id,string>();
    Map<ID, Product2> oldAccMap = new Map<ID,Product2>(Trigger.old);
    for (Product2 prod: Trigger.new) {
        if (  (oldAccMap.get(prod.Id).sc_service_type_ranking__c != prod.sc_service_type_ranking__c) )  {
            changedProductMap.put(prod.id, prod.sc_service_type_ranking__c);                   
        }
    }
    system.debug(changedProductMap);
    if (changedProductMap.size()>0){

        List<Entitlement> entitlements = [ select id, sc_service_type_ranking__c,SC_Product_ID__c 
      from entitlement where is_trial__c != true and
            SC_Product_ID__c in :changedProductMap.keySet()]; //trial entitlement ranking always 10
        for (entitlement e:entitlements){
             e.sc_service_type_ranking__c = Integer.valueof(changedProductMap.get(e.SC_Product_ID__c));
        }
        update entitlements;    
    }
}