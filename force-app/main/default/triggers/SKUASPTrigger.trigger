trigger SKUASPTrigger on SKU_ASP__c (Before Insert,before update,after insert,after update ) {
if(!DataMigrationSwitch__c.getInstance(UserInfo.getProfileId()).DisableTrigger__c) { 
  If(trigger.isBefore&&(trigger.IsInsert || trigger.isUpdate)){
      SKUASPTriggerHandler.checkskuisavibility(trigger.new);
  }
  
   If(trigger.isAfter&&(trigger.IsInsert || trigger.isUpdate)){
      SKUASPTriggerHandler.updaterespectiveschedules(trigger.new);
  }
}
}