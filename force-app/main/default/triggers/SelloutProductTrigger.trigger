trigger SelloutProductTrigger on Sellout_Products__c (before insert,before update) {
  if(trigger.isBefore && (Trigger.isInsert || trigger.isUpdate)){
      SelloutProductTriggerHandler.checkskuisavibility(Trigger.new);
     SelloutProductTriggerHandler.identifyDuplicaterecords(trigger.new); // This Method is to Update the invoice Price 
     //SelloutProductTriggerHandler.updateInvoicePrice(trigger.new); // This Method is to Update the invoice Price 
  }
}