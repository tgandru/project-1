trigger SelloutTrigger on Sellout__c (after Update,before Update) {
    if(trigger.isAfter){
        
        if(trigger.isUpdate){
            
                SelloutTriggerHandler.deleteAttachementfromTemplate(trigger.new,trigger.oldmap);// To Delete the Attachement related to Template
        }
    }else{
        
        if(trigger.isUpdate){
            
                SelloutTriggerHandler.UpdateComments(trigger.new,trigger.oldmap);// To Update Comments Box
        }
    }
  
}