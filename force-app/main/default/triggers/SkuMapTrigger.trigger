trigger SkuMapTrigger on Sellout_SKU_Mapping__c (before insert,before update) {
  
  if(trigger.IsBefore){
      SkuMapTriggerHelper.checkskuisavibility(trigger.new);
  }
}