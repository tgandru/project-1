/*
 * Description: This Master trigger is for the Task object. 
 * All Apex classes for the Task object must be invoked from
 * this trigger.
 *
 * Created By: Mayank S
 *
 * Created Date: Mar 08, 2016
 * 
 * Revisions: NIL
*/

trigger TaskTrigger on Task (after insert,after update,before insert,before update) {
    if(!DataMigrationSwitch__c.getInstance(UserInfo.getProfileId()).DisableTrigger__c) { 
	    if(Trigger.isAfter) {
	        if(Trigger.isInsert) {
	            TaskUtilities.changeQuoteStatusOnSendEmail(Trigger.New);
	            Set<Id> ids = new Set<Id>();
				for(Task ta : Trigger.new){
					ids.add(ta.Id);
				}
				SVC_GCICFeedItem.insertFeedItem(ids, 'Task');
	        }
	        TaskUtilities.svc_CompleteMileStones(Trigger.New);
	         String currentuserprfid=UserInfo.getProfileId();
	         String integrationids=Label.Integration_ProfileIDs;
	        
	        if(TaskUtilities.isFirstTime && (!integrationids.contains(currentuserprfid)) ){
	           TaskUtilities.isFirstTime = false;
	           TaskUtilities.UpdateOpportunityUpdateDate(Trigger.New); 
	        }
	        
	    }
	    
	    if(trigger.isBefore && trigger.isInsert){
	        TaskUtilities.copyactivitytoinquiry(Trigger.New);// Added By Vijay 4/18/2018 for M* Inquiry Process
	    }
	    
	    if(trigger.isBefore && (trigger.isInsert || trigger.isUpdate)){
	        TaskUtilities.CopyTypeFromActivityType(trigger.New,trigger.oldMap);// Added By Vijay on 9/18/2018 - To Make In Sync of Type and Activity Type fields
	    }
    }
}