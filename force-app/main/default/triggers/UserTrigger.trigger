trigger UserTrigger on User ( before insert, after insert, before update, after update) {
    
    UserTriggerHandler handler = new UserTriggerHandler();
   
    if(Trigger.isAfter) {
        handler.onAfter(Trigger.new);
    }
    else if(Trigger.isBefore && Trigger.isUpdate) {
        handler.onBeforeUpdate(Trigger.newMap, Trigger.oldMap);
    }
}