trigger contacttrigger on Contact(before delete, after Update){
    profile sysAdmin = [select id, Name from profile where name= 'System Administrator' limit 1];
  
    
    if(trigger.isDelete && trigger.isBefore){
          List<profile> salesleader = [select id, Name from profile where (name= 'B2B Sales Leader - Mobile' OR name= 'B2B Sales Leader - IT') limit 2];
        if(sysAdmin.id != userInfo.getProfileId() && salesleader[0].id != userInfo.getProfileId() &&  salesleader[1].id != userInfo.getProfileId()){ // If logged in user is not a system admin, then only we have to execute below logic
           
            contacttriggerhandler.beforedelete(trigger.old);
        }
	}
	if(trigger.isAfter && trigger.isUpdate){
	   // contacttriggerhandler.afterupdate(trigger.new,trigger.oldMap);
	    
		List<Contact> conList = new List<Contact>(); 
        for(Contact con : Trigger.new){
            Contact oldc = Trigger.oldMap.get(con.Id); 
            if(con.BP_Number__c != NULL){
                conList.add(con);
            }
        }
        system.debug(conList.size());
        if(!Test.isRunningTest() && conList.size() == 1){
            SVC_GCICBpUpdation.conValidation(conList.get(0));
        } 
	}
}