trigger mesageQueueTrigger on message_queue__c (before insert, before update,after insert,after update) {
    
    if(trigger.isBefore &&  trigger.isUpdate){
        
         for(message_queue__c nmq : trigger.new){
               if(nmq.Status__c=='failed-retry' && nmq.Object_Name__c=='Credit Memo'){
                   nmq.Status__c='failed';
               }
               
               /*for(message_queue__c omq : trigger.old){
                   if(nmq.Id==omq.id){
                       if(nmq.MSGUID__c != omq.MSGUID__c ){
                           string er='';
                           if(omq.ERRORTEXT__c !=null){
                               Datetime dateGMT=System.now();
                               Datetime d1=Datetime.valueOf(dateGMT);
                               string crtime=d1.format();
                               er= 'GUID :-'+omq.MSGUID__c+'--'+crtime+'\n'+'Error  :- '+omq.ERRORTEXT__c;
                           }
                           //string er= 'GUID :-'+omq.MSGUID__c+'\n'+'Error  :- '+omq.ERRORTEXT__c;
                           nmq.ERRORTEXT__c=nmq.ERRORTEXT__c+'\n'+er;
                       }
                   }
               }*/
           }
          for(message_queue__c mq : trigger.new){
             if(mq.Status__c=='success'&& mq.Object_Name__c == 'Account'){
               mq.ERRORTEXT__c = null;
               //&& mq.retry_counter__c<>0
             }
        }
        
    }
    
    
   if((trigger.isInsert || trigger.isUpdate)&& trigger.isAfter){
        map<string, message_queue__c> mqForAcc = new map<string,message_queue__c>();
        for(message_queue__c mq : trigger.new){
            if(mq.Integration_Flow_Type__c == '003' && mq.Object_Name__c == 'Account' && (mq.Status__c == 'failed-retry' ||mq.Status__c == 'failed' || mq.Status__c=='success') ){
                mqForAcc.put(mq.Identification_Text__c,mq);
            }
        }
       
        if(mqForAcc.size() > 0){
            List<Account> accToUpdate = new list<Account>([select id, duplicate_code__c from Account where id in:mqForAcc.keySet()]);
            for(Account acc : accToUpdate){
                if(mqForAcc.get(acc.id) != null){
                    message_queue__c mq = mqForAcc.get(acc.id);
                    if(mq.Status__c == 'failed-retry' ||mq.Status__c == 'failed'){
                        String msg= 'Account ID is missing due to '+mq.ERRORTEXT__c+'.  Please raise a Case to our Admin Team.';
                        if(acc.duplicate_code__c != msg){
                             acc.duplicate_code__c = 'Account ID is missing due to '+mq.ERRORTEXT__c+'.  Please raise a Case to our Admin Team.';
                        }
                      
                    }else if(mq.Status__c=='success'){
                        acc.duplicate_code__c = null;
                     }
                }
            }
            if(accToUpdate.size() > 0){
              update accToUpdate;
            }
        }
    }
}