trigger oliTrigger on OpportunityLineItem (before insert,after insert, before update, before delete,after update, after delete) {
    
    if(!DataMigrationSwitch__c.getInstance(UserInfo.getProfileId()).DisableTrigger__c) { 

        if(Trigger.isBefore && Trigger.isInsert) {
            OliTriggerHelper.BeforeInsertHelper(Trigger.new);
        }
        
        if(Trigger.isAfter && Trigger.isInsert) {
            OliTriggerHelper.AfterInsertHelper(Trigger.new);
        }
        
        if(Trigger.isBefore && Trigger.isUpdate) {
            OliTriggerHelper.beforeUpdateHelper(Trigger.newMap, Trigger.oldMap);
        }

        if(Trigger.isBefore && Trigger.isDelete) {
            OliTriggerHelper.deleteHelper(Trigger.oldMap);
        }
        
        if(trigger.isAfter){
            OliTriggerHelper.calculateNoOfDevices(trigger.newMap, trigger.oldMap, trigger.isDelete);
            //Added by Thiru--Method to store the Deleted OLIs to Deleted_Record_History Object--Added 03/07/2019
            if(trigger.isDelete){
                OliTriggerHelper.storeDeletedOLI(trigger.old);
            }
        }
    }
    
}